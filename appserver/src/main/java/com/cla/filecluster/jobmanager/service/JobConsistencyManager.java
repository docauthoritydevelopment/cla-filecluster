package com.cla.filecluster.jobmanager.service;

import com.cla.common.domain.dto.crawler.JobCompletionStatus;
import com.cla.common.domain.dto.crawler.JobStateByDate;
import com.cla.common.domain.dto.crawler.RunStatus;
import com.cla.common.domain.dto.jobmanager.*;
import com.cla.common.domain.dto.messages.IngestResultMessage;
import com.cla.common.domain.dto.security.SystemSettingsDto;
import com.cla.common.utils.LoggingUtils;
import com.cla.connector.utils.Pair;
import com.cla.connector.utils.TimeSource;
import com.cla.eventbus.EventBus;
import com.cla.eventbus.Topics;
import com.cla.eventbus.events.Context;
import com.cla.eventbus.events.Event;
import com.cla.eventbus.events.EventKeys;
import com.cla.eventbus.events.handler.ErrorType;
import com.cla.filecluster.config.security.AuthenticationHelper;
import com.cla.filecluster.config.security.AutoAuthenticate;
import com.cla.filecluster.domain.entity.audit.AuditType;
import com.cla.filecluster.domain.entity.crawler.CrawlRun;
import com.cla.filecluster.domain.entity.dataCenter.CustomerDataCenter;
import com.cla.filecluster.domain.entity.filetree.RootFolder;
import com.cla.filecluster.jobmanager.domain.entity.*;
import com.cla.filecluster.mediaproc.AsyncIngestProducerService;
import com.cla.filecluster.mediaproc.ScanDoneConsumer;
import com.cla.filecluster.mediaproc.TaskProducingStrategySelector;
import com.cla.filecluster.mediaproc.connectors.MediaProcessorCommService;
import com.cla.filecluster.service.audit.AuditAction;
import com.cla.filecluster.service.audit.EventAuditService;
import com.cla.filecluster.service.crawler.FileCrawlerExecutionDetailsService;
import com.cla.filecluster.service.crawler.analyze.AnalyzeFinalizeService;
import com.cla.filecluster.service.crawler.ingest.IngestFinalizationService;
import com.cla.filecluster.service.crawler.scan.ScanFinalizeService;
import com.cla.filecluster.service.files.filetree.DocStoreService;
import com.cla.filecluster.service.scheduling.ControlledScheduling;
import com.cla.filecluster.service.security.SystemSettingsService;
import com.cla.filecluster.service.security.UserService;
import com.cla.filecluster.service.syscomponent.SysComponentService;
import com.cla.filecluster.util.query.DBTemplateUtils;
import com.google.common.collect.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.event.Level;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionDefinition;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.cla.common.domain.dto.jobmanager.JobType.*;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.partitioningBy;

/**
 * Manages jobs and tasks consistency<br/>
 * Retry failed tasks, take care of stuck tasks etc.
 * <p>
 * Created by vladi on 5/10/2017.
 */
@Service
public class JobConsistencyManager implements ControlledScheduling {

    private final static Logger logger = LoggerFactory.getLogger(JobConsistencyManager.class);

    private TimeSource timeSource = new TimeSource();

    @Autowired
    private EventBus eventBus;

    @Autowired
    private JobManagerService jobManager;

    @Autowired
    private JobManagerAppService jobManagerAppService;

    @Autowired
    private JobCoordinator jobCoordinator;

    @Autowired
    private DBTemplateUtils DBTemplateUtils;

    @Autowired
    private SysComponentService sysComponentService;

    @Autowired
    private FileCrawlerExecutionDetailsService runService;

    @Autowired
    private MediaProcessorCommService mediaProcessorCommService;

    @Autowired
    private AsyncIngestProducerService asyncIngestProducerService;

    @Autowired
    private IngestFinalizationService ingestFinalizationService;

    @Autowired
    private DocStoreService docStoreService;

    @Autowired
    private ScanFinalizeService scanFinalizeService;

    @Autowired
    private AnalyzeFinalizeService analyzeFinalizeService;

    @Autowired
    private UserService userService;

    @Autowired
    private TaskProducingStrategySelector taskProducingStrategySelector;

    @Autowired
    private EventAuditService eventAuditService;

    @Autowired
    private SystemSettingsService systemSettingsService;

    @Autowired
    private ScanDoneConsumer scanDoneConsumer;

    @Value("${job-manager.jobs.in-progress-timeout-sec:3600}")
    private int inProgressJobTimeoutSec;

    @Value("${job-manager.tasks.enqueued-timeout-sec:600}")
    private int enqueuedTaskTimeoutSec;

    @Value("${job-manager.tasks.handle-restart.init-delay-millis:60000}") 
    private int handleRestartDelayMs;

    @Value("${job-manager.tasks.max-retries:3}")
    private int maxTaskRetries;

    @Value("${job-manager.jobs.advertise.threshold.sec:60}")
    private int pausedJobsUpdatedThresholdSec;

    @Value("${is.toolExecutionMode:false}")
    private boolean isToolExecutionMode;

    @Value("${job-manager.debug-cached-jobs-map:false}")
    private boolean debugCachedJobsMap;

    @Value("${media-processor.resume-throttle-run.period:60000}")
    private long resumeThrottledRunPeriod;

    @Value("${job-manager.resume-job-good-datacenter:true}")
    private boolean resumeJobActiveDatacenter;

    @Value("${job-manager.pause-job-bad-datacenter:true}")
    private boolean pauseJobsFaultyDatacenter;

    @Value("${job-manager.jobs-require-datacenter:SCAN,SCAN_FINALIZE,INGEST}")
    private String[] jobTypesRequireMPStr;

    @Value("${job-manager.handle-stuck-in-pause-runs.retry-threshold-days:1}")
    private int pauseRunRetryThresholdDays;

    @Value("${job-manager.handle-stuck-in-pause-runs.on:true}")
    private boolean pauseRunHandlingOn;

    private boolean restartHandled = false;
    private long systemStartTime;
    private int pauseRunStopThresholdDays;

    private ReentrantReadWriteLock pauseLock = new ReentrantReadWriteLock(true);
    private String pauseLockOwner;
    private Set<Long> pausedJobIds = Sets.newHashSet();
    private boolean handleRestartScheduled = false;

    private Map<Long, ResumeRunTask> throttlePausedRunMap = Maps.newConcurrentMap();
    private ScheduledExecutorService resumeThrottlePausedExecutor = Executors.newSingleThreadScheduledExecutor();

    private Map<Long, Boolean> dataCenterActiveMediaProcessor = new HashMap<>();
    private Boolean solrActive = null;
    private List<JobType> jobTypesRequireMP;
    private boolean isSchedulingActive = false;

    public boolean isSchedulingActive() {
        return isSchedulingActive;
    }

    public void setSchedulingActive(boolean schedulingActive) {
        isSchedulingActive = schedulingActive;
    }

    @PostConstruct
    public void init() {
        List<SystemSettingsDto> settings = systemSettingsService.getDefaultSetting("handle-stuck-in-pause-runs.stop-threshold-days");
        pauseRunStopThresholdDays = Integer.parseInt(settings.get(0).getValue());
        systemStartTime = timeSource.currentTimeMillis();
        eventBus.subscribe(Topics.KVDB_ERRORS, this::handleKvdbErrorEvent);
        // handle paused jobs
        DBTemplateUtils.doInTransaction(() -> {
            List<SimpleJob> pausedJobs = jobManager.getIdleJobs(systemStartTime, JobState.PAUSED);
            pausedJobIds = pausedJobs == null ? new HashSet<>() : pausedJobs.stream().map(Job::getId).collect(Collectors.toSet());
        });

        jobTypesRequireMP = Arrays.stream(jobTypesRequireMPStr).map(s -> JobType.valueOf(s)).collect(Collectors.toList());
        logger.info("Init: resumeJobActiveDatacenter={}, pauseJobsFaultyDatacenter={}, jobTypesRequireMP={}",
                resumeJobActiveDatacenter, pauseJobsFaultyDatacenter,
                jobTypesRequireMP.stream().map(t->t.toString()).collect(Collectors.joining(",")));
    }

    private void handleKvdbErrorEvent(Event event) {
        Context eventContext = event.getContext();
        ErrorType errorType = eventContext.get(EventKeys.ERROR_TYPE, ErrorType.class);
        String errorMsg = eventContext.get(EventKeys.ERROR_MESSAGE, String.class);
        logger.error("Handling a KVDB error event of type {}. The event's error message: {}", errorType, errorMsg);

        Long runId = eventContext.get(EventKeys.RUN_ID, Long.class);
        if (runId != null) {
            logger.error("Pausing run due to KVDB error event");
            pauseRun(runId, PauseReason.KVDB_ERROR);
        }
    }

    @SuppressWarnings("unused")
    @EventListener
    public void handleContextRefresh(ContextRefreshedEvent event) {
        if (handleRestartScheduled) {
            logger.warn("handleRestart already scheduled. (after {} event)", event.getClass().toString());
            return;
        }
        handleRestartScheduled = true;
        if (isToolExecutionMode) {
            logger.debug("ToolExecutionMode: no handleRestart. (after {} event)", event.getClass().toString());
        } else {
            logger.debug("Scheduling handleRestart in {} ms. (after {} event)", handleRestartDelayMs, event.getClass().toString());
            ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
            scheduler.schedule(() -> AuthenticationHelper.doWithAuthentication(
                    AutoAuthenticate.ADMIN, false, Executors.callable(this::handleRestart), userService),
                    handleRestartDelayMs, TimeUnit.MILLISECONDS);
        }
    }

    @Scheduled(initialDelayString = "${job-manager.handle-stuck-tasks-init-millis:10000}",
            fixedRateString = "${job-manager.handle-stuck-tasks-rate-millis:60000}")
    @ConditionalOnProperty(value = "is.toolExecutionMode", havingValue = "false", matchIfMissing = true)
    public void handleStuckEntities() {
        if (!isSchedulingActive) {
            return;
        }

        logger.trace("start running handleStuckEntities");

        DBTemplateUtils.doInTransaction(this::handleStuckEnqueuedTasks);
        DBTemplateUtils.doInTransaction(this::handleStuckInProgressJobs);
        DBTemplateUtils.doInTransaction(this::handleStuckInRunningRuns);
        DBTemplateUtils.doInTransaction(this::checkMediaProcessorStatus);
        DBTemplateUtils.doInTransaction(this::checkSolrStatus);

        jobManager.cleanPhaseRunningRateMap();
    }

    @Scheduled(fixedRateString = "${job-manager.handle-stuck-in-waiting-jobs.rate-millis:600000}")
    @ConditionalOnProperty(value = "is.toolExecutionMode", havingValue = "false", matchIfMissing = true)
    public void handleStuckInWaitingJobs() {
        if (!isSchedulingActive) {
            return;
        }

        List<SimpleJob> currentlyWaitingJobs = jobManager.getIdleJobs(timeSource.currentTimeMillis(), JobState.WAITING);

        // Save one waiting Analyze-Fin job (if any) to be run once for all
        // Analyze-Fin jobs (there's no need to run Analyze-Fin for each job)
        Optional<SimpleJob> oneWaitingAnalyzeFinJob =
                currentlyWaitingJobs.stream().
                        filter(simpleJob -> simpleJob.getType() == ANALYZE_FINALIZE).
                        findFirst();

        if (!jobCoordinator.isLockHeld()) {
            // kick-start all non Analyze-Fin jobs first
            for (SimpleJob waitingJob : currentlyWaitingJobs) {
                if (waitingJob.getType() != ANALYZE_FINALIZE) {
                    resumeAwaitingIdleJob(waitingJob);
                }
            }
            // kick start one (possibly) waiting Analyze-Fin job at he end to reduce coordination lock contention
            oneWaitingAnalyzeFinJob.ifPresent(this::resumeAwaitingIdleJob);
        }
    }

    @Scheduled(initialDelayString = "${job-manager.handle-stuck-in-pause-runs.init-millis:300000}",
            fixedRateString = "${job-manager.handle-stuck-in-pause-runs.rate-millis:15000000}")
    @ConditionalOnProperty(value = "is.toolExecutionMode", havingValue = "false", matchIfMissing = true)
    public void handledPausedRuns() {
        if (!isSchedulingActive) {
            return;
        }

        if (!pauseRunHandlingOn) return;

        Boolean wasChange = false;
        List<CrawlRun> runs = runService.getCrawlRunsByStatus(RunStatus.PAUSED);
        for (CrawlRun run : runs) {
            try {
                // if time from first pause is over a week - stop run gracefully
                if (run.getTimeFirstPause() != null && timeSource.daysSince(run.getTimeFirstPause()) >= pauseRunStopThresholdDays) {
                    logger.debug("stopping paused run {}", run.getId());
                    stopRunGraceful(run.getId(), PauseReason.SYSTEM_INITIATED);
                    wasChange = true;
                    eventAuditService.audit(AuditType.SCAN, "Stop run gracefully", AuditAction.STOP_GRACEFUL, CrawlRun.class, run.getId());
                }

                // if time from last pause is more than a day and was not user initiated - retry
                else if (run.getStopTime() != null && timeSource.daysSince(run.getStopTime().getTime()) >= pauseRunRetryThresholdDays &&
                        !PauseReason.USER_INITIATED.equals(run.getPauseReason())) {
                    logger.debug("resuming paused run {}", run.getId());
                    resumeRun(run.getId());
                    wasChange = true;
                    eventAuditService.audit(AuditType.SCAN, "Resume run", AuditAction.RESUME, CrawlRun.class, run.getId());
                }
            } catch (Exception e) {
                logger.error("problem handling paused run {}", run.getId(), e);
            }
        }

        if (wasChange) {
            jobManagerAppService.markStatusChange();
        }
    }

    private boolean isSolrUp() {
        return sysComponentService.isSolrWorking();
    }

    private void checkSolrStatus() {
        try {
            // get solr status
            Boolean isSolrUp = isSolrUp();
            logger.debug("checkSolrStatus component state is up {}", isSolrUp);

            // if status was changed
            if (solrActive == null || !solrActive.equals(isSolrUp)) {
                if (isSolrUp) {
                    logger.debug("Solr is back up: looking for jobs to resume...");
                } else {
                    logger.debug("Solr is down: looking for jobs to pause...");
                }

                // get runs
                List<RunStatus> statuses = new ArrayList<>();
                statuses.add(RunStatus.RUNNING);
                statuses.add(RunStatus.PAUSED);
                List<CrawlRun> runs = runService.getCrawlRunsByStatus(statuses);
                if (runs == null || runs.isEmpty()) return;

                if (isSolrUp) {
                    runs.forEach(crawlRun -> {
                        if (crawlRun.getRunStatus().equals(RunStatus.PAUSED) && PauseReason.SOLR_DOWN.equals(crawlRun.getPauseReason())) {
                            logger.info("Resuming run {} after solr is back up", crawlRun.getId());
                            resumeRun(crawlRun.getId());
                        }
                    });
                } else {
                    runs.forEach(crawlRun -> {
                        if (crawlRun.getRunStatus().equals(RunStatus.RUNNING)) {
                            logger.info("Suspending run {} as Solr is down", crawlRun.getId());
                            pauseRun(crawlRun.getId(), PauseReason.SOLR_DOWN);
                        }
                    });
                }
            }
            solrActive = isSolrUp;
        } catch (Exception e) {
            logger.error("Failed checkSolrStatus", e);
        }
    }

    private void checkMediaProcessorStatus() {
        try {

            // get runs
            List<RunStatus> statuses = new ArrayList<>();
            statuses.add(RunStatus.RUNNING);
            statuses.add(RunStatus.PAUSED);
            List<CrawlRun> runs = runService.getCrawlRunsByStatus(statuses);
            if (runs == null || runs.isEmpty()) return;
            Map<Long, CrawlRun> runById = runs.stream().collect(Collectors.toMap(CrawlRun::getId, Function.identity()));

            // get relevant root folders
            List<Long> rootFolderIds = runs.stream().map(CrawlRun::getRootFolderId).filter(Objects::nonNull).collect(Collectors.toList());
            if (rootFolderIds == null || rootFolderIds.size() == 0) {
                return;
            }
            List<RootFolder> rootFolders = docStoreService.getRootFoldersCached(rootFolderIds);
            if (rootFolders == null || rootFolders.size() == 0) {
                return;
            }

            // get data center ids
            Set<Long> dataCenterIds = rootFolders.stream().map(rf -> rf.getCustomerDataCenter().getId()).collect(Collectors.toSet());

            // for each data center
            dataCenterIds.forEach(dc -> {

                // get if has active media processors or not
                boolean hasActiveMP = sysComponentService.doesCustomerDataCenterHaveActiveMediaProcessor(dc);
                Boolean lastVal = dataCenterActiveMediaProcessor.get(dc);

                // if status was changed
                if (lastVal == null || !lastVal.equals(hasActiveMP)) {
                    if (hasActiveMP) {
                        if (resumeJobActiveDatacenter) {
                            rootFolders.forEach(rf -> {
                                if (rf.getCustomerDataCenter().getId() == dc) {
                                    CrawlRun crawlRun = runById.get(rf.getLastRunId());
                                    if (crawlRun != null && crawlRun.getRunStatus().equals(RunStatus.PAUSED) && PauseReason.NO_AVAILABLE_MEDIA_PROCESSOR.equals(crawlRun.getPauseReason())) {
                                        logger.info("Resuming run {} - data center has available media processors", crawlRun.getId());
                                        resumeRun(crawlRun.getId());
                                    }
                                }
                            });
                        }
                    } else {
                        if (pauseJobsFaultyDatacenter) {
                            rootFolders.forEach(rf -> {
                                if (rf.getCustomerDataCenter().getId() == dc) {
                                    CrawlRun crawlRun = runById.get(rf.getLastRunId());
                                    if (crawlRun != null && crawlRun.getRunStatus().equals(RunStatus.RUNNING)) {
                                        JobType type = jobManager.getRunJobActive(crawlRun.getId());
                                        if (jobTypesRequireMP.contains(type)) {
                                            logger.info("Suspending run {} - no available media processors in data center", crawlRun.getId());
                                            pauseRun(crawlRun.getId(), PauseReason.NO_AVAILABLE_MEDIA_PROCESSOR);
                                        }
                                    }
                                }
                            });
                        }
                    }
                }
                dataCenterActiveMediaProcessor.put(dc, hasActiveMP);
            });
        } catch (Exception e) {
            logger.error("failed checkMediaProcessorStatus", e);
        }
    }

    private void resumeAwaitingIdleJob(SimpleJob waitingJob) {
        jobManager.updateJobState(waitingJob.getId(), JobState.IN_PROGRESS, null, "Resuming idle waiting job", false);
        SimpleJobDto waitingJobDto = JobManagerService.simpleJobToDto(waitingJob);
        kickStartJob(waitingJobDto, false);
    }

    @Scheduled(initialDelayString = "${job-manager.advertise-mp-state.init-millis:30000}",
            fixedRateString = "${job-manager.advertise-mp-state.rate-millis:300000}")
    @ConditionalOnProperty(value = "is.toolExecutionMode", havingValue = "false", matchIfMissing = true)
    public void advertiseState() {
        if (debugCachedJobsMap && logger.isDebugEnabled()) {
            logger.debug("cachedJobsMap={}", jobManager.cachedJobsMapToString());
        }
        // TODO: should advertise upon state change and not periodically IR1008
        DBTemplateUtils.doInTransaction(this::sendJobsStates);
    }

    public void pauseAllJobs(PauseReason pauseReason){
        logger.info("Pausing all currently running jobs.");
        List<SimpleJob> startedJobs = jobManager.getIdleJobs(timeSource.currentTimeMillis(),JobState.getStartedStates());
        pauseJobs(startedJobs, pauseReason);
    }

    private void sendJobsStates() {
        List<JobStateByDate> recentlyUpdatedStates = jobManager.getRecentlyUpdatedStates(pausedJobsUpdatedThresholdSec);
        mediaProcessorCommService.sendJobsStatesControlRequest(recentlyUpdatedStates);
    }

    public void resumeAllPausedJobs() {
        logger.info("Resuming all paused jobs.");
        List<SimpleJob> pausedJobs = jobManager.getIdleJobs(timeSource.currentTimeMillis(), JobState.PAUSED);
        if (pausedJobs != null && !pausedJobs.isEmpty()) {
            List<SimpleJobDto> pausedJobDtos = pausedJobs.stream()
                    .map(JobManagerService::simpleJobToDto)
                    .collect(Collectors.toList());
            resumePausedJobs(pausedJobDtos);
        }
    }

    public boolean resumeJob(long jobId) {
        SimpleJob job = jobManager.getJobById(jobId);
        return resumePausedJobs(Lists.newArrayList(JobManagerService.simpleJobToDto(job))).getKey().size() > 0;
    }

    public void pauseRun(long runContext, PauseReason pauseReason){
        CrawlRun crawlRun = runService.getCrawlRun(runContext);
        if (crawlRun == null) {
            logger.error("Failed to pause run with ID {}, run object was not found.", runContext);
            return;
        }

        abortThrottledRun(runContext);
        boolean hasChildRuns = crawlRun.getHasChildRuns() == null ? false : crawlRun.getHasChildRuns();
        if (!hasChildRuns) {
            List<SimpleJob> runJobs = jobManager.getRunJobsObjects(runContext);
            List<SimpleJob> startedRunJobs = runJobs.stream().filter(job -> job.getState().isStarted()).collect(Collectors.toList());
            if(startedRunJobs.size() > 0) {
                pauseJobs(startedRunJobs, pauseReason);
            }
            else {
                // force recalculate run state, in case run is still running but there are no active jobs
                // execute in its own transaction in order to avoid flushing the previously changed jobs
                // during list queries called inside handleFinishedRun (in order to prevent locks in the DB)
                logger.info("No active jobs for run {} - recalculating run state", runContext);
                if (logger.isDebugEnabled()) {
                    logger.debug("Current jobs state for run {}: {}", runContext,
                            runJobs.stream().map(job -> String.format("[ job-id: %s, job-state: %s, state-update-time: %d]",
                                    job.getId(), job.getState(), job.getStateUpdateTime())).collect(Collectors.joining(", ")));
                }
                DBTemplateUtils.doInTransaction(() ->
                        runService.handleFinishedRun(runContext, pauseReason), TransactionDefinition.PROPAGATION_REQUIRES_NEW);
            }
        } else {
            List<CrawlRun> childCrawlRuns = runService.getChildCrawlRuns(crawlRun.getId());
            childCrawlRuns.forEach(childRun -> pauseRun(childRun.getId(), pauseReason));
        }
        jobManagerAppService.markStatusChange();
    }

    public void resumeRun(long runContext) {
        handleResumeRun(runContext, false);
    }

    private void handleResumeRun(long runContext, boolean resumeForParentRun) {
        logger.debug("Resume run jobs. Run Id {}", runContext);
        try {
            CrawlRun crawlRun = runService.getCrawlRun(runContext);
            if (crawlRun.getRootFolderId() != null) {
                docStoreService.evictFromCache(crawlRun.getRootFolderId());
            }
            if (crawlRun.getRunStatus() != RunStatus.PAUSED) {
                logger.debug("Run #{} is not PAUSED (it's {}), skipping resume; crawlRun: {}",
                        crawlRun.getId(), crawlRun.getRunStatus(), crawlRun);
                return;
            }
            abortThrottledRun(runContext);
            runService.updateCrawlRunState(crawlRun.getId(), RunStatus.RUNNING);
            if (!crawlRun.getHasChildRuns()) {
                List<SimpleJob> runJobs = jobManager.getRunJobsObjects(runContext);
                List<SimpleJobDto> pausedJobs = runJobs.stream()
                        .filter(job -> JobState.PAUSED == job.getState())
                        .map(JobManagerService::simpleJobToDto)
                        .collect(Collectors.toList());

                if (!pausedJobs.isEmpty()) {
                    Callable<Pair<List<SimpleJobDto>, PauseReason>> task = () -> resumePausedJobs(pausedJobs);
                    Pair<List<SimpleJobDto>, PauseReason> resumedJobsResult = DBTemplateUtils.doInTransaction(task);
                    if (resumedJobsResult.getKey().size() < pausedJobs.size()) { 
                        // If there are some jobs that are still paused, recalculate their run status
                        // and update the pause reason if the run should be paused. For example, if the MPs
                        // are not running, the run will become paused with new pause reason, which will
                        // allow it to be resumed automatically when MPs are back online
                        PauseReason reason = resumedJobsResult.getValue();
                        runService.handleFinishedRun(runContext, reason != null ? reason : PauseReason.SYSTEM_INITIATED);
                    }
                } else {
                    // In case there was an unexpected running job - update status
                    boolean isRunningJobs = runJobs.stream().anyMatch(job -> JobState.IN_PROGRESS == job.getState());
                    if (isRunningJobs) {
                        runService.handleFinishedRun(runContext, PauseReason.SYSTEM_INITIATED);
                    }
                }
            } else {
                List<CrawlRun> childCrawlRuns = runService.getChildCrawlRuns(crawlRun.getId());
                childCrawlRuns.forEach(childRun -> handleResumeRun(childRun.getId(), true));
            }
            jobManagerAppService.markStatusChange();
        } catch (Exception e) {
            logger.error("Fail to resume crawl run with ID {}. {}", runContext, e);
        }
    }

    private void abortThrottledRun(long runContext) {
        Optional<ResumeRunTask> task = Optional.ofNullable(throttlePausedRunMap.remove(runContext));
        if (task.isPresent()) {
            task.get().cancel();
        }
    }

    /**
     * Find a job by run context and type, check whether it is paused
     *
     * @param runContext run context
     * @param jobType    job type
     * @return true if the job is PAUSED, false otherwise
     */
    public boolean isJobPaused(long runContext, JobType jobType) {
        boolean wasLocked = false;
        try {
            pauseLock.readLock().lock();
            wasLocked = true;
            pauseLockOwner="isJobPaused "+runContext+" "+jobType;
            Long jobId = jobManager.getJobIdCached(runContext, jobType);
            return (jobId == null) || pausedJobIds.contains(jobId);
        } catch (Exception e) {
            return false;
        } finally {
            if (wasLocked) {
                pauseLockOwner = null;
                pauseLock.readLock().unlock();
            }
        }
    }

    public boolean pauseJob(long jobToPause, PauseReason pauseReason) {
        SimpleJob simpleJob = jobManager.getJobById(jobToPause);
        return simpleJob != null && pauseJob(simpleJob, pauseReason);
    }

    private boolean pauseJob(SimpleJob jobToPause, PauseReason pauseReason){
        return pauseJobs(Lists.newArrayList(jobToPause), pauseReason);
    }

    // -------------------- private methods --------------------------------------------------

    /**
     * Pause specified jobs
     *
     * @param jobsToPause collection of jobs to pause
     * @return success - true if successfully paused
     */
    private boolean pauseJobs(Collection<SimpleJob> jobsToPause, PauseReason pauseReason){
        boolean paused = false;
        Set<Long> localPausedJobIds = jobsToPause.stream().map(SimpleJob::getId).collect(Collectors.toSet());
        Set<Long> localPausedJobs = Sets.newHashSet();
        try {
            if (pauseLock.writeLock().tryLock(10, TimeUnit.SECONDS)) {
                pauseLockOwner="pauseJobs "+localPausedJobIds;
                jobsToPause.forEach(job -> {
                    if (isMediaProcessorRelated(job.getType())) {
                        localPausedJobs.add(job.getId());
                    }
                    jobManager.pauseJob(job.getId(), job.getRunContext(), "", false);
                });
                jobsToPause.stream().map(SimpleJob::getRunContext).distinct().forEach(r -> runService.handleFinishedRun(r, pauseReason));
                // notify the MPs
                if (!localPausedJobs.isEmpty()) {
                    Map<Long, SimpleJob> jobsById =  jobsToPause.stream().collect(Collectors.toMap(SimpleJob::getId, r -> r));
                    List<JobStateByDate> jobStates = new ArrayList<>();
                    localPausedJobs.forEach(jobId -> {
                        SimpleJob job = jobsById.get(jobId);
                        jobStates.add(jobManager.createStateByDateForJob(jobId, JobState.PAUSED, job.getRunContext()));
                    });
                    mediaProcessorCommService.sendJobsStatesControlRequest(jobStates);
                    pausedJobIds.addAll(localPausedJobs);
                }
                paused = true;
            } else {
                logger.warn("Failed to acquire lock {}, jobs were not paused. {}", pauseLockOwner, localPausedJobIds);
            }
        } catch (Exception e) {
            logger.error("Failed to pause currently running jobs. {}", localPausedJobIds, e);
        } finally {
            if (pauseLock.writeLock().isHeldByCurrentThread()) {
                pauseLockOwner = null;
                pauseLock.writeLock().unlock();
            }
        }
        return paused;
    }

    /**
     * Resume paused jobs
     *
     * @param pausedJobs collection of paused jobs
     * @return successfully resumed jobs
     */
    private Pair<List<SimpleJobDto>, PauseReason> resumePausedJobs(List<SimpleJobDto> pausedJobs) {
        List<SimpleJobDto> resumed = new ArrayList<>();
        PauseReason pauseReason = null;
        if (pausedJobs == null || pausedJobs.size() == 0) {
            logger.info("Asked to resume jobs: No jobs to resume.");
            return Pair.of(resumed, pauseReason);
        }

        if (!isSolrUp()) {
            logger.warn("Solr is currently down, do not resume jobs: {}", pausedJobs);
            return Pair.of(resumed, PauseReason.SOLR_DOWN);
        }

        Set<Long> localPausedJobIds = pausedJobs.stream().map(SimpleJobDto::getId).collect(Collectors.toSet());
        logger.info("Resuming paused {} jobs.", localPausedJobIds.size());
        try {
            if (pauseLock.writeLock().tryLock(10, TimeUnit.SECONDS)) {
                pauseLockOwner = "resumePausedJobs "+localPausedJobIds;

                List<SimpleJobDto> jobsToResume = new ArrayList<>(pausedJobs);

                        // get all the paused jobs and resume their status
                jobsToResume.forEach(job -> {
                    jobManager.resumeJob(job, "Resuming job");
                    job.setState(job.getResumeState()); // update this instance for the next operations to work
                });

                Set<SimpleJobDto> mpRelatedPausedJobs = jobsToResume.stream()
                        .filter(job -> isMediaProcessorRelated(job.getType()))
                        .collect(Collectors.toSet());

                logger.trace("test mp related jobs to see if mp is up {}", mpRelatedPausedJobs);

                // if no mp to handle job, do not resume it
                for (SimpleJobDto job : mpRelatedPausedJobs) {
                    long runId = job.getRunContext();
                    CrawlRun run = runService.getCrawlRunFromCache(runId);
                    Long rootFolderId = run.getRootFolderId();
                    RootFolder rootFolder = docStoreService.getRootFolderCached(rootFolderId);
                    if (rootFolder != null) {
                        List<Long> activeMediaProcessors = sysComponentService.getActiveMediaProcessors(rootFolder.getCustomerDataCenter().getId(), true);
                        if (activeMediaProcessors.isEmpty()) {
                            logger.warn("cannot resume job {} of run {} - no available media processors for data center", job.getId(), runId);
                            jobsToResume.remove(job);
                            localPausedJobIds.remove(job.getId());
                            pauseReason = PauseReason.NO_AVAILABLE_MEDIA_PROCESSOR;
                        } else {
                            logger.trace("resume job {} of run {} - found available media processors {} for data center {}",
                                    job.getId(), runId, activeMediaProcessors, rootFolder.getCustomerDataCenter().getId());
                        }
                    }
                }

                // Kick-start jobs that were resumed to IN_PROGRESS state
                jobsToResume.stream()
                        .filter(job -> job.getState() == JobState.IN_PROGRESS ||  job.getState() == JobState.WAITING)
                        .forEach(j -> kickStartJob(j, false));

                jobsToResume.stream().map(SimpleJobDto::getRunContext).distinct().forEach(r -> runService.handleFinishedRun(r, PauseReason.SYSTEM_INITIATED));

                if (!jobsToResume.isEmpty() && !mpRelatedPausedJobs.isEmpty()) {
                    this.pausedJobIds.removeAll(localPausedJobIds);

                    List<JobStateByDate> jobStates = new ArrayList<>();
                    jobsToResume.forEach(job ->
                        jobStates.add(jobManager.createStateByDateForJob(job.getId(), job.getState(), job.getRunContext()))
                    );

                    // notify the MPs
                    mediaProcessorCommService.sendJobsStatesControlRequest(jobStates);
                }

                resumed.addAll(jobsToResume);
            } else {
                logger.warn("Failed to acquire lock {}, paused jobs were not resumed. {}", pauseLockOwner, localPausedJobIds);
            }
        } catch (Exception e) {
            logger.error("Failed to resume paused {} jobs {}.", localPausedJobIds.size(),
                    pausedJobs.stream().map(j -> String.valueOf(j.getId()))
                            .collect(Collectors.joining(",", "{", "}")), e);
        } finally {
            if (pauseLock.writeLock().isHeldByCurrentThread()) {
                pauseLockOwner = null;
                pauseLock.writeLock().unlock();
            }
        }
        return Pair.of(resumed, pauseReason);
    }

    public void stopRunGraceful(Long runId, PauseReason pauseReason) {
        abortThrottledRun(runId);
        CrawlRun crawlRun = runService.getCrawlRun(runId);
        if (!crawlRun.getHasChildRuns()) {
            if (crawlRun.getRunStatus() == RunStatus.PAUSED) {
                if (crawlRun.getRootFolderId() == null) {
                    stopPausedRun(runId, pauseReason);
                } else {
                    logger.info("Stopping gracefully crawl run with ID {}.", runId);
                    List<SimpleJobDto> runJobs = jobManager.getRunJobs(runId);
                    List<SimpleJobDto> activeJobs = runJobs.stream()
                            .filter(j -> !j.getState().equals(JobState.DONE))
                            .collect(Collectors.toList());

                    Long mapJobId = null;
                    Long analyzeJobId = null;
                    for (SimpleJobDto job : activeJobs) {
                        if (job.getType().equals(JobType.SCAN)) {
                            mapJobId = job.getId();
                        } else if (job.getType().equals(JobType.ANALYZE) && job.getState().equals(JobState.NEW)) {
                            analyzeJobId = job.getId();
                        }
                    }

                    List<SimpleJobDto> pausedJobs = runJobs.stream()
                            .filter(job -> JobState.PAUSED == job.getState())
                            .collect(Collectors.toList());

                    jobManager.removeTaskMarkingsForRun(runId);

                    runService.updateCrawlRunGracefulStop(runId);

                    if (analyzeJobId != null) {
                        jobManager.updateJobState(analyzeJobId, JobState.DONE, JobCompletionStatus.CANCELLED, "Analysis skipped", true);
                    }

                    if (mapJobId != null) {
                        RootFolder rf = docStoreService.getRootFolder(crawlRun.getRootFolderId());
                        scanDoneConsumer.finishScan(runId, crawlRun.getRootFolderId(),
                                rf.getFirstScan() == null || rf.getFirstScan() || rf.getReingestTimeStampMs() > 0);
                    } else {
                        resumePausedJobs(pausedJobs);
                    }
                }
            } else {
                logger.info("Cannot graceful stop run with status {}. crawl run with ID {}. Only pause run can be stopped", crawlRun.getRunStatus(), runId);
            }
        } else {
            List<CrawlRun> childCrawlRuns = runService.getChildCrawlRuns(crawlRun.getId());
            childCrawlRuns.forEach(childRun -> stopRunGraceful(childRun.getId(), pauseReason));
        }
    }

    public void stopPausedRun(Long runId, PauseReason pauseReason) {
        abortThrottledRun(runId);
        CrawlRun crawlRun = runService.getCrawlRun(runId);
        if (!crawlRun.getHasChildRuns()) {
            if (crawlRun.getRunStatus() == RunStatus.PAUSED) {
                logger.info("Stopping crawl run with ID {}.", runId);
                List<SimpleJobDto> runJobs = jobManager.getRunJobs(runId);
                boolean atLeastOnRunIsPaused = false;
                for (SimpleJobDto runJob : runJobs) {
                    switch (runJob.getState()) {
                        case NEW:
                        case READY:
                        case PENDING:
                        case WAITING:
                            jobManager.updateJobState(runJob.getId(), JobState.DONE, JobCompletionStatus.CANCELLED, "", true);
                            break;
                        case PAUSED:
                        case IN_PROGRESS: //in case of job that does not  pause
                            jobManager.updateJobState(runJob.getId(), JobState.DONE, JobCompletionStatus.STOPPED, null, true);
                            atLeastOnRunIsPaused = true;
                            break;
                    }
                }
                if (!atLeastOnRunIsPaused) {
                    logger.error("Abnormal run job status. Paused run without any paused job");
                    for (SimpleJobDto runJob : runJobs) {
                        if (!JobState.DONE.equals(runJob.getState())) {
                            jobManager.updateJobState(runJob.getId(), JobState.DONE, JobCompletionStatus.STOPPED, "Force stopped", true);
                            break;
                        }
                    }
                }

                runService.handleFinishedRun(runId, pauseReason);
            } else {
                logger.info("Cannot stop run with status {}. crawl run with ID {}. Only pause run can be stopped", crawlRun.getRunStatus(), runId);
            }
        } else {
            List<CrawlRun> childCrawlRuns = runService.getChildCrawlRuns(crawlRun.getId());
            childCrawlRuns.forEach(childRun -> stopPausedRun(childRun.getId(), pauseReason));
        }

    }

    /**
     * Housekeeping on jobs/tasks that needs to be done once the server starts.
     * 1. All the tasks that were interrupted in CONSUMING stage should be restarted from the beginning.
     * 2. Scan jobs that were interrupted in IN_PROGRESS state should be restarted from the beginning.
     * 3. Scan-finalize, ingest-finalize and analyze-finalize jobs that were interrupted in IN_PROGRESS state
     * should be reran from where they stopped.
     */
    private void handleRestart() {
        logger.info("Handling Jobs/Tasks states after system restart.");

        // 1. Restart tasks that were interrupted in CONSUMING stage
        List<Long> olderTaskIds = jobManager.getIdleTaskIds(systemStartTime, TaskState.CONSUMING);
        jobManager.updateTasksStatus(olderTaskIds, TaskState.NEW);
        restartHandled = true;

        // 2,3. Handling jobs that were interrupted in IN_PROGRESS state.
        List<SimpleJobDto> interruptedInProgressJobs = jobManager.getIdleJobsDtos(systemStartTime, JobState.IN_PROGRESS);
        for (SimpleJobDto interruptedJob : interruptedInProgressJobs) {
            // only kick-start jobs that are not scheduled
            JobType jobType = interruptedJob.getType();
            if (!(jobType == SCAN || jobType == INGEST || jobType == ANALYZE || jobType == EXTRACT)) {
                kickStartJob(interruptedJob, true);
            }
        }

        logger.info("Finished handling Jobs/Tasks states after system restart, {} tasks were updated.", olderTaskIds.size());
    }

    private boolean kickStartJob(SimpleJobDto jobDto, boolean resumeForParentRun) {
        logger.debug("Kickstarting job id {} resumeForParentRun {} {}", jobDto.getId(), resumeForParentRun, jobDto);
        long jobId = jobDto.getId();
        long runId = jobDto.getRunContext();
        JobType jobType = jobDto.getType();
        boolean jobResumed;
        String stateOpMsg = "Resuming...";
        switch (jobType) {
            case SCAN:
                jobManager.updateJobState(jobId, JobState.IN_PROGRESS, null, stateOpMsg, true);
                List<SimpleTask> scanTasks = jobManager.getTaskByJobIdAndTaskTypeAndStates(jobId, TaskType.SCAN_ROOT_FOLDER, TaskState.FAILED);
                scanTasks.forEach(t -> {
                    jobManager.updateTaskStatus(t.getId(), TaskState.NEW);
                });
                jobResumed = true;
                break;
            case SCAN_FINALIZE:
                scanFinalizeService.resumeScanFinJob(runId, jobDto.getItemId(), jobId);
                List<SimpleTask> scanFinalizeTasks = jobManager.getTaskByJobIdAndTaskTypeAndStates(jobId, TaskType.SCAN_FINALIZE, TaskState.ENQUEUED, TaskState.CONSUMING, TaskState.FAILED);
                scanFinalizeTasks.forEach(t -> {
                    jobManager.updateTaskStatus(t.getId(), TaskState.NEW);
                });
                jobResumed = true;
                break;
            case INGEST:
                jobManager.updateJobState(jobId, JobState.IN_PROGRESS, null, stateOpMsg, true);
                jobResumed = true;
                break;
            case INGEST_FINALIZE:
                asyncIngestProducerService.handleIngestFinalization(runId, false);
                jobResumed = true;
                break;
            case OPTIMIZE_INDEXES:
                ingestFinalizationService.handleOptimizeIndexes(runId, null);
                jobResumed = true;
                break;
            case ANALYZE:
                jobManager.updateJobState(jobId, JobState.IN_PROGRESS, null, stateOpMsg, true);
                jobResumed = true;
                break;
            case ANALYZE_FINALIZE:
                jobManager.updateJobState(jobId, JobState.WAITING, null, stateOpMsg, true);
                List<SimpleTask> analyzeFinalizeTasks = jobManager.getTaskByJobIdAndTaskTypeAndStates(jobId, TaskType.ANALYZE_FINALIZE, TaskState.ENQUEUED, TaskState.CONSUMING, TaskState.FAILED);
                analyzeFinalizeTasks.forEach(t -> {
                    jobManager.updateTaskStatus(t.getId(), TaskState.NEW);
                });
                jobResumed = true;
                break;
            case EXTRACT:
                jobManager.updateJobState(jobId, JobState.READY, null, stateOpMsg, true);
                jobResumed = true;
                break;
            case EXTRACT_FINALIZE:
                jobManager.updateJobState(jobId, JobState.IN_PROGRESS, null, stateOpMsg, true);
                jobResumed = true;
                break;
            default:
                jobResumed = false;
                logger.error("Job with id {} has unsupported job type {} for resume", jobId, jobType);
        }
        return jobResumed;
    }

    /**
     * When Media Processor falls or is restarted, it can drop the task it is currently processing
     * after it had already removed it from the queue. Such task will stay in ENQUEUED state forever
     * unless it is handled specifically. Some tasks can be stuck in CONSUMING state if the consumer had failed.
     * Such tasks should be re-enqueued to the queue.
     */
    private void handleStuckEnqueuedTasks() {
        try {
            // wait until the restart handling has finished
            if (!restartHandled) {
                return;
            }

            long threshold = timeSource.currentTimeMillis() - TimeUnit.SECONDS.toMillis(enqueuedTaskTimeoutSec);
            // in case of restart, allow enqueuedTaskTimeoutSec to pass from start-time before considering tasks as stuck
            if (threshold < systemStartTime) {
                return;
            }
            // find all the tasks that are stuck in ENQUEUED or CONSUMING states for X seconds and belong to jobs that show no progress
            List<SimpleTask> olderTasks = jobManager.getIdleTasks(threshold, 500,
                    Lists.newArrayList(TaskType.INGEST_FINALIZE_CONTENT, TaskType.ANALYZE_FINALIZE,
                            TaskType.EXTRACT_BIZ_LIST_ITEM, TaskType.EXTRACT_FINALIZE, TaskType.SCAN_FINALIZE, TaskType.OPTIMIZE_INDEXES),
                    Lists.newArrayList(TaskState.ENQUEUED, TaskState.CONSUMING));

            List<SimpleJob> analyzeJobs = jobManager.gotJobs(Collections.singletonList(JobType.ANALYZE), Arrays.asList(JobState.IN_PROGRESS, JobState.PENDING));
            if (!analyzeJobs.isEmpty()) {
                List<Long> runIds = analyzeJobs.stream().map(Job::getRunContext).collect(Collectors.toList());
                List<SimpleTask> idleAnalyze = jobManager.getIdleAnalyzeTasks(threshold, 500, runIds);
                olderTasks.addAll(idleAnalyze);
            }

            List<SimpleJob> ingestJobs = jobManager.gotJobs(Collections.singletonList(JobType.INGEST), Arrays.asList(JobState.IN_PROGRESS, JobState.PENDING));
            if (!ingestJobs.isEmpty()) {
                Set<Long> runIds = ingestJobs.stream().map(Job::getRunContext).collect(Collectors.toSet());
                Map<Long, Long> runToRf = runService.getRootFoldersForRun(runIds);
                Map<Long, CustomerDataCenter> dcForRfs = docStoreService.getDCForRootFolders(runToRf.values());
                Map<Integer, List<Long>> timeoutForRun = new HashMap<>();
                runIds.forEach(r -> {
                    Long rf = runToRf.get(r);
                    CustomerDataCenter dc = dcForRfs.get(rf);
                    Integer timeout = dc.getIngestTaskRetryTime() != null ? dc.getIngestTaskRetryTime() : enqueuedTaskTimeoutSec;
                    List<Long> runsForTimeout = timeoutForRun.computeIfAbsent(timeout, k -> new ArrayList<>());
                    runsForTimeout.add(r);
                });
                for (Integer timeout : timeoutForRun.keySet()) {
                    threshold = timeSource.currentTimeMillis() - TimeUnit.SECONDS.toMillis(timeout);
                    if (threshold < systemStartTime) {
                        continue;
                    }
                    List<Long> runsForTimeout = timeoutForRun.get(timeout);
                    List<SimpleTask> idleIngest = jobManager.getIdleIngestTasks(threshold, 500, runsForTimeout);
                    olderTasks.addAll(idleIngest);
                }
            }

            if (olderTasks.isEmpty()) {
                return;
            }

            // group tasks by their jobs
            Multimap<Long, SimpleTask> tasksByJob = LinkedListMultimap.create();
            olderTasks.forEach(task -> tasksByJob.put(task.getJobId(), task));

            List<SimpleJob> olderJobs = jobManager.getJobsByIds(tasksByJob.keySet());
            // go over the job and take care of their tasks
            for (SimpleJob job : olderJobs) {

                if (job.getType() == JobType.SCAN) {
                    continue;
                }

                Collection<SimpleTask> tasks = tasksByJob.get(job.getId());
                handleStuckTask(job, tasks);
            }
        } catch (Exception e) {
            logger.error("handleStuckEnqueuedTasks failed running", e);
        }
    }

    private void handleStuckTask(SimpleJob job, Collection<SimpleTask> tasks) {
        switch (job.getState()) {
            case DONE:
                setTasksState(TaskState.FAILED, tasks);
                jobManager.incJobFailedCounters(job.getId(), tasks.size());
                LoggingUtils.log(logger, (tasks.size() > 0) ? Level.WARN : Level.DEBUG,
                        "Job #{} in DONE state. {} tasks were stuck, marking as FAILED.",
                        job.getId(), tasks.size());
                break;

            case IN_PROGRESS:
            case PENDING:
            case NEW:
            case READY:
                Map<Boolean, List<SimpleTask>> failedAndRetryableTasks =
                        tasks.stream().collect(partitioningBy(t -> t.getRetries() > maxTaskRetries));
                List<SimpleTask> failedTasks = failedAndRetryableTasks.get(Boolean.TRUE);
                List<Long> retryableTaskIds =
                        failedAndRetryableTasks.get(Boolean.FALSE).stream().map(Task::getId).collect(Collectors.toList());

                jobManager.updateTasksStatusInBatch(job.getRunContext(), job.getType(), failedTasks, TaskState.FAILED);
                jobManager.incJobFailedCounters(job.getId(), failedTasks.size());
                jobManager.updateJobTaskStateIncrementRetryForEver(job.getRunContext(), job.getId(), job.getType(), retryableTaskIds, null);

                int failedCount = failedTasks.size();
                int retryCount = retryableTaskIds.size();

                LoggingUtils.log(logger, (retryCount > 0 || failedCount > 0) ? Level.WARN : Level.DEBUG,
                        "Job #{} in {} state of type {}. {} tasks will be retried, {} tasks were marked as FAILED.",
                        job.getId(), job.getState().toString(), job.getType().toString(), retryCount, failedCount);
                break;

            case PAUSED:
                // mark as NEW, so that the tasks would get retried when the job resumes
                setTasksState(TaskState.NEW, tasks);
                logger.debug("Job #{} in {} state. {} tasks will be restarted.", job.getId(), job.getState().toString(), tasks.size());
                break;
        }
    }

    /**
     * Handle runs that got stuck in Running state with no running jobs.
     */
    private void handleStuckInRunningRuns() {
        try {
            List<CrawlRun> runningRuns = runService.getCrawlRunsByStatus(RunStatus.RUNNING);
            boolean stoppedRun = false;
            for (CrawlRun runningRun : runningRuns) {
                try {
                    List<SimpleJobDto> jobs = jobManager.getRunJobs(runningRun.getId());
                    if (jobs.size() > 0) {
                        boolean allRunJobsAreDone = jobs.stream().allMatch(job -> job.getState() == JobState.DONE);
                        if (allRunJobsAreDone) {
                            logger.debug("Handling run #{} which was stuck in RUNNING state with no active jobs", runningRun.getId());
                            runService.handleFinishedRun(runningRun.getId(), PauseReason.SYSTEM_INITIATED);
                            stoppedRun = true;
                        }
                    }
                } catch (Exception e) {
                    logger.error("handleStuckInRunningRuns failed for run {}", runningRun, e);
                }
            }
            if (stoppedRun) {
                jobManagerAppService.markStatusChange();
            }
        } catch (Exception e) {
            logger.error("handleStuckInRunningRuns failed", e);
        }
    }

    /**
     * Handle jobs stuck in progress.
     * Analyze the tasks, mark job as DONE if there are no active tasks.
     * Mark as TIMED_OUT if the job shows no progress for specified amount of time
     */
    private void handleStuckInProgressJobs() {
        // wait until the restart handling has finished
        if (!restartHandled) {
            return;
        }

        try {
            List<SimpleJob> scanJobsPending = jobManager.gotJobs(Collections.singletonList(JobType.SCAN), Arrays.asList(JobState.IN_PROGRESS, JobState.PENDING));
            boolean stoppedRun = false;
            for (SimpleJob job : scanJobsPending) {
                if (jobManager.shouldFailJob(job.getId(), job.getTasksPartition())) {
                    logger.debug("scan job {} had failed tasks and no active ones - fail scan and run", job.getId());
                    jobManager.handleFailedFinishJob(job.getId(), "Scan failed");
                    runService.handleFinishedRun(job.getRunContext(), PauseReason.SYSTEM_INITIATED);
                    stoppedRun = true;
                }
            }

            List<SimpleJob> scanJobs = jobManager.getJobs(JobType.SCAN, JobState.IN_PROGRESS);
            for (SimpleJob job : scanJobs) {
                // if no consuming/enqueued tasks but still has new tasks, set job to pending
                if (jobManager.shouldJobBePending(job.getId(), job.getTasksPartition(), JobType.SCAN)) {
                    logger.debug("scan job {} is waiting for its tasks to be processed, set to pending", job.getId());
                    jobManager.updateJobState(job.getId(), JobState.PENDING, null, "Waiting to be processed", false);
                }
            }

            List<SimpleJob> ingestJobs = jobManager.getJobs(JobType.INGEST, JobState.IN_PROGRESS);
            for (SimpleJob job : ingestJobs) {
                // if no consuming tasks but still has new/enqueued tasks, set job to pending
                if (jobManager.shouldJobBePending(job.getId(), job.getTasksPartition(), JobType.INGEST)) {
                    logger.debug("Ingest job {} is waiting for its tasks to be processed, set to pending", job.getId());
                    jobManager.updateJobState(job.getId(), JobState.PENDING, null, "Waiting to be processed", false);
                }
            }

            long threshold = timeSource.currentTimeMillis() - TimeUnit.SECONDS.toMillis(inProgressJobTimeoutSec);
            // in case of restart, allow inProgressJobTimeoutSec to pass from start-time before considering jobs as stuck
            if (threshold < systemStartTime) {
                if (stoppedRun) {
                    jobManagerAppService.markStatusChange();
                }
                return;
            }

            List<SimpleJob> olderJobs = Lists.newLinkedList();
            for (SimpleJob job : jobManager.getIdleJobs(threshold, JobState.IN_PROGRESS)) {

                if (JobType.SCAN.equals(job.getType())) {
                    logger.debug("handle ENQUEUED and CONSUMING tasks for stuck scan job {}", job.getId());
                    List<SimpleTask> tasks = jobManager.getTaskByJobIdAndTaskTypeAndStates(job.getId(), TaskType.SCAN_ROOT_FOLDER, TaskState.ENQUEUED, TaskState.CONSUMING);
                    handleStuckTask(job, tasks);
                    continue;
                }

                if (JobType.INGEST.equals(job.getType())) {
                    logger.debug("Ingest job {} seems to be inactive for more than {} sec. Ignored", job.getId(), inProgressJobTimeoutSec);
                    continue;
                }

                if (JobType.ANALYZE.equals(job.getType())) {
                    if (analyzeFinalizeService.isFinalizationInProgress()) {
                        logger.debug("Analysis job {} is waiting for analysis finalization to finish.", job.getId());
                        continue;
                    }
                }

                if (JobType.EXTRACT.equals(job.getType()) || JobType.EXTRACT_FINALIZE.equals(job.getType())) {
                    logger.debug("{} job {} seems to be inactive for more than {} sec. Ignored", job.getType(), job.getId(), inProgressJobTimeoutSec);
                    continue;
                }

                Map<TaskState, Number> taskStates = jobManager.getTaskStatesByJobId(job.getId(), job.getTasksPartition());
                int active = countStates(taskStates, TaskState.NEW, TaskState.ENQUEUED, TaskState.CONSUMING);

                olderJobs.add(job);


                if (active == 0) {
                    logger.warn("Job {} was stuck in progress, although it had no active tasks.", job.getId());
                    List<JobStateUpdateInfo> jobStateUpdateInfos = new ArrayList<>();
                    jobStateUpdateInfos.add(new JobStateUpdateInfo(job.getId(), JobState.DONE, "Done with no active tasks", true));
                    Collection<JobType> nextJobTypes = jobManagerAppService.nextJobTypes(job.getType());
                    for (JobType nextJobType : nextJobTypes) {
                        // Important!! we will never get here extract & extract finalize job, so
                        // using the job id cache which return only 1 job id is OK
                        Long nextJobId = jobManager.getJobIdCached(job.getRunContext(), nextJobType);
                        if (nextJobId == null) {
                            continue;
                        }
                        JobState nextState = nextJobType == JobType.EXTRACT ? JobState.READY : JobState.IN_PROGRESS;
                        String nextOpMsg = jobManagerAppService.getInProgressOpMsg(nextJobType);
                        jobStateUpdateInfos.add(new JobStateUpdateInfo(nextJobId, nextState, nextOpMsg, false));
                    }
                    jobManager.updateJobStates(jobStateUpdateInfos);

                } else if (taskProducingStrategySelector.getSelectedStrategy().shouldJobTimeout(job.getType())) {
                    logger.warn("Job {} was showing no progress, marking as timed out.", job.getId());
                    jobManager.updateJobState(job.getId(), JobState.PAUSED, JobCompletionStatus.TIMED_OUT,
                            "No progress for more than " + TimeUnit.SECONDS.toMinutes(inProgressJobTimeoutSec) + " minutes", true);
                    jobManager.setRunningTasksToNewForJob(job.getId(), job.getRunContext());
                }
            }
            olderJobs.stream().map(Job::getRunContext).distinct().forEach(r -> runService.handleFinishedRun(r, PauseReason.SYSTEM_INITIATED));
            if (stoppedRun || !olderJobs.isEmpty()) {
                jobManagerAppService.markStatusChange();
            }
        } catch (Exception e) {
            logger.error("handleStuckInProgressJobs failed", e);
        }
     }

    /**
     * Update tasks states to the specified state
     *
     * @param state task state to update to
     * @param tasks tasks to update
     */
    private void setTasksState(TaskState state, Collection<SimpleTask> tasks) {
        Map<TaskType, List<SimpleTask>> byTypes = tasks.stream()
                .collect(groupingBy(Task::getTaskType));
        if (byTypes.containsKey(TaskType.INGEST_FILE)) {
            List<SimpleTask> ingestTasks = byTypes.remove(TaskType.INGEST_FILE);
            jobManager.updateIngestTasksStatus(
                    ingestTasks.stream().map(SimpleTask::getItemId).collect(Collectors.toList()), state);
        }
        if (byTypes.containsKey(TaskType.ANALYZE_CONTENT)) {
            List<SimpleTask> analysisTasks = byTypes.remove(TaskType.ANALYZE_CONTENT);
            jobManager.updateAnalysisTasksStatus(
                    analysisTasks.stream().map(SimpleTask::getItemId).collect(Collectors.toList()), state
            );
        }

        if (!byTypes.isEmpty()) {
            List<SimpleTask> others = new ArrayList<>();
            byTypes.values().forEach(others::addAll);
            jobManager.updateTasksStatus(others.stream().map(SimpleTask::getId).collect(Collectors.toSet()), state);
        }
    }

    /**
     * Count tasks in specified states, i.e. aggregation of several state counters
     *
     * @param taskStates states to add to the count
     * @param states     map of state -> number of tasks in this state
     * @return number of tasks in the specified states
     */
    private int countStates(Map<TaskState, Number> taskStates, TaskState... states) {
        int result = 0;
        for (TaskState state : states) {
            Number number = taskStates.get(state);
            result += (number == null) ? 0 : number.intValue();
        }
        return result;
    }

    private boolean isMediaProcessorRelated(JobType jobType) {
        return JobType.SCAN == jobType ||
                JobType.INGEST == jobType;
    }

    public void handleTooManyThrottlingErrors(IngestResultMessage result) {
        final Long runId = result.getRunContext();
        pauseRun(runId, PauseReason.SYSTEM_INITIATED);
        if (!throttlePausedRunMap.containsKey(runId)) {
            logger.trace("Attempting to add paused run {}", runId);
            ResumeRunTask task = new ResumeRunTask(runId);
            ResumeRunTask oldTask = throttlePausedRunMap.putIfAbsent(runId, task);

            if (oldTask == null) {
                logger.debug("Scheduling auto-resume run {} in {} ms", runId, resumeThrottledRunPeriod);
                resumeThrottlePausedExecutor.schedule(task, resumeThrottledRunPeriod, TimeUnit.MILLISECONDS);
            }
        }
    }

    class ResumeRunTask implements Runnable {
        private long runId;
        private boolean canceled;

        ResumeRunTask(long runId) {
            this.runId = runId;
        }

        @Override
        public void run() {
            logger.info("Auto resuming throttled run {}", runId);
            throttlePausedRunMap.remove(runId);
            if (canceled) {
                logger.info("Auto resuming throttled run {} canceled", runId);
                return;
            }

            logger.trace("Auto-resuming throttled run {}", runId);
            try {
                resumeRun(runId);
            } catch (Throwable t) {
                logger.error("Failed to resume paused-throttled run", t);
            }
        }

        void cancel() {
            canceled = true;
        }
    }
}
