package com.cla.filecluster.jobmanager.service;

import com.cla.connector.progress.ProgressTracker;
import com.cla.connector.utils.TimeSource;

import java.util.Objects;

/**
 * A ProgressTracker decorator that updates the progress timestamp if the underlying ProgressTracker didn't report
 * progress.
 * <p>
 * The actual timestamp update happens every so often. This is controlled by specifying an update rate
 * in a constructor argument.
 *
 * <p>
 * Created by Itai Marko.
 */
public class ProgressTimestampTracker implements ProgressTracker {

    private final ProgressTracker delegate;
    private final long timestampUpdateRate;
    private long lastTimestampUpdateTime = 0L;
    private final TimeSource timeSource = new TimeSource();

    private final long jobId;
    private final JobManagerService jobManagerService;

    /**
     * Creates a new instance of ProgressTimestampTracker using the given underlying ProgressTracker and update rate.
     * <p>
     * The update rate refers to the rate in which to update progress timestamp. It must be non-negative. Zero means to
     * always update (unless the underlying ProgressTracker already did).
     *
     * @param delegate            the underlying ProgressTracker
     * @param timestampUpdateRate the update rate
     * @param jobId               the id of the job for which to update the timestamp
     * @param jobManagerService   used to update the timestamp
     */
    public ProgressTimestampTracker(ProgressTracker delegate, long timestampUpdateRate,
                                    long jobId, JobManagerService jobManagerService) {
        Objects.requireNonNull(delegate);
        if (timestampUpdateRate < 0L) {
            throw new IllegalArgumentException("Negative timestampUpdateRate: " + timestampUpdateRate);
        }

        this.delegate = delegate;
        this.timestampUpdateRate = timestampUpdateRate;
        this.jobId = jobId;
        this.jobManagerService = jobManagerService;
    }

    /**
     * Creates a new instance of ProgressTimestampTracker using the given underlying ProgressTracker.
     * <p>
     * The timestamp update will happen X times in an interval where X is specified by {@code intervalRate} and
     * the interval is specified by {@code intervalInSec}
     *
     * @param delegate          the underlying ProgressTracker
     * @param intervalInSec     the interval in seconds (typically a timeout interval)
     * @param intervalRate      the number of times in an interval to update the timestamp
     * @param jobId             the id of the job for which to update the timestamp
     * @param jobManagerService used to update the timestamp
     */
    public ProgressTimestampTracker(ProgressTracker delegate, int intervalInSec, int intervalRate,
                                    long jobId, JobManagerService jobManagerService) {
        this(delegate, intervalInSec * 1000 / intervalRate, jobId, jobManagerService);
    }

    @Override
    public void startStageTracking(int stageWeight) {
        delegate.startStageTracking(stageWeight);
    }

    @Override
    public void setCurStageEstimatedTotal(long stageTotalEstimate) {
        delegate.setCurStageEstimatedTotal(stageTotalEstimate);
    }

    @Override
    public boolean incrementProgress(long amount) {
        boolean progressReported = false;
        boolean delegateReportedProgress = delegate.incrementProgress(amount);

        if (delegateReportedProgress) {
            lastTimestampUpdateTime = timeSource.currentTimeMillis();
            progressReported = true;
        } else if (timeSource.millisSince(lastTimestampUpdateTime) >= timestampUpdateRate) {
            // sending a negative amount causes it to only update the lastTaskCountTime field of the job
            jobManagerService.updateJobCounters(jobId, -1, 0);
            lastTimestampUpdateTime = timeSource.currentTimeMillis();
            progressReported = true;
        }
        return progressReported;
    }

    @Override
    public void endCurStage() {
        delegate.endCurStage();
    }

    @Override
    public void endTracking() {
        delegate.endTracking();
    }
}
