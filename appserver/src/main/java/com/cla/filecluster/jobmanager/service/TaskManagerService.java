package com.cla.filecluster.jobmanager.service;

import com.cla.filecluster.jobmanager.domain.entity.SimpleTask;
import com.cla.filecluster.jobmanager.domain.entity.TaskState;
import com.cla.filecluster.jobmanager.repository.jpa.SimpleTaskRepository;
import com.cla.filecluster.batch.writer.utils.TaskCreation;
import com.cla.filecluster.repository.SequenceIdGenerator;
import com.cla.filecluster.util.query.DBTemplateUtils;
import com.google.common.collect.Iterables;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionDefinition;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.Callable;

/**
 * Created by: yael
 * Created on: 5/10/2018
 */
@Service
public class TaskManagerService {

    private static final Logger logger = LoggerFactory.getLogger(TaskManagerService.class);

    @Autowired
    private SimpleTaskRepository simpleTaskRepository;

    @Autowired
    private DBTemplateUtils DBTemplateUtils;

    @Autowired
    private SequenceIdGenerator sequenceIdGenerator;

    @Autowired
    private TaskManagerServiceTxUtil taskManagerServiceTxUtil;

    @Value("${task-manager.task-update-partition-size:1000}")
    private int taskUpdatePartitionSize;

    public synchronized void updateTaskState(Collection<Long> taskIds, TaskState taskState) {
        Long currTimeMilli = System.currentTimeMillis();
        logger.trace("Updating {} tasks state to {} by page size of {}", taskIds.size(), taskState, taskUpdatePartitionSize);
        DBTemplateUtils.doInTransaction(() ->
                        Iterables.partition(taskIds, taskUpdatePartitionSize).forEach(p -> {
                            logger.debug("Updating page of {} tasks state to {}", p.size(), taskState);
                            simpleTaskRepository.updateTaskState(p, taskState, currTimeMilli);
                            if (logger.isTraceEnabled()) {
                                logger.trace("Task ids updated to {}: {}", taskState, p);
                            }
                        }),
                TransactionDefinition.PROPAGATION_REQUIRES_NEW);
    }

    public synchronized void updateJobTaskState(Long jobId, TaskState taskStateNew, TaskState taskStateOld, Long stateUpdateTime) {
        DBTemplateUtils.doInTransaction(() -> {
            int cng = simpleTaskRepository.updateJobTaskState(jobId, taskStateNew, taskStateOld, stateUpdateTime);
            logger.info("job {} change tasks in state ENQUEUED to NEW updated {} records", jobId, cng);
        }, TransactionDefinition.PROPAGATION_REQUIRES_NEW);
    }

    public synchronized void updateTaskStateIncrementRetry(Collection<Long> taskIds, TaskState taskState, Long stateUpdateTime, Long partitionId) {
        if (partitionId == null) {
            DBTemplateUtils.doInTransaction(() ->
                            simpleTaskRepository.updateTaskStateIncrementRetry(taskIds, taskState, stateUpdateTime),
                    TransactionDefinition.PROPAGATION_REQUIRES_NEW);
        } else {
            DBTemplateUtils.doInTransaction(() ->
                            simpleTaskRepository.updateTaskStateIncrementRetry(taskIds, stateUpdateTime, taskState, partitionId),
                    TransactionDefinition.PROPAGATION_REQUIRES_NEW);
        }
    }

    public synchronized void updateTaskStatus(Long taskId, TaskState taskState, String json) {
        taskManagerServiceTxUtil.updateTaskStatus(taskId, taskState, json);
    }

    public synchronized SimpleTask createTask(SimpleTask task) {
        task.setId(getTaskNextId());

        Callable<SimpleTask> taskCreation = () -> simpleTaskRepository.save(task);

        return DBTemplateUtils.doInTransaction(taskCreation,
                TransactionDefinition.PROPAGATION_REQUIRES_NEW);
    }

    private Long getTaskNextId() {
        return sequenceIdGenerator.nextSequence(SequenceIdGenerator.SequenceType.JM_TASKS);
    }

    public void createTasksInBatch(List<SimpleTask> tasks) {
        TaskCreation.batchedTaskStatement(sequenceIdGenerator, tasks).ifPresent(statement -> {
            synchronized (this) {
                DBTemplateUtils.doInTransaction(jdbcTemplate -> jdbcTemplate.execute(statement));
            }
        });
    }

    public void createTasksBatch(List<SimpleTask> tasks) {
        tasks.forEach(t ->
                t.setId(getTaskNextId())
        );
        synchronized (this) {
            DBTemplateUtils.persistEntitiesBatchInTx(tasks);
        }
    }
}
