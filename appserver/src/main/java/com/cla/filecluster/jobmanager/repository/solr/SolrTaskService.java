package com.cla.filecluster.jobmanager.repository.solr;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.constants.ContentMetadataFieldType;
import com.cla.common.constants.SolrCommitType;
import com.cla.common.constants.SolrFieldOp;
import com.cla.filecluster.config.security.AutoAuthenticate;
import com.cla.filecluster.domain.dto.SolrContentMetadataEntity;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.jobmanager.domain.entity.SimpleTask;
import com.cla.filecluster.jobmanager.domain.entity.TaskState;
import com.cla.filecluster.jobmanager.domain.entity.TaskType;
import com.cla.filecluster.repository.solr.infra.SolrRepositoryUtils;
import com.cla.filecluster.repository.solr.query.AtomicUpdateBuilder;
import com.cla.filecluster.repository.solr.query.Criteria;
import com.cla.filecluster.repository.solr.query.Query;
import com.cla.filecluster.repository.solr.query.SolrOperator;
import com.cla.filecluster.service.categories.FileCatService;
import com.cla.filecluster.service.files.ContentMetadataService;
import com.cla.filecluster.util.categories.FileCatCompositeId;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Lists;
import com.google.common.collect.Table;
import org.apache.solr.client.solrj.response.PivotField;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrInputDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.stream.Collectors;

import static com.cla.common.constants.CatFileFieldType.*;
import static com.cla.filecluster.repository.solr.query.SolrOperator.SET;

@Service
public class SolrTaskService {

    private static final Logger logger = LoggerFactory.getLogger(SolrTaskService.class);

    @Autowired
    private FileCatService fileCatService;

    @Autowired
    private ContentMetadataService contentMetadataService;

    @Value("${solr.maxBooleanClauses:64000}")
    private int solrMaxBooleanClauses;

    @Value("${solr.taskService.isNoWaitCommit:true}")
    private boolean isNoWaitCommit;

    private SolrCommitType commitType;

    @PostConstruct
    void init() {
        commitType = (isNoWaitCommit ? SolrCommitType.SOFT_NOWAIT : SolrCommitType.SOFT);
        logger.info("Init: set commitType={}", commitType);
    }

    //////////////////////////////// file methods (ingest)

    public Table<Long, TaskState, Number> getIngestTaskByRunAndStateNum(List<Long> runs) {
        Table<Long, TaskState, Number> result = HashBasedTable.create();

        if (runs == null || runs.isEmpty()) {
            return result;
        }

        Query query = Query.create()
                .addFilterWithCriteria(Criteria.create(CURRENT_RUN_ID, SolrOperator.IN, runs))
                .addFacetPivot(CURRENT_RUN_ID)
                .addFacetPivot(TASK_STATE)
                .setRows(0)
                .setFacetPivotAsOne(true)
                .setFacetPivotMinCount(1);

        QueryResponse resp = fileCatService.runQuery(query.build());
        for (int i = 0; i < resp.getFacetPivot().size(); ++i) {
            List<PivotField> runData = resp.getFacetPivot().getVal(i);
            for (PivotField aRunData : runData) {
                Long runId = (Long) aRunData.getValue();
                List<PivotField> states = aRunData.getPivot();
                states.forEach(state -> {
                    String value = (String) state.getValue();
                    int amount = state.getCount();
                    result.put(runId, TaskState.valueOf(value), amount);
                });
            }
        }
        return result;
    }

    public long getIngestTaskCount(long runId) {
        return getIngestTaskCount(Lists.newArrayList(runId));
    }

    public long getIngestTaskCount(List<Long> runs) {
        if (runs == null || runs.isEmpty()) {
            return 0;
        }

        Query query = Query.create()
                .addFilterWithCriteria(Criteria.create(CURRENT_RUN_ID, SolrOperator.IN, runs))
                .setRows(0);
        QueryResponse resp = fileCatService.runQuery(query.build());
        return resp.getResults().getNumFound();
    }

    public List<SolrFileEntity> getIngestTasks(Long runId, TaskState state, Integer limit) {
        Query query = Query.create()
                .addFilterWithCriteria(Criteria.create(CURRENT_RUN_ID, SolrOperator.EQ, runId))
                .addFilterWithCriteria(Criteria.create(TASK_STATE, SolrOperator.EQ, state.name()))
                .setRows(limit);

        return fileCatService.find(query.build());
    }

    @AutoAuthenticate
    public List<SolrFileEntity> getIdleIngestTasks(long olderThan, int limit, List<Long> runIds) {
        if (runIds == null || runIds.isEmpty()) {
            return new ArrayList<>();
        }

        Query query = Query.create().addFilterWithCriteria(Criteria.create(CURRENT_RUN_ID, SolrOperator.IN, runIds))
                .addFilterWithCriteria(Criteria.create(TASK_STATE, SolrOperator.NE, TaskState.NEW.name()))
                .addFilterWithCriteria(Criteria.create(TASK_STATE_DATE, SolrOperator.LT, olderThan))
                .setRows(limit);

        return fileCatService.find(query.build());
    }

    @AutoAuthenticate
    public List<Long> getIngestTasksIds(Long runId, TaskState state) {
        Query query = Query.create()
                .addFilterWithCriteria(Criteria.create(CURRENT_RUN_ID, SolrOperator.EQ, runId))
                .addFilterWithCriteria(Criteria.create(TASK_STATE, SolrOperator.EQ, state.name()))
                .addField(ID).addField(FILE_ID);

        List<SolrFileEntity> entities = fileCatService.find(query.build());
        return entities.stream().map(SolrFileEntity::getFileId).collect(Collectors.toList());
    }

    @AutoAuthenticate
    public void updateTaskStateByFileId(TaskState state, Collection<Long> fileIds) {
        if (fileIds == null || fileIds.isEmpty()) {
            return;
        }

        updateFileStates(state, FILE_ID.inQuery(fileIds));
    }

    @AutoAuthenticate
    public void updateFilesForIngest(Long runId) {
        updateFileStates(TaskState.NEW, CURRENT_RUN_ID.filter(runId));
    }

    @AutoAuthenticate
    public void endTaskForFiles(Collection<Long> fileIds) {
        if (fileIds == null || fileIds.isEmpty()) {
            return;
        }

        Integer cnged = updateFilesByFilter(FILE_ID.inQuery(fileIds), null, null, null, null, null);

        logger.trace("remove ingest task data from files {} chnged {}", fileIds, cnged);
    }

    @AutoAuthenticate
    public Integer updateFilesForIngestStart(Long runId, Collection<Long> fileIds, String params) {
        if (fileIds == null || fileIds.isEmpty()) {
            return null;
        }

        return updateFilesByFilter(FILE_ID.inQuery(fileIds), TaskState.NEW.name(),
                String.valueOf(System.currentTimeMillis()), String.valueOf(runId), "0", params);
    }

    @AutoAuthenticate
    public void endTaskForIngest(Long runId) {
        Integer cnged = updateFilesByFilter(CURRENT_RUN_ID.filter(runId), null, null, null, null, null);
        logger.trace("remove ingest task data from run {} cnged {}", runId, cnged);
    }

    @AutoAuthenticate
    public void taskRetryIncFiles(Collection<Long> fileIds) {
        if (fileIds == null || fileIds.isEmpty()) {
            return;
        }

        List<SolrFileEntity> files = fileCatService.find(Query.create()
                .addFilterWithCriteria(Criteria.create(FILE_ID, SolrOperator.IN, fileIds))
                .setRows(fileIds.size())
                .addField(FILE_ID).addField(ID)
                .build()
        );
        Map<Long, String> fileIdsMap = files.stream().collect(
                Collectors.toMap(SolrFileEntity::getFileId, SolrFileEntity::getId, SolrTaskService::keyCollisionResolver));

        List<SolrInputDocument> fileCountUpdates = fileIds.stream()
                .filter(fileIdsMap::containsKey)
                .map(fId ->
                        createAtomicUpdateRetry(AtomicUpdateBuilder.create()
                                .setId(ID, fileIdsMap.get(fId)))
                )
                .collect(Collectors.toList());
        if (!fileCountUpdates.isEmpty()) {
            fileCatService.saveAll(fileCountUpdates);
            fileCatService.softCommitNoWaitFlush();
        }
    }

    public static <T> T keyCollisionResolver(T obj1, T obj2){
        logger.warn("Collision between keys during collection of stream into map: {} vs. {}", obj1, obj2);
        if (obj1 != null && obj2 != null) {
            try {
                String id1 = obj1.toString();
                String id2 = obj2.toString();

                if (id1.contains("!") && id2.contains("!")) {
                    FileCatCompositeId fid1 = FileCatCompositeId.of(id1);
                    FileCatCompositeId fid2 = FileCatCompositeId.of(id2);
                    if (fid1.getContentId() > fid2.getContentId()) {
                        return obj2;
                    }
                }
            } catch (Exception e) {
                logger.error("Error solving Collision between keys during collection of stream into map: {} vs. {}", obj1, obj2, e);
            }
        }
        return obj1;
    }

    //////////////////////////////// content methods (analysis)

    public Table<Long, TaskState, Number> getAnalysisTaskByRunAndStateNum(List<Long> runs) {
        Table<Long, TaskState, Number> result = HashBasedTable.create();

        if (runs == null || runs.isEmpty()) {
            return result;
        }

        Query query = Query.create()
                .addFilterWithCriteria(Criteria.create(CURRENT_RUN_ID, SolrOperator.IN, runs))
                .addFacetPivot(CURRENT_RUN_ID)
                .addFacetPivot(TASK_STATE)
                .setRows(0)
                .setFacetPivotAsOne(true)
                .setFacetPivotMinCount(1);

        QueryResponse resp = contentMetadataService.runQuery(query.build());
        for (int i = 0; i < resp.getFacetPivot().size(); ++i) {
            List<PivotField> runData = resp.getFacetPivot().getVal(i);
            for (PivotField aRunData : runData) {
                Long runId = (Long) aRunData.getValue();
                List<PivotField> states = aRunData.getPivot();
                states.forEach(state -> {
                    String value = (String) state.getValue();
                    int amount = state.getCount();
                    result.put(runId, TaskState.valueOf(value), amount);
                });
            }
        }
        return result;
    }

    public long getAnalysisTaskCount(long runId) {
        Query query = Query.create()
                .addFilterWithCriteria(Criteria.create(CURRENT_RUN_ID, SolrOperator.EQ, runId))
                .setRows(0);
        QueryResponse resp = contentMetadataService.runQuery(query.build());
        return resp.getResults().getNumFound();
    }

    public long getAnalysisTaskCount(List<Long> runs) {
        if (runs == null || runs.isEmpty()) {
            return 0;
        }

        Query query = Query.create()
                .addFilterWithCriteria(Criteria.create(CURRENT_RUN_ID, SolrOperator.IN, runs))
                .setRows(0);
        QueryResponse resp = contentMetadataService.runQuery(query.build());
        return resp.getResults().getNumFound();
    }

    public List<SolrContentMetadataEntity> getAnalysisTasks(Long runId, TaskState state, Integer limit) {
        Query query = Query.create()
                .addFilterWithCriteria(Criteria.create(CURRENT_RUN_ID, SolrOperator.EQ, runId))
                .addFilterWithCriteria(Criteria.create(TASK_STATE, SolrOperator.EQ, state.name()))
                .setRows(limit);

        return contentMetadataService.find(query.build());
    }

    @AutoAuthenticate
    public List<Long> getAnalysisTasksIds(Long runId, TaskState state) {
        Query query = Query.create()
                .addFilterWithCriteria(Criteria.create(CURRENT_RUN_ID, SolrOperator.EQ, runId))
                .addFilterWithCriteria(Criteria.create(TASK_STATE, SolrOperator.EQ, state.name()))
                .addField(ID);

        List<SolrContentMetadataEntity> entities = contentMetadataService.find(query.build());
        return entities.stream().map(SolrContentMetadataEntity::getId).map(Long::valueOf).collect(Collectors.toList());
    }

    @AutoAuthenticate
    public void updateTaskStateByContentId(TaskState state, Collection<Long> contentIds) {
        if (contentIds == null || contentIds.isEmpty()) {
            return;
        }
        updateContentsByFilter(ID.inQuery(contentIds), state);
    }

    @AutoAuthenticate
    public Integer endTaskForContents(Collection<Long> contentIds) {
        if (contentIds == null || contentIds.isEmpty()) {
            return 0;
        }

        List<List<Long>> partitions;
        if (contentIds.size() > solrMaxBooleanClauses) {
            partitions = Lists.partition(new ArrayList<>(contentIds), solrMaxBooleanClauses);
        } else {
            partitions = new ArrayList<>();
            partitions.add(new ArrayList<>(contentIds));
        }

        int cnged = 0;
        for (List<Long> contents : partitions) {
            Integer res = updateContentsByFilter(ID.inQuery(contents), null, null, null, null);
            cnged += (res == null ? 0 : res);
        }

        logger.trace("remove analysis task data from contents {} cnged {}", contentIds, cnged);
        return cnged;
    }

    @AutoAuthenticate
    public void taskRetryIncContents(Collection<Long> contentIds) {
        if (contentIds == null || contentIds.isEmpty()) {
            return;
        }
        List<SolrInputDocument> fileCountUpdates = contentIds.stream()
                .map(cId ->
                        createAtomicUpdateRetry(AtomicUpdateBuilder.create()
                                .setId(ContentMetadataFieldType.ID, cId))
                )
                .collect(Collectors.toList());
        if (!fileCountUpdates.isEmpty()) {
            contentMetadataService.saveAll(fileCountUpdates);
            contentMetadataService.softCommitNoWaitFlush();
        }
    }

    @AutoAuthenticate
    public Integer updateContentsForAnalysisStart(Long runId, Collection<Long> contentIds) {
        if (contentIds == null || contentIds.isEmpty()) {
            return null;
        }

        return updateContentsByFilter(ID.inQuery(contentIds), TaskState.NEW.name(),
                String.valueOf(System.currentTimeMillis()), String.valueOf(runId), "0");
    }

    @AutoAuthenticate
    public void updateContentsForAnalysis(Long runId) {
        updateContentsByFilter(CURRENT_RUN_ID.filter(runId), TaskState.NEW);
    }

    @AutoAuthenticate
    public void endTaskForAnalysis(Long runId) {
        Integer cnged = updateContentsByFilter(CURRENT_RUN_ID.filter(runId), null, null, null, null);
        logger.trace("remove analysis task data from run {} cnged {}", runId, cnged);
    }

    @AutoAuthenticate
    public List<SolrContentMetadataEntity> getIdleAnalysisTasks(long olderThan, int limit, List<Long> runIds) {
        if (runIds == null || runIds.isEmpty()) {
            return new ArrayList<>();
        }

        Query query = Query.create().addFilterWithCriteria(Criteria.create(CURRENT_RUN_ID, SolrOperator.IN, runIds))
                .addFilterWithCriteria(Criteria.create(TASK_STATE, SolrOperator.NE, TaskState.NEW.name()))
                .addFilterWithCriteria(Criteria.create(TASK_STATE_DATE, SolrOperator.LT, olderThan))
                .setRows(limit);

        return contentMetadataService.find(query.build());
    }

    //////////////////////////////// private methods

    private Integer updateFileStates(TaskState state, String filter) {
        return fileCatService.updateFieldByQueryMultiFields(
                new String[]{TASK_STATE.getSolrName(), TASK_STATE_DATE.getSolrName()},
                new String[]{state.name(), String.valueOf(System.currentTimeMillis())},
                SolrFieldOp.SET.getOpName(),
                filter, null,
                commitType
        );
    }

    private Integer updateFilesByFilter(String filter, String taskState, String changeDate, String runId, String retries, String params) {
        return fileCatService.updateFieldByQueryMultiFields(
                new String[]{TASK_STATE.getSolrName(), TASK_STATE_DATE.getSolrName(),
                        CURRENT_RUN_ID.getSolrName(), RETRIES.getSolrName(), TASK_PARAM.getSolrName()},
                new String[]{taskState, changeDate, runId, retries, params},
                SolrFieldOp.SET.getOpName(),
                filter, null,
                commitType
        );
    }

    private SolrInputDocument createAtomicUpdateRetry(AtomicUpdateBuilder builder) {
        return builder
                .addModifier(RETRIES, SolrOperator.INC, 1)
                .addModifier(TASK_STATE, SolrOperator.SET, TaskState.NEW.name())
                .addModifier(TASK_STATE_DATE, SolrOperator.SET, System.currentTimeMillis())
                .build();
    }

    private Integer updateContentsByFilter(String filter, String taskState, String changeDate, String runId, String retries) {
        return contentMetadataService.updateFieldByQueryMultiFields(
                new String[]{TASK_STATE.getSolrName(), TASK_STATE_DATE.getSolrName(),
                        CURRENT_RUN_ID.getSolrName(), RETRIES.getSolrName()},
                new String[]{taskState, changeDate, runId, retries},
                SolrFieldOp.SET.getOpName(),
                filter, null,
                commitType
        );
    }

    private Integer updateContentsByFilter(String filter, TaskState taskState) {
        return contentMetadataService.updateFieldByQueryMultiFields(
                new String[]{TASK_STATE.getSolrName(), TASK_STATE_DATE.getSolrName()},
                new String[]{taskState.name(), String.valueOf(System.currentTimeMillis())},
                SolrFieldOp.SET.getOpName(),
                filter, null,
                commitType
        );
    }

    //////////////////////////////// static methods

    public static SimpleTask createForIngest(SolrFileEntity file, Long jobId) {
        SimpleTask task = new SimpleTask(TaskType.INGEST_FILE);
        task.setId(file.getFileId());
        task.setJsonData(file.getTaskParameters());
        task.setItemId(file.getFileId());
        task.setRunContext(file.getCurrentRunId());
        task.setState(TaskState.valueOf(file.getTaskState()));
        task.setStateUpdateTime(file.getTaskStateDate());
        task.setRetries(file.getRetries());
        task.setName(TaskType.INGEST_FILE.name());
        task.setJobId(jobId);
        return task;
    }

    public static SimpleTask createForAnalysis(SolrContentMetadataEntity content, Long jobId) {
        SimpleTask task = new SimpleTask(TaskType.ANALYZE_CONTENT);
        task.setId(Long.parseLong(content.getId()));
        task.setItemId(Long.parseLong(content.getId()));
        task.setRunContext(content.getCurrentRunId());
        task.setState(TaskState.valueOf(content.getTaskState()));
        task.setStateUpdateTime(content.getTaskStateDate());
        task.setRetries(content.getRetries());
        task.setName(TaskType.ANALYZE_CONTENT.name());
        task.setJobId(jobId);
        return task;
    }

    public static void setNewTask(SolrInputDocument doc, Long runContext, String params) {
        doc.setField(CatFileFieldType.CURRENT_RUN_ID.getSolrName(), SolrRepositoryUtils.setValueMap(runContext));
        doc.setField(CatFileFieldType.TASK_STATE.getSolrName(), SolrRepositoryUtils.setValueMap(TaskState.NEW.name()));
        doc.setField(CatFileFieldType.TASK_STATE_DATE.getSolrName(), SolrRepositoryUtils.setValueMap(System.currentTimeMillis()));
        doc.setField(CatFileFieldType.RETRIES.getSolrName(), SolrRepositoryUtils.setValueMap(0));
        if (params != null) {
            doc.setField(CatFileFieldType.TASK_PARAM.getSolrName(), SolrRepositoryUtils.setValueMap(params));
        }
    }

    public static SolrInputDocument createNewTask(String id, Long runId, String params) {
        AtomicUpdateBuilder builder = AtomicUpdateBuilder.create();
        builder.setId(ID, id)
            .addModifierNotNull(CURRENT_RUN_ID, SET, runId)
            .addModifierNotNull(TASK_STATE, SET, TaskState.NEW.name())
            .addModifierNotNull(TASK_STATE_DATE, SET, System.currentTimeMillis())
            .addModifierNotNull(RETRIES, SET, 0);
        if (params != null) {
            builder.addModifierNotNull(TASK_PARAM, SET, params);
        }
        return builder.build();
    }
}