package com.cla.filecluster.jobmanager.service;

import com.cla.common.domain.dto.crawler.*;
import com.cla.common.domain.dto.filetree.CrawlRunDetailsDto;
import com.cla.common.domain.dto.filetree.RootFolderDto;
import com.cla.common.domain.dto.jobmanager.*;
import com.cla.common.domain.dto.schedule.ScheduleGroupDto;
import com.cla.connector.utils.Pair;
import com.cla.connector.utils.TimeSource;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.entity.crawler.CrawlRun;
import com.cla.filecluster.domain.exceptions.StoppedByUserException;
import com.cla.filecluster.jobmanager.domain.entity.SimpleJob;
import com.cla.filecluster.jobmanager.repository.jpa.SimpleJobRepository;
import com.cla.filecluster.service.label.LabelingService;
import com.cla.filecluster.repository.jpa.crawler.CrawlRunRepository;
import com.cla.filecluster.repository.jpa.filetree.RootFolderRepository;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.crawler.FileCrawlerExecutionDetailsService;
import com.cla.filecluster.service.schedule.ScheduleGroupService;
import com.cla.filecluster.util.ControllerUtils;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.util.*;
import java.util.stream.Collectors;

import static com.cla.common.domain.dto.jobmanager.JobState.*;

@Service
public class JobManagerAppService {
    private static final String STATUS_COUNTER_CACHE_KEY = "countRootFoldersPerRunStatus";
    private static final String SUSPENDED = "Suspended";
    private static final Long LABELING_RUN_ID = -99L;
    private static final String LABELING_COUNTER_CACHE_KEY = "countLabelingRunStatus";

    private Logger logger = LoggerFactory.getLogger(JobManagerAppService.class);

    @Value("${job-manager.rate-calc-interval-sec:20}")
    private long rateCalcIntervalSec;

    @Autowired
    private SimpleJobRepository simpleJobRepository;

    @Autowired
    private CrawlRunRepository crawlRunRepository;

    @Autowired
    private JobManagerService jobManagerService;

    @Autowired
    private ScheduleGroupService scheduleGroupService;

    @Autowired
    private RootFolderRepository rootFolderRepository;

    private TimeSource timeSource = new TimeSource();

    @Autowired
    private JobConsistencyManager jobConsistencyManager;

    @Autowired
    private MessageHandler messageHandler;

    @Autowired
    private FileCrawlerExecutionDetailsService fileCrawlerExecutionDetailsService;

    @Autowired
    private ControllerUtils controllerUtils;

    @Autowired
    private LabelingService labelingService;

    public void waitForRunToFinish(Long runId) {
        int counter = 0;
        long activeJobsCount;
        do {
            if (counter % 10 == 0) {
                logger.debug("Get crawlRun {}", runId);
            }

            CrawlRun run = fileCrawlerExecutionDetailsService.getCrawlRun(runId);
            if (!run.getRunStatus().equals(RunStatus.RUNNING)) {
                logger.debug("Run {} ended with status {}", runId, run.getRunStatus());
                return;
            }

            List<SimpleJobDto> runJobs = jobManagerService.getRunJobs(runId);
            Optional<SimpleJobDto> pausedJobFound = runJobs.stream().filter(f -> f.getState().equals(JobState.PAUSED)).findAny();
            if (pausedJobFound.isPresent()) {
                logger.debug("Run {} is paused on job {}", runId,pausedJobFound.get());
                return;
            }
            activeJobsCount = runJobs.stream().filter(job -> job.getState().isActive()).count();

            if (activeJobsCount > 0) {
                if (counter % 10 == 0) {
                    logger.debug("Waiting for run {} to finish", runId);
                    for (SimpleJobDto runJob : runJobs) {
                        logger.debug("RunJob: {}", runJob);
                    }
                }
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    throw new StoppedByUserException("Stopped by user while waiting for run to finish");
                }
            }
            counter++;
        } while (activeJobsCount > 0);
        logger.debug("Run {} has finished.", runId);
    }

    @Transactional(readOnly = true)
    public List<PhaseDetailsDto> listRunJobs(long runId) {
        List<PhaseDetailsDto> phaseDetailsDtos = null;
        if (LABELING_RUN_ID.equals(runId)) {
            Pair<Long, Long> result = getLabelingCountStatus();
            PhaseDetailsDto dto = new PhaseDetailsDto();
            dto.setName("Labeling extraction");
            dto.setJobState(result.getKey() == 0 ? JobState.DONE : JobState.IN_PROGRESS);
            dto.setPhaseCounter(result.getValue().intValue());
            dto.setPhaseMaxMark(result.getKey() + result.getValue());
            dto.setStateDescription(result.getKey() == 0 ? "Done" : "Running labeling extraction");
            dto.setJobId(LABELING_RUN_ID);
            dto.setJobType(JobType.LABELING);
            dto.setCompletionStatus(result.getKey() == 0 ? JobCompletionStatus.OK : JobCompletionStatus.UNKNOWN);
            phaseDetailsDtos = new ArrayList<>();
            phaseDetailsDtos.add(dto);
        } else {

            List<SimpleJobDto> runJobs = jobManagerService.getRunJobs(runId);
            runJobs.forEach(job -> {
                if (job.getType().equals(JobType.SCAN)) {
                    Long unprocessed = jobManagerService.getExtraScanTaskCountForJob(job.getId());
                    job.setCurrentTaskCount(job.getCurrentTaskCount() + unprocessed.intValue());
                }
            });
            phaseDetailsDtos = JobManagerService.convertSimpleJobDto(runJobs, jobManagerService.getPhaseRunningRateDetailsMap());
        }
        return phaseDetailsDtos;
    }

    private Pair<Long, Long> getLabelingCountStatus() {
        Object element = controllerUtils.getFromCache(LABELING_COUNTER_CACHE_KEY);
        if (element != null) {
            return (Pair<Long, Long>)element;
        }
        Pair<Long, Long> result = labelingService.getLabelingRunStatus();
        controllerUtils.putInCache(LABELING_COUNTER_CACHE_KEY, result);
        return result;
    }

    @Transactional
    public void setEnqTasksToNewForJob(List<Long> jobIdsList) {
        for (Long jobId : jobIdsList) {
            jobManagerService.setEnqTasksToNewForJob(jobId);
        }
    }

    @Transactional
    public void setEnqTasksToNewForRun(List<Long> runIdsList) {
        for (Long runId : runIdsList) {
            List<SimpleJobDto> runJobs = jobManagerService.getRunJobs(runId);
            runJobs.stream().filter(j -> j.getState().isStarted())
                    .forEach(j -> jobManagerService.setEnqTasksToNewForJob(j.getId()));
        }
    }

    @Transactional
    public void activateAnalyzeWhileIngestIsRunning(long runContext) {
        jobManagerService.activateAnalyzeWhileIngestIsRunning(runContext);
    }

    @Transactional(readOnly = true)
    public Page<CrawlRunDetailsDto> listRuns(DataSourceRequest dataSourceRequest) {
        dataSourceRequest.addDefaultSort("id");
        PageRequest pageRequest = dataSourceRequest.createPageRequestWithSort();

        Page<CrawlRun> allRuns;
        if (dataSourceRequest.getNamingSearchTerm() != null) {
            List<BigInteger> ids = crawlRunRepository.findBySearchTerm(dataSourceRequest.getNamingSearchTerm(), pageRequest.getOffset(), pageRequest.getPageSize());
            allRuns = getRunForIdsWithPageRequest(ids, pageRequest);
        } else if (dataSourceRequest.getRootFolderId() != null) {
            allRuns = crawlRunRepository.findByRootFolder(dataSourceRequest.getRootFolderId(), pageRequest);
        } else {
            allRuns = crawlRunRepository.findAll(pageRequest);
        }

        if (dataSourceRequest.getRootFolderId() == null && dataSourceRequest.getNamingSearchTerm() == null) {
            CrawlRun labelingRun = createLabelingRunDataForDisplay();
            if (labelingRun != null) {
                List<CrawlRun> data = new ArrayList<>();
                data.add(labelingRun);
                data.addAll(allRuns.getContent());
                allRuns = new PageImpl<>(data, pageRequest, allRuns.getTotalElements() + 1);
            }
        }

        Page<CrawlRunDetailsDto> crawlRunDetailsDtos = FileCrawlerExecutionDetailsService.convertCrawlRuns(allRuns, pageRequest);
        scheduleGroupService.fillScheduleGroupDetails(crawlRunDetailsDtos);
        return crawlRunDetailsDtos;
    }

    @Transactional(readOnly = true)
    public Page<CrawlRunDetailsDto> listRuns(DataSourceRequest dataSourceRequest, List<RunStatus> filterByRunStatus) {
        PageRequest pageRequestWithSort = dataSourceRequest.createPageRequestWithSort();
        Page<CrawlRun> filteredRuns;
        if(dataSourceRequest.getRootFolderId() != null) {
            filteredRuns = crawlRunRepository.findByStatesForRootFolder(dataSourceRequest.getRootFolderId(), filterByRunStatus, pageRequestWithSort);
        } else {
            if (dataSourceRequest.getNamingSearchTerm() != null) {
                List<String> states = filterByRunStatus.stream().map(Enum::name).collect(Collectors.toList());
                List<BigInteger> ids = crawlRunRepository.findByStates(states, dataSourceRequest.getNamingSearchTerm(), pageRequestWithSort.getOffset(), pageRequestWithSort.getPageSize());
                int total = crawlRunRepository.countByStates(states, dataSourceRequest.getNamingSearchTerm());
                filteredRuns =  new PageImpl<>(getRunForIds(ids), pageRequestWithSort, total);
            } else {
                filteredRuns = crawlRunRepository.findByStates(filterByRunStatus, pageRequestWithSort);
            }
        }
        if (dataSourceRequest.getNamingSearchTerm() == null && dataSourceRequest.getRootFolderId() == null) {
            CrawlRun labelingRun = createLabelingRunDataForDisplay();
            if (labelingRun != null && filterByRunStatus.contains(labelingRun.getRunStatus())) {
                List<CrawlRun> data = new ArrayList<>();
                data.add(labelingRun);
                data.addAll(filteredRuns.getContent());
                filteredRuns = new PageImpl<>(data, pageRequestWithSort, filteredRuns.getTotalElements() + 1);
            }
        }
        Page<CrawlRunDetailsDto> crawlRunDetailsDtos = FileCrawlerExecutionDetailsService.convertCrawlRuns(filteredRuns, pageRequestWithSort);
        scheduleGroupService.fillScheduleGroupDetails(crawlRunDetailsDtos);
        return crawlRunDetailsDtos;
    }

    private CrawlRun createLabelingRunDataForDisplay() {
        Pair<Long, Long> result = getLabelingCountStatus();
        if (result.getKey() == 0 && result.getValue() == 0) {
            return null;
        }
        CrawlRun run = new CrawlRun();
        run.setId(LABELING_RUN_ID);
        run.setManual(false);
        run.setRootFolderAccessible(true);
        run.setStateString(result.getKey() == 0 ? "Done" : "Running labeling extraction");
        run.setTotalProcessedFiles(result.getValue());
        run.setRunType(RunType.LABELING);
        run.setRunStatus(result.getKey() == 0 ? RunStatus.FINISHED_SUCCESSFULLY : RunStatus.RUNNING);
        return run;
    }

    private List<CrawlRun> getRunForIds(List<BigInteger> ids) {
        List<Long> idsAsLong = ids.stream().map(BigInteger::longValue).collect(Collectors.toList());
        Set<CrawlRun> runs = (ids.size() > 0) ? crawlRunRepository.findByIds(idsAsLong) : new HashSet<>();
        return Lists.newArrayList(runs);
    }


    private Page<CrawlRun> getRunForIdsWithPageRequest(List<BigInteger> ids,PageRequest pageRequest) {
        List<Long> idsAsLong = ids.stream().map(BigInteger::longValue).collect(Collectors.toList());
        return (ids.size() > 0) ? crawlRunRepository.findByIdsWithPageRequest(idsAsLong, pageRequest) : new PageImpl<>(new ArrayList<>());
    }

    @Transactional(readOnly = true)
    public List<RunSummaryInfoDto> getActiveRunsDetails(DataSourceRequest dataSourceRequest) {

        List<CrawlRun> activeCrawlRuns;
        if (dataSourceRequest.getNamingSearchTerm() != null) {
            activeCrawlRuns = getRunForIds(crawlRunRepository.getCrawlRunsByRunStatus(RunStatus.RUNNING.name(), dataSourceRequest.getNamingSearchTerm()));
        } else {
            activeCrawlRuns = crawlRunRepository.getCrawlRunsByRunStatus(RunStatus.RUNNING);
        }

        if (activeCrawlRuns.isEmpty()) {
            return Collections.<RunSummaryInfoDto>emptyList();
        }
        List<Long> activeCrawlRunIds = activeCrawlRuns.stream().map(CrawlRun::getId).collect(Collectors.toList());
        List<SimpleJob> jobsByState =
                simpleJobRepository.findJobsByStates(Lists.newArrayList(IN_PROGRESS, READY, WAITING, PENDING), activeCrawlRunIds);
        Map<Long, List<SimpleJob>> jobsByRunId =
                jobsByState.stream().collect(Collectors.groupingBy(SimpleJob::getRunContext));

        return activeCrawlRuns
                .stream().filter(r->jobsByRunId.containsKey(r.getId()) && jobsByRunId.get(r.getId()).size() > 0)
                .map(r -> getRunSummaryInfoDto(r, jobsByRunId.get(r.getId())))
                .sorted(Comparator.comparing(u -> u.getCrawlRunDetailsDto().getRootFolderDto() == null ?
                        u.getRunningJobName() : u.getCrawlRunDetailsDto().getRootFolderDto().getIdentifier()))
                .collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    public RootFoldersAggregatedSummaryInfo getRootFoldersAggregatedSummaryInfo(DataSourceRequest dataSourceRequest) {


        List<Object[]> result;
        if (dataSourceRequest.getNamingSearchTerm() != null) {
            logger.debug("Root folders aggregated summary info with search - calculate it.");
            result = rootFolderRepository.countRootFoldersPerRunStatus(dataSourceRequest.getNamingSearchTerm());
        } else {
            Object element = controllerUtils.getFromCache(STATUS_COUNTER_CACHE_KEY);
            if (element != null) {
                logger.debug("cached Root folders aggregated summary info exists... using it.");
                return (RootFoldersAggregatedSummaryInfo)element;
            }
            logger.debug("Root folders aggregated summary info not cached - calculate it.");
            result = rootFolderRepository.countRootFoldersPerRunStatus();
        }

        RootFoldersAggregatedSummaryInfo rootFoldersAggregatedSummaryInfo = createRootFoldersAggregatedSummaryInfo(result);

        if (dataSourceRequest.getNamingSearchTerm() == null) {
            controllerUtils.putInCache(STATUS_COUNTER_CACHE_KEY, rootFoldersAggregatedSummaryInfo);
        }

        return rootFoldersAggregatedSummaryInfo;

    }

    private static void addToSummaryInfo(HashMap<String, Long> runStatusCount, String key, Long value) {
        Long amount = runStatusCount.getOrDefault(key, 0L);
        runStatusCount.put(key, (value + amount));
    }

    public static RootFoldersAggregatedSummaryInfo createRootFoldersAggregatedSummaryInfo(List<Object[]> result) {
        RootFoldersAggregatedSummaryInfo rootFoldersAggregatedSummaryInfo = new RootFoldersAggregatedSummaryInfo();
        HashMap<String, Long> runStatusCount = new HashMap<>();
        result.forEach(line ->{
            if(line[0] != null) {
                if (RunStatus.PAUSED.name().equals(line[0]) && line[1] != null) {
                    if (PauseReason.USER_INITIATED.name().equals(line[1])) {
                        addToSummaryInfo(runStatusCount, (String) line[0], ((Number) line[2]).longValue());
                    } else {
                        addToSummaryInfo(runStatusCount, SUSPENDED, ((Number) line[2]).longValue());
                    }
                } else {
                    addToSummaryInfo(runStatusCount, (String) line[0], ((Number) line[2]).longValue());
                }
            } else {
                addToSummaryInfo(runStatusCount, RunStatus.NA.name(), ((Number) line[2]).longValue());
            }
        });
        Long sum = runStatusCount.entrySet().stream().mapToLong(i->i.getValue()).sum();
        rootFoldersAggregatedSummaryInfo.setTotalRootFoldersCount(sum);
        rootFoldersAggregatedSummaryInfo.setScannedRootFoldersCount(runStatusCount.getOrDefault(RunStatus.FINISHED_SUCCESSFULLY.name(),0L).longValue());
        rootFoldersAggregatedSummaryInfo.setNewRootFoldersCount(runStatusCount.getOrDefault(RunStatus.NA.name(),0L).longValue());
        rootFoldersAggregatedSummaryInfo.setFailedRootFoldersCount(runStatusCount.getOrDefault(RunStatus.FAILED.name(),0L).longValue());
        rootFoldersAggregatedSummaryInfo.setRunningRootFoldersCount(runStatusCount.getOrDefault(RunStatus.RUNNING.name(),0L).longValue());
        rootFoldersAggregatedSummaryInfo.setPausedRootFoldersCount(runStatusCount.getOrDefault(RunStatus.PAUSED.name(),0L).longValue());
        rootFoldersAggregatedSummaryInfo.setStoppedRootFoldersCount(runStatusCount.getOrDefault(RunStatus.STOPPED.name(),0L).longValue());
        rootFoldersAggregatedSummaryInfo.setSuspendedRootFoldersCount(runStatusCount.getOrDefault(SUSPENDED,0L).longValue());
        return rootFoldersAggregatedSummaryInfo;
    }

    private RunSummaryInfoDto getRunSummaryInfoDto(CrawlRun r, List<SimpleJob> jobsByState) {
        RunSummaryInfoDto runSummaryInfoDto = FileCrawlerExecutionDetailsService.convertRunSummaryInfo(r, jobsByState, jobManagerService.getPhaseRunningRateDetailsMap());
        if (r.getScheduleGroupId() != null) {
            ScheduleGroupDto scheduleGroup = scheduleGroupService.getScheduleGroup(r.getScheduleGroupId());
            if (scheduleGroup != null) {
                runSummaryInfoDto.getCrawlRunDetailsDto().setScheduleGroupName(scheduleGroup.getName());
            } else {
                logger.warn("CrawlRun is connected to a schedule group that was probably deleted ({})",r.getScheduleGroupId());
            }
        }
        return runSummaryInfoDto;
    }

    public void markStatusChange() {
        controllerUtils.removeFromCache(STATUS_COUNTER_CACHE_KEY);
    }

    public void resumeRuns(List<Long> runIds) {
        runIds.forEach(jobConsistencyManager::resumeRun);
        markStatusChange();
    }

    public void resumeAllRuns() {
        jobConsistencyManager.resumeAllPausedJobs();
        markStatusChange();
    }

    public void pauseRuns(List<Long> runIds) {
        runIds.forEach(r -> jobConsistencyManager.pauseRun(r, PauseReason.USER_INITIATED));
        markStatusChange();
    }

    public void pauseAllRuns() {
        jobConsistencyManager.pauseAllJobs(PauseReason.USER_INITIATED);
        markStatusChange();
    }

    public void endRun(Long runContext, PauseReason pauseReason) {
        fileCrawlerExecutionDetailsService.handleFinishedRun(runContext, pauseReason);
        markStatusChange();
    }

    public void stopPausedRuns(List<Long> runIds) {
        runIds.forEach(r -> jobConsistencyManager.stopPausedRun(r, PauseReason.USER_INITIATED));
        markStatusChange();
    }

    public void stopRunsGraceful(List<Long> runIds) {
        runIds.forEach(r -> jobConsistencyManager.stopRunGraceful(r, PauseReason.USER_INITIATED));
        markStatusChange();
    }

    public Boolean resumeJob(long runId, JobType jobType) {
        boolean jobResumed = false;
        List<Long> jobIds = jobManagerService.getJobIdsByRunAndType(runId, jobType);
        if (jobIds.size() == 0) {
            logger.debug("No jobs to resume in run {} of type {}", runId, jobIds);
        } else {
            jobResumed = true;
            for (Long jobId : jobIds) {
                jobResumed &= resumeJob(jobId);
            }
        }
        return jobResumed;
    }

    public boolean resumeJob(long jobId) {
        return resumeJob(jobId, false);
    }

    public boolean resumeJob(long jobId, boolean force) {
        SimpleJobDto jobDto = jobManagerService.getJobAsDto(jobId);
        if (jobDto==null) {
            logger.warn("Error while trying to resume: job {} not found", jobId);
            throw new RuntimeException("Job " + jobId + " not found");
        }
        long runId = jobDto.getRunContext();
        JobState jobState = jobDto.getState();
        if (jobState != JobState.PAUSED && jobState != JobState.DONE) {
            logger.warn("Can not resume job #{} while in state {}.", jobId, jobState);
            return false;
        }
        JobType jobType = jobDto.getType();
        if (!previousJobDone(runId, jobDto)) {
            logger.warn("Trying to resume {} job with id {} (of run {}) before its previous job ({}) finished (force={})",
                    jobType, jobId, runId, previousJobId(runId, jobDto), (force?"true":"false"));
            if (!force) {
                return false;
            }
        }
        boolean result = jobConsistencyManager.resumeJob(jobDto.getId());
        markStatusChange();
        return result;
    }

    private boolean previousJobDone(long runId, SimpleJobDto jobDto) {
        JobType jobType = jobDto.getType();
        if (jobType == JobType.SCAN) {
            return true;
        }
        JobState prevJobState;
        if (jobType == JobType.EXTRACT_FINALIZE) {
            SimpleJob prevJob = jobManagerService.getJobs(runId, prevJobType(jobType), jobDto.getItemId()).get(0);
            prevJobState = prevJob.getState();
        } else {
            Long prevJobId = jobManagerService.getJobIdCached(runId, prevJobType(jobType));
            SimpleJobDto prevJobDto = jobManagerService.getJobAsDto(prevJobId);
            prevJobState = prevJobDto.getState();
        }

        return prevJobState != JobState.DONE;
    }

    private Long previousJobId(long runId, SimpleJobDto jobDto) {
        JobType jobType = jobDto.getType();
        if (jobType == JobType.SCAN) {
            return null;
        }
        if (jobType == JobType.EXTRACT_FINALIZE) {
            SimpleJob prevJob = jobManagerService.getJobs(runId, prevJobType(jobType), jobDto.getItemId()).get(0);
            return prevJob.getId();
        } else {
            Long prevJobId = jobManagerService.getJobIdCached(runId, prevJobType(jobType));
            SimpleJobDto prevJobDto = jobManagerService.getJobAsDto(prevJobId);
            return prevJobDto.getId();
        }
    }

    // Job order and logic should be in this class (rather than encoded in the ENUM definition)
    private JobType prevJobType(final JobType jobType) {
        JobType prevType ;
        switch (jobType) {
            case UNKNOWN:
                prevType = JobType.UNKNOWN;
                break;
            case SCAN:
                prevType = null;
                break;
            case EXTRACT:
                prevType = JobType.ANALYZE;
                break;
            default:
                prevType = JobType.valueOf(jobType.ordinal()-1);
        }
        return prevType;
    }

    public Collection<JobType> nextJobTypes(final JobType jobType) {
        Objects.requireNonNull(jobType);
        Collection<JobType> nextTypes = new ArrayList<>(2);
        switch (jobType) {
            case UNKNOWN:
                nextTypes.add(JobType.UNKNOWN);
                break;
            case ANALYZE:
                nextTypes.add(JobType.ANALYZE_FINALIZE);
                nextTypes.add(JobType.EXTRACT);
                break;
            case EXTRACT_FINALIZE:
                // return an empty collection
                break;
            default:
                JobType nextType = JobType.valueOf(jobType.ordinal()+1);
                nextTypes.add(nextType);
        }
        return nextTypes;
    }

    public String getInProgressOpMsg(JobType jobType) {
        // TODO Itai: Make amends with the gods of object oriented for writing this code
        switch (jobType) {
            case SCAN:
                // using the first scan msg here. rescan's msg is: "Start remapping root folder"
                return messageHandler.getMessage("op-msg.start-map");
            case SCAN_FINALIZE:
                return messageHandler.getMessage("op-msg.start-map-fin");
            case INGEST:
                return messageHandler.getMessage("op-msg.start-ingest");
            case INGEST_FINALIZE:
                return messageHandler.getMessage("op-msg.start-ingest-fin");
            case OPTIMIZE_INDEXES:
                return messageHandler.getMessage("op-msg.start-opt-ind");
            case ANALYZE:
                return messageHandler.getMessage("op-msg.start-analyze");
            case ANALYZE_FINALIZE:
                return messageHandler.getMessage("op-msg.start-analyze-fin");
            case EXTRACT:
                return null; // null is what's used in AsyncAnalyzerManager.handleAnalyzeFinalization()
            case EXTRACT_FINALIZE:
                return messageHandler.getMessage("op-msg.start-extract-fin");
            default:
                throw new IllegalArgumentException("jobType " + jobType + " no supported");
        }
    }

    public void createAnalysisRunForRootFolder(RootFolderDto rootFolderDto, Long crawlRunId) {
        jobManagerService.createAnalysisRunForRootFolder(rootFolderDto, crawlRunId);
    }

    public void createIngestRunForRootFolder(RootFolderDto rootFolderDto, Long crawlRunId) {
        jobManagerService.createIngestRunForRootFolder(rootFolderDto, crawlRunId);
    }

    public void createFinalizeTasksIfNeeded(Long runContext) {
        jobManagerService.createFinalizeTasksIfNeeded(runContext);
    }

    public boolean pauseJob(long jobId, boolean force) {
        SimpleJobDto jobDto = jobManagerService.getJobAsDto(jobId);
        if (jobDto==null) {
            logger.info("Error while trying to resume: job {} not found", jobId);
            throw new RuntimeException(messageHandler.getMessage("job.missing"));
        }
        long runId = jobDto.getRunContext();
        JobState jobState = jobDto.getState();
        if (!jobState.isStarted()) {
            logger.info("Should not pause job #{} of run {} while in state {} (force={}).", jobId, runId, jobState, (force?"true":"false"));
            if (!force) {
                return false;
            }
        }

        return jobConsistencyManager.pauseJob(jobId, PauseReason.USER_INITIATED);
    }
}
