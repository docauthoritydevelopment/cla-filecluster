package com.cla.filecluster.jobmanager.service;

import com.cla.common.domain.dto.crawler.JobCompletionStatus;
import com.cla.common.domain.dto.crawler.PhaseDetailsDto;
import com.cla.common.domain.dto.crawler.JobStateByDate;
import com.cla.common.domain.dto.crawler.PhaseRunningRateDetails;
import com.cla.common.domain.dto.crawler.RunStatus;
import com.cla.common.domain.dto.filetree.RootFolderDto;
import com.cla.common.domain.dto.jobmanager.JobState;
import com.cla.common.domain.dto.jobmanager.JobStateUpdateInfo;
import com.cla.common.domain.dto.jobmanager.JobType;
import com.cla.common.domain.dto.jobmanager.SimpleJobDto;
import com.cla.common.utils.PersistableSafeCache;
import com.cla.common.utils.ReflectionUtils;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.connector.utils.Converters;
import com.cla.connector.utils.Pair;
import com.cla.connector.utils.TimeSource;
import com.cla.filecluster.batch.loader.IngestBatchLoader;
import com.cla.filecluster.batch.writer.analyze.BatchAnalyzeResultsExecutor;
import com.cla.filecluster.batch.writer.ingest.BatchIngestResultsExecutor;
import com.cla.filecluster.batch.writer.map.BatchMapResultsExecutor;
import com.cla.filecluster.config.security.AutoAuthenticate;
import com.cla.filecluster.domain.dto.SolrContentMetadataEntity;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.domain.entity.crawler.CrawlRun;
import com.cla.filecluster.domain.entity.filetree.RootFolder;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.domain.exceptions.StoppedByUserException;
import com.cla.filecluster.jobmanager.domain.entity.*;
import com.cla.filecluster.jobmanager.repository.jpa.SimpleJobRepository;
import com.cla.filecluster.jobmanager.repository.jpa.SimpleTaskRepository;
import com.cla.filecluster.jobmanager.repository.solr.SolrTaskService;
import com.cla.filecluster.mediaproc.ScanDoneConsumer;
import com.cla.filecluster.mediaproc.connectors.MediaProcessorCommService;
import com.cla.filecluster.repository.jpa.crawler.CrawlRunRepository;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.categories.FileCatService;
import com.cla.filecluster.service.crawler.FileCrawlerExecDetailsServiceTxUtil;
import com.cla.filecluster.service.files.filetree.DocStoreService;
import com.cla.filecluster.service.files.managers.OpStateService;
import com.cla.filecluster.service.scheduling.ControlledScheduling;
import com.cla.filecluster.util.executors.BlockingExecutor;
import com.cla.filecluster.util.query.DBTemplateUtils;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Table;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.PageRequest;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionTemplate;

import javax.annotation.PostConstruct;
import java.math.BigInteger;
import java.time.LocalDate;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

@SuppressWarnings({"WeakerAccess", "unused", "SameParameterValue", "UnusedReturnValue"})
@Service
public class JobManagerService implements ControlledScheduling {
    private static final Logger logger = LoggerFactory.getLogger(JobManagerService.class);

    private static final int DAYS_PER_PARTITION = 7;

    private ConcurrentMap<Long, PhaseRunningRateDetails> phaseRunningRateDetailsMap = new ConcurrentHashMap<>();

    // TODO Itai: this is just to resolve Spring circular dependencies. Resolve better
    @Autowired
    private CrawlRunRepository crawlRunRepository;

    @Autowired
    private SimpleJobRepository simpleJobRepository;

    @Autowired
    private SimpleTaskRepository simpleTaskRepository;

    @Autowired
    private SimpleTaskService simpleTaskService;

    @Autowired
    private OpStateService opStateService;

    @Autowired
    private DBTemplateUtils DBTemplateUtils;

    @Autowired
    private DocStoreService docStoreService;

    @Autowired
    private JobManagerServiceTxUtil jobManagerServiceTxUtil;

    @Autowired
    private MediaProcessorCommService mediaProcessorCommService;

    @Autowired
    private PlatformTransactionManager transactionManager;

    @Autowired
    private TaskManagerService taskManagerService;

    @Autowired
    private FileCatService fileCatService;

    @Autowired
    private ScanDoneConsumer scanDoneConsumer;

    @Autowired
    private BatchMapResultsExecutor batchMapResultsExecutor;

    @Autowired
    private BatchIngestResultsExecutor batchIngestResultsExecutor;

    @Autowired
    private BatchAnalyzeResultsExecutor batchAnalyzeResultsExecutor;

    @Autowired
    private SolrTaskService solrTaskService;

    @Autowired
    private MessageHandler messageHandler;

    @Autowired
    private BlockingExecutor scanBlockingExecutor;

    @Autowired
    private FileCrawlerExecDetailsServiceTxUtil fileCrawlerExecDetailsServiceTxUtil;

    private final Set<Long> rootFolderAlreadyIngestedFinalizeIds = new HashSet<>();

    @Value("${job-manager.pause-job-on-error:false}")
    private boolean pauseJobOnError;

    @Value("${job-manager.pause-scan-job-on-error:false}")
    private boolean pauseScanJobOnError;

    @Value("${job-manager.tasks.max-retries:3}")
    private int maxTaskRetries;

    @Value("${job-manager.tasks-table-maintenance.active:true}")
    private boolean tasksTableMaintenanceActive;

    @Value("${jobmanager.task.updatePageSize:10000}")
    private int taskUpdatePageSize;

    @Value("${job-manager.rate-calc-interval-sec:20}")
    private long rateCalcIntervalSec;

    @Value("${job-manager.rate-intervals-keep:100}")
    private int rateIntervalsToKeep;

    @Value("${job-manager.rate-interval-cache-update:60000}")
    private int updateJobCacheIntervalAllowed;

    @Value("${job-manager.create-task-batch-size:10000}")
    private int createTaskBatchSize;

    public static final String DONE = "Done";

    private LoadingCache<JobCacheKey, List<Long>> jobIdCache;
    private PersistableSafeCache<Long, SimpleJobDto> jobCache;
    
    // table of scan task progress counters: jobId -> taskId -> counter
    // the counter represents number of files mapped in MPs, but not 
    // sent to the AppServer because they haven't changed
    private Table<Long, Long, Long> scanJobTaskProgress;
    private final Object scanJobTaskProgressLock = new Object();

    private long lastCacheRefreshTime = 0L;

    private final static TimeSource timeSource = new TimeSource();

    @Autowired
    private IngestBatchLoader ingestBatchLoader;

    private boolean isSchedulingActive = false;

    public boolean isSchedulingActive() {
        return isSchedulingActive;
    }

    public void setSchedulingActive(boolean schedulingActive) {
        isSchedulingActive = schedulingActive;
    }

    @SuppressWarnings("unused")
    @PostConstruct
    private void init() {
        jobCache = PersistableSafeCache.Builder
                .create(Long.class, SimpleJobDto.class)
                .maximumSize(1000)
                .setLoader(new JobCacheLoader())
                .setPersister((key, value) -> updateJob(value))
                .build();

        //noinspection NullableProblems
        jobIdCache = CacheBuilder.newBuilder()
                .maximumSize(1000)
                .build(new CacheLoader<JobCacheKey, List<Long>>() {
                    @Override
                    public List<Long> load(JobCacheKey key) {
                        List<Long> jobId = getJobId(key.getRunContext(), key.getJobType());
                        return jobId == null ? new ArrayList<>() : jobId;
                    }
                });
        getRunningJobsForCache();

        rootFolderAlreadyIngestedFinalizeIds.addAll(simpleJobRepository.alreadyIngestedFinalizedRootFolders());
        logger.debug("Already ingested finalized root folders: {} ", rootFolderAlreadyIngestedFinalizeIds);
        scanJobTaskProgress = HashBasedTable.create();
    }

    private synchronized void getRunningJobsForCache() {

        if (lastCacheRefreshTime > System.currentTimeMillis() - updateJobCacheIntervalAllowed) return;

        DBTemplateUtils.doInTransaction(() -> {
            List<CrawlRun> runningRuns = crawlRunRepository.findAllByStatus(RunStatus.RUNNING);
            if (runningRuns.isEmpty()) return;
            List<Long> runIds = runningRuns.stream().map(CrawlRun::getId).collect(Collectors.toList());
            List<SimpleJob> runningJobs = simpleJobRepository.findJobsByContextList(runIds);
            runningJobs.forEach(j -> addJobToCache(j.getRunContext(), j.getType(), j.getId()));
        });

        logger.debug("cachedJobsMap={}", cachedJobsMapToString());

        lastCacheRefreshTime = System.currentTimeMillis();
    }

    private synchronized void addJobToCache(Long runId, JobType type, Long jobId) {
        JobCacheKey key = new JobCacheKey(runId, type);
        List<Long> jobs = jobIdCache.getUnchecked(key);
        jobs.add(jobId);
        jobIdCache.put(key, jobs);
    }

    public void scanTaskEnded(Long jobId, Long taskId) {
        synchronized (scanJobTaskProgressLock) {
            Long value = scanJobTaskProgress.remove(jobId, taskId);
            if (value != null && value > 0) {
                incJobCounters(jobId, value, 0);
            }
        }
    }

    public void scanTasksEnded(Long jobId) {
        scanTasksEnded(jobId, false);
    }

    private void scanTasksEnded(Long jobId, boolean isPaused) {
        synchronized (scanJobTaskProgressLock) {
            Map<Long, Long> tasksData = scanJobTaskProgress.row(jobId);
            tasksData.forEach((taskId, counter) -> {
                Long value = scanJobTaskProgress.remove(jobId, taskId);
                if (!isPaused && value != null && value > 0) {
                    incJobCounters(jobId, value, 0);
                }
            });
        }
    }

    public Long getExtraScanTaskCountForJob(Long jobId) {
        Map<Long, Long> values = scanJobTaskProgress.row(jobId);
        return values == null ? 0L : values.values().stream().reduce(0L, Long::sum);
    }

    public void updateScanTaskJobProgress(Long jobId, Long taskId, Long items) {
        synchronized (scanJobTaskProgressLock) {
            scanJobTaskProgress.put(jobId, taskId, items);
        }
    }

    @Scheduled(initialDelayString = "${job-manager.scan-jobs-chk-initial-delay-millis:5000}",
            fixedRateString = "${job-manager.scan-jobs-chk-frequency-millis:15000}")
    @ConditionalOnProperty(value = "is.toolExecutionMode", havingValue = "false", matchIfMissing = true)
    public void scanJobCheckLimit() {
        if (!isSchedulingActive) {
            return;
        }

        Collection<Long> jobIds = new ArrayList<>();

        for (List<Long> jobs : jobIdCache.asMap().values()) {
            jobIds.addAll(jobs);
        }

        jobIds.forEach(id -> {
                    if (id > 0) {
                        SimpleJobDto job = jobCache.getForRead(id);
                        if (job != null && job.getType() == JobType.SCAN && job.getCurrentTaskCount() > 0 && job.getState().isActive()) {
                            RootFolder rf = docStoreService.getRootFolder(job.getItemId());
                            final int rootFolderScanLimit = rf.getScannedFilesCountCap();
                            if (rootFolderScanLimit > 0 && job.getCurrentTaskCount() >= rootFolderScanLimit) {
                                mediaProcessorCommService.sendLimitReachedMessage(id);
                                logger.debug("send message scan limit reached for job {}", id);
                            }
                            if (rf.getScannedMeaningfulFilesCountCap() > 0 && job.getScannedMeaningfulFilesCount() >= rf.getScannedMeaningfulFilesCountCap()) {
                                mediaProcessorCommService.sendLimitReachedMessage(id);
                                logger.debug("send message meaningful scan limit reached for job {}", id);
                            }
                        }
                    }
                }
        );
    }

    public SimpleJob saveJob(SimpleJob job) {
        simpleTaskService.createPartitionIfNeeded(job.getTasksPartition());
        TransactionTemplate tmpl = new TransactionTemplate(transactionManager);
        tmpl.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRES_NEW);
        SimpleJob res = tmpl.execute(status -> simpleJobRepository.save(job));
        if (res != null) {
            addJobToCache(res.getRunContext(), res.getType(), res.getId());
        }
        return res;
    }

    public void fixPartitionIfNeeded(SimpleJob job) {
        simpleTaskService.createPartitionIfNeeded(job.getTasksPartition());
    }

    public void updateTasksStatusInBatch(Long runId, JobType type, List<SimpleTask> tasks, TaskState newState) {
        if (logger.isTraceEnabled()) {
            List<String> taskIds = tasks.stream().map(Task::getId).map(Object::toString).collect(toList());
            logger.trace("Update status to {} for tasks with ids: {}", newState, String.join(", ", taskIds));
        }

        if (type.equals(JobType.INGEST)) {
            updateIngestTasksStatus(tasks.stream().map(Task::getId).collect(toList()), newState);
        } else if (type.equals(JobType.ANALYZE)) {
            updateAnalysisTasksStatus(tasks.stream().map(Task::getId).collect(toList()), newState);
        } else {
            tasks.forEach(t -> {
                t.setState(newState);
                t.setStateUpdateTime();
            });
            DBTemplateUtils.persistEntitiesBatchInTx(tasks);
        }
    }

    @Transactional(readOnly = true)
    public Map<JobState, Long> getTasksPartitionJobState(long partitionId) {
        List<Object[]> jobStatesWithCount = simpleTaskRepository.getTasksPartitionJobState(partitionId);
        try {
            return jobStatesWithCount.stream()
                    .collect(Collectors.toMap(entry -> (JobState.valueOf((String) entry[0])), entry -> ((BigInteger) entry[1]).longValue()));
        } catch (Exception e) {
            throw new RuntimeException("jobStatesWithCount=" +
                    jobStatesWithCount.stream().map(r ->
                            Arrays.stream(r).map(o -> o.getClass() + ":" + o.toString())
                                    .collect(Collectors.joining(",", "[", "]")))
                            .collect(Collectors.joining(",", "{", "}")),
                    e);
        }
    }

    public List<Long> getTaskTablePartitionIds() {
        return simpleTaskService.getTaskTablePartitionIds();
    }

    /**
     * Get table of context IDs, task states and numbers of tasks
     * but only for jobs that are <b>IN_PROGRESS</b> <br/>
     * <br/>
     * [runContext, state, count(*)]<br/>
     * <br/>
     * For example: <br/>
     * <table>
     * <tr>
     * <td>context ID</td><td>task state</td><td>number of tasks</td>
     * </tr>
     * <tr>
     * <td>1</td><td>NEW</td><td>10</td>
     * </tr>
     * <tr>
     * <td>1</td><td>ENQUEUED</td><td>5</td>
     * </tr>
     * </table>
     * etc...
     *
     * @param taskType type of the task, e.g. INGEST_FILE
     * @return table
     */
    @Transactional(readOnly = true)
    public Table<Long, TaskState, Number> getTaskStatesByType(TaskType taskType) {
        List<Object[]> taskStatesByType;
        Table<Long, TaskState, Number> result = HashBasedTable.create();

        List<Long> jobIds = getRunningJobs();
        if (jobIds == null || jobIds.isEmpty()) {
            logger.trace("no running jobs found, return");
            return result;
        }

        if (taskType.equals(TaskType.INGEST_FILE)) {
            taskStatesByType = simpleTaskRepository.getJobTaskStatesByTypePerRun(jobIds,
                    taskType.toInt(), Arrays.asList(JobState.IN_PROGRESS.name(), JobState.PENDING.name()));
        } else {
            taskStatesByType = simpleTaskRepository.getJobTaskStatesByTypePerRun(jobIds,
                    taskType.toInt(), Collections.singletonList(JobState.IN_PROGRESS.name()));
        }

        taskStatesByType.forEach(line ->
                result.put(((Number) line[0]).longValue(), TaskState.of((int) line[1]), (Number) line[2]));
        return result;
    }

    @Transactional(readOnly = true)
    public Table<Long, TaskState, Number> getTaskStatesByTypePerJob(TaskType taskType) {
        List<Long> jobIds = simpleJobRepository.findNoneDeletedJobIdsByState(JobState.getStartedStates());
        Table<Long, TaskState, Number> result = HashBasedTable.create();
        if (jobIds != null && !jobIds.isEmpty()) {
            List<Object[]> taskStatesByType = simpleTaskRepository.getTaskStatesByTypePerJob(jobIds,
                    taskType.toInt(), JobState.IN_PROGRESS.name());
            taskStatesByType.forEach(line ->
                    result.put(((Number) line[0]).longValue(), TaskState.of((int) line[1]), (Number) line[2]));
        }
        return result;
    }

    @Transactional(readOnly = true)
    public Map<Long, Number> getTaskStatesByTypePerJob(TaskType taskType, List<Long> jobIds) {
        Map<Long, Number> result = new HashMap<>();
        if (jobIds != null && !jobIds.isEmpty()) {
            List<Object[]> taskByJob = simpleTaskRepository.getTaskByTypePerJob(jobIds, taskType.toInt());
            taskByJob.forEach(line ->
                    result.put(((Number) line[0]).longValue(), (Number) line[1]));
        }
        return result;
    }

    public Map<Long, Long> getTaskCountPerRun(TaskType taskType, JobState jobState, TaskState taskState) {
        List<Long> jobIds = getRunningJobs();
        Map<Long, Long> result = new HashMap<>();
        if (jobIds != null && !jobIds.isEmpty()) {
            List<Object[]> taskCountPerRun = simpleTaskRepository.getTaskCountPerRunByTypeAndState(jobIds,
                    taskType.toInt(), taskState.ordinal(), jobState.name());
            for (Object[] runIdAndTaskCount : taskCountPerRun) {
                result.put(((Number) runIdAndTaskCount[0]).longValue(), ((Number) runIdAndTaskCount[1]).longValue());
            }
        }
        return result;
    }

    @Transactional(readOnly = true)
    public Map<TaskState, Number> getTaskStatesByJobId(long jobId, long partitionId) {
        List<Object[]> taskStatesByType = partitionId == 0 ?
                simpleTaskRepository.getTaskStatesByJobId(jobId) :
                simpleTaskRepository.getTaskStatesByJobId(jobId, partitionId);

        Map<TaskState, Number> result = Maps.newHashMap();
        taskStatesByType.forEach(line ->
                result.put((TaskState) line[0], (Number) line[1]));
        return result;
    }

    @Transactional(readOnly = true)
    public Map<Long, Integer> getTaskCountPerDataCenterId(Set<Long> dataCenterIds, TaskType taskType, TaskState... taskStates) {
        Objects.requireNonNull(dataCenterIds);
        if (dataCenterIds.size() == 0) {
            throw new IllegalArgumentException("Zero length dataCenterIds");
        }
        validateTaskRootFolderJoin(taskType);

        List<Integer> taskStateOrdinals = Arrays.stream(taskStates).map(Enum::ordinal).collect(toList());

        List<Long> jobIds = getRunningJobs();
        if (jobIds == null || jobIds.isEmpty()) {
            return new HashMap<>();
        }

        List<Object[]> resultSet =
                simpleTaskRepository.getTaskCountGroupByDataCenterId(jobIds, dataCenterIds, taskType.toInt(), taskStateOrdinals);
        Map<Long, Integer> result = new HashMap<>(resultSet.size());
        for (Object[] line : resultSet) {
            logger.debug("line:");
            for (int i = 0; i < line.length; i++) {
                logger.debug("    line[{}] = {}", i, line[i]);
            }
            result.put(((Number) line[0]).longValue(), ((Number) line[1]).intValue());
        }
        return result;
    }

    private void validateTaskRootFolderJoin(TaskType taskType) {
        if (taskType != TaskType.SCAN_ROOT_FOLDER && taskType != TaskType.INGEST_FILE) {
            throw new IllegalArgumentException("Getting tasks  per data center is only relevant for " +
                    TaskType.SCAN_ROOT_FOLDER + " tasks and " + TaskType.INGEST_FILE + " tasks. Got: " + taskType);
        }
    }

    public List<Long> getRunsForRunningJobsByType(JobType type) {
        List<Long> result = new ArrayList<>();

        Collection<Long> jobIds = new ArrayList<>();

        for (List<Long> jobs : jobIdCache.asMap().values()) {
            jobIds.addAll(jobs);
        }

        jobIds.forEach(id -> {
            SimpleJobDto job = jobCache.getForRead(id);
            if (job.getType().equals(type) && job.getState().isStarted()) {
                result.add(job.getRunContext());
            }
        });

        return result;
    }

    private List<Long> getRunningJobs() {
        List<Long> result = new ArrayList<>();

        Collection<Long> jobIds = new ArrayList<>();

        for (List<Long> jobs : jobIdCache.asMap().values()) {
            jobIds.addAll(jobs);
        }

        jobIds.forEach(id -> {
            if (id > 0) {
                SimpleJobDto job = jobCache.getForRead(id);
                if (job.getState().isStarted()) {
                    result.add(id);
                }
            }
        });
        if (result.isEmpty()) {
            getRunningJobsForCache();
            result.add(0L);
        }
        return result;
    }

    /**
     * Get tasks that belong to one of the specified jobs and that changed status before the specified timestamp
     *
     * @param olderThan timestamp
     * @param limit     limit how many tasks to get
     * @param states    which task states are relevant to this query
     * @return list of tasks
     */
    @Transactional(readOnly = true)
    public List<SimpleTask> getIdleTasks(long olderThan, int limit, List<TaskType> types, List<TaskState> states) {
        List<Integer> taskTypes = types.stream().map(TaskType::toInt).collect(toList());
        List<Long> jobIds = getRunningJobs();
        if (jobIds == null || jobIds.isEmpty()) {
            return new ArrayList<>();
        }
        return simpleTaskRepository.getIdleTasks(jobIds, taskTypes, states, olderThan,
                PageRequest.of(0, limit));
    }

    @Transactional(readOnly = true)
    public List<SimpleTask> getIdleTasks(long olderThan, int limit, List<TaskType> types, List<TaskState> states, List<Long> runIds) {
        List<Integer> taskTypes = types.stream().map(TaskType::toInt).collect(toList());
        return simpleTaskRepository.getIdleTasks(taskTypes, states, olderThan, runIds,
                PageRequest.of(0, limit));
    }

    public List<SimpleTask> getIdleIngestTasks(long olderThan, int limit, List<Long> runIds) {
        List<SolrFileEntity> files = solrTaskService.getIdleIngestTasks(olderThan, limit, runIds);
        List<SimpleTask> result = new ArrayList<>();
        if (files != null && !files.isEmpty()) {
            files.forEach(file -> {
                SimpleTask task = SolrTaskService.createForIngest(file, getJobIdCached(file.getCurrentRunId(), JobType.INGEST));
                result.add(task);
            });
        }
        return result;
    }

    public List<SimpleTask> getIdleAnalyzeTasks(long olderThan, int limit, List<Long> runIds) {
     List<SolrContentMetadataEntity> contents = solrTaskService.getIdleAnalysisTasks(olderThan, limit, runIds);
        List<SimpleTask> result = new ArrayList<>();
        if (contents != null && !contents.isEmpty()) {
            contents.forEach(content -> {
                SimpleTask task = SolrTaskService.createForAnalysis(content, getJobIdCached(content.getCurrentRunId(), JobType.ANALYZE));
                result.add(task);
            });
        }
        return result;
    }

    /**
     * Get task IDs of tasks that changed status before the specified timestamp
     *
     * @param olderThan timestamp
     * @param states    which task states are relevant to this query
     * @return list of task IDs
     */
    @Transactional(readOnly = true)
    public List<Long> getIdleTaskIds(long olderThan, TaskState... states) {
        List<Long> jobIds = getRunningJobs();
        if (jobIds == null || jobIds.isEmpty()) {
            return new ArrayList<>();
        }
        return simpleTaskRepository.getIdleTaskIds(jobIds, Lists.newArrayList(states), olderThan);
    }

    @Transactional(readOnly = true)
    public List<SimpleJob> getIdleJobs(long olderThan, JobState... states) {
        return simpleJobRepository.getIdleJobs(Lists.newArrayList(states), olderThan);
    }

    @Transactional(readOnly = true)
    public List<SimpleJobDto> getIdleJobsDtos(long olderThan, JobState... states) {
        List<SimpleJob> idleJobs = simpleJobRepository.getIdleJobs(Lists.newArrayList(states), olderThan);
        return simpleJobsToDto(idleJobs);
    }

    @Transactional(readOnly = true)
    public List<SimpleJob> getIdleJobs(long olderThan, Collection<JobState> states) {
        return simpleJobRepository.getIdleJobs(states, olderThan);
    }

    @Transactional(readOnly = true)
    public List<SimpleTask> getTasksByContext(TaskType taskType, TaskState state, long contextId, int limit) {
        return simpleTaskRepository.getTasksByContext(taskType.toInt(), state.ordinal(), contextId, PageRequest.of(0, limit));
    }

    @Transactional(readOnly = true)
    public List<Long> getTaskIdsByJobsAndStates(Collection<Long> jobIds, TaskState... taskStates) {
        return simpleTaskRepository.getTaskIdsByJobsAndStates(jobIds, Arrays.asList(taskStates));
    }

    @Transactional(readOnly = true)
    public List<SimpleTask> getTaskByJobIdAndTaskTypeAndStates(long jobId, TaskType taskType, TaskState... taskStates) {
        return simpleTaskRepository.getTaskByJobIdAndTaskTypeAndStates(jobId, taskType.toInt(), Arrays.asList(taskStates));
    }

    @Transactional(readOnly = true)
    public int getNotDoneTasksForJobByType(long jobId, TaskType taskType) {
        return simpleTaskRepository.getNotDoneTasksForJobByType(jobId, taskType.toInt());
    }

    @Transactional(readOnly = true)
    public SimpleTask getTaskById(Long taskId) {
        return simpleTaskRepository.findById(taskId).orElse(null);
    }

    @Transactional(readOnly = true)
    public boolean shouldJobBePending(long jobId, long partitionId, JobType type) {
        if (type.equals(JobType.SCAN)) {
            return simpleTaskRepository.checkScanJobShouldBePending(jobId, partitionId) > 0;
        } else if (type.equals(JobType.INGEST)) {
            return false;   // for now there is no reliable indication for a pending ingest
//            return simpleTaskRepository.checkIngestJobShouldBePending(jobId, partitionId) > 0;
        } else {
            return false;
        }
    }

    @Transactional(readOnly = true)
    public boolean shouldFailJob(long jobId, long partitionId) {
        return simpleTaskRepository.checkScanJobShouldBeFailed(jobId, partitionId) > 0;
    }

    public List<SimpleTask> getTasksByTypeAndState(TaskType taskType, TaskState taskState, JobState... jobStates) {
        if (jobStates.length == 0) {
            throw new IllegalArgumentException("job states cannot be empty");
        }

        List<Long> jobIds = getRunningJobs();
        if (jobIds == null || jobIds.isEmpty()) {
            return new ArrayList<>();
        }

        List<SimpleTask> tasksByTypeAndState = simpleTaskRepository.getTasksByTypeAndState(jobIds,
                Lists.newArrayList(jobStates), taskType.toInt(), taskState);
        if (logger.isTraceEnabled()) {
            logger.trace("tasksByTypeAndState: {}", Arrays.toString(tasksByTypeAndState.toArray()));
        }
        return tasksByTypeAndState;
    }


    @Transactional(readOnly = true)
    public long getJobTasksCount(Long jobId) {
        return simpleTaskRepository.countNonDeletedTasksByJob(jobId);
    }

    public long currentPartitionId() {
        long baseline = LocalDate.of(2017, 1, 1).toEpochDay();
        long gap = timeSource.millisSince(baseline);
        return TimeUnit.MILLISECONDS.toDays(gap) / DAYS_PER_PARTITION;
    }

    @Transactional
    public SimpleJobDto createRootFolderJob(long rootFolderId, long runId, JobType jobType) {
        SimpleJob simpleJob = new SimpleJob(currentPartitionId(), runId);
        simpleJob.setType(jobType);
        simpleJob.setItemId(rootFolderId);
        simpleJob = saveJob(simpleJob);
        logger.debug("Created RootFolder {} Job {} on run {} (jobId={})", rootFolderId, jobType, runId, simpleJob.getId());
        return simpleJobToDto(simpleJob);
    }

    @Transactional
    public SimpleJob createGenericJob(long runId, JobType jobType, String jobName, JobState jobState, Long itemId) {
        logger.debug("Create Generic Job {} on run {} with name {} and item ID", jobType, runId, jobName, itemId);
        SimpleJob simpleJob = new SimpleJob(currentPartitionId(), runId);
        simpleJob.setType(jobType);
        simpleJob.setState(jobState == null ? JobState.NEW : jobState);
        simpleJob.setName(jobName);
        if (itemId != null) {
            simpleJob.setItemId(itemId);
        }
        simpleJob = saveJob(simpleJob);
        return simpleJob;
    }

    public void updateJobState(long jobId, JobState jobState, JobCompletionStatus jobCompletionStatus, String stateOpMessage, boolean allowPrevStateInactive) {
        updateJobState(jobId, jobState, jobCompletionStatus, stateOpMessage, false, null, allowPrevStateInactive);
    }

    /**
     * Updates job state immediately and invalidates the jobs cache
     *
     * @param jobId    job ID
     * @param jobState new job state
     */
    private void updateJobState(long jobId, JobState jobState, JobCompletionStatus jobCompletionStatus,
                                             String stateOpMessage, boolean makeNewJobResumable, Boolean acceptingTasks, boolean allowPrevStateInactive) {
        logger.debug("Updating state for job #{} to {}. CompletionStatus={}. msg={} ...", jobId, jobState, jobCompletionStatus, stateOpMessage);
        SimpleJobDto targetJobDto;
        try {
            SimpleJobDto sourceJobDto = jobCache.lockForUpdate(jobId);
            // transaction starts inside the following method
            targetJobDto = jobManagerServiceTxUtil.updateJobStateInTx(jobId, sourceJobDto, jobState, jobCompletionStatus, stateOpMessage, makeNewJobResumable, acceptingTasks, allowPrevStateInactive);
            // at this point transaction is committed
            if (targetJobDto != null) {
                jobCache.replaceByKey(jobId, targetJobDto);
            } else {
                logger.trace("result of job {} state change is null, do not update cache", jobId);
            }
        } finally {
            jobCache.unlockUpdate(jobId);
        }
    }

    public void updateJobTaskStateIncrementRetryForEver(Long runId, Long jobId, JobType type, Collection<Long> taskIds, Long partitionId) {
        if (taskIds == null || taskIds.size() <= 0) {
            return;
        }
        long currTimeMilli = timeSource.currentTimeMillis();
        if (type.equals(JobType.INGEST)) {
            solrTaskService.taskRetryIncFiles(taskIds);
        } else if (type.equals(JobType.ANALYZE)) {
            solrTaskService.taskRetryIncContents(taskIds);
        } else {
            taskManagerService.updateTaskStateIncrementRetry(taskIds, TaskState.NEW, currTimeMilli, partitionId);
        }
        incJobRetryCount(jobId, taskIds.size());
    }

    public void setEnqTasksToNewForJob(Long jobId) {
        SimpleJobDto job = jobCache.getForRead(jobId);
        if (job != null && job.getState().isStarted()) {
            if (job.getType().equals(JobType.INGEST)) {
                scanBlockingExecutor.execute(() -> solrTaskService.updateFilesForIngest(job.getRunContext()));
            } else if (job.getType().equals(JobType.ANALYZE)) {
                scanBlockingExecutor.execute(() -> solrTaskService.updateContentsForAnalysis(job.getRunContext()));
            } else {
                taskManagerService.updateJobTaskState(jobId, TaskState.NEW, TaskState.ENQUEUED, timeSource.currentTimeMillis());
            }
        } else if (job != null) {
            logger.info("job {} in state {} will not change its tasks status", jobId, job.getState().name());
        } else {
            logger.warn("cannot find job with id {}", jobId);
        }
    }

    public void updateTasksStatus(Collection<Long> taskIds, TaskState taskState) {
        // No @Transactional annotation here - transaction is on a single partition bases
        taskManagerService.updateTaskState(taskIds, taskState);
    }

    public void updateIngestTasksStatus(Collection<Long> taskIds, TaskState taskState) {
        if (TaskState.ACTIVE_STATES.contains(taskState)) {
            solrTaskService.updateTaskStateByFileId(taskState, taskIds);
        } else {
            solrTaskService.endTaskForFiles(taskIds);
        }
    }

    public void updateAnalysisTasksStatus(Collection<Long> taskIds, TaskState taskState) {
        if (TaskState.ACTIVE_STATES.contains(taskState)) {
            solrTaskService.updateTaskStateByContentId(taskState, taskIds);
        } else {
            solrTaskService.endTaskForContents(taskIds);
        }
    }

    public void removeTaskMarkingsForRun(Long runId) {
        scanBlockingExecutor.execute(() -> {
            solrTaskService.endTaskForIngest(runId);
            solrTaskService.endTaskForAnalysis(runId);
        });
    }

    public void updateJobAndTaskStates(JobStateUpdateInfo info, long taskId, TaskState taskState, boolean allowPrevStateInactive) {
        updateJobState(info.getId(), info.getState(), info.getCompletionStatus(), info.getStateOpMessage(), allowPrevStateInactive);
        updateTaskStatus(taskId, taskState);
    }

    public void updateTaskStatus(Long taskId, TaskState taskState) {
        updateTaskStatus(taskId, taskState, null);
    }

    public void updateTaskStatus(Long taskId, TaskState taskState, String json) {
        taskManagerService.updateTaskStatus(taskId, taskState, json);
    }

    @Transactional
    public void pauseJob(Long jobId, Long runId, String opMsg, boolean makeNewJobResumable, JobStateUpdateInfo... prevStatesToUpdate) {
        if (prevStatesToUpdate != null) {
            updateJobStates(Arrays.stream(prevStatesToUpdate).collect(toList()));
        }

        if (makeNewJobResumable) { //In case job was born as paused, it will not be able to resume it if prev state was not running
            updateJobState(jobId, JobState.IN_PROGRESS, null, opMsg, true, null, true);
        }
        updateJobState(jobId, JobState.PAUSED, null, opMsg, true);
        scanBlockingExecutor.execute(() -> setRunningTasksToNewForJob(jobId, runId));
        scanTasksEnded(jobId, true);
    }

    @Transactional
    public void setRunningTasksToNewForJob(Long jobId, Long runId) {
        JobType type = getCachedJobType(jobId);
        if (type == null) {
            type = getJobById(jobId).getType();
        }

        List<Long> taskIds;
        if (type.equals(JobType.INGEST)) {
            taskIds = solrTaskService.getIngestTasksIds(runId, TaskState.ENQUEUED);
            if (taskIds.size() > 0) {
                solrTaskService.updateTaskStateByFileId(TaskState.NEW, taskIds);
            }
        } else if (type.equals(JobType.ANALYZE)) {
            taskIds = solrTaskService.getAnalysisTasksIds(runId, TaskState.ENQUEUED);
            if (taskIds.size() > 0) {
                solrTaskService.updateTaskStateByContentId(TaskState.NEW, taskIds);
            }
        } else {
            taskIds = getTaskIdsByJobsAndStates(Lists.newArrayList(jobId), TaskState.ENQUEUED, TaskState.CONSUMING);
            if (taskIds.size() > 0) {
                updateTasksStatus(taskIds, TaskState.NEW);
            }
        }

        if (taskIds.size() > 0) {
            switch (type) {
                case SCAN:
                    scanDoneConsumer.pausedTasks(taskIds);
                    batchMapResultsExecutor.clearTaskData(taskIds);
                    break;
                case INGEST:
                    batchIngestResultsExecutor.clearTaskData(taskIds);
                    break;
                case ANALYZE:
                    batchAnalyzeResultsExecutor.clearTaskData(taskIds);
                    break;
                default:

            }
        }
    }

    public void resumeJob(SimpleJobDto job, String opMsg) {
        updateJobState(job.getId(), job.getResumeState(), null, opMsg, true);
        addJobToCache(job.getRunContext(),job.getType(),job.getId());
    }

    public void handleFinishJob(long jobId, String stateOpMessage) {
        updateJobState(jobId, JobState.DONE, null, stateOpMessage, true);
    }

    public void handleFailedFinishJob(long jobId, String stateOpMessage) {
        boolean shouldPauseInstead = false;

        if (pauseJobOnError) { // should pause failed job instead of stop
            if (!pauseScanJobOnError) { // should not pause map jobs
                SimpleJobDto job = jobCache.getForRead(jobId);
                if (job != null && !JobType.SCAN.equals(job.getType())) { // pause only if not map
                    shouldPauseInstead = true;
                }
            } else {
                shouldPauseInstead = true;
            }
        }

        if (shouldPauseInstead) {
            updateJobState(jobId, JobState.PAUSED, JobCompletionStatus.ERROR, stateOpMessage, true);
        } else {
            updateJobState(jobId, JobState.DONE, JobCompletionStatus.ERROR, stateOpMessage, true);
        }
    }

    public CrawlRun getCrawlRun(Long runId) {
        Callable<CrawlRun> task = () -> crawlRunRepository.findById(runId).orElse(null);
        return DBTemplateUtils.doInTransaction(task);
    }

    public void updateJobStates(List<JobStateUpdateInfo> jobStateUpdateInfos) {
        DBTemplateUtils.doInTransaction(() -> {
            Map<Long, CrawlRun> runsCache = new HashMap<>();

            for (JobStateUpdateInfo info : jobStateUpdateInfos) {
                if (info != null) {
                    SimpleJobDto jobDto = getJobAsDto(info.getId());
                    long runId = jobDto.getRunContext();
                    CrawlRun run = runsCache.computeIfAbsent(runId, this::getCrawlRun);

                    if (run.getRunStatus() == RunStatus.PAUSED) {
                        // update only to-done jobs to their intended state, and update the rest to PAUSED
                        if (info.getState() == JobState.DONE) {
                            logger.debug("Job #{} moving to state DONE (though its run was paused)", info.getId());
                            updateJobState(info.getId(), info.getState(), info.getCompletionStatus(), info.getStateOpMessage(), info.isAllowPrevStateInactive());
                        } else {
                            logger.debug("Job #{} moving to state PAUSED rather than to {} because its run was paused", info.getId(), info.getState());
                            updateJobState(info.getId(), JobState.PAUSED, info.getCompletionStatus(), info.getStateOpMessage(), info.isAllowPrevStateInactive());
                        }
                    } else {
                        updateJobState(info.getId(), info.getState(), info.getCompletionStatus(),
                                info.getStateOpMessage(), false, info.isAcceptingTasks(), info.isAllowPrevStateInactive());
                    }
                }
            }
        });
    }

    @Transactional
    public SimpleTask createTaskIfAbsent(Long itemId, long jobId, TaskType taskType) {
        return createTaskIfAbsent(itemId, jobId, taskType, null);
    }

    /**
     * Creates a {@link TaskState#NEW NEW} task with the given parameters if there isn't any NEW one already.
     * <p>
     * Used to ensure that a job has only one task of the given type in state NEW.
     *
     * @param itemId   the id of the item for the new task, possibly null
     * @param jobId    the id of the job for which to create the new task
     * @param taskType the type for the new task
     * @return the newly created task or the already existing tasks
     */
    @Transactional
    public SimpleTask createTaskIfAbsent(Long itemId, long jobId, TaskType taskType, String json) {
        List<SimpleTask> tasks = simpleTaskRepository.getTaskByJobIdAndTaskTypeAndStates(
                jobId, taskType.toInt(), Collections.singletonList(TaskState.NEW));
        SimpleTask task;
        switch (tasks.size()) {
            case 0:
                task = createTask(itemId, jobId, taskType, json);
                logger.debug("No other tasks. Created new task {}", task);
                break;
            case 1:
                task = tasks.get(0);
                logger.debug("Got one already existing task {}", task);
                break;
            default:
                logger.warn("Scan Job {} has {} scan tasks. Using only the first. Already existing tasks: {}",
                        jobId, tasks.size(), Arrays.toString(tasks.toArray()));
                task = tasks.get(0);
        }
        return task;
    }

    @Transactional
    public SimpleTask createTask(Long itemId, long jobId, TaskType taskType, String jsonData) {
        SimpleJob job = simpleJobRepository.getOne(jobId);
        SimpleTask task = createTaskObject(job, itemId, taskType, jsonData);
        return taskManagerService.createTask(task);
    }

    @Transactional
    public SimpleTask createTask(SimpleJobDto job, TaskType taskType) {
        SimpleTask task = new SimpleTask(taskType, job.getPartitionId());
        task.setJobId(job.getId());
        task.setRunContext(job.getRunContext());
        return taskManagerService.createTask(task);
    }

    @Transactional
    public SimpleTask createTask(Long runContext, JobType jobType, TaskType taskType, Long itemId) {
        Long jobId = getJobIdCached(runContext, jobType);
        if (jobId != null) {
            Long jobPartition = getJobPartition(jobId);
            SimpleTask task = new SimpleTask(taskType, jobPartition);
            task.setJobId(jobId);
            task.setRunContext(runContext);
            task.setItemId(itemId);
            return taskManagerService.createTask(task);
        } else {
            return null;
        }
    }

    public SimpleTask createTaskObject(Long runContext, Long jobId, TaskType taskType, Long itemId, String jsonData) {
        Long jobPartition = getJobPartition(jobId);
        SimpleTask task = new SimpleTask(taskType, jobPartition);
        task.setJobId(jobId);
        task.setRunContext(runContext);
        task.setItemId(itemId);
        task.setJsonData(jsonData);
        return task;
    }

    public SimpleTask createTaskObject(Long runContext, Long jobId, TaskType taskType, Long itemId, String jsonData, String path) {
        SimpleTask task = createTaskObject(runContext, jobId, taskType, itemId, jsonData);
        task.setPath(path);
        return task;
    }

    public SimpleTask createTaskObject(Long runContext, JobType jobType, TaskType taskType, Long itemId) {
        Long jobId = getJobIdCached(runContext, jobType);
        if (jobId != null) {
            Long jobPartition = getJobPartition(jobId);
            SimpleTask task = new SimpleTask(taskType, jobPartition);
            task.setJobId(jobId);
            task.setRunContext(runContext);
            task.setItemId(itemId);
            return task;
        } else {
            return null;
        }
    }

    /**
     * Creates a new transient {@link SimpleTask} object.
     * <p>
     * The term transient means that the new task is not saved to the database
     * and is not associated with any Hibernate {@link Session}.
     *
     * @param job      the {@link SimpleJob} for which to create the new task
     * @param itemId   the item id for the new task, possibly null
     * @param taskType the type of the task to create
     * @param jsonData json data for the new task
     * @return the newly created transient SimpleTask
     */
    private SimpleTask createTaskObject(SimpleJob job, Long itemId, TaskType taskType, String jsonData) {
        SimpleTask simpleTask = new SimpleTask(job, taskType);
        if (itemId != null) {
            simpleTask.setItemId(itemId);
        }
        simpleTask.setJsonData(jsonData);
        return simpleTask;
    }

    @Transactional
    public SimpleTask createTask(Long itemId, long jobId, TaskType taskType, String jsonData, String path) {
        SimpleJob one = simpleJobRepository.getOne(jobId);
        SimpleTask simpleTask = createTaskObject(one, itemId, taskType, jsonData);
        simpleTask.setPath(path);
        return taskManagerService.createTask(simpleTask);
    }

    /**
     * Creates a batch od {@link SimpleTask tasks} for each of the received jsonData strings.
     * <p>
     * Each task is created with one of the jsonData strings.
     * All of the tasks are created with the received job, itemId, and task type.
     *
     * @param job          the job for which to create the tasks
     * @param itemId       the itemId with which to create the tasks
     * @param taskType     the taskType of which to create the tasks
     * @param jsonDataList a list of jsonData , one per task to create
     */
    public void createTasksBatch(SimpleJob job, long itemId, TaskType taskType, List<String> jsonDataList) {
        List<SimpleTask> transientTasksBatch =
                jsonDataList.stream().
                        map(jsonData -> createTaskObject(job, itemId, taskType, jsonData)).
                        collect(toList());
        taskManagerService.createTasksBatch(transientTasksBatch);
    }

    public boolean chkIfScanTaskAlreadyExists(long jobId, String path) {
        return simpleTaskRepository.getTaskByJobAndPathAndType(jobId, path, TaskType.SCAN_ROOT_FOLDER.toInt()) > 0;
    }

    public boolean runHasNewIngestTasks(long runId) {
        if (runId < 1) {
            throw new IllegalArgumentException("runId must be a long greater than or equal to 1. Got: " + runId);
        }

        SimpleTask probe = new SimpleTask(TaskType.INGEST_FILE, 0L);
        probe.setState(TaskState.NEW);
        probe.setRunContext(runId);

        ExampleMatcher runStateAndTypeMatcher = ExampleMatcher.
                matching().
                withIgnorePaths(ReflectionUtils.fieldNames(probe, "type", "state", "runContext"));
        Example<SimpleTask> newIngestTaskOfRun = Example.of(probe, runStateAndTypeMatcher);
        return simpleTaskRepository.exists(newIngestTaskOfRun);
    }

    /**
     * Safely returns null if not present in the DB, doesn't throw exception
     *
     * @param runContext run ID
     * @param jobType    job type
     * @return job ID in the DB
     */
    @Transactional(readOnly = true)
    public Long getJobIdCached(long runContext, JobType jobType) {
        List<Long> jobId = jobIdCache.getUnchecked(new JobCacheKey(runContext, jobType));
        return jobId == null || jobId.isEmpty() ? null : jobId.get(0);
    }

    public String cachedJobsMapToString() {
        ConcurrentMap<JobCacheKey, List<Long>> map = jobIdCache.asMap();
        return map.entrySet().stream().map(e -> e.getKey().toString() + ":" + e.getValue())
                .collect(Collectors.joining(",", "{", "}"));
    }

    @Transactional(readOnly = true)
    public List<SimpleJob> getJobs(JobType jobType, JobState jobState) {
        return simpleJobRepository.listJobsByTypeAndState(jobType, jobState);
    }

    @Transactional(readOnly = true)
    public List<SimpleJob> getJobs(Long runContext, JobType jobType, Long itemId) {
        return simpleJobRepository.listJobsByTypeAndItemId(runContext, jobType, itemId);
    }

    @Transactional(readOnly = true)
    public List<Long> getJobIdsByRunAndType(Long runContext, JobType jobType) {
        return simpleJobRepository.findJobId(runContext, jobType);
    }

    @Transactional(readOnly = true)
    public List<SimpleJob> getJobsByIds(Set<Long> ids) {
        return simpleJobRepository.findAllById(ids);
    }

    public boolean isJobActive(long jobId) {
        SimpleJobDto job = jobCache.getForRead(jobId);
        return job.getState().isActive();
    }

    public boolean isJobStarted(long jobId) {
        SimpleJobDto job = jobCache.getForRead(jobId);
        return job.getState().isStarted();
    }

    /**
     * Checks if there's currently an {@link JobState#isActive() active} job, of one of the
     * given {@link JobType JobTypes}.
     *
     * @param jobTypes the JobTypes of which to look for active jobs
     * @return true iff there's such an active job
     */
    @Transactional(readOnly = true)
    public boolean gotActiveJobs(JobType... jobTypes) {
        if (jobTypes == null || jobTypes.length == 0) {
            return false;
        }
        try {
            List<SimpleJob> jobs = simpleJobRepository.listJobsByTypesAndStates(Arrays.asList(jobTypes), JobState.getActiveStates());
            return !jobs.isEmpty();
        } catch (Exception e) {
            logger.error("Error listJobsByTypesAndStates({},{}). {}",
                    Arrays.stream(jobTypes).map(Enum::toString).collect(Collectors.joining(",")),
                    JobState.getActiveStates().stream().map(Enum::toString).collect(Collectors.joining(",")),
                    e.getMessage());
            return false;
        }
    }

    @Transactional(readOnly = true)
    public List<SimpleJob> gotJobs(List<JobType> jobTypes, List<JobState> jobStates) {
        return simpleJobRepository.listJobsByTypesAndStates(jobTypes, jobStates);
    }

    public Long getJobPartition(long jobId) {
        SimpleJobDto job = jobCache.getForRead(jobId);
        return (job == null) ? null : job.getPartitionId();
    }


    public void updateInitiationDetails(Long jobId, String initiationDetails) {
        try {
            jobCache.lockForUpdate(jobId);
            DBTemplateUtils.doInTransaction(
                    () -> {
                        SimpleJob job = simpleJobRepository.getOne(jobId);
                        job.setInitiationDetails(initiationDetails);
                        simpleJobRepository.save(job);
                    }
            , TransactionDefinition.PROPAGATION_REQUIRES_NEW);
        } finally {
            jobCache.unlockUpdate(jobId);
        }
    }

    public void updateEstimatedTaskCount(Long jobId, Number estimatedTaskCount) {
        if (jobId != null) {
            try {
                SimpleJobDto job = jobCache.lockForUpdate(jobId);
                job.setEstimatedTaskCount(estimatedTaskCount.intValue());
            } finally {
                jobCache.unlockUpdate(jobId);
            }
        } else {
            opStateService.setCurrentPhaseProgressFinishMark(estimatedTaskCount.intValue());
        }
    }

    @Transactional(readOnly = true)
    public List<SimpleJob> getRunJobsObjects(Long runId) {
        return simpleJobRepository.findRunJobs(runId);
    }

    @Transactional(readOnly = true)
    public List<SimpleJobDto> getRunJobs(Long runId) {
        List<SimpleJob> runJobs = simpleJobRepository.findRunJobs(runId);
        return simpleJobsToDto(runJobs);
    }

    @Transactional(readOnly = true)
    public JobType getRunJobActive(Long runId) {
        List<SimpleJob> runJobs = simpleJobRepository.findRunJobs(runId);
        if (runJobs == null || runJobs.isEmpty()){
            return null;
        }
        return runJobs.stream()
                .filter(Objects::nonNull)
                .filter(j -> j.getState().isStarted())
                .min(Comparator.comparingInt(o -> o.getType().ordinal()))
                .map(Job::getType).orElse(null);
    }

    @Transactional
    public void updateJobCounters(Long runId, JobType jobType, Map<TaskState, Number> taskStatesByType) {
        if (runId != null) {
            int totalTasksCounter = taskStatesByType.entrySet().stream().mapToInt(s -> s.getValue().intValue()).sum();
            updateJobCounters(runId, jobType,
                    Converters.numberIntValue(taskStatesByType.get(TaskState.DONE)),
                    totalTasksCounter,
                    Converters.numberIntValue(taskStatesByType.get(TaskState.FAILED)));
        }
    }

    @Transactional
    public void updateJobCounters(Long jobId, Map<TaskState, Number> taskStatesByType) {
        if (jobId != null) {
            int totalTasksCounter = taskStatesByType.entrySet().stream().mapToInt(s -> s.getValue().intValue()).sum();
            updateJobCounters(jobId,
                    Converters.numberIntValue(taskStatesByType.get(TaskState.DONE)),
                    totalTasksCounter,
                    Converters.numberIntValue(taskStatesByType.get(TaskState.FAILED)), 0);
        }
    }

    @Transactional
    public void updateJobCounters(Long runId, JobType jobType, int taskCounter, int totalTasksCounter, int failedCounter) {
        if (runId != null) {
            Long jobId = getJobIdCached(runId, jobType);
            updateJobCounters(jobId, taskCounter, totalTasksCounter, failedCounter, 0);
        }
    }

    @Transactional
    public void updateJobCounters(Long jobId, int taskCounter, int totalTasksCounter, int failedCounter, Integer meaningfulFilesCount) {
        if (jobId != null) {
            updateEstimatedTaskCount(jobId, totalTasksCounter);
            updateJobFailedCounters(jobId, failedCounter);
            int currentCount = failedCounter + taskCounter;
            updateJobCounters(jobId, currentCount, meaningfulFilesCount);
        }
    }

    @Transactional
    public void updateJobCounters(Long jobId, Number taskCounter, Number totalTasksCounter, Integer meaningfulFilesCount) {
        if (jobId != null) {
            updateEstimatedTaskCount(jobId, totalTasksCounter);
            updateJobCounters(jobId, taskCounter, meaningfulFilesCount);
        }
    }

    @Transactional
    public void updateJobFailedCounters(Long jobId, Number failureCount) {
        if (jobId != null) {
            try {
                SimpleJobDto job = jobCache.lockForUpdate(jobId);
                job.setFailedCount(failureCount.intValue());
            } finally {
                jobCache.unlockUpdate(jobId);
            }
        }
    }

    public void incJobCounters(Long jobId, Number amount, Integer meaningfulFilesCount) {
        if (jobId != null) {
            try {
                SimpleJobDto job = jobCache.lockForUpdate(jobId);
                updateJobCounters(job, job.getCurrentTaskCount() + amount.intValue(),
                        job.getScannedMeaningfulFilesCount() + meaningfulFilesCount);
            } finally {
                jobCache.unlockUpdate(jobId);
            }
        }
    }

    public void incJobFailedCounters(Long jobId) {
        incJobFailedCounters(jobId, 1);
    }

    public void incJobFailedCounters(Long jobId, int counter) {
        if (jobId != null) {
            try {
                SimpleJobDto job = jobCache.lockForUpdate(jobId);
                job.setFailedCount(job.getFailedCount() + counter);
                //   updateJobFailedCounters(job.getId(), job.getFailedCount() + 1);
            } finally {
                jobCache.unlockUpdate(jobId);
            }
        }
    }

    private void incJobRetryCount(Long jobId, int counter) {
        if (jobId != null) {
            try {
                SimpleJobDto job = jobCache.lockForUpdate(jobId);
                job.setTotalTaskRetriesCount(job.getTotalTaskRetriesCount() + counter);
            } finally {
                jobCache.unlockUpdate(jobId);
            }
        }

    }

    public void activateAnalyzeWhileIngestIsRunning(Long runContext) {
        CrawlRun crawlRun = crawlRunRepository.findById(runContext).orElse(null);
        if (crawlRun == null || !crawlRun.getRunStatus().equals(RunStatus.RUNNING)) {
            throw new BadRequestException(messageHandler.getMessage("run.failure.not-running"), BadRequestType.SCAN_NOT_RUNNING);
        }

        if (!crawlRun.isIngestFiles() || !crawlRun.isAnalyzeFiles()){
            throw new BadRequestException(messageHandler.getMessage("run.failure.no-ingest-or-analysis"), BadRequestType.OTHER);
        }

        Long ingestJobId = getJobIdCached(runContext, JobType.INGEST);
        boolean ingestJobStarted = ingestJobId != null && getCachedJobState(ingestJobId).isStarted();
        if (!ingestJobStarted) {
            throw new BadRequestException(messageHandler.getMessage("ingest-job.failure.not-running"), BadRequestType.JOB_NOT_RUNNING);
        }

        Long analyzeJobId = getJobIdCached(runContext, JobType.ANALYZE);
        JobState analyzeJobState = getCachedJobState(analyzeJobId);

        if (analyzeJobState.isStarted()) {
            throw new BadRequestException(messageHandler.getMessage("analyze-job.failure.started"), BadRequestType.JOB_ALREADY_STARTED);
        } else if (!analyzeJobState.isActive()) {
            throw new BadRequestException(messageHandler.getMessage("analyze-job.failure.not-active"), BadRequestType.JOB_NOT_RUNNING);
        }

        if (getNotDoneTasksForJobByType(analyzeJobId, TaskType.ANALYZE_CONTENT) <= 0) {
            throw new BadRequestException(messageHandler.getMessage("analyze-job.failure.no-task"), BadRequestType.NO_PENDING_TASKS);
        }

        if (crawlRun.isAnalyzeFiles()) {
            updateJobState(analyzeJobId, JobState.IN_PROGRESS, null,
                    messageHandler.getMessage("op-msg.start-analyze"), false, true, false);
        }
    }

    /**
     * Updates the job counters in the cache
     * The persistence is achieved later, periodically
     *
     * @param jobId       job ID
     * @param taskCounter tasks counter
     */
    public void updateJobCounters(Long jobId, Number taskCounter, Integer meaningfulFilesCount) {
        if (jobId != null) {
            try {
                SimpleJobDto job = jobCache.lockForUpdate(jobId);
                updateJobCounters(job, taskCounter, meaningfulFilesCount);
            } finally {
                jobCache.unlockUpdate(jobId);
            }
        }
    }

    public void setOpMsg(Long jobId, String stateString) {
        setOpMsg(jobId, stateString, true);
    }

    /**
     * Updates the job message in the cache
     * The persistence is achieved later, periodically
     *
     * @param jobId       job ID
     * @param stateString state string
     * @param printToLog  Print the Operation message to the log file as well?
     */
    public void setOpMsg(Long jobId, String stateString, boolean printToLog) {
        if (stateString == null) {
            return;
        }
        if (printToLog) {
            opStateService.setOpMsg(stateString);
        }
        if (jobId != null) {
            try {
                SimpleJobDto job = jobCache.lockForUpdate(jobId);
                if (stateString.length() > 4096) {
                    stateString = stateString.substring(0, 4096);
                }
                job.setJobStateString(stateString);
            } finally {
                jobCache.unlockUpdate(jobId);
            }
        }
    }

    @Transactional(readOnly = true)
    public SimpleJobDto getJobAsDto(long jobId) {
        SimpleJob job = getJobById(jobId);
        return simpleJobToDto(job);
    }

    /**
     * Periodically persist job statistical data from cache.
     * Not transactional, since the transaction is manual and happens within the persister.
     * This is important, because we need to unlock the locks after the transaction is committed and not before
     */
    @Scheduled(initialDelayString = "${job-manager.persist-jobs-initial-delay-millis:5000}",
            fixedRateString = "${job-manager.persist-jobs-frequency-millis:25000}")
    @ConditionalOnProperty(value = "is.toolExecutionMode", havingValue = "false", matchIfMissing = true)
    public void persistJobCache() {
        logger.trace("persist jobs cache to db");
        jobCache.persistAll();
    }

    @Transactional(readOnly = true)
    public List<JobStateByDate> getRecentlyUpdatedStates(int updatedThresholdSec) {
        // TODO: should advertise upon state change and not periodically. This method should be avoided IR1008
        List<SimpleJob> newerJobs = simpleJobRepository.getNewerJobs(timeSource.timeSecondsAgo(updatedThresholdSec));
        List<JobStateByDate> states = new ArrayList<>();
        newerJobs.forEach(job -> states.add(createStateByDateForJob(job.getId(), job.getState(),job.getRunContext())));
        return states;
    }

    public JobStateByDate createStateByDateForJob(Long jobId, JobState state, Long runId) {
        return JobStateByDate.of(jobId, state,
                fileCrawlerExecDetailsServiceTxUtil.getRunDetails(runId).getStopTimeAsLong());
    }

    public JobState getCachedJobState(long jobId) {
        if (jobId > 0) {
            SimpleJobDto job = jobCache.getForRead(jobId);
            return job == null ? null : job.getState();
        }
        return null;
    }

    public JobType getCachedJobType(Long jobId) {
        if (jobId > 0) {
            SimpleJobDto job = jobCache.getForRead(jobId);
            return job == null ? null : job.getType();
        }
        return null;
    }

    public Integer getCachedJobTaskCounter(Long jobId) {
        if (jobId > 0) {
            SimpleJobDto job = jobCache.getForRead(jobId);
            return job == null ? null : job.getCurrentTaskCount();
        }
        return null;
    }

    public List<JobStateByDate> getCachedJobStates() {
        Collection<Long> jobIds = new ArrayList<>();
        for (List<Long> jobs : jobIdCache.asMap().values()) {
            jobIds.addAll(jobs);
        }

        List<JobStateByDate> states = new ArrayList<>();
        jobIds.forEach(id -> {
            if (id > 0) {
                SimpleJobDto job = jobCache.getForRead(id);
                if (job.getState() != JobState.DONE) {
                    states.add(createStateByDateForJob(id, job.getState(), job.getRunContext()));
                }
            }
        });
        return states;
    }

    @Scheduled(cron = "${job-manager.tasks-table-maintenance.cron:0 15 2 * * *}")   // Default: daily at 2:15am
    @ConditionalOnProperty(value = "is.toolExecutionMode", havingValue = "false", matchIfMissing = true)
    public void tasksTableMaintenanceSchedule() {
        if (!tasksTableMaintenanceActive) {
            logger.debug("Periodic tasks table maintenance not active. Skip");
            return;
        }
        logger.info("Running periodic tasks table maintenance");
        cleanTasksTablePartitions();
    }

    private void cleanTasksTablePartitions() {
        // TODO: implement ...
        long currentPartitionId = currentPartitionId();
        List<Long> partitionIds = getTaskTablePartitionIds();
        if (!partitionIds.remove(currentPartitionId)) {
            logger.warn("Current partition {} is not in partitions list: {}", currentPartitionId,
                    partitionIds.stream().map(p -> "" + p).collect(Collectors.joining(",")));
        }
        for (Long partition : partitionIds) {
            Map<JobState, Long> jobsPerState = getTasksPartitionJobState(partition);
            long unDone = jobsPerState.entrySet().stream()
                    .filter(e -> !JobState.DONE.equals(e.getKey()))
                    .mapToLong(Map.Entry::getValue).sum();
            if (logger.isDebugEnabled()) {
                logger.debug("Jobs states for task partition {}{}: {}",
                        partition, (partition.intValue() == currentPartitionId ? " (current)" : ""),
                        jobsPerState.entrySet().stream()
                                .map(e -> "{" + e.getKey() + ":" + e.getValue() + "}")
                                .collect(Collectors.joining(",")));
            }
            logger.info("Found {} tasks for un-done jobs in partition {}", unDone, partition);
            if (unDone == 0) {
                logger.info("Deleting tasks-table partition {} ...");
                boolean deleted = simpleTaskService.deleteSimpleTaskByPartitionId(partition);
                logger.warn("Tasks-table partition {}{} deleted", partition, deleted ? "" : " NOT");
            }
        }
    }


    // -------------------------------- private methods -----------------------------------------------------------

    /**
     * Get job ID by run ID and job type
     * This method is transactional with help of manual transaction.
     * The reason for manual transaction is that this method sometimes is called from within
     *
     * @param runContext run ID
     * @param jobType    job type
     * @return job ID in the DB
     */
    private List<Long> getJobId(Long runContext, JobType jobType) {
        TransactionTemplate tmpl = new TransactionTemplate(transactionManager);
        tmpl.setReadOnly(true);
        return tmpl.execute(status -> simpleJobRepository.findJobId(runContext, jobType));
    }

    /**
     * Retrieve job by ID
     * This method is transactional!
     * The transaction is programmatic, since sometimes this method is called from within the service
     *
     * @param jobId job ID
     * @return simple job
     */
    public SimpleJob getJobById(long jobId) {
        return simpleJobRepository.getOne(jobId);
    }

    // this method is only called by cache persist which
    // locks the cache for write first
    private void updateJob(final SimpleJobDto jobDto) {
        if (jobDto == null) {
            return;
        }
        try {
            DBTemplateUtils.doInTransaction(
                    () -> {
                        SimpleJob persistentJob = simpleJobRepository.getOne(jobDto.getId());
                        mergeSimpleJob(persistentJob, jobDto);
                        logger.trace("persist job {} to db, data {}", jobDto.getId(), persistentJob);
                        simpleJobRepository.save(persistentJob);
                    } ,TransactionDefinition.PROPAGATION_REQUIRES_NEW);
        } catch (Exception e) {
            logger.error("Failed to update state for job {}. jobDto={}", jobDto.getId(), jobDto, e);
            throw new RuntimeException("Failed to update state for job " + jobDto.getId(), e);
        }
    }

    // Negative number will result time stamp update with no counter update
    private void updateJobCounters(SimpleJobDto job, Number taskCounter, Integer meaningfulFilesCount) {
        if (taskCounter == null) {
            return;
        }
        if (taskCounter.intValue() < 0) {
            job.setLastTaskCountTime(timeSource.currentTimeMillis());
        } else if (job.getCurrentTaskCount() != taskCounter.intValue()) {
            PhaseRunningRateDetails phaseRunningRateDetails = getPhaseRunningRateDetails(job.getId(), rateCalcIntervalSec);
            phaseRunningRateDetails.calculateRunningRate(job.getCurrentTaskCount(), true);
            job.setCurrentTaskCount(taskCounter.intValue());
            job.setScannedMeaningfulFilesCount(meaningfulFilesCount);
            job.setLastTaskCountTime(timeSource.currentTimeMillis()); //TODO Liora, this is 0 before first flush. causes problems in consistency manager
        }
        if (job.getStartRunTime() == 0 && job.getState().equals(JobState.IN_PROGRESS)) {
            job.setStartRunTime(timeSource.currentTimeMillis());
            logger.debug("Forced un-initialized start time for job#{}/{} to now", job.getId(), job.getType().toString());
        }
    }

    public PhaseRunningRateDetails getPhaseRunningRateDetails(long id, long rateCalcIntervalSec) {
        PhaseRunningRateDetails prd = phaseRunningRateDetailsMap.get(id);
        if (prd == null) {
            prd = new PhaseRunningRateDetails(id, rateCalcIntervalSec);
            phaseRunningRateDetailsMap.put(id, prd);
        }
        return prd;
    }

    public void cleanPhaseRunningRateMap() {
        try {
            long cleanTimeThreshold = timeSource.timeSecondsAgo(rateIntervalsToKeep * rateCalcIntervalSec);
            List<Long> idsToClean = phaseRunningRateDetailsMap.values().stream()
                    .filter(p -> p.getPrevTaskCountTime() < cleanTimeThreshold)
                    .map(PhaseRunningRateDetails::getJobId).collect(toList());
            logger.debug("Cleaning {} items from phaseRunningRateDetailsMap: {}", idsToClean.size(), idsToClean);
            idsToClean.forEach(id -> phaseRunningRateDetailsMap.remove(id));
        } catch (Exception e) {
            logger.error("cleanPhaseRunningRateMap failed", e);
        }
    }

    public ConcurrentMap<Long, PhaseRunningRateDetails> getPhaseRunningRateDetailsMap() {
        return phaseRunningRateDetailsMap;
    }

    private boolean isJobPaused(Long jobId) {
        SimpleJobDto job = jobCache.getForRead(jobId);
        return JobState.PAUSED.equals(job.getState());
    }

    @Transactional(readOnly = true)
    public void checkIfJobPaused(Long jobId) {
        if (isJobPaused(jobId)) {
            throw new StoppedByUserException("Job is paused. Stop job processing");
        }
    }

    @Transactional
    public void createIngestFinalizeTask(Long runContext, Long ingestFinJobId) {
        if (ingestFinJobId == null) {
            ingestFinJobId = getJobIdCached(runContext, JobType.INGEST_FINALIZE);
        }
        Long jobPartition = getJobPartition(ingestFinJobId);

        SimpleTask task = new SimpleTask(TaskType.INGEST_FINALIZE_CONTENT, jobPartition);
        task.setJobId(ingestFinJobId);
        task.setState(TaskState.NEW);
        task.setRunContext(runContext);
        taskManagerService.createTask(task);
    }

    @Transactional
    public void createFinalizeTasksIfNeeded(Long runContext) {
        Long ingestFinJobId = getJobIdCached(runContext, JobType.INGEST_FINALIZE);
        if (ingestFinJobId != null) {
            createIngestFinalizeTask(runContext, ingestFinJobId);
        }
        createTask(runContext, JobType.ANALYZE_FINALIZE, TaskType.ANALYZE_FINALIZE, null);
    }

    public boolean rootFolderJobWasDone(long rootFolderId, JobType jobType) {
        if (rootFolderId < 1) {
            throw new IllegalArgumentException("rootFolderId must be greater than or equal to 1. Got: " + rootFolderId);
        }
        SimpleJob probe = new SimpleJob();
        probe.setType(jobType);
        probe.setState(JobState.DONE);
        probe.setItemId(rootFolderId);

        ExampleMatcher jobTypeStateAndItemIdMatcher = ExampleMatcher.matching()
                .withIgnorePaths(ReflectionUtils.fieldNames(probe, "type", "state", "itemId"));

        Example<SimpleJob> doneIngestJobOfRootFolder = Example.of(probe, jobTypeStateAndItemIdMatcher);
        return simpleJobRepository.exists(doneIngestJobOfRootFolder);
    }

    public boolean rootFolderWasAlreadyIngestedFinalized(Long rootFolderId) {
        return rootFolderAlreadyIngestedFinalizeIds.contains(rootFolderId);
    }

    @AutoAuthenticate
    public void createAnalysisRunForRootFolder(RootFolderDto rootFolderDto, Long crawlRunId) {
        createAnalysisRunForRootFolder(crawlRunId, cursor -> fileCatService.getIngestedContents(cursor, createTaskBatchSize, rootFolderDto.getId()), true);
    }

    private void createAnalysisRunForRootFolder(Long crawlRunId, Function<String, Pair<String, Set<Long>>> fileSearcher, boolean changeAnalysisJob) {
        List<Long> jobs = getJobId(crawlRunId, JobType.ANALYZE);
        if (jobs == null || jobs.isEmpty()) {
            throw new RuntimeException(messageHandler.getMessage("analyze-job.missing"));
        }
        // get job
        Long analysisJobId = jobs.get(0);
        SimpleJob analysisJob = getJobById(analysisJobId);

        if (changeAnalysisJob) {
            setOpMsg(analysisJobId, "Calculate work");
        }

        // create tasks
        String cursor = "*";
        Pair<String, Set<Long>> getResult;
        Set<Long> contents;
        int analyzeTasksCount = 0;
        try {
            do {
                long startTime = System.currentTimeMillis();
                getResult = fileSearcher.apply(cursor);
                cursor = getResult.getKey();
                contents = getResult.getValue();
                Integer upd = solrTaskService.updateContentsForAnalysisStart(crawlRunId, contents);
                analyzeTasksCount += (upd == null ? 0 : upd);
                logger.debug("{} tasks created for run {} and analysis  job {}", upd, crawlRunId, analysisJobId);
            } while (!contents.isEmpty());
        } catch (Exception e) {
            logger.error("fail to create analysis tasks for job {} run {} and root folder {}", analysisJobId, crawlRunId, e);
        }

        // start job
        if (changeAnalysisJob) {
            CrawlRun run = getCrawlRun(crawlRunId);
            if (run != null && run.getRunStatus().isActive()) {
                updateEstimatedTaskCount(analysisJobId, analyzeTasksCount);
                updateJobState(analysisJobId, JobState.IN_PROGRESS, null,
                        "Start analyzing", true);
            } else {
                logger.trace("cannot set analysis job {} to in progress as run {} is not active", analysisJobId, crawlRunId);
            }
        }
    }

    @AutoAuthenticate
    public void createIngestRunForRootFolder(RootFolderDto rootFolderDto, Long crawlRunId) {
        Long rootFolderId = rootFolderDto.getId();
        createIngestRunForRootFolder(rootFolderId, crawlRunId, cursor -> fileCatService.getScannedFiles(cursor, createTaskBatchSize, rootFolderId), true);
    }

    private void createIngestRunForRootFolder(Long rootFolderId, Long crawlRunId, Function<String, Pair<String ,Map<Long, String>>> fileSearcher, boolean changeIngestJob) {
        List<Long> jobs = getJobId(crawlRunId, JobType.INGEST);
        if (jobs == null || jobs.isEmpty()) {
            throw new RuntimeException(messageHandler.getMessage("ingest-job.missing"));
        }
        // get job
        Long ingestJobId = jobs.get(0);
        SimpleJob ingestJob = getJobById(ingestJobId);

        if (changeIngestJob) {
            setOpMsg(ingestJobId, "Calculate work");
        }

        // create tasks
        String params = IngestTaskJsonProperties.ofFirstScan().toJsonString();

        Predicate<? super String> predicate = docStoreService.getRootFolderIngestTypePredicate(rootFolderId);

        String cursor = "*";
        Pair<String ,Map<Long, String>> getResult;
        Map<Long, String> files;
        int ingestTasksCount = 0;
        try {
            do {
                long startTime = System.currentTimeMillis();
                getResult = fileSearcher.apply(cursor);
                cursor = getResult.getKey();
                files = getResult.getValue();
                List<Long> fileIdsToIngest = new ArrayList<>();
                for (Map.Entry<Long, String> entry : files.entrySet()) {
                    if (predicate.test(entry.getValue())) {
                        fileIdsToIngest.add(entry.getKey());
                    }
                }
                if (!fileIdsToIngest.isEmpty()) {
                    Integer upd = solrTaskService.updateFilesForIngestStart(crawlRunId, fileIdsToIngest, params);
                    ingestTasksCount += (upd == null ? 0 : upd);
                    logger.debug("{} ingest tasks created for job {} run {} and root folder {}", upd, ingestJobId, crawlRunId, rootFolderId);
                }
            } while (!files.isEmpty());
        } catch (Exception e) {
            logger.error("fail to create ingest tasks for job {} run {} and root folder {}", ingestJobId, crawlRunId, rootFolderId, e);
        }

        // start job
        if (changeIngestJob) {
            CrawlRun run = getCrawlRun(crawlRunId);
            if (run != null && run.getRunStatus().isActive()) {
                updateEstimatedTaskCount(ingestJobId, ingestTasksCount);
                updateJobState(ingestJobId, JobState.IN_PROGRESS, null,
                    "Start ingesting", true);
                updateMpJobState(ingestJobId);
            } else {
                logger.trace("cannot set ingest job {} to in progress as run {} is not active", ingestJobId, crawlRunId);
            }
        }
    }

    public List<SimpleTask> getTasksByIds(List<Long> taskIds) {
        return simpleTaskRepository.findAllById(taskIds);
    }

    public void setRootFolderWasIngestedFinalized(Long rootFolderId) {
        rootFolderAlreadyIngestedFinalizeIds.add(rootFolderId);
    }

    /**
     * Creating ingest tasks for files who were scanned only without ingest operation.
     * The ingest includes all files from previous runs which are in SCANNED state (but excludes current run)
     * it is a fix of bug DAMAIN-5365
     */
    public void createIngestTasksForMappedOnlyFiles(long runId, RootFolder rootFolder) {
        List<Long> ingestJobs = getJobId(runId, JobType.INGEST);
        if (ingestJobs.isEmpty()) {
            logger.debug("No ingest job for runID {}, skipping the enforcement of \"ingest scanned only files\".", runId);
            return;
        }

        DBTemplateUtils.doInTransaction(
                () -> {
                    List<Long> currentScanJob = getJobId(runId, JobType.SCAN);
                    logger.info("Forcing ingest tasks job for runID {}, in order to ingest \"scanned only\" files.", runId);
                    SimpleJob scanJob = currentScanJob != null && currentScanJob.size() > 0 ? getJobById(currentScanJob.get(0)) : null;
                    Date scanJobStartedDate = scanJob != null ? new Date(scanJob.getStartRunTime()) : getCrawlRun(runId).getStartTime();
                    Long rootFolderId = rootFolder.getId();
                    createIngestRunForRootFolder(rootFolderId, runId, cursor ->
                            fileCatService.getMappedNotIngestedFiles(cursor, createTaskBatchSize, rootFolderId, scanJobStartedDate), false);
                }
        );
    }

    /**
     * Creating analysis tasks for contents who were ingested only without analysis operation.
     * The analysis includes all contents from previous runs which are in INGESTED state (but excludes current run)
     */
    @AutoAuthenticate
    public void createAnalysisTasksForIngestedOnlyContents(long runId, Long rootFolderId) {
        List<Long> analysisJobs = getJobId(runId, JobType.ANALYZE);
        if (analysisJobs.isEmpty()) {
            logger.debug("No analysis job for runID {}, skipping the enforcement of \"analysis of ingest only contents\".", runId);
            return;
        }

        try {
            DBTemplateUtils.doInTransaction(
                    () -> {
                        List<Long> currentScanJob = getJobId(runId, JobType.SCAN);
                        logger.info("Forcing analysis tasks job for runID {}, in order to analyze \"ingest only\" contents.", runId);
                        SimpleJob scanJob = currentScanJob != null && currentScanJob.size() > 0 ? getJobById(currentScanJob.get(0)) : null;
                        Date scanJobStartedDate = scanJob != null ? new Date(scanJob.getStartRunTime()) : getCrawlRun(runId).getStartTime();
                        createAnalysisRunForRootFolder(runId, cursor -> fileCatService.getIngestNotAnalyzedContents(cursor, createTaskBatchSize, rootFolderId, scanJobStartedDate), false);
                    }
            );
        } catch (Exception e) {
            logger.error("failed forcing analysis tasks job to ingested contents run {} root folder {}", runId, rootFolderId, e);
        }
    }

    public List<Long> getRunsByJobAndState(JobType jobType, JobState jobState) {
        return simpleJobRepository.findRunsByJobAndState(jobType.name(), jobState.name())
                .stream().map(BigInteger::longValue).collect(Collectors.toList());
    }

    private class JobCacheLoader extends CacheLoader<Long, SimpleJobDto> {
        private TransactionTemplate tmpl;

        public JobCacheLoader() {
            tmpl = new TransactionTemplate(transactionManager);
            tmpl.setReadOnly(true);
        }

        @Override
        public SimpleJobDto load(@SuppressWarnings("NullableProblems") Long key) {
            tmpl.setReadOnly(true);
            return tmpl.execute(status -> {
                SimpleJob jobEntity = simpleJobRepository.getOne(key);
                return simpleJobToDto(jobEntity);
            });
        }
    }

    /**
     * Used for caching job IDs by context and type
     */
    private class JobCacheKey {
        private long runContext;
        private JobType jobType;

        public JobCacheKey(long runContext, JobType jobType) {
            this.runContext = runContext;
            this.jobType = jobType;
        }

        public long getRunContext() {
            return runContext;
        }

        public JobType getJobType() {
            return jobType;
        }

        @Override
        public String toString() {
            return "JobCacheKey{" +
                    "runContext=" + runContext +
                    ", jobType=" + jobType +
                    '}';
        }

        @Override
        @SuppressWarnings("SimplifiableIfStatement")
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            JobCacheKey that = (JobCacheKey) o;

            if (runContext != that.runContext) {
                return false;
            }
            return jobType == that.jobType;
        }

        @Override
        public int hashCode() {
            int result = (int) (runContext ^ (runContext >>> 32));
            result = 31 * result + (jobType != null ? jobType.hashCode() : 0);
            return result;
        }
    }

    public static List<PhaseDetailsDto> convertRunJobsToPhaseDetailsDto(List<SimpleJob> simpleJobs, ConcurrentMap<Long, PhaseRunningRateDetails> phaseRunningRateDetailsMap) {
        List<PhaseDetailsDto> result = new ArrayList<>();
        for (SimpleJob simpleJob : simpleJobs) {
            result.add(convertRunJobToPhaseDetailsDto(simpleJobToDto(simpleJob), phaseRunningRateDetailsMap.get(simpleJob.getId())));
        }
        return result;
    }

    public static List<PhaseDetailsDto> convertSimpleJobDto(List<SimpleJobDto> simpleJobs, ConcurrentMap<Long, PhaseRunningRateDetails> phaseRunningRateDetailsMap) {
        List<PhaseDetailsDto> result = new ArrayList<>();
        for (SimpleJobDto simpleJob : simpleJobs) {
            result.add(convertRunJobToPhaseDetailsDto(simpleJob, phaseRunningRateDetailsMap.get(simpleJob.getId())));
        }
        return result;
    }

    public static PhaseDetailsDto convertRunJobToPhaseDetailsDto(SimpleJobDto runJob, PhaseRunningRateDetails phaseRunningRateDetails) {
        PhaseDetailsDto phaseDetailsDto = new PhaseDetailsDto();
        phaseDetailsDto.setName(runJob.getName());
        phaseDetailsDto.setJobType(runJob.getType());
        phaseDetailsDto.setJobId(runJob.getId());
        phaseDetailsDto.setPhaseMaxMark(runJob.getEstimatedTaskCount() < runJob.getCurrentTaskCount() ? 0 : (long) runJob.getEstimatedTaskCount()); // new files, remove max
        phaseDetailsDto.setPhaseCounter(runJob.getCurrentTaskCount());
        phaseDetailsDto.setPhaseStart(runJob.getStartRunTime());
        phaseDetailsDto.setPauseDuration(runJob.getPauseDuration());
        phaseDetailsDto.setPhaseLastStopTime(runJob.getLastStopTime());
        phaseDetailsDto.setStateDescription(runJob.getJobStateString());
        phaseDetailsDto.setJobState(runJob.getState());
        phaseDetailsDto.setCompletionStatus(runJob.getCompletionStatus());
        phaseDetailsDto.setFailedTasksCount(runJob.getFailedCount());
        phaseDetailsDto.setTotalTaskRetriesCount(runJob.getTotalTaskRetriesCount());

        if (JobType.EXTRACT.equals(runJob.getType()) || JobType.EXTRACT_FINALIZE.equals(runJob.getType())) {
            phaseDetailsDto.setBizListId(runJob.getItemId());
        }
        if (runJob.getStartRunTime() > 0) {
            double runningTimeSeconds = 0;
            switch (runJob.getState()) {
                case NEW:
                    break;
                case DONE:
                    runningTimeSeconds = TimeUnit.MILLISECONDS.toSeconds(
                            runJob.getLastStopTime() - runJob.getStartRunTime() - runJob.getPauseDuration()
                    );
                    break;
                case PAUSED:
                    runningTimeSeconds = TimeUnit.MILLISECONDS.toSeconds(
                            runJob.getLastStopTime() - runJob.getStartRunTime() - runJob.getPauseDuration()
                    );
                    break;
                case IN_PROGRESS:
                    runningTimeSeconds = timeSource.secondsSince(runJob.getStartRunTime());
                    runningTimeSeconds -= TimeUnit.MILLISECONDS.toSeconds(runJob.getPauseDuration());
                    break;
                case READY:
                    break;
            }
            if (runningTimeSeconds > 0) {
                phaseDetailsDto.setTasksPerSecRate(
                        runJob.getCurrentTaskCount() / runningTimeSeconds
                );
            }
        }
        if (phaseRunningRateDetails == null) {
            phaseDetailsDto.setTasksPerSecRunningRate(0d);
            phaseDetailsDto.setRunningRateDurationInMsc(TimeUnit.SECONDS.toMillis(10));     // Dummy value
            logger.trace("Job#{} recent-rate to zero (running rate details not found)", runJob.getId());
        } else {
            if (JobState.IN_PROGRESS.equals(runJob.getState())) {
                double runningRate = phaseRunningRateDetails.calculateRunningRate(runJob.getCurrentTaskCount(), false);
                phaseDetailsDto.setTasksPerSecRunningRate(runningRate);
                phaseDetailsDto.setRunningRateDurationInMsc(TimeUnit.SECONDS.toMillis(phaseRunningRateDetails.getRateCalcIntervalSec()));
                if (logger.isTraceEnabled()) {
                    logger.trace("Job#{} calculated runnning rate {}", runJob.getId(), ((int) (1000 * runningRate)) / 1000f);
                }
            } else {
                // Hold last calculated value for a while
                phaseDetailsDto.setTasksPerSecRunningRate(phaseRunningRateDetails.getRecentRunningRate());
            }
        }

//        int tasksPerTimeInterval = runJob.getCurrentTaskCount() - runJob.getPreviousTaskCount();
//        long timeInterval = runJob.getLastTaskCountTime() - runJob.getPrevTaskCountTime();
//        if(timeInterval>0  && timeInterval>1000 ) {
//            double durationIntervalSeconds = TimeUnit.MILLISECONDS.toSeconds(timeInterval);
//            phaseDetailsDto.setRunningRateDurationInMsc(timeInterval);
//            phaseDetailsDto.setTasksPerSecRunningRate(tasksPerTimeInterval / durationIntervalSeconds);
//        }

        return phaseDetailsDto;
    }

    public static SimpleJobDto simpleJobToDto(final SimpleJob job) {
        SimpleJobDto copy = new SimpleJobDto();
        copy.setId(job.getId());
        copy.setName(job.getName());
        copy.setState(job.getState());
        copy.setItemId(job.getItemId());
        copy.setRunContext(job.getRunContext());
        copy.setType(job.getType());
        copy.setStateUpdateTime(job.getStateUpdateTime());
        copy.setPauseDuration(job.getPauseDuration());
        copy.setLastStopTime(job.getLastStopTime());
        copy.setStartRunTime(job.getStartRunTime());
        copy.setResumeState(job.getResumeState());
        copy.setCurrentTaskCount(job.getCurrentTaskCount());
        copy.setLastTaskCountTime(job.getLastTaskCountTime());
        copy.setCompletionStatus(job.getCompletionStatus());
        copy.setJobStateString(job.getJobStateString());
        copy.setEstimatedTaskCount(job.getEstimatedTaskCount());
        copy.setFailedCount(job.getFailedCount());
        copy.setTotalTaskRetriesCount(job.getTaskRetriesCount());
        copy.setInitiationDetails(job.getInitiationDetails());
        copy.setPartitionId(job.getTasksPartition());
        copy.setScannedMeaningfulFilesCount(job.getScannedMeaningfulFilesCount());
        return copy;
    }

    public static void mergeSimpleJob(final SimpleJob job, final SimpleJobDto dto) {
        job.setCurrentTaskCount(dto.getCurrentTaskCount());
        job.setEstimatedTaskCount(dto.getEstimatedTaskCount());
        // Prev counter and its timestamp should not be persistant (IR.)
//        job.setPreviousTaskCount(dto.getPreviousTaskCount());
//        job.setPrevTaskCountTime(dto.getPrevTaskCountTime());
        job.setLastTaskCountTime(dto.getLastTaskCountTime());
        job.setFailedCount(dto.getFailedCount());
        job.setJobStateString(dto.getJobStateString());
        job.setTaskRetriesCount(dto.getTotalTaskRetriesCount());
        job.setScannedMeaningfulFilesCount(dto.getScannedMeaningfulFilesCount());
    }

    public static List<SimpleJobDto> simpleJobsToDto(List<SimpleJob> runJobs) {
        return runJobs.stream().map(JobManagerService::simpleJobToDto).collect(Collectors.toList());
    }

    public void updateMpJobState(Long jobId) {
        SimpleJobDto job = jobCache.getForRead(jobId);
        if (job != null) {
            List<JobStateByDate> jobStates = new ArrayList<>();
            jobStates.add(createStateByDateForJob(jobId, job.getState(), job.getRunContext()));
            mediaProcessorCommService.sendJobsStatesControlRequest(jobStates);
            logger.trace("send mp update on job {} state {}", jobId, job.getState());
        } else {
            logger.warn("cannot find job {} will not update mp", jobId);
        }
    }
}
