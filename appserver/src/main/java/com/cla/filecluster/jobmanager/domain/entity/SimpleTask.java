package com.cla.filecluster.jobmanager.domain.entity;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;

@Entity
@Table(name = "jm_tasks",
        indexes = {
                @Index(name = "job_idx", columnList = "job_id, deleted"),
                @Index(name = "job_state_idx", columnList = "job_id, state, deleted"),
                @Index(name = "job_type_idx", columnList = "job_id, type"),
                @Index(name = "job_run_context_idx", columnList = "run_context"),
                @Index(name = "tasks_by_state_idx", columnList = "deleted, type, run_context, state, job_id")
        })
public class SimpleTask extends Task {

    @Column(name = "item_id")
    private Long itemId;

    @Column(columnDefinition = "char(36)", name = "str_item_id")
    private String strItemId;

    @Column(name = "name")
    private String name;

    @Column(columnDefinition = "json", name = "json_data")
    private String jsonData;

    // partitioning in MySql doesn't support existence of foreign keys.
    // the job reference is therefore a soft one.
    @Column(nullable = false, name = "job_id")
    private long jobId;

    @Column(nullable = false, name = "part_id")
    private long partitionId = 0;

    @Length(max = 2048)
    private String path;

    @Version
    @ColumnDefault(value="'0'")
    private int version = 0;

    public SimpleTask() {
    }

    public SimpleTask(SimpleJob job, TaskType taskType) {
        super(taskType);
        if (job.getId() == null) {
            throw new RuntimeException("Task creation must have a jobId");
        }
        this.jobId = job.getId();
        this.name = taskType.name();
        this.partitionId = job.getTasksPartition();
        this.setRunContext(job.getRunContext());

    }

    public SimpleTask(TaskType taskType) {
        super(taskType);
    }

    public SimpleTask(TaskType taskType, Long partitionId) {
        super(taskType);
        this.partitionId = partitionId;
    }

    @PostLoad
    protected void populateTransientFields() {
        super.populateTransientFields();
    }

    public long getJobId() {
        return jobId;
    }

    public void setJobId(long jobId) {
        this.jobId = jobId;
    }

    public Long getItemId() {
        return itemId;
    }

    public void setItemId(long itemId) {
        this.itemId = itemId;
    }

    public String getStrItemId() {
        return strItemId;
    }

    public void setStrItemId(String strItemId) {
        this.strItemId = strItemId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getJsonData() {
        return jsonData;
    }

    public void setJsonData(String jsonData) {
        this.jsonData = jsonData;
    }

    public long getPartitionId() {
        return partitionId;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }

    public void setPartitionId(long partitionId) {
        this.partitionId = partitionId;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    @Override
    public String toString() {
        return "SimpleTask{" +
                super.toString()+
                ",itemId=" + itemId +
                ", strItemId='" + strItemId + '\'' +
                ", name='" + name + '\'' +
                ", jsonData='" + jsonData + '\'' +
                ", jobId=" + jobId +
                ", partitionId=" + partitionId +
                ", path='" + path + '\'' +
                '}';
    }
}
