package com.cla.filecluster.jobmanager.domain.entity;

import javax.persistence.*;


@MappedSuperclass
public class Task extends BaseEntity implements IdentifiableByLong {

    @Id
    private Long id;    // unique long ID

    /**
     * Job run context, in which the job was triggered
     * e.g. specific manual scan triggered from UI
     * or scheduled scan. Enables to trace a process end-to-end
     */
    @Column(nullable = false, name = "run_context")
    private Long runContext;

    @Enumerated(EnumType.ORDINAL)
    @Column(nullable = false, name = "state")
    protected TaskState state = TaskState.NEW;

    @Column(name = "state_update_time")
    private Long stateUpdateTime = 0L;

    @Column(nullable = false, name = "type")
    protected int type;

    @Column(name = "retries")
    private int retries = 0;

    @Transient
    private TaskType taskType = TaskType.UNKNOWN;

    // -------------------------- Constructors ----------------------

    public Task() {
        this(TaskType.UNKNOWN);
    }

    public Task(TaskType taskType) {
        setTaskType(taskType);
        setStateUpdateTime();
    }

    // ---------------------- getters/setters ------------------------

    public Long getId() {
        return id;
    }

    public Long getRunContext() {
        return runContext;
    }

    public void setRunContext(Long runContext) {
        this.runContext = runContext;
    }

    public int getType() {
        return type;
    }

    public TaskType getTaskType() {
        if (TaskType.UNKNOWN.toInt() != type) {
            taskType = TaskType.valueOf(type);
        }
        return taskType;
    }

    private void setTaskType(TaskType taskType) {
        this.type = taskType.toInt();
        this.taskType = taskType;
    }

    public TaskState getState() {
        return state;
    }

    public void setState(TaskState state) {
        this.state = state;
        setStateUpdateTime();
    }

    public Long getStateUpdateTime() {
        return stateUpdateTime;
    }

    public void setStateUpdateTime(Long stateUpdateTime) {
        this.stateUpdateTime = stateUpdateTime;
	}

    @PreUpdate
    public void setStateUpdateTime() {
//        this.stateUpdateTime =  (new Date()).getTime();
        this.stateUpdateTime =  System.currentTimeMillis();
    }

    public int getRetries() {
        return retries;
    }

    public void setRetries(int retries) {
        this.retries = retries;
    }

    protected void populateTransientFields() {
        taskType = TaskType.valueOf(type);
    }

    @Override
    public String toString() {
        return "Task{" +
                "id=" + id +
                ", runContext=" + runContext +
                ", state=" + state +
                ", type=" + type +
                ", taskType=" + taskType +
                '}';
    }

    public void setId(Long id) {
        this.id = id;
    }
}
