package com.cla.filecluster.jobmanager.domain.entity;

import com.cla.common.domain.dto.messages.IngestTaskParameters;
import com.cla.common.utils.JsonDtoConversionUtils;
import com.cla.connector.domain.dto.file.DiffType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.base.Strings;

import java.util.*;

/**
 * Created By Itai Marko
 */
public class IngestTaskJsonProperties {

    private Map<String, String> stringProps = new HashMap<>();

    private List<Long> containedFileIDs = Collections.emptyList();

    public static IngestTaskJsonProperties ofFirstScan() {
        IngestTaskJsonProperties jsonProps = new IngestTaskJsonProperties();
        jsonProps.stringProps.put(IngestTaskParameters.FIRST_SCAN, "true");
        return jsonProps;
    }

    public static IngestTaskJsonProperties forDiffType(DiffType diffType) {
        IngestTaskJsonProperties jsonProps = new IngestTaskJsonProperties();
        switch (diffType) {
            case NEW:
                jsonProps.stringProps.put(IngestTaskParameters.FIRST_SCAN, "true");
                return jsonProps;
            case ACL_UPDATED:
                jsonProps.stringProps.put(IngestTaskParameters.METADATA_ONLY_PARAM, "true");
                jsonProps.stringProps.put(IngestTaskParameters.FIRST_SCAN, "false");
                return jsonProps;
            case RENAMED:
            case CONTENT_UPDATED:
                jsonProps.stringProps.put(IngestTaskParameters.FIRST_SCAN, "false");
                return jsonProps;
            default:
                throw new IllegalArgumentException("Ingest-task json properties was requested with diffType: "
                        + diffType + " in which case, an ingest task is not supposed to be created");
        }
    }

    public static IngestTaskJsonProperties fromJsonString(String json) {
        if (Strings.isNullOrEmpty(json)) {
            return new IngestTaskJsonProperties();
        }
        return JsonDtoConversionUtils.convertJsonStringToObj(json, IngestTaskJsonProperties.class);
    }


    public String toJsonString() {
        return JsonDtoConversionUtils.convertObjToJson(this);
    }

    @JsonIgnore
    public boolean isFirstScan() {
        return getBooleanProp(IngestTaskParameters.FIRST_SCAN);
    }

    @JsonIgnore
    public boolean isMetadataOnly() {
        return getBooleanProp(IngestTaskParameters.METADATA_ONLY_PARAM);
    }

    private boolean getBooleanProp(String propName) {
        if (stringProps.containsKey(propName)) {
            String boolPropVal = stringProps.get(propName);
            return Boolean.parseBoolean(boolPropVal);
        }
        return false;
    }

    @SuppressWarnings("unused") // Necessary for Json serialization
    public Map<String, String> getStringProps() {
        return stringProps;
    }

    @SuppressWarnings("unused") // Necessary for Json deserialization
    public void setStringProps(Map<String, String> stringProps) {
        this.stringProps = stringProps;
    }

    public List<Long> getContainedFileIDs() {
        return containedFileIDs;
    }

    public void setContainedFileIDs(List<Long> containedFileIDs) {
        this.containedFileIDs = Objects.requireNonNull(containedFileIDs);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IngestTaskJsonProperties that = (IngestTaskJsonProperties) o;
        return Objects.equals(stringProps, that.stringProps) &&
                Objects.equals(containedFileIDs, that.containedFileIDs);
    }

    @Override
    public int hashCode() {
        return Objects.hash(stringProps, containedFileIDs);
    }
}
