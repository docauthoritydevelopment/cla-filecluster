package com.cla.filecluster.jobmanager.service;

import com.cla.common.domain.dto.crawler.JobCompletionStatus;
import com.cla.common.domain.dto.jobmanager.JobState;
import com.cla.common.domain.dto.jobmanager.JobType;
import com.cla.common.domain.dto.jobmanager.SimpleJobDto;
import com.cla.connector.utils.TimeSource;
import com.cla.filecluster.jobmanager.domain.entity.SimpleJob;
import com.cla.filecluster.jobmanager.domain.entity.TaskState;
import com.cla.filecluster.jobmanager.repository.jpa.SimpleJobRepository;
import com.cla.filecluster.jobmanager.repository.jpa.SimpleTaskRepository;
import com.cla.filecluster.service.api.ModelDtoConversionUtils;
import org.hibernate.StaleStateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * used to create new transactions for status change and allow retry
 *
 * Created by: yael
 * Created on: 5/2/2018
 */
@Service
public class JobManagerServiceTxUtil {

    private static final int RETRY_MAX_OPT_LOCK_JOB = 2;

    private static final Logger logger = LoggerFactory.getLogger(JobManagerServiceTxUtil.class);

    @Autowired
    private SimpleJobRepository simpleJobRepository;

    @Autowired
    private SimpleTaskRepository simpleTaskRepository;

    private TimeSource timeSource = new TimeSource();

    /**
     *
     * @param jobId               job ID
     * @param sourceDto           job DTO from cache, which is the source of the statistical data
     * @param newJobState         new job state
     * @param jobCompletionStatus new completion status
     * @param stateOpMessage      new message
     * @return new DTO, result of merge between the job from the DB and the statistical data from cache plus status and messages updates
     */
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Retryable(maxAttempts = RETRY_MAX_OPT_LOCK_JOB, value = {StaleStateException.class, ObjectOptimisticLockingFailureException.class})
    public SimpleJobDto updateJobStateInTx(long jobId, SimpleJobDto sourceDto, JobState newJobState,
                                           JobCompletionStatus jobCompletionStatus, String stateOpMessage,
                                           boolean makeNewJobResumable, Boolean acceptingTasks, boolean allowPrevStateInactive) {

        SimpleJob job = simpleJobRepository.getOne(jobId);

        JobState prevState = job.getState();
        if (newJobState.equals(prevState)) {
            logger.warn("Job {} attempted to change job state to the same state {}", job.getId(),job.getState());
            if (acceptingTasks != null) {
                job.setAcceptingTasks(acceptingTasks);
                simpleJobRepository.save(job);
                logger.info("Job {} changed acceptingTasks to {}", job.getId(), acceptingTasks);
            }
            return null;
        }

        if (!allowPrevStateInactive && !prevState.isActive()) {
            logger.warn("Job {} attempted to change state from {} to {} not allowed", job.getId(),job.getState(), newJobState);
            return null;
        }

        logger.debug("Update Job {} to state {}. CompletionStatus={}", job, newJobState, jobCompletionStatus==null?"-":jobCompletionStatus);
        switch (newJobState) {
            case WAITING:
            case IN_PROGRESS:
                if (JobState.PAUSED == prevState) {
                    long currentPauseDuration = timeSource.millisSince(job.getLastStopTime());
                    job.incrementPauseDuration(currentPauseDuration);
                    logger.debug("Job {} resumed after being paused for {}. Prev. state was {}", job.getId(), currentPauseDuration, job.getResumeState());
                    job.setResumeState(null);
                } else if (JobState.DONE == prevState) {
                    logger.warn("Attempt to change JobId {} from state DONE to IN_PROGRESS. Attempt blocked", jobId);
                    return null;
                } else if (JobState.NEW == prevState || JobState.PENDING == prevState || JobState.READY == prevState) {
                    if (job.getStartRunTime() == 0) {
                        job.setStartRunTime(timeSource.currentTimeMillis());
                    }
                }
                break;

            case DONE:
                if (JobState.PAUSED == prevState) {
                    long currentPauseDuration = timeSource.millisSince(job.getLastStopTime());
                    job.incrementPauseDuration(currentPauseDuration);
                    logger.debug("Job {} set to DONE after being paused for {}. Prev. state was {}", job.getId(), currentPauseDuration, job.getResumeState());
                    job.setResumeState(null);
                }
                if (jobCompletionStatus != null) {
                    job.setCompletionStatus(jobCompletionStatus);
                } else {
                    boolean failedTasks = job.getFailedCount() > 0 || getJobTasksCountByTaskState(job.getId(), TaskState.FAILED) > 0;
                    job.setCompletionStatus(failedTasks ? JobCompletionStatus.DONE_WITH_ERRORS : JobCompletionStatus.OK);
                }
                job.setLastStopTime(timeSource.currentTimeMillis());
                break;
            case PAUSED:
                JobState resumeState = prevState;
                if (prevState == JobState.NEW) { // in case prev state is new resume state shouldn't be cause we cant recover
                    resumeState = job.getType().equals(JobType.ANALYZE_FINALIZE) ? JobState.WAITING : JobState.IN_PROGRESS;
                }
                job.setLastStopTime(timeSource.currentTimeMillis());
                job.setResumeState(resumeState);
                job.setCompletionStatus(jobCompletionStatus);
                break;
        }

        job.setState(newJobState);
        job.setStateUpdateTime(timeSource.currentTimeMillis());
        JobManagerService.mergeSimpleJob(job, sourceDto);

        if (makeNewJobResumable) {
            job.setResumeState(JobState.IN_PROGRESS);
        }

        if (acceptingTasks != null) {
            job.setAcceptingTasks(acceptingTasks);
        }

        if (stateOpMessage != null) {
            String safeStateOpMessage = stateOpMessage;
            if (stateOpMessage.length() > SimpleJob.JOB_STATE_STRING_MAX_SIZE) {
                safeStateOpMessage = stateOpMessage.substring(0, SimpleJob.JOB_STATE_STRING_MAX_SIZE);
            }
            job.setJobStateString(safeStateOpMessage);
        }

        SimpleJobDto targetJobDto = JobManagerService.simpleJobToDto(simpleJobRepository.save(job));
        return targetJobDto;
    }

    private int getJobTasksCountByTaskState(long jobId, TaskState taskState) {
        return getJobTasksCountByTaskState(jobId, 0, taskState);
    }

    private int getJobTasksCountByTaskState(long jobId, long partitionId, TaskState taskState) {
        return partitionId == 0 ?
                simpleTaskRepository.getJobTasksCountByTaskState(jobId, taskState) :
                simpleTaskRepository.getJobTasksCountByTaskState(jobId, partitionId, taskState);
    }
}
