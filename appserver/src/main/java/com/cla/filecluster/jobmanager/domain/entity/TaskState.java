package com.cla.filecluster.jobmanager.domain.entity;

import java.util.Collections;
import java.util.EnumSet;
import java.util.Set;

public enum TaskState {
    NEW,            /*   0 */
    ENQUEUED,       /*   1 */
    CONSUMING,      /*   2 */
    DONE,           /*   3 */
    FAILED          /*   4 */;

    public static final Set<TaskState> ACTIVE_STATES;
    private static TaskState[] cachedValues;
    static {
        cachedValues  = values();
        ACTIVE_STATES = Collections.unmodifiableSet(EnumSet.of(NEW, ENQUEUED, CONSUMING));
    }

    public static TaskState of(int ordinal){
        if (ordinal < 0 || ordinal >= cachedValues.length){
            throw new IndexOutOfBoundsException("Ordinal value is out of bounds: " + ordinal);
        }
        return cachedValues[ordinal];
    }

    public static Set<TaskState> getActiveStates() {
        return ACTIVE_STATES;
    }
}
