package com.cla.filecluster;

import com.cla.filecluster.processes.controllers.ProcessController;
import com.cla.filecluster.processes.domain.scan.ScanBehaviorType;
import com.cla.filecluster.processes.domain.scan.ScanProcessBehavior;
import com.cla.filecluster.processes.domain.scan.ScanProcessConfig;
import com.cla.filecluster.processes.domain.scan.ScanProcessDefinition;
import com.cla.filecluster.scripts.RunUpgradeSqlScripts;
import com.cla.filecluster.service.ecncryptor.CsvEncryptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.solr.SolrAutoConfiguration;
import org.springframework.boot.web.servlet.ServletListenerRegistrationBean;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.retry.annotation.EnableRetry;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.statemachine.config.EnableWithStateMachine;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.UUID;
import java.util.stream.LongStream;

@Configuration
@EnableAutoConfiguration(exclude = {SolrAutoConfiguration.class})
@EnableRetry
@ComponentScan(basePackages = {
        "com.cla.filecluster"
        , "com.cla.common"
        , "com.cla.kvdb"
})
@RestController
@EnableScheduling
@EnableWithStateMachine
@EnableJms
public class Application {
    private static final Logger logger = LoggerFactory.getLogger(Application.class);
    public static final String CLA_FILE_CLUSTER_RUN_UPGRADE_PROPERTY = "com.cla.filecluster.runUpgrade";

    private static ConfigurableApplicationContext context;

    @Autowired
    private CsvEncryptor csvEncryptor;

    public static void main(final String[] args) throws SQLException, ClassNotFoundException, IOException {
        Boolean runUpgrade = Boolean.valueOf(System.getProperty(CLA_FILE_CLUSTER_RUN_UPGRADE_PROPERTY, "false"));
        if (runUpgrade) {
            RunUpgradeSqlScripts.main(new String[]{RunUpgradeSqlScripts.EXECUTE});
        }
        final SpringApplication app = new SpringApplication(Application.class);
        app.setAdditionalProfiles("webapp");
        context = app.run();
        final Application application = context.getBean(Application.class);

        logger.warn("Active profiles : {}", Arrays.asList(context.getEnvironment().getActiveProfiles()));

        if (profilesContains("encryptor")) {
            System.out.println("Call encryptor");
            application.csvEncryptor.encrypt(args);
            context.close();
        } else if (profilesContains("recreateschema")) {
            logger.info("====================================");
            logger.info("Run in recreateschema mode. Exiting.");
            logger.info("====================================");
            context.close();
        } else if (profilesContains("fileprocessor")) {
            logger.info("================================================");
            logger.warn("Run in fileprocessor mode !!!??? (Deprecated)");
            logger.info("================================================");
        }

        System.out.println("AppServer started.");
        //startAndPause(2000);
    }

    private static void startAndPause(long scansToCreate) {
        System.out.println("Starting " + scansToCreate + " scans");
        ProcessController processController = context.getBean(ProcessController.class);
        LongStream.range(0, scansToCreate)
                .forEach(rfId -> processController.startScan(rfId, buildDummyProcessDefinition()));
        try {
            Thread.sleep(11000);
        } catch (InterruptedException e) {
        }
        System.out.println("BOOM!!!");
        System.exit(-1);
        //pauseScansRandomly(processController,scansToCreate);
    }

    private static void pauseScansRandomly(ProcessController processController, long scansToCreate) {
        long minIndex = (long) Math.random() * scansToCreate;
        long maxIndex = minIndex + (int) (Math.random() * ((scansToCreate - minIndex) + 1));
        try {
            Thread.sleep(20000);
        } catch (InterruptedException e) {
        }
        System.out.println(String.format("Pausing from %d to %d", minIndex, maxIndex));
        LongStream.range(minIndex, maxIndex + 1)
                .forEach(processController::pauseScan);
    }

    private static ScanProcessDefinition buildDummyProcessDefinition() {
        ScanProcessDefinition scanProcessDefinition = new ScanProcessDefinition();
        ScanProcessBehavior scanProcessBehavior = new ScanProcessBehavior();
        scanProcessBehavior.setScanBehaviorType(ScanBehaviorType.FULL_SCAN);
        scanProcessDefinition.setBehavior(scanProcessBehavior);
        ScanProcessConfig scanProcessConfig = new ScanProcessConfig();
        scanProcessConfig.setRootFolderPath("rf-path-" + UUID.randomUUID().toString());
        scanProcessDefinition.setConfig(scanProcessConfig);
        return scanProcessDefinition;
    }


    @RequestMapping("/stop")
    public void stop() {
        logger.warn("GOT STOP SIGNAL VIA HTTP REQUEST. STOPPING THE SYSTEM !!!");
        System.exit(0);
    }


    private static boolean profilesContains(final String profile) {
        return Arrays.asList(context.getEnvironment().getActiveProfiles()).contains(profile);
    }

    private static class StdinExitListener extends Thread {

        public void run() {
            int c;
            try {
                c = System.in.read();
                while (c != -1) {
                    System.out.print(c);
                }
            } catch (IOException e) {
                logger.error(e.getMessage(), e);
            }
            logger.error("Parent process is no longer alive. Killing current process");
            System.exit(0);
        }
    }

    @NotNull
    @Bean
    ServletListenerRegistrationBean<ServletContextListener> appServletListener() {
        ServletListenerRegistrationBean<ServletContextListener> srb =
                new ServletListenerRegistrationBean<>();
        srb.setListener(new AppServletContextListener());
        return srb;
    }

    public class AppServletContextListener implements ServletContextListener {
        @Override
        public void contextInitialized(ServletContextEvent sce) {
            // Context Initialised
            logger.info("application context initialized");
        }

        @Override
        public void contextDestroyed(ServletContextEvent sce) {
            // Here - what you want to do that context shutdown
            logger.info("application context destroyed - exit app");
            //System.exit(0);
        }
    }
}
