package com.cla.filecluster.domain.entity.security;

import static java.util.stream.Collectors.toSet;

import java.util.Set;
import java.util.stream.Stream;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import com.cla.common.domain.dto.security.Authority;
import com.cla.connector.domain.dto.JsonSerislizableVisible;
import com.google.common.collect.Sets;

@Entity
@Table(	name = "roles", 
indexes = { 
		@Index(unique=true, name = "role_idx", columnList = "name")
})

public class Role implements JsonSerislizableVisible {
	@Id
	@GeneratedValue
	private Integer id;	
	
	@NotNull
	private String name;

    private String displayName;

	private String description;
	

	@ElementCollection
	@CollectionTable(uniqueConstraints = @UniqueConstraint(columnNames = {"role_id", "authorities"}))
	private Set<Authority> authorities = Sets.newHashSet();
	
	public Role() {
	}

    public Role(final String name, final String description, final Authority... authorities) {
        this(name, name, description, authorities);
    }

    public Role(final String name, final String displayName, final String description, final Authority... authorities) {
        this.name = name;
        this.displayName = displayName;
        this.description = description;
        this.authorities = Stream.of(authorities).collect(toSet());
    }

    public String getDescription() {
		return description;
	}



	public void setDescription(final String description) {
		this.description = description;
	}

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}


	public Set<Authority> getAuthorities() {
		return authorities;
	}

	public void setAuthorities(final Set<Authority> authorities) {
		this.authorities = authorities;
	}

	public Integer getId() {
		return id;
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}



	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Role other = (Role) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}



	
	
	
	
	

}
