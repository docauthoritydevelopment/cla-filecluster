package com.cla.filecluster.domain.entity.filetag;

import com.cla.connector.domain.dto.file.AssociationType;
import com.cla.filecluster.domain.entity.bizlist.SimpleBizListItem;
import com.cla.filecluster.domain.entity.department.Department;
import com.cla.filecluster.domain.entity.doctype.DocType;
import com.cla.filecluster.domain.entity.scope.BasicScope;
import com.cla.filecluster.domain.entity.security.FunctionalRole;

/**
 * Created by: yael
 * Created on: 1/22/2019
 */
public class TagAssociation implements EntityAssociation {

    private long id;

    private String associationObjectId;

    private AssociationType associationType;

    private FileTag fileTag;

    private SimpleBizListItem simpleBizListItem;

    private long associationTimeStamp;

    private boolean dirty;

    private boolean implicit;

    private Long originFolderId;

    private Integer originDepthFromRoot;

    private boolean deleted;

    private DocType docType;

    private FunctionalRole functionalRole;

    private Department department;

    private BasicScope scope;

    public TagAssociation() {
    }

    public TagAssociation(AssociableEntity associableEntity) {
        if (associableEntity instanceof FileTag) {
            setFileTag((FileTag) associableEntity);
        } else if (associableEntity instanceof SimpleBizListItem) {
            setSimpleBizListItem((SimpleBizListItem) associableEntity);
        } else if (associableEntity instanceof FunctionalRole) {
            setFunctionalRole((FunctionalRole) associableEntity);
        } else if (associableEntity instanceof Department) {
            setDepartment((Department)associableEntity);
        } else {
            setDocType((DocType) associableEntity);
        }
        this.associationTimeStamp = System.currentTimeMillis();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getAssociationObjectId() {
        return associationObjectId;
    }

    public void setAssociationObjectId(String associationObjectId) {
        this.associationObjectId = associationObjectId;
    }

    public AssociationType getAssociationType() {
        return associationType;
    }

    public void setAssociationType(AssociationType associationType) {
        this.associationType = associationType;
    }

    public FileTag getFileTag() {
        return fileTag;
    }

    public void setFileTag(FileTag fileTag) {
        this.fileTag = fileTag;
    }

    public SimpleBizListItem getSimpleBizListItem() {
        return simpleBizListItem;
    }

    public void setSimpleBizListItem(SimpleBizListItem simpleBizListItem) {
        this.simpleBizListItem = simpleBizListItem;
    }

    public long getAssociationTimeStamp() {
        return associationTimeStamp;
    }

    public void setAssociationTimeStamp(long associationTimeStamp) {
        this.associationTimeStamp = associationTimeStamp;
    }

    public boolean isDirty() {
        return dirty;
    }

    public void setDirty(boolean dirty) {
        this.dirty = dirty;
    }

    public boolean isImplicit() {
        return implicit;
    }

    public void setImplicit(boolean implicit) {
        this.implicit = implicit;
    }

    public Long getOriginFolderId() {
        return originFolderId;
    }

    public void setOriginFolderId(Long originFolderId) {
        this.originFolderId = originFolderId;
    }

    public Integer getOriginDepthFromRoot() {
        return originDepthFromRoot;
    }

    public void setOriginDepthFromRoot(Integer originDepthFromRoot) {
        this.originDepthFromRoot = originDepthFromRoot;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public DocType getDocType() {
        return docType;
    }

    public void setDocType(DocType docType) {
        this.docType = docType;
    }

    public FunctionalRole getFunctionalRole() {
        return functionalRole;
    }

    public void setFunctionalRole(FunctionalRole functionalRole) {
        this.functionalRole = functionalRole;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public BasicScope getScope() {
        return scope;
    }

    public void setScope(BasicScope scope) {
        this.scope = scope;
    }

    public Long getObjectIdWhereRelevant() {
        if (!associationType.equals(AssociationType.GROUP)) {
            return Long.parseLong(associationObjectId);
        }
        return null;
    }

    @Override
    public AssociableEntity getAssociableEntity() {
        if (getFileTag() != null) {
            return getFileTag();
        } else if (getSimpleBizListItem() != null) {
            return getSimpleBizListItem();
        } else if (getFunctionalRole() != null) {
            return getFunctionalRole();
        } else if (getDepartment() != null) {
            return getDepartment();
        }
        return getDocType();
    }
}
