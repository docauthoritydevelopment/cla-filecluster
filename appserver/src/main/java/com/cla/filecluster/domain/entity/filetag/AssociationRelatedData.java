package com.cla.filecluster.domain.entity.filetag;

import com.cla.common.domain.dto.AssociableEntityDto;
import com.cla.filecluster.domain.dto.AssociationEntity;
import com.cla.filecluster.domain.entity.scope.BasicScope;
import com.cla.filecluster.service.categories.AssociationsService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Holds all association related data for entity/entities
 * Created by: yael
 * Created on: 5/1/2019
 */
public class AssociationRelatedData {
    private List<AssociationEntity> associations; // associations list
    private List<AssociableEntityDto> entities; // related associable entities
    private Map<Long, BasicScope> scopes; // related scopes
    private Map<Long, List<AssociationEntity>> perEntityAssociations; // if needed for list of several entities - map per entity
    private Map<String, List<AssociationEntity>> perGroupAssociations; // if needed for list of several entities - map per entity

    public AssociationRelatedData(List<AssociationEntity> associations,
                                  List<AssociableEntityDto> entities, Map<Long, BasicScope> scopes) {
        this.associations = associations;
        this.entities = entities;
        this.scopes = scopes;
        this.perEntityAssociations = new HashMap<>();
    }

    public AssociationRelatedData(List<AssociationEntity> associations, List<AssociableEntityDto> entities,
                                  Map<Long, BasicScope> scopes, Map<Long, List<AssociationEntity>> perEntityAssociations) {
        this.associations = associations;
        this.entities = entities;
        this.scopes = scopes;
        this.perEntityAssociations = perEntityAssociations;
    }

    public List<AssociationEntity> getAssociations() {
        return associations;
    }

    public void setAssociations(List<AssociationEntity> associations) {
        this.associations = associations;
    }

    public List<AssociableEntityDto> getEntities() {
        return entities;
    }

    public void setEntities(List<AssociableEntityDto> entities) {
        this.entities = entities;
    }

    public Map<Long, BasicScope> getScopes() {
        return scopes;
    }

    public void setScopes(Map<Long, BasicScope> scopes) {
        this.scopes = scopes;
    }

    public Map<Long, List<AssociationEntity>> getPerEntityAssociations() {
        return perEntityAssociations;
    }

    public void setPerEntityAssociations(Map<Long, List<AssociationEntity>> perEntityAssociations) {
        this.perEntityAssociations = perEntityAssociations;
    }

    public Map<String, List<AssociationEntity>> getPerGroupAssociations() {
        return perGroupAssociations;
    }

    public void setPerGroupAssociations(Map<String, List<AssociationEntity>> perGroupAssociations) {
        this.perGroupAssociations = perGroupAssociations;
    }

    public Map<Long, List<AssociationEntity>> getPerEntityDocTypes() {
        Map<Long, List<AssociationEntity>> result = new HashMap<>();
        if (perEntityAssociations != null) {
            perEntityAssociations.forEach((id, associations) -> {
                List<AssociationEntity> docTypes = AssociationsService.getDocTypeAssociation(associations);
                if (docTypes != null && !docTypes.isEmpty()) {
                    result.put(id, docTypes);
                }
            });
        }
        return result;
    }

    public Map<String, List<AssociationEntity>> getPerGroupDocTypes() {
        Map<String, List<AssociationEntity>> result = new HashMap<>();
        if (perGroupAssociations != null) {
            perGroupAssociations.forEach((id, associations) -> {
                List<AssociationEntity> docTypes = AssociationsService.getDocTypeAssociation(associations);
                if (docTypes != null && !docTypes.isEmpty()) {
                    result.put(id, docTypes);
                }
            });
        }
        return result;
    }
}
