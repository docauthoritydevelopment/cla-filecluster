package com.cla.filecluster.domain.entity.categorization;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("GCF")
public class GroupCategoryFilter extends CategoryFilter {

    public GroupCategoryFilter() {
    }

    public GroupCategoryFilter(String groupId) {
        setValue(groupId);
    }


}
