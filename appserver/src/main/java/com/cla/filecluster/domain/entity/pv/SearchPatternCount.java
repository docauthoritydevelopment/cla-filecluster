package com.cla.filecluster.domain.entity.pv;

import org.apache.solr.client.solrj.beans.Field;
import com.fasterxml.jackson.annotation.JsonInclude;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.util.List;

@Embeddable
public class SearchPatternCount {

    @Column(name = "patternId", nullable = false)
    @Field
    private int patternId;

    @Field
    private String patternName;

    @Field
    private int patternCount;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<String> matches;

    public void setPatternId(int patternId) {
        this.patternId = patternId;
    }

    public void setPatternName(String patternName) {
        this.patternName = patternName;
    }

    public void setPatternCount(int patternCount) {
        this.patternCount = patternCount;
    }

    public SearchPatternCount() {
	}

	public SearchPatternCount(final int patternId, final String patternName, final int patternCount, List<String> matches) {
		this.patternId = patternId;
		this.patternName = patternName;
		this.patternCount = patternCount;
		this.matches = matches;
	}

	public int getPatternId() {
		return patternId;
	}

	public int getPatternCount() {
		return patternCount;
	}

	public String getPatternName() {
		return patternName;
	}

    public List<String> getMatches() {
        return matches;
    }

    public void setMatches(List<String> matches) {
        this.matches = matches;
    }

    @Override
    public String toString() {
        return "SearchPatternCount{" +
                "patternId=" + patternId +
                ", patternName='" + patternName + '\'' +
                ", patternCount=" + patternCount +
                ", matches=" + matches +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SearchPatternCount that = (SearchPatternCount) o;

        return patternId == that.patternId;

    }

    @Override
    public int hashCode() {
        return patternId;
    }
}
