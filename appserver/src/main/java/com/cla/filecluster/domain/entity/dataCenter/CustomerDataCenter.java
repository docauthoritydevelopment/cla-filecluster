
package com.cla.filecluster.domain.entity.dataCenter;


import javax.persistence.*;
import java.util.Date;

/**
 * Created by liora on 28/05/2017.
 */
@Entity
@Table(name = "customer_data_center",
         uniqueConstraints = {
                 @UniqueConstraint(name = "name_constraint",columnNames = "name")}
)
public class CustomerDataCenter {
    @Id
    @Access(AccessType.PROPERTY)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(nullable = false)
    private String name;  // Ordinal within the type

    private String location;
    private String description;

    @Column(name = "is_default")
    private boolean isDefault;

    @Column(columnDefinition = "datetime(3)")
    private Date createdOnDB;    // Time when was added to the system

    @Column(length = 1024, name = "public_key")
    private String publicKey;

    @Column(name = "ingest_tasks_queue_limit")
    private Integer ingestTasksQueueLimit;

    @Column(name = "ingest_task_retry_time") // in seconds
    private Integer ingestTaskRetryTime;

    @Column(name = "ingest_task_expire_time") // in seconds
    private Integer ingestTaskExpireTime;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreatedOnDB() {
        return createdOnDB;
    }

    public boolean isDefault() {
        return isDefault;
    }

    public void setDefault(boolean aDefault) {
        isDefault = aDefault;
    }

    public void setCreatedOnDB(Date createdOnDB) {
        this.createdOnDB = createdOnDB;
    }

    public String getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }

    @PrePersist
    public void setCreatedOnDB() {
        this.createdOnDB = new Date();
    }

    public Integer getIngestTasksQueueLimit() {
        return ingestTasksQueueLimit;
    }

    public void setIngestTasksQueueLimit(Integer ingestTasksQueueLimit) {
        this.ingestTasksQueueLimit = ingestTasksQueueLimit;
    }

    public Integer getIngestTaskRetryTime() {
        return ingestTaskRetryTime;
    }

    public void setIngestTaskRetryTime(Integer ingestTaskRetryTime) {
        this.ingestTaskRetryTime = ingestTaskRetryTime;
    }

    public Integer getIngestTaskExpireTime() {
        return ingestTaskExpireTime;
    }

    public void setIngestTaskExpireTime(Integer ingestTaskExpireTime) {
        this.ingestTaskExpireTime = ingestTaskExpireTime;
    }

    @Override
    public String toString() {
        return "CustomerDataCenter{" +
                "id=" + id +
                ", name=" + name +
                ", location='" + location + '\'' +
                ", createdOnDB='" + createdOnDB + '\'' +
                ", location='" + location + '\'' +
                ", isDefault='" + isDefault + '\'' +
                ", publicKey='" + publicKey + '\'' +
                '}';
    }

}
