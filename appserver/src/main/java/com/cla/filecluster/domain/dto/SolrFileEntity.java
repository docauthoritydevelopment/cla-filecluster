package com.cla.filecluster.domain.dto;

import com.cla.common.domain.dto.file.ClaFileState;
import com.cla.common.domain.dto.file.FileIngestionState;
import com.cla.connector.domain.dto.JsonSerislizableVisible;
import com.cla.filecluster.domain.entity.pv.SearchPatternCount;
import org.apache.solr.client.solrj.beans.Field;

import javax.persistence.Transient;
import java.util.Date;
import java.util.List;

@SuppressWarnings("unused")
public class SolrFileEntity implements JsonSerislizableVisible, SolrEntity {

    @Field
    private String id;

    @Field
    private long fileId;

	@Field
	private List<String> parentFolderIds;

    @Field
    private Long folderId; // doc folder ID

    @Field
    private String fullPath;

    @Field
    private String baseName;

	@Field
	private String sortName;

    @Field
    private String fullName;

	@Field
	private String fullNameSearch;

    @Field
    private Long rootFolderId;

    @Field
    private String extension;

    @Field("size")
    private Long sizeOnMedia;

    @Field
    private int type; // FileType

    @Field
    private String mime;

    @Field("processing_state")
    private String state = ClaFileState.INGESTED.name();

    @Field("last_record_update")
    private Date lastRecordUpdate = new Date();

    @Field
    private String author;
    @Field
    private String company;
    @Field
    private String subject;
    @Field
    private String title;
    @Field
    private List<String> keywords;
    @Field
    private List<String> keywordsOriginal;
    @Field
    private String generatingApp;

    @Field
    private String contentId;

    @Field("appCreationDate")
    private Date appCreationDate;

    @Field("item_count_1")
    private Integer itemsCountL1;
    @Field("item_count_2")
    private Integer itemsCountL2;
    @Field("item_count_3")
    private Integer itemsCountL3;

    @Field
    private String analysisHint;

	@Transient
    private List<SearchPatternCount> searchPatternsCounting;

    @Field
	private List<String> extractedEntitiesIds;

	@Field
	private List<String> extractedTextIds;

	@Field
	private List<String> extractedPatternsIds;

	@Field
	private String owner;

	@Field("owner_f")
	private String ownerFull;

	@Field("acl_read")
	private List<String> aclRead;

	@Field("acl_write")
	private List<String> aclWrite;

	@Field("acl_denied_read")
	private List<String> aclDenyRead;

	@Field("acl_denied_write")
	private List<String> aclDenyWrite;

	@Field("acl_read_f")
	private List<String> aclReadFull;

	@Field("acl_write_f")
	private List<String> aclWriteFull;

	@Field("acl_denied_read_f")
	private List<String> aclDenyReadFull;

	@Field("acl_denied_write_f")
	private List<String> aclDenyWriteFull;

	@Field("search_acl_read")
	private List<String> searchAclRead;

	@Field("search_acl_denied_read")
	private List<String> searchAclDenyRead;

	@Field
	private String aclSignature;

	@Field
	private String aclInheritanceType;

	@Field("groupId")
	private String analysisGroupId;

	@Field("userGroupId")
	private String userGroupId;

	@Field
	private String groupName;

	@Field
	private String fileNameHashed;

	@Field
	private Long packageTopFileId;

	@Field
	private Date creationDate;
	@Field
	private Date modificationDate;
	@Field
	private Date accessDate;
	@Field
	private Date deletionDate;        // Latest time when record was marked as deleted
	@Field
	private boolean deleted = false;
	@Field
	private Date initialScanDate;    // Time when was added to the system or un-deleted as it is found again as an active file

	@Field
	private String mediaEntityId;    // optional unique id provided by media repository

	@Field
	private FileIngestionState ingestionState;

	@Field
	private List<String> tags;

	@Field
	private List<String> tags2;

	@Field
	private List<String> scopedTags;

	@Field
	private List<Integer> matchedPatternsSingle;

	@Field
	private List<Integer> matchedPatternsMulti;

	@Field
	private List<String> matchedPatternsCounts;

	@Field
	private List<String> docTypes;

	@Field
	private Date createdOnDB;

	@Field
	private Date updatedOnDB;

	@Field("group_categories")
	private List<String> groupCategories;

	@Field("account_categories")
	private List<String> accountCategories;

	@Field("user_categories")
	private List<String> userCategories;

	@Field
	private Integer docStoreId;

	@Field
	private String userContentSignature;

	@Field
	private Long lastMetadataChangeDate;

	@Field
	private Long contentChangeDate;

	@Field
	private Long ingestDate;

	@Field
	private Long analysisDate;

	@Field
	private Long lastAssociationChangeDate;

	@Field
	private Integer ingestionErrorType;

	@Field
	private Integer encryption;

	@Field
	private int mediaType;

	@Field
	private Long wstId;

	@Field
	private List<String> workflowData;

	@Field
	private String senderName;

	@Field
	private String senderAddress;

	@Field
	private String senderFullAddress;

	@Field
	private String senderDomain;

	@Field
	private List<String> recipientsNames;

	@Field
	private List<String> recipientsAddresses;

	@Field
	private List<String> recipientsFullAddresses;

	@Field
	private List<String> recipientsDomains;

	@Field
	private Date sentDate;

	@Field
	private Integer itemType;

	@Field
	private Integer numOfAttachments;

	@Field
	private List<String> containerIds;

	@Field
	private String mailboxUpn;

	@Field
	private String itemSubject;

	@Field
	private List<String> associations;

	@Field
	private String department;

	@Field
	private Long currentRunId;

	@Field
	private String taskState;

	@Field
	private Long taskStateDate;

	@Field
	private Integer retries;

	@Field
	private String taskParameters;

	@Field
	private List<String> dataRoles;

	@Field
	private List<String> bizListItems;

	@Field
	private Long labelProcessDate;

	//region labeling and metadata related

	/**
	 * Used for indicating that the system has recognized potential label on a file. Format:
	 * <action_type>.<origin>.<id>.<native-id>.<number-of-attempted-retries>
	 * i.e.
	 * labelsExpected.mip.123.666-aaa-ccc-444.12
	 * */
	@Field
	private List<String> actionsExpected;

	/**
	 * IDs of all the DA Labels found on the file, where ID is the same as in MySQL
	 * */
	@Field
	private List<Long> daLabels;

	/**
	 * Metadata which resides beside the file, like MIP, or BOX`s metadata  property
	 * */
	@Field
	private List<String> externalMetadata;

	@Field
	private List<String> generalMetadata;

	@Field
	private List<String> actionConfirmation;

	/**
	 * serialized json key-value
	 * */
	@Field
	private String rawMetadata;

	@Field
	private List<String> daMetadata;

	@Field
	private List<String> daMetadataType;

    @Field
    private List<String> activeAssociations;

    @Field
	private Long updateDate;

	@Field
	private String folderName;

	//endregion

	public String getIdAsString() {return id;}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public long getFileId() {
		return fileId;
	}

	public void setFileId(long fileId) {
		this.fileId = fileId;
	}

	public List<String> getParentFolderIds() {
		return parentFolderIds;
	}

	public void setParentFolderIds(List<String> parentFolderIds) {
		this.parentFolderIds = parentFolderIds;
	}

	public Long getFolderId() {
		return folderId;
	}

	public void setFolderId(long folderId) {
		this.folderId = folderId;
	}

	public String getFullPath() {
		return fullPath;
	}

	public void setFullPath(String fullPath) {
		this.fullPath = fullPath;
	}

	public String getBaseName() {
		return baseName;
	}

	public void setBaseName(String baseName) {
		this.baseName = baseName;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public Long getRootFolderId() {
		return rootFolderId;
	}

	public void setRootFolderId(Long rootFolderId) {
		this.rootFolderId = rootFolderId;
	}

	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	public Long getSizeOnMedia() {
		return sizeOnMedia;
	}

	public void setSizeOnMedia(Long sizeOnMedia) {
		this.sizeOnMedia = sizeOnMedia;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getMime() {
		return mime;
	}

	public void setMime(String mime) {
		this.mime = mime;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Date getLastRecordUpdate() {
		return lastRecordUpdate;
	}

	public void setLastRecordUpdate(Date lastRecordUpdate) {
		this.lastRecordUpdate = lastRecordUpdate;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<String> getKeywords() {
		return keywords;
	}

	public void setKeywords(List<String> keywords) {
		this.keywords = keywords;
	}

	public List<String> getKeywordsOriginal() {
		return keywordsOriginal;
	}

	public void setKeywordsOriginal(List<String> keywordsOriginal) {
		this.keywordsOriginal = keywordsOriginal;
	}

	public String getGeneratingApp() {
		return generatingApp;
	}

	public void setGeneratingApp(String generatingApp) {
		this.generatingApp = generatingApp;
	}

	public String getContentId() {
		return contentId;
	}

	public void setContentId(String contentId) {
		this.contentId = contentId;
	}

	public Date getAppCreationDate() {
		return appCreationDate;
	}

	public void setAppCreationDate(Date appCreationDate) {
		this.appCreationDate = appCreationDate;
	}

	public Integer getItemsCountL1() {
		return itemsCountL1;
	}

	public void setItemsCountL1(Integer itemsCountL1) {
		this.itemsCountL1 = itemsCountL1;
	}

	public Integer getItemsCountL2() {
		return itemsCountL2;
	}

	public void setItemsCountL2(Integer itemsCountL2) {
		this.itemsCountL2 = itemsCountL2;
	}

	public Integer getItemsCountL3() {
		return itemsCountL3;
	}

	public void setItemsCountL3(Integer itemsCountL3) {
		this.itemsCountL3 = itemsCountL3;
	}

	public String getAnalysisHint() {
		return analysisHint;
	}

	public void setAnalysisHint(String analysisHint) {
		this.analysisHint = analysisHint;
	}

	public List<SearchPatternCount> getSearchPatternsCounting() {
		return searchPatternsCounting;
	}

	public void setSearchPatternsCounting(List<SearchPatternCount> searchPatternsCounting) {
		this.searchPatternsCounting = searchPatternsCounting;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getOwnerFull() {
		return ownerFull;
	}

	public void setOwnerFull(String ownerFull) {
		this.ownerFull = ownerFull;
	}

	public List<String> getAclRead() {
		return aclRead;
	}

	public void setAclRead(List<String> aclRead) {
		this.aclRead = aclRead;
	}

	public List<String> getAclWrite() {
		return aclWrite;
	}

	public void setAclWrite(List<String> aclWrite) {
		this.aclWrite = aclWrite;
	}

	public List<String> getAclDenyRead() {
		return aclDenyRead;
	}

	public void setAclDenyRead(List<String> aclDenyRead) {
		this.aclDenyRead = aclDenyRead;
	}

	public List<String> getAclDenyWrite() {
		return aclDenyWrite;
	}

	public void setAclDenyWrite(List<String> aclDenyWrite) {
		this.aclDenyWrite = aclDenyWrite;
	}

	public List<String> getAclReadFull() {
		return aclReadFull;
	}

	public void setAclReadFull(List<String> aclReadFull) {
		this.aclReadFull = aclReadFull;
	}

	public List<String> getAclWriteFull() {
		return aclWriteFull;
	}

	public void setAclWriteFull(List<String> aclWriteFull) {
		this.aclWriteFull = aclWriteFull;
	}

	public List<String> getAclDenyReadFull() {
		return aclDenyReadFull;
	}

	public void setAclDenyReadFull(List<String> aclDenyReadFull) {
		this.aclDenyReadFull = aclDenyReadFull;
	}

	public List<String> getAclDenyWriteFull() {
		return aclDenyWriteFull;
	}

	public void setAclDenyWriteFull(List<String> aclDenyWriteFull) {
		this.aclDenyWriteFull = aclDenyWriteFull;
	}

	public List<String> getSearchAclRead() {
		return searchAclRead;
	}

	public void setSearchAclRead(List<String> searchAclRead) {
		this.searchAclRead = searchAclRead;
	}

	public List<String> getSearchAclDenyRead() {
		return searchAclDenyRead;
	}

	public void setSearchAclDenyRead(List<String> searchAclDenyRead) {
		this.searchAclDenyRead = searchAclDenyRead;
	}

	public String getAclSignature() {
		return aclSignature;
	}

	public void setAclSignature(String aclSignature) {
		this.aclSignature = aclSignature;
	}

	public String getAclInheritanceType() {
		return aclInheritanceType;
	}

	public void setAclInheritanceType(String aclInheritanceType) {
		this.aclInheritanceType = aclInheritanceType;
	}

	public String getAnalysisGroupId() {
		return analysisGroupId;
	}

	public void setAnalysisGroupId(String analysisGroupId) {
		this.analysisGroupId = analysisGroupId;
	}

	public String getUserGroupId() {
		return userGroupId;
	}

	public void setUserGroupId(String userGroupId) {
		this.userGroupId = userGroupId;
	}

	public String getFileNameHashed() {
		return fileNameHashed;
	}

	public void setFileNameHashed(String fileNameHashed) {
		this.fileNameHashed = fileNameHashed;
	}

	public Long getPackageTopFileId() {
		return packageTopFileId;
	}

	public void setPackageTopFileId(Long packageTopFileId) {
		this.packageTopFileId = packageTopFileId;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getModificationDate() {
		return modificationDate;
	}

	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}

	public Date getAccessDate() {
		return accessDate;
	}

	public void setAccessDate(Date accessDate) {
		this.accessDate = accessDate;
	}

	public Date getDeletionDate() {
		return deletionDate;
	}

	public void setDeletionDate(Date deletionDate) {
		this.deletionDate = deletionDate;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public Date getInitialScanDate() {
		return initialScanDate;
	}

	public void setInitialScanDate(Date initialScanDate) {
		this.initialScanDate = initialScanDate;
	}

	public String getMediaEntityId() {
		return mediaEntityId;
	}

	public void setMediaEntityId(String mediaEntityId) {
		this.mediaEntityId = mediaEntityId;
	}

	public FileIngestionState getIngestionState() {
		return ingestionState;
	}

	public void setIngestionState(FileIngestionState ingestionState) {
		this.ingestionState = ingestionState;
	}

	public List<String> getTags() {
		return tags;
	}

	public void setTags(List<String> tags) {
		this.tags = tags;
	}

	public List<String> getTags2() {
		return tags2;
	}

	public void setTags2(List<String> tags2) {
		this.tags2 = tags2;
	}

	public List<String> getMatchedPatternsCounts() {
		return matchedPatternsCounts;
	}

	public void setMatchedPatternsCounts(List<String> matchedPatternsCounts) {
		this.matchedPatternsCounts = matchedPatternsCounts;
	}

	public List<String> getDocTypes() {
		return docTypes;
	}

	public void setDocTypes(List<String> docTypes) {
		this.docTypes = docTypes;
	}

	public Date getCreatedOnDB() {
		return createdOnDB;
	}

	public void setCreatedOnDB(Date createdOnDB) {
		this.createdOnDB = createdOnDB;
	}

	public Date getUpdatedOnDB() {
		return updatedOnDB;
	}

	public void setUpdatedOnDB(Date updatedOnDB) {
		this.updatedOnDB = updatedOnDB;
	}

	public List<String> getGroupCategories() {
		return groupCategories;
	}

	public void setGroupCategories(List<String> groupCategories) {
		this.groupCategories = groupCategories;
	}

	public List<String> getExtractedEntitiesIds() {
		return extractedEntitiesIds;
	}

	public void setExtractedEntitiesIds(List<String> extractedEntitiesIds) {
		this.extractedEntitiesIds = extractedEntitiesIds;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public List<String> getExtractedTextIds() {
		return extractedTextIds;
	}

	public void setExtractedTextIds(List<String> extractedTextIds) {
		this.extractedTextIds = extractedTextIds;
	}

	public List<String> getAccountCategories() {
		return accountCategories;
	}

	public void setAccountCategories(List<String> accountCategories) {
		this.accountCategories = accountCategories;
	}

	public List<String> getUserCategories() {
		return userCategories;
	}

	public void setUserCategories(List<String> userCategories) {
		this.userCategories = userCategories;
	}

	public Integer getDocStoreId() {
		return docStoreId;
	}

	public void setDocStoreId(Integer docStoreId) {
		this.docStoreId = docStoreId;
	}

	public String getUserContentSignature() {
		return userContentSignature;
	}

	public void setUserContentSignature(String userContentSignature) {
		this.userContentSignature = userContentSignature;
	}

	public Long getLastMetadataChangeDate() {
		return lastMetadataChangeDate;
	}

	public void setLastMetadataChangeDate(Long lastMetadataChangeDate) {
		this.lastMetadataChangeDate = lastMetadataChangeDate;
	}

	public List<String> getWorkflowData() {
		return workflowData;
	}

	public void setWorkflowData(List<String> workflowData) {
		this.workflowData = workflowData;
	}

	public String getSenderName() {
		return senderName;
	}

	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}

	public String getSenderAddress() {
		return senderAddress;
	}

	public void setSenderAddress(String senderAddress) {
		this.senderAddress = senderAddress;
	}

	public String getSenderDomain() {
		return senderDomain;
	}

	public void setSenderDomain(String senderDomain) {
		this.senderDomain = senderDomain;
	}

	public List<String> getRecipientsNames() {
		return recipientsNames;
	}

	public void setRecipientsNames(List<String> recipientsNames) {
		this.recipientsNames = recipientsNames;
	}

	public List<String> getRecipientsAddresses() {
		return recipientsAddresses;
	}

	public void setRecipientsAddresses(List<String> recipientsAddresses) {
		this.recipientsAddresses = recipientsAddresses;
	}

	public List<String> getRecipientsDomains() {
		return recipientsDomains;
	}

	public void setRecipientsDomains(List<String> recipientsDomains) {
		this.recipientsDomains = recipientsDomains;
	}

	public String getSenderFullAddress() {
		return senderFullAddress;
	}

	public void setSenderFullAddress(String senderFullAddress) {
		this.senderFullAddress = senderFullAddress;
	}

	public List<String> getRecipientsFullAddresses() {
		return recipientsFullAddresses;
	}

	public void setRecipientsFullAddresses(List<String> recipientsFullAddresses) {
		this.recipientsFullAddresses = recipientsFullAddresses;
	}

	public Date getSentDate() {
		return sentDate;
	}

	public void setSentDate(Date sentDate) {
		this.sentDate = sentDate;
	}

	public List<String> getScopedTags() {
		return scopedTags;
	}

	public void setScopedTags(List<String> scopedTags) {
		this.scopedTags = scopedTags;
	}

	public Long getContentChangeDate() {
		return contentChangeDate;
	}

	public void setContentChangeDate(Long contentChangeDate) {
		this.contentChangeDate = contentChangeDate;
	}

	public Long getIngestDate() {
		return ingestDate;
	}

	public void setIngestDate(Long ingestDate) {
		this.ingestDate = ingestDate;
	}

	public Long getAnalysisDate() {
		return analysisDate;
	}

	public void setAnalysisDate(Long analysisDate) {
		this.analysisDate = analysisDate;
	}

	public Long getLastAssociationChangeDate() {
		return lastAssociationChangeDate;
	}

	public void setLastAssociationChangeDate(Long lastAssociationChangeDate) {
		this.lastAssociationChangeDate = lastAssociationChangeDate;
	}

	public Integer getIngestionErrorType() {
		return ingestionErrorType;
	}

	public void setIngestionErrorType(Integer ingestionErrorType) {
		this.ingestionErrorType = ingestionErrorType;
	}

	public Integer getEncryption() {
		return encryption;
	}

	public void setEncryption(Integer encryption) {
		this.encryption = encryption;
	}

	public int getMediaType() {
		return mediaType;
	}

	public void setMediaType(int mediaType) {
		this.mediaType = mediaType;
	}

	public List<String> getExtractedPatternsIds() {
		return extractedPatternsIds;
	}

	public void setExtractedPatternsIds(List<String> extractedPatternsIds) {
		this.extractedPatternsIds = extractedPatternsIds;
	}

	public Integer getItemType() {
		return itemType;
	}

	public void setItemType(Integer itemType) {
		this.itemType = itemType;
	}

	public Integer getNumOfAttachments() {
		return numOfAttachments;
	}

	public void setNumOfAttachments(Integer numOfAttachments) {
		this.numOfAttachments = numOfAttachments;
	}

	public List<String> getContainerIds() {
		return containerIds;
	}

	public void setContainerIds(List<String> containerIds) {
		this.containerIds = containerIds;
	}

	public String getMailboxUpn() {
		return mailboxUpn;
	}

	public void setMailboxUpn(String mailboxUpn) {
		this.mailboxUpn = mailboxUpn;
	}

	public String getItemSubject() {
		return itemSubject;
	}

	public void setItemSubject(String itemSubject) {
		this.itemSubject = itemSubject;
	}

	public List<String> getAssociations() {
		return associations;
	}

	public void setAssociations(List<String> associations) {
		this.associations = associations;
	}

	public List<Integer> getMatchedPatternsMulti() {
		return matchedPatternsMulti;
	}

	public void setMatchedPatternsMulti(List<Integer> matchedPatternsMulti) {
		this.matchedPatternsMulti = matchedPatternsMulti;
	}

	public List<Integer> getMatchedPatternsSingle() {
		return matchedPatternsSingle;
	}

	public void setMatchedPatternsSingle(List<Integer> matchedPatternsSingle) {
		this.matchedPatternsSingle = matchedPatternsSingle;
	}

	public String getSortName() {
		return sortName;
	}

	public void setSortName(String sortName) {
		this.sortName = sortName;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public void setFolderId(Long folderId) {
		this.folderId = folderId;
	}

	public Long getCurrentRunId() {
		return currentRunId;
	}

	public void setCurrentRunId(Long currentRunId) {
		this.currentRunId = currentRunId;
	}

	public String getTaskState() {
		return taskState;
	}

	public void setTaskState(String taskState) {
		this.taskState = taskState;
	}

	public Long getTaskStateDate() {
		return taskStateDate;
	}

	public void setTaskStateDate(Long taskStateDate) {
		this.taskStateDate = taskStateDate;
	}

	public Integer getRetries() {
		return retries;
	}

	public void setRetries(Integer retries) {
		this.retries = retries;
	}

	public String getTaskParameters() {
		return taskParameters;
	}

	public void setTaskParameters(String taskParameters) {
		this.taskParameters = taskParameters;
	}

	public List<String> getDataRoles() {
		return dataRoles;
	}

	public void setDataRoles(List<String> dataRoles) {
		this.dataRoles = dataRoles;
	}

	public List<String> getBizListItems() {
		return bizListItems;
	}

	public void setBizListItems(List<String> bizListItems) {
		this.bizListItems = bizListItems;
	}

	public List<String> getActionsExpected() {
		return actionsExpected;
	}

	public SolrFileEntity setActionsExpected(List<String> actionsExpected) {
		this.actionsExpected = actionsExpected;
		return this;
	}

	public List<Long> getDaLabels() {
		return daLabels;
	}

	public SolrFileEntity setDaLabels(List<Long> daLabels) {
		this.daLabels = daLabels;
		return this;
	}

	public List<String> getDaMetadata() {
		return daMetadata;
	}

	public void setDaMetadata(List<String> daMetadata) {
		this.daMetadata = daMetadata;
	}

	public List<String> getDaMetadataType() {
		return daMetadataType;
	}

	public void setDaMetadataType(List<String> daMetadataType) {
		this.daMetadataType = daMetadataType;
	}

	public List<String> getExternalMetadata() {
		return externalMetadata;
	}

	public SolrFileEntity setExternalMetadata(List<String> externalMetadata) {
		this.externalMetadata = externalMetadata;
		return this;
	}

	public String getRawMetadata() {
		return rawMetadata;
	}

	public SolrFileEntity setRawMetadata(String rawMetadata) {
		this.rawMetadata = rawMetadata;
		return this;
	}

	public Long getLabelProcessDate() {
		return labelProcessDate;
	}

	public SolrFileEntity setLabelProcessDate(Long labelProcessDate) {
		this.labelProcessDate = labelProcessDate;
		return this;
	}

	public List<String> getGeneralMetadata() {
		return generalMetadata;
	}

	public SolrFileEntity setGeneralMetadata(List<String> generalMetadata) {
		this.generalMetadata = generalMetadata;
		return this;
	}

	public List<String> getActionConfirmation() {
		return actionConfirmation;
	}

	public SolrFileEntity setActionConfirmation(List<String> actionConfirmation) {
		this.actionConfirmation = actionConfirmation;
		return this;
	}

	public Long getWstId() {
		return wstId;
	}

	public void setWstId(Long wstId) {
		this.wstId = wstId;
	}

    public List<String> getActiveAssociations() {
        return activeAssociations;
    }

    public void setActiveAssociations(List<String> activeAssociations) {
        this.activeAssociations = activeAssociations;
    }

	public String getFullNameSearch() {
		return fullNameSearch;
	}

	public void setFullNameSearch(String fullNameSearch) {
		this.fullNameSearch = fullNameSearch;
	}

	public Long getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Long updateDate) {
		this.updateDate = updateDate;
	}

	public String getFolderName() {
		return folderName;
	}

	public void setFolderName(String folderName) {
		this.folderName = folderName;
	}

	@Override
	public String toString() {
		return "SolrFileEntity{" +
				"id='" + id + '\'' +
				", fileId=" + fileId +
				", folderId=" + folderId +
				", fullPath='" + fullPath + '\'' +
				", baseName='" + baseName + '\'' +
				", rootFolderId=" + rootFolderId +
				", extension='" + extension + '\'' +
				", sizeOnMedia=" + sizeOnMedia +
				", type=" + type +
				", state='" + state + '\'' +
				'}';
	}
}
