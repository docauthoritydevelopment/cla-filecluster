package com.cla.filecluster.domain.entity.pv;

import com.cla.connector.domain.dto.error.ProcessingErrorType;
import com.cla.connector.domain.dto.media.MediaType;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by uri on 04/11/2015.
 */
@Entity
@Table(name = "err_scanned_files",
        indexes = {
                @Index(name = "run_id_idx", columnList = "runId"),
        })
public class ScanError {

    static final int  MAX_TEXT_LENGTH = 4096;

    //    @GenericGenerator(name="err-scan-table-hilo-generator", strategy="org.hibernate.id.MultipleHiLoPerTableGenerator",
//            parameters={
//                    @org.hibernate.annotations.Parameter(name = MultipleHiLoPerTableGenerator.ID_TABLE, value="da_main_hilo_autogen"),
//                    @org.hibernate.annotations.Parameter(name = MultipleHiLoPerTableGenerator.MAX_LO, value = "200"),
//            })
    @TableGenerator(name = "err-scan_table_hilo_generator", allocationSize = 200)
    @GeneratedValue(generator="err-scan_table_hilo_generator", strategy = GenerationType.TABLE)
//    @GeneratedValue
    @Id
    @Access(AccessType.PROPERTY)
    private Long id;

    @Column(length = MAX_TEXT_LENGTH)
    private String text;

    @Column(length = MAX_TEXT_LENGTH)
    private String exceptionText;

    @Column(name = "doc_folder_id")
    private Long docFolderId;

    private ProcessingErrorType type;

    @Length(max = MAX_TEXT_LENGTH)
    private String path;

    private Long runId;

    @Column(name = "root_folder_id")
    private Long rootFolderId;

    private Long dateCreated;

    @Column(columnDefinition = "datetime(3)", updatable=false)
	private Date createdOnDB;

    private MediaType mediaType;

	@PrePersist
	private void setCreatedOnDB(){
		createdOnDB = new Date();
	}

    public ScanError() {
    }

    public ScanError(String text, String exceptionText, Long docFolderId,
                     String path, Long runId, Long rootFolderId, MediaType mediaType) {
        setText(text);
        setExceptionText(exceptionText);
        setPath(path);
        this.docFolderId = docFolderId;
        this.runId = runId;
        this.rootFolderId = rootFolderId;
        this.dateCreated = System.currentTimeMillis();
        this.mediaType = mediaType;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getExceptionText() {
        return exceptionText;
    }

    public String getPath() {
        return path;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = StringUtils.substring(text, 0, MAX_TEXT_LENGTH);
    }

    public Long getDocFolder() {
        return docFolderId;
    }

    public void setDocFolder(Long docFolderId) {
        this.docFolderId = docFolderId;
    }

    public ProcessingErrorType getType() {
        return type;
    }

    public void setType(ProcessingErrorType type) {
        this.type = type;
    }

    public Date getCreatedOnDB() {
        return createdOnDB;
    }

    public void setCreatedOnDB(Date createdOnDB) {
        this.createdOnDB = createdOnDB;
    }

    public void setExceptionText(String exceptionText) {
        this.exceptionText = StringUtils.substring(exceptionText, 0, MAX_TEXT_LENGTH);
    }

    public void setPath(String path) {
        this.path = StringUtils.substring(path, 0, MAX_TEXT_LENGTH);;
    }

    public Long getRunId() {
        return runId;
    }

    public void setRunId(Long runId) {
        this.runId = runId;
    }

    public Long getDocFolderId() {
        return docFolderId;
    }

    public void setDocFolderId(Long docFolderId) {
        this.docFolderId = docFolderId;
    }

    public Long getRootFolderId() {
        return rootFolderId;
    }

    public void setRootFolderId(Long rootFolderId) {
        this.rootFolderId = rootFolderId;
    }

    public Long getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Long dateCreated) {
        this.dateCreated = dateCreated;
    }

    public MediaType getMediaType() {
        return mediaType;
    }

    public void setMediaType(MediaType mediaType) {
        this.mediaType = mediaType;
    }
}
