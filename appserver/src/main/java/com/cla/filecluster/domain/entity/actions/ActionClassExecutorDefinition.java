package com.cla.filecluster.domain.entity.actions;

import com.cla.common.domain.dto.action.ActionClass;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by shtand on 15/02/2016.
 */
@Entity
@Table(name = "action_class_executor_def")
public class ActionClassExecutorDefinition {

    @Id
    @Access(AccessType.PROPERTY)
    private String id;

    @NotNull
    private String name;

    private String description;

    @NotNull
    private ActionClass actionClass;

    @NotNull
    private String javaClassName;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ActionClass getActionClass() {
        return actionClass;
    }

    public void setActionClass(ActionClass actionClass) {
        this.actionClass = actionClass;
    }

    public String getJavaClassName() {
        return javaClassName;
    }

    public void setJavaClassName(String javaClassName) {
        this.javaClassName = javaClassName;
    }
}
