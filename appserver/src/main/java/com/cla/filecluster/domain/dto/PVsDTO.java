package com.cla.filecluster.domain.dto;

import org.apache.solr.client.solrj.beans.Field;

public class PVsDTO {
	@Field
	private String id;
	@Field
	private String fileName;


	@Field
	private long fileId;
	
	@Field
	private String part;	// "" or null for Word, sheet name for Excel
	
	@Field
	private float score;
	

	public PVsDTO() {
	}

	
	public PVsDTO(final String fileName, final long fileId, final String part) {
		this.fileName = fileName;
		this.fileId = fileId;
		this.part = part;
		id=part+"@"+fileId;
	}


	public String getId() {
		return id;
	}

	public String getFileName() {
		return fileName;
	}

	public long getFileId() {
		return fileId;
	}

	public String getPart() {
		return part;
	}


	public float getScore() {
		return score;
	}

	
	
	

	

}
