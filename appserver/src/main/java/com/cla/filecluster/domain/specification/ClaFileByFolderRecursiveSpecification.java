package com.cla.filecluster.domain.specification;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.constants.CatFoldersFieldType;
import com.cla.common.domain.dto.kendo.FilterDescriptor;
import com.cla.filecluster.domain.entity.pv.ClaFile;
import com.cla.filecluster.repository.solr.query.Criteria;
import com.cla.filecluster.repository.solr.query.Query;
import com.cla.filecluster.repository.solr.query.SolrOperator;
import com.cla.filecluster.repository.solr.query.SolrQueryGenerator;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.FilenameUtils;

import javax.persistence.criteria.*;
import java.util.List;

/**
 * Created by uri on 09/07/2015.
 */
public class ClaFileByFolderRecursiveSpecification extends ClaFileByFolderSpecification {

    private final String docFolderPath;
    private List<Long> folderIds;
    private long rootFolderId = 0;

    public ClaFileByFolderRecursiveSpecification(FilterDescriptor filter, long docFolderId, String docFolderPath, Long rootFolderId) {
        super(filter,docFolderId);
        this.docFolderPath = docFolderPath;
        if (rootFolderId != null) {
            this.rootFolderId = rootFolderId;
        }
    }

    public Query toQuery() {
        Query base = Query.create();

        if (!docFolderPath.equals("/")) {
            String pattern = "\"" + docFolderPath + SolrQueryGenerator.ALL + "\"";
            Query pathFilter = Query.create().addFilterWithCriteria(Criteria.create(CatFileFieldType.FULLPATH, SolrOperator.EQ, pattern));
            base.addFilterQuery(pathFilter);
        }

        if (folderIds != null && !folderIds.isEmpty()) {
            Query foldersFilter = Query.create().addFilterWithCriteria(Criteria.create(CatFileFieldType.FOLDER_ID, SolrOperator.IN, folderIds));
            base.addFilterQuery(foldersFilter);
        }
        if (rootFolderId > 0) {
            Query rfFilter = Query.create().addFilterWithCriteria(Criteria.create(CatFileFieldType.ROOT_FOLDER_ID, SolrOperator.EQ, rootFolderId));
            base.addFilterQuery(rfFilter);
        }

        if (filter == null || CollectionUtils.isEmpty(filter.getFilters())) {
            //Return only the group filter
            return base;
        }
        else {
            Query filterPredicate = buildFilter(this.filter);
            base.addFilterQuery(filterPredicate); // TODO support AND ???
            return base;
        }
    }

    @Override
    public Predicate toPredicate(Root<ClaFile> root, CriteriaQuery<?> query, CriteriaBuilder cb) {

        //This part allow to use this specification in pageable queries
        Class clazz = query.getResultType();
        if (clazz.equals(ClaFile.class)) {
            //Prevent N+1 problem. see http://jdpgrailsdev.github.io/blog/2014/09/09/spring_data_hibernate_join.html
            root.fetch("analysisGroup", JoinType.LEFT);
            root.fetch("userGroup", JoinType.LEFT);
            root.fetch("contentMetadata",JoinType.LEFT);
        }

        //Where claFile.docFolder.path like docFolderDto.path
        Path<String> fullPathColumn= root.get("fullPath");
        String pathToSearch = FilenameUtils.separatorsToWindows(docFolderPath);
        pathToSearch = pathToSearch.replace("\\","\\\\\\\\");
        Predicate pathFilter = cb.like(fullPathColumn, pathToSearch + "%");
        Predicate baseFilter = pathFilter;
        if (folderIds != null && !folderIds.isEmpty()) {
            Path<Long> objectPath = root.get("docFolderId");
            CriteriaBuilder.In<Long> folderIdsFilter = cb.in(objectPath);
            folderIdsFilter.in(folderIds);
            baseFilter = cb.and(pathFilter, folderIdsFilter);
        }
        if (filter == null || CollectionUtils.isEmpty(filter.getFilters())) {
            //Return only the group filter
            return baseFilter;
        }
        else {
            Predicate filterPredicate = buildFilter(this.filter, root, query, cb);
            return cb.and(baseFilter,filterPredicate);
        }
    }

    public void setFolderIds(List<Long> folderIds) {
        this.folderIds = folderIds;
    }
}
