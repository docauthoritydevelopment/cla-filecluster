package com.cla.filecluster.domain.entity.filetag;

import com.cla.common.domain.dto.scope.ScopedEntity;
import com.cla.filecluster.domain.entity.scope.BasicScope;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by uri on 29/09/2015.
 */


@Entity
@Table(
        indexes = {
                @Index(name = "name_idx", columnList = "name"),
                @Index(name = "sensitive_tag_idx", columnList = "sensitiveTag"),
                @Index(name = "single_value_tag_idx", columnList = "single_value_tag")
        }
)
public class FileTagType implements Serializable, ScopedEntity {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Access(AccessType.PROPERTY)
    @Id
    private long id;

    private String name;

    private String alternativeName;

    private String description;

    private boolean sensitiveTag;

    private boolean system;

    private boolean hidden;

    @Column(name = "single_value_tag")
    private boolean singleValueTag;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "tag_type_scope_id")
    private BasicScope scope;

    private boolean deleted;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAlternativeName() {
        return alternativeName;
    }

    public void setAlternativeName(String alternativeName) {
        this.alternativeName = alternativeName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isSensitiveTag() {
        return sensitiveTag;
    }

    public void setSensitiveTag(boolean sensitiveTag) {
        this.sensitiveTag = sensitiveTag;
    }

    public boolean isSystem() {
        return system;
    }

    public void setSystem(boolean system) {
        this.system = system;
    }

    public boolean isHidden() {
        return hidden;
    }

    public void setHidden(boolean hidden) {
        this.hidden = hidden;
    }

    public BasicScope getScope() {
        return scope;
    }

    public void setScope(BasicScope scope) {
        this.scope = scope;
    }

    @Override
    public String toString() {
        return "FileTagType{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", sensitiveTag=" + sensitiveTag +
                '}';
    }

    public boolean isSingleValueTag() {
        return singleValueTag;
    }

    public void setSingleValueTag(boolean singleValueTag) {
        this.singleValueTag = singleValueTag;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
}
