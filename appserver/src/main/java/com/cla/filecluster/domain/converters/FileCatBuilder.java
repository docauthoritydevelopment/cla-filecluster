package com.cla.filecluster.domain.converters;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.domain.dto.extraction.SearchPatternCountDto;
import com.cla.common.domain.dto.file.ClaFileState;
import com.cla.common.domain.dto.file.MailEntryPropertiesDto;
import com.cla.common.domain.dto.file.PstEntryPropertiesDto;
import com.cla.common.utils.CsvUtils;
import com.cla.common.utils.EmailUtils;
import com.cla.connector.domain.dto.error.ProcessingErrorType;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.file.EncryptionType;
import com.cla.connector.domain.dto.file.FileType;
import com.cla.connector.domain.dto.file.ItemType;
import com.cla.connector.domain.dto.media.EmailAddress;
import com.cla.connector.domain.dto.media.Exchange365ItemDto;
import com.cla.connector.domain.dto.media.MailPropertiesDto;
import com.cla.connector.domain.dto.media.MediaType;
import com.cla.connector.utils.FileNamingUtils;
import com.cla.connector.utils.FileTypeUtils;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.repository.solr.query.AtomicUpdateBuilder;
import com.cla.filecluster.service.files.ingest.ModelIngestUtils;
import com.cla.filecluster.util.categories.FileCatCompositeId;
import com.cla.filecluster.util.categories.FileCatUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrInputDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

import static com.cla.common.constants.CatFileFieldType.*;
import static com.cla.filecluster.repository.solr.query.SolrOperator.REMOVE;
import static com.cla.filecluster.repository.solr.query.SolrOperator.SET;

public class FileCatBuilder extends SolrEntityBuilder<SolrFileEntity> {

    private final static Logger logger = LoggerFactory.getLogger(FileCatBuilder.class);

    private String id;
    private Long fileId;
    private Long rootFolderId;
    private Long docFolderId;
    private List<String> parentFolderInfo;
    private Long packageEntityId;
    private ClaFilePropertiesDto props;
    private ClaFileState state = ClaFileState.SCANNED;
    private boolean removeOwnerQuotes = true;
    private Boolean deleted = null;
    private Long lastMetadataChangeDate = null;
    private String analysisGroupId;
    private String userGroupId;
    private Long contentId;
    private FileType fileType;
    private Date initialScanDate;
    private String rootFolderPath;
    private Long contentChangeDate;
    private Long ingestDate;
    private Long analysisDate;
    private Long lastAssociationChangeDate;
    private ProcessingErrorType ingestErrType;
    private EncryptionType encryptionType;
    private MediaType mediaType;
    private Long appCreationTimeMilli;
    private Set<SearchPatternCountDto> patterns;
    private List<String> containerIds;
    private String mailboxUpn;
    private Integer numberOfAttachments;
    private String sortName;
    private List<String> extractedEntitiesIds;

    private List<String> aclRead;
    private List<String> aclWrite;
    private List<String> aclDenyRead;
    private List<String> aclDenyWrite;
    private List<String> aclReadFull;
    private List<String> aclWriteFull;
    private List<String> aclDenyReadFull;
    private List<String> aclDenyWriteFull;
    private List<String> searchAclRead;
    private List<String> searchAclDenyRead;

    private Long currentRunId;
    private String taskState;
    private Long taskStateDate;
    private Integer retries;
    private String taskParameters;
    private boolean parseX500Address = false;
    private boolean extractNonDomainX500CN = false;

    private String rawMetadata;
    private List<String> generalMetadata;
    private List<String> actionExpected;
    private List<String> daMetadata;
    private List<String> daMetadataType;

    public static FileCatBuilder create(){
        return new FileCatBuilder();
    }

    public static String convertOwner(String ownerName, boolean removeOwnerQuotes){
        String owner = CsvUtils.notNull(FileCatUtils.getEmptyDefault(ownerName));
        if (removeOwnerQuotes) {
            //noinspection RegExpRedundantEscape
            owner = owner.replaceAll("\\\"", "");
        }
        return owner;
    }

    @Override
    public SolrFileEntity build() {
        if (rootFolderId == null || props == null) {
            throw new IllegalArgumentException("Missing required parameters");
        }
        SolrFileEntity entity = new SolrFileEntity();

        String fileUrl = props.getFileName();
        if (Strings.isNullOrEmpty(fileUrl)) {
            throw new IllegalArgumentException("Missing file URL from the file properties.");
        }

        FileType fileTypeVal = calcFileType(fileUrl);

        if (!Strings.isNullOrEmpty(id)) {
            entity.setId(id);
            entity.setFileId(FileCatCompositeId.of(id).getFileId());
        }

        entity.setRootFolderId(rootFolderId);
        entity.setPackageTopFileId(packageEntityId);
        entity.setFolderId(docFolderId);
        entity.setParentFolderIds(parentFolderInfo);

        if (containerIds != null) {
            entity.setContainerIds(containerIds);
        }

        fileUrl = fileUrl.startsWith(rootFolderPath) ? fileUrl.substring(rootFolderPath.length()) : fileUrl;

        entity.setFileNameHashed(getHashedString(fileUrl));
        entity.setBaseName(FileNamingUtils.trimToSize(FileNamingUtils.getFilenameBaseName(fileUrl)));

        if (Exchange365ItemDto.class.isInstance(props)) {
            entity.setExtension(ItemType.MESSAGE.getSpecialExtension());
        } else if (MailEntryPropertiesDto.class.isInstance(props) && ItemType.MESSAGE.equals(((MailEntryPropertiesDto)props).getItemType())) {
            entity.setExtension(ItemType.MESSAGE.getSpecialExtension());
        } else {
            entity.setExtension(FileNamingUtils.calcExtension(fileUrl));
        }

        boolean isNotExchangeAttachment = ((!MailEntryPropertiesDto.class.isInstance(props) &&
                (!Exchange365ItemDto.class.isInstance(props) || FileType.MAIL.equals(props.getType()))) ||
                (MailEntryPropertiesDto.class.isInstance(props) &&
                        !ItemType.ATTACHMENT.equals(((MailEntryPropertiesDto)props).getItemType())));

        if (sortName != null) {
            entity.setSortName(sortName);
        } else if (props instanceof PstEntryPropertiesDto) {
            entity.setSortName(calcPstAttachmentSortName((PstEntryPropertiesDto)props, entity.getBaseName()));
        } else if (isNotExchangeAttachment) {
            entity.setSortName(FileCatUtils.calcSortName(entity.getBaseName()));
        }

        String fullPathVal = FileNamingUtils.convertPathToUniversalString(FileNamingUtils.getFileNameFullPath(fileUrl, entity.getBaseName()));
        entity.setFullPath(fullPathVal);
        entity.setFullName(fileUrl);
        entity.setFullNameSearch(fileUrl.toLowerCase());
        entity.setType(fileTypeVal.toInt());

        entity.setState(state.name());

        if (initialScanDate != null) {
            entity.setInitialScanDate(initialScanDate);
        } else {
            entity.setInitialScanDate(new Date());
        }

        if (lastMetadataChangeDate != null) {
            entity.setLastMetadataChangeDate(lastMetadataChangeDate);
        }

        if (ingestDate != null) {
            entity.setIngestDate(ingestDate);
        }

        if (analysisDate != null) {
            entity.setAnalysisDate(analysisDate);
        }

        if (contentChangeDate != null) {
            entity.setContentChangeDate(contentChangeDate);
        }

        if (lastAssociationChangeDate != null) {
            entity.setLastAssociationChangeDate(lastAssociationChangeDate);
        }

        if (contentId != null) {
            entity.setContentId(contentId.toString());
        }

        if (analysisGroupId != null) {
            entity.setAnalysisGroupId(analysisGroupId);
        }

        if (userGroupId != null) {
            entity.setUserGroupId(userGroupId);
        }

        if (ingestErrType != null) {
            entity.setIngestionErrorType(ingestErrType.getValue());
        }

        if (encryptionType != null) {
            entity.setEncryption(encryptionType.toInt());
        }

        if (mediaType != null) {
            entity.setMediaType(mediaType.getValue());
        }

        if (mailboxUpn != null) {
            entity.setMailboxUpn(mailboxUpn);
        }

        entity.setCreationDate(new Date(props.getCreationTimeMilli()));
        entity.setAccessDate(new Date(props.getAccessTimeMilli()));
        entity.setModificationDate(new Date(props.getModTimeMilli()));
        entity.setAclSignature(props.getAclSignature());

        String mediaId = props.getMediaItemId();
        if (mediaId != null) {
            String mediaIdStr = mediaId.startsWith(rootFolderPath) ? mediaId.substring(rootFolderPath.length()) : mediaId;
            entity.setMediaEntityId(mediaIdStr);
        }

        if (props.getAclInheritanceType() != null) {
            entity.setAclInheritanceType(props.getAclInheritanceType().name());
        }

        if (props.getFileSize() != null) {
            entity.setSizeOnMedia(props.getFileSize());
        }

        if (appCreationTimeMilli != null) {
            entity.setAppCreationDate(new Date(appCreationTimeMilli));
        }

        entity.setOwner(convertOwner(props.getOwnerName(), removeOwnerQuotes));
        entity.setLastMetadataChangeDate(System.currentTimeMillis());

        Optional.ofNullable(aclRead).ifPresent(entity::setAclRead);
        Optional.ofNullable(aclWrite).ifPresent(entity::setAclWrite);

        Optional.ofNullable(aclDenyRead).ifPresent(entity::setAclDenyRead);
        Optional.ofNullable(aclDenyWrite).ifPresent(entity::setAclDenyWrite);

        Optional.ofNullable(aclReadFull).ifPresent(entity::setAclReadFull);
        Optional.ofNullable(aclWriteFull).ifPresent(entity::setAclWriteFull);

        Optional.ofNullable(aclDenyReadFull).ifPresent(entity::setAclDenyReadFull);
        Optional.ofNullable(aclDenyWriteFull).ifPresent(entity::setAclDenyWriteFull);

        Optional.ofNullable(searchAclRead).ifPresent(entity::setSearchAclRead);
        Optional.ofNullable(searchAclDenyRead).ifPresent(entity::setSearchAclDenyRead);

        if (patterns != null && !patterns.isEmpty()) {
            String searchPatternsJson =
                    FileCatUtils.convertSearchPatternCountingToJson(patterns);
            entity.setExtractedPatternsIds(Lists.newArrayList(searchPatternsJson));
        }

        Optional.ofNullable(extractedEntitiesIds).ifPresent(entity::setExtractedEntitiesIds);

        Optional.ofNullable(currentRunId).ifPresent(entity::setCurrentRunId);
        Optional.ofNullable(taskState).ifPresent(entity::setTaskState);
        Optional.ofNullable(taskStateDate).ifPresent(entity::setTaskStateDate);
        Optional.ofNullable(retries).ifPresent(entity::setRetries);
        Optional.ofNullable(taskParameters).ifPresent(entity::setTaskParameters);


        Optional.ofNullable(rawMetadata).ifPresent(entity::setRawMetadata);
        Optional.ofNullable(generalMetadata).ifPresent(entity::setGeneralMetadata);
        Optional.ofNullable(daMetadata).ifPresent(entity::setDaMetadata);
        Optional.ofNullable(daMetadataType).ifPresent(entity::setDaMetadataType);
        Optional.ofNullable(actionExpected).ifPresent(entity::setActionsExpected);

        if (entity.getActionsExpected() != null && entity.getLabelProcessDate() == null) {
            entity.setLabelProcessDate(0L);
        }

        entity.setUpdateDate(System.currentTimeMillis());

        return entity;
    }

    @Override
    public void update(final SolrFileEntity entity) {

        if (props.getModTimeMilli() > 0) {
            entity.setModificationDate(new Date(props.getModTimeMilli()));
        }

        if (props.getAccessTimeMilli() > 0) {
            entity.setAccessDate(new Date(props.getAccessTimeMilli()));
        }

        if (entity.getOwner() == null || props.getOwnerName() != null) {
            entity.setOwner(convertOwner(props.getOwnerName(), removeOwnerQuotes));
        }

        if (props.getAclSignature() != null) {
            entity.setAclSignature(props.getAclSignature());
        }

        if (props.getAclInheritanceType() != null) {
            entity.setAclInheritanceType(props.getAclInheritanceType().name());
        }

        entity.setLastMetadataChangeDate(System.currentTimeMillis());

        if (deleted != null){
            entity.setDeleted(deleted);
        }

        if (initialScanDate != null) {
            entity.setInitialScanDate(initialScanDate);
        }

        if (state != null){
            entity.setState(state.name());
        }
        if (props.getFileSize() != null && props.getFileSize() >= 0) {
            entity.setSizeOnMedia(props.getFileSize());
        }
        if (props.getMime() != null) {
            entity.setMime(props.getMime());
        }
        if (props.getType() != null) {
            entity.setType(props.getType().toInt());
        }

        if (ingestErrType != null) {
            entity.setIngestionErrorType(ingestErrType.getValue());
        }

        if (encryptionType != null) {
            entity.setEncryption(encryptionType.toInt());
        }

        if (mediaType != null) {
            entity.setMediaType(mediaType.getValue());
        }

        if (appCreationTimeMilli != null) {
            entity.setAppCreationDate(new Date(appCreationTimeMilli));
        }

        Optional.ofNullable(aclRead).ifPresent(entity::setAclRead);
        Optional.ofNullable(aclWrite).ifPresent(entity::setAclWrite);

        Optional.ofNullable(aclDenyRead).ifPresent(entity::setAclDenyRead);
        Optional.ofNullable(aclDenyWrite).ifPresent(entity::setAclDenyWrite);

        Optional.ofNullable(aclReadFull).ifPresent(entity::setAclReadFull);
        Optional.ofNullable(aclWriteFull).ifPresent(entity::setAclWriteFull);

        Optional.ofNullable(aclDenyReadFull).ifPresent(entity::setAclDenyReadFull);
        Optional.ofNullable(aclDenyWriteFull).ifPresent(entity::setAclDenyWriteFull);

        Optional.ofNullable(searchAclRead).ifPresent(entity::setSearchAclRead);
        Optional.ofNullable(searchAclDenyRead).ifPresent(entity::setSearchAclDenyRead);

        if (patterns != null && !patterns.isEmpty()) {
            String searchPatternsJson =
                    FileCatUtils.convertSearchPatternCountingToJson(patterns);
            entity.setExtractedPatternsIds(Lists.newArrayList(searchPatternsJson));
        }

        Optional.ofNullable(containerIds).ifPresent(entity::setContainerIds);
        Optional.ofNullable(extractedEntitiesIds).ifPresent(entity::setExtractedEntitiesIds);
        Optional.ofNullable(currentRunId).ifPresent(entity::setCurrentRunId);
        Optional.ofNullable(taskState).ifPresent(entity::setTaskState);
        Optional.ofNullable(taskStateDate).ifPresent(entity::setTaskStateDate);
        Optional.ofNullable(retries).ifPresent(entity::setRetries);
        Optional.ofNullable(taskParameters).ifPresent(entity::setTaskParameters);

        Optional.ofNullable(rawMetadata).ifPresent(entity::setRawMetadata);
        Optional.ofNullable(generalMetadata).ifPresent(entity::setGeneralMetadata);
        Optional.ofNullable(daMetadata).ifPresent(entity::setDaMetadata);
        Optional.ofNullable(daMetadataType).ifPresent(entity::setDaMetadataType);
        Optional.ofNullable(actionExpected).ifPresent(entity::setActionsExpected);

        if (entity.getActionsExpected() != null && entity.getLabelProcessDate() == null) {
            entity.setLabelProcessDate(0L);
        }
        entity.setUpdateDate(System.currentTimeMillis());
    }

    private FileType calcFileType(String fileUrl) {
        FileType fileTypeVal;
        if (fileType != null) {
            fileTypeVal = fileType;
        } else if (props != null && props.getType() != null) {
            fileTypeVal = props.getType();
        } else {
            fileTypeVal = FileTypeUtils.getFileType(fileUrl);
        }
        return fileTypeVal;
    }

    private String calcPstAttachmentSortName(PstEntryPropertiesDto pstProp, String baseName) {
        Long mailFileId = 0L;
        if (containerIds != null && !containerIds.isEmpty()) {
            for (String cont : containerIds) {
                if (cont.startsWith(FileType.MAIL.name())) {
                    try {
                        mailFileId = Long.parseLong(cont.substring(5));
                    } catch (Exception e) {
                    }
                }
            }
        }
        return FileCatUtils.calcSortName(baseName,
                pstProp.getItemType() == null ? ItemType.ATTACHMENT : pstProp.getItemType(),
                pstProp.getEmailSubject(), mailFileId);
    }

    @Override
    public SolrInputDocument buildAtomicUpdate() {
        if (Strings.isNullOrEmpty(id)){
            throw new IllegalArgumentException("Must specify document ID");
        }
        AtomicUpdateBuilder builder = AtomicUpdateBuilder.create();
        builder.setId(ID, id);
        fileId = FileCatCompositeId.of(id).getFileId();

        builder
            .addModifier(FILE_ID, SET, fileId)
            .addModifierNotNull(ROOT_FOLDER_ID, SET, rootFolderId)
            .addModifierNotNull(PACKAGE_TOP_FILE_ID, SET, packageEntityId)
            .addModifierNotNull(FOLDER_ID, SET, docFolderId)
            .addModifier(UPDATE_DATE, SET, System.currentTimeMillis());

        if (state != null) {
            builder.addModifierNotNull(PROCESSING_STATE, SET, state.name());
        }

        if (parentFolderInfo != null){
            builder.addModifier(FOLDER_IDS, SET, parentFolderInfo);
        }

        if (containerIds != null) {
            builder.addModifier(CONTAINER_IDS, SET, containerIds);
        }

        builder.addModifierNotNull(LAST_METADATA_CNG_DATE, SET, lastMetadataChangeDate);
        builder.addModifierNotNull(INGEST_DATE, SET, ingestDate);
        builder.addModifierNotNull(ANALYSIS_DATE, SET, analysisDate);
        builder.addModifierNotNull(CONTENT_CHANGE_DATE, SET, contentChangeDate);
        builder.addModifierNotNull(LAST_ASSOC_CNG_DATE, SET, lastAssociationChangeDate);

        builder.addModifierNotNull(INITIAL_SCAN_DATE, SET, initialScanDate);

        builder.addModifierNotNull(RAW_METADATA, SET, rawMetadata);
        builder.addModifierNotNull(GENERAL_METADATA, SET, generalMetadata);
        builder.addModifierNotNull(DA_METADATA, SET, daMetadata);
        builder.addModifierNotNull(DA_METADATA_TYPE, SET, daMetadataType);
        builder.addModifierNotNull(ACTIONS_EXPECTED, SET, actionExpected);
        if (actionExpected != null) {
            builder.addModifier(LABEL_PROCESS_DATE, SET, 0);
        }

        if (props != null) {
            String fileUrl = props.getFileName();
            String mediaIdStr = props.getMediaItemId();

            if (!Strings.isNullOrEmpty(rootFolderPath)) {
                fileUrl = fileUrl.startsWith(rootFolderPath) ? fileUrl.substring(rootFolderPath.length()) : fileUrl;
                mediaIdStr = mediaIdStr.startsWith(rootFolderPath) ? mediaIdStr.substring(rootFolderPath.length()) : mediaIdStr;
            }

            FileType fileTypeVal = calcFileType(fileUrl);

            String baseName = FileNamingUtils.trimToSize(FileNamingUtils.getFilenameBaseName(fileUrl));
            String fullPath = FileNamingUtils.getFileNameFullPath(fileUrl, baseName);
            String fullPathVal = FileNamingUtils.convertPathToUniversalString(fullPath);

            builder.addModifier(FILE_NAME_HASHED, SET, getHashedString(fileUrl))
                    .addModifier(BASE_NAME, SET, baseName)
                    .addModifier(FULLPATH, SET, fullPathVal)
                    .addModifier(FULL_NAME, SET, fullPath + baseName)
                    .addModifier(FULL_NAME_SEARCH, SET, (fullPath + baseName).toLowerCase())
                    .addModifier(FOLDER_NAME, SET, FileNamingUtils.getParentNameOnly((fullPath + baseName).toLowerCase()))
                    .addModifier(TYPE, SET, fileTypeVal.toInt())
                    .addModifier(LAST_MODIFIED, SET, new Date(props.getModTimeMilli()))
                    .addModifierNotNull(ACL_SIGNATURE, SET, props.getAclSignature())
                    .addModifierNotNull(ACL_INHERITANCE_TYPE, SET, props.getAclInheritanceType() == null ? null : props.getAclInheritanceType().name())
                    .addModifierNotNull(MIME, SET, props.getMime())
                    .addModifierNotNull(SIZE, SET, props.getFileSize())
                    .addModifierNotNull(LAST_ACCESS, SET, new Date(props.getAccessTimeMilli()))
                    .addModifierNotNull(CREATION_DATE, SET, new Date(props.getCreationTimeMilli()))
                    .addModifierNotNull(MEDIA_ENTITY_ID, SET, mediaIdStr)
					.addModifierNotNull(LAST_RECORD_UPDATE, SET, new Date());

            boolean isNotExchangeAttachment = ((!MailEntryPropertiesDto.class.isInstance(props) &&
                    (!Exchange365ItemDto.class.isInstance(props) || FileType.MAIL.equals(props.getType()))) ||
                    (MailEntryPropertiesDto.class.isInstance(props) &&
                            !ItemType.ATTACHMENT.equals(((MailEntryPropertiesDto)props).getItemType())));

            if (sortName != null) {
                builder.addModifier(SORT_NAME, SET, sortName);
            } else if (props instanceof PstEntryPropertiesDto) {
                builder.addModifier(SORT_NAME, SET, calcPstAttachmentSortName((PstEntryPropertiesDto)props, baseName));
            } else if (isNotExchangeAttachment) {
                builder.addModifier(SORT_NAME, SET, FileCatUtils.calcSortName(baseName));
            }

            if (props.getOwnerName() != null) {
                builder.addModifier(OWNER, SET, convertOwner(props.getOwnerName(), removeOwnerQuotes));
            }

            if (props instanceof MailPropertiesDto) {
                MailPropertiesDto mailProp = (MailPropertiesDto)props;
                EmailAddress sender = mailProp.getSender();
                if (sender != null) {
                    builder.addModifierNotNull(SENDER_NAME, SET, EmailUtils.getMailName(sender.getName()))
                            .addModifierNotNull(SENDER_ADDRESS, SET, EmailUtils.getEmailAddressValue(sender.getAddress(), parseX500Address, extractNonDomainX500CN))
                            .addModifierNotNull(SENDER_DOMAIN, SET, EmailUtils.extractDomainFromEmailAddress(sender.getAddress(), parseX500Address, extractNonDomainX500CN))
                            .addModifierNotNull(SENDER_FULL_ADDRESS, SET, ModelIngestUtils.getEmailJson(sender));
                    if (logger.isTraceEnabled()) {
                        logger.trace("getEmailAddressValue({}, {}, {})={}",sender.getAddress(), parseX500Address, extractNonDomainX500CN,
                                EmailUtils.getEmailAddressValue(sender.getAddress(), parseX500Address, extractNonDomainX500CN));
                    }
                }

                List<EmailAddress> recipients = mailProp.getRecipients();
                if (recipients != null && !recipients.isEmpty()) {
                    Set<String> recipientsNames = new HashSet<>(recipients.size());
                    Set<String> recipientsAddresses = new HashSet<>(recipients.size());
                    Set<String> recipientsDomains = new HashSet<>(recipients.size());
                    List<String> recipientsFullAddresses = new ArrayList<>(recipients.size());
                    recipients.forEach(recipient -> {
                        String name = EmailUtils.getRecipientName(recipient);
                        if (name != null) {
                            recipientsNames.add(name);
                        }
                        recipientsAddresses.add(EmailUtils.getEmailAddressValue(recipient.getAddress(), parseX500Address, extractNonDomainX500CN));
                        recipientsDomains.add(EmailUtils.extractDomainFromEmailAddress(recipient.getAddress(), parseX500Address, extractNonDomainX500CN));
                        recipientsFullAddresses.add(ModelIngestUtils.getEmailJson(recipient));
                        if (logger.isTraceEnabled()) {
                            logger.trace("getEmailAddressValue({}, {}, {})={}",recipient.getAddress(), parseX500Address, extractNonDomainX500CN,
                                    EmailUtils.getEmailAddressValue(recipient.getAddress(), parseX500Address, extractNonDomainX500CN));
                        }
                    });
                    builder.addModifierNotNull(RECIPIENTS_NAMES, SET, recipientsNames)
                            .addModifierNotNull(RECIPIENTS_ADDRESSES, SET, recipientsAddresses)
                            .addModifierNotNull(RECIPIENTS_DOMAINS, SET, recipientsDomains)
                            .addModifierNotNull(RECIPIENTS_FULL_ADDRESSES, SET, recipientsFullAddresses);
                }
                builder.addModifierNotNull(ITEM_SUBJECT, SET, mailProp.getEmailSubject());
            }

            ItemType itemType = null;
            if (props instanceof PstEntryPropertiesDto) {
                PstEntryPropertiesDto pstProps = (PstEntryPropertiesDto) props;
                builder.addModifierNotNull(NUM_OF_ATTACHMENTS, SET, pstProps.getNumOfAttachments());
            } else if (props instanceof Exchange365ItemDto && FileType.MAIL.equals(props.getType())) {
                Exchange365ItemDto exngProp = (Exchange365ItemDto) props;
                if (exngProp.getAttachmentInfos() != null) {
                    builder.addModifierNotNull(NUM_OF_ATTACHMENTS, SET, exngProp.getAttachmentInfos().size());
                }
                builder.addModifierNotNull(SENT_DATE, SET, new Date(exngProp.getSentDate()));
                builder.addModifierNotNull(MAILBOX_UPN, SET, exngProp.getMailboxUpn());
                itemType = ItemType.MESSAGE;
            }

            if (props instanceof MailEntryPropertiesDto) {
                MailEntryPropertiesDto mailProp = (MailEntryPropertiesDto) props;
                if (mailProp.getItemType() != null) {
                    itemType = mailProp.getItemType();
                }
                builder.addModifierNotNull(SENT_DATE, SET, mailProp.getSentDate());
            }

            if (itemType != null) {
                builder.addModifierNotNull(ITEM_TYPE, SET, itemType.toInt());
                if (itemType.equals(ItemType.MESSAGE)) {
                    builder.addModifier(EXTENSION, SET, ItemType.MESSAGE.getSpecialExtension());
                }
            }

            if (itemType == null || itemType.equals(ItemType.ATTACHMENT)) {
                builder.addModifier(EXTENSION, SET, FileNamingUtils.calcExtension(fileUrl));
            }

            if (props.getExternalMetadataToRem() != null && !props.getExternalMetadataToRem().isEmpty()) {
                props.getExternalMetadataToRem().forEach(metadata -> builder.addModifier(EXTERNAL_METADATA, REMOVE, metadata));
            }

            if (props.getDaLabelIdsToRem() != null && !props.getDaLabelIdsToRem().isEmpty()) {
                props.getDaLabelIdsToRem().forEach(labelId -> builder.addModifier(DA_LABELS, REMOVE, labelId));
            }

            if (props.getDaMetadataTypeToRem() != null && !props.getDaMetadataTypeToRem().isEmpty()) {
                props.getDaMetadataTypeToRem().forEach(mdt -> builder.addModifier(DA_METADATA_TYPE, REMOVE, mdt));
            }

            if (props.getDaMetadataToRem() != null && !props.getDaMetadataToRem().isEmpty()) {
                props.getDaMetadataToRem().forEach(md -> builder.addModifier(DA_METADATA, REMOVE, md));
            }
        }

        if (numberOfAttachments != null) {
            builder.addModifierNotNull(NUM_OF_ATTACHMENTS, SET, numberOfAttachments);
        }

        if (appCreationTimeMilli != null) {
            builder.addModifierNotNull(APP_CREATION_DATE, SET, new Date(appCreationTimeMilli));
        }

        if (ingestErrType != null) {
            builder.addModifier(INGEST_ERR_TYPE, SET, ingestErrType.getValue());
        }

        if (encryptionType != null) {
            builder.addModifier(ENCRYPTION, SET, encryptionType.toInt());
        }

        if (mediaType != null) {
            builder.addModifier(MEDIA_TYPE, SET, mediaType.getValue());
        }

        builder.addModifierNotNull(CONTENT_ID, SET, contentId);
        builder.addModifierNotNull(ANALYSIS_GROUP_ID, SET, analysisGroupId);
        builder.addModifierNotNull(USER_GROUP_ID, SET, userGroupId);
        builder.addModifierNotNull(DELETED, SET, deleted);

        builder.addModifierNotNull(ACL_READ, SET, aclRead);
        builder.addModifierNotNull(ACL_WRITE, SET, aclWrite);
        builder.addModifierNotNull(ACL_DENIED_READ, SET, aclDenyRead);
        builder.addModifierNotNull(ACL_DENIED_WRITE, SET, aclDenyWrite);
        builder.addModifierNotNull(ACL_READ_F, SET, aclReadFull);
        builder.addModifierNotNull(ACL_WRITE_F, SET, aclWriteFull);
        builder.addModifierNotNull(ACL_DENIED_READ_F, SET, aclDenyReadFull);
        builder.addModifierNotNull(ACL_DENIED_WRITE_F, SET, aclDenyWriteFull);

        builder.addModifierNotNull(SEARCH_ACL_READ, SET, searchAclRead);
        builder.addModifierNotNull(SEARCH_ACL_DENIED_READ, SET, searchAclDenyRead);

        if (patterns != null && !patterns.isEmpty()) {
            String searchPatternsJson =
                    FileCatUtils.convertSearchPatternCountingToJson(patterns);
            builder.addModifierNotNull(EXTRACTED_PATTERNS_IDS, SET, searchPatternsJson);
        }

        if (mailboxUpn != null) {
            builder.addModifierNotNull(MAILBOX_UPN, SET, mailboxUpn);
        }

        builder.addModifierNotNull(EXTRACTED_ENTITIES_IDS, SET, extractedEntitiesIds);

        builder.addModifierNotNull(CURRENT_RUN_ID, SET, currentRunId);
        builder.addModifierNotNull(TASK_STATE, SET, taskState);
        builder.addModifierNotNull(TASK_STATE_DATE, SET, taskStateDate);
        builder.addModifierNotNull(RETRIES, SET, retries);
        builder.addModifierNotNull(TASK_PARAM, SET, taskParameters);

        return builder.build();
    }

    public Map<String, FileCatBuilder> buildAtomicUpdateAttachments() {
        Map<String, FileCatBuilder> attachments = new LinkedHashMap<>();

        if (props != null) {
            if (props instanceof Exchange365ItemDto) {
                Exchange365ItemDto exngProp = (Exchange365ItemDto) props;
                if (exngProp.getAttachmentInfos() != null && exngProp.getAttachmentInfos().size() > 0) {

                    String fileUrl = props.getFileName();
                    //fileUrl = fileUrl.startsWith(rootFolderPath) ? fileUrl.substring(rootFolderPath.length()) : fileUrl;
                    String baseName = FileNamingUtils.trimToSize(FileNamingUtils.getFilenameBaseName(fileUrl));
                    String filePath = FileNamingUtils.convertPathToUniversalString(FileNamingUtils.getFileNameFullPath(fileUrl, baseName));

                    String containerFile = FileCatUtils.calcSortName(baseName);

                    exngProp.getAttachmentInfos().forEach(attachment -> {
                        FileCatBuilder attachmentBuilder = FileCatBuilder.create();
                        attachmentBuilder.setRootFolderId(rootFolderId);
                        attachmentBuilder.setDocFolderId(docFolderId);
                        attachmentBuilder.setParentFolderInfo(parentFolderInfo);
                        attachmentBuilder.setPackageEntityId(packageEntityId);
                        attachmentBuilder.setParseX500Address(parseX500Address);
                        attachmentBuilder.setExtractNonDomainX500CN(extractNonDomainX500CN);
                        attachmentBuilder.setState(state);
                        attachmentBuilder.setDeleted(false);
                        attachmentBuilder.setSortName(FileCatUtils.calcSortName(attachment.getFilename(), ItemType.ATTACHMENT, containerFile, fileId));
                        attachmentBuilder.setLastMetadataChangeDate(lastMetadataChangeDate);
                        attachmentBuilder.setInitialScanDate(initialScanDate);
                        attachmentBuilder.setRootFolderPath(rootFolderPath);
                        attachmentBuilder.setMediaType(mediaType);
                        attachmentBuilder.setMailboxUpn(mailboxUpn);
                        attachmentBuilder.setContainerIds(Lists.newArrayList(props.getType() + "." + fileId));
                        MailEntryPropertiesDto propertiesDto = new MailEntryPropertiesDto();
                        propertiesDto.setItemType(ItemType.ATTACHMENT);
                        propertiesDto.setRecipients(exngProp.getRecipients());
                        propertiesDto.setSender(exngProp.getSender());
                        propertiesDto.setSentDate(new Date(exngProp.getSentDate()));
                        propertiesDto.setFileName(filePath + attachment.getFilename());
                        propertiesDto.setEmailSubject(attachment.getItemSubject());
                        propertiesDto.setModTimeMilli(exngProp.getModTimeMilli());
                        propertiesDto.setAccessTimeMilli(exngProp.getAccessTimeMilli());
                        propertiesDto.setCreationTimeMilli(exngProp.getCreationTimeMilli());
                        propertiesDto.setMime(attachment.getMimeType());
                        propertiesDto.setMediaItemId(attachment.getId());
                        propertiesDto.setFileSize(attachment.getSize() == null ? null : attachment.getSize().longValue());
                        attachmentBuilder.setClaFilePropertiesDto(propertiesDto);
                        attachments.put(attachment.getId(), attachmentBuilder);
                    });
                }
            }
        }
        return attachments;
    }

    public static SolrFileEntity build(SolrInputDocument doc) {
        SolrFileEntity entity = new SolrFileEntity();
        entity.setId((String)doc.get(ID.getSolrName()).getValue());
        entity.setFileId((Long)doc.get(FILE_ID.getSolrName()).getValue());
        entity.setContentId((String)doc.get(CONTENT_ID.getSolrName()).getValue());
        entity.setDeleted((Boolean)doc.get(DELETED.getSolrName()).getValue());
        entity.setFullPath((String)doc.get(FULLPATH.getSolrName()).getValue());
        entity.setAccessDate((Date)doc.get(LAST_ACCESS.getSolrName()).getValue());
        entity.setModificationDate((Date)doc.get(LAST_MODIFIED.getSolrName()).getValue());
        entity.setAclInheritanceType((String)doc.get(ACL_INHERITANCE_TYPE.getSolrName()).getValue());
        entity.setAclSignature((String)doc.get(ACL_SIGNATURE.getSolrName()).getValue());
        entity.setFullName((String)doc.get(FULL_NAME.getSolrName()).getValue());
        entity.setFullNameSearch((String)doc.get(FULL_NAME_SEARCH.getSolrName()).getValue());
        entity.setFolderName((String)doc.get(FOLDER_NAME.getSolrName()).getValue());
        entity.setLastMetadataChangeDate((Long)doc.get(LAST_METADATA_CNG_DATE.getSolrName()).getValue());
        entity.setLastAssociationChangeDate((Long)doc.get(LAST_ASSOC_CNG_DATE.getSolrName()).getValue());
        entity.setIngestDate((Long)doc.get(INGEST_DATE.getSolrName()).getValue());
        entity.setAnalysisDate((Long)doc.get(ANALYSIS_DATE.getSolrName()).getValue());
        entity.setContentChangeDate((Long)doc.get(CONTENT_CHANGE_DATE.getSolrName()).getValue());
        entity.setOwner((String)doc.get(OWNER.getSolrName()).getValue());
        entity.setState((String)doc.get(PROCESSING_STATE.getSolrName()).getValue());
        entity.setType((Integer)doc.get(TYPE.getSolrName()).getValue());
        entity.setBaseName((String)doc.get(BASE_NAME.getSolrName()).getValue());
        entity.setSortName((String)doc.get(SORT_NAME.getSolrName()).getValue());
        entity.setCreationDate((Date)doc.get(CREATION_DATE.getSolrName()).getValue());
        entity.setExtension((String)doc.get(EXTENSION.getSolrName()).getValue());
        entity.setFileNameHashed((String)doc.get(FILE_NAME_HASHED.getSolrName()).getValue());
        entity.setFolderId((Long)doc.get(FOLDER_ID.getSolrName()).getValue());
        entity.setInitialScanDate((Date)doc.get(INITIAL_SCAN_DATE.getSolrName()).getValue());
        entity.setMediaEntityId((String)doc.get(MEDIA_ENTITY_ID.getSolrName()).getValue());
        entity.setMime((String)doc.get(MIME.getSolrName()).getValue());
        entity.setPackageTopFileId((Long)doc.get(PACKAGE_TOP_FILE_ID.getSolrName()).getValue());
        entity.setRootFolderId((Long)doc.get(ROOT_FOLDER_ID.getSolrName()).getValue());
        entity.setSizeOnMedia((Long)doc.get(SIZE.getSolrName()).getValue());
        entity.setAnalysisGroupId((String)doc.get(ANALYSIS_GROUP_ID.getSolrName()).getValue());
        entity.setUserGroupId((String)doc.get(USER_GROUP_ID.getSolrName()).getValue());
        entity.setAnalysisHint((String)doc.get(ANALYSIS_HINT.getSolrName()).getValue());
        entity.setAppCreationDate((Date)doc.get(APP_CREATION_DATE.getSolrName()).getValue());
        entity.setIngestionErrorType((Integer) doc.get(INGEST_ERR_TYPE.getSolrName()).getValue());
        entity.setEncryption((Integer) doc.get(ENCRYPTION.getSolrName()).getValue());
        entity.setItemType((Integer)doc.get(ITEM_TYPE.getSolrName()).getValue());
        entity.setNumOfAttachments((Integer)doc.get(NUM_OF_ATTACHMENTS.getSolrName()).getValue());
        List<String> aclReadData = (List<String>) doc.get(CatFileFieldType.ACL_READ.getSolrName()).getValue();
        List<String> aclDeniedReadData = (List<String>) doc.get(CatFileFieldType.ACL_DENIED_READ.getSolrName()).getValue();
        List<String> aclWriteData = (List<String>) doc.get(CatFileFieldType.ACL_WRITE.getSolrName()).getValue();
        List<String> aclDeniedWriteData = (List<String>) doc.get(CatFileFieldType.ACL_DENIED_WRITE.getSolrName()).getValue();
        entity.setAclRead(aclReadData);
        entity.setAclDenyRead(aclDeniedReadData);
        entity.setAclWrite(aclWriteData);
        entity.setAclDenyWrite(aclDeniedWriteData);
        entity.setMailboxUpn((String)doc.get(MAILBOX_UPN.getSolrName()).getValue());
        entity.setContainerIds((List<String>)doc.get(CONTAINER_IDS.getSolrName()).getValue());
        entity.setExtractedEntitiesIds((List<String>) doc.get(CatFileFieldType.EXTRACTED_ENTITIES_IDS.getSolrName()).getValue());
        entity.setCurrentRunId((Long)doc.get(CURRENT_RUN_ID.getSolrName()).getValue());
        entity.setTaskState((String)doc.get(TASK_STATE.getSolrName()).getValue());
        entity.setTaskStateDate((Long)doc.get(TASK_STATE_DATE.getSolrName()).getValue());
        entity.setRetries((Integer) doc.get(RETRIES.getSolrName()).getValue());
        entity.setTaskParameters((String)doc.get(TASK_PARAM.getSolrName()).getValue());
        entity.setDataRoles((List<String>) doc.get(CatFileFieldType.FUNCTIONAL_ROLE_ASSOCIATION.getSolrName()).getValue());
        entity.setBizListItems((List<String>) doc.get(CatFileFieldType.BIZLIST_ITEM_ASSOCIATION.getSolrName()).getValue());
        entity.setActionsExpected((List<String>) doc.get(CatFileFieldType.ACTIONS_EXPECTED.getSolrName()).getValue());
        entity.setDaLabels((List<Long>) doc.get(CatFileFieldType.DA_LABELS.getSolrName()).getValue());
        entity.setExternalMetadata((List<String>) doc.get(CatFileFieldType.EXTERNAL_METADATA.getSolrName()).getValue());
        entity.setGeneralMetadata((List<String>) doc.get(CatFileFieldType.GENERAL_METADATA.getSolrName()).getValue());
        entity.setRawMetadata((String)doc.get(RAW_METADATA.getSolrName()).getValue());
        entity.setDaMetadata((List<String>)doc.get(DA_METADATA.getSolrName()).getValue());
        entity.setDaMetadataType((List<String>)doc.get(DA_METADATA_TYPE.getSolrName()).getValue());
        entity.setLabelProcessDate((Long)doc.get(LABEL_PROCESS_DATE.getSolrName()).getValue());
        entity.setActionConfirmation((List<String>) doc.get(CatFileFieldType.ACTION_CONFIRMATION.getSolrName()).getValue());
        entity.setActiveAssociations((List<String>) doc.get(CatFileFieldType.ACTIVE_ASSOCIATIONS.getSolrName()).getValue());
        return  entity;
    }

    @SuppressWarnings("unchecked")
    public SolrFileEntity build(SolrDocument doc){
        SolrFileEntity entity = new SolrFileEntity();
        entity.setId((String)doc.get(ID.getSolrName()));
        entity.setFileId((Long)doc.get(FILE_ID.getSolrName()));
        entity.setContentId((String)doc.get(CONTENT_ID.getSolrName()));
        entity.setDeleted((Boolean)doc.get(DELETED.getSolrName()));
        entity.setFullPath((String)doc.get(FULLPATH.getSolrName()));
        entity.setAccessDate((Date)doc.get(LAST_ACCESS.getSolrName()));
        entity.setModificationDate((Date)doc.get(LAST_MODIFIED.getSolrName()));
        entity.setAclInheritanceType((String)doc.get(ACL_INHERITANCE_TYPE.getSolrName()));
        entity.setAclSignature((String)doc.get(ACL_SIGNATURE.getSolrName()));
        entity.setFullName((String)doc.get(FULL_NAME.getSolrName()));
        entity.setFullNameSearch((String)doc.get(FULL_NAME_SEARCH.getSolrName()));
        entity.setFolderName((String)doc.get(FOLDER_NAME.getSolrName()));
        entity.setLastMetadataChangeDate((Long)doc.get(LAST_METADATA_CNG_DATE.getSolrName()));
        entity.setLastAssociationChangeDate((Long)doc.get(LAST_ASSOC_CNG_DATE.getSolrName()));
        entity.setIngestDate((Long)doc.get(INGEST_DATE.getSolrName()));
        entity.setAnalysisDate((Long)doc.get(ANALYSIS_DATE.getSolrName()));
        entity.setContentChangeDate((Long)doc.get(CONTENT_CHANGE_DATE.getSolrName()));
        entity.setOwner((String)doc.get(OWNER.getSolrName()));
        entity.setState((String)doc.get(PROCESSING_STATE.getSolrName()));
        entity.setType((Integer)doc.get(TYPE.getSolrName()));
        entity.setBaseName((String)doc.get(BASE_NAME.getSolrName()));
        entity.setSortName((String)doc.get(SORT_NAME.getSolrName()));
        entity.setCreationDate((Date)doc.get(CREATION_DATE.getSolrName()));
        entity.setExtension((String)doc.get(EXTENSION.getSolrName()));
        entity.setFileNameHashed((String)doc.get(FILE_NAME_HASHED.getSolrName()));
        entity.setFolderId((Long)doc.get(FOLDER_ID.getSolrName()));
        entity.setInitialScanDate((Date)doc.get(INITIAL_SCAN_DATE.getSolrName()));
        entity.setMediaEntityId((String)doc.get(MEDIA_ENTITY_ID.getSolrName()));
        entity.setMime((String)doc.get(MIME.getSolrName()));
        entity.setPackageTopFileId((Long)doc.get(PACKAGE_TOP_FILE_ID.getSolrName()));
        entity.setRootFolderId((Long)doc.get(ROOT_FOLDER_ID.getSolrName()));
        entity.setSizeOnMedia((Long)doc.get(SIZE.getSolrName()));
        entity.setAnalysisGroupId((String)doc.get(ANALYSIS_GROUP_ID.getSolrName()));
        entity.setUserGroupId((String)doc.get(USER_GROUP_ID.getSolrName()));
        entity.setAnalysisHint((String)doc.get(ANALYSIS_HINT.getSolrName()));
        entity.setAppCreationDate((Date)doc.get(APP_CREATION_DATE.getSolrName()));
        entity.setIngestionErrorType((Integer) doc.get(INGEST_ERR_TYPE.getSolrName()));
        entity.setEncryption((Integer) doc.get(ENCRYPTION.getSolrName()));
        entity.setItemType((Integer)doc.get(ITEM_TYPE.getSolrName()));
        entity.setNumOfAttachments((Integer)doc.get(NUM_OF_ATTACHMENTS.getSolrName()));
        List<String> aclReadData = (List<String>) doc.get(CatFileFieldType.ACL_READ.getSolrName());
        List<String> aclDeniedReadData = (List<String>) doc.get(CatFileFieldType.ACL_DENIED_READ.getSolrName());
        List<String> aclWriteData = (List<String>) doc.get(CatFileFieldType.ACL_WRITE.getSolrName());
        List<String> aclDeniedWriteData = (List<String>) doc.get(CatFileFieldType.ACL_DENIED_WRITE.getSolrName());
        entity.setAclRead(aclReadData);
        entity.setAclDenyRead(aclDeniedReadData);
        entity.setAclWrite(aclWriteData);
        entity.setAclDenyWrite(aclDeniedWriteData);
        entity.setMailboxUpn((String)doc.get(MAILBOX_UPN.getSolrName()));
        entity.setContainerIds((List<String>)doc.get(CONTAINER_IDS.getSolrName()));
        entity.setExtractedEntitiesIds((List<String>) doc.get(CatFileFieldType.EXTRACTED_ENTITIES_IDS.getSolrName()));
        entity.setCurrentRunId((Long)doc.get(CURRENT_RUN_ID.getSolrName()));
        entity.setTaskState((String)doc.get(TASK_STATE.getSolrName()));
        entity.setTaskStateDate((Long)doc.get(TASK_STATE_DATE.getSolrName()));
        entity.setRetries((Integer) doc.get(RETRIES.getSolrName()));
        entity.setTaskParameters((String)doc.get(TASK_PARAM.getSolrName()));
        entity.setDataRoles((List<String>) doc.get(CatFileFieldType.FUNCTIONAL_ROLE_ASSOCIATION.getSolrName()));
        entity.setBizListItems((List<String>) doc.get(CatFileFieldType.BIZLIST_ITEM_ASSOCIATION.getSolrName()));
        entity.setActionsExpected((List<String>) doc.get(CatFileFieldType.ACTIONS_EXPECTED.getSolrName()));
        entity.setDaLabels((List<Long>) doc.get(CatFileFieldType.DA_LABELS.getSolrName()));
        entity.setExternalMetadata((List<String>) doc.get(CatFileFieldType.EXTERNAL_METADATA.getSolrName()));
        entity.setGeneralMetadata((List<String>) doc.get(CatFileFieldType.GENERAL_METADATA.getSolrName()));
        entity.setRawMetadata((String)doc.get(RAW_METADATA.getSolrName()));
        entity.setDaMetadata((List<String>)doc.get(DA_METADATA.getSolrName()));
        entity.setDaMetadataType((List<String>)doc.get(DA_METADATA_TYPE.getSolrName()));
        entity.setLabelProcessDate((Long)doc.get(LABEL_PROCESS_DATE.getSolrName()));
        entity.setActionConfirmation((List<String>) doc.get(CatFileFieldType.ACTION_CONFIRMATION.getSolrName()));
        entity.setActiveAssociations((List<String>) doc.get(CatFileFieldType.ACTIVE_ASSOCIATIONS.getSolrName()));
        return  entity;
    }


    public FileCatBuilder setId(String id) {
        this.id = id;
        return this;
    }

    public FileCatBuilder setFileId(Long fileId) {
        this.fileId = fileId;
        return this;
    }

    public FileCatBuilder setRootFolderId(long rootFolderId) {
        this.rootFolderId = rootFolderId;
        return this;
    }

    public FileCatBuilder setDocFolderId(Long docFolderId) {
        this.docFolderId = docFolderId;
        return this;
    }

    public FileCatBuilder setClaFilePropertiesDto(ClaFilePropertiesDto props) {
        this.props = props;
        return this;
    }

    public FileCatBuilder setState(ClaFileState state) {
        this.state = state;
        if (ClaFileState.ANALYSED.equals(state)) {
            analysisDate = System.currentTimeMillis();
        } else if (ClaFileState.INGESTED.equals(state)) {
            ingestDate = System.currentTimeMillis();
        }
        return this;
    }

    public FileCatBuilder setPackageEntityId(Long packageEntityId) {
        this.packageEntityId = packageEntityId;
        return this;
    }

    public FileCatBuilder setRemoveOwnerQuotes(boolean removeOwnerQuotes) {
        this.removeOwnerQuotes = removeOwnerQuotes;
        return this;
    }

    public FileCatBuilder setParseX500Address(boolean parseX500Address) {
        this.parseX500Address = parseX500Address;
        return this;
    }

    public FileCatBuilder setExtractNonDomainX500CN(boolean extractNonDomainX500CN) {
        this.extractNonDomainX500CN = extractNonDomainX500CN;
        return this;
    }

    public FileCatBuilder setParentFolderInfo(List<String> parentFolderInfo) {
        this.parentFolderInfo = parentFolderInfo;
        return this;
    }

    public FileCatBuilder setDeleted(boolean deleted) {
        this.deleted = deleted;
        return this;
    }

    public FileCatBuilder setLastMetadataChangeDate(Long lastMetadataChangeDate) {
        this.lastMetadataChangeDate = lastMetadataChangeDate;
        return this;
    }

    public FileCatBuilder setAnalysisGroupId(String analysisGroupId) {
        this.analysisGroupId = analysisGroupId;
        return this;
    }

    public FileCatBuilder setUserGroupId(String userGroupId) {
        this.userGroupId = userGroupId;
        return this;
    }

    public FileCatBuilder setContentId(Long contentId) {
        this.contentId = contentId;
        contentChangeDate = System.currentTimeMillis();
        return this;
    }

    public FileCatBuilder setFileType(FileType fileType) {
        this.fileType = fileType;
        return this;
    }

    public FileCatBuilder setInitialScanDate(Date initialScanDate) {
        this.initialScanDate = initialScanDate;
        return this;
    }

    public FileCatBuilder setRootFolderPath(String rootFolderPath) {
        this.rootFolderPath = rootFolderPath;
        return this;
    }

    public FileCatBuilder setLastAssociationChangeDate(Long lastAssociationChangeDate) {
        this.lastAssociationChangeDate = lastAssociationChangeDate;
        return this;
    }

    public FileCatBuilder setIngestErrType(ProcessingErrorType ingestErrType) {
        this.ingestErrType = ingestErrType;
        return this;
    }

    public FileCatBuilder setEncryptionType(EncryptionType encryptionType) {
        this.encryptionType = encryptionType;
        return this;
    }

    public FileCatBuilder setMediaType(MediaType mediaType) {
        this.mediaType = mediaType;
        return this;
    }

    public FileCatBuilder setAppCreationTimeMilli(Long appCreationTimeMilli) {
        this.appCreationTimeMilli = appCreationTimeMilli;
        return this;
    }

    public FileCatBuilder setAclRead(List<String> aclRead) {
        this.aclRead = aclRead;
        return this;
    }

    public FileCatBuilder setAclWrite(List<String> aclWrite) {
        this.aclWrite = aclWrite;
        return this;
    }

    public FileCatBuilder setAclDenyRead(List<String> aclDenyRead) {
        this.aclDenyRead = aclDenyRead;
        return this;
    }

    public FileCatBuilder setAclDenyWrite(List<String> aclDenyWrite) {
        this.aclDenyWrite = aclDenyWrite;
        return this;
    }

    public FileCatBuilder setAclReadFull(List<String> aclReadFull) {
        this.aclReadFull = aclReadFull;
        return this;
    }

    public FileCatBuilder setAclWriteFull(List<String> aclWriteFull) {
        this.aclWriteFull = aclWriteFull;
        return this;
    }

    public FileCatBuilder setAclDenyReadFull(List<String> aclDenyReadFull) {
        this.aclDenyReadFull = aclDenyReadFull;
        return this;
    }

    public FileCatBuilder setAclDenyWriteFull(List<String> aclDenyWriteFull) {
        this.aclDenyWriteFull = aclDenyWriteFull;
        return this;
    }

    public FileCatBuilder setSearchAclRead(List<String> searchAclRead) {
        this.searchAclRead = searchAclRead;
        return this;
    }

    public FileCatBuilder setSearchAclDenyRead(List<String> searchAclDenyRead) {
        this.searchAclDenyRead = searchAclDenyRead;
        return this;
    }

    public FileCatBuilder setSearchPatterns(Set<SearchPatternCountDto> patterns) {
        this.patterns = patterns;
        return this;
    }

    public FileCatBuilder setContainerIds(List<String> containerIds) {
        this.containerIds = containerIds;
        return this;
    }

    public FileCatBuilder setMailboxUpn(String mailboxUpn) {
        this.mailboxUpn = mailboxUpn;
        return this;
    }

    public FileCatBuilder setNumberOfAttachments(Integer numberOfAttachments) {
        this.numberOfAttachments = numberOfAttachments;
        return this;
    }

    public FileCatBuilder setSortName(String sortName) {
        this.sortName = sortName;
        return this;
    }

    public FileCatBuilder setExtractedEntitiesIds(List<String> extractedEntitiesIds) {
        this.extractedEntitiesIds = extractedEntitiesIds;
        return this;
    }

    public FileCatBuilder setCurrentRunId(Long currentRunId) {
        this.currentRunId = currentRunId;
        return this;
    }

    public FileCatBuilder setTaskState(String taskState) {
        this.taskState = taskState;
        return this;
    }

    public FileCatBuilder setTaskStateDate(Long taskStateDate) {
        this.taskStateDate = taskStateDate;
        return this;
    }

    public FileCatBuilder setRetries(Integer retries) {
        this.retries = retries;
        return this;
    }

    public FileCatBuilder setTaskParameters(String taskParameters) {
        this.taskParameters = taskParameters;
        return this;
    }

    public FileCatBuilder setRawMetadata(String rawMetadata) {
        this.rawMetadata = rawMetadata;
        return this;
    }

    public FileCatBuilder setGeneralMetadata(List<String> generalMetadata) {
        this.generalMetadata = generalMetadata;
        return this;
    }

    public FileCatBuilder setActionExpected(List<String> actionExpected) {
        this.actionExpected = actionExpected;
        return this;
    }

    public FileCatBuilder setDaMetadata(List<String> daMetadata) {
        this.daMetadata = daMetadata;
        return this;
    }

    public FileCatBuilder setDaMetadataType(List<String> daMetadataType) {
        this.daMetadataType = daMetadataType;
        return this;
    }

    public String getId() {
        return id;
    }

    public Long getFileId() {
        return fileId;
    }

    public ClaFilePropertiesDto getProps() {
        return props;
    }

    public FileType getFileType() {
        return fileType;
    }

    public String getRawMetadata() {
        return rawMetadata;
    }

    public List<String> getGeneralMetadata() {
        return generalMetadata;
    }

    public List<String> getActionExpected() {
        return actionExpected;
    }

    public List<String> getDaMetadata() {
        return daMetadata;
    }

    public List<String> getDaMetadataType() {
        return daMetadataType;
    }
}
