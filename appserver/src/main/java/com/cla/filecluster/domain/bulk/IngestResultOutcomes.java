package com.cla.filecluster.domain.bulk;

import com.cla.filecluster.jobmanager.domain.entity.SimpleTask;
import org.apache.solr.common.SolrInputDocument;

public class IngestResultOutcomes {
    private SolrInputDocument solrInputDocument;
    private SimpleTask analysisTask;

    public void setSolrInputDocument(SolrInputDocument solrInputDocument) {
        this.solrInputDocument = solrInputDocument;
    }

    public SolrInputDocument getSolrInputDocument() {
        return solrInputDocument;
    }

    public void setAnalysisTask(SimpleTask analysisTask) {
        this.analysisTask = analysisTask;
    }

    public SimpleTask getAnalysisTask() {
        return analysisTask;
    }
}
