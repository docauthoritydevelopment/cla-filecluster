package com.cla.filecluster.domain.entity.date;

import javax.persistence.*;
import java.util.List;

/**
 * Created by uri on 05/06/2016.
 */
@Entity
public class DateRangePartition {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String description;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @OrderBy("orderValue")
    private List<SimpleDateRangeItem> simpleDateRangeItems;

    private boolean continuousRanges;

    private boolean isSystem;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<SimpleDateRangeItem> getSimpleDateRangeItems() {
        return simpleDateRangeItems;
    }

    public void setSimpleDateRangeItems(List<SimpleDateRangeItem> simpleDateRangeItems) {
        this.simpleDateRangeItems = simpleDateRangeItems;
    }

    @PrePersist
    public void prePersist() {
        updateSimpleDateRangeItemsOrder();
    }

    @Override
    public String toString() {
        return "DateRangePartition{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                '}';
    }

    public void updateSimpleDateRangeItemsOrder() {
        if (simpleDateRangeItems!=null && !simpleDateRangeItems.isEmpty()) {
            for (int i=0;i<simpleDateRangeItems.size();++i) {
                SimpleDateRangeItem simpleDateRange = simpleDateRangeItems.get(i);
                simpleDateRange.setOrderValue(Long.valueOf(i));
            }
        }
    }

    public boolean isContinuousRanges() {
        return continuousRanges;
    }

    public void setContinuousRanges(boolean continuousRanges) {
        this.continuousRanges = continuousRanges;
    }

    public boolean isSystem() {
        return isSystem;
    }

    public void setSystem(boolean system) {
        isSystem = system;
    }
}
