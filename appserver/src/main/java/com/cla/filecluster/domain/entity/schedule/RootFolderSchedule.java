package com.cla.filecluster.domain.entity.schedule;

import com.cla.filecluster.domain.entity.filetree.RootFolder;

import javax.persistence.*;

/**
 * Created by uri on 02/05/2016.
 */
@Entity
@Table(indexes = {@Index(columnList = "schedule_group_id,number")})
public class RootFolderSchedule {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long number;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "schedule_group_id")
    private ScheduleGroup scheduleGroup;

    @ManyToOne(fetch = FetchType.EAGER)
    private RootFolder rootFolder;

    public ScheduleGroup getScheduleGroup() {
        return scheduleGroup;
    }

    public void setScheduleGroup(ScheduleGroup scheduleGroup) {
        this.scheduleGroup = scheduleGroup;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getNumber() {
        return number;
    }

    public void setNumber(Long number) {
        this.number = number;
    }

    public RootFolder getRootFolder() {
        return rootFolder;
    }

    public void setRootFolder(RootFolder rootFolder) {
        this.rootFolder = rootFolder;
    }

    @Override
    public String toString() {
        return "RootFolderSchedule{" +
                "id=" + id +
                ", number=" + number +
                ", scheduleGroup=" + scheduleGroup +
                ", rootFolder=" + rootFolder +
                '}';
    }
}
