package com.cla.filecluster.domain.entity.monitor;

import java.io.Serializable;

/**
 * Primary key representation for component status also contains timestamp for partitioning
 *
 * Created by: yael
 * Created on: 3/19/2018
 */
public class ClaComponentStatusPK implements Serializable {
    private long id;
    private long timestamp;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ClaComponentStatusPK that = (ClaComponentStatusPK) o;

        if (id != that.id) return false;
        return timestamp == that.timestamp;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (int) (timestamp ^ (timestamp >>> 32));
        return result;
    }
}
