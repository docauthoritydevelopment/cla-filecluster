package com.cla.filecluster.domain.entity.pv;

/*
 * A rule that defines a refinement of an analysis group and by which we break the analysis group into sub groups
 * Created by: yael
 * Created on: 2/5/2018
 */

import com.cla.filecluster.domain.entity.BasicEntity;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "refinement_rules")
public class RefinementRule extends BasicEntity {

    @Column(columnDefinition = "char(36)")
    private String subgroupId;

    @Column(columnDefinition = "char(36)")
    private String rawAnalysisGroupId;

    private String subgroupName;

    @Length(max=2048)
    private String body;
    
    private boolean active = false;
    private boolean dirty = false;
    private boolean deleted = false;
    private int priority;

    public String getSubgroupId() {
        return subgroupId;
    }

    public void setSubgroupId(String subgroupId) {
        this.subgroupId = subgroupId;
    }

    public String getRawAnalysisGroupId() {
        return rawAnalysisGroupId;
    }

    public void setRawAnalysisGroupId(String rawAnalysisGroupId) {
        this.rawAnalysisGroupId = rawAnalysisGroupId;
    }

    public String getSubgroupName() {
        return subgroupName;
    }

    public void setSubgroupName(String subgroupName) {
        this.subgroupName = subgroupName;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isDirty() {
        return dirty;
    }

    public void setDirty(boolean dirty) {
        this.dirty = dirty;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    @Override
    public String toString() {
        return "RefinementRule{" +
                "subgroupId='" + subgroupId + '\'' +
                ", rawAnalysisGroupId='" + rawAnalysisGroupId + '\'' +
                ", subgroupName='" + subgroupName + '\'' +
                ", body='" + body + '\'' +
                ", active=" + active +
                ", dirty=" + dirty +
                ", priority=" + priority +
                ", id=" + id +
                '}';
    }
}
