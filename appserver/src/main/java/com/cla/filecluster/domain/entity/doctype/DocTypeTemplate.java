package com.cla.filecluster.domain.entity.doctype;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by uri on 17/08/2015.
 */
@Entity
@Table(uniqueConstraints = {@UniqueConstraint(name = "name_acc",columnNames = {"name"})})
public class DocTypeTemplate {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull
    private String name;

    public DocTypeTemplate(String templateFileName) {
        this.name = templateFileName;
    }

    public DocTypeTemplate() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
