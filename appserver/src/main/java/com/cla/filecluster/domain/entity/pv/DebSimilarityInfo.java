package com.cla.filecluster.domain.entity.pv;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.cla.connector.domain.dto.file.FileType;
import org.hibernate.validator.constraints.Length;

@Entity
@Table(name = "deb_similarity_info")
public class DebSimilarityInfo {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;	// Unique long ID
	
	@Column(name = "lid1")
	private Long lId1; 
	
	@Column(name = "lid2")
	private Long lId2; 
	
	private Long tsMilli; 
	
	@NotNull
	@Column(columnDefinition = "char(36)", name = "uuid1")
	private String uuid1;

	@NotNull
	@Column(columnDefinition = "char(36)", name = "uuid2")
	private String uuid2;

	private String part1;	// Sheet name or null
	private String part2;	// Sheet name or null

	@NotNull
	@Enumerated
	private FileType type;

	private final String groupingHint;

	@Length(max=1024)
	private final String rates;

	@Length(max=4096)
	private String info;

	public DebSimilarityInfo(final Long lId1, final Long lId2, final String uuid1,
			final String uuid2, final String part1, final String part2, final FileType type,
			final String rates, final String groupingHint, final String info) {
		this(lId1, lId2, System.currentTimeMillis(),uuid1, uuid2, part1, part2, type, rates, groupingHint, info);
	}

	public DebSimilarityInfo(final Long lId1, final Long lId2, final Long tsMilli,
			final String uuid1, final String uuid2, final String part1, final String part2, final FileType type,
			final String rates, final String groupingHint, final String info) {
		super();
		this.lId1 = lId1;
		this.lId2 = lId2;
		this.tsMilli = tsMilli;
		this.uuid1 = uuid1;
		this.uuid2 = uuid2;
		this.part1 = part1;
		this.part2 = part2;
		this.type = type;
		this.groupingHint = groupingHint;
		this.rates = rates;
		this.info = (info.length()>=4096)?info.substring(0, 4095):info;
	}

	public Long getlId1() {
		return lId1;
	}

	public void setlId1(final Long lId1) {
		this.lId1 = lId1;
	}

	public Long getlId2() {
		return lId2;
	}

	public void setlId2(final Long lId2) {
		this.lId2 = lId2;
	}

	public Long getTsMilli() {
		return tsMilli;
	}

	public void setTsMilli(final Long tsMilli) {
		this.tsMilli = tsMilli;
	}

	public String getUuid1() {
		return uuid1;
	}

	public void setUuid1(final String uuid1) {
		this.uuid1 = uuid1;
	}

	public String getUuid2() {
		return uuid2;
	}

	public void setUuid2(final String uuid2) {
		this.uuid2 = uuid2;
	}

	public String getPart1() {
		return part1;
	}

	public void setPart1(final String part1) {
		this.part1 = part1;
	}

	public String getPart2() {
		return part2;
	}

	public void setPart2(final String part2) {
		this.part2 = part2;
	}

	public FileType getType() {
		return type;
	}

	public void setType(final FileType type) {
		this.type = type;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(final String info) {
		this.info = info;
	}

	public Long getId() {
		return id;
	}

}
