package com.cla.filecluster.domain.entity.alert;

import com.cla.common.domain.dto.alert.AlertSeverity;
import com.cla.common.domain.dto.event.EventType;
import com.cla.common.domain.dto.messages.action.PluginInput;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * alert object for persistence
 * Created by: yael
 * Created on: 1/10/2018
 */
@Entity
@IdClass(AlertPK.class)
@Table(	name = "alerts")
public class Alert implements PluginInput {

    @Id
    @Access(AccessType.PROPERTY)
    protected Long id;

    @NotNull
    @Id
    @Column(name = "date_created")
    private Long dateCreated;

    @Length(max = 2048)
    private String message;

    private String description;

    @Enumerated(EnumType.ORDINAL)
    private AlertSeverity severity;

    private boolean acknowledged = false;

    @Enumerated(EnumType.ORDINAL)
    @Column(nullable = false, name = "event_type")
    private EventType eventType;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Long dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public AlertSeverity getSeverity() {
        return severity;
    }

    public void setSeverity(AlertSeverity severity) {
        this.severity = severity;
    }

    public boolean isAcknowledged() {
        return acknowledged;
    }

    public void setAcknowledged(boolean acknowledged) {
        this.acknowledged = acknowledged;
    }

    public EventType getEventType() {
        return eventType;
    }

    public void setEventType(EventType eventType) {
        this.eventType = eventType;
    }

    @Override
    public String toString() {
        return "Alert{" +
                "id=" + id +
                ", dateCreated=" + dateCreated +
                ", message='" + message + '\'' +
                ", description='" + description + '\'' +
                ", severity=" + severity +
                ", acknowledged=" + acknowledged +
                ", eventType=" + eventType +
                '}';
    }
}
