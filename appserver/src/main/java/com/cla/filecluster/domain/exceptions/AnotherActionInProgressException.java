package com.cla.filecluster.domain.exceptions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by uri on 18/11/2015.
 */
@ResponseStatus(value= HttpStatus.CONFLICT, reason="Another action in progress")
public class AnotherActionInProgressException extends RuntimeException {
    Logger logger = LoggerFactory.getLogger(AnotherActionInProgressException.class);

    public AnotherActionInProgressException(String reason) {
        super("Another Action already in progress - ["+reason+"]");
        logger.warn(this.getMessage());
    }
}
