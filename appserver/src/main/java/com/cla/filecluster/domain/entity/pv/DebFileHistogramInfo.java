package com.cla.filecluster.domain.entity.pv;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.cla.connector.domain.dto.file.FileType;
import org.hibernate.validator.constraints.Length;

@Entity
@Table(	name = "deb_file_hist_info")
public class DebFileHistogramInfo {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;	// Unique long ID
	
	@Column(name = "lid")
	private Long lId;	// File Id 
	
	private Long tsMilli; 
	
	@NotNull
	@Column(columnDefinition = "char(36)", name = "uuid")
	private String uuid;

	private String part;	// Sheet name or null

	@NotNull
	@Enumerated
	private FileType type;

	@Length(max=4096)
	private String info;

	public DebFileHistogramInfo(final Long lId, final String uuid,
			final String part, final FileType type, final String info) {
		this(lId, System.currentTimeMillis(), uuid, part, type, info);
	}
	public DebFileHistogramInfo(final Long lId, final Long tsMilli, final String uuid,
								final String part, final FileType type, final String info) {
		super();
		this.lId = lId;
		this.tsMilli = tsMilli;
		this.uuid = uuid;
		this.part = part;
		this.type = type;
		this.info = (info.length()>=4096)?info.substring(0, 4095):info;
	}

	public Long getlId() {
		return lId;
	}

	public void setlId(final Long lId) {
		this.lId = lId;
	}

	public Long getTsMilli() {
		return tsMilli;
	}

	public void setTsMilli(final Long tsMilli) {
		this.tsMilli = tsMilli;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(final String uuid) {
		this.uuid = uuid;
	}

	public String getPart() {
		return part;
	}

	public void setPart(final String part) {
		this.part = part;
	}

	public FileType getType() {
		return type;
	}

	public void setType(final FileType type) {
		this.type = type;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(final String info) {
		this.info = info;
	}

	public Long getId() {
		return id;
	}

}
