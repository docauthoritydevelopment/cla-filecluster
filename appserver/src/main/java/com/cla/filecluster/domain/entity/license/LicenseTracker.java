package com.cla.filecluster.domain.entity.license;

import com.cla.common.domain.dto.kendo.FilterDescriptor;
import com.cla.common.utils.json.FileCollectionsDeserializer;
import com.cla.common.utils.json.FileCollectionsSerializer;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;

/**
 * Created by Itay on 06/10/2019.
 * Based on LicenseResource concepts
 */
@Entity
public class LicenseTracker {

    @Id
    private String id;

    private String value;

    private long updatedTime;

    private String signature;

    private LicenseTracker() {
    }

    public LicenseTracker(String id, Long value, long updatedTime) {
        this(id, value.toString(), updatedTime);
    }

    public LicenseTracker(String id, String value, long updatedTime) {
        this.id = id;
        this.value = value;
        this.updatedTime = updatedTime;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    @Override
    public String toString() {
        return "LicenseTracker{" +
                "id='" + id + '\'' +
                ", value='" + value + '\'' +
                ", updatedTime=" + updatedTime +
                '}';
    }

    public long getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(long updatedTime) {
        this.updatedTime = updatedTime;
    }

}
