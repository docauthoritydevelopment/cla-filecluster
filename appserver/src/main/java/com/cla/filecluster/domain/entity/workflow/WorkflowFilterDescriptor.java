package com.cla.filecluster.domain.entity.workflow;

import com.cla.common.domain.dto.kendo.FilterDescriptor;
import com.cla.common.domain.dto.workflow.WorkflowFileData;
import com.cla.filecluster.domain.entity.BasicEntity;
import org.hibernate.annotations.Type;

import javax.persistence.*;

/**
 * Created by: yael
 * Created on: 2/21/2018
 */

@Entity
@Table(name = "workflow_filter_descriptor")
public class WorkflowFilterDescriptor extends BasicEntity {

    @Type(type = "json")
    @Column(columnDefinition = "json")
    private FilterDescriptor filter;

    @Type(type = "json")
    @Column(columnDefinition = "json")
    private WorkflowFileData workflowFileData;

    public FilterDescriptor getFilter() {
        return filter;
    }

    public void setFilter(FilterDescriptor filter) {
        this.filter = filter;
    }

    public WorkflowFileData getWorkflowFileData() {
        return workflowFileData;
    }

    public void setWorkflowFileData(WorkflowFileData workflowFileData) {
        this.workflowFileData = workflowFileData;
    }

    @Override
    public String toString() {
        return "WorkflowFilterDescriptor{" +
                "filter=" + filter +
                "} " + super.toString();
    }
}
