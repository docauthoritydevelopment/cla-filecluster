package com.cla.filecluster.domain.entity.pv;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@SuppressWarnings("serial")
@Entity
@Table(name = "group_unification_helper",
        indexes = {
                @Index(name = "orig_group_idx", columnList = "orig_group,processed")
        })
public class GroupUnificationHelper implements Serializable {

    @TableGenerator(name = "grp_uni_helper_table_hilo_generator", allocationSize = 1000)
    @GeneratedValue(generator = "grp_uni_helper_table_hilo_generator", strategy = GenerationType.TABLE)
    @Id
    private Long id;

    @Column(name = "orig_group")
    @NotNull
    private String origGroupId;

    @NotNull
    private String newGroupId;

    boolean processed = false;

    @Column(columnDefinition = "datetime(3)", updatable = false)
    private Date createdOnDB;

    @PrePersist
    private void setCreatedOnDB() {
        createdOnDB = new Date();
    }

    public String getOrigGroupId() {
        return origGroupId;
    }

    public void setOrigGroupId(String origGroupId) {
        this.origGroupId = origGroupId;
    }

    public String getNewGroupId() {
        return newGroupId;
    }

    public void setNewGroupId(String newGroupId) {
        this.newGroupId = newGroupId;
    }

    public boolean isProcessed() {
        return processed;
    }

    public void setProcessed(boolean processed) {
        this.processed = processed;
    }

    public Long getId() {
        return id;
    }

    public GroupUnificationHelper(final String from, final String to) {
        this.origGroupId = from;
        this.newGroupId = to;
    }

    public GroupUnificationHelper() {
    }


    public Date getCreatedOnDB() {
        return createdOnDB;
    }

    @Override
    public String toString() {
        return "{origGroupId=" + origGroupId + ", newGroupId=" + newGroupId + ", createdOnDB=" + createdOnDB + "}";
    }

}
