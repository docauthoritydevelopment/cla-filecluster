package com.cla.filecluster.domain.dto;

import com.cla.common.domain.dto.kendo.FilterDescriptor;
import com.cla.common.domain.dto.kendo.SortDescriptor;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.domain.specification.solr.SolrQueryBuilder;
import com.cla.filecluster.repository.solr.FacetSortType;
import com.cla.filecluster.service.convertion.FileDtoFieldConverter;
import com.cla.filecluster.util.categories.FileCatUtils;
import com.cla.filecluster.util.query.CriteriaQueryUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.*;
import java.util.function.Function;

/**
 * Created by uri on 07/07/2015.
 */
public class DataSourceRequest {

    private static final Logger logger = LoggerFactory.getLogger(DataSourceRequest.class);

    private static final int DEFAULT_PAGE_SIZE = 30;
    private static final int FACET_LARGE_PAGE_SIZE = 500;

    private final static ObjectMapper mapper = new ObjectMapper();
    public static final String SELECTED_ITEM_ID = "selectedItemId";
    public static final String TAKE = "take";
    public static final String BASE_FILTER_VALUE = "baseFilterValue";
    public static final String BASE_FILTER_FIELD
            = "baseFilterField";
    public static final String WORKFLOW_ID = "workflowId";
    public static final String WAIT = "wait";
    private static final String FULLY_CONTAINED = "fullyContained";
    public static final String FILTER = "filter";
    public static final String SOLR_PAGING_CURSOR = "solrPagingCursor";
    public static final String PAGE_NUMBER = "page";

    private int page = 1;

    private int pageSize = DEFAULT_PAGE_SIZE;

    private SortDescriptor[] sort;
    private FilterDescriptor filter;

    private FilterDescriptor subsetFilter;

    private Integer maxDepth;
    private Integer relDepth;

    private Long rootFolderId;

    private Long scheduleGroupId;

    private String baseFilterValue;

    private String baseFilterField;

    private String contentFilter;

    private String selectedItemId;
    private String tagTypeFilter;

    private boolean wait;

    private boolean forceRefresh;

    private boolean forceReingest;

    private boolean collapseDuplicates;

    private boolean showHidden;

    // is this request intended for export.
    // used usually for auditing purposes.
    private boolean export = false;

    private String groupFilter;
    private FilterDescriptor groupJoinFilter;

    private int historyLastHours = -1;

    private String namingSearchTerm;
    private String workflowIdentifier;

    private String solrPagingCursor;

    private FacetSortType sortBy;

    private boolean withoutPagenation = false;

    private Integer queryMinCount = 1;

    private String queryPrefix = null;

    private boolean queryWithoutDeleted = true;

    private boolean queryWithoutDeletedRootFolders = true;

    private boolean byUser = false;

    private boolean ignoreSharedPermissions = false;

    public void setFilter(FilterDescriptor filter) {
        this.filter = filter;
    }

    public FilterDescriptor getFilter() {
        return filter;
    }

    public FilterDescriptor getSubsetFilter() {
        return subsetFilter;
    }

    private void setSubsetFilter(FilterDescriptor subsetFilter) {
        this.subsetFilter = subsetFilter;
    }

    public PageRequest createPageRequest() {
        int pageSize = getPageSize();
        if (pageSize < 1) {
            throw new BadRequestException("Illegal page size. Must be at least 1", pageSize, BadRequestType.UNSUPPORTED_VALUE);
        }
        return PageRequest.of(getPage() - 1, pageSize);
    }

    public PageRequest createPageRequestWithSort() {
        if (getSort() != null && getSort().length > 0) {
            SortDescriptor firstSortDescriptor = getSort()[0];
            String[] properties = firstSortDescriptor.getField().split(";");
            Sort jpaSort = new Sort(convertDirection(firstSortDescriptor),properties);
            return PageRequest.of(getPage() - 1, getPageSize(), jpaSort);
        }
        return PageRequest.of(getPage() - 1, getPageSize());
    }

    public PageRequest createPageRequest(Sort sortOption) {
        return PageRequest.of(getPage() - 1, getPageSize(), sortOption);
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public void setSort(SortDescriptor[] sort) {
        this.sort = sort;
    }

    public SortDescriptor[] getSort() {
        return sort;
    }

    public void setMaxDepth(Integer maxDepth) {
        this.maxDepth = maxDepth;
    }

    public Integer getMaxDepth() {
        return maxDepth;
    }

    public String getNamingSearchTerm() {
        return namingSearchTerm;
    }

    public void setNamingSearchTerm(String namingSearchTerm) {
        this.namingSearchTerm = namingSearchTerm;
    }

    @SuppressWarnings("unchecked")
    public static DataSourceRequest build(Map params, int maxPageSize) {
        DataSourceRequest dataSourceRequest = build(params);
        if (dataSourceRequest.getPageSize() > maxPageSize) {
            dataSourceRequest.setPageSize(maxPageSize);
        }
        return dataSourceRequest;
    }

    public static DataSourceRequest build(Map<String, String> params) {
        if (params == null) {
            params = new HashMap<>();
        }
        DataSourceRequest dataSourceRequest = new DataSourceRequest();

        extractPage(params, dataSourceRequest);
        String take = params.get(TAKE);
        if (take != null) {
            dataSourceRequest.setPageSize(Integer.valueOf(take));
        }

        extractSortByParameter(params, dataSourceRequest);
        extractSortParameter(params, dataSourceRequest);
        extractFiltersParameter(params, dataSourceRequest);
        extractContentFilterParameter(params, dataSourceRequest);
        extractMaxDepth(params, dataSourceRequest);
        extractRelDepth(params, dataSourceRequest);
        extractSelectedItemId(params, dataSourceRequest);
        extractAsyncParameters(params, dataSourceRequest);
        extractGroupFilter(params, dataSourceRequest);
        extractOtherParameters(params, dataSourceRequest);
        extractSolrPagingCursor(params, dataSourceRequest);
        dataSourceRequest.setExport(extractIsExport(params));
        dataSourceRequest.setByUser(extractIsByUser(params));
        dataSourceRequest.setIgnoreSharedPermissions(extractIgnoreSharedPermissions(params));
        return dataSourceRequest;
    }

    private static boolean extractIsExport(Map<String, String> params) {
        return "true".equals(params.get("export"));
    }

    private static boolean extractIsByUser(Map<String, String> params) {
        return "true".equals(params.get("byUser"));
    }

    private static boolean extractIgnoreSharedPermissions(Map<String, String> params) {
        return "true".equals(params.get("ignoreSharedPermissions"));
    }

    private static void extractOtherParameters(Map<String, String> params, DataSourceRequest dataSourceRequest) {
        String rootFolderId = params.get("rootFolderId");
        if (rootFolderId != null) {
            dataSourceRequest.setRootFolderId(Long.valueOf(rootFolderId));
        }
        String scheduleGroupId = params.get("scheduleGroupId");
        if (scheduleGroupId != null) {
            dataSourceRequest.setScheduleGroupId(Long.valueOf(scheduleGroupId));
        }
        String collapseDuplicates = params.get("collapseDuplicates");
        if (collapseDuplicates != null) {
            dataSourceRequest.setCollapseDuplicates(Boolean.valueOf(collapseDuplicates));
        }
        String forceRefresh = params.get("forceRefresh");
        if (forceRefresh != null) {
            dataSourceRequest.setForceRefresh(Boolean.valueOf(forceRefresh));
        }
        String forceReingest = params.get("forceReingest");
        if (forceReingest != null) {
            dataSourceRequest.setForceReingest(Boolean.valueOf(forceReingest));
        }
        String namingSearchTerm = params.get("namingSearchTerm");
        if (namingSearchTerm != null) {
            String value = null;
            try {
                String namingSearchTermDecoded = java.net.URLDecoder.decode(namingSearchTerm, "UTF-8");
                value = namingSearchTermDecoded.trim().toLowerCase();
            } catch (UnsupportedEncodingException e) {
                logger.warn("Failed URLDecode of {}. Use as is", namingSearchTerm);
                value = namingSearchTerm.trim().toLowerCase();
            }

            // escape slashes correctly for sql query
            value = CriteriaQueryUtils.convertSlashesForQuerySearch(value);

            dataSourceRequest.setNamingSearchTerm(value);
        }
    }

    private static void extractAsyncParameters(Map<String, String> params, DataSourceRequest dataSourceRequest) {
        String wait = params.getOrDefault(WAIT, "false");
        dataSourceRequest.setWait(Boolean.valueOf(wait));
    }

    private static void extractGroupFilter(Map<String, String> params, DataSourceRequest dataSourceRequest) {
        dataSourceRequest.setGroupFilter(params.get("groupFilter"));
    }

    private static void extractSolrPagingCursor(Map<String, String> params, DataSourceRequest dataSourceRequest) {
        dataSourceRequest.setSolrPagingCursor(params.get(SOLR_PAGING_CURSOR));
    }

    private static void extractSelectedItemId(Map<String, String> params, DataSourceRequest dataSourceRequest) {
        String selectedItemId = params.get(SELECTED_ITEM_ID);
        dataSourceRequest.setSelectedItemId(selectedItemId);
    }

    private static void extractContentFilterParameter(Map<String, String> params, DataSourceRequest dataSourceRequest) {
        String contentFilter = params.get("contentFilter");
        if (contentFilter != null) {
            dataSourceRequest.setContentFilter(contentFilter);
        }
    }

    private static void extractPage(Map<String, String> params, DataSourceRequest dataSourceRequest) {
        String page = params.get(PAGE_NUMBER);
        if (page != null) {
            int pageNumber = Integer.valueOf(page);
            if (pageNumber == 0) {
                logger.warn("Zero page value found in 1-origin service set to 1");
                pageNumber = 1;
            }
            dataSourceRequest.setPage(pageNumber);
        }
    }

    private static void extractSubsetFilterParameter(Map<String, String> params, DataSourceRequest dataSourceRequest) {
        String filter = params.get("subsetFilter");
        if (filter == null || filter.equalsIgnoreCase("null")) {
            return;
        }
        try {
            FilterDescriptor resultFilter = mapper.readValue(filter, FilterDescriptor.class);
            dataSourceRequest.setSubsetFilter(resultFilter);
        } catch (IOException e) {
            throw new BadRequestException("subsetFilter", filter, e, BadRequestType.UNSUPPORTED_VALUE);
        }
    }

    private static void extractFiltersParameter(Map<String, String> params, DataSourceRequest dataSourceRequest) {
        String filter = params.get(FILTER);
        String baseFilterValue = params.get(BASE_FILTER_VALUE);
        String baseFilterField = params.get(BASE_FILTER_FIELD);
        FilterDescriptor baseFilterDescriptor = null;
        FilterDescriptor fullyContainedFilter = null;
        if (baseFilterField != null) {
            baseFilterDescriptor = new FilterDescriptor();
            baseFilterDescriptor.setField(baseFilterField);
            baseFilterDescriptor.setValue(baseFilterValue);
            baseFilterDescriptor.setOperator("eq");
            dataSourceRequest.setBaseFilterField(baseFilterField);
            dataSourceRequest.setBaseFilterValue(baseFilterValue);
            String fullyContainedString = params.get(FULLY_CONTAINED);
            if (fullyContainedString != null) {
                //Create an additional "fully contained filter"
                String operator = fullyContainedString.equals("true") ? "eq" : "neq";
                fullyContainedFilter = new FilterDescriptor(SolrQueryBuilder.GROUP_FC_PREFIX + baseFilterField, baseFilterValue, operator);
            }
        }
        List<FilterDescriptor> subFilters = new ArrayList<>();
        if (filter != null && !filter.equalsIgnoreCase("null")) {
            try {
                FilterDescriptor filterDescriptor = mapper.readValue(filter, FilterDescriptor.class);
                subFilters.add(filterDescriptor);
            } catch (IOException e) {
                throw new BadRequestException("filter", filter, e, BadRequestType.UNSUPPORTED_VALUE);
            }
        }

        String workFlowId = params.get(WORKFLOW_ID);
        if (!Strings.isNullOrEmpty(workFlowId)) {
            subFilters.add(new FilterDescriptor(
                    FileDtoFieldConverter.DtoFields.workflowData.getAlternativeName(),
                    workFlowId + FileCatUtils.CATEGORY_SEPARATOR + "*",
                    "eq"));
            dataSourceRequest.setWorkflowIdentifier(workFlowId);
        }
        if (baseFilterDescriptor != null) {
            subFilters.add(baseFilterDescriptor);
        }
        if (fullyContainedFilter != null) {
            subFilters.add(fullyContainedFilter);
        }
        if (subFilters.size() > 1) {
            FilterDescriptor resultFilter = new FilterDescriptor();
            resultFilter.setLogic("AND");
            resultFilter.setFilters(subFilters);
            dataSourceRequest.setFilter(resultFilter);
        } else if (subFilters.size() == 1) {
            dataSourceRequest.setFilter(subFilters.get(0));
        }
    }


    private static void extractSortParameter(Map<String, String> params, DataSourceRequest dataSourceRequest) {
        String sort = params.get("sort");
        if (sort != null) {
            try {
                SortDescriptor[] sortDescriptors = mapper.readValue(sort, SortDescriptor[].class);
                dataSourceRequest.setSort(sortDescriptors);
            } catch (IOException e) {
                throw new BadRequestException("sort", sort, BadRequestType.UNSUPPORTED_VALUE);
            }
        }
    }

    private static void extractSortByParameter(Map<String, String> params, DataSourceRequest dataSourceRequest) {
        String sortBy = params.get("sortBy");
        if (sortBy != null) {
            FacetSortType facetSortType = FacetSortType.valueOf(sortBy);
            dataSourceRequest.setSortBy(facetSortType);
        }
    }

    private static void extractMaxDepth(Map<String, String> params, DataSourceRequest dataSourceRequest) {
        String maxDepthString = params.get("maxdepth");
        Integer maxDepth = null;
        if (maxDepthString != null) {
            maxDepth = Integer.valueOf(maxDepthString);
        }
        dataSourceRequest.setMaxDepth(maxDepth);
    }

    private static void extractRelDepth(Map<String, String> params, DataSourceRequest dataSourceRequest) {
        String relDepthString = params.get("reldepth");
        Integer relDepth = null;
        if (relDepthString != null) {
            relDepth = Integer.valueOf(relDepthString);
        }
        dataSourceRequest.setRelDepth(relDepth);
    }

    public Integer getRelDepth() {
        return relDepth;
    }

    public void setRelDepth(Integer relDepth) {
        this.relDepth = relDepth;
    }

    public String getBaseFilterValue() {
        return baseFilterValue;
    }

    public void setBaseFilterValue(String baseFilterValue) {
        this.baseFilterValue = baseFilterValue;
    }

    public String getBaseFilterField() {
        return baseFilterField;
    }

    public void setBaseFilterField(String baseFilterField) {
        this.baseFilterField = baseFilterField;
    }

    public String getContentFilter() {
        return contentFilter;
    }

    public void setContentFilter(String contentFilter) {
        this.contentFilter = contentFilter;
    }

    public String getSelectedItemId() {
        return selectedItemId;
    }

    public void setSelectedItemId(String selectedItemId) {
        this.selectedItemId = selectedItemId;
    }

    public String getTagTypeFilter() {
        return tagTypeFilter;
    }

    public void setTagTypeFilter(String tagTypeFilter) {
        this.tagTypeFilter = tagTypeFilter;
    }

    public boolean isShowHidden() {
        return showHidden;
    }

    public void setShowHidden(boolean showHidden) {
        this.showHidden = showHidden;
    }

    public boolean isExport() {
        return export;
    }

    public void setExport(boolean export) {
        this.export = export;
    }

    public String getGroupFilter() {
        return groupFilter;
    }

    public void setGroupFilter(String groupFilter) {
        this.groupFilter = groupFilter;
    }


    public FilterDescriptor getGroupJoinFilter() {
        return groupJoinFilter;
    }

    public void setGroupJoinFilter(FilterDescriptor groupJoinFilter) {
        this.groupJoinFilter = groupJoinFilter;
    }

    public boolean isWait() {
        return wait;
    }

    public void setWait(boolean wait) {
        this.wait = wait;
    }

    public Long getRootFolderId() {
        return rootFolderId;
    }

    public void setRootFolderId(Long rootFolderId) {
        this.rootFolderId = rootFolderId;
    }

    public Long getScheduleGroupId() {
        return scheduleGroupId;
    }

    public void setScheduleGroupId(Long scheduleGroupId) {
        this.scheduleGroupId = scheduleGroupId;
    }

    public void addDefaultSort(String fieldName) {
        if (sort == null || sort.length == 0) {
            SortDescriptor sortDescriptor = new SortDescriptor();
            sortDescriptor.setDir("asc");
            sortDescriptor.setField(fieldName);
            sort = new SortDescriptor[]{sortDescriptor};
        }
    }

    public boolean isCollapseDuplicates() {
        return collapseDuplicates;
    }

    public void setCollapseDuplicates(boolean collapseDuplicates) {
        this.collapseDuplicates = collapseDuplicates;
    }

    public boolean isForceRefresh() {
        return forceRefresh;
    }

    public void setForceRefresh(boolean forceRefresh) {
        this.forceRefresh = forceRefresh;
    }

    public boolean isForceReingest() {
        return forceReingest;
    }

    public void setForceReingest(boolean forceReingest) {
        this.forceReingest = forceReingest;
    }

    public int getHistoryLastHours() {
        return historyLastHours;
    }

    public void setHistoryLastHours(int historyLastHours) {
        this.historyLastHours = historyLastHours;
    }

    public String getSolrPagingCursor() {
        return solrPagingCursor;
    }

    public void setSolrPagingCursor(String solrPagingCursor) {
        this.solrPagingCursor = solrPagingCursor;
    }

    public FacetSortType getSortBy() {
        return sortBy;
    }

    public void setSortBy(FacetSortType sortBy) {
        this.sortBy = sortBy;
    }

    public boolean isWithoutPagenation() {
        return withoutPagenation;
    }

    public void setWithoutPagenation(boolean withoutPagenation) {
        this.withoutPagenation = withoutPagenation;
    }

    public Integer getQueryMinCount() {
        return queryMinCount;
    }

    public void setQueryMinCount(Integer queryMinCount) {
        this.queryMinCount = queryMinCount;
    }

    public String getQueryPrefix() {
        return queryPrefix;
    }

    public void setQueryPrefix(String queryPrefix) {
        this.queryPrefix = queryPrefix;
    }

    public boolean isQueryWithoutDeleted() {
        return queryWithoutDeleted;
    }

    public void setQueryWithoutDeleted(boolean queryWithoutDeleted) {
        this.queryWithoutDeleted = queryWithoutDeleted;
    }

    public boolean isQueryWithoutDeletedRootFolders() {
        return queryWithoutDeletedRootFolders;
    }

    public void setQueryWithoutDeletedRootFolders(boolean queryWithoutDeletedRootFolders) {
        this.queryWithoutDeletedRootFolders = queryWithoutDeletedRootFolders;
    }

    public boolean isByUser() {
        return byUser;
    }

    public void setByUser(boolean byUser) {
        this.byUser = byUser;
    }

    public boolean isIgnoreSharedPermissions() {
        return ignoreSharedPermissions;
    }

    public void setIgnoreSharedPermissions(boolean ignoreSharedPermissions) {
        this.ignoreSharedPermissions = ignoreSharedPermissions;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("DataSourceRequest{");
        sb.append("page=").append(page);
        sb.append(", pageSize=").append(pageSize);
        sb.append(", filter=").append(filter);
        sb.append(", subsetFilter=").append(subsetFilter);
        sb.append(", maxDepth=").append(maxDepth);
        sb.append(", relDepth=").append(relDepth);
        sb.append(", rootFolderId=").append(rootFolderId);
        sb.append(", scheduleGroupId=").append(scheduleGroupId);
        sb.append(", baseFilterValue='").append(baseFilterValue).append('\'');
        sb.append(", baseFilterField='").append(baseFilterField).append('\'');
        sb.append(", contentFilter='").append(contentFilter).append('\'');
        sb.append(", selectedItemId='").append(selectedItemId).append('\'');
        sb.append(", tagTypeFilter='").append(tagTypeFilter).append('\'');
        sb.append(", wait=").append(wait);
        sb.append(", forceRefresh=").append(forceRefresh);
        sb.append(", collapseDuplicates=").append(collapseDuplicates);
        sb.append(", showHidden=").append(showHidden);
        sb.append(", export=").append(export);
        sb.append(", groupFilter='").append(groupFilter).append('\'');
        sb.append(", groupJoinFilter=").append(groupJoinFilter);
        sb.append(", historyLastHours=").append(historyLastHours);
        sb.append(", forceReingest=").append(forceReingest);
        sb.append(", sortBy=").append(sortBy);
        sb.append(", withoutPagenation=").append(withoutPagenation);
        sb.append(", queryMinCount=").append(queryMinCount);
        sb.append(", queryPrefix=").append(queryPrefix);
        sb.append(", queryWithoutDeleted=").append(queryWithoutDeleted);
        sb.append(", queryWithoutDeletedRootFolders=").append(queryWithoutDeletedRootFolders);
        sb.append(", byUser=").append(byUser);
        sb.append('}');
        return sb.toString();
    }

    public void setWorkflowIdentifier(String workflowIdentifier) {
        this.workflowIdentifier = workflowIdentifier;
    }

    public String getWorkflowIdentifier() {
        return workflowIdentifier;
    }

    public static Sort.Direction convertDirection(SortDescriptor sortDescriptor) {
        Sort.Direction direction;
        if ("asc".equalsIgnoreCase(sortDescriptor.getDir())) {
            direction = Sort.Direction.ASC;
        } else {
            direction = Sort.Direction.DESC;
        }
        return direction;
    }

    private static Sort.Order convertOrder(SortDescriptor sortDescriptor, Function<String, String> fieldConverter) {
        return new Sort.Order(convertDirection(sortDescriptor), fieldConverter.apply(sortDescriptor.getField()));
    }

    public static Sort convertSort(DataSourceRequest dataSourceRequest, Function<String, String> fieldConverter) {
        List<Sort.Order> orders = new ArrayList<>(dataSourceRequest.getSort().length);
        for (SortDescriptor sortDescriptor : dataSourceRequest.getSort()) {
            orders.add(convertOrder(sortDescriptor, fieldConverter));
        }

        return Sort.by(orders);
    }

    public void clearForUnfilteredForSpecific() {
        clearFilters();
        page = 1;
        pageSize = FACET_LARGE_PAGE_SIZE;
    }

    public void clearFilters() {
        filter = null;
        /*subsetFilter = null;
        baseFilterValue = null;
        baseFilterField = null;
        contentFilter = null;
        tagTypeFilter = null;
        groupFilter = null;
        groupJoinFilter = null;
        namingSearchTerm = null;
        workflowIdentifier = null;*/
    }

    public boolean hasFilter() {
        return filter != null /*||
                subsetFilter != null ||
                baseFilterValue != null ||
                baseFilterField != null ||
                contentFilter != null ||
                tagTypeFilter != null ||
                groupFilter != null ||
                groupJoinFilter != null ||
                namingSearchTerm != null ||
                workflowIdentifier != null*/
                ;
    }
}
