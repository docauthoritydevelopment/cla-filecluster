package com.cla.filecluster.domain.entity.security;

import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "system_settings",
        indexes = {@Index(name = "name_index",columnList = "name")})
public class SystemSettings implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    @Length(max = 100)
    private String name;

    @Length(max = 500)
    private String value;

    @Column(name = "created_time_stamp")
    private long createdTimeStampMs;

    @Column(name = "modified_time_stamp")
    private long modifiedTimeStampMs;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public long getCreatedTimeStampMs() {
        return createdTimeStampMs;
    }

    public void setCreatedTimeStampMs(long createdTimeStampMs) {
        this.createdTimeStampMs = createdTimeStampMs;
    }

    public long getModifiedTimeStampMs() {
        return modifiedTimeStampMs;
    }

    public void setModifiedTimeStampMs(long modifiedTimeStampMs) {
        this.modifiedTimeStampMs = modifiedTimeStampMs;
    }

    @Override
    public String toString() {
        return "SystemSettings{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", value='" + value + '\'' +
                ", createdTimeStampMs=" + createdTimeStampMs +
                ", modifiedTimeStampMs=" + modifiedTimeStampMs +
                '}';
    }
}
