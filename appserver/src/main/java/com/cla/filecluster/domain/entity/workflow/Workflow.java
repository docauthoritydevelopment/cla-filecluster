package com.cla.filecluster.domain.entity.workflow;

/**
 * Workflow persistent entity
 *
 * Created by: yael
 * Created on: 2/21/2018
 */

import com.cla.common.domain.dto.kendo.FilterDescriptor;
import com.cla.common.domain.dto.workflow.WorkflowPriority;
import com.cla.common.domain.dto.workflow.WorkflowState;
import com.cla.common.domain.dto.workflow.WorkflowType;
import com.cla.filecluster.domain.entity.BasicEntity;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@Entity
@Table(name = "workflow")
public class Workflow extends BasicEntity {

    @Enumerated
    private WorkflowType workflowType;

    private Long dateClosed;

    private Long dateLastStateChange;

    private Long dueDate;

    private Long currentStateDueDate;

    @Enumerated
    private WorkflowPriority priority;

    @Enumerated
    private WorkflowState state;

    private Long assignee;

    private Long assignedWorkgroup;

    @Type(type = "json")
    @Column(columnDefinition = "json")
    private Map<String, String> requestDetails;

    private String contactInfo;

    @OneToMany(orphanRemoval = true, cascade = CascadeType.ALL)
    @JoinColumn(name = "workflow_id")
    private List<WorkflowFilterDescriptor> filters = new LinkedList<>();

    public WorkflowType getWorkflowType() {
        return workflowType;
    }

    public void setWorkflowType(WorkflowType workflowType) {
        this.workflowType = workflowType;
    }

    public Long getDateClosed() {
        return dateClosed;
    }

    public void setDateClosed(Long dateClosed) {
        this.dateClosed = dateClosed;
    }

    public Long getDateLastStateChange() {
        return dateLastStateChange;
    }

    public void setDateLastStateChange(Long dateLastStateChange) {
        this.dateLastStateChange = dateLastStateChange;
    }

    public Long getDueDate() {
        return dueDate;
    }

    public void setDueDate(Long dueDate) {
        this.dueDate = dueDate;
    }

    public Long getCurrentStateDueDate() {
        return currentStateDueDate;
    }

    public void setCurrentStateDueDate(Long currentStateDueDate) {
        this.currentStateDueDate = currentStateDueDate;
    }

    public WorkflowPriority getPriority() {
        return priority;
    }

    public void setPriority(WorkflowPriority priority) {
        this.priority = priority;
    }

    public WorkflowState getState() {
        return state;
    }

    public void setState(WorkflowState state) {
        this.state = state;
    }

    public Long getAssignee() {
        return assignee;
    }

    public void setAssignee(Long assignee) {
        this.assignee = assignee;
    }

    public Long getAssignedWorkgroup() {
        return assignedWorkgroup;
    }

    public void setAssignedWorkgroup(Long assignedWorkgroup) {
        this.assignedWorkgroup = assignedWorkgroup;
    }

    public Map<String, String> getRequestDetails() {
        return requestDetails;
    }

    public void setRequestDetails(Map<String, String> requestDetails) {
        this.requestDetails = requestDetails;
    }

    public String getContactInfo() {
        return contactInfo;
    }

    public void setContactInfo(String contactInfo) {
        this.contactInfo = contactInfo;
    }


    @Override
    public String toString() {
        return "Workflow{" +
                "dateClosed=" + dateClosed +
                ", dateLastStateChange=" + dateLastStateChange +
                ", dueDate=" + dueDate +
                ", currentStateDueDate=" + currentStateDueDate +
                ", priority=" + priority +
                ", state='" + state + '\'' +
                ", assignee=" + assignee +
                ", requestDetails=" + requestDetails +
                ", contactInfo='" + contactInfo + '\'' +
                super.toString() +
                '}';
    }

    public List<WorkflowFilterDescriptor> getFilters() {
        return filters;
    }

    public void setFilters(List<WorkflowFilterDescriptor> filters) {
        this.filters = filters;
    }
}
