package com.cla.filecluster.domain.entity.audit;

import java.io.Serializable;

public class DaAuditEventPK implements Serializable {

    private long id;
    private long timeStampMs;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getTimeStampMs() {
        return timeStampMs;
    }

    public void setTimeStampMs(long timeStampMs) {
        this.timeStampMs = timeStampMs;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DaAuditEventPK that = (DaAuditEventPK) o;

        if (id != that.id) return false;
        return timeStampMs == that.timeStampMs;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (int) (timeStampMs ^ (timeStampMs >>> 32));
        return result;
    }
}
