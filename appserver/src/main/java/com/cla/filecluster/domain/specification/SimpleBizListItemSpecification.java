package com.cla.filecluster.domain.specification;

import com.cla.common.domain.dto.bizlist.BizListItemState;
import com.cla.common.domain.dto.kendo.FilterDescriptor;
import com.cla.filecluster.domain.entity.bizlist.SimpleBizListItem;
import org.apache.commons.collections.CollectionUtils;

import javax.persistence.criteria.*;

/**
 * Created by uri on 05/01/2016.
 */
public class SimpleBizListItemSpecification extends KendoFilterSpecification<SimpleBizListItem> {

    private final long bizListId;

    public SimpleBizListItemSpecification(long bizListId, FilterDescriptor filter) {
        super(SimpleBizListItem.class, filter);
        this.bizListId = bizListId;
    }

    @Override
    protected Object parseFilterValue(String field, Object value) {
        if (field.equals("state")) {
            return BizListItemState.valueOf(value.toString());
        }
        return super.parseFilterValue(field, value);
    }

    @Override
    protected boolean isDateField(String dtoField) {
        return false;
    }

    @Override
    protected String convertModelField(String dtoField) {
        return dtoField;
    }

    @Override
    public Predicate toPredicate(Root<SimpleBizListItem> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
        //This part allow to use this specification in pageable queries
        Class clazz = query.getResultType();
        if (clazz.equals(SimpleBizListItem.class)) {
            //Prevent N+1 problem. see http://jdpgrailsdev.github.io/blog/2014/09/09/spring_data_hibernate_join.html
            root.fetch("bizListItemAliases", JoinType.LEFT);
            root.fetch("bizList", JoinType.LEFT);
        }
        Path<Object> bizListIdObject = root.get("bizList").get("id");
        Predicate baseFilter = cb.equal(bizListIdObject,bizListId);
        if (filter == null || CollectionUtils.isEmpty(filter.getFilters())) {
            //Return only the group filter
            return baseFilter;
        }
        else {
            Predicate filterPredicate = buildFilter(this.filter, root, query, cb);
            return cb.and(baseFilter,filterPredicate);
        }
    }
}
