package com.cla.filecluster.domain.entity.report;

import javax.persistence.*;

/**
 * Created by uri on 27/07/2016.
 */
@Entity
@Table(name = "trend_chart_data",indexes = {
        @Index(name = "trend_chart_idx",columnList = "trend_chart_id")
})
public class TrendChartData {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Access(AccessType.PROPERTY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="trend_chart_id")
    private TrendChart trendChart;

    /**
     * The official timestamp associated with this sample
     */
    private long requestedTimeStamp;

    /**
     * The actual timestamp in which the sample was taken
     */
    private long actualTimeStamp;

    private Long viewResolutionMs;

    @Column(length = 4096)
    private String value;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TrendChart getTrendChart() {
        return trendChart;
    }

    public void setTrendChart(TrendChart trendChart) {
        this.trendChart = trendChart;
    }

    public long getActualTimeStamp() {
        return actualTimeStamp;
    }

    public void setActualTimeStamp(long actualTimeStamp) {
        this.actualTimeStamp = actualTimeStamp;
    }

    public long getRequestedTimeStamp() {
        return requestedTimeStamp;
    }

    /**
     *
     * @param requestedTimeStamp - for now, it's rounded down to the nearest hour
     */
    public void setRequestedTimeStamp(long requestedTimeStamp) {
        this.requestedTimeStamp = requestedTimeStamp;
    }

    public Long getViewResolutionMs() {
        return viewResolutionMs;
    }

    public void setViewResolutionMs(Long viewResolutionMs) {
        this.viewResolutionMs = viewResolutionMs;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "TrendChartData{" +
                "id=" + id +
                ", requestedTimeStamp=" + requestedTimeStamp +
                ", actualTimeStamp=" + actualTimeStamp +
                '}';
    }
}
