package com.cla.filecluster.domain.converters;

import com.cla.common.domain.dto.group.GroupType;
import com.cla.filecluster.domain.dto.SolrFileGroupEntity;
import com.cla.filecluster.repository.solr.query.AtomicUpdateBuilder;
import org.apache.solr.common.SolrInputDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Optional;

import static com.cla.common.constants.FileGroupsFieldType.*;
import static com.cla.filecluster.repository.solr.query.SolrOperator.SET;

/**
 * Created by: yael
 * Created on: 8/26/2019
 */
public class SolrFileGroupBuilder extends SolrEntityBuilder<SolrFileGroupEntity> {

    private final static Logger logger = LoggerFactory.getLogger(SolrFileGroupBuilder.class);

    private String id;
    private String name;
    private String generatedName;
    private Boolean deleted;
    private Boolean dirty;
    private Integer type;
    private List<String> associations;
    private Long creationDate;
    private Long numOfFiles;
    private Long countOnLastNaming;
    private Long firstMemberId;
    private Long firstMemberContentId;
    private String rawAnalysisGroupId;
    private String parentGroupId;
    private Boolean hasSubgroups;

    private String firstProposedNames;
    private String proposedNames;
    private String proposedNewNames;
    private String secondProposedNames;
    private String thirdProposedNames;

    private String fileNamestv;
    private String parentDirtv;
    private String grandParentDirtv;

    public static SolrFileGroupBuilder create(){
        return new SolrFileGroupBuilder();
    }

    @Override
    public SolrFileGroupEntity build() {
        SolrFileGroupEntity entity = new SolrFileGroupEntity();

        entity.setId(id);

        Optional.ofNullable(name).ifPresent(entity::setName);
        Optional.ofNullable(generatedName).ifPresent(entity::setGeneratedName);
        Optional.ofNullable(deleted).ifPresent(entity::setDeleted);
        Optional.ofNullable(dirty).ifPresent(entity::setDirty);
        Optional.ofNullable(type).ifPresent(entity::setType);

        if (dirty != null && dirty) {
            entity.setDirtyDate(System.currentTimeMillis());
        }

        Optional.ofNullable(associations).ifPresent(entity::setAssociations);

        Optional.ofNullable(creationDate).ifPresent(entity::setCreationDate);
        entity.setUpdateDate(System.currentTimeMillis());

        Optional.ofNullable(numOfFiles).ifPresent(entity::setNumOfFiles);
        Optional.ofNullable(countOnLastNaming).ifPresent(entity::setCountOnLastNaming);
        Optional.ofNullable(firstMemberId).ifPresent(entity::setFirstMemberId);
        Optional.ofNullable(firstMemberContentId).ifPresent(entity::setFirstMemberContentId);

        Optional.ofNullable(rawAnalysisGroupId).ifPresent(entity::setRawAnalysisGroupId);
        Optional.ofNullable(parentGroupId).ifPresent(entity::setParentGroupId);
        Optional.ofNullable(hasSubgroups).ifPresent(entity::setHasSubgroups);

        Optional.ofNullable(firstProposedNames).ifPresent(entity::setFirstProposedNames);
        Optional.ofNullable(proposedNames).ifPresent(entity::setProposedNames);
        Optional.ofNullable(proposedNewNames).ifPresent(entity::setProposedNewNames);
        Optional.ofNullable(secondProposedNames).ifPresent(entity::setSecondProposedNames);
        Optional.ofNullable(thirdProposedNames).ifPresent(entity::setThirdProposedNames);

        Optional.ofNullable(fileNamestv).ifPresent(entity::setFileNamestv);
        Optional.ofNullable(parentDirtv).ifPresent(entity::setParentDirtv);
        Optional.ofNullable(grandParentDirtv).ifPresent(entity::setGrandParentDirtv);

        return entity;
    }

    @Override
    public void update(SolrFileGroupEntity existing) {
        throw new RuntimeException("Not implemented");
    }

    @Override
    public SolrInputDocument buildAtomicUpdate() {
        if (id == null){
            throw new IllegalArgumentException("Must specify document ID");
        }

        AtomicUpdateBuilder builder = AtomicUpdateBuilder.create().setId(ID, id);
        builder.addModifierNotNull(NAME, SET, name);
        builder.addModifierNotNull(GENERATED_NAME, SET, generatedName);
        builder.addModifierNotNull(TYPE, SET, type);
        builder.addModifierNotNull(DELETED, SET, deleted);
        builder.addModifierNotNull(DIRTY, SET, dirty);

        if (dirty != null && dirty) {
            builder.addModifierNotNull(DIRTY_DATE, SET, System.currentTimeMillis());
        }

        builder.addModifierNotNull(GROUP_ASSOCIATIONS, SET, associations);

        builder.addModifierNotNull(CREATION_DATE, SET, creationDate);
        builder.addModifier(UPDATE_DATE, SET, System.currentTimeMillis());

        builder.addModifierNotNull(NUM_OF_FILES, SET, numOfFiles);
        builder.addModifierNotNull(COUNT_LAST_NAMING, SET, countOnLastNaming);
        builder.addModifierNotNull(FIRST_MEMBER_ID, SET, firstMemberId);
        builder.addModifierNotNull(FIRST_MEMBER_CONTENT_ID, SET, firstMemberContentId);

        builder.addModifierNotNull(RAW_ANALYSIS_GROUP_ID, SET, rawAnalysisGroupId);
        builder.addModifierNotNull(PARENT_GROUP_ID, SET, parentGroupId);
        builder.addModifierNotNull(HAS_SUBGROUPS, SET, hasSubgroups);

        builder.addModifierNotNull(FST_PROP_NAMES, SET, firstProposedNames);
        builder.addModifierNotNull(PROP_NAMES, SET, proposedNames);
        builder.addModifierNotNull(PROP_NEW_NAMES, SET, proposedNewNames);
        builder.addModifierNotNull(SND_PROP_NAMES, SET, secondProposedNames);
        builder.addModifierNotNull(TRD_PROP_NAMES, SET, thirdProposedNames);

        builder.addModifierNotNull(FILE_NAMESTV, SET, fileNamestv);
        builder.addModifierNotNull(PARENT_DIRTV, SET, parentDirtv);
        builder.addModifierNotNull(GRANDPARENT_DIRTV, SET, grandParentDirtv);

        return builder.build();
    }

    public SolrFileGroupBuilder setFieldsForNewGroup(String id, GroupType type) {
        setDirty(true);
        setDeleted(false);
        setHasSubgroups(false);
        setCreationDate(System.currentTimeMillis());
        setType(type.toInt());
        setId(id);
        return this;
    }

    public SolrFileGroupBuilder setId(String id) {
        this.id = id;
        return this;
    }

    public SolrFileGroupBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public SolrFileGroupBuilder setGeneratedName(String generatedName) {
        this.generatedName = generatedName;
        return this;
    }

    public SolrFileGroupBuilder setDeleted(Boolean deleted) {
        this.deleted = deleted;
        return this;
    }

    public SolrFileGroupBuilder setDirty(Boolean dirty) {
        this.dirty = dirty;
        return this;
    }

    public SolrFileGroupBuilder setType(Integer type) {
        this.type = type;
        return this;
    }

    public SolrFileGroupBuilder setAssociations(List<String> associations) {
        this.associations = associations;
        return this;
    }

    public SolrFileGroupBuilder setCreationDate(Long creationDate) {
        this.creationDate = creationDate;
        return this;
    }

    public SolrFileGroupBuilder setNumOfFiles(Long numOfFiles) {
        this.numOfFiles = numOfFiles;
        return this;
    }

    public SolrFileGroupBuilder setCountOnLastNaming(Long countOnLastNaming) {
        this.countOnLastNaming = countOnLastNaming;
        return this;
    }

    public SolrFileGroupBuilder setFirstMemberId(Long firstMemberId) {
        this.firstMemberId = firstMemberId;
        return this;
    }

    public SolrFileGroupBuilder setFirstMemberContentId(Long firstMemberContentId) {
        this.firstMemberContentId = firstMemberContentId;
        return this;
    }

    public SolrFileGroupBuilder setRawAnalysisGroupId(String rawAnalysisGroupId) {
        this.rawAnalysisGroupId = rawAnalysisGroupId;
        return this;
    }

    public SolrFileGroupBuilder setParentGroupId(String parentGroupId) {
        this.parentGroupId = parentGroupId;
        return this;
    }

    public SolrFileGroupBuilder setHasSubgroups(Boolean hasSubgroups) {
        this.hasSubgroups = hasSubgroups;
        return this;
    }

    public SolrFileGroupBuilder setFirstProposedNames(String firstProposedNames) {
        this.firstProposedNames = firstProposedNames;
        return this;
    }

    public SolrFileGroupBuilder setProposedNames(String proposedNames) {
        this.proposedNames = proposedNames;
        return this;
    }

    public SolrFileGroupBuilder setProposedNewNames(String proposedNewNames) {
        this.proposedNewNames = proposedNewNames;
        return this;
    }

    public SolrFileGroupBuilder setSecondProposedNames(String secondProposedNames) {
        this.secondProposedNames = secondProposedNames;
        return this;
    }

    public SolrFileGroupBuilder setThirdProposedNames(String thirdProposedNames) {
        this.thirdProposedNames = thirdProposedNames;
        return this;
    }

    public SolrFileGroupBuilder setFileNamestv(String fileNamestv) {
        this.fileNamestv = fileNamestv;
        return this;
    }

    public SolrFileGroupBuilder setParentDirtv(String parentDirtv) {
        this.parentDirtv = parentDirtv;
        return this;
    }

    public SolrFileGroupBuilder setGrandParentDirtv(String grandParentDirtv) {
        this.grandParentDirtv = grandParentDirtv;
        return this;
    }
}
