package com.cla.filecluster.domain.entity.security;

import com.cla.filecluster.domain.entity.BasicEntity;

import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;


@Entity
@Table(name = "work_groups",
        indexes = {
                @Index(unique = true, name = "wg_name_idx", columnList = "name")
        })
public class WorkGroup extends BasicEntity {

    private String name;

    private String description;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "WorkGroup{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", id=" + id +
                "} " + super.toString();
    }
}
