package com.cla.filecluster.domain.entity.security;

import com.cla.common.domain.dto.security.FunctionalRoleInterface;
import com.cla.filecluster.domain.entity.filetag.AssociableEntity;
import com.cla.filecluster.util.categories.FileCatUtils;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.Objects;

/**
 * Created by uri on 07/09/2016.
 */
@Entity
@DiscriminatorValue(value="FUNC_ROLE")
public class FunctionalRole extends CompositeRole implements AssociableEntity, FunctionalRoleInterface {

    private String managerUsername;

    public String getManagerUsername() {
        return managerUsername;
    }

    public void setManagerUsername(String managerUsername) {
        this.managerUsername = managerUsername;
    }

    @SuppressWarnings("StringBufferReplaceableByString")
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("FunctionalRole{");
        sb.append("super=").append(super.toString());
        sb.append("manager=").append(managerUsername);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        // self check
        if (this == o)
            return true;

        // null check
        if (o == null)
            return false;

        // type check and cast
        if (!(o instanceof FunctionalRole))
            return false;

        // field comparison
        FunctionalRole other = (FunctionalRole) o;
        return Objects.equals(other.getId(), this.getId())
                && Objects.equals(other.getName(), this.getName());
    }

    @Override
    public int hashCode() {
        int result = getId() != null ? getId().hashCode() : 0;
        result = 31 * result + getName().hashCode();
        result = 31 * result + getClass().hashCode();
        return result;
    }

    @Override
    public String createIdentifierString() {
        return createFuncRoleIdentifierString();
    }
}
