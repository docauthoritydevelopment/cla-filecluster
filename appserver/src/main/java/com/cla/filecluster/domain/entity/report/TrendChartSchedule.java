package com.cla.filecluster.domain.entity.report;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Created by uri on 27/07/2016.
 */
@Entity
public class TrendChartSchedule {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private boolean active;

    private String cronTriggerString;

    private long lastRunStartTime;

    private long lastRunEndTime;

    private Long lastRequestedTimeStamp;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getCronTriggerString() {
        return cronTriggerString;
    }

    public void setCronTriggerString(String cronTriggerString) {
        this.cronTriggerString = cronTriggerString;
    }

    public long getLastRunStartTime() {
        return lastRunStartTime;
    }

    public void setLastRunStartTime(long lastRunStartTime) {
        this.lastRunStartTime = lastRunStartTime;
    }

    public long getLastRunEndTime() {
        return lastRunEndTime;
    }

    public void setLastRunEndTime(long lastRunEndTime) {
        this.lastRunEndTime = lastRunEndTime;
    }

    public Long getLastRequestedTimeStamp() {
        return lastRequestedTimeStamp;
    }

    public void setLastRequestedTimeStamp(Long lastRequestedTimeStamp) {
        this.lastRequestedTimeStamp = lastRequestedTimeStamp;
    }
}
