package com.cla.filecluster.domain.entity.board;

import com.cla.common.domain.dto.board.BoardType;
import com.cla.filecluster.domain.entity.BasicEntity;
import lombok.*;
import lombok.experimental.Accessors;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

@NoArgsConstructor
@Getter
@Setter
@Accessors(chain = true)
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "boards")
public class Board extends BasicEntity {

    private String name;

    private String description;

    private long ownerId;

    private boolean isPublic;

    @Enumerated(EnumType.STRING)
    private BoardType boardType;
}
