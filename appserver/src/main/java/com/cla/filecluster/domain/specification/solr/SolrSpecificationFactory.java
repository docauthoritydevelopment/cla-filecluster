package com.cla.filecluster.domain.specification.solr;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.domain.dto.bizlist.BizListItemType;
import com.cla.common.domain.dto.bizlist.SimpleBizListItemDto;
import com.cla.common.domain.dto.date.DateRangeItemDto;
import com.cla.common.domain.dto.date.DateRangePointDto;
import com.cla.common.domain.dto.date.DateRangePointType;
import com.cla.common.domain.dto.date.TimePeriod;
import com.cla.common.domain.dto.doctype.DocTypeDto;
import com.cla.common.domain.dto.kendo.FilterDescriptor;
import com.cla.common.utils.AssociationIdUtils;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.connector.utils.TimeSource;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.entity.bizlist.BizList;
import com.cla.filecluster.domain.entity.bizlist.SimpleBizListItem;
import com.cla.filecluster.domain.entity.date.DateRangePoint;
import com.cla.filecluster.domain.entity.date.SimpleDateRangeItem;
import com.cla.filecluster.domain.entity.department.Department;
import com.cla.filecluster.domain.entity.doctype.DocType;
import com.cla.filecluster.domain.entity.filetag.FileTag;
import com.cla.filecluster.domain.entity.filetag.FileTagType;
import com.cla.filecluster.domain.entity.filetree.DocFolder;
import com.cla.filecluster.domain.entity.filetype.Extension;
import com.cla.filecluster.domain.entity.filetype.FileTypeCategory;
import com.cla.filecluster.domain.entity.scope.BasicScope;
import com.cla.filecluster.domain.entity.searchpatterns.TextSearchPattern;
import com.cla.filecluster.domain.entity.security.FunctionalRole;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.repository.jpa.date.SimpleDateRangeItemRepository;
import com.cla.filecluster.repository.jpa.doctype.DocTypeRepository;
import com.cla.filecluster.repository.jpa.entitylist.BizListRepository;
import com.cla.filecluster.repository.jpa.entitylist.SimpleBizListItemRepository;
import com.cla.filecluster.repository.jpa.filetag.FileTagRepository;
import com.cla.filecluster.repository.jpa.filetag.FileTagTypeRepository;
import com.cla.filecluster.repository.jpa.security.FunctionalRoleRepository;
import com.cla.filecluster.repository.solr.FacetType;
import com.cla.filecluster.repository.solr.SolrFacetQueryJson;
import com.cla.filecluster.repository.solr.SolrFacetQueryResponse;
import com.cla.filecluster.repository.solr.query.Criteria;
import com.cla.filecluster.repository.solr.query.Query;
import com.cla.filecluster.repository.solr.query.SolrOperator;
import com.cla.filecluster.service.api.ModelDtoConversionUtils;
import com.cla.filecluster.service.categories.FileTypeCategoryService;
import com.cla.filecluster.service.convertion.FileDtoFieldConverter;
import com.cla.filecluster.service.department.DepartmentService;
import com.cla.filecluster.service.extractor.pattern.ClaFileSearchPatternService;
import com.cla.filecluster.service.files.filetree.DocFolderService;
import com.cla.filecluster.service.files.filetree.DocStoreService;
import com.cla.filecluster.service.license.LicenseService;
import com.cla.filecluster.service.scope.ScopeService;
import com.cla.filecluster.util.categories.FileCatUtils;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.apache.solr.client.solrj.response.FacetField;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * This factory is used to create Solr Specifications.
 * It's also used by the solrSpecifications as a gateway to service and repository functionality.
 * Created by Shtand on 24/09/2015.
 */
@Service
public class SolrSpecificationFactory {

    private static final Logger logger = LoggerFactory.getLogger(SolrSpecificationFactory.class);

    public static final String NOT_DEF_STR_FIELD = "NOT_DEF";


    private static final long ONE_YEAR_MILLIS = TimeUnit.DAYS.toMillis(365);
    private final String LABEL2DROP = CatFileFieldType.LAST_MODIFIED.getSolrName() + ":(-*)";

    @Autowired
    private FileTagRepository fileTagRepository;

    @Autowired
    private FileTagTypeRepository fileTagTypeRepository;

    @Autowired
    private DocFolderService docFolderService;

    @Autowired
    private ClaFileSearchPatternService claFileSearchPatternService;

    @Autowired
    private DocTypeRepository docTypeRepository;

    @Autowired
    private SimpleDateRangeItemRepository simpleDateRangeItemRepository;

    @Autowired
    private BizListRepository bizListRepository;

    @Autowired
    private LicenseService licenseService;

    @Autowired
    private DepartmentService departmentService;

    @Autowired
    private FileTypeCategoryService fileTypeCategoryService;

    @Autowired
    private SimpleBizListItemRepository simpleBizListItemRepository;

    @Autowired
    private FunctionalRoleRepository functionalRoleRepository;

    @Autowired
    private ScopeService scopeService;

    @Autowired
    private DocStoreService docStoreService;

    @Value("${scanner.metadata.ownerDbWithQuotes:true}")
    private boolean ownerDbWithQuotes;

    @Value("${api.filecount.should-get-facet-file-num-no-filter:true}")
    private boolean shouldGetFacetFileNumNoFilter;

    private TimeSource timeSource = new TimeSource();

    public boolean isShouldGetFacetFileNumNoFilter() {
        return shouldGetFacetFileNumNoFilter;
    }

    public static List<String> getValuesFromFacet(SolrFacetQueryResponse solrFacetQueryResponse) {
        return getValuesFromFacet(solrFacetQueryResponse.getFirstResult());
    }

    public static List<String> getValuesFromFacet(FacetField field) {
        List<String> values = new ArrayList<>();
        for (FacetField.Count count : field.getValues()) {
            values.add(count.getName());
        }
        return values;
    }

    //blt.1.2 bli.1.2.2_EPSA - we want the 1st 2 (from the blt.1.2)
//    private Pattern bizListIdPattern = Pattern.compile("blt\\.(\\d+)\\.(\\d+)\\sbli\\.(\\d+)\\.(\\d+)\\.(.+)");

    public SolrSpecification buildSolrSpecification(DataSourceRequest dataSourceRequest) {
        licenseService.validateLoginExpiry();
        SolrSpecification solrSpecification = new SolrSpecification(dataSourceRequest.getFilter());
        if (dataSourceRequest.getContentFilter() != null) {
            solrSpecification.addToContentFilter(dataSourceRequest.getContentFilter());
        }
        if (dataSourceRequest.isCollapseDuplicates()) {
            solrSpecification.setGroupByField("contentId");
            solrSpecification.setGroupMain(true);
        }
        addDeletedFilesLimitation(dataSourceRequest, solrSpecification);
        addDeletedRootFolderLimitation(dataSourceRequest, solrSpecification);
        return solrSpecification;
    }

    public SolrSpecification buildSolrSpecification(FilterDescriptor filterDescriptor) {
        licenseService.validateLoginExpiry();
        return new SolrSpecification(filterDescriptor);
    }

    public SolrSpecification buildSolrFacetSpecification(DataSourceRequest dataSourceRequest, CatFileFieldType facetField, boolean ignoreDeleted) {
        licenseService.validateLoginExpiry();
        SolrSpecification solrSpecification = new SolrSingleFieldFacetSpecification(dataSourceRequest.getFilter(), facetField);

        if (dataSourceRequest.getContentFilter() != null) {
            solrSpecification.addToContentFilter(dataSourceRequest.getContentFilter());
        }

        if (ignoreDeleted) {
            addDeletedFilesLimitation(dataSourceRequest, solrSpecification);
        }
        addDeletedRootFolderLimitation(dataSourceRequest, solrSpecification);
        solrSpecification.setGroupJoinFilter(dataSourceRequest.getGroupJoinFilter());
        return solrSpecification;
    }

    public void addDeletedRootFolderLimitation(DataSourceRequest dataSourceRequest, SolrSpecification solrSpecification) {
        if (dataSourceRequest.isQueryWithoutDeletedRootFolders()) {
            Set<Long> deletedRootFolderIds = docStoreService.getDeletedRootFolderIds();
            if (!deletedRootFolderIds.isEmpty()) {
                solrSpecification.setAdditionalFilter(CatFileFieldType.ROOT_FOLDER_ID.notInQuery(deletedRootFolderIds));
            }
        }
    }

    public void addDeletedFilesLimitation(DataSourceRequest dataSourceRequest, SolrSpecification solrSpecification) {
        if (dataSourceRequest.isQueryWithoutDeleted()) {
            solrSpecification.setAdditionalFilter(CatFileFieldType.DELETED.filter(false));
        }
    }

    public Query addFilterDeletedFiles(Query query) {
        query.addFilterWithCriteria(Criteria.create(CatFileFieldType.DELETED, SolrOperator.EQ, "false"));
        Set<Long> deletedRootFolderIds = docStoreService.getDeletedRootFolderIds();
        if (!deletedRootFolderIds.isEmpty()) {
            query.addFilterWithCriteria(Criteria.create(CatFileFieldType.ROOT_FOLDER_ID, SolrOperator.NOT_IN, deletedRootFolderIds));
        }
        return query;
    }

    public SolrSpecification buildSolrFacetSpecification(DataSourceRequest dataSourceRequest, CatFileFieldType facetField) {
        return buildSolrFacetSpecification(dataSourceRequest, facetField, true);
    }

    public SolrSpecification buildSolrFacetSpecification(DataSourceRequest dataSourceRequest,
                                                         CatFileFieldType facetField, Collection<String> queryValues) {
        licenseService.validateLoginExpiry();
        SolrSpecification solrSpecification = new SolrSingleFieldFacetSpecification(this, dataSourceRequest.getFilter(), facetField, queryValues);

        if (dataSourceRequest.getContentFilter() != null) {
            solrSpecification.addToContentFilter(dataSourceRequest.getContentFilter());
        }

        addDeletedFilesLimitation(dataSourceRequest, solrSpecification);
        addDeletedRootFolderLimitation(dataSourceRequest, solrSpecification);
        return solrSpecification;
    }

    public SolrSpecification buildSolrFacetSpecification(FilterDescriptor baseFilter, CatFileFieldType facetField, List<String> queryValues) {
        licenseService.validateLoginExpiry();
        return new SolrSingleFieldFacetSpecification(this, baseFilter, facetField, queryValues);
    }

    public SolrSpecification buildSolrFacetSpecification(FilterDescriptor baseFilter, CatFileFieldType facetField) {
        licenseService.validateLoginExpiry();
        return facetField == null ? new SolrSpecification(baseFilter)
                : new SolrSingleFieldFacetSpecification(baseFilter, facetField);
    }

    String convertTagTypeIdToTagTypeFilterValue2(Object tagTypeId) {
        return "type." + tagTypeId.toString();
    }

    /**
     * This can be used inside a filter query to limit only documents that has at least minCount times the value in the field.
     *
     * @return A solr filter that limits results to those with at least minCount items
     */
    String createMinCountFilter(String field, String value, int minCount) {
        return "{!frange l=" + minCount + "}termfreq(" + field + ",'" + value + "')";
    }

    private List<String> extractFilterByField(FilterDescriptor filterDescriptor, CatFileFieldType field, String operator) {
        if (StringUtils.isNotEmpty(filterDescriptor.getField())) {
            CatFileFieldType catFileFieldType = FileDtoFieldConverter.convertToCatFileField(filterDescriptor.getField());
            if (field.equals(catFileFieldType)) {
                if (operator == null || operator.equals(filterDescriptor.getOperator())) {
                    //Found it
                    return Lists.newArrayList(String.valueOf(filterDescriptor.getValue()));
                }
            }
        } else if (filterDescriptor.getFilters() != null) {
            List<String> result = new ArrayList<>();
            for (FilterDescriptor childDescriptor : filterDescriptor.getFilters()) {
                List<String> value = extractFilterByField(childDescriptor, field, operator);
                if (value != null) {
                    //Found it
                    result.addAll(value);
                }
            }
            return result.isEmpty() ? null : result;
        }
        return null;
    }

    public List<String> extractFilterByField(DataSourceRequest dataSourceRequest, CatFileFieldType field, String operator) {
        if (dataSourceRequest.getFilter() == null) {
            return null;
        }
        return extractFilterByField(dataSourceRequest.getFilter(), field, operator);
    }

    String convertFolderIdToFilterValue(Long folderId) {
        DocFolder folder = docFolderService.findById(folderId);
        if (folder == null) {
            logger.error("filter by non existing folderId {}", folderId);
            return NOT_DEF_STR_FIELD;
        }
        return folder.getDepthFromRoot() + FileCatUtils.CATEGORY_SEPARATOR + folderId;
    }

    String convertBizListIdToFilterValue(Long bizListId) {
        return bizListRepository.findById(bizListId)
                .map(bz -> FileDtoFieldConverter.BIZLIST_SOLR_PREFIX + bz.getBizListItemType().ordinal() + "." + bz.getId())
                .orElse(NOT_DEF_STR_FIELD);
    }

    String convertBizListItemIdToFilterValue(String bizListItemId) {
        Optional<SimpleBizListItem> result = simpleBizListItemRepository.findById(bizListItemId);
        if (result.isPresent()) {
            BizList list = result.get().getBizList();
            return FileDtoFieldConverter.convertBizListItemToSolrValue(false, list.getBizListItemType(), list.getId(), bizListItemId);
        } else {
            logger.error("filter by non existing bizListItemId {}", bizListItemId);
            return NOT_DEF_STR_FIELD;
        }
    }

    String convertBizListTypeToFilterValue(String bizListType) {
        String bizListOrdinal;
        if (bizListType.matches("\\d+")) {
            bizListOrdinal = bizListType;
        } else {
            bizListOrdinal = String.valueOf(BizListItemType.valueOf(bizListType).ordinal());
        }
        return FileDtoFieldConverter.BIZLIST_SOLR_PREFIX + bizListOrdinal + "*";
    }

    String convertBizListExtractionRuleToFilterValue(String extractionRule) {
        return FileDtoFieldConverter.EXTRACTION_RULE_SOLR_PREFIX + extractionRule;
    }

    String convertBizListItemTypeToFilterValue(String bizListType) {
        String bizListOrdinal;
        if (bizListType.matches("\\d+")) {
            bizListOrdinal = bizListType;
        } else {
            bizListOrdinal = String.valueOf(BizListItemType.valueOf(bizListType).ordinal());
        }
        return FileDtoFieldConverter.BIZLIST_ITEM_SOLR_PREFIX + bizListOrdinal + "*";
    }

    String[] convertDepartmentParentsToFilterValue(String departmentNamePart) {
        List<Department> parents = departmentService.getByPartialName(departmentNamePart);
        if (parents == null || parents.isEmpty()) {
            return new String[]{NOT_DEF_STR_FIELD};
        }
        List<String> all = new ArrayList<>();
        for (Department parent : parents) {

            String[] res = convertDepartmentParentsToFilterValue(parent.getId(), true);
            all.addAll(Arrays.asList(res));
        }
        return all.toArray(new String[all.size()]);
    }

    String[] convertPatternMatchNameToFilterValue(String patternNamePart) {
        List<TextSearchPattern> patterns = claFileSearchPatternService.getByPartialName(patternNamePart);
        if (patterns == null || patterns.isEmpty()) {
            return new String[]{NOT_DEF_STR_FIELD};
        }
        return patterns.stream().map(TextSearchPattern::getId).map(String::valueOf).toArray(String[]::new);
    }


    String[] convertFileTypeCategoryNameToFilterValue(String patternNamePart) {
        List<FileTypeCategory> fileTypeCategories = fileTypeCategoryService.getFileTypeCategoryByPartialName(patternNamePart);
        if (fileTypeCategories == null || fileTypeCategories.isEmpty()) {
            return new String[]{NOT_DEF_STR_FIELD};
        }
        ArrayList<Extension> extensionList = new ArrayList<>();
        for (FileTypeCategory ftg : fileTypeCategories) {
            extensionList.addAll(ftg.getExtensions());
        }
        return extensionList.stream().map(Extension::getName).toArray(String[]::new);
    }


    String[] convertDepartmentParentsToFilterValue(Long departmentId, boolean exactMatch) {
        List<Department> children = departmentService.getSubDepartments(departmentId);
        int size = (children == null) ? 1 : children.size() + 1;
        String[] result = new String[size];
        int i = 0;
        result[i++] = AssociationIdUtils.createDepartmentIdentifier(departmentId) +
                (exactMatch ? "" : FileCatUtils.CATEGORY_SEPARATOR + "*");
        if (children != null) {
            for (Department child : children) {
                result[i++] = AssociationIdUtils.createDepartmentIdentifier(child.getId()) +
                        (exactMatch ? "" : FileCatUtils.CATEGORY_SEPARATOR + "*");
            }
        }
        return result;
    }

    String[] convertDocTypeParentsToFilterValue(Long docTypeId) {
        Optional<DocType> docTypeOpt = docTypeRepository.findById(docTypeId);
        if (docTypeOpt.isPresent()) {
            DocType docType = docTypeOpt.get();
            Set<Long> scopeIds = scopeService.resolveScopeIds(docType.getScope());
            return generateParentScopedDocTypeFilterValues(docType.getDocTypeIdentifier(), scopeIds, docType.getScope() == null);
        } else {
            logger.error("filter by non existing docTypeId {}", docTypeId);
            return new String[]{NOT_DEF_STR_FIELD};
        }
    }

    public List<String> createTimeHistogramQueryValues() {
        List<String> queryValues = new ArrayList<>();
        //THIS might Break down on Solr 5.3
        queryValues.add("[NOW/DAY-1MONTH TO NOW/DAY+1DAY]");
        queryValues.add("[NOW/DAY-3MONTHS TO NOW/DAY-1MONTH]");
        queryValues.add("[NOW/DAY-6MONTHS TO NOW/DAY-3MONTH]");
        queryValues.add("[NOW/DAY-12MONTHS TO NOW/DAY-6MONTH]");
        queryValues.add("[NOW/DAY-5YEARS TO NOW/DAY-12MONTH]");
        queryValues.add("[* TO NOW/DAY-5YEARS]");
        queryValues.add("(-*)");
        return queryValues;
    }

//    public Map<String, SimpleDateRangeItem> createTimeHistogramQueryValues(List<SimpleDateRangeItem> simpleDateRangeItems) {
//        Map<String, SimpleDateRangeItem> result = new HashMap<>();
//        DateRangePoint previousStart = null;
//        for (SimpleDateRangeItem simpleDateRangeItem : simpleDateRangeItems) {
//            DateRangePoint start = simpleDateRangeItem.getStart();
//            DateRangePoint end = simpleDateRangeItem.getEnd() != null ? simpleDateRangeItem.getEnd() : previousStart;
//            String filter = null;
//            switch (start.getType()) {
//                case ABSOLUTE:
//                    throw new RuntimeException("Absolute dateTime is not supported yet");
//                case RELATIVE:
//                    filter = buildDateTimeFilter(start, end);
//            }
//            result.put(filter, simpleDateRangeItem);
//            previousStart = start;
//        }
//
//        return result;
//    }

    private String buildDateTimeFilter(DateRangePoint start, DateRangePoint end) {
        StringBuilder sb = new StringBuilder("[");
        if (start == null) { // DEPRECATED BEHAVIOR
            //The end of times.
            sb.append("*");
        } else {
            DateRangePointType type = start.getType() == null ? DateRangePointType.RELATIVE : start.getType();
            switch (type) {
                case ABSOLUTE:
                    addSolrAbsoluteDateRangeFilter(start.getAbsoluteTime().getTime(), sb);
                    break;
                case RELATIVE:
                    //TODO - use round HOURS and DAYS in some cases
                    addSolrRelativeDateRangeFilter(start.getRelativePeriodAmount(), start.getRelativePeriod(), sb);
                    break;
                case EVER:
                    addSolrAbsoluteDateRangeFilter(ONE_YEAR_MILLIS, sb); //starts from 1/1/1971
                    break;
                case UNAVAILABLE:
                    sb.append("*");
                    break;
            }
        }
        sb.append(" TO ");
        if (end == null) {
            sb.append("NOW"); //TODO - maybe use NOW/DAY+1DAY
        } else if (end.getType() == DateRangePointType.UNAVAILABLE) {
            addSolrAbsoluteDateRangeFilter(ONE_YEAR_MILLIS, sb); //starts from 1/1/1971
        } else {
            if (end.getRelativePeriod() == null) {
                addSolrAbsoluteDateRangeFilter(end.getAbsoluteTime().getTime(), sb);
            } else {
                addSolrRelativeDateRangeFilter(end.getRelativePeriodAmount(), end.getRelativePeriod(), sb);
            }
        }
        sb.append("]");
        return sb.toString();
    }

    String buildNumberRangeFilter(long from, long to) {
        return "[" + from +
                " TO " +
                to;
    }

    public String buildDateTimeFilter(DateRangePointDto start, DateRangePointDto end) {
        StringBuilder sb = new StringBuilder("[");
        if (start == null) { //DEPRECATED behavior
            //The end of times.
            sb.append("*");
        } else {
            switch (start.getType()) {
                case ABSOLUTE:
                    addSolrAbsoluteDateRangeFilter(start.getAbsoluteTime(), sb);
                    break;
                case RELATIVE:
                    //TODO - use round HOURS and DAYS in some cases
                    if (start.getRelativePeriod() == null) {
                        throw new BadRequestException("start.relativePeriod", "null", BadRequestType.MISSING_FIELD);
                    }
                    addSolrRelativeDateRangeFilter(start.getRelativePeriodAmount(), start.getRelativePeriod(), sb);
                    break;
                case EVER:
                    addSolrAbsoluteDateRangeFilter(ONE_YEAR_MILLIS, sb); //starts from 1/1/1971
                    break;
                case UNAVAILABLE:
                    sb.append("*");
                    break;
            }
        }
        sb.append(" TO ");
        if (end == null) {
            sb.append("NOW"); //TODO - maybe use NOW/DAY+1DAY
        } else if (end.getType() == DateRangePointType.UNAVAILABLE) {
            addSolrAbsoluteDateRangeFilter(ONE_YEAR_MILLIS, sb); //starts from 1/1/1971
        } else {
            if (end.getRelativePeriod() == null) {
                addSolrAbsoluteDateRangeFilter(end.getAbsoluteTime(), sb);
//                throw new BadHttpRequestException("end.relativePeriod","null",BadRequestType.MISSING_FIELD);
            } else {
                addSolrRelativeDateRangeFilter(end.getRelativePeriodAmount(), end.getRelativePeriod(), sb);
            }
        }
        sb.append("]");
        return sb.toString();
    }

    private void addSolrAbsoluteDateRangeFilter(Long absoluteTime, StringBuilder sb) {
        DateTimeFormatter isoInstant = DateTimeFormatter.ISO_INSTANT;
        absoluteTime = absoluteTime < ONE_YEAR_MILLIS ? ONE_YEAR_MILLIS : absoluteTime; //min date is 1/1/1971
        String format = isoInstant.format(Instant.ofEpochMilli(absoluteTime));
        sb.append(format);
    }

    private void addSolrRelativeDateRangeFilter(Integer relativePeriodAmount, TimePeriod relativePeriod, StringBuilder sb) {

        if (relativePeriodAmount == 0) {
            sb.append("NOW");
            return;
        }

        // if the range provided by the user goes back to the past beyond 1/1/1971,
        // convert it to absolute date equall to 1/1/1971
        long relativePeriodInMillis = relativePeriodAmount * TimePeriod.periodsInMs(relativePeriod);
        if (timeSource.timeMillisAgo(relativePeriodInMillis) < ONE_YEAR_MILLIS) {
            addSolrAbsoluteDateRangeFilter(ONE_YEAR_MILLIS, sb);
            return;
        }

        // Year is represented by 12 months
        if (TimePeriod.YEARS.equals(relativePeriod)) {
            relativePeriodAmount = relativePeriodAmount * 12;
        }

        // Week is represented by 7 days
        if (TimePeriod.WEEKS.equals(relativePeriod)) {
            relativePeriodAmount = relativePeriodAmount * 7;
        }

        sb.append("NOW-").append(relativePeriodAmount);
        switch (relativePeriod) {
            case HOURS:
                sb.append(TimePeriod.HOURS.name());
                break;
            case DAYS:
            case WEEKS:
                sb.append(TimePeriod.DAYS.name());
                break;
            case MONTHS:
            case YEARS:
                sb.append(TimePeriod.MONTHS.name());
                break;
        }
    }

    public boolean isTimeHistogramValuesToDrop(final String label) {
        return LABEL2DROP.equals(label);
    }

    public List<SimpleBizListItemDto> extractBizListItemsFromSolrIds(Collection<String> extractedEntitiesIds) {
        //"blt.0.2 bli.0.2.agg_rule_Name erb.0.2.Name.aggregated er.Name"
        return extractedEntitiesIds.stream()
                .filter(StringUtils::isNotEmpty)
                .filter(f -> f.startsWith("b"))
                .map(this::extractBizListItemFromSolrId)
                .collect(Collectors.toList());
    }

    /**
     * @param solrString (blt.1.2 bli.1.2.2_EPSA)
     *                   Or bli.0.2.agg_rule_Name
     *                   "blt.0.2 bli.0.2.agg_rule_Name erb.0.2.Name.aggregated er.Name"
     */
    private SimpleBizListItemDto extractBizListItemFromSolrId(String solrString) {
        String[] split = StringUtils.split(solrString, ' ');
        SimpleBizListItemDto result = new SimpleBizListItemDto();
        boolean isConsumer = false;
        for (String s : split) {
            if (s.startsWith(FileDtoFieldConverter.BIZLIST_ITEM_SOLR_PREFIX)) {
                String[] items = StringUtils.split(s, '.');
                int bizListTypeId = Integer.valueOf(items[1]);
                long bizListId = Long.valueOf(items[2]);
                result.setBizListId(bizListId);
                result.setType(BizListItemType.valueOf(bizListTypeId));

                String bizListItemSid = items[3];
                if (bizListItemSid.startsWith(FileDtoFieldConverter.AGG_RULE_PREFIX)) {
                    result.setAggregated(true);
                } else {
                    result.setId(bizListItemSid);
                }

                if (bizListTypeId == BizListItemType.CONSUMERS.ordinal()) {
                    isConsumer = true;

                } else {
                    return result;
                }
            }
        }
        for (String s : split) {
            if (isConsumer && s.startsWith(FileDtoFieldConverter.EXTRACTION_RULE_SOLR_PREFIX)) {
                String[] items = StringUtils.split(s, '.');
                result.setRuleName(items[1]);
                if (result.isAggregated()) {
                    result.setId(items[1]);
                }
                return result;
            }

        }
        logger.error("Failed to extract BizListId from SolrId {}" + solrString);
        return null;
    }

    String convertSimpleDateRangeToFilterValue(String simpleDateRange) {
        if (simpleDateRange.startsWith("{")) {
            DateRangeItemDto dateRangeItemDto = convertJsonToDateRangeItemDto(simpleDateRange);
            return buildDateTimeFilter(dateRangeItemDto.getStart(), dateRangeItemDto.getEnd());
        } else if (simpleDateRange.contains("[")) { //CONCRETE QUERY STRING
            return simpleDateRange;
        } else if (simpleDateRange.contains("Z")) {
            //This is also a concrete SOLR QUERY STRING
            return simpleDateRange;
        } else {
            Long id = Long.valueOf(simpleDateRange);
            SimpleDateRangeItem one = simpleDateRangeItemRepository.getOne(id);
            return buildDateTimeFilter(one.getStart(), one.getEnd());
        }
    }

    boolean isOwnerDbWithQuotes() {
        return ownerDbWithQuotes;
    }

    public String[] convertScopedTagIdToFilterValue(String tagIdString) {
        if (tagIdString == null) {
            throw new BadRequestException("Can't convert null tag id to filter value", BadRequestType.ITEM_NOT_FOUND);
        }
        if ("*".equals(tagIdString)) { // Is that a real use case ??
            return new String[]{tagIdString}; // Support * matching
        }
        long tagId = Long.valueOf(tagIdString);
        FileTag tag = fileTagRepository.findById(tagId).orElse(null);
        if (tag == null) {
            logger.error("filter by non existing tagId {}", tagId);
            return new String[]{NOT_DEF_STR_FIELD};
        }

        FileTagType type = tag.getType();
        BasicScope tagTypeScope = type.getScope();

        Set<Long> scopeIds = scopeService.resolveScopeIds(tagTypeScope);

        return generatePriorityScopedTagFilterValues(tag, scopeIds, tagTypeScope == null, true);
    }

    private String[] generatePriorityScopedTagFilterValues(FileTag tag, Set<Long> scopeIds, boolean global, boolean withPriority) {
        String[] result = new String[global ? scopeIds.size() + 1 : scopeIds.size()];
        int index = 0;
        if (global) {
            result[0] = withPriority ? generatePriorityTagFilter(tag, FileCatUtils.GLOBAL_SCOPE_IDENTIFIER) :
                    generateTagFilter(tag, FileCatUtils.GLOBAL_SCOPE_IDENTIFIER);
            index++;
        }

        for (Long scopeId : scopeIds) {
            result[index] = withPriority ? generatePriorityTagFilter(tag, String.valueOf(scopeId)) :
                    generateTagFilter(tag, String.valueOf(scopeId));
            index++;
        }

        return result;
    }

    String[] convertScopedTagNameToFilterValue(String tagNamePartial) {
        List<FileTag> tags = fileTagRepository.findTagNameLike(tagNamePartial);
        if (tags == null || tags.isEmpty()) {
            logger.error("filter by non existing tag name {}", tagNamePartial);
            return new String[]{NOT_DEF_STR_FIELD};
        }

        List<String> all = new ArrayList<>();
        for (FileTag fileTag : tags) {
            FileTagType type = fileTag.getType();
            BasicScope tagTypeScope = type.getScope();

            Set<Long> scopeIds = scopeService.resolveScopeIds(tagTypeScope);

            String[] res = generatePriorityScopedTagFilterValues(fileTag, scopeIds, tagTypeScope == null, false);

            all.addAll(Arrays.asList(res));
        }
        return all.toArray(new String[all.size()]);
    }

    String[] convertFuncRoleNameToFilterValue(String funcRoleNamePartial) {
        List<FunctionalRole> froles = functionalRoleRepository.findFuncRoleNameLike(funcRoleNamePartial);
        if (froles == null || froles.isEmpty()) {
            logger.error("filter by non existing data role name {}", funcRoleNamePartial);
            return new String[]{NOT_DEF_STR_FIELD};
        }

        List<String> all = new ArrayList<>();
        for (FunctionalRole role : froles) {
            all.add(AssociationIdUtils.createFunctionalRoleIdentfier(role.getId()) + FileCatUtils.CATEGORY_SEPARATOR + "*");
        }
        return all.toArray(new String[all.size()]);
    }

    String[] convertScopedTagTypeNameToFilterValue(String tagTypeNamePartial) {
        List<FileTagType> tagTypes = fileTagTypeRepository.findTagTypeNameLike(tagTypeNamePartial);
        if (tagTypes == null || tagTypes.isEmpty()) {
            logger.error("filter by non existing tag Type name {}", tagTypeNamePartial);
            return new String[]{NOT_DEF_STR_FIELD};
        }

        List<String> all = new ArrayList<>();
        for (FileTagType fileTagType : tagTypes) {
            String[] res = generateScopedTagTypeFilterValues(fileTagType,
                    scopeService.resolveScopeIds(fileTagType.getScope()),
                    fileTagType.getScope() == null);
            all.addAll(Arrays.asList(res));
        }
        return all.toArray(new String[all.size()]);
    }

    String[] convertScopedTagTypeToFilterValue(String tagTypeId) {
        if ("*".equals(tagTypeId)) {
            return new String[]{tagTypeId};
        }

        Optional<FileTagType> fileTagTypeOpt = fileTagTypeRepository.findById(Long.valueOf(tagTypeId));
        if (fileTagTypeOpt.isPresent()) {
            FileTagType fileTagType = fileTagTypeOpt.get();
            return generateScopedTagTypeFilterValues(fileTagType,
                    scopeService.resolveScopeIds(fileTagType.getScope()),
                    fileTagType.getScope() == null);
        } else {
            logger.error("filter by non existing tagTypeId {}", tagTypeId);
            return new String[]{NOT_DEF_STR_FIELD};
        }
    }

    private String[] generateScopedTagTypeFilterValues(FileTagType fileTagType, Set<Long> scopeIds, boolean global) {

        List<String> result = new LinkedList<>();


        if (global) {
            result.add(generatePriorityTagTypeFilter(fileTagType, FileCatUtils.GLOBAL_SCOPE_IDENTIFIER));
        }

        scopeIds.forEach(scopeId -> result.add(generatePriorityTagTypeFilter(fileTagType, String.valueOf(scopeId))));

        return result.toArray(new String[]{});
    }

    private String generatePriorityTagTypeFilter(FileTagType fileTagType, String scopeIdentifier) {
        return scopeIdentifier + FileCatUtils.CATEGORY_SEPARATOR + fileTagType.getId() + FileCatUtils.CATEGORY_SEPARATOR + "*";
    }

    private String generatePriorityTagFilter(FileTag fileTag, String scopeIdentifier) {
        return scopeIdentifier +
                FileCatUtils.CATEGORY_SEPARATOR +
                fileTag.getType().getId() +
                FileCatUtils.CATEGORY_SEPARATOR +
                fileTag.getId() +
                FileCatUtils.CATEGORY_SEPARATOR +
                "*";
    }

    private String generateTagFilter(FileTag fileTag, String scopeIdentifier) {
        return scopeIdentifier +
                FileCatUtils.CATEGORY_SEPARATOR +
                fileTag.getType().getId() +
                FileCatUtils.CATEGORY_SEPARATOR +
                fileTag.getId();
    }

    String[] convertDepartmentToFilterValue(Long departmentId) {
        return new String[]{AssociationIdUtils.createDepartmentIdentifier(departmentId) + FileCatUtils.CATEGORY_SEPARATOR + "*"};
    }

    String[] convertScopedDocTypesNameToFilterValue(String docTypeNamePart) {
        List<DocType> docTypes = docTypeRepository.findByNameLike(docTypeNamePart);

        if (docTypes == null || docTypes.isEmpty()) {
            logger.error("filter by non existing doc Type name part {}", docTypeNamePart);
            return new String[]{NOT_DEF_STR_FIELD};
        }

        List<String> all = new ArrayList<>();
        for (DocType docType : docTypes) {
            Set<Long> scopeIds = scopeService.resolveScopeIds(docType.getScope());
            String[] res = generateParentScopedDocTypeFilterValues(docType.getDocTypeIdentifier(), scopeIds, docType.getScope() == null);
            all.addAll(Arrays.asList(res));
        }
        return all.toArray(new String[all.size()]);
    }

    String[] convertScopedDocTypesToFilterValue(String docTypeId) {
        if ("*".equals(docTypeId)) {
            return new String[]{docTypeId};
        }

        DocType docType = docTypeRepository.findById(Long.valueOf(docTypeId)).orElse(null);
        if (docType == null) {
            logger.error("filter by non existing docTypeId {}", docTypeId);
            return new String[]{NOT_DEF_STR_FIELD};
        }
        Set<Long> scopeIds = scopeService.resolveScopeIds(docType.getScope());
        return generatePriorityScopedDocTypeFilterValues(docType.getDocTypeIdentifier(), scopeIds, docType.getScope() == null);
    }

    private String[] generatePriorityScopedDocTypeFilterValues(String docTypeIdentifier, Set<Long> scopeIds, boolean global) {
        String[] result = new String[global ? scopeIds.size() + 1 : scopeIds.size()];
        int index = 0;
        if (global) {
            result[0] = generatePriorityDocTypeFilter(docTypeIdentifier, FileCatUtils.GLOBAL_SCOPE_IDENTIFIER);
            index++;
        }

        for (Long scopeId : scopeIds) {
            result[index] = generatePriorityDocTypeFilter(docTypeIdentifier, String.valueOf(scopeId));
            index++;
        }

        return result;
    }

    private String generatePriorityDocTypeFilter(String docTypeIdentifier, String scopeIdentifier) {
        return DocTypeDto.ID_PREFIX + scopeIdentifier + FileCatUtils.CATEGORY_SEPARATOR + AssociationIdUtils.docTypeIdHierarchy(docTypeIdentifier) + "..*";
    }

    private String[] generateParentScopedDocTypeFilterValues(String docTypeIdentifier, Set<Long> scopeIds, boolean global) {
        String[] result = new String[(global ? scopeIds.size() + 1 : scopeIds.size()) * 2];

        String wildCard = FileCatUtils.CATEGORY_SEPARATOR + "*";

        int index = 0;
        if (global) {
            result[0] = AssociationIdUtils.createScopedDocTypeIdentifier(docTypeIdentifier, null) + wildCard;
            result[1] = AssociationIdUtils.createScopedDocTypeIdentifier(docTypeIdentifier, null);
            index = 2;
        }

        for (Long scopeId : scopeIds) {
            result[index] = AssociationIdUtils.createScopedDocTypeIdentifier(docTypeIdentifier, scopeId) + wildCard;
            result[index + 1] = AssociationIdUtils.createScopedDocTypeIdentifier(docTypeIdentifier, scopeId);
            index += 2;
        }

        return result;
    }

    public static DateRangeItemDto convertJsonToDateRangeItemDto(String simpleDateRange) {
        try {
            return ModelDtoConversionUtils.getMapper().readValue(simpleDateRange, DateRangeItemDto.class);
        } catch (IOException e) {
            logger.error("Failed to convert JSON {} to DateRangeItemDto", simpleDateRange);
            throw new BadRequestException("Failed to convert JSON to DateRangeItemDto", e, BadRequestType.UNSUPPORTED_VALUE);
        }
    }

    public SolrSpecification buildSolrFacetJsonSpecification(DataSourceRequest dataSourceRequest,
                                                             List<SolrFacetQueryJson> facetJsonObjectList,
                                                             String...additionalFilters) {

        licenseService.validateLoginExpiry();
        SolrSpecification solrSpecification = new SolfFacetJsonSpecification(dataSourceRequest.getFilter(),
                facetJsonObjectList);

        Stream.of(additionalFilters).forEach(solrSpecification::setAdditionalFilter);

        if (dataSourceRequest.getContentFilter() != null) {
            solrSpecification.addToContentFilter(dataSourceRequest.getContentFilter());
        }

        addDeletedFilesLimitation(dataSourceRequest, solrSpecification);
        addDeletedRootFolderLimitation(dataSourceRequest, solrSpecification);

        if (dataSourceRequest.getQueryMinCount() > 0) {
            solrSpecification.setMinCount(dataSourceRequest.getQueryMinCount());
        }
        solrSpecification.setGroupJoinFilter(dataSourceRequest.getGroupJoinFilter());

        solrSpecification.setPageRequest(dataSourceRequest.isWithoutPagenation() ?
                PageRequest.of(0, Integer.MAX_VALUE) : dataSourceRequest.createPageRequest());

        solrSpecification.setSelectedItemId(dataSourceRequest.getSelectedItemId());
        return solrSpecification;
    }

    public SolrSpecification buildSolrFacetJsonSpecification(DataSourceRequest dataSourceRequest) {
        return buildSolrFacetJsonSpecification(dataSourceRequest, Lists.newArrayList());
    }

    public SolrSpecification buildTermJsonFacetSpecification(DataSourceRequest dataSourceRequest,
                                                             CatFileFieldType catFileFieldType) {
        SolrFacetQueryJson theFacet = new SolrFacetQueryJson(catFileFieldType.getSolrName());
        theFacet.setType(FacetType.TERMS);
        theFacet.setField(catFileFieldType);
        if (dataSourceRequest.getSortBy() != null) {
            theFacet.setSortField(dataSourceRequest.getSortBy().getName());
        }
        theFacet.setMincount(dataSourceRequest.getQueryMinCount());
        if (dataSourceRequest.getQueryPrefix() != null) {
            theFacet.setPrefix(dataSourceRequest.getQueryPrefix());
        }
        /*SolrFacetQueryJson innerFacet = new SolrFacetQueryJson(CatFileFieldType.SIZE.getSolrName());
        innerFacet.setType(FacetType.SIMPLE);
        innerFacet.setField(CatFileFieldType.SIZE);
        innerFacet.setFunc(FacetFunction.SUM);
        theFacet.addFacet(innerFacet);*/

        return buildSolrFacetJsonSpecification(dataSourceRequest,Lists.newArrayList(theFacet));

    }
}
