package com.cla.filecluster.domain.specification;

import com.cla.common.constants.CatFileFieldType;
import com.cla.filecluster.repository.solr.query.Criteria;
import com.cla.filecluster.repository.solr.query.Query;
import com.cla.filecluster.repository.solr.query.SolrOperator;
import com.cla.filecluster.repository.solr.query.SolrQueryGenerator;
import com.cla.filecluster.service.convertion.FileDtoFieldConverter;
import com.cla.common.domain.dto.kendo.FilterDescriptor;
import com.cla.filecluster.domain.entity.pv.ClaFile;
import org.apache.commons.collections.CollectionUtils;

import javax.persistence.criteria.*;

/**
 * Created by uri on 09/07/2015.
 */
public class ClaFileByUserGroupSpecification extends KendoFilterSpecification<ClaFile> {

    private String groupId;

    public ClaFileByUserGroupSpecification(FilterDescriptor filter, String groupId) {
        super(ClaFile.class, filter);
        this.groupId = groupId;
    }

    public Query toQuery() {
        Query base = Query.create();

        if (groupId != null && !groupId.isEmpty()) {
            Query groupIdFilter = Query.create().addFilterWithCriteria(Criteria.create(CatFileFieldType.USER_GROUP_ID, SolrOperator.EQ, groupId));
            base.addFilterQuery(groupIdFilter);
        }

        if (filter == null || CollectionUtils.isEmpty(filter.getFilters())) {
            //Return only the group filter
            return base;
        }
        else {
            Query filterPredicate = buildFilter(this.filter);
            base.addFilterQuery(filterPredicate); // TODO support AND ???
            return base;
        }
    }

    @Override
    public Predicate toPredicate(Root<ClaFile> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
        //Join group and folder
        Class clazz = query.getResultType();
        if (clazz.equals(ClaFile.class)) {
            //Prevent N+1 problem. see http://jdpgrailsdev.github.io/blog/2014/09/09/spring_data_hibernate_join.html
           // root.fetch("docFolder", JoinType.LEFT);
           // root.fetch("userGroup", JoinType.LEFT);
        }
        Path<Object> userGroupId= root.get("userGroupId");
        Path<Object> deleted = root.get("deleted");
        Predicate groupFilter = cb.equal(userGroupId, groupId);
        Predicate deletedFilter = cb.equal(deleted,Boolean.FALSE);
        Predicate baseFilter = cb.and(groupFilter,deletedFilter);
        if (filter == null || CollectionUtils.isEmpty(filter.getFilters())) {
            //Return only the group filter
            return baseFilter;
        }
        else {
            Predicate filterPredicate = buildFilter(this.filter, root, query, cb);
            return cb.and(baseFilter,filterPredicate);
        }
    }

    protected String convertModelField(String dtoField) {
        return FileDtoFieldConverter.convertDtoField(dtoField);
    }

    @Override
    protected boolean isDateField(String dtoField) {
        return FileDtoFieldConverter.isDateField(dtoField);
    }
}
