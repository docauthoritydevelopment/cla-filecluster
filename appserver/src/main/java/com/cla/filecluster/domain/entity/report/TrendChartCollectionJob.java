package com.cla.filecluster.domain.entity.report;

import com.cla.common.domain.dto.security.UserDto;
import com.cla.filecluster.domain.entity.security.ClaUserDetails;
import com.cla.filecluster.service.report.TrendChartAppService;
import com.cla.filecluster.service.security.UserService;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;

import static com.cla.filecluster.domain.entity.schedule.RescanJob.RESCAN_GROUP;
import static com.cla.filecluster.service.convertion.FileDtoFieldConverter.DtoFields.groupId;

/**
 * Created by uri on 27/07/2016.
 */
@Component
@Scope("prototype")
public class TrendChartCollectionJob implements Runnable {

    private static final Logger logger = LoggerFactory.getLogger(TrendChartCollectionJob.class);

    @Autowired
    TrendChartAppService trendChartAppService;

    @Autowired
    UserService userService;

    private long requestTimeStampRoundingFactor;

    @Override
    public void run() {
        if (trendChartAppService == null) {
            logger.error("trendChartAppService was not autowired properly");
        }
        try {
            fillAuthentication();
            long requestedTimeStamp = calculateRequestedTimeStamp();
            trendChartAppService.collectTrendChartsData(requestedTimeStamp);
        } catch (RuntimeException e) {
            logger.error("Failed to execute schedule job on group {} ({})",groupId, e.getMessage(), e);
        } catch (Exception e) {
            logger.error("Exception during RescanJob on group {} ({})",groupId, e.getMessage(), e);
        } catch (Error e) {
            logger.error("Irrecoverable error during RescanJob on group {} ({})",groupId, e.getMessage(), e);
        }
    }

    /**
     *
     * @return requestedTimeStamp - rounded down to the nearest hour
     */
    private long calculateRequestedTimeStamp() {
        LocalDateTime localDateTime = LocalDateTime.now().truncatedTo(ChronoUnit.HOURS);
        return localDateTime.toEpochSecond(ZoneOffset.UTC) * 1000;
    }

    private void fillAuthentication() {
        UserDto userDto = userService.getScheduleUserDto();
        ClaUserDetails claUserDetails = new ClaUserDetails(userDto);
        logger.debug("Fill authentication for schedule - set user");
        Authentication auth = new UsernamePasswordAuthenticationToken(claUserDetails, userDto.getPassword(),
                claUserDetails.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(auth);
    }

    public void execute(JobExecutionContext context) throws JobExecutionException {
        JobDataMap dataMap = context.getJobDetail().getJobDataMap();
        Long groupId = dataMap.getLong(RESCAN_GROUP);
        logger.debug("Executing rescan job on schedule group {}",groupId);
    }

    public long getRequestTimeStampRoundingFactor() {
        return requestTimeStampRoundingFactor;
    }

    public void setRequestTimeStampRoundingFactor(long requestTimeStampRoundingFactor) {
        this.requestTimeStampRoundingFactor = requestTimeStampRoundingFactor;
    }
}
