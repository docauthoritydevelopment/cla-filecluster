package com.cla.filecluster.domain.entity.license;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Map;

/**
 * Created by uri on 27/06/2016.
 */
@Entity
public class DocAuthorityLicense implements Serializable {

    @Id
    private long id;

    private long creationDate;

    private long applyDate;

    private String registeredTo;

    private String issuer;

    private String signature;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(long creationDate) {
        this.creationDate = creationDate;
    }

    public long getApplyDate() {
        return applyDate;
    }

    public void setApplyDate(long applyDate) {
        this.applyDate = applyDate;
    }

    public String getRegisteredTo() {
        return registeredTo;
    }

    public void setRegisteredTo(String registeredTo) {
        this.registeredTo = registeredTo;
    }

    public String getIssuer() {
        return issuer;
    }

    public void setIssuer(String issuer) {
        this.issuer = issuer;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    @Override
    public String toString() {
        return "DocAuthorityLicense{" +
                "id=" + id +
                ", creationDate=" + creationDate +
                ", applyDate=" + applyDate +
                ", registeredTo='" + registeredTo + '\'' +
                ", issuer='" + issuer + '\'' +
                ", signature='" + signature + '\'' +
                '}';
    }
}
