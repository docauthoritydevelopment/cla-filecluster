package com.cla.filecluster.domain.entity.media;

import com.cla.connector.domain.dto.media.MediaType;
import com.cla.connector.domain.dto.media.MediaAuthenticationType;
import com.cla.filecluster.domain.entity.security.User;

import javax.persistence.*;

/**
 * Created by uri on 08/08/2016.
 */
@Entity
public class MediaConnectionDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Access(AccessType.PROPERTY)
    private Long id;

    private String name;

    private MediaType mediaType;

    private MediaAuthenticationType mediaAuthenticationType;

    @Column(length = 4096)
    @Basic(fetch = FetchType.EAGER)
    private String connectionDetailsJson;

    @ManyToOne
    private User owner;

    private String url;

    private String username;

    private boolean pstCacheEnabled = true;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static MediaConnectionDetails create(){
        return new MediaConnectionDetails();
    }

    public String getConnectionDetailsJson() {
        return connectionDetailsJson;
    }

    public void setConnectionDetailsJson(String connectionDetailsJson) {
        this.connectionDetailsJson = connectionDetailsJson;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public MediaType getMediaType() {
        return mediaType;
    }

    public MediaConnectionDetails setMediaType(MediaType mediaType) {
        this.mediaType = mediaType;
        return this;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public boolean isPstCacheEnabled() {
        return pstCacheEnabled;
    }

    public MediaConnectionDetails setPstCacheEnabled(boolean pstCacheEnabled) {
        this.pstCacheEnabled = pstCacheEnabled;
        return this;
    }

    public MediaAuthenticationType getMediaAuthenticationType() {
        return mediaAuthenticationType;
    }

    public void setMediaAuthenticationType(MediaAuthenticationType mediaAuthenticationType) {
        this.mediaAuthenticationType = mediaAuthenticationType;
    }

    @Override
    public String toString() {
        return "MediaConnectionDetails{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", mediaType=" + mediaType +
                ", url='" + url + '\'' +
                '}';
    }
}
