package com.cla.filecluster.domain.exceptions;

import com.cla.connector.domain.dto.error.BadRequestType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * Created by uri on 07/07/2015.
 */
@ResponseStatus(value=HttpStatus.BAD_REQUEST, reason="Bad request")
public class BadHttpRequestException extends RuntimeException {

    private Logger logger = LoggerFactory.getLogger(BadHttpRequestException.class);
    private BadRequestType type;

    public BadHttpRequestException(String requestField, Object value, BadRequestType type) {
        super("Bad Http Request - request field ["+requestField+ "] has invalid value ["+value+"]");
        this.type = type;
        logger.warn(this.getMessage());
    }

    public BadHttpRequestException(String requestField, Object value,Exception cause, BadRequestType type) {
        super("Bad Http Request - request field ["+requestField+ "] has invalid value ["+value+"]",cause);
        this.type = type;
        logger.warn(this.getMessage(),cause);
    }

    public BadHttpRequestException(String requestField, Object value, String reason, BadRequestType type) {
        super("Bad Http Request - request field ["+requestField+ "] has invalid value ["+value+"] due to ["+reason+"]");
        this.type = type;
        logger.warn(this.getMessage(),reason);
    }

    public BadHttpRequestException(String reason, Exception e, BadRequestType type) {
        super("Bad Http Request - ["+reason+"]",e);
        this.type = type;
        logger.warn(this.getMessage(),e);
    }

    public BadHttpRequestException(String reason, BadRequestType type) {
        super(reason);
        this.type = type;
    }

    public BadRequestType getType() {
        return type;
    }

    public void setType(BadRequestType type) {
        this.type = type;
    }
}
