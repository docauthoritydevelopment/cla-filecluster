package com.cla.filecluster.domain.entity.audit;

import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Oren, Itay on 2017-10-10.
 *
 * DaAuditEvent class is for READ-ONLY of the audit table events.
 * Events are created by the EventAuditService using logger DB appender.
 * This entity is created so the table is created and for retrieving items for reporting and BI purposes.
 *
 * Changes in fields/columns should be in line with the columns definition at com.cla.filecluster.service.audit.AuditColumns
 *      and com.cla.filecluster.service.audit.AuditDBAppender
 *
 */

@Entity
@IdClass(DaAuditEventPK.class)
@Table( name = "audit")
public class DaAuditEvent implements Serializable {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Access(AccessType.PROPERTY)
    @Id
    @Column(name = "event_id")
    private long id;

    @Id
    @Column(name = "time_stamp")
    private long timeStampMs;

    @Column(name = "username")
    private String username;

    @Column(name = "sub_system")
    private String subSystem;

    @Length(max = 1024)
    @Column(name = "message")
    private String message;

    @Column(name = "action")
    private String action;

    @Column(name = "object_type")
    private String objectType;

    @Length(max = 1024)
    @Column(name = "object_id")
    private String objectId;

    @Column(name = "on_object_type")
    private String onObjectType;

    @Length(max = 1024)
    @Column(name = "on_object_id")
    private String onObjectId;

    public static final int PARAMS_FIELD_MAX_LENGTH = 2047;

    @Length(max = PARAMS_FIELD_MAX_LENGTH+1)
    @Column(name = "params")
    private String params;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getTimeStampMs() {
        return timeStampMs;
    }

    public void setTimeStampMs(long timeStampMs) {
        this.timeStampMs = timeStampMs;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSubSystem() {
        return subSystem;
    }

    public void setSubSystem(String subSystem) {
        this.subSystem = subSystem;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getObjectType() {
        return objectType;
    }

    public void setObjectType(String objectType) {
        this.objectType = objectType;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public String getOnObjectType() {
        return onObjectType;
    }

    public void setOnObjectType(String onObjectType) {
        this.onObjectType = onObjectType;
    }

    public String getOnObjectId() {
        return onObjectId;
    }

    public void setOnObjectId(String onObjectId) {
        this.onObjectId = onObjectId;
    }

    public String getParams() {
        return params;
    }

    public void setParams(String params) {
        this.params = params;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DaAuditEvent that = (DaAuditEvent) o;

        if (id != that.id) return false;
        if (timeStampMs != that.timeStampMs) return false;
        if (username != null ? !username.equals(that.username) : that.username != null) return false;
        if (subSystem != null ? !subSystem.equals(that.subSystem) : that.subSystem != null) return false;
        if (message != null ? !message.equals(that.message) : that.message != null) return false;
        if (action != null ? !action.equals(that.action) : that.action != null) return false;
        if (objectType != null ? !objectType.equals(that.objectType) : that.objectType != null) return false;
        if (objectId != null ? !objectId.equals(that.objectId) : that.objectId != null) return false;
        if (onObjectType != null ? !onObjectType.equals(that.onObjectType) : that.onObjectType != null) return false;
        if (onObjectId != null ? !onObjectId.equals(that.onObjectId) : that.onObjectId != null) return false;
        return params != null ? params.equals(that.params) : that.params == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (int) (timeStampMs ^ (timeStampMs >>> 32));
        result = 31 * result + (username != null ? username.hashCode() : 0);
        result = 31 * result + (subSystem != null ? subSystem.hashCode() : 0);
        result = 31 * result + (message != null ? message.hashCode() : 0);
        result = 31 * result + (action != null ? action.hashCode() : 0);
        result = 31 * result + (objectType != null ? objectType.hashCode() : 0);
        result = 31 * result + (objectId != null ? objectId.hashCode() : 0);
        result = 31 * result + (onObjectType != null ? onObjectType.hashCode() : 0);
        result = 31 * result + (onObjectId != null ? onObjectId.hashCode() : 0);
        result = 31 * result + (params != null ? params.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("DaAuditEvent{");
        sb.append("id=").append(id);
        sb.append(", timeStampMs=").append(timeStampMs);
        sb.append(", username='").append(username).append('\'');
        sb.append(", subSystem='").append(subSystem).append('\'');
        sb.append(", message='").append(message).append('\'');
        sb.append(", action='").append(action).append('\'');
        sb.append(", objectType='").append(objectType).append('\'');
        sb.append(", objectId='").append(objectId).append('\'');
        sb.append(", onObjectType='").append(onObjectType).append('\'');
        sb.append(", onObjectId='").append(onObjectId).append('\'');
        sb.append(", params='").append(params).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
