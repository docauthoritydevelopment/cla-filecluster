package com.cla.filecluster.domain.entity.categorization;

import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import com.cla.common.domain.dto.category.FileCategoryScope;
import com.cla.common.domain.dto.category.FileCategoryState;
import com.cla.common.domain.dto.category.FileCategoryType;
import com.cla.connector.domain.dto.JsonSerislizableVisible;

@Entity
@Table(name = "file_categories")
public class FileCategory implements JsonSerislizableVisible {
	@Id
	@GeneratedValue
	private Long id;
	

	@NotNull
	private String name;
	
	@OneToMany(orphanRemoval = true, cascade = CascadeType.ALL)
	@JoinColumn(name="category_id")
	private List<CategoryFilter> filters;

    private boolean staticCategory;

    @NotNull
    private FileCategoryScope scope;

    @NotNull
    private FileCategoryType type;

    @Enumerated(EnumType.STRING)
    private FileCategoryState state;

    @PrePersist
    public void prePersist() {
        if (state == null) {
            state = FileCategoryState.ACTIVE;
        }
    }

    public List<CategoryFilter> getFilters() {
        return filters;
    }

    public CategoryFilter getFirstFilter() {
        return filters.get(0);
    }

    public void setFilters(List<CategoryFilter> filters) {
        this.filters = filters;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isStaticCategory() {
        return staticCategory;
    }

    public void setStaticCategory(boolean staticCategory) {
        this.staticCategory = staticCategory;
    }

    public FileCategoryScope getScope() {
        return scope;
    }

    public void setScope(FileCategoryScope scope) {
        this.scope = scope;
    }

    public FileCategoryType getType() {
        return type;
    }

    public void setType(FileCategoryType type) {
        this.type = type;
    }

    public void setState(FileCategoryState state) {
        this.state = state;
    }

    public FileCategoryState getState() {
        return state;
    }
}
