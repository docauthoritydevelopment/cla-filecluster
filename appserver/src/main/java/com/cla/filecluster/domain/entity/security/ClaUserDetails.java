package com.cla.filecluster.domain.entity.security;

import com.cla.common.domain.dto.security.BizRoleDto;
import com.cla.common.domain.dto.security.SystemRoleDto;
import com.cla.common.domain.dto.security.UserDto;
import com.cla.common.domain.dto.security.UserState;
import com.cla.connector.utils.TimeSource;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import static java.util.stream.Collectors.toSet;

public class ClaUserDetails implements UserDetails {

    // Auto unlock 'locked' accounts after time since last login failure
    private static long autoUnlockTimeSec = 0;

    private UserDto user;

    public ClaUserDetails(final UserDto user) {
        this.user = user;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return (user == null) ? null : getAllUserRoles().stream()
                .flatMap(r -> r.getAuthorities().stream()).map(a -> new SimpleGrantedAuthority(a.toString())).collect(toSet());
    }

    private Collection<SystemRoleDto> getAllUserRoles() {
        Set<SystemRoleDto> allRoles = new HashSet<>();
        if (user.getBizRoleDtos() != null) {
            for (BizRoleDto bizRole : user.getBizRoleDtos()) {
                allRoles.addAll(bizRole.getTemplate().getSystemRoleDtos());
            }
        }
        allRoles.addAll(user.getSystemRoleDtos());
        return allRoles;
    }

    @Override
    public String getPassword() {
        return (user == null) ? null : user.getPassword();
    }

    @Override
    public String getUsername() {
        return (user == null) ? null : user.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        return (user == null) || user.isAccountNonExpired();
    }

    @Override
    public boolean isAccountNonLocked() {
        return (user == null) || checkLockedWithAutoUnlock();
    }

    private boolean checkLockedWithAutoUnlock() {
        if (user.isAccountNonLocked()) {
            return true;
        }
        if (getAutoUnlockTimeSec() > 0) {
            if (user.getLastFailedLogin() == null) {
                return false;
            }
            if (isLastFailureTimeoutPassed(user.getLastFailedLogin().getTime())) return true;
        }
        return false;
    }

    public static boolean isLastFailureTimeoutPassed(final long lastFailedLoginMs) {
        if (getAutoUnlockTimeSec() <= 0) {
            return false;
        }
        TimeSource timeSource = new TimeSource();
        return timeSource.secondsSince(lastFailedLoginMs) > getAutoUnlockTimeSec();
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return !user.isPasswordMustBeChanged();
    }

    @SuppressWarnings("SimplifiableIfStatement")
    @Override
    public boolean isEnabled() {
        if (user == null) {
            return true;
        }
        if (user.isDeleted()) {
            return false;
        }
        return user.getUserState() == UserState.ACTIVE;
    }

    public UserDto getUser() {
        return user;
    }

    public static long getAutoUnlockTimeSec() {
        return autoUnlockTimeSec;
    }

    public static void setAutoUnlockTimeSec(long autoUnlockTimeSec) {
        ClaUserDetails.autoUnlockTimeSec = autoUnlockTimeSec;
    }

    @Override
    public String toString() {
        return "ClaUserDetails{" +
                "user=" + user +
                '}';
    }
}
