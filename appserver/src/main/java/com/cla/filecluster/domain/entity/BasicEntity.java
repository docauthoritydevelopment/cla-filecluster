package com.cla.filecluster.domain.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Basic entity for hibernate for inheritance with generated id and dates for audit
 * Created by: yael
 * Created on: 12/13/2017
 */
@MappedSuperclass
public abstract class BasicEntity extends ModifiableEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Access(AccessType.PROPERTY)
    protected Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return
                "id=" + id +
                        ", dateCreated=" + getDateCreated() +
                        ", dateModified=" + getDateModified();
    }

}
