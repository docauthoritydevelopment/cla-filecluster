package com.cla.filecluster.domain.entity.bizlist;

import com.cla.common.domain.dto.bizlist.BizListItemSource;
import com.cla.common.domain.dto.bizlist.BizListItemState;
import com.cla.common.domain.dto.bizlist.BizListItemType;
import com.cla.common.domain.dto.bizlist.SimpleBizListItemInterface;
import com.cla.filecluster.domain.entity.filetag.AssociableEntity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * Created by uri on 31/12/2015.
 */
@Entity
@Table(
		name = "bl_item_simple",
		indexes = {@Index(name = "state_idx", columnList = "state")})
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
public class SimpleBizListItem implements Serializable, AssociableEntity, SimpleBizListItemInterface {

    @Id
    String sid;

    @NotNull
    String entityId;

    String name;

    @ManyToOne(fetch = FetchType.EAGER,optional = false)
    BizList bizList;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "entity", cascade = CascadeType.REMOVE)
    List<BizListItemAlias> bizListItemAliases;

    @NotNull
    private BizListItemState state;

    private BizListItemSource source;

    /**
     * Marks that an item was imported as part of a particular batch
     */
    private Long importId;

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<BizListItemAlias> getBizListItemAliases() {
        return bizListItemAliases;
    }

    public void setBizListItemAliases(List<BizListItemAlias> bizListItemAliases) {
        this.bizListItemAliases = bizListItemAliases;
    }

    public BizList getBizList() {
        return bizList;
    }

    public void setBizList(BizList bizList) {
        this.bizList = bizList;
    }

    public void generateSid() {
        if (entityId == null) {
            throw new RuntimeException("Unable to generate SID for SimpleBizListItem - missing business ID");
        }
        this.sid = generateSid(bizList.getId(), entityId);
    }

    public static String generateSid(long bizListId, String entityId) {
        return bizListId + "_"+entityId.replace('.','-').replace(' ','-');
    }

    public BizListItemState getState() {
        return state;
    }

    public void setState(BizListItemState state) {
        this.state = state;
    }

    public void setImportId(Long importId) {
        this.importId = importId;
    }

    public Long getImportId() {
        return importId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SimpleBizListItem that = (SimpleBizListItem) o;

        return sid != null ? sid.equals(that.sid) : that.sid == null;

    }

    @Override
    public int hashCode() {
        return sid != null ? sid.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "SimpleBizListItem{" +
                "sid='" + sid + '\'' +
                ", entityId='" + entityId + '\'' +
                ", name='" + name + '\'' +
                ", bizList=" + bizList +
                '}';
    }

    @Override
    public BizListItemType getListType() {
        return getBizList().getBizListItemType();
    }

    @Override
    public Long getBizListId() {
        return getBizList().getId();
    }

    @Override
    public String createIdentifierString() {
        return createBizListIdentifierString();
    }
}
