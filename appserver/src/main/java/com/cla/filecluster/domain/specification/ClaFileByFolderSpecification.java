package com.cla.filecluster.domain.specification;

import com.cla.common.constants.CatFileFieldType;
import com.cla.filecluster.repository.solr.query.Criteria;
import com.cla.filecluster.repository.solr.query.Query;
import com.cla.filecluster.repository.solr.query.SolrOperator;
import com.cla.filecluster.service.convertion.FileDtoFieldConverter;
import com.cla.common.domain.dto.kendo.FilterDescriptor;
import com.cla.filecluster.domain.entity.pv.ClaFile;
import org.apache.commons.collections.CollectionUtils;

import javax.persistence.criteria.*;

/**
 * Created by uri on 09/07/2015.
 */
public class ClaFileByFolderSpecification extends KendoFilterSpecification<ClaFile> {

    protected long folderId;

    public ClaFileByFolderSpecification(FilterDescriptor filter, long folderId) {
        super(ClaFile.class, filter);
        this.folderId= folderId;
    }

    @Override
    public Predicate toPredicate(Root<ClaFile> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
        //This part allow to use this specification in pageable queries
        Class clazz = query.getResultType();
        if (clazz.equals(ClaFile.class)) {
            //Prevent N+1 problem. see http://jdpgrailsdev.github.io/blog/2014/09/09/spring_data_hibernate_join.html
            root.fetch("userGroup", JoinType.LEFT);
            root.fetch("contentMetadata",JoinType.LEFT);
        }

        Path<Object> docFolderId= root.get("docFolderId");
        Predicate folderFilter = cb.equal(docFolderId, folderId);
        if (filter == null || CollectionUtils.isEmpty(filter.getFilters())) {
            //Return only the group filter
            return folderFilter;
        }
        else {
            Predicate filterPredicate = buildFilter(this.filter, root, query, cb);
            return cb.and(folderFilter,filterPredicate);
        }
    }

    public Query toQuery() {
        Query base = Query.create();

        if (folderId > 0) {
            Query rfFilter = Query.create().addFilterWithCriteria(Criteria.create(CatFileFieldType.FOLDER_ID, SolrOperator.EQ, folderId));
            base.addFilterQuery(rfFilter);
        }

        if (filter == null || CollectionUtils.isEmpty(filter.getFilters())) {
            //Return only the group filter
            return base;
        }
        else {
            Query filterPredicate = buildFilter(this.filter);
            base.addFilterQuery(filterPredicate); // TODO support AND ???
            return base;
        }
    }

    protected String convertModelField(String dtoField) {
        return FileDtoFieldConverter.convertDtoField(dtoField);
    }

    @Override
    protected boolean isDateField(String dtoField) {
        return FileDtoFieldConverter.isDateField(dtoField);
    }

    public long getFolderId() {
        return folderId;
    }
}
