package com.cla.filecluster.domain.entity.alert;

import java.io.Serializable;

/**
 * Primary key representation for alert also contains timestamp for partitioning
 *
 * Created by: yael
 * Created on: 1/11/2018
 */
public class AlertPK implements Serializable {

    private long id;
    private long dateCreated;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(long dateCreated) {
        this.dateCreated = dateCreated;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AlertPK that = (AlertPK) o;

        if (id != that.id) return false;
        return dateCreated == that.dateCreated;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (int) (dateCreated ^ (dateCreated >>> 32));
        return result;
    }
}
