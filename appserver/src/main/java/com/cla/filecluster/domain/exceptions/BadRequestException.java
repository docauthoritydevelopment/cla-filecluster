package com.cla.filecluster.domain.exceptions;

import com.cla.connector.domain.dto.error.BadRequestType;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by: yael
 * Created on: 7/25/2019
 */
@ResponseStatus(value= HttpStatus.BAD_REQUEST)
public class BadRequestException extends RuntimeException {

    private BadRequestType type;

    public BadRequestException(String requestField, Object value, BadRequestType type) {
        super("Bad Http Request - request field ["+requestField+ "] has invalid value ["+value+"]");
        this.type = type;
    }

    public BadRequestException(String requestField, Object value,Exception cause, BadRequestType type) {
        super("Bad Http Request - request field ["+requestField+ "] has invalid value ["+value+"]",cause);
        this.type = type;
    }

    public BadRequestException(String requestField, Object value, String reason, BadRequestType type) {
        super("Bad Http Request - request field ["+requestField+ "] has invalid value ["+value+"] due to ["+reason+"]");
        this.type = type;
    }

    public BadRequestException(String reason, Exception e, BadRequestType type) {
        super("Bad Http Request - ["+reason+"]",e);
        this.type = type;
    }

    public BadRequestException(String reason, BadRequestType type) {
        super(reason);
        this.type = type;
    }

    public BadRequestType getType() {
        return type;
    }

    public void setType(BadRequestType type) {
        this.type = type;
    }
}
