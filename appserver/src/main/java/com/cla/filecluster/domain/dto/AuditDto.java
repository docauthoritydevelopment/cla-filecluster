package com.cla.filecluster.domain.dto;

import com.cla.filecluster.domain.entity.audit.DaAuditEvent;
import com.cla.filecluster.service.audit.AuditAction;
import com.cla.filecluster.domain.entity.audit.AuditType;

/**
 * Created by oren on 04/10/2017.
 */
public class AuditDto {

    private long id;

    private String username;

    private long timestamp;

    private AuditType auditType;

    private String message;

    private AuditAction crudAction;

    // Operation main entity type
    private String objectType;

    // Operation main entity id
    private String objectId;

    // Operation secondary entity type
    private String onObjectType;

    // Operation secondary entity id
    private String onObjectId;

    private String params;

    public AuditDto() {
    }

    public AuditDto(long id, String username, long timestamp, AuditType auditType, String message, AuditAction crudAction, String objectType, String objectId, String onObjectType, String onObjectId, String params) {
        this.id = id;
        this.username = username;
        this.timestamp = timestamp;
        this.auditType = auditType;
        this.message = message;
        this.crudAction = crudAction;
        this.objectType = objectType;
        this.objectId = objectId;
        this.params = (params.length() <= DaAuditEvent.PARAMS_FIELD_MAX_LENGTH) ? params : params.substring(0, DaAuditEvent.PARAMS_FIELD_MAX_LENGTH);
        this.onObjectType = onObjectType;
        this.onObjectId = onObjectId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public AuditType getAuditType() {
        return auditType;
    }

    public void setAuditType(AuditType auditType) {
        this.auditType = auditType;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public AuditAction getCrudAction() {
        return crudAction;
    }

    public void setCrudAction(AuditAction crudAction) {
        this.crudAction = crudAction;
    }

    public String getObjectType() {
        return objectType;
    }

    public void setObjectType(String objectType) {
        this.objectType = objectType;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public String getParams() {
        return params;
    }

    public void setParams(String params) {
        this.params = params;
    }

    public String getOnObjectType() {
        return onObjectType;
    }

    public void setOnObjectType(String onObjectType) {
        this.onObjectType = onObjectType;
    }

    public String getOnObjectId() {
        return onObjectId;
    }

    public void setOnObjectId(String onObjectId) {
        this.onObjectId = onObjectId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AuditDto auditDto = (AuditDto) o;

        if (timestamp != auditDto.timestamp) return false;
        if (username != null ? !username.equals(auditDto.username) : auditDto.username != null) return false;
        if (auditType != auditDto.auditType) return false;
        if (message != null ? !message.equals(auditDto.message) : auditDto.message != null) return false;
        if (crudAction != auditDto.crudAction) return false;
        if (objectType != null ? !objectType.equals(auditDto.objectType) : auditDto.objectType != null) return false;
        if (objectId != null ? !objectId.equals(auditDto.objectId) : auditDto.objectId != null) return false;
        if (params != null ? !params.equals(auditDto.params) : auditDto.params != null) return false;
        if (onObjectType != null ? !onObjectType.equals(auditDto.onObjectType) : auditDto.onObjectType != null) return false;
        return onObjectId != null ? onObjectId.equals(auditDto.onObjectId) : auditDto.onObjectId == null;
    }

    @Override
    public int hashCode() {
        int result = username != null ? username.hashCode() : 0;
        result = 31 * result + (int) (timestamp ^ (timestamp >>> 32));
        result = 31 * result + (auditType != null ? auditType.hashCode() : 0);
        result = 31 * result + (message != null ? message.hashCode() : 0);
        result = 31 * result + (crudAction != null ? crudAction.hashCode() : 0);
        result = 31 * result + (objectType != null ? objectType.hashCode() : 0);
        result = 31 * result + (objectId != null ? objectId.hashCode() : 0);
        result = 31 * result + (onObjectType != null ? onObjectType.hashCode() : 0);
        result = 31 * result + (onObjectId != null ? onObjectId.hashCode() : 0);
        result = 31 * result + (params != null ? params.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("AuditDto{");
        sb.append("username='").append(username).append('\'');
        sb.append(", timestamp=").append(timestamp);
        sb.append(", auditType=").append(auditType);
        sb.append(", message='").append(message).append('\'');
        sb.append(", crudAction=").append(crudAction);
        sb.append(", objectType='").append(objectType).append('\'');
        sb.append(", objectId='").append(objectId).append('\'');
        sb.append(", onObjectType='").append(onObjectType).append('\'');
        sb.append(", onObjectId='").append(onObjectId).append('\'');
        sb.append(", params='").append(params).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
