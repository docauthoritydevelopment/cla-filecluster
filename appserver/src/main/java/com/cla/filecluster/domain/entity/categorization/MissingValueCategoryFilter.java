package com.cla.filecluster.domain.entity.categorization;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("MVCF")
public class MissingValueCategoryFilter extends CategoryFilter {

    public MissingValueCategoryFilter() {
    }

    public MissingValueCategoryFilter(String fieldName) {
        setValue(fieldName);
    }


}
