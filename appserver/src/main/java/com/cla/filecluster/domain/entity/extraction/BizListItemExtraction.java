package com.cla.filecluster.domain.entity.extraction;

import com.cla.common.domain.dto.bizlist.BizListItemType;

/**
 * Created by uri on 13/01/2016.
 */
public class BizListItemExtraction {

    private String id;

    private long claFileId;

    private long bizListId;

    private String bizListItemSid;

    private BizListItemType bizListItemType;

    private String ruleName;

    private int aggMinCount;

    private boolean dirty;

    private long counter;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getClaFileId() {
        return claFileId;
    }

    public void setClaFileId(long claFileId) {
        this.claFileId = claFileId;
    }

    public long getBizListId() {
        return bizListId;
    }

    public void setBizListId(long bizListId) {
        this.bizListId = bizListId;
    }

    public String getBizListItemSid() {
        return bizListItemSid;
    }

    public void setBizListItemSid(String bizListItemSid) {
        this.bizListItemSid = bizListItemSid;
    }

    public String getRuleName() {
        return ruleName;
    }

    public void setRuleName(String ruleName) {
        this.ruleName = ruleName;
    }

    public boolean isDirty() {
        return dirty;
    }

    public void setDirty(boolean dirty) {
        this.dirty = dirty;
    }

    public BizListItemType getBizListItemType() {
        return bizListItemType;
    }

    public void setBizListItemType(BizListItemType bizListItemType) {
        this.bizListItemType = bizListItemType;
    }

    public int getAggMinCount() {
        return aggMinCount;
    }

    public void setAggMinCount(int aggMinCount) {
        this.aggMinCount = aggMinCount;
    }

    public long getCounter() {
        return counter;
    }

    public void setCounter(long counter) {
        this.counter = counter;
    }
}
