package com.cla.filecluster.domain.entity.date;

import com.cla.common.domain.dto.date.DateRangePointType;
import com.cla.common.domain.dto.date.TimePeriod;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by uri on 05/06/2016.
 */
@Embeddable
public class DateRangePoint {

//    @Id
//    @GeneratedValue
//    private Long id;

    private DateRangePointType type;

    private TimePeriod relativePeriod;

    private Integer relativePeriodAmount;

    @Column(columnDefinition = "datetime(3)")
    private Date absoluteTime;

//    public Long getId() { return id; }
//    public void setId(Long id) { this.id = id; }

    public DateRangePointType getType() {
        return type;
    }

    public void setType(DateRangePointType type) {
        this.type = type;
    }

    public TimePeriod getRelativePeriod() {
        return relativePeriod;
    }

    public void setRelativePeriod(TimePeriod relativePeriod) {
        this.relativePeriod = relativePeriod;
    }

    public Integer getRelativePeriodAmount() {
        return relativePeriodAmount;
    }

    public void setRelativePeriodAmount(Integer relativePeriodAmount) {
        this.relativePeriodAmount = relativePeriodAmount;
    }

    public Date getAbsoluteTime() {
        return absoluteTime;
    }

    public void setAbsoluteTime(Date absoluteTime) {
        this.absoluteTime = absoluteTime;
    }

    public Date relativePeriodToAbsolute() {
        return (type == DateRangePointType.ABSOLUTE)? null :
                (new Date(System.currentTimeMillis() - TimePeriod.periodsInMs(relativePeriod)*relativePeriodAmount));
    }

    public Date getDateValue() {
        return (type == DateRangePointType.ABSOLUTE)? getAbsoluteTime() :
                relativePeriodToAbsolute();
    }

}
