package com.cla.filecluster.domain.entity.pv;

import com.cla.connector.domain.dto.JsonSerislizableVisible;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "missed_file_match",
        indexes = {
                @Index(name = "processed_idx", columnList = "processed")
        })
public class MissedFileMatch implements JsonSerislizableVisible {
    public enum Reason {
        REASON_NULL, REASON_UPDATE_FAILED, REASON_PRE_MATCHED, REASON_OTHER
    }

    @TableGenerator(name = "fmm-table-hilo-generator", allocationSize = 1000)
    @GeneratedValue(generator = "fmm-table-hilo-generator", strategy = GenerationType.TABLE)
    @Id
    private Long id;

    @Column(name = "file1_id")
    @NotNull
    private Long file1Id;

    @Column(name = "file2_id")
    @NotNull
    private Long file2Id;

    @Enumerated(EnumType.ORDINAL)
    @NotNull
    private Reason reason;

    boolean processed = false;

    @Column(columnDefinition = "datetime(3)", updatable = false)
    private Date createdOnDB;

    @PrePersist
    private void setCreatedOnDB() {
        createdOnDB = new Date();
    }

    public MissedFileMatch(final Long file1Id, final Long file2Id,
                           final Reason reason) {
        if (file1Id > file2Id) {
            // swap - #1 is always the lower value
            this.file1Id = file2Id;
            this.file2Id = file1Id;
        } else {
            this.file1Id = file1Id;
            this.file2Id = file2Id;
        }
        this.reason = reason;
    }

    public MissedFileMatch() {
    }

    public Reason getReason() {
        return reason;
    }

    public void setReason(final Reason reason) {
        this.reason = reason;
    }

    public Long getFile1Id() {
        return file1Id;
    }

    public void setFile1Id(final Long file1Id) {
        this.file1Id = file1Id;
    }

    public Long getFile2Id() {
        return file2Id;
    }

    public void setFile2Id(final Long file2Id) {
        this.file2Id = file2Id;
    }

    public Date getCreatedOnDB() {
        return createdOnDB;
    }

    @Override
    public String toString() {
        return "MissedFileMatch{" +
                "id=" + id +
                ", file1Id=" + file1Id +
                ", file2Id=" + file2Id +
                ", reason=" + reason +
                ", processed=" + processed +
                ", createdOnDB=" + createdOnDB +
                '}';
    }
}
