package com.cla.filecluster.domain.statistics;

/**
 * Created by uri on 19/07/2015.
 */
public class SingleAnalyzeStatistics {

    private long timeSpentPartitioningGroupCandidates;

    public void setTimeSpentPartitioningGroupCandidates(long timeSpentPartitioningGroupCandidates) {
        this.timeSpentPartitioningGroupCandidates = timeSpentPartitioningGroupCandidates;
    }

    public long getTimeSpentPartitioningGroupCandidates() {
        return timeSpentPartitioningGroupCandidates;
    }

    public static enum State {
        OK, NULL_COLLECTIONS, NULL_PV_DATA
    }


    private State state;

    private long timeSpentFindMatchesInPotentialCandidatesWord;

    public void setTimeSpentFindMatchesInPotentialCandidatesWord(long timeSpentFindMatchesInPotentialCandidatesWord) {
        this.timeSpentFindMatchesInPotentialCandidatesWord = timeSpentFindMatchesInPotentialCandidatesWord;
    }

    public long getTimeSpentFindMatchesInPotentialCandidatesWord() {
        return timeSpentFindMatchesInPotentialCandidatesWord;
    }


    public SingleAnalyzeStatistics(State state) {
        this.state = state;
    }

    public static SingleAnalyzeStatistics NullPvDataResult() {
        return new SingleAnalyzeStatistics(State.NULL_PV_DATA);
    }

    public static SingleAnalyzeStatistics NullCollectionsResult() {
        return new SingleAnalyzeStatistics(State.NULL_COLLECTIONS);
    }

    public State getState() {
        return state;
    }
}
