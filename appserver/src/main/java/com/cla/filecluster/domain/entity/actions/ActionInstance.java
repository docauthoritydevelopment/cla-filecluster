package com.cla.filecluster.domain.entity.actions;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by uri on 14/02/2016.
 */
@Entity
@Table(name = "action_instance")
public class ActionInstance {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Access(AccessType.PROPERTY)
    private long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @NotNull
    private ActionDefinition action;

    private String name;

    private String parameters;

    public ActionInstance() {
    }

    public ActionInstance(ActionDefinition action, String name) {
        this.action = action;
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public ActionDefinition getAction() {
        return action;
    }

    public void setAction(ActionDefinition action) {
        this.action = action;
    }

    public String getParameters() {
        return parameters;
    }

    public void setParameters(String parameters) {
        this.parameters = parameters;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
