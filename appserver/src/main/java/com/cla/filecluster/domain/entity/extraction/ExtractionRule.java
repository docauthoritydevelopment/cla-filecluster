package com.cla.filecluster.domain.entity.extraction;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Enumerated;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

@Embeddable
public class ExtractionRule {
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(ExtractionRule.class);

	public enum RuleType {
		SIMPLE_RULE, AGGREGATED_RULE
	}

	@NotNull
	@Column(name="rule")
	private String name;
	
	@Transient
	private String query;
	
	@Enumerated
	@NotNull
	@Column(name="rule_type")
	private RuleType type;
	
	@Column(name="rule_param")
	private short param;

	public ExtractionRule() {
	}
	public ExtractionRule(final ExtractionRule o) {
		this();
		this.name = o.name;
		this.query = o.query;
		this.type = o.type;
		this.param = o.param;
	}
	public ExtractionRule(final String name, final String query, final RuleType type, final short param) {
		this();
		this.name = name;
		this.query = query;
		this.type = type;
		this.param = param;
	}

	public String getName() {
		return name;
	}
	public void setName(final String name) {
		this.name = name;
	}
	public String getQuery() {
		return query;
	}
	public void setQuery(final String query) {
		this.query = query;
	}
	public RuleType getType() {
		return type;
	}
	public void setType(final RuleType type) {
		this.type = type;
	}
	public short getParam() {
		return param;
	}
	public void setParam(final short param) {
		this.param = param;
	}
	public static org.slf4j.Logger getLogger() {
		return logger;
	}
	@Override
	public String toString() {
		return String.format("{%s/%s:%d=%s}" , name,type.name(),param,query);
	}
}
