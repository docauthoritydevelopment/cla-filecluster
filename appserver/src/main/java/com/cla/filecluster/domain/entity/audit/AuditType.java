package com.cla.filecluster.domain.entity.audit;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by uri on 23/02/2016.
 */
public enum AuditType {
    ACTION_EVENT(false),
    GROUP(true),
    FILE(true),
    FOLDER(false),
    BIZ_LIST(true),
    ROOT_FOLDER(true),
    ENTITY_CREDENTIAL(true),
    MEDIA_PROCESSOR(true),
    MEDIA_CONNECTION_DETAILS(true),
    SEARCH_PATTERN(true),
    PATTERN_CATEGORIES(true),
    REGULATIONS(true),
    SCHEDULE_GROUP(true),
    DOC_TYPE(true),
    TAG_TYPE(true),
    TAG(true),
    DATE_PARTITION(true),
    CUSTOMER_DATA_CENTER(true),
    SCAN(true),
    SOLR(true),
    USER(true),
    BIZ_ROLE(false), // deprecated use OPERATIONAL_ROLE
    FUNCTIONAL_ROLE(false), // deprecated use DATA_ROLE
    ROLE_TEMPLATE(true),
    SETTINGS(true),
    CHART(true),
    TREND_CHART(true),
    QUERY(true),
    LOGIN(true),
    LOGOUT(true),
    OPERATIONAL_ROLE(true),
    DATA_ROLE(true),
    CHANGE_KEYS(true),
    ALERT(true),
    REFINEMENT_RULE(true),
    WORKFLOW(true),
    WORK_GROUP(true),
    ACTION_SCHEDULE(true),
    FILTER(true),
    DISCOVER_FILTER(true),
    ANALYSIS(true),
    INGEST(true),
    EXTRACT(true),
    EXPORT(true),
    MAILBOX(true),
    DEPARTMENT(true),
    LABEL(true),
    REPORT_WIDGET(true),
    DISCOVER_VIEW(true),
    DASHBOARD(true),
    TRACKER(true),
    BOARD(true),
    BOARD_TASK(true);

    private boolean isInUse;

    AuditType(boolean isInUse) {
        this.isInUse = isInUse;
    }

    public static List<AuditType> getInUse() {
        return Arrays.stream(values()).filter(v -> v.isInUse).sorted(new AuditTypeComparator()).collect(Collectors.toList());
    }

    static class AuditTypeComparator implements Comparator<AuditType> {
        public int compare(AuditType o1, AuditType o2) {
            return o1.name().compareTo(o2.name()); // this compares length
        }
    }
}
