package com.cla.filecluster.domain.entity.filetype;

import javax.persistence.*;
import java.io.Serializable;

/**
 *
 * Created by: ophir
 * Created on: 3/25/2019
 */
@Entity
@Table(
        name = "extension"
)
public class Extension {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Access(AccessType.PROPERTY)
    @Id
    private long id;

    private String name;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
