package com.cla.filecluster.domain.dto;

import com.cla.common.domain.dto.file.AnalyzeHint;
import com.cla.common.domain.dto.file.ClaFileState;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.apache.solr.client.solrj.beans.Field;

import java.util.Collection;
import java.util.Date;
import java.util.List;

public class SolrContentMetadataEntity implements SolrEntity{

    @Field
    private String id;

    @Field
    private int type;                   // FileType - WORD, EXCEL etc.

    @Field
    private String contentSignature;

    @Field
    private String userContentSignature;

    @Field("groupId")
    private String analysisGroupId;

    @Field
    private String userGroupId;

    @Field
    private String analysisHint = AnalyzeHint.NORMAL.name();

    @Field
    private String groupName;

    @Field
    private Date creationDate;

    @Field
    private Long updateDate;

    @Field
    private long size;

    @Field
    private int fileCount = 0;

    @Field("processing_state")
    private String state = ClaFileState.INGESTED.name();

    @Field("proposed_titles")
    private Collection<String> proposedTitles;

    @Field
    private Long lastIngestTimestampMs;

    @Field
    private Long sampleFileId;

    @Field
    private String author;

    @Field
    private String company;

    @Field
    private String subject;

    @Field
    private String title;

    @Field
    private String keywords;

    @Field
    private String keywordsOriginal;

    @Field
    private String generatingApp;

    @Field
    private String template;

    @Field
    private Long itemsCountL1;

    @Field
    private Long itemsCountL2;

    @Field
    private Long itemsCountL3;

    @Field
    private boolean groupDirty = false;

    @Field
    private boolean populationDirty = false;

    @Field
    private int skipAnalysisReason;

    @JsonIgnore
    private boolean alreadyAccessed = false;

    @Field
    private List<String> extractedEntitiesIds;

    @Field
    private Long currentRunId;

    @Field
    private String taskState;

    @Field
    private Long taskStateDate;

    @Field
    private Integer retries;

    @Field
    private Long groupDirtyDate;

    @Field
    private Long popDirtyDate;

    public String getIdAsString() {return id;}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getContentSignature() {
        return contentSignature;
    }

    public void setContentSignature(String contentSignature) {
        this.contentSignature = contentSignature;
    }

    public String getUserContentSignature() {
        return userContentSignature;
    }

    public void setUserContentSignature(String userContentSignature) {
        this.userContentSignature = userContentSignature;
    }

    public String getAnalysisGroupId() {
        return analysisGroupId;
    }

    public void setAnalysisGroupId(String analysisGroupId) {
        this.analysisGroupId = analysisGroupId;
    }

    public String getUserGroupId() {
        return userGroupId;
    }

    public void setUserGroupId(String userGroupId) {
        this.userGroupId = userGroupId;
    }

    public String getAnalysisHint() {
        return analysisHint;
    }

    public void setAnalysisHint(String analysisHint) {
        this.analysisHint = analysisHint;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public long getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getFileCount() {
        return fileCount;
    }

    public void setFileCount(int fileCount) {
        this.fileCount = fileCount;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Collection<String> getProposedTitles() {
        return proposedTitles;
    }

    public void setProposedTitles(Collection<String> proposedTitles) {
        this.proposedTitles = proposedTitles;
    }

    public Long getLastIngestTimestampMs() {
        return lastIngestTimestampMs;
    }

    public void setLastIngestTimestampMs(Long lastIngestTimestampMs) {
        this.lastIngestTimestampMs = lastIngestTimestampMs;
    }

    public Long getSampleFileId() {
        return sampleFileId;
    }

    public void setSampleFileId(Long sampleFileId) {
        this.sampleFileId = sampleFileId;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getKeywordsOriginal() {
        return keywordsOriginal;
    }

    public void setKeywordsOriginal(String keywordsOriginal) {
        this.keywordsOriginal = keywordsOriginal;
    }

    public String getGeneratingApp() {
        return generatingApp;
    }

    public void setGeneratingApp(String generatingApp) {
        this.generatingApp = generatingApp;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public boolean isAlreadyAccessed() {
        return alreadyAccessed;
    }

    public void setAlreadyAccessed(boolean alreadyAccessed) {
        this.alreadyAccessed = alreadyAccessed;
    }

    public Long getItemsCountL1() {
        return itemsCountL1;
    }

    public void setItemsCountL1(Long itemsCountL1) {
        this.itemsCountL1 = itemsCountL1;
    }

    public Long getItemsCountL2() {
        return itemsCountL2;
    }

    public void setItemsCountL2(Long itemsCountL2) {
        this.itemsCountL2 = itemsCountL2;
    }

    public Long getItemsCountL3() {
        return itemsCountL3;
    }

    public void setItemsCountL3(Long itemsCountL3) {
        this.itemsCountL3 = itemsCountL3;
    }

    public boolean isGroupDirty() {
        return groupDirty;
    }

    public void setGroupDirty(boolean groupDirty) {
        this.groupDirty = groupDirty;
    }

    public boolean isPopulationDirty() {
        return populationDirty;
    }

    public void setPopulationDirty(boolean populationDirty) {
        this.populationDirty = populationDirty;
    }

    public int getSkipAnalysisReason() {
        return skipAnalysisReason;
    }

    public void setSkipAnalysisReason(int skipAnalysisReason) {
        this.skipAnalysisReason = skipAnalysisReason;
    }

    public List<String> getExtractedEntitiesIds() {
        return extractedEntitiesIds;
    }

    public void setExtractedEntitiesIds(List<String> extractedEntitiesIds) {
        this.extractedEntitiesIds = extractedEntitiesIds;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public Long getCurrentRunId() {
        return currentRunId;
    }

    public void setCurrentRunId(Long currentRunId) {
        this.currentRunId = currentRunId;
    }

    public String getTaskState() {
        return taskState;
    }

    public void setTaskState(String taskState) {
        this.taskState = taskState;
    }

    public Long getTaskStateDate() {
        return taskStateDate;
    }

    public void setTaskStateDate(Long taskStateDate) {
        this.taskStateDate = taskStateDate;
    }

    public Integer getRetries() {
        return retries;
    }

    public void setRetries(Integer retries) {
        this.retries = retries;
    }

    public Long getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Long updateDate) {
        this.updateDate = updateDate;
    }

    public Long getGroupDirtyDate() {
        return groupDirtyDate;
    }

    public void setGroupDirtyDate(Long groupDirtyDate) {
        this.groupDirtyDate = groupDirtyDate;
    }

    public Long getPopDirtyDate() {
        return popDirtyDate;
    }

    public void setPopDirtyDate(Long popDirtyDate) {
        this.popDirtyDate = popDirtyDate;
    }
}
