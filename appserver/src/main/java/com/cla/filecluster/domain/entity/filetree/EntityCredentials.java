package com.cla.filecluster.domain.entity.filetree;

import javax.persistence.*;

/**
 * Entity credential is a record of username/password used to access an entity (currently only root folder)
 * Created by yael on 11/9/2017.
 */
@Entity
@Table(name = "entity_credentials",
        indexes = {@Index(name = "entity_identify",columnList = "entity_type,entity_id")}
)
public class EntityCredentials {

    @Id
    @Access(AccessType.PROPERTY)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(nullable = false, name = "entity_type")
    private String entityType;

    @Column(nullable = false, name = "entity_id")
    private long entityId;

    @Column(length = 50)
    private String username;

    @Column(nullable = false, length = 500)
    private String password;

    @Column(name = "created_time_stamp")
    private long createdTimeStampMs;

    @Column(name = "modified_time_stamp")
    private long modifiedTimeStampMs;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEntityType() {
        return entityType;
    }

    public void setEntityType(String entityType) {
        this.entityType = entityType;
    }

    public long getEntityId() {
        return entityId;
    }

    public void setEntityId(long entityId) {
        this.entityId = entityId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public long getCreatedTimeStampMs() {
        return createdTimeStampMs;
    }

    public void setCreatedTimeStampMs(long createdTimeStampMs) {
        this.createdTimeStampMs = createdTimeStampMs;
    }

    public long getModifiedTimeStampMs() {
        return modifiedTimeStampMs;
    }

    public void setModifiedTimeStampMs(long modifiedTimeStampMs) {
        this.modifiedTimeStampMs = modifiedTimeStampMs;
    }

    @Override
    public String toString() {
        return "EntityCredentials{" +
                "id=" + id +
                ", entityType='" + entityType + '\'' +
                ", entityId=" + entityId +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", createdTimeStampMs=" + createdTimeStampMs +
                ", modifiedTimeStampMs=" + modifiedTimeStampMs +
                '}';
    }
}
