package com.cla.filecluster.domain.entity.actions;

import com.cla.common.domain.dto.action.ActionClass;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Created by uri on 25/01/2016.
 */
@Entity
@Table(name = "action_definition")
public class ActionDefinition implements Serializable {

    @Id
    @Access(AccessType.PROPERTY)
    private String id;

    @NotNull
    private String name;

    private String description;

    @NotNull
    private ActionClass actionClass;

    @NotNull
    private String javaMethodName;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ActionClass getActionClass() {
        return actionClass;
    }

    public void setActionClass(ActionClass actionClass) {
        this.actionClass = actionClass;
    }

    public String getJavaMethodName() {
        return javaMethodName;
    }

    public void setJavaMethodName(String javaMethodName) {
        this.javaMethodName = javaMethodName;
    }

    @Override
    public String toString() {
        return "ActionDefinition{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", actionClass=" + actionClass +
                '}';
    }
}
