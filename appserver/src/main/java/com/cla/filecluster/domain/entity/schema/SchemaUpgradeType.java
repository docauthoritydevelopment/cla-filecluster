package com.cla.filecluster.domain.entity.schema;

/**
 * Created by uri on 03/03/2016.
 */
public enum SchemaUpgradeType {
    SQL, JAVA
}
