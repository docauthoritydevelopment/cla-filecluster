package com.cla.filecluster.domain.dto;

import java.util.List;

import com.cla.common.domain.dto.FacetFieldEntry;
import com.cla.common.domain.dto.PivotFacetFieldEntry;
import com.cla.connector.domain.dto.JsonSerislizableVisible;
import com.fasterxml.jackson.annotation.JsonProperty;

public class FileDTOSearchResult implements JsonSerislizableVisible {

	@JsonProperty
	private List<SolrExtractionEntity> extractionSolrEntities;
	
	@JsonProperty
	private List<FacetFieldEntry> facetFieldEntries;
	
	@JsonProperty
	private List<PivotFacetFieldEntry> pivotFacetFieldEntries;
	
	@JsonProperty
	private String nextCursorMark;


	public FileDTOSearchResult() {
	}

	public FileDTOSearchResult(final List<SolrExtractionEntity> extractionSolrEntities,
			final List<FacetFieldEntry> facetFieldEntries,
			final List<PivotFacetFieldEntry> pivotFacetFieldEntries,
			final String nextCursorMark) {
		this.extractionSolrEntities = extractionSolrEntities;
		this.facetFieldEntries = facetFieldEntries;
		this.pivotFacetFieldEntries=pivotFacetFieldEntries;
		this.nextCursorMark = nextCursorMark;
	}

	public List<SolrExtractionEntity> getExtractionSolrEntities() {
		return extractionSolrEntities;
	}

	public List<FacetFieldEntry> getFacetFieldEntries() {
		return facetFieldEntries;
	}

	public List<PivotFacetFieldEntry> getPivotFacetFieldEntries() {
		return pivotFacetFieldEntries;
	}

	public String getNextCursorMark() {
		return nextCursorMark;
	}

	@Override
	public String toString() {
		return "FileDTOSearchResult:{extractionSolrEntities=" + extractionSolrEntities + ", facetFieldEntries=" + facetFieldEntries
				+ ", pivotFacetFieldEntries=" + pivotFacetFieldEntries + ", nextCursorMark=" + nextCursorMark + "}";
	}
	
}
