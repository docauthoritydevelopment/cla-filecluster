package com.cla.filecluster.domain.entity.doctype;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Created by uri on 17/08/2015.
 */
@Entity
@Table(
		uniqueConstraints = {
				@UniqueConstraint(name = "name_acc",columnNames = {"name"})
		}
	)
public class DocTypeRiskInformation implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull
    private String name;

    private int defaultCostPerItem;

    public DocTypeRiskInformation() {
    }

    public DocTypeRiskInformation(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDefaultCostPerItem() {
        return defaultCostPerItem;
    }

    public void setDefaultCostPerItem(int defaultCostPerItem) {
        this.defaultCostPerItem = defaultCostPerItem;
    }

}
