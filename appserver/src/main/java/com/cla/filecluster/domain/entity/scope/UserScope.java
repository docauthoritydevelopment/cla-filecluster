package com.cla.filecluster.domain.entity.scope;

import com.cla.filecluster.domain.entity.security.User;

import javax.persistence.*;


@Entity
@DiscriminatorValue(value = "USER")
public class UserScope extends BasicScope<User> {

    @OneToOne
    @JoinColumn(name = "user_id")
    private User user;

    @Override
    public User getScopeBindings() {
        return user;
    }

    public void setScopeBindings(User user) {
        this.user = user;
    }
}
