package com.cla.filecluster.domain.entity.crawler;

import com.cla.common.domain.dto.crawler.RunStatus;
import com.cla.common.domain.dto.crawler.RunType;
import com.cla.common.domain.dto.jobmanager.PauseReason;
import org.hibernate.annotations.ColumnDefault;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import java.util.Arrays;
import java.util.Date;

/**
 * Object that represents a run on a root folder
 * (usually scan-ingest-finalize)
 * Created by uri on 01/05/2016.
 */
@Entity
@Table(name = "crawlRuns",
        indexes = {
                @Index(name = "idx_rfid", columnList = "rootFolderId")
                ,@Index(name = "idx_runstatus", columnList = "run_status")
                ,@Index(name = "idx_parentrun", columnList = "parentRunId")
})
public class CrawlRun {

    private static final Logger logger = LoggerFactory.getLogger(CrawlRun.class);

    @TableGenerator(name = "crawl-run-table-hilo-generator", allocationSize = 100)
    @GeneratedValue(generator = "crawl-run-table-hilo-generator", strategy = GenerationType.TABLE)
    @Id
    private Long id;

    private String stateString;

    @Lob
    private String runConfig;

    @Column(columnDefinition = "datetime(3)")
    private Date startTime;

    @Enumerated(EnumType.STRING)
    @Column(name = "run_status", columnDefinition = "char(32)")
    private RunStatus runStatus;

    @Enumerated(EnumType.STRING)
    @Column(name = "run_type", columnDefinition = "char(32)")
    private RunType runType;

    private Long rootFolderId;

    private Long parentRunId;

    private Boolean hasChildRuns;

    private Long scheduleGroupId;

    private boolean scanFiles;

    private boolean ingestFiles;

    private boolean analyzeFiles;

    private boolean extractFiles;

    @Column(columnDefinition = "datetime(3)")
    private Date stopTime;

    private boolean manual;

    @Column(length = 4096)
    private String exceptionText;

    @Column(length = 4096)
    private String ingestedRootFolders;

    @Column(length = 4096)
    private String analyzedRootFolders;
    private long processedWordFiles;
    private long processedExcelFiles;
    private long processedPdfFiles;
    private long processedOtherFiles;
    private long totalProcessedFiles;

    private long newFiles;

    private long updatedContentFiles;

    private long updatedMetadataFiles;

    private long deletedFiles;

    private boolean rootFolderAccessible;

    private long scanErrors;

    private long ingestionErrors;

    @Column(length = 4096)
    private String mediaChangeLogPosition;

    @Version
    @ColumnDefault(value="'0'")
    private int version = 0;

    @Enumerated(EnumType.STRING)
    @Column(name = "pause_reason", length = 50)
    private PauseReason pauseReason;

    private Long timeFirstPause;

    private Boolean gracefulStop;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStateString() {
        return stateString;
    }

    public void setStateString(String stateString) {
        this.stateString = stateString;
    }

    public String getRunConfig() {
        return runConfig;
    }

    public void setRunConfig(String runConfig) {
        this.runConfig = runConfig;
    }


    public RunStatus getRunStatus() {
        return runStatus;
    }

    public void setRunStatus(RunStatus runStatus) {
        if (this.runStatus == null || !this.runStatus.isFinished()) {
            this.runStatus = runStatus;
        } else if (logger.isDebugEnabled()) {
            String stackTrace = Arrays.toString(Thread.currentThread().getStackTrace());
            logger.debug("Preventing run #{} from changing status from {} to {}", this.id, this.runStatus, runStatus);
            logger.debug("Stack Trace: {}", stackTrace);
        }
    }

    public Long getRootFolderId() {
        return rootFolderId;
    }

    public void setRootFolderId(Long rootFolderId) {
        this.rootFolderId = rootFolderId;
    }

    public Date getStartTime() {
        return startTime;
    }

    @Transient
    public Long getStartTimeAsLong() {
        return getStartTime() == null ? null : getStartTime().getTime();
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getStopTime() {
        return stopTime;
    }

    @Transient
    public Long getStopTimeAsLong() {
        return getStopTime() == null ? null : getStopTime().getTime();
    }

    public void setStopTime(Date stopTime) {
        this.stopTime = stopTime;
    }

    public String getExceptionText() {
        return exceptionText;
    }

    public void setExceptionText(String exceptionText) {
        this.exceptionText = exceptionText;
    }

    public boolean isScanFiles() {
        return scanFiles;
    }

    public void setScanFiles(boolean scanFiles) {
        this.scanFiles = scanFiles;
    }

    public boolean isIngestFiles() {
        return ingestFiles;
    }

    public void setIngestFiles(boolean ingestFiles) {
        this.ingestFiles = ingestFiles;
    }

    public boolean isAnalyzeFiles() {
        return analyzeFiles;
    }

    public void setAnalyzeFiles(boolean analyzeFiles) {
        this.analyzeFiles = analyzeFiles;
    }

    public Long getScheduleGroupId() {
        return scheduleGroupId;
    }

    public void setScheduleGroupId(Long scheduleGroupId) {
        this.scheduleGroupId = scheduleGroupId;
    }

    public String getIngestedRootFolders() {
        return ingestedRootFolders;
    }

    public void setIngestedRootFolders(String ingestedRootFolders) {
        this.ingestedRootFolders = ingestedRootFolders;
    }

    public String getAnalyzedRootFolders() {
        return analyzedRootFolders;
    }

    public void setAnalyzedRootFolders(String analyzedRootFolders) {
        this.analyzedRootFolders = analyzedRootFolders;
    }

    public void setProcessedWordFiles(long processedWordFiles) {
        this.processedWordFiles = processedWordFiles;
    }

    public long getProcessedWordFiles() {
        return processedWordFiles;
    }

    public void setProcessedExcelFiles(long processedExcelFiles) {
        this.processedExcelFiles = processedExcelFiles;
    }

    public long getProcessedExcelFiles() {
        return processedExcelFiles;
    }

    public void setProcessedPdfFiles(long processedPdfFiles) {
        this.processedPdfFiles = processedPdfFiles;
    }

    public long getProcessedPdfFiles() {
        return processedPdfFiles;
    }

    public void setProcessedOtherFiles(long processedOtherFiles) {
        this.processedOtherFiles = processedOtherFiles;
    }

    public long getProcessedOtherFiles() {
        return processedOtherFiles;
    }

    public void setTotalProcessedFiles(long totalProcessedFiles) {
        this.totalProcessedFiles = totalProcessedFiles;
    }

    public long getTotalProcessedFiles() {
        return totalProcessedFiles;
    }

    public boolean isManual() {
        return manual;
    }

    public void setManual(boolean manual) {
        this.manual = manual;
    }

    public long getScanErrors() {
        return scanErrors;
    }

    public void setScanErrors(long scanErrors) {
        this.scanErrors = scanErrors;
    }

    public long getIngestionErrors() {
        return ingestionErrors;
    }

    public void setIngestionErrors(long ingestionErrors) {
        this.ingestionErrors = ingestionErrors;
    }

    public boolean isRootFolderAccessible() {
        return rootFolderAccessible;
    }

    public void setRootFolderAccessible(boolean rootFolderAccessible) {
        this.rootFolderAccessible = rootFolderAccessible;
    }

    public long getNewFiles() {
        return newFiles;
    }

    public void setNewFiles(long newFiles) {
        this.newFiles = newFiles;
    }

    public long getUpdatedContentFiles() {
        return updatedContentFiles;
    }

    public void setUpdatedContentFiles(long updatedContentFiles) {
        this.updatedContentFiles = updatedContentFiles;
    }

    public long getDeletedFiles() {
        return deletedFiles;
    }

    public void setDeletedFiles(long deletedFiles) {
        this.deletedFiles = deletedFiles;
    }

    public long getUpdatedMetadataFiles() {
        return updatedMetadataFiles;
    }

    public void setUpdatedMetadataFiles(long updatedMetadataFiles) {
        this.updatedMetadataFiles = updatedMetadataFiles;
    }

    public String getMediaChangeLogPosition() {
        return mediaChangeLogPosition;
    }

    public void setMediaChangeLogPosition(String mediaChangeLogPosition) {
        this.mediaChangeLogPosition = mediaChangeLogPosition;
    }

    public boolean isExtractFiles() {
        return extractFiles;
    }

    public void setExtractFiles(boolean extractFiles) {
        this.extractFiles = extractFiles;
    }


    public RunType getRunType() {
        return runType;
    }

    public void setRunType(RunType runType) {
        this.runType = runType;
    }

    public Long getParentRunId() {
        return parentRunId;
    }

    public void setParentRunId(Long parentRunId) {
        this.parentRunId = parentRunId;
    }

    public Boolean getHasChildRuns() {
        return hasChildRuns;
    }

    public void setHasChildRuns(Boolean hasChildRuns) {
        this.hasChildRuns = hasChildRuns;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public PauseReason getPauseReason() {
        return pauseReason;
    }

    public void setPauseReason(PauseReason pauseReason) {
        this.pauseReason = pauseReason;
    }

    public Long getTimeFirstPause() {
        return timeFirstPause;
    }

    public void setTimeFirstPause(Long timeFirstPause) {
        this.timeFirstPause = timeFirstPause;
    }

    public Boolean getGracefulStop() {
        return gracefulStop;
    }

    public void setGracefulStop(Boolean gracefulStop) {
        this.gracefulStop = gracefulStop;
    }

    @Override
    public String toString() {
        return "CrawlRun{" +
                "id=" + id +
                ", stateString='" + stateString + '\'' +
                ", runStatus=" + runStatus +
                ", runType=" + runType +
                ", rootFolderId=" + rootFolderId +
                ", scheduleGroupId=" + scheduleGroupId +
                ", parentRunId=" + parentRunId +
                ", exceptionText='" + exceptionText + '\'' +
                '}';
    }

}
