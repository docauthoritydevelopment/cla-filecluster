package com.cla.filecluster.domain.entity.actions;

import com.cla.common.domain.dto.action.ActionTriggerType;
import com.cla.common.domain.dto.crawler.RunStatus;
import com.cla.common.domain.dto.kendo.FilterDescriptor;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Properties;

/**
 * Created by uri on 16/02/2016.
 */
@Entity
@Table(name = "action_execution_task_status")
public class ActionExecutionTaskStatus {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Access(AccessType.PROPERTY)
    private long id;

    private RunStatus status;

    private long progressMark;

    private long progressMaxMark;

    private long triggerTime;
    private ActionTriggerType type;

    private String userName;

    private long userId;
    private long errorsCount;

    @Column(length = 4096)
    private String firstError;

    @Type(type = "json")
    @Column(columnDefinition = "json")
    private FilterDescriptor filterDescriptor;

    private String actionId;

    @Type(type = "json")
    @Column(columnDefinition = "json")
    private Properties actionParams;

    private String entityType;

    private String entityId;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public RunStatus getStatus() {
        return status;
    }

    public void setStatus(RunStatus status) {
        this.status = status;
    }

    public long getTriggerTime() {
        return triggerTime;
    }

    public void setTriggerTime(long triggerTime) {
        this.triggerTime = triggerTime;
    }

    public void setType(ActionTriggerType type) {
        this.type = type;
    }

    public ActionTriggerType getType() {
        return type;
    }

    public long getProgressMark() {
        return progressMark;
    }

    public void setProgressMark(long progressMark) {
        this.progressMark = progressMark;
    }

    public long getProgressMaxMark() {
        return progressMaxMark;
    }

    public void setProgressMaxMark(long progressMaxMark) {
        this.progressMaxMark = progressMaxMark;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getErrorsCount() {
        return errorsCount;
    }

    public void setErrorsCount(long errorsCount) {
        this.errorsCount = errorsCount;
    }

    public String getFirstError() {
        return firstError;
    }

    public void setFirstError(String firstError) {
        this.firstError = firstError;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public FilterDescriptor getFilterDescriptor() {
        return filterDescriptor;
    }

    public void setFilterDescriptor(FilterDescriptor filterDescriptor) {
        this.filterDescriptor = filterDescriptor;
    }

    public String getActionId() {
        return actionId;
    }

    public void setActionId(String actionId) {
        this.actionId = actionId;
    }

    public Properties getActionParams() {
        return actionParams;
    }

    public void setActionParams(Properties actionParams) {
        this.actionParams = actionParams;
    }

    public String getEntityType() {
        return entityType;
    }

    public void setEntityType(String entityType) {
        this.entityType = entityType;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }
}
