package com.cla.filecluster.domain.dto;

import org.apache.solr.client.solrj.beans.Field;

import java.util.Objects;

/**
 * Represents biz list solr entity
 *
 * Created by: yael
 * Created on: 7/1/2018
 */
public class SolrBizListEntity implements SolrEntity {
    @Field
    private String id;

    @Field
    private Long fileId;

    @Field
    private Long bizListId;

    @Field
    private Integer bizListItemType;

    @Field
    private Integer aggMinCount;

    @Field
    private String bizListItemSid;

    @Field
    private String ruleName;

    @Field
    private Boolean dirty;

    @Field
    private Long counter;

    public String getIdAsString() {return id;}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    public Long getBizListId() {
        return bizListId;
    }

    public void setBizListId(Long bizListId) {
        this.bizListId = bizListId;
    }

    public Integer getBizListItemType() {
        return bizListItemType;
    }

    public void setBizListItemType(Integer bizListItemType) {
        this.bizListItemType = bizListItemType;
    }

    public Integer getAggMinCount() {
        return aggMinCount;
    }

    public void setAggMinCount(Integer aggMinCount) {
        this.aggMinCount = aggMinCount;
    }

    public String getBizListItemSid() {
        return bizListItemSid;
    }

    public void setBizListItemSid(String bizListItemSid) {
        this.bizListItemSid = bizListItemSid;
    }

    public String getRuleName() {
        return ruleName;
    }

    public void setRuleName(String ruleName) {
        this.ruleName = ruleName;
    }

    public Boolean getDirty() {
        return dirty;
    }

    public void setDirty(Boolean dirty) {
        this.dirty = dirty;
    }

    public Long getCounter() {
        return counter;
    }

    public void setCounter(Long counter) {
        this.counter = counter;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SolrBizListEntity that = (SolrBizListEntity) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(fileId, that.fileId) &&
                Objects.equals(bizListId, that.bizListId) &&
                Objects.equals(bizListItemType, that.bizListItemType) &&
                Objects.equals(aggMinCount, that.aggMinCount) &&
                Objects.equals(bizListItemSid, that.bizListItemSid) &&
                Objects.equals(ruleName, that.ruleName) &&
                Objects.equals(dirty, that.dirty);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, fileId, bizListId, bizListItemType, aggMinCount, bizListItemSid, ruleName, dirty);
    }
}
