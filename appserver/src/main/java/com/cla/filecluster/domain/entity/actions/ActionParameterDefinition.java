package com.cla.filecluster.domain.entity.actions;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by uri on 25/02/2016.
 */
@Entity
@Table(name = "action_parameter_definition")
public class ActionParameterDefinition {

    @Id
    @Access(AccessType.PROPERTY)
    private String id;

    @ManyToOne(fetch = FetchType.LAZY)
    private ActionDefinition actionDefinition;

    @NotNull
    private String name;

    private String description;

    private String regex;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ActionDefinition getActionDefinition() {
        return actionDefinition;
    }

    public void setActionDefinition(ActionDefinition actionDefinition) {
        this.actionDefinition = actionDefinition;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRegex() {
        return regex;
    }

    public void setRegex(String regex) {
        this.regex = regex;
    }
}
