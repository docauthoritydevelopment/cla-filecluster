package com.cla.filecluster.domain.entity.department;

import com.cla.common.domain.dto.department.DepartmentDto;
import com.cla.filecluster.domain.entity.BasicEntity;
import com.cla.filecluster.domain.entity.filetag.AssociableEntity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Created by ginat on 12/03/2019.
 */
@Entity
public class Department extends BasicEntity implements Serializable, AssociableEntity {

    @NotNull
    private String name;

    @NotNull
    private String uniqueName;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "parent_id")
    private Department parent;

    private boolean deleted;

    private String description;

    public Department() { }

    public Department(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Department getParent() {
        return parent;
    }

    public void setParent(Department parent) {
        this.parent = parent;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public String getUniqueName() {
        return uniqueName;
    }

    public void setUniqueName(String uniqueName) {
        this.uniqueName = uniqueName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Department{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", uniqueName='" + uniqueName + '\'' +
                ", deleted=" + deleted +
                '}';
    }

    @Override
    public String createIdentifierString() {
        return DepartmentDto.ID_PREFIX + String.valueOf(id);
    }

}
