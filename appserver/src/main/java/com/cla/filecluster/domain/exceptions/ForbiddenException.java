package com.cla.filecluster.domain.exceptions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.FORBIDDEN)
public class ForbiddenException extends RuntimeException {

    private Logger logger = LoggerFactory.getLogger(ForbiddenException.class);

    public ForbiddenException(String reason) {
        super("Forbidden Request - ["+reason+"]");
        logger.warn(this.getMessage());
    }
}