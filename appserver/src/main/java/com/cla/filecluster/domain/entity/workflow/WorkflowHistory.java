package com.cla.filecluster.domain.entity.workflow;

import com.cla.common.domain.dto.workflow.ActionType;
import com.cla.common.domain.dto.workflow.WorkflowHistoryChangeDescription;
import com.cla.filecluster.domain.entity.BasicEntity;
import org.hibernate.annotations.Type;

import javax.persistence.*;

/**
 * Created by: yael
 * Created on: 2/21/2018
 */

@Entity
@Table(	name = "workflow_history")
public class WorkflowHistory extends BasicEntity {

    @Enumerated
    private ActionType actionType;

    @Type(type = "json")
    @Column(columnDefinition = "json")
    private WorkflowHistoryChangeDescription changeDescription;

    private String actor;

    private Long workflowId;

    private String comment;

    public ActionType getActionType() {
        return actionType;
    }

    public void setActionType(ActionType actionType) {
        this.actionType = actionType;
    }

    public WorkflowHistoryChangeDescription getChangeDescription() {
        return changeDescription;
    }

    public void setChangeDescription(WorkflowHistoryChangeDescription changeDescription) {
        this.changeDescription = changeDescription;
    }

    public String getActor() {
        return actor;
    }

    public void setActor(String actor) {
        this.actor = actor;
    }

    public Long getWorkflowId() {
        return workflowId;
    }

    public void setWorkflowId(Long workflowId) {
        this.workflowId = workflowId;
    }

    @Override
    public String toString() {
        return "WorkflowHistory{" +
                "actionType=" + actionType +
                ", changeDescription=" + changeDescription +
                ", actor=" + actor +
                ", workflowId=" + workflowId +
                super.toString() +
                '}';
    }

    public void setComment(String userComment) {
        this.comment = userComment;
    }

    public String getComment() {
        return comment;
    }
}
