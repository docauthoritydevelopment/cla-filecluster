package com.cla.filecluster.domain.entity.monitor;

import com.cla.common.domain.dto.components.ComponentState;

import javax.persistence.*;

/**
 * Created by itay on 22/02/2017.
 */

@Entity
@IdClass(ClaComponentStatusPK.class)
@Table(name = "mon_component_status", indexes = {
        @Index(name = "idx_component",columnList = "cla_component_id")
})
public class ClaComponentStatus {
    @Id
    @Access(AccessType.PROPERTY)
    @GeneratedValue
    private long id;

    @Column(name = "cla_component_id")
    private Long claComponentId;

    @Id
    @Column(nullable = false)
    private long timestamp;         // Time when record is generated

    @Column(name = "component_type", columnDefinition = "char(32)")
    private String componentType;   // Denormilized type

    @Enumerated
    private ComponentState componentState;

    @Column(length=2048, name = "extra_metrics")
    private String extraMetrics;

    @Column(length=2048, name = "drive_data")
    private String driveData;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Long getClaComponentId() {
        return claComponentId;
    }

    public void setClaComponentId(Long claComponentId) {
        this.claComponentId = claComponentId;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getComponentType() {
        return componentType;
    }

    public void setComponentType(String componentType) {
        this.componentType = componentType;
    }

    public ComponentState getComponentState() {
        return componentState;
    }

    public void setComponentState(ComponentState componentState) {
        this.componentState = componentState;
    }

    public String getExtraMetrics() {
        return extraMetrics;
    }

    public void setExtraMetrics(String extraMetrics) {
        this.extraMetrics = extraMetrics;
    }

    public String getDriveData() {
        return driveData;
    }

    public void setDriveData(String driveData) {
        this.driveData = driveData;
    }

    @Override
    public String toString() {
        return "ClaComponentStatus{" +
                "id=" + id +
                ", claComponentId=" + claComponentId +
                ", timestamp=" + timestamp +
                ", componentType='" + componentType + '\'' +
                ", componentState=" + componentState +
                ", extraMetrics='" + extraMetrics + '\'' +
                '}';
    }
}
