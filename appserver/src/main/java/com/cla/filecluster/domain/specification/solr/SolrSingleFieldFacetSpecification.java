package com.cla.filecluster.domain.specification.solr;

import com.cla.common.domain.dto.kendo.FilterDescriptor;
import com.cla.common.constants.CatFileFieldType;

import java.util.Collection;

/**
 * Created by shtand on 09/09/2015.
 */
public class SolrSingleFieldFacetSpecification extends SolrSpecification {

    private CatFileFieldType facetField;


    public SolrSingleFieldFacetSpecification(FilterDescriptor filterDescriptor, CatFileFieldType facetField) {
        super(filterDescriptor);
        this.facetField = facetField;
    }

    public SolrSingleFieldFacetSpecification(SolrSpecificationFactory solrSpecificationFactory, FilterDescriptor filterDescriptor, CatFileFieldType facetField, Collection<String> queryValues) {
        super(filterDescriptor);
        this.facetField = facetField;
        setFacetQueryValues(queryValues);
    }

    @Override
    public String[] getFacetFields() {
        return new String[]{facetField.getSolrName()};
    }


}
