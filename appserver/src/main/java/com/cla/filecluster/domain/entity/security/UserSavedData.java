package com.cla.filecluster.domain.entity.security;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by uri on 27/04/2016.
 */
@Entity
@Table(indexes = {@Index(name = "user_key_index",columnList = "user, user_key")})
public class UserSavedData implements Serializable {

    @Id
    @GeneratedValue
    Long id;

    @Column(columnDefinition = "char(64)")
    String user;

    @Column(name = "user_key",
            columnDefinition = "char(190)")
    String key;

    @Lob
    String data;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user.getUsername();
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
