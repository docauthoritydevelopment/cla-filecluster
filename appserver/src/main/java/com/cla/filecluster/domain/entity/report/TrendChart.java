package com.cla.filecluster.domain.entity.report;

import com.cla.common.domain.dto.report.ChartDisplayType;
import com.cla.common.domain.dto.report.ChartValueFormatType;
import com.cla.common.domain.dto.report.SortType;
import com.cla.common.domain.dto.report.TimeResolutionType;
import com.cla.filecluster.domain.entity.date.DateRangePoint;
import com.cla.filecluster.domain.entity.security.User;
import com.drew.lang.annotations.Nullable;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;

/**
 * Created by uri on 27/07/2016.
 */
@Entity
@Table(name = "trend_chart")
public class TrendChart {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Access(AccessType.PROPERTY)
    private Long id;

    /**
     * If not null, this trendChart is not collecting data, but uses data from another trendChart
     */
    private Long trendChartCollectorId;

    @NotEmpty
    private String name;

    private ChartDisplayType chartDisplayType;

    private ChartValueFormatType chartValueFormatType;

    @Nullable
    @ManyToOne(optional = true, fetch = FetchType.EAGER)
    private User owner;

    private boolean hidden;

    private String seriesType;

    private String valueType;

    private SortType sortType;

    @Column(length = 4096)
    private String transformerScript;

    private boolean deleted;

    @Nullable
    @Column(length = 4096)
    private String filters;

    /**
     * JSON format UI configuration
     */
    @Nullable
    @Column(length = 2048)
    private String viewConfiguration;

    /**
     * Timestamp for the first (earliest) point
     */
    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "type", column = @Column(name = "start_type"))
            , @AttributeOverride(name = "relativePeriod", column = @Column(name = "start_relative_period"))
            , @AttributeOverride(name = "relativePeriodAmount", column = @Column(name = "start_relative_period_amount"))
            , @AttributeOverride(name = "absoluteTime", column = @Column(name = "start_absolute_time"))
    })
    private DateRangePoint viewFrom;

    /**
     * Maximum time that the chart will show. Nullable
     */
    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "type", column = @Column(name = "end_type"))
            , @AttributeOverride(name = "relativePeriod", column = @Column(name = "end_relative_period"))
            , @AttributeOverride(name = "relativePeriodAmount", column = @Column(name = "end_relative_period_amount"))
            , @AttributeOverride(name = "absoluteTime", column = @Column(name = "end_absolute_time"))
    })
    private DateRangePoint viewTo;

    /**
     * Optional number of points to show
     */
    private Integer viewSampleCount;

    private TimeResolutionType resolutionType;      // default is TR_miliSec
    /**
     * The view resolution in milliseconds (resolutionType = TR_miliSec)
     */
    private Long viewResolutionMs;

    /**
     * The view resolution details (resolutionType != TR_miliSec)
     */
    private Integer viewResolutionCount;    // default is 1
    private Integer viewOffset;             // default is 0
    private Integer viewResolutionStep;     // default is 1

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ChartDisplayType getChartDisplayType() {
        return chartDisplayType;
    }

    public void setChartDisplayType(ChartDisplayType chartDisplayType) {
        this.chartDisplayType = chartDisplayType;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public boolean isHidden() {
        return hidden;
    }

    public void setHidden(boolean hidden) {
        this.hidden = hidden;
    }

    public String getSeriesType() {
        return seriesType;
    }

    public void setSeriesType(String seriesType) {
        this.seriesType = seriesType;
    }

    public String getValueType() {
        return valueType;
    }

    public void setValueType(String valueType) {
        this.valueType = valueType;
    }

    public SortType getSortType() {
        return sortType;
    }

    public void setSortType(SortType sortType) {
        this.sortType = sortType;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public String getFilters() {
        return filters;
    }

    public void setFilters(String filters) {
        this.filters = filters;
    }

    public String getViewConfiguration() {
        return viewConfiguration;
    }

    public void setViewConfiguration(String viewConfiguration) {
        this.viewConfiguration = viewConfiguration;
    }

    public Integer getViewSampleCount() {
        return viewSampleCount;
    }

    public void setViewSampleCount(Integer viewSampleCount) {
        this.viewSampleCount = viewSampleCount;
    }

    public DateRangePoint getViewFrom() {
        return viewFrom;
    }

    public void setViewFrom(DateRangePoint viewFrom) {
        this.viewFrom = viewFrom;
    }

    public DateRangePoint getViewTo() {
        return viewTo;
    }

    public void setViewTo(DateRangePoint viewTo) {
        this.viewTo = viewTo;
    }

    public TimeResolutionType getResolutionType() {
        return resolutionType;
    }

    public void setResolutionType(TimeResolutionType resolutionType) {
        this.resolutionType = resolutionType;
    }

    public Long getViewResolutionMs() {
        return viewResolutionMs;
    }

    public void setViewResolutionMs(Long viewResolutionMs) {
        this.viewResolutionMs = viewResolutionMs;
    }

    public ChartValueFormatType getChartValueFormatType() {
        return chartValueFormatType;
    }

    public void setChartValueFormatType(ChartValueFormatType chartValueFormatType) {
        this.chartValueFormatType = chartValueFormatType;
    }

    public String getTransformerScript() {
        return transformerScript;
    }

    public void setTransformerScript(String transformerScript) {
        this.transformerScript = transformerScript;
    }

    public Long getTrendChartCollectorId() {
        return trendChartCollectorId;
    }

    public void setTrendChartCollectorId(Long trendChartCollectorId) {
        this.trendChartCollectorId = trendChartCollectorId;
    }

    public Integer getViewResolutionCount() {
        return viewResolutionCount;
    }

    public void setViewResolutionCount(Integer viewResolutionCount) {
        this.viewResolutionCount = viewResolutionCount;
    }

    public Integer getViewOffset() {
        return viewOffset;
    }

    public void setViewOffset(Integer viewOffset) {
        this.viewOffset = viewOffset;
    }

    public Integer getViewResolutionStep() {
        return viewResolutionStep;
    }

    public void setViewResolutionStep(Integer viewResolutionStep) {
        this.viewResolutionStep = viewResolutionStep;
    }

    @Override
    public String toString() {
        return "TrendChart{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", seriesType='" + seriesType + '\'' +
                ", filters='" + filters + '\'' +
                '}';
    }
}
