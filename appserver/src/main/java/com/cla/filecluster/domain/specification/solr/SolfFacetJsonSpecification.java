package com.cla.filecluster.domain.specification.solr;

import com.cla.common.domain.dto.kendo.FilterDescriptor;
import com.cla.filecluster.repository.solr.SolrFacetQueryJson;

import java.util.List;

/**
 * Created By Itai Marko
 */
public class SolfFacetJsonSpecification extends SolrSpecification {



    public SolfFacetJsonSpecification(FilterDescriptor filter, List<SolrFacetQueryJson> facetJsonObjectList) {
        super(filter);
        setFacetJsonObjectList(facetJsonObjectList);
    }
}
