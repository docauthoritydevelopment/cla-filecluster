package com.cla.filecluster.domain.exceptions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by Itay on 30/09/2015.
 */
@ResponseStatus(value=HttpStatus.BAD_REQUEST, reason="Bad solr query")
public class BadSolrQuery extends RuntimeException {
    Logger logger = LoggerFactory.getLogger(BadSolrQuery.class);
    
    public BadSolrQuery(String query) {
        super("Bad solr query: "+query);
        logger.warn("{} {}", this.getMessage());
    }

    public BadSolrQuery(String query, Exception cause) {
        super("Bad solr query: "+query, cause);
        logger.warn("{} {}", this.getMessage(),cause);
    }

}
