package com.cla.filecluster.domain.entity.actions;

import com.cla.common.domain.dto.action.ActionClass;

/**
 * Created by uri on 14/02/2016.
 */
public class FileActionDispatch {

    private long id;

    private long claFileId;

    private String url;

    private ActionDefinition actionDefinition;

    private ActionClass actionClass;

    private long actionTriggerId;

    private String properties;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public ActionDefinition getActionDefinition() {
        return actionDefinition;
    }

    public void setActionDefinition(ActionDefinition actionDefinition) {
        this.actionDefinition = actionDefinition;
    }

    public long getClaFileId() {
        return claFileId;
    }

    public void setClaFileId(long claFileId) {
        this.claFileId = claFileId;
    }

    public void setActionClass(ActionClass actionClass) {
        this.actionClass = actionClass;
    }

    public ActionClass getActionClass() {
        return actionClass;
    }

    public void setActionTriggerId(long actionTriggerId) {
        this.actionTriggerId = actionTriggerId;
    }

    public long getActionTriggerId() {
        return actionTriggerId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setProperties(String properties) {
        this.properties = properties;
    }

    public String getProperties() {
        return properties;
    }

    @Override
    public String toString() {
        return "Action Dispatch {" +
                ", url='" + url + '\'' +
                ", actionDefinition=" + actionDefinition +
                ", actionClass=" + actionClass +
                ", actionTriggerId=" + actionTriggerId +
                ", properties='" + properties + '\'' +
                ", id=" + id +
                '}';
    }
}
