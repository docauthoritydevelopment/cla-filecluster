package com.cla.filecluster.domain.entity.filetree;

import org.hibernate.validator.constraints.Length;

import javax.persistence.*;

/**
 * Created by: yael
 * Created on: 2/10/2019
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "mailboxes", uniqueConstraints = {
        @UniqueConstraint(name = "upn_rf_uqc", columnNames = {"root_folder_id", "upn"})
})
public class Mailbox {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Access(AccessType.PROPERTY)
    private Long id;

    @Column(name = "root_folder_id", nullable = false)
    private Long rootFolderId;

    @Column(nullable = false)
    private String upn;

    @Column(name = "sync_state", length = 2048)
    private String syncState;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getRootFolderId() {
        return rootFolderId;
    }

    public void setRootFolderId(Long rootFolderId) {
        this.rootFolderId = rootFolderId;
    }

    public String getUpn() {
        return upn;
    }

    public void setUpn(String upn) {
        this.upn = upn;
    }

    public String getSyncState() {
        return syncState;
    }

    public void setSyncState(String syncState) {
        this.syncState = syncState;
    }
}
