package com.cla.filecluster.domain.dto;

import org.apache.solr.client.solrj.beans.Field;

import java.util.Date;

import static com.cla.common.constants.ExtractionFieldType.*;

@SuppressWarnings("unused")
public class SolrExtractionEntity implements SolrEntity{

    private static final String[] FIELD_NAMES_NO_CONTENT = new String[]{
            ID.getSolrName(), FILE_NAME.getSolrName(), TYPE.getSolrName(), PART.getSolrName(),
            GROUP_ID.getSolrName(),
            CREATION_DATE.getSolrName(), MODIFICATION_DATE.getSolrName(), LAST_ACCESS_DATE.getSolrName(),
            JOB_ID.getSolrName(),
            EXTRACTED_ENTITIES.getSolrName(), EXTRACTED_PATTERNS.getSolrName(),
            ALLOW_TOKEN_DOCUMENTS.getSolrName(), ALLOW_TOKEN_PARENTS.getSolrName(), ALLOW_TOKEN_SHARES.getSolrName(),
            DENY_TOKEN_DOCUMENTS.getSolrName(), DENY_TOKEN_PARENTS.getSolrName(), DENY_TOKEN_SHARES.getSolrName()
    };

    @Field
    private String id;
    @Field
    private Integer type;
    @Field
    private String fileName;
    @Field
    private String part;
    @Field
    private String content;
    @Field
    private String groupId;
    @Field
    private Date creationDate;
    @Field
    private Date modificationDate;
    @Field
    private Date accessDate;
    @Field("last_record_update")
    private Date lastRecordUpdate;
    @Field
    private String jobId;
    @Field
    private String extractedEntities;
    @Field
    private String extractedPatterns;
    @Field
    private String allowTokenDocuments;
    @Field
    private String allowTokenParents;
    @Field
    private String allowTokenShares;
    @Field
    private String denyTokenDocuments;
    @Field
    private String denyTokenParents;
    @Field
    private String denyTokenShares;

    public static String[] getFieldNamesNoContent() {
        return FIELD_NAMES_NO_CONTENT;
    }

    public String getIdAsString() {return id;}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getPart() {
        return part;
    }

    public void setPart(String part) {
        this.part = part;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(Date modificationDate) {
        this.modificationDate = modificationDate;
    }

    public Date getAccessDate() {
        return accessDate;
    }

    public void setAccessDate(Date accessDate) {
        this.accessDate = accessDate;
    }

    public Date getLastRecordUpdate() {
        return lastRecordUpdate;
    }

    public void setLastRecordUpdate(Date lastRecordUpdate) {
        this.lastRecordUpdate = lastRecordUpdate;
    }

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public String getExtractedEntities() {
        return extractedEntities;
    }

    public void setExtractedEntities(String extractedEntities) {
        this.extractedEntities = extractedEntities;
    }

    public String getExtractedPatterns() {
        return extractedPatterns;
    }

    public void setExtractedPatterns(String extractedPatterns) {
        this.extractedPatterns = extractedPatterns;
    }

    public String getAllowTokenParents() {
        return allowTokenParents;
    }

    public void setAllowTokenParents(String allowTokenParents) {
        this.allowTokenParents = allowTokenParents;
    }

    public String getAllowTokenShares() {
        return allowTokenShares;
    }

    public void setAllowTokenShares(String allowTokenShares) {
        this.allowTokenShares = allowTokenShares;
    }

    public String getDenyTokenDocuments() {
        return denyTokenDocuments;
    }

    public void setDenyTokenDocuments(String denyTokenDocuments) {
        this.denyTokenDocuments = denyTokenDocuments;
    }

    public String getDenyTokenParents() {
        return denyTokenParents;
    }

    public void setDenyTokenParents(String denyTokenParents) {
        this.denyTokenParents = denyTokenParents;
    }

    public String getDenyTokenShares() {
        return denyTokenShares;
    }

    public void setDenyTokenShares(String denyTokenShares) {
        this.denyTokenShares = denyTokenShares;
    }

    public String getAllowTokenDocuments() {
        return allowTokenDocuments;
    }

    public void setAllowTokenDocuments(String allowTokenDocuments) {
        this.allowTokenDocuments = allowTokenDocuments;
    }
}
