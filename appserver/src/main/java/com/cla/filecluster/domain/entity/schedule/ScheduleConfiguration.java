package com.cla.filecluster.domain.entity.schedule;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Created by uri on 03/05/2016.
 */
@Entity

public class ScheduleConfiguration {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String cronTemplate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCronTemplate() {
        return cronTemplate;
    }

    public void setCronTemplate(String cronTemplate) {
        this.cronTemplate = cronTemplate;
    }
}
