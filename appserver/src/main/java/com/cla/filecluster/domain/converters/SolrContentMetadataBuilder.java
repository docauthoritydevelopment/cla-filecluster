package com.cla.filecluster.domain.converters;

import com.cla.common.constants.SkipAnalysisReasonType;
import com.cla.common.domain.dto.file.ClaFileState;
import com.cla.common.domain.dto.file.ContentMetadataDto;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.file.FileType;
import com.cla.connector.utils.FileTypeUtils;
import com.cla.filecluster.domain.dto.SolrContentMetadataEntity;
import com.cla.filecluster.repository.solr.query.AtomicUpdateBuilder;
import com.cla.filecluster.repository.solr.query.SolrOperator;
import org.apache.solr.common.SolrInputDocument;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import static com.cla.common.constants.ContentMetadataFieldType.*;

public class SolrContentMetadataBuilder extends SolrEntityBuilder<SolrContentMetadataEntity>{

    private String id;
    private Integer type;
    private Long sampleFileId;
    private ClaFilePropertiesDto props;
    private ClaFileState state = ClaFileState.SCANNED;
    private Integer fileCount;
    private String author;
    private String company;
    private String subject;
    private String title;
    private String keywords;
    private String generatingApp;
    private String template;
    private Long itemsCountL1;
    private Long itemsCountL2;
    private Long itemsCountL3;
    private String fileGroupId;
    private String userGroupId;
    private Long lastIngestMs;
    private String analyzeHint;
    private List<String> proposedTitles;
    private Date appCreationDate;
    private Boolean populationDirty;
    private Boolean groupDirty;
    private SkipAnalysisReasonType skipAnalysisReason;
    private Long currentRunId;
    private String taskState;
    private Long taskStateDate;
    private Integer retries;

    public static SolrContentMetadataBuilder create(){
        return new SolrContentMetadataBuilder();
    }

    @Override
    public SolrContentMetadataEntity build() {
        if (props == null){
            throw new IllegalArgumentException("Missing required parameters");
        }

        SolrContentMetadataEntity entity = new SolrContentMetadataEntity();
        if (id == null) {
            entity.setId(props.getContentMetadataId().toString());
        }
        else{
            entity.setId(id);
        }
        entity.setContentSignature(props.getContentSignature());
        entity.setUserContentSignature(props.getUserContentSignature());

        String fileUrl = props.getFileName();

        // set file type --------------------------------------
        FileType fileType;
        if (props.getType() != null){
            fileType = props.getType();
        }
        else {
            fileType = FileTypeUtils.getFileType(fileUrl);
        }
        entity.setType(fileType.toInt());
        // ----------------------------------------------------

        if (fileCount != null) {
            entity.setFileCount(fileCount);
        } else {
            entity.setFileCount(1);
        }

        if (sampleFileId != null) {
            entity.setSampleFileId(sampleFileId);
        }
        else{
            entity.setSampleFileId(props.getClaFileId());
        }

        entity.setState(state.name());

        Optional.ofNullable(author).ifPresent(entity::setAuthor);
        Optional.ofNullable(company).ifPresent(entity::setCompany);
        Optional.ofNullable(subject).ifPresent(entity::setSubject);
        Optional.ofNullable(title).ifPresent(entity::setTitle);
        Optional.ofNullable(keywords).ifPresent(entity::setKeywords);

        Optional.ofNullable(generatingApp).ifPresent(entity::setGeneratingApp);
        Optional.ofNullable(appCreationDate).ifPresent(entity::setCreationDate);
        Optional.ofNullable(template).ifPresent(entity::setTemplate);

        Optional.ofNullable(itemsCountL1).ifPresent(entity::setItemsCountL1);
        Optional.ofNullable(itemsCountL2).ifPresent(entity::setItemsCountL2);
        Optional.ofNullable(itemsCountL3).ifPresent(entity::setItemsCountL3);

        Optional.ofNullable(fileGroupId).ifPresent(entity::setAnalysisGroupId);
        Optional.ofNullable(userGroupId).ifPresent(entity::setUserGroupId);

        Optional.ofNullable(lastIngestMs).ifPresent(entity::setLastIngestTimestampMs);
        Optional.ofNullable(analyzeHint).ifPresent(entity::setAnalysisHint);
        Optional.ofNullable(proposedTitles).ifPresent(entity::setProposedTitles);
        Optional.ofNullable(populationDirty).ifPresent(entity::setPopulationDirty);
        Optional.ofNullable(groupDirty).ifPresent(entity::setGroupDirty);
        Optional.ofNullable(skipAnalysisReason).ifPresent(value -> entity.setSkipAnalysisReason(value.toInt()));

        Optional.ofNullable(currentRunId).ifPresent(entity::setCurrentRunId);
        Optional.ofNullable(taskState).ifPresent(entity::setTaskState);
        Optional.ofNullable(taskStateDate).ifPresent(entity::setTaskStateDate);
        Optional.ofNullable(retries).ifPresent(entity::setRetries);

        entity.setUpdateDate(System.currentTimeMillis());

        if (populationDirty != null && populationDirty) {
            entity.setPopDirtyDate(System.currentTimeMillis());
        }

        if (groupDirty != null && groupDirty) {
            entity.setGroupDirtyDate(System.currentTimeMillis());
        }

        return entity;
    }

    @Override
    public void update(final SolrContentMetadataEntity entity) {
        if (props != null) {
            if (props.getContentSignature() != null) {
                entity.setContentSignature(props.getContentSignature());
            }
            if (props.getUserContentSignature() != null) {
                entity.setUserContentSignature(props.getUserContentSignature());
            }
            entity.setSampleFileId(props.getClaFileId());
        }

        if (sampleFileId != null) {
            entity.setSampleFileId(sampleFileId);
        }

        if (fileCount != null) {
            entity.setFileCount(fileCount);
        }

        entity.setState(state.name());

        Optional.ofNullable(author).ifPresent(entity::setAuthor);
        Optional.ofNullable(company).ifPresent(entity::setCompany);
        Optional.ofNullable(subject).ifPresent(entity::setSubject);
        Optional.ofNullable(title).ifPresent(entity::setTitle);
        Optional.ofNullable(keywords).ifPresent(entity::setKeywords);

        Optional.ofNullable(generatingApp).ifPresent(entity::setGeneratingApp);
        Optional.ofNullable(appCreationDate).ifPresent(entity::setCreationDate);
        Optional.ofNullable(template).ifPresent(entity::setTemplate);

        Optional.ofNullable(itemsCountL1).ifPresent(entity::setItemsCountL1);
        Optional.ofNullable(itemsCountL2).ifPresent(entity::setItemsCountL2);
        Optional.ofNullable(itemsCountL3).ifPresent(entity::setItemsCountL3);

        Optional.ofNullable(fileGroupId).ifPresent(entity::setAnalysisGroupId);
        Optional.ofNullable(userGroupId).ifPresent(entity::setUserGroupId);

        Optional.ofNullable(lastIngestMs).ifPresent(entity::setLastIngestTimestampMs);
        Optional.ofNullable(analyzeHint).ifPresent(entity::setAnalysisHint);
        Optional.ofNullable(proposedTitles).ifPresent(entity::setProposedTitles);
        Optional.ofNullable(populationDirty).ifPresent(entity::setPopulationDirty);
        Optional.ofNullable(groupDirty).ifPresent(entity::setGroupDirty);
        Optional.ofNullable(skipAnalysisReason).ifPresent(value -> entity.setSkipAnalysisReason(value.toInt()));

        Optional.ofNullable(currentRunId).ifPresent(entity::setCurrentRunId);
        Optional.ofNullable(taskState).ifPresent(entity::setTaskState);
        Optional.ofNullable(taskStateDate).ifPresent(entity::setTaskStateDate);
        Optional.ofNullable(retries).ifPresent(entity::setRetries);

        entity.setUpdateDate(System.currentTimeMillis());

        if (populationDirty != null && populationDirty) {
            entity.setPopDirtyDate(System.currentTimeMillis());
        }

        if (groupDirty != null && groupDirty) {
            entity.setGroupDirtyDate(System.currentTimeMillis());
        }
    }

    @Override
    public SolrInputDocument buildAtomicUpdate() {
        if (id == null){
            throw new IllegalArgumentException("Missing required parameters");
        }
        AtomicUpdateBuilder builder = AtomicUpdateBuilder.create();
        builder.setId(ID, id);

        // set file type --------------------------------------
        if (type != null){
            builder.addModifier(TYPE, SolrOperator.SET, type);
        } else if (props.getType() != null) {
            builder.addModifier(TYPE, SolrOperator.SET, props.getType().toInt());
        } else if (props.getFileName() != null) {
            FileType fileType = FileTypeUtils.getFileType(props.getFileName());
            builder.addModifier(TYPE, SolrOperator.SET, fileType.toInt());
        }

        if (props != null) {
            builder.addModifierNotNull(CONTENT_SIGNATURE, SolrOperator.SET, props.getContentSignature());
            builder.addModifierNotNull(USER_CONTENT_SIGNATURE, SolrOperator.SET, props.getUserContentSignature());
            builder.addModifierNotNull(SAMPLE_FILE_ID, SolrOperator.SET, props.getClaFileId());
            builder.addModifierNotNull(SIZE, SolrOperator.SET, props.getFileSize());
        }

        if ((props == null || props.getClaFileId() == null) && sampleFileId != null) {
            builder.addModifier(SAMPLE_FILE_ID, SolrOperator.SET, sampleFileId);
        }

        if (fileCount != null) {
            if (fileCount <= 1) {
                builder.addModifierNotNull(FILE_COUNT, SolrOperator.SET, 1);
            } else {
                builder.addModifierNotNull(FILE_COUNT, SolrOperator.INC, 1);
            }
        }

        if (state != null) {
            builder.addModifier(PROCESSING_STATE, SolrOperator.SET, state.name());
        }

        builder.addModifierNotNull(AUTHOR, SolrOperator.SET, author);
        builder.addModifierNotNull(COMPANY, SolrOperator.SET, company);
        builder.addModifierNotNull(SUBJECT, SolrOperator.SET, subject);
        builder.addModifierNotNull(TITLE, SolrOperator.SET, title);
        builder.addModifierNotNull(KEYWORDS, SolrOperator.SET, keywords);

        builder.addModifierNotNull(GENERATING_APP, SolrOperator.SET, generatingApp);
        builder.addModifierNotNull(TEMPLATE, SolrOperator.SET, template);

        builder.addModifierNotNull(ITEMS_COUNT1, SolrOperator.SET, itemsCountL1);
        builder.addModifierNotNull(ITEMS_COUNT2, SolrOperator.SET, itemsCountL2);
        builder.addModifierNotNull(ITEMS_COUNT3, SolrOperator.SET, itemsCountL3);

        builder.addModifierNotNull(GROUP_ID, SolrOperator.SET, fileGroupId);
        builder.addModifierNotNull(USER_GROUP_ID, SolrOperator.SET, userGroupId);

        builder.addModifierNotNull(LAST_INGEST_TIMESTAMP_MS, SolrOperator.SET, lastIngestMs);
        builder.addModifierNotNull(ANALYSIS_HINT, SolrOperator.SET, analyzeHint);

        builder.addModifierNotNull(PROPOSED_TITLES, SolrOperator.SET, proposedTitles);
        builder.addModifierNotNull(CREATION_DATE, SolrOperator.SET, appCreationDate);
        builder.addModifierNotNull(POPULATION_DIRTY, SolrOperator.SET, populationDirty);
        builder.addModifierNotNull(GROUP_DIRTY, SolrOperator.SET, groupDirty);

        if (skipAnalysisReason != null) {
            builder.addModifier(SKIP_ANALYSIS_REASON, SolrOperator.SET, skipAnalysisReason.toInt());
        }

        if (populationDirty != null && populationDirty) {
            builder.addModifierNotNull(POP_DIRTY_DATE, SolrOperator.SET,System.currentTimeMillis());
        }

        if (groupDirty != null && groupDirty) {
            builder.addModifierNotNull(GROUP_DIRTY_DATE, SolrOperator.SET,System.currentTimeMillis());
        }

        builder.addModifierNotNull(CURRENT_RUN_ID, SolrOperator.SET, currentRunId);
        builder.addModifierNotNull(TASK_STATE, SolrOperator.SET, taskState);
        builder.addModifierNotNull(TASK_STATE_DATE, SolrOperator.SET, taskStateDate);
        builder.addModifierNotNull(RETRIES, SolrOperator.SET, retries);

        builder.addModifier(UPDATE_DATE, SolrOperator.SET, System.currentTimeMillis());

        return builder.build();
    }

    public SolrContentMetadataBuilder setId(String id) {
        this.id = id;
        return this;
    }

    public SolrContentMetadataBuilder setType(Integer type) {
        this.type = type;
        return this;
    }

    public SolrContentMetadataBuilder setSampleFileId(Long sampleFileId) {
        this.sampleFileId = sampleFileId;
        return this;
    }

    public SolrContentMetadataBuilder setProps(ClaFilePropertiesDto props) {
        this.props = props;
        return this;
    }

    public SolrContentMetadataBuilder setState(ClaFileState state) {
        this.state = state;
        return this;
    }

    public SolrContentMetadataBuilder setFileCount(Integer fileCount) {
        this.fileCount = fileCount;
        return this;
    }

    public SolrContentMetadataBuilder setAuthor(String author) {
        this.author = author;
        return this;
    }

    public SolrContentMetadataBuilder setCompany(String company) {
        this.company = company;
        return this;
    }

    public SolrContentMetadataBuilder setSubject(String subject) {
        this.subject = subject;
        return this;
    }

    public SolrContentMetadataBuilder setTitle(String title) {
        this.title = title;
        return this;
    }

    public SolrContentMetadataBuilder setKeywords(String keywords) {
        this.keywords = keywords;
        return this;
    }

    public SolrContentMetadataBuilder setGeneratingApp(String generatingApp) {
        this.generatingApp = generatingApp;
        return this;
    }

    public SolrContentMetadataBuilder setTemplate(String template) {
        this.template = template;
        return this;
    }

    public SolrContentMetadataBuilder setItemsCountL1(Long itemsCountL1) {
        this.itemsCountL1 = itemsCountL1;
        return this;
    }

    public SolrContentMetadataBuilder setItemsCountL2(Long itemsCountL2) {
        this.itemsCountL2 = itemsCountL2;
        return this;
    }

    public SolrContentMetadataBuilder setItemsCountL3(Long itemsCountL3) {
        this.itemsCountL3 = itemsCountL3;
        return this;
    }

    public SolrContentMetadataBuilder setFileGroupId(String fileGroupId) {
        this.fileGroupId = fileGroupId;
        return this;
    }

    public SolrContentMetadataBuilder setUserGroupId(String userGroupId) {
        this.userGroupId = userGroupId;
        return this;
    }

    public SolrContentMetadataBuilder setLastIngestMs(Long lastIngestMs) {
        this.lastIngestMs = lastIngestMs;
        return this;
    }

    public SolrContentMetadataBuilder setAnalyzeHint(String analyzeHint) {
        this.analyzeHint = analyzeHint;
        return this;
    }

    public SolrContentMetadataBuilder setProposedTitles(List<String> proposedTitles) {
        this.proposedTitles = proposedTitles;
        return this;
    }

    public SolrContentMetadataBuilder setAppCreationDate(Date appCreationDate) {
        this.appCreationDate = appCreationDate;
        return this;
    }

    public SolrContentMetadataBuilder setPopulationDirty(Boolean populationDirty) {
        this.populationDirty = populationDirty;
        return this;
    }

    public SolrContentMetadataBuilder setGroupDirty(Boolean groupDirty) {
        this.groupDirty = groupDirty;
        return this;
    }

    public SolrContentMetadataBuilder setSkipAnalysisReason(SkipAnalysisReasonType skipAnalysisReason) {
        this.skipAnalysisReason = skipAnalysisReason;
        return this;
    }

    public SolrContentMetadataBuilder setCurrentRunId(Long currentRunId) {
        this.currentRunId = currentRunId;
        return this;
    }

    public SolrContentMetadataBuilder setTaskState(String taskState) {
        this.taskState = taskState;
        return this;
    }

    public SolrContentMetadataBuilder setTaskStateDate(Long taskStateDate) {
        this.taskStateDate = taskStateDate;
        return this;
    }

    public SolrContentMetadataBuilder setRetries(Integer retries) {
        this.retries = retries;
        return this;
    }

    public void setFromDto(ContentMetadataDto contentMetadata) {
        setId(String.valueOf(contentMetadata.getContentId()));
        setType(contentMetadata.getType().toInt());
        setState(contentMetadata.getState());

        ClaFilePropertiesDto props = ClaFilePropertiesDto.create();
        props.setContentSignature(contentMetadata.getContentSignature());
        props.setUserContentSignature(contentMetadata.getUserContentSignature());
        props.setFileSize(contentMetadata.getFsFileSize());
        setProps(props);

        setFileGroupId(contentMetadata.getAnalysisGroupId());
        setUserGroupId(contentMetadata.getUserGroupId());
        setTitle(contentMetadata.getTitle());
        setTemplate(contentMetadata.getTemplate());
        setAuthor(contentMetadata.getAuthor());
        setGeneratingApp(contentMetadata.getGeneratingApp());
        setKeywords(contentMetadata.getKeywords());
        setSubject(contentMetadata.getSubject());
        setSampleFileId(contentMetadata.getSampleFileId());
        setCompany(contentMetadata.getCompany());

        setAnalyzeHint(contentMetadata.getAnalyzeHint().name());
        setProposedTitles(contentMetadata.getProposedTitles());
        setAppCreationDate(contentMetadata.getAppCreationDate());
    }

    public boolean idWasSet() {
        return (id != null && !id.isEmpty()) || (props.getContentMetadataId() != null);
    }
}
