package com.cla.filecluster.domain.entity.schedule;

import com.cla.common.domain.dto.crawler.RunStatus;
import com.cla.common.domain.dto.filetree.CrawlRunDetailsDto;
import com.cla.common.domain.dto.security.UserDto;
import com.cla.filecluster.domain.entity.security.ClaUserDetails;
import com.cla.filecluster.domain.entity.security.User;
import com.cla.filecluster.mediaproc.StartScanProducer;
import com.cla.filecluster.service.crawler.FileCrawlerExecutionDetailsService;
import com.cla.filecluster.service.files.managers.ExecutionManagerService;
import com.cla.filecluster.service.schedule.ScheduleAppService;
import com.cla.filecluster.service.schedule.ScheduleGroupService;
import com.cla.filecluster.service.security.UserService;
import com.cla.filecluster.util.FileCrawlerUtils;
import com.cla.filecluster.util.query.DBTemplateUtils;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

/**
 * Created by uri on 03/05/2016.
 */
@Component
@Scope("prototype")
public class RescanJob implements Runnable {

    public static final String RESCAN_GROUP = "rescan";
    //public static final String SCHEDULE_GROUP_ID = "scheduleGroupId";

    private static final Logger logger = LoggerFactory.getLogger(RescanJob.class);

    @Autowired
    private ScheduleAppService scheduleAppService;

    @Autowired
    private FileCrawlerExecutionDetailsService fileCrawlerExecutionDetailsService;

    @Autowired
    private StartScanProducer startScanProducer;

    @Autowired
    private ExecutionManagerService executionManagerService;

    @Autowired
    private UserService userService;

    @Autowired
    private DBTemplateUtils DBTemplateUtils;

    private Long groupId;

    @Autowired
    private ScheduleGroupService scheduleGroupService;

    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }

    @Override
    public void run() {

        try {
            DBTemplateUtils.doInTransaction(() -> {

                CrawlRunDetailsDto scheduleGroupCrawlRun = fileCrawlerExecutionDetailsService.getScheduleGroupCrawlRun(groupId);
                if (scheduleGroupCrawlRun != null && RunStatus.RUNNING.equals(scheduleGroupCrawlRun.getRunOutcomeState())) {
                    logger.warn("Schedule group is already RUNNING. Skipping current run");
                    scheduleGroupService.markScheduleGroupAsMissed(groupId);
                    return;
                }
                scheduleAppService.printFutureSchedules();
                //        fillAuthentication(groupId);
            });
            executionManagerService.setAsExecutionThread(Thread.currentThread());

            startScanProducer.startScanScheduleGroup(groupId, false, FileCrawlerUtils.allOperations, false);

        } catch (RuntimeException e) {
            logger.error("Failed to execute schedule job on group {} ({})", groupId, e.getMessage(), e);
        } catch (Exception e) {
            logger.error("Exception during RescanJob on group {} ({})", groupId, e.getMessage(), e);
        } catch (Error e) {
            logger.error("Irrecoverable error during RescanJob on group {} ({})", groupId, e.getMessage(), e);
        }
    }

    private void fillAuthentication(Long groupId) {
        User scheduleUser = userService.getScheduleUser();
        UserDto userDto = UserService.convertUser(scheduleUser,true);
        ClaUserDetails claUserDetails = new ClaUserDetails(userDto);
        logger.warn("Fill authentication for schedule - set user {}",scheduleUser);
        Authentication auth = new UsernamePasswordAuthenticationToken(claUserDetails, scheduleUser.getPassword(), claUserDetails.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(auth);
    }

    public void execute(JobExecutionContext context) throws JobExecutionException {
        JobDataMap dataMap = context.getJobDetail().getJobDataMap();
        Long groupId = dataMap.getLong(RESCAN_GROUP);
        logger.debug("Executing rescan job on schedule group {}",groupId);
    }
}
