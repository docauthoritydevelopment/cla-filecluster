package com.cla.filecluster.domain.entity.monitor;

import com.cla.common.domain.dto.components.ClaComponentType;
import com.cla.common.domain.dto.components.ComponentState;
import com.cla.filecluster.domain.entity.dataCenter.CustomerDataCenter;

import javax.persistence.*;

/**
 * Created by itay on 22/02/2017.
 */

@Entity
@Table(name = "mon_components",
        indexes = {
                @Index(name = "idx_type",columnList = "component_type")
                ,@Index(name = "idx_instance_component",columnList = "instanceId,claComponentType",unique=true)
        }
)
public class ClaComponent {
    @Id
    @Access(AccessType.PROPERTY)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(nullable = false,updatable = false)
   // @Enumerated
    private ClaComponentType claComponentType;

    @Column(name = "component_type", columnDefinition = "char(32)")
    private String componentType;

    @Column(nullable = false)
    private String instanceId;  // Ordinal within the type

    private String location;

    private String externalNetAddress;

    @Column(name = "local_net_address", columnDefinition = "varchar(2048)")
    private String localNetAddress;

    private Long createdOnDB;    // Time when was added to the system

    private Boolean active = true;

    private ComponentState state;

    private Long stateChangeDate;

    private Long lastResponseDate;

    private Long lastStatusId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "customer_data_center_id")
     private CustomerDataCenter customerDataCenter;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public ClaComponentType getClaComponentType() {
        return claComponentType;
    }

    public void setClaComponentType(ClaComponentType claComponentType) {
        this.claComponentType = claComponentType;
    }

    public String getComponentType() {
        return componentType;
    }

    public void setComponentType(String componentType) {
        this.componentType = componentType;
    }

    public String getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getExternalNetAddress() {
        return externalNetAddress;
    }

    public void setExternalNetAddress(String externalNetAddress) {
        this.externalNetAddress = externalNetAddress;
    }

    public String getLocalNetAddress() {
        return localNetAddress;
    }

    public void setLocalNetAddress(String localNetAddress) {
        this.localNetAddress = localNetAddress;
    }

    public Long getCreatedOnDB() {
        return createdOnDB;
    }

    @PrePersist
    public void setCreatedOnDB() {
        this.createdOnDB = System.currentTimeMillis();
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public ComponentState getState() {
        return state;
    }

    public void setState(ComponentState state) {
        this.state = state;
    }

    public Long getStateChangeDate() {
        return stateChangeDate;
    }

    public void setStateChangeDate(Long stateChangeDate) {
        this.stateChangeDate = stateChangeDate;
    }

    public Long getLastResponseDate() {
        return lastResponseDate;
    }

    public void setLastResponseDate(Long lastResponseDate) {
        this.lastResponseDate = lastResponseDate;
    }

    public CustomerDataCenter getCustomerDataCenter() {
        return customerDataCenter;
    }

    public void setCustomerDataCenter(CustomerDataCenter customerDataCenter) {
        this.customerDataCenter = customerDataCenter;
    }

    public Long getLastStatusId() {
        return lastStatusId;
    }

    public void setLastStatusId(Long lastStatusId) {
        this.lastStatusId = lastStatusId;
    }

    @Override
    public String toString() {
        return "ClaComponent{" +
                "id=" + id +
                ", claComponentType=" + claComponentType +
                ", componentType='" + componentType + '\'' +
                ", instanceId='" + instanceId + '\'' +
                ", location='" + location + '\'' +
                ", externalNetAddress='" + externalNetAddress + '\'' +
                ", localNetAddress='" + localNetAddress + '\'' +
                ", createdOnDB=" + createdOnDB +
                ", active=" + active +
                ", state=" + state +
                ", stateChangeDate=" + stateChangeDate +
                '}';
    }
}
