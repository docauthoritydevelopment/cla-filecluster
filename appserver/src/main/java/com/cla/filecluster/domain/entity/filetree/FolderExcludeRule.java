package com.cla.filecluster.domain.entity.filetree;

import javax.persistence.*;

/**
 * Created by uri on 04/04/2016.
 */
@Entity
@Table(indexes = {@Index(name = "rootfolder_idx",columnList = "root_folder_id")})
public class FolderExcludeRule {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Access(AccessType.PROPERTY)
    Long id;

    @ManyToOne
    @JoinColumn(name = "root_folder_id")
    RootFolder rootFolder;

    String matchString;

    private String operator;

    private boolean disabled;

    public RootFolder getRootFolder() {
        return rootFolder;
    }

    public void setRootFolder(RootFolder rootFolder) {
        this.rootFolder = rootFolder;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMatchString() {
        return matchString;
    }

    public void setMatchString(String matchString) {
        this.matchString = matchString;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getOperator() {
        return operator;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }
}
