package com.cla.filecluster.domain.entity.pv;

import com.cla.common.domain.dto.file.AnalyzeHint;
import com.cla.common.domain.dto.file.ClaFileState;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

/**
 * Document meta-data
 * Created by uri on 08/03/2016.
 */
public class ContentMetadata {

    final private static int defaultStrFieldSize = 256;

    private Long id;

    private String contentSignature;       // signature of the whole file (or of the first part of it, if the size exceeds threshold)

    private String userContentSignature;   // signature of the user content, excluding metadata


    private boolean populationDirty;

    private int fileCount = 0;

    private boolean groupDirty;

    private Long sampleFileId;

    private String author;      // in Solr CatFile
    private String company;     // in Solr CatFile
    private String subject;     // in Solr CatFile
    private String title;       // in Solr CatFile
    private String keywords;        // NOT in Solr
    private String generatingApp;   // NOT in Solr
    private String template;        // NOT in Solr

    /**
     * The timestamp that the application layer (word/excel/pdf) said the file was created at
     */
    private Date appCreationDate;   // in Solr CatFile (goes into creationDate)

    private Long fsFileSize;        // in Solr CatFile -> size

    private Long itemsCountL1;      // in Solr CatFile
    private Long itemsCountL2;      // in Solr CatFile
    private Long itemsCountL3;      // in Solr CatFile

    @NotNull
    private Integer type;           // FileType

    private String analysisGroupId;        // in Solr CatFile

    private String userGroupId;            // in Solr CatFile

    private ClaFileState state = ClaFileState.INGESTED;

    private AnalyzeHint analyzeHint = AnalyzeHint.NORMAL;

    private long lastIngestTimestampMs;

    private boolean alreadyAccessed;

    private List<String> extractedEntitiesIds;

    public String getContentSignature() {
        return contentSignature;
    }

    public void setContentSignature(String contentSignature) {
        this.contentSignature = contentSignature;
    }

    public String getUserContentSignature() {
        return userContentSignature;
    }

    public void setUserContentSignature(String userContentSignature) {
        this.userContentSignature = userContentSignature;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(final String str) {
        if (str == null) return;
        final int len = str.length();
        this.author = (len < defaultStrFieldSize) ? str : str.substring(0, defaultStrFieldSize - 1);
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(final String str) {
        if (str == null) return;
        final int len = str.length();
        this.company = (len < defaultStrFieldSize) ? str : str.substring(0, defaultStrFieldSize - 1);
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(final String str) {
        if (str == null) return;
        final int len = str.length();
        this.subject = (len < defaultStrFieldSize) ? str : str.substring(0, defaultStrFieldSize - 1);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(final String str) {
        if (str == null) return;
        final int len = str.length();
        this.title = (len < defaultStrFieldSize) ? str : str.substring(0, defaultStrFieldSize - 1);
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(final String str) {
        if (str == null) return;
        final int len = str.length();
        this.keywords = (len < defaultStrFieldSize) ? str : str.substring(0, defaultStrFieldSize - 1);
    }

    public String getGeneratingApp() {
        return generatingApp;
    }

    public void setGeneratingApp(final String str) {
        if (str == null) return;
        final int len = str.length();
        this.generatingApp = (len < defaultStrFieldSize) ? str : str.substring(0, defaultStrFieldSize - 1);
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(final String str) {
        if (str == null) return;
        final int len = str.length();
        this.template = (len < defaultStrFieldSize) ? str : str.substring(0, defaultStrFieldSize - 1);
    }

    public ClaFileState getState() {
        return state;
    }

    public void setState(ClaFileState state) {
        this.state = state;
    }

    public Long getFsFileSize() {
        return fsFileSize;
    }

    public void setFsFileSize(Long fsFileSize) {
        this.fsFileSize = fsFileSize;
    }

    public String getAnalysisGroupId() {
        return analysisGroupId;
    }

    public void setAnalysisGroupId(String analysisGroupId) {
        this.analysisGroupId = analysisGroupId;
    }

    public String getUserGroupId() {
        return userGroupId;
    }

    public void setUserGroupId(String userGroupId) {
        this.userGroupId = userGroupId;
    }

    public boolean isGroupDirty() {
        return groupDirty;
    }

    @Deprecated
    public void setGroupDirty(boolean groupDirty) {
        this.groupDirty = groupDirty;
    }

    public Long getSampleFileId() {
        return sampleFileId;
    }

    public void setSampleFileId(Long sampleFileId) {
        this.sampleFileId = sampleFileId;
    }

    public int getFileCount() {
        return fileCount;
    }

    public void setFileCount(int fileCount) {
        this.fileCount = fileCount;
    }

    public void decreaseFileCount() {
        populationDirty = true;
        fileCount--;
    }

    public void increaseFileCount() {
        populationDirty = true;
        fileCount++;
    }

    public Long getItemsCountL1() {
        return itemsCountL1;
    }

    public void setItemsCountL1(final long itemsCountL1) {
        this.itemsCountL1 = itemsCountL1;
    }

    public Long getItemsCountL2() {
        return itemsCountL2;
    }

    public void setItemsCountL2(final long itemsCountL2) {
        this.itemsCountL2 = itemsCountL2;
    }

    public Long getItemsCountL3() {
        return itemsCountL3;
    }

    public void setItemsCountL3(final long itemsCountL3) {
        this.itemsCountL3 = itemsCountL3;
    }

    public Date getAppCreationDate() {
        return appCreationDate;
    }

    public void setAppCreationDate(Date appCreationDate) {
        this.appCreationDate = appCreationDate;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public boolean isPopulationDirty() {
        return populationDirty;
    }

    public void setPopulationDirty(boolean populationDirty) {
        this.populationDirty = populationDirty;
    }

    public AnalyzeHint getAnalyzeHint() {
        return analyzeHint;
    }

    public void setAnalyzeHint(AnalyzeHint analyzeHint) {
        this.analyzeHint = analyzeHint;
    }

    public long getLastIngestTimestampMs() {
        return lastIngestTimestampMs;
    }

    public void setLastIngestTimestampMs(long lastIngestTimestampMs) {
        this.lastIngestTimestampMs = lastIngestTimestampMs;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("ContentMetadata{");
        sb.append("id=").append(id);
        sb.append(", contentSignature='").append(contentSignature).append('\'');
        sb.append(", userContentSignature='").append(userContentSignature).append('\'');
        sb.append(", fileCount=").append(fileCount);
        sb.append(", fsFileSize=").append(fsFileSize);
        sb.append(", type=").append(type);
        sb.append(", state=").append(state);
        sb.append('}');
        return sb.toString();
    }

    public String toFullString() {
        final StringBuffer sb = new StringBuffer("ContentMetadata{");
        sb.append("id=").append(id);
        sb.append(", contentSignature='").append(contentSignature).append('\'');
        sb.append(", userContentSignature='").append(userContentSignature).append('\'');
        sb.append(", populationDirty=").append(populationDirty);
        sb.append(", fileCount=").append(fileCount);
        sb.append(", groupDirty=").append(groupDirty);
        sb.append(", sampleFileId=").append(sampleFileId);
        sb.append(", author='").append(author).append('\'');
        sb.append(", company='").append(company).append('\'');
        sb.append(", subject='").append(subject).append('\'');
        sb.append(", title='").append(title).append('\'');
        sb.append(", keywords='").append(keywords).append('\'');
        sb.append(", generatingApp='").append(generatingApp).append('\'');
        sb.append(", template='").append(template).append('\'');
        sb.append(", appCreationDate=").append(appCreationDate);
        sb.append(", fsFileSize=").append(fsFileSize);
        sb.append(", itemsCountL1=").append(itemsCountL1);
        sb.append(", itemsCountL2=").append(itemsCountL2);
        sb.append(", itemsCountL3=").append(itemsCountL3);
        sb.append(", type=").append(type);
        sb.append(", analysisGroup=").append(analysisGroupId);
        sb.append(", userGroup=").append(userGroupId);
        sb.append(", state=").append(state);
        sb.append(", analyzeHint=").append(analyzeHint);
        sb.append(", lastIngestTimestampMs=").append(lastIngestTimestampMs);
        sb.append('}');
        return sb.toString();
    }

    public void setAlreadyAccessed(boolean alreadyAccessed) {
        this.alreadyAccessed = alreadyAccessed;
    }

    public boolean isAlreadyAccessed() {
        return alreadyAccessed;
    }

    public List<String> getExtractedEntitiesIds() {
        return extractedEntitiesIds;
    }

    public void setExtractedEntitiesIds(List<String> extractedEntitiesIds) {
        this.extractedEntitiesIds = extractedEntitiesIds;
    }
}
