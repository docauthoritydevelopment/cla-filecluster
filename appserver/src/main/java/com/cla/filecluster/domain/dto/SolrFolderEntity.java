package com.cla.filecluster.domain.dto;

import org.apache.solr.client.solrj.beans.Field;

import java.util.List;

public class SolrFolderEntity implements SolrEntity {

    @Field
    private String id;
    @Field
    private String path;
    @Field
    private String realPath;
    @Field
    private String mediaEntityId;
    @Field
    private String name;
    @Field
    private String folderHash;
    @Field
    private Long rootFolderId;
    @Field
    private Long parentFolderId;
    @Field
    private Integer depthFromRoot;
    @Field
    private Integer absoluteDepth;
    @Field
    private List<String> parentsInfo;
    @Field
    private String pathDescriptorType;
    @Field
    private String aclSignature;
    @Field("acl_read")
    private List<String> aclRead;
    @Field("acl_write")
    private List<String> aclWrite;
    @Field("acl_denied_read")
    private List<String> aclDeniedRead;
    @Field("acl_denied_write")
    private List<String> aclDeniedWrite;
    @Field
    private Boolean deleted;
    @Field
    private int type;
    @Field
    private String mailboxUpn;
    @Field
    private String syncState;
    @Field
    private List<String> associations;
    @Field
    private Long creationDate;
    @Field
    private Long updateDate;

    public String getIdAsString() {return id;}

    public Long getId() {
        return Long.parseLong(id);
    }

    public void setId(Long id) {
        this.id = Long.toString(id);
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getRealPath() {
        return realPath;
    }

    public void setRealPath(String realPath) {
        this.realPath = realPath;
    }

    public String getMediaEntityId() {
        return mediaEntityId;
    }

    public void setMediaEntityId(String mediaEntityId) {
        this.mediaEntityId = mediaEntityId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFolderHash() {
        return folderHash;
    }

    public void setFolderHash(String folderHash) {
        this.folderHash = folderHash;
    }

    public Long getRootFolderId() {
        return rootFolderId;
    }

    public void setRootFolderId(Long rootFolderId) {
        this.rootFolderId = rootFolderId;
    }

    public Long getParentFolderId() {
        return parentFolderId;
    }

    public void setParentFolderId(Long parentFolderId) {
        this.parentFolderId = parentFolderId;
    }

    public Integer getDepthFromRoot() {
        return depthFromRoot;
    }

    public void setDepthFromRoot(Integer depthFromRoot) {
        this.depthFromRoot = depthFromRoot;
    }

    public Integer getAbsoluteDepth() {
        return absoluteDepth;
    }

    public void setAbsoluteDepth(Integer absoluteDepth) {
        this.absoluteDepth = absoluteDepth;
    }

    public List<String> getParentsInfo() {
        return parentsInfo;
    }

    public void setParentsInfo(List<String> parentsInfo) {
        this.parentsInfo = parentsInfo;
    }

    public String getPathDescriptorType() {
        return pathDescriptorType;
    }

    public void setPathDescriptorType(String pathDescriptorType) {
        this.pathDescriptorType = pathDescriptorType;
    }

    public String getAclSignature() {
        return aclSignature;
    }

    public void setAclSignature(String aclSignature) {
        this.aclSignature = aclSignature;
    }

    public List<String> getAclRead() {
        return aclRead;
    }

    public void setAclRead(List<String> aclRead) {
        this.aclRead = aclRead;
    }

    public List<String> getAclWrite() {
        return aclWrite;
    }

    public void setAclWrite(List<String> aclWrite) {
        this.aclWrite = aclWrite;
    }

    public List<String> getAclDeniedRead() {
        return aclDeniedRead;
    }

    public void setAclDeniedRead(List<String> aclDeniedRead) {
        this.aclDeniedRead = aclDeniedRead;
    }

    public List<String> getAclDeniedWrite() {
        return aclDeniedWrite;
    }

    public void setAclDeniedWrite(List<String> aclDeniedWrite) {
        this.aclDeniedWrite = aclDeniedWrite;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getSyncState() {
        return syncState;
    }

    public void setSyncState(String syncState) {
        this.syncState = syncState;
    }

    public String getMailboxUpn() {
        return mailboxUpn;
    }

    public void setMailboxUpn(String mailboxUpn) {
        this.mailboxUpn = mailboxUpn;
    }

    public List<String> getAssociations() {
        return associations;
    }

    public void setAssociations(List<String> associations) {
        this.associations = associations;
    }

    public Long getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Long creationDate) {
        this.creationDate = creationDate;
    }

    public Long getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Long updateDate) {
        this.updateDate = updateDate;
    }
}
