package com.cla.filecluster.domain.entity.filetree;

import com.cla.filecluster.domain.entity.ModifiableEntity;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import java.nio.file.attribute.AclEntryType;

/**
 * Created by: yael
 * Created on: 9/16/2019
 */
@Entity
public class RootFolderSharePermission extends ModifiableEntity {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Access(AccessType.PROPERTY)
    private Long id;

    @Column(name = "root_folder_id")
    private Long rootFolderId;

    private String user;

    @Column(name = "acl_entry_type")
    @Enumerated(value = EnumType.STRING)
    private AclEntryType aclEntryType;

    private Long mask;

    @Length(max=2048)
    private String textPermissions;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getRootFolderId() {
        return rootFolderId;
    }

    public void setRootFolderId(Long rootFolderId) {
        this.rootFolderId = rootFolderId;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public AclEntryType getAclEntryType() {
        return aclEntryType;
    }

    public void setAclEntryType(AclEntryType aclEntryType) {
        this.aclEntryType = aclEntryType;
    }

    public Long getMask() {
        return mask;
    }

    public void setMask(Long mask) {
        this.mask = mask;
    }

    public String getTextPermissions() {
        return textPermissions;
    }

    public void setTextPermissions(String textPermissions) {
        this.textPermissions = textPermissions;
    }

    @Override
    public String toString() {
        return "RootFolderSharePermission{" +
                "id=" + id +
                ", rootFolderId=" + rootFolderId +
                ", user='" + user + '\'' +
                ", aclEntryType=" + aclEntryType +
                ", mask=" + mask +
                ", textPermissions='" + textPermissions + '\'' +
                '}';
    }
}
