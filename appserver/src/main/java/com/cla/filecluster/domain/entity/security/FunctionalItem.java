package com.cla.filecluster.domain.entity.security;

import java.util.Set;

/**
 * System entity with restricted access, such as Folder, DocType, Tag, Group etc.
 */
public interface FunctionalItem {

    String FILE = "file";
    String FOLDER = "folder";
    String GROUP = "group";
    String TAG = "tag";
    String DOC_TYPE = "doc-type";

    Set<FunctionalRole> getFunctionalRoles();

}
