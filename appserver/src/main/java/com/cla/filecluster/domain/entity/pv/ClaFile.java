package com.cla.filecluster.domain.entity.pv;

import com.cla.common.domain.dto.extraction.SearchPatternCountDto;
import com.cla.common.domain.dto.file.ClaFileState;
import com.cla.common.domain.dto.file.DirtyState;
import com.cla.common.domain.dto.file.FileIngestionState;
import com.cla.connector.domain.dto.acl.AclInheritanceType;
import com.cla.connector.domain.dto.file.FileType;
import com.cla.connector.domain.dto.file.ItemType;
import com.cla.connector.domain.dto.media.MediaType;
import com.cla.connector.utils.FileNamingUtils;
import org.springframework.util.DigestUtils;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.*;
import java.util.regex.Pattern;

public class ClaFile implements Serializable {

    //public static final int DEFAULT_STR_FIELD_SIZE = 256;
    private static final Pattern WIN_FILE_SEPARATOR = Pattern.compile("\\\\");
    private static final String UNIX_FILE_SEPARATOR = "/";

    private Long id;    // Unique long ID

    private String fullPath;    // with delimiter string

    transient private String fileName;

    private String extension;

    private String fileNameHashed;

    private Long docFolderId;

    private Long rootFolderId;

    private Long packageTopFileId;

    private Integer type;

    transient private FileType fileType;

    private Date fsLastModified;

    private Date fsLastAccess;

    private Date creationDate;

    private String owner;

    private String mime;

    private boolean deleted;


    private Date initialScanDate;    // Time when was added to the system or un-deleted as it is found again as an active file

    private Date deletionDate;        // Latest time when record was marked as deleted

    private boolean contentDirty;

                  // length no longer than 2048, otherwise the index (rf_media_itm_idx) is not efficient
    private String mediaEntityId;    // optional unique id provided by media repository 

    private String aclSignature;

    private ClaFileState state = ClaFileState.INGESTED;

    private FileIngestionState ingestionState;

    private String analysisGroupId;

    private String userGroupId;

    private Long contentMetadataId;

    private Date createdOnDB;

    private Date updatedOnDB;

    private DirtyState dirty; // NOT IN SOLR

    private boolean dirtyWip;        // mark with True when dirty record is being processed (intermediate state) // NOT IN SOLR

    private String baseName;

    private Long sizeOnMedia;

    private AclInheritanceType aclInheritanceType;

    private Long lastMetadataChangeDate;

    private Set<SearchPatternCountDto> searchPatternsCounting = null;

    private String senderName;

    private String senderAddress;

    private String senderDomain;

    private List<String> recipientsNames;

    private List<String> recipientsAddresses;

    private List<String> recipientsDomains;

    private String senderFullAddress;

    private List<String> recipientsFullAddresses;

    private Date sentDate;

    private ItemType itemType;

    private Integer numOfAttachments;

    private List<String> containerIds;

    private String mailboxUpn;

    private String itemSubject;

    private MediaType mediaType;

    private List<String> associations;

    private String sortName;

    private Long ingestDate;

    private Long analysisDate;

    private List<Long> daLabels;

    private List<String> externalMetadata;

    private List<String> generalMetadata;

    private List<String> actionsExpected;

    private List<String> actionConfirmation;

    private String rawMetadata;

    private List<String> daMetadata;

    private List<String> daMetadataType;

    private Long labelProcessDate;

    public ClaFile(final String fileName, final FileType type) {
        setFileName(fileName);
        this.fileType = type;
        this.type = type.toInt();

    }

    public Date getCreatedOnDB() {
        return createdOnDB;
    }

    public Date getUpdatedOnDB() {
        return updatedOnDB;
    }

    public void setCreatedOnDB(Date createdOnDB) {
        this.createdOnDB = createdOnDB;
    }

    public void setUpdatedOnDB(Date updatedOnDB) {
        this.updatedOnDB = updatedOnDB;
    }

    public String getBaseName() {
        return baseName;
    }

    public void setBaseName(final String str) {
        this.baseName = FileNamingUtils.trimToSize(str);
    }

    void populateTransientFields() {
        fileType = FileType.valueOf(type);
        fileName = fullPath + getBaseName();
    }

    public ClaFile() {
    }

    public void recalculateHash() {
        populateTransientFields();
        fileNameHashed = getHashedFileName(fileName);
    }


    public void setFullPathProperties(final String name) {
        setBaseName(FileNamingUtils.getFilenameBaseName(name));
        setExtension(FileNamingUtils.calcExtension(name));
        this.fullPath = FileNamingUtils.getFileNameFullPath(name, getBaseName());
    }

    public void setType(final FileType type) {
        this.fileType = type;
        this.type = type.toInt();
    }

    public void setFileName(final String fileName) {
        this.fileName = fileName;
        fileNameHashed = getHashedFileName(fileName);
    }

    public String getMediaEntityId() {
        return mediaEntityId;
    }

    public void setMediaEntityId(String mediaEntityId) {
        this.mediaEntityId = mediaEntityId;
    }

    public String getFileNameHashed() {
        return fileNameHashed;
    }

    public void setFileNameHashed(String fileNameHashed) {
        this.fileNameHashed = fileNameHashed;
    }

    public static String getHashedFileName(final String fileName) {
        try {
            return DigestUtils.md5DigestAsHex(fileName.getBytes("UTF-8"));
        } catch (final UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }


    public String getFileName() {
        return fileName;
    }

    public String getFullPath() {
        return fullPath;
    }

    public void setFullPath(String fullPath) {
        this.fullPath = fullPath;
    }

    public FileType getType() {
        return fileType;
    }

    public int getTypeOrdinal() {
        return type;
    }


    public ClaFileState getState() {
        return state;
    }

    public void setState(final ClaFileState state) {
        this.state = state;
    }

    public FileIngestionState getIngestionState() {
        return ingestionState;
    }

    public void setIngestionState(FileIngestionState ingestionState) {
        this.ingestionState = ingestionState;
    }

    public Long getId() {
        return id;
    }

    public String getBaseNameNoExtention() {
        String fn = getBaseName();
        final String ext = getExtension();
        if (fn != null && ext != null) {
            fn = fn.substring(0, Integer.max(0, fn.length() - ext.length() - 1));
        }
        return fn;
    }

    public String getMime() {
        return mime;
    }

    public void setMime(final String mime) {
        this.mime = mime;
    }


    public void setId(final Long id) {
        this.id = id;
    }


    public void overrideHashValue(final String stringToHash) {
        this.fileNameHashed = getHashedFileName(stringToHash);
    }

    public static String fileName2Basename(final String str) {
        final int lastInx = WIN_FILE_SEPARATOR.matcher(str).replaceAll(UNIX_FILE_SEPARATOR).lastIndexOf(UNIX_FILE_SEPARATOR);
        return (lastInx < 0) ? str : str.substring(lastInx + 1);
    }

    public static String fileName2Path(final String str) {
        final int lastInx = WIN_FILE_SEPARATOR.matcher(str).replaceAll(UNIX_FILE_SEPARATOR).lastIndexOf(UNIX_FILE_SEPARATOR);
        return (lastInx < 0) ? "" : str.substring(0, lastInx + 1);
    }

    public Long getDocFolderId() {
        return docFolderId;
    }

    public void setDocFolderId(Long docFolderId) {
        this.docFolderId = docFolderId;
    }

    public DirtyState getDirty() {
        return dirty;
    }

    public void setDirty(DirtyState dirty) {
        this.dirty = dirty;
        this.dirtyWip = false;
    }

    public boolean isDirtyWip() {
        return dirtyWip;
    }

    public void setDirtyWip(boolean value) {
        this.dirtyWip = value;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        if (id != null) {
            result = prime * result + id.hashCode();
        } else {
            result = prime * result + ((fileNameHashed == null) ? 0 : fileNameHashed.hashCode());
            result = prime * result + ((fullPath == null) ? 0 : fullPath.hashCode());
        }
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ClaFile other = (ClaFile) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else
            return id.equals(other.id);

        if (fileNameHashed == null) {
            if (other.fileNameHashed != null)
                return false;
        } else if (!fileNameHashed.equals(other.fileNameHashed))
            return false;
        if (fullPath == null) {
            if (other.fullPath != null)
                return false;
        } else if (!fullPath.equals(other.fullPath))
            return false;

        return true;
    }

    public Long getContentMetadataId() {
        return contentMetadataId;
    }

    public void setContentMetadataId(Long contentMetadataId) {
        this.contentMetadataId = contentMetadataId;
    }

    public Long getRootFolderId() {
        return rootFolderId;
    }

    public void setRootFolderId(Long rootFolderId) {
        this.rootFolderId = rootFolderId;
    }

    public Long getPackageTopFileId() {
        return packageTopFileId;
    }

    public void setPackageTopFileId(Long packageTopFileId) {
        this.packageTopFileId = packageTopFileId;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(final String str) {
        this.extension = str;
    }

    public Date getFsLastModified() {
        return fsLastModified;
    }

    public void setFsLastModified(Date fsLastModified) {
        this.fsLastModified = fsLastModified;
    }

    public Date getFsLastAccess() {
        return fsLastAccess;
    }

    public void setFsLastAccess(Date fsLastAccess) {
        this.fsLastAccess = fsLastAccess;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Long getSizeOnMedia() {
        return sizeOnMedia;
    }

    public void setSizeOnMedia(Long sizeOnMedia) {
        this.sizeOnMedia = sizeOnMedia;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(final Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getInitialScanDate() {
        return initialScanDate;
    }

    public void setInitialScanDate(Date initialScanDate) {
        this.initialScanDate = initialScanDate;
    }

    public Date getDeletionDate() {
        return deletionDate;
    }

    public void setDeletionDate(Date deletionDate) {
        this.deletionDate = deletionDate;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(final String str) {
        this.owner = FileNamingUtils.trimToSize(str);
    }

    public String getAclSignature() {
        return aclSignature;
    }

    public void setAclSignature(String aclSignature) {
        this.aclSignature = aclSignature;
    }

    public boolean isContentDirty() {
        return contentDirty;
    }

    public void setContentDirty(boolean contentDirty) {
        this.contentDirty = contentDirty;
    }

    public String getAnalysisGroupId() {
        return analysisGroupId;
    }

    public void setAnalysisGroupId(String analysisGroupId) {
        this.analysisGroupId = analysisGroupId;
    }

    public String getUserGroupId() {
        return userGroupId;
    }

    public void setUserGroupId(String userGroupId) {
        this.userGroupId = userGroupId;
    }

    public boolean isInheritAcls() {
        return AclInheritanceType.FOLDER.equals(aclInheritanceType);
    }

    public AclInheritanceType getAclInheritanceType() {
        return aclInheritanceType;
    }

    public void setAclInheritanceType(AclInheritanceType aclInheritanceType) {
        this.aclInheritanceType = aclInheritanceType;
    }

    public Set<SearchPatternCountDto> getSearchPatternsCounting() {
        return searchPatternsCounting;
    }

    public void setSearchPatternsCounting(Set<SearchPatternCountDto> searchPatternsCounting) {
        this.searchPatternsCounting = searchPatternsCounting;
    }

    public Long getLastMetadataChangeDate() {
        return lastMetadataChangeDate;
    }

    public void setLastMetadataChangeDate(Long lastMetadataChangeDate) {
        this.lastMetadataChangeDate = lastMetadataChangeDate;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getSenderAddress() {
        return senderAddress;
    }

    public void setSenderAddress(String senderAddress) {
        this.senderAddress = senderAddress;
    }

    public String getSenderDomain() {
        return senderDomain;
    }

    public void setSenderDomain(String senderDomain) {
        this.senderDomain = senderDomain;
    }

    public List<String> getRecipientsNames() {
        return recipientsNames;
    }

    public void setRecipientsNames(List<String> recipientsNames) {
        this.recipientsNames = recipientsNames;
    }

    public List<String> getRecipientsAddresses() {
        return recipientsAddresses;
    }

    public void setRecipientsAddresses(List<String> recipientsAddresses) {
        this.recipientsAddresses = recipientsAddresses;
    }

    public List<String> getRecipientsDomains() {
        return recipientsDomains;
    }

    public void setRecipientsDomains(List<String> recipientsDomains) {
        this.recipientsDomains = recipientsDomains;
    }

    public String getSenderFullAddress() {
        return senderFullAddress;
    }

    public void setSenderFullAddress(String senderFullAddress) {
        this.senderFullAddress = senderFullAddress;
    }

    public List<String> getRecipientsFullAddresses() {
        return recipientsFullAddresses;
    }

    public void setRecipientsFullAddresses(List<String> recipientsFullAddresses) {
        this.recipientsFullAddresses = recipientsFullAddresses;
    }

    public Date getSentDate() {
        return sentDate;
    }

    public void setSentDate(Date sentDate) {
        this.sentDate = sentDate;
    }

    public ItemType getItemType() {
        return itemType;
    }

    public void setItemType(ItemType itemType) {
        this.itemType = itemType;
    }

    public Integer getNumOfAttachments() {
        return numOfAttachments;
    }

    public void setNumOfAttachments(Integer numOfAttachments) {
        this.numOfAttachments = numOfAttachments;
    }

    public List<String> getContainerIds() {
        return containerIds;
    }

    public void setContainerIds(List<String> containerIds) {
        this.containerIds = containerIds;
    }

    public String getMailboxUpn() {
        return mailboxUpn;
    }

    public void setMailboxUpn(String mailboxUpn) {
        this.mailboxUpn = mailboxUpn;
    }

    public String getItemSubject() {
        return itemSubject;
    }

    public void setItemSubject(String itemSubject) {
        this.itemSubject = itemSubject;
    }

    public MediaType getMediaType() {
        return mediaType;
    }

    public void setMediaType(MediaType mediaType) {
        this.mediaType = mediaType;
    }

    public List<String> getAssociations() {
        return associations;
    }

    public void setAssociations(List<String> associations) {
        this.associations = associations;
    }

    public String getSortName() {
        return sortName;
    }

    public void setSortName(String sortName) {
        this.sortName = sortName;
    }

    public Long getIngestDate() {
        return ingestDate;
    }

    public void setIngestDate(Long ingestDate) {
        this.ingestDate = ingestDate;
    }

    public Long getAnalysisDate() {
        return analysisDate;
    }

    public void setAnalysisDate(Long analysisDate) {
        this.analysisDate = analysisDate;
    }

    public List<Long> getDaLabels() {
        return daLabels;
    }

    public void setDaLabels(List<Long> daLabels) {
        this.daLabels = daLabels;
    }

    public List<String> getExternalMetadata() {
        return externalMetadata;
    }

    public void setExternalMetadata(List<String> externalMetadata) {
        this.externalMetadata = externalMetadata;
    }

    public List<String> getActionsExpected() {
        return actionsExpected;
    }

    public void setActionsExpected(List<String> actionsExpected) {
        this.actionsExpected = actionsExpected;
    }

    public String getRawMetadata() {
        return rawMetadata;
    }

    public void setRawMetadata(String rawMetadata) {
        this.rawMetadata = rawMetadata;
    }

    public List<String> getGeneralMetadata() {
        return generalMetadata;
    }

    public void setGeneralMetadata(List<String> generalMetadata) {
        this.generalMetadata = generalMetadata;
    }

    public List<String> getActionConfirmation() {
        return actionConfirmation;
    }

    public void setActionConfirmation(List<String> actionConfirmation) {
        this.actionConfirmation = actionConfirmation;
    }

    public Long getLabelProcessDate() {
        return labelProcessDate;
    }

    public void setLabelProcessDate(Long labelProcessDate) {
        this.labelProcessDate = labelProcessDate;
    }

    public List<String> getDaMetadata() {
        return daMetadata;
    }

    public void setDaMetadata(List<String> daMetadata) {
        this.daMetadata = daMetadata;
    }

    public List<String> getDaMetadataType() {
        return daMetadataType;
    }

    public void setDaMetadataType(List<String> daMetadataType) {
        this.daMetadataType = daMetadataType;
    }

    @Override
    public String toString() {
        String claFileStr = "ClaFile{" +
                "ID=" + id +
                ", fullPath='" + fullPath + '\'' +
                ", baseName='" + baseName + '\'';

        if (mediaEntityId != null) {
            claFileStr += ", mediaId='" + mediaEntityId + '\'';
        }
        claFileStr += ", size: " + sizeOnMedia;
        claFileStr += ", type: " + type;
        claFileStr += ", state: " + state;
        claFileStr += ", dirty:" + dirty + '}';
        return claFileStr;
    }
}
