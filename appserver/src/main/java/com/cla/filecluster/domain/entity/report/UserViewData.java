package com.cla.filecluster.domain.entity.report;

import com.cla.common.domain.dto.report.DisplayType;
import com.cla.filecluster.domain.entity.filter.SavedFilter;
import com.cla.filecluster.domain.entity.scope.BasicScope;
import com.cla.filecluster.domain.entity.security.User;
import com.drew.lang.annotations.Nullable;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;

/**
 * Display data per user used by ui
 *
 * Created by: yael
 * Created on: 3/8/2018
 */
@Entity
@Table(name = "user_view_data")
public class UserViewData {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotEmpty
    private String name;

    @Column(name = "display_data")
    @Type(type="text")
    private String displayData;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "scope_id")
    private BasicScope scope;

    @ManyToOne
    @JoinColumn(name = "filter_id")
    private SavedFilter filter;

    @Enumerated(EnumType.STRING)
    private DisplayType displayType;

    @Nullable
    @ManyToOne(optional = true,fetch = FetchType.EAGER)
    private User owner;

    @JoinColumn(name = "global_filter")
    private Boolean globalFilter;

    @ManyToOne
    @JoinColumn(name = "container_id")
    private UserViewData container;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDisplayData() {
        return displayData;
    }

    public void setDisplayData(String displayData) {
        this.displayData = displayData;
    }

    public BasicScope getScope() {
        return scope;
    }

    public void setScope(BasicScope scope) {
        this.scope = scope;
    }

    public SavedFilter getFilter() {
        return filter;
    }

    public void setFilter(SavedFilter filter) {
        this.filter = filter;
    }

    public DisplayType getDisplayType() {
        return displayType;
    }

    public void setDisplayType(DisplayType displayType) {
        this.displayType = displayType;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public Boolean isGlobalFilter() { return globalFilter; }

    public void setGlobalFilter(Boolean globalFilter) { this.globalFilter = globalFilter; }

    public UserViewData getContainer() {
        return container;
    }

    public void setContainer(UserViewData container) {
        this.container = container;
    }
}
