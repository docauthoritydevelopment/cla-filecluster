package com.cla.filecluster.domain.dto;

import org.apache.solr.client.solrj.beans.Field;

import java.util.UUID;

public class FileContent {
	@Field
	private final String id = UUID.randomUUID().toString();
	
	@Field
	private long claFileId;

	@Field
	private String content;
	
	@Field("time_to_live")
	private String timeToLive;

	public FileContent() {
	}

	public FileContent(final long claFileId, final String content) {
		this.claFileId = claFileId;
		this.content = content;
	}

	public long getClaFileId() {
		return claFileId;
	}

	public void setClaFileId(final long claFileId) {
		this.claFileId = claFileId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(final String content) {
		this.content = content;
	}

	public String getId() {
		return id;
	}

	public String getTimeToLive() {
		return timeToLive;
	}

	public void setTimeToLive(final String timeToLive) {
		this.timeToLive = timeToLive;
	}

	
	
	
	

	
	
	
	

}
