package com.cla.filecluster.domain.specification.solr;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.domain.dto.bizlist.BizListItemType;
import com.cla.common.domain.dto.kendo.FilterDescriptor;
import com.cla.filecluster.repository.solr.FacetType;
import com.cla.filecluster.repository.solr.SolrFacetQueryJson;
import com.cla.filecluster.repository.solr.query.SolrQueryGenerator;
import org.apache.solr.client.solrj.SolrQuery;
import org.springframework.data.domain.PageRequest;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 *
 * Created by shtand on 09/09/2015.
 */
public class SolrSpecification {

    private FilterDescriptor filterDescriptor;

    private List<String> additionalFilter = new ArrayList<>();

    private Optional<Integer> minCount = Optional.empty();

    protected CatFileFieldType facetRangeField;
    protected LocalDateTime facetDateStart;
    protected LocalDateTime facetDateEnd;
    protected String facetRangeGap;

    private Collection<String> facetQueryValues;

    private String facetPrefix;

    private List<String> contentFilters = new ArrayList<>();

    private String tagTypeMismatchFilter;

    private FilterDescriptor groupJoinFilter;

    private String groupByField;

    private boolean groupMain;

    private String id;          // @CR: changed to 'private'. What are these fields used for?
    private long fileID;
    private int highlightItemFullLength;
    private int highlightPageSize;

    private List<SolrFacetQueryJson> facetJsonObjectList =  new ArrayList<>();

//    private boolean proximitySearchOnContent;

    private String sortField;
    private SolrQuery.ORDER sortOrder = SolrQuery.ORDER.asc;
    private PageRequest pageRequest;
    private String selectedItemId;

    protected SolrSpecification(FilterDescriptor filterDescriptor) {
        this.filterDescriptor = filterDescriptor;
    }

    public void addToContentFilter(String s) {
        contentFilters.add(s);
    }

    public String[] getFacetFields() {
        return null;
    }

    public String getBaseQuery() {
        return SolrQueryGenerator.ALL_QUERY;
    }

    public void setMinCount(int minCount) {
        this.minCount = Optional.of(minCount);
    }

    public Optional<Integer> getMinCount() {
        return minCount;
    }

    public void setAdditionalFilter(String additionalFilter) {
        this.additionalFilter.add(additionalFilter);
    }

    public void setAdditionalFilter(List<String> additionalFilter) {
        this.additionalFilter = additionalFilter;
    }

    public List<String> getContentFilters() {
        return contentFilters;
    }

    public void setContentFilters( List<String> contentFilters) {
        this.contentFilters = contentFilters;
    }

    public void addContentFilters( List<String> contentFilters) {
        this.contentFilters.addAll(contentFilters);
    }

    public CatFileFieldType getFacetRangeField() {
        return facetRangeField;
    }

    public void setFacetRangeField(CatFileFieldType facetRangeField) {
        this.facetRangeField = facetRangeField;
    }

    public LocalDateTime getFacetDateStart() {
        return facetDateStart;
    }

    public void setFacetDateStart(LocalDateTime facetDateStart) {
        this.facetDateStart = facetDateStart;
    }

    public LocalDateTime getFacetDateEnd() {
        return facetDateEnd;
    }

    public void setFacetDateEnd(LocalDateTime facetDateEnd) {
        this.facetDateEnd = facetDateEnd;
    }

    public String getFacetPrefix() {
        return facetPrefix;
    }

    public void setFacetPrefix(String facetPrefix) {
        this.facetPrefix = facetPrefix;
    }

    public String getFacetRangeGap() {
        return facetRangeGap;
    }

    public void setFacetRangeGap(String facetRangeGap) {
        this.facetRangeGap = facetRangeGap;
    }

    public void addFacetJsonObject(SolrFacetQueryJson newFacetJson)   {
        this.facetJsonObjectList.add(newFacetJson);
    }

    public List<SolrFacetQueryJson> getFacetJsonObjectList(){
        return this.facetJsonObjectList;
    }

    public void setFacetJsonObjectList(List<SolrFacetQueryJson> facetJsonObjectList) {
        this.facetJsonObjectList = facetJsonObjectList;
    }

    public boolean isUsingJsonFacet() {   //this function return true if we are using the object json facet (not the string json facet )
        return (this.facetJsonObjectList != null  && facetJsonObjectList.size()>0);
    }

    public String buildJsonFacet() {
        if (isUsingJsonFacet()) {
            return facetJsonObjectList.stream().map(SolrFacetQueryJson::toQueryString)
                    .collect(Collectors.joining(", ","{", "}"));
        }
        else {
            return "";
        }
    }

    public Collection<String> getFacetQueryValues() {
        return facetQueryValues;
    }

    public void setFacetQueryValues(Collection<String> facetQueryValues) {
        this.facetQueryValues = facetQueryValues;
    }

    public String getTagTypeMismatchFilter() {
        return tagTypeMismatchFilter;
    }

    public void setTagTypeMismatchFilter(String tagTypeMismatchFilter) {
        this.tagTypeMismatchFilter = tagTypeMismatchFilter;
    }

    public String getSortField() {
        return sortField;
    }

    public void setSortField(String sortField) {
        this.sortField = sortField;
    }

    public SolrQuery.ORDER getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(SolrQuery.ORDER sortOrder) {
        this.sortOrder = sortOrder;
    }

    public FilterDescriptor getGroupJoinFilter() {
        return groupJoinFilter;
    }

    public void setGroupJoinFilter(FilterDescriptor groupJoinFilter) {
        this.groupJoinFilter = groupJoinFilter;
    }

    public String getGroupByField() {
        return groupByField;
    }


    public void setGroupByField(String groupByField) {
        this.groupByField = groupByField;
    }

    public boolean isGroupMain() {
        return groupMain;
    }

    public void setGroupMain(boolean groupMain) {
        this.groupMain = groupMain;
    }

    public void filterByBizListType(BizListItemType type) {
        setFacetPrefix("bli."+type.ordinal()+".");
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getFileID() {
        return fileID;
    }

    public void setFileID(long fileID) {
        this.fileID = fileID;
    }

    public int getHighlightItemFullLength() {
        return highlightItemFullLength;
    }

    public void setHighlightItemFullLength(int highlightItemFullLength) {
        this.highlightItemFullLength = highlightItemFullLength;
    }

    public int getHighlightPageSize() {
        return highlightPageSize;
    }

    public void setHighlightPageSize(int highlightPageSize) {
        this.highlightPageSize = highlightPageSize;
    }

    public FilterDescriptor getFilterDescriptor() {
        return filterDescriptor;
    }

    public void setFilterDescriptor(FilterDescriptor filterDescriptor) {
        this.filterDescriptor = filterDescriptor;
    }

    public List<String> getAdditionalFilter() {
        return additionalFilter;
    }

    public void setMinCount(Optional<Integer> minCount) {
        this.minCount = minCount;
    }

    public boolean isTermJsonFacet(){
        return (this.getFacetJsonObjectList().size() == 1 && (this.getFacetJsonObjectList().get(0).getType() == FacetType.TERMS));
    }

    public SolrFacetQueryJson getTermJsonFacet(){
        if (isTermJsonFacet()) {
            return this.getFacetJsonObjectList().get(0);
        }
        throw new RuntimeException("No valid term json facet found");
    }

    @Override
    public String toString() {
        return "SolrSpecification{" +
                "filterDescriptor=" + filterDescriptor +
                ", jsonFacet='" + buildJsonFacet()+ '\'' +
                ", additionalFilter='" + additionalFilter + '\'' +
                ", facetRangeField=" + facetRangeField +
                ", facetPrefix='" + facetPrefix + '\'' +
                ", contentFilters=" + contentFilters +
                ", groupByField='" + groupByField + '\'' +
                ", sortField='" + sortField + '\'' +
                '}';
    }

    public PageRequest getPageRequest() {
        return pageRequest;
    }

    public void setPageRequest(PageRequest pageRequest) {
        this.pageRequest = pageRequest;
    }

    public String getSelectedItemId() {
        return selectedItemId;
    }

    public void setSelectedItemId(String selectedItemId) {
        this.selectedItemId = selectedItemId;
    }
}
