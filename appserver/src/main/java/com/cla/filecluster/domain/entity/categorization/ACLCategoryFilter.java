package com.cla.filecluster.domain.entity.categorization;

import java.util.List;

import javax.persistence.DiscriminatorValue;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("ACLCF")
public class ACLCategoryFilter extends CategoryFilter {

	@ElementCollection
	private List<String> acl;

}
