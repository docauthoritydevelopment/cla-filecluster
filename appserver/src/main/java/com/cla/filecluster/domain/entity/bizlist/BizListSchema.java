package com.cla.filecluster.domain.entity.bizlist;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by uri on 23/08/2015.
 */
public class BizListSchema {
    private String[] headers;

    private int clearIdHeaderIndex;
    private int clearNameHeaderIndex;

    private boolean nameHeaderInUse;

    private List<BizListItemRow> bizListItemRows = new ArrayList<>();
    private List<BizListItemRow> errorBizListItemRows = new ArrayList<>();
    private List<BizListItemRow> duplicateBizListItemRows = new ArrayList<>();

    private int aliasesHeaderIndex = -1;

    private List<Integer> singleAliasHeadersIndexes = null;

    public List<Integer> getSingleAliasHeadersIndexes() {
        return singleAliasHeadersIndexes;
    }

    public void addSingleAliasHeadersIndex(int inx) {
        if (singleAliasHeadersIndexes==null) {
            singleAliasHeadersIndexes = new ArrayList<>();
        }
        singleAliasHeadersIndexes.add(Integer.valueOf(inx));
    }

    public boolean isSingleAliasHeadersIndex(int inx) {
        return (singleAliasHeadersIndexes != null && singleAliasHeadersIndexes.contains(Integer.valueOf(inx)));
    }

    public void setSingleAliasHeadersIndexes(List<Integer> singleAliasHeadersIndexes) {
        this.singleAliasHeadersIndexes = singleAliasHeadersIndexes;
    }

    public void setHeaders(String[] headers) {
        this.headers = headers;
    }

    public void setClearIdHeaderIndex(int clearIdHeaderIndex) {
        this.clearIdHeaderIndex = clearIdHeaderIndex;
    }

    public void setClearNameHeaderIndex(int clearNameHeaderIndex) {
        nameHeaderInUse = true;
        this.clearNameHeaderIndex = clearNameHeaderIndex;
    }

    public boolean isNameHeaderInUse() {
        return nameHeaderInUse;
    }

    public int getClearNameHeaderIndex() {
        return clearNameHeaderIndex;
    }

    public int getClearIdHeaderIndex() {
        return clearIdHeaderIndex;
    }

    public List<BizListItemRow> getBizListItemRows() {
        return bizListItemRows;
    }

    public void addEntityListItem(BizListItemRow bizListItemRow) {
        bizListItemRows.add(bizListItemRow);
    }

    public String[] getHeaders() {
        return headers;
    }

    public void setAliasesHeaderIndex(int aliasesHeaderIndex) {
        this.aliasesHeaderIndex = aliasesHeaderIndex;
    }

    public int getAliasesHeaderIndex() {
        return aliasesHeaderIndex;
    }

    public List<BizListItemRow> getErrorBizListItemRows() {
        return errorBizListItemRows;
    }

    public List<BizListItemRow> getDuplicateBizListItemRows() {
        return duplicateBizListItemRows;
    }
}
