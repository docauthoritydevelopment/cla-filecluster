package com.cla.filecluster.domain.entity.filetag;

import com.cla.filecluster.domain.entity.bizlist.SimpleBizListItem;
import com.cla.filecluster.domain.entity.department.Department;
import com.cla.filecluster.domain.entity.doctype.DocType;
import com.cla.filecluster.domain.entity.scope.BasicScope;
import com.cla.filecluster.domain.entity.security.FunctionalRole;

/**
 * Created by uri on 31/01/2016.
 */
public interface EntityAssociation extends ScopedEntityAssociation {
    
    long getId();

    FileTag getFileTag();

    void setFileTag(FileTag fileTag);

    long getAssociationTimeStamp();

    void setAssociationTimeStamp(long associationTimeStamp);

    boolean isDeleted();

    SimpleBizListItem getSimpleBizListItem();

    void setSimpleBizListItem(SimpleBizListItem simpleBizListItem);

    FunctionalRole getFunctionalRole();

    void setFunctionalRole(FunctionalRole functionalRole);

    DocType getDocType();

    void setDocType(DocType docType);

    Department getDepartment();

    void setDepartment(Department department);

    BasicScope getScope();

    void setScope(BasicScope scope);

    boolean isImplicit();


}
