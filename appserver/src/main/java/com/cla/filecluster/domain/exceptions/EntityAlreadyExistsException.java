package com.cla.filecluster.domain.exceptions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by uri on 01/11/2015.
 */
@ResponseStatus(value= HttpStatus.CONFLICT, reason="Conflict")
public class EntityAlreadyExistsException extends RuntimeException {

    Logger logger = LoggerFactory.getLogger(EntityAlreadyExistsException.class);
    public EntityAlreadyExistsException(String entityType, String entityId) {
        super("Conflict - Entity by type ["+entityType+ "] already exists ["+entityId+"]");
        logger.warn(this.getMessage());
    }

}
