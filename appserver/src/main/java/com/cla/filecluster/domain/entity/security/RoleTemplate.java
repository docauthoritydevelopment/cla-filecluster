package com.cla.filecluster.domain.entity.security;


import com.cla.common.domain.dto.security.RoleTemplateType;

import com.cla.filecluster.domain.entity.BasicEntity;
import com.google.common.collect.Sets;
import org.hibernate.Hibernate;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Set;

/**
 * Role template holds the individual roles connected to a composite role
 * Created by: yael
 * Created on: 11/30/2017
 */
@Entity
@Table(name = "comp_role_templates",
        indexes = {
                @Index(unique = true, name = "name_idx", columnList = "name"),
                @Index(unique = true, name = "displayname_idx", columnList = "displayName")
        })
public class RoleTemplate extends BasicEntity {

    @NotNull
    private String name;

    @NotNull
    private String displayName;

    private String description;

    private boolean deleted;

    @Enumerated
    @NotNull
    private RoleTemplateType templateType;

    @ManyToMany
    @JoinTable(name = "template_roles",
            joinColumns = @JoinColumn(name = "template_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Role> roles = Sets.newHashSet();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public RoleTemplateType getTemplateType() {
        return templateType;
    }

    public void setTemplateType(RoleTemplateType templateType) {
        this.templateType = templateType;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    @Override
    public String toString() {
        return "RoleTemplate{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", displayName='" + displayName + '\'' +
                ", description='" + description + '\'' +
                ", deleted=" + isDeleted() +
                ", templateType=" + templateType +
                ", roles=" + roles +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) {
            return false;
        }
        if (getClass() != o.getClass()) {
            if (Hibernate.getClass(this) != Hibernate.getClass(o)) {
                return false;
            }
        }

        RoleTemplate that = (RoleTemplate) o;

        return id.equals(that.id) && name.equals(that.name);
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + name.hashCode();
        result = 31 * result + getClass().hashCode();
        return result;
    }
}
