package com.cla.filecluster.domain.converters;

import com.cla.common.domain.dto.file.FolderType;
import com.cla.common.domain.dto.filetree.PathDescriptorType;
import com.cla.connector.utils.FileNamingUtils;
import com.cla.filecluster.domain.dto.SolrFolderEntity;
import com.cla.filecluster.repository.solr.query.AtomicUpdateBuilder;
import com.cla.filecluster.service.files.filetree.DocFolderUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.apache.solr.common.SolrInputDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static com.cla.common.constants.CatFoldersFieldType.*;
import static com.cla.filecluster.repository.solr.query.SolrOperator.SET;

public class SolrFolderBuilder extends SolrEntityBuilder<SolrFolderEntity> {

    private final static Logger logger = LoggerFactory.getLogger(SolrFolderBuilder.class);

    private Long id;
    private Long rootFolderId;
    private Long parentFolderId;
    private Integer rootDepth;
    private String path;
    private List<String> parentInfo;
    private PathDescriptorType pathDescriptorType;
    private boolean deleted = false;
    private String mediaEntityId;
    private String aclSignature;
    private FolderType type;
    private Collection<String> allowedReadPrincipalsNames = Sets.newHashSet();
    private Collection<String> deniedReadPrincipalsNames = Sets.newHashSet();
    private Collection<String> allowedWritePrincipalsNames = Sets.newHashSet();
    private Collection<String> deniedWritePrincipalsNames = Sets.newHashSet();
    private String rootFolderPath;
    private String folderHash;
    private String syncState;
    private String mailboxUpn;
    private List<String> associations;
    private Long creationDate;

    public static SolrFolderBuilder create(){
        return new SolrFolderBuilder();
    }

    @Override
    public SolrFolderEntity build() {
        int depthFromRoot = 0;
        int absoluteDepth = 0;
        String universalPath = null;
        String folderName = null;
        if (path != null){
            String depthPath = FileNamingUtils.convertPathToUniversalString(rootFolderPath + path);
            universalPath = FileNamingUtils.convertPathToUniversalString(path);
            folderName = DocFolderUtils.extractFolderName(universalPath);
            absoluteDepth = DocFolderUtils.calculateDepth(depthPath);
            depthFromRoot = absoluteDepth - rootDepth;
        }

        if (depthFromRoot < 0) {
            logger.error("Can't create a entity with depth < 0 ({}, universalPath: {})", depthFromRoot, universalPath);
            return null;
        }

        if (Strings.isNullOrEmpty(path) && depthFromRoot > 0){
            throw new IllegalArgumentException("Empty or null path");
        }

        String folderHashStr = folderHash;
        if (folderHashStr == null) {
            String pathString = path != null ? FileNamingUtils.convertPathToUniversalString(path) : "";
            folderHashStr = getHashedString(rootFolderId + ":" + pathString);
        }

        SolrFolderEntity entity = new SolrFolderEntity();

        entity.setId(id);
        entity.setType(type.ordinal());
        entity.setRootFolderId(rootFolderId);
        entity.setParentFolderId(parentFolderId);
        entity.setPathDescriptorType(pathDescriptorType.name());
        entity.setDepthFromRoot(depthFromRoot);
        entity.setAbsoluteDepth(absoluteDepth);
        entity.setRealPath(path);
        entity.setPath(universalPath);
        entity.setName(folderName);
        entity.setFolderHash(folderHashStr);
        entity.setDeleted(deleted);

        Optional.ofNullable(syncState).ifPresent(entity::setSyncState);
        Optional.ofNullable(mailboxUpn).ifPresent(entity::setMailboxUpn);
        Optional.ofNullable(associations).ifPresent(entity::setAssociations);

        Optional.ofNullable(creationDate).ifPresent(entity::setCreationDate);
        entity.setUpdateDate(System.currentTimeMillis());

        if (id != null) {
            List<String> parents = Lists.newArrayList();
            String currentInfoStr = depthFromRoot + "." + id;
            if (parentInfo != null && depthFromRoot > 0) {
                parents.addAll(parentInfo);
            }
            if (!parents.contains(currentInfoStr)) {
                parents.add(currentInfoStr);
            }
            entity.setParentsInfo(parents);
        }

        return entity;
    }

    @Override
    public void update(final SolrFolderEntity existing) {
        throw new RuntimeException("Not implemented");
    }

    @Override
    public SolrInputDocument buildAtomicUpdate() {
        if (id == null){
            throw new IllegalArgumentException("Must specify document ID");
        }
        AtomicUpdateBuilder builder = AtomicUpdateBuilder.create().setId(ID, id);

        if (path != null){
            String depthPath = FileNamingUtils.convertPathToUniversalString(rootFolderPath+path);
            String universalPath = FileNamingUtils.convertPathToUniversalString(path);
            String folderName = DocFolderUtils.extractFolderName(universalPath);
            int absoluteDepth = DocFolderUtils.calculateDepth(depthPath);

            builder.addModifier(PATH, SET, universalPath);
            builder.addModifier(REAL_PATH, SET, path);
            builder.addModifier(NAME, SET, folderName);
            builder.addModifier(ABSOLUTE_DEPTH, SET, absoluteDepth);

            if (rootDepth != null) {
                int depthFromRoot = absoluteDepth - rootDepth;
                if (depthFromRoot < 0) {
                    logger.error("Can't create a entity with depth < 0 ({}, universalPath: {})", depthFromRoot, universalPath);
                    return null;
                }
                builder.addModifier(DEPTH_FROM_ROOT, SET, depthFromRoot);
            }

            if (rootFolderId != null) {
                builder.addModifier(FOLDER_HASH, SET, getHashedString(rootFolderId + ":" + universalPath));
            }
        }

        if (pathDescriptorType != null){
            builder.addModifier(PATH_DESCRIPTOR_TYPE, SET, pathDescriptorType.name());
        }

        builder.addModifier(DELETED, SET, deleted);

        if (mediaEntityId != null) {
            builder.addModifier(MEDIA_ENTITY_ID, SET, mediaEntityId);
        }

        if (parentInfo != null) {
            builder.addModifier(PARENTS_INFO, SET, parentInfo);
        }

        if (aclSignature != null) {
            builder.addModifier(ACL_SIGNATURE, SET, aclSignature);
        }

        if (allowedReadPrincipalsNames != null && !allowedReadPrincipalsNames.isEmpty()) {
            builder.addModifier(ACL_READ, SET, allowedReadPrincipalsNames);
        }

        if (allowedWritePrincipalsNames != null &&!allowedWritePrincipalsNames.isEmpty()) {
            builder.addModifier(ACL_WRITE, SET, allowedWritePrincipalsNames);
        }

        if (deniedReadPrincipalsNames != null &&!deniedReadPrincipalsNames.isEmpty()) {
            builder.addModifier(ACL_DENIED_READ, SET, deniedReadPrincipalsNames);
        }

        if (deniedWritePrincipalsNames != null &&!deniedWritePrincipalsNames.isEmpty()) {
            builder.addModifier(ACL_DENIED_WRITE, SET, deniedWritePrincipalsNames);
        }

        if (parentFolderId != null) {
            builder.addModifier(PARENT_FOLDER_ID, SET, parentFolderId);
        }

        if (rootFolderId != null) {
            builder.addModifierNotNull(ROOT_FOLDER_ID, SET, rootFolderId);
        }

        if (type != null) {
            builder.addModifierNotNull(TYPE, SET, type.ordinal());
        }

        if (folderHash != null) {
            builder.addModifierNotNull(FOLDER_HASH, SET, folderHash);
        }

        if (syncState != null) {
            builder.addModifierNotNull(SYNC_STATE, SET, syncState);
        }

        if (mailboxUpn != null) {
            builder.addModifierNotNull(MAILBOX_UPN, SET, mailboxUpn);
        }

        if (creationDate != null) {
            builder.addModifierNotNull(CREATION_DATE, SET, creationDate);
        }

        builder.addModifier(UPDATE_DATE, SET, System.currentTimeMillis());

        builder.addModifierNotNull(FOLDER_ASSOCIATIONS, SET, associations);

        return builder.build();
    }

    public SolrFolderBuilder setId(Long id) {
        this.id = id;
        return this;
    }

    public SolrFolderBuilder setRootFolderId(Long rootFolderId) {
        this.rootFolderId = rootFolderId;
        return this;
    }

    public SolrFolderBuilder setPath(String path) {
        this.path = path;
        return this;
    }

    public SolrFolderBuilder setRootDepth(Integer rootDepth) {
        this.rootDepth = rootDepth < 0 ? 0 : rootDepth;
        return this;
    }

    public SolrFolderBuilder setPathDescriptorType(PathDescriptorType pathDescriptorType) {
        this.pathDescriptorType = pathDescriptorType;
        return this;
    }

    public SolrFolderBuilder setParentFolderId(Long parentFolderId) {
        this.parentFolderId = parentFolderId;
        return this;
    }

    public SolrFolderBuilder setParentInfo(List<String> parentInfo) {
        this.parentInfo = parentInfo;
        return this;
    }

    public SolrFolderBuilder setDeleted(boolean deleted) {
        this.deleted = deleted;
        return this;
    }

    public SolrFolderBuilder setMediaEntityId(String mediaEntityId) {
        this.mediaEntityId = mediaEntityId;
        return this;
    }

    public SolrFolderBuilder setAclSignature(String aclSignature) {
        this.aclSignature = aclSignature;
        return this;
    }

    public SolrFolderBuilder setAllowedReadPrincipalsNames(Collection<String> allowedReadPrincipalsNames) {
        this.allowedReadPrincipalsNames = allowedReadPrincipalsNames;
        return this;
    }

    public SolrFolderBuilder setDeniedReadPrincipalsNames(Collection<String> deniedReadPrincipalsNames) {
        this.deniedReadPrincipalsNames = deniedReadPrincipalsNames;
        return this;
    }

    public SolrFolderBuilder setAllowedWritePrincipalsNames(Collection<String> allowedWritePrincipalsNames) {
        this.allowedWritePrincipalsNames = allowedWritePrincipalsNames;
        return this;
    }

    public SolrFolderBuilder setDeniedWritePrincipalsNames(Collection<String> deniedWritePrincipalsNames) {
        this.deniedWritePrincipalsNames = deniedWritePrincipalsNames;
        return this;
    }

    public SolrFolderBuilder setType(FolderType type) {
        this.type = type;
        return this;
    }

    public SolrFolderBuilder setRootFolderPath(String rootFolderPath) {
        this.rootFolderPath = rootFolderPath;
        return this;
    }

    public SolrFolderBuilder setFolderHash(String folderHash) {
        this.folderHash = folderHash;
        return this;
    }

    public SolrFolderBuilder setSyncState(String syncState) {
        this.syncState = syncState;
        return this;
    }

    public SolrFolderBuilder setMailboxUpn(String mailboxUpn) {
        this.mailboxUpn = mailboxUpn;
        return this;
    }

    public SolrFolderBuilder setAssociations(List<String> associations) {
        this.associations = associations;
        return this;
    }

    public SolrFolderBuilder setCreationDate(Long creationDate) {
        this.creationDate = creationDate;
        return this;
    }
}
