package com.cla.filecluster.domain.dto;

import com.cla.common.domain.dto.crawler.RootFoldersAggregatedSummaryInfo;
import com.cla.common.domain.dto.filetree.RootFolderRunSummaryInfo;
import com.cla.common.domain.dto.filetree.RootFolderSummaryInfo;
import org.springframework.data.domain.Page;

/**
 * Created by: yael
 * Created on: 12/12/2018
 */
public class ActiveScanQueryResult {
    private Page<RootFolderRunSummaryInfo> rootFoldersData;
    private RootFoldersAggregatedSummaryInfo stateSummaryInfo;

    public ActiveScanQueryResult() { // for qa
    }

    public ActiveScanQueryResult(Page<RootFolderRunSummaryInfo> rootFoldersData, RootFoldersAggregatedSummaryInfo stateSummaryInfo) {
        this.rootFoldersData = rootFoldersData;
        this.stateSummaryInfo = stateSummaryInfo;
    }

    public Page<RootFolderRunSummaryInfo> getRootFoldersData() {
        return rootFoldersData;
    }

    public void setRootFoldersData(Page<RootFolderRunSummaryInfo> rootFoldersData) {
        this.rootFoldersData = rootFoldersData;
    }

    public RootFoldersAggregatedSummaryInfo getStateSummaryInfo() {
        return stateSummaryInfo;
    }

    public void setStateSummaryInfo(RootFoldersAggregatedSummaryInfo stateSummaryInfo) {
        this.stateSummaryInfo = stateSummaryInfo;
    }
}
