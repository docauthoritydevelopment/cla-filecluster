package com.cla.filecluster.domain.entity.filetag;

import com.cla.common.domain.dto.filetag.FileTagAction;
import com.cla.common.domain.dto.filetag.FileTagInterface;
import com.cla.filecluster.domain.entity.doctype.DocType;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Created by uri on 24/08/2015.
 */
@Entity
@Table(
		name = "file_tag"
		, uniqueConstraints = {
				@UniqueConstraint(columnNames = {"identifier"})
		}
		,indexes = {
				@Index(name = "name_idx", columnList = "name")
		}
	)
public class FileTag implements Serializable, AssociableEntity, FileTagInterface {

	// TODO: missing @Filter definition to utilise the account field

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Access(AccessType.PROPERTY)
    @Id
    private long id;

    /**
     * For user generated FileTags identfier is just the id.
     * For automatically generated FileTags identfier is the name of the FileTag when it was created.
     * This is used to allow us to reuse the automatically generated fileTags by finding them even if the name was changed.
     */
    @NotNull
    private String identifier;

    private String name;

    private String alternativeName;

    private String description;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "type_id")
    private FileTagType type;

    private long ownerId;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "doctype_id")
    private DocType docTypeContext;

    private String entityIdContext;

    private FileTagAction action;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "parent_tag_id")
    private FileTag parentFileTag;

    private boolean deleted;

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getTypeId() {
        return type.getId();
    }

    public FileTagType getType() {
        return type;
    }

    public void setType(FileTagType type) {
        this.type = type;
    }

    public long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(long ownerId) {
        this.ownerId = ownerId;
    }

    public DocType getDocTypeContext() {
        return docTypeContext;
    }

    public void setDocTypeContext(DocType docTypeContext) {
        this.docTypeContext = docTypeContext;
    }

    public String getEntityIdContext() {
        return entityIdContext;
    }

    public void setEntityIdContext(String entityIdContext) {
        this.entityIdContext = entityIdContext;
    }

    public FileTagAction getAction() {
        return action;
    }

    public void setAction(FileTagAction action) {
        this.action = action;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FileTag fileTag = (FileTag) o;
        return !(identifier != null ? !identifier.equals(fileTag.identifier) : fileTag.identifier != null);

    }

    @Override
    public int hashCode() {
        return identifier != null ? identifier.hashCode() : 0;
    }

    public FileTag getParentFileTag() {
        return parentFileTag;
    }

    public void setParentFileTag(FileTag parentFileTag) {
        this.parentFileTag = parentFileTag;
    }

    @Override
    public String toString() {
        return "FileTag{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", type=" + type +
                '}';
    }

    public String getAlternativeName() {
        return alternativeName;
    }

    public void setAlternativeName(String alternativeName) {
        this.alternativeName = alternativeName;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public Long getDocTypeContextId() {
        return docTypeContext == null ? null : docTypeContext.getId();
    }

    @Override
    public boolean isTypeHidden() {
        return type.isHidden();
    }

    @Override
    public String createIdentifierString() {
        return createFileTagIdentifierString();
    }

    @Override
    public boolean isSingleValueTagType() {
        return type.isSingleValueTag();
    }
}
