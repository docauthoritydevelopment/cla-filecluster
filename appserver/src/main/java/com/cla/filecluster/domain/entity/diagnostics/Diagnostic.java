package com.cla.filecluster.domain.entity.diagnostics;

import com.cla.filecluster.service.diagnostics.DiagnosticType;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Map;

/**
 * Created by: yael
 * Created on: 5/2/2019
 */
@Entity
public class Diagnostic {

    @Id
    @Access(AccessType.PROPERTY)
    @GeneratedValue
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false, name = "diagnostic_type")
    private DiagnosticType diagnosticType;

    @Type(type = "json")
    @Column(columnDefinition = "json", name = "data")
    private Map<String,String> opData;

    @Column(name = "create_time")
    private Long createTime;

    @Column(name = "start_time")
    private Long startTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public DiagnosticType getDiagnosticType() {
        return diagnosticType;
    }

    public void setDiagnosticType(DiagnosticType diagnosticType) {
        this.diagnosticType = diagnosticType;
    }

    public Map<String, String> getOpData() {
        return opData;
    }

    public void setOpData(Map<String, String> opData) {
        this.opData = opData;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    @PrePersist
    private void setCreatedOnDB() {
        this.createTime = System.currentTimeMillis();
    }

    @Override
    public String toString() {
        return "Diagnostic{" +
                "id=" + id +
                ", diagnosticType=" + diagnosticType +
                ", opData=" + opData +
                ", createTime=" + createTime +
                ", startTime=" + startTime +
                '}';
    }
}
