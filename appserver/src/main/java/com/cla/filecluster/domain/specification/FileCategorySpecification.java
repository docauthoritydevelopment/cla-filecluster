package com.cla.filecluster.domain.specification;

import com.cla.common.domain.dto.category.FileCategoryScope;
import com.cla.common.domain.dto.category.FileCategoryState;
import com.cla.common.domain.dto.kendo.FilterDescriptor;
import com.cla.filecluster.domain.entity.categorization.FileCategory;
import org.apache.commons.collections.CollectionUtils;

import javax.persistence.criteria.*;

/**
 * Created by uri on 09/08/2015.
 */
public class FileCategorySpecification extends KendoFilterSpecification<FileCategory> {

    private final FileCategoryScope fileCategoryScope;

    public FileCategorySpecification(FilterDescriptor filter, FileCategoryScope fileCategoryScope) {
        super(FileCategory.class, filter);
        this.fileCategoryScope = fileCategoryScope;
    }

    @Override
    protected boolean isDateField(String dtoField) {
        return false;
    }

    @Override
    protected String convertModelField(String dtoField) {
        return dtoField;
    }

    @Override
    public Predicate toPredicate(Root<FileCategory> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
        //This part allow to use this specification in pageable queries
        Path<Object> scope = root.get("scope");
        Path<Object> state = root.get("state");
        Predicate fileCategoryScopePredicate = cb.equal(scope, fileCategoryScope);
        Predicate ignoreMerged = cb.notEqual(state, FileCategoryState.MERGED);
        Predicate basePredicate = cb.and(fileCategoryScopePredicate,ignoreMerged);
        if (filter == null || CollectionUtils.isEmpty(filter.getFilters())) {
            //Return an empty or (bogus filter)
            return basePredicate;
        }
        Predicate filterPredicate = buildFilter(this.filter, root, query, cb);
        return cb.and(basePredicate,filterPredicate);

    }
}
