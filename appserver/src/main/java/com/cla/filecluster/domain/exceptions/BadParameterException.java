package com.cla.filecluster.domain.exceptions;

import com.cla.connector.domain.dto.error.BadRequestType;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by: yael
 * Created on: 1/3/2019
 */
@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class BadParameterException extends RuntimeException {

    private BadRequestType type;

    public BadParameterException(String reason, BadRequestType type) {
        super(reason);
        this.type = type;
    }

    public BadRequestType getType() {
        return type;
    }
}
