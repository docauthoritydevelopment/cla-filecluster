package com.cla.filecluster.domain.entity.bizlist;

import javax.persistence.Column;
import javax.persistence.Entity;

/**
 * Created by uri on 20/03/2016.
 */
@Entity
public class ConsumerBizListItem extends SimpleBizListItem {


    private String firstName;

    private String lastName;


    @Column(length = 4096)
    private String encryptedFieldsJson;

    @Override
    public String toString() {
        return "ConsumerBizListItem{"+this.getSid()+" ("+this.getEntityId()+")}";
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEncryptedFieldsJson() {
        return encryptedFieldsJson;
    }

    public void setEncryptedFieldsJson(String encryptedFieldsJson) {
        this.encryptedFieldsJson = encryptedFieldsJson;
    }
}
