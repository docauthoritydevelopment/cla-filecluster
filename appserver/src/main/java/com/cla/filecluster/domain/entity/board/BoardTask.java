package com.cla.filecluster.domain.entity.board;

import com.cla.common.domain.dto.board.BoardEntityType;
import com.cla.common.domain.dto.board.BoardTaskState;
import com.cla.common.domain.dto.board.BoardTaskType;
import com.cla.filecluster.domain.entity.BasicEntity;
import lombok.*;
import lombok.experimental.Accessors;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

@NoArgsConstructor
@Getter
@Setter
@Accessors(chain = true)
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "board_tasks")
public class BoardTask extends BasicEntity {

    private long boardId;

    private long ownerId;

    private long assignee;

    private long dueDate;

    @Enumerated(EnumType.STRING)
    private BoardTaskType boardTaskType;

    @Enumerated(EnumType.STRING)
    private BoardTaskState boardTaskState;

    private String entityId;

    @Enumerated(EnumType.STRING)
    private BoardEntityType boardEntityType;

}
