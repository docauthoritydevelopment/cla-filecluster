package com.cla.filecluster.domain.entity.filetag;

import com.cla.filecluster.domain.entity.doctype.DocType;

import javax.persistence.*;

/**
 * Created by uri on 24/08/2015.
 */
@Entity
@Table(
        name = "file_tag_settings"
)
public class FileTagSetting {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Access(AccessType.PROPERTY)
    @Id
    private long id;


    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "doc_type_id")
    private DocType docType;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "file_tag_id")
    private FileTag fileTag;

    private boolean dirty;

    private boolean deleted;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public DocType getDocType() {
        return docType;
    }

    public void setDocType(DocType docType) {
        this.docType = docType;
    }

    public FileTag getFileTag() {
        return fileTag;
    }

    public void setFileTag(FileTag fileTag) {
        this.fileTag = fileTag;
    }

    public boolean isDirty() {
        return dirty;
    }

    public void setDirty(boolean dirty) {
        this.dirty = dirty;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
}
