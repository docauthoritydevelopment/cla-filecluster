package com.cla.filecluster.domain.entity.doctype;

import com.cla.common.domain.dto.filetag.FileTagSource;
import com.cla.filecluster.domain.entity.filetag.AssociableEntity;
import com.cla.filecluster.domain.entity.filetag.ScopedEntityAssociation;
import com.cla.filecluster.domain.entity.scope.BasicScope;
import com.cla.filecluster.jobmanager.domain.entity.IdentifiableByLong;

import java.io.Serializable;

/**
 * Created by uri on 26/08/2015.
 */
public class DocTypeAssociation implements Serializable, ScopedEntityAssociation, IdentifiableByLong {

    private long id;

    private DocType docType;

    private boolean implicit;

    private Long originFolderId;

    private Integer originDepthFromRoot;

    private Long folderId;

    private Long claFileId;

    private String groupId;

    private FileTagSource associationSource;

    private boolean dirty;

    private boolean deleted;

    private long associationTimeStamp;

    private BasicScope scope;

    /**
     * If true, implies that files which have this association should be updated in SOLR
     */
    private boolean filesShouldBeDirty;

    public DocTypeAssociation(DocType docType, FileTagSource associationSource) {

        this.docType = docType;
        this.associationSource = associationSource;
        this.associationTimeStamp = System.currentTimeMillis();
    }

    public DocTypeAssociation(DocType docType, boolean dirty, FileTagSource associationSource) {
        this.docType = docType;
        this.dirty = dirty;
        this.associationSource = associationSource;
        this.associationTimeStamp = System.currentTimeMillis();
    }

    public DocTypeAssociation() {
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public DocType getDocType() {
        return docType;
    }

    public void setDocType(DocType docType) {
        this.docType = docType;
    }

    public long getAssociationTimeStamp() {
        return associationTimeStamp;
    }

    public void setAssociationTimeStamp(long associationTimeStamp) {
        this.associationTimeStamp = associationTimeStamp;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public boolean isDirty() {
        return dirty;
    }

    public void setDirty(boolean dirty) {
        this.dirty = dirty;
    }

    public boolean isImplicit() {
        return implicit;
    }

    public void setImplicit(boolean implicit) {
        this.implicit = implicit;
    }

    public Long getOriginFolderId() {
        return originFolderId;
    }

    public void setOriginFolderId(Long originFolderId) {
        this.originFolderId = originFolderId;
    }

    public Long getFolderId() {
        return folderId;
    }

    public void setFolderId(Long folderId) {
        this.folderId = folderId;
    }

    public Long getClaFileId() {
        return claFileId;
    }

    public void setClaFileId(Long claFileId) {
        this.claFileId = claFileId;
    }

    public FileTagSource getAssociationSource() {
        return associationSource;
    }

    public void setAssociationSource(FileTagSource associationSource) {
        this.associationSource = associationSource;
    }

    public BasicScope getScope() {
        return scope;
    }

    public void setScope(BasicScope scope) {
        this.scope = scope;
    }

    public boolean isFilesShouldBeDirty() {
        return filesShouldBeDirty;
    }

    public void setFilesShouldBeDirty(boolean filesShouldBeDirty) {
        this.filesShouldBeDirty = filesShouldBeDirty;
    }

    public Integer getOriginDepthFromRoot() {
        return originDepthFromRoot;
    }

    public void setOriginDepthFromRoot(Integer originDepthFromRoot) {
        this.originDepthFromRoot = originDepthFromRoot;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DocTypeAssociation that = (DocTypeAssociation) o;

        if (implicit != that.implicit) return false;
        if (dirty != that.dirty) return false;
        if (deleted != that.deleted) return false;
        if (docType != null ? !docType.equals(that.docType) : that.docType != null) return false;
        if (originFolderId != null ? !originFolderId.equals(that.originFolderId) : that.originFolderId != null) return false;
        if (associationSource != that.associationSource) return false;
        return scope != null ? scope.equals(that.scope) : that.scope == null;
    }

    @Override
    public int hashCode() {
        int result = docType != null ? docType.hashCode() : 0;
        result = 31 * result + (implicit ? 1 : 0);
        result = 31 * result + (originFolderId != null ? originFolderId.hashCode() : 0);
        result = 31 * result + (associationSource != null ? associationSource.hashCode() : 0);
        result = 31 * result + (dirty ? 1 : 0);
        result = 31 * result + (deleted ? 1 : 0);
        result = 31 * result + (scope != null ? scope.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "DocTypeAssociation{" +
                "id=" + id +
                ", implicit=" + implicit +
                ", originFolderId=" + originFolderId +
                ", folderId=" + folderId +
                ", associationSource=" + associationSource +
                ", dirty=" + dirty +
                ", deleted=" + deleted +
                ", associationTimeStamp=" + associationTimeStamp +
                ", scope=" + scope +
                ", filesShouldBeDirty=" + filesShouldBeDirty +
                '}';
    }

    @Override
    public AssociableEntity getAssociableEntity() {
        return docType;
    }
}
