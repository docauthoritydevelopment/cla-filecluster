package com.cla.filecluster.domain.entity.security;

import com.cla.common.domain.dto.security.LdapServerDialect;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by Itai Marko.
 */

@Entity
@Table(name = "ldap_connection_details",
       indexes = {
               @Index(unique = true, name = "name_idx", columnList = "name")
       })
public class LdapConnectionDetails {

    @Id
    @GeneratedValue
    private Long id;

    @NotNull
    private String name;

    @Enumerated(EnumType.STRING)
    private LdapServerDialect serverDialect;

    @NotNull
    private Boolean isSSL;

    @NotNull
    private String host;

    @NotNull
    private Integer port;

    @NotNull
    private String manageDN;

    @NotNull
    private String managerPass;

    private boolean enabled = true;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LdapServerDialect getServerDialect() {
        return serverDialect;
    }

    public void setServerDialect(LdapServerDialect serverDialect) {
        this.serverDialect = serverDialect;
    }

    public Boolean isSSL() {
        return isSSL;
    }

    public void setIsSSL(Boolean SSL) {
        isSSL = SSL;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public String getManageDN() {
        return manageDN;
    }

    public void setManageDN(String manageDN) {
        this.manageDN = manageDN;
    }

    public String getManagerPass() {
        return managerPass;
    }

    public void setManagerPass(String managerPass) {
        this.managerPass = managerPass;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public String toString() {
        return "LdapConnectionDetails{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", isSSL=" + isSSL +
                ", host='" + host + '\'' +
                ", port=" + port +
                ", manageDN='" + manageDN + '\'' +
                ", managerPass='" + managerPass + '\'' +
                '}';
    }
}
