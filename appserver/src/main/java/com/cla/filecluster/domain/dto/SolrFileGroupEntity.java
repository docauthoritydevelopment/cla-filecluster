package com.cla.filecluster.domain.dto;

import org.apache.solr.client.solrj.beans.Field;

import java.util.List;

public class SolrFileGroupEntity implements SolrEntity {

    @Field
    private String id;

    @Field
    private String name;

    @Field
    private String generatedName;

    @Field
    private Boolean deleted;

    @Field
    private Boolean dirty;

    @Field
    private Integer type;

    @Field
    private List<String> associations;

    @Field
    private Long creationDate;

    @Field
    private Long updateDate;

    @Field
    private Long numOfFiles;

    @Field
    private Long countOnLastNaming;

    @Field
    private Long firstMemberId;

    @Field
    private Long firstMemberContentId;

    @Field
    private String rawAnalysisGroupId;

    @Field
    private String parentGroupId;

    @Field
    private Boolean hasSubgroups;

    @Field
    private String firstProposedNames;
    @Field
    private String proposedNames;
    @Field
    private String proposedNewNames;
    @Field
    private String secondProposedNames;
    @Field
    private String thirdProposedNames;

    @Field
    private String fileNamestv;
    @Field
    private String parentDirtv;
    @Field
    private String grandParentDirtv;

    @Field
    private Long dirtyDate;

    public String getIdAsString() {return id;}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGeneratedName() {
        return generatedName;
    }

    public void setGeneratedName(String generatedName) {
        this.generatedName = generatedName;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Boolean getDirty() {
        return dirty;
    }

    public void setDirty(Boolean dirty) {
        this.dirty = dirty;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public List<String> getAssociations() {
        return associations;
    }

    public void setAssociations(List<String> associations) {
        this.associations = associations;
    }

    public Long getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Long creationDate) {
        this.creationDate = creationDate;
    }

    public Long getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Long updateDate) {
        this.updateDate = updateDate;
    }

    public Long getNumOfFiles() {
        return numOfFiles;
    }

    public void setNumOfFiles(Long numOfFiles) {
        this.numOfFiles = numOfFiles;
    }

    public Long getCountOnLastNaming() {
        return countOnLastNaming;
    }

    public void setCountOnLastNaming(Long countOnLastNaming) {
        this.countOnLastNaming = countOnLastNaming;
    }

    public Long getFirstMemberId() {
        return firstMemberId;
    }

    public void setFirstMemberId(Long firstMemberId) {
        this.firstMemberId = firstMemberId;
    }

    public Long getFirstMemberContentId() {
        return firstMemberContentId;
    }

    public void setFirstMemberContentId(Long firstMemberContentId) {
        this.firstMemberContentId = firstMemberContentId;
    }

    public String getRawAnalysisGroupId() {
        return rawAnalysisGroupId;
    }

    public void setRawAnalysisGroupId(String rawAnalysisGroupId) {
        this.rawAnalysisGroupId = rawAnalysisGroupId;
    }

    public String getParentGroupId() {
        return parentGroupId;
    }

    public void setParentGroupId(String parentGroupId) {
        this.parentGroupId = parentGroupId;
    }

    public Boolean getHasSubgroups() {
        return hasSubgroups;
    }

    public void setHasSubgroups(Boolean hasSubgroups) {
        this.hasSubgroups = hasSubgroups;
    }

    public String getFirstProposedNames() {
        return firstProposedNames;
    }

    public void setFirstProposedNames(String firstProposedNames) {
        this.firstProposedNames = firstProposedNames;
    }

    public String getProposedNames() {
        return proposedNames;
    }

    public void setProposedNames(String proposedNames) {
        this.proposedNames = proposedNames;
    }

    public String getProposedNewNames() {
        return proposedNewNames;
    }

    public void setProposedNewNames(String proposedNewNames) {
        this.proposedNewNames = proposedNewNames;
    }

    public String getSecondProposedNames() {
        return secondProposedNames;
    }

    public void setSecondProposedNames(String secondProposedNames) {
        this.secondProposedNames = secondProposedNames;
    }

    public String getThirdProposedNames() {
        return thirdProposedNames;
    }

    public void setThirdProposedNames(String thirdProposedNames) {
        this.thirdProposedNames = thirdProposedNames;
    }

    public String getFileNamestv() {
        return fileNamestv;
    }

    public void setFileNamestv(String fileNamestv) {
        this.fileNamestv = fileNamestv;
    }

    public String getParentDirtv() {
        return parentDirtv;
    }

    public void setParentDirtv(String parentDirtv) {
        this.parentDirtv = parentDirtv;
    }

    public String getGrandParentDirtv() {
        return grandParentDirtv;
    }

    public void setGrandParentDirtv(String grandParentDirtv) {
        this.grandParentDirtv = grandParentDirtv;
    }

    public Long getDirtyDate() {
        return dirtyDate;
    }

    public void setDirtyDate(Long dirtyDate) {
        this.dirtyDate = dirtyDate;
    }
}
