package com.cla.filecluster.domain.entity.bizlist;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by uri on 31/12/2015.
 */
@Entity
public class ClientBizListItem extends SimpleBizListItem {

    @Override
    public String toString() {
        return "ClientBizListItem{"+this.getSid()+"}";
    }
}
