package com.cla.filecluster.domain.specification;

import com.cla.filecluster.domain.entity.report.TrendChartData;

import javax.persistence.criteria.*;

/**
 * Created by uri on 27/07/2016.
 */
public class TrendChartDataSpecification extends KendoFilterSpecification<TrendChartData> {

    private long trendChartId;
    private final Long startTime;
    private final Long endTime;

    public TrendChartDataSpecification(long trendChartId, Long startTime, Long endTime) {
        super(TrendChartData.class, null);
        this.trendChartId = trendChartId;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    @Override
    protected boolean isDateField(String dtoField) {
        return false;
    }

    @Override
    protected String convertModelField(String dtoField) {
        return dtoField;
    }

    @Override
    public Predicate toPredicate(Root<TrendChartData> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
        Path<Long> requestedTimeStamp= root.get("requestedTimeStamp");
        Path<Long> trendChartIdPath = root.get("trendChart").get("id");

        Predicate trendChartFilter = cb.equal(trendChartIdPath,trendChartId);
        Predicate fromFilter = cb.greaterThanOrEqualTo(requestedTimeStamp,startTime);
        Predicate toFilter = cb.lessThanOrEqualTo(requestedTimeStamp,endTime);
        Predicate basePredicate = cb.and(trendChartFilter,fromFilter,toFilter);
        return basePredicate;
    }
}
