package com.cla.filecluster.domain.dto;

import com.cla.common.domain.dto.AssociableEntityDto;
import com.cla.common.domain.dto.bizlist.SimpleBizListItemDto;
import com.cla.common.domain.dto.department.DepartmentDto;
import com.cla.common.domain.dto.doctype.DocTypeDto;
import com.cla.common.domain.dto.filetag.FileTagDto;
import com.cla.common.domain.dto.security.FunctionalRoleDto;
import com.cla.filecluster.domain.entity.bizlist.SimpleBizListItem;
import com.cla.filecluster.domain.entity.department.Department;
import com.cla.filecluster.domain.entity.doctype.DocType;
import com.cla.filecluster.domain.entity.filetag.AssociableEntity;
import com.cla.filecluster.domain.entity.filetag.FileTag;
import com.cla.filecluster.domain.entity.security.FunctionalRole;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.Objects;

/**
 * Created by: yael
 * Created on: 4/21/2019
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AssociationEntity {

    private Long creationDate;

    private Long departmentId;
    private Long docTypeId;
    private Long fileTagId;
    private Long functionalRoleId;
    private String simpleBizListItemSid;

    private Long associationScopeId;

    private Boolean implicit;
    private Long originFolderId;
    private Integer originDepthFromRoot;

    public Long getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Long creationDate) {
        this.creationDate = creationDate;
    }

    public Long getDocTypeId() {
        return docTypeId;
    }

    public void setDocTypeId(Long docTypeId) {
        this.docTypeId = docTypeId;
    }

    public Long getFileTagId() {
        return fileTagId;
    }

    public void setFileTagId(Long fileTagId) {
        this.fileTagId = fileTagId;
    }

    public Long getFunctionalRoleId() {
        return functionalRoleId;
    }

    public void setFunctionalRoleId(Long functionalRoleId) {
        this.functionalRoleId = functionalRoleId;
    }

    public String getSimpleBizListItemSid() {
        return simpleBizListItemSid;
    }

    public void setSimpleBizListItemSid(String simpleBizListItemSid) {
        this.simpleBizListItemSid = simpleBizListItemSid;
    }

    public Long getAssociationScopeId() {
        return associationScopeId;
    }

    public void setAssociationScopeId(Long associationScopeId) {
        this.associationScopeId = associationScopeId;
    }

    public Boolean getImplicit() {
        return implicit;
    }

    public void setImplicit(Boolean implicit) {
        this.implicit = implicit;
    }

    public Long getOriginFolderId() {
        return originFolderId;
    }

    public void setOriginFolderId(Long originFolderId) {
        this.originFolderId = originFolderId;
    }

    public Integer getOriginDepthFromRoot() {
        return originDepthFromRoot;
    }

    public void setOriginDepthFromRoot(Integer originDepthFromRoot) {
        this.originDepthFromRoot = originDepthFromRoot;
    }

    public Long getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Long departmentId) {
        this.departmentId = departmentId;
    }

    public boolean sameEntity(AssociableEntity associableEntity) {
        if (associableEntity instanceof FileTag) {
           return this.getFileTagId() != null && this.getFileTagId().equals(((FileTag)associableEntity).getId());
        } else if (associableEntity instanceof DocType) {
            return this.getDocTypeId() != null && this.getDocTypeId().equals(((DocType)associableEntity).getId());
        } else if (associableEntity instanceof FunctionalRole) {
            return this.getFunctionalRoleId() != null && this.getFunctionalRoleId().equals(((FunctionalRole)associableEntity).getId());
        } else if (associableEntity instanceof SimpleBizListItem) {
            return this.getSimpleBizListItemSid() != null && this.getSimpleBizListItemSid().equals(((SimpleBizListItem)associableEntity).getSid());
        } else if (associableEntity instanceof Department) {
            return this.getDepartmentId() != null && this.getDepartmentId().equals(((Department)associableEntity).getId());
        }
        return false;
    }

    public boolean sameEntity(AssociableEntityDto associableEntity) {
        if (associableEntity instanceof FileTagDto) {
            return this.getFileTagId() != null && this.getFileTagId().equals(((FileTagDto)associableEntity).getId());
        } else if (associableEntity instanceof DocTypeDto) {
            return this.getDocTypeId() != null && this.getDocTypeId().equals(((DocTypeDto)associableEntity).getId());
        } else if (associableEntity instanceof FunctionalRoleDto) {
            return this.getFunctionalRoleId() != null && this.getFunctionalRoleId().equals(((FunctionalRoleDto)associableEntity).getId());
        } else if (associableEntity instanceof SimpleBizListItemDto) {
            return this.getSimpleBizListItemSid() != null && this.getSimpleBizListItemSid().equals(((SimpleBizListItemDto)associableEntity).getSid());
        } else if (associableEntity instanceof DepartmentDto) {
            return this.getDepartmentId() != null && this.getDepartmentId().equals(((DepartmentDto)associableEntity).getId());
        }
        return false;
    }

    public boolean similarAssociation(AssociationEntity entity) {
        return Objects.equals(departmentId, entity.departmentId) &&
                Objects.equals(docTypeId, entity.docTypeId) &&
                Objects.equals(fileTagId, entity.fileTagId) &&
                Objects.equals(functionalRoleId, entity.functionalRoleId) &&
                Objects.equals(simpleBizListItemSid, entity.simpleBizListItemSid) &&
                Objects.equals(associationScopeId, entity.associationScopeId);
    }

    public void copyAssociation(AssociationEntity entity) {
        entity.setDepartmentId(departmentId);
        entity.setFunctionalRoleId(functionalRoleId);
        entity.setFileTagId(fileTagId);
        entity.setSimpleBizListItemSid(simpleBizListItemSid);
        entity.setDocTypeId(docTypeId);
        entity.setAssociationScopeId(associationScopeId);
        entity.setImplicit(implicit);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AssociationEntity entity = (AssociationEntity) o;
        return Objects.equals(departmentId, entity.departmentId) &&
                Objects.equals(docTypeId, entity.docTypeId) &&
                Objects.equals(fileTagId, entity.fileTagId) &&
                Objects.equals(functionalRoleId, entity.functionalRoleId) &&
                Objects.equals(simpleBizListItemSid, entity.simpleBizListItemSid) &&
                Objects.equals(associationScopeId, entity.associationScopeId) &&
                Objects.equals(implicit, entity.implicit) &&
                Objects.equals(originFolderId, entity.originFolderId) &&
                Objects.equals(originDepthFromRoot, entity.originDepthFromRoot);
    }

    @Override
    public int hashCode() {
        return Objects.hash(creationDate, departmentId, docTypeId, fileTagId, functionalRoleId, simpleBizListItemSid, associationScopeId, implicit, originFolderId, originDepthFromRoot);
    }

    @JsonIgnore
    public boolean isValid() {
        return (departmentId != null || docTypeId != null || fileTagId != null || functionalRoleId != null || simpleBizListItemSid != null);
    }

    @Override
    public String toString() {
        return "AssociationEntity{" +
                "creationDate=" + creationDate +
                ", departmentId=" + departmentId +
                ", docTypeId=" + docTypeId +
                ", fileTagId=" + fileTagId +
                ", functionalRoleId=" + functionalRoleId +
                ", simpleBizListItemSid='" + simpleBizListItemSid + '\'' +
                ", associationScopeId=" + associationScopeId +
                ", implicit=" + implicit +
                ", originFolderId=" + originFolderId +
                ", originDepthFromRoot=" + originDepthFromRoot +
                '}';
    }
}
