package com.cla.filecluster.domain.specification.solr;

import com.cla.common.domain.dto.kendo.FilterDescriptor;
import com.cla.common.constants.CatFileFieldType;

import java.time.LocalDateTime;

/**
 * Created by uri on 12/10/2015.
 */
public class SolrRangeFacetSpecification extends SolrSpecification {

    public SolrRangeFacetSpecification(FilterDescriptor filterDescriptor, CatFileFieldType rangeFacetField, String facetRangeGap,
                                       LocalDateTime facetDateRangeStart, LocalDateTime facetDateRangeStop) {

        super(filterDescriptor);
        this.facetRangeField = rangeFacetField;
        this.facetRangeGap = facetRangeGap;
        this.facetDateStart = facetDateRangeStart;
        this.facetDateEnd = facetDateRangeStop;
    }

}
