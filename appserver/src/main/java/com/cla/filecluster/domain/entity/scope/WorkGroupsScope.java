package com.cla.filecluster.domain.entity.scope;

import com.cla.filecluster.domain.entity.security.WorkGroup;
import com.google.common.collect.Sets;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Set;


@Entity
@DiscriminatorValue(value = "WORK_GROUP")
public class WorkGroupsScope extends BasicScope<Set<WorkGroup>> {

    @NotNull
    private String name;

    private String description;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "work_groups_scope_work_groups",
            joinColumns = @JoinColumn(name = "work_groups_scope_id"),
            inverseJoinColumns = @JoinColumn(name = "work_group_id"))
    private Set<WorkGroup> workGroups = Sets.newHashSet();


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public Set<WorkGroup> getScopeBindings() {
        return workGroups;
    }

    public void setScopeBindings(Set<WorkGroup> workGroups) {
        this.workGroups = workGroups;
    }

}
