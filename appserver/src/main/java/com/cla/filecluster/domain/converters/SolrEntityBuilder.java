package com.cla.filecluster.domain.converters;

import com.cla.filecluster.domain.dto.SolrEntity;
import com.google.common.base.Charsets;
import org.apache.solr.common.SolrInputDocument;
import org.springframework.util.DigestUtils;

import java.io.UnsupportedEncodingException;

public abstract class SolrEntityBuilder<T extends SolrEntity> {

    public abstract T build();

    public abstract void update(final T existing);

    public abstract SolrInputDocument buildAtomicUpdate();

    public static String getHashedString(final String fileName) {
        try {
            return DigestUtils.md5DigestAsHex(fileName.getBytes(Charsets.UTF_8.name()));
        }
        catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }
}
