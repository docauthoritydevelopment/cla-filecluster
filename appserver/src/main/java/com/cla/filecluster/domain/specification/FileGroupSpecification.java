package com.cla.filecluster.domain.specification;

import com.cla.common.domain.dto.group.GroupDtoFieldConverter;
import com.cla.common.domain.dto.kendo.FilterDescriptor;
import com.cla.filecluster.domain.entity.pv.FileGroup;
import org.apache.commons.collections.CollectionUtils;

import javax.persistence.criteria.*;

import static com.cla.connector.constants.KendoFilterConstants.*;

/**
 *
 * Created by uri on 08/07/2015.
 */
public class FileGroupSpecification extends KendoFilterSpecification<FileGroup> {

    public FileGroupSpecification(FilterDescriptor filter) {
        super(FileGroup.class, filter);
    }

    @Override
    public Predicate toPredicate(Root<FileGroup> root, CriteriaQuery<?> query, CriteriaBuilder cb) {

        //This part allow to use this specification in pageable queries
        Class clazz = query.getResultType();
        if (clazz.equals(FileGroup.class)) {
            //Prevent N+1 problem. see http://jdpgrailsdev.github.io/blog/2014/09/09/spring_data_hibernate_join.html
            root.fetch("firstMember", JoinType.LEFT);
            root.fetch("associations", JoinType.LEFT);
        }
        Path<Object> deleted = root.get("deleted");
        Predicate basePredicate = cb.equal(deleted,Boolean.FALSE);
        if (filter == null || CollectionUtils.isEmpty(filter.getFilters())) {
            //Return an empty or (bogus filter)
            return basePredicate;
        }
        Predicate filterPredicate = buildFilter(this.filter, root, query, cb);
        return cb.and(basePredicate,filterPredicate);
    }

    @Override
    protected String convertOperatorIfNeeded(String dtoField, Object value, String operator) {
        if ("id".equals(dtoField)) {
            if (EQ.equals(operator)) {
                return STARTSWITH;
            }
            if (NEQ.equals(operator)) {
                return DOESNOTCONTAIN;
            }
        }
        return super.convertOperatorIfNeeded(dtoField, value, operator);
    }

    protected String convertModelField(String dtoField) {
        return GroupDtoFieldConverter.convertDtoField(dtoField);
    }

    @Override
    protected boolean isDateField(String dtoField) {
        return false;
    }
}
