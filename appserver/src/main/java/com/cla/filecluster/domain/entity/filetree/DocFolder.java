package com.cla.filecluster.domain.entity.filetree;

import com.cla.common.domain.dto.file.FolderType;
import com.cla.common.domain.dto.filetree.PathDescriptorType;
import com.cla.connector.utils.FileNamingUtils;
import org.springframework.util.DigestUtils;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.List;

/**
 * Documents folder
 * Created by uri on 11/08/2015.
 */
@SuppressWarnings("serial")
public class DocFolder implements Serializable {

    private Long id;

    /**
     * Always ends with /
     * Only / between names
     */
    private String path;

    /**
     * Not Normalized. This can be used as a prefix for claFile.fullPath
     */
    private String realPath;

    private String name;

    private String folderHashed;

    private Long parentFolderId;

    private Integer numOfSubFolders;

    private PathDescriptorType pathDescriptorType;

    private Long rootFolderId;

    private int depthFromRoot;

    private int absoluteDepth;

    private List<String> parentsInfo;

    private String mediaEntityId;

    private int numOfDirectFiles;

    private int numOfAllFiles;

    private FolderType folderType;

    private boolean deleted;

    private String aclSignature;

    private String syncState;

    private String mailboxUpn;

    private List<String> associations;

    public Long getId() {
        return id;
    }

    /**
     * We expect that all paths are given in Unix style. Without trailing slash and canonical
     */
    public String getPath() {
        return path;
    }

    /**
     * We expect that all paths are given in Unix style. Without trailing slash and canonical
     *
     * @param path folder path
     */
    public void setPath(String path) {
        this.path = path;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PathDescriptorType getPathDescriptorType() {
        return pathDescriptorType;
    }

    public void setPathDescriptorType(PathDescriptorType pathDescriptorType) {
        this.pathDescriptorType = pathDescriptorType;
    }

    public Long getRootFolderId() {
        return rootFolderId;
    }

    public void setRootFolderId(Long rootFolderId) {
        this.rootFolderId = rootFolderId;
    }

    public int getDepthFromRoot() {
        return depthFromRoot;
    }

    public void setDepthFromRoot(int depthFromRoot) {
        this.depthFromRoot = depthFromRoot;
    }

    public int getAbsoluteDepth() {
        return absoluteDepth;
    }

    public void setAbsoluteDepth(int absoluteDepth) {
        this.absoluteDepth = absoluteDepth;
    }

    public Long getParentFolderId() {
        return parentFolderId;
    }

    public void setParentFolderId(Long parentFolderId) {
        this.parentFolderId = parentFolderId;
    }

    public static String getHashedFolderString(final String path, final Long rootFolderId) {
        try {
            String pathString = FileNamingUtils.convertPathToUniversalString(path);
            return DigestUtils.md5DigestAsHex((rootFolderId + ":" + pathString).getBytes("UTF-8"));
        } catch (final UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    public void updateHashedFolderString() {
        String hashedFolderString = getHashedFolderString(getPath(), rootFolderId);
        setFolderHashed(hashedFolderString);
    }

    public String getFolderHashed() {
        return folderHashed;
    }

    public void setFolderHashed(String folderHashed) {
        this.folderHashed = folderHashed;
    }

    public Integer getNumOfSubFolders() {
        return numOfSubFolders;
    }

    public void setNumOfSubFolders(Integer numOfSubFolders) {
        this.numOfSubFolders = numOfSubFolders;
    }

    public List<String> getParentsInfo() {
        return parentsInfo;
    }

    public void setParentsInfo(List<String> parentsInfo) {
        this.parentsInfo = parentsInfo;
    }

    public int getNumOfDirectFiles() {
        return numOfDirectFiles;
    }

    public void setNumOfDirectFiles(int numOfDirectFiles) {
        this.numOfDirectFiles = numOfDirectFiles;
    }

    public int getNumOfAllFiles() {
        return numOfAllFiles;
    }

    public void setNumOfAllFiles(int numOfAllFiles) {
        this.numOfAllFiles = numOfAllFiles;
    }

    public String getRealPath() {
        return realPath;
    }

    public void setRealPath(String realPath) {
        this.realPath = realPath;
    }

    @Override
    public String toString() {
        return "DocFolder{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", path='" + path + '\'' +
                '}';
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DocFolder docFolder = (DocFolder) o;
        if (id != null) {
            return id.equals(docFolder.id);
        }
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        }
        return super.hashCode();
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public String getMediaEntityId() {
        return mediaEntityId;
    }

    public void setMediaEntityId(String mediaEntityId) {
        this.mediaEntityId = mediaEntityId;
    }

    public String getAclSignature() {
        return aclSignature;
    }

    public void setAclSignature(String aclSignature) {
        this.aclSignature = aclSignature;
    }

    public FolderType getFolderType() {
        return folderType;
    }

    public void setFolderType(FolderType folderType) {
        this.folderType = folderType;
    }

    public String getSyncState() {
        return syncState;
    }

    public void setSyncState(String syncState) {
        this.syncState = syncState;
    }

    public String getMailboxUpn() {
        return mailboxUpn;
    }

    public void setMailboxUpn(String mailboxUpn) {
        this.mailboxUpn = mailboxUpn;
    }

    public List<String> getAssociations() {
        return associations;
    }

    public void setAssociations(List<String> associations) {
        this.associations = associations;
    }
}

