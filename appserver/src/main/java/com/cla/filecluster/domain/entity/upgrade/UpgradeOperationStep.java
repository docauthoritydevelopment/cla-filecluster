package com.cla.filecluster.domain.entity.upgrade;

import com.vladmihalcea.hibernate.type.json.JsonStringType;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.*;
import java.util.Map;

@Entity
@TypeDefs({
        @TypeDef(name = "json", typeClass = JsonStringType.class)
})
@Table(	name = "upgrade_op_step")
public class UpgradeOperationStep {

    @Id
    @GeneratedValue
    private Integer id;

    @Column(name = "upgrade_version")
    private Integer upgradeVersion;

    @Column(name = "upgrade_step")
    private Integer upgradeStep;

    @Column(name = "start_time")
    private Long startTime;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false, name = "state")
    private UpgradeOperationState state = UpgradeOperationState.NEW;

    @Column(name = "last_state_time")
    private Long stateUpdateTime;

    @Type(type = "json")
    @Column(columnDefinition = "json", name = "data")
    private Map<String,String> opData;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUpgradeVersion() {
        return upgradeVersion;
    }

    public void setUpgradeVersion(Integer upgradeVersion) {
        this.upgradeVersion = upgradeVersion;
    }

    public Integer getUpgradeStep() {
        return upgradeStep;
    }

    public void setUpgradeStep(Integer upgradeStep) {
        this.upgradeStep = upgradeStep;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public UpgradeOperationState getState() {
        return state;
    }

    public void setState(UpgradeOperationState state) {
        this.state = state;
    }

    public Long getStateUpdateTime() {
        return stateUpdateTime;
    }

    public void setStateUpdateTime(Long stateUpdateTime) {
        this.stateUpdateTime = stateUpdateTime;
    }

    public Map<String, String> getOpData() {
        return opData;
    }

    public void setOpData(Map<String, String> opData) {
        this.opData = opData;
    }
}
