package com.cla.filecluster.domain.entity.pv;

import com.cla.connector.domain.dto.JsonSerislizableVisible;
import com.cla.connector.domain.dto.error.ProcessingErrorType;
import com.cla.connector.domain.dto.media.MediaType;
import com.cla.filecluster.jobmanager.domain.entity.IdentifiableByLong;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "err_processed_files",
        indexes = {
                @Index(name = "run_id_idx", columnList = "runId"),
        })
public class ErrProcessedFile implements JsonSerislizableVisible,IdentifiableByLong {

    static final int  MAX_TEXT_LENGTH = 4096;

    @TableGenerator(name = "err-process-table-hilo-generator", allocationSize = 400)
    @GeneratedValue(generator = "err-process-table-hilo-generator", strategy = GenerationType.TABLE)
    @Id
    private Long id;


    @Column(name = "cla_file_id")
    private Long claFile;

    @Column(name = "content_id")
    private Long contentId;

    @Column(name = "folder_id")
    private Long folderId;

    @Column(name = "root_folder_id")
    private Long rootFolderId;

    @Column(name = "full_path", length = MAX_TEXT_LENGTH)
    private String fullPath;

    private Integer fileType;

    @Column(length = MAX_TEXT_LENGTH)
    private String text;

    @Column(name = "exception_text", length = MAX_TEXT_LENGTH)
    private String exceptionText;

    private ProcessingErrorType type;

    private Long runId;

    private Long dateCreated;

    @Column(columnDefinition = "datetime(3)", updatable = false)
    private Date createdOnDB;

    private MediaType mediaType;

    @Length(max = 20)
    private String extension;

    @PrePersist
    private void setCreatedOnDB() {
        createdOnDB = new Date();
    }

    public ErrProcessedFile() {
    }

    public ErrProcessedFile(final Long fileId,
                            final ProcessingErrorType type,
                            final String text,
                            final String exceptionText,
                            final Long runId,
                            final String extension,
                            final MediaType mediaType) {
        this.claFile = fileId;
        this.type = type;
        this.text = trimField(text);
        this.exceptionText = trimField(exceptionText);
        this.runId = runId;
        this.dateCreated = System.currentTimeMillis();
        setExtension(extension);
        this.mediaType = mediaType;
    }

    private static final int fieldLength = 255;

    private static String trimField(final String v) {
        return trimField(v, fieldLength);
    }

    private static String trimField(final String v, int fieldLength) {
        return (v == null || v.length() < fieldLength) ? v : v.substring(0, fieldLength);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getClaFile() {
        return claFile;
    }

    public void setClaFile(final Long claFile) {
        this.claFile = claFile;
    }

    public String getText() {
        return text;
    }

    public void setText(final String text) {
        this.text = trimField(text);
    }

    public String getExceptionText() {
        return exceptionText;
    }

    public void setExceptionText(final String exceptionText) {
        this.exceptionText = trimField(exceptionText);
    }

    public ProcessingErrorType getType() {
        return type;
    }

    public void setType(final ProcessingErrorType type) {
        this.type = type;
    }

    public Date getCreatedOnDB() {
        return createdOnDB;
    }

    public Long getRunId() {
        return runId;
    }

    public void setRunId(Long runId) {
        this.runId = runId;
    }

    public String getFullPath() {
        return fullPath;
    }

    public void setFullPath(String fullPath) {
        this.fullPath = fullPath;
    }

    public Integer getFileType() {
        return fileType;
    }

    public void setFileType(Integer fileType) {
        this.fileType = fileType;
    }

    public Long getContentId() {
        return contentId;
    }

    public void setContentId(Long contentId) {
        this.contentId = contentId;
    }

    public Long getFolderId() {
        return folderId;
    }

    public void setFolderId(Long folderId) {
        this.folderId = folderId;
    }

    public Long getRootFolderId() {
        return rootFolderId;
    }

    public void setRootFolderId(Long rootFolderId) {
        this.rootFolderId = rootFolderId;
    }

    public Long getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Long dateCreated) {
        this.dateCreated = dateCreated;
    }

    public MediaType getMediaType() {
        return mediaType;
    }

    public void setMediaType(MediaType mediaType) {
        this.mediaType = mediaType;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = trimField(extension, 20);
    }
}
