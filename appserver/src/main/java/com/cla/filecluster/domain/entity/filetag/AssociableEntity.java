package com.cla.filecluster.domain.entity.filetag;

/**
 * Created by uri on 31/01/2016.
 */
public interface AssociableEntity {

    String createIdentifierString();

    String getName();
}
