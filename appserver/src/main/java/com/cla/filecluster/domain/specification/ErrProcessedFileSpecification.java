package com.cla.filecluster.domain.specification;

import com.cla.common.domain.dto.kendo.FilterDescriptor;
import com.cla.filecluster.domain.entity.pv.ErrProcessedFile;
import org.apache.commons.collections.CollectionUtils;

import javax.persistence.criteria.*;

/**
 * Created by uri on 25/11/2015.
 */
public class ErrProcessedFileSpecification extends KendoFilterSpecification {

    Long rootFolderId;

    public ErrProcessedFileSpecification(FilterDescriptor filter) {
        super(ErrProcessedFile.class, filter);
    }

    public ErrProcessedFileSpecification(Long rootFolderId, FilterDescriptor filter) {
        super(ErrProcessedFile.class, filter);
        this.rootFolderId = rootFolderId;
    }

    @Override
    protected boolean isDateField(String dtoField) {
        return false;
    }

    @Override
    protected String convertModelField(String dtoField) {
        return dtoField;
    }

    @Override
    public Predicate toPredicate(Root root, CriteriaQuery query, CriteriaBuilder cb) {
        //Join group and folder
        Class clazz = query.getResultType();

        Predicate baseFilter = cb.conjunction();
        if (rootFolderId != null) {
            Path<Object> rootFolderId = root.get("rootFolderId");
            baseFilter = cb.equal(rootFolderId,rootFolderId);
        }
        if (filter == null || CollectionUtils.isEmpty(filter.getFilters())) {
            //Return empty filter
            return baseFilter;
        }
        else {
            Predicate filterPredicate = buildFilter(this.filter, root, query, cb);
            return cb.and(baseFilter, filterPredicate);
        }
    }
}
