package com.cla.filecluster.domain.entity.security;

import javax.persistence.*;
import java.util.Objects;

/**
 * Created by uri on 07/09/2016.
 */
@Entity
@DiscriminatorValue(value="BIZ_ROLE")
public class BizRole extends CompositeRole {

    @Override
    public boolean equals(Object o) {
        // self check
        if (this == o)
            return true;

        // null check
        if (o == null)
            return false;

        // type check and cast
        if (!(o instanceof BizRole))
            return false;

        // field comparison
        BizRole other = (BizRole) o;
        return Objects.equals(other.getId(), this.getId())
                && Objects.equals(other.getName(), this.getName());
    }

    @Override
    public int hashCode() {
        int result = getId() != null ? getId().hashCode() : 0;
        result = 31 * result + getName().hashCode();
        result = 31 * result + getClass().hashCode();
        return result;
    }
}
