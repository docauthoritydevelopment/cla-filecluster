package com.cla.filecluster.domain.entity.searchpatterns;

import com.google.common.collect.Sets;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

/**
 * Created by ophir on 7/04/2019.
 */

@Entity
public class Regulation {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;

    private boolean active = false;

    @Column(updatable=false)
    private Long createdOnDB;

    @PrePersist
    private void setCreatedOnDB(){
        createdOnDB = System.currentTimeMillis();
    }

    public Long getCreatedOnDB() {
        return createdOnDB;
    }

    public void setCreatedOnDB(Long createdOnDB) {
        this.createdOnDB = createdOnDB;
    }

    @ManyToMany(fetch = FetchType.EAGER,mappedBy = "regulations")
    private Set<TextSearchPattern> patterns = Sets.newHashSet();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Set<TextSearchPattern> getPatterns() {
        return patterns;
    }

    public void setPatterns(Set<TextSearchPattern> patterns) {
        this.patterns = patterns;
    }
}
