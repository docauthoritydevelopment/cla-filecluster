package com.cla.filecluster.domain.converters;

import com.cla.common.constants.BizListFieldType;
import com.cla.filecluster.domain.dto.SolrBizListEntity;
import com.cla.filecluster.repository.solr.query.AtomicUpdateBuilder;
import org.apache.solr.common.SolrInputDocument;

import static com.cla.filecluster.repository.solr.query.SolrOperator.SET;

/**
 * Builder for biz list solr entity
 *
 * Created by: yael
 * Created on: 7/1/2018
 */
public class SolrBizListBuilder extends SolrEntityBuilder<SolrBizListEntity> {

    private String id;
    private Long fileId;
    private Long bizListId;
    private Integer bizListItemType;
    private Integer aggMinCount;
    private String bizListItemSid;
    private String ruleName;
    private Boolean dirty;
    private Long counter;

    public static SolrBizListBuilder create(){
        return new SolrBizListBuilder();
    }

    @Override
    public SolrBizListEntity build() {

        SolrBizListEntity entity = new SolrBizListEntity();

        entity.setId(id);
        entity.setFileId(fileId);
        entity.setAggMinCount(aggMinCount);
        entity.setBizListId(bizListId);
        entity.setBizListItemSid(bizListItemSid);
        entity.setBizListItemType(bizListItemType);
        entity.setRuleName(ruleName);
        entity.setDirty(dirty);
        entity.setCounter(counter);

        return entity;
    }

    @Override
    public void update(final SolrBizListEntity existing) {
        throw new RuntimeException("Not implemented");
    }

    @Override
    public SolrInputDocument buildAtomicUpdate() {
        if (id == null){
            throw new IllegalArgumentException("Must specify document ID");
        }
        AtomicUpdateBuilder builder = AtomicUpdateBuilder.create().setId(BizListFieldType.ID, id);

        if (fileId != null){
            builder.addModifier(BizListFieldType.FILE_ID, SET, fileId);
        }

        if (aggMinCount != null) {
            builder.addModifier(BizListFieldType.AGG_MIN_COUNT, SET, aggMinCount);
        }

        if (bizListId != null) {
            builder.addModifier(BizListFieldType.BIZ_LIST_ID, SET, bizListId);
        }

        if (bizListItemSid != null) {
            builder.addModifier(BizListFieldType.BIZ_LIST_ITEM_SID, SET, bizListItemSid);
        }

        if (bizListItemType != null) {
            builder.addModifier(BizListFieldType.BIZ_LIST_ITEM_TYPE, SET, bizListItemType);
        }

        if (ruleName != null) {
            builder.addModifier(BizListFieldType.RULE_NAME, SET, ruleName);
        }

        if (dirty != null) {
            builder.addModifier(BizListFieldType.DIRTY, SET, dirty);
        }

        if (counter != null) {
            builder.addModifier(BizListFieldType.COUNTER, SET, counter);
        }

        return builder.build();
    }

    public SolrBizListBuilder setId(String id) {
        this.id = id;
        return this;
    }

    public SolrBizListBuilder setFileId(Long fileId) {
        this.fileId = fileId;
        return this;
    }

    public SolrBizListBuilder setBizListId(Long bizListId) {
        this.bizListId = bizListId;
        return this;
    }

    public SolrBizListBuilder setBizListItemType(Integer bizListItemType) {
        this.bizListItemType = bizListItemType;
        return this;
    }

    public SolrBizListBuilder setAggMinCount(Integer aggMinCount) {
        this.aggMinCount = aggMinCount;
        return this;
    }

    public SolrBizListBuilder setBizListItemSid(String bizListItemSid) {
        this.bizListItemSid = bizListItemSid;
        return this;
    }

    public SolrBizListBuilder setRuleName(String ruleName) {
        this.ruleName = ruleName;
        return this;
    }

    public SolrBizListBuilder setDirty(Boolean dirty) {
        this.dirty = dirty;
        return this;
    }

    public SolrBizListBuilder setCounter(Long counter) {
        this.counter = counter;
        return this;
    }
}
