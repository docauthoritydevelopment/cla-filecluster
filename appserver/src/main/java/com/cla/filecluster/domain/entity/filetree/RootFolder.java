package com.cla.filecluster.domain.entity.filetree;

import com.cla.common.domain.dto.file.DirectoryExistStatus;
import com.cla.common.domain.dto.filetree.*;
import com.cla.connector.domain.dto.media.MediaType;
import com.cla.filecluster.domain.entity.dataCenter.CustomerDataCenter;
import com.cla.filecluster.domain.entity.media.MediaConnectionDetails;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

/**
 * Mapped entity representing a root folder
 * Created by uri on 11/08/2015.
 */

@SuppressWarnings("serial")
@Entity
@Table(name = "root_folder")
public class RootFolder implements Serializable{

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Access(AccessType.PROPERTY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "doc_store_include")
    private DocStore docStoreInclude;

    @OneToMany(fetch = FetchType.LAZY,mappedBy = "rootFolder", cascade = CascadeType.REMOVE)
    private Set<FolderExcludeRule> folderExcludeRules;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "media_connection_details")
    private MediaConnectionDetails mediaConnectionDetails;

    private boolean rescanActive;

    private int baseDepth;

    private Integer foldersCount;

    private Integer filesCount;

    @Length(max=2048)
	//@Column(columnDefinition = "varchar(2048) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci") DAMAIN-1928
    private String path;

    @Length(max=2048)
    //@Column(columnDefinition = "varchar(2048) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci") DAMAIN-1928
    private String realPath;

    private String nickName;

    @Length(max=4096)
    private String description;

    private Long lastRunId;

    @Column(name = "file_types")
    private String mapFileTypes;

    private String ingestFileTypes;

    private String externalCrawlerJobId;

    @Enumerated(value = EnumType.STRING)
    private PathDescriptorType pathDescriptorType;

    private StoreLocation storeLocation;

    private StorePurpose storePurpose;

    private StoreSecurity storeSecurity;

    private StoreRescanMethod storeRescanMethod;

    private MediaType mediaType;

    private String mediaEntityId;	// optional unique id provided by media repository
    private CrawlerType crawlerType;

    private Integer scannedFilesCountCap;
    private Integer scannedMeaningfulFilesCountCap;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "customer_data_center_id")
    private CustomerDataCenter customerDataCenter;

    @Column(name = "reingest_time_stamp", columnDefinition="bigint(20) default '0'")
    private long reingestTimeStampMs;

    private Integer scanTaskDepth;

    @Enumerated(value = EnumType.ORDINAL)
    private DirectoryExistStatus isAccessible = DirectoryExistStatus.UNKNOWN;

    private Long accessabilityLastCngDate;

    private Boolean isFirstScan = true;

    private Long fromDateScanFilter;

    private String mailboxGroup;

    private Long departmentId;

    private Boolean ignoreAccessErrors;

    private boolean deleted = false;

    private boolean failureGettingSharePermission = false;

    public RootFolder() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public DocStore getDocStore() {
        return docStoreInclude;
    }

    public DocStore getDocStoreInclude() {
        return docStoreInclude;
    }

    public boolean isRescanActive() { return rescanActive; }

    public void setRescanActive(boolean rescanActive) { this.rescanActive = rescanActive; }

    public int getBaseDepth() {
        return baseDepth;
    }

    public void setBaseDepth(int baseDepth) {
        this.baseDepth = baseDepth;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public void setDocStore(DocStore docStore) {
        setDocStoreInclude(docStore);
    }

    public Integer getFoldersCount() {
        return foldersCount;
    }

    public void setFoldersCount(Integer foldersCount) {
        this.foldersCount = foldersCount;
    }

    public Integer getFilesCount() {
        return filesCount;
    }

    public void setFilesCount(Integer filesCount) {
        this.filesCount = filesCount;
    }

    public PathDescriptorType getPathDescriptorType() {
        return pathDescriptorType;
    }

    public void setPathDescriptorType(PathDescriptorType pathDescriptorType) {
        this.pathDescriptorType = pathDescriptorType;
    }

    public StoreLocation getStoreLocation() {
        return storeLocation;
    }

    public void setStoreLocation(StoreLocation storeLocation) {
        this.storeLocation = storeLocation;
    }

    public StorePurpose getStorePurpose() {
        return storePurpose;
    }

    public void setStorePurpose(StorePurpose storePurpose) {
        this.storePurpose = storePurpose;
    }

    public StoreSecurity getStoreSecurity() {
        return storeSecurity;
    }

    public void setStoreSecurity(StoreSecurity storeSecurity) {
        this.storeSecurity = storeSecurity;
    }

    public String getRealPath() {
        return realPath;
    }

    public void setRealPath(String realPath) {
        this.realPath = realPath;
    }


    public Set<FolderExcludeRule> getFolderExcludeRules() {
        return folderExcludeRules;
    }

    public void setFolderExcludeRules(Set<FolderExcludeRule> folderExcludeRules) {
        this.folderExcludeRules = folderExcludeRules;
    }

    public StoreRescanMethod getStoreRescanMethod() {
        return storeRescanMethod;
    }

    public void setStoreRescanMethod(StoreRescanMethod storeRescanMethod) {
        this.storeRescanMethod = storeRescanMethod;
    }


    public Long getLastRunId() {
        return lastRunId;
    }

    public void setLastRunId(Long lastRunId) {
        this.lastRunId = lastRunId;
    }

    public String getMapFileTypes() {
        return mapFileTypes;
    }

    public void setMapFileTypes(String mapFileTypes) {
        this.mapFileTypes = mapFileTypes;
    }

    public String getIngestFileTypes() {
        return ingestFileTypes;
    }

    public void setIngestFileTypes(String ingestFileTypes) {
        this.ingestFileTypes = ingestFileTypes;
    }

    public void setMediaType(MediaType mediaType) {
        this.mediaType = mediaType;
    }

    public MediaType getMediaType() {
        return mediaType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getExternalCrawlerJobId() {
        return externalCrawlerJobId;
    }
    public void setExternalCrawlerJobId(String externalCrawlerJobId) { this.externalCrawlerJobId = externalCrawlerJobId; }

    public String getMediaEntityId() { return mediaEntityId; }

    public void setMediaEntityId(String mediaEntityId) { this.mediaEntityId = mediaEntityId; }

    public int getScannedFilesCountCap() {
        return (scannedFilesCountCap==null)?-1:scannedFilesCountCap.intValue();
    }

    public void setScannedFilesCountCap(Integer scannedFilesCountCap) {
        this.scannedFilesCountCap = scannedFilesCountCap;
    }

    public int getScannedMeaningfulFilesCountCap() {
        return scannedMeaningfulFilesCountCap == null ? -1 : scannedMeaningfulFilesCountCap.intValue();
    }

    public void setScannedMeaningfulFilesCountCap(Integer scannedMeaningfulFilesCountCap) {
        this.scannedMeaningfulFilesCountCap = scannedMeaningfulFilesCountCap;
    }

    public void setCrawlerType(CrawlerType crawlerType) {
        this.crawlerType = crawlerType;
    }

    public CrawlerType getCrawlerType() {
        return crawlerType;
    }

    public MediaConnectionDetails getMediaConnectionDetails() {
        return mediaConnectionDetails;
    }

    public void setMediaConnectionDetails(MediaConnectionDetails mediaConnectionDetails) {
        this.mediaConnectionDetails = mediaConnectionDetails;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public Long getMediaConnectionDetailsId() {
        return (mediaConnectionDetails==null)?null:mediaConnectionDetails.getId();
    }
    public String getMediaConnectionName() {
        return (mediaConnectionDetails==null)?null:mediaConnectionDetails.getName();
    }

    private void setDocStoreInclude(DocStore docStoreInclude) {
        this.docStoreInclude = docStoreInclude;
    }


    public CustomerDataCenter getCustomerDataCenter() {
        return customerDataCenter;
    }

    public void setCustomerDataCenter(CustomerDataCenter customerDataCenter) {
        this.customerDataCenter = customerDataCenter;
    }

    public long getReingestTimeStampMs() {
        return reingestTimeStampMs;
    }

    public void setReingestTimeStampMs(long reingestTimeStampMs) {
        this.reingestTimeStampMs = reingestTimeStampMs;
    }

    public Integer getScanTaskDepth() {
        return scanTaskDepth;
    }

    public void setScanTaskDepth(Integer scanTaskDepth) {
        this.scanTaskDepth = scanTaskDepth;
    }

    public DirectoryExistStatus getAccessible() {
        return isAccessible;
    }

    public void setAccessible(DirectoryExistStatus accessible) {
        isAccessible = accessible;
    }

    public Long getAccessabilityLastCngDate() {
        return accessabilityLastCngDate;
    }

    public void setAccessabilityLastCngDate(Long accessabilityLastCngDate) {
        this.accessabilityLastCngDate = accessabilityLastCngDate;
    }

    public Boolean getFirstScan() {
        return isFirstScan;
    }

    public void setFirstScan(Boolean firstScan) {
        isFirstScan = firstScan;
    }

    public Long getFromDateScanFilter() {
        return fromDateScanFilter;
    }

    public void setFromDateScanFilter(Long fromDateScanFilter) {
        this.fromDateScanFilter = fromDateScanFilter;
    }

    public String getMailboxGroup() {
        return mailboxGroup;
    }

    public void setMailboxGroup(String mailboxGroup) {
        this.mailboxGroup = mailboxGroup;
    }

    public Long getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Long departmentId) {
        this.departmentId = departmentId;
    }

    public Boolean getIgnoreAccessErrors() {
        return ignoreAccessErrors;
    }

    public void setIgnoreAccessErrors(Boolean ignoreAccessErrors) {
        this.ignoreAccessErrors = ignoreAccessErrors;
    }

    public boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public boolean isFailureGettingSharePermission() {
        return failureGettingSharePermission;
    }

    public void setFailureGettingSharePermission(boolean failureGettingSharePermission) {
        this.failureGettingSharePermission = failureGettingSharePermission;
    }

    @Override
    public String toString() {
        return "RootFolder{" +
                "id=" + id +
                ", path='" + path + '\'' +
                ", rescanActive=" + rescanActive +
                ", crawler=" + crawlerType+
                ", mediaType=" + mediaType +
                '}';
    }
}

