package com.cla.filecluster.domain.entity.actions;

import com.cla.filecluster.domain.entity.BasicEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;

/**
 * A schedule for a specific action. the action will run at times specified by the schedule
 *
 * Created by: yael
 * Created on: 3/4/2018
 */
@Entity
@Table(	name = "action_schedule",
        indexes = {
                @Index(unique=true, name = "name_idx", columnList = "name")
        })
public class ActionSchedule extends BasicEntity {

    private String name;

    private long actionExecutionTaskStatusId;

    private String cronTriggerString;

    private String resultPath;

    private boolean active;

    @Column(length = 4096)
    private String schedulingJson;

    private long lastExecutionDate = 0L;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public long getActionExecutionTaskStatusId() {
        return actionExecutionTaskStatusId;
    }

    public void setActionExecutionTaskStatusId(long actionExecutionTaskStatusId) {
        this.actionExecutionTaskStatusId = actionExecutionTaskStatusId;
    }

    public String getCronTriggerString() {
        return cronTriggerString;
    }

    public void setCronTriggerString(String cronTriggerString) {
        this.cronTriggerString = cronTriggerString;
    }

    public String getResultPath() {
        return resultPath;
    }

    public void setResultPath(String resultPath) {
        this.resultPath = resultPath;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getSchedulingJson() {
        return schedulingJson;
    }

    public void setSchedulingJson(String schedulingJson) {
        this.schedulingJson = schedulingJson;
    }

    public long getLastExecutionDate() {
        return lastExecutionDate;
    }

    public void setLastExecutionDate(long lastExecutionDate) {
        this.lastExecutionDate = lastExecutionDate;
    }
}
