package com.cla.filecluster.domain.entity.schedule;

import com.cla.common.domain.dto.schedule.CrawlerPhaseBehavior;

import javax.persistence.*;
import java.util.List;

/**
 * Created by uri on 02/05/2016.
 */
@Entity
@Table(uniqueConstraints = {@UniqueConstraint(name = "name_constraint",columnNames = "name")})
public class ScheduleGroup {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private boolean active;

    /**
     * When the schedule group should run but missed it's turn, the timestamp is filled.
     * When the schedule group is run - the timestamp is empties
     */
    private Long missedScheduleTimeStamp;

    private String cronTriggerString;

    private String schedulingDescription;

    @Column(length = 4096)
    private String schedulingJson;

    private CrawlerPhaseBehavior analyzePhaseBehavior;

    @OneToMany(mappedBy="scheduleGroup" ,cascade = CascadeType.REMOVE)
    @OrderBy("number")
    private List<RootFolderSchedule> rootFolderSchedules;

    private long lastRunStartTime;

//    private RunStatus state;

    private long lastRunEndTime;

    private boolean resumeAfterRestart;

    private Long lastRunId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CrawlerPhaseBehavior getAnalyzePhaseBehavior() {
        return analyzePhaseBehavior;
    }

    public void setAnalyzePhaseBehavior(CrawlerPhaseBehavior analyzePhaseBehavior) {
        this.analyzePhaseBehavior = analyzePhaseBehavior;
    }

    public List<RootFolderSchedule> getRootFolderSchedules() {
        return rootFolderSchedules;
    }

    public void setRootFolderSchedules(List<RootFolderSchedule> rootFolderSchedules) {
        this.rootFolderSchedules = rootFolderSchedules;
    }

    public Long getMissedScheduleTimeStamp() {
        return missedScheduleTimeStamp;
    }

    public void setMissedScheduleTimeStamp(Long missedScheduleTimeStamp) {
        this.missedScheduleTimeStamp = missedScheduleTimeStamp;
    }

    public String getCronTriggerString() {
        return cronTriggerString;
    }

    public void setCronTriggerString(String cronTriggerString) {
        this.cronTriggerString = cronTriggerString;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getSchedulingDescription() {
        return schedulingDescription;
    }

    public void setSchedulingDescription(String schedulingDescription) {
        this.schedulingDescription = schedulingDescription;
    }

    public String getSchedulingJson() {
        return schedulingJson;
    }

    public void setSchedulingJson(String schedulingJson) {
        this.schedulingJson = schedulingJson;
    }

    @Override
    public String toString() {
        return "ScheduleGroup{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", active=" + active +
                ", analyzeBehavior=" + analyzePhaseBehavior+
                ", cronTriggerString='" + cronTriggerString + '\'' +
                '}';
    }

    public void setLastRunStartTime(long lastRunStartTime) {
        this.lastRunStartTime = lastRunStartTime;
    }

    public long getLastRunStartTime() {
        return lastRunStartTime;
    }

//    public void setState(RunStatus state) {
//        this.state = state;
//    }
//
//    public RunStatus getState() {
//        return state;
//    }

    public void setLastRunEndTime(long lastRunEndTime) {
        this.lastRunEndTime = lastRunEndTime;
    }

    public long getLastRunEndTime() {
        return lastRunEndTime;
    }

    public boolean isResumeAfterRestart() {
        return resumeAfterRestart;
    }

    public void setResumeAfterRestart(boolean resumeAfterRestart) {
        this.resumeAfterRestart = resumeAfterRestart;
    }

    public Long getLastRunId() {
        return lastRunId;
    }

    public void setLastRunId(Long lastRunId) {
        this.lastRunId = lastRunId;
    }

}
