package com.cla.filecluster.domain.entity.doctype;

import com.cla.common.domain.dto.scope.ScopedEntity;
import com.cla.filecluster.domain.entity.bizlist.BizList;
import com.cla.filecluster.domain.entity.filetag.AssociableEntity;
import com.cla.filecluster.domain.entity.filetag.FileTagSetting;
import com.cla.filecluster.domain.entity.scope.BasicScope;
import com.google.common.collect.Sets;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Set;

/**
 * Created by uri on 17/08/2015.
 */
@Entity
@Table(
        uniqueConstraints = {
                @UniqueConstraint(name = "name_par_acc", columnNames = {"name", "parent_doc_type_id"})
        }
        , indexes = {
        @Index(name = "doc_type_identifier_idx", columnList = "docTypeIdentifier")
}
)
public class DocType implements Serializable, AssociableEntity, ScopedEntity {

    @Id
    @Access(AccessType.PROPERTY)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull
    private String name;

    @NotNull
    private String uniqueName;

    private String alternativeName;

    private String description;

    private String docTypeIdentifier;

    @Column(name = "single_value")
    private boolean singleValue;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "parent_doc_type_id")
    private DocType parentDocType;

    @ManyToOne(fetch = FetchType.LAZY)
    private DocTypeTemplate template;

    @ManyToOne(fetch = FetchType.LAZY)
    private DocTypeRiskInformation docTypeRiskInformation;

    @ManyToOne(fetch = FetchType.LAZY)
    private BizList bizList;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "doc_type_scope_id")
    private BasicScope scope;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "docType", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<FileTagSetting> fileTagSettings = Sets.newHashSet();

    private boolean deleted;

    public DocType() {
    }

    public DocType(String name) {
        this.name = name;
    }

    public DocType(String name, DocTypeTemplate template) {
        this.name = name;
        this.template = template;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public DocType getParentDocType() {
        return parentDocType;
    }

    public void setParentDocType(DocType parentDocType) {
        this.parentDocType = parentDocType;
    }

    public DocTypeTemplate getTemplate() {
        return template;
    }

    public void setTemplate(DocTypeTemplate template) {
        this.template = template;
    }

    public DocTypeRiskInformation getDocTypeRiskInformation() {
        return docTypeRiskInformation;
    }

    public void setDocTypeRiskInformation(DocTypeRiskInformation docTypeRiskInformation) {
        this.docTypeRiskInformation = docTypeRiskInformation;
    }

    public void setBizList(BizList bizList) {
        this.bizList = bizList;
    }

    public BizList getBizList() {
        return bizList;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DocType docType = (DocType) o;

        return id == docType.id;

    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }

    public String getDocTypeIdentifier() {
        return docTypeIdentifier;
    }

    public String getDocTypeIdentifierAsRegex() {
        return docTypeIdentifier.replaceAll("\\.", "\\\\.");
    }

    public void setDocTypeIdentifier(String docTypeIdentifier) {
        this.docTypeIdentifier = docTypeIdentifier;
    }

    @Override
    public String toString() {
        return "DocType{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", docTypeIdentifier='" + docTypeIdentifier + '\'' +
                ", parentDocType=" + parentDocType +
                '}';
    }

    public String getUniqueName() {
        return uniqueName;
    }

    public void setUniqueName(String uniqueName) {
        this.uniqueName = uniqueName;
    }

    public String getAlternativeName() {
        return alternativeName;
    }

    public void setAlternativeName(String alternativeName) {
        this.alternativeName = alternativeName;
    }

    @Override
    public String createIdentifierString() {
        return getDocTypeIdentifier();
    }

    public boolean isSingleValue() {
        return singleValue;
    }

    public void setSingleValue(boolean singleValue) {
        this.singleValue = singleValue;
    }

    public BasicScope getScope() {
        return scope;
    }

    public void setScope(BasicScope scope) {
        this.scope = scope;
    }

    public Set<FileTagSetting> getFileTagSettings() {
        return fileTagSettings;
    }

    public void setFileTagSettings(Set<FileTagSetting> fileTagSettings) {
        this.fileTagSettings = fileTagSettings;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
}
