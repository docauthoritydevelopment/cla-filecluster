package com.cla.filecluster.domain.entity.audit;

import java.io.Serializable;

/**
 * Created by uri on 23/02/2016.
 */
public class AuditField implements Serializable {

    String field;

    String value;

    public static AuditField build(String field, String value) {
        return new AuditField(field,value);
    }
    public AuditField(String field, String value) {
        this.field = field;
        this.value = value;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
