package com.cla.filecluster.domain.entity.security;

import com.google.common.collect.Sets;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by Itai Marko.
 */
@Entity
@Table(name = "ldap_group_mapping",
        indexes = {
                @Index(name = "name_idx", columnList = "groupName")
        })
public class LdapGroupMapping {

    @Id
    @GeneratedValue
    private Long id;

    private String groupName;

    private String groupDN;

    @ManyToMany(fetch = FetchType.EAGER)
    private Set<CompositeRole> daRoles = Sets.newHashSet();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getGroupDN() {
        return groupDN;
    }

    public void setGroupDN(String groupDN) {
        this.groupDN = groupDN;
    }

    public Set<CompositeRole> getDaRoles() {
        return daRoles;
    }

}
