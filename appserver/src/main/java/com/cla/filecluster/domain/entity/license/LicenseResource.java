package com.cla.filecluster.domain.entity.license;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Created by uri on 27/06/2016.
 */
@Entity
public class LicenseResource {

    @Id
    private String id;

    private String value;

    private long issueTime;

    private String signature;

    public LicenseResource() {
    }

    public LicenseResource(String id, String value,long issueTime) {
        this.id = id;
        this.value = value;
        this.issueTime = issueTime;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    @Override
    public String toString() {
        return "LicenseResource{" +
                "id='" + id + '\'' +
                ", value='" + value + '\'' +
                '}';
    }

    public long getIssueTime() {
        return issueTime;
    }

    public void setIssueTime(long issueTime) {
        this.issueTime = issueTime;
    }
}
