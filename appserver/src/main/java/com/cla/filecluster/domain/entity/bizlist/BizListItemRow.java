package com.cla.filecluster.domain.entity.bizlist;

import com.google.common.collect.Sets;
import org.apache.commons.lang.StringUtils;

import java.util.Set;

/**
 * Created by uri on 23/08/2015.
 */
public class BizListItemRow {
    private String clearId;
    private String clearName;
    private String[] fieldValues;

    private Set<String> aliases;

    public BizListItemRow(String clearId, String clearName) {
        this.clearId = clearId;
        this.clearName = clearName;
    }

    public BizListItemRow() {
    }

    public void setClearId(String clearId) {
        this.clearId = clearId;
    }

    public String getClearId() {
        return clearId;
    }

    public void setClearName(String clearName) {
        this.clearName = clearName;
    }

    public String getClearName() {
        return clearName;
    }

    public void setFieldValues(String[] fieldValues) {
        this.fieldValues = fieldValues;
        this.aliases = Sets.newHashSet();
        for (String fieldValue : fieldValues) {
            if (StringUtils.isEmpty(fieldValue)) {
                continue;
            }
            aliases.add(fieldValue);
        }
    }

    public void setFieldValuesOnly(String[] fieldValues) {
        this.fieldValues = fieldValues;
    }

    public Set<String> getAliasValues() {
        return aliases;
    }
    public String[] getFieldValues() {
        return fieldValues;
    }

    @Override
    public String toString() {
        return "BizListItemRow{" +
                "clearId='" + clearId + '\'' +
                ", clearName='" + clearName + '\'' +
                '}';
    }
}
