package com.cla.filecluster.domain.specification;

import com.cla.common.constants.CatFoldersFieldType;
import com.cla.common.domain.dto.kendo.FilterDescriptor;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.connector.utils.Pair;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.repository.solr.query.Criteria;
import com.cla.filecluster.repository.solr.query.Query;
import com.cla.filecluster.repository.solr.query.SolrOperator;
import com.cla.filecluster.repository.solr.query.SolrQueryGenerator;
import com.cla.filecluster.service.convertion.FileDtoFieldConverter;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.cla.connector.constants.KendoFilterConstants.*;

/**
 *
 * Created by uri on 08/07/2015.
 */
public abstract class KendoFilterSpecification<T> implements Specification<T> {

    final Class<T> typeParameterClass;
    final FilterDescriptor filter;

    protected KendoFilterSpecification(Class<T> typeParameterClass, FilterDescriptor filter) {
        this.typeParameterClass = typeParameterClass;
        this.filter = filter;
    }

    protected Query restrict(FilterDescriptor filter) {
        String operator = filter.getOptionalOperator().orElse(NONE);
        String dtoField= filter.getField();
        boolean ignoreCase = filter.isIgnoreCase();
        Object value = filter.getValue();

        //Parse the field value
        String modelField = convertModelField(dtoField);
        CatFoldersFieldType solrField = convertField(dtoField);
        value = parseFilterValue(modelField, value);

        //Handle special cases
        operator = convertOperatorIfNeeded(dtoField,value,operator);

        Pair<SolrOperator, Object> result = getSolrOperatorAndValidate(operator, value);
        SolrOperator op = result.getKey();
        value = result.getValue();

        //Handle date
        if (isDateField(dtoField)) {
            Date date = convertValueToDate(value);
            return Query.create().addFilterWithCriteria(Criteria.create(solrField, op, date));
        }
        else if (ignoreCase && value instanceof String) { //Handle case
            value = ((String) value).toLowerCase();
        }
        return Query.create().addFilterWithCriteria(Criteria.create(solrField, op, value));
    }

    protected Predicate restrict(FilterDescriptor filter, Root<T> root , CriteriaQuery query, CriteriaBuilder cb) {
        String operator = filter.getOptionalOperator().orElse(NONE);
        String dtoField= filter.getField();
        boolean ignoreCase = filter.isIgnoreCase();
        Object value = filter.getValue();

        //Parse the field value
        String modelField = convertModelField(dtoField);
        value = parseFilterValue(modelField, value);
        //Convert field if needed
        Expression fieldPath = convertModelField(root, dtoField);
        //Handle special cases
        operator = convertOperatorIfNeeded(dtoField,value,operator);
        //Handle date
        if (isDateField(dtoField)) {
            fieldPath = fieldPath.as(Date.class);
            Date date = convertValueToDate(value);
            return getConcreteDatePredicate(cb, operator, date, fieldPath);
        }
        else if (ignoreCase && value instanceof String) { //Handle case
            fieldPath = cb.lower(fieldPath);
            value = ((String) value).toLowerCase();
        }
        return getConcretePredicate(cb, operator, value, fieldPath);
    }

    private Date convertValueToDate(Object value) {
        //2000-07-08t21:00:00.000z
        try {
            return DateFormat.getDateTimeInstance().parse(value.toString());
        } catch (ParseException e) {
            throw new BadRequestException("filter (dateformat)",value.toString(),e, BadRequestType.UNSUPPORTED_VALUE);
        }
    }

    protected abstract boolean isDateField(String dtoField);

    protected abstract String convertModelField(String dtoField);

    protected CatFoldersFieldType convertField(String dtoField) {
        return FileDtoFieldConverter.convertToCatFolderField(dtoField);
    }

    protected Expression convertModelField(Root<T> root, String dtoField) {
        String modelField = convertModelField(dtoField);
        if (modelField.contains(".")) {
            String[] modelPath = modelField.split("\\.");
            //Extract the root
            Path<Object> objectPath = root.get(modelPath[0]);
            //Walk the path
            for (int i=1; i< modelPath.length; i++) {
                objectPath = objectPath.get(modelPath[i]);
            }
            return objectPath;
        }
        else {
            return root.get(modelField);
        }
    }

    private Predicate getConcreteDatePredicate(CriteriaBuilder cb, String operator, Date value, Expression fieldPath) {
        switch(operator) {
            case GT:
                return cb.greaterThan(fieldPath,value);
            case GTE:
                return cb.greaterThanOrEqualTo(fieldPath, value);
            case LT:
                return cb.lessThan(fieldPath, value);
            case LTE:
                return cb.lessThanOrEqualTo(fieldPath, value);
        }
        return cb.disjunction();
    }

    private Pair<SolrOperator, Object> getSolrOperatorAndValidate(String operator, Object value) {
        switch(operator) {
            case EQ:
                return Pair.of(SolrOperator.EQ, value);
            case NEQ:
                return Pair.of(SolrOperator.NE, value);
            case STARTSWITH:
                return Pair.of(SolrOperator.EQ, SolrQueryGenerator.ALL + value.toString());
            case ENDSWITH:
                return Pair.of(SolrOperator.EQ, value.toString() + SolrQueryGenerator.ALL);
            case CONTAINS:
                return Pair.of(SolrOperator.EQ, SolrQueryGenerator.ALL + value.toString() + SolrQueryGenerator.ALL);
            case DOESNOTCONTAIN:
                Pair.of(SolrOperator.NE, SolrQueryGenerator.ALL + value.toString() + SolrQueryGenerator.ALL);
            case GT:
                verifyNumber(value);
                return Pair.of(SolrOperator.GT, value);
            case GTE:
                verifyNumber(value);
                return Pair.of(SolrOperator.GTE, value);
            case LT:
                verifyNumber(value);
                return Pair.of(SolrOperator.LT, value);
            case LTE:
                verifyNumber(value);
                return Pair.of(SolrOperator.LTE, value);
            case NONE:
                return null;
        }
        return null;
    }

    private Predicate getConcretePredicate(CriteriaBuilder cb, String operator, Object value, Expression fieldPath) {
//        logger.debug("Get concrete predicate op={} val={}",operator,value);
        switch(operator) {
            case EQ:
                if (value instanceof String) {
                    return cb.like(fieldPath,value.toString());
                } else {
                    return cb.equal(fieldPath,value);
                }
            case NEQ:
                if (value instanceof String) {
                    return cb.notLike(fieldPath,value.toString());
                }
                else {
                    return cb.notEqual(fieldPath,value);
                }
            case STARTSWITH:
                return cb.like(fieldPath, value.toString() + "%");
            case ENDSWITH:
                return cb.like(fieldPath,"%"+value.toString());
            case CONTAINS:
                return cb.like(fieldPath,"%"+value.toString()+"%");
            case DOESNOTCONTAIN:
                return cb.notLike(fieldPath, "%" + value.toString() + "%");
            case GT:
                return cb.gt(fieldPath,verifyNumber(value));
            case GTE:
                return cb.ge(fieldPath, verifyNumber(value));
            case LT:
                return cb.lt(fieldPath, verifyNumber(value));
            case LTE:
                return cb.le(fieldPath, verifyNumber(value));
            case NONE:
                return cb.disjunction();

        }
        //Unsupported filter
        return cb.disjunction();
    }

    private Number verifyNumber(Object value) {
        if (value instanceof Number) {
            return (Number) value;
        }
        throw new BadRequestException("filter","Illegal number: ["+value.toString()+"]", BadRequestType.UNSUPPORTED_VALUE);
    }

    protected Query buildFilter(FilterDescriptor subFilter) {
        List<FilterDescriptor> filters = subFilter.getFilters();
        if (filters != null && !filters.isEmpty()) {
            final List<Query> predicates = new ArrayList<>();
            for(FilterDescriptor entry : filters) {
                Query predicate;
                if (!entry.getFilters().isEmpty()) {
                    predicate = buildFilter(entry);
                } else {
                    predicate = restrict(entry);
                }
                predicates.add(predicate);
            }
            if (subFilter.getFilters().size() == 1) {
                return predicates.get(0);
            }
            if (subFilter.getLogic().toString().equals("or")) {
                return Query.create().addFilterQuery(predicates);
            }
            else {
                return Query.create().addFilterQuery(predicates); // TODO support AND ?????
            }
        }
        else if (filter.getField() != null) {
            return restrict(filter);
        }
        else {
            //no sub filters
            return null;
        }
    }

    protected Predicate buildFilter(FilterDescriptor subFilter, Root<T> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
        List<FilterDescriptor> filters = subFilter.getFilters();
        if (filters != null && !filters.isEmpty()) {
            final List<Predicate> predicates = new ArrayList<>();
            for(FilterDescriptor entry : filters) {
                Predicate predicate;
                if (!entry.getFilters().isEmpty()) {
                    predicate = buildFilter(entry, root, query, cb);
                } else {
                    predicate = restrict(entry,root,query, cb);
                }
                predicates.add(predicate);
            }
            if (subFilter.getFilters().size() == 1) {
                return predicates.get(0);
            }
            Predicate[] predicateArray = predicates.toArray(new Predicate[predicates.size()]);
            if (subFilter.getLogic().toString().equals("or")) {
                return cb.or(predicateArray);
            }
            else {
                return cb.and(predicateArray);
            }
        }
        else if (filter.getField() != null) {
            return restrict(filter,root,query, cb);
        }
        else {
            //no sub filters
            return cb.disjunction();
        }
    }

    /**
     * Allows us to convert operators that have no meaning to operators that does - in specific cases
     * @param dtoField
     * @param value
     * @param operator
     * @return
     */
    protected String convertOperatorIfNeeded(String dtoField, Object value, String operator) {
        return operator;
    }

    protected Object parseFilterValue(String field, Object value) {
        try {
            Class<?> type = new PropertyDescriptor(field, typeParameterClass).getPropertyType();
            if (type == double.class || type == Double.class) {
                value = Double.parseDouble(value.toString());
            } else if (type == float.class || type == Float.class) {
                value = Float.parseFloat(value.toString());
            } else if (type == long.class || type == Long.class) {
                value = Long.parseLong(value.toString());
            } else if (type == int.class || type == Integer.class) {
                value = Integer.parseInt(value.toString());
            } else if (type == short.class || type == Short.class) {
                value = Short.parseShort(value.toString());
            } else if (type == boolean.class || type == Boolean.class) {
                value = Boolean.parseBoolean(value.toString());
            }
        }catch (IntrospectionException e) {
        }catch(NumberFormatException nfe) {
        }
        return value;
    }

}
