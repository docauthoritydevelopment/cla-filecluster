package com.cla.filecluster.domain.specification;

import com.cla.common.domain.dto.kendo.FilterDescriptor;
import com.cla.filecluster.domain.entity.filetree.RootFolder;
import com.google.common.base.Strings;
import org.apache.commons.collections.CollectionUtils;

import javax.persistence.criteria.*;

/**
 * Created by uri on 21/03/2016.
 */
public class RootFolderSpecification  extends KendoFilterSpecification<RootFolder> {

    private final long docStoreId;
    private String namingSearchTerm;
    private boolean hideDeleted = true;

    public RootFolderSpecification(long docStoreId, FilterDescriptor filter) {
        super(RootFolder.class, filter);
        this.docStoreId = docStoreId;
    }

    public RootFolderSpecification(long docStoreId, FilterDescriptor filter, String namingSearchTerm, boolean hideDeleted) {
        super(RootFolder.class, filter);
        this.docStoreId = docStoreId;
        this.namingSearchTerm = namingSearchTerm;
        this.hideDeleted = hideDeleted;
    }

    @Override
    protected boolean isDateField(String dtoField) {
        return false;
    }

    @Override
    protected String convertModelField(String dtoField) {
        if (dtoField.equalsIgnoreCase("numberOfFoldersFound")) {
            return "foldersCount";
        }
        return dtoField;
    }

    @Override
    public Predicate toPredicate(Root<RootFolder> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
        //This part allow to use this specification in pageable queries
        Class clazz = query.getResultType();
        if (clazz.equals(RootFolder.class)) {
            //Prevent N+1 problem. see http://jdpgrailsdev.github.io/blog/2014/09/09/spring_data_hibernate_join.html
            root.fetch("docStoreInclude", JoinType.LEFT);
        }

        Path<Object> docStoreId = root.get("docStoreInclude").get("id");
        Predicate basePredicate = cb.equal(docStoreId, docStoreId);

        if (hideDeleted) {
            Predicate nonDelCond = cb.equal(root.get("deleted"), false);
            basePredicate = cb.and(basePredicate, nonDelCond);
        }

        if (!Strings.isNullOrEmpty(namingSearchTerm)) {
            Predicate conditionMain = cb.like(cb.lower(root.get("nickName")), "%" + namingSearchTerm + "%");
            Predicate condition = cb.like(cb.lower(root.get("path")), "%" + namingSearchTerm + "%");
            conditionMain = cb.or(conditionMain, condition);
            condition = cb.like(cb.lower(root.get("realPath")), "%" + namingSearchTerm + "%");
            conditionMain = cb.or(conditionMain, condition);
            condition = cb.like(cb.lower(root.get("description")), "%" + namingSearchTerm + "%");
            conditionMain = cb.or(conditionMain, condition);
            condition = cb.like(cb.lower(root.get("mailboxGroup")), "%" + namingSearchTerm + "%");
            conditionMain = cb.or(conditionMain, condition);
            basePredicate = cb.and(basePredicate, conditionMain);
        }

        if (filter == null || (CollectionUtils.isEmpty(filter.getFilters())) && filter.getField() == null) {
            //Return only the group filter
            return basePredicate;
        }
        else {
            Predicate filterPredicate = buildFilter(this.filter, root, query, cb);
            return cb.and(basePredicate, filterPredicate);
        }
    }
}
