package com.cla.filecluster.domain.entity.categorization;

import javax.persistence.*;

import com.cla.connector.domain.dto.JsonSerislizableVisible;

import java.util.List;

@Entity
@Table(name = "category_filters")
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="filter_type", discriminatorType = DiscriminatorType.STRING)
public abstract class CategoryFilter implements JsonSerislizableVisible {

	@Id
	@GeneratedValue
	private Integer id;

	private String description;

    private String value;

    @Column(name = "f_values")
    @ElementCollection(targetClass = String.class)
    @CollectionTable(name = "cat_fil_val",joinColumns = @JoinColumn(name="cat_filter"))
    private List<String> values;

    public Integer getId() {
        return id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public List<String> getValues() {
        return values;
    }

    public void setValues(List<String> values) {
        this.values = values;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
