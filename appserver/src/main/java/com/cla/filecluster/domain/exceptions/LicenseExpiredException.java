package com.cla.filecluster.domain.exceptions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by uri on 28/06/2016.
 */
@ResponseStatus(value= HttpStatus.FORBIDDEN, reason="Bad request")
public class LicenseExpiredException extends RuntimeException {

    Logger logger = LoggerFactory.getLogger(LicenseExpiredException.class);

    public LicenseExpiredException(String reason) {
        super("License Expired - ["+reason+"]");
        logger.warn(this.getMessage());
    }
}
