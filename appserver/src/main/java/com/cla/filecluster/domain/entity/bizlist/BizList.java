package com.cla.filecluster.domain.entity.bizlist;

import com.cla.common.domain.dto.bizlist.BizListItemType;
import com.cla.common.domain.dto.bizlist.EntityListEncryptionType;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 *
 * Created by uri on 20/08/2015.
 */
@Entity
@Table(uniqueConstraints = {@UniqueConstraint(name = "name", columnNames = {"name"})})
public class BizList {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Access(AccessType.PROPERTY)
    private long id;

    @NotNull
    private String name;

    private String description;

    private EntityListEncryptionType entityListEncryptionType;

    private String bizListSource;

    private Long creationTimeStamp;

    private Long lastUpdateTimeStamp;

    private Long lastSuccessfulRunDate;

    private String externalId;

    private String rulesFileName;

    private String bizListSolrId;

    private String template;

    private Boolean active;

    @NotNull
    private BizListItemType bizListItemType;

    @Transient
    private BizListSchema bizListSchema;

    private long extractionSessionId;

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBizListSource() {
        return bizListSource;
    }

    public void setBizListSource(String bizListSource) {
        this.bizListSource = bizListSource;
    }

    public BizListItemType getBizListItemType() {
        return bizListItemType;
    }

    public void setBizListItemType(BizListItemType bizListItemType) {
        this.bizListItemType = bizListItemType;
    }

    public BizListSchema getBizListSchema() {
        return bizListSchema;
    }

    public void setBizListSchema(BizListSchema bizListSchema) {
        this.bizListSchema = bizListSchema;
    }

    public EntityListEncryptionType getEntityListEncryptionType() {
        return entityListEncryptionType;
    }

    public void setEntityListEncryptionType(EntityListEncryptionType entityListEncryptionType) {
        this.entityListEncryptionType = entityListEncryptionType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "BizList{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", active='" + active + '\'' +
                ", bizListItemType=" + bizListItemType +
                '}';
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public Long getCreationTimeStamp() {
        return creationTimeStamp;
    }

    public void setCreationTimeStamp(Long creationTimeStamp) {
        this.creationTimeStamp = creationTimeStamp;
    }

    public Long getLastUpdateTimeStamp() {
        return lastUpdateTimeStamp;
    }

    public void setLastUpdateTimeStamp(Long lastUpdateTimeStamp) {
        this.lastUpdateTimeStamp = lastUpdateTimeStamp;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setExtractionSessionId(long extractionSessionId) {
        this.extractionSessionId = extractionSessionId;
    }

    public long getExtractionSessionId() {
        return extractionSessionId;
    }

    public String getRulesFileName() {
        return rulesFileName;
    }

    public void setRulesFileName(String rulesFileName) {
        this.rulesFileName = rulesFileName;
    }

    public String getBizListSolrId() {
        return bizListSolrId;
    }

    public void setBizListSolrId(String bizListSolrId) {
        this.bizListSolrId = bizListSolrId;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public Long getLastSuccessfulRunDate() { return lastSuccessfulRunDate; }

    public void setLastSuccessfulRunDate(Long lastSuccessfulRunDate) { this.lastSuccessfulRunDate = lastSuccessfulRunDate; }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
}
