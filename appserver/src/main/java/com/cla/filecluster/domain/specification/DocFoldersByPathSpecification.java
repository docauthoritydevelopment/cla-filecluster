package com.cla.filecluster.domain.specification;

import com.cla.common.constants.CatFoldersFieldType;
import com.cla.common.domain.dto.kendo.FilterDescriptor;
import com.cla.filecluster.domain.entity.filetree.DocFolder;
import com.cla.filecluster.repository.solr.query.Criteria;
import com.cla.filecluster.repository.solr.query.Query;
import com.cla.filecluster.repository.solr.query.SolrOperator;
import com.cla.filecluster.repository.solr.query.SolrQueryGenerator;
import com.cla.filecluster.service.convertion.FileDtoFieldConverter;
import org.apache.commons.collections.CollectionUtils;

import javax.persistence.criteria.*;

/**
 * Created by uri on 09/07/2015.
 */
public class DocFoldersByPathSpecification extends KendoFilterSpecification<DocFolder> {

    private DocFolder baseFolder;
    private Integer maxDepth;
    private Integer minDepth;
    boolean includeTagFolderOrigin;

    public DocFoldersByPathSpecification(FilterDescriptor filter, DocFolder baseFolder) {
        super(DocFolder.class, filter);
        this.baseFolder = baseFolder;
        includeTagFolderOrigin = false;
    }

    public Query toQuery() {
        Query base = Query.create();

        String pattern = baseFolder.getDepthFromRoot() + "." + baseFolder.getId();
        Query parentsInfoFilter = Query.create().addFilterWithCriteria(
                Criteria.create(CatFoldersFieldType.PARENTS_INFO, SolrOperator.EQ, pattern));
        base.addFilterQuery(parentsInfoFilter);

        Query rootFolderFilter = createRootFolderFilter();
        base.addFilterQuery(rootFolderFilter);

        Query skipSelf = Query.create().addFilterWithCriteria(Criteria.create(CatFoldersFieldType.ID, SolrOperator.NE, baseFolder.getId()));
        base.addFilterQuery(skipSelf);

        if (maxDepth != null) {
            Query depth = Query.create().addFilterWithCriteria(Criteria.create(CatFoldersFieldType.DEPTH_FROM_ROOT, SolrOperator.LTE, maxDepth));
            base.addFilterQuery(depth);
        }
        if (minDepth != null) {
            Query depth = Query.create().addFilterWithCriteria(Criteria.create(CatFoldersFieldType.DEPTH_FROM_ROOT, SolrOperator.GTE, minDepth));
            base.addFilterQuery(depth);
        }
        if (filter == null || CollectionUtils.isEmpty(filter.getFilters())) {
            //Return only the group filter
            return base;
        }
        else {
            Query filterPredicate = buildFilter(this.filter);
            base.addFilterQuery(filterPredicate); // TODO support AND ???
            return base;
        }
    }

    @Override
    public Predicate toPredicate(Root<DocFolder> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
        //This part allow to use this specification in pageable queries
        Class clazz = query.getResultType();
        if (clazz.equals(DocFolder.class)) {
            //Prevent N+1 problem. see http://jdpgrailsdev.github.io/blog/2014/09/09/spring_data_hibernate_join.html

            /* TODO do we use this via ui ? do we need to find a way to support in solr ?
            root.fetch("parentFolder", JoinType.LEFT);
            root.fetch("docStore", JoinType.LEFT);
            Fetch<Object, Object> associations = root.fetch("associations", JoinType.LEFT);
            if (includeTagFolderOrigin) {
                associations.fetch("originFolder",JoinType.LEFT);
            }
            Fetch<Object, Object> docTypeAssociations = root.fetch("docTypeAssociations", JoinType.LEFT);
            if (includeTagFolderOrigin) {
                docTypeAssociations.fetch("originFolder",JoinType.LEFT);
            }
             */
        }
        String baseFolderPath = baseFolder.getPath();
        //where docFolder.rootFolder = :rootFolder and docFolder.path like 'basePath/%'
        Path<String> docFolderPath= root.get("path");
        Path<Long> docFolderId = root.get("id");
        String pattern = baseFolderPath + "%";
        Predicate pathFilter = cb.like(docFolderPath, pattern);
        Predicate rootFolderFilter = createRootFolderFilter(root, query, cb);
        Predicate skipSelf = cb.notEqual(docFolderId,baseFolder.getId());
        Predicate basePredicate = cb.and(pathFilter, rootFolderFilter, skipSelf);
        if (maxDepth != null) {
            Path<Integer> depthFromRoot= root.get("depthFromRoot");
            Predicate maxDepthPredicate = cb.lessThanOrEqualTo(depthFromRoot,maxDepth);
            basePredicate = cb.and(basePredicate , maxDepthPredicate);
        }
        if (minDepth != null) {
            Path<Integer> depthFromRoot= root.get("depthFromRoot");
            Predicate minDepthPredicate = cb.greaterThanOrEqualTo(depthFromRoot,minDepth);
            basePredicate = cb.and(basePredicate , minDepthPredicate);

        }
        if (filter == null || CollectionUtils.isEmpty(filter.getFilters())) {
            //Return only the group filter
            return basePredicate;
        }
        else {
            Predicate filterPredicate = buildFilter(this.filter, root, query, cb);
            return cb.and(basePredicate, filterPredicate);
        }
    }

    private Predicate createRootFolderFilter(Root<DocFolder> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
        if (baseFolder.getRootFolderId() == null) {
            return cb.isNull(root.get("rootFolderId"));
        }
        return cb.equal(root.get("rootFolderId"), baseFolder.getRootFolderId());
    }

    private Query createRootFolderFilter() {
        if (baseFolder.getRootFolderId() == null) {
            return Query.create().addFilterWithCriteria(Criteria.create(CatFoldersFieldType.ROOT_FOLDER_ID, SolrOperator.MISSING, ""));
        }
        return Query.create().addFilterWithCriteria(Criteria.create(CatFoldersFieldType.ROOT_FOLDER_ID, SolrOperator.EQ, baseFolder.getRootFolderId()));
    }

    protected String convertModelField(String dtoField) {
        return FileDtoFieldConverter.convertDtoField(dtoField);
    }

    @Override
    protected boolean isDateField(String dtoField) {
        return FileDtoFieldConverter.isDateField(dtoField);
    }

    public void setMaxDepth(Integer maxDepth) {
        this.maxDepth = maxDepth;
    }

    public void setIncludeTagFolderOrigin(boolean includeTagFolderOrigin) {
        this.includeTagFolderOrigin = includeTagFolderOrigin;
    }

    public Integer getMinDepth() {
        return minDepth;
    }

    public void setMinDepth(Integer minDepth) {
        this.minDepth = minDepth;
    }
}
