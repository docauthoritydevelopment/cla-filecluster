package com.cla.filecluster.domain.entity.categorization;

import java.util.Arrays;
import java.util.List;

import javax.persistence.DiscriminatorValue;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("MGCF")
public class MultiGroupCategoryFilter extends CategoryFilter {

    public MultiGroupCategoryFilter(String... groupIds) {
        List<String> values = Arrays.asList(groupIds);
        setValues(values);
    }

    public MultiGroupCategoryFilter(List<String> groupIds) {
        setValues(groupIds);
    }
}
