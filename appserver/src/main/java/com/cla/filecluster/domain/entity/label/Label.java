package com.cla.filecluster.domain.entity.label;

import com.cla.filecluster.domain.entity.BasicEntity;
import lombok.*;
import lombok.experimental.Accessors;

import javax.persistence.Entity;
import javax.persistence.Table;

@NoArgsConstructor
@Getter
@Setter
@Accessors(chain = true)
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "labels")
public class Label extends BasicEntity {
	private String name;
	private String description;
}
