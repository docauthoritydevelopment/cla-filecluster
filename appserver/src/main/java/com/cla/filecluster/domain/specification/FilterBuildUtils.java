package com.cla.filecluster.domain.specification;

import com.cla.connector.constants.KendoFilterConstants;
import com.cla.filecluster.service.convertion.FileDtoFieldConverter;
import com.cla.common.domain.dto.kendo.FilterDescriptor;
import com.cla.filecluster.domain.entity.filetag.FileTag;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by uri on 11/10/2015.
 */
public final class FilterBuildUtils {

    public static FilterDescriptor createFileTagsFilterDescriptor(List<FileTag> fileTags) {
        FilterDescriptor baseFilter = new FilterDescriptor();
        List<FilterDescriptor> subFilters = new ArrayList<>();
        for (FileTag fileTag : fileTags) {
            subFilters.add(new FilterDescriptor(FileDtoFieldConverter.DtoFields.tags.name(),fileTag.getId(), KendoFilterConstants.EQ));
        }
        baseFilter.setLogic("or");
        baseFilter.setFilters(subFilters);
        return baseFilter;
    }

    public static FilterDescriptor createAndFilter(FilterDescriptor... filterDescriptors) {
        List<FilterDescriptor> validFilters = new ArrayList<>();
        for (FilterDescriptor filterDescriptor : filterDescriptors) {
            if (filterDescriptor != null) {
                validFilters.add(filterDescriptor);
            }
        }
        if (validFilters.size() == 0) {
            return null;
        }
        if (validFilters.size() == 1) {
            return validFilters.get(0);
        }
        FilterDescriptor baseFilter = new FilterDescriptor();
        baseFilter.setLogic("and");
        baseFilter.setFilters(validFilters);
        return baseFilter;
    }

    public static FilterDescriptor createAccountFilter() {
        return new FilterDescriptor();
    }
}
