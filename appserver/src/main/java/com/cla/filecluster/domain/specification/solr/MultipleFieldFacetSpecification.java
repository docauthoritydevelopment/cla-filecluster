package com.cla.filecluster.domain.specification.solr;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.domain.dto.kendo.FilterDescriptor;

import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by shtand on 09/09/2015.
 */
public class MultipleFieldFacetSpecification extends SolrSpecification {

    private String[] facetFields;


    public MultipleFieldFacetSpecification(FilterDescriptor filterDescriptor, CatFileFieldType[] facetFields) {
        super(filterDescriptor);
        this.facetFields = Stream.of(facetFields).map(cfft -> cfft.getSolrName()).collect(Collectors.toList()).toArray(new String[]{});
    }

    @Override
    public String[] getFacetFields() {
        return facetFields;
    }


}
