package com.cla.filecluster.domain.entity.date;

import javax.persistence.*;

/**
 * Created by uri on 05/06/2016.
 */
@Entity
public class SimpleDateRangeItem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "type", column = @Column(name = "start_type"))
            , @AttributeOverride(name = "relativePeriod", column = @Column(name = "start_relative_period"))
            , @AttributeOverride(name = "relativePeriodAmount", column = @Column(name = "start_relative_period_amount"))
            , @AttributeOverride(name = "absoluteTime", column = @Column(name = "start_absolute_time"))
    })
    private DateRangePoint start;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "type", column = @Column(name = "end_type"))
            , @AttributeOverride(name = "relativePeriod", column = @Column(name = "end_relative_period"))
            , @AttributeOverride(name = "relativePeriodAmount", column = @Column(name = "end_relative_period_amount"))
            , @AttributeOverride(name = "absoluteTime", column = @Column(name = "end_absolute_time"))
    })
    private DateRangePoint end;

    private Long orderValue;

    public SimpleDateRangeItem() {
    }
    public SimpleDateRangeItem(Long orderValue) {
        this.orderValue = orderValue;
    }

    public Long getOrderValue() { return orderValue; }
    public void setOrderValue(Long orderValue) { this.orderValue = orderValue; }

    @Transient
    public Long getOrderValueNotNull() {
        return orderValue == null ? 0 : orderValue;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DateRangePoint getStart() {
        return isStartTimeNull()?null:start;
    }

    public void setStart(DateRangePoint start) {
        this.start = start;
    }

    public DateRangePoint getEnd() {
        return isEndTimeNull()?null:end;
    }

    public void setEnd(DateRangePoint end) {
        this.end = end;
    }

    public boolean isStartTimeNull() { return (start == null || start.getType() == null); }
    public boolean isEndTimeNull() { return (end == null || end.getType() == null); }

    @Override
    public String toString() {
        return "SimpleDateRangeItem{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", start=" + start +
                ", end=" + end +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SimpleDateRangeItem that = (SimpleDateRangeItem) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        return name != null ? name.equals(that.name) : that.name == null;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }
}
