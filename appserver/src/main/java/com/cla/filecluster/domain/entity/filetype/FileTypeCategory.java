package com.cla.filecluster.domain.entity.filetype;

import com.google.common.collect.Sets;

import javax.persistence.*;
import java.util.Set;

/**
 *
 * Created by: ophir
 * Created on: 3/25/2019
 */
@Entity
@Table(
        name = "file_type_category"
)
public class FileTypeCategory {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Access(AccessType.PROPERTY)
    @Id
    private long id;

    @OneToMany(fetch = FetchType.EAGER, cascade=CascadeType.ALL)
    @JoinColumn(name = "file_type_category_id")
    private Set<Extension> extensions = Sets.newHashSet();

    private String name;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Extension> getExtensions() {
        return extensions;
    }

    public void setExtensions(Set<Extension> extensions) {
        this.extensions = extensions;
    }
}
