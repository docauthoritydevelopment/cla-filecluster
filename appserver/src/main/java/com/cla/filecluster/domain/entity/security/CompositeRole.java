package com.cla.filecluster.domain.entity.security;

import org.hibernate.Hibernate;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;


/**
 * Created by IR on 15/10/2017.
 */
@Entity
@Table(	name = "comp_role",
        indexes = {
                @Index(unique=true, name = "name_idx", columnList = "name,type"),
                @Index(unique=true, name = "displayname_idx", columnList = "displayName,type")
        })
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="type", discriminatorType = DiscriminatorType.STRING)
public abstract class CompositeRole implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Access(AccessType.PROPERTY)
    private Long id;

    @NotNull
    private String name;

    @NotNull
    private String displayName;

    private String description;

    private boolean deleted;

    @ManyToOne
    @JoinColumn(name = "template_id")
    private RoleTemplate template;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public RoleTemplate getTemplate() {
        return template;
    }

    public void setTemplate(RoleTemplate template) {
        this.template = template;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("CompositeRole{");
        sb.append("id=").append(id);
        sb.append(", name='").append(name).append('\'');
        sb.append(", displayName='").append(displayName).append('\'');
        sb.append(", description='").append(description).append('\'');
        sb.append(", deleted=").append(deleted);
        sb.append(", template=").append(template.getId());
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null){
            return false;
        }
        if(getClass() != o.getClass()){
            if (Hibernate.getClass(this) != Hibernate.getClass(o)){
                return false;
            }
        }

        CompositeRole that = (CompositeRole) o;

        if (!id.equals(that.id)) return false;
        return name.equals(that.name);
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + name.hashCode();
        result = 31 * result + getClass().hashCode();
        return result;
    }
}
