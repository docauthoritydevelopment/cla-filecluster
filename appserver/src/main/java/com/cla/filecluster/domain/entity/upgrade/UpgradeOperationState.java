package com.cla.filecluster.domain.entity.upgrade;

public enum UpgradeOperationState {
    NEW,
    IN_PROGRESS,
    DONE,
    FAILED
}
