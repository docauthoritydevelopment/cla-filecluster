package com.cla.filecluster.domain.entity.pv;

import com.cla.common.domain.dto.group.GroupType;
import com.cla.connector.domain.dto.JsonSerislizableVisible;

import java.util.List;

public class FileGroup implements JsonSerislizableVisible {
	
	private String name;
	
	private String generatedName;

	private Long firstMemberId;
	
	private Long numOfFiles;

    private boolean deleted = false;

    private boolean dirty = false;

    private Long countOnLastNaming;

	private String proposedNames;

	private String proposedNewNames;

	private String firstProposedNames;

	private String secondProposedNames;

	private String thirdProposedNames;

	private String fileNamesTV;

	private String parentDirTV;

	private String grandParentDirTV;

	private String id;

    private Long firstMemberContentId;

    private String parentGroupId;

    private GroupType groupType;

    private String rawAnalysisGroupId; // ID of the original (parent) analysis group

	private boolean hasSubgroups = false; // true if analysis groups has refinement rules assigned to it

	private List<String> associations;

	public FileGroup() {
	}

	public String getId() {
		return id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public String getNameForReport() {
		return (name == null || name.isEmpty())?
				(generatedName == null ? "" : generatedName)
				:name;
	}

	public String getGeneratedName() {
		return generatedName;
	}

	public void setGeneratedName(final String generatedName) {
		this.generatedName = generatedName;
	}

	public long getNumOfFiles() {
		return numOfFiles == null ? -1L : numOfFiles;
	}

	public Long getNumOfFilesNotNull() {
		return numOfFiles == null ? 0 : numOfFiles;
	}

	public void setNumOfFiles(final Long numOfFiles) {
		this.numOfFiles = numOfFiles;
	}

	public String getProposedNames() {
		return proposedNames == null ? "" : proposedNames;
	}

	public void setProposedNames(final String proposedNames) {
		this.proposedNames = proposedNames;
	}

	public String getFirstProposedNames() {
		return firstProposedNames == null ? "" : firstProposedNames;
	}

	public void setFirstProposedNames(final String firstProposedNames) {
		this.firstProposedNames = firstProposedNames;
	}

	public String getSecondProposedNames() {
		return secondProposedNames == null ? "" : secondProposedNames;
	}

	public void setSecondProposedNames(final String secondProposedNames) {
		this.secondProposedNames = secondProposedNames;
	}

	public String getThirdProposedNames() {
		return thirdProposedNames==null ? "" : thirdProposedNames;
	}

	public void setThirdProposedNames(final String thirdProposedNames) {
		this.thirdProposedNames = thirdProposedNames;
	}

	public String getFileNamesTV() {
		return fileNamesTV == null ? "" : fileNamesTV;
	}

	public void setFileNamesTV(final String fileNamesTV) {
		this.fileNamesTV = fileNamesTV;
	}
	
	public String getParentDirTV() {
		return parentDirTV==null?"":parentDirTV;
	}

	public void setParentDirTV(final String parentDirTV) {
		this.parentDirTV = parentDirTV;
	}
	
	public String getGrandParentDirTV() {
		return grandParentDirTV == null ? "" : grandParentDirTV;
	}

	public void setGrandParentDirTV(final String grandParentDirTV) {
		this.grandParentDirTV = grandParentDirTV;
	}



	public String getShortName() {
		String groupName = getName();
		if (groupName==null || groupName.isEmpty()) {
			groupName = getGeneratedName();
		}
		if (groupName==null || groupName.isEmpty()) {
			groupName = id.substring(0,8)+"...";
		}
		return groupName;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(final boolean deleted) {
		this.deleted = deleted;
		this.dirty = !deleted;	// deleted record should not be dirty; un-deleted record should be marked dirty
	}

	@Override
	public String toString() {
		return "FileGroup{" +
				" id='" + id + '\'' +
				", " + (name == null ? "generatedName='" + generatedName : "name='" + name) + '\'' +
				", groupType=" + groupType +
				", numOfFiles=" + numOfFiles +
				", deleted=" + deleted +
				", parentGroupId='" + parentGroupId + '\'' +
				", rawAnalysisGroupId='" + rawAnalysisGroupId + '\'' +
				", hasSubgroups=" + hasSubgroups +
				'}';
	}

	public void setId(String id) {
		this.id = id;
	}

	public Long getFirstMemberId() {
		return firstMemberId;
	}

	public void setFirstMemberId(Long firstMemberId) {
		this.firstMemberId = firstMemberId;
	}

	public void setFirstMemberContentId(Long firstMemberContentId) {
        this.firstMemberContentId = firstMemberContentId;
    }

    public Long getFirstMemberContentId() {
        return firstMemberContentId;
    }

    public boolean isDirty() {
        return dirty;
    }

    public void setDirty(boolean dirty) {
        this.dirty = dirty;
    }

    public Long getCountOnLastNaming() { return countOnLastNaming; }

    public void setCountOnLastNaming(Long countOnLastNaming) {
        this.countOnLastNaming = countOnLastNaming;
    }

	public long getCountOnLastNamingNotNull() { return countOnLastNaming==null?0:countOnLastNaming; }

    public GroupType getGroupType() {
        return groupType;
    }

    public void setGroupType(GroupType groupType) {
        this.groupType = groupType;
    }

    public String getParentGroupId() {
        return parentGroupId;
    }

    public void setParentGroupId(String parentGroupId) {
        this.parentGroupId = parentGroupId;
    }

	public String getRawAnalysisGroupId() {
		return rawAnalysisGroupId;
	}

	public void setRawAnalysisGroupId(String rawAnalysisGroupId) {
		this.rawAnalysisGroupId = rawAnalysisGroupId;
	}

	public boolean isHasSubgroups() {
		return hasSubgroups;
	}

	public void setHasSubgroups(boolean hasSubgroups) {
		this.hasSubgroups = hasSubgroups;
	}

	public String getProposedNewNames() {
		return proposedNewNames;
	}

	public void setProposedNewNames(String proposedNewNames) {
		this.proposedNewNames = proposedNewNames;
	}

	public List<String> getAssociations() {
		return associations;
	}

	public void setAssociations(List<String> associations) {
		this.associations = associations;
	}
}
