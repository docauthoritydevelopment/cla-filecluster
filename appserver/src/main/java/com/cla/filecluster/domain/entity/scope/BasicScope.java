package com.cla.filecluster.domain.entity.scope;

import com.cla.common.domain.dto.scope.Scope;
import com.cla.filecluster.domain.entity.BasicEntity;
import com.cla.filecluster.domain.entity.ModifiableEntity;

import javax.persistence.*;

@Entity
@Table(name = "scope",
        indexes = {
                @Index(unique = true, name = "us_user_id_idx", columnList = "user_id"),
                @Index(unique = true, name = "wgs_name_idx", columnList = "name")
        })
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "type", discriminatorType = DiscriminatorType.STRING)
public abstract class BasicScope<T> extends BasicEntity implements Scope<T> {

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BasicScope that = (BasicScope) o;

        return id == that.id;
    }
}
