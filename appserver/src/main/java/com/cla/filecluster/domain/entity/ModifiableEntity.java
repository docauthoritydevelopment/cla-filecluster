package com.cla.filecluster.domain.entity;

import com.vladmihalcea.hibernate.type.json.JsonStringType;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Basic entity for hibernate for inheritance with generated id and dates for audit
 * Created by: yael
 * Created on: 12/13/2017
 */
@TypeDefs({
        @TypeDef(name = "json", typeClass = JsonStringType.class)
})
@MappedSuperclass
public abstract class ModifiableEntity {

    private Long dateCreated;

    private Long dateModified;

    public Long getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Long dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Long getDateModified() {
        return dateModified;
    }

    public void setDateModified(Long dateModified) {
        this.dateModified = dateModified;
    }

    @PreUpdate
    protected void setLastUpdate() {
        dateModified = System.currentTimeMillis();
    }

    @PrePersist
    protected void setCreated() {
        dateCreated = System.currentTimeMillis();
        dateModified = dateCreated;
    }
}
