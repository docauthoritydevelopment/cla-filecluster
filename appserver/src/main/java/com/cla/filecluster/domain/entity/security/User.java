package com.cla.filecluster.domain.entity.security;

import com.cla.common.domain.dto.security.UserState;
import com.cla.common.domain.dto.security.UserType;
import com.cla.connector.domain.dto.JsonSerislizableVisible;
import com.google.common.collect.Sets;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toSet;

@Entity
@Table(	name = "users", 
indexes = {
		@Index(unique = true, name = "username_idx", columnList = "username")
        ,@Index(name = "deleted_idx", columnList = "deleted")
})
public class User implements JsonSerislizableVisible {

	@Id
	@GeneratedValue
	private Long id;
	
	@NotNull
	private String name;
	
	@NotNull
	private String roleName;

	@NotNull
	private String username;

    private UserType userType;

    private UserState userState;

    private String email;

    @Column(columnDefinition = "datetime(3)")
    private Date lastPasswordChange;

    private boolean passwordMustBeChanged;

    @Column(name = "last_good_login", columnDefinition = "datetime(3)")
    private Date lastSuccessfulLogin;

    @Column(name = "last_bad_login", columnDefinition = "datetime(3)")
    private Date lastFailedLogin;

    @Column(name = "login_failures")
    private int consecutiveLoginFailuresCount;

    private boolean accountNonExpired;

    private boolean accountNonLocked;

    private boolean credentialsNonExpired;

//	@JsonIgnore
	@Column(name = "password")
	private String passwd;
	
	@ManyToMany
	private Set<Role> roles = new HashSet<>();

    @ManyToMany
    @JoinTable(name = "users_comp_roles",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "comp_role_id"))
    private Set<CompositeRole> compositeRoles = Sets.newHashSet();

    @ManyToMany
    @JoinTable(name = "users_work_groups",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "work_group_id"))
    private Set<WorkGroup> workGroups = Sets.newHashSet();

    @ElementCollection(fetch = FetchType.LAZY)
    private Set<String> accessTokens = Sets.newHashSet();

    private boolean deleted;

    @NotNull
    private boolean ldapUser = false;

    private Long lastLdapFound;

	public User() {
	}
	
	public User(final String name, final String username, final String password, final String roleName, final Role... roles) {
        this(name, username, password, roleName, true, true, true,
                UserType.INTERNAL, UserState.ACTIVE, Stream.of(roles).collect(toSet()));
	}

    public User(final String name, final String username, final String password, final String roleName, final Set<Role> roles) {
        this(name, username, password, roleName, true, true, true,
                UserType.INTERNAL, UserState.ACTIVE, roles);
    }

    public User(final String name, final String username, final String password, final String roleName,
                final boolean accountNonExpired, final boolean accountNonLocked, final boolean credentialsNonExpired,
                final UserType userType, final UserState userState, final Role... roles) {
        this(name, username, password, roleName, accountNonExpired, accountNonLocked, credentialsNonExpired,
                userType, userState,
                (roles == null ? null : Stream.of(roles).collect(toSet())));
    }

    public User(final String name, final String username, final String password, final String roleName,
                final boolean accountNonExpired, final boolean accountNonLocked, final boolean credentialsNonExpired,
                final UserType userType, final UserState userState, final Set<Role> roles) {
        this.name = name;
        this.username = username;
        this.passwd = password;
        this.roleName = roleName;
        this.roles = new HashSet<>(roles);
        this.userType = userType;
        this.userState = userState;

        consecutiveLoginFailuresCount = 0;
        this.accountNonExpired = accountNonExpired;
        this.accountNonLocked = accountNonLocked;
        this.credentialsNonExpired = credentialsNonExpired;
    }



	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(final String roleName) {
		this.roleName = roleName;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(final String username) {
		this.username = username;
	}

//	@JsonIgnore
	public String getPassword() {
		return passwd;
	}

	
//	@JsonProperty
	public void setPassword(final String password) {
		this.passwd = password;
	}

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public Set<Role> getRoles() {
		return roles;
	}
	
	public void addRole(final Role role){
		roles.add(role);
	}
	
	public void removeRole(final Role role){
		roles.remove(role);
	}


	public Long getId() {
		return id;
	}


    public void setId(Long id) {
        this.id = id;
    }

    public UserType getUserType() {
        return userType;
    }

    public void setUserType(UserType userType) {
        this.userType = userType;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getLastPasswordChange() {
        return lastPasswordChange;
    }

    public void setLastPasswordChange(Date lastPasswordChange) {
        this.lastPasswordChange = lastPasswordChange;
    }

    public boolean isPasswordMustBeChanged() {
        return passwordMustBeChanged;
    }

    public void setPasswordMustBeChanged(boolean passwordMustBeChanged) {
        this.passwordMustBeChanged = passwordMustBeChanged;
    }

    public Date getLastSuccessfulLogin() {
        return lastSuccessfulLogin;
    }

    public void setLastSuccessfulLogin(Date lastSuccessfulLogin) {
        this.lastSuccessfulLogin = lastSuccessfulLogin;
    }

    public Date getLastFailedLogin() {
        return lastFailedLogin;
    }

    public void setLastFailedLogin(Date lastFailedLogin) {
        this.lastFailedLogin = lastFailedLogin;
    }

    public int getConsecutiveLoginFailuresCount() {
        return consecutiveLoginFailuresCount;
    }

    public void setConsecutiveLoginFailuresCount(int consecutiveLoginFailuresCount) {
        this.consecutiveLoginFailuresCount = consecutiveLoginFailuresCount;
    }

    public boolean isAccountNonExpired() {
        return accountNonExpired;
    }

    public void setAccountNonExpired(boolean accountNonExpired) {
        this.accountNonExpired = accountNonExpired;
    }

    public boolean isAccountNonLocked() {
        return accountNonLocked;
    }

    public void setAccountNonLocked(boolean accountNonLocked) {
        this.accountNonLocked = accountNonLocked;
    }

    public boolean isCredentialsNonExpired() {
        return credentialsNonExpired;
    }

    public void setCredentialsNonExpired(boolean credentialsNonExpired) {
        this.credentialsNonExpired = credentialsNonExpired;
    }

    public UserState getUserState() {
        return userState;
    }

    public void setUserState(UserState userState) {
        this.userState = userState;
    }

    public Set<String> getAccessTokens() {
        return accessTokens;
    }

    public void setAccessTokens(Set<String> accessTokens) {
        this.accessTokens = accessTokens;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Set<BizRole> getBizRoles() {
        return compositeRoles.stream()
                .filter(r->r instanceof BizRole)
                .map(obj->(BizRole)obj)
                .collect(Collectors.toSet());
    }

    public Set<FunctionalRole> getFunctionalRoles() {
        return compositeRoles.stream()
                .filter(r->r instanceof FunctionalRole)
                .map(obj->(FunctionalRole)obj)
                .collect(Collectors.toSet());
    }

    public void setBizRoles(Set<BizRole> bizRoles) {
        this.compositeRoles.removeAll(getBizRoles());
        this.compositeRoles.addAll(bizRoles);
    }

    public void setFunctionalRoles(Set<FunctionalRole> functionalRoles) {
        this.compositeRoles.removeAll(getFunctionalRoles());
        this.compositeRoles.addAll(functionalRoles);
    }

    public boolean isLdapUser() {
        return ldapUser;
    }

    public void setLdapUser(Boolean ldapUser) {
        this.ldapUser = ldapUser;
    }

    public Set<WorkGroup> getWorkGroups() {
        return workGroups;
    }

    public void setWorkGroups(Set<WorkGroup> workGroups) {
        this.workGroups = workGroups;
    }

    public Long getLastLdapFound() {
        return lastLdapFound;
    }

    public void setLastLdapFound(Long lastLdapFound) {
        this.lastLdapFound = lastLdapFound;
    }

    @SuppressWarnings("StringBufferReplaceableByString")
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("User{");
        sb.append("id=").append(id);
        sb.append(", name='").append(name).append('\'');
        sb.append(", roleName='").append(roleName).append('\'');
        sb.append(", username='").append(username).append('\'');
        sb.append(", userType=").append(userType);
        sb.append(", userState=").append(userState);
        sb.append(", email='").append(email).append('\'');
        sb.append(", lastPasswordChange=").append(lastPasswordChange);
        sb.append(", passwordMustBeChanged=").append(passwordMustBeChanged);
        sb.append(", lastSuccessfulLogin=").append(lastSuccessfulLogin);
        sb.append(", lastFailedLogin=").append(lastFailedLogin);
        sb.append(", consecutiveLoginFailuresCount=").append(consecutiveLoginFailuresCount);
        sb.append(", accountNonExpired=").append(accountNonExpired);
        sb.append(", accountNonLocked=").append(accountNonLocked);
        sb.append(", credentialsNonExpired=").append(credentialsNonExpired);
        sb.append(", roles=").append(roles);
        sb.append(", compositeRoles=").append(compositeRoles);
        sb.append(", accessTokens=").append(accessTokens);
        sb.append(", deleted=").append(deleted);
        sb.append(", ldapUser=").append(ldapUser);
        sb.append('}');
        return sb.toString();
    }

}
