package com.cla.filecluster.domain.entity.bizlist;

import javax.persistence.*;

/**
 * Created by uri on 31/12/2015.
 */
@Entity
@Table(name = "bl_item_alias")
public class BizListItemAlias {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Column(length = 1024)
    String alias;

    @Column(length = 4096)
    String encryptedAlias;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    SimpleBizListItem entity;

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public SimpleBizListItem getEntity() {
        return entity;
    }

    public void setEntity(SimpleBizListItem entity) {
        this.entity = entity;
    }

    public String getEncryptedAlias() {
        return encryptedAlias;
    }

    public void setEncryptedAlias(String encryptedAlias) {
        this.encryptedAlias = encryptedAlias;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BizListItemAlias that = (BizListItemAlias) o;

        if (id == null) {
            return super.equals(o);
        }
        return id == that.id;

    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }
}
