package com.cla.filecluster.domain.entity.filetag;

import com.cla.common.domain.dto.scope.ScopedEntity;
import com.cla.filecluster.domain.entity.scope.BasicScope;

public interface ScopedEntityAssociation extends ScopedEntity<BasicScope> {

    AssociableEntity getAssociableEntity();
}