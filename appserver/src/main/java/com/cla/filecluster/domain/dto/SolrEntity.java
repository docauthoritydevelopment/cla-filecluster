package com.cla.filecluster.domain.dto;

/**
 * Based class for annotated POJOs to facilitate data entry/retrieval to/from SOLR
 */
public interface SolrEntity {
    String getIdAsString();
}
