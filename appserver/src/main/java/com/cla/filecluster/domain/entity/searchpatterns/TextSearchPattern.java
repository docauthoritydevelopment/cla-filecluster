package com.cla.filecluster.domain.entity.searchpatterns;

import com.cla.common.domain.dto.filesearchpatterns.PatternType;
import com.google.common.collect.Sets;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import java.util.*;

/**
 * Created by uri on 23/06/2016.
 */

@Entity
public class TextSearchPattern {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;

    private String description;

    @Length(max = 1024)
    String pattern;

    PatternType patternType;

    boolean active;

    private String validatorClass;

    @ManyToMany(fetch = FetchType.EAGER)
    private Set<PatternCategory> subCategories =  Sets.newHashSet();

    @ManyToMany(fetch = FetchType.EAGER)
    private Set<Regulation> regulations = Sets.newHashSet();

    @Column(updatable=false)
    private Long createdOnDB;

    @PrePersist
    private void setCreatedOnDB(){
        createdOnDB = System.currentTimeMillis();
    }

    public Long getCreatedOnDB() {
        return createdOnDB;
    }

    public void setCreatedOnDB(Long createdOnDB) {
        this.createdOnDB = createdOnDB;
    }

    public Set<PatternCategory> getSubCategories() {
        return subCategories;
    }

    public void setSubCategories(Set<PatternCategory> subCategories) {
        this.subCategories = subCategories;
    }


    public Set<Regulation> getRegulations() {
        return regulations;
    }

    public void setRegulations(Set<Regulation> regulations) {
        this.regulations = regulations;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPattern() {
        return pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }

    public PatternType getPatternType() {
        return patternType;
    }

    public void setPatternType(PatternType patternType) {
        this.patternType = patternType;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getValidatorClass() {
        return validatorClass;
    }

    public void setValidatorClass(String validatorClass) {
        this.validatorClass = validatorClass;
    }
}
