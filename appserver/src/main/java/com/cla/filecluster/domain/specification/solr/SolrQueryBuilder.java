package com.cla.filecluster.domain.specification.solr;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.domain.dto.date.DateRangeItemDto;
import com.cla.common.domain.dto.date.DateRangePointDto;
import com.cla.common.domain.dto.file.ClaFileState;
import com.cla.common.domain.dto.file.ConstFileTypeCategory;
import com.cla.common.domain.dto.file.FileSizePartitionDto;
import com.cla.common.domain.dto.kendo.FilterDescriptor;
import com.cla.common.utils.AssociationIdUtils;
import com.cla.connector.constants.KendoFilterConstants;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.connector.domain.dto.file.FileType;
import com.cla.connector.domain.dto.file.ItemType;
import com.cla.connector.domain.dto.media.MediaType;
import com.cla.filecluster.domain.entity.filetag.FileTag;
import com.cla.filecluster.domain.entity.filetree.RootFolder;
import com.cla.filecluster.domain.entity.filetype.Extension;
import com.cla.filecluster.domain.entity.filetype.FileTypeCategory;
import com.cla.filecluster.domain.entity.searchpatterns.Regulation;
import com.cla.filecluster.domain.entity.searchpatterns.TextSearchPattern;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.repository.jpa.searchpattern.RegulationRepository;
import com.cla.filecluster.repository.solr.query.SolrQueryGenerator;
import com.cla.filecluster.repository.solr.*;
import com.cla.filecluster.service.api.ModelDtoConversionUtils;
import com.cla.filecluster.service.categories.FileTypeCategoryService;
import com.cla.filecluster.service.convertion.FileDtoFieldConverter;
import com.cla.filecluster.service.files.FileSizeRangeService;
import com.cla.filecluster.service.files.filetree.DocStoreService;
import com.cla.filecluster.service.files.filetree.SharePermissionService;
import com.cla.filecluster.service.filetag.FileTagService;
import com.cla.filecluster.util.categories.FileCatUtils;
import com.cla.filecluster.util.query.CriteriaQueryUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.solr.client.solrj.util.ClientUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

import static com.cla.common.constants.CatFileFieldType.FULL_NAME_SEARCH;
import static com.cla.common.domain.dto.kendo.FilterDescriptor.CONTENT_FILTER;

@Service
public class SolrQueryBuilder {

    private final static Logger logger = LoggerFactory.getLogger(SolrQueryBuilder.class);

    private static final String MIN_COUNT = "minCount";

    private static final String FACET_PREFIX = "facetPrefix";

    public static final String GROUP_FC_PREFIX = "group.fc.";

    private static final String JOIN_FROM_GROUP_ID_TO_GROUP_ID = "{!join from=userGroupId to=userGroupId}-";
    private static DateTimeFormatter solrDateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");

    @Value("${security.acl.generic-tokens}")
    private String[] openByAccessRightsTokens;

    @Value("${solr.catFile.expiredUserKey:ExpiredUser}")
    private String expiredUserKey;

    @Value("${reports.document-file-types:1,2,20}")
    private String[] documentFileTypes;

    @Autowired
    private SolrSpecificationFactory solrSpecificationFactory;

    @Autowired
    private DocStoreService docStoreService;

    @Autowired
    private SharePermissionService sharePermissionService;

    @Autowired
    private FileTypeCategoryService fileTypeCategoryService;

    @Autowired
    private RegulationRepository regulationRepository;

    @Autowired
    private FileSizeRangeService fileSizeRangeService;

    @Autowired
    private FileTagService fileTagService;


    @Transactional(readOnly = true)
    public List<String> getFilterQueries(SolrSpecification solrSpecification) {
        List<String> filterQueries;
        if (solrSpecification.getFilterDescriptor() == null) {
            filterQueries = Lists.newArrayList();
        } else {
            solrSpecification.setFilterDescriptor(removeSingleItemNodes(solrSpecification.getFilterDescriptor()));
            String filterQuery = generateFilterQuery(solrSpecification.getFilterDescriptor(), false, solrSpecification);
            filterQueries = Lists.newArrayList(filterQuery);
        }
        if (solrSpecification.getAdditionalFilter() != null && solrSpecification.getAdditionalFilter().size() > 0) {
            filterQueries.addAll(solrSpecification.getAdditionalFilter());
        }
        if (solrSpecification.getTagTypeMismatchFilter() != null) {
            filterQueries.add(solrSpecification.getTagTypeMismatchFilter());
        }
        if (solrSpecification.getGroupJoinFilter() != null) {
            filterQueries.add(getGroupJoinFilterQuery(solrSpecification));
        }
        return filterQueries;
    }

    /**
     * {!join from=groupId to=groupId}-parentFolderIds:2.133
     *
     * @return query
     */
    private String getGroupJoinFilterQuery(SolrSpecification solrSpecification) {
        String operator = solrSpecification.getGroupJoinFilter().getOptionalOperator().orElse(KendoFilterConstants.EQ);
        String dtoField = solrSpecification.getGroupJoinFilter().getField();
        if (dtoField == null) {
            throw new BadRequestException("filter", "Failed to get concrete query. Null filter dtoField (op:" + operator + ")", BadRequestType.MISSING_FIELD);
        }
        CatFileFieldType field = convertToCatFileFieldType(dtoField);
        StringBuilder sb = new StringBuilder();
        Object value = solrSpecification.getGroupJoinFilter().getValue();
        value = parseFilterValue(solrSpecification.getGroupJoinFilter(), field, value);
        if (operator.toLowerCase().equals(KendoFilterConstants.EQ)) {
            sb.append("-");
        }
        sb.append(JOIN_FROM_GROUP_ID_TO_GROUP_ID);
        sb.append(field.getSolrName()).append(":").append(value);
        return sb.toString();
    }

    private FilterDescriptor getFileNameQuery(FilterDescriptor filterDescriptor, String fieldSearch) {
        String searchFor = filterDescriptor.getValue().toString();

        while (searchFor.startsWith("\\") || searchFor.startsWith("/")) {
            searchFor = searchFor.substring(1);
        }

        // get path parts options e.g path is D:/Enron/a options are D:/ , D:/Enron , D:/Enron/a
        final List<String> options = new ArrayList<>();
        String possibleMailboxGroupName = null;
        if (searchFor.matches(".*[\\\\/]+.*")) {
            final String[] pathParts = searchFor.matches(".*[\\\\/]+.*") ? searchFor.split("[\\\\/]") : new String[]{searchFor};
            if (pathParts.length > 0) {
                possibleMailboxGroupName = pathParts[0];
            }
            for (int i = 0; i < pathParts.length; ++i) {
                StringBuilder option = new StringBuilder();
                for (int j = 0; j <= i; ++j) {
                    option.append(pathParts[j]).append("/");
                }
                options.add(option.toString());
            }
        } else {
            options.add(searchFor);
            possibleMailboxGroupName = searchFor;
        }

        logger.trace("search for {} options {} possibleMailboxGroupName {}", searchFor, options, possibleMailboxGroupName);

        // get root folders whose path contain said path parts
        Set<RootFolder> rfs = new HashSet<>();
        Map<Long, List<String>> rfsForParts = new HashMap<>();
        options.forEach(o -> {
            List<RootFolder> res = docStoreService.findContainsPath(o);
            if (res != null && !res.isEmpty()) {
                res.forEach(r -> {
                    rfs.add(r);
                    List<String> parts = rfsForParts.getOrDefault(r.getId(), new ArrayList<>());
                    parts.add(o);
                    rfsForParts.put(r.getId(), parts);
                });
            }
        });

        List<RootFolder> rfByMailbox = new ArrayList<>();
        if (!Strings.isNullOrEmpty(possibleMailboxGroupName)) {
            rfByMailbox = docStoreService.findByMailboxGroup(possibleMailboxGroupName);
        }

        logger.trace("search for {} found path rf {} mailbox rf {}", searchFor, rfsForParts.keySet(), rfByMailbox.size());

        List<FilterDescriptor> filters = new ArrayList<>();
        for (RootFolder rootFolder : rfs) {

            // get the longest path found for this root folder
            List<String> partsFound = rfsForParts.get(rootFolder.getId());
            String maxPartFound = Collections.max(partsFound, Comparator.comparing(String::length));

            // get the part of the root folder left after the part that was found
            String rfPath = (rootFolder.getPath().contains(maxPartFound) ? rootFolder.getPath() : rootFolder.getRealPath());
            String pathLeftRf = rfPath.substring(rfPath.indexOf(maxPartFound) + maxPartFound.length());

            // get the left part of the search path not part of the root folder path
            String pathLeft = searchFor.substring(searchFor.indexOf(maxPartFound) + maxPartFound.length());

            // this means there is more to the path to search and the root folder also has more path but they dont match
            if (!Strings.isNullOrEmpty(pathLeftRf) && !Strings.isNullOrEmpty(pathLeft)) {
                continue;
            }

            filters.add(createFilterForPath(rootFolder.getId(), pathLeft, fieldSearch));
        }

        for (RootFolder rootFolder : rfByMailbox) {
            // get the left part of the search path not part of the root folder path
            String pathLeft = searchFor.substring(searchFor.indexOf(possibleMailboxGroupName) + possibleMailboxGroupName.length());
            filters.add(createFilterForPath(rootFolder.getId(), pathLeft, fieldSearch));
        }

        if (filters.size() == 0) { // probably just part of the file name, search for it
            return new FilterDescriptor(fieldSearch, searchFor.toLowerCase(), KendoFilterConstants.CONTAINS);
        }

        if (filters.size() == 1) { // only one filter created, add it
            return filters.get(0);
        }

        // create mega filter for all the filters
        FilterDescriptor filter = new FilterDescriptor();
        filter.setLogic("OR");
        filter.setFilters(filters);
        return filter;
    }

    private FilterDescriptor createFilterForPath(Long rfId, String pathLeft, String fieldSearch) {
        if (pathLeft.replaceAll("[\\\\/]", "").length() > 0) { // path left search for it in the name of the file
            FilterDescriptor filter = new FilterDescriptor();
            filter.addFilter(new FilterDescriptor(FileDtoFieldConverter.DtoFields.rootFolderId.getAlternativeName(), rfId, KendoFilterConstants.EQ));
            filter.addFilter(new FilterDescriptor(fieldSearch, pathLeft.toLowerCase(), KendoFilterConstants.CONTAINS));
            filter.setLogic("AND");
            return filter;
        } else { // the path is part of the root folder path, just add it
            return new FilterDescriptor(FileDtoFieldConverter.DtoFields.rootFolderId.getAlternativeName(), rfId, KendoFilterConstants.EQ);
        }
    }

    public String generateFilterQuery(FilterDescriptor filterDescriptor, boolean addParentheses, SolrSpecification solrSpecification) {
        List<FilterDescriptor> filters = filterDescriptor.getFilters();
        String logic = filterDescriptor.getLogic() != null ? filterDescriptor.getLogic() : "and";

        String filterQuery;
        if (CollectionUtils.isEmpty(filters)) {
            if (CONTENT_FILTER.equalsIgnoreCase(filterDescriptor.getField())) {
                //content filter is unique - not a normal filter
                solrSpecification.addToContentFilter(filterDescriptor.getValue().toString());
                return ""; // Down the road - Solr repository ignores empty filter queries
            } else if (FileDtoFieldConverter.DtoFields.fullName.getAlternativeName().equalsIgnoreCase(filterDescriptor.getField())) {
                FilterDescriptor fd = getFileNameQuery(filterDescriptor, filterDescriptor.getField());
                if (CollectionUtils.isEmpty(fd.getFilters())) {
                    return getConcreteQuery(fd, solrSpecification);
                } else if (fd.getLogic().equalsIgnoreCase("AND")) {
                    StringJoiner joiner = addParentheses ? new StringJoiner(" AND ", "(", ")") : new StringJoiner(" AND ");
                    for (FilterDescriptor current : fd.getFilters()) {
                        joiner.add(getConcreteQuery(current, solrSpecification));
                    }
                    return joiner.toString();
                } else {
                    return generateFilterQuery(fd, addParentheses, solrSpecification);
                }
            } else if (filterDescriptor.getField() == null) {
                return "";
            } else if (MIN_COUNT.equalsIgnoreCase(filterDescriptor.getField())) {
                solrSpecification.setMinCount((Integer) filterDescriptor.getValue());
                return ""; // Down the road - Solr repository ignores empty filter queries
            } else if (FACET_PREFIX.equalsIgnoreCase(filterDescriptor.getField())) {
                solrSpecification.setFacetPrefix((String) filterDescriptor.getValue());
                return ""; // Down the road - Solr repository ignores empty filter queries
            } else if (filterDescriptor.getField().startsWith(GROUP_FC_PREFIX)) {
                addToGroupJoinFilter(filterDescriptor, solrSpecification);
                return "";
            } else {
                //Build a single filter and return it
                filterQuery = getConcreteQuery(filterDescriptor, solrSpecification);
                return filterQuery;
            }
        }

        //A collection of filters
        String delimiter = " AND ";
        String notFixer = SolrQueryGenerator.ALL_QUERY;
        if ("or".equalsIgnoreCase(logic)) {
            delimiter = " OR ";
            notFixer = "fileId:0";
        }
        StringJoiner joiner;
        if (addParentheses) {
            joiner = new StringJoiner(delimiter, "(", ")");
        } else {
            joiner = new StringJoiner(delimiter);
        }
        boolean emptyFilter = true;
        for (FilterDescriptor subFilter : filterDescriptor.getFilters()) {
            if (CONTENT_FILTER.equalsIgnoreCase(subFilter.getField())) {
                //content filter is unique - not a normal filter
                solrSpecification.addToContentFilter(subFilter.getValue().toString());
            } else {
                String subFilterString = generateFilterQuery(subFilter, true, solrSpecification);
                if (StringUtils.isNotEmpty(subFilterString)) {
                    if (emptyFilter && subFilterString.startsWith("-")) {
                        joiner.add(notFixer);
                    }
                    joiner.add(subFilterString);
                    emptyFilter = false;
                }
            }
        }
        if (emptyFilter) {
            return ""; // Down the road - Solr repository ignores empty filter queries
        }
        filterQuery = joiner.toString();
        return filterQuery;
    }

    private void addToGroupJoinFilter(FilterDescriptor filterDescriptor, SolrSpecification solrSpecification) {
        if (filterDescriptor.getField().startsWith(GROUP_FC_PREFIX)) {
            String concreteFilter = filterDescriptor.getField().substring(GROUP_FC_PREFIX.length());
            filterDescriptor.setField(concreteFilter);
        }
        solrSpecification.setGroupJoinFilter(filterDescriptor);
    }

    private FilterDescriptor removeSingleItemNodes(FilterDescriptor filterDescriptor) {

        List<FilterDescriptor> filters = filterDescriptor.getFilters();
        if (filters == null) {
            return filterDescriptor;
        }
        if (filters.stream().anyMatch(f -> (f.getFilters() != null && f.getFilters().size() == 1))) {
            filterDescriptor.setFilters(filters.stream().map(this::removeSingleItemNodes).collect(Collectors.toList()));
        }
        String logic = filterDescriptor.getLogic() != null ? filterDescriptor.getLogic() : "and";

        // Convert single item AND/OR to the item itself
        if (filters.size() == 1 &&
                ("or".equalsIgnoreCase(logic) || "and".equalsIgnoreCase(logic))) {
            return (filters.get(0));
        }
        return filterDescriptor;
    }

    public String getConcreteQuery(FilterDescriptor filter, SolrSpecification solrSpecification) {
        String operator = filter.getOptionalOperator().orElse(KendoFilterConstants.CONTAINS);
        String dtoField = filter.getField();
        if (dtoField == null) {
            throw new BadRequestException("filter", "Failed to get concrete query. Null filter dtoField (op:" + operator + ")", BadRequestType.MISSING_FIELD);
        }
        CatFileFieldType field = convertToCatFileFieldType(dtoField);

        if (CatFileFieldType.TAG_TYPE_MISMATCH.equals(field)) {
            String tagTypeValue = solrSpecificationFactory.convertTagTypeIdToTagTypeFilterValue2(filter.getValue());
            String minCountFilter = solrSpecificationFactory.createMinCountFilter(
                    CatFileFieldType.TAG_TYPE_2.getSolrName(), tagTypeValue, 2);
            solrSpecification.setTagTypeMismatchFilter(minCountFilter);
            return ""; // Down the road, we have something that prunes empty filters.
        }

        if (CatFileFieldType.TAG_TYPE_ASSOCIATION.equals(field)) {
            Integer tagTypeValue = (Integer)filter.getValue();
            String[] values = solrSpecificationFactory.convertScopedTagTypeToFilterValue(String.valueOf(tagTypeValue));

            StringBuilder sb = new StringBuilder();
            createOrString(values, sb, operator, CatFileFieldType.ACTIVE_ASSOCIATIONS);
            solrSpecification.setAdditionalFilter(sb.toString());

            return "";
        }

        if (CatFileFieldType.FILE_TAG_TYPE_ASSOC.equals(field)) {
            List<FileTag> tags = fileTagService.getFileTagsByType(Long.parseLong(String.valueOf(filter.getValue())));
            List<String> fq = new ArrayList<>();
            tags.forEach(tag -> {
                fq.add("*fileTagId\\\"\\:"+tag.getId()+",*");
                fq.add("*fileTagId\\\"\\:"+tag.getId()+"\\}*");
            });

            if (!fq.isEmpty()) {
                if (operator.toLowerCase().equals(KendoFilterConstants.EQ)) {
                    solrSpecification.setAdditionalFilter(CatFileFieldType.FILE_ASSOCIATIONS.inQuery(fq));
                } else if (operator.toLowerCase().equals(KendoFilterConstants.NEQ)) {
                    solrSpecification.setAdditionalFilter(CatFileFieldType.FILE_ASSOCIATIONS.notInQuery(fq));
                }
            }

            return "";
        }

        if (CatFileFieldType.FILE_TAG_ASSOC.equals(field)) {

            List<String> values = Lists.newArrayList("*fileTagId\\\"\\:"+filter.getValue()+",*",
                    "*fileTagId\\\"\\:"+filter.getValue()+"\\}*");

            if (operator.toLowerCase().equals(KendoFilterConstants.EQ)) {
                solrSpecification.setAdditionalFilter(CatFileFieldType.FILE_ASSOCIATIONS.inQuery(values));
            } else if (operator.toLowerCase().equals(KendoFilterConstants.NEQ)) {
                solrSpecification.setAdditionalFilter(CatFileFieldType.FILE_ASSOCIATIONS.notInQuery(values));
            }

            return "";
        }

        if (CatFileFieldType.SHARED_PERMISSION.equals(field)) {
            Long sharedPermissionId = Long.parseLong(String.valueOf(filter.getValue()));
            List<Long> rootFolderIds = sharePermissionService.getRelevantRootFolders(sharedPermissionId);
            if (rootFolderIds.isEmpty()) {
                rootFolderIds.add(0L);
            }

            if (operator.toLowerCase().equals(KendoFilterConstants.EQ)) {
                solrSpecification.setAdditionalFilter(CatFileFieldType.ROOT_FOLDER_ID.inQuery(rootFolderIds));
            } else if (operator.toLowerCase().equals(KendoFilterConstants.NEQ)) {
                solrSpecification.setAdditionalFilter(CatFileFieldType.ROOT_FOLDER_ID.notInQuery(rootFolderIds));
            }

            return "";
        }

        if (CatFileFieldType.UN_GROUPED.equals(field)) {
            solrSpecification.setAdditionalFilter(CatFileFieldType.PROCESSING_STATE.filter(ClaFileState.ANALYSED.name()));

            if (operator.toLowerCase().equals(KendoFilterConstants.EQ)) {
                solrSpecification.setAdditionalFilter(CatFileFieldType.USER_GROUP_ID.notFilter(SolrQueryGenerator.ALL_RANGE));
            } else if (operator.toLowerCase().equals(KendoFilterConstants.NEQ)) {
                solrSpecification.setAdditionalFilter(CatFileFieldType.USER_GROUP_ID.filter(SolrQueryGenerator.ALL_RANGE));
            }

            return "";
        }

        if (CatFileFieldType.GENERIC_STATE.equals(field)) {
            String type = (String)filter.getValue();
            if (type.equalsIgnoreCase("Ingested")) {
                solrSpecification.setAdditionalFilter(CatFileFieldType.PROCESSING_STATE.inQuery(
                        Lists.newArrayList(ClaFileState.INGESTED.name(), ClaFileState.ANALYSED.name())));
            } else if (type.equalsIgnoreCase("Analyzed")) {
                solrSpecification.setAdditionalFilter(CatFileFieldType.PROCESSING_STATE.filter(ClaFileState.ANALYSED.name()));
            }

            return "";
        }

        if (CatFileFieldType.SPECIFIC_STATE.equals(field)) {
            String type = (String)filter.getValue();
            if (type.equalsIgnoreCase("Map")) {
                solrSpecification.setAdditionalFilter(CatFileFieldType.PROCESSING_STATE.filter(ClaFileState.SCANNED.name()));
            } else if (type.equalsIgnoreCase("Error")) {
                solrSpecification.setAdditionalFilter(CatFileFieldType.PROCESSING_STATE.inQuery(
                        Lists.newArrayList(ClaFileState.ERROR.name(), ClaFileState.SCANNED_AND_STOPPED.name())));
            } else if (type.equalsIgnoreCase("Ingested")) {
                solrSpecification.setAdditionalFilter(CatFileFieldType.PROCESSING_STATE.filter(ClaFileState.INGESTED.name()));
            } else if (type.equalsIgnoreCase("Analyzable")) {
                solrSpecification.setAdditionalFilter(CatFileFieldType.PROCESSING_STATE.filter(ClaFileState.INGESTED.name()));
                solrSpecification.setAdditionalFilter(CatFileFieldType.CONTENT_ID.filter(SolrQueryGenerator.ALL_RANGE));
                solrSpecification.setAdditionalFilter(CatFileFieldType.TYPE.inQuery(Arrays.asList(documentFileTypes)));
            } else if (type.equalsIgnoreCase("Analyzed")) {
                solrSpecification.setAdditionalFilter(CatFileFieldType.PROCESSING_STATE.filter(ClaFileState.ANALYSED.name()));
            }

            return "";
        }

        if (CatFileFieldType.NO_MAIL_RELATED.equals(field)) {
            String values = ItemType.getAllEmailRelated().stream().map(t ->
                    String.valueOf(ItemType.valueOf(t.toString()).toInt())).collect(
                    Collectors.joining(" ", "(", ")"));
            solrSpecification.setAdditionalFilter("-" + CatFileFieldType.ITEM_TYPE + ":" + values);
            return "";
        }

        if (CatFileFieldType.EXPIRED_USER.equals(field)) {
            if (operator.equals(KendoFilterConstants.EQ)) {
                solrSpecification.setAdditionalFilter(
                        CatFileFieldType.ACL_READ.filter(expiredUserKey) + " OR " +
                                CatFileFieldType.ACL_WRITE.filter(expiredUserKey));
            } else {
                solrSpecification.setAdditionalFilter(
                        CatFileFieldType.ACL_READ.notFilter(expiredUserKey) + " AND " +
                                CatFileFieldType.ACL_WRITE.notFilter(expiredUserKey));
            }
            return "";
        }

        if (CatFileFieldType.OPEN_ACCESS_RIGHTS.equals(field)) {
            String query = CatFileFieldType.OPEN_ACCESS_RIGHTS.inQuery(
                    Arrays.stream(openByAccessRightsTokens).
                            map(String::toLowerCase)
                            .collect(Collectors.toList()));
            StringBuilder aclSb = new StringBuilder();
            if (operator.toLowerCase().equals(KendoFilterConstants.EQ)) {
                aclSb.append(" (");
            } else {
                aclSb.append(" -(");
            }
            aclSb.append(query);
            aclSb.append("  )");
            return aclSb.toString();
        }

        if (CatFileFieldType.REGULATION_NAME.equals(field) || CatFileFieldType.REGULATION_ID.equals(field)) {
            Regulation reg;

            if (CatFileFieldType.REGULATION_NAME.equals(field)) {
                reg = this.regulationRepository.findByName(String.valueOf(filter.getValue()));
            } else {
                reg = this.regulationRepository.findById(Integer.parseInt(String.valueOf(filter.getValue()))).orElse(null);
            }

            if (reg == null) {
                return SolrSpecificationFactory.NOT_DEF_STR_FIELD;
            }

            StringBuilder sb = new StringBuilder();
            List<String> patternIds = new ArrayList<>();

            if (reg.isActive()) {
                reg.getPatterns().forEach((TextSearchPattern pattern) -> {
                    if (pattern.isActive() && pattern.getSubCategories().iterator().next().isActive()) {
                        patternIds.add(pattern.getId().toString());
                    }
                });
            }
            if (patternIds.size() == 0) {
                return "'true' != 'false'"; //always false
            }
            createOrString(patternIds.toArray(new String[0]), sb, operator, CatFileFieldType.MATCHED_PATTERNS_SINGLE);
            return sb.toString();
        }


        if (CatFileFieldType.FILE_TYPE_CATEGORY_ID.equals(field)) {
            if (!String.valueOf(filter.getValue()).equals(String.valueOf(ConstFileTypeCategory.OTHER.getId()))) {
                Optional<FileTypeCategory> ftc = fileTypeCategoryService.findById(Long.parseLong(String.valueOf(filter.getValue())));
                if (!ftc.isPresent()) {
                    return "";
                }
                StringBuilder sb = new StringBuilder();
                createOrString(ftc.get().getExtensions().stream()
                        .map(Extension::getName).toArray(String[]::new), sb, operator, CatFileFieldType.EXTENSION);
                return sb.toString();
            } else {
                List<FileTypeCategory> allFtc = fileTypeCategoryService.getAll();
                List<String> otherExtNames = new ArrayList<>();
                if (allFtc.size() > 0) {
                    allFtc.forEach((FileTypeCategory ftc) -> otherExtNames.addAll(ftc.getExtensions().stream()
                            .map(Extension::getName)
                            .collect(Collectors.toList())));
                    StringBuilder sb = new StringBuilder();
                    if (!operator.equals(KendoFilterConstants.NEQ) && !operator.equals(KendoFilterConstants.DOESNOTCONTAIN)) {
                        sb.append("-");
                    }
                    createOrString(otherExtNames.toArray(new String[0]), sb, KendoFilterConstants.EQ, CatFileFieldType.EXTENSION);
                    return sb.toString();
                } else {
                    return "";
                }
            }
        }

        if (CatFileFieldType.EMAIL_PARTS.equals(field)) {
            solrSpecification.setAdditionalFilter(CatFileFieldType.CONTAINER_IDS + ":" + FileType.MAIL + "." + filter.getValue()
                    + " OR " + CatFileFieldType.FILE_ID + ":" + filter.getValue());
            return "";
        }

        StringBuilder sb = new StringBuilder();

        //Parse the field value
        Object value = filter.getValue();
        value = parseFilterValue(filter, field, value);
        // In case field is tag then

        field = resolveScopedFieldIfNeeded(filter, field);

        validateFilterValueType(value, field);
        if (value instanceof String[]) {
            String[] values = (String[]) value;
            createOrString(values, sb, operator, field);
        } else {
            handleFilterOperator(operator, field, value, sb);
        }
        String res = sb.toString();
        return res.startsWith("--") ?
                res.substring(res.indexOf('(', 2) - 1) :       // In case multiple '-' were wrongly inserted, keep only one
                res;
    }


    private void createOrString(String[] values, StringBuilder sb, String operator, CatFileFieldType field) {
        String onGoingOpeartor = operator;
        if (operator.equals(KendoFilterConstants.NEQ)) {
            onGoingOpeartor = KendoFilterConstants.EQ;
            sb.append("-");
        } else if (operator.equals(KendoFilterConstants.DOESNOTCONTAIN)) {
            onGoingOpeartor = KendoFilterConstants.CONTAINS;
            sb.append("-");
        }
        sb.append("(");
        for (int i = 0; i < values.length; i++) {
            StringBuilder filterOperatorSb = new StringBuilder();
            handleFilterOperator(onGoingOpeartor, field, values[i], filterOperatorSb);
            sb.append(filterOperatorSb.toString());
            if (i + 1 < values.length) {
                sb.append(" or ");
            }
        }
        sb.append(")");
    }

    private CatFileFieldType resolveScopedFieldIfNeeded(FilterDescriptor filter, CatFileFieldType field) {

        if (filter.getValue().equals("*")) {
            return field;
        }


        if (field == CatFileFieldType.TAG_TYPE || field == CatFileFieldType.TAGS || field == CatFileFieldType.DOC_TYPES || field == CatFileFieldType.DOC_TYPE_PARENTS) {
            return CatFileFieldType.SCOPED_TAGS;
        }

        return field;
    }

    private void validateFilterValueType(Object value, CatFileFieldType field) {
        if (Long.class.equals(field.getType())) {
            if (value instanceof String && !"*".equalsIgnoreCase(value.toString())) {
                //Verify value is a concrete number
                try {
                    Long.valueOf(value.toString());
                } catch (NumberFormatException e) {
                    throw new BadRequestException("Illegal filter value on numeric filter field", value, BadRequestType.UNSUPPORTED_VALUE);
                }
            }
        }
    }

    private Object handleUniqueOperators(String operator, CatFileFieldType field, Object value) {
        if (operator.toLowerCase().equals(KendoFilterConstants.GT)) {
            value = increaseValue(field.getType(), value);
        }
        if (operator.toLowerCase().equals(KendoFilterConstants.LT)) {
            value = decreaseValue(field.getType(), value);
        }
        return value;
    }

    private void handleFilterOperator(String operator, CatFileFieldType field, Object value, StringBuilder sb) {
        value = handleUniqueOperators(operator, field, value);
        if (value == null) {
            throw new BadRequestException(field.toString(), "Unexpected null value in filter for field " + field.toString(), BadRequestType.MISSING_FIELD);
        }

        if (value instanceof LocalDateTime) { // Force solr formatting
            value = solrDateFormat.format((LocalDateTime) value);
        }

        sb.append(field.getSolrName()).append(":");

        switch (operator.toLowerCase()) {
            case KendoFilterConstants.EQ:
                if (field.getFieldType().equals(String.class) && String.valueOf(value).isEmpty()) {
                    sb.append("\"\"");
                } else {
                    sb.append(value);
                }
                break;
            case KendoFilterConstants.CONTAINS:
                if (field.getType().equals(String.class)) {
                    String value1 = (String) value;
                    if (!(value1.contains(" ") || value1.contains("\""))) {
                        sb.append("/.*").append(value).append(".*/");
                    } else if (value1.contains(" ")) {
                        sb.append("*").append(value).append("*");
                    } else {
                        sb.append(value);
                    }
                } else {
                    sb.append(value);
                }
                break;
            case KendoFilterConstants.LT:
            case KendoFilterConstants.LTE:
                sb.append("[* TO ").append(value).append("]");
                break;
            case KendoFilterConstants.GT:
            case KendoFilterConstants.GTE:
                sb.append("[").append(value).append(" TO *]");
                break;
            case KendoFilterConstants.NEQ:
            case KendoFilterConstants.DOESNOTCONTAIN:
                sb.insert(0, "-").append(value);
                break;
        }
    }

    private Object increaseValue(Class type, Object value) {
        if (value instanceof Long) {
            return ((Long) value) + 1;
        } else if (value instanceof Integer) {
            return ((Integer) value) + 1;
        } else if (value instanceof LocalDateTime) {
            return ((LocalDateTime) value).minus(1, ChronoUnit.SECONDS);
        }
        throw new RuntimeException("Unable to parse increase-support value " + value + " from type " + type);
    }

    private Object decreaseValue(Class type, Object value) {
        if (value instanceof Long) {
            return ((Long) value) - 1;
        } else if (value instanceof Integer) {
            return ((Integer) value) - 1;
        } else if (value instanceof LocalDateTime) {
            return ((LocalDateTime) value).minus(1, ChronoUnit.SECONDS);
        }
        throw new RuntimeException("Unable to parse increase-support value " + value + " from type " + type);
    }

    private CatFileFieldType convertToCatFileFieldType(String dtoField) {
        return FileDtoFieldConverter.convertToCatFileField(dtoField);
    }

    private Object parseFilterValue(FilterDescriptor filter, CatFileFieldType dtoField, Object value) {
        String operator = filter.getOptionalOperator().orElse(KendoFilterConstants.EQ);
        try {
            if (value instanceof FileType) {
                return ((FileType) value).toInt();
            }
            if (value instanceof String && "*".equals(value)) {
                if (KendoFilterConstants.EQ.equalsIgnoreCase(operator)) { //For now * means any
                    return value;
                }
                if (KendoFilterConstants.NEQ.equalsIgnoreCase(operator)) { //For now * means any
                    return value;
                }
            }

            switch (dtoField) {
                case MEDIA_TYPE:
                    String mediaTypeStr = value.toString();
                    return MediaType.valueOf(mediaTypeStr).getValue();
                case SIZE_PARTITION:
                    FileSizePartitionDto part = fileSizeRangeService.getPartition(convertToId(value));
                    return SolrFileSizeQueryUtils.getFileSizeFilter(part);
                case TYPE:
                    if (value instanceof Collection<?>) {
                        return ((Collection<FileType>) value).stream().map(t ->
                                String.valueOf(FileType.valueOf(t.toString()).toInt())).collect(
                                Collectors.joining(" ", "(", ")"));
                    }
                    return FileType.valueOf(value.toString()).toInt();
//                case PRIORITY_SCOPED_TAGS:
//                    return solrSpecificationFactory.convertScopedTagIdToFilterValue(value.toString());
//                case SCOPED_TAGS:
//                    return solrSpecificationFactory.convertExactScopedTagIdToFilterValue(value.toString());
                case ITEM_TYPE:
                    if (value instanceof Collection<?>) {
                        return ((Collection<ItemType>) value).stream().map(t ->
                                String.valueOf(ItemType.valueOf(t.toString()).toInt())).collect(
                                Collectors.joining(" ", "(", ")"));
                    }
                    return ItemType.valueOf(value.toString()).toInt();
                case TAGS:
                    return solrSpecificationFactory.convertScopedTagIdToFilterValue(value.toString());
                case TAG_NAME:
                    return solrSpecificationFactory.convertScopedTagNameToFilterValue(value.toString());
                case TAG_TYPE:
                    return solrSpecificationFactory.convertScopedTagTypeToFilterValue(value.toString());
                case TAG_TYPE_NAME:
                    return solrSpecificationFactory.convertScopedTagTypeNameToFilterValue(value.toString());
                case BIZLIST_ITEM_ASSOCIATION:
                    return solrSpecificationFactory.convertBizListItemIdToFilterValue(value.toString()) + FileCatUtils.CATEGORY_SEPARATOR + "*";
                case BIZLIST_ITEM_ASSOCIATION_TYPE:
                    return solrSpecificationFactory.convertBizListItemTypeToFilterValue(value.toString()) + FileCatUtils.CATEGORY_SEPARATOR + "*";
                case FUNCTIONAL_ROLE_ASSOCIATION:
                    return AssociationIdUtils.createFunctionalRoleIdentfier(Long.valueOf(value.toString())) + FileCatUtils.CATEGORY_SEPARATOR + "*";
                case FUNCTIONAL_ROLE_ASSOCIATION_NAME:
                    return solrSpecificationFactory.convertFuncRoleNameToFilterValue(value.toString());
                case FOLDER_IDS:
                    return solrSpecificationFactory.convertFolderIdToFilterValue(Long.valueOf(value.toString()));
                case DOC_TYPES:
                    return solrSpecificationFactory.convertScopedDocTypesToFilterValue(value.toString());
                case DOC_TYPE_PARENTS:
                    return solrSpecificationFactory.convertDocTypeParentsToFilterValue(convertToId(value));
                case DOC_TYPE_PARENTS_NAME:
                    return solrSpecificationFactory.convertScopedDocTypesNameToFilterValue(value.toString());
                case BIZLIST:
                    return solrSpecificationFactory.convertBizListIdToFilterValue(Long.valueOf(value.toString()));
                case BIZLIST_ITEM_EXTRACTION:
                    return solrSpecificationFactory.convertBizListItemIdToFilterValue(value.toString());
                case BIZLIST_TYPE:
                    return solrSpecificationFactory.convertBizListTypeToFilterValue(value.toString());
                case EXTRACTION_RULE:
                    return solrSpecificationFactory.convertBizListExtractionRuleToFilterValue(value.toString());
                case DEPARTMENT_ID:
                    return solrSpecificationFactory.convertDepartmentToFilterValue(convertToId(value));
                case DEPARTMENT_PARENTS:
                    return solrSpecificationFactory.convertDepartmentParentsToFilterValue(convertToId(value), false);
                case DEPARTMENT_PARENTS_NAME:
                    return solrSpecificationFactory.convertDepartmentParentsToFilterValue(value.toString());
                case MATCHED_PATTERNS_SINGLE_NAME:
                    return solrSpecificationFactory.convertPatternMatchNameToFilterValue(value.toString());
                case FILE_TYPE_CATEGORY_NAME:
                    return solrSpecificationFactory.convertFileTypeCategoryNameToFilterValue(value.toString());
                case MATCHED_PATTERNS_MULTI_NAME:
                    return solrSpecificationFactory.convertPatternMatchNameToFilterValue(value.toString());
                case CREATION_DATE:
                case LAST_MODIFIED:
                case LAST_ACCESS:
                    if (value instanceof DateRangeItemDto) {
                        DateRangeItemDto rangeItemDto = (DateRangeItemDto) value;
                        return solrSpecificationFactory.buildDateTimeFilter(rangeItemDto.getStart(), rangeItemDto.getEnd());
                    } else if (value instanceof Map) {
                        Map rangeItem = (Map) value;
                        DateRangePointDto start = convertMapToDateRangePointDto((Map) rangeItem.get("start"));
                        DateRangePointDto end = convertMapToDateRangePointDto((Map) rangeItem.get("end"));
                        return solrSpecificationFactory.buildDateTimeFilter(start, end);
                    }
                    if (KendoFilterConstants.EQ.equalsIgnoreCase(operator) || KendoFilterConstants.NEQ.equalsIgnoreCase(operator)) {
                        return solrSpecificationFactory.convertSimpleDateRangeToFilterValue(value.toString());
                    }
                    break;
                case LAST_METADATA_CNG_DATE:
                    long from = 0;
                    long to = System.currentTimeMillis();
                    if (value instanceof DateRangeItemDto) {
                        DateRangeItemDto rangeItemDto = (DateRangeItemDto) value;
                        from = (rangeItemDto.getStart() == null ? from : rangeItemDto.getStart().getAbsoluteTime());
                        to = (rangeItemDto.getEnd() == null ? to : rangeItemDto.getEnd().getAbsoluteTime());
                    } else if (value instanceof Map) {
                        Map rangeItem = (Map) value;
                        DateRangePointDto start = convertMapToDateRangePointDto((Map) rangeItem.get("start"));
                        DateRangePointDto end = convertMapToDateRangePointDto((Map) rangeItem.get("end"));
                        from = (start == null ? from : start.getAbsoluteTime());
                        to = (end == null ? to : end.getAbsoluteTime());
                    }
                    return solrSpecificationFactory.buildNumberRangeFilter(from, to);
                case INITIAL_SCAN_DATE:
                    return convertToLocalDateTime(value, false);
                case OWNER:
                    if (solrSpecificationFactory.isOwnerDbWithQuotes() && !value.toString().startsWith("\"")) {
                        value = "\"" + value.toString() + "\"";
                    }
                    break;
                case ROOT_FOLDER_NAME_LIKE:
                    List<Long> foldersIds;
                    String namingSearchTerm = value.toString();

                    if (namingSearchTerm != null) {
                        try {
                            String namingSearchTermDecoded = java.net.URLDecoder.decode(namingSearchTerm, "UTF-8");
                            namingSearchTerm = namingSearchTermDecoded.trim().toLowerCase();
                        } catch (Exception e) {
                            namingSearchTerm = namingSearchTerm.trim().toLowerCase();
                        }

                        // escape slashes correctly for sql query
                        namingSearchTerm = CriteriaQueryUtils.convertSlashesForQuerySearch(namingSearchTerm);
                    }

                    foldersIds = docStoreService.listRootFoldersIds(namingSearchTerm);
                    if (foldersIds == null || foldersIds.isEmpty()) {
                        return "(0)";
                    }
                    return foldersIds.stream().map(String::valueOf).collect(Collectors.joining(" ", "(", ")"));
                case WORKFLOW_DATA:
                    return value;
            }
            if (dtoField.getType().equals(LocalDateTime.class)) {
                return convertToLocalDateTime(value, true);
            }
            if (value instanceof String) {
                String stringValue = (String) value;
                if ("*".equals(value)) {
                    if (KendoFilterConstants.EQ.equalsIgnoreCase(operator)) { //For now * means any
                        return value;
                    }
                    if (KendoFilterConstants.NEQ.equalsIgnoreCase(operator)) { //For now * means any
                        return value;
                    }
                }

                if (KendoFilterConstants.CONTAINS.equalsIgnoreCase(operator) && stringValue.contains(" ")) {
                    value = SolrQueryGenerator.escape(stringValue, false, dtoField.equals(FULL_NAME_SEARCH));
                } else {
                    value = ClientUtils.escapeQueryChars(stringValue);
                }
            }
            return value;
        } catch (NumberFormatException e) {
            throw new BadRequestException(dtoField.name(), value.toString(), "Bad filter request - should be number", BadRequestType.UNSUPPORTED_VALUE);
        }
    }

    private Long convertToId(Object value) {
        return Long.valueOf(value.toString());
    }

    private Object convertToLocalDateTime(Object value, boolean isSeconds) {
        LocalDateTime parsed;
        if (value.toString().contains(":")) {
            parsed = LocalDateTime.parse(value.toString(), solrDateFormat);
        } else if (isSeconds) {
            parsed = LocalDateTime.ofEpochSecond(Long.valueOf(value.toString()), 0, ZoneOffset.UTC);
        } else {
            parsed = LocalDateTime.ofInstant(Instant.ofEpochMilli(Long.valueOf(value.toString())), ZoneOffset.UTC);
        }
        return parsed;
    }

    public static DateRangePointDto convertMapToDateRangePointDto(Map dateRangePointDtoMap) {
        if (dateRangePointDtoMap == null) {
            return null;
        }
        return ModelDtoConversionUtils.getMapper().convertValue(dateRangePointDtoMap, DateRangePointDto.class);
    }
}
