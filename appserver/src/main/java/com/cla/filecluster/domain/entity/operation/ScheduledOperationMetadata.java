package com.cla.filecluster.domain.entity.operation;

import com.cla.common.domain.dto.schedule.ScheduledOperationPriority;
import com.cla.common.domain.dto.schedule.ScheduledOperationState;
import com.cla.common.domain.dto.schedule.ScheduledOperationType;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Map;
import com.cla.common.predicate.*;

/**
 * Data saved in db to allow recovery for large operations on groups
 * we make sure to know which step was performed and what should be done
 *
 * Created by: yael
 * Created on: 5/31/2018
 */
@Entity
@Table(name = "scheduled_operations")
public class ScheduledOperationMetadata {

    @Id
    @Access(AccessType.PROPERTY)
    @GeneratedValue
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false, name = "operation_type")
    private ScheduledOperationType operationType;

    private int step;

    @Type(type = "json")
    @Column(columnDefinition = "json", name = "data")
    private Map<String,String> opData;

    @Column(name = "create_time")
    private Long createTime;

    @Column(name = "start_time")
    private Long startTime;

    @Column(name = "update_time")
    private Long updateTime;

    private boolean isUserOperation;

    private Integer retryLeft;

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "priority")
    private ScheduledOperationPriority priority;

    @Type(type = "json")
    @Column(columnDefinition = "json")
    private KeyValuePredicate opCondition;

    private Long ownerId;

    private String description;

    @Enumerated(EnumType.STRING)
    private ScheduledOperationState state;

    @Column(name = "should_display")
    private Boolean shouldDisplay;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ScheduledOperationType getOperationType() {
        return operationType;
    }

    public void setOperationType(ScheduledOperationType operationType) {
        this.operationType = operationType;
    }

    public int getStep() {
        return step;
    }

    public void setStep(int step) {
        this.step = step;
    }

    public Map<String, String> getOpData() {
        return opData;
    }

    public void setOpData(Map<String, String> opData) {
        this.opData = opData;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public boolean getUserOperation() {
        return isUserOperation;
    }

    public void setUserOperation(boolean userOperation) {
        isUserOperation = userOperation;
    }

    public ScheduledOperationPriority getPriority() {
        return priority;
    }

    public void setPriority(ScheduledOperationPriority priority) {
        this.priority = priority;
    }

    public Integer getRetryLeft() {
        return retryLeft;
    }

    public void setRetryLeft(Integer retryLeft) {
        this.retryLeft = retryLeft;
    }

    public KeyValuePredicate getOpCondition() {
        return opCondition;
    }

    public void setOpCondition(KeyValuePredicate opCondition) {
        this.opCondition = opCondition;
    }

    public Long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(Long ownerId) {
        this.ownerId = ownerId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ScheduledOperationState getState() {
        return state;
    }

    public void setState(ScheduledOperationState state) {
        this.state = state;
    }

    public Long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Long updateTime) {
        this.updateTime = updateTime;
    }

    @PreUpdate
    protected void setLastUpdate() {
        updateTime = System.currentTimeMillis();
    }

    public Boolean getShouldDisplay() {
        return shouldDisplay;
    }

    public void setShouldDisplay(Boolean shouldDisplay) {
        this.shouldDisplay = shouldDisplay;
    }

    @Override
    public String toString() {
        return "ScheduledOperationMetadata{" +
                "id=" + id +
                ", operationType=" + operationType +
                ", step=" + step +
                ", opData=" + opData +
                ", createTime=" + createTime +
                ", startTime=" + startTime +
                ", isUserOperation=" + isUserOperation +
                ", retryLeft=" + retryLeft +
                ", priority=" + priority +
                ", opCondition=" + opCondition +
                ", ownerId=" + ownerId +
                ", description='" + description + '\'' +
                ", state='" + state + '\'' +
                '}';
    }
}
