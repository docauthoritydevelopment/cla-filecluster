package com.cla.filecluster.domain.entity.searchpatterns;

import com.google.common.collect.Sets;
import javax.persistence.*;
import java.util.Date;
import java.util.Set;

/**
 * Created by ophir on 7/04/2019.
 */

@Entity
public class PatternCategory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    private Integer id;

    private String name;

    private boolean active;

    @ManyToOne(fetch = FetchType.EAGER)
    private PatternCategory parent;

    @ManyToMany(mappedBy = "subCategories")
    private Set<TextSearchPattern> patterns = Sets.newHashSet();

    @Column(updatable=false)
    private Long createdOnDB;

    @PrePersist
    private void setCreatedOnDB(){
        createdOnDB = System.currentTimeMillis();
    }

    public Long getCreatedOnDB() {
        return createdOnDB;
    }

    public void setCreatedOnDB(Long createdOnDB) {
        this.createdOnDB = createdOnDB;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PatternCategory getParent() {
        return parent;
    }

    public void setParent(PatternCategory parentId) {
        this.parent = parentId;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Set<TextSearchPattern> getPatterns() {
        return patterns;
    }

    @Override
    public String toString() {
        return "PatternCategory{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", active=" + active +
                ", parentId=" + ((parent==null)?"null":parent.getId()) +
                '}';
    }

    public void setPatterns(Set<TextSearchPattern> patterns) {
        this.patterns = patterns;
    }
}
