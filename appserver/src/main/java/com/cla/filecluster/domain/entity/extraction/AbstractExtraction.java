package com.cla.filecluster.domain.entity.extraction;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

@MappedSuperclass
public abstract class AbstractExtraction {

	@Id
	@GeneratedValue
	private Integer id;

	@NotNull
	private long claFileId;

	@NotNull
	@Length(max=4096)
	private String claFileName;
	
	private String claFilePart;
	
	@Column(columnDefinition = "char(36)")
	private String groupId;
	
	private String groupName = "UN_GROUPED";
	
	public AbstractExtraction() {
	}


	public AbstractExtraction(final long claFileId, final String claFileName, final String claFilePart) {
		this.claFileId = claFileId;
		this.claFileName = claFileName;
		this.claFilePart = claFilePart;
	}


	public Integer getId() {
		return id;
	}


	public void setId(final Integer id) {
		this.id = id;
	}


	public long getClaFileId() {
		return claFileId;
	}


	public void setClaFileId(final long claFileId) {
		this.claFileId = claFileId;
	}


	public String getClaFileName() {
		return claFileName;
	}


	public void setClaFileName(final String claFileName) {
		this.claFileName = claFileName;
	}


	public String getClaFilePart() {
		return claFilePart;
	}


	public void setClaFilePart(final String claFilePart) {
		this.claFilePart = claFilePart;
	}


	public String getGroupId() {
		return groupId;
	}


	public void setGroupId(final String groupId) {
		this.groupId = groupId;
	}


	public String getGroupName() {
		return groupName;
	}


	public void setGroupName(final String groupName) {
		this.groupName = groupName;
	}


	
	

	


}
