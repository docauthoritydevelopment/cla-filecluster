package com.cla.filecluster.domain.dto;

/*
 * Summary of ACLs, biz roles and functional roles, as well as system user indicators and other relevant flags.
 */
import com.google.common.collect.Sets;

import java.util.Set;

@SuppressWarnings("SameParameterValue")
public class AccessState {
    private boolean accessDenied = true;
    private boolean allowAllFiles = false;
    private boolean allowPermittedFiles = false;
    // ACLs
    private Set<String> accessTokens = Sets.newHashSet();
    // functional roles that give their owner a right to see any file regardless of ACLs
    private Set<String> allowAllFuncRoles = Sets.newHashSet();
    // functional roles that give their owner a right to see files that are allowed for him according to ACLs
    private Set<String> allowPermittedFuncRoles = Sets.newHashSet();

    private Set<Long> deniedRootFolders = Sets.newHashSet();

    public boolean isAccessDenied() {
        return accessDenied;
    }
    public void setAccessDenied(boolean accessDenied) {
        this.accessDenied = accessDenied;
    }
    public boolean isAllowAllFiles() {
        return allowAllFiles;
    }
    public void setAllowAllFiles(boolean allowAllFiles) {
        this.allowAllFiles = allowAllFiles;
    }
    public boolean isAllowPermittedFiles() {
        return allowPermittedFiles;
    }
    public void setAllowPermittedFiles(boolean allowPermittedFiles) {
        this.allowPermittedFiles = allowPermittedFiles;
    }
    public Set<String> getAccessTokens() {
        return accessTokens;
    }
    public Set<String> getAllowAllFuncRoles() {
        return allowAllFuncRoles;
    }
    public Set<String> getAllowPermittedFuncRoles() {
        return allowPermittedFuncRoles;
    }
    public Set<Long> getDeniedRootFolders() {
        return deniedRootFolders;
    }
    public void setDeniedRootFolders(Set<Long> deniedRootFolders) {
        this.deniedRootFolders = deniedRootFolders;
    }
}
