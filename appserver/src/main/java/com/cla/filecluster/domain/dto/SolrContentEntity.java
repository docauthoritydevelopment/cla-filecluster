package com.cla.filecluster.domain.dto;

import org.apache.solr.client.solrj.beans.Field;

import java.util.Date;
import java.util.List;

public class SolrContentEntity implements SolrEntity {
	@Field
	private String id;

	@Field
	private String fileName;

	@Field
	private int type;

	@Field
	private String part;	// "" or null for Word, sheet name for Excel

	@Field
	private String content;

	@Field
	private String groupId;

	@Field
	private Date creationDate;

	@Field
	private Date modificationDate;

	@Field("last_record_update")
	private Date lastRecordUpdate;

	@Field
	private Date accessDate;

	@Field
	private String jobId;

	@Field
	private List<String> extractedEntities;

	@Field
	private List<String> extractedPatterns;

	@Field
	private List<String> allowTokenDocuments;

	@Field
	private List<String> allowTokenParents;

	@Field
	private List<String> allowTokenShares;

	@Field
	private List<String> denyTokenDocuments;

	@Field
	private List<String> denyTokenParents;

	@Field
	private List<String> denyTokenShares;

	private static final String[] fieldNamesNoContent = new String[]{
		"id", "fileName", "type", "part",
		// "content",
		"groupId",
		"creationDate", "modificationDate", "accessDate", "jobId", "extractedEntities", "extractedPatterns",
		"allowTokenDocuments", "allowTokenParents", "allowTokenShares", "denyTokenDocuments", "denyTokenParents", "denyTokenShares"
	};

	public SolrContentEntity() {
	}

	public SolrContentEntity(final Long id, final String fileName, final int type, final String content, Date modificationDate) {
		this(id.toString(), fileName,null, type, content, modificationDate);
	}
	public SolrContentEntity(final String id, final String fileName, final int type, final String content, Date modificationDate) {
		this(id, fileName,null, type, content, modificationDate);
	}
	public SolrContentEntity(final Long id, final String fileName, final String part, final int type, final String content, Date modificationDate) {
		this(id.toString(), fileName, part, type, content, modificationDate);
	}
	public SolrContentEntity(final String id, final String fileName, final String part, final int type, final String content, Date modificationDate) {
		this.id = id;
		this.fileName = fileName;
		this.part = part;
		this.type = type;
		this.content = content;
		this.modificationDate = modificationDate;
		this.lastRecordUpdate = new Date();
//		this.content = content.replaceAll("-", "M");	// becomes redundant after hash codes are set as hex numbers
	}

	public String getIdAsString() {return id;}

	public String getId() {
		return id;
	}

	public String getFileName() {
		return fileName;
	}

	public int getType() {
		return type;
	}

	public String getPart() {
		return part;
	}

	public String getContent() {
		return content;
	}
	
	public static String[] getFieldnamesnocontent() {
		return fieldNamesNoContent;
	}

	public Date getModificationDate() { return modificationDate; }

	public void setModificationDate(Date modificationDate) { this.modificationDate = modificationDate; }

	public Date getLastRecordUpdate() { return lastRecordUpdate; }

	public void setLastRecordUpdate(Date lastRecordUpdate) { this.lastRecordUpdate = lastRecordUpdate; }

	@Override
	public String toString() {
		return "{"+id+"="+fileName+((part==null)?"":("|"+part))+"}";
	}

}
