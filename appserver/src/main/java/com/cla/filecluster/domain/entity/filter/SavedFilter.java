package com.cla.filecluster.domain.entity.filter;

import com.cla.common.domain.dto.kendo.FilterDescriptor;
import com.cla.filecluster.domain.entity.scope.BasicScope;
import com.cla.filecluster.domain.entity.security.User;
import com.drew.lang.annotations.Nullable;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;

/**
 * Created by uri on 08/11/2015.
 */
@Entity
@Table(name = "saved_filters")

public class SavedFilter {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotEmpty
    private String name;

    @Column(length = 4096)
    private String rawFilter;

    @Type(type = "json")
    @Column(columnDefinition = "json")
    private FilterDescriptor filterDescriptor;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "scope_id")
    private BasicScope scope;

    @Nullable
    @ManyToOne(optional = true,fetch = FetchType.EAGER)
    private User owner;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRawFilter() {
        return rawFilter;
    }

    public void setRawFilter(String rawFilter) {
        this.rawFilter = rawFilter;
    }

    public FilterDescriptor getFilterDescriptor() {
        return filterDescriptor;
    }

    public void setFilterDescriptor(FilterDescriptor filterDescriptor) {
        this.filterDescriptor = filterDescriptor;
    }

    public BasicScope getScope() {
        return scope;
    }

    public void setScope(BasicScope scope) {
        this.scope = scope;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }
}
