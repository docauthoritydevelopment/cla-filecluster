package com.cla.filecluster.domain.entity.filetree;

import com.cla.common.domain.dto.filetree.PathDescriptorType;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Created by uri on 11/08/2015.
 */
@Entity
@Table(name = "doc_store")
public class DocStore implements Serializable {
	
	// TODO: missing @Filter definition to utilise the account field
    @TableGenerator(name = "doc-store-table-hilo-generator", allocationSize = 5)
    @GeneratedValue(generator="doc-store-table-hilo-generator", strategy = GenerationType.TABLE)
    @Id
    private Long id;

    @Enumerated(value = EnumType.STRING)
    private PathDescriptorType pathDescriptorType;

    private String description;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "docStoreInclude")
    private List<RootFolder> rootFolders;

//    @OneToMany(fetch = FetchType.LAZY, mappedBy = "docStoreIgnore")
//    private List<RootFolder> ignoredRootFolders;

//    public List<RootFolder> getIgnoredRootFolders() {
//        return ignoredRootFolders;
//    }
//
//    public void setIgnoredRootFolders(List<RootFolder> ignoredRootFolders) {
//        this.ignoredRootFolders = ignoredRootFolders;
//    }

    public List<RootFolder> getRootFolders() {
        return rootFolders;
    }

    public void setRootFolders(List<RootFolder> rootFolders) {
        this.rootFolders = rootFolders;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PathDescriptorType getPathDescriptorType() {
        return pathDescriptorType;
    }

    public void setPathDescriptorType(PathDescriptorType pathDescriptorType) {
        this.pathDescriptorType = pathDescriptorType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
