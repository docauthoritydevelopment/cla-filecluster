package com.cla.filecluster.domain.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by uri on 29/03/2016.
 */
@ResponseStatus(value= HttpStatus.BAD_REQUEST, reason="Bad request")
public class BadImportException extends RuntimeException {

    public BadImportException(String message) {
        super(message);
    }

    public BadImportException(String s, Exception e) {
        super(s,e);
    }
}
