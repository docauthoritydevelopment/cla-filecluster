package com.cla.filecluster.policy.plugin.transformer;

import com.cla.common.domain.dto.alert.AlertSeverity;
import com.cla.common.domain.dto.event.DAEvent;
import com.cla.common.domain.dto.event.EventType;
import com.cla.common.domain.dto.event.ScanDiffPayload;
import com.cla.filecluster.domain.entity.alert.Alert;

import java.util.HashMap;
import java.util.Map;

public class AlertTransformer implements ActionPluginInputTransformer<Alert, ScanDiffPayload> {

    public static final String SCAN_DIFF_ALERT_MESSAGE_PATTERN = "%s (path:%s, fileId:%d)";

    private Map<EventType, AlertSeverity> severityMappings = new HashMap<>();

    public Map<EventType, AlertSeverity> getSeverityMappings() {
        return severityMappings;
    }

    public AlertTransformer() {
    }

    public AlertTransformer(Map<EventType, AlertSeverity> severityMappings) {
        this.severityMappings = severityMappings;
    }

    public void setSeverityMappings(Map<EventType, AlertSeverity> severityMappings) {
        this.severityMappings = severityMappings;
    }

    @Override
    public Alert transform(DAEvent<ScanDiffPayload> daEvent) {
        Alert alert = new Alert();
        AlertSeverity alertSeverity = severityMappings.get(daEvent.getEventType());
        if (alertSeverity != null) {
            alert.setSeverity(alertSeverity);
        }
        alert.setDateCreated(daEvent.getReported());
        alert.setEventType(daEvent.getEventType());
        ScanDiffPayload payload = daEvent.getPayload();
        alert.setMessage(String.format(SCAN_DIFF_ALERT_MESSAGE_PATTERN, daEvent.getEventType().getValue(), payload.getPath(), payload.getFileId()));
        return alert;
    }
}
