package com.cla.filecluster.policy.repository.jpa;

import com.cla.filecluster.policy.domain.entity.Policy;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface PolicyRepository extends JpaRepository<Policy, Long> {

    @Query(nativeQuery = true,
            value = "select p.* from policies p where JSON_EXTRACT(p.config,'$.name') = :name")
    Policy getByName(@Param("name") String name);
}
