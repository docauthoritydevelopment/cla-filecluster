package com.cla.filecluster.policy.domain.dto;

import com.cla.common.domain.dto.event.EventType;
import com.cla.common.predicate.KeyValuePredicate;
import com.cla.filecluster.policy.domain.PolicyConfig;

import javax.validation.constraints.NotNull;
import java.util.LinkedList;
import java.util.List;

/**
 * Represnt the data object of a policy for the api controllers
 */
public class PolicyDto {

    private long id;

    @NotNull
    private Long dateCreated;

    @NotNull
    private Long dateModified;

    private PolicyConfig policyConfig;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Long getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Long dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Long getDateModified() {
        return dateModified;
    }

    public void setDateModified(Long dateModified) {
        this.dateModified = dateModified;
    }
}
