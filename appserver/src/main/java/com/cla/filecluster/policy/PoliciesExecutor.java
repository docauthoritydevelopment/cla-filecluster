package com.cla.filecluster.policy;

import com.cla.common.domain.dto.event.DAEvent;
import com.cla.common.domain.dto.event.EventType;
import com.cla.common.domain.dto.messages.action.ActionPluginRequest;
import com.cla.common.domain.dto.messages.action.PluginInput;
import com.cla.filecluster.policy.domain.PolicyConfig;
import com.cla.filecluster.policy.domain.dto.ActionPluginsDefinition;
import com.cla.filecluster.policy.factsenricher.FactsEnricher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.concurrent.BlockingQueue;

/**
 * Receive event from eventQueue and execute all relevant policies according to the policy flow.<br/>
 * 1. Enrich facts into the event via the list of factsEnrichers<br/>
 * 2. Fetch relevant policies via {@link PolicyMappings#getPolicies(EventType)}<br/>
 * 3. Accept or deny the event according to the predicate list
 * 4. If accepted the according to each {@link ActionPluginsDefinition}:<br/>
 * 4.1 Transform the event in the {@link PluginInput}<br/>
 * 4.2 Create {@link ActionPluginRequest} for each defined {@link com.cla.common.domain.dto.messages.action.ActionPluginConfig}
 */
public class PoliciesExecutor extends Thread {

    private static final Logger logger = LoggerFactory.getLogger(PoliciesExecutor.class);
    private final PolicyMappings policyMappings;
    private final BlockingQueue<DAEvent> eventQueue;
    private final List<FactsEnricher> factsEnrichers;
    private final BlockingQueue<ActionPluginRequest> actionPluginRequestQueue;
    private boolean run = true;

    PoliciesExecutor(List<FactsEnricher> factsEnrichers, PolicyMappings policyMappings, BlockingQueue<DAEvent> eventQueue,
                     BlockingQueue<ActionPluginRequest> actionPluginRequestQueue) {
        this.factsEnrichers = factsEnrichers;
        this.policyMappings = policyMappings;
        this.eventQueue = eventQueue;
        this.actionPluginRequestQueue = actionPluginRequestQueue;
    }


    @Override
    public void run() {
        logger.debug("Started policy executor");
        while (run) {
            try {
                DAEvent daEvent = eventQueue.take();
                List<PolicyConfig> policies = policyMappings.getPolicies(daEvent.getEventType());
                if (policies != null) {
                    //noinspection unchecked
                    factsEnrichers.forEach(factsEnricher -> factsEnricher.enrich(daEvent, daEvent.getFactsMap()));
                    policies.forEach(p -> executePolicyFlow(p, daEvent));
                } else {
                    logger.debug("No policies defined for {}", daEvent);
                }
            } catch (InterruptedException e) {
                // while loop will catch and re-try
            } catch (Exception e) {
                logger.error("Failed to process event from queue", e);
            }
        }
        logger.debug("Stopped policy executor");
    }

    private void executePolicyFlow(PolicyConfig policy, DAEvent daEvent) {
        logger.trace("**** Executing policy {} on {}", policy.getName(), daEvent);
        //noinspection unchecked
        boolean passedPredicates = policy.getPredicates().stream().allMatch(p -> p.accept(daEvent.getFactsMap()));
        if (passedPredicates) {
            logger.trace("**** Passed predicates {} on {}, preparing plugin action requests...", policy.getPredicates(), daEvent.getFactsMap());
            policy.getActionPluginsDefinitions().forEach(apd -> sendActionPluginRequest(apd, daEvent));
        } else {
            logger.trace("**** Did not passed predicates {} on {}, skipping", policy.getPredicates(), daEvent.getFactsMap());
        }
    }

    private void sendActionPluginRequest(ActionPluginsDefinition actionPluginsDefinition, DAEvent daEvent) {
        //noinspection unchecked
        PluginInput pluginInput = actionPluginsDefinition.getPluginInputTransformer().transform(daEvent);
        actionPluginsDefinition.getActionPluginConfigs().stream()
                .map(config -> new ActionPluginRequest(pluginInput, config))
                .forEach(this::sendRequest);
    }

    private void sendRequest(ActionPluginRequest pluginActionRequest) {
        logger.trace("**** Sending request for {} with {}", pluginActionRequest.getConfig().getPluginId(), pluginActionRequest.getInput());
        actionPluginRequestQueue.add(pluginActionRequest);
    }

    public void shutdown() {
        this.run = false;
        interrupt();
        logger.trace("Marked policy executor to stop");
    }
}
