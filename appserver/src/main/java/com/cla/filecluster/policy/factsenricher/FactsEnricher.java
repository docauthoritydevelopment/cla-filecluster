package com.cla.filecluster.policy.factsenricher;

import com.cla.common.domain.dto.event.DAEvent;
import com.cla.common.domain.dto.event.EventPayload;

import java.util.Map;

/**
 * Interface for providing additional facts to events, mainly used by the policy predicates.<br/>
 * e.g. {@link SolrFactsEnricher}
 * @param <P>
 */
public interface FactsEnricher<P extends EventPayload> {

    void enrich(DAEvent<P> daEvent, Map<String, Object> factsMap);

    boolean validateEvent(DAEvent<P> daEvent);

}
