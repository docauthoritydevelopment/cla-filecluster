package com.cla.filecluster.policy.domain.entity;

import com.cla.filecluster.domain.entity.BasicEntity;
import com.cla.filecluster.policy.domain.PolicyConfig;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Represent the policy entity in our db
 * Currently we did not modeled our {@link PolicyConfig} in the DB (time constraints & overhead) so we save it as json
 */
@Entity
@Table(name = "policies")
public class Policy extends BasicEntity {

    @Type(type = "json")
    @Column(columnDefinition = "json")
    private PolicyConfig config;

    public PolicyConfig getPolicyConfig() {
        return config;
    }

    public void setConfig(PolicyConfig config) {
        this.config = config;
    }
}
