package com.cla.filecluster.policy.domain.dto;

import com.cla.common.domain.dto.messages.action.ActionPluginConfig;
import com.cla.filecluster.policy.plugin.transformer.ActionPluginInputTransformer;

import java.util.LinkedList;
import java.util.List;

public class ActionPluginsDefinition {

    private ActionPluginInputTransformer pluginInputTransformer;

    private List<ActionPluginConfig> actionPluginConfigs = new LinkedList<>();

    public ActionPluginInputTransformer getPluginInputTransformer() {
        return pluginInputTransformer;
    }

    public void setPluginInputTransformer(ActionPluginInputTransformer pluginInputTransformer) {
        this.pluginInputTransformer = pluginInputTransformer;
    }

    public List<ActionPluginConfig> getActionPluginConfigs() {
        return actionPluginConfigs;
    }

    public void setActionPluginConfigs(List<ActionPluginConfig> actionPluginConfigs) {
        this.actionPluginConfigs = actionPluginConfigs;
    }
}
