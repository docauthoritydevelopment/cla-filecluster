package com.cla.filecluster.policy.factsenricher;

import com.cla.common.domain.dto.event.DAEvent;
import com.cla.common.domain.dto.event.EventType;
import com.cla.common.domain.dto.event.ScanDiffPayload;
import com.cla.filecluster.config.security.AutoAuthenticate;
import com.cla.filecluster.service.categories.FileCatService;
import org.apache.solr.common.SolrDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * Responsible for enriching scan diff events facts from solr (such as tags, parentsInfo...)
 */
@Component
public class SolrFactsEnricher implements FactsEnricher<ScanDiffPayload> {

    private static final Logger logger = LoggerFactory.getLogger(SolrFactsEnricher.class);

    @Autowired
    private FileCatService fileCatService;

    @Override
    @AutoAuthenticate
    public void enrich(DAEvent<ScanDiffPayload> daEvent, Map<String, Object> factsMap) {
        if (!validateEvent(daEvent)){
            return;
        }

        SolrDocument categoryFile = fileCatService.findCategoryFile(daEvent.getPayload().getFileId());
        if (categoryFile == null) {
            if (daEvent.getEventType() != EventType.SCAN_DIFF_NEW) {
                logger.trace("Cannot enrich file (id:{}, path:{}), not found in solr", daEvent.getPayload().getFileId(), daEvent.getPayload().getPath());
            }
            return;
        }

        categoryFile.getFieldNames().forEach(f -> factsMap.put(f, categoryFile.get(f)));
        logger.trace("Enriched {}", daEvent);

    }

    @Override
    public boolean validateEvent(DAEvent<ScanDiffPayload> daEvent) {
        if (daEvent == null){
            logger.warn("Got null Scan Diff Event, cannot enrich.");
            return false;
        }
        if (daEvent.getPayload() == null || daEvent.getPayload().getFileId() == null){
            // TODO: should handle this better moving forward: identify and troubleshoot if happens at the field
            logger.trace("Got invalid event payload, cannot enrich {}", daEvent);
            return false;
        }
        return true;
    }
}
