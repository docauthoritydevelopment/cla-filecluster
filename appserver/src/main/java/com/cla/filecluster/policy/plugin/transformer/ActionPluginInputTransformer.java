package com.cla.filecluster.policy.plugin.transformer;

import com.cla.common.domain.dto.event.DAEvent;
import com.cla.common.domain.dto.event.EventPayload;
import com.cla.common.domain.dto.messages.action.PluginInput;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "@type")
public interface ActionPluginInputTransformer<T extends PluginInput, P extends EventPayload> {

    T transform(DAEvent<P> daEvent);

}
