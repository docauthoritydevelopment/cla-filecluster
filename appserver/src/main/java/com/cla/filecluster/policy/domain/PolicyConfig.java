package com.cla.filecluster.policy.domain;

import com.cla.common.domain.dto.event.EventType;
import com.cla.common.predicate.KeyValuePredicate;
import com.cla.filecluster.policy.domain.dto.ActionPluginsDefinition;

import java.util.LinkedList;
import java.util.List;

/**
 * Represent Config for policy including: relvantTypes, predicates and action plugins
 */
public class PolicyConfig {

    private List<EventType> relevantEventTypes = new LinkedList<>();

    private List<KeyValuePredicate> predicates = new LinkedList<>();

    private String name;

    private List<ActionPluginsDefinition> actionPluginsDefinitions = new LinkedList<>();

    public List<EventType> getRelevantEventTypes() {
        return relevantEventTypes;
    }

    public void setRelevantEventTypes(List<EventType> relevantEventTypes) {
        this.relevantEventTypes = relevantEventTypes;
    }

    public List<KeyValuePredicate> getPredicates() {
        return predicates;
    }

    public void setPredicates(List<KeyValuePredicate> predicates) {
        this.predicates = predicates;
    }

    public List<ActionPluginsDefinition> getActionPluginsDefinitions() {
        return actionPluginsDefinitions;
    }

    public void setActionPluginsDefinitions(List<ActionPluginsDefinition> actionPluginsDefinitions) {
        this.actionPluginsDefinitions = actionPluginsDefinitions;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
