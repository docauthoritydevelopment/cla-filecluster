package com.cla.filecluster.policy;

import com.cla.common.domain.dto.event.EventType;
import com.cla.common.predicate.KeyValuePredicate;
import com.cla.common.utils.AssociationIdUtils;
import com.cla.filecluster.domain.entity.filetag.FileTag;
import com.cla.filecluster.domain.entity.filetag.FileTagType;
import com.cla.filecluster.policy.domain.PolicyConfig;
import com.cla.filecluster.policy.domain.entity.Policy;
import com.cla.filecluster.policy.repository.jpa.PolicyRepository;
import com.cla.filecluster.service.filetag.FileTagService;
import com.cla.filecluster.service.filetag.FileTagTypeService;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.stream.Collectors;

/**
 * Load at boot time all configured policies serve as an api to get relevant policies according to event type
 * {@link PolicyMappings#getPolicies(EventType)}
 */
@Component
public class PolicyMappings {

    private static final Logger logger = LoggerFactory.getLogger(PolicyMappings.class);

    private String ALERT_POLICY_TAG_PLACE_HOLDER = "%ALERT_POLICY%";

    private static String TAG_TYPE_AND_TAG_VALUE_SEPARATOR = "\\|";

    @Autowired
    private FileTagService fileTagService;

    @Autowired
    private FileTagTypeService fileTagTypeService;

    @Autowired
    private PolicyRepository policyRepository;

    private ReadWriteLock lock = new ReentrantReadWriteLock();

    // | separates between tag type and tag value
    @Value("${policy.alert.tagIdentifier:Policy|Alert}")
    private String policyAlertTagIdentifier;

    private ListMultimap<EventType, PolicyConfig> policiesByEventTypes = ArrayListMultimap.create();
    private String policyTagTypeName;
    private String alertTagName;

    @PostConstruct
    private void init() throws IOException {
        String[] typeAndValue = policyAlertTagIdentifier.split(TAG_TYPE_AND_TAG_VALUE_SEPARATOR);
        policyTagTypeName = typeAndValue[0].trim();
        alertTagName = typeAndValue[1].trim();
        reloadPolicies();
    }

    public void reloadPolicies() {
        List<Policy> policies = policyRepository.findAll();
        if (policies == null || policies.isEmpty()) {
            logger.warn("No policies defined in DB, events will not invoke any actions");
            return;
        }

        logger.info("Loading {} policies", policies.size());
        loadPolicies(policies.stream().map(p -> p.getPolicyConfig()).collect(Collectors.toList()));
    }

    public void loadPolicies(List<PolicyConfig> policies) {
        lock.writeLock().lock();
        try {
            for (PolicyConfig policy : policies) {
                configurePolicy(policy);
                logger.info("Connecting policy \"{}\" to events: {}", policy.getName(), policy.getRelevantEventTypes());
                for (EventType eventType : policy.getRelevantEventTypes()) {
                    policiesByEventTypes.put(eventType, policy);
                }
            }
        } finally {
            lock.writeLock().unlock();
        }
    }

    private void configurePolicy(PolicyConfig policy) {
        configurePolicyPredicates(policy);
    }

    private void configurePolicyPredicates(PolicyConfig policy) {
        logger.info("Configuring policy \"{}\" predicates: {}", policy.getName(), policy.getPredicates());
        List<KeyValuePredicate> updatedPredicates = policy.getPredicates().stream()
                .map(prd -> buildNewPredicateIfNeeded(prd))
                .collect(Collectors.toList());
        policy.setPredicates(updatedPredicates);
    }

    private KeyValuePredicate buildNewPredicateIfNeeded(KeyValuePredicate prd) {
        KeyValuePredicate result = prd;
        Object desiredValue = prd.getDesiredValue();
        // Case the value of the predicate is a list search inside for place holders to replace
        if (desiredValue instanceof List) {
            List ls = (List) desiredValue;
            Object newDesiredValue = ls.stream().
                    map(v -> v instanceof String ? replacePolicyAlertPlaceHolder((String) v) : v)
                    .collect(Collectors.toList());
            result = new KeyValuePredicate(prd.getKey(), prd.getCompareOperation(), newDesiredValue);
        }
        // Case String search place holder to replace
        else if (desiredValue instanceof String) {
            result = new KeyValuePredicate(prd.getKey(), prd.getCompareOperation(), replacePolicyAlertPlaceHolder((String) desiredValue));
        }
        return result;
    }

    private String replacePolicyAlertPlaceHolder(String value) {
        if (value.contains(ALERT_POLICY_TAG_PLACE_HOLDER)) {
            logger.debug("Found {} place holder, replace it with {}", ALERT_POLICY_TAG_PLACE_HOLDER, policyAlertTagIdentifier);
            FileTagType policyTagType = fileTagTypeService.getFileTagTypeByName(this.policyTagTypeName);
            if (policyTagType == null) {
                logger.error("Alerts will not function!! failed to replace {}, reason: there is no policy tag type by the name {}", ALERT_POLICY_TAG_PLACE_HOLDER, this.policyTagTypeName);
                return value;
            }
            FileTag alertTag = fileTagService.getFileTagByName(alertTagName, policyTagType.getId());
            if (alertTag == null) {
                logger.error("Alerts will not function!! failed to replace {}, reason: there is no alert tag by the name {}", ALERT_POLICY_TAG_PLACE_HOLDER, this.policyTagTypeName);
                return value;
            }
            String replacedValue = value.replaceAll(ALERT_POLICY_TAG_PLACE_HOLDER, AssociationIdUtils.createTagIdentifier(alertTag));
            logger.debug("**** Replaced predicate value {} with {}", value, replacedValue);
            return replacedValue;
        }
        return value;
    }

    /**
     * Get relevant configured policies according to event type
     *
     * @param eventType
     * @return relevant configured policies
     */
    public List<PolicyConfig> getPolicies(EventType eventType) {
        lock.readLock().lock();
        try {
            return policiesByEventTypes.get(eventType);
        } finally {
            lock.readLock().unlock();
        }
    }

}
