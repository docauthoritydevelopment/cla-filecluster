package com.cla.filecluster.policy;

import com.cla.common.domain.dto.event.DAEvent;
import com.cla.common.domain.dto.messages.action.ActionPluginRequest;
import com.cla.filecluster.policy.factsenricher.FactsEnricher;
import com.cla.filecluster.policy.factsenricher.SolrFactsEnricher;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.BlockingQueue;


/**
 * Responsible for activating async policy executors as configured in policyEngine.concurrency application.properties
 * The policy executors receive events from the eventsQueue and  execute actions according
 * to the defined policies in policies table
 */
@Service
public class PolicyEngine {

    private static final Logger logger = LoggerFactory.getLogger(PolicyEngine.class);

    @Autowired
    private BlockingQueue<DAEvent> eventQueue;

    @Autowired
    private BlockingQueue<ActionPluginRequest> actionPluginRequestQueue;

    @Autowired
    private PolicyMappings policyMappings;

    @Autowired
    private List<FactsEnricher> factsEnrichers;

    @Value("${policyEngine.concurrency:2}")
    private int numOfPolicyExecutors;


    List<PoliciesExecutor> policyExecutors = new LinkedList<>();

    public PolicyEngine() {
    }

    public PolicyEngine(List<FactsEnricher> factsEnrichers, PolicyMappings policyMappings, BlockingQueue<DAEvent> eventQueue, BlockingQueue<ActionPluginRequest> actionPluginRequestQueue, int numOfExecutors) {
        this.policyMappings = policyMappings;
        this.eventQueue = eventQueue;
        this.actionPluginRequestQueue = actionPluginRequestQueue;
        this.numOfPolicyExecutors = numOfExecutors;
        this.factsEnrichers = factsEnrichers;
    }

    @PostConstruct
    private void init() {
        start();
    }

    /**
     * Start policy engine with PoliciesExecutors according to the
     * policyEngine.concurrency configured in application.properties
     */
    public void start() {
        for (int i = 0; i < numOfPolicyExecutors; i++) {
            PoliciesExecutor policiesExecutor = new PoliciesExecutor(factsEnrichers,
                    policyMappings, eventQueue, actionPluginRequestQueue);
            policyExecutors.add(policiesExecutor);
        }
        policyExecutors.forEach(pex -> pex.start());
        logger.info("Started with {} policy executors", policyExecutors.size());
    }

    /**
     * Stopping all PoliciesExecutors
     * @throws InterruptedException
     */
    public void stop() throws InterruptedException {
        destroy();
    }

    @PreDestroy
    private void destroy() throws InterruptedException {
        policyExecutors.forEach(pex -> pex.shutdown());
        logger.info("Shutdown {} policy executors", policyExecutors.size());
    }
}
