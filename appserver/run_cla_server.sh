#!/bin/bash

# Add default JVM options here. You can also use JAVA_OPTS and CLA_FILECLUSTER_OPTS to pass JVM options to this script.
JMX_OPTS='-Djava.rmi.server.hostname=54.201.49.22'
GCOPT='-XX:+UseParNewGC -XX:+UseConcMarkSweepGC -XX:+CMSParallelRemarkEnabled -XX:SurvivorRatio=8 -XX:MaxTenuringThreshold=15 -XX:CMSInitiatingOccupancyFraction=75 -XX:+UseCMSInitiatingOccupancyOnly'
JAVA_OPTS="$JMX_OPTS $GCOPT"
export JAVA_OPTS

bin/cla-filecluster
