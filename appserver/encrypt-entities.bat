@echo off
set VERSION=3.0.0
set DIRNAME=%~dp0
set CLA_HOME=%DIRNAME%
set LIB_ROOT=.\build\libs\

echo Checking input
if '%1' == '' goto usage
if '%2' == '' goto usage

echo.
echo DocAuthority data extract hashing tool
echo Start processing...
echo     input=%1
echo     output=%2
echo.
echo Initialising...

REM java -Dspring.profiles.active=encryptor -jar %CLA_HOME%build/libs/cla-filecluster-1.0.0.jar %1 %2 %3
set JVMOPT=-Dspring.profiles.active=encryptor -Dfile.encoding=UTF8
set JMXOPT=
set MEM_OPT=-Xms1g -Xmx2g
set GC_OPT=
SET CP=-cp .;.\config;%LIB_ROOT%;%LIB_ROOT%lib\;%LIB_ROOT%.\cla-filecluster-%VERSION%.jar;%CONTENT_DIR%

java %MEM_OPT% %JVMOPT% %GC_OPT% %JMXOPT% %CP% org.springframework.boot.loader.JarLauncher %1 %2 %3 %4
goto finally

:usage
echo.
echo DocAuthority data extract hashing tool
echo.
echo Usage %0 <input> <output> [?validation-file-suffix?]
echo.
echo     input      - path of the data extract file
echo     output     - path of the unreadable (hashed) extract file
echo     validation-file-suffix    - optional
goto finally

:finally
