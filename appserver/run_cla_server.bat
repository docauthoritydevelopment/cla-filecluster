set LIB_ROOT=.\build\libs\
set LIB_RUNTIME=.\build\runtimeLibs\*
REM set CONTENT_DIR=.\src\main\resources\static
set CONTENT_DIR=.\static
set JMX_REMOTE_PORT=7004
set VERSION=3.0.0
set JVMOPT=-Xms2g -Xmx2g -Xss512k "-Dspring.profiles.active=win,dev" -Dfile.encoding=UTF8
set JVMOPT=%JVMOPT% 
set JVMOPT=%JVMOPT% -Dcom.sun.management.jmxremote.authenticate=false
set JVMOPT=%JVMOPT% -Dcom.sun.management.jmxremote.ssl=false
set JVMOPT=%JVMOPT% -Dcom.sun.management.jmxremote.port=%JMX_REMOTE_PORT%
set JVMOPT=%JVMOPT% -XX:+UnlockCommercialFeatures -XX:+FlightRecorder

REM set JVMOPT=%JVMOPT% -Djava.rmi.server.hostname=%EXTERN_IP%

set GCOPT=-XX:+UseParNewGC -XX:+UseConcMarkSweepGC -XX:+CMSParallelRemarkEnabled -XX:SurvivorRatio=8 -XX:MaxTenuringThreshold=15 -XX:CMSInitiatingOccupancyFraction=75 -XX:+UseCMSInitiatingOccupancyOnly
REM set GCOPT=-XX:SurvivorRatio=8 -XX:MaxTenuringThreshold=15 -verbosegc -XX:+PrintGCDetails -XX:+UseCMSInitiatingOccupancyOnly -XX:CMSInitiatingOccupancyFraction=50
REM set GCOPT=-XX:NewRatio=2 -XX:SurvivorRatio=6
REM set GCOPT=-XX:NewRatio=8 -XX:SurvivorRatio=4
REM set GCOPT=-XX:+UseParNewGC -XX:+UseConcMarkSweepGC -XX:+CMSParallelRemarkEnabled -XX:SurvivorRatio=3 -XX:MaxHeapFreeRatio=40 -XX:MinHeapFreeRatio=25
REM set GCOPT=-XX:+UseG1GC -XX:SurvivorRatio=4 -XX:NewRatio=2 -XX:MaxTenuringThreshold=15 -XX:MaxHeapFreeRatio=40 -XX:MinHeapFreeRatio=25
REM set GCOPT=-XX:+UseG1GC

SET CP=-cp .;.\config;%LIB_ROOT%;%LIB_ROOT%lib\;%LIB_ROOT%.\appserver-%VERSION%-fat.jar;%CONTENT_DIR%

java %JVMOPT% %GCOPT% %CP% org.springframework.boot.loader.JarLauncher %1 %2 %3
