-- TODO: update with latest DB structure cla_files_md ...

use cla;
select count(*) count from cla_files where  isnull(group_id) ;
select type,lower(extension) ext,state,count(*) count from cla_files group by type,ext,state order by count desc limit 99999 ;

select state,extension,count(*) count from cla_files where  not isnull(group_id) group by state,extension limit 99999 ;
select state,extension,count(*) count from cla_files group by state,extension limit 99999 ;
select group_id gr,count(id) count from cla_files where not isnull(group_id) group by group_id order by count desc limit 99999;

-- Grouped and ungrouped combined !
select t1.id,t1.file_name,t1.type,ifnull(t1.group_id,"-ungrouped-") group_id,t1.state,IF(ISNULL(t1.group_id),0,t2.count) cnt,t1.acl_read,t1.acl_write,t1.author,t1.base_name,
  t1.company,t1.creation_date,t1.extension,t1.generating_app,t1.items_countl1,t1.items_countl2,t1.items_countl3,t1.keywords,
  t1.last_modified,t1.path_level1,t1.path_level2,t1.path_level3,t1.path_level4,t1.path_level5,t1.path_level6,t1.subject,t1.template,t1.title 
	from cla_files t1,(select ifnull(group_id,"-") gr,count(id) count from cla_files group by gr) t2 
	where ifnull(t1.group_id,"-")=t2.gr order by cnt desc,group_id asc,t1.type,t1.base_name asc limit 999999;

-- Grouped
select t1.id,t1.file_name,t1.type,t1.group_id,t1.state,t3.count,t1.acl_read,t1.acl_write,t1.author,t1.base_name,t1.company,t1.creation_date,t1.extension,t1.generating_app,t1.items_countl1,t1.items_countl2,t1.items_countl3,t1.keywords,t1.last_modified,t1.path_level1,t1.path_level2,t1.path_level3,t1.path_level4,t1.path_level5,t1.path_level6,t1.subject,t1.template,t1.title 
	from cla_files t1,(select group_id gr,count(id) count from cla_files where not isnull(group_id) group by group_id) t3 
	where t1.group_id=t3.gr order by t3.count desc,t1.group_id asc limit 99999;
-- Ungrouped
select t1.id,t1.file_name,t1.type,"" group_id,t1.state,0 count,t1.acl_read,t1.acl_write,t1.author,t1.base_name,t1.company,t1.creation_date,t1.extension,t1.generating_app,t1.items_countl1,t1.items_countl2,t1.items_countl3,t1.keywords,t1.last_modified,t1.path_level1,t1.path_level2,t1.path_level3,t1.path_level4,t1.path_level5,t1.path_level6,t1.subject,t1.template,t1.title 
	from cla_files t1
	where isnull(t1.group_id) order by t1.file_name asc limit 99999;

select file_name from cla_files where group_id='9e3c9ad9-f308-4d01-881c-b6c5fba9542c';

create table t1 
  select c.file_name,c.type,length(p.pv_collections) pv_length,
    length(p.analysis_meadata) md_length,sha1(p.pv_collections) pv_sig 
    from cla_files c,pv_analysis_data p where p.lid=c.id limit 999999 ;

select c.*,GROUP_CONCAT(DISTINCT e.rule ORDER BY e.rule ) rules, count(e.cla_file_id) issues 
	from extracted_entities e, cla_files c where e.cla_file_id=c.id
	group by e.cla_file_id order by issues desc;

select c.*,g.generated_name, GROUP_CONCAT(DISTINCT e.rule ORDER BY e.rule ) rules, count(e.cla_file_id) issues 
	from extracted_entities e
	left join cla_files c on (e.cla_file_id=c.id)
	left join file_groups g on (c.file_group_id = g.id )
	group by c.file_group_id order by issues desc;

-- Repositories by Group (for sparced groups)
SELECT IF(ISNULL(g.name) OR LENGTH(g.name)=0,g.generated_name,g.name) AS group_name
 ,c.path_level1 repo,s.cnt AS numRepos,c.file_group_id AS group_id
 ,COUNT(c.id) as numFiles,g.num_of_files
 FROM 
 (SELECT * from 
   (SELECT c1.file_group_id,count(DISTINCT c1.path_level1) AS cnt 
    from cla_files c1 where (NOT isnull(c1.file_group_id)) group by c1.file_group_id) AS s1 
    where s1.cnt>1) As s
 ,cla_files AS c, file_groups AS g
 WHERE c.file_group_id=s.file_group_id and (NOT isnull(c.file_group_id)) and g.id=c.file_group_id
 GROUP BY group_id,repo order by num_of_files desc,group_name asc, cnt desc limit 999999;

-- Groups by Repositories (for sparced Repositories)
SELECT c.path_level1 AS repo,c.file_group_id AS group_id
 ,IF(ISNULL(g.name) OR LENGTH(g.name)=0,g.generated_name,g.name) AS group_name
 ,COUNT(*) as cnt,g.num_of_files files_in_group,s.cnt_tot repo_total
 FROM 
 (SELECT * from 
   (SELECT c1.path_level1 AS repo1,count(DISTINCT c1.file_group_id) AS cnt ,count(*) AS cnt_tot
    from cla_files c1 where (NOT isnull(c1.file_group_id)) group by c1.path_level1) AS s1 
    where s1.cnt>1) As s
 ,cla_files AS c, file_groups AS g
 WHERE (NOT isnull(c.file_group_id)) and g.id=c.file_group_id and s.repo1=(c.path_level1) 
 GROUP BY c.path_level1,group_id order by c.path_level1,cnt desc limit 999999;

	
-- select count(*) count,floor(length(long_histograms)/1000) size from cla_files group by size order by size desc limit 1000;
-- update cla_files set file_group_id=null,state=0 limit 99999;

-- Update the state of the files, to they will be ingested again
update cla_files set state=3 where state=5;

-- Find groups with more then one type
select tmp.ty,g.* from (
select file_group_id, count(distinct type) ty from cla_files_md
where type <10 and file_group_id is not null group by file_group_id) tmp
left join file_groups g on g.id = tmp.file_group_id
where ty > 1 order by g.num_of_files desc;

--Demo user
INSERT INTO users (`id`, `name`, `password`, `role_name`, `username`, `account_id`) VALUES (3, 'demo', '$2a$10$nt0yaBKO6oVQIE8HKOl1lesqRnQmBGy6T7hsPJ3MGOA/QRXtKzLQ2', 'Administrator', 'demo', 1);
INSERT INTO users_roles (`users_id`, `roles_id`) VALUES (3, 1);
INSERT INTO users_roles (`users_id`, `roles_id`) VALUES (3, 2);
INSERT INTO users_roles (`users_id`, `roles_id`) VALUES (3, 3);
INSERT INTO users_roles (`users_id`, `roles_id`) VALUES (3, 4);
