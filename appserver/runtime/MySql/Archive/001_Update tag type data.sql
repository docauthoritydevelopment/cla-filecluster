#Update tag type data
alter table file_tag_type add column `hidden` bit(1) NOT NULL;
alter table file_tag_type add column `system` bit(1) NOT NULL;

ALTER TABLE `business_id_tag_association` MODIFY COLUMN `file_tag_id` bigint(20) DEFAULT NULL;
ALTER TABLE `business_id_tag_association` ADD COLUMN `doc_type_id` bigint(20) DEFAULT NULL;
ALTER TABLE `business_id_tag_association` ADD COLUMN `simple_biz_list_item_sid` varchar(255) DEFAULT NULL;
ALTER TABLE `business_id_tag_association` ADD KEY `FK_eiodnvsf11tgtbb0eggp0i85t` (`doc_type_id`);
ALTER TABLE `business_id_tag_association` ADD KEY `FK_qmi8gsx5gi83d0vkxpxvmhfmw` (`simple_biz_list_item_sid`);
ALTER TABLE `business_id_tag_association` ADD CONSTRAINT `FK_eiodnvsf11tgtbb0eggp0i85t` FOREIGN KEY (`doc_type_id`) REFERENCES `doc_type` (`id`);
ALTER TABLE `business_id_tag_association` ADD CONSTRAINT `FK_qmi8gsx5gi83d0vkxpxvmhfmw` FOREIGN KEY (`simple_biz_list_item_sid`) REFERENCES `bl_item_simple` (`sid`);

ALTER TABLE `cla_file_tag_association` MODIFY COLUMN `doc_type_id` bigint(20) DEFAULT NULL;
ALTER TABLE `cla_file_tag_association` ADD COLUMN `file_tag_id` bigint(20) DEFAULT NULL;
ALTER TABLE `cla_file_tag_association` ADD COLUMN `simple_biz_list_item_sid` varchar(255) DEFAULT NULL;
ALTER TABLE `cla_file_tag_association` ADD KEY `FK_321x1e582kff2l1w7ypjpscx` (`doc_type_id`);
ALTER TABLE `cla_file_tag_association` ADD KEY `FK_dbgnlx2pksjsrhafclp3xj6ob` (`simple_biz_list_item_sid`);
ALTER TABLE `cla_file_tag_association` ADD CONSTRAINT `FK_321x1e582kff2l1w7ypjpscx` FOREIGN KEY (`doc_type_id`) REFERENCES `doc_type` (`id`);
ALTER TABLE `cla_file_tag_association` ADD CONSTRAINT `FK_dbgnlx2pksjsrhafclp3xj6ob` FOREIGN KEY (`simple_biz_list_item_sid`) REFERENCES `bl_item_simple` (`sid`);

ALTER TABLE `folder_tag_association` MODIFY COLUMN `doc_type_id` bigint(20) DEFAULT NULL;
ALTER TABLE `folder_tag_association` ADD COLUMN `file_tag_id` bigint(20) DEFAULT NULL;
ALTER TABLE `folder_tag_association` ADD COLUMN `simple_biz_list_item_sid` varchar(255) DEFAULT NULL;
ALTER TABLE `folder_tag_association` ADD KEY `FK_7md1p1i71qgv2q9j6tfae01n` (`doc_type_id`);
ALTER TABLE `folder_tag_association` ADD KEY `FK_m4cbjjtiv37mn91phi41pv1ai` (`simple_biz_list_item_sid`);
ALTER TABLE `folder_tag_association` ADD CONSTRAINT `FK_7md1p1i71qgv2q9j6tfae01n` FOREIGN KEY (`doc_type_id`) REFERENCES `doc_type` (`id`);
ALTER TABLE `folder_tag_association` ADD CONSTRAINT `FK_m4cbjjtiv37mn91phi41pv1ai` FOREIGN KEY (`simple_biz_list_item_sid`) REFERENCES `bl_item_simple` (`sid`);
