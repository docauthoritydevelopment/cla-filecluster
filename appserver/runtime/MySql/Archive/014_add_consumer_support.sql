Alter TABLE `bl_item_simple` add column `encrypted_fields_json` varchar(4096) DEFAULT NULL;
Alter TABLE `bl_item_simple` add column `first_name` varchar(255) DEFAULT NULL;
Alter TABLE `bl_item_simple` add column `last_name` varchar(255) DEFAULT NULL;

Alter TABLE `biz_list_item_extraction` add column `agg_min_count` int(11) NOT NULL default 0;
Alter TABLE `biz_list_item_extraction` add KEY `bl_e_bli` (`biz_list_id`);

Alter TABLE `biz_list` add column `template` varchar(255) DEFAULT NULL;
