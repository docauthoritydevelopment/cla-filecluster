-- Table structure for table `action_class_executor_definition`
--

CREATE TABLE `action_class_executor_definition` (
  `id` varchar(255) NOT NULL,
  `action_class` int(11) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `java_class_name` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `action_definition`
--

CREATE TABLE `action_definition` (
  `id` varchar(255) NOT NULL,
  `action_class` int(11) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `java_method_name` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `action_execution_task_status`
--

CREATE TABLE `action_execution_task_status` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `progress_mark` bigint(20) NOT NULL,
  `progress_max_mark` bigint(20) NOT NULL,
  `status` int(11) DEFAULT NULL,
  `trigger_time` bigint(20) NOT NULL,
  `type` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `action_instance`
--

CREATE TABLE `action_instance` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `parameters` varchar(255) DEFAULT NULL,
  `action_id` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_eek9qnr3wvjcn2j6fxj884a7q` (`action_id`),
  CONSTRAINT `FK_eek9qnr3wvjcn2j6fxj884a7q` FOREIGN KEY (`action_id`) REFERENCES `action_definition` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `compound_policy_item`
--

CREATE TABLE `compound_policy_item` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `desired_state_definition` (
  `id` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `java_class_name` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `policy_layer` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `file_action_desired` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `action_class` int(11) DEFAULT NULL,
  `action_trigger_id` bigint(20) NOT NULL,
  `cla_file_id` bigint(20) NOT NULL,
  `properties` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `action_definition_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `UK_6f8tsch0klyf3q0eqfvgwlo6o` (`cla_file_id`),
  KEY `UK_67wnb2w152r1nc0bjypam8f3h` (`action_class`),
  KEY `UK_2c6n5otxwqh869nh8p9v1k4de` (`action_trigger_id`),
  KEY `FK_a82yorssa7uq8tles9v6nm2ym` (`action_definition_id`),
  CONSTRAINT `FK_a82yorssa7uq8tles9v6nm2ym` FOREIGN KEY (`action_definition_id`) REFERENCES `action_definition` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `policy_item` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `policy_layer` int(11) NOT NULL,
  `priority` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `policy_item_action_instances` (
  `policy_item_id` bigint(20) NOT NULL,
  `action_instances_id` bigint(20) NOT NULL,
  PRIMARY KEY (`policy_item_id`,`action_instances_id`),
  KEY `FK_nnh3ap1g8eqb9aheptsbxhpg5` (`action_instances_id`),
  CONSTRAINT `FK_aooglynfuck1et6klwti8jm7l` FOREIGN KEY (`policy_item_id`) REFERENCES `policy_item` (`id`),
  CONSTRAINT `FK_nnh3ap1g8eqb9aheptsbxhpg5` FOREIGN KEY (`action_instances_id`) REFERENCES `action_instance` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `policy_item_attachment` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `population_filter_json` varchar(255) NOT NULL,
  `policy_item_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_oi5y1swp1sds37lvux6gfx7c` (`policy_item_id`),
  CONSTRAINT `FK_oi5y1swp1sds37lvux6gfx7c` FOREIGN KEY (`policy_item_id`) REFERENCES `policy_item` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `policy_rule_instance` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `filter` varchar(2048) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `compound_policy_item_policy_items` (
  `compound_policy_item_id` bigint(20) NOT NULL,
  `policy_items_id` bigint(20) NOT NULL,
  PRIMARY KEY (`compound_policy_item_id`,`policy_items_id`),
  KEY `FK_6i1hnok104518usvdrw0n0mhw` (`policy_items_id`),
  CONSTRAINT `FK_6i1hnok104518usvdrw0n0mhw` FOREIGN KEY (`policy_items_id`) REFERENCES `policy_item` (`id`),
  CONSTRAINT `FK_ri9q06gasxrt2v9goe2pntnv1` FOREIGN KEY (`compound_policy_item_id`) REFERENCES `compound_policy_item` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `policy_item_rules` (
  `policy_item_id` bigint(20) NOT NULL,
  `rules_id` bigint(20) NOT NULL,
  UNIQUE KEY `UK_1pv38rrequb8it90vbogsn4t` (`rules_id`),
  KEY `FK_avj16pk8vdncyh14x9q4b5msb` (`policy_item_id`),
  CONSTRAINT `FK_1pv38rrequb8it90vbogsn4t` FOREIGN KEY (`rules_id`) REFERENCES `policy_rule_instance` (`id`),
  CONSTRAINT `FK_avj16pk8vdncyh14x9q4b5msb` FOREIGN KEY (`policy_item_id`) REFERENCES `policy_item` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `action_parameter_definition` (
  `id` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `regex` varchar(255) DEFAULT NULL,
  `action_definition_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_lgj2m0w6d7mm95prl16hk7jn` (`action_definition_id`),
  CONSTRAINT `FK_lgj2m0w6d7mm95prl16hk7jn` FOREIGN KEY (`action_definition_id`) REFERENCES `action_definition` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Alter tables

alter table folder_tag_association add column `compound_policy_item_id` bigint(20) DEFAULT NULL;
alter table folder_tag_association add KEY `FK_17poen2bw2nirliv2efo62832` (`compound_policy_item_id`);
alter table folder_tag_association add CONSTRAINT `FK_17poen2bw2nirliv2efo62832` FOREIGN KEY (`compound_policy_item_id`) REFERENCES `compound_policy_item` (`id`);

alter table `business_id_tag_association` add column`compound_policy_item_id` bigint(20) DEFAULT NULL;
alter table `business_id_tag_association` add KEY `FK_pt6wrfn6ey26gn12l1ks6ao8d` (`compound_policy_item_id`);
alter table `business_id_tag_association` add CONSTRAINT `FK_pt6wrfn6ey26gn12l1ks6ao8d` FOREIGN KEY (`compound_policy_item_id`) REFERENCES `compound_policy_item` (`id`);

alter table `cla_file_tag_association` add column `compound_policy_item_id` bigint(20) DEFAULT NULL;
alter table `cla_file_tag_association` add KEY `FK_damcgwlew39d839mulfv6gycd` (`compound_policy_item_id`);
alter table `cla_file_tag_association` add CONSTRAINT `FK_damcgwlew39d839mulfv6gycd` FOREIGN KEY (`compound_policy_item_id`) REFERENCES `compound_policy_item` (`id`);

