
alter table `cla_files_md` add column `initial_scan_date` datetime DEFAULT NULL;
alter table `cla_files_md` add column `deletion_date` datetime DEFAULT NULL;

update `cla_files_md` set initial_scan_date = created_ondb where id > 0;
update `cla_files_md` set deletion_date = updated_ondb where deleted = 1 and id > 0 ;
