/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `date_range_partition`
--

DROP TABLE IF EXISTS `date_range_partition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `date_range_partition` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `date_range_partition`
--

LOCK TABLES `date_range_partition` WRITE;
/*!40000 ALTER TABLE `date_range_partition` DISABLE KEYS */;
INSERT INTO `date_range_partition` VALUES (1,'default','default');
/*!40000 ALTER TABLE `date_range_partition` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `date_range_partition_simple_date_range_items`
--

DROP TABLE IF EXISTS `date_range_partition_simple_date_range_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `date_range_partition_simple_date_range_items` (
  `date_range_partition_id` bigint(20) NOT NULL,
  `simple_date_range_items_id` bigint(20) NOT NULL,
  UNIQUE KEY `UK_pd4n2bi7we4rupwa3o2f1c6gj` (`simple_date_range_items_id`),
  KEY `FK_g66w92q1l1fnacoxvxpowe13o` (`date_range_partition_id`),
  CONSTRAINT `FK_g66w92q1l1fnacoxvxpowe13o` FOREIGN KEY (`date_range_partition_id`) REFERENCES `date_range_partition` (`id`),
  CONSTRAINT `FK_pd4n2bi7we4rupwa3o2f1c6gj` FOREIGN KEY (`simple_date_range_items_id`) REFERENCES `simple_date_range_item` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `date_range_partition_simple_date_range_items`
--

LOCK TABLES `date_range_partition_simple_date_range_items` WRITE;
/*!40000 ALTER TABLE `date_range_partition_simple_date_range_items` DISABLE KEYS */;
INSERT INTO `date_range_partition_simple_date_range_items` VALUES (1,1),(1,2),(1,3),(1,4),(1,5);
/*!40000 ALTER TABLE `date_range_partition_simple_date_range_items` ENABLE KEYS */;
UNLOCK TABLES;


--
-- Table structure for table `simple_date_range_item`
--

DROP TABLE IF EXISTS `simple_date_range_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `simple_date_range_item` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `end_absolute_time` datetime DEFAULT NULL,
  `end_relative_period` int(11) DEFAULT NULL,
  `end_relative_period_amount` int(11) DEFAULT NULL,
  `end_type` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `order_value` bigint(20) DEFAULT NULL,
  `start_absolute_time` datetime DEFAULT NULL,
  `start_relative_period` int(11) DEFAULT NULL,
  `start_relative_period_amount` int(11) DEFAULT NULL,
  `start_type` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `simple_date_range_item`
--

LOCK TABLES `simple_date_range_item` WRITE;
/*!40000 ALTER TABLE `simple_date_range_item` DISABLE KEYS */;
INSERT INTO `simple_date_range_item` VALUES (1,NULL,NULL,NULL,NULL,'Last month',NULL,NULL,4,1,1),(2,NULL,4,1,1,'Last 3 months',NULL,NULL,4,3,1),(3,NULL,4,3,1,'Last Year',NULL,NULL,4,12,1),(4,NULL,4,12,1,'Last 3 Years',NULL,NULL,4,36,1),(5,NULL,4,36,1,'Last 5 Years',NULL,NULL,4,60,1);
/*!40000 ALTER TABLE `simple_date_range_item` ENABLE KEYS */;
UNLOCK TABLES;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
