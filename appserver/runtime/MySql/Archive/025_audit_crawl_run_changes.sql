ALTER TABLE `audit_phase_details` CHANGE COLUMN `id` `id` bigint(20) NOT NULL AUTO_INCREMENT;

ALTER TABLE `crawl_runs` CHANGE COLUMN `stop_time` `stop_time` BIGINT(20) NULL ;

ALTER TABLE `crawl_runs` ADD COLUMN `root_folder_accessible` bit(1) NOT NULL DEFAULT b'1';
