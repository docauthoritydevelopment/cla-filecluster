alter table `cla_files_md` add column `user_group_id` char(36) DEFAULT NULL;
alter table `content_metadata` add column `user_group_id` char(36) DEFAULT NULL;

-- The lines below are done by JAVA after server goes up
--update `cla_files_md` set user_group_id = file_group_id;
--update `content_metadata` set user_group_id = file_group_id;

alter table `content_metadata` add column `analyze_hint` int(11) NOT NULL default 0;

alter table `file_groups` add column `group_type` int(11) NOT NULL default 0;
alter table `file_groups` add column `parent_group_id` char(36) DEFAULT NULL;

alter table `pv_analysis_data` add column `analyze_hint` int(11) NOT NULL default 0;
