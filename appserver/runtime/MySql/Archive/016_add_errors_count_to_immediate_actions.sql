
alter table `action_execution_task_status` add column `errors_count` bigint(20) NOT NULL default 0;

alter table `action_execution_task_status` add column `first_error` varchar(4096) DEFAULT NULL;
