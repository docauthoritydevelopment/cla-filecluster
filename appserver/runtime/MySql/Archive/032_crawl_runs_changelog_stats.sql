ALTER TABLE `crawl_runs`
ADD COLUMN `deleted_files` BIGINT(20) NOT NULL default 0 ,
ADD COLUMN `new_files` BIGINT(20) NOT NULL default 0 ,
ADD COLUMN `updated_content_files` BIGINT(20) NOT NULL default 0 ,
ADD COLUMN `updated_metadata_files` BIGINT(20) NOT NULL default 0 ;
