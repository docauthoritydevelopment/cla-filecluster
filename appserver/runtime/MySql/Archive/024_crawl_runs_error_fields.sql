alter table `crawl_runs` add column `scan_errors` bigint(20) NOT NULL default 0;
alter table `crawl_runs` add column `ingestion_errors` bigint(20) NOT NULL default 0;
