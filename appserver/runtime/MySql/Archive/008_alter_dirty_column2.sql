alter table `doc_type_association` add column
  `files_should_be_dirty` bit(1) NOT NULL;

alter table `business_id_tag_association` add column
  `files_should_be_dirty` bit(1) NOT NULL;

alter table `folder_tag_association` add column
  `files_should_be_dirty` bit(1) NOT NULL;

