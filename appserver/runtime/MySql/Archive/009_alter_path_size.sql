ALTER TABLE `phase_details` MODIFY COLUMN `error` varchar(4096) DEFAULT NULL;

ALTER TABLE `scan_errors` MODIFY COLUMN `path` varchar(4096) DEFAULT NULL;

ALTER TABLE `doc_folder` DROP KEY `path_idx`;
ALTER TABLE `doc_folder` MODIFY COLUMN `path` varchar(4096) DEFAULT NULL;
ALTER TABLE `doc_folder` MODIFY COLUMN `real_path` varchar(4096) DEFAULT NULL;
ALTER TABLE `doc_folder` ADD KEY `path_idx` (`path` (255));

ALTER TABLE `root_folder` DROP KEY `path`;
ALTER TABLE `root_folder` MODIFY COLUMN `path` varchar(4096) DEFAULT NULL;
ALTER TABLE `root_folder` ADD KEY `path_idx` (`path` (255));

