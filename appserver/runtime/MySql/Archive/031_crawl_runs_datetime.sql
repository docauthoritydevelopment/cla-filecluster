ALTER TABLE `crawl_runs` CHANGE COLUMN `start_time` `start_time` BIGINT(20) NULL ;

update docauthority.crawl_runs set start_time = null, stop_time = null;

ALTER TABLE `crawl_runs`
CHANGE COLUMN `start_time` `start_time` DATETIME(3) DEFAULT NULL,
CHANGE COLUMN `stop_time` `stop_time` DATETIME(3) DEFAULT NULL ;
