# Create bizList
CREATE TABLE `biz_list` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `biz_list_item_type` int(11) NOT NULL,
  `biz_list_solr_id` varchar(255) DEFAULT NULL,
  `biz_list_source` varchar(255) DEFAULT NULL,
  `creation_time_stamp` bigint(20) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `entity_list_encryption_type` int(11) DEFAULT NULL,
  `external_id` varchar(255) DEFAULT NULL,
  `extraction_session_id` bigint(20) NOT NULL,
  `last_update_time_stamp` bigint(20) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `rules_file_name` varchar(255) DEFAULT NULL,
  `account_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_acc` (`name`,`account_id`),
  KEY `FK_heiks1tibj5hvtiq29dqsr3gi` (`account_id`),
  CONSTRAINT `FK_heiks1tibj5hvtiq29dqsr3gi` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

CREATE TABLE `biz_list_item_extraction` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `biz_list_id` bigint(20) NOT NULL,
  `biz_list_item_sid` varchar(255) NOT NULL,
  `biz_list_item_type` int(11) NOT NULL,
  `cla_file_id` bigint(20) NOT NULL,
  `dirty` bit(1) NOT NULL,
  `extraction_session_id` bigint(20) NOT NULL,
  `rule_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `bl_e_fid` (`cla_file_id`),
  KEY `bl_e_dirty_bli` (`dirty`,`biz_list_id`),
  KEY `bl_e_dirty_f` (`dirty`,`cla_file_id`),
  KEY `bl_e_dirty` (`dirty`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
