
INSERT INTO `action_class_executor_def` (`id`,`action_class`,`description`,`java_class_name`,`name`) VALUES
    ('ENCRYPTION_MANAGEMENT_SCRIPT_GENERATOR',3,
        'Creates files encryption scripts',
        'com.cla.filecluster.actions.executors.EncryptionScriptGenerator',
        'Creates files encryption scripts')
    ,('DLP_POLICY_SCRIPT_GENERATOR',2,
        'Creates DLP policy altering scripts',
        'com.cla.filecluster.actions.executors.DlpScriptGenerator',
        'Creates DLP policy altering scripts');

INSERT INTO `action_definition` (`id`,`action_class`,`description`,`java_method_name`,`name`) VALUES
    ('ADD_PERMISSION',0,'Grunt file access by user/group','addPermission','Add permission')
    ,('REMOVE_PERMISSION',0,'Deny file access by user/group','removePermission','Remove permission')
    ,('DENY_EMAIL_DLP', '2', 'Deny file to be sent to email (list) through DLP', 'denyEmail', 'Deny email list through DLP')
    ,('ENCRYPT_FILE',3,'Encrypt file','encryptFile','Encrypt file');

DELETE FROM action_parameter_definition WHERE id IS NOT NULL;
INSERT INTO action_parameter_definition (`action_definition_id`,`id`,`name`,`description`,`regex`) VALUES
    ('REPORT_FILE_DETAILS', 'fields', 'fields', 'The list of fields that will be reported on the file', '.*')
    ,('ADD_PERMISSION', 'userPermAdd', 'user', 'The user to add permission for', '.*')
    ,('REMOVE_PERMISSION', 'userPermRemove', 'user', 'The user to deny permission for', '.*')
    ,('COPY_FILE', 'targetFileCopy', 'target', 'The copy target', '.*')
    ,('MOVE_FILE', 'targetFileMove', 'target', 'The move target', '.*')
    ,('ENCRYPT_FILE', 'targetFileEnc', 'target', 'The encryption target', '.*')
    ,('ALLOW_EMAIL_DLP', 'userDlpAllow', 'user', 'The user to allow email for', '.*')
    ,('DENY_EMAIL_DLP', 'userDlpDeny', 'user', 'The user to block email for', '.*');

 DELETE FROM action_parameter_definition WHERE action_definition_id in
    ('ADD_READ_PERMISSION'
    ,'ADD_WRITE_PERMISSION'
    ,'SET_READ_PERMISSION'
    ,'SET_WRITE_PERMISSION');

DELETE FROM `action_definition` WHERE ID in
    ('ADD_READ_PERMISSION'
    ,'ADD_WRITE_PERMISSION'
    ,'SET_READ_PERMISSION'
    ,'SET_WRITE_PERMISSION');

