ALTER TABLE`cla_files_md` MODIFY COLUMN `created_ondb` datetime(3) DEFAULT NULL;
ALTER TABLE`cla_files_md` MODIFY COLUMN `creation_date` datetime(3) DEFAULT NULL;
ALTER TABLE`cla_files_md` MODIFY COLUMN `deletion_date` datetime(3) DEFAULT NULL;
ALTER TABLE`cla_files_md` MODIFY COLUMN `fs_last_modified` datetime(3) DEFAULT NULL;
ALTER TABLE`cla_files_md` MODIFY COLUMN `initial_scan_date` datetime(3) DEFAULT NULL;
ALTER TABLE`cla_files_md` MODIFY COLUMN `updated_ondb` datetime(3) DEFAULT NULL;

ALTER TABLE`content_metadata` MODIFY COLUMN `app_creation_date` datetime(3) DEFAULT NULL;

ALTER TABLE`err_processed_files` MODIFY COLUMN `created_ondb` datetime(3) DEFAULT NULL;

ALTER TABLE`err_scanned_files` MODIFY COLUMN `created_ondb` datetime(3) DEFAULT NULL;

ALTER TABLE`group_unification_helper` MODIFY COLUMN `created_ondb` datetime(3) DEFAULT NULL;

ALTER TABLE`missed_file_match` MODIFY COLUMN `created_ondb` datetime(3) DEFAULT NULL;
