alter table doc_type add column `unique_name` varchar(255) NOT NULL;
alter table doc_type add column `alternative_name` varchar(255) DEFAULT NULL;
alter table file_tag add column `alternative_name` varchar(255) DEFAULT NULL;