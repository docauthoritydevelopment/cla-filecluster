/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `act_file_action_desired`
--

DROP TABLE IF EXISTS `act_file_action_desired`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `act_file_action_desired` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `action_class` int(11) DEFAULT NULL,
  `action_trigger_id` bigint(20) NOT NULL,
  `cla_file_id` bigint(20) NOT NULL,
  `properties` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `action_definition_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `UK_shssku9x89u3u8smqs90xwci` (`cla_file_id`),
  KEY `UK_ci0sis7edvxh5u6dhe0q8t553` (`action_class`),
  KEY `UK_hv5bfug2sdwis4o5o7x7i1ai5` (`action_trigger_id`),
  KEY `FK_1aqcpt8u4tva11yi1a76u2jxj` (`action_definition_id`),
  CONSTRAINT `FK_1aqcpt8u4tva11yi1a76u2jxj` FOREIGN KEY (`action_definition_id`) REFERENCES `action_definition` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `action_class_executor_def`
--

DROP TABLE IF EXISTS `action_class_executor_def`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `action_class_executor_def` (
  `id` varchar(255) NOT NULL,
  `action_class` int(11) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `java_class_name` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `action_class_executor_def`
--

LOCK TABLES `action_class_executor_def` WRITE;
/*!40000 ALTER TABLE `action_class_executor_def` DISABLE KEYS */;
INSERT INTO `action_class_executor_def` VALUES ('ACCESS_MANAGEMENT_SCRIPT_GENERATOR',0,'Creates permission altering scripts','com.cla.filecluster.actions.executors.AccessManagementScriptGenerator','Access Management Script Generator'),('REPORTING_CSV_GENERATOR',6,'Creates a CSV report on the files','com.cla.filecluster.actions.executors.ReportingCsvGenerator','Reporting CSV generator'),('STORAGE_POLICY_SCRIPT_GENERATOR',5,'Create scripts that move/copy files','com.cla.filecluster.actions.executors.StoragePolicyScriptGenerator','Storage Policy Script Generator');
/*!40000 ALTER TABLE `action_class_executor_def` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `action_definition`
--

DROP TABLE IF EXISTS `action_definition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `action_definition` (
  `id` varchar(255) NOT NULL,
  `action_class` int(11) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `java_method_name` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `action_definition`
--

LOCK TABLES `action_definition` WRITE;
/*!40000 ALTER TABLE `action_definition` DISABLE KEYS */;
INSERT INTO `action_definition` VALUES ('ADD_READ_PERMISSION',0,'Allow user/group to read the file','addReadPermission','Add Read Permission'),('ADD_WRITE_PERMISSION',0,'Allow user/group to write the file','addWritePermission','Add Write Permission'),('ALLOW_EMAIL_DLP',2,'Allow file to be sent to email (list) through DLP','allowEmail','Allow email list through DLP'),('COPY_FILE',5,'Copy a file to a different location','copyFile','Copy a file'),('MOVE_FILE',5,'Move a file to a different location','moveFile','Move a file'),('REPORT_FILE_DETAILS',6,'Add the file to a report','reportFile','Report on the file'),('SET_READ_PERMISSION',0,'Allow only user/group to read the file','setReadPermission','Set Read Permission'),('SET_WRITE_PERMISSION',0,'Allow only user/group to write the file','setWritePermission','Set Write Permission');
/*!40000 ALTER TABLE `action_definition` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `action_execution_task_status`
--

DROP TABLE IF EXISTS `action_execution_task_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `action_execution_task_status` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `progress_mark` bigint(20) NOT NULL,
  `progress_max_mark` bigint(20) NOT NULL,
  `status` int(11) DEFAULT NULL,
  `trigger_time` bigint(20) NOT NULL,
  `type` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `UK_t7ammbfd2vkosb6fp405ut7xw` (`trigger_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `action_instance`
--

DROP TABLE IF EXISTS `action_instance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `action_instance` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `parameters` varchar(255) DEFAULT NULL,
  `action_id` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_eek9qnr3wvjcn2j6fxj884a7q` (`action_id`),
  CONSTRAINT `FK_eek9qnr3wvjcn2j6fxj884a7q` FOREIGN KEY (`action_id`) REFERENCES `action_definition` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `action_parameter_definition`
--

DROP TABLE IF EXISTS `action_parameter_definition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `action_parameter_definition` (
  `id` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `regex` varchar(255) DEFAULT NULL,
  `action_definition_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_lgj2m0w6d7mm95prl16hk7jn` (`action_definition_id`),
  CONSTRAINT `FK_lgj2m0w6d7mm95prl16hk7jn` FOREIGN KEY (`action_definition_id`) REFERENCES `action_definition` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `action_parameter_definition`
--

LOCK TABLES `action_parameter_definition` WRITE;
/*!40000 ALTER TABLE `action_parameter_definition` DISABLE KEYS */;
INSERT INTO `action_parameter_definition` VALUES ('fields','The list of fields that will be reported on the file','fields','.*','REPORT_FILE_DETAILS'),('target','The move target','target','.*','MOVE_FILE'),('user','The user to add set permission for','user','.*','SET_WRITE_PERMISSION');
/*!40000 ALTER TABLE `action_parameter_definition` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `err_scanned_files`
--

DROP TABLE IF EXISTS `err_scanned_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `err_scanned_files` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_ondb` datetime DEFAULT NULL,
  `exception_text` varchar(255) DEFAULT NULL,
  `path` varchar(4096) DEFAULT NULL,
  `text` varchar(255) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `doc_folder_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_r54t5i4jjfcyshauqmkse9bf4` (`doc_folder_id`),
  CONSTRAINT `FK_r54t5i4jjfcyshauqmkse9bf4` FOREIGN KEY (`doc_folder_id`) REFERENCES `doc_folder` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pol_compound_policy_item`
--

DROP TABLE IF EXISTS `pol_compound_policy_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pol_compound_policy_item` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pol_compound_policy_item_policy_items`
--

DROP TABLE IF EXISTS `pol_compound_policy_item_policy_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pol_compound_policy_item_policy_items` (
  `pol_compound_policy_item_id` bigint(20) NOT NULL,
  `policy_items_id` bigint(20) NOT NULL,
  PRIMARY KEY (`pol_compound_policy_item_id`,`policy_items_id`),
  KEY `FK_3rth36gni1jx8xou27abvj3uh` (`policy_items_id`),
  CONSTRAINT `FK_3rth36gni1jx8xou27abvj3uh` FOREIGN KEY (`policy_items_id`) REFERENCES `policy_item` (`id`),
  CONSTRAINT `FK_swc4wlxvolxwcw8vds2l496lf` FOREIGN KEY (`pol_compound_policy_item_id`) REFERENCES `pol_compound_policy_item` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pol_desired_state_definition`
--

DROP TABLE IF EXISTS `pol_desired_state_definition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pol_desired_state_definition` (
  `id` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `java_class_name` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `policy_layer` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pol_desired_state_definition`
--

LOCK TABLES `pol_desired_state_definition` WRITE;
/*!40000 ALTER TABLE `pol_desired_state_definition` DISABLE KEYS */;
INSERT INTO `pol_desired_state_definition` VALUES ('READ_PERMISSIONS_SET','Only the set User/Group has read permissions','com.cla.filecluster.policy.state.readPermissionsSetDesiredState','Only Set User/Group can read',0),('WRITE_PERMISSIONS_SET','Only the set User/Group has write permissions','com.cla.filecluster.policy.state.writePermissionsSetDesiredState','Only Set User/Group can write',0);
/*!40000 ALTER TABLE `pol_desired_state_definition` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `policy_item`
--

DROP TABLE IF EXISTS `policy_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `policy_item` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `policy_layer` int(11) NOT NULL,
  `priority` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `policy_item_action_instances`
--

DROP TABLE IF EXISTS `policy_item_action_instances`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `policy_item_action_instances` (
  `policy_item_id` bigint(20) NOT NULL,
  `action_instances_id` bigint(20) NOT NULL,
  PRIMARY KEY (`policy_item_id`,`action_instances_id`),
  KEY `FK_nnh3ap1g8eqb9aheptsbxhpg5` (`action_instances_id`),
  CONSTRAINT `FK_aooglynfuck1et6klwti8jm7l` FOREIGN KEY (`policy_item_id`) REFERENCES `policy_item` (`id`),
  CONSTRAINT `FK_nnh3ap1g8eqb9aheptsbxhpg5` FOREIGN KEY (`action_instances_id`) REFERENCES `action_instance` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `policy_item_attachment`
--

DROP TABLE IF EXISTS `policy_item_attachment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `policy_item_attachment` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `population_filter_json` varchar(255) NOT NULL,
  `policy_item_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_oi5y1swp1sds37lvux6gfx7c` (`policy_item_id`),
  CONSTRAINT `FK_oi5y1swp1sds37lvux6gfx7c` FOREIGN KEY (`policy_item_id`) REFERENCES `policy_item` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `policy_item_rules`
--

DROP TABLE IF EXISTS `policy_item_rules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `policy_item_rules` (
  `policy_item_id` bigint(20) NOT NULL,
  `rules_id` bigint(20) NOT NULL,
  UNIQUE KEY `UK_1pv38rrequb8it90vbogsn4t` (`rules_id`),
  KEY `FK_avj16pk8vdncyh14x9q4b5msb` (`policy_item_id`),
  CONSTRAINT `FK_1pv38rrequb8it90vbogsn4t` FOREIGN KEY (`rules_id`) REFERENCES `policy_rule_instance` (`id`),
  CONSTRAINT `FK_avj16pk8vdncyh14x9q4b5msb` FOREIGN KEY (`policy_item_id`) REFERENCES `policy_item` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `policy_rule_instance`
--

DROP TABLE IF EXISTS `policy_rule_instance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `policy_rule_instance` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `filter` varchar(2048) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-03-10 16:49:31
