ALTER TABLE `saved_filters`
CHANGE COLUMN `filter_json` `filter_json` VARCHAR(2048) NULL DEFAULT NULL COMMENT '' ;

ALTER TABLE `saved_charts`
CHANGE COLUMN `population_json_filter` `population_json_filter` VARCHAR(2048) NULL DEFAULT NULL COMMENT '' ;

ALTER TABLE `saved_charts`
CHANGE COLUMN `population_subset_json_filter` `population_subset_json_filter` VARCHAR(2048) NULL DEFAULT NULL COMMENT '' ;

