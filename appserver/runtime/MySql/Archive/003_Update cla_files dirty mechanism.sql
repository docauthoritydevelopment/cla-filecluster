#Update cla_files dirty mechanism
ALTER TABLE cla_files MODIFY dirty INTEGER(11);

alter table cla_files add column `dirty_wip` bit(1) NOT NULL;