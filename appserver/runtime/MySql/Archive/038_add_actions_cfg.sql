INSERT INTO `action_class_executor_def` (id, action_class, description, java_class_name, name)
    VALUES
    ('FILE_PROPERTY_MANIPULATOR_SCRIPT_GENERATOR', '2', 'Creates file properties altering scripts', 'com.cla.filecluster.actions.executors.FileTaggingScriptGenerator', 'File Properties Manipulation Script Generator');

INSERT INTO `action_definition` (id, action_class, description, java_method_name, name)
    VALUES
    ('ADD_PROPERTY_VALUE', '2', 'Add a value to the properties of a file', 'addPropertyValue', 'Add file property value')
    ,('REM_PROPERTY_VALUE', '2', 'Remove a value from the properties of a file', 'remPropertyValue', 'Remove file property value');
