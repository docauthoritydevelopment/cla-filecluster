create index acctRfTypeState_idx on cla_files_md (`account_id`,`root_folder_id`,`type`,`state`,`deleted`);
create index acct_content_metadata_idx on cla_files_md (`account_id`,`content_metadata_id`);

create index group_idx on content_metadata (`file_group_id`);
