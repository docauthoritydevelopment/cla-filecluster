alter table file_groups add KEY `del_nof_idx` (`deleted`,`num_of_files`);
alter table file_groups add KEY `dirty_idx` (`dirty`);
alter table file_groups add KEY `del_dirty_idx` (`deleted`,`dirty`);

update file_groups set dirty=0 where deleted=1 and dirty=1;
update file_groups set dirty=0 where deleted=0 and dirty=1 and generated_name is not null;
