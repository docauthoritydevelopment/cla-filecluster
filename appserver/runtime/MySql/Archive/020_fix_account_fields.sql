
alter table `doc_store` drop column `account`;
alter table `doc_store` add column `account_id` int(11) NOT NULL DEFAULT 1;
alter table `doc_store` add KEY `FK_2f0nbhu6chfe6pra6w7j1u9p6` (`account_id`);
alter table `doc_store` add CONSTRAINT `FK_2f0nbhu6chfe6pra6w7j1u9p6` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`);

alter table `saved_filters` drop column `account`;
alter table `saved_filters` add column `account_id` int(11) NOT NULL DEFAULT 1;
alter table `saved_filters` add KEY `FK_t4v8kl4r53an4dqivgrv41hre` (`account_id`);
alter table `saved_filters` add CONSTRAINT `FK_t4v8kl4r53an4dqivgrv41hre` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`);
