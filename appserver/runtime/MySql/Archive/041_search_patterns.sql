CREATE TABLE `text_search_pattern` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `active` bit(1) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `pattern` varchar(255) DEFAULT NULL,
  `pattern_type` int(11) DEFAULT NULL,
  `validator_class` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

INSERT INTO `text_search_pattern` (`id`,`active`,`description`,`name`,`pattern`,`pattern_type`,`validator_class`) VALUES (1,0,'Search for emails','email',NULL,1,'com.cla.filecluster.domain.dto.filesearchpatterns.EmailTextSearchPattern');
INSERT INTO `text_search_pattern` (`id`,`active`,`description`,`name`,`pattern`,`pattern_type`,`validator_class`) VALUES (2,0,'Search for emails sample pattern','emailPattern','\\\\b[_A-Za-z0-9-]+(\\\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\\\.[A-Za-z0-9-]+)*(\\\\.[A-Za-z]{2,})\\\\b',0,NULL);
INSERT INTO `text_search_pattern` (`id`,`active`,`description`,`name`,`pattern`,`pattern_type`,`validator_class`) VALUES (3,1,'Social Security Number','ssn',NULL,1,'com.cla.filecluster.domain.dto.filesearchpatterns.SSNTextSearchPattern');
INSERT INTO `text_search_pattern` (`id`,`active`,`description`,`name`,`pattern`,`pattern_type`,`validator_class`) VALUES (4,0,'Israeli ID number','israeli-id',NULL,1,'com.cla.filecluster.domain.dto.filesearchpatterns.IsraeliIdTextSearchPattern');
INSERT INTO `text_search_pattern` (`id`,`active`,`description`,`name`,`pattern`,`pattern_type`,`validator_class`) VALUES (5,1,'Valid credit card numbers','credit-card-valid',NULL,1,'com.cla.filecluster.domain.dto.filesearchpatterns.GenericCreditCardTextSearchPattern');
INSERT INTO `text_search_pattern` (`id`,`active`,`description`,`name`,`pattern`,`pattern_type`,`validator_class`) VALUES (6,1,'Invalid credit card numbers','credit-card-invalid',NULL,1,'com.cla.filecluster.domain.dto.filesearchpatterns.GenericCreditCardBadLuhnTextSearchPattern');
INSERT INTO `text_search_pattern` (`id`,`active`,`description`,`name`,`pattern`,`pattern_type`,`validator_class`) VALUES (7,1,'IPV4 Addresses','ipv4-addr','(?:[^.\\\\-\\\\w])(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(?![\\\\-\\\\.\\\\w])',0,NULL);
INSERT INTO `text_search_pattern` (`id`,`active`,`description`,`name`,`pattern`,`pattern_type`,`validator_class`) VALUES (8,1,'IPV6 Addresses','ipv6-addr','(?<![:\\\\.\\\\w])(?:[A-Fa-f0-9]{1,4}:){7}[A-Fa-f0-9]{1,4}(?![:\\\\.\\\\w])',0,NULL);
INSERT INTO `text_search_pattern` (`id`,`active`,`description`,`name`,`pattern`,`pattern_type`,`validator_class`) VALUES (9,0,'testUpdateSearchPattern','testUpdateSearchPattern 2','[Pp]urchase [Oo]ffer',0,NULL);
