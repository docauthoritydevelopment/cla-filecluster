-- Add to file_groups a counter of the files (content) in the last naming
alter table `file_groups` add column `count_on_last_naming` bigint(20) NOT NULL default 0;

