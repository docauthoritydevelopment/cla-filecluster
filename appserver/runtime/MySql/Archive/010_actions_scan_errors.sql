ALTER TABLE `err_processed_files` ADD COLUMN `created_ondb` datetime DEFAULT NULL;

--
-- Table structure for table `err_scanned_files`
--
CREATE TABLE `err_scanned_files` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_ondb` datetime DEFAULT NULL,
  `exception_text` varchar(255) DEFAULT NULL,
  `path` varchar(4096) DEFAULT NULL,
  `text` varchar(255) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `doc_folder_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_r54t5i4jjfcyshauqmkse9bf4` (`doc_folder_id`),
  CONSTRAINT `FK_r54t5i4jjfcyshauqmkse9bf4` FOREIGN KEY (`doc_folder_id`) REFERENCES `doc_folder` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `action_definition` VALUES 
  ('COPY_FILE',5,'Copy a file to a different location','copyFile','Copy a file'),
  ('MOVE_FILE',5,'Move a file to a different location','moveFile','Move a file');
