DIRNAME=`dirname $0`
CLA_HOME=`cd $DIRNAME/../..;pwd;`
export CLA_HOME;

JAVA_OPTS="$JAVA_OPTS -Dsolr.solr.home=$CLA_HOME/solr"
CATALINA_OPTS='-Xms512m -Xmx2048m'
