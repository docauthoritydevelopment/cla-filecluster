set SOLR_DATA_DIR=D:\Data\SolrData

set DIRNAME=%~dp0
set CLA_HOME=%DIRNAME%..\..
set JAVA_OPTS=%JAVA_OPTS% -Dsolr.solr.home=%CLA_HOME%\solr -Dsolr.data.dir=%SOLR_DATA_DIR%
set CATALINA_OPTS=-Xms512m -Xmx2048m
