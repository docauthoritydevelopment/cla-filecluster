#!/bin/bash
DIRNAME=`dirname $0`
if [ -z "$2" ]; then
        echo 'usage: $0 <file-id> <file-id>'
	exit 1
fi

grep 'file1,file2' `ls -t /data/logs/similarities.log* |sort|tail -1`
#gawk -F, "{ if ((\$3==$1 || \$4==$1) && \$14==1) print \$0 }" similarities.log
gawk -F, "{ if (\$3==$1 && \$4==$2) print \$0 }" `ls -t /data/logs/sim*`
