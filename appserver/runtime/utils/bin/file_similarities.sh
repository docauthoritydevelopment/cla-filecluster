#!/bin/bash
DIRNAME=`dirname $0`
if [ -z "$1" ]; then
        echo 'usage: $0 <file-id>[,<file-id>...]'
	exit 1
fi

grep 'file1,file2' `ls -t /data/logs/similarities.log* |sort|tail -1`
#cat `ls /data/logs/sim*|sort -r` | gawk -F, "BEGIN { split(\"$1\",tmp,\",\"); for (i in tmp) ids[tmp[i]]=1; } { if (((\$3 in ids) || (\$4 in ids)) && \$(NF)==1) print \$0 }" 
cat `ls /data/logs/sim*|sort -r` | gawk -F, "BEGIN { split(\"$1\",tmp,\",\"); for (i in tmp) ids[tmp[i]]=1; } { if (((\$3 in ids) || (\$4 in ids))) print \$0 }" 
