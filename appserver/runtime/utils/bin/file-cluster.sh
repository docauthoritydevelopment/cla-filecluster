#!/bin/bash
echo "Starting file-cluster Server..."

DIRNAME=`dirname $0`
CLA_HOME=`cd $DIRNAME/..;pwd;`
export CLA_HOME;

#EXTERN_IP=`wget -qO- http://ipecho.net/plain ; echo`
EXTERN_IP=54.69.62.165
export EXTERN_IP
export JMX_REMOTE_PORT=7004
JMX_OPTS="-Dcom.sun.management.jmxremote"
JMX_OPTS="$JMX_OPTS -Dcom.sun.management.jmxremote.authenticate=false"
JMX_OPTS="$JMX_OPTS -Dcom.sun.management.jmxremote.ssl=false"
JMX_OPTS="$JMX_OPTS -Djava.rmi.server.hostname=$EXTERN_IP"
JMX_OPTS="$JMX_OPTS -Dcom.sun.management.jmxremote.port=$JMX_REMOTE_PORT"
export JMX_OPTS
MEM_OPT="-Xms4g -Xmx4g"
export MEM_OPT
GC_OPT=""
#GC_OPT="$GC_OPT -XX:+UseParNewGC"
#GC_OPT="$GC_OPT -XX:+UseConcMarkSweepGC"
#GC_OPT="$GC_OPT -XX:SurvivorRatio=6"
#GC_OPT="$GC_OPT -XX:+CMSParallelRemarkEnabled"
#GC_OPT="$GC_OPT -XX:CMSInitiatingOccupancyFraction=75"
#GC_OPT="$GC_OPT -XX:+UseCMSInitiatingOccupancyOnly"
export GC_OPT

java $MEM_OPT $GC_OPT -Dfile.encoding=UTF8 -Dspring.profiles.active=prod $JMX_OPTS -cp .:./config:./lib/:./cla-filecluster-1.0.0.jar org.springframework.boot.loader.JarLauncher