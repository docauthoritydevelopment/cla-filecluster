#!/bin/bash
#
du -h /data/logs/*
grep -A 1 'file1,file2' `ls -t /data/logs/sim*|tail -1` |tail -1; tail -1 /data/logs/similarities.log
(grep -A 1 'file1,file2' `ls -t /data/logs/sim*|tail -1` |tail -1; tail -1 /data/logs/similarities.log)|gawk -F, -v c=8838 '{ v[i++] = $1;v[i++] = $2;} END {t=(v[3]-v[1])/(v[2]-v[0]); r=(c*(c-1)/2-v[2])*t/1000/3600; p=(v[3]-v[1])/3600000; print "avg=" t "ms, running for " p " hrs, finish in " r " hrs" }'
#echo -n 'Scaning similarity logs ... '
#cat /data/logs/similarities.log*| gawk -F, "BEGIN {m=0;e=0;} { if ( \$14==1) m++; if ( \$14==2) e++; } END {print \"Found \" m \" matches and \" e \" errors.\" }" 
#echo -n 'Num of SQL exceptions: '
#cat /data/logs/similarities.log*| gawk -F, "{ if ( \$14==2) print \$0 }" |wc -l
