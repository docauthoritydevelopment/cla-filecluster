@echo off
REM
REM Install cla Solr as a service.
REM
REM 
echo Install cla Solr as a service...
SETlocal
SET DIRNAME=%~dp0

SET CLA_HOME=D:\Work\CloudAuthority\cla-filecluster2
REM SET JAVA_HOME=C:\Program Files\Java\jdk1.8.0_20
SET TOMCAT_HOME=D:\Work\CloudAuthority\cla-entity-extractor\tomcat
SET SOLR_HOME=D:\Work\CloudAuthority\cla-entity-extractor\solr
SET SOLR_DATA=D:\Data\SolrData
SET JMX_REMOTE_PORT=7005
REM SET EXTERN_IP=
SET SERVICE_NAME=ClaSolr
SET "DISPLAY_NAME=DocAuthority Solr Service"

SET JVMOPT=-Xms512m;-Xmx2g
SET "JVMOPT=%JVMOPT%;-Dsolr.solr.home=%SOLR_HOME%;-Dsolr.data.dir=%SOLR_DATA%"
SET "JVMOPT=%JVMOPT%;-Djava.util.logging.config.file=%TOMCAT_HOME%\conf\logging.properties"
SET "JVMOPT=%JVMOPT%;-Djava.util.logging.manager=org.apache.juli.ClassLoaderLogManager"
SET "JVMOPT=%JVMOPT%;-Djava.endorsed.dirs=%TOMCAT_HOME%\endorsed"
SET "JVMOPT=%JVMOPT%;-Dcatalina.base=%TOMCAT_HOME%;-Dcatalina.home=%TOMCAT_HOME%"
SET "JVMOPT=%JVMOPT%;-Djava.io.tmpdir=%TOMCAT_HOME%\temp"
SET JVMOPT=%JVMOPT%;-Dcom.sun.management.jmxremote.authenticate=false
SET JVMOPT=%JVMOPT%;-Dcom.sun.management.jmxremote.ssl=false
REM SET JVMOPT=%JVMOPT%;-Dcom.sun.management.jmxremote.port=%JMX_REMOTE_PORT%
REM SET JVMOPT=%JVMOPT%;-Djava.rmi.server.hostname=%EXTERN_IP%
SET GCOPT=
SET STARTCLASS=org.apache.catalina.startup.Bootstrap
SET "CP=%TOMCAT_HOME%\bin\bootstrap.jar;%TOMCAT_HOME%\bin\tomcat-juli.jar"

%DIRNAME%procrun\prunsrv.exe update %SERVICE_NAME% --Install="%DIRNAME%procrun\prunsrv.exe" --Jvm=auto --Startup=manual --JavaHome "%JAVA_HOME%" ^
 --StartMode java --StartClass %STARTCLASS% --StartParams start --StopMode java --StopClass %STARTCLASS% --StopParams  stop ^
 --Classpath="%CP%" --DisplayName="%DISPLAY_NAME%" ^
 --JvmOptions="%JVMOPT%;%GCOPT%" --LogPath "%CLA_HOME%\logs" --PidFile PidFile%SERVICE_NAME%.txt --LogPrefix %SERVICE_NAME% --StartPath "%CLA_HOME%" ^
 --StdOutput auto --StdError auto --StopTimeout 5
 
:finally
endlocal
