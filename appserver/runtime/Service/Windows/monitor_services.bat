@echo off
REM
REM Start monitoring for cla services.
REM
REM 
echo Activate PROCRUN monitoring for the CLA services
setlocal
SET DIRNAME=%~dp0
cd %DIRNAME%

set "MONITOR=%DIRNAME%procrun\prunmgr.exe"
start %MONITOR% //MS//ClaFilecluster
start %MONITOR% //MS//ClaSolr
start %MONITOR% //MS//DataStax_Cassandra_Community_Server

endlocal
