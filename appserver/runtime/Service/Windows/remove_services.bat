@echo off
REM
REM Remove installed cla services.
REM
REM 
echo Remove CLA services...
setlocal
SET DIRNAME=%~dp0
SET REMOVE_CMD="%DIRNAME%procrun\prunsrv.exe" delete
SET STOP_MONITOR="%DIRNAME%procrun\prunmgr.exe" //MQ//

%STOP_MONITOR%ClaFilecluster
%STOP_MONITOR%ClaSolr
%STOP_MONITOR%DataStax_Cassandra_Community_Server

%REMOVE_CMD% ClaFilecluster
%REMOVE_CMD% ClaSolr
endlocal
