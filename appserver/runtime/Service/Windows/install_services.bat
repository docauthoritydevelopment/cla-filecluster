@echo off
REM
REM Installed cla services.
REM
REM 
echo Install CLA Services...
setlocal
SET DIRNAME=%~dp0
cd %DIRNAME%
call install_solr_service.bat
call install_filecluster_service.bat

endlocal
