REM @echo off
REM
REM Install cla-filecluster as a service.
REM
REM Work in progress:
REM 	Todo: implement start / stop mechanism to gracefully stop the service
REM		Todo: Wrap Solr as a service as well
REM		Todo: All long processing shold be 'auto restartable', including ingest, processing state should be persistant.
REM 	Todo: avoid duplicate logging in 'filecluster' and stdout (console). (disable console logging)
REM 
echo Script is not finished yet ...
REM goto finally

setlocal
set DIRNAME=%~dp0
set CLA_HOME=D:\Work\CloudAuthority\cla-filecluster2
REM set JAVA_HOME=C:\Program Files\Java\jdk1.8.0_20
set SERVICE_NAME=ClaFilecluster
SET "DISPLAY_NAME=DocAuthority Filecluster"
set LIB_ROOT=%CLA_HOME%\build\libs\
REM set CONTENT_DIR=.\src\main\resources\static
set CONTENT_DIR=%CLA_HOME%\static
set JMX_REMOTE_PORT=7004
set DEPENDENCIES=ClaSolr;DataStax_Cassandra_Community_Server;MySQL56
set SERVER_PORT=8080
set STOP_URL="http://localhost:%SERVER_PORT%/stop"
set STOP_IMAGE="%DIRNAME%curl\curl.exe"

set JVMOPT=-Xms2g;-Xmx2g;-Xss128k;-Dspring.profiles.active=win;-Dfile.encoding=UTF8
set JVMOPT=%JVMOPT%;-Dcom.sun.management.jmxremote.authenticate=false
set JVMOPT=%JVMOPT%;-Dcom.sun.management.jmxremote.ssl=false
set JVMOPT=%JVMOPT%;-Dcom.sun.management.jmxremote.port=%JMX_REMOTE_PORT%
REM set JVMOPT=%JVMOPT%;-Djava.rmi.server.hostname=%EXTERN_IP%

set GCOPT=-XX:+UseParNewGC;-XX:+UseConcMarkSweepGC;-XX:+CMSParallelRemarkEnabled;-XX:SurvivorRatio=8;-XX:MaxTenuringThreshold=15;-XX:CMSInitiatingOccupancyFraction=75;-XX:+UseCMSInitiatingOccupancyOnly
set STARTCLASS=org.springframework.boot.loader.JarLauncher

SET CP=%CLA_HOME%\;%CLA_HOME%\config;%LIB_ROOT%;%LIB_ROOT%lib\;%LIB_ROOT%cla-filecluster-1.0.0.jar;%CONTENT_DIR%
REM java %JVMOPT% %GCOPT% %CP% %STARTCLASS% 

%DIRNAME%procrun\prunsrv.exe update %SERVICE_NAME% --Install="%DIRNAME%procrun\prunsrv.exe" --Jvm=auto --Startup=manual --JavaHome "%JAVA_HOME%" ^
 --StartMode java --StartClass %STARTCLASS% --StartParams start --StopMode exe --StopImage %STOP_IMAGE% --StopParams %STOP_URL% ^
 --Classpath="%CP%" --DisplayName="%DISPLAY_NAME%" --DependsOn "%DEPENDENCIES%" ^
 --JvmOptions="%JVMOPT%;%GCOPT%" --LogPath "%CLA_HOME%\logs" --PidFile PidFile%SERVICE_NAME%.txt --LogPrefix %SERVICE_NAME% --StartPath "%CLA_HOME%" ^
 --StdOutput auto --StdError auto --StopTimeout 5
  
:finally
endlocal
