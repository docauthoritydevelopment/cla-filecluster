@echo off
REM  
REM Activation of this service with the following parameters:
REM 	- no-parameters - start the daemon in a separate process
REM		- 'DoService' (for internal use only) run the daemon loop
REM
REM 
REM
set SRC_DIR1="C:\Enron\Extraction"
set DST_DIR="C:\Enron\RescanTest"
set WAIT_TIME=1800
set LOGFILE="C:\DocAuthority\RescanTest.log"

REM
REM To make script operational remove 'ECHO _' from the following two lines.
set CP_CMD=ECHO _XCOPY /O /I /E

if "x%1" == "xDoService" goto startDaemon
start "DA RescanTest" /MIN CMD /C %0 DoService
goto :finally

:startDaemon

goto doService

REM Relevant for file poling deamon
:polingLoop
if exist %ACTION_FILENAME% goto doService
timeout /t 360000
if NOT %ERRORLEVEL% == 0 goto finally
goto polingLoop

:doService
@ECHO ON
CD %SRC_DIR1%
FOR /D %%G IN ( * ) DO (
	%CP_CMD% "%%G" %DST_DIR%\"%%G"  || ( ECHO %time% Err %ERRORLEVEL% cp %%G %DST_DIR% >> %LOGFILE% ) 
	ECHO %%G copied
	timeout /t %WAIT_TIME%
)
@ECHO OFF
goto polingLoop

:exitPoll
echo Action file %ACTION_FILENAME% esists after deleted. Exiting restart loop  1>>%LOGFILE% 2>&1
REM timeout /t 3600
REM goto polingLoop

:finally
ENDLOCAL
