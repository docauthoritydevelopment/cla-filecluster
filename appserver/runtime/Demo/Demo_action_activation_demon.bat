@echo off
REM  
REM Activation of this service with the following parameters:
REM 	- no-parameters - start the Demo-script-activation daemon in a separate process
REM		- 'DoService' (for internal use only) run the daemon loop
REM
REM     Flow:
REM         Periodically look for activation file presence. Once found, delete it and perform the reset action:
REM             Look for *.bat files under the 'action' sub-tree and process them;
REM             Process up to 10 files in a row.
REM             Process file by calling it in a separate CMD window and wait until it ends.
REM             After processing, move the file to the 'done' folder.
REM             Wait a second after files are identified to make sure server finish creating them.
REM 
REM
set BASE_FOLDER=C:\DocAuthority\filecluster\temp
REM set BASE_FOLDER=D:\Work\DA\cla-filecluster\runtime\Demo
set ACTION_SCRIPTS_FOLDER="%BASE_FOLDER%\act"
set DONE_SCRIPTS_FOLDER="%BASE_FOLDER%\done"
set LOGS_FOLDER=%BASE_FOLDER%\logs
set ACK_ERR_FILENAME="%LOGS_FOLDER%\action_demon_err.txt"
set LOGFILE="%LOGS_FOLDER%\action_demon.log"
set /A MAX_POLLED=10
REM
REM To make script operational remove 'ECHO _' from the following two lines.
set RM_CMD=ECHO _RMDIR /Q /S
set CP_CMD=ECHO _XCOPY /O /I /E
set MV_CMD=MOVE /Y 

SETLOCAL ENABLEDELAYEDEXPANSION

if "x%1" == "xDoService" goto startDaemon
start "DA Action script activation demon" CMD /C %0 DoService
goto :finally

:startDaemon

:polingLoop
timeout /t 30
SET count=0
FOR /F "tokens=* USEBACKQ" %%F IN (`dir /s/b %ACTION_SCRIPTS_FOLDER%\"*.bat"`) DO (
  SET /a count=!count!+1
  SET var[!count!]=%%F
  IF !count!==%MAX_POLLED% goto loopBreak
)
if %count%==0 goto polingLoop

:loopBreak
ECHO Processing %count% files
timeout /t 1
FOR /L %%G IN (1,1,%count%) DO (
  ECHO Activating "!var[%%G]!" ...
  CALL :activate "!var[%%G]!"
  %MV_CMD% "!var[%%G]!" %DONE_SCRIPTS_FOLDER%
)
goto polingLoop

:activate
START /WAIT /D "%BASE_FOLDER%" CMD /C %1
goto :eof

:finally
ENDLOCAL
