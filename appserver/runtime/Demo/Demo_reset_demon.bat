@echo off
REM  
REM Activation of this service with the following parameters:
REM 	- no-parameters - start the Demo-resetart daemon in a separate process - need to be activated as 'Admin'
REM		- 'restart' - initial service restart action by the running daemon
REM		- 'DoService' (for internal use only) run the daemon loop
REM
REM     Flow:
REM         Periodically look for activation file presence. Once found, delete it and perform the reset action:
REM             1) Stop Solr, MySql services; 2) Delete and restore content of data dirs; 3) Start the services;
REM 
REM this batch should run as ADMIN to allow service re-start
REM
set ACTION_FILENAME="C:\Data\GoogleDrive\DemoControl\_DoReset.txt"
set ACK_ERR_FILENAME="C:\Data\GoogleDrive\DemoControl\_ResetError.txt"
set ACK_OK_FILENAME="C:\Data\GoogleDrive\DemoControl\_ResetDone.txt"

set LOGFILE="C:\Data\GoogleDrive\DemoControl\DemoReset.log"
set SRV_NAME1=DocAuthority-solr
set SRV_NAME2=MySQL56
set SRC_DIR1="D:\DemoReference\MySql\data\ec_db"
set DST_DIR1="C:\ProgramData\MySQL\MySQL Server 5.6\Data\ec_db"
set SRC_DIR2="D:\DemoReference\Solr\DaSolrData"
set DST_DIR2="C:\DaSolrData"

REM
REM To make script operational remove 'ECHO _' from the following two lines.
set RM_CMD=ECHO _RMDIR /Q /S
set CP_CMD=ECHO _XCOPY /O /I /E

set START_CMD=sc start
set STOP_CMD=sc stop
set QUERY_CMD=sc query

if "x%1" == "xDoService" goto startDaemon
if "x%1" == "xrestart" goto restart
start "DA Demo reset demon" /MIN CMD /C %0 DoService
goto :finally

:restart
echo Signal action for %0 > %ACTION_FILENAME%
goto finally

:startDaemon

:polingLoop
if exist %ACTION_FILENAME% goto restartService
timeout /t 5
if NOT %ERRORLEVEL% == 0 goto finally
goto polingLoop

:restartService
@ECHO ON
del %ACTION_FILENAME%

%STOP_CMD% %SRV_NAME1% || ( ECHO %time% Err %ERRORLEVEL% stop %SRV_NAME1% >> %ACK_ERR_FILENAME% ) 
%STOP_CMD% %SRV_NAME2% || ( ECHO %time% Err %ERRORLEVEL% stop %SRV_NAME2% >> %ACK_ERR_FILENAME% ) 
timeout /t 15
%RM_CMD% %DST_DIR1% || ( ECHO %time% Err %ERRORLEVEL% del %DST_DIR1% >> %ACK_ERR_FILENAME% ) 
%CP_CMD% %SRC_DIR1% %DST_DIR1%  || ( ECHO %time% Err %ERRORLEVEL% cp %SRC_DIR1% %DST_DIR1% >> %ACK_ERR_FILENAME% ) 
%RM_CMD% %DST_DIR2% || ( ECHO %time% Err %ERRORLEVEL% del %DST_DIR2% >> %ACK_ERR_FILENAME% ) 
%CP_CMD% %SRC_DIR2% %DST_DIR2% || ( ECHO %time% Err %ERRORLEVEL% cp %SRC_DIR2% %DST_DIR2% >> %ACK_ERR_FILENAME% ) 
timeout /t 1
%START_CMD% %SRV_NAME1% || ( ECHO %time% Err %ERRORLEVEL% start %SRV_NAME1% >> %ACK_ERR_FILENAME% ) 
%START_CMD% %SRV_NAME2% || ( ECHO %time% Err %ERRORLEVEL% start %SRV_NAME2% >> %ACK_ERR_FILENAME% ) 
timeout /t 5
echo Reset done at %time% >> %ACK_OK_FILENAME%
echo Reset done at %time% >> %LOGFILE%%
if exist %ACTION_FILENAME% goto exitPoll
@ECHO OFF
goto polingLoop

:exitPoll
echo Action file %ACTION_FILENAME% exists after deleted. Exiting restart loop  1>>%LOGFILE% 2>&1
echo Action file %ACTION_FILENAME% exists after deleted. Exiting restart loop  1>>%ACK_ERR_FILENAME% 2>&1
REM timeout /t 3600
REM goto polingLoop

:finally
ENDLOCAL
