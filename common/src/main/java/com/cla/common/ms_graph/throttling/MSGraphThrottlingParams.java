package com.cla.common.ms_graph.throttling;

import com.cla.common.ms_graph.MsGraphParams;
import com.cla.common.ms_graph.util.NestedBuilder;
import lombok.Data;


@Data
public class MSGraphThrottlingParams {
    private final ThrottlingStartledTurtle throttler;
    private final int maxRetriesOnThrottling;

    private MSGraphThrottlingParams(Builder builder) {
        this.throttler = builder.throttler;
        this.maxRetriesOnThrottling = builder.maxRetriesOnThrottling;
    }

    public static final class Builder extends NestedBuilder<MsGraphParams.Builder, MSGraphThrottlingParams> {
        private ThrottlingStartledTurtle throttler;
        private int maxRetriesOnThrottling;

        public Builder withThrottler(ThrottlingStartledTurtle throttler) {
            this.throttler = throttler;
            return this;
        }

        public Builder withMaxRetriesOnThrottling(int maxRetriesOnThrottling) {
            this.maxRetriesOnThrottling = maxRetriesOnThrottling;
            return this;
        }

        @Override
        public MSGraphThrottlingParams build() {
            return new MSGraphThrottlingParams(this);
        }
    }
}
