package com.cla.common.ms_graph.auth;

import com.cla.common.ms_graph.MsGraphParams;
import com.cla.common.ms_graph.util.NestedBuilder;
import lombok.Data;


@Data
public class MSGraphAuthConfig {
    private final int maxAttempts;
    private final int retrySleepInterval;
    private final int prefetchTokenBeforeExpiry;
    private final long readTimeout;
    private final long connectTimeout;

    private MSGraphAuthConfig(Builder builder) {
        this.maxAttempts = builder.maxAttempts;
        this.retrySleepInterval = builder.retrySleepInterval;
        this.prefetchTokenBeforeExpiry = builder.prefetchTokenBeforeExpiry;
        this.readTimeout = builder.readTimeout;
        this.connectTimeout = builder.connectTimeout;
    }

    public static final class Builder extends NestedBuilder<MsGraphParams.Builder, MSGraphAuthConfig> {

        private int maxAttempts;
        private int retrySleepInterval;
        private int prefetchTokenBeforeExpiry;
        private int readTimeout;
        private long connectTimeout;


        public Builder withMaxAttempts(int maxAttempts) {
            this.maxAttempts = maxAttempts;
            return this;
        }
        public Builder withRetrySleepInterval(int retrySleepInterval) {
            this.retrySleepInterval = retrySleepInterval;
            return this;
        }

        public Builder withPrefetchTokenBeforeExpiry(int prefetchTokenBeforeExpiry) {
            this.prefetchTokenBeforeExpiry = prefetchTokenBeforeExpiry;
            return this;
        }

        public Builder withReadTimeout(int readTimeout) {
            this.readTimeout = readTimeout;
            return this;
        }

        public Builder withConnectTimeout(long connectTimeout) {
            this.connectTimeout = connectTimeout;
            return this;
        }

        @Override
        public MSGraphAuthConfig build() {
            return new MSGraphAuthConfig(this);
        }
    }


}
