package com.cla.common.ms_graph;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.microsoft.graph.auth.confidentialClient.ClientCredentialProvider;
import com.microsoft.graph.auth.enums.NationalCloud;
import com.microsoft.graph.content.MSBatchRequestContent;
import com.microsoft.graph.content.MSBatchRequestStep;
import com.microsoft.graph.httpcore.*;
import lombok.extern.slf4j.Slf4j;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;


@Slf4j
public class MSGraphClient {

    public static final String MS_GRAPH_URL = "https://graph.microsoft.com/v1.0";
    private final OkHttpClient httpClient;
    private final OkHttpClient unAuthenticatedClient;

    public MSGraphClient(MsGraphParams graphParams) {

        MSGraphConnectionParams connectionParams = graphParams.getConnectionParams();
        ClientCredentialProvider authProvider = new ClientCredentialProvider(
                connectionParams.getApplicationId(),
                Stream.of("https://graph.microsoft.com/.default").collect(Collectors.toList()),
                connectionParams.getPassword(),
                connectionParams.getTenantId(),
                NationalCloud.Global);

        // todo: add timeouts configurations + retry configuration + throttling
        //httpClient = HttpClients.createDefault(authProvider)
        httpClient = HttpClients.custom()
                .followRedirects(false)
                .addInterceptor(new AuthenticationHandler(authProvider))
                .addInterceptor(new RetryHandler())
                .addInterceptor(new RedirectHandler())
                .addInterceptor(new TelemetryHandler())
                .callTimeout(connectionParams.getConnectionReadTimeout(), TimeUnit.SECONDS)
                .readTimeout(connectionParams.getConnectionReadTimeout(), TimeUnit.SECONDS)
                .connectTimeout(connectionParams.getConnectionConnectTimeout(), TimeUnit.SECONDS)
                .build();
        unAuthenticatedClient = new OkHttpClient();
    }

    /**
     * creates a batch request from a list of requests the requests will be created with a request id
     * corresponding to the index in the provided list
     * the methods supports null elements in the provided list.
     * if there are null elements, the requests will be created skipping the null elements but keeping the order of the
     * request ids
     * if the provided list contains only null requests, an empty optional will be returned.
     *
     * @param requests a list of requests to put in a batch
     * @return an Optional of a batch request. if all elements were null, an empty Optional will be returned.
     */

    public Optional<Request> createBatchRequest(@Nonnull List<Request> requests) {
        AtomicInteger requestIndex = new AtomicInteger(0);
        List<MSBatchRequestStep> batchRequestSteps = requests.stream()
                .map(request -> {
                    int index = requestIndex.getAndIncrement();
                    return request == null ?
                            null :
                            new MSBatchRequestStep(String.valueOf(index), request, null);
                })
                .filter(Objects::nonNull)
                .collect(Collectors.toList());

        if (batchRequestSteps.isEmpty()) {
            return Optional.empty();
        }

        MSBatchRequestContent requestContent = new MSBatchRequestContent(batchRequestSteps);
        String content = requestContent.getBatchRequestContent();

        return Optional.of(new Request
                .Builder()
                .url("https://graph.microsoft.com/v1.0/$batch")
                .post(RequestBody.create(MediaType.parse("application/json"), content))
                .build());
    }

    public <T> T executeRequest(Request request, Function<Response, T> responseTransformer) {
        try {
            Response response = httpClient.newCall(request).execute();
            return responseTransformer.apply(response);
        } catch (IOException e) {
            log.error("error executing request to {} {}", request.method(), request.url(), e);
            throw new RuntimeException(String.format("error executing request to %s %s", request.method(), request.url()), e);
        }
    }

    public void executeRequest(Request request, Consumer<Response> responseHandler) {
        executeRequest(request, response -> {
            responseHandler.accept(response);
            return null;
        });
    }

    public Stream<JsonObject> executeRequestJsonResponse(Request request) {
        return executeRequest(request, response -> {
            try {
                if (response.body() == null) {
                    throw new IllegalArgumentException("body is null");
                }
                String responseBody = response.body().string();
                JsonParser parser = new JsonParser();
                JsonObject bodyJson = parser.parse(responseBody).getAsJsonObject();
                if (bodyJson.has("responses")) {
                    // batch call, stream sub elements
                    JsonArray responses = bodyJson.get("responses").getAsJsonArray();
                    return StreamSupport
                            .stream(responses.spliterator(), false)
                            .map(JsonElement::getAsJsonObject);
                } else {
                    // non-batch, stream the response as a single element
                    JsonObject resultObj = new JsonObject();
                    resultObj.add("body", bodyJson);
                    resultObj.addProperty("id", "0");
                    resultObj.addProperty("status", response.code());
                    //TODO add response headers
                    return Stream.of(resultObj);
                }
            } catch (Exception e) {
                log.error("Error processing Response from {}", request.url(), e);
                throw new RuntimeException(String.format("Error processing Response from %s", request.url()), e);
            }
        });
    }

    public JsonObject executeRequestJsonSingleResponse(Request request) {
        return executeRequestJsonResponse(request).collect(Collectors.collectingAndThen(
                Collectors.toList(),
                list -> {
                    if (list.size() != 1) {
                        throw new IllegalStateException(String.format("Only one response should return, got %s", list.size()));
                    }
                    return list.get(0);
                }));
    }

    public void checkResponse(JsonObject siteResponseObj, String errorMessage) {
        int status = siteResponseObj.get("status").getAsInt();
        if (status != 200) {
            throw new RuntimeException(String.format("msg: %s code: %d, errorBody: %s",
                    errorMessage,
                    status,
                    siteResponseObj.has("body") ?
                            siteResponseObj.get("body").getAsJsonObject().toString() :
                            ""));
        }
    }

    public Response executeUnauthenticated(Request request) {
        try {
            return unAuthenticatedClient.newCall(request).execute();
        } catch (IOException e) {
            log.error("error executing request to {} {}", request.method(), request.url(), e);
            throw new RuntimeException(String.format("error executing request to %s %s", request.method(), request.url()), e);
        }
    }
}
