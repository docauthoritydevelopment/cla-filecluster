package com.cla.common.ms_graph;

import com.cla.common.ms_graph.auth.MSGraphAuthConfig;
import com.cla.common.ms_graph.throttling.MSGraphThrottlingParams;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Created By Itai Marko
 */
public final class MsGraphParams {
    @Getter private final MSGraphConnectionParams connectionParams;
    @Getter private final MSGraphAuthConfig authConfig;
    @Getter private final MSGraphThrottlingParams throttlingParams;
    @Getter private final int graphBatchMaxSize;

    private MsGraphParams(Builder builder) {
        this.connectionParams = builder.connectionParams;
        this.authConfig = builder.authConfig;
        this.throttlingParams = builder.throttlingParams;
        this.graphBatchMaxSize = builder.graphBatchMaxSize;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {

        private MSGraphConnectionParams connectionParams;
        private MSGraphAuthConfig authConfig;
        private MSGraphThrottlingParams throttlingParams;
        private int graphBatchMaxSize;

        public Builder withConnectionParams(MSGraphConnectionParams connectionParams) {
            this.connectionParams = connectionParams;
            return this;
        }

        public Builder withAuthConfig(MSGraphAuthConfig authConfig) {
            this.authConfig = authConfig;
            return this;
        }

        public Builder withThrottlingParams(MSGraphThrottlingParams throttlingParams) {
            this.throttlingParams = throttlingParams;
            return this;
        }

        public Builder withGraphBatchMaxSize(int maxSize) {
            this.graphBatchMaxSize = maxSize;
            return this;
        }

        public MsGraphParams build() {
            return new MsGraphParams(this);
        }

        public MSGraphConnectionParams.Builder addConnectionParams() {
            return new MSGraphConnectionParams.Builder().withParentBuilder(this);
        }

        public MSGraphAuthConfig.Builder addAuthConfig() {
            return new MSGraphAuthConfig.Builder().withParentBuilder(this);
        }

        public MSGraphThrottlingParams.Builder addThrottlingParams() {
            return new MSGraphThrottlingParams.Builder().withParentBuilder(this);
        }
    }
}
