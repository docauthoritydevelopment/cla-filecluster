package com.cla.common.ms_graph.util;

import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

@Slf4j
public abstract class NestedBuilder<T, V> {
    /**
     * To get the parent builder
     *
     * @return T the instance of the parent builder
     */
    public T done() {
        Class<?> parentClass = parent.getClass();
        V build = this.build();

        String methodName = "with" + getFieldName(parentClass, build);

        try {
            Method method = parentClass.getDeclaredMethod(methodName, build.getClass());
            method.invoke(parent, build);
        } catch (NoSuchMethodException
                | IllegalAccessException
                | InvocationTargetException e) {
            log.error("builder error invoking {}", methodName, e);
        }
        return parent;
    }

    private String getFieldName(Class<?> parentClass, V build) {
        for (Field field : parentClass.getDeclaredFields()) {
            if (field.getType().equals(build.getClass())) {
                String name = field.getName();
                return name.substring(0, 1).toUpperCase() + name.substring(1);
            }
        }
        return build.getClass().getSimpleName();
    }

    public abstract V build();
    protected T parent;
    /**
     * @param parent
     * @return
     */
    public <P extends NestedBuilder<T, V>> P withParentBuilder(T parent) {
        this.parent = parent;
        return (P) this;
    }
}