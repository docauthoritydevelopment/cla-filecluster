package com.cla.common.ms_graph.throttling;

import com.cla.common.ms_graph.throttling.MSGraphThrottlingParams;
import com.microsoft.graph.concurrency.ICallback;
import com.microsoft.graph.core.ClientException;
import com.microsoft.graph.core.IConnectionConfig;
import com.microsoft.graph.http.GraphServiceException;
import com.microsoft.graph.http.IHttpProvider;
import com.microsoft.graph.http.IHttpRequest;
import com.microsoft.graph.http.IStatefulResponseHandler;
import com.microsoft.graph.serializer.ISerializer;
import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.Callable;

/**
 * A decorator around {@link IHttpProvider} that wraps the IHttpProvider send() methods to handle throttling.
 * <p/>
 * The Wrapped send() methods are: <ul>
 *     <li>{@link IHttpProvider#send(IHttpRequest, Class, Object)}</li>
 *     <li>{@link IHttpProvider#send(IHttpRequest, ICallback, Class, Object)}</li>
 *     <li>{@link IHttpProvider#send(IHttpRequest, Class, Object, IStatefulResponseHandler)}</li>
 * </ul>
 *
 * <p/>
 * Handling throttling means two things: <ol>
 *     <li>Acquiring a throttling permit from the {@link ThrottlingStartledTurtle throttler} supplied to the constructor</li>
 *     <li>Catching throttling exception, waiting for the throttling period to pass and retrying the send() call afterwards</li>
 * </ol>
 *
 * <p/>
 * Created By Itai Marko
 */
public class MsGraphThrottlingHttpProvider implements IHttpProvider {

    private static final Logger logger = LoggerFactory.getLogger(MsGraphThrottlingHttpProvider.class);

    /**
     * Used to access the private field {@link GraphServiceException#requestHeaders} by refection
     */
    private static final String GRAPH_SERVICE_EXCEPTION_RESPONSE_HEADERS_FIELD_NAME = "responseHeaders";

    /**
     * Used to search for the Retry-After header among other headers
     */
    private static final String RETRY_AFTER_HEADER_PREFIX = "Retry-After : ";

    private static final String REFLECTION_ERROR_HINT = " Did we upgrade the version of MS-Graph SDK from com.microsoft.graph:microsoft-graph:1.1.0 to a newer version?";

    private final IHttpProvider delegate;
    private final ThrottlingStartledTurtle throttler;

    /**
     * Maximum number of attempts to retry sending a call to Exchange 365 MS-Graph API.
     * Zero or negative values indicates to retry forever.
     */
    private final Integer maxRetriesOnThrottling;

    public MsGraphThrottlingHttpProvider(IHttpProvider delegate, MSGraphThrottlingParams throttlingParams) {

        this.delegate = Objects.requireNonNull(delegate);
        this.throttler = Objects.requireNonNull(throttlingParams).getThrottler();
        this.maxRetriesOnThrottling = throttlingParams.getMaxRetriesOnThrottling();
    }

    @Override
    public ISerializer getSerializer() {
        return delegate.getSerializer();
    }

    @Override
    public IConnectionConfig getConnectionConfig() {
        return delegate.getConnectionConfig();
    }

    @Override
    public void setConnectionConfig(IConnectionConfig connectionConfig) {
        delegate.setConnectionConfig(connectionConfig);
    }

    @Override
    public <Result, BodyType> void send(
            IHttpRequest request,
            ICallback<Result> callback,
            Class<Result> resultClass,
            BodyType serializable) {


        Callable<Void> actualSend = () -> {
            delegate.send(request, callback, resultClass, serializable);
            return null;
        };
        callHandlingThrottling(actualSend);
    }

    @Override
    public <Result, BodyType> Result send(IHttpRequest request,
                                          Class<Result> resultClass,
                                          BodyType serializable) throws ClientException {

        Callable<Result> actualSend = () ->
                delegate.send(request, resultClass, serializable);

        return callHandlingThrottling(actualSend);
    }

    @Override
    public <Result, BodyType, DeserializeType> Result send(
            IHttpRequest request,
            Class<Result> resultClass,
            BodyType serializable,
            IStatefulResponseHandler<Result, DeserializeType> handler) throws ClientException {

        Callable<Result> actualSend = () ->
                delegate.send(request, resultClass, serializable, handler);

        return callHandlingThrottling(actualSend);
    }

    /**
     * Executes the sendCall after acquiring throttling permit, handling any throttling Exceptions
     *
     * @param sendCall the send call to execute
     * @param <T>      the return type of the send call
     * @return the return value of the send call
     */
    private <T> T callHandlingThrottling(Callable<T> sendCall) {

        // Wrap the send call by a callable that waits for throttling permit first
        Callable<T> sendCallAfterAcquiringPermit = () -> {
            if (throttler.acquireThrottlingPermit()) {
                return sendCall.call();
            } else {
                throw new RuntimeException("Could not acquire throttling permit");
            }
        };

        // Further wrap the send call to handle possible throttling exception
        return tryCallAndRetryOnThrottlingException(sendCallAfterAcquiringPermit, sendCall, 0);
    }

    /**
     * Executes the send call and retries it if a throttling exception is thrown
     *
     * @param sendCall      the send call to execute and handle its possible throttling exception
     * @param retrySendCall the send call to execute when retrying, this should be the inner call
     * @param <T>           the return type of the send call
     * @return the return value of the send call
     */
    @SneakyThrows(Exception.class) // The sendCall and retrySendCall only wraps one of IHttpProvider send() methods which do not declare to throw checked exceptions
    private <T> T tryCallAndRetryOnThrottlingException(Callable<T> sendCall,
                                                       Callable<T> retrySendCall,
                                                       int retryCount) {

        if (maxRetriesOnThrottling <= 0) {
            logger.warn("Retrying attempt #{}, maxRetriesOnThrottling is set to {} which means retrying forever",
                    retryCount, maxRetriesOnThrottling);
        } else if (retryCount > maxRetriesOnThrottling) {
            // Stop recursion
            throw new RuntimeException("Exceeded max throttling retries of " + maxRetriesOnThrottling);
        }

        try {
            return sendCall.call();

        } catch (GraphServiceException e) {
            if (e.getResponseCode() != 429) {
                throw e;
            }

            // Handle Throttling Exception
            logger.warn("Got throttled: ", e);
            retryCount++;
            int retryAfterSeconds = extractRetryAfterHeader(e);
            if (throttler.throttleFor(retryAfterSeconds)) {
                logger.info("Retry #{} after throttled for {} seconds", retryCount, retryAfterSeconds);
                // Recurse for retry
                return tryCallAndRetryOnThrottlingException(retrySendCall, retrySendCall, retryCount);
            } else {
                throw new RuntimeException("Could not acquire throttling permit");
            }
        }
    }

    /**
     * Use reflection to extract the
     * {@link MsGraphThrottlingHttpProvider#GRAPH_SERVICE_EXCEPTION_RESPONSE_HEADERS_FIELD_NAME} private List of String
     * headers and search for the Retry-After header by the
     * {@link MsGraphThrottlingHttpProvider#RETRY_AFTER_HEADER_PREFIX Retry-After header prefix}
     *
     * @param throttlingException An exception thrown by the MS-Graph SDK that indicates throttling
     * @return the extracted Retry-After header
     */
    private static int extractRetryAfterHeader(GraphServiceException throttlingException) {
        try {
            Field responseHeadersField = throttlingException
                    .getClass()
                    .getDeclaredField(GRAPH_SERVICE_EXCEPTION_RESPONSE_HEADERS_FIELD_NAME);
            if (Modifier.isPrivate(responseHeadersField.getModifiers())) {
                responseHeadersField.setAccessible(true);
            }
            Object responseHeadersObj = responseHeadersField.get(throttlingException);
            //noinspection unchecked
            List<String> responseHeaders = (List<String>) responseHeadersObj;
            for (String responseHeader : responseHeaders) {
                if (responseHeader != null &&
                        responseHeader.toLowerCase().startsWith(RETRY_AFTER_HEADER_PREFIX.toLowerCase())) {

                    String retryAfterStr = responseHeader.substring(RETRY_AFTER_HEADER_PREFIX.length());
                    retryAfterStr = retryAfterStr.trim();
                    int retryAfterSeconds = Integer.parseInt(retryAfterStr);
                    if (retryAfterSeconds <= 0) {
                        throw new RuntimeException("Extracted a non-positive Retry-After header of: " + retryAfterSeconds + " from a throttling exception: " + throttlingException.getMessage());
                    }
                    return retryAfterSeconds;
                }
            }

            throw new RuntimeException("Could not find the Retry-After header in a throttling Exception using prefix:" + RETRY_AFTER_HEADER_PREFIX + " extracted headers: " + responseHeaders + REFLECTION_ERROR_HINT);

        } catch (NoSuchFieldException e) {
            throw new RuntimeException("Got throttlingException without a declared field: " + GRAPH_SERVICE_EXCEPTION_RESPONSE_HEADERS_FIELD_NAME + REFLECTION_ERROR_HINT, e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException("Couldn't access " + GRAPH_SERVICE_EXCEPTION_RESPONSE_HEADERS_FIELD_NAME + " private field of class " + GraphServiceException.class.getSimpleName() + " even after setting it to be accessible " + REFLECTION_ERROR_HINT, e);
        }
    }
}
