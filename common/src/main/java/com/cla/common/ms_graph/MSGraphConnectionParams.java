package com.cla.common.ms_graph;

import com.cla.common.ms_graph.util.NestedBuilder;
import lombok.Data;

@Data
public class MSGraphConnectionParams {
    private final String tenantId;
    private final String applicationId;
    private final String password;
    private final String adUrlOverride;
    private final String graphEndpointOverride;
    private final long connectionConnectTimeout;
    private final long connectionReadTimeout;

    private MSGraphConnectionParams(Builder builder) {
        this.tenantId = builder.tenantId;
        this.applicationId = builder.applicationId;
        this.password = builder.password;
        this.adUrlOverride = builder.adUrlOverride;
        this.graphEndpointOverride = builder.graphEndpointOverride;
        this.connectionConnectTimeout = builder.connectionConnectTimeout;
        this.connectionReadTimeout = builder.connectionReadTimeout;
    }

    public static final class Builder extends NestedBuilder<MsGraphParams.Builder, MSGraphConnectionParams> {

        private String tenantId;
        private String applicationId;
        private String password;
        private String adUrlOverride;
        private String graphEndpointOverride;
        private long connectionConnectTimeout;
        private long connectionReadTimeout;

        public Builder withTenantId(String tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public Builder withApplicationId(String applicationId) {
            this.applicationId = applicationId;
            return this;
        }

        public Builder withPassword(String password) {
            this.password = password;
            return this;
        }

        public Builder withAdUrlOverride(String adUrlOverride) {
            this.adUrlOverride = adUrlOverride;
            return this;
        }

        public Builder withGraphEndpointOverride(String graphEndpointOverride) {
            this.graphEndpointOverride = graphEndpointOverride;
            return this;
        }

        public Builder withConnectionConnectTimeout(long connectionConnectTimeout) {
            this.connectionConnectTimeout = connectionConnectTimeout;
            return this;
        }

        public Builder withConnectionReadTimeout(long connectionReadTimeout) {
            this.connectionReadTimeout = connectionReadTimeout;
            return this;
        }

        @Override
        public MSGraphConnectionParams build() {
            return new MSGraphConnectionParams(this);
        }
    }


}
