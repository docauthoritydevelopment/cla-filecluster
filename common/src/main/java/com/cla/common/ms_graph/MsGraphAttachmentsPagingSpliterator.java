package com.cla.common.ms_graph;

import com.microsoft.graph.models.extensions.Attachment;
import com.microsoft.graph.requests.extensions.IAttachmentCollectionPage;
import com.microsoft.graph.requests.extensions.IAttachmentCollectionRequestBuilder;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Objects;
import java.util.Spliterator;
import java.util.function.Consumer;
import java.util.function.IntConsumer;

/**
 * A {@link Spliterator} of {@link Attachment Attachments} that iterates the attachments of an
 * {@link IAttachmentCollectionPage attachmentsPage} and retrieves any following pages as necessary, until all pages are
 * exhaust. Partitioning is done by returning a Spliterator of the remaining current page and advancing this Spliterator
 * to the next page. If there's no next page, no splitting is done.
 *
 * <p/> This Spliterator characteristics are {@link #ORDERED}, {@link #NONNULL} and {@link #IMMUTABLE}
 *
 * <p> This Spliterator iterates attachments in encounter order, hence the ORDERED characteristic.
 * <p> The attachments backing this Spliterator are expected to always be non-null hence the NONNULL characteristic.
 * <p> As the attachments backing this Spliterator are fetched from MS-Graph, they are not expected to change, hence the
 * IMMUTABLE characteristic.
 * <p> As this Spliterator reports IMMUTABLE, this Spliterator does not try to detect concurrent modification or
 * structural interference (and no ConcurrentModificationException will get thrown by this Spliterator).
 * <p> However, the current attachments page is backed by a List of Attachments that was
 * created by {@link Arrays#asList(Object[])}. If a reference to this list gets modified by a different thread then
 * it may throw such exception. Please don't do stuff like that.
 *
 * <p/>
 * Created By Itai Marko
 * <p/>
 */
public class MsGraphAttachmentsPagingSpliterator implements Spliterator<Attachment> {

    private static final int CHARACTERISTICS = ORDERED | NONNULL | IMMUTABLE;
    private static final PageLoader NO_NEXT_PAGE = new NoPageDummyLoader();

    private IAttachmentCollectionPage attachmentsPage;
    private Iterator<Attachment> curPageIterator;
    private PageLoader nextPageLoader;

    /**
     * Creates a new instance of MsGraphAttachmentsPagingSpliterator, early-bound to the given attachment page.
     *
     * @param attachmentsPage           The first page of attachments. Not null.
     * @param retrievedPageSizeConsumer A callback that will get applied to the retrieved page size, after it is retrieved
     */
    MsGraphAttachmentsPagingSpliterator(IAttachmentCollectionPage attachmentsPage, IntConsumer retrievedPageSizeConsumer) {
        this.attachmentsPage = Objects.requireNonNull(attachmentsPage);
        this.curPageIterator = attachmentsPage
                .getCurrentPage()
                .iterator();
        this.nextPageLoader = new NextPageLoader(retrievedPageSizeConsumer);
    }

    /**
     * a private constructor used to spawn a new Spliterator from this one in the same position in the current page and
     * without any following pages (even if this one has a following page). Used for splitting this Spliterator.
     *
     * @param curPageIterator an Iterator set to the desired position, to use to produce next attachment
     */
    private MsGraphAttachmentsPagingSpliterator(Iterator<Attachment> curPageIterator) {
        this.curPageIterator = Objects.requireNonNull(curPageIterator);
        this.nextPageLoader = NO_NEXT_PAGE;
    }

    /**
     * Performs the given action on the next {@link Attachment}, if exist either in the currently loaded page or in a
     * possible following page.
     *
     * <p/> Returns true if the next attachment was retrieved and the action was applied to it.
     * <p> Returns false if the last attachment of the last page was previously retrieved.
     *
     * <p> If the current page is exhaust and there is a next page, this method will retrieve the next page and try to
     * get the next attachment from it.
     *
     * <p> This Spliterator is {@link #ORDERED}, thus the action is performed on the next attachment in encounter order.
     *
     * <p> Exceptions thrown by the action are relayed to the caller.
     *
     * @param action The action to apply to the next attachment
     * @return false if no remaining elements existed upon entry to this method, else true.
     * @throws NullPointerException if the specified action is null
     */
    @Override
    public boolean tryAdvance(Consumer<? super Attachment> action) {
        Objects.requireNonNull(action);

        loadAttachmentPageIfNecessary();

        if (curPageIterator.hasNext()) { // Got more attachments in the current page
            Attachment attachment = curPageIterator.next();
            action.accept(attachment);
            return true;
        }

        return false;
    }

    /**
     * Performs the given action for each remaining element, sequentially in the current thread, until all elements
     * have been processed or the action throws an exception.
     *
     * <p/> This Spliterator is {@link #ORDERED}, thus the actions are performed in encounter order.
     *
     * <p> Exceptions thrown by the action are relayed to the caller.
     *
     * @param action The action to apply to each remaining attachment
     * @throws NullPointerException if the specified action is null
     */
    @Override
    public void forEachRemaining(Consumer<? super Attachment> action) {
        do {
            while (curPageIterator.hasNext()) {
                Attachment attachment = curPageIterator.next();
                action.accept(attachment);
            }
        } while (tryLoadNextPage());
    }

    /**
     * If there's a following page to be retrieved, returns a new Spliterator covering the attachments of the remainder
     * of this Spliterator's currently loaded page, with no following pages (even though this Spliterator has more pages).
     * This spliterator is advanced to the next page.
     *
     * <p/> If there's no following page, returns null (there's no splitting of the current page).
     *
     * <p/> This Spliterator is {@link #ORDERED}, thus the returned Spliterator, if not null, covers a strict prefix
     * of the attachments.
     *
     * @implNote
     * Note that since the total amount of pages/attachments is unknown, the splitting here is not balanced.
     * However, this may start slowing down performance if there are three or more pages which is unlikely.
     *
     * @return returns a Spliterator covering a strict prefix of the attachments, or null if no more pages
     */
    @Override
    public Spliterator<Attachment> trySplit() {

        IAttachmentCollectionPage nextPage = tryRetrieveNextPage();
        if (nextPage == null) { // No next page
            // This impl doesn't split the current page as it is usually small.
            // Splitting the current page means that the returned Spliterator will be backed by an array of the first half of
            // the current page and this spliterator needs to get advanced to the second half. The returned can be produced by
            // this.attachmentsPage.getCurrentPage().spliterator().trySplit().
            // This Spliterator will then need to temporarily (until fetching the next page) delegate operations to
            // attachmentsPage.getCurrentPage().spliterator() (returned before the split above)
            // This will add some complications of detecting whether there are more attachments and whether there are more pages.
            // Will impl if it turns out useful.
            return null;
        }

        // In order to preserve order we need to return a Spliterator backed by the current iterator
        // and make sure it does not overlap this Spliterator when it tries to retrieve its next page (this is done by
        // means of using the NO_NEXT_PAGE PageLoader inside this ctor).
        Spliterator<Attachment> prefixAttachmentSpliterator = new MsGraphAttachmentsPagingSpliterator(curPageIterator);
        // Advance this spliterator to the retrieved next page
        loadNextPage(nextPage);
        return prefixAttachmentSpliterator;
    }

    /**
     * Returns {@link Long#MAX_VALUE}, to signify unknown size.
     *
     * @return Long.MAX_VALUE
     */
    @Override
    public long estimateSize() {
        return  Long.MAX_VALUE;
    }

    /**
     * Returns {@code -1L}, to signify unknown size.
     *
     * @return -1L
     */
    @Override
    public long getExactSizeIfKnown() {
        return -1L;
    }

    /**
     * This Spliterator characteristics are {@link #ORDERED} as iteration is done in encounter order,
     * {@link #NONNULL} as no {@link Attachment} is expected to be null, and
     * {@link #IMMUTABLE} as the covered attachments and their pages are not expected to be modified.
     *
     * @return ORDERED, NONNULL and IMMUTABLE
     */
    @Override
    public int characteristics() {
        return CHARACTERISTICS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasCharacteristics(int characteristics) {
        return (CHARACTERISTICS & characteristics) == characteristics;
    }

    /**
     * This Spliterator's source is not {@link #SORTED}, thus this method throws {@link IllegalStateException}.
     *
     * @return never returns, always throws IllegalStateException.
     *
     * @throws IllegalStateException always
     */
    @Override
    public Comparator<? super Attachment> getComparator() {
        throw new IllegalStateException();
    }

    private void loadAttachmentPageIfNecessary() {
        nextPageLoader.loadAttachmentPageIfNecessary();
    }

    private boolean tryLoadNextPage() {
        return nextPageLoader.tryLoadNextPage();
    }

    private IAttachmentCollectionPage tryRetrieveNextPage() {
        return nextPageLoader.tryRetrieveNextPage();
    }

    private void loadNextPage(IAttachmentCollectionPage nextPage) {
        nextPageLoader.loadNextPage(nextPage);
    }

    /**
     * An interface for strategies of loading the next attachment page.
     * This allows to create an {@link MsGraphAttachmentsPagingSpliterator} with different page loading behaviour, by
     * means of composition rather than inheritance (which may make protected methods that should be private).
     */
    private interface PageLoader {
        void loadAttachmentPageIfNecessary();
        boolean tryLoadNextPage();
        IAttachmentCollectionPage tryRetrieveNextPage();
        void loadNextPage(IAttachmentCollectionPage nextPage);
    }

    /**
     * A PageLoader that's bound to the enclosing {@link MsGraphAttachmentsPagingSpliterator Spliterator's} current
     * page and iterator.
     */
    private class NextPageLoader implements PageLoader {

        private final IntConsumer retrievedPageSizeConsumer;

        public NextPageLoader(IntConsumer retrievedPageSizeConsumer) {
            this.retrievedPageSizeConsumer = retrievedPageSizeConsumer;
        }

        @Override
        public void loadAttachmentPageIfNecessary() {
            if (curPageIterator.hasNext()) { // Got more attachments in the current page, no need to load next page
                return;
            }
            tryLoadNextPage();
        }

        @Override
        public boolean tryLoadNextPage() {
            IAttachmentCollectionPage nextPage = tryRetrieveNextPage();
            if (nextPage == null) { // No more pages to retrieve
                return false;
            }

            loadNextPage(nextPage);
            return true;
        }

        @Override
        public IAttachmentCollectionPage tryRetrieveNextPage() {
            IAttachmentCollectionRequestBuilder nextAttachmentPageBuilder = attachmentsPage.getNextPage();
            if (nextAttachmentPageBuilder == null) { // No next page
                return null;
            }

            IAttachmentCollectionPage nextPage = nextAttachmentPageBuilder
                    .buildRequest()
                    .get();

            int retrievedPageSize = nextPage
                    .getCurrentPage()
                    .size();
            retrievedPageSizeConsumer.accept(retrievedPageSize);

            return nextPage;
        }

        @Override
        public void loadNextPage(IAttachmentCollectionPage nextPage) {
            attachmentsPage = nextPage;
            curPageIterator = attachmentsPage.getCurrentPage().iterator();
        }
    }


    /**
     * A {@link PageLoader} that always indicates there's no next page. Thread-safe
     */
    private static class NoPageDummyLoader implements PageLoader {

        @Override
        public void loadAttachmentPageIfNecessary() {}

        @Override
        public boolean tryLoadNextPage() { return false; }

        @Override
        public IAttachmentCollectionPage tryRetrieveNextPage() { return null; }

        @Override
        public void loadNextPage(IAttachmentCollectionPage nextPage) { }
    }
}
