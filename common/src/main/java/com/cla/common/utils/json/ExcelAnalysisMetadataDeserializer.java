package com.cla.common.utils.json;

import com.cla.common.domain.dto.file.ExcelAnalysisMetadata;

/**
 * Concrete class for ExcelAnalysisMetadata zipping de-serializer, ready to be used in Jackson annotations
 * @see ExcelAnalysisMetadata
 * @see com.fasterxml.jackson.databind.annotation.JsonDeserialize
 *
 * Created by vladi on 3/8/2017.
 */
public class ExcelAnalysisMetadataDeserializer extends ZippingObjectDeserializer<ExcelAnalysisMetadata> {
}
