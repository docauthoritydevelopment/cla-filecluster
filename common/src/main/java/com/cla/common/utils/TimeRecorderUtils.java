package com.cla.common.utils;

import java.util.function.Consumer;
import java.util.function.Supplier;

public class TimeRecorderUtils {

    public static <T> RecordTimeResult<T> recordTime(Supplier<T> supplier) {
        long start = System.currentTimeMillis();
        T t = supplier.get();
        return new RecordTimeResult<>(System.currentTimeMillis() - start, t);
    }

    public static <T> RecordTimeResult<Void> recordTime(T param, Consumer<T> consumer) {
        long start = System.currentTimeMillis();
        consumer.accept(param);
        return new RecordTimeResult(System.currentTimeMillis() - start, Void.TYPE);
    }


    public static class RecordTimeResult<T> {

        private final T result;
        private final long duration;

        public RecordTimeResult(long duration, T result) {
            this.duration = duration;
            this.result = result;
        }

        public T getResult() {
            return result;
        }

        public long getDuration() {
            return duration;
        }
    }
}
