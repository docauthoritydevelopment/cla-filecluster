package com.cla.common.utils;

import com.cla.connector.utils.FileNamingUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.FileTime;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;
import java.util.stream.Stream;

/**
 * Created by: yael
 * Created on: 5/23/2018
 */
public class LogExportUtils {

    private static final Logger logger = LoggerFactory.getLogger(LogExportUtils.class);

    public static List<Path> getLogFilesToExport(String[] exportFiles, int exportFilesTopIndex, int exportFilesTopDays) {
        List<Path> filesNames = new ArrayList<>();
        List<Path> files = new ArrayList<>();
        try {

            // for files with running index - so we can limit what we return
            String indexIndicator = "%";
            // for files with dates in their names - so we can return all of them
            String dateIndicator = "*";

            // get all possible file names by index linit
            for (String fileName : exportFiles) {
                if (fileName.contains(indexIndicator)) {
                    addIfExists(filesNames, fileName.replace(indexIndicator, ""), exportFilesTopDays);
                    for (int i = 1; i < exportFilesTopIndex; ++i) {
                        addIfExists(filesNames, fileName.replace(indexIndicator, "." + i), exportFilesTopDays);
                    }
                } else if (fileName.contains(dateIndicator)) {
                    String path = FileNamingUtils.getFilenamePath(fileName);
                    String base = FileNamingUtils.getFilenameBaseName(fileName).replace(dateIndicator, ".*");
                    List<Path> existing = getFileListByRegex(path, base, exportFilesTopDays);
                    filesNames.addAll(existing);
                } else {
                    addIfExists(filesNames, fileName, exportFilesTopDays);
                }
            }

            // filter only existing files
            filesNames.stream().filter(f -> f.toFile().exists()).forEach(files::add);

        } catch (Exception ex) {
            logger.info("Error writing log files to output stream.", ex);
            throw new RuntimeException("IOError writing log files to output stream");
        }
        return files;
    }

    public static List<Path> getFileListByRegex(String path, String fileSpecRegex, int exportFilesTopDays) {
        final Pattern logFilePattern = Pattern.compile(fileSpecRegex);
        List<Path> files = new ArrayList<>();
        try (Stream<Path> logFileList = Files.list(Paths.get(path))) {
            logger.debug("zip each log file in <{}>", fileSpecRegex);
            logFileList.filter(p ->
                    logFilePattern.matcher(p.getFileName().toString()).matches()
                    && chkDateInTopDays(getFileLastModified(p), exportFilesTopDays))
                    .forEach(files::add);
        } catch (Exception e) {
            logger.info("path problem (doesnt exists ?) {} err {}", path, e.getMessage());
        }
        return files;

    }

    public static void addIfExists(List<Path> filesNames, String fileName, int exportFilesTopDays) {
        try {
            Path file = Paths.get(fileName);
            if (chkDateInTopDays(getFileLastModified(file), exportFilesTopDays)) {
                filesNames.add(file);
            }
        } catch (Exception e) {
            logger.info("path problem (doesnt exists ?) {} err {}", fileName, e.getMessage());
        }
    }

    public static boolean chkDateInTopDays(long dateToCheck, int exportFilesTopDays) {
        if (exportFilesTopDays <= 0) return true;
        long timePassed = System.currentTimeMillis() - dateToCheck;
        long days = TimeUnit.MILLISECONDS.toDays(timePassed);
        return days <= exportFilesTopDays;
    }

    public static long getFileLastModified(Path path) {
        try {
            FileTime timeLastModif = java.nio.file.Files.getLastModifiedTime(path);
            return timeLastModif.toMillis();
        } catch (Exception e) {
            return 0L;
        }
    }
}
