package com.cla.common.utils;

import java.util.Objects;

/**
 * Created by: yael
 * Created on: 6/6/2018
 */
public class Triple<L,M,R> {
    private L left;
    private M middle;
    private R right;

    public static <L,M,R> Triple<L,M,R> of(L left, M middle, R right){
        return new Triple<>(left, middle, right);
    }

    public Triple(L left, M middle, R right) {
        this.left = left;
        this.middle = middle;
        this.right = right;
    }

    public L getLeft() {
        return left;
    }

    public M getMiddle() {
        return middle;
    }

    public R getRight() {
        return right;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Triple<?, ?, ?> triple = (Triple<?, ?, ?>) o;
        return Objects.equals(left, triple.left) &&
                Objects.equals(middle, triple.middle) &&
                Objects.equals(right, triple.right);
    }

    @Override
    public int hashCode() {
        return Objects.hash(left, middle, right);
    }
}
