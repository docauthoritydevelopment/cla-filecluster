package com.cla.common.utils.thread;

/**
 *
 * Created by itay on 19/02/2017.
 */
public class ClaThreadPropertiesSet {

    public static final String KEY_PATH = "path";
    public static final String KEY_TRIMMED = "trimmed";

    private static final ThreadLocal<ClaPropertySet> mySet = ThreadLocal.withInitial(ClaPropertySet::create);

    public static ClaPropertySet getThreadLocalPropertiesSet() {
        return mySet.get();
    }
}



