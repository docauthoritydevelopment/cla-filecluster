package com.cla.common.utils.tokens;

import java.util.List;
import java.util.Map;

/**
 *
 * Created by uri on 05/02/2017.
 */
public interface DocAuthorityTokenizer {

    String WHITESPACE_CHARS = ""       /* dummy empty string for homogeneity */
            + "\u0009" // CHARACTER TABULATION
            + "\n" // LINE FEED (LF)
            + "\u000B" // LINE TABULATION
            + "\u000C" // FORM FEED (FF)
            + "\r" // CARRIAGE RETURN (CR)
            + "\u0020" // SPACE
            + "\u0085" // NEXT LINE (NEL)
            + "\u00A0" // NO-BREAK SPACE
            + "\u1680" // OGHAM SPACE MARK
            + "\u180E" // MONGOLIAN VOWEL SEPARATOR
            + "\u2000" // EN QUAD
            + "\u2001" // EM QUAD
            + "\u2002" // EN SPACE
            + "\u2003" // EM SPACE
            + "\u2004" // THREE-PER-EM SPACE
            + "\u2005" // FOUR-PER-EM SPACE
            + "\u2006" // SIX-PER-EM SPACE
            + "\u2007" // FIGURE SPACE
            + "\u2008" // PUNCTUATION SPACE
            + "\u2009" // THIN SPACE
            + "\u200A" // HAIR SPACE
            + "\u2028" // LINE SEPARATOR
            + "\u2029" // PARAGRAPH SEPARATOR
            + "\u202F" // NARROW NO-BREAK SPACE
            + "\u205F" // MEDIUM MATHEMATICAL SPACE
            + "\u3000" // IDEOGRAPHIC SPACE
            ;

    /* A \s that actually works for Java’s native character set: Unicode */
    String whitespaceRegExp = "[" + WHITESPACE_CHARS + "]+";

    long MD5Long(String item);

    static String resetWhiteSpaces(final String s) {
        return s.replaceAll(whitespaceRegExp, " ");
    }

    String getHashReuseTokenizer(final String text, final HashedTokensConsumer hashedTokensConsumer, final String locationDesc,
                                 final TokenizerObject tokenizerObject);

    TokenizerObject getTokenizerObject();

    List<Long> getHashlist(String t, int titleNgramSize);

    Map<Long, String> getDebugHashMapper();

    void setDebugHashMapper(final Map<Long, String> debugHashMapper);
}
