package com.cla.common.utils.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.zip.GZIPOutputStream;

/**
 * Abstract class to be extended by concrete object serializers
 * Serializes object to binary stream, ZIPs it and converts to Base64
 *
 * @see ZippingObjectDeserializer
 *
 * Created by vladi on 3/8/2017.
 */
public abstract class ZippingObjectSerializer<T extends Serializable> extends JsonSerializer<T> {
    @Override
    public void serialize(T value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        try (
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                GZIPOutputStream zipOutputStream = new GZIPOutputStream(baos, true);
                ObjectOutputStream oos = new ObjectOutputStream(zipOutputStream)
        ){
            oos.writeObject(value);
            oos.flush();
            baos.flush();
            gen.writeBinary(baos.toByteArray());
        }
    }
}
