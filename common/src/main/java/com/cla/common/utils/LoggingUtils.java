package com.cla.common.utils;

import org.slf4j.Logger;
import org.slf4j.event.Level;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;


public class LoggingUtils {

    public static void log(Logger logger, Level level, String message, Object ... args){
        switch(level){
            case TRACE:
                logger.trace(message, args);
                break;
            case DEBUG:
                logger.debug(message, args);
                break;
            case INFO:
                logger.info(message, args);
                break;
            case WARN:
                logger.warn(message, args);
                break;
            case ERROR:
                logger.error(message, args);
                break;
            default:
                logger.info(message, args);
        }
    }

    public static String stackTraceToString(Exception e){
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        e.printStackTrace(pw);
        return sw.toString();
    }

    public static String stackTraceToString(StackTraceElement[] elements){
        return Arrays.stream(elements)
                .map(StackTraceElement::toString)
                .collect(Collectors.joining("\n "));
    }

    public static String collectionToString(Collection<?> objects){
        return objects.stream().map(Object::toString).collect(Collectors.joining(","));
    }
}
