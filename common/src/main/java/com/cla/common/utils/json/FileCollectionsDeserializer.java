package com.cla.common.utils.json;

import com.cla.common.domain.dto.file.FileCollections;

/**
 * Concrete class for FileCollections zipping de-serializer, ready to be used in Jackson annotations
 * @see FileCollections
 * @see com.fasterxml.jackson.databind.annotation.JsonDeserialize
 *
 * Created by vladi on 3/8/2017.
 */
public class FileCollectionsDeserializer extends ZippingObjectDeserializer<FileCollections> {
}
