package com.cla.common.utils;

import com.cla.common.domain.dto.jobmanager.JobInitiationDetails;
import com.cla.common.domain.dto.kendo.FilterDescriptor;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 *
 * Created by uri on 20-Feb-17.
 */
public class JsonDtoConversionUtils {

    private final static ObjectMapper mapper;

    private static Logger logger = LoggerFactory.getLogger(JsonDtoConversionUtils.class);

    static {
        mapper = new ObjectMapper();
        mapper.configure(JsonParser.Feature.ALLOW_SINGLE_QUOTES, true);
    }

    public static String convertFilterDescriptorToJson(FilterDescriptor filterDescriptor) {
        try {
            return mapper.writeValueAsString(filterDescriptor);
        }
        catch (IOException e) {
            logger.error("Failed to convert filterDescriptor to FILTER_JSON (IO Exception): " + e.getMessage(), e);
            throw new RuntimeException("Failed to convert filterDescriptor to FILTER_JSON (IO Exception): " + e.getMessage());
        }
    }


    public static String convertObjToJson(Object obj) {
        try {
            return mapper.writeValueAsString(obj);
        }
        catch (IOException e) {
            logger.error("Failed to convert obj to json (IO Exception): " + e.getMessage(), e);
            throw new RuntimeException("Failed to convert obj to JSON (IO Exception): " + e.getMessage());
        }
    }

    public static <T> T convertJsonStringToObj(String json, Class<T> clazz) {
        try {
            return mapper.readValue(json, clazz);
        } catch (IOException e) {
            throw new RuntimeException("Failed to deserialize json object to class: " + clazz + ", from json string: " + json,  e);
        }
    }

    public static String jobInitiationDetailsToString(JobInitiationDetails initiationDetails) {
        try {
            return mapper.writeValueAsString(initiationDetails);
        } catch (JsonProcessingException e) {
            throw new RuntimeException("Failed to convert JobInitiationDetails to String", e);
        }
    }

    public static JobInitiationDetails jobInitiationDetailsFromString(String initiationDetails) {
        try {
            return mapper.readValue(initiationDetails, JobInitiationDetails.class);
        } catch (IOException e) {
            throw new RuntimeException("Failed to deserialize JobInitiationDetails", e);
        }
    }
}
