package com.cla.common.utils.signature;

import com.google.common.base.Strings;

/**
 * Bean used for returning read content and its signature
 * Created by vladi on 2/21/2017.
 */
public class ContentAndSignature {

    private byte[] content;
    private String signature;
    private int actualSize;
    private boolean contentExistsInDb = false;
    private boolean contentReadFully = false;

    public static ContentAndSignature create(){
        return new ContentAndSignature();
    }

    public byte[] getContent() {
        return content;
    }

    public int contentLength(){
        return content != null ? content.length : 0;
    }

    public boolean hasContent(){
        return content != null && content.length > 0;
    }

    public ContentAndSignature setContent(byte[] content) {
        this.content = content;
        return this;
    }

    public String getSignature() {
        return signature;
    }

    public boolean hasSignature(){
        return !Strings.isNullOrEmpty(signature);
    }

    public ContentAndSignature setSignature(String signature) {
        this.signature = signature;
        return this;
    }

    public int getActualSize() {
        return actualSize;
    }

    public ContentAndSignature setActualSize(int actualSize) {
        this.actualSize = actualSize;
        return this;
    }

    public boolean contentExistsInDb() {
        return contentExistsInDb;
    }

    public ContentAndSignature setContentExistsInDb(boolean contentExistsInDb) {
        this.contentExistsInDb = contentExistsInDb;
        return this;
    }

    public boolean contentReadFully() {
        return contentReadFully;
    }

    public ContentAndSignature setContentReadFully(boolean contentReadFully) {
        this.contentReadFully = contentReadFully;
        return this;
    }
}
