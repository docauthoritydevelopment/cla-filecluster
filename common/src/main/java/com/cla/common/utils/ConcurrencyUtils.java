package com.cla.common.utils;

import java.util.Collection;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;

public class ConcurrencyUtils {

    public static <T> void executeTasks(Collection<T> items, Consumer<T> consumer, ExecutorService threadPool, long time, TimeUnit timeUnit) throws InterruptedException {
        AtomicReference<Throwable> error = new AtomicReference<>();

        CountDownLatch countDownLatch = executeConcurrentTasks(items, consumer, threadPool, error);

        if (!countDownLatch.await(time, timeUnit)) {
            throw new RuntimeException(String.format("Timeout passed (%d %s) for executing tasks", time, timeUnit.name()));
        }

        if (error.get() != null) {
            throw new RuntimeException("Failed to completed all tasks", error.get());
        }
    }

    public static <T> void executeTasks(Collection<T> items, Consumer<T> consumer, ExecutorService threadPool) throws InterruptedException {
        AtomicReference<Throwable> error = new AtomicReference<>();
        CountDownLatch countDownLatch = executeConcurrentTasks(items, consumer, threadPool, error);

        countDownLatch.await();

        if (error.get() != null) {
            throw new RuntimeException("Failed to completed all tasks", error.get());
        }
    }

    private static <T> CountDownLatch executeConcurrentTasks(Collection<T> items, Consumer<T> consumer, ExecutorService threadPool, AtomicReference<Throwable> error) {
        CountDownLatch countDownLatch = new CountDownLatch(items.size());
        items.stream().
                forEach(t -> threadPool.execute(() -> {
                    try {
                        consumer.accept(t);
                    } catch (Throwable th) {
                        error.set(th);
                    } finally {
                        countDownLatch.countDown();
                    }
                }));
        return countDownLatch;
    }
}
