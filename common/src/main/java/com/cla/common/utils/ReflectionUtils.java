package com.cla.common.utils;


import com.google.common.collect.Sets;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.HashSet;

public class ReflectionUtils {

    private static String MULTI_LINE_PATTERN = "(=.*\\r\\n)|(=.*)";

    public static String[] fieldNames(Object obj, String... excludes) {
        String s = ReflectionToStringBuilder.reflectionToString(obj, ToStringStyle.MULTI_LINE_STYLE);
        return parseFields(s, excludes);
    }

    private static String[] parseFields(String fieldsDescription, String... fieldsToExclude) {
        String[] fieldAndValues = fieldsDescription.substring(fieldsDescription.indexOf("[") + 1, fieldsDescription.indexOf("]") - 1).trim().split(MULTI_LINE_PATTERN);
        String[] result = new String[fieldAndValues.length - fieldsToExclude.length];
        HashSet<String> fieldsToExcludeSet = Sets.newHashSet(fieldsToExclude);
        int index = 0;
        for (int i = 0; i < fieldAndValues.length; i++) {
            String trimmedValue = fieldAndValues[i].trim();
            if (!fieldsToExcludeSet.contains(trimmedValue)) {
                result[index] = trimmedValue;
                index++;
            }
        }
        return result;
    }
}
