package com.cla.common.utils;

import com.cla.common.domain.dto.bizlist.SimpleBizListItemDto;
import com.cla.common.domain.dto.bizlist.SimpleBizListItemInterface;
import com.cla.common.domain.dto.department.DepartmentDto;
import com.cla.common.domain.dto.doctype.DocTypeDto;
import com.cla.common.domain.dto.filetag.FileTagInterface;
import com.cla.common.domain.dto.security.FunctionalRoleDto;
import com.cla.common.domain.dto.security.FunctionalRoleInterface;
import com.cla.common.domain.dto.workflow.FileAction;
import com.cla.common.domain.dto.workflow.FileState;
import com.cla.common.domain.dto.workflow.WorkflowFileData;
import com.cla.common.tag.priority.TagPriority;
import com.google.common.base.Strings;
import org.apache.commons.lang3.StringUtils;

import javax.validation.constraints.NotNull;

/**
 * Created by: yael
 * Created on: 5/6/2019
 */
public class AssociationIdUtils {

    public static final String CATEGORY_SEPARATOR = ".";

    public static final String GLOBAL_SCOPE_IDENTIFIER = "g";

    public static String createTag2Identifier(FileTagInterface fileTag) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("type.").append(fileTag.getTypeId())
                .append(" ")
                .append("tag.").append(fileTag.getId());
        addCommonTagIDFeatures(stringBuilder, fileTag);
        return stringBuilder.toString();
    }

    private static void addCommonTagIDFeatures(StringBuilder stringBuilder, FileTagInterface fileTag) {
        if (fileTag.getDocTypeContextId() != null) {
            stringBuilder.append(CATEGORY_SEPARATOR).append(DocTypeDto.ID_PREFIX);
            stringBuilder.append(fileTag.getDocTypeContextId());
        }
        if (fileTag.getEntityIdContext() != null) {
            stringBuilder.append(CATEGORY_SEPARATOR).append(fileTag.getEntityIdContext());
        }
        stringBuilder.append(CATEGORY_SEPARATOR).append(fileTag.getAction().toString());
    }

    public static String createSimpleBizListItemIdentfier(SimpleBizListItemInterface simpleBizListItem) {
        return createSimpleBizListItemIdentfier(simpleBizListItem, null);
    }

    public static String createTagIdentifier(FileTagInterface fileTag) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(fileTag.getTypeId());
        stringBuilder.append(CATEGORY_SEPARATOR).append(fileTag.getId());
        addCommonTagIDFeatures(stringBuilder, fileTag);
        return stringBuilder.toString();
    }

    public static String createPriorityScopedTagIdentifier(FileTagInterface fileTag, Long scopeId, TagPriority tagPriority) {
        return createBasicScopedTagIdentifier(fileTag, scopeId)
                .append(CATEGORY_SEPARATOR).append(tagPriority.getPriority())
                .toString();

    }

    @NotNull
    private static StringBuilder createBasicScopedTagIdentifier(FileTagInterface fileTag, Long scopeId) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(scopeId == null ? GLOBAL_SCOPE_IDENTIFIER : scopeId);
        stringBuilder.append(CATEGORY_SEPARATOR).append(fileTag.getTypeId());
        stringBuilder.append(CATEGORY_SEPARATOR).append(fileTag.getId());
        addCommonTagIDFeatures(stringBuilder, fileTag);
        return stringBuilder;
    }

    /**
     * @param simpleBizListItem
     * @return bli.type.bizListId.bizListItemId
     */
    @SuppressWarnings("StringBufferReplaceableByString")
    public static String createSimpleBizListItemIdentfier(SimpleBizListItemInterface simpleBizListItem, TagPriority tagPriority) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(SimpleBizListItemDto.ID_PREFIX)
                .append(simpleBizListItem.getListType().ordinal())
                .append(CATEGORY_SEPARATOR).append(simpleBizListItem.getBizListId())
                .append(CATEGORY_SEPARATOR).append(simpleBizListItem.getSid());
        if (tagPriority != null) {
            stringBuilder.append(CATEGORY_SEPARATOR).append(tagPriority.getPriority());
        }
        return stringBuilder.toString();
    }

    public static String extractBizListItemIdentfier(String identifier) {
        if (Strings.isNullOrEmpty(identifier)) {
            return null;
        }
        String[] vars = StringUtils.split(identifier, CATEGORY_SEPARATOR);
        return vars.length == 5 ? vars[3] : vars[vars.length-1];
    }

    public static String createFunctionalRoleIdentfier(FunctionalRoleInterface functionalRole) {
        return createFunctionalRoleIdentfier(functionalRole, null);
    }

    @SuppressWarnings("StringBufferReplaceableByString")
    public static String createFunctionalRoleIdentfier(FunctionalRoleInterface functionalRole, TagPriority tagPriority) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(createFunctionalRoleIdentfier(functionalRole.getId()));
        if (tagPriority != null) {
            stringBuilder.append(CATEGORY_SEPARATOR).append(tagPriority.getPriority());
        }
        return stringBuilder.toString();
    }

    public static String createFunctionalRoleIdentfier(Long functionalRoleId) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(FunctionalRoleDto.ID_PREFIX).append(functionalRoleId);
        return stringBuilder.toString();
    }

    @SuppressWarnings("StringBufferReplaceableByString")
    public static String createDepartmentIdentifier(Long departmentId, TagPriority tagPriority) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(createDepartmentIdentifier(departmentId));
        if (tagPriority != null) {
            stringBuilder.append(CATEGORY_SEPARATOR).append(tagPriority.getPriority());
        }
        return stringBuilder.toString();
    }

    public static String createDepartmentIdentifier(Long departmentId) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(DepartmentDto.ID_PREFIX).append(departmentId);
        return stringBuilder.toString();
    }

    public static long extractFunctionalRoleIdFromSolrIdentifier(String solrIdentifier) {
        String[] split = StringUtils.split(solrIdentifier, CATEGORY_SEPARATOR);
        return Long.valueOf(split[1]);
    }

    public static long extractDepartmentIdFromSolrIdentifier(String solrIdentifier) {
        String[] split = StringUtils.split(solrIdentifier, CATEGORY_SEPARATOR);
        return Long.valueOf(split[1]);
    }

    public static long extractTagIdFromSolrIdentifier(String solrIdentifier) {
        String id = extractTagStringFromSolrIdentifier(solrIdentifier);
        return Long.valueOf(id);
    }

    public static String extractTagStringFromSolrIdentifier(String solrIdentifier) {
        String[] split = StringUtils.split(solrIdentifier, CATEGORY_SEPARATOR);
        return split[1];
    }

    public static String extractTagIdFromScopedSolrIdentifier(String solrIdentifier) {
        String[] vars = StringUtils.split(solrIdentifier, CATEGORY_SEPARATOR);
        return vars[2];
    }

    private static String extractLastElementId(String name) {
        int li = name.lastIndexOf(CATEGORY_SEPARATOR);
        return name.substring(li + 1);
    }

    public static String createScopedDocTypeIdentifier(String docTypeIdentifier, Long scopeId) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(DocTypeDto.ID_PREFIX);
        stringBuilder.append(scopeId == null ? GLOBAL_SCOPE_IDENTIFIER : scopeId);
        stringBuilder.append(CATEGORY_SEPARATOR).append(docTypeIdHierarchy(docTypeIdentifier));
        return stringBuilder.toString();
    }

    public static String createPriorityScopedDocTypeIdentifier(String docTypeIdentifier, Long scopeId, TagPriority tagPriority) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(DocTypeDto.ID_PREFIX);
        stringBuilder.append(scopeId == null ? GLOBAL_SCOPE_IDENTIFIER : scopeId);
        stringBuilder.append(CATEGORY_SEPARATOR).append(docTypeIdHierarchy(docTypeIdentifier));
        stringBuilder.append(CATEGORY_SEPARATOR).append(CATEGORY_SEPARATOR).append(tagPriority.getPriority());
        return stringBuilder.toString();
    }

    public static String docTypeIdHierarchy(String docTypeIdentifier) {
        return docTypeIdentifier.substring(DocTypeDto.ID_PREFIX.length());
    }

    public static String createViaDocTypePriorityScopedTagIdentifier(FileTagInterface fileTag, Long scopeId, TagPriority tagPriority) {
        return createBasicScopedTagIdentifier(fileTag, scopeId)
                .append(CATEGORY_SEPARATOR).append(TagPriority.VIA_DOC_TYPE_INDICATION)
                .append(CATEGORY_SEPARATOR).append(tagPriority.getPriority())
                .toString();
    }

    public static String createScopedTagIdentifier(FileTagInterface fileTag, Long scopeId) {
        StringBuilder stringBuilder = createBasicScopedTagIdentifier(fileTag, scopeId);
        return stringBuilder.toString();
    }

    public static String createWorkFlowIdentifier(long workflowId, FileAction fileAction, FileState fileState) {
        return new StringBuilder()
                .append(workflowId)
                .append(CATEGORY_SEPARATOR)
                .append(fileAction)
                .append(CATEGORY_SEPARATOR)
                .append(fileState)
                .toString();

    }

    public static WorkflowFileData extractWorkflowFileDataDto(String wfData) {
        String[] parts = StringUtils.split(wfData, CATEGORY_SEPARATOR);
        WorkflowFileData workflowFileData = new WorkflowFileData();
        workflowFileData.setFileAction(parts[2].equalsIgnoreCase("null") ? null : FileAction.valueOf(parts[2]));
        workflowFileData.setFileState(parts[3].equalsIgnoreCase("null") ? null : FileState.valueOf(parts[3]));
        return workflowFileData;
    }

    public static Long extractWorkflowId(String wfData) {
        return Long.valueOf(wfData.substring(0, wfData.indexOf(".")));
    }

    public static long extractDocTypeIdFromSolrIdentifier(String name) {
        if (name.contains("..")) {
            String[] vars = name.substring(0, name.indexOf("..")).split("\\.");
            return Long.valueOf(vars[vars.length - 1]);
        }
        String[] vars = name.split("\\.");
        return Long.valueOf(vars[vars.length - 1]);
    }
}
