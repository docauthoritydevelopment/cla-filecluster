package com.cla.common.utils.json;

import com.cla.common.domain.dto.file.FileCollections;

/**
 * Concrete class for FileCollections zipping serializer, ready to be used in Jackson annotations
 * @see FileCollections
 * @see com.fasterxml.jackson.databind.annotation.JsonSerialize
 *
 * Created by vladi on 3/8/2017.
 */
public class FileCollectionsSerializer extends ZippingObjectSerializer<FileCollections>{
}
