package com.cla.common.utils;

import com.cla.common.domain.dto.filetree.RsaKey;
import com.google.common.base.Strings;
import org.apache.commons.codec.binary.Base64;
import org.bouncycastle.jce.X509Principal;
import org.bouncycastle.x509.X509V3CertificateGenerator;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.*;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * EncryptionUtils allows creation of RSA keys and encryption & decryption of strings using them
 * Created by yael on 11/9/2017.
 */
public class EncryptionUtils {

    public static String ALGO = "RSA";
    public static int KEY_SIZE = 2048;
    public static String ENCODING = "UTF-8";

    public static String encryptUsingPublicKeyStr(String keyStr, String str)
            throws InvalidKeySpecException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, UnsupportedEncodingException, BadPaddingException, IllegalBlockSizeException {
        PublicKey key = getPublicKeyFromString(keyStr);
        return Base64.encodeBase64String(encryptUsingPublicKey(key, str.getBytes(ENCODING)));
    }

    static byte[] encryptUsingPublicKey(PublicKey key, byte[] bytes)
            throws InvalidKeySpecException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, UnsupportedEncodingException, BadPaddingException, IllegalBlockSizeException {
        Cipher cipher = Cipher.getInstance(ALGO);
        cipher.init(Cipher.ENCRYPT_MODE, key);
        return cipher.doFinal(bytes);
    }

    public static String decryptUsingPrivateKeyStr(String keyStr, String str)
            throws InvalidKeySpecException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, UnsupportedEncodingException, BadPaddingException, IllegalBlockSizeException {
        PrivateKey key = getPrivateKeyFromString(keyStr);
        return decryptStringUsingPrivateKey(key, str);
    }

    public static String decryptStringUsingPrivateKey(PrivateKey key, String str)
            throws InvalidKeySpecException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, UnsupportedEncodingException, BadPaddingException, IllegalBlockSizeException {
        byte[] toDecryptedBytes = Base64.decodeBase64(str);
        byte[] decryptedBytes = decryptUsingPrivateKey(key, toDecryptedBytes);
        return new String(decryptedBytes, ENCODING);
    }

    static byte[] decryptUsingPrivateKey(PrivateKey key, byte[] bytes)
            throws InvalidKeySpecException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, UnsupportedEncodingException, BadPaddingException, IllegalBlockSizeException {
        Cipher cipher = Cipher.getInstance(ALGO);
        cipher.init(Cipher.DECRYPT_MODE, key);
        return cipher.doFinal(bytes);
    }

    static PublicKey getPublicKeyFromString(String key) throws NoSuchAlgorithmException, InvalidKeySpecException {
        byte[] publicBytes = Base64.decodeBase64(key);
        X509EncodedKeySpec spec = new X509EncodedKeySpec(publicBytes);
        KeyFactory kf = KeyFactory.getInstance(ALGO);
        return kf.generatePublic(spec);
    }

    public static PrivateKey getPrivateKeyFromString(String key) throws NoSuchAlgorithmException, InvalidKeySpecException {
        byte[] privateBytes = Base64.decodeBase64(key);
        PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(privateBytes);
        KeyFactory kf = KeyFactory.getInstance(ALGO);
        return kf.generatePrivate(spec);
    }

    static String convertPublicKeyToString(PublicKey key) throws Exception {
        return Base64.encodeBase64String(key.getEncoded());
    }

    static String convertPrivateKeyToString(PrivateKey key) throws Exception {
        return Base64.encodeBase64String(key.getEncoded());
    }

    static KeyPair generateNewKeys() throws Exception {
        KeyPairGenerator keyPairGen = KeyPairGenerator.getInstance(ALGO);
        keyPairGen.initialize(KEY_SIZE);
        return keyPairGen.genKeyPair();
    }

    public static RsaKey generateNewKeysComplete() throws Exception {
        KeyPair keys = generateNewKeys();
        String certificate = generateCertificate(keys);
        return new RsaKey(convertPublicKeyToString(keys.getPublic()), convertPrivateKeyToString(keys.getPrivate()), certificate);
    }

    public static X509Certificate getCertificateFromString(String certStr) throws Exception {
        ByteArrayInputStream pemByteIn = new ByteArrayInputStream( Base64.decodeBase64(certStr) );
        return (X509Certificate) CertificateFactory.getInstance("X.509").generateCertificate(pemByteIn);
    }

    static String generateCertificate(KeyPair keyPair) throws Exception {
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
        Date validityBeginDate = new Date(System.currentTimeMillis() - TimeUnit.DAYS.toMillis(1)); // yesterday
        long untilDateMillis = System.currentTimeMillis() + TimeUnit.DAYS.toMillis(365) * 10;
        Date validityEndDate = new Date(untilDateMillis); // 10 yrs
        X509V3CertificateGenerator cert = new X509V3CertificateGenerator();
        cert.setSerialNumber(BigInteger.valueOf(1));   //or generate a random number
        cert.setSubjectDN(new X509Principal("CN=localhost"));  //see examples to add O,OU etc
        cert.setIssuerDN(new X509Principal("CN=localhost")); //same since it is self-signed
        cert.setPublicKey(keyPair.getPublic());
        cert.setSignatureAlgorithm("SHA1WithRSAEncryption");
        cert.setNotBefore(validityBeginDate);
        cert.setNotAfter(validityEndDate);
        PrivateKey signingKey = keyPair.getPrivate();
        X509Certificate certificate = cert.generate(signingKey, "BC");
        return Base64.encodeBase64String(certificate.getEncoded());
    }

    public static String encryptSymmetric(final String valueEnc, String encryptionKey, String encryptionAlgorithm) throws Exception {
        if(Strings.isNullOrEmpty(valueEnc)) {
            return valueEnc;
        }
        String encryptedVal = null;
        final Key key = generateKeyFromString(encryptionKey);
        final Cipher c = Cipher.getInstance(encryptionAlgorithm);
        c.init(Cipher.ENCRYPT_MODE, key);
        final byte[] encValue = c.doFinal(valueEnc.getBytes());
        encryptedVal = Base64.encodeBase64String(encValue);
        return encryptedVal;
    }

    public static String decryptSymmetric(String encryptedVal, String encryptionKey, String encryptionAlgorithm) throws Exception {
        if(Strings.isNullOrEmpty(encryptedVal)) {
            return encryptedVal;
        }
        String decryptedValue = null;
        final Cipher c = Cipher.getInstance(encryptionAlgorithm);
        final Key key = generateKeyFromString(encryptionKey);
        c.init(Cipher.DECRYPT_MODE, key);
        final byte[] decorVal = Base64.decodeBase64(encryptedVal);
        final byte[] decValue = c.doFinal(decorVal);
        decryptedValue = new String(decValue);
        return decryptedValue;
    }

    private static Key generateKeyFromString(final String secKey) {

        final byte[] keyVal = Base64.decodeBase64(secKey);
        final byte[] keyPadded = new byte[16];
        System.arraycopy(keyVal,0,keyPadded,0,Math.min(keyVal.length,16));
//        final byte[] keyVal = new BASE64Decoder().decodeBuffer(secKey);
        return new SecretKeySpec(keyPadded, "AES");
    }
}
