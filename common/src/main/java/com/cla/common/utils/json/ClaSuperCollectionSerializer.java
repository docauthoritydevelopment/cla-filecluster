package com.cla.common.utils.json;

import com.cla.common.domain.dto.histogram.ClaSuperCollection;

/**
 * Serializer for PV histograms (ingestion results)
 * @see ClaSuperCollection
 *
 * Created by vladi on 3/7/2017.
 */
public class ClaSuperCollectionSerializer extends ZippingObjectSerializer<ClaSuperCollection> {
}
