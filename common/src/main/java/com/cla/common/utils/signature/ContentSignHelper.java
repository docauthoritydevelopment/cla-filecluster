package com.cla.common.utils.signature;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.Arrays;

/**
 *
 * Created by vladi on 2/21/2017.
 */
public class ContentSignHelper {

    private static final Logger logger = LoggerFactory.getLogger(ContentSignHelper.class);

    private int signatureMaxSize;

    private final String EMPTY_SIG;

    public ContentSignHelper(int signatureMaxSize) {
        this.signatureMaxSize = signatureMaxSize;
        EMPTY_SIG = sign(new byte[0]);
    }

    /**
     * Read first chunk of content from the stream (up to signatureMaxSize) and sign it
     * @param is                input stream
     * @param fileSize          total file size
     * @param filename          file name (for logging)
     * @return                  content and signature bean
     * @throws IOException      in case of IO failure
     */
    public ContentAndSignature readAndSign(InputStream is, int fileSize, String filename) throws IOException {

        int maxBytesToRead = signatureMaxSize;
        if (fileSize >= 0 && fileSize < signatureMaxSize) {
            maxBytesToRead = fileSize;
        }

        byte[] actualContent = null;
        byte[] content = new byte[maxBytesToRead];
        int bytesRead = is.read(content);

        ContentAndSignature result = ContentAndSignature.create();

        if (bytesRead <= 0){
            logger.warn("Could not read content from stream for file {}", filename);
            result.setSignature(EMPTY_SIG);
            return result.setActualSize(0);
        }

        // ZIP file entries may hold size of value UNKNOWN (-1). Need to read the input stream to detect actual size.
        if (fileSize < 0){
            logger.info("Negative file size detected for file {}", filename);
            if (bytesRead < signatureMaxSize) {
                actualContent = Arrays.copyOf(content, bytesRead);
            }
            else{
                logger.warn("File {} has unknown size, but is too large to fully.", filename);
            }
        }
        else{
            actualContent = content;
        }

        if (actualContent != null) {
            result.setSignature(sign(content));
        }

        return result.setContent(actualContent).setActualSize(bytesRead);
    }

    /**
     * After the first chunk of content was read from the stream and signed,
     * this method reads the rest of the content for ingestion purposes
     * @param cont          content and signature bean
     * @param is            input stream
     * @param fileSize      file size
     * @throws IOException  in case of IO failure
     */
    public void readRestOfFile(ContentAndSignature cont, InputStream is, int fileSize) throws IOException {
        int lng = cont.contentLength();
        if (lng < fileSize) {
            logger.debug("File size {} is greater than max signature length {}. Reading rest of file", fileSize, signatureMaxSize);

            ByteBuffer buffer = ByteBuffer.allocate(fileSize);
            buffer.put(cont.getContent());
            byte[] content = buffer.array();

            int bytesRead = is.read(content, lng, fileSize - lng);

            cont.setContent(content).setActualSize(lng + bytesRead);
        }
    }

    /**
     * Sign the first chunk of the content and return the signature as base 64 string
     * @param content chunk of content to sign
     * @return signature as base 64 string
     */
    public String sign(final byte[] content) {
        byte[] temp = content;
        if (content.length > signatureMaxSize) {
            temp = ArrayUtils.subarray(content, 0, signatureMaxSize);
        }
        byte[] bytes = DigestUtils.sha1(temp);
        return Base64.encodeBase64String(bytes);
    }

}
