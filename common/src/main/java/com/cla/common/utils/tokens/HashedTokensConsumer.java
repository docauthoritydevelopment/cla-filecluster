package com.cla.common.utils.tokens;

public interface HashedTokensConsumer {
	void accept(String token,long hashedToken, String locationDesc);
}
