package com.cla.common.utils;

import com.cla.common.domain.dto.mediaproc.excel.IngestConstants;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;

/**
 * Common methods used for CSV escaping and manipulation
 * Created by vladi on 3/7/2017.
 */
public class CsvUtils {

    /**
     * Make sure string is not null properly escaped to keep CSV format.
     * @param str original string
     * @return non-null CSV-ready escaped string
     */
    public static String notNull(final String str) {
        return str == null ? "" :
                ("\"" + str.replaceAll(IngestConstants.UTF8_FOUR_BYTES, "")
                        .replaceAll("\"", "\"\"") + "\"");
    }

    public static String notNull(final Date d) {
        return d == null ? "" : d.toString();
    }

    public static Collection<String> notNull(Collection<String> sl) {
        return sl == null ? Collections.emptyList() : sl;
    }
}
