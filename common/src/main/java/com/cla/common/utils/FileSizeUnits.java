package com.cla.common.utils;

import java.math.BigDecimal;

/**
 * File size units converter (rounds the result to long)
 * Created by vladi on 4/19/2017.
 */
public enum FileSizeUnits {

    GB(3),
    MB(2),
    KB(1),
    BYTE(0);

    public static int ROUND_UP = 1;
    public static int ROUND = 0;
    public static int ROUND_DOWN = -1;

    private static final int KILO = 1024;
    private int power;

    FileSizeUnits(int power) {
        this.power = power;
    }

    public long toBytes(double num){
        return toUnit(num, 0, ROUND);
    }

    public long toKilobytes(double num){
        return toUnit(num, 1, ROUND);
    }

    public long toMegabytes(double num){
        return toUnit(num, 2, ROUND);
    }

    public long toGigabytes(double num){
        return toUnit(num, 3, ROUND);
    }

    public long toBytes(double num, int roundMode){
        return toUnit(num, 0, roundMode);
    }

    public long toKilobytes(double num, int roundMode){
        return toUnit(num, 1, roundMode);
    }

    public double toKilobytes(double num, int roundMode, int scale){
        return toUnitRounded(num, 1, roundMode, scale);
    }

    public long toMegabytes(double num, int roundMode){
        return toUnit(num, 2, roundMode);
    }

    public double toMegabytes(double num, int roundMode, int scale){
        return toUnitRounded(num, 2, roundMode, scale);
    }

    public long toGigabytes(double num, int roundMode){
        return toUnit(num, 3, roundMode);
    }

    public double toGigabytes(double num, int roundMode, int scale){
        return toUnitRounded(num, 3, roundMode, scale);
    }

    private long toUnit(double num, int powerMod, int roundMode){
        double rawResult = num * Math.pow(KILO, power - powerMod);
        if (roundMode < 0){
            return Math.round(Math.floor(rawResult));
        }
        else
        if (roundMode > 0){
            return Math.round(Math.ceil(rawResult));
        }
        roundMode = 0; // reset to default
        return Math.round(rawResult);
    }

    private double toUnitRounded(double num, int powerMod, int roundMode, int scale){
        double rawResult = num * Math.pow(KILO, power - powerMod);
        if (roundMode < 0){
            return new BigDecimal(rawResult).setScale(scale, BigDecimal.ROUND_DOWN).doubleValue();
        }
        else
        if (roundMode > 0){
            return new BigDecimal(rawResult).setScale(scale, BigDecimal.ROUND_UP).doubleValue();
        }
        return new BigDecimal(rawResult).setScale(scale, BigDecimal.ROUND_HALF_UP).doubleValue();
    }

}
