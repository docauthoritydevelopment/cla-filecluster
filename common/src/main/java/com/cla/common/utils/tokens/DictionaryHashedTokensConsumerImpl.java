package com.cla.common.utils.tokens;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class DictionaryHashedTokensConsumerImpl implements HashedTokensConsumer {
	private final Set<String> hexHashTokenFilter;
	private final Map<String, String> hexHashTokenToTokenMapping = Maps.newHashMap();
	private final Map<String, List<String>> tokensLocations = Maps.newHashMap();
	private boolean locateClearTokens = false;
	
	public DictionaryHashedTokensConsumerImpl(final Set<String> tokensFilter){
		this(tokensFilter,false);
	}

	public DictionaryHashedTokensConsumerImpl(final Set<String> tokensFilter, final boolean locateClearTokens){
		this.hexHashTokenFilter=tokensFilter;
		this.locateClearTokens=locateClearTokens;
	}

	@Override
	public void accept(final String token, final long hashedToken,final String locationDesc) {
		if (locateClearTokens) {
			// Just mark location of the clear token
			if(hexHashTokenFilter.contains(token)){
				if(locationDesc!=null){
					List<String> list = tokensLocations.computeIfAbsent(token, k -> Lists.newLinkedList());
					list.add(locationDesc);
				}
			}
		}
		else {
			// Add to mapping + mark location
			final String hexHashToken = String.format("%X",hashedToken);
			if(hexHashTokenFilter.contains(hexHashToken)){
				hexHashTokenToTokenMapping.put(hexHashToken, token);
				if(locationDesc != null){
					List<String> list = tokensLocations.computeIfAbsent(token, k -> Lists.newLinkedList());
					list.add(locationDesc);
				}
			}
		}
	}
	
	public Map<String, List<String>> getTokensLocations() {
		return tokensLocations;
	}

	public String getClearToken(final String hashToken){
		return hexHashTokenToTokenMapping.get(hashToken);
	}

	@Override
	public String toString() {
		return "DictionaryHashedTokensConsumerImpl{" +
				"hexHashTokenFilter=" + hexHashTokenFilter +
				", hexHashTokenToTokenMapping=" + hexHashTokenToTokenMapping +
				", tokensLocations=" + tokensLocations +
				", locateClearTokens=" + locateClearTokens +
				'}';
	}
}
