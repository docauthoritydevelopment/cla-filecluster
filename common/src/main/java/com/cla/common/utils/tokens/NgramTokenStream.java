package com.cla.common.utils.tokens;

import com.cla.common.utils.tokens.DocAuthorityTokenStream;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;

import java.io.IOException;

/**
 *
 * Created by uri on 07/02/2017.
 */
public class NgramTokenStream implements DocAuthorityTokenStream {

    private TokenStream tokenStream;

    public NgramTokenStream(TokenStream tokenStream) {
        this.tokenStream = tokenStream;
    }

    @Override
    public Object getCharTermAttribute() {
        return tokenStream.getAttribute(CharTermAttribute.class);
    }

    @Override
    public void reset() throws IOException {
        tokenStream.reset();
    }

    @Override
    public boolean incrementToken() throws IOException {
        return tokenStream.incrementToken();
    }

    @Override
    public void close() throws IOException {
        tokenStream.close();
    }
}
