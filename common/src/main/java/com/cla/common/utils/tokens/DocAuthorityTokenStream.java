package com.cla.common.utils.tokens;

import java.io.Closeable;
import java.io.IOException;

/**
 * Created by uri on 07/02/2017.
 */
public interface DocAuthorityTokenStream extends Closeable {

    Object getCharTermAttribute();

    void reset() throws IOException;

    boolean incrementToken() throws IOException;

    void close() throws IOException;
}
