package com.cla.common.utils.json;

/**
 * Concrete class for string zipping serializer, ready to be used in Jackson annotations
 * @see com.fasterxml.jackson.databind.annotation.JsonSerialize
 *
 * Created by vladi on 3/8/2017.
 */
public class ZippingStringSerializer extends ZippingObjectSerializer<String>{
}
