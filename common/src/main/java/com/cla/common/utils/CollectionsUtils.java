package com.cla.common.utils;

import com.google.common.base.Objects;
import com.google.common.collect.Sets;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

import static java.util.Collections.newSetFromMap;

public class CollectionsUtils {

    private final static Logger logger = LoggerFactory.getLogger(CollectionsUtils.class);

    public static <E> Set<E> newConcurrentHashSet() {
        return newSetFromMap(new ConcurrentHashMap<E, Boolean>());
    }

    public static <K, V> Map<K, V> getSubMap(TreeMap<K, V> original, int offset, int size) {
        if (original.size() > size) {
            boolean inclusive = false;
            int lastInd = offset + size;
            if (lastInd >= original.size()) {
                lastInd = original.size() - 1;
                inclusive = true;
            }
            List<Long> keys = new ArrayList(original.keySet());
            Long startKey = keys.get(offset);
            Long endKey = keys.get(lastInd);
            if (inclusive) {
                return ((TreeMap) original).subMap(startKey, true, endKey, true);
            } else {
                return ((TreeMap) original).subMap(startKey, endKey);
            }
        }
        return original;
    }

    public static <T> T keyCollisionResolver(T obj1, T obj2){
        logger.warn("Collision between keys during collection of stream into map: {} vs. {}", obj1, obj2);
        return obj1;
    }

    public static <T> boolean compareAsSet(Collection<T> first, Collection<T> second){
        if (first == second){
            return  true;
        }
        if (first == null || second == null){
            return  false;
        }
        return com.google.common.base.Objects.equal(Sets.newHashSet(first), Sets.newHashSet(second));
    }

    public static <T> boolean compareArrayAsSet(T[] first, T[] second){
        if (first == second){
            return  true;
        }
        if (first == null || second == null){
            return  false;
        }
        return Objects.equal(Sets.newHashSet(first), Sets.newHashSet(second));
    }
}
