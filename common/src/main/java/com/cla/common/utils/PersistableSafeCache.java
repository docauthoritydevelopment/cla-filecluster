package com.cla.common.utils;

import com.google.common.cache.*;

import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * Thread safe cache supporting persistence.
 * Locks all the values during the persisting process.
 * Locks individual values during update.
 *
 * Created by vladi on 5/17/2017.
 */
public class PersistableSafeCache<K,V> {

    private CacheLoader<K,V> loader;
    private CachePersister<K,V> persister;
    private int maximumSize;

    private boolean expireAfterAccess = false;
    private int expireAfterAccessValue = 0;
    private TimeUnit expireAfterAccessTimeUnit = TimeUnit.SECONDS;

    private LoadingCache<K,ValueWrapper<V>> innerCache;

    private ReentrantReadWriteLock lock;

    PersistableSafeCache() {}

    void init(){
        lock = new ReentrantReadWriteLock();
        CacheBuilder<Object, Object> builder = CacheBuilder.newBuilder().maximumSize(maximumSize);
        if (expireAfterAccess){
            builder.expireAfterAccess(expireAfterAccessValue, expireAfterAccessTimeUnit);
        }

        builder.removalListener((RemovalListener) notification -> {
            if (notification.getCause().equals(RemovalCause.EXPIRED) ||
                    notification.getCause().equals(RemovalCause.SIZE)) {
                K key = (K)notification.getKey();
                ValueWrapper<V> wrapper = (ValueWrapper<V>)notification.getValue();
                if (wrapper.isDirty() && wrapper.getValue() != null) {
                    persister.persist(key, wrapper.getValue());
                }
            }
        });

        //noinspection NullableProblems
        innerCache = builder.build(new CacheLoader<K, ValueWrapper<V>>() {
                    @Override
                    public ValueWrapper<V> load(K key) throws Exception {
                        return ValueWrapper.create(loader.load(key));
                    }
                });
    }

    public V getForRead(K key){
        ValueWrapper<V> wrapper = innerCache.getIfPresent(key);
        if (wrapper == null){
            try {
                lock.writeLock().lock();
                wrapper = innerCache.getUnchecked(key);
            }
            finally {
                if (lock.writeLock().isHeldByCurrentThread()){
                    lock.writeLock().unlock();
                }
            }
        }
        return (wrapper != null) ? wrapper.getValue() : null;
    }

    public V lockForUpdate(K key){
        ValueWrapper<V> wrapper = innerCache.getIfPresent(key);
        if (wrapper == null){
            try {
                lock.writeLock().lock();
                wrapper = innerCache.getUnchecked(key);
            }
            finally {
                if (lock.writeLock().isHeldByCurrentThread()){
                    lock.writeLock().unlock();
                }
            }
        }
        if (wrapper != null){
            wrapper.lock();
            return wrapper.getValue();
        }
        return null;
    }

    public void unlockUpdate(K key){
        ValueWrapper<V> wrapper = innerCache.getIfPresent(key);
        if (wrapper != null){
            wrapper.flagAsDirty();
            wrapper.unlock();
        }
    }

    public void persistAll(){
        doSafe(() ->{
            ConcurrentMap<K, ValueWrapper<V>> map = innerCache.asMap();
            map.forEach((key, wrapper) -> {
                        if (wrapper.isDirty() && wrapper.tryLock()) {
                            try {
                                persister.persist(key, wrapper.getValue());
                                wrapper.clearDirtyFlag();
                            } finally {
                                wrapper.unlock();
                            }
                        }
                    });
        });
    }

    public void evict(K key){
        doSafe(() -> this.invalidate(key));
    }

    public void evictBy(Predicate<V> predicate){
        doSafe(() -> innerCache.asMap().entrySet().stream()
                .filter(entry -> predicate.test(entry.getValue().getValue()))
                .forEach(entry -> this.invalidate(entry.getKey())));
    }

    public void evictAll(){
        doSafe(() -> evictBy(v -> true));
    }

    public void replaceByKey(K key, V newValue){
        ValueWrapper<V> wrapper = innerCache.getIfPresent(key);
        if (wrapper == null){
            try {
                lock.writeLock().lock();
                wrapper = innerCache.getUnchecked(key);
            }
            finally {
                if (lock.writeLock().isHeldByCurrentThread()){
                    lock.writeLock().unlock();
                }
            }
        }
        if (wrapper != null){
            try {
                wrapper.lock();
                wrapper.setValue(newValue);
            } finally {
                wrapper.unlock();
            }
        }
    }

    @SuppressWarnings("unused")
    public void updateByKey(K key, Function<V,V> updater){
        doSafe(() -> {
            ValueWrapper<V> wrapper = innerCache.getUnchecked(key);
            if (wrapper != null){
                wrapper.setValue(updater.apply(wrapper.getValue()));
            }
        });
    }

    /**
     * Retrieve a value that corresponds to the provided key, perform the provided function on the value,
     * but only if the provided predicate yields true.
     * This operation is atomic (thread-safe).
     *
     * @param key       key corresponding to the value to be modified
     * @param condition predicate that has to be satisfied in order for the update to happen
     * @param updater   the function that performs the update on the value
     * @return          true if the predicate was satisfied and the update has happened
     */
    @SuppressWarnings("unused")
    public boolean conditionalUpdateByKey(K key, Predicate<V> condition, Function<V,V> updater){
        AtomicBoolean updated = new AtomicBoolean(false);
        doSafe(() -> {
            ValueWrapper<V> wrapper = innerCache.getUnchecked(key);
            if (wrapper != null && condition.test(wrapper.getValue())){
                wrapper.setValue(updater.apply(wrapper.getValue()));
                updated.set(true);
            }
        });
        return updated.get();
    }

    @SuppressWarnings("unused")
    public void conditionalUpdate(Predicate<V> predicate, Function<V,V> updater){
        doSafe(() -> innerCache.asMap().entrySet().stream()
                .filter(entry -> predicate.test(entry.getValue().getValue()))
                .forEach(entry -> entry.getValue().setValue(updater.apply(entry.getValue().getValue()))));
    }

    void setLoader(CacheLoader<K, V> loader) {
        this.loader = loader;
    }

    void setPersister(CachePersister<K, V> persister) {
        this.persister = persister;
    }

    void setMaximumSize(int maximumSize) {
        this.maximumSize = maximumSize;
    }

    public void setExpireAfterAccess(boolean expireAfterAccess) {
        this.expireAfterAccess = expireAfterAccess;
    }

    public void setExpireAfterAccessValue(int expireAfterAccessValue) {
        this.expireAfterAccessValue = expireAfterAccessValue;
    }

    public void setExpireAfterAccessTimeUnit(TimeUnit expireAfterAccessTimeUnit) {
        this.expireAfterAccessTimeUnit = expireAfterAccessTimeUnit;
    }

    private void doSafe(Runnable runnable){
         lock.writeLock().lock();
         try{
             runnable.run();
         }
         finally {
             if (lock.writeLock().isHeldByCurrentThread()){
                 lock.writeLock().unlock();
             }
         }
    }

    private void invalidate(K key){
        ValueWrapper<V> wrapper = innerCache.getUnchecked(key);
        wrapper.clearDirtyFlag(); // so it won't be saved by accident
        innerCache.invalidate(key);
    }

    /**
     * Builder for the cache featuring validation of required fields
     */
    public static class Builder<K,V>{

        private CacheLoader<K,V> loader;
        private CachePersister<K,V> persister;
        private int maximumSize = -1;
        private boolean expireAfterAccess = false;
        private int expireAfterAccessValue = 0;
        private TimeUnit expireAfterAccessTimeUnit = TimeUnit.SECONDS;

        @SuppressWarnings("unused")
        public static <S,T> Builder<S,T> create(Class<S> keyClass, Class<T> valueClass){
            return new Builder<>();
        }

        public Builder<K,V> expireAfterAccess(int timeValue, TimeUnit timeUnit){
            this.expireAfterAccess = true;
            this.expireAfterAccessValue = timeValue;
            this.expireAfterAccessTimeUnit = timeUnit;
            return this;
        }

        public Builder<K,V> maximumSize(int maximumSize){
            this.maximumSize = maximumSize;
            return this;
        }

        public Builder<K,V> setLoader(CacheLoader<K, V> loader) {
            this.loader = loader;
            return this;
        }

        public Builder<K,V> setPersister(CachePersister<K,V> persister) {
            this.persister = persister;
            return this;
        }

        public PersistableSafeCache<K,V> build(){
            if (maximumSize < 0 || loader == null || persister == null){
                throw new IllegalStateException("Insufficient initialization.");
            }
            PersistableSafeCache<K,V> result = new PersistableSafeCache<>();
            result.setMaximumSize(this.maximumSize);
            result.setLoader(this.loader);
            result.setPersister(this.persister);
            result.setExpireAfterAccess(this.expireAfterAccess);
            result.setExpireAfterAccessValue(this.expireAfterAccessValue);
            result.setExpireAfterAccessTimeUnit(this.expireAfterAccessTimeUnit);
            result.init();
            return result;
        }
    }

    /**
     * Code to persist a value
     * @param <V> value type
     */
    public interface CachePersister<K,V>{
        void persist(K key, V value);
    }

    /**
     * Value wrapper with built-in lock
     * @param <V> value type
     */
    public static class ValueWrapper<V>{
        private V value;
        private boolean dirty = false;
        private ReentrantLock valueLock;

        ValueWrapper(V value) {
            this.value = value;
            valueLock = new ReentrantLock();
        }

        public static <T> ValueWrapper<T> create(T value){
            return new ValueWrapper<>(value);
        }

        public V getValue() {
            return value;
        }

        public void setValue(V value) {
            this.value = value;
        }

        public void lock(){
            valueLock.lock();
        }

        public boolean tryLock(){
            return valueLock.tryLock();
        }

        public void unlock(){
            try {
                valueLock.unlock();
            }
            catch(IllegalMonitorStateException e){
                // do nothing
            }
        }

        void flagAsDirty(){
            this.dirty = true;
        }

        void clearDirtyFlag(){
            this.dirty = false;
        }

        public boolean isDirty() {
            return dirty;
        }
    }

}
