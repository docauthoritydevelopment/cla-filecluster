package com.cla.common.utils;

public class DbUtils {

    public static String getStrOrNull(String fieldValue) {
        return fieldValue == null ? null : "'" + escapeSingleQuote(fieldValue) + "'";
    }

    public static String escapeSingleQuote(String fieldValue) {
        return fieldValue == null ? null : fieldValue.replace("'", "''");

    }

    public static String escapeBackwardSeparator(String path) {
        return path == null ? null : path.replaceAll("\\\\", "\\\\\\\\");
    }
}
