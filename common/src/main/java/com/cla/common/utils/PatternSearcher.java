package com.cla.common.utils;

import com.cla.common.domain.dto.extraction.SearchPatternCountDto;
import com.cla.common.domain.dto.filesearchpatterns.TextSearchPatternImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.stream.Collectors;

/**
 * Created by: yael
 * Created on: 8/5/2018
 */
public class PatternSearcher {

    private static final Logger logger = LoggerFactory.getLogger(PatternSearcher.class);

    private boolean patternSearchActive = true;
    private boolean countDistinctMatches = true;
    private String[] ignoreValuesList;
    private Integer maxSingleSearchPatternMatches;
    private boolean searchPatternStopSearchAfterMulti = false;
    private Integer searchPatternCountThresholdMulti;

    public PatternSearcher(boolean patternSearchActive, boolean countDistinctMatches, String[] ignoreValuesList) {
        this.patternSearchActive = patternSearchActive;
        this.countDistinctMatches = countDistinctMatches;
        this.ignoreValuesList = ignoreValuesList;
    }

    public void setMaxSingleSearchPatternMatches(Integer maxSingleSearchPatternMatches) {
        this.maxSingleSearchPatternMatches = maxSingleSearchPatternMatches;
    }

    public void setSearchPatternStopSearchAfterMulti(boolean searchPatternStopSearchAfterMulti) {
        this.searchPatternStopSearchAfterMulti = searchPatternStopSearchAfterMulti;
    }

    public void setSearchPatternCountThresholdMulti(Integer searchPatternCountThresholdMulti) {
        this.searchPatternCountThresholdMulti = searchPatternCountThresholdMulti;
    }

    public Set<SearchPatternCountDto> getSearchPatternsCounting(String fileText, Collection<TextSearchPatternImpl> searchPatterns){
        return getSearchPatternsCounting(fileText, false, searchPatterns);
    }

    public Set<SearchPatternCountDto> getSearchPatternsCounting(String fileText, boolean withMatchingTerms,
                                                                Collection<TextSearchPatternImpl> searchPatterns){
        final Set<SearchPatternCountDto> searchPatternsCounting = new HashSet<>(0);
        if (!patternSearchActive) {
            logger.trace("Search pattern is not active. Not returning results");
            return searchPatternsCounting;
        }
        // Fetch all patterns including consumers as well as search patterns
        for (final TextSearchPatternImpl textSearchPatternImpl: searchPatterns) {
            if (textSearchPatternImpl.isActive()) {
                if (logger.isTraceEnabled()) {
                    logger.trace("Counting pattern {} {} ...", textSearchPatternImpl.getId(), textSearchPatternImpl.getName());
                }
                List<String> matches = countMatchings(fileText, textSearchPatternImpl);
                if (matches.size() > 0) {
                    if (countDistinctMatches) {
                        matches = matches.stream().distinct().collect(Collectors.toList());
                    }
                    List<String> matchingTerms = withMatchingTerms ? matches : null;
                    searchPatternsCounting.add(new SearchPatternCountDto(textSearchPatternImpl.getId(),
                            textSearchPatternImpl.getName(), matches.size(), matchingTerms));
                }
            }
        }
        return searchPatternsCounting;
    }

    public boolean isPatternSearchActive() {
        return patternSearchActive;
    }

    Integer calcActualLimitForPattern() {
        if (searchPatternCountThresholdMulti != null && maxSingleSearchPatternMatches != null) {
            if (maxSingleSearchPatternMatches < searchPatternCountThresholdMulti || searchPatternStopSearchAfterMulti) {
                return searchPatternCountThresholdMulti;
            } else {
                return maxSingleSearchPatternMatches;
            }
        } else if (maxSingleSearchPatternMatches != null) {
            return maxSingleSearchPatternMatches;
        } else if (searchPatternCountThresholdMulti != null && searchPatternStopSearchAfterMulti) {
            return searchPatternCountThresholdMulti;
        }
        return null;
    }

    private List<String> countMatchings(final String input, final TextSearchPatternImpl textSearchPatternImpl){
        List<String> findings = new ArrayList<>();
        final Predicate<String> validator = textSearchPatternImpl.getPredicate();
        final Matcher m = textSearchPatternImpl.getCompiledPattern().matcher(input);
        String firstMatch = null;
        String firstValidMatch = null;
        Integer limit = calcActualLimitForPattern();
        while ((limit == null || limit > findings.size()) && m.find()) {
            final String match = m.group();
            if (isInIgnoreList(match)) {
                continue;
            }
            if (firstMatch == null) {
                firstMatch = match;
            }
            final boolean valid = (validator == null || validator.test(match));
            if (valid) {
                findings.add(match);
                if (firstValidMatch == null) {
                    firstValidMatch = match;
                }
            }
        }
        if (logger.isTraceEnabled()) {
            logger.trace("Found {} matches for pattern {}. First {} match is '{}'", findings.size(),
                    textSearchPatternImpl.getName(),(firstValidMatch == null) ? "" : " valid",
                    (firstValidMatch == null) ? ((firstMatch == null) ? "" : firstMatch) : firstValidMatch);
        }
        return findings;
    }

    private boolean isInIgnoreList(String match) {
        if (ignoreValuesList == null) return false;
        for (String ignore : ignoreValuesList) {
            if (match.matches(ignore)) return true;
        }
        return false;
    }

    public Set<String> getAllMatchingStrings(final String input,  final int limit, Collection<TextSearchPatternImpl> searchPatterns){
        if (!patternSearchActive) {
            return new HashSet<>(0);
        }
        Set<String> result = new HashSet<>();
        for (final TextSearchPatternImpl textSearchPatternImpl: searchPatterns) {
            if (logger.isTraceEnabled()) {
                logger.trace("Counting pattern {} {} ...", textSearchPatternImpl.getId(), textSearchPatternImpl.getName());
            }
            Set<String> patternList = getMatchingStrings(input, textSearchPatternImpl, limit);
            result.addAll( patternList);

        }

        return result;
    }

    private Set<String> getMatchingStrings(final String input, final TextSearchPatternImpl textSearchPatternImpl, final int limit){
        int count = 0;
        final Set<String> result = new HashSet<>();
        final Predicate<String> validator = textSearchPatternImpl.getPredicate();
        final Matcher m = textSearchPatternImpl.getCompiledPattern().matcher(input);
        while (m.find()) {
            final String match = m.group();
            if (isInIgnoreList(match)) {
                continue;
            }
            final boolean valid = (validator == null || validator.test(match));
            if(valid){
                result.add(new String(match));
                count++;
                if (limit > 0 && result.size() >= limit) {
                    break;
                }
            }
        }
        return result;
    }
}
