package com.cla.common.utils.thread;

import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class ClaPropertySet extends ConcurrentHashMap<String, Object> {

    public static ClaPropertySet create(){
        return new ClaPropertySet();
    }

    public Boolean setBoolean(String key, boolean val) {
        return safeGetValue(Boolean.class, put(key, val));
    }

    public Boolean getBoolean(String key) {
        return safeGetValue(Boolean.class, get(key));
    }

    public String setString(String key, String val) {
        return safeGetValue(String.class, put(key, val));
    }

    public String getString(String key) {
        return safeGetValue(String.class, get(key));
    }

    public Integer setInteger(String key, Integer val) {
        return safeGetValue(Integer.class, put(key, val));
    }

    public Integer getInteger(String key) {
        return safeGetValue(Integer.class, get(key));
    }

    @Override
    public String toString() {
        return this.entrySet().stream()
                .map(e -> "{" + e.getKey() + ":" + e.getValue().toString() + "}")
                .collect(Collectors.joining(","));
    }

    private <T> T safeGetValue(Class<T> valueClass, Object value){
        //noinspection unchecked
        return (value == null || !(valueClass.isAssignableFrom(valueClass))) ? null : (T)value;
    }
}
