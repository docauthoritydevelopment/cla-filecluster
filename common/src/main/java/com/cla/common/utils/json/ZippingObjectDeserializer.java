package com.cla.common.utils.json;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.io.*;
import java.util.zip.GZIPInputStream;

/**
 * Abstract class to be extended by concrete object de-serializers
 * De-serializes object from Base64 string, unzips the result and converts back to object
 *
 * @see ZippingObjectSerializer
 *
 * Created by vladi on 3/8/2017.
 */
@SuppressWarnings("unchecked")
public class ZippingObjectDeserializer <T extends Serializable> extends JsonDeserializer<T> {
    @Override
    public T deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {
        try (
                ByteArrayInputStream bi = new ByteArrayInputStream(jp.getBinaryValue());
                GZIPInputStream zipInputStream = new GZIPInputStream(bi);
                ObjectInputStream si = new ObjectInputStream(zipInputStream)
        ){
            return (T) si.readObject();
        }
        catch(ClassNotFoundException cnfe){
            throw new RuntimeException(cnfe.toString());
        }
    }
}
