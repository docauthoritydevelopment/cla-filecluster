package com.cla.common.utils;

import ch.qos.logback.core.rolling.FixedWindowRollingPolicy;

/**
 * Created by: yael
 * Created on: 5/21/2018
 */
public class FixedWindowRollingPolicyNoLimit extends FixedWindowRollingPolicy {

    @Override
    protected int getMaxWindowSize() {
        return 500;
    }
}
