package com.cla.common.utils;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.InvalidPathException;
import java.util.Arrays;

/**
 * Created By Itai Marko
 */
public final class ErrorHandlingUtils {

    private ErrorHandlingUtils() {}

    public static boolean isIORelatedException(Throwable t) {
        return  t instanceof IOException ||
                t instanceof UncheckedIOException ||
                t instanceof InvalidPathException;
    }

    public static String getCurrentStackTraceAsString() {
        return Arrays.toString(Thread.currentThread().getStackTrace());
    }
}

