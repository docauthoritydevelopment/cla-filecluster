package com.cla.common.utils.json;


import com.cla.common.domain.dto.file.FileContentHolder;

/**
 * Serializer for download file (file content download results)
 * @see FileContentHolder
 *
 * Created by liron
 */
public class FileContentHolderSerializer extends ZippingObjectSerializer<FileContentHolder> {
}
