package com.cla.common.utils;

import com.cla.connector.domain.dto.media.EmailAddress;

import java.util.stream.Stream;

/**
 * Created By Itai Marko
 */
public class EmailUtils {

    public static final String EMPTY_VALUE = "_empty_";
    public static final String UNKNOWN_VALUE = "_unknown_";

    /*
     * Heuristic logic to trim last 3 extra characters from extracted X500 addresses.
     */
    private static final boolean X500_useHeuristicTrimming = true;

    /*
     *
     * Support for X.500/LDAP address notation that was common in Exchange
     * Sample address: /o=Organisation/ou=Administrative Group/cn=Recipients/cn=sample.user@mydomain.com
     *
     *      (different than original X.500 notation https://tools.ietf.org/html/rfc1779)
     */
    private static final String X500_CN = "cn=";
    private static final String X500_SEPARATOR = "/";

    private EmailUtils() {}

    public static String extractDomainFromEmailAddress(String emailAddress, boolean parseX500Address, boolean extractNonDomainCN) {
        if (emailAddress == null || emailAddress.trim().isEmpty()) return EMPTY_VALUE;
        if (parseX500Address && isX500Address(emailAddress)) {
            String x500DomainAddress = extractX500DomainAddress(emailAddress, extractNonDomainCN);
            if (x500DomainAddress != null) {
                emailAddress = x500DomainAddress;
            }
        }
        int lastIndexOfAt = emailAddress.lastIndexOf('@');
        if (lastIndexOfAt == -1 || lastIndexOfAt == emailAddress.length()-1) {
            return UNKNOWN_VALUE;
        }
        return emailAddress.substring(lastIndexOfAt+1).toLowerCase();
    }

    public static boolean containsDomain(String emailAddress) {
        if (emailAddress == null || emailAddress.trim().isEmpty()) return false;

        int lastIndexOfAt = emailAddress.lastIndexOf('@');
        if (lastIndexOfAt == -1 || lastIndexOfAt == emailAddress.length()-1) {
            return false;
        }
        return true;
    }

    public static String getEmailAddressValue(String emailAddress, boolean parseX500Address, boolean extractNonDomainCN) {
        if (emailAddress == null || emailAddress.trim().isEmpty()) return EMPTY_VALUE;
        if (parseX500Address && isX500Address(emailAddress)) {
            String x500DomainAddress = extractX500DomainAddress(emailAddress, extractNonDomainCN);
            if (x500DomainAddress != null) {
                emailAddress = x500DomainAddress;
            }
        }
        return emailAddress.toLowerCase();
    }

    public static String extractX500DomainAddress(String emailAddress, boolean extractNonDomainCN) {
        String[] sp = emailAddress.toLowerCase().split(X500_SEPARATOR, 20);
        String addressItem = null;
        if (containsDomain(emailAddress)) {
            addressItem = Stream.of(sp)
                    .filter(item -> item.startsWith(X500_CN))
                    .filter(item -> item.contains("@")).findFirst().orElse(null);
        } else if (extractNonDomainCN) {
            // Corrupted or internal address - use last /cn clause value
            for (int i=sp.length-1; (addressItem == null && i>=0); i--) {
                if (sp[i].startsWith(X500_CN)) {
                    addressItem = sp[i];
                }
            }
        }
        if (addressItem == null) {
            return null;
        }
        addressItem = addressItem.substring(X500_CN.length());

        /*
         * Heuristic logic to trim last 3 extra characters from extracted X500 addresses.
         * Reason for this extra characters is unknown
         * Todo: further investigate when based on more field experience
         */
        if (X500_useHeuristicTrimming) {
            int lastDomainPartLength = addressItem.lastIndexOf('.');
            lastDomainPartLength = (lastDomainPartLength < 0) ? 0 : addressItem.length() - lastDomainPartLength - 1;
            if (lastDomainPartLength > 3) {
                addressItem = addressItem.substring(0, addressItem.length() - 3);
            }
        }
        return addressItem;
    }

    private static boolean isX500Address(String emailAddress) {
        return emailAddress != null && emailAddress.toLowerCase().contains(X500_SEPARATOR+X500_CN);
    }

    public static String getRecipientName(EmailAddress recipient) {
        return getMailName(recipient.getName());
    }

    public static String getMailName(String name) {
        if (name == null || name.trim().isEmpty()) {
            return EMPTY_VALUE;
        } else {
            return name.toLowerCase();
        }
    }

}
