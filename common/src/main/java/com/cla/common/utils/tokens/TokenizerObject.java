package com.cla.common.utils.tokens;

import java.io.IOException;

/**
 * Created by uri on 05/02/2017.
 */
public interface TokenizerObject {

    DocAuthorityTokenStream setInput(String input) throws IOException;
}
