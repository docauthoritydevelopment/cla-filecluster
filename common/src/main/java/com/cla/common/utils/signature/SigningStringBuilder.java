package com.cla.common.utils.signature;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;

import java.security.MessageDigest;

/**
 * String Builder that adds the appended sequences to a MessageDigest
 * and is capable of producing the SHA-1 signature of the underlying text upon request.
 *
 * Created by vladi on 12/27/2016.
 */
public class SigningStringBuilder implements Appendable{
    private boolean signingEnabled = true;
    private StringBuilder builder;
    private MessageDigest digest;
    private byte[] signature;

    public static SigningStringBuilder create(){
        return new SigningStringBuilder(DigestUtils.getSha1Digest(), true);
    }

    public static SigningStringBuilder create(MessageDigest commonDigest){
        return new SigningStringBuilder(commonDigest, true);
    }

    public static SigningStringBuilder create(MessageDigest commonDigest, boolean enabled){
        return new SigningStringBuilder(commonDigest, enabled);
    }

    public static SigningStringBuilder create(boolean enabled){
        return new SigningStringBuilder(DigestUtils.getSha1Digest(), enabled);
    }

    private SigningStringBuilder(MessageDigest commonDigest, boolean enabled) {
        builder = new StringBuilder();
        digest = commonDigest;
        signingEnabled = enabled;
    }

    @Override
    public SigningStringBuilder append(CharSequence csq) {
        return append(csq.toString());
    }

    @Override
    public SigningStringBuilder append(CharSequence csq, int start, int end) {
        return append(csq.subSequence(start, end).toString());
    }

    @Override
    public SigningStringBuilder append(char c) {
        return append(String.valueOf(c));
    }

    public SigningStringBuilder append(String s) {
        if (signature != null) {
            throw new IllegalStateException("cannot append once get signature was called");
        }

        builder.append(s);
        if (signingEnabled) {
            digest.update(s.getBytes());
        }
        return this;
    }

    public byte[] getSignature(){
        if (!signingEnabled) {
            return null;
        }

        if (signature == null) {
            synchronized (this) {
                if (signature == null) {
                    signature = digest.digest();
                }
            }
        }
        return signature;
    }

    public String getSignatureAsBase64(){
        return signingEnabled ? Base64.encodeBase64String(getSignature()) : null;
    }

    @Override
    public String toString() {
        return builder.toString();
    }

}
