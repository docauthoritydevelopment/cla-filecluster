package com.cla.common.utils.json;

import com.cla.common.domain.dto.histogram.ClaSuperCollection;

/**
 * Deserializer for PV histograms (ingestion results)
 * @see ClaSuperCollection
 *
 * Created by vladi on 3/7/2017.
 */
public class ClaSuperCollectionDeserializer extends ZippingObjectDeserializer<ClaSuperCollection>{
}
