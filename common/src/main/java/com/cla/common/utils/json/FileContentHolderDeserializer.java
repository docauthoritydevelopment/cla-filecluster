package com.cla.common.utils.json;


import com.cla.common.domain.dto.file.FileContentHolder;

/**
 * Concrete class for FileContentHolder zipping de-serializer, ready to be used in Jackson annotations
 * @see FileContentHolder
 * @see com.fasterxml.jackson.databind.annotation.JsonDeserialize
 *
 * Created by liron
 */
public class FileContentHolderDeserializer extends ZippingObjectDeserializer<FileContentHolder> {
}
