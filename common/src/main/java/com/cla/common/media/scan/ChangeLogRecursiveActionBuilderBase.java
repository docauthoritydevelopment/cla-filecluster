package com.cla.common.media.scan;

import com.cla.connector.domain.dto.messages.ScanTaskParameters;

/**
 * Created by oren on 2/4/2018.
 */
public abstract class ChangeLogRecursiveActionBuilderBase<T extends ChangeLogRecursiveActionBase<V>, V> {
    protected ScanTaskParameters scanParams;

    protected long fromTime;
    private V path;

    public ChangeLogRecursiveActionBuilderBase withScanParameters(ScanTaskParameters scanParams) {
        this.scanParams = scanParams;
        return this;
    }

    public ChangeLogRecursiveActionBuilderBase withPath(V path) {
        this.path = path;
        return this;
    }

    public ChangeLogRecursiveActionBuilderBase withFromTime(long fromTime) {
        this.fromTime = fromTime;
        return this;
    }

    public abstract T build();

    protected T build(T task){
        task.scanParams = scanParams;
        task.fromTime = fromTime;
        task.path = path;
        return task;
    }
}