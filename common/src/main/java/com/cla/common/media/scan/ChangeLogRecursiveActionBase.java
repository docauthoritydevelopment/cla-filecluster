package com.cla.common.media.scan;

import com.cla.connector.domain.dto.messages.ScanTaskParameters;

import java.util.concurrent.RecursiveAction;

/**
 * Created by oren on 2/4/2018.
 */
public abstract class ChangeLogRecursiveActionBase<V> extends RecursiveAction {

    protected ScanTaskParameters scanParams;

    protected long fromTime;

    protected V path;

    public long getFromTime() {
        return fromTime;
    }

    public void setFromTime(long fromTime) {
        this.fromTime = fromTime;
    }

    public V getPath() {
        return path;
    }

    public void setPath(V path) {
        this.path = path;
    }
}
