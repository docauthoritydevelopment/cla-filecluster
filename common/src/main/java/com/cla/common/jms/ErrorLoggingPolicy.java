package com.cla.common.jms;

import org.slf4j.Logger;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;


/*
    Track IO errors from active mq, log them and provide basic stats
    see TransportErrorStats
 */
public class ErrorLoggingPolicy {

    Map<String, TransportErrorStats> errorStatsMap = new HashMap<>();


    public void onException(Logger logger, Exception error) {
        trackError(error);
        logger.debug("Transport exception occurred:\n {\ntype: {},\nmessage: {}\nstats: {}\n}",
                error.getClass().getName(), error.getMessage(), errorStatsMap.get(error.getClass().getName()));
    }


    private void trackError(Exception error) {
        TransportErrorStats transportErrorStats = errorStatsMap.get(error.getClass().getName());
        if (transportErrorStats == null) {
            synchronized (this) {
                transportErrorStats = errorStatsMap.get(error.getClass().getName());
                if (transportErrorStats == null) {
                    transportErrorStats = new TransportErrorStats();
                    errorStatsMap.put(error.getClass().getName(), transportErrorStats);
                }
            }
        }
        transportErrorStats.collect(error);
    }


    /**
     * collect error stats with the following format:
     * {
     *     errorType:
     *     rate:
     *     lastTime:
     * }
     */
    private class TransportErrorStats {
        private long count;
        private long lastTime;
        private long totalTime;
        private double rate;

        public synchronized void collect(Exception error) {
            count++;
            long update = System.currentTimeMillis();
            totalTime += update - (lastTime == 0 ? update : lastTime);
            lastTime = update;
            rate = (double) ((totalTime / count) / 1000);
        }

        @Override
        public String toString() {
            return "TransportErrorStats{" +
                    "count=" + count +
                    ", rate= every " + (rate + " sec") +
                    ", lastTime=" + new Date(lastTime) +
                    '}';
        }
    }
}
