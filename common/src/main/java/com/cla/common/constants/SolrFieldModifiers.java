package com.cla.common.constants;

/**
 * Atomic Updates<br/><br/>
 * Solr supports several modifiers that atomically update values of a document.
 * This allows updating only specific fields, which can help speed indexing processes in an environment
 * where speed of index additions is critical to the application.
 * To use atomic updates, add a modifier to the field that needs to be updated.
 * The content can be updated, added to, or incrementally increased if a number.
 * <br/>
 * Created by vladi on 7/17/2017.
 */
public interface SolrFieldModifiers {

    // Set or replace the field value(s) with the specified value(s), or remove the values if 'null' or empty list is specified as the new value.
    // May be specified as a single value, or as a list for multiValued fields
    String SET = "set";

    // Adds the specified values to a multiValued field.
    // May be specified as a single value, or as a list.
    String ADD = "add";

    // Removes (all occurrences of) the specified values from a multiValued field.
    // May be specified as a single value, or as a list.
    String REMOVE = "remove";

    // Removes all occurrences of the specified regex from a multiValued field.
    // May be specified as a single value, or as a list.
    String REMOVE_REGEX = "removeregex";

    // Increments a numeric value by a specific amount.
    // Must be specified as a single numeric value.
    String INC = "inc";

}
