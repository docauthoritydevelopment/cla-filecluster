package com.cla.common.constants;

import java.util.Collection;
import java.util.stream.Collectors;

public interface SolrCoreSchema {

    Class getType();
    String getSolrName();
    String filter(Object value);
    String inQuery(Collection<?> col);
    SolrCoreSchema idField();
    String notFilter(Object value);
    String notInQuery(Collection<?> col);
    
    default Class getFieldType() {
        return getType();
    }

    default String inQuery(String solrName, Collection<?> col){
        String prefix = solrName + ":(";
        return col.stream().map(String::valueOf).collect(Collectors.joining(" ", prefix, ")"));
    }

    default String notInQuery(String solrName, Collection<?> col){
        String prefix = "-" + solrName + ":(";
        return col.stream().map(String::valueOf).collect(Collectors.joining(" ", prefix, ")"));
    }

    default String filter(String solrName, Object value){
        return solrName + ":" + String.valueOf(value);
    }

    default String notFilter(String solrName, Object value){
        return "-" + solrName + ":" + String.valueOf(value);
    }

    default String toString(String solrName) {
        return solrName;
    }

}
