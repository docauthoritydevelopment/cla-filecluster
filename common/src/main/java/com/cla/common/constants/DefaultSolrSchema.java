package com.cla.common.constants;

import java.util.Collection;

public enum DefaultSolrSchema implements SolrCoreSchema{
    ID(Long.class, "id");

    private Class type;
    private String solrName;

    DefaultSolrSchema(Class type, String solrName) {
        this.type = type;
        this.solrName = solrName;
    }

    @Override
    public Class getType() {
        return type;
    }

    @Override
    public String getSolrName() {
        return solrName;
    }

    @Override
    public String filter(Object value){
        return filter(solrName, value);
    }

    @Override
    public String inQuery(Collection<?> col){
        return inQuery(solrName, col);
    }

    @Override
    public String notInQuery(Collection<?> col){
        return notInQuery(solrName, col);
    }

    @Override
    public String notFilter(Object value){
        return notFilter(solrName, value);
    }

    @Override
    public SolrCoreSchema idField() {
        return ID;
    }

    @Override
    public String toString() {
        return toString(solrName);
    }

}
