package com.cla.common.constants;

import java.time.LocalDateTime;
import java.util.Collection;

public enum ContentMetadataFieldType implements SolrCoreSchema{

    ID(String.class, "id"),
    TYPE(Integer.class, "type"),
    CONTENT_SIGNATURE(String.class, "contentSignature"),
    USER_CONTENT_SIGNATURE(String.class, "userContentSignature"),
    GROUP_ID(String.class, "groupId"),
    USER_GROUP_ID(String.class, "userGroupId"),
    ANALYSIS_HINT(String.class, "analysisHint"),
    GROUP_NAME(String.class, "groupName"),
    CREATION_DATE(LocalDateTime.class, "creationDate"),
    SIZE(Long.class, "size"),
    FILE_COUNT(Integer.class, "fileCount"),
    PROCESSING_STATE(String.class, "processing_state"),
    DELETED(Boolean.class, "deleted"),
    PROPOSED_TITLES(String.class, "proposed_titles"),
    AUTHOR(String.class, "author"),
    COMPANY(String.class, "company"),
    SUBJECT(String.class, "subject"),
    TITLE(String.class, "title"),
    KEYWORDS(String.class, "keywords"),
    GENERATING_APP(String.class, "generatingApp"),
    TEMPLATE(String.class, "template"),
    LAST_INGEST_TIMESTAMP_MS(Long.class, "lastIngestTimestampMs"),
    SAMPLE_FILE_ID(Long.class, "sampleFileId"),
    GROUP_DIRTY(Boolean.class, "groupDirty"),
    POPULATION_DIRTY(Boolean.class, "populationDirty"),
    ITEMS_COUNT1(Long.class, "itemsCountL1"),
    ITEMS_COUNT2(Long.class, "itemsCountL2"),
    ITEMS_COUNT3(Long.class, "itemsCountL3"),
    SKIP_ANALYSIS_REASON(Integer.class, "skipAnalysisReason"),
    EXTRACTED_ENTITIES_IDS(String.class, "extractedEntitiesIds"),
    CURRENT_RUN_ID(Long.class, "currentRunId"),
    TASK_STATE(String.class, "taskState"),
    TASK_STATE_DATE(Long.class, "taskStateDate"),
    RETRIES(Integer.class, "retries"),
    UPDATE_DATE(Long.class, "updateDate"),
    GROUP_DIRTY_DATE(Long.class, "groupDirtyDate"),
    POP_DIRTY_DATE(Long.class, "popDirtyDate"),
    ;

    private Class type;
    private String solrName;

    ContentMetadataFieldType(Class type, String solrName) {
        this.type = type;
        this.solrName = solrName;
    }

    @Override
    public Class getType() {
        return type;
    }

    @Override
    public String getSolrName() {
        return solrName;
    }

    @Override
    public String filter(Object value){
        return filter(solrName, value);
    }

    @Override
    public String inQuery(Collection<?> col){
        return inQuery(solrName, col);
    }

    @Override
    public String notInQuery(Collection<?> col){
        return notInQuery(solrName, col);
    }

    @Override
    public String notFilter(Object value){
        return notFilter(solrName, value);
    }

    @Override
    public String toString() {
        return toString(solrName);
    }

    @Override
    public SolrCoreSchema idField() {
        return ID;
    }
}
