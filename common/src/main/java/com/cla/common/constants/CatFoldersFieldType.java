package com.cla.common.constants;

import java.util.Collection;

public enum CatFoldersFieldType implements SolrCoreSchema{
    ID(String.class, "id"),
    PATH(String.class, "path"),
    REAL_PATH(String.class, "realPath"),
    MEDIA_ENTITY_ID(String.class, "mediaEntityId"),
    NAME(String.class, "name"),
    FOLDER_HASH(String.class, "folderHash"),
    ROOT_FOLDER_ID(Long.class,"rootFolderId"),
    PARENT_FOLDER_ID(Long.class, "parentFolderId"),
    DEPTH_FROM_ROOT(Integer.class, "depthFromRoot"),
    ABSOLUTE_DEPTH(Integer.class, "absoluteDepth"),
    PARENTS_INFO(String.class, "parentsInfo"),
    PATH_DESCRIPTOR_TYPE(String.class, "pathDescriptorType"),
    ACL_SIGNATURE(String.class, "aclSignature"),
    ACL_READ(String.class,"acl_read"),
    ACL_WRITE(String.class,"acl_write"),
    ACL_DENIED_READ(String.class,"acl_denied_read"),
    ACL_DENIED_WRITE(String.class,"acl_denied_write"),
    DELETED(Boolean.class, "deleted"),
    TYPE(Integer.class, "type"),
    SUB_FOLDERS_NUM(Integer.class, "numOfSubFolders"), // deprecated
    DIRECT_FILES_NUM(Integer.class, "numOfDirectFiles"), // deprecated
    ALL_FILES_NUM(Integer.class, "numOfAllFiles"), // deprecated
    MAILBOX_UPN(String.class, "mailboxUpn"),
    SYNC_STATE(String.class, "syncState"),
    FOLDER_ASSOCIATIONS(String.class, "associations"),
    CREATION_DATE(Long.class, "creationDate"),
    UPDATE_DATE(Long.class, "updateDate"),
    ;

    private Class type;
    private String solrName;

    CatFoldersFieldType(Class type, String solrName) {
        this.type = type;
        this.solrName = solrName;
    }

    @Override
    public Class getType() {
        return type;
    }

    @Override
    public String getSolrName() {
        return solrName;
    }

    @Override
    public String filter(Object value){
        return filter(solrName, value);
    }

    @Override
    public String inQuery(Collection<?> col){
        return inQuery(solrName, col);
    }

    @Override
    public String toString() {
        return toString(solrName);
    }

    @Override
    public String notInQuery(Collection<?> col){
        return notInQuery(solrName, col);
    }

    @Override
    public String notFilter(Object value){
        return notFilter(solrName, value);
    }

    @Override
    public SolrCoreSchema idField() {
        return ID;
    }
}
