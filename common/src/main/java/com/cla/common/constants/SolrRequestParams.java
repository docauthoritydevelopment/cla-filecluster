package com.cla.common.constants;

/**
 * Parameters used in Solr queries
 * Created by uri on 02/09/2015.
 */
public interface SolrRequestParams {
    String TAG = "tag";
    String FIELD = "field";
    String TAG_OP = "tag.op";
    String COUNT = "cnt";
    String TAG_SINGLE_VALUE = "tag.singleValue";
    String TAG_MULTI_VALUE = "tag.multiValue";
    String OPERATION = "operation";
    String COMMIT_TYPE = "commitType";

    String UPDATE_OP = "update.op";
    String VALUE = "value";

    String ACTION_ADD = "add";
    String ACTION_SET = "set";
}
