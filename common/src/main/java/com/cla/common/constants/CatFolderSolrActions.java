package com.cla.common.constants;

/**
 * Created by: yael
 * Created on: 3/27/2018
 */
public enum CatFolderSolrActions {
    UPDATE_PARENT_INFO_NO_PARENT("updateFoldersParentsInformationNoParent"),
    ;

    private String name;

    CatFolderSolrActions(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
