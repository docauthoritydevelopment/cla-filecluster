package com.cla.common.constants;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

public enum SolrFieldOp {
    // Set or replace the field value(s) with the specified value(s),
    // or remove the values if 'null' or empty list is specified as the new value.
    // May be specified as a single value, or as a list for multiValued fields
    SET("set"),

    // Adds the specified values to a multiValued field.
    // May be specified as a single value, or as a list.
    ADD("add"),

    // Removes (all occurrences of) the specified values from a multiValued field.
    // May be specified as a single value, or as a list.
    REMOVE("remove"),

    // Removes all occurrences of the specified regex from a multiValued field.
    // May be specified as a single value, or as a list.
    REMOVE_REGEX("removeregex"),

    // Increments a numeric value by a specific amount.
    // Must be specified as a single numeric value.
    INC("inc");

    private static Map<String, SolrFieldOp> opByName = new HashMap<>();

    static {
        Stream.of(SolrFieldOp.values()).forEach(op -> opByName.put(op.opName, op));
    }

    private String opName;

    SolrFieldOp(String opName) {
        this.opName = opName;
    }

    public String getOpName() {
        return opName;
    }

    public static SolrFieldOp byName(String opName){
        return opByName.get(opName);
    }
}
