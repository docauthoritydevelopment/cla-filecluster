package com.cla.common.constants;

import com.google.common.collect.Maps;

import java.util.Map;

/**
 * Created by: yael
 * Created on: 1/2/2019
 */
public enum SkipAnalysisReasonType {
    NOT_SKIPPED(0),
    GROUPED_BY_ANOTHER(1),
    SKIPPED_SELECTIVELY(2)
    ;

    private static final Map<Integer, SkipAnalysisReasonType> intMapping = Maps.newHashMap();
    private final int value;

    SkipAnalysisReasonType(int value) {
        this.value = value;
    }

    static {
        for (final SkipAnalysisReasonType t : SkipAnalysisReasonType.values()) {
            intMapping.put(t.value, t);
        }
    }

    public int toInt() {
        return value;
    }

    public static SkipAnalysisReasonType valueOf(final int value) {
        final SkipAnalysisReasonType mt = SkipAnalysisReasonType.intMapping.get(value);
        if (mt == null) {
            throw new RuntimeException("Invalid value: " + value);
        }
        return mt;
    }
}
