package com.cla.common.constants;

import java.util.Collection;

public enum FileGroupsFieldType implements SolrCoreSchema {
    ID(String.class, "id"),
    NAME(String.class, "name"),
    GENERATED_NAME(String.class, "generatedName"),
    DELETED(Boolean.class, "deleted"),
    DIRTY(Boolean.class, "dirty"),
    TYPE(Integer.class, "type"),
    NUM_OF_FILES(Long.class, "numOfFiles"),
    COUNT_LAST_NAMING(Long.class, "countOnLastNaming"),
    GROUP_ASSOCIATIONS(String.class, "associations"),
    CREATION_DATE(Long.class, "creationDate"),
    UPDATE_DATE(Long.class, "updateDate"),
    FIRST_MEMBER_ID(Long.class, "firstMemberId"),
    FIRST_MEMBER_CONTENT_ID(Long.class, "firstMemberContentId"),
    RAW_ANALYSIS_GROUP_ID(String.class, "rawAnalysisGroupId"),
    PARENT_GROUP_ID(String.class, "parentGroupId"),
    HAS_SUBGROUPS(Boolean.class, "hasSubgroups"),

    FST_PROP_NAMES(String.class, "firstProposedNames"),
    PROP_NAMES(String.class, "proposedNames"),
    PROP_NEW_NAMES(String.class, "proposedNewNames"),
    SND_PROP_NAMES(String.class, "secondProposedNames"),
    TRD_PROP_NAMES(String.class, "thirdProposedNames"),

    FILE_NAMESTV(String.class, "fileNamestv"),
    PARENT_DIRTV(String.class, "parentDirtv"),
    GRANDPARENT_DIRTV(String.class, "grandParentDirtv"),

    DIRTY_DATE(Long.class, "dirtyDate"),
    ;

    private Class type;
    private String solrName;

    FileGroupsFieldType(Class type, String solrName) {
        this.type = type;
        this.solrName = solrName;
    }

    @Override
    public Class getType() {
        return type;
    }

    @Override
    public String getSolrName() {
        return solrName;
    }

    @Override
    public String filter(Object value){
        return filter(solrName, value);
    }

    @Override
    public String inQuery(Collection<?> col){
        return inQuery(solrName, col);
    }

    @Override
    public String notInQuery(Collection<?> col){
        return notInQuery(solrName, col);
    }

    @Override
    public String notFilter(Object value){
        return notFilter(solrName, value);
    }

    @Override
    public String toString() {
        return toString(solrName);
    }

    @Override
    public SolrCoreSchema idField() {
        return ID;
    }
}
