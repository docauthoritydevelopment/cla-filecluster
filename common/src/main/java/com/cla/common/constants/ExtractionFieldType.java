package com.cla.common.constants;

import java.time.LocalDateTime;
import java.util.Collection;

public enum ExtractionFieldType implements SolrCoreSchema{

    ID(Long.class, "id"),
    TYPE(Integer.class, "type"),
    FILE_NAME(String.class, "fileName"),
    PART(String.class, "part"),
    CONTENT(String.class, "content"),
    GROUP_ID(String.class, "groupId"),
    CREATION_DATE(LocalDateTime.class, "creationDate"),
    MODIFICATION_DATE(LocalDateTime.class, "modificationDate"),
    LAST_RECORD_UPDATE(LocalDateTime.class, "last_record_update"),
    LAST_ACCESS_DATE(LocalDateTime.class, "accessDate"),
    JOB_ID(String.class, "jobId"),
    EXTRACTED_ENTITIES(String.class, "extractedEntities"),
    EXTRACTED_PATTERNS(String.class, "extractedPatterns"),
    ALLOW_TOKEN_DOCUMENTS(String.class, "allowTokenDocuments"),
    ALLOW_TOKEN_PARENTS(String.class, "allowTokenParents"),
    ALLOW_TOKEN_SHARES(String.class, "allowTokenShares"),
    DENY_TOKEN_DOCUMENTS(String.class, "denyTokenDocuments"),
    DENY_TOKEN_PARENTS(String.class, "denyTokenParents"),
    DENY_TOKEN_SHARES(String.class, "denyTokenShares");

    private Class type;
    private String solrName;

    ExtractionFieldType(Class type, String solrName) {
        this.type = type;
        this.solrName = solrName;
    }

    @Override
    public Class getType() {
        return type;
    }

    @Override
    public String getSolrName() {
        return solrName;
    }

    @Override
    public String filter(Object value){
        return filter(solrName, value);
    }

    @Override
    public String inQuery(Collection<?> col){
        return inQuery(solrName, col);
    }

    @Override
    public String notInQuery(Collection<?> col){
        return notInQuery(solrName, col);
    }

    @Override
    public String notFilter(Object value){
        return notFilter(solrName, value);
    }

    @Override
    public SolrCoreSchema idField() {
        return ID;
    }

    @Override
    public String toString() {
        return toString(solrName);
    }
}
