package com.cla.common.constants;

import java.time.LocalDateTime;
import java.util.Collection;

public enum CatFileFieldType implements SolrCoreSchema {
    ID(Long.class, "id"),
    FILE_ID(Long.class, "fileId"),
    ACCOUNT_CATEGORIES(String.class, "account_categories"),
    GROUP_CATEGORIES(String.class, "group_categories"),
    USER_CATEGORIES(String.class, "user_categories"),
    TAGS(String.class, "tags"),
    TAGS_2(String.class, "tags2"),
    SCOPED_TAGS(String.class, "scopedTags"),
    MATCHED_PATTERNS_SINGLE(Integer.class, "matchedPatternsSingle"),
    MATCHED_PATTERNS_SINGLE_NAME(Integer.class, "matchedPatternsSingle"),
    MATCHED_PATTERNS_MULTI_NAME(Integer.class, "matchedPatternsMulti"),
    FILE_TYPE_CATEGORY_NAME(Integer.class, "extension"),
    MATCHED_PATTERNS_MULTI(Integer.class, "matchedPatternsMulti"),
    MATCHED_PATTERNS_COUNTS(String.class, "matchedPatternsCounts"),
    BIZLIST_ITEM_ASSOCIATION(String.class, "bizListItems"),
    TAG_TYPE(String.class, "tags"),
    TAG_TYPE_2(String.class, "tags2"),
    TAG_TYPE_MISMATCH(String.class, "tags2"),
    FILE_TYPE_CATEGORY_ID(String.class, "extension"),
    REGULATION_ID(Integer.class, "matchedPatternsSingle"),
    REGULATION_NAME(Integer.class, "matchedPatternsSingle"),
    NO_MAIL_RELATED(String.class, "itemType"),
    EMAIL_PARTS(Long.class, "containerIds"),
    COMPOUND_POLICY_ASSOCIATION(String.class, "tags2"),
    DOC_TYPES(String.class, "docTypes"),
    DOC_TYPE_PARENTS(String.class, "docTypes"),
    DOC_TYPE_PARENTS_NAME(String.class, "activeAssociations"),
    TYPE(Integer.class, "type"),
    ITEM_TYPE(Integer.class, "itemType"),
    BASE_NAME(String.class, "baseName"),
    SORT_NAME(String.class, "sortName"),
    FULLPATH(String.class, "fullPath"),
    FULL_NAME(String.class, "fullName"),
    SIZE(Long.class, "size"),
    SIZE_PARTITION(String.class, "size", Long.class),
    AUTHOR(String.class, "author"),
    COMPANY(String.class, "company"),
    SUBJECT(String.class, "subject"),
    TITLE(String.class, "title"),
    MIME(String.class, "mime"),
    PROCESSING_STATE(String.class, "processing_state"),
    DOC_STORE_ID(Integer.class, "docStoreId"),
    LAST_MODIFIED(LocalDateTime.class, "modificationDate"),
    LAST_ACCESS(LocalDateTime.class, "accessDate"),
    CREATION_DATE(LocalDateTime.class, "creationDate"),
    APP_CREATION_DATE(LocalDateTime.class, "appCreationDate"),
    ANALYSIS_GROUP_ID(String.class, "groupId"),
    USER_GROUP_ID(String.class, "userGroupId"),
    FOLDER_IDS(String.class, "parentFolderIds"),
    FOLDER_ID(Long.class, "folderId"),
    WORKFLOW_SUBTREE_ID(Long.class, "wstId"),
    WORKFLOW_DATA(String.class, "workflowData"),
    GROUP_NAME(String.class, "groupName"),
    OWNER(String.class, "owner"),
    OWNER_F(String.class, "owner_f"),
    EXTRACTED_ENTITIES_IDS(String.class, "extractedEntitiesIds"),
    BIZLIST(String.class, "extractedEntitiesIds"),
    BIZLIST_ITEM_EXTRACTION(String.class, "extractedEntitiesIds"),
    BIZLIST_TYPE(String.class, "extractedEntitiesIds"),
    EXTRACTION_RULE(String.class, "extractedEntitiesIds"),
    BIZLIST_ITEM_ASSOCIATION_TYPE(String.class, "bizListItems"),
    EXTRACTED_TEXT_IDS(String.class, "extractedTextIds"),
    EXTENSION(String.class, "extension"),
    CONTENT_ID(String.class, "contentId"),
    USER_CONTENT_SIGNATURE(String.class, "userContentSignature"),
    ROOT_FOLDER_ID(Long.class, "rootFolderId"),
    LAST_RECORD_UPDATE(LocalDateTime.class, "last_record_update"),
    PACKAGE_TOP_FILE_ID(Long.class, "packageTopFileId"),
    ITEM_COUNT_1(Integer.class, "item_count_1"),
    ITEM_COUNT_2(Integer.class, "item_count_2"),
    ITEM_COUNT_3(Integer.class, "item_count_3"),
    ANALYSIS_HINT(String.class, "analysisHint"),
    ACL_READ(String.class, "acl_read"),
    ACL_WRITE(String.class, "acl_write"),
    ACL_DENIED_READ(String.class, "acl_denied_read"),
    ACL_DENIED_WRITE(String.class, "acl_denied_write"),
    ACL_READ_F(String.class, "acl_read_f"),
    ACL_WRITE_F(String.class, "acl_write_f"),
    ACL_DENIED_READ_F(String.class, "acl_denied_read_f"),
    ACL_DENIED_WRITE_F(String.class, "acl_denied_write_f"),
    EXTRACTED_PATTERNS_IDS(String.class, "extractedPatternsIds"),
    FUNCTIONAL_ROLE_ASSOCIATION(String.class, "dataRoles"),
    FUNCTIONAL_ROLE_ASSOCIATION_NAME(String.class, "dataRoles"),
    ROOT_FOLDER_NAME_LIKE(String.class, "rootFolderId"),
    FILE_NAME_HASHED(String.class, "fileNameHashed"),
    MEDIA_ENTITY_ID(String.class, "mediaEntityId"),
    INGESTION_STATE(String.class, "ingestionState"),
    GENERATING_APP(String.class, "generatingApp"),
    INITIAL_SCAN_DATE(LocalDateTime.class, "initialScanDate"),
    DELETION_DATE(LocalDateTime.class, "deletionDate"),
    CREATED_ON_DB(LocalDateTime.class, "createdOnDB"),
    UPDATED_ON_DB(LocalDateTime.class, "updatedOnDB"),
    KEYWORDS(String.class, "keywords"),
    KEYWORDS_ORIGINAL(String.class, "keywordsOriginal"),
    ACL_SIGNATURE(String.class, "aclSignature"),
    ACL_INHERITANCE_TYPE(String.class, "aclInheritanceType"),
    DELETED(Boolean.class, "deleted"),
    LAST_METADATA_CNG_DATE(Long.class, "lastMetadataChangeDate"),
    SENDER_NAME(String.class, "senderName"),
    SENDER_ADDRESS(String.class, "senderAddress"),
    SENDER_FULL_ADDRESS(String.class, "senderFullAddress"),
    RECIPIENTS_NAMES(String.class, "recipientsNames"),
    RECIPIENTS_ADDRESSES(String.class, "recipientsAddresses"),
    RECIPIENTS_FULL_ADDRESSES(String.class, "recipientsFullAddresses"),
    SENT_DATE(LocalDateTime.class, "sentDate"),
    CONTENT_CHANGE_DATE(Long.class, "contentChangeDate"),
    INGEST_DATE(Long.class, "ingestDate"),
    ANALYSIS_DATE(Long.class, "analysisDate"),
    LAST_ASSOC_CNG_DATE(Long.class, "lastAssociationChangeDate"),
    ENCRYPTION(Integer.class, "encryption"),
    INGEST_ERR_TYPE(Integer.class, "ingestionErrorType"),
    SEARCH_ACL_READ(String.class, "search_acl_read"),
    SEARCH_ACL_DENIED_READ(String.class, "search_acl_denied_read"),
    MEDIA_TYPE(Integer.class, "mediaType"),
    SENDER_DOMAIN(String.class, "senderDomain"),
    RECIPIENTS_DOMAINS(String.class, "recipientsDomains"),
    NUM_OF_ATTACHMENTS(Integer.class, "numOfAttachments"),
    CONTAINER_IDS(String.class, "containerIds"),
    MAILBOX_UPN(String.class, "mailboxUpn"),
    ITEM_SUBJECT(String.class, "itemSubject"),
    DEPARTMENT_ID(String.class, "department"),
    DEPARTMENT_PARENTS(String.class, "department"),
    DEPARTMENT_PARENTS_NAME(String.class, "activeAssociations"),
    FILE_ASSOCIATIONS(String.class, "associations"),
    CURRENT_RUN_ID(Long.class, "currentRunId"),
    TASK_STATE(String.class, "taskState"),
    TASK_STATE_DATE(Long.class, "taskStateDate"),
    RETRIES(Integer.class, "retries"),
    TASK_PARAM(String.class, "taskParameters"),
	ACTIONS_EXPECTED(String.class, "actionsExpected"),
	DA_LABELS(Long.class, "daLabels"),
	EXTERNAL_METADATA(String.class, "externalMetadata"),
    GENERAL_METADATA(String.class, "generalMetadata"),
	RAW_METADATA(String.class, "rawMetadata"),
    DA_METADATA(String.class, "daMetadata"),
    DA_METADATA_TYPE(String.class, "daMetadataType"),
    LABEL_PROCESS_DATE(Long.class, "labelProcessDate"),
    ACTION_CONFIRMATION(String.class, "actionConfirmation"),
    OPEN_ACCESS_RIGHTS(String.class, "search_acl_read"),
    ACTIVE_ASSOCIATIONS(String.class, "activeAssociations"),
    SHARED_PERMISSION(Long.class, "rootFolderId"),
    FULL_NAME_SEARCH(String.class, "fullNameSearch"),
    FOLDER_NAME(String.class, "folderName"),
    EXPIRED_USER(String.class, "acl_read"),
    TAG_TYPE_ASSOCIATION(String.class, "activeAssociations"),
    TAG_TYPE_NAME(String.class, "activeAssociations"),
    TAG_NAME(String.class, "activeAssociations"),
    UN_GROUPED(String.class, "userGroupId"),
    GENERIC_STATE(String.class, "processing_state"),
    SPECIFIC_STATE(String.class, "processing_state"),
    FILE_TAG_TYPE_ASSOC(String.class, "associations"),
    FILE_TAG_ASSOC(String.class, "associations"),
    UPDATE_DATE(Long.class, "updateDate"),
    ;

    private Class type;
    private String solrName;
    private Class fieldType;

    CatFileFieldType(Class type, String solrName) {
        this.type = type;
        this.fieldType = type;
        this.solrName = solrName;
    }

    CatFileFieldType(Class type, String solrName, Class fieldType) {
        this.type = type;
        this.solrName = solrName;
        this.fieldType = fieldType;
    }

    @Override
    public Class getType() {
        return type;
    }

    @Override
    public String getSolrName() {
        return solrName;
    }

    @Override
    public Class getFieldType() {
        return fieldType;
    }

    @Override
    public String filter(Object value) {
        return filter(solrName, value);
    }

    @Override
    public String inQuery(Collection<?> col) {
        return inQuery(solrName, col);
    }

    @Override
    public String notInQuery(Collection<?> col){
        return notInQuery(solrName, col);
    }

    @Override
    public String notFilter(Object value){
        return notFilter(solrName, value);
    }

    @Override
    public SolrCoreSchema idField() {
        return ID;
    }

    @Override
    public String toString() {
        return toString(solrName);
    }

}
