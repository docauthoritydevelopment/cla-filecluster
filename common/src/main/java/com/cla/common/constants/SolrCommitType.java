package com.cla.common.constants;

public enum SolrCommitType {

    HARD, SOFT, SOFT_NOWAIT, NONE
}
