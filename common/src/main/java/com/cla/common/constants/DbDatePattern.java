package com.cla.common.constants;

import java.text.SimpleDateFormat;

public class DbDatePattern {

   public  static SimpleDateFormat CREATED_ON_DB_PATTERN = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
}
