package com.cla.common.constants;

public final class PerformanceMessages {

    public static final String PERFORMANCE_MEASURE_PATTERN = ">>>> KPI(method: {}, operation: {}, line: {} ,time: {} ms)";
}
