package com.cla.common.constants;

import java.util.Collection;

/**
 * Created by: yael
 * Created on: 7/1/2018
 */
public enum BizListFieldType implements SolrCoreSchema {
    ID(Long.class, "id"),
    FILE_ID(Long.class, "fileId"),
    BIZ_LIST_ID(Long.class, "bizListId"),
    BIZ_LIST_ITEM_TYPE(Integer.class, "bizListItemType"),
    AGG_MIN_COUNT(Integer.class, "aggMinCount"),
    BIZ_LIST_ITEM_SID(String.class, "bizListItemSid"),
    RULE_NAME(String.class, "ruleName"),
    DIRTY(Boolean.class, "dirty"),
    COUNTER(Long.class, "counter")
    ;

    public void setSolrName(String solrName) {
        this.solrName = solrName;
    }

    private Class type;
    private String solrName;

    BizListFieldType(Class type, String solrName) {
        this.type = type;
        this.solrName = solrName;
    }

    @Override
    public Class getType() {
        return type;
    }

    @Override
    public String getSolrName() {
        return solrName;
    }

    @Override
    public String filter(Object value){
        return filter(solrName, value);
    }

    @Override
    public String inQuery(Collection<?> col){
        return inQuery(solrName, col);
    }

    @Override
    public String toString() {
        return toString(solrName);
    }

    @Override
    public SolrCoreSchema idField() {
        return BizListFieldType.ID;
    }

    @Override
    public String notInQuery(Collection<?> col){
        return notInQuery(solrName, col);
    }

    @Override
    public String notFilter(Object value){
        return notFilter(solrName, value);
    }
}
