package com.cla.common.predicate;

import java.util.Collection;
import java.util.Map;

public class KeyValuePredicate {

    private CompareOperation compareOperation;
    private String key;
    private Object desiredValue;

    public KeyValuePredicate() {
    }

    public KeyValuePredicate(String key, CompareOperation compareOperation, Object desiredValue) {
        this.key = key;
        this.compareOperation = compareOperation;
        this.desiredValue = desiredValue;
    }

    public boolean accept(Map<String, Object> data) {
        Object value = data.get(key);
        switch (compareOperation) {
            case EQUALS:
                return value.equals(desiredValue);
            case LOWER:
                return isLower(value, desiredValue);
            case LOWER_EQUALS:
                return value.equals(desiredValue) || isLower(value, desiredValue);
            case GREATER:
                return isGreater(value, desiredValue);
            case GREATER_EQUALS:
                return value.equals(desiredValue) || isGreater(value, desiredValue);
            case CONTAINS:
                return contains(value, desiredValue);

        }
        return false;
    }

    private boolean contains(Object value, Object desiredValue) {
        return value instanceof Collection && desiredValue instanceof Collection ? ((Collection) value).containsAll((Collection<?>) desiredValue) : false;
    }

    private boolean isGreater(Object value, Object desiredValue) {
        return value instanceof Comparable ? ((Comparable) value).compareTo(desiredValue) > 0 : false;
    }

    private boolean isLower(Object value, Object desiredValue) {
        return value instanceof Comparable ? ((Comparable) value).compareTo(desiredValue) < 0 : false;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public CompareOperation getCompareOperation() {
        return compareOperation;
    }

    public void setCompareOperation(CompareOperation compareOperation) {
        this.compareOperation = compareOperation;
    }

    public Object getDesiredValue() {
        return desiredValue;
    }

    public void setDesiredValue(Object desiredValue) {
        this.desiredValue = desiredValue;
    }

    @Override
    public String toString() {
        return "KeyValuePredicate{" +
                " key='" + key + '\'' +
                ", compareOperation=" + compareOperation +
                ", desiredValue=" + desiredValue +
                '}';
    }
}
