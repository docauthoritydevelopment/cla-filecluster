package com.cla.common.predicate;

public enum CompareOperation {

    EQUALS, LOWER, LOWER_EQUALS, GREATER, GREATER_EQUALS, CONTAINS
}
