package com.cla.common.connectors;

import com.cla.common.domain.dto.messages.ContentMetadataRequestMessage;
import com.cla.common.domain.dto.messages.ContentMetadataResponseMessage;

/**
 *
 * Created by uri on 07-Mar-17.
 */
public interface ExistingContentConnector {

    ContentMetadataResponseMessage checkForExistingContent(ContentMetadataRequestMessage mdRequest);
}
