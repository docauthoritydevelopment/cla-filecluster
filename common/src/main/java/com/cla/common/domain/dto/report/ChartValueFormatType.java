package com.cla.common.domain.dto.report;

/**
 * Created by uri on 09/11/2015.
 */
public enum ChartValueFormatType {
    NUMERIC,PERCENTAGE
}
