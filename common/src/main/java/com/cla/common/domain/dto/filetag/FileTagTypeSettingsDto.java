package com.cla.common.domain.dto.filetag;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class FileTagTypeSettingsDto {

    private FileTagTypeDto fileTagType;
    private List<FileTagDto> fileTags;

    public void setFileTagType(FileTagTypeDto fileTagType) {
        this.fileTagType = fileTagType;
    }

    public FileTagTypeDto getFileTagType() {
        return fileTagType;
    }

    public List<FileTagDto> getFileTags() {
        return fileTags;
    }

    public void setFileTags(List<FileTagDto> fileTags) {
        this.fileTags = fileTags;
    }
}
