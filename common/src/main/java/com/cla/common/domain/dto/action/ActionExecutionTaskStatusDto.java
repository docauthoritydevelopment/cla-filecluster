package com.cla.common.domain.dto.action;

import com.cla.common.domain.dto.crawler.RunStatus;
import com.cla.common.domain.dto.kendo.FilterDescriptor;

import java.io.Serializable;
import java.util.Properties;

/**
 * Created by uri on 16/02/2016.
 */
public class ActionExecutionTaskStatusDto implements Serializable {

    private long actionTriggerId;

    private RunStatus status;

    private long progressMark;

    private long progressMaxMark;

    private long errorsCount;

    private long triggerTime;

    private ActionTriggerType type;

    private long userId;

    private String userName;

    private String scriptFolders;

    private String logPath;

    private String firstError;

    private Properties actionParams;

    private FilterDescriptor filterDescriptor;

    private String actionId;

    private String entityType;

    private String entityId;

    private int page = 0;

    public long getActionTriggerId() {
        return actionTriggerId;
    }

    public void setActionTriggerId(long actionTriggerId) {
        this.actionTriggerId = actionTriggerId;
    }

    public RunStatus getStatus() {
        return status;
    }

    public void setStatus(RunStatus status) {
        this.status = status;
    }

    public long getTriggerTime() {
        return triggerTime;
    }

    public void setTriggerTime(long triggerTime) {
        this.triggerTime = triggerTime;
    }

    public ActionTriggerType getType() {
        return type;
    }

    public void setType(ActionTriggerType type) {
        this.type = type;
    }

    public long getProgressMark() {
        return progressMark;
    }

    public void setProgressMark(long progressMark) {
        this.progressMark = progressMark;
    }

    public long getProgressMaxMark() {
        return progressMaxMark;
    }

    public void setProgressMaxMark(long progressMaxMark) {
        this.progressMaxMark = progressMaxMark;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getScriptFolders() {
        return scriptFolders;
    }

    public void setScriptFolders(String scriptFolders) {
        this.scriptFolders = scriptFolders;
    }

    public String getLogPath() {
        return logPath;
    }

    public void setLogPath(String logPath) {
        this.logPath = logPath;
    }

    public long getErrorsCount() {
        return errorsCount;
    }

    public void setErrorsCount(long errorsCount) {
        this.errorsCount = errorsCount;
    }

    public String getFirstError() {
        return firstError;
    }

    public void setFirstError(String firstError) {
        this.firstError = firstError;
    }

    public void addError(String errorMessage) {
        if (firstError == null) {
            firstError = errorMessage;
        }
        errorsCount++;
    }

    public Properties getActionParams() {
        return actionParams;
    }

    public void setActionParams(Properties actionParams) {
        this.actionParams = actionParams;
    }

    public FilterDescriptor getFilterDescriptor() {
        return filterDescriptor;
    }

    public void setFilterDescriptor(FilterDescriptor filterDescriptor) {
        this.filterDescriptor = filterDescriptor;
    }

    public String getActionId() {
        return actionId;
    }

    public void setActionId(String actionId) {
        this.actionId = actionId;
    }

    public String getEntityType() {
        return entityType;
    }

    public void setEntityType(String entityType) {
        this.entityType = entityType;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }
}
