package com.cla.common.domain.dto.messages;

import com.cla.connector.domain.dto.error.ProcessingErrorType;

/**
 * Bean representing ingestion error, used for ingestion result message
 * @see ExcelIngestResult
 *
 * Created by vladi on 3/7/2017.
 */
public class IngestErrorInfo {

    private ProcessingErrorType type = ProcessingErrorType.ERR_NONE;
    private String text;
    private String exceptionText;
    private String exceptionType;

    public ProcessingErrorType getType() {
        return type;
    }

    public void setType(ProcessingErrorType type) {
        this.type = type;
    }

    public boolean isEmpty(){
        return type == null || type == ProcessingErrorType.ERR_NONE;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getExceptionText() {
        return exceptionText;
    }

    public void setExceptionText(String exceptionText) {
        this.exceptionText = exceptionText;
    }

    public String getExceptionType() {
        return exceptionType;
    }

    public void setExceptionType(String exceptionType) {
        this.exceptionType = exceptionType;
    }
}
