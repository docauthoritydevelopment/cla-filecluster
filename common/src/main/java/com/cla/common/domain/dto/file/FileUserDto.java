package com.cla.common.domain.dto.file;

import java.io.Serializable;


@SuppressWarnings("serial")
public class FileUserDto implements Serializable {
    private String user;
//	private final FileUserType userType;

    public FileUserDto() {
    }


    public FileUserDto(String user) {
        this.user = user;
//		this.userType = FileUserType.UserPrincipal;
    }

//	public FileUserDTO(final FileUserType userType, final String user) {
//		this.user = user;
//		this.userType = userType;
//	}

    public String getUser() {
        return user;
    }

//	public FileUserType getUserType() {
//		return userType;
//	}


    public void setUser(String user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "{user=" + user + "}";
//		return "{userType="+userType.name()+" ,user=" + user + "}";
    }

}
