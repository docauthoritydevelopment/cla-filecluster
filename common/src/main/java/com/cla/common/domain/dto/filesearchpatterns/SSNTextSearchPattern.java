package com.cla.common.domain.dto.filesearchpatterns;

import java.util.function.Predicate;



public class SSNTextSearchPattern extends TextSearchPatternImpl {
	
	public static final String SSN_PATTERN = "\\b\\d{3}([\\- ])\\d{2}\\1\\d{4}\\b";		// Must have pattern separator: 'nnn-nn-nnnn' or 'nnn nn nnnn'

	public SSNTextSearchPattern() {}

	public SSNTextSearchPattern(final Integer id, final String name) {
		super(id,name,SSN_PATTERN);
	}
	
	@Override
	public Predicate<String> getPredicate(){
		return (str)->( (!str.startsWith("000")) && (!str.startsWith("9")) && (!str.startsWith("666")
				&& (!str.replaceAll("[\\- ]","").equals("999999999"))));
	}

}
