package com.cla.common.domain.dto.mcf.mcf_api;

import java.io.Serializable;

/**
 * Created by uri on 07/07/2016.
 */
public class McfTransformationConnectionsDtoList implements Serializable {
    McfTransformationConnectionDto[] transformationconnection;

    public McfTransformationConnectionsDtoList() {
    }

    public McfTransformationConnectionsDtoList(McfTransformationConnectionDto dto) {
        transformationconnection = new McfTransformationConnectionDto[1];
        transformationconnection[0] = dto;
    }

    public McfTransformationConnectionDto[] getTransformationconnection() {
        return transformationconnection;
    }

    public void setTransformationconnection(McfTransformationConnectionDto[] transformationconnection) {
        this.transformationconnection = transformationconnection;
    }
}
