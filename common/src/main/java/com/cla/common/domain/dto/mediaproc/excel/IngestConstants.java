package com.cla.common.domain.dto.mediaproc.excel;

import java.util.regex.Pattern;

/**
 *
 * Created by vladi on 3/7/2017.
 */
public interface IngestConstants {


    Pattern ALL_DIGITS_PATTERN = Pattern.compile("[0-9\\-_/,\\.\\(\\)]*");
    Pattern ANY_DIGIT_PATTERN = Pattern.compile("[0-9]");

    String UTF8_FOUR_BYTES = "[^\\u0000-\\uFFFF]";
    String TITLE_KEY_FORMAT_STRING = "%s=%02d=%03d";

    String PROPOSED_TITLE_KEY_SEPARATOR = ":";

    // Excel ------------------------------------------------------------------
    int MIN_TITLE_LENGTH = 10;
    int MAX_TITLE_LENGTH = 80;
    int ROW_COUNT_THRESHOLD = 15;
    int COL_COUNT_THRESHOLD = 15;
    short MIN_TITLE_FONT_SIZE_IN_POINTS = 10;
    short MIN_SUPER_TITLE_FONT_SIZE = 14;	// Allow search of sub-title
    int SUB_TITLE_RATE_CONSTANT = 5;
    int MAX_STORED_CELL_VALUES = 4000;
    int MAX_CELL_LAYOUT_REPEAT_COUNT = 3;
    int MAX_ROW_LAYOUT_REPEAT_COUNT = 5;
    int MAX_ROW_LAYOUT_COUNT_VALUE = 20;	// set a cap to the number of times each row-layout can be counted
    int MIN_CELLS_IN_SIGNIFICANT_LAYOUT_ROW = 2;
    int MIN_NUMBER_OF_SIGNIFICANT_LAYOUT_ROWS = 1;
    int MIN_NUMBER_OF_DIFFERENT_ROW_LAYOUTS = 2;

    int OUT_LIER_TRIM_MIN_NUM_ITEMS = 10;
    int OUT_LIER_TRIM_DIFF_FACTOR = 5;
    int OUT_LIER_TRIM_MIN_ITEM_COUNT = 10;

    int TEXT_NGRAM_SIZE = 3;
    int TITLE_NGRAM_SIZE = 3;
    int MIN_TEXT_LENGTH_FOR_NG_CALC = 10;

    int SHEET_NAME_RATE_VALUE = 8;

    int MAX_NUM_HEADINGS_IN_PROPOSED_TITLE = 4;
}
