package com.cla.common.domain.dto.schedule;

public enum ScheduledOperationState {
    NEW,
    ENQUEUED,
    IN_PROGRESS,
    FAILED
}
