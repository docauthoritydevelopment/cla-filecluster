package com.cla.common.domain.dto.workflow;

import com.cla.common.domain.dto.kendo.FilterDescriptor;

import java.util.Date;
import java.util.Map;

/**
 * Created by: yael
 * Created on: 2/22/2018
 */
public enum WorkflowField {
    ASSIGNEE("assignee", "Long"),
    ASSIGNEE_WORKGROUP("assignee workgroup", "Long"),
    CONTACT_INFO("contact info", "String"),
    CURRENT_STATE_DUE_DATE("current state due date", "Date"),
    DATE_CLOSED("date closed", "Date"),
    DUE_DATE("due date", "Date"),
    FILTER("filter", "Json"),
    PRIORITY("priority", "String"),
    REQUEST_DETAILS("request details", "Map"),
    STATE("state", "String"),
    TYPE("type", "String"),
    FILE_DATA("workflow file data", "String");

    private String name;
    private String className;

    WorkflowField(String name, String className) {
        this.name = name;
        this.className = className;
    }

    public String getName() {
        return name;
    }

    public String getClassName() {
        return className;
    }
}
