package com.cla.common.domain.dto.messages.control;

/**
 * Created by liora on 19/06/2017.
 */
public class ServerHandShakeRequestPayload extends ControlMessagePayload {

    private Long dataCenterId;
    private boolean isDataCenterRequiresPrefix;
    private boolean active;

    public Long getDataCenterId() {
        return dataCenterId;
    }

    public void setDataCenterId(Long dataCenterId) {
        this.dataCenterId = dataCenterId;
    }

    @Override
    public String toString() {
        return "ServerHandShakeRequestPayload{" +
                " dataCenterId='" + dataCenterId + '\'' +
                " active='" + active + '\'' +
                " isDataCenterRequiresPrefix='" + isDataCenterRequiresPrefix + '\'' +
                '}';
    }

    public boolean isDataCenterRequiresPrefix() {
        return isDataCenterRequiresPrefix;
    }

    public void setDataCenterRequiresPrefix(boolean dataCenterRequiresPrefix) {
        isDataCenterRequiresPrefix = dataCenterRequiresPrefix;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isActive() {
        return active;
    }
}
