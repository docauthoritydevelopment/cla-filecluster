package com.cla.common.domain.dto;

import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.JsonSerislizableVisible;

import java.util.List;
import java.util.Map;

public class RawDocumentMessageDto implements JsonSerislizableVisible {

    private String url;
    private byte[] content;
    private Map<String, String[]> metadata;
    private Map<String, List<String>> acls;
    private Map<String, List<String>> denyAcls;
    private Map<String, List<String>> shareDenyAcls;
    private Map<String, List<String>> shareAcls;
    private Map<String, List<String>> parentDenyAcls;
    private Map<String, List<String>> parentAcls;

    private String claJobId;
    private boolean isFileFetcher;

    private ClaFilePropertiesDto claFilePropertiesDto;

    public RawDocumentMessageDto() {
    }

    public RawDocumentMessageDto(final String url, final String claJobId, final byte[] content, final Map<String, String[]> metadata,
                              final Map<String, List<String>> acls, final Map<String, List<String>> denyAcls, final boolean isFileFetcher) {
        this.url = url;
        this.claJobId = claJobId;
        this.content = content;
        this.metadata = metadata;
        this.acls = acls;
        this.denyAcls = denyAcls;
        this.isFileFetcher=isFileFetcher;
    }

    public RawDocumentMessageDto(final String url, final byte[] content, final Map<String, String[]> metadata,
                              final Map<String, List<String>> acls, final Map<String, List<String>> denyAcls) {
        this.url = url;
        this.content = content;
        this.metadata = metadata;
        this.acls = acls;
        this.denyAcls = denyAcls;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(final String url) {
        this.url = url;
    }

    public byte[] getContent() {
        return content;
    }

    public void setContent(final byte[] content) {
        this.content = content;
    }

    public Map<String, String[]> getMetadata() {
        return metadata;
    }

    public void setMetadata(final Map<String, String[]> metadata) {
        this.metadata = metadata;
    }

    public Map<String, List<String>> getAcls() {
        return acls;
    }

    public void setAcls(final Map<String, List<String>> acls) {
        this.acls = acls;
    }

    public Map<String, List<String>> getDenyAcls() {
        return denyAcls;
    }

    public void setDenyAcls(final Map<String, List<String>> denyAcls) {
        this.denyAcls = denyAcls;
    }

    public String getClaJobId() {
        return claJobId;
    }

    public void setClaJobId(final String claJobId) {
        this.claJobId = claJobId;
    }

    public boolean isFileFetcher() {
        return isFileFetcher;
    }

    public void setFileFetcher(final boolean isFileFetcher) {
        this.isFileFetcher = isFileFetcher;
    }


    @Override
    public String toString() {
        return "RawDocumentMessageDto{" +
                "url='" + url + '\'' +
                ", metadata=" + toString(metadata) +
                '}';
    }

    private String toString(Map<String, String[]> metadata) {
        StringBuilder sb = new StringBuilder();
        if (metadata != null) {
            for (Map.Entry<String, String[]> stringEntry : metadata.entrySet()) {
                sb.append(" ").append(stringEntry.getKey()).append(":");
                for (String s : stringEntry.getValue()) {
                    sb.append(" ").append(s);
                }
            }
        }
        return sb.toString();
    }

    public ClaFilePropertiesDto getClaFilePropertiesDto() {
        return claFilePropertiesDto;
    }

    public void setClaFilePropertiesDto(ClaFilePropertiesDto claFilePropertiesDto) {
        this.claFilePropertiesDto = claFilePropertiesDto;
    }
}
