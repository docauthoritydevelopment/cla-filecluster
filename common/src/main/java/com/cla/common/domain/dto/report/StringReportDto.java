package com.cla.common.domain.dto.report;

import java.io.Serializable;

/**
 * Created by uri on 07/10/2015.
 */
public class StringReportDto implements Serializable {
    String name;
    String description;
    String value;

    public StringReportDto() {
    }

    public StringReportDto(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
