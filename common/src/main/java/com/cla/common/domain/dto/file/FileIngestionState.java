package com.cla.common.domain.dto.file;

public enum FileIngestionState {
    Pending,
    Ok,
    Encrypted,
    Error,
    BadFormat
}
