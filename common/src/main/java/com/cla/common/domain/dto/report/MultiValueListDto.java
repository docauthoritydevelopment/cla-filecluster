package com.cla.common.domain.dto.report;

import java.util.List;

/**
 * Created by uri on 11/10/2015.
 */
public class MultiValueListDto {

    List<CounterReportDto> data;

    private String name;

    private MultiValueListType type;

    public MultiValueListDto() {
    }

    public MultiValueListDto(String name, List<CounterReportDto> data, MultiValueListType type) {
        this.data = data;
        this.name = name;
        this.type = type;
    }

    public List<CounterReportDto> getData() {
        return data;
    }

    public void setData(List<CounterReportDto> data) {
        this.data = data;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public MultiValueListType getType() {
        return type;
    }
}
