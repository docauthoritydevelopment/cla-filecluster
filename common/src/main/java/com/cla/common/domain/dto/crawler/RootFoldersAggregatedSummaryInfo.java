package com.cla.common.domain.dto.crawler;

public class RootFoldersAggregatedSummaryInfo {

    private Long totalRootFoldersCount;
    private Long failedRootFoldersCount;
    private Long newRootFoldersCount;
    private Long scannedRootFoldersCount;
    private Long runningRootFoldersCount;
    private Long pausedRootFoldersCount;
    private Long suspendedRootFoldersCount;
    private Long stoppedRootFoldersCount;

    public Long getTotalRootFoldersCount() {
        return totalRootFoldersCount;
    }

    public void setTotalRootFoldersCount(Long totalRootFoldersCount) {
        this.totalRootFoldersCount = totalRootFoldersCount;
    }

    public Long getFailedRootFoldersCount() {
        return failedRootFoldersCount;
    }

    public void setFailedRootFoldersCount(Long failedRootFoldersCount) {
        this.failedRootFoldersCount = failedRootFoldersCount;
    }

    public Long getNewRootFoldersCount() {
        return newRootFoldersCount;
    }

    public void setNewRootFoldersCount(Long newRootFoldersCount) {
        this.newRootFoldersCount = newRootFoldersCount;
    }

    public Long getScannedRootFoldersCount() {
        return scannedRootFoldersCount;
    }

    public void setScannedRootFoldersCount(Long scannedRootFoldersCount) {
        this.scannedRootFoldersCount = scannedRootFoldersCount;
    }

    public Long getRunningRootFoldersCount() {
        return runningRootFoldersCount;
    }

    public void setRunningRootFoldersCount(Long runningRootFoldersCount) {
        this.runningRootFoldersCount = runningRootFoldersCount;
    }

    public Long getPausedRootFoldersCount() {
        return pausedRootFoldersCount;
    }

    public void setPausedRootFoldersCount(Long pausedRootFoldersCount) {
        this.pausedRootFoldersCount = pausedRootFoldersCount;
    }

    public Long getSuspendedRootFoldersCount() {
        return suspendedRootFoldersCount;
    }

    public void setSuspendedRootFoldersCount(Long suspendedRootFoldersCount) {
        this.suspendedRootFoldersCount = suspendedRootFoldersCount;
    }

    public Long getStoppedRootFoldersCount() {
        return stoppedRootFoldersCount;
    }

    public void setStoppedRootFoldersCount(Long stoppedRootFoldersCount) {
        this.stoppedRootFoldersCount = stoppedRootFoldersCount;
    }

    @Override
    public String toString() {
        return "RootFoldersAggregatedSummaryInfo{" +
                "totalRootFoldersCount=" + totalRootFoldersCount +
                ", failedRootFoldersCount=" + failedRootFoldersCount +
                ", newRootFoldersCount=" + newRootFoldersCount +
                ", scannedRootFoldersCount=" + scannedRootFoldersCount +
                ", runningRootFoldersCount=" + runningRootFoldersCount +
                ", pausedRootFoldersCount=" + pausedRootFoldersCount +
                ", suspendedRootFoldersCount=" + suspendedRootFoldersCount +
                ", stoppedRootFoldersCount=" + stoppedRootFoldersCount +
                '}';
    }
}
