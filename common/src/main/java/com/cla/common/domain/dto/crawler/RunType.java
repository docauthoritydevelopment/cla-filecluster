package com.cla.common.domain.dto.crawler;

public enum RunType {
    ROOT_FOLDER,
    BIZ_LIST,
    SCHEDULE_GROUP,
    LABELING
}
