package com.cla.common.domain.dto.group;

import java.io.Serializable;
import java.util.Set;

/**
 * Created by uri on 28/06/2016.
 */
public class GroupDetailsDto implements Serializable {

    Set<GroupDto> childrenGroups;

    Set<GroupDto> siblingGroups;

    private GroupDto groupDto;

    public Set<GroupDto> getChildrenGroups() {
        return childrenGroups;
    }

    public void setChildrenGroups(Set<GroupDto> childrenGroups) {
        this.childrenGroups = childrenGroups;
    }

    public GroupDto getGroupDto() {
        return groupDto;
    }

    public void setGroupDto(GroupDto groupDto) {
        this.groupDto = groupDto;
    }

    public Set<GroupDto> getSiblingGroups() {
        return siblingGroups;
    }

    public void setSiblingGroups(Set<GroupDto> siblingGroups) {
        this.siblingGroups = siblingGroups;
    }

    @Override
    public String toString() {
        return "GroupDetailsDto{" +
                "childrenGroups=" + childrenGroups +
                ", siblingGroups=" + siblingGroups +
                ", groupDto=" + groupDto +
                '}';
    }
}
