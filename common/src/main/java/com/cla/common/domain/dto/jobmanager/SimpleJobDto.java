package com.cla.common.domain.dto.jobmanager;

import com.cla.common.domain.dto.crawler.JobCompletionStatus;

public class SimpleJobDto {

    private long id;

    private long runContext;

    private long itemId;

    private String strItemId;

    private String name;

    private JobType type = JobType.UNKNOWN;

    private JobState state = JobState.NEW;

    private JobState resumeState;             // state of the job before it was paused

    private Long stateUpdateTime = 0L;

    private boolean acceptingTasks = true;

    private JobCompletionStatus completionStatus;

    // when the job status was changed to IN_PROGRESS
    private long startRunTime = 0;

    // how many total milliseconds the job spent in paused mode
    private long pauseDuration = 0;

    // when the job was paused or stopped last time
    private long lastStopTime = 0;

    // total original number of tasks
    private int estimatedTaskCount = 0;

    // how many failures (files/tasks/etc..)
    private int failedCount = 0;

    // how many tasks were done last time we checked
    private int currentTaskCount = 0;

    // when the latest done tasks count was taken
    private long lastTaskCountTime = 0;

    // initiation context for auditing, system analysis. e.g. auto, manual + user-name, etc.
    private String initiationDetails;

    private String jobStateString;

    private long totalTaskRetriesCount;

    private Long partitionId;

    private int scannedMeaningfulFilesCount = 0;

    // Constructors -----------------------------------------------------
    public SimpleJobDto() {
    }

    public SimpleJobDto(long runId) {
        setRunContext(runId);
    }


    // Getters / Setters -------------------------------------------------

    public long getId() {
        return id;
    }

    public long getTotalTaskRetriesCount() {
        return totalTaskRetriesCount;
    }

    public void setTotalTaskRetriesCount(long totalTaskRetriesCount) {
        this.totalTaskRetriesCount = totalTaskRetriesCount;
    }

    public void setId(long id) {
        this.id = id;
    }

    public JobType getType() {
        return type;
    }

    public void setType(JobType type) {
        this.type = type;
    }

    public JobState getState() {
        return state;
    }

    public void setState(JobState state) {
        this.state = state;
    }

    public JobState getResumeState() {
        return resumeState;
    }

    public void setResumeState(JobState resumeState) {
        this.resumeState = resumeState;
    }

    public Long getStateUpdateTime() {
        return stateUpdateTime;
    }

    public void setStateUpdateTime(Long stateUpdateTime) {
        this.stateUpdateTime = stateUpdateTime;
    }

    public boolean isAcceptingTasks() {
        return acceptingTasks;
    }

    public void setAcceptingTasks(boolean acceptingTasks) {
        this.acceptingTasks = acceptingTasks;
    }

    public JobCompletionStatus getCompletionStatus() {
        return completionStatus;
    }

    public void setCompletionStatus(JobCompletionStatus completionStatus) {
        this.completionStatus = completionStatus;
    }

    public void setItemId(long itemId) {
        this.itemId = itemId;
    }

    public void setStrItemId(String strItemId) {
        this.strItemId = strItemId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setInitiationDetails(String initiationDetails) {
        this.initiationDetails = initiationDetails;
    }

    public long getItemId() {
        return itemId;
    }

    public String getStrItemId() {
        return strItemId;
    }

    public String getName() {
        return name;
    }

    public String getInitiationDetails() {
        return initiationDetails;
    }

    public int getEstimatedTaskCount() {
        return estimatedTaskCount;
    }

    public void setEstimatedTaskCount(int estimatedTaskCount) {
        this.estimatedTaskCount = estimatedTaskCount;
    }

    public int getCurrentTaskCount() {
        return currentTaskCount;
    }

    public void setCurrentTaskCount(int currentTaskCount) {
        this.currentTaskCount = currentTaskCount;
    }

    public long getLastTaskCountTime() {
        return lastTaskCountTime;
    }

    public void setLastTaskCountTime(long lastTaskCountTime) {
        this.lastTaskCountTime = lastTaskCountTime;
    }

    public long getStartRunTime() {
        return startRunTime;
    }

    public void setStartRunTime(long startRunTime) {
        this.startRunTime = startRunTime;
    }

    public long getPauseDuration() {
        return pauseDuration;
    }

    public void setPauseDuration(long pauseDuration) {
        this.pauseDuration = pauseDuration;
    }

    public long getLastStopTime() {
        return lastStopTime;
    }

    public void setLastStopTime(long lastStopTime) {
        this.lastStopTime = lastStopTime;
    }

    public String getJobStateString() {
        return jobStateString;
    }

    public void setJobStateString(String jobStateString) {
        this.jobStateString = jobStateString;
    }

    public void setRunContext(long runContext) {
        this.runContext = runContext;
    }

    public long getRunContext() {
        return runContext;
    }

    public int getFailedCount() {
        return failedCount;
    }

    public void setFailedCount(int failedCount) {
        this.failedCount = failedCount;
    }

    public Long getPartitionId() {
        return partitionId;
    }

    public void setPartitionId(long partitionId) {
        this.partitionId = partitionId;
    }

    public int getScannedMeaningfulFilesCount() {
        return scannedMeaningfulFilesCount;
    }

    public void setScannedMeaningfulFilesCount(int scannedMeaningfulFilesCount) {
        this.scannedMeaningfulFilesCount = scannedMeaningfulFilesCount;
    }

    @Override
    public String toString() {
        return "SimpleJobDto{" +
                "runContext=" + runContext +
                ", Id=" + id+
                ", type='" + type + '\'' +
                ", state='" + state + '\'' +
                ", completionStatus='" + completionStatus + '\'' +
                ", itemId=" + itemId +
                ", name='" + name + '\'' +
                ", strItemId='" + strItemId + '\'' +
                ", startRunTime=" + startRunTime +
                ", pauseDuration=" + pauseDuration +
                ", lastStopTime=" + lastStopTime +
                ", estimatedTaskCount=" + estimatedTaskCount +
                ", currentTaskCount=" + currentTaskCount +
                ", lastTaskCountTime=" + lastTaskCountTime +
                ", failedCount=" + failedCount +
                ", initiationDetails='" + initiationDetails + '\'' +
                ", jobStateString='" + jobStateString + '\'' +
                ", totalTaskRetriesCount='" + totalTaskRetriesCount + '\'' +
                '}';
    }


}
