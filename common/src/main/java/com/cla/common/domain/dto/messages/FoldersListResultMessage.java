package com.cla.common.domain.dto.messages;

import com.cla.connector.domain.dto.file.ServerResourceDto;

import java.util.List;

public class FoldersListResultMessage extends FileContentResultMessage {
	private String requestId;
	private List<ServerResourceDto> foldersList;

	public FoldersListResultMessage(List<ServerResourceDto> foldersList, String requestId) {
		super(null);
		this.foldersList = foldersList;
		this.setPayloadType(MessagePayloadType.LIST_FOLDERS_RESPONSE);
		this.requestId = requestId;
	}

	public FoldersListResultMessage() {
		super(null);
	}

	public FoldersListResultMessage setFoldersList(List<ServerResourceDto> foldersList) {
		this.foldersList = foldersList;
		return this;
	}

	public List<ServerResourceDto> getFoldersList() {
		return foldersList;
	}

	public String getRequestId() {
		return requestId;
	}

	public FoldersListResultMessage setRequestId(String requestId) {
		this.requestId = requestId;
		return this;
	}
}
