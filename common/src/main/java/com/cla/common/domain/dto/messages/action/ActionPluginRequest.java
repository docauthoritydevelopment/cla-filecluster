package com.cla.common.domain.dto.messages.action;

/**
 * a request to perform plugin action on
 * Created by: yael
 * Created on: 1/10/2018
 */
public class ActionPluginRequest {
    private PluginInput input;
    private ActionPluginConfig config;

    public ActionPluginRequest(PluginInput input, ActionPluginConfig config) {
        this.input = input;
        this.config = config;
    }

    public PluginInput getInput() {
        return input;
    }

    public ActionPluginConfig getConfig() {
        return config;
    }

    @Override
    public String toString() {
        return "ActionPluginRequest{" +
                "input=" + input +
                ", config=" + config +
                '}';
    }
}
