package com.cla.common.domain.dto.security;

/**
 * Created by Itai Marko.
 */
public enum LdapServerDialect {
    ACTIVE_DIRECTORY("Active directory"),
    ACTIVE_DIRECTORY_LEGACY("Active directory legacy"),
    ApacheDS("ApacheDS"),
    PROPERTY_BASED("property based");

    private String name;

    LdapServerDialect(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
