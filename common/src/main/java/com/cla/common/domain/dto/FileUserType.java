package com.cla.common.domain.dto;

import com.fasterxml.jackson.annotation.JsonValue;

import java.util.HashMap;
import java.util.Map;

public enum FileUserType {
	UserPrincipal(1, "user-principal")
	,OTHER(99, "other")
	;
	
	private static final Map<Integer, FileUserType> intMapping = new HashMap<Integer, FileUserType>();
	private final int value;

    private final String name;

//	FileType(final int value) {
//		this(value, "_"+value);
//	}
//
	FileUserType(final int value, final String name) {
		this.value = value;
		this.name = name;
	}

	static {
		for (final FileUserType t : FileUserType.values()) {
			intMapping.put(t.value, t);
		}
	}

	public int toInt() {
		return value;
	}

	@Override
    @JsonValue
	public String toString() {
		return name;
	}

	public static FileUserType valueOf(final int value) {
		final FileUserType mt = FileUserType.intMapping.get(value);
		if (mt == null) {
			throw new RuntimeException("Invalid value: " + value);
		}
		return mt;
	}
}
