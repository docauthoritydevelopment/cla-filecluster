package com.cla.common.domain.dto.report;

import java.io.Serializable;

/**
 * Created by uri on 31/07/2016.
 */
public class NamedIdentifiableDto implements Serializable {

    public NamedIdentifiableDto() {
    }

    public NamedIdentifiableDto(String id, String name) {
        this.id = id;
        this.name = name;
    }

    String id;

    String name;

    public NamedIdentifiableDto(long id, String name) {
        this.id = String.valueOf(id);
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
