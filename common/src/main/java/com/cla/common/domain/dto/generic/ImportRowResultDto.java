package com.cla.common.domain.dto.generic;



import com.cla.common.domain.dto.RowDTO;

import java.io.Serializable;

/**
 * Created by liora on 21/03/2017.
 */
public class ImportRowResultDto implements Serializable {

    private RowDTO rowDto;
    private String message;


    private Boolean importFailed;

    public RowDTO getRowDto() {
        return rowDto;
    }

    public void setRowDto(RowDTO rowDto) {
        this.rowDto = rowDto;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    public Boolean getImportFailed() {
        return importFailed;
    }

    public void setImportFailed(Boolean importFailed) {
        this.importFailed = importFailed;
    }

    @Override
    public String toString() {
        return "ImportRowResultDto{" +
                "rowDto=" + rowDto +
                ", message='" + message + '\'' +
                ", importFailed=" + importFailed +
                '}';
    }
}
