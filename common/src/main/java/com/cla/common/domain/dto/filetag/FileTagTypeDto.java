package com.cla.common.domain.dto.filetag;

import com.cla.common.domain.dto.scope.Scope;
import com.cla.connector.domain.dto.JsonSerislizableVisible;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Created by uri on 29/09/2015.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FileTagTypeDto implements JsonSerislizableVisible {

    private long id;

    private String name;

    private String description;

    private String alternativeName;

    private boolean sensitive;

    private boolean system;

    private boolean hidden;

    private Integer tagItemsCount;

    private Scope scope;

    private boolean singleValueTag;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "FileTagTypeDto{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", altNname='" + alternativeName + '\'' +
                '}';
    }
    public String getAlternativeName() {
        return alternativeName;
    }

    public void setAlternativeName(String alternativeName) {
        this.alternativeName = alternativeName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FileTagTypeDto that = (FileTagTypeDto) o;

        if (id != that.id) return false;
        return name != null ? name.equals(that.name) : that.name == null;

    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    public boolean isSystem() {
        return system;
    }

    public void setSystem(boolean system) {
        this.system = system;
    }

    public boolean isHidden() {
        return hidden;
    }

    public void setHidden(boolean hidden) {
        this.hidden = hidden;
    }

    public Integer getTagItemsCount() {
        return tagItemsCount;
    }

    public void setTagItemsCount(Integer tagItemsCount) {
        this.tagItemsCount = tagItemsCount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isSensitive() {
        return sensitive;
    }

    public void setSensitive(boolean sensitive) {
        this.sensitive = sensitive;
    }

    public Scope getScope() {
        return scope;
    }

    public void setScope(Scope scope) {
        this.scope = scope;
    }

    public boolean isSingleValueTag() {
        return singleValueTag;
    }

    public void setSingleValueTag(boolean singleValueTag) {
        this.singleValueTag = singleValueTag;
    }
}
