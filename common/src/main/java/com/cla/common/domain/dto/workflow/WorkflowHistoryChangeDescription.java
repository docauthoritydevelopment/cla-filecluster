package com.cla.common.domain.dto.workflow;

/**
 * Created by: yael
 * Created on: 2/21/2018
 */
public class WorkflowHistoryChangeDescription {

    private WorkflowField field;
    private String valueBefore;
    private String valueAfter;


    public WorkflowField getField() {
        return field;
    }

    public void setField(WorkflowField field) {
        this.field = field;
    }

    public String getValueBefore() {
        return valueBefore;
    }

    public void setValueBefore(String valueBefore) {
        this.valueBefore = valueBefore;
    }

    public String getValueAfter() {
        return valueAfter;
    }

    public void setValueAfter(String valueAfter) {
        this.valueAfter = valueAfter;
    }

    @Override
    public String toString() {
        return "WorkflowHistoryChangeDescription{" +
                "field='" + field + '\'' +
                ", valueBefore='" + valueBefore + '\'' +
                ", valueAfter='" + valueAfter + '\'' +
                '}';
    }
}
