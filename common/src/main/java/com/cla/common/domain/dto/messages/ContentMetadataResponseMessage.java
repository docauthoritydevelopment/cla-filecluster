package com.cla.common.domain.dto.messages;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * Created by vladi on 2/23/2017.
 */
public class ContentMetadataResponseMessage implements ResponseMessage{

    private Long contentMetadataId = null;
    private boolean metadataExists = false;
    private boolean forceReIngest = false;
    private String requestId = null;
    private String mediaProcessorId;

    public static ContentMetadataResponseMessage create(){
        return new ContentMetadataResponseMessage();
    }

    public Long getContentMetadataId() {
        return contentMetadataId;
    }

    public ContentMetadataResponseMessage() {
    }

    public ContentMetadataResponseMessage(Long contentMetadataId, boolean metadataExists) {

        this.contentMetadataId = contentMetadataId;
        this.metadataExists = metadataExists;
    }

    public ContentMetadataResponseMessage setContentMetadataId(Long contentMetadataId) {
        if (contentMetadataId != null) {
            this.metadataExists = true;
            this.contentMetadataId = contentMetadataId;
        }
        return this;
    }

    public boolean metadataExists() {
        return metadataExists;
    }

    public boolean isForceReIngest() {
        return forceReIngest;
    }

    public void setForceReIngest(boolean forceReIngest) {
        this.forceReIngest = forceReIngest;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getMediaProcessorId() {
        return mediaProcessorId;
    }

    public void setMediaProcessorId(String mediaProcessorId) {
        this.mediaProcessorId = mediaProcessorId;
    }

    @Override
    public String toString() {
        return "ContentMetadataResponseMessage{" +
                "contentMetadataId=" + contentMetadataId +
                ", metadataExists=" + metadataExists +
                ", forceReIngest=" + forceReIngest +
                '}';
    }
}
