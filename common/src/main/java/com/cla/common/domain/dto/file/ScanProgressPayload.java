package com.cla.common.domain.dto.file;

import com.cla.connector.domain.dto.error.ProcessingErrorType;
import com.cla.connector.domain.dto.messages.PhaseMessagePayload;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Itai Marko.
 */
public class ScanProgressPayload implements PhaseMessagePayload {

    private final long itemsScanned;

    @JsonCreator
    public ScanProgressPayload(@JsonProperty("itemsScanned")long itemsScanned) {
        if (itemsScanned < 0) {
            throw new IllegalArgumentException("Got negative itemsScanned in ScanProgressPayload constructor");
        }

        this.itemsScanned = itemsScanned;
    }

    public long getItemsScanned() {
        return itemsScanned;
    }

    @Override
    public ProcessingErrorType calculateProcessErrorType() {
        return ProcessingErrorType.ERR_NONE;
    }
}
