package com.cla.common.domain.dto.doctype;

import java.io.Serializable;

/**
 *
 * Created by uri on 21/02/2016.
 */
public class DocTypeDetailsDto implements Serializable {

    private DocTypeDto docTypeDto;
    private long totalAssociations;
    private long fileAssociations = 0;
    private long groupAssociations;
    private long explicitFolderAssociations;

    public DocTypeDto getDocTypeDto() {
        return docTypeDto;
    }

    public void setDocTypeDto(DocTypeDto docTypeDto) {
        this.docTypeDto = docTypeDto;
    }

    public long getTotalAssociations() {
        return totalAssociations;
    }

    public void setTotalAssociations(long totalAssociations) {
        this.totalAssociations = totalAssociations;
    }

    public long getFileAssociations() {
        return fileAssociations;
    }

    public void setFileAssociations(long fileAssociations) {
        this.fileAssociations = fileAssociations;
    }

    public long getGroupAssociations() {
        return groupAssociations;
    }

    public void setGroupAssociations(long groupAssociations) {
        this.groupAssociations = groupAssociations;
    }

    public long getExplicitFolderAssociations() {
        return explicitFolderAssociations;
    }

    public void setExplicitFolderAssociations(long explicitFolderAssociations) {
        this.explicitFolderAssociations = explicitFolderAssociations;
    }

    public void addToTotalAssociations(Long add) {
        if (add !=null) {
            this.totalAssociations = this.totalAssociations + add;
        }
    }

    public void addToFileAssociations(Long add) {
        if (add != null) {
            this.fileAssociations= this.fileAssociations + add;
        }
    }

    public void addToGroupAssociations(Long add) {
        if (add != null) {
            this.groupAssociations = this.groupAssociations + add;
        }
    }
}
