package com.cla.common.domain.dto.filesearchpatterns;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.util.function.Predicate;
import java.util.regex.Pattern;

/**
 * Created by uri on 23/06/2016.
 *
 * usage:
 * 1) call getCompiledPattern to test and get the relevant text if exists
 * 2) call getPredicate to do additional tests to check if true
 */
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class TextSearchPatternImpl extends TextSearchPatternDto {

    private Pattern compiledPattern;

    public TextSearchPatternImpl() {
    }

    public TextSearchPatternImpl(final Integer id, final String name, final String pattern) {
        this(id, name, pattern, true, true);
    }

    public TextSearchPatternImpl(final Integer id, final String name, final String pattern, final Boolean subCategoryActive) {
        this(id, name, pattern, true, subCategoryActive);
    }
    public TextSearchPatternImpl(final Integer id, final String name, final String pattern, final Boolean active, final Boolean subCategoryActive) {
        super(id, name, pattern, active, subCategoryActive);
        this.compiledPattern = Pattern.compile(pattern);
    }

    public Predicate<String> getPredicate() {
        return s -> true;
    }

    public void updatePattern(String pattern) {
        setCompiledPattern(Pattern.compile(pattern));
        setPattern(pattern);
    }

    public Pattern getCompiledPattern() {
        return compiledPattern;
    }

    public void setCompiledPattern(Pattern compiledPattern) {
        this.compiledPattern = compiledPattern;
    }

    @Override
    public String toString() {
        return "TextSearchPatternImpl{" +
                "patternType=" + patternType +
                ", description='" + description + '\'' +
                ", name='" + getName() + '\'' +
                ", id='" + getId() + '\'' +
                ", active=" + active +
                ", subCategoryActive=" + isSubCategoryActive() +
                '}';
    }
}
