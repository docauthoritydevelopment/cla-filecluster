package com.cla.common.domain.dto.pv;

import com.google.common.base.Strings;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Predicate by which to determine if a content belongs to a sub group or nor
 * Created by: yael
 * Created on: 2/5/2018
 */
public class RefinementRulePredicate {

    private RuleOperator operator;
    private List<String> values;
    private List<RefinementRulePredicate> predicates;
    private RefinementRuleField field;

    public static List<String> stopWords = Arrays.asList("OR","AND","NOT");

    /**
     * validate the rule's predicate is valid
     * @return indication if valid or not
     */
    public boolean validateRule(Function<String, Boolean> f) {
        if (RuleOperator.CONTAINS.equals(operator)) {
            if (predicates != null) return false;
            if (field == null) return false;
            if (values == null || values.isEmpty()) return false;
            values = values.stream().filter(v -> !Strings.isNullOrEmpty(v) && !Strings.isNullOrEmpty(v.trim())).map(String::trim).collect(Collectors.toList());
            values = values.stream().filter(v -> !stopWords.contains(v.toUpperCase()) && !v.contains(" ") && f.apply(v)).collect(Collectors.toList());
            return !values.isEmpty();
        } else if (RuleOperator.OR.equals(operator) || RuleOperator.AND.equals(operator)) {
            if (predicates != null && predicates.size() > 1 && values == null && field == null) {
                for (RefinementRulePredicate p : predicates) {
                    if (!p.validateRule(f)) return false;
                }
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Get all predicates that are not and/or
     * @param node - highest level predicate
     * @return the predicates that are not and/or
     */
    public static List<RefinementRulePredicate> returnAllPredicatesMultiLevel(RefinementRulePredicate node) {
        List<RefinementRulePredicate> listOfNodes = new ArrayList<>();
        addAllNodes(node, listOfNodes);
        return listOfNodes;
    }

    // return the predicates that are not and/or
    private static void addAllNodes(RefinementRulePredicate node, List<RefinementRulePredicate> listOfNodes) {
        if (node != null) {
            if (node.getOperator().equals(RuleOperator.CONTAINS)) listOfNodes.add(node);
            List<RefinementRulePredicate> children = node.getPredicates();
            if (children != null) {
                for (RefinementRulePredicate child: children) {
                    addAllNodes(child, listOfNodes);
                }
            }
        }
    }

    /**
     * Calculate predicate result for a specific content
     * @param results of inner predicates (that are not and/or)
     * @return does the content matches the predicate or not
     */
    public boolean testRule(Map<RefinementRulePredicate, Boolean> results) {
        if (RuleOperator.CONTAINS.equals(operator)) {
            return results == null ? false : results.getOrDefault(this, false);
        } else if (RuleOperator.OR.equals(operator)) {
            for (RefinementRulePredicate p : predicates) {
                if (p.testRule(results)) return true;
            }
            return false;
        } else if (RuleOperator.AND.equals(operator)) {
            for (RefinementRulePredicate p : predicates) {
                if (!p.testRule(results)) return false;
            }
            return true;
        }
        return false;
    }

    public RuleOperator getOperator() {
        return operator;
    }

    public void setOperator(RuleOperator operator) {
        this.operator = operator;
    }

    public List<String> getValues() {
        return values;
    }

    public void setValues(List<String> values) {
        this.values = values;
    }

    public List<RefinementRulePredicate> getPredicates() {
        return predicates;
    }

    public void setPredicates(List<RefinementRulePredicate> predicates) {
        this.predicates = predicates;
    }

    public RefinementRuleField getField() {
        return field;
    }

    public void setField(RefinementRuleField field) {
        this.field = field;
    }

    @Override
    public String toString() {
        return "RefinementRulePredicate{" +
                "operator=" + operator +
                ", values=" + values +
                ", predicates=" + predicates +
                ", field=" + field +
                '}';
    }
}
