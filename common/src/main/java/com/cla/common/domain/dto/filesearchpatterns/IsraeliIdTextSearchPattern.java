package com.cla.common.domain.dto.filesearchpatterns;

import java.util.function.Predicate;

public class IsraeliIdTextSearchPattern extends TextSearchPatternImpl {

	public static final String IL_ID_PATERN = "\\b\\d{5,9}\\b";

	public IsraeliIdTextSearchPattern() {}

	public IsraeliIdTextSearchPattern(final Integer id, final String name) {
		super(id, name, IL_ID_PATERN);
	}

	@Override
	public Predicate<String> getPredicate() {
		return this::checkLuhn;
	}

	private boolean checkLuhn(final String number) {
		final int[] digits = ("0000"+number).substring(number.length()-5).chars().toArray();	// (length + 4) - 9 = length - 5
		int sum = 0;
		final int length = digits.length;
		for (int i = 0; i < length; i++) {

			// get digits in reverse order
			int digit = digits[length - i - 1] - '0';
			
			// every 2nd number multiply with 2
			if (i % 2 == 1) {
				digit *= 2;
			}
			sum += digit > 9 ? digit - 9 : digit;
		}
		return sum % 10 == 0;
	}
}
