package com.cla.common.domain.dto.security;

import com.google.common.base.Strings;

import static com.cla.common.domain.dto.security.Authority.Const.*;

public enum Authority {

	MngScanConfig(MNG_SCAN_CONFIG, "Manage scan configuration", "Manage root folders and scan configuration"),
	RunScans(RUN_SCANS, "Run scans", "Run scans"),
	MngRoles(MNG_ROLES, "Manage roles", "Manage system roles"),
	MngUsers(MNG_USERS, "Manage users", "Manage system users"),
	ViewUsers(VIEW_USERS, "View users", "View users configuration"),
	MngUserRoles(MNG_USER_ROLES, "Assign roles", "Assign roles to users"),
	ViewAuditTrail(VIEW_AUDIT_TRAIL, "View audit", "View audit trail"),
	GeneralAdmin(GENERAL_ADMIN, "General administration", "General system administration"),
	MngTagConfig(MNG_TAG_CONFIG, "Manage tags", "Manage tags configuration"),
	AssignTag(ASSIGN_TAG, "Assign tags", "Assign tags"),
	AssignSecurityTag(ASSIGN_SECURITY_TAG, "Assign security tags", "Assign security tags"),
	ViewTagTypes(VIEW_TAG_TYPES, "View tag types", "View tag types"),
	ViewTagAssociation(VIEW_TAG_ASSOCIATION, "View assigned tags", "View tag associations"),
	ViewSecurityTagAssociation(VIEW_SECURITY_TAG_ASSOCIATION, "View security tags", "View security tag associations"),
	MngDocTypeConfig(MNG_DOC_TYPE_CONFIG, "Manage DocTypes", "Manage DocTypes"),
	AssignDocType(ASSIGN_DOC_TYPE, "Assign DocTypes", "Assign DocTypes"),
	ViewDocTypeTree(VIEW_DOC_TYPE_TREE, "View DocTypes tree", "View DocTypes hierarchy"),
	ViewDocTypeAssociation(VIEW_DOC_TYPE_ASSOCIATION, "View assigned DocTypes", "View DocTypes associations"),
	AssignDataRole(ASSIGN_DATA_ROLE, "Assign data role", "Assign data role"),
	ViewDataRoleAssociation(VIEW_DATA_ROLE_ASSOCIATION, "View assigned data role", "View data role associations"),
	MngBizLists(MNG_BIZ_LISTS, "Manage business lists", "Manage business lists"),
	RunActions(RUN_ACTIONS, "Run actions", "Run actions"),
	ViewContent(VIEW_CONTENT, "View files content", "View all files content"),
	ViewFiles(VIEW_FILES, "View files", "View all files"),
	ViewPermittedContent(VIEW_PERMITTED_CONTENT, "View permitted content", "View permitted content"),
	ViewPermittedFiles(VIEW_PERMITTED_FILES, "View permitted files", "View permitted files"),
	ViewBizListAssociation(VIEW_BIZ_LIST_ITEM_ASSOCIATION, "View assigned business list items", "View business list associations"),
	AssignBizList(ASSIGN_BIZ_LIST_ITEM, "Assign business list item", "Assign business list item"),
	MngGroups(MNG_GROUPS, "Manage groups", "Manage group names and content"),
	ViewReports(VIEW_REPORTS, "View reports", "View saved reports"),
	MngReports(MNG_REPORTS, "Manage reports", "Manage saved reports"),
	TechSupport(TECH_SUPPORT, "Tech support", "Technical support"),
	MngGDPR(MNG_GDPR, "Manage GDPR", "Manage GDPR"),
	MngDepartmentConfig(MNG_DEPARTMENT_CONFIG, "Manage Departments", "Manage Departments"),
	AssignDepartment(ASSIGN_DEPARTMENT, "Assign Departments", "Assign Departments"),
	ViewDepartmentTree(VIEW_DEPARTMENT_TREE, "View Departments tree", "View Departments hierarchy"),
	ViewDepartmentAssociation(VIEW_DEPARTMENT_ASSOCIATION, "View assigned Departments", "View Departments associations"),
	ManageSearchPatterns(MANAGE_SEARCH_PATTERNS, "Manage search patterns", "Manage search patterns"),
	ActivateRegulations(ACTIVATE_REGULATIONS, "Activate regulations", "Activate regulations "),
	ActivatePatternCategories(ACTIVATE_SEARCH_PATTERNS_CATEGORIES, "Activate search pattern categories", "Activate search pattern categories "),
	MngUserViewData(MNG_USER_VIEW_DATA, "Manage user view data", "Manage saved filters, charts and dashboards"),
	MngLicense(MNG_LICENSE, "Manage license", "Validate and update license key"),
	AssignPublicUserViewData(ASSIGN_PUBLIC_USER_VIEW_DATA, "Assign public user view data", "Assign filter/chart/dashboard widget as public"),
	AssignTagFilesBatch(ASSIGN_TAG_FILES_BATCH, "Assign tags to files in batch mode", "Assign tags to files via filter in batch mode")
	;

	private String name;
	private String displayName;
	private String description;

	Authority(String name, String displayName, String description) {
		this.name = name;
		this.displayName = displayName;
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public boolean nameEq(String name){
		return (!Strings.isNullOrEmpty(name)) && this.name.equals(name);
	}

	public String getDisplayName() {
		return displayName;
	}

	public String getDescription() {
		return description;
	}

	public static class Const {
		public static final String MNG_SCAN_CONFIG = 						"MngScanConfig";
		public static final String RUN_SCANS = 								"RunScans";
		public static final String MNG_ROLES = 								"MngRoles";
		public static final String MNG_USERS = 								"MngUsers";
		public static final String VIEW_USERS = 							"ViewUsers";
		public static final String MNG_USER_ROLES = 						"MngUserRoles";
		public static final String VIEW_AUDIT_TRAIL = 						"ViewAuditTrail";
		public static final String GENERAL_ADMIN = 							"GeneralAdmin";
		public static final String MNG_TAG_CONFIG = 						"MngTagConfig";
		public static final String ASSIGN_TAG = 							"AssignTag";
		public static final String ASSIGN_SECURITY_TAG = 					"AssignSecurityTag";
		public static final String VIEW_TAG_TYPES = 						"ViewTagTypes";
		public static final String VIEW_TAG_ASSOCIATION = 					"ViewTagAssociation";
		public static final String VIEW_SECURITY_TAG_ASSOCIATION = 			"ViewSecurityTagAssociation";
		public static final String MNG_DOC_TYPE_CONFIG = 					"MngDocTypeConfig";
		public static final String ASSIGN_DOC_TYPE = 						"AssignDocType";
		public static final String VIEW_DOC_TYPE_TREE = 					"ViewDocTypeTree";
		public static final String VIEW_DOC_TYPE_ASSOCIATION = 				"ViewDocTypeAssociation";
		public static final String ASSIGN_DATA_ROLE = 						"AssignDataRole";
		public static final String VIEW_DATA_ROLE_ASSOCIATION =				"ViewDataRoleAssociation";
		public static final String MNG_BIZ_LISTS = 							"MngBizLists";
		public static final String RUN_ACTIONS = 							"RunActions";
		public static final String VIEW_CONTENT = 							"ViewContent";
		public static final String VIEW_FILES = 							"ViewFiles";
		public static final String VIEW_PERMITTED_CONTENT = 				"ViewPermittedContent";
		public static final String VIEW_PERMITTED_FILES = 					"ViewPermittedFiles";
		public static final String MNG_GROUPS = 							"MngGroups";
		public static final String ASSIGN_BIZ_LIST_ITEM = 					"AssignBizList";
		public static final String VIEW_BIZ_LIST_ITEM_ASSOCIATION = 		"ViewBizListAssociation";
		public static final String VIEW_REPORTS = 							"ViewReports";
		public static final String MNG_REPORTS = 							"MngReports";
		public static final String TECH_SUPPORT = 							"TechSupport";
		public static final String MNG_GDPR =								"MngGDPR";
		public static final String MNG_DEPARTMENT_CONFIG = 					"MngDepartmentConfig";
		public static final String ASSIGN_DEPARTMENT = 						"AssignDepartment";
		public static final String VIEW_DEPARTMENT_TREE = 					"ViewDepartmentTree";
		public static final String VIEW_DEPARTMENT_ASSOCIATION = 			"ViewDepartmentAssociation";
		public static final String MANAGE_SEARCH_PATTERNS = 				"ManageSearchPatterns";
		public static final String ACTIVATE_REGULATIONS = 					"ActivateRegulations";
		public static final String ACTIVATE_SEARCH_PATTERNS_CATEGORIES = 	"ActivatePatternCategories";
		public static final String MNG_USER_VIEW_DATA = 					"MngUserViewData";
		public static final String MNG_LICENSE = 							"MngLicense";
		public static final String ASSIGN_PUBLIC_USER_VIEW_DATA = 			"AssignPublicUserViewData";
		public static final String ASSIGN_TAG_FILES_BATCH = 				"AssignTagFilesBatch";

		public static final String SERVER_CONTENT_PERMISSIONS = 	VIEW_CONTENT + ',' + VIEW_PERMITTED_CONTENT;
	}
}
