package com.cla.common.domain.dto.workflow;

import java.util.Map;

public class WorkflowSummaryDto {
    private long numOfFiles;
    private Map<FileAction, Long> fileActionCount;
    private Map<FileState, Long> fileStateCount;

    public void setNumOfFiles(long numOfFiles) {
        this.numOfFiles = numOfFiles;
    }

    public long getNumOfFiles() {
        return numOfFiles;
    }

    public void setFileActionCount(Map<FileAction, Long> fileActionCount) {
        this.fileActionCount = fileActionCount;
    }

    public Map<FileAction, Long> getFileActionCount() {
        return fileActionCount;
    }

    public void setFileStateCount(Map<FileState, Long> fileStateCount) {
        this.fileStateCount = fileStateCount;
    }

    public Map<FileState, Long> getFileStateCount() {
        return fileStateCount;
    }

    @Override
    public String toString() {
        return "WorkflowSummaryDto{" +
                "numOfFiles=" + numOfFiles +
                ", fileActionCount=" + fileActionCount +
                ", fileStateCount=" + fileStateCount +
                '}';
    }
}
