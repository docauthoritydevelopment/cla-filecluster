package com.cla.common.domain.dto.filetag;

import java.util.List;

/**
 * Created by uri on 05/06/2016.
 */
public class FileTagTypeAndTagsDto {

    FileTagTypeDto fileTagTypeDto;

    List<FileTagDto> fileTags;

    public FileTagTypeDto getFileTagTypeDto() {
        return fileTagTypeDto;
    }

    public void setFileTagTypeDto(FileTagTypeDto fileTagTypeDto) {
        this.fileTagTypeDto = fileTagTypeDto;
    }

    public List<FileTagDto> getFileTags() {
        return fileTags;
    }

    public void setFileTags(List<FileTagDto> fileTags) {
        this.fileTags = fileTags;
    }
}
