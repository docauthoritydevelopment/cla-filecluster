package com.cla.common.domain.dto.components;

import java.io.Serializable;

/**
 * Created by itay on 22/02/2017.
 */

public class ClaComponentStatusDto implements Serializable {
    private Long id;                // Optional

    private Long claComponentId;

    private Long updatedOnDb;

    private Long timestamp;         // Time when record is applicable

    private String componentType;   // De-normalized type

    private ComponentState componentState;

    private String extraMetrics;

    private String driveData;

    public Long getId() {
        return id;
    }

    public Long getClaComponentId() {
        return claComponentId;
    }

    public Long getUpdatedOnDb() {
        return updatedOnDb;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public String getComponentType() {
        return componentType;
    }

    public ComponentState getComponentState() {
        return componentState;
    }

    public String getExtraMetrics() {
        return extraMetrics;
    }

    public String getDriveData() {
        return driveData;
    }

	public ClaComponentStatusDto setId(Long id) {
		this.id = id;
		return this;
	}

	public ClaComponentStatusDto setClaComponentId(Long claComponentId) {
		this.claComponentId = claComponentId;
		return this;
	}

	public ClaComponentStatusDto setUpdatedOnDb(Long updatedOnDb) {
		this.updatedOnDb = updatedOnDb;
		return this;
	}

	public ClaComponentStatusDto setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
		return this;
	}

	public ClaComponentStatusDto setComponentType(String componentType) {
		this.componentType = componentType;
		return this;
	}

	public ClaComponentStatusDto setComponentState(ComponentState componentState) {
		this.componentState = componentState;
		return this;
	}

	public ClaComponentStatusDto setExtraMetrics(String extraMetrics) {
		this.extraMetrics = extraMetrics;
		return this;
	}

    public ClaComponentStatusDto setDriveData(String driveData) {
        this.driveData = driveData;
        return this;
    }

    @Override
    public String toString() {
        return "ClaComponentStatusDto{" +
                "id=" + id +
                ", claComponentId=" + claComponentId +
                ", updatedOnDb=" + updatedOnDb +
                ", timestamp=" + timestamp +
                ", componentType='" + componentType + '\'' +
                ", componentState=" + componentState +
                ", extraMetrics='" + extraMetrics + '\'' +
                '}';
    }
}
