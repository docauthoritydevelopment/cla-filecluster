package com.cla.common.domain.dto.event;

/**
 * represents the source of the event
 *
 * Created by: yael
 * Created on: 1/10/2018
 */
public enum EventProviderType {
    DOCAUTHORITY
}
