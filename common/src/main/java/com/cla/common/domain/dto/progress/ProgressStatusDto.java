package com.cla.common.domain.dto.progress;

import lombok.Data;

import java.io.Serializable;
import java.util.Map;

@Data
public class ProgressStatusDto implements Serializable {
    private String id;
    private String progressType;
    private ProgressStatus status;
    private Long createdDate;
    private Long currentTimeStamp;
    private Long lastUpdateDate;
    private Long statusChangedDate;
    private String createdBy;
    private Map<String, String> userParams;
    private Integer progress;
    private String message;
}
