package com.cla.common.domain.dto.filetree;

/**
 * Created by uri on 19/04/2016.
 */
public enum StoreRescanMethod {
    FULL, INCREMENTAL
}
