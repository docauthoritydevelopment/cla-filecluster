package com.cla.common.domain.dto.event;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by: yael
 * Created on: 1/10/2018
 */
public abstract class DAEvent <T extends EventPayload> {

    private long id;

    private long created;

    private long reported; // date of original event creation in origin system

    private EventType eventType;

    private EventProviderType eventProvider;

    private String description;

    private Map<String, Object> factsMap;

    private T payload;

    public DAEvent() {
        factsMap = new HashMap<>();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getCreated() {
        return created;
    }

    public void setCreated(long created) {
        this.created = created;
    }

    /**
     *
     * @return date of original event creation in origin system
     */
    public long getReported() {
        return reported;
    }

    public void setReported(long reported) {
        this.reported = reported;
    }

    public EventType getEventType() {
        return eventType;
    }

    public void setEventType(EventType eventType) {
        this.eventType = eventType;
    }

    public EventProviderType getEventProvider() {
        return eventProvider;
    }

    public void setEventProvider(EventProviderType eventProvider) {
        this.eventProvider = eventProvider;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Map<String, Object> getFactsMap() {
        return factsMap;
    }

    public void setFactsMap(Map<String, Object> factsMap) {
        this.factsMap = factsMap;
    }

    public T getPayload() {
        return payload;
    }

    public void setPayload(T payload) {
        this.payload = payload;
    }

    @Override
    public String toString() {
        return "DAEvent{" +
                "id=" + id +
                ", created=" + created +
                ", reported=" + reported +
                ", eventType=" + eventType +
                ", eventProvider=" + eventProvider +
                ", description='" + description + '\'' +
                ", factsMap=" + factsMap +
                ", payload=" + payload +
                '}';
    }
}
