package com.cla.common.domain.dto.group;

import com.google.common.collect.Maps;

import java.util.Map;

/**
 * Created by uri on 02/06/2016.
 */
public enum GroupType {
    ANALYSIS_GROUP(0),
    USER_GROUP(1),
    SUB_GROUP(2)
    ;

    private static final Map<Integer, GroupType> intMapping = Maps.newHashMap();
    private final int value;

    GroupType(int value) {
        this.value = value;
    }

    static {
        for (final GroupType t : GroupType.values()) {
            intMapping.put(t.value, t);
        }
    }

    public int toInt() {
        return value;
    }

    public static GroupType valueOf(final int value) {
        final GroupType mt = GroupType.intMapping.get(value);
        if (mt == null) {
            throw new RuntimeException("Invalid value: " + value);
        }
        return mt;
    }
}
