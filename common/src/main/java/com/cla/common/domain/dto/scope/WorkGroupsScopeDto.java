package com.cla.common.domain.dto.scope;

import com.cla.common.domain.dto.security.WorkGroupDto;

import javax.validation.constraints.NotNull;
import java.util.Set;


public class WorkGroupsScopeDto implements Scope<Set<WorkGroupDto>> {

    private Long id;

    private Long dateCreated;

    private Long dateModified;

    private String name;

    private String description;

    private Set<WorkGroupDto> workGroups;

    @Override
    public Set<WorkGroupDto> getScopeBindings() {
        return workGroups;
    }

    public void setScopeBindings(Set<WorkGroupDto> workGroups) {
        this.workGroups = workGroups;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Long dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Long getDateModified() {
        return dateModified;
    }

    public void setDateModified(Long dateModified) {
        this.dateModified = dateModified;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
