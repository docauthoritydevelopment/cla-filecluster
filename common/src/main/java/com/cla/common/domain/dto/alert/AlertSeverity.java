package com.cla.common.domain.dto.alert;

/**
 * Created by: yael
 * Created on: 1/10/2018
 */
public enum AlertSeverity {
    INFO,
    LOW,
    MEDIUM,
    HIGH,
    CRITICAL
}
