package com.cla.common.domain.dto.file;

public class FileTypeCategoryDto {
	private final String name;
	private final long id;

	public FileTypeCategoryDto(final long id, final String name ) {
		this.name = name;
		this.id = id;
	}
	

	public String getName() {
		return name;
	}
	public long getId() {
		return id;
	}

	@Override
	public String toString() {
		return "{name=" + name+ "}";
	}
	
}
