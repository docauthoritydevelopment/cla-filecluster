package com.cla.common.domain.dto.workflow;

/**
 * Created by: yael
 * Created on: 2/21/2018
 */
public enum WorkflowPriority {
    URGENT, HIGH, MEDIUM, LOW;
}
