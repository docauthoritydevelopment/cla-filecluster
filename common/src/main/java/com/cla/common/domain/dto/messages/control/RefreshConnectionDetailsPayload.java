package com.cla.common.domain.dto.messages.control;

import com.cla.connector.domain.dto.media.MediaConnectionDetailsDto;

/**
 * Created by oren on 7/2/2018.
 *
 */
public class RefreshConnectionDetailsPayload extends ControlMessagePayload {

    private MediaConnectionDetailsDto mediaConnectionDetails;

    public RefreshConnectionDetailsPayload() {

    }

    public RefreshConnectionDetailsPayload(MediaConnectionDetailsDto mediaConnectionDetails) {
        this.mediaConnectionDetails = mediaConnectionDetails;
    }

    public MediaConnectionDetailsDto getMediaConnectionDetails() {
        return mediaConnectionDetails;
    }
}
