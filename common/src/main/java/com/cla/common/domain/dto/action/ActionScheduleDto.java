package com.cla.common.domain.dto.action;

import com.cla.common.domain.dto.schedule.ScheduleConfigDto;

/**
 * Created by: yael
 * Created on: 3/4/2018
 */
public class ActionScheduleDto {

    private Long id;

    private Long dateCreated;

    private Long dateModified;

    private String name;

    private long actionExecutionTaskStatusId;

    private String cronTriggerString;

    private String resultPath;

    private Boolean active;

    private ScheduleConfigDto scheduleConfigDto;

    private long lastExecutionDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Long dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Long getDateModified() {
        return dateModified;
    }

    public void setDateModified(Long dateModified) {
        this.dateModified = dateModified;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getActionExecutionTaskStatusId() {
        return actionExecutionTaskStatusId;
    }

    public void setActionExecutionTaskStatusId(long actionExecutionTaskStatusId) {
        this.actionExecutionTaskStatusId = actionExecutionTaskStatusId;
    }

    public String getCronTriggerString() {
        return cronTriggerString;
    }

    public void setCronTriggerString(String cronTriggerString) {
        this.cronTriggerString = cronTriggerString;
    }

    public String getResultPath() {
        return resultPath;
    }

    public void setResultPath(String resultPath) {
        this.resultPath = resultPath;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public ScheduleConfigDto getScheduleConfigDto() {
        return scheduleConfigDto;
    }

    public void setScheduleConfigDto(ScheduleConfigDto scheduleConfigDto) {
        this.scheduleConfigDto = scheduleConfigDto;
    }

    public long getLastExecutionDate() {
        return lastExecutionDate;
    }

    public void setLastExecutionDate(long lastExecutionDate) {
        this.lastExecutionDate = lastExecutionDate;
    }
}
