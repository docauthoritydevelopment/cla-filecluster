package com.cla.common.domain.dto.security;

import com.cla.connector.domain.dto.JsonSerislizableVisible;
import com.google.common.collect.Sets;

import java.util.Arrays;
import java.util.Date;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by uri on 17/07/2016.
 */
public class UserDto implements JsonSerislizableVisible {

    private Long id;

    private String name;

    private String roleName;

    private String username;

    private String password;

    private UserState userState;

    private UserType userType;

    private Long lastPasswordChangeTimeStamp;

    private String email;

    private boolean passwordMustBeChanged;

    private Date lastSuccessfulLogin;

    private Date lastFailedLogin;

    private int consecutiveLoginFailuresCount;

    private boolean accountNonExpired;

    private boolean accountNonLocked;

    private boolean credentialsNonExpired;

    private Set<SystemRoleDto> systemRoleDtos = Sets.newHashSet();

    private Set<BizRoleDto> bizRoleDtos = Sets.newHashSet();

    private Set<FunctionalRoleDto> funcRoleDtos = Sets.newHashSet();

    private Set<String> accessTokens = Sets.newHashSet();

    private Set<WorkGroupDto> workGroupDtos = Sets.newHashSet();

    private boolean deleted;

    private boolean ldapUser;

    private boolean loginLicenseExpired = false;

    public UserDto() {
    }

    public UserDto(String name, String username, String password) {
        this(name, username, password, null, true, true, true, (SystemRoleDto[]) null);
    }

    public UserDto(final String name, final String username, final String password, final String roleName, final SystemRoleDto... roles) {
        this(name, username, password, roleName, true, true, true, roles);
    }

    public UserDto(final String name, final String username, final String password, final String roleName,
                   final boolean accountNonExpired, final boolean accountNonLocked, final boolean credentialsNonExpired,
                   final SystemRoleDto... roles) {
        this.name = name;
        this.username = username;
        this.password = password;
        this.roleName = roleName;
        this.systemRoleDtos = roles == null ? null : Arrays.stream(roles).collect(Collectors.toSet());

        this.accountNonExpired = accountNonExpired;
        this.accountNonLocked = accountNonLocked;
        this.credentialsNonExpired = credentialsNonExpired;
        this.consecutiveLoginFailuresCount = 0;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Set<SystemRoleDto> getSystemRoleDtos() {
        return systemRoleDtos;
    }

    public void setSystemRoleDtos(Set<SystemRoleDto> systemRoleDtos) {
        this.systemRoleDtos = systemRoleDtos;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<String> getAccessTokens() {
        return accessTokens;
    }

    public void setAccessTokens(Set<String> accessTokens) {
        this.accessTokens = accessTokens;
    }


    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    public UserType getUserType() {
        return userType;
    }

    public void setUserType(UserType userType) {
        this.userType = userType;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getLastPasswordChangeTimeStamp() {
        return lastPasswordChangeTimeStamp;
    }

    public void setLastPasswordChangeTimeStamp(Long lastPasswordChangeTimeStamp) {
        this.lastPasswordChangeTimeStamp = lastPasswordChangeTimeStamp;
    }

    public boolean isPasswordMustBeChanged() {
        return passwordMustBeChanged;
    }

    public void setPasswordMustBeChanged(boolean passwordMustBeChanged) {
        this.passwordMustBeChanged = passwordMustBeChanged;
    }

    public Date getLastSuccessfulLogin() {
        return lastSuccessfulLogin;
    }

    public void setLastSuccessfulLogin(Date lastSuccessfulLogin) {
        this.lastSuccessfulLogin = lastSuccessfulLogin;
    }

    public Date getLastFailedLogin() {
        return lastFailedLogin;
    }

    public void setLastFailedLogin(Date lastFailedLogin) {
        this.lastFailedLogin = lastFailedLogin;
    }

    public int getConsecutiveLoginFailuresCount() {
        return consecutiveLoginFailuresCount;
    }

    public void setConsecutiveLoginFailuresCount(int consecutiveLoginFailuresCount) {
        this.consecutiveLoginFailuresCount = consecutiveLoginFailuresCount;
    }

    public boolean isAccountNonExpired() {
        return accountNonExpired;
    }

    public void setAccountNonExpired(boolean accountNonExpired) {
        this.accountNonExpired = accountNonExpired;
    }

    public boolean isAccountNonLocked() {
        return accountNonLocked;
    }

    public void setAccountNonLocked(boolean accountNonLocked) {
        this.accountNonLocked = accountNonLocked;
    }

    public boolean isCredentialsNonExpired() {
        return credentialsNonExpired;
    }

    public void setCredentialsNonExpired(boolean credentialsNonExpired) {
        this.credentialsNonExpired = credentialsNonExpired;
    }

    public UserState getUserState() {
        return userState;
    }

    public void setUserState(UserState userState) {
        this.userState = userState;
    }

    public Set<BizRoleDto> getBizRoleDtos() {
        return bizRoleDtos;
    }

    public void setBizRoleDtos(Set<BizRoleDto> bizRoleDtos) {
        this.bizRoleDtos = bizRoleDtos;
    }

    public Set<FunctionalRoleDto> getFuncRoleDtos() {
        return funcRoleDtos;
    }

    public void setFuncRoleDtos(Set<FunctionalRoleDto> funcRoleDtos) {
        this.funcRoleDtos = funcRoleDtos;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public boolean isLdapUser() {
        return ldapUser;
    }

    public void setLdapUser(boolean ldapUser) {
        this.ldapUser = ldapUser;
    }

    public Set<WorkGroupDto> getWorkGroupDtos() {
        return workGroupDtos;
    }

    public void setWorkGroupDtos(Set<WorkGroupDto> workGroupDtos) {
        this.workGroupDtos = workGroupDtos;
    }

    public boolean isLoginLicenseExpired() {
        return loginLicenseExpired;
    }

    public void setLoginLicenseExpired(boolean loginLicenseExpired) {
        this.loginLicenseExpired = loginLicenseExpired;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserDto userDto = (UserDto) o;

        if (id != null ? !id.equals(userDto.id) : userDto.id != null) return false;
        return name != null ? name.equals(userDto.name) : userDto.name == null;

    }

    @Override
    public String toString() {
        return "UserDto{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", roleName='" + roleName + '\'' +
                ", username='" + username + '\'' +
                ", systemRoleDtos=" + systemRoleDtos +
                '}';
    }

}
