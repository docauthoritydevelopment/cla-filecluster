package com.cla.common.domain.dto.progress;

public enum  ProgressStatus {
    NEW, IN_PROGRESS, DONE, FAILED
}
