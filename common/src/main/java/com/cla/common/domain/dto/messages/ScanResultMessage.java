package com.cla.common.domain.dto.messages;

import com.cla.connector.domain.dto.messages.PhaseMessagePayload;
import com.cla.connector.domain.dto.messages.ScanErrorDto;

import java.util.List;
import java.util.Set;

/**
 * Scan result DTO/message
 * Created by vladi on 2/20/2017.
 */
public class ScanResultMessage extends ProcessingPhaseMessage<PhaseMessagePayload, ScanErrorDto> implements ResponseMessage{

    private Long rootFolderId;

    private boolean firstScan;

    private boolean rootFolderAccessible;

    private boolean deltaScan;

    private String errorMessage;

    private boolean packagedScan = false;

    private String path;

    private List<ScanResultMessage> containedMessages;

    private Set<Long> taskIds;

    private String mailboxUPN;

    private String syncState;

    private Long progressUnreportedFiles;

    public ScanResultMessage() {
    }

    public static ScanResultMessage scanDoneMessage(long runContext, long rootFolderId,
                                                    boolean firstScan, Long taskId,
                                                    Long jobId, boolean deltaPayload) {
        ScanResultMessage scanResultMessage =
                new ScanResultMessage(null, rootFolderId, firstScan,runContext, taskId, jobId);
        scanResultMessage.setPayloadType(MessagePayloadType.SCAN_DONE);
        scanResultMessage.setDeltaScan(deltaPayload);
        return scanResultMessage;
    }

    public static ScanResultMessage scanFailedMessage(long runContext, long rootFolderId,
                                                      boolean firstScan, Long taskId, Long jobId) {
        ScanResultMessage scanResultMessage =
                new ScanResultMessage(null, rootFolderId, firstScan,runContext, taskId, jobId);
        scanResultMessage.setPayloadType(MessagePayloadType.SCAN_FAILED);
        return scanResultMessage;
    }

    public ScanResultMessage(PhaseMessagePayload payload, long rootFolderId, boolean firstScan,
                             long runContext, Long taskId, Long jobId) {
        super(payload);
        this.rootFolderId = rootFolderId;
        this.firstScan = firstScan;
        if (payload != null) {
            setPayloadType(MessagePayloadType.typeOf(payload));
        }
        setRunContext(runContext);
        setTaskId(taskId);
        setJobId(jobId);
    }

    public Long getRootFolderId() {
        return rootFolderId;
    }

    public void setRootFolderId(Long rootFolderId) {
        this.rootFolderId = rootFolderId;
    }

    public boolean isFirstScan() {
        return firstScan;
    }

    public void setFirstScan(boolean firstScan) {
        this.firstScan = firstScan;
    }

    public List<ScanResultMessage> getContainedMessages() {
        return containedMessages;
    }

    public void setContainedMessages(List<ScanResultMessage> containedMessages) {
        this.containedMessages = containedMessages;
    }

    public boolean isRootFolderAccessible() {
        return rootFolderAccessible;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public void setRootFolderAccessible(boolean rootFolderAccessible) {
        this.rootFolderAccessible = rootFolderAccessible;
    }

    public boolean isDeltaScan() {
        return deltaScan;
    }

    public void setDeltaScan(boolean deltaScan) {
        this.deltaScan = deltaScan;
    }

    public void setPackagedScan(boolean packagedScan) {
        this.packagedScan = packagedScan;
    }

    public boolean isPackagedScan() {
        return packagedScan;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Set<Long> getTaskIds() {
        return taskIds;
    }

    public void setTaskIds(Set<Long> taskIds) {
        this.taskIds = taskIds;
    }

    public String getMailboxUPN() {
        return mailboxUPN;
    }

    public void setMailboxUPN(String mailboxUPN) {
        this.mailboxUPN = mailboxUPN;
    }

    public String getSyncState() {
        return syncState;
    }

    public void setSyncState(String syncState) {
        this.syncState = syncState;
    }

    public Long getProgressUnreportedFiles() {
        return progressUnreportedFiles;
    }

    public void setProgressUnreportedFiles(Long progressUnreportedFiles) {
        this.progressUnreportedFiles = progressUnreportedFiles;
    }

    @Override
    public String toString() {
        return "ScanResultMessage{" +
                super.toString() +
                "rootFolderId=" + rootFolderId +
                ", firstScan=" + firstScan +
                ", rootFolderAccessible=" + rootFolderAccessible +
                ", deltaScan=" + deltaScan +
                ", errorMessage='" + errorMessage + '\'' +
                ", packagedScan=" + packagedScan +
                ", path='" + path + '\'' +
                ", containedMessages size=" + (containedMessages == null ? 0 : containedMessages.size()) +
                ", runContext=" + getRunContext() +
                ", taskId=" + getTaskId() +
                ", errors size=" + (getErrors()  == null ? 0 : getErrors().size()) +
                ", expirationTime=" + getExpirationTime() +
                '}';
    }
}
