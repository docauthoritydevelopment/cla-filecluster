package com.cla.common.domain.dto.file;

import java.io.Serializable;
import java.util.List;

/**
 * Created by uri on 14/12/2015.
 */
public class FileAclDataDto implements Serializable {

    private long fileId;
    private List<String> aclReadData;
    private List<String> aclWriteData;
    private List<String> aclDeniedReadData;
    private List<String> aclDeniedWriteData;


    public long getFileId() {
        return fileId;
    }

    public FileAclDataDto setFileId(long fileId) {
        this.fileId = fileId;
		return this;
	}

    public FileAclDataDto setAclReadData(List<String> aclReadData) {
        this.aclReadData = aclReadData;
		return this;
    }

    public List<String> getAclReadData() {
        return aclReadData;
    }

    public FileAclDataDto setAclWriteData(List<String> aclWriteData) {
        this.aclWriteData = aclWriteData;
		return this;
    }

    public List<String> getAclWriteData() {
        return aclWriteData;
    }

    public FileAclDataDto setAclDeniedReadData(List<String> aclDeniedReadData) {
        this.aclDeniedReadData = aclDeniedReadData;
		return this;
    }

    public List<String> getAclDeniedReadData() {
        return aclDeniedReadData;
    }

    public FileAclDataDto setAclDeniedWriteData(List<String> aclDeniedWriteData) {
        this.aclDeniedWriteData = aclDeniedWriteData;
		return this;
    }

    public List<String> getAclDeniedWriteData() {
        return aclDeniedWriteData;
    }
}
