package com.cla.common.domain.dto.pv;

import com.cla.connector.domain.dto.file.FileType;
import com.cla.connector.domain.dto.error.ProcessingErrorType;
import com.cla.connector.domain.dto.media.MediaType;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * Created by uri on 25/11/2015.
 */
public class FileProcessingErrorDto implements Serializable {

    private static String pattern = "dd-MM-yy HH:mm";
    private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

    private ProcessingErrorType type;

    private Long id;

    private String text;

    private String detailText;

    private Long fileId;

    private String fileName;

    private String baseName;

    private String rootFolder;

    private Long rootFolderId;

    private String folder;

    private FileType fileType;

    private Long timeStamp;

    private Long runId;

    private Long contentId;

    private Integer duplicationsCount;

    private MediaType mediaType;

    private String extension;

    public ProcessingErrorType getType() {
        return type;
    }

    public void setType(ProcessingErrorType type) {
        this.type = type;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getDetailText() {
        return detailText;
    }

    public void setDetailText(String detailText) {
        this.detailText = detailText;
    }

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getBaseName() {
        return baseName;
    }

    public void setBaseName(String baseName) {
        this.baseName = baseName;
    }

    public String getRootFolder() {
        return rootFolder;
    }

    public void setRootFolder(String rootFolder) {
        this.rootFolder = rootFolder;
    }

    public String getFolder() {
        return folder;
    }

    public void setFolder(String folder) {
        this.folder = folder;
    }

    public FileType getFileType() {
        return fileType;
    }

    public void setFileType(FileType fileType) {
        this.fileType = fileType;
    }

    public Long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public Long getRunId() {
        return runId;
    }

    public void setRunId(Long runId) {
        this.runId = runId;
    }

    public Long getContentId() {
        return contentId;
    }

    public void setContentId(Long contentId) {
        this.contentId = contentId;
    }

    public Integer getDuplicationsCount() {
        return duplicationsCount;
    }

    public void setDuplicationsCount(Integer duplicationsCount) {
        this.duplicationsCount = duplicationsCount;
    }

    public Long getRootFolderId() {
        return rootFolderId;
    }

    public void setRootFolderId(Long rootFolderId) {
        this.rootFolderId = rootFolderId;
    }

    public String getDateString() {
        if (timeStamp != null) {
            return simpleDateFormat.format(new Date(timeStamp));
        }
        return null;
    }

    public MediaType getMediaType() {
        return mediaType;
    }

    public void setMediaType(MediaType mediaType) {
        this.mediaType = mediaType;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    @Override
    public String toString() {
        return "FileProcessingErrorDto{" +
                "type=" + type +
                ", id=" + id +
                ", text='" + text + '\'' +
                ", detailText='" + detailText + '\'' +
                ", fileId=" + fileId +
                ", fileName='" + fileName + '\'' +
                ", baseName='" + baseName + '\'' +
                '}';
    }
}
