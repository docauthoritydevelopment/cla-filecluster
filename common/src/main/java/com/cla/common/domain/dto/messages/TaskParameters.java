package com.cla.common.domain.dto.messages;

import com.cla.connector.domain.dto.file.FileType;
import com.cla.connector.domain.dto.error.ProcessingErrorType;
import com.cla.connector.domain.dto.messages.PhaseMessagePayload;
import com.fasterxml.jackson.annotation.JsonIgnore;

public class TaskParameters implements PhaseMessagePayload {

    private Long fileId;
    private Long rootFolderId;
    private String filePath;
    private FileType fileType;
	private String mediaEntityId;

    public static TaskParameters create(){
        return new TaskParameters();
    }

    public Long getFileId() {
        return fileId;
    }

    public TaskParameters setFileId(Long fileId) {
        this.fileId = fileId;
        return this;
    }

    public Long getRootFolderId() {
        return rootFolderId;
    }

    public TaskParameters setRootFolderId(Long rootFolderId) {
        this.rootFolderId = rootFolderId;
        return this;
    }

    public String getFilePath() {
        return filePath;
    }

    public TaskParameters setFilePath(String filePath) {
        this.filePath = filePath;
        return this;
    }

    public FileType getFileType() {
        return fileType;
    }

    public TaskParameters setFileType(FileType fileType) {
        this.fileType = fileType;
        return this;
    }

    @JsonIgnore
    public ProcessingErrorType calculateProcessErrorType() {
        return ProcessingErrorType.ERR_NONE;
    }

	public <T extends TaskParameters>  T setMediaEntityId(String mediaEntityId) {
		this.mediaEntityId = mediaEntityId;
		return (T) this;
	}

	public String getMediaEntityId() {
		return mediaEntityId;
	}
}
