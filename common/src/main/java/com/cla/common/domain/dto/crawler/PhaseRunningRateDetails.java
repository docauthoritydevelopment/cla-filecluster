package com.cla.common.domain.dto.crawler;


import com.cla.connector.utils.TimeSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;

/**
 * Created by itay on 24/09/2017.
 */
public class PhaseRunningRateDetails {
    private static final Logger logger = LoggerFactory.getLogger(PhaseRunningRateDetails.class);

    private Long jobId;

    // how many tasks were done previous time we checked
    private int previousTaskCount = 0;

    // when the previous done tasks count was taken
    private long prevTaskCountTime = 0;

    // recent calculated interim average rate
    private double recentRunningRate = 0;

    private long rateCalcIntervalSec;

    public PhaseRunningRateDetails(Long jobId, long rateCalcIntervalSec) {
        this.jobId = jobId;
        this.rateCalcIntervalSec = rateCalcIntervalSec;
    }

    public Long getJobId() {
        return jobId;
    }

    public void setJobId(Long jobId) {
        this.jobId = jobId;
    }

    public int getPreviousTaskCount() {
        return previousTaskCount;
    }

    public void setPreviousTaskCount(int previousTaskCount) {
        this.previousTaskCount = previousTaskCount;
    }

    public long getPrevTaskCountTime() {
        return prevTaskCountTime;
    }

    public void setPrevTaskCountTime(long prevTaskCountTime) {
        this.prevTaskCountTime = prevTaskCountTime;
    }
    public void incrementPrevTaskCountTime(long increment) {
        this.prevTaskCountTime += increment;
    }

    public double getRecentRunningRate() {
        return recentRunningRate;
    }

    public void setRecentRunningRate(double recentRunningRate) {
        this.recentRunningRate = recentRunningRate;
    }

    public long getRateCalcIntervalSec() {
        return rateCalcIntervalSec;
    }

    public void setRateCalcIntervalSec(long rateCalcIntervalSec) {
        this.rateCalcIntervalSec = rateCalcIntervalSec;
    }

    private TimeSource timeSource = new TimeSource();

    public double calculateRunningRate(final int currentTaskCounter, final boolean update) {
        long passedIntervals = timeSource.secondsSince(getPrevTaskCountTime()) / rateCalcIntervalSec;
        double result = getRecentRunningRate();
        double tasksCounted = 0d;

        // Should we use value other than recent calculated rate
        if (passedIntervals >= 1) {
            if (passedIntervals > 1) {
                // Too much time without update - set rate = 0
                result = 0d;
                logger.trace("Set Job#{} recent-rate to zero (intervals={}, counted={}/{} update={})", getJobId(), passedIntervals,
                        currentTaskCounter - getPreviousTaskCount(), currentTaskCounter, (update?"yes":"no"));
            } else {
                // Update past interval rate
                tasksCounted = currentTaskCounter - getPreviousTaskCount();
                result = (tasksCounted >= 0) ? (tasksCounted / (passedIntervals * rateCalcIntervalSec)) : 0d;
                logger.trace("Set Job#{} recent-rate to {} (intervals={}, counted={}/{} update={})",
                        getJobId(), result, passedIntervals, tasksCounted, currentTaskCounter, (update ? "yes" : "no"));
            }
            if (update) {
//                synchronized (timeSource) {
                // Move prev to recent full interval multiplier
                incrementPrevTaskCountTime(TimeUnit.SECONDS.toMillis(passedIntervals * rateCalcIntervalSec));
                setRecentRunningRate(result);
                setPreviousTaskCount(currentTaskCounter);
//                }
                logger.debug("Updating running-rate details for job#{}. rate={}, intervals={}, counter={}/{}, updateTs={}",
                        getJobId(), result, passedIntervals, tasksCounted, currentTaskCounter, getPrevTaskCountTime());
            }
        }
        return result;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("PhaseRunningRateDetails{");
        sb.append("jobId=").append(jobId);
        sb.append(", previousTaskCount=").append(previousTaskCount);
        sb.append(", prevTaskCountTime=").append(prevTaskCountTime);
        sb.append(", recentRunningRate=").append(recentRunningRate);
        sb.append(", rateCalcIntervalSec=").append(rateCalcIntervalSec);
        sb.append('}');
        return sb.toString();
    }

}
