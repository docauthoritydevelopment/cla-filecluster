package com.cla.common.domain.dto.filetree;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

/**
 * An object for a complete RSA key
 * Created by yael on 11/12/2017.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RsaKey implements Serializable {

    @JsonIgnore
    private String publicKey;
    private String privateKey;
    private String certificate;

    public RsaKey() {
    }

    public RsaKey(String privateKey, String certificate) {
        this.privateKey = privateKey;
        this.certificate = certificate;
    }

    public RsaKey(String publicKey, String privateKey, String certificate) {
        this.privateKey = privateKey;
        this.publicKey = publicKey;
        this.certificate = certificate;
    }

    public String getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(String privateKey) {
        this.privateKey = privateKey;
    }

    public String getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }

    public String getCertificate() {
        return certificate;
    }

    public void setCertificate(String certificate) {
        this.certificate = certificate;
    }

    @Override
    public String toString() {
        return "RsaKey{" +
                ", privateKey='" + privateKey + '\'' +
                ", publicKey='" + publicKey + '\'' +
                ", certificate='" + certificate + '\'' +
                '}';
    }
}
