package com.cla.common.domain.dto;

import com.cla.common.domain.dto.filetree.DocFolderDto;

import java.io.Serializable;

@SuppressWarnings("serial")
public class AggregationCountFolderDto implements Serializable {
	private DocFolderDto item;
	private int numOfDirectFiles;
	private int numOfAllFiles;

    public static final int UNKNOWN = -1;

	public AggregationCountFolderDto() { // for qa JSON conversion
	}

	public AggregationCountFolderDto(final DocFolderDto docFolderDto, final int numOfDirectFiles) {
		this(docFolderDto,numOfDirectFiles,UNKNOWN);
	}
	public AggregationCountFolderDto(final DocFolderDto docFolderDto, final int numOfDirectFiles, final int numOfAllFiles) {
		this.item = docFolderDto;
		this.item.setNumOfAllFiles(numOfAllFiles);
		this.item.setNumOfDirectFiles(numOfDirectFiles);
		this.numOfDirectFiles = numOfDirectFiles;
		this.numOfAllFiles = numOfAllFiles;
	}

    public DocFolderDto getItem() {
        return item;
    }
    public int getNumOfDirectFiles() {
		return numOfDirectFiles;
	}
	public int getNumOfAllFiles() {
		return numOfAllFiles;
	}

	public void setItem(DocFolderDto item) {
		this.item = item;
	}

	public void setNumOfDirectFiles(int numOfDirectFiles) {
		this.numOfDirectFiles = numOfDirectFiles;
	}

	public void setNumOfAllFiles(int numOfAllFiles) {
		this.numOfAllFiles = numOfAllFiles;
	}

	@Override
	public String toString() {
		return "{item=" + item.toString() + ", numOfDirectFiles=" + numOfDirectFiles + (numOfAllFiles ==(-1)?"":(", numOfAllFiles="+ numOfAllFiles))+ "}";
	}

}
