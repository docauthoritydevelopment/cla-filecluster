package com.cla.common.domain.dto.filter;

import com.cla.common.domain.dto.kendo.FilterDescriptor;
import com.cla.common.domain.dto.scope.Scope;

import java.io.Serializable;

/**
 * Created by uri on 08/11/2015.
 */
public class SavedFilterDto implements Serializable{

    private long id;
    private String name;
    private String rawFilter;
    private FilterDescriptor filterDescriptor;
    private boolean globalFilter;
    private Scope scope;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setGlobalFilter(boolean globalFilter) {
        this.globalFilter = globalFilter;
    }

    public boolean isGlobalFilter() {
        return globalFilter;
    }

    public String getRawFilter() {
        return rawFilter;
    }

    public void setRawFilter(String rawFilter) {
        this.rawFilter = rawFilter;
    }

    public FilterDescriptor getFilterDescriptor() {
        return filterDescriptor;
    }

    public void setFilterDescriptor(FilterDescriptor filterDescriptor) {
        this.filterDescriptor = filterDescriptor;
    }

    public Scope getScope() {
        return scope;
    }

    public void setScope(Scope scope) {
        this.scope = scope;
    }
}
