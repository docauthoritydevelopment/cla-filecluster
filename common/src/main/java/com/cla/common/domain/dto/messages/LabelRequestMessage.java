package com.cla.common.domain.dto.messages;

/**
 * Created by: yael
 * Created on: 7/4/2019
 */
public class LabelRequestMessage extends ProcessingPhaseMessage<LabelTaskParameters, LabelingError> implements RequestMessage {

    public LabelRequestMessage() {
    }

    public LabelRequestMessage(LabelTaskParameters payload) {
        super(payload);
    }

    @Override
    public String toString() {
        return "LabelRequestMessage: " + super.toString();
    }
}
