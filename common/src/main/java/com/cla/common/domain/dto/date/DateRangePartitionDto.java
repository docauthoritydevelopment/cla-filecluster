package com.cla.common.domain.dto.date;

import java.io.Serializable;
import java.util.List;

/**
 * Created by uri on 05/06/2016.
 */
public class DateRangePartitionDto implements Serializable {
    private Long id;
    private String name;
    private String description;

    private boolean continuousRanges;
    private boolean isSystem;

    private List<DateRangeItemDto> dateRangeItemDtos;


    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return "DateRangePartitionDto{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                '}';
    }

    public void setDateRangeItemDtos(List<DateRangeItemDto> dateRangeItemDtos) {
        this.dateRangeItemDtos = dateRangeItemDtos;
    }

    public List<DateRangeItemDto> getDateRangeItemDtos() {
        return dateRangeItemDtos;
    }

    public boolean isContinuousRanges() {
        return continuousRanges;
    }

    public void setContinuousRanges(boolean continuousRanges) {
        this.continuousRanges = continuousRanges;
    }

    public boolean isSystem() {
        return isSystem;
    }

    public void setSystem(boolean system) {
        isSystem = system;
    }
}
