package com.cla.common.domain.dto.action;

/**
 * Created by uri on 16/02/2016.
 */
public enum ActionTriggerType {
    POLICY, IMMEDIATE
}
