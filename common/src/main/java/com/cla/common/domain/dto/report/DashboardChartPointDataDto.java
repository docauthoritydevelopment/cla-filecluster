package com.cla.common.domain.dto.report;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DashboardChartPointDataDto {
    private String name;
    private String description;
    private String id;
    private String parent;
    private Double value;
    private Integer depth;
    private Double directValue;
    private Double percentageValue;
    private String rfId;
    private int colorIndex;
}
