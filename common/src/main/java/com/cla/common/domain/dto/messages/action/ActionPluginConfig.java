package com.cla.common.domain.dto.messages.action;

import java.util.HashMap;
import java.util.Map;

/**
 * a config for the action plugin to work with
 * also tells the executor which plugin to create
 * Created by: yael
 * Created on: 1/10/2018
 */

public class ActionPluginConfig {

    private Map<String, Object> config = new HashMap<>();

    private String pluginId;

    public ActionPluginConfig() {
    }

    public ActionPluginConfig(String pluginId) {
        this.pluginId = pluginId;
    }

    public ActionPluginConfig(String pluginId, Map<String, Object> config) {
        this.pluginId = pluginId;
        this.config = config;
    }

    public String getPluginId() {
        return pluginId;
    }

    public void setPluginId(String pluginId) {
        this.pluginId = pluginId;
    }


    public Map<String, Object> getConfig() {
        return config;
    }

    public void setConfig(Map<String, Object> config) {
        this.config = config;
    }
}
