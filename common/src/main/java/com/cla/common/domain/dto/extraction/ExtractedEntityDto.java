package com.cla.common.domain.dto.extraction;

import com.cla.common.domain.dto.bizlist.BizListItemType;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by shtand on 22/09/2015.
 */
public class ExtractedEntityDto implements Serializable {
    Long id;

    private String clearId;

    private String clearName;

    private Map<String,Object> fields;

    private Long bizListId;

    private long extractionSessionId;
    private BizListItemType bizListItemType;

    public ExtractedEntityDto() {
    }

    public ExtractedEntityDto(String clearId) {
        this.clearId = clearId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getClearId() {
        return clearId;
    }

    public void setClearId(String clearId) {
        this.clearId = clearId;
    }

    public String getClearName() {
        return clearName;
    }

    public void setClearName(String clearName) {
        this.clearName = clearName;
    }

    public Map<String, Object> getFields() {
        return fields;
    }

    public void setFields(Map<String, Object> fields) {
        this.fields = fields;
    }

    public Long getBizListId() {
        return bizListId;
    }

    public void setBizListId(Long bizListId) {
        this.bizListId = bizListId;
    }

    public void setExtractionSessionId(long extractionSessionId) {
        this.extractionSessionId = extractionSessionId;
    }

    public long getExtractionSessionId() {
        return extractionSessionId;
    }

    @Override
    public String toString() {
        return "ExtractedEntityDto{" +
                "id=" + id +
                ", clearId='" + clearId + '\'' +
                ", clearName='" + clearName + '\'' +
                ", bizListId=" + bizListId +
                '}';
    }

    public BizListItemType getBizListItemType() {
        return bizListItemType;
    }

    public void setBizListItemType(BizListItemType bizListItemType) {
        this.bizListItemType = bizListItemType;
    }

    public void addFields(Map<String, Object> headerValueMap) {
        if (fields == null) {
            fields = new HashMap<>();
        }
        for (Map.Entry<String, Object> entry : headerValueMap.entrySet()) {
            fields.put(entry.getKey(),entry.getValue());
        }

    }
}
