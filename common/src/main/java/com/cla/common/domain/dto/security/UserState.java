package com.cla.common.domain.dto.security;

/**
 * Created by uri on 17/07/2016.
 */
public enum UserState {
    ACTIVE
    ,EXPIRED
    ,DISABLED
    ,BLOCKED
}
