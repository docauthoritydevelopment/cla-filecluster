package com.cla.common.domain.dto.filesearchpatterns;

import java.util.function.Predicate;

public class GenericCreditCardBadLuhnTextSearchPattern extends GenericCreditCardTextSearchPattern {

	public GenericCreditCardBadLuhnTextSearchPattern(final Integer id, final String name, final String patern) {
		super(id, name, patern);
	}

	public GenericCreditCardBadLuhnTextSearchPattern(final Integer id, final String name) {
		super(id, name);
	}

	public GenericCreditCardBadLuhnTextSearchPattern() {
	}

	@Override
	public Predicate<String> getPredicate() {
		// Validates only 'invalid' numbers by negating superclass luhn predicate.
		return s->(!super.getPredicate().test(s));
	}

}
