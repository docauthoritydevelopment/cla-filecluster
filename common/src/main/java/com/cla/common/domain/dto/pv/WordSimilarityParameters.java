package com.cla.common.domain.dto.pv;

public class WordSimilarityParameters  {
	public float wordSimilarityContentHighThreshold;
	public float wordSimilarityContentLowThreshold;
	public float wordSimilarityStyleHighThreshold;
	public float wordSimilarityStyleLowThreshold;
	public float wordSimilarityHeadingsLowThreshold;
	public float wordSimilarityHeadingsHighThreshold;
	public float wordSimilarityDocstartLowThreshold;
	public int wordMinHeadingItems;
	public int wordMinPLabelItems;
	public int wordMinStyleItems;
	public int wordMinStyleSequenceItems;
	public int wordMinLetterHeadItems;
	
	public WordSimilarityParameters(float wordSimilarityContentHighThreshold, float wordSimilarityContentLowThreshold,
			float wordSimilarityStyleHighThreshold, float wordSimilarityStyleLowThreshold,
			float wordSimilarityHeadingsLowThreshold, float wordSimilarityHeadingsHighThreshold,
			float wordSimilarityDocstartLowThreshold, int wordMinHeadingItems, int wordMinPLabelItems,
			int wordMinStyleItems, int wordMinStyleSequenceItems, int wordMinLetterHeadItems) {
		super();
		this.wordSimilarityContentHighThreshold = wordSimilarityContentHighThreshold;
		this.wordSimilarityContentLowThreshold = wordSimilarityContentLowThreshold;
		this.wordSimilarityStyleHighThreshold = wordSimilarityStyleHighThreshold;
		this.wordSimilarityStyleLowThreshold = wordSimilarityStyleLowThreshold;
		this.wordSimilarityHeadingsLowThreshold = wordSimilarityHeadingsLowThreshold;
		this.wordSimilarityHeadingsHighThreshold = wordSimilarityHeadingsHighThreshold;
		this.wordSimilarityDocstartLowThreshold = wordSimilarityDocstartLowThreshold;
		this.wordMinHeadingItems = wordMinHeadingItems;
		this.wordMinPLabelItems = wordMinPLabelItems;
		this.wordMinStyleItems = wordMinStyleItems;
		this.wordMinStyleSequenceItems = wordMinStyleSequenceItems;
		this.wordMinLetterHeadItems = wordMinLetterHeadItems;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("{wordSimilarityContentHighThreshold=");
		builder.append(wordSimilarityContentHighThreshold);
		builder.append(", wordSimilarityContentLowThreshold=");
		builder.append(wordSimilarityContentLowThreshold);
		builder.append(", wordSimilarityStyleHighThreshold=");
		builder.append(wordSimilarityStyleHighThreshold);
		builder.append(", wordSimilarityStyleLowThreshold=");
		builder.append(wordSimilarityStyleLowThreshold);
		builder.append(", wordSimilarityHeadingsLowThreshold=");
		builder.append(wordSimilarityHeadingsLowThreshold);
		builder.append(", wordSimilarityHeadingsHighThreshold=");
		builder.append(wordSimilarityHeadingsHighThreshold);
		builder.append(", wordSimilarityDocstartLowThreshold=");
		builder.append(wordSimilarityDocstartLowThreshold);
		builder.append(", wordMinHeadingItems=");
		builder.append(wordMinHeadingItems);
		builder.append(", wordMinPLabelItems=");
		builder.append(wordMinPLabelItems);
		builder.append(", wordMinStyleItems=");
		builder.append(wordMinStyleItems);
		builder.append(", wordMinStyleSequenceItems=");
		builder.append(wordMinStyleSequenceItems);
		builder.append(", wordMinLetterHeadItems=");
		builder.append(wordMinLetterHeadItems);
		builder.append("}");
		return builder.toString();
	}
	
}
