package com.cla.common.domain.dto.jobmanager;

import com.cla.common.domain.dto.crawler.PhaseDetailsDto;
import com.cla.common.domain.dto.filetree.CrawlRunDetailsDto;

import java.util.List;

public class RunSummaryInfoDto {

    private List<PhaseDetailsDto> jobs;

    private CrawlRunDetailsDto crawlRunDetailsDto;


    public List<PhaseDetailsDto> getJobs() {
        return jobs;
    }

    public void setJobs(List<PhaseDetailsDto> jobs) {
        this.jobs = jobs;
    }

    public CrawlRunDetailsDto getCrawlRunDetailsDto() {
        return crawlRunDetailsDto;
    }

    public void setCrawlRunDetailsDto(CrawlRunDetailsDto crawlRunDetailsDto) {
        this.crawlRunDetailsDto = crawlRunDetailsDto;
    }

    public String getRunningJobName() {
        if (jobs == null || jobs.isEmpty()) return "";
        return jobs.get(0).getJobType().name();
    }

    @Override
    public String toString() {
        return "RunSummaryInfoDto{" +
                ", jobs='" + jobs + '\'' +
                ", crawlRunDetailsDto='" + crawlRunDetailsDto + '\'' +
                '}';
    }

}
