package com.cla.common.domain.dto.mcf.mcf_api;

import java.io.Serializable;

/**
 * Created by uri on 07/07/2016.
 */
public class McfConfigurationParameterDto implements Serializable {

    String _value_;

    String _attribute_name;

    public McfConfigurationParameterDto() {
    }

    public McfConfigurationParameterDto(String key, String value) {
        this._attribute_name = key;
        this._value_ = value;
    }

    public String get_value_() {
        return _value_;
    }

    public void set_value_(String _value_) {
        this._value_ = _value_;
    }

    public String get_attribute_name() {
        return _attribute_name;
    }

    public void set_attribute_name(String _attribute_name) {
        this._attribute_name = _attribute_name;
    }

}
