package com.cla.common.domain.dto.bizlist;

/**
 * Created by uri on 20/08/2015.
 */
public enum BizListItemType {
    CONSUMERS, CLIENTS, EMPLOYEES, PROJECTS_SERVICES, SIMPLE;

    public static BizListItemType valueOf(int ordinal) {
        return BizListItemType.values()[ordinal];
    }
}
