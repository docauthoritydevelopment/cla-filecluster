package com.cla.common.domain.dto.mediaproc;

/**
 * Created by: yael
 * Created on: 8/8/2018
 */
public interface TextualResult {
    void handleSearchPatterns(String requestId, SearchPatternServiceInterface searchPatternService);
}
