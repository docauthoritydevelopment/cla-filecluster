package com.cla.common.domain.dto.doctype;

import com.cla.common.domain.dto.AssociableEntityDto;
import com.cla.common.domain.dto.Identifiable;
import com.cla.common.domain.dto.filetag.FileTagTypeSettingsDto;
import com.cla.common.domain.dto.scope.Scope;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

import java.util.List;
import java.util.Arrays;


/**
 * Created by uri on 19/08/2015.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DocTypeDto implements Serializable, Identifiable<Long>, AssociableEntityDto {
    public static final String ID_SEPERATOR = ".";
    public static final String ID_PREFIX = "dt.";
    public final static String SEPARATOR = "\\";
    private Long id;
    private String name;
    private String uniqueName;
    private Long parentId;
    private String description;
    private String alternativeName;
    private boolean singleValue;
    private String docTypeIdentifier;

    private Long[] parents;

    private boolean implicit;
    private Scope scope;
    private List<FileTagTypeSettingsDto> fileTagTypeSettings;

    private boolean deleted;
    private String path;

    public DocTypeDto() {
    }

    public DocTypeDto(String name) {
        this.name = name;
    }

    public DocTypeDto(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public Long getParentId() {
        return parentId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "DocTypeDto{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", parentId=" + parentId +
                ", description='" + description + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DocTypeDto that = (DocTypeDto) o;

        if (singleValue != that.singleValue) return false;
        if (implicit != that.implicit) return false;
        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (uniqueName != null ? !uniqueName.equals(that.uniqueName) : that.uniqueName != null) return false;
        if (parentId != null ? !parentId.equals(that.parentId) : that.parentId != null) return false;
        if (description != null ? !description.equals(that.description) : that.description != null) return false;
        if (alternativeName != null ? !alternativeName.equals(that.alternativeName) : that.alternativeName != null)
            return false;
        // Probably incorrect - comparing Object[] arrays with Arrays.equals
        if (!Arrays.equals(parents, that.parents)) return false;
        return scope != null ? scope.equals(that.scope) : that.scope == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (uniqueName != null ? uniqueName.hashCode() : 0);
        result = 31 * result + (parentId != null ? parentId.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (alternativeName != null ? alternativeName.hashCode() : 0);
        result = 31 * result + (singleValue ? 1 : 0);
        result = 31 * result + Arrays.hashCode(parents);
        result = 31 * result + (implicit ? 1 : 0);
        result = 31 * result + (scope != null ? scope.hashCode() : 0);
        return result;
    }

    public Long[] getParents() {
        return parents;
    }

    public void setParents(Long[] parents) {
        this.parents = parents;
    }

    public boolean isImplicit() {
        return implicit;
    }

    public void setImplicit(boolean implicit) {
        this.implicit = implicit;
    }

    public String getUniqueName() {
        return uniqueName;
    }

    public void setUniqueName(String uniqueName) {
        this.uniqueName = uniqueName;
    }

    public String getAlternativeName() {
        return alternativeName;
    }

    public void setAlternativeName(String alternativeName) {
        this.alternativeName = alternativeName;
    }

    public boolean isSingleValue() {
        return singleValue;
    }

    public void setSingleValue(boolean singleValue) {
        this.singleValue = singleValue;
    }

    public void setScope(Scope scope) {
        this.scope = scope;
    }

    public Scope getScope() {
        return scope;
    }

    public void setFileTagTypeSettings(List<FileTagTypeSettingsDto> fileTagTypeSettings) {
        this.fileTagTypeSettings = fileTagTypeSettings;
    }

    public List<FileTagTypeSettingsDto> getFileTagTypeSettings() {
        return fileTagTypeSettings;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public String getDocTypeIdentifier() {
        return docTypeIdentifier;
    }

    public void setDocTypeIdentifier(String docTypeIdentifier) {
        this.docTypeIdentifier = docTypeIdentifier;
    }

    @Override
    public boolean isIdMatch(Object idToCheck) {
        return idToCheck != null && idToCheck instanceof Long && idToCheck.equals(id);
    }

    @JsonIgnore
    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getFullName() {
        return path == null ? name : path + SEPARATOR + name;
    }

    @Override
    @JsonIgnore
    public String createIdentifierString() {
        return getDocTypeIdentifier();
    }

    public DocTypeDto(DocTypeDto other) {
        this.id = other.id;
        this.name = other.name;
        this.uniqueName = other.uniqueName;
        this.parentId = other.parentId;
        this.description = other.description;
        this.alternativeName = other.alternativeName;
        this.singleValue = other.singleValue;
        this.docTypeIdentifier = other.docTypeIdentifier;
        this.parents = other.parents;
        this.implicit = other.implicit;
        this.scope = other.scope;
        this.fileTagTypeSettings = other.fileTagTypeSettings;
        this.deleted = other.deleted;
        this.path = other.path;
    }
}
