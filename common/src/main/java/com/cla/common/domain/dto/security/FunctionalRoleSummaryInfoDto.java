package com.cla.common.domain.dto.security;

import java.util.List;

public class FunctionalRoleSummaryInfoDto {
    private FunctionalRoleDto functionalRoleDto;
    private List<LdapGroupMappingDto> ldapGroupMappingDtos;


    public FunctionalRoleDto getFunctionalRoleDto() {
        return functionalRoleDto;
    }

    public void setFunctionalRoleDto(FunctionalRoleDto functionalRoleDto) {
        this.functionalRoleDto = functionalRoleDto;
    }

    public List<LdapGroupMappingDto> getLdapGroupMappingDtos() {
        return ldapGroupMappingDtos;
    }

    public void setLdapGroupMappingDtos(List<LdapGroupMappingDto> ldapGroupMappingDtos) {
        this.ldapGroupMappingDtos = ldapGroupMappingDtos;
    }


    @Override
    public String toString() {
        return "FunctionalRoleSummaryInfoDto{" +
                ", functionalRoleDto='" + functionalRoleDto + '\'' +
                ", ldapGroupMappingDtos='" + ldapGroupMappingDtos + '\'' +
                '}';
    }
}
