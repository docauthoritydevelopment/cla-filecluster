package com.cla.common.domain.dto.filetag;

import com.cla.connector.domain.dto.JsonSerislizableVisible;

import java.util.Objects;

/**
 * Created by uri on 20/09/2016.
 */
public class FileTagMinimalDto implements JsonSerislizableVisible {

    long id;
    String name;

    long typeId;
    String typeName;

    boolean sensitive;

    public FileTagMinimalDto() {
    }

    public FileTagMinimalDto(long id, String name, FileTagTypeDto type) {
        this.id = id;
        this.name = name;
        if (type != null) {
            this.typeId = type.getId();
            this.typeName = type.getName();
            this.sensitive = type.isSensitive();
        }
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getTypeId() {
        return typeId;
    }

    public void setTypeId(long typeId) {
        this.typeId = typeId;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    @Override
    public String toString() {
        return "FileTagMinimalDto{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", typeId=" + typeId +
                ", typeName='" + typeName + '\'' +
                '}';
    }

    public boolean isSensitive() {
        return sensitive;
    }

    public void setSensitive(boolean sensitive) {
        this.sensitive = sensitive;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FileTagMinimalDto that = (FileTagMinimalDto) o;
        return id == that.id &&
                typeId == that.typeId &&
                sensitive == that.sensitive &&
                Objects.equals(name, that.name) &&
                Objects.equals(typeName, that.typeName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, typeId, typeName, sensitive);
    }
}
