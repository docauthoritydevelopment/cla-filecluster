package com.cla.common.domain.dto.filesearchpatterns;

import java.io.Serializable;
import java.util.List;

public class TextSearchPatternDto  implements Serializable {

    private int id;

    private String name;

    private String pattern;

    private String validatorClass;

    PatternType patternType;

    String description;

    boolean active;

    private Integer subCategoryId;

    private Integer categoryId;

    private String categoryName;

    private String subCategoryName;

    private List<Integer> regulationIds;

    private String regulationStr;

    private boolean subCategoryActive;


    public TextSearchPatternDto() {
    }

    public String getRegulationStr() {
        return regulationStr;
    }

    public void setRegulationStr(String regulationStr) {
        this.regulationStr = regulationStr;
    }

    public Integer getSubCategoryId() {
        return subCategoryId;
    }

    public void setSubCategoryId(Integer subCategoryId) {
        this.subCategoryId = subCategoryId;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getSubCategoryName() {
        return subCategoryName;
    }

    public void setSubCategoryName(String subCategoryName) {
        this.subCategoryName = subCategoryName;
    }

    public List<Integer> getRegulationIds() {
        return regulationIds;
    }

    public void setRegulationIds(List<Integer> regulationIds) {
        this.regulationIds = regulationIds;
    }

    public TextSearchPatternDto(final String name) {
        this.name = name;
    }

    public TextSearchPatternDto(Integer id, String name, String pattern, final Boolean subCategoryActive) {
        this(id, name, pattern, true, subCategoryActive);
    }

    public TextSearchPatternDto(Integer id, String name, String pattern, final boolean active, final Boolean subCategoryActive) {
        this.name = name;
        this.pattern = pattern;
        this.id = id;
        this.active = active;
        this.subCategoryActive = subCategoryActive;
    }


    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }


    public PatternType getPatternType() {
        return patternType;
    }

    public void setPatternType(PatternType patternType) {
        this.patternType = patternType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getPattern() {
        return pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }

    public String getValidatorClass() {
        return validatorClass;
    }

    public void setValidatorClass(String validatorClass) {
        this.validatorClass = validatorClass;
    }

    public boolean isSubCategoryActive() {
        return subCategoryActive;
    }

    public void setSubCategoryActive(boolean subCategoryActive) {
        this.subCategoryActive = subCategoryActive;
    }

    @Override
    public String toString() {
        /*
        sb.append(", subCategoryId='").append(subCategoryId).append('\'');
        sb.append(", categoryId='").append(categoryId).append('\'');
        sb.append(", categoryName='").append(categoryName!= null ? categoryName : "").append('\'');
        sb.append(", subCategoryName='").append(subCategoryName != null ? subCategoryName : "").append('\'');
        sb.append(", regulationIds='").append(regulationIds!= null  ? regulationIds.toString() : "").append('\'');
        */
        return "TextSearchPatternDto{" + "id=" + id +
                ", name='" + name + '\'' +
                ", active=" + active +
                ", pattern='" + pattern + '\'' +
                ", validatorClass='" + validatorClass + '\'' +
                ", patternType=" + patternType +
                ", category=" + categoryName +
                ", subCategory=" + subCategoryName +
                ", subCategoryActive=" + subCategoryActive +
                ", regulations=" + String.join(",",regulationStr) +
                ", description='" + description + '\'' +
                '}';
    }
}
