package com.cla.common.domain.dto.pv;

public class ExcelSimilarityParameters  {
	public int excelMinHeadingCountThreshold;
	public int excelMinStyleCountThreshold;
	public int excelMinLayoutCountThreshold;
	public int excelMinValueThreshold;
	public int excelMinConcreteValueThreshold;
	public int excelMinFormulaThreshold;
	public float excelSimilarityHighContentThreshold;
	public float excelSimilarityHighStyleThreshold;
	public float excelSimilarityHighLayoutThreshold;
	public float excelSimilarityHeadingHighSimThreshold;
	public float excelWorkbookAggregatedSimilarityThreshold;
	
	public ExcelSimilarityParameters(int excelMinHeadingCountThreshold, int excelMinStyleCountThreshold,
			int excelMinLayoutCountThreshold, int excelMinValueThreshold, int excelMinConcreteValueThreshold,
			int excelMinFormulaThreshold, float excelSimilarityHighContentThreshold,
			float excelSimilarityHighStyleThreshold, float excelSimilarityHighLayoutThreshold,
			float excelSimilarityHeadingHighSimThreshold, float excelWorkbookAggregatedSimilarityThreshold) {
		super();
		this.excelMinHeadingCountThreshold = excelMinHeadingCountThreshold;
		this.excelMinStyleCountThreshold = excelMinStyleCountThreshold;
		this.excelMinLayoutCountThreshold = excelMinLayoutCountThreshold;
		this.excelMinValueThreshold = excelMinValueThreshold;
		this.excelMinConcreteValueThreshold = excelMinConcreteValueThreshold;
		this.excelMinFormulaThreshold = excelMinFormulaThreshold;
		this.excelSimilarityHighContentThreshold = excelSimilarityHighContentThreshold;
		this.excelSimilarityHighStyleThreshold = excelSimilarityHighStyleThreshold;
		this.excelSimilarityHighLayoutThreshold = excelSimilarityHighLayoutThreshold;
		this.excelSimilarityHeadingHighSimThreshold = excelSimilarityHeadingHighSimThreshold;
		this.excelWorkbookAggregatedSimilarityThreshold = excelWorkbookAggregatedSimilarityThreshold;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("{excelMinHeadingCountThreshold=");
		builder.append(excelMinHeadingCountThreshold);
		builder.append(", excelMinStyleCountThreshold=");
		builder.append(excelMinStyleCountThreshold);
		builder.append(", excelMinLayoutCountThreshold=");
		builder.append(excelMinLayoutCountThreshold);
		builder.append(", excelMinValueThreshold=");
		builder.append(excelMinValueThreshold);
		builder.append(", excelMinConcreteValueThreshold=");
		builder.append(excelMinConcreteValueThreshold);
		builder.append(", excelMinFormulaThreshold=");
		builder.append(excelMinFormulaThreshold);
		builder.append(", excelSimilarityHighContentThreshold=");
		builder.append(excelSimilarityHighContentThreshold);
		builder.append(", excelSimilarityHighStyleThreshold=");
		builder.append(excelSimilarityHighStyleThreshold);
		builder.append(", excelSimilarityHighLayoutThreshold=");
		builder.append(excelSimilarityHighLayoutThreshold);
		builder.append(", excelSimilarityHeadingHighSimThreshold=");
		builder.append(excelSimilarityHeadingHighSimThreshold);
		builder.append(", excelWorkbookAggregatedSimilarityThreshold=");
		builder.append(excelWorkbookAggregatedSimilarityThreshold);
		builder.append("}");
		return builder.toString();
	}
	
}
