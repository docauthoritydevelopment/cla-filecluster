package com.cla.common.domain.dto.mcf.mcf_api.job;

import java.util.List;

/**
 * Created by uri on 18/07/2016.
 */
public class McfJobStatusWrapperDto {

    List<McfJobStatusDto> jobstatus;

    public List<McfJobStatusDto> getJobstatus() {
        return jobstatus;
    }

    public void setJobstatus(List<McfJobStatusDto> jobstatus) {
        this.jobstatus = jobstatus;
    }
}
