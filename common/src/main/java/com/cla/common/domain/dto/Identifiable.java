package com.cla.common.domain.dto;

public interface Identifiable<T> {

    T getId();

    void setId(T id);
}
