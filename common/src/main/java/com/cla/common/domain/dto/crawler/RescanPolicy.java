package com.cla.common.domain.dto.crawler;

/**
 * Created by uri on 08/03/2016.
 */
public enum RescanPolicy {
    FULL, DYNAMIC
}
