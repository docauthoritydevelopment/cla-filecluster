package com.cla.common.domain.dto.filetag;

import com.cla.common.utils.AssociationIdUtils;

/**
 * Created by: yael
 * Created on: 5/5/2019
 */
public interface FileTagInterface {

    Long getId();

    Long getTypeId();

    Long getDocTypeContextId();

    String getEntityIdContext();

    FileTagAction getAction();

    boolean isTypeHidden();

    boolean isSingleValueTagType();

    default String createFileTagIdentifierString() {
        return AssociationIdUtils.createTag2Identifier(this);
    }
}
