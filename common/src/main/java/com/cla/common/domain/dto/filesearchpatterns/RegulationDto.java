package com.cla.common.domain.dto.filesearchpatterns;

import java.io.Serializable;
import java.util.List;

public class RegulationDto implements Serializable {

    private int id;

    private String name;

    private boolean active;

    private List<TextSearchPatternDto> patterns;

    public List<TextSearchPatternDto> getPatterns() {
        return patterns;
    }

    public void setPatterns(List<TextSearchPatternDto> patterns) {
        this.patterns = patterns;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("RegulationDto{");
        sb.append("id=").append(id);
        sb.append(", name='").append(name).append('\'');
        sb.append(", active=").append(active);
        sb.append("}");
        return sb.toString();
    }
}
