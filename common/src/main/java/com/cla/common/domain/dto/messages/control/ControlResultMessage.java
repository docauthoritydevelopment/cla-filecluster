package com.cla.common.domain.dto.messages.control;

import com.cla.common.domain.dto.messages.MessagePayloadType;
import com.cla.common.domain.dto.messages.ProcessingPhaseMessage;
import com.cla.common.domain.dto.messages.ResponseMessage;

/**
 * Created by uri on 16-May-17.
 */
public class ControlResultMessage extends ProcessingPhaseMessage<ControlMessagePayload,ControlProcessingError> implements ResponseMessage {

    private String mediaProcessorId;

    private boolean isFirstTimeSinceGoingUp = false;

    public ControlResultMessage() {
        super();
    }

    public ControlResultMessage(ControlMessagePayload payload, MessagePayloadType type, String mediaProcessorId) {
        super(payload);
        setPayloadType(type);
        this.mediaProcessorId = mediaProcessorId;
    }

    public String getMediaProcessorId() {
        return mediaProcessorId;
    }

    public void setMediaProcessorId(String mediaProcessorId) {
        this.mediaProcessorId = mediaProcessorId;
    }

    public boolean isFirstTimeSinceGoingUp() {
        return isFirstTimeSinceGoingUp;
    }

    public void setFirstTimeSinceGoingUp(boolean firstTimeSinceGoingUp) {
        isFirstTimeSinceGoingUp = firstTimeSinceGoingUp;
    }

    @Override
    public String toString() {
        return "ControlResultMessage{" +
                "timestamp='"+getSendTime()+'\''+
                " mediaProcessorId='" + mediaProcessorId + '\'' +
                " payloadType='" + getPayloadType() + '\'' +
                " payload='" + getPayload() + '\'' +
                " isFirstTimeSinceGoingUp='" + isFirstTimeSinceGoingUp + '\'' +
                '}';
    }
}
