package com.cla.common.domain.dto.security;

import com.cla.connector.domain.dto.JsonSerislizableVisible;

/**
 * Business Role, composed of individual Roles
 */
public class BizRoleDto extends CompositeRoleDto implements JsonSerislizableVisible {
}
