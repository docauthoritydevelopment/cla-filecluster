package com.cla.common.domain.dto.security;

import com.cla.common.domain.dto.AssociableEntityDto;
import com.cla.connector.domain.dto.JsonSerislizableVisible;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class FunctionalRoleDto extends CompositeRoleDto implements JsonSerislizableVisible, AssociableEntityDto, FunctionalRoleInterface {

    public static final String ID_PREFIX = "frole.";

    private String managerUsername;

    private Boolean implicit;

    public String getManagerUsername() {
        return managerUsername;
    }

    public void setManagerUsername(String managerUsername) {
        this.managerUsername = managerUsername;
    }

    public Boolean getImplicit() {
        return implicit;
    }

    public void setImplicit(Boolean implicit) {
        this.implicit = implicit;
    }

    @Override
    public String toString() {
        return "FunctionalRoleDto{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", displayName='" + displayName + '\'' +
                ", description='" + description + '\'' +
                ", managerUsername='" + managerUsername + '\'' +
                '}';
    }

    @Override
    public boolean isIdMatch(Object idToCheck) {
        return idToCheck != null && idToCheck instanceof Long && getId().equals(idToCheck);
    }

    @JsonIgnore
    @Override
    public String createIdentifierString() {
        return createFuncRoleIdentifierString();
    }

    public FunctionalRoleDto() {
    }

    public FunctionalRoleDto(FunctionalRoleDto other) {
        this.managerUsername = other.managerUsername;
        this.implicit = other.implicit;
        copy(other);
    }
}
