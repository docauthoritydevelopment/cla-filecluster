package com.cla.common.domain.dto.mcf.mcf_api;

import java.io.Serializable;

/**
 * Created by uri on 07/07/2016.
 */
public class OutputConnectionObjectDtoList implements Serializable {

    OutputConnectionObjectDto[] outputconnection;

    public OutputConnectionObjectDtoList() {
    }

    public OutputConnectionObjectDtoList(OutputConnectionObjectDto outputconnection) {
        this.outputconnection = new OutputConnectionObjectDto[1];
        this.outputconnection[0] = outputconnection;
    }

    public OutputConnectionObjectDto[] getOutputconnection() {
        return outputconnection;
    }

    public void setOutputconnection(OutputConnectionObjectDto[] outputconnection) {
        this.outputconnection = outputconnection;
    }
}
