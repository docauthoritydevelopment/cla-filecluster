package com.cla.common.domain.dto.mcf.mcf_api;

import java.io.Serializable;

/**
 * Created by uri on 07/07/2016.
 */
public class McfBaseObjectDto implements Serializable {

    String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
