package com.cla.common.domain.dto.jobmanager;

import com.cla.common.domain.dto.crawler.JobCompletionStatus;

import java.util.Objects;

/**
 * Holds information that is relevant when updating a job's state.
 *
 * Created by Itai Marko.
 */
public class JobStateUpdateInfo {

    private final Long id;
    private final JobState state;
    private final JobCompletionStatus completionStatus;
    private final String stateOpMessage;
    private Boolean acceptingTasks = false;
    private boolean allowPrevStateInactive = true;

    public JobStateUpdateInfo(Long id, JobState state, String stateOpMessage, boolean allowPrevStateInactive) {
        this(id, state, null, stateOpMessage, allowPrevStateInactive);
    }

    public JobStateUpdateInfo(Long id, JobState state, JobCompletionStatus completionStatus, String stateOpMessage, boolean allowPrevStateInactive) {
        this(id, state, completionStatus, stateOpMessage, allowPrevStateInactive, null);
    }

    public JobStateUpdateInfo(Long id, JobState state, JobCompletionStatus completionStatus, String stateOpMessage, boolean allowPrevStateInactive, Boolean acceptingTasks) {
        Objects.requireNonNull(id);
        Objects.requireNonNull(state);
        this.id = id;
        this.state = state;
        this.completionStatus = completionStatus;
        this.stateOpMessage = stateOpMessage;
        this.acceptingTasks = acceptingTasks;
        this.allowPrevStateInactive = allowPrevStateInactive;
    }

    public Long getId() {
        return id;
    }

    public JobState getState() {
        return state;
    }

    public JobCompletionStatus getCompletionStatus() {
        return completionStatus;
    }

    public String getStateOpMessage() {
        return stateOpMessage;
    }

    public Boolean isAcceptingTasks() {
        return acceptingTasks;
    }

    public boolean isAllowPrevStateInactive() {
        return allowPrevStateInactive;
    }
}
