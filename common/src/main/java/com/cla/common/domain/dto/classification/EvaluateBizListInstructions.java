package com.cla.common.domain.dto.classification;

import java.io.Serializable;

/**
 * Created by uri on 13/06/2016.
 */
public class EvaluateBizListInstructions implements Serializable {


    private int classifyMinimumGroupSize;
    private long singleBliMatchTypeId;
    private long multiBliMatchTypeId;
    private double classifcationSingleBliMinimumPercentage;
    private double classificationSingleBliMaxOthersPercentage;
    private int classificationSingleBliMaxOthers;
    private int classificationMultiMatchMinBli;
    private double classificationMultiMatchMinOthersPercentage;
    private long multiMatchTagId;

    public void setClassifyMinimumGroupSize(int classifyMinimumGroupSize) {
        this.classifyMinimumGroupSize = classifyMinimumGroupSize;
    }

    public int getClassifyMinimumGroupSize() {
        return classifyMinimumGroupSize;
    }

    public void setSingleBliMatchTypeId(long singleBliMatchTypeId) {
        this.singleBliMatchTypeId = singleBliMatchTypeId;
    }

    public long getSingleBliMatchTypeId() {
        return singleBliMatchTypeId;
    }

    public void setMultiBliMatchTypeId(long multiBliMatchTypeId) {
        this.multiBliMatchTypeId = multiBliMatchTypeId;
    }

    public long getMultiBliMatchTypeId() {
        return multiBliMatchTypeId;
    }

    public void setClassifcationSingleBliDominantPercentage(double classifcationSingleBluMinimumPercentage) {
        this.classifcationSingleBliMinimumPercentage = classifcationSingleBluMinimumPercentage;
    }

    public double getClassifcationSingleBliMinimumPercentage() {
        return classifcationSingleBliMinimumPercentage;
    }

    public void setClassificationSingleBliMaxOthersPercentage(double classificationSingleBliMaxOthersPercentage) {
        this.classificationSingleBliMaxOthersPercentage = classificationSingleBliMaxOthersPercentage;
    }

    public double getClassificationSingleBliMaxOthersPercentage() {
        return classificationSingleBliMaxOthersPercentage;
    }

    public void setClassificationSingleBliMaxOthers(int classificationSingleBliMaxOthers) {
        this.classificationSingleBliMaxOthers = classificationSingleBliMaxOthers;
    }

    public int getClassificationSingleBliMaxOthers() {
        return classificationSingleBliMaxOthers;
    }

    public void setClassificationMultiMatchMinBli(int classificationMultiMatchMinBli) {
        this.classificationMultiMatchMinBli = classificationMultiMatchMinBli;
    }

    public int getClassificationMultiMatchMinBli() {
        return classificationMultiMatchMinBli;
    }

    public void setClassificationMultiMatchMinOthersPercentage(double classificationMultiMatchMinOthersPercentage) {
        this.classificationMultiMatchMinOthersPercentage = classificationMultiMatchMinOthersPercentage;
    }

    public double getClassificationMultiMatchMinOthersPercentage() {
        return classificationMultiMatchMinOthersPercentage;
    }

    public void setMultiMatchTagId(long multiMatchTagId) {
        this.multiMatchTagId = multiMatchTagId;
    }

    public long getMultiMatchTagId() {
        return multiMatchTagId;
    }
}
