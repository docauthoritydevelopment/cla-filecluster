package com.cla.common.domain.dto.file;

public enum ConstFileTypeCategory {
    OTHER(1000000, "Other");

    private String name;
    private long id;

    ConstFileTypeCategory(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public long getId() {
        return id;
    }
}
