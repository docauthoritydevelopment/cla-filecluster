package com.cla.common.domain.dto.report;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
public class DashboardChartDataDto implements Iterable<DashboardChartSeriesDataDto> {


    private List<DashboardChartSeriesDataDto> series;
    private List<String> categories;
    private List<DashboardChartPointDataDto> points;
    private List<String> categoryIds;
    private Long totalElements;
    private boolean dataMayNotBeAccurate;

    public DashboardChartDataDto(){
        series = new ArrayList<>();
        categories = new ArrayList<>();
        dataMayNotBeAccurate = false;
    }

    @Override
    public Iterator<DashboardChartSeriesDataDto> iterator() {
        return series.iterator();
    }
}
