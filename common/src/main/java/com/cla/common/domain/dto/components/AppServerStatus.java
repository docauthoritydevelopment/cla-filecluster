package com.cla.common.domain.dto.components;

/**
 * Created by: yael
 * Created on: 2/21/2018
 */
public class AppServerStatus extends ComponentStatus {
    private long analyzedTaskRequests;
    private long analyzedTaskRequestsFinished;
    private long analyzedTaskRequestFailure;
    private double averageAnalyzedThroughput = 0;
    private double analyzedRunningThroughput = 0d;

    private long ingestTaskRequests;
    private long ingestTaskRequestsFinished;
    private long ingestTaskRequestFailure;
    private double averageIngestThroughput =0;
    private double ingestRunningThroughput = 0d;

    private double precentageDuplicateContentFound  = 0d;

    private boolean optimizeIndexesRunning = false;

    public long getAnalyzedTaskRequests() {
        return analyzedTaskRequests;
    }

    public void setAnalyzedTaskRequests(long analyzedTaskRequests) {
        this.analyzedTaskRequests = analyzedTaskRequests;
    }

    public long getAnalyzedTaskRequestsFinished() {
        return analyzedTaskRequestsFinished;
    }

    public void setAnalyzedTaskRequestsFinished(long analyzedTaskRequestsFinished) {
        this.analyzedTaskRequestsFinished = analyzedTaskRequestsFinished;
    }

    public long getAnalyzedTaskRequestFailure() {
        return analyzedTaskRequestFailure;
    }

    public void setAnalyzedTaskRequestFailure(long analyzedTaskRequestFailure) {
        this.analyzedTaskRequestFailure = analyzedTaskRequestFailure;
    }

    public double getAverageAnalyzedThroughput() {
        return averageAnalyzedThroughput;
    }

    public void setAverageAnalyzedThroughput(double averageAnalyzedThroughput) {
        this.averageAnalyzedThroughput = averageAnalyzedThroughput;
    }

    public double getAnalyzedRunningThroughput() {
        return analyzedRunningThroughput;
    }

    public void setAnalyzedRunningThroughput(double analyzedRunningThroughput) {
        this.analyzedRunningThroughput = analyzedRunningThroughput;
    }

    public long getIngestTaskRequests() {
        return ingestTaskRequests;
    }

    public void setIngestTaskRequests(long ingestTaskRequests) {
        this.ingestTaskRequests = ingestTaskRequests;
    }

    public long getIngestTaskRequestsFinished() {
        return ingestTaskRequestsFinished;
    }

    public void setIngestTaskRequestsFinished(long ingestTaskRequestsFinished) {
        this.ingestTaskRequestsFinished = ingestTaskRequestsFinished;
    }

    public long getIngestTaskRequestFailure() {
        return ingestTaskRequestFailure;
    }

    public void setIngestTaskRequestFailure(long ingestTaskRequestFailure) {
        this.ingestTaskRequestFailure = ingestTaskRequestFailure;
    }

    public double getAverageIngestThroughput() {
        return averageIngestThroughput;
    }

    public void setAverageIngestThroughput(double averageIngestThroughput) {
        this.averageIngestThroughput = averageIngestThroughput;
    }

    public double getIngestRunningThroughput() {
        return ingestRunningThroughput;
    }

    public void setIngestRunningThroughput(double ingestRunningThroughput) {
        this.ingestRunningThroughput = ingestRunningThroughput;
    }

    public boolean isOptimizeIndexesRunning() {
        return optimizeIndexesRunning;
    }

    public void setOptimizeIndexesRunning(boolean optimizeIndexesRunning) {
        this.optimizeIndexesRunning = optimizeIndexesRunning;
    }

    public double getPrecentageDuplicateContentFound() {
        return precentageDuplicateContentFound;
    }
    public void setPrecentageDuplicateContentFound(double precentageDuplicateContentFound) {
        this.precentageDuplicateContentFound = precentageDuplicateContentFound;
    }

    @Override
    public String toString() {
        return "AppServerStatus{" +
                "analyzedTaskRequests=" + analyzedTaskRequests +
                ", analyzedTaskRequestsFinished=" + analyzedTaskRequestsFinished +
                ", analyzedTaskRequestFailure=" + analyzedTaskRequestFailure +
                ", averageAnalyzedThroughput=" + averageAnalyzedThroughput +
                ", analyzedRunningThroughput=" + analyzedRunningThroughput +
                ", ingestTaskRequests=" + ingestTaskRequests +
                ", ingestTaskRequestsFinished=" + ingestTaskRequestsFinished +
                ", ingestTaskRequestFailure=" + ingestTaskRequestFailure +
                ", averageIngestThroughput=" + averageIngestThroughput +
                ", ingestRunningThroughput=" + ingestRunningThroughput +
                ", precentageDuplicateContentFound=" + precentageDuplicateContentFound +
                ", optimizeIndexesRunning=" + optimizeIndexesRunning +
                '}';
    }
}
