package com.cla.common.domain.dto.components;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by itay on 22/02/2017.
 */
public enum ClaComponentType {
    MEDIA_PROCESSOR,
    BROKER,
    ZOOKEEPER,
    SOLR_CLAFILE,
    SOLR_SIMILARITY,
    APPSERVER,
    DB,
    BROKER_DC,
    SOLR_NODE;

    private static Map<Integer, ClaComponentType> map = new HashMap<Integer, ClaComponentType>();

    static {
        for (ClaComponentType state : ClaComponentType.values()) {
            map.put(state.ordinal(), state);
        }
    }

    public static ClaComponentType valueOf(int state) {
        return map.get(state);
    }

}
