package com.cla.common.domain.dto.schedule;

/**
 * Created by uri on 03/05/2016.
 */
public class SchedulePresetDto {

    Long id;

    String name;

    String cronTemplate;

}
