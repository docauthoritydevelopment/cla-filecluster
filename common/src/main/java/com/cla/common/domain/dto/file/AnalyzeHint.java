package com.cla.common.domain.dto.file;

import com.google.common.collect.Maps;

import java.util.Map;

/**
 * Created by uri on 21/06/2016.
 */
public enum AnalyzeHint {
    NORMAL(0),
    EXCLUDED(1),
    MANUAL(2);

    public static AnalyzeHint convert(String analysisHint) {
        if (analysisHint == null) {
            return NORMAL;
        }
        return AnalyzeHint.valueOf(analysisHint);
    }

    private static final Map<Integer, AnalyzeHint> intMapping = Maps.newHashMap();
    private final int value;

    AnalyzeHint(final int value) {
        this.value = value;
    }

    static {
        for (final AnalyzeHint t : AnalyzeHint.values()) {
            intMapping.put(t.value, t);
        }
    }

    public int toInt() {
        return value;
    }

    public static AnalyzeHint valueOf(final int value) {
        final AnalyzeHint mt = AnalyzeHint.intMapping.get(value);
        if (mt == null) {
            throw new RuntimeException("Invalid value: " + value);
        }
        return mt;
    }
}
