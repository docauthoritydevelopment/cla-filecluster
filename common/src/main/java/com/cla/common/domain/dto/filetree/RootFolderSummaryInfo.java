package com.cla.common.domain.dto.filetree;

import com.cla.common.domain.dto.department.DepartmentDto;
import com.cla.common.domain.dto.schedule.ScheduleGroupDto;
import com.cla.connector.domain.dto.file.FolderExcludeRuleDto;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * Created by uri on 02/05/2016.
 */
public class RootFolderSummaryInfo {

    public static final String SUSPENDED_STATUS = "SUSPENDED";
    public static final String OK_STATUS = "OK";
    public static final String NEW_STATUS = "NEW";
    public static final String STOPPING_STATUS = "STOPPING";

    private CrawlRunDetailsDto crawlRunDetailsDto;

    private RootFolderDto rootFolderDto;

    private String runStatus;

    private Set<ScheduleGroupDto> scheduleGroupDtos;

    private boolean accessible;

    private boolean customerDataCenterReady;

    private boolean accessibleUnknown;

    private DepartmentDto departmentDto;

    private List<FolderExcludeRuleDto> folderExcludeRules;

    public CrawlRunDetailsDto getCrawlRunDetailsDto() {
        return crawlRunDetailsDto;
    }

    public void setCrawlRunDetailsDto(CrawlRunDetailsDto crawlRunDetailsDto) {
        this.crawlRunDetailsDto = crawlRunDetailsDto;
    }

    public RootFolderDto getRootFolderDto() {
        return rootFolderDto;
    }

    public void setRootFolderDto(RootFolderDto rootFolderDto) {
        this.rootFolderDto = rootFolderDto;
    }


    public void setAccessible(boolean accessible) {
        this.accessible = accessible;
    }

    public boolean isAccessible() {
        return accessible;
    }

    public boolean isAccessibleUnknown() {
        return accessibleUnknown;
    }

    public void setAccessibleUnknown(boolean accessibleUnknown) {
        this.accessibleUnknown = accessibleUnknown;
    }

    public String getRunStatus() {
        return runStatus;
    }

    public void setRunStatus(String runStatus) {
        this.runStatus = runStatus;
    }

    @Override
    public String toString() {
        return "RootFolderSummaryInfo{" +
                "crawlRunDetailsDto=" + crawlRunDetailsDto +
                ", rootFolderDto=" + rootFolderDto +
                ", runState=" + runStatus +
                ", accessible=" + accessible +
                ", accessibleUnknown=" + accessibleUnknown +
                ", customerDataCenterReady=" +  customerDataCenterReady+
                '}';
    }

    public ScheduleGroupDto getScheduleGroupDto() {
        return (scheduleGroupDtos == null || scheduleGroupDtos.isEmpty()) ? null : scheduleGroupDtos.iterator().next();
    }

    public Set<ScheduleGroupDto> getScheduleGroupDtos() {
        return scheduleGroupDtos;
    }

    public void setScheduleGroupDtos(Set<ScheduleGroupDto> scheduleGroupDtos) {
        this.scheduleGroupDtos = scheduleGroupDtos;
    }

    public void addScheduleGroupDto(ScheduleGroupDto dto) {
        if (this.scheduleGroupDtos == null) {
            this.scheduleGroupDtos = new HashSet<>();
        }
        this.scheduleGroupDtos.add(dto);
    }

    public boolean isCustomerDataCenterReady() {
        return customerDataCenterReady;
    }

    public void setCustomerDataCenterReady(boolean customerDataCenterReady) {
        this.customerDataCenterReady = customerDataCenterReady;
    }

    public DepartmentDto getDepartmentDto() {
        return departmentDto;
    }

    public void setDepartmentDto(DepartmentDto departmentDto) {
        this.departmentDto = departmentDto;
    }

    public List<FolderExcludeRuleDto> getFolderExcludeRules() {
        return folderExcludeRules;
    }

    public void setFolderExcludeRules(List<FolderExcludeRuleDto> folderExcludeRules) {
        this.folderExcludeRules = folderExcludeRules;
    }
}
