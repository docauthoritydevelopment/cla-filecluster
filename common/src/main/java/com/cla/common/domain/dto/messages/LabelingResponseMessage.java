package com.cla.common.domain.dto.messages;

import com.cla.connector.mediaconnector.labeling.LabelResult;

import java.util.List;

/**
 * Created by: yael
 * Created on: 7/4/2019
 */
public class LabelingResponseMessage extends ProcessingPhaseMessage<LabelTaskParameters, LabelingError> implements ResponseMessage {

    public LabelingResponseMessage() {
    }

    public LabelingResponseMessage(LabelTaskParameters payload) {
        super(payload);
    }

    private List<LabelResult> externalMetadata;

    public List<LabelResult> getExternalMetadata() {
        return externalMetadata;
    }

    public void setExternalMetadata(List<LabelResult> externalMetadata) {
        this.externalMetadata = externalMetadata;
    }

    @Override
    public String toString() {
        return "LabelingResponseMessage{" +
                "externalMetadata=" + externalMetadata +
                "} " + super.toString();
    }
}
