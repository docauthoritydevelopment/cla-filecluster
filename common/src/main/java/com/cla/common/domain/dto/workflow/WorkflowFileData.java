package com.cla.common.domain.dto.workflow;

public class WorkflowFileData {

    private FileAction fileAction;
    private FileState fileState;

    public void setFileAction(FileAction fileAction) {
        this.fileAction = fileAction;
    }

    public FileAction getFileAction() {
        return fileAction;
    }

    public void setFileState(FileState fileState) {
        this.fileState = fileState;
    }

    public FileState getFileState() {
        return fileState;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        WorkflowFileData that = (WorkflowFileData) o;

        if (fileAction != that.fileAction) return false;
        return fileState == that.fileState;
    }

    @Override
    public int hashCode() {
        int result = fileAction != null ? fileAction.hashCode() : 0;
        result = 31 * result + (fileState != null ? fileState.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "WorkflowFileData{" +
                "fileAction=" + fileAction +
                ", fileState=" + fileState +
                '}';
    }
}
