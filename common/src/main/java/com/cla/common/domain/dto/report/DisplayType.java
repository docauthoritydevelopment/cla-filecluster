package com.cla.common.domain.dto.report;

/**
 * Created by: yael
 * Created on: 3/8/2018
 */
public enum DisplayType {
    CHART,
    DISCOVER,
    DASHBOARD_WIDGET,
    DISCOVER_VIEW,
    DASHBOARD
}
