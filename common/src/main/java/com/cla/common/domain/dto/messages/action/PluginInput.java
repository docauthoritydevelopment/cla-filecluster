package com.cla.common.domain.dto.messages.action;

/**
 * The input to perform the plugin action on
 *
 * Created by: yael
 * Created on: 1/10/2018
 */
public interface PluginInput {
}
