package com.cla.common.domain.dto.filetree;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by uri on 08/03/2016.
 */
public enum StorePurpose {
    UNDEFINED(0),
    PUBLIC(1),
    DEPARTMENT(2),
    PERSONAL(3),
    IT(4),
    SPECIFIC(5),
    OTHER(6)
    ;

    private static Map<Integer, StorePurpose> map = new HashMap<Integer, StorePurpose>();

    static {
        for (StorePurpose state : StorePurpose.values()) {
            map.put(state.ordinal(), state);
        }
    }

    public static StorePurpose valueOf(int state) {
        return map.get(state);
    }

    private int value;

    StorePurpose(int value) {
        this.value = value;
    }
}
