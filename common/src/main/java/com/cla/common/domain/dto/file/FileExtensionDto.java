package com.cla.common.domain.dto.file;



public class FileExtensionDto  {
	private String extension;

	public FileExtensionDto() {} 

	public FileExtensionDto(final String extension) {
		this.extension = extension;
	}

	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	@Override
	public String toString() {
		return "{extension=" + extension + "}";
	}
	
}
