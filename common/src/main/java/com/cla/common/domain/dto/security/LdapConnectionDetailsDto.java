package com.cla.common.domain.dto.security;

/**
 * Created by liora on 02/07/2017.
 */
public class LdapConnectionDetailsDto {

    private Long id;

    private String name;

    private LdapServerDialect serverDialect;

    private boolean ssl;

    private String host;

    private Integer port;

    private String manageDN;

    private String managerPass;

    private boolean enabled = true;

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LdapServerDialect getServerDialect() {
        return serverDialect;
    }

    public void setServerDialect(LdapServerDialect serverDialect) {
        this.serverDialect = serverDialect;
    }

    public boolean isSsl() {
        return ssl;
    }

    public void setSsl(boolean ssl) {
        this.ssl = ssl;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public String getManageDN() {
        return manageDN;
    }

    public void setManageDN(String manageDN) {
        this.manageDN = manageDN;
    }


    public String getManagerPass() {
        return managerPass;
    }

    public void setManagerPass(String managerPass) {
        this.managerPass = managerPass;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public String toString() {
        return "LdapConnectionDetailsDto{" +
                "name=" + name +
                ", id='" + id + '\'' +
                ", ssl=" + ssl +
                ", host=" + host +
                ", port=" + port +
                ", manageDN=" + manageDN +
                ", managerPass=" + managerPass +
                '}';
    }

    public void setId(Long id) {
        this.id = id;
    }
}
