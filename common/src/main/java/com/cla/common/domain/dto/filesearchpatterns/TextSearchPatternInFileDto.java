package com.cla.common.domain.dto.filesearchpatterns;

import java.io.Serializable;

public class TextSearchPatternInFileDto implements Serializable {

    private int id;

    private String name;

    private Integer count;

    private String categoryName;

    private String subCategoryName;

    private boolean multi;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public TextSearchPatternInFileDto(Integer id, String name) {
        this.name = name;
        this.id = id;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getSubCategoryName() {
        return subCategoryName;
    }

    public void setSubCategoryName(String subCategoryName) {
        this.subCategoryName = subCategoryName;
    }

    public boolean isMulti() {
        return multi;
    }

    public void setMulti(boolean multi) {
        multi = multi;
    }

    @Override
    public String toString() {
        return "TextSearchPatternDto{" + "id=" + id +
                ", name='" + name + '\'' +
                ", categoryName='" + categoryName + '\'' +
                ", subCategoryName='" + subCategoryName + '\'' +
                ", count='" + count+ '\'' +
                '}';
    }
}
