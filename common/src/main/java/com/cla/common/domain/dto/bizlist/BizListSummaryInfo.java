package com.cla.common.domain.dto.bizlist;

import com.cla.common.domain.dto.filetree.CrawlRunDetailsDto;

public class BizListSummaryInfo {

    private BizListDto bizListDto;
    private CrawlRunDetailsDto crawlRunDetailsDto;
    private Long itemsCount;
    private Long activeItemsCount;

    public BizListDto getBizListDto() {
        return bizListDto;
    }

    public void setBizListDto(BizListDto bizListDto) {
        this.bizListDto = bizListDto;
    }

    public CrawlRunDetailsDto getCrawlRunDetailsDto() {
        return crawlRunDetailsDto;
    }

    public void setCrawlRunDetailsDto(CrawlRunDetailsDto crawlRunDetailsDto) {
        this.crawlRunDetailsDto = crawlRunDetailsDto;
    }

    public Long getItemsCount() {
        return itemsCount;
    }

    public void setItemsCount(Long itemsCount) {
        this.itemsCount = itemsCount;
    }

    public Long getActiveItemsCount() {
        return activeItemsCount;
    }

    public void setActiveItemsCount(Long activeItemsCount) {
        this.activeItemsCount = activeItemsCount;
    }

    @Override
    public String toString() {
        return "BizListSummaryInfo{" +
                ", bizListDto='" + bizListDto + '\'' +
                ", crawlRunDetailsDto='" + crawlRunDetailsDto + '\'' +
                ", itemsCount='" + itemsCount + '\'' +
                ", activeItemsCount='" + activeItemsCount + '\'' +
                '}';
    }
}
