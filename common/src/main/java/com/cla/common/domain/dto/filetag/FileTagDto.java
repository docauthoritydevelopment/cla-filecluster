package com.cla.common.domain.dto.filetag;

import com.cla.common.domain.dto.AssociableEntityDto;
import com.cla.connector.domain.dto.JsonSerislizableVisible;
import com.cla.common.domain.dto.Identifiable;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Created by uri on 24/08/2015.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FileTagDto implements JsonSerislizableVisible, Identifiable<Long>, AssociableEntityDto, FileTagInterface {

    //FIXME: This is a temporary behavior, once we start using scopes we must implement a tag prefix (same as department and doc types)
    public static final String ID_PREFIX = "g.";

    private Long id;

    private String name;

    private FileTagTypeDto type;

    private FileTagAction fileTagAction;

    private String description;

    private String alternativeName;

    private Long docTypeContextId;

    private String docTypeContextName;

    private String entityIdContextId;

    private String entityIdContextName;

    private boolean implicit;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public FileTagTypeDto getType() {
        return type;
    }

    public void setType(FileTagTypeDto type) {
        this.type = type;
    }

    public FileTagAction getFileTagAction() {
        return fileTagAction;
    }

    public void setFileTagAction(FileTagAction fileTagAction) {
        this.fileTagAction = fileTagAction;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getDocTypeContextId() {
        return docTypeContextId;
    }

    public void setDocTypeContextId(Long docTypeContextId) {
        this.docTypeContextId = docTypeContextId;
    }

    public String getDocTypeContextName() {
        return docTypeContextName;
    }

    public void setDocTypeContextName(String docTypeContextName) {
        this.docTypeContextName = docTypeContextName;
    }

    public String getEntityIdContextId() {
        return entityIdContextId;
    }

    public void setEntityIdContextId(String entityIdContextId) {
        this.entityIdContextId = entityIdContextId;
    }

    public String getEntityIdContextName() {
        return entityIdContextName;
    }

    public void setEntityIdContextName(String entityIdContextName) {
        this.entityIdContextName = entityIdContextName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FileTagDto that = (FileTagDto) o;

        if (implicit != that.implicit) return false;
        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (type != null ? !type.equals(that.type) : that.type != null) return false;
        if (fileTagAction != that.fileTagAction) return false;
        if (description != null ? !description.equals(that.description) : that.description != null) return false;
        if (alternativeName != null ? !alternativeName.equals(that.alternativeName) : that.alternativeName != null)
            return false;
        if (docTypeContextId != null ? !docTypeContextId.equals(that.docTypeContextId) : that.docTypeContextId != null)
            return false;
        if (docTypeContextName != null ? !docTypeContextName.equals(that.docTypeContextName) : that.docTypeContextName != null)
            return false;
        if (entityIdContextId != null ? !entityIdContextId.equals(that.entityIdContextId) : that.entityIdContextId != null)
            return false;
        return entityIdContextName != null ? entityIdContextName.equals(that.entityIdContextName) : that.entityIdContextName == null;
    }

    @Override
    public String toString() {
        return "FileTagDto{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", type=" + type +
                '}';
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (fileTagAction != null ? fileTagAction.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (alternativeName != null ? alternativeName.hashCode() : 0);
        result = 31 * result + (docTypeContextId != null ? docTypeContextId.hashCode() : 0);
        result = 31 * result + (docTypeContextName != null ? docTypeContextName.hashCode() : 0);
        result = 31 * result + (entityIdContextId != null ? entityIdContextId.hashCode() : 0);
        result = 31 * result + (entityIdContextName != null ? entityIdContextName.hashCode() : 0);
        result = 31 * result + (implicit ? 1 : 0);
        return result;
    }

    public String getAlternativeName() {
        return alternativeName;
    }

    public void setAlternativeName(String alternativeName) {
        this.alternativeName = alternativeName;
    }

    public boolean isImplicit() {
        return implicit;
    }

    public void setImplicit(boolean implicit) {
        this.implicit = implicit;
    }

    public String getTypeName() {
        return type == null ? null : type.getName();
    }

    @Override
    public boolean isIdMatch(Object idToCheck) {
        return idToCheck != null && idToCheck instanceof Long && new Long(id).equals(idToCheck);
    }

    @JsonIgnore
    @Override
    public Long getTypeId() {
        return getType().getId();
    }

    @JsonIgnore
    @Override
    public String getEntityIdContext() {
        return entityIdContextId;
    }

    @JsonIgnore
    @Override
    public FileTagAction getAction() {
        return fileTagAction;
    }

    @JsonIgnore
    @Override
    public boolean isTypeHidden() {
        return type.isHidden();
    }

    @JsonIgnore
    @Override
    public String createIdentifierString() {
        return createFileTagIdentifierString();
    }

    @JsonIgnore
    @Override
    public boolean isSingleValueTagType() {
        return type.isSingleValueTag();
    }

    public FileTagDto() {}

    public FileTagDto(FileTagDto other) {
        this.id = other.id;
        this.name = other.name;
        this.type = other.type;
        this.fileTagAction = other.fileTagAction;
        this.description = other.description;
        this.alternativeName = other.alternativeName;
        this.docTypeContextId = other.docTypeContextId;
        this.docTypeContextName = other.docTypeContextName;
        this.entityIdContextId = other.entityIdContextId;
        this.entityIdContextName = other.entityIdContextName;
        this.implicit = other.implicit;
    }
}
