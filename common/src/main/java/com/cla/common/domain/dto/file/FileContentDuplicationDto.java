package com.cla.common.domain.dto.file;

public class FileContentDuplicationDto {
	private final String name;
	private final String id;

	public FileContentDuplicationDto(final String id, final String name ) {
		this.name = name;
		this.id = id;
	}
	

	public String getName() {
		return name;
	}
	public String getId() {
		return id;
	}

	@Override
	public String toString() {
		return "{name=" + name+ "}";
	}
	
}
