package com.cla.common.domain.dto.jobmanager;

/**
 * Created by: yael
 * Created on: 5/27/2018
 */
public enum PauseReason {
    USER_INITIATED,
    SOLR_DOWN,
    NO_AVAILABLE_MEDIA_PROCESSOR,
    SYSTEM_INITIATED,
    LICENSE_EXPIRED,
    SYSTEM_ERROR,
    KVDB_ERROR
}