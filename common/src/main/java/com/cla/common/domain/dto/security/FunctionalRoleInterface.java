package com.cla.common.domain.dto.security;

import com.cla.common.utils.AssociationIdUtils;

/**
 * Created by: yael
 * Created on: 5/5/2019
 */
public interface FunctionalRoleInterface {
    Long getId();

    default String createFuncRoleIdentifierString() {
        return AssociationIdUtils.createFunctionalRoleIdentfier(this);
    }
}
