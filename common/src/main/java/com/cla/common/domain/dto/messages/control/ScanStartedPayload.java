package com.cla.common.domain.dto.messages.control;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Itai Marko.
 */
public class ScanStartedPayload extends ControlMessagePayload {

    private final long taskId;
    private final long jobId;
    private final boolean firstScan;

    @JsonCreator
    public ScanStartedPayload(@JsonProperty("taskId") long taskId,
                              @JsonProperty("jobId") long jobId,
                              @JsonProperty("firstScan")  boolean firstScan) {
        this.taskId = taskId;
        this.jobId = jobId;
        this.firstScan = firstScan;
    }

    public long getTaskId() {
        return taskId;
    }

    public long getJobId() {
        return jobId;
    }

    public boolean isFirstScan() {
        return firstScan;
    }
}
