package com.cla.common.domain.dto.date;

import java.io.Serializable;

/**
 * Created by uri on 05/06/2016.
 */
public class DateRangePointDto implements Serializable {
//    private Long id;
    private Long absoluteTime;
    private TimePeriod relativePeriod;
    private Integer relativePeriodAmount;
    private DateRangePointType type;

    public DateRangePointDto() {
    }

    public DateRangePointDto(TimePeriod relativePeriod, Integer relativePeriodAmount) {
        this.relativePeriod = relativePeriod;
        this.relativePeriodAmount = relativePeriodAmount;
        this.type = DateRangePointType.RELATIVE;
    }

    public DateRangePointDto(Long absoluteTime) {
        this.absoluteTime = absoluteTime;
        this.type = DateRangePointType.ABSOLUTE;
    }

    public DateRangePointDto(DateRangePointDto other) {
        this.absoluteTime = other.absoluteTime;
        this.relativePeriod = other.relativePeriod;
        this.relativePeriodAmount = other.relativePeriodAmount;
        this.type = other.type;
    }

//    public void setId(Long id) { this.id = id; }
//    public Long getId() { return id; }

    public void setAbsoluteTime(Long absoluteTime) {
        this.absoluteTime = absoluteTime;
    }

    public Long getAbsoluteTime() {
        return absoluteTime;
    }

    public void setRelativePeriod(TimePeriod relativePeriod) {
        this.relativePeriod = relativePeriod;
    }

    public TimePeriod getRelativePeriod() {
        return relativePeriod;
    }

    public void setRelativePeriodAmount(Integer relativePeriodAmount) {
        this.relativePeriodAmount = relativePeriodAmount;
    }

    public Integer getRelativePeriodAmount() {
        return relativePeriodAmount;
    }

    public void setType(DateRangePointType type) {
        this.type = type;
    }

    public DateRangePointType getType() {
        return type;
    }

    @Override
    public String toString() {
        return "DateRangePointDto{" +
//                "id=" + id +
                ", absoluteTime=" + absoluteTime +
                ", relativePeriod=" + relativePeriod +
                ", relativePeriodAmount=" + relativePeriodAmount +
                ", type=" + type +
                '}';
    }

//    public Long relativePeriodToAbsolute() {
//        return (type == DateRangePointType.ABSOLUTE)? null :
//                (System.currentTimeMillis() - TimePeriod.periodsInMs(relativePeriod)*relativePeriodAmount);
//    }


//    public Long getDateValueEstimate() {
//        return (type == DateRangePointType.ABSOLUTE)? getAbsoluteTime() :
//                relativePeriodToAbsolute();
//    }

}
