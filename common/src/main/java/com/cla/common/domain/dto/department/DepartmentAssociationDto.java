package com.cla.common.domain.dto.department;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

/**
 * Created by: yael
 * Created on: 3/24/2019
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DepartmentAssociationDto implements Serializable {

    private Long id;
    private DepartmentDto departmentDto;
    private long associationTimeStamp;
    private Long originalFolderId;
    private boolean deleted;
    private Integer originDepthFromRoot;
    private boolean implicit;

    public DepartmentAssociationDto(Long id, DepartmentDto departmentDto, long associationTimeStamp,
                                    Long originalFolderId, Integer originDepthFromRoot, boolean implicit) {
        this.id = id;
        this.departmentDto = departmentDto;
        this.associationTimeStamp = associationTimeStamp;
        this.originalFolderId = originalFolderId;
        this.originDepthFromRoot = originDepthFromRoot;
        this.implicit = implicit;
    }

    public DepartmentAssociationDto() {
    }

    public DepartmentDto getDepartmentDto() {
        return departmentDto;
    }

    public void setDepartmentDto(DepartmentDto departmentDto) {
        this.departmentDto = departmentDto;
    }

    public long getAssociationTimeStamp() {
        return associationTimeStamp;
    }

    public void setAssociationTimeStamp(long associationTimeStamp) {
        this.associationTimeStamp = associationTimeStamp;
    }

    public Long getOriginalFolderId() {
        return originalFolderId;
    }

    public void setOriginalFolderId(Long originalFolderId) {
        this.originalFolderId = originalFolderId;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Integer getOriginDepthFromRoot() {
        return originDepthFromRoot;
    }

    public void setOriginDepthFromRoot(Integer originDepthFromRoot) {
        this.originDepthFromRoot = originDepthFromRoot;
    }

    public boolean isImplicit() {
        return implicit;
    }

    public void setImplicit(boolean implicit) {
        this.implicit = implicit;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}