package com.cla.common.domain.dto.pv;

import java.util.Objects;

/**
 * Created by: yael
 * Created on: 2/5/2018
 */
public class RefinementRuleDto implements Comparable {

    private Long id;
    private String subgroupId;
    private String rawAnalysisGroupId;
    private String subgroupName;
    private boolean active = false;
    private boolean dirty = false;
    private boolean deleted = false;
    private int priority;
    private RefinementRulePredicate predicate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSubgroupId() {
        return subgroupId;
    }

    public void setSubgroupId(String subgroupId) {
        this.subgroupId = subgroupId;
    }

    public String getRawAnalysisGroupId() {
        return rawAnalysisGroupId;
    }

    public void setRawAnalysisGroupId(String rawAnalysisGroupId) {
        this.rawAnalysisGroupId = rawAnalysisGroupId;
    }

    public String getSubgroupName() {
        return subgroupName;
    }

    public void setSubgroupName(String subgroupName) {
        this.subgroupName = subgroupName;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isDirty() {
        return dirty;
    }

    public void setDirty(boolean dirty) {
        this.dirty = dirty;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public RefinementRulePredicate getPredicate() {
        return predicate;
    }

    public void setPredicate(RefinementRulePredicate predicate) {
        this.predicate = predicate;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    @Override
    public String toString() {
        return "RefinementRuleDto{" +
                "id=" + id +
                ", subgroupId='" + subgroupId + '\'' +
                ", rawAnalysisGroupId='" + rawAnalysisGroupId + '\'' +
                ", subgroupName='" + subgroupName + '\'' +
                ", active=" + active +
                ", dirty=" + dirty +
                ", priority=" + priority +
                '}';
    }

    @Override
    public int compareTo(Object o) {

        final int BEFORE = -1;
        final int EQUAL = 0;
        final int AFTER = 1;

        if (o == null) return AFTER;
        if (this == o) return EQUAL;

        RefinementRuleDto other = (RefinementRuleDto)o;

        if (this.getPriority() != other.getPriority()) {
            return Integer.compare(this.getPriority(), other.getPriority());
        } else {
            return Long.compare(this.getId(), other.getId());
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RefinementRuleDto that = (RefinementRuleDto) o;

        if (id != null && Objects.equals(id, that.id)) return true;

        if (id == null && that.id == null) {
            return active == that.active &&
                    dirty == that.dirty &&
                    deleted == that.deleted &&
                    priority == that.priority &&
                    Objects.equals(subgroupId, that.subgroupId) &&
                    Objects.equals(rawAnalysisGroupId, that.rawAnalysisGroupId) &&
                    Objects.equals(subgroupName, that.subgroupName) &&
                    Objects.equals(predicate, that.predicate);
        }

        return false;
    }

    @Override
    public int hashCode() {
        if (id != null)  return Objects.hash(id);
        return Objects.hash(subgroupId, rawAnalysisGroupId, subgroupName, active, dirty, deleted, priority, predicate);
    }
}
