package com.cla.common.domain.dto.components;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Created by: yael
 * Created on: 6/6/2018
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ClaComponentSummaryDto {

    private ClaComponentDto claComponentDto;
    private String extraMetrics;
    private Long statusTimestamp;
    private String driveData;

    public ClaComponentDto getClaComponentDto() {
        return claComponentDto;
    }

    public void setClaComponentDto(ClaComponentDto claComponentDto) {
        this.claComponentDto = claComponentDto;
    }

    public String getExtraMetrics() {
        return extraMetrics;
    }

    public void setExtraMetrics(String extraMetrics) {
        this.extraMetrics = extraMetrics;
    }

    public Long getStatusTimestamp() {
        return statusTimestamp;
    }

    public void setStatusTimestamp(Long statusTimestamp) {
        this.statusTimestamp = statusTimestamp;
    }

    public String getDriveData() {
        return driveData;
    }

    public void setDriveData(String driveData) {
        this.driveData = driveData;
    }
}
