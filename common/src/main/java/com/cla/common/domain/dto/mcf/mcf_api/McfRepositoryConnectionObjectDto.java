package com.cla.common.domain.dto.mcf.mcf_api;

import java.io.Serializable;
import java.util.Map;

/**
 * Created by uri on 20/07/2016.
 {"isnew":"false","name":"FileSystem","class_name":"org.apache.manifoldcf.crawler.connectors.filesystem.FileConnector","max_connections":"10","description":"","configuration":{}}
 */
public class McfRepositoryConnectionObjectDto implements Serializable {

    String isnew;

    String name;

    String class_name;

    int max_connections;

    String description;

    Map configuration;

    public String getIsnew() {
        return isnew;
    }

    public void setIsnew(String isnew) {
        this.isnew = isnew;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getClass_name() {
        return class_name;
    }

    public void setClass_name(String class_name) {
        this.class_name = class_name;
    }

    public int getMax_connections() {
        return max_connections;
    }

    public void setMax_connections(int max_connections) {
        this.max_connections = max_connections;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Map getConfiguration() {
        return configuration;
    }

    public void setConfiguration(Map configuration) {
        this.configuration = configuration;
    }
}
