package com.cla.common.domain.dto.crawler;

import com.cla.connector.domain.dto.file.DiffType;

import java.io.Serializable;

/**
 * Created by uri on 17/03/2016.
 */
public class CrawlFileChangeLogDto implements Serializable {
    private Long id;
    private Long fileId;
    private DiffType diffType;
    private String fileName;
    private String baseName;

    private String mediaItemId;

    private String folder; //Normalized
    private long rootFolderId;

    private Long runId;

    private Long timeStamp;

    private boolean processed;

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    public Long getFileId() {
        return fileId;
    }

    public void setCrawlEventType(DiffType diffType) {
        this.diffType = diffType;
    }

    public DiffType getCrawlEventType() {
        return diffType;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileName() {
        return fileName;
    }

    public void setRootFolderId(long rootFolderId) {
        this.rootFolderId = rootFolderId;
    }

    public long getRootFolderId() {
        return rootFolderId;
    }

    public boolean isProcessed() {
        return processed;
    }

    public void setProcessed(boolean processed) {
        this.processed = processed;
    }

    public String getBaseName() {
        return baseName;
    }

    public void setBaseName(String baseName) {
        this.baseName = baseName;
    }

    public String getFolder() {
        return folder;
    }

    public void setFolder(String folder) {
        this.folder = folder;
    }

    public Long getRunId() {
        return runId;
    }

    public void setRunId(Long runId) {
        this.runId = runId;
    }

    public Long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getMediaItemId() {
        return mediaItemId;
    }

    public void setMediaItemId(String mediaItemId) {
        this.mediaItemId = mediaItemId;
    }

    @Override
    public String toString() {
        return "CrawlFileChangeLogDto{" +
                "id=" + id +
                ", fileId=" + fileId +
                ", mediaItemId='" + mediaItemId + '\'' +
                ", diffType=" + diffType +
                ", fileName='" + fileName + '\'' +
                ", rootFolderId=" + rootFolderId +
                '}';
    }
}
