package com.cla.common.domain.pst;

import com.cla.connector.domain.dto.media.mail.MailItemType;
import com.cla.connector.mediaconnector.mail.MailRelated;
import com.cla.connector.utils.FileNamingUtils;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.mutable.MutableInt;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;
import java.util.OptionalLong;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static com.cla.connector.domain.dto.media.mail.MailItemType.*;
import static com.cla.connector.utils.FileTypeUtils.PST_FILE_EXT;
import static com.cla.connector.utils.pckg.PackagePathResolver.PACKAGE_SEPARATOR;

/**
 * Represents the path of an entry within a pst file.
 * <p/>
 * An instance of a PstPath may hold an entry ID which locates the entry within the PST file, if such an ID is
 * available for the entry.
 *
 * Created By Itai Marko
 */
public class PstPath {

    //<editor-fold desc="Static Members">

    private static final Logger logger = LoggerFactory.getLogger(PstPath.class);

    /**
     * "*\"
     */
    private static final String PST_ROOT_FOLDER_PATH = PACKAGE_SEPARATOR + File.separator;

    /**
     * ".pst*"
     */
    public static final String PST_PSEUDO_FOLDER_MARKER = "." + PST_FILE_EXT + PACKAGE_SEPARATOR;

    /**
     * ".pst*\"
     */
    private static final String PST_PATH_MARKER = PST_PSEUDO_FOLDER_MARKER + File.separator;

    /**
     * Used in the {@link #uniqueEntryNameRegex} below.
     */
    private static final String entryTypeShortNamesRegex = String.join("|", Arrays.stream(MailItemType.values()).map(entryType -> entryType.shortName).collect(Collectors.toList()));

    /**
     * Used to extract the PST entry name and the unique PST entry ID from a uniquified entry name.
     * NOTE: changing this requires a change to {@link #uniquify(String, long, MailItemType)}. Looks as follows:
     * <p/>
     * "*\(.*) [(fl|em|at)#(\d*)]\..+\z"
     */
    private static final Pattern uniqueEntryNameRegex =
            Pattern.compile(Pattern.quote(PST_ROOT_FOLDER_PATH) + "(.*) " + Pattern.quote("[") + "(" + entryTypeShortNamesRegex + ")#(\\d*)" + Pattern.quote("]") + "\\..+\\z");

    //</editor-fold>

    //<editor-fold desc="Private Fields">

    /**
     * The Path of the containing PST file
     */
    private final Path pstFilePath;

    /**
     * The path of the PST entry within the containing PST file
     */
    private final String entryPath;

    /**
     * An optional ID that specifies the PST entry within the containing PST file. -1 signifies an unspecified entry ID
     */
    private final long entryId;

    /**
     * Represents the type of the PST entry.
     */
    private final MailItemType entryType;

    //</editor-fold>

    //<editor-fold desc="Constructors">

    /**
     * Creates an instance of PstPath representing the root PST file.
     *
     * @param pstFilePath the Path of the PST file
     */
    public PstPath(Path pstFilePath) {
        this(pstFilePath, PST_ROOT_FOLDER_PATH, -1L, FOLDER);
    }

    /**
     * Creates an instance of PstPath representing an entry with the given path within a pst file in the given Path.
     * <p/>
     * @apiNote This is a private constructor. Building instances of PstPath representing entries should be done by
     * either using the {@link #resolveFolder(String)} or {@link #resolveUniqueEntry(String, long, MailItemType, Long)} methods
     * against a parent PstPath instance or using the static {@link #fromString(String)} factory method (using a String
     * produced by a call to toString() of a properly built PstPath instance).
     *
     * @param pstFilePath the Path of the PST file
     * @param entryPath   the path of the entry within the PST file
     * @param entryId     the unique entry ID withing the PST file or zero to signify an unspecified entryId. Must be non negative
     */
    private PstPath(Path pstFilePath, String entryPath, long entryId, MailItemType entryType) {
        Objects.requireNonNull(pstFilePath);
        Objects.requireNonNull(entryPath);
        if (entryId < -1) {
            throw new IllegalArgumentException("EntryId must be -1 or greater. Got: " + entryId + " PST file path: " + pstFilePath + " entryPath: " + entryPath);
        }
        this.pstFilePath = pstFilePath;
        this.entryPath = entryPath;
        this.entryId = entryId;
        this.entryType = entryType;
    }

    //</editor-fold>

    public static boolean isPstEntryPath(String path) {
        return isPstEntryPath(path, new MutableInt());
    }

    private static boolean isPstEntryPath(String path, MutableInt markerIndex) {
        Objects.requireNonNull(markerIndex);
        markerIndex.setValue(-1);
        if (path == null || path.isEmpty()) {
            return false;
        }
        int i = path.toLowerCase().indexOf(PST_PATH_MARKER);
        markerIndex.setValue(i);
        return i != -1;
    }

    public PstPath resolveUniqueEntry(String entryName, long entryId, MailItemType entryType, Long emailMsgId) {
        if (entryType == FOLDER) {
            return resolveFolder(entryName);
        }
        if (entryId < 0L || entryType == null || entryName == null) {
            throw new IllegalArgumentException("EntryId: " + entryId + " was attempted to be resolved with " + entryName + " and entryType: " + entryType+ " against PstPath: " + toString());
        }

        if (entryName.isEmpty()) {
            logger.debug("Got an empty entryName of type {} with id {} to resolve against {}", entryType, entryId, toString());
            entryName = MailRelated.EMPTY_SUBJECT; // TODO Itai: [DAMAIN-3075] [DAMAIN-3763] move to constants
        }

        if (entryType == EMAIL) {
            entryName = MailRelated.changeSubjectToFileName(entryName, String.valueOf(entryId));
        } else if (entryType == ATTACHMENT) {
            entryName = FileNamingUtils.replaceIllegalPathChars(entryName);
            entryName = MailRelated.addHashToBaseName(entryName, String.valueOf(emailMsgId) + entryId);
        }

        entryName = FileNamingUtils.replaceIllegalPathChars(entryName);
        entryName = uniquify(entryName, entryId, entryType);

        return new PstPath(pstFilePath, resolveEntryPath(entryName), entryId, entryType);
    }

    public PstPath resolveFolder(String entryName) {
        if (StringUtils.isEmpty(entryName)) {
            throw new IllegalArgumentException("Empty entryName: " + entryName + " was attempted to be resolved against PstPath: " + toString());
        }

        entryName = FileNamingUtils.replaceIllegalPathChars(entryName);
        return new PstPath(pstFilePath, resolveEntryPath(entryName), -1L, FOLDER);
    }

    private String resolveEntryPath(String entryName) {
        String separator = entryPath.endsWith(File.separator) ? "" : File.separator;
        return entryPath + separator + entryName;
    }

    /**
     * Returns the PstPath toString representation with the uniquifier blocks stripped.
     * For an Attachment this method will also remove the containing email from the path and put the attachment name
     * under the email's parent folder.
     */
    @JsonIgnore
    public String getNonUniquePath() {
        switch (entryType) {
            case FOLDER:
                // No uniquifier segments in pstPath of folder. Just return the original toString
                return toString();

            case EMAIL:
                // Strip the email's uniquifier segment
                String emailUniquifierSegment = getUniquifierSegment(EMAIL, entryId);
                String nonUniqueEntryPath = removeLastOccurrence(entryPath, emailUniquifierSegment);
                return pstFilePath.toString() + nonUniqueEntryPath;

            case ATTACHMENT:
                // get the toStrings of the parent (actually, grandma) folder and this attachments entry name
                Pair<String, String> pairOfContainingEmailAndThisAttachment = splitEntryNameFromParent(toString());
                String containingEmailToString = pairOfContainingEmailAndThisAttachment.getLeft();
                String thisAttachmentUniqueName = pairOfContainingEmailAndThisAttachment.getRight();
                String parentFolderToString = getParentToString(containingEmailToString);
                // Strip the attachment's uniquifier segment
                String attachmentUniquifierSegment = getUniquifierSegment(ATTACHMENT, entryId);
                String thisAttachmentNonUniqueName =
                        removeLastOccurrence(thisAttachmentUniqueName, attachmentUniquifierSegment);
                return parentFolderToString + thisAttachmentNonUniqueName;

            default:
                throw new IllegalStateException("Unsupported entryType:" + entryType + " in PstPath: " + toString());
        }
    }

    private static String removeLastOccurrence(String str, String toRemove) {
        int lastOccurrenceIndex = str.lastIndexOf(toRemove);
        if (lastOccurrenceIndex == -1) {
            return str;
        }

        return str.substring(0, lastOccurrenceIndex) + str.substring(lastOccurrenceIndex + toRemove.length());
    }

    private static String uniquify(String entryPath, long entryId, MailItemType entryType) {
        // NOTE: changing this requires a change to UNIQUE_ENTRY_NAME_REGEX (a static const defined above)
        Pair<String, String> entryNameAndExt = entryType.splitEntryNameFromExt(entryPath);
        String entryName = entryNameAndExt.getLeft();
        String ext = entryNameAndExt.getRight();
        return entryName + getUniquifierSegment(entryType, entryId) + "." + ext;
    }

    private static String getUniquifierSegment(MailItemType entryType, long entryId) {
        if (entryType == FOLDER) {
            throw new UnsupportedOperationException(
                    "Unsupported call to getUniquifierSegment() on MailItemType.FOLDER with entryId: " + entryId);
        }

        return " [" + entryType.shortName + "#" + entryId + "]";
    }

    private boolean isRootPstFolder() {
        return entryPath.equals(PST_ROOT_FOLDER_PATH);
    }

    @JsonIgnore
    public PstPath getParent() {
        if (isRootPstFolder()) {
            return null;
        }
        String thisToString = toString();
        String parentToString = getParentToString(thisToString);
        return fromString(parentToString);
    }

    private String getParentToString(String pstPathToString) {
        return splitEntryNameFromParent(pstPathToString).getLeft();
    }

    private static Pair<String ,String> splitEntryNameFromParent(String pstPathStr) {
        int fileSeparatorLastIndex = pstPathStr.lastIndexOf(File.separatorChar);
        if (fileSeparatorLastIndex == -1) {
            throw new IllegalStateException("No file separator found in PstPath: " + pstPathStr);
        }
        return Pair.of(pstPathStr.substring(0, fileSeparatorLastIndex), pstPathStr.substring(fileSeparatorLastIndex));
    }

    @JsonIgnore
    public Path getPstFilePath() {
        return pstFilePath;
    }

    @JsonIgnore
    public String getEntryPath() {
        return entryPath;
    }

    @JsonIgnore
    public OptionalLong getEntryId() {
        if (entryId >= 0) {
            return OptionalLong.of(entryId);
        }
        return OptionalLong.empty();
    }

    @JsonIgnore
    public MailItemType getEntryType() {
        return entryType;
    }

    @JsonCreator
    public static PstPath fromString(@JsonProperty("pstFullPath") String pstEntryPath) {
        MutableInt pstPathMarkerIndex = new MutableInt();
        if (!isPstEntryPath(pstEntryPath, pstPathMarkerIndex)) {
            throw new IllegalArgumentException("Invalid pstEntryPath: " + pstEntryPath);
        }

        // extract the path of the containing pst file
        int i = pstPathMarkerIndex.getValue();
        String pstFilePathName = pstEntryPath.substring(0, i+4); // i+4 includes the ".pst" extension
        Path pstFilePath = Paths.get(pstFilePathName);

        // extract the path of the pst entry within the containing pst file
        String entryName = pstEntryPath.substring(i+4); // i+4 includes the PST_ROOT_FOLDER_PATH ("*\")
        Matcher matcher = uniqueEntryNameRegex.matcher(entryName);
        if (matcher.find()) {
            // => This is a unique PstPath. Extract the pstEntryName, entryTypeShortName and pstEntryId
            String pstEntryName = matcher.group(1);
            String entryTypeShortName = matcher.group(2);
            String entryIdStr = matcher.group(3);
            long entryId = Long.parseLong(entryIdStr);
            logger.trace("Producing a unique PstPath with ID: {}, pstEntryName: {}, entryName: {} in pstFile: {} from string: {}", entryIdStr, pstEntryName, entryName, pstFilePath, pstEntryPath);
            Optional<MailItemType> mailItemType = MailItemType.forShortName(entryTypeShortName);
            if (mailItemType.isPresent()) {
                return new PstPath(pstFilePath, entryName, entryId, mailItemType.get());
            } else {
                throw new IllegalArgumentException("Invalid mailItemType shortName:" + entryTypeShortName + ", in arg to PstPath.fromString(): " + pstEntryPath);
            }
        } else {
            // => This is a non unique PstPath
            logger.trace("Producing a non unique PstPath with entryName: {} in pstFile: {} from string: {}", entryName, pstFilePath, pstEntryPath);
            return new PstPath(pstFilePath, entryName, -1L, FOLDER);
        }
    }

    @Override
    @JsonProperty("pstFullPath")
    public String toString() {
        return pstFilePath.toString() + entryPath;
    }

}
