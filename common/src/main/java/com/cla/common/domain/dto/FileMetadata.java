package com.cla.common.domain.dto;

import com.cla.common.utils.CsvUtils;
import com.cla.connector.domain.dto.acl.AclAware;
import com.google.common.collect.Lists;

import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Base class for file metadata
 */
@SuppressWarnings("unused")
public abstract class FileMetadata implements AclAware, Serializable {

    private String author;
    private String owner;
    private String company;
    private String subject;
    private String title;
    private String keywords;
    private String appNameVer;
    private String template;

    private Date createDate;
    private Long fsFileSize;
    private Date fsLastModified;
    private Collection<String> aclReadAllowed;
    private Collection<String> aclWriteAllowed;
    private Collection<String> aclReadDenied;
    private Collection<String> aclWriteDenied;

    protected final List<String> proposedTitles = Lists.newArrayList();

    protected FileMetadata() {
    }

    public FileMetadata(final ClaDocProperties fileProps) {
        if (fileProps != null) {
            this.author = fileProps.getCreator();
            this.company = fileProps.getCompany();
            this.subject = fileProps.getSubject();
            this.title = fileProps.getTitle();
            this.keywords = fileProps.getKeywords();
            this.appNameVer = fileProps.getAppNameVer();
            this.template = fileProps.getTemplate();
            this.createDate = fileProps.getCreated();
        }
    }

    public abstract List<String> toCsvInner();

    public abstract List<String> toCsvTitleInner();

    private String toCsvStringTitle() {
        final List<String> sb = Lists.newArrayList();
        sb.add("author");
        sb.add("owner");
        sb.add("company");
        sb.add("subject");
        sb.add("title");
        sb.add("keywords");
        sb.add("appNameVer");
        sb.add("template");
        sb.add("creatDate");

        sb.addAll(toCsvTitleInner());

        sb.add("aclReadAllowed");
        sb.add("aclWriteAllowed");
        sb.add("aclReadDenied");
        sb.add("aclWriteDenied");
        return sb.stream().collect(Collectors.joining());
    }

    public String toCsvString() {
        final List<String> sb = Lists.newArrayList();
        sb.add(CsvUtils.notNull(author));
        sb.add(CsvUtils.notNull(owner));
        sb.add(CsvUtils.notNull(company));
        sb.add(CsvUtils.notNull(subject));
        sb.add(CsvUtils.notNull(title));
        sb.add(CsvUtils.notNull(keywords));
        sb.add(CsvUtils.notNull(appNameVer));
        sb.add(CsvUtils.notNull(template));
        sb.add(CsvUtils.notNull(createDate));

        sb.addAll(toCsvInner());

        sb.add(CsvUtils.notNull(aclReadAllowed).stream().collect(Collectors.joining("|")));
        sb.add(CsvUtils.notNull(aclWriteAllowed).stream().collect(Collectors.joining("|")));
        sb.add(CsvUtils.notNull(aclReadDenied).stream().collect(Collectors.joining("|")));
        sb.add(CsvUtils.notNull(aclWriteDenied).stream().collect(Collectors.joining("|")));
        return sb.stream().collect(Collectors.joining(","));
    }


    public String aclCsvStringTitle(final String sep) {
        return "docId" + sep + "AclType" + sep + "AclName";
    }

    public String aclCsvString(final UUID docId, final String sep) {
        final List<String> sb = Lists.newArrayList();
        CsvUtils.notNull(aclReadAllowed).forEach(acl -> sb.add(docId.toString() + sep + "R" + sep + "\"" + acl + "\""));
        CsvUtils.notNull(aclWriteAllowed).forEach(acl -> sb.add(docId.toString() + sep + "W" + sep + "\"" + acl + "\""));
        return (sb.size() == 0) ? null :
                sb.stream().collect(Collectors.joining("\n"));
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getAppNameVer() {
        return appNameVer;
    }

    public void setAppNameVer(String appNameVer) {
        this.appNameVer = appNameVer;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public Date getCreatDate() {
        return createDate;
    }

    public void setCreatDate(Date createDate) {
        this.createDate = createDate;
    }

    public Long getFsFileSize() {
        return fsFileSize;
    }

    public void setFsFileSize(Long fsFileSize) {
        this.fsFileSize = fsFileSize;
    }

    public Date getFsLastModified() {
        return fsLastModified;
    }

    public void setFsLastModified(Date fsLastModified) {
        this.fsLastModified = fsLastModified;
    }

    public List<String> getProposedTitles() {
        return proposedTitles;
    }

    @Override
    public Collection<String> getAclReadAllowed() {
        return aclReadAllowed;
    }

    public void setAclReadAllowed(Collection<String> aclReadAllowed) {
        this.aclReadAllowed = aclReadAllowed;
    }

    @Override
    public Collection<String> getAclWriteAllowed() {
        return aclWriteAllowed;
    }

    public void setAclWriteAllowed(Collection<String> aclWriteAllowed) {
        this.aclWriteAllowed = aclWriteAllowed;
    }

    @Override
    public Collection<String> getAclReadDenied() {
        return aclReadDenied;
    }

    public void setAclReadDenied(Collection<String> aclReadDenied) {
        this.aclReadDenied = aclReadDenied;
    }

    @Override
    public Collection<String> getAclWriteDenied() {
        return aclWriteDenied;
    }

    public void setAclWriteDenied(Collection<String> aclWriteDenied) {
        this.aclWriteDenied = aclWriteDenied;
    }
}
