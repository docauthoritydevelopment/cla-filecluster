package com.cla.common.domain.dto.components.solr;

/**
 * Created by uri on 04-Jun-17.
 */
public class SolrNodeDto {

    private String address;

    public SolrNodeDto() {
    }

    public SolrNodeDto(String address) {
        this.address = address;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "SolrNodeDto{" +
                "address='" + address + '\'' +
                '}';
    }
}
