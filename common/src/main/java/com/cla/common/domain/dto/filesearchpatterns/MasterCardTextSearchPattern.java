package com.cla.common.domain.dto.filesearchpatterns;


public class MasterCardTextSearchPattern extends GenericCreditCardTextSearchPattern {

	public static final String MASTERCARD_PATERN = "\\b5[1-5]\\d{2}([ -]?)\\d{4}\\1\\d{4}\\1\\d{4}\\b";

	public MasterCardTextSearchPattern(final Integer id, final String name) {
		super(id, name, MASTERCARD_PATERN);
	}

	public MasterCardTextSearchPattern() {
	}
}
