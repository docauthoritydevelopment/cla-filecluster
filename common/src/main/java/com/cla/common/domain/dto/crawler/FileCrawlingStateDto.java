package com.cla.common.domain.dto.crawler;


import java.util.List;

/**
 * Created by uri on 27/10/2015.
 */
public class FileCrawlingStateDto {
    private String stateString;
    private List<String> runOperationList;
    private RunStatus lastRunStatus;
    private Long lastRunStartTime;
    private Long lastRunStopTime;
    private boolean fileCrawlerRunning;
    private long  currentPhaseProgressCounter;
    private long  currentPhaseProgressFinishMark;
    private FileCrawlPhase fileCrawlPhase;
    private String scannedTypes;
    private Double runningRate;
    private Double rate;

    private boolean manualRun;

    private boolean fileProcessorRunning;

    private Long rootFolderId;

    private String rootFolderPath;

    private String scheduleGroupName;

    private Long scheduleGroupId;

    private int errorsCount;

    private long runErrorsCount;
    private Boolean remoteFileProcessorAlive;
    private Boolean remoteFileProcessorRunning;
    private String remoteFileProcessorError;

    public void setStateString(String stateString) {
        this.stateString = stateString;
    }

    public void setRunOperationList(List<String> runOperationList) {
        this.runOperationList = runOperationList;
    }

    public void setLastRunStartTime(Long lastRunStartTime) {
        this.lastRunStartTime = lastRunStartTime;
    }

    public void setLastRunStatus(RunStatus lastRunStatus) {
        this.lastRunStatus = lastRunStatus;
    }

    public void setLastRunStopTime(Long lastRunStopTime) {
        this.lastRunStopTime = lastRunStopTime;
    }

    public void setFileCrawlerRunning(boolean fileCrawlerRunning) {
        this.fileCrawlerRunning = fileCrawlerRunning;
    }

    public String getStateString() {
        return stateString;
    }

    public List<String> getRunOperationList() {
        return runOperationList;
    }

    public Long getLastRunStartTime() {
        return lastRunStartTime;
    }

    public RunStatus getLastRunStatus() {
        return lastRunStatus;
    }

    public Long getLastRunStopTime() {
        return lastRunStopTime;
    }

    public boolean isFileCrawlerRunning() {
        return fileCrawlerRunning;
    }

    public void setCurrentPhaseProgressCounter(long  currentPhaseProgressCounter) {
        this.currentPhaseProgressCounter = currentPhaseProgressCounter;
    }

    public long  getCurrentPhaseProgressCounter() {
        return currentPhaseProgressCounter;
    }

    public void setCurrentPhaseProgressFinishMark(long  currentPhaseProgressFinishMark) {
        this.currentPhaseProgressFinishMark = currentPhaseProgressFinishMark;
    }

    public long  getCurrentPhaseProgressFinishMark() {
        return currentPhaseProgressFinishMark;
    }

    public FileCrawlPhase getFileCrawlPhase() {
        return fileCrawlPhase;
    }

    public void setFileCrawlPhase(FileCrawlPhase fileCrawlPhase) {
        this.fileCrawlPhase = fileCrawlPhase;
    }

    public String getScannedTypes() {
        return scannedTypes;
    }

    public void setScannedTypes(String scannedTypes) {
        this.scannedTypes = scannedTypes;
    }

    public Double getRunningRate() {
        return runningRate;
    }

    public void setRunningRate(Double runningRate) {
        this.runningRate = runningRate;
    }

    public int getErrorsCount() {
        return errorsCount;
    }

    public void setErrorsCount(int errorsCount) {
        this.errorsCount = errorsCount;
    }

    public Long getRootFolderId() {
        return rootFolderId;
    }

    public void setRootFolderId(Long rootFolderId) {
        this.rootFolderId = rootFolderId;
    }

    public Long getScheduleGroupId() {
        return scheduleGroupId;
    }

    public void setScheduleGroupId(Long scheduleGroupId) {
        this.scheduleGroupId = scheduleGroupId;
    }

    public String getRootFolderPath() {
        return rootFolderPath;
    }

    public void setRootFolderPath(String rootFolderPath) {
        this.rootFolderPath = rootFolderPath;
    }

    public String getScheduleGroupName() {
        return scheduleGroupName;
    }

    public void setScheduleGroupName(String scheduleGroupName) {
        this.scheduleGroupName = scheduleGroupName;
    }

    public Double getRate() {
        return rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }

    public boolean isFileProcessorRunning() {
        return fileProcessorRunning;
    }

    public void setFileProcessorRunning(boolean fileProcessorRunning) {
        this.fileProcessorRunning = fileProcessorRunning;
    }

    public boolean isManualRun() {
        return manualRun;
    }

    public void setManualRun(boolean manualRun) {
        this.manualRun = manualRun;
    }

    public long getRunErrorsCount() {
        return runErrorsCount;
    }

    public void setRunErrorsCount(long runErrorsCount) {
        this.runErrorsCount = runErrorsCount;
    }

    public void setRemoteFileProcessorAlive(boolean remoteFileProcessorAlive) {
        this.remoteFileProcessorAlive = remoteFileProcessorAlive;
    }

    public Boolean isRemoteFileProcessorAlive() {
        return remoteFileProcessorAlive;
    }

    public void setRemoteFileProcessorRunning(boolean remoteFileProcessorRunning) {
        this.remoteFileProcessorRunning = remoteFileProcessorRunning;
    }

    public Boolean isRemoteFileProcessorRunning() {
        return remoteFileProcessorRunning;
    }

    public void setRemoteFileProcessorError(String remoteFileProcessorError) {
        this.remoteFileProcessorError = remoteFileProcessorError;
    }

    public String getRemoteFileProcessorError() {
        return remoteFileProcessorError;
    }
}
