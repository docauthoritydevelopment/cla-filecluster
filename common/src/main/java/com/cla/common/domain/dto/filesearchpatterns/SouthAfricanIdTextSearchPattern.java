package com.cla.common.domain.dto.filesearchpatterns;

import java.util.function.Predicate;

/**
 * Created by: itay
 * Created on: 25/06/2019
 *
 * A South African person identification number is a 13-digit number containing only numeric characters, and no whitespace, punctuation, or alpha characters. It is defined as YYMMDDSSSSCAZ:
 *
 * YYMMDD represents the date of birth (DoB);
 * SSSS is a sequence number registered with the birth date (where females are assigned sequential numbers starting with 0 to 4 and males from 5 to 9)
 * C is the citizenship with 0 if the person is a SA citizen, 1 if the person is a permanent resident;
 * A is 8 or 9. Prior to 1994 this number was used to indicate the holder's race;
 * Z is a checksum digit.
 * Using ID Number 8001015009087 as an example, it would read as follows:
 * The ID indicates that a male citizen was born on 1 January 1980; he was the 10th male to be registered (assuming that the first male to be registered on that day would be assigned the sequence number 5000).
 *
 * The checksum digit is calculated using the Luhn algorithm
 */
public class SouthAfricanIdTextSearchPattern extends TextSearchPatternImpl {

	public static final String SA_ID_PATERN = "\\b\\d{2}0*([1-9]|1[0-2])0*([1-9]|[12][0-9]|3[01])\\d{4}[01][89]\\d\\b";

	public SouthAfricanIdTextSearchPattern() {}

	public SouthAfricanIdTextSearchPattern(final Integer id, final String name) {
		super(id, name, SA_ID_PATERN);
	}

	@Override
	public Predicate<String> getPredicate() {
		return this::checkLuhn;
	}

	private boolean checkLuhn(final String number) {
		final int[] digits = ("0000"+number).substring(number.length()-9).chars().toArray();	// (length + 4) - 13 = length - 9
		int sum = 0;
		final int length = digits.length;
		for (int i = 0; i < length; i++) {

			// get digits in reverse order
			int digit = digits[length - i - 1] - '0';
			
			// every 2nd number multiply with 2
			if (i % 2 == 1) {
				digit *= 2;
			}
			sum += digit > 9 ? digit - 9 : digit;
		}
		return sum % 10 == 0;
	}
}
