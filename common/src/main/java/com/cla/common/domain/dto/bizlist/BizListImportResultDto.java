package com.cla.common.domain.dto.bizlist;

import java.io.Serializable;

/**
 * Created by uri on 05/01/2016.
 */
public class BizListImportResultDto implements Serializable {

    private String resultDescription;

    private Long importId;

    private int importedRowCount;

    private int duplicateRowCount;
    private int errorRowCount;

    public String getResultDescription() {
        return resultDescription;
    }

    public void setResultDescription(String resultDescription) {
        this.resultDescription = resultDescription;
    }

    public Long getImportId() {
        return importId;
    }

    public void setImportId(Long importId) {
        this.importId = importId;
    }

    public int getImportedRowCount() {
        return importedRowCount;
    }

    public void setImportedRowCount(int importedRowCount) {
        this.importedRowCount = importedRowCount;
    }

    public int getDuplicateRowCount() {
        return duplicateRowCount;
    }

    public void setDuplicateRowCount(int duplicateRowCount) {
        this.duplicateRowCount = duplicateRowCount;
    }

    public void setErrorRowCount(int errorRowCount) {
        this.errorRowCount = errorRowCount;
    }

    public int getErrorRowCount() {
        return errorRowCount;
    }

    @Override
    public String toString() {
        return "BizListImportResultDto{" +
                "resultDescription='" + resultDescription + '\'' +
                ", importId=" + importId +
                ", importedRowCount=" + importedRowCount +
                ", duplicateRowCount=" + duplicateRowCount +
                ", errorRowCount=" + errorRowCount +
                '}';
    }
}
