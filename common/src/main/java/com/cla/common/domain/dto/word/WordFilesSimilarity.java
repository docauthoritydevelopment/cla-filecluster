package com.cla.common.domain.dto.word;

import com.cla.common.domain.dto.histogram.ClaHistogramCollection;
import com.cla.common.domain.dto.mediaproc.word.WordFile;
import com.cla.common.domain.dto.pv.PVType;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;


public class WordFilesSimilarity {

    private static final int minLetterHeadItems = 3;

    private final Map<PVType, SimilarityVector> similarities = new LinkedHashMap<>();

    public WordFilesSimilarity() {
        init();
    }

    public WordFilesSimilarity(final WordFile fileA, final WordFile fileB) {
        init();

        Arrays.asList(ClaHistogramCollection.wordLongHistogramNames)
                .forEach(simName -> setSimilarityVector(simName, fileA.getSimilarity(fileB, simName),
                        fileA.getLongHistogram(simName).getGlobalCount(),
                        fileB.getLongHistogram(simName).getGlobalCount(),
                        fileA.getLongHistogram(simName).getItemsCount(),
                        fileB.getLongHistogram(simName).getItemsCount(),
                        fileA.getOrderedMatchCount(fileB, simName)));
    }

    public WordFilesSimilarity(
            final Map<String, ClaHistogramCollection<Long>> fileAHistograms,
            final Map<String, ClaHistogramCollection<Long>> fileBHistograms) {
        init();

        Arrays.asList(ClaHistogramCollection.wordLongHistogramNames)
                .forEach(simName -> {
                    final ClaHistogramCollection<Long> fileAHist = fileAHistograms.get(simName.name());
                    final ClaHistogramCollection<Long> fileBHist = fileBHistograms.get(simName.name());
                    if (fileAHist != null && fileBHist != null) {
                        setSimilarityVector(simName, fileAHist.getNormalizedSimilarityRatio(fileBHist),
                                fileAHist.getGlobalCount(),
                                fileBHist.getGlobalCount(),
                                fileAHist.getItemsCount(),
                                fileBHist.getItemsCount(),
                                fileAHist.getOrderedSimilarityCount(fileBHist));
                    } else {
                        setSimilarityVector(simName, 0f, 0, 0, 0, 0, 0);
                    }
                });
    }

    private void init() {
        createSimilarityValues();
    }

    private void createSimilarityValues() {
        Arrays.asList(ClaHistogramCollection.wordLongHistogramNames).forEach(name -> similarities.put(name, new SimilarityVector()));
    }

    public Set<PVType> getSimilaritiesKeySet() {
        return similarities.keySet();
    }

    public float getSimilarityValue(final PVType name) {
        final SimilarityVector ret = similarities.get(name);
        if (ret == null) {
            throw new RuntimeException("similarities name not found: " + name);
        }
        return ret.rate;
    }

    public String similarityVectorToString(final PVType name) {
        final SimilarityVector ret = similarities.get(name);
        return (ret == null) ? "null" : ret.toString();
    }

    public void setSimilarityValue(final PVType name, final float val) {
        final SimilarityVector ret = similarities.get(name);
        if (ret == null) {
            throw new RuntimeException("similarities name not found: " + name);
        }

        ret.rate = val;
    }

    public int getCountA(final PVType name) {
        final SimilarityVector ret = similarities.get(name);
        if (ret == null) {
            throw new RuntimeException("similarities name not found: " + name);
        }
        return ret.countA;
    }

    public void setCountA(final PVType name, final int val) {
        final SimilarityVector ret = similarities.get(name);
        if (ret == null) {
            throw new RuntimeException("similarities name not found: " + name);
        }

        ret.countA = val;
    }

    public int getMinCount(final PVType name) {
        return Integer.min(getCountA(name), getCountB(name));
    }

    public int getMaxCount(final PVType name) {
        return Integer.max(getCountA(name), getCountB(name));
    }

    public int getCountB(final PVType name) {
        final SimilarityVector ret = similarities.get(name);
        if (ret == null) {
            throw new RuntimeException("similarities name not found: " + name);
        }
        return ret.countB;
    }

    public void setCountB(final PVType name, final int val) {
        final SimilarityVector ret = similarities.get(name);
        if (ret == null) {
            throw new RuntimeException("similarities name not found: " + name);
        }

        ret.countB = val;
    }

    public int getItemsA(final PVType name) {
        final SimilarityVector ret = similarities.get(name);
        if (ret == null) {
            throw new RuntimeException("similarities name not found: " + name);
        }
        return ret.itemsA;
    }

    public void setItemsA(final PVType name, final int val) {
        final SimilarityVector ret = similarities.get(name);
        if (ret == null) {
            throw new RuntimeException("similarities name not found: " + name);
        }

        ret.itemsA = val;
    }

    public int getItemsB(final PVType name) {
        final SimilarityVector ret = similarities.get(name);
        if (ret == null) {
            throw new RuntimeException("similarities name not found: " + name);
        }
        return ret.itemsB;
    }

    public void setItemsB(final PVType name, final int val) {
        final SimilarityVector ret = similarities.get(name);
        if (ret == null) {
            throw new RuntimeException("similarities name not found: " + name);
        }

        ret.itemsB = val;
    }

    public int getMinItems(final PVType name) {
        return Integer.min(getItemsA(name), getItemsB(name));
    }

    public int getMaxItems(final PVType name) {
        return Integer.max(getItemsA(name), getItemsB(name));
    }

    public int getOrderedMatchCount(final PVType name) {
        final SimilarityVector ret = similarities.get(name);
        if (ret == null) {
            throw new RuntimeException("similarities name not found: " + name);
        }
        return ret.orderedMatchCount;
    }

    public int getOrderedMatchCountsHistLetterHeadLabels() {
        return getOrderedMatchCount(PVType.PV_LetterHeadLabels);
    }

    public int getOrderedMatchCountsHistParagraphLabels() {
        return getOrderedMatchCount(PVType.PV_ParagraphLabels);
    }

    public void setOrderedMatchCountsHistLetterHeadLabels(final int orderedMatchCountsHistLetterHeadLabels) {
    }

    public void setOrderedMatchCountsHistParagraphLabels(final int orderedMatchCountsHistLetterHeadLabels) {
    }


    public void setOrderedMatchCount(final PVType name, final int val) {
        final SimilarityVector ret = similarities.get(name);
        if (ret == null) {
            throw new RuntimeException("similarities name not found: " + name);
        }

        ret.orderedMatchCount = val;
    }

    public void setSimilarityVector(final PVType name, final float rate,
                                    final int countA, final int countB,
                                    final int itemsA, final int itemsB,
                                    final int orderedMatchCount) {
        final SimilarityVector ret = similarities.get(name);
        if (ret == null) {
            throw new RuntimeException("similarities name not found: " + name);
        }

        ret.rate = rate;
        ret.countA = countA;
        ret.countB = countB;
        ret.itemsA = itemsA;
        ret.itemsB = itemsB;
        ret.orderedMatchCount = orderedMatchCount;
    }

    public SimilarityVector getSimilarityVector(final PVType pvt) {
        final SimilarityVector ret = similarities.get(pvt);
        if (ret == null) {
            throw new RuntimeException("similarities name not found: " + pvt);
        }
        return ret;
    }

    public boolean passThreshold(final float contentHighThreshold,
                                 final float contentLowThreshold,
                                 final float styleHighThreshold) {

        final SimilarityVector docstartNgramsSimVec = getSimilarityVector(PVType.PV_DocstartNgrams);
        final SimilarityVector bodyNgramsSimVec = getSimilarityVector(PVType.PV_BodyNgrams);
        final SimilarityVector subBodyNgramsSimVec = getSimilarityVector(PVType.PV_SubBodyNgrams);
        final SimilarityVector styleSignaturesSimVec = getSimilarityVector(PVType.PV_StyleSignatures);
        final SimilarityVector styleSequenceSimVec = getSimilarityVector(PVType.PV_StyleSequence);
        final SimilarityVector absoluteStyleSignaturesSimVec = getSimilarityVector(PVType.PV_AbsoluteStyleSignatures);
        final SimilarityVector absoluteSequenceSimVec = getSimilarityVector(PVType.PV_AbsoluteStyleSequence);
        final SimilarityVector headingsSimVec = getSimilarityVector(PVType.PV_Headings);
        final SimilarityVector pLabelSimVec = getSimilarityVector(PVType.PV_ParagraphLabels);
        final SimilarityVector letterHeadLabelsSimVec = getSimilarityVector(PVType.PV_LetterHeadLabels);

        // Style similarity
        if ((docstartNgramsSimVec.rate > contentLowThreshold ||
                bodyNgramsSimVec.rate > contentLowThreshold ||
                subBodyNgramsSimVec.rate > contentLowThreshold) &&
                (styleSignaturesSimVec.rate > styleHighThreshold ||
                        styleSequenceSimVec.rate > styleHighThreshold ||
                        absoluteStyleSignaturesSimVec.rate > styleHighThreshold ||
                        absoluteSequenceSimVec.rate > styleHighThreshold)) {
            // if one file has letter-head presence, make sure there is minimal similarity
            final boolean disqualify =
                    (Integer.max(letterHeadLabelsSimVec.countA, letterHeadLabelsSimVec.countB) >= minLetterHeadItems &&
                            letterHeadLabelsSimVec.orderedMatchCount < minLetterHeadItems) ||
                            // Differentiate legal doc classes based on paragraph labels
                            (Integer.max(pLabelSimVec.countA, pLabelSimVec.countB) >= minLetterHeadItems &&
                                    pLabelSimVec.rate <= contentHighThreshold);
            if (!disqualify) {
                return true;
            }
        }
        // Headings similarity
        if (headingsSimVec.rate > contentHighThreshold &&
                Integer.min(headingsSimVec.countA, headingsSimVec.countB) > 3) {
            return true;
        }

        if (docstartNgramsSimVec.rate > contentHighThreshold) {
            return true;
        }

        if ((bodyNgramsSimVec.rate > contentHighThreshold ||
                subBodyNgramsSimVec.rate > contentHighThreshold) &&
                (Integer.max(letterHeadLabelsSimVec.countA, letterHeadLabelsSimVec.countB) < minLetterHeadItems ||
                        letterHeadLabelsSimVec.orderedMatchCount >= minLetterHeadItems)) {
            return true;
        }

        return false;
    }

    /**
     * Get a basic indication for a potential similarity
     *
     * @return A float indicating minimal similarity ratio [0-1]
     */
    public float getPotentialSimilarityRate() {
        return similarities.size() == 0 ? 0.0f : similarities.values().stream().map(v -> v.rate).max(Float::compare).get();
    }

    public static String getTitleCsvString() {
        return
                Arrays.asList(ClaHistogramCollection.wordLongHistogramNames).stream().map(t -> t.toString())
                        .collect(Collectors.joining(","));
    }

    public String getCsvString() {
        return
                Arrays.asList(ClaHistogramCollection.wordLongHistogramNames).stream()
                        .map(name -> String.valueOf(getSimilarityValue(name)))
                        .collect(Collectors.joining(","));
    }

    public String dumpSimilarityVector() {
        return
                Arrays.asList(ClaHistogramCollection.wordLongHistogramNames).stream()
                        .map(name -> name.toString().substring(0, 3) + ":" + similarityVectorToString(name))
                        .collect(Collectors.joining("|"));
    }

    private class SimilarityVector {
        public float rate = 0.0f;
        public int countA = 0;
        public int countB = 0;
        public int itemsA = 0;
        public int itemsB = 0;
        public int orderedMatchCount = 0;

        @Override
        public String toString() {
            return String.format("%.2f;%d;%d;%d;%d;%d", rate, countA, countB, itemsA, itemsB,/*totalCountA,totalCountB,*/orderedMatchCount);
        }
    }
}
