package com.cla.common.domain.dto.messages;

public class FileContentRequestMessage extends ProcessingPhaseMessage<FileContentTaskParameters, IngestError> implements RequestMessage {

    public FileContentRequestMessage(){

    }
    public FileContentRequestMessage(FileContentTaskParameters payload) {

        super(payload);
    }
}
