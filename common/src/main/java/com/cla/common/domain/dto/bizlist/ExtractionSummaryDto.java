package com.cla.common.domain.dto.bizlist;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.util.List;

/**
 * Created by uri on 26/01/2016.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ExtractionSummaryDto implements Serializable {

    private Long bizListId;
    private String bizListName;
    private BizListItemType bizListItemType;
    private List<SimpleBizListItemDto> sampleExtractions;
    private int count;


    public Long getBizListId() {
        return bizListId;
    }

    public void setBizListId(Long bizListId) {
        this.bizListId = bizListId;
    }

    public String getBizListName() {
        return bizListName;
    }

    public void setBizListName(String bizListName) {
        this.bizListName = bizListName;
    }

    public BizListItemType getBizListItemType() {
        return bizListItemType;
    }

    public void setBizListItemType(BizListItemType bizListItemType) {
        this.bizListItemType = bizListItemType;
    }

    public List<SimpleBizListItemDto> getSampleExtractions() {
        return sampleExtractions;
    }

    public void setSampleExtractions(List<SimpleBizListItemDto> sampleExtractions) {
        this.sampleExtractions = sampleExtractions;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "ExtractionSummaryDto{" +
                "bizListId=" + bizListId +
                ", bizListItemType=" + bizListItemType +
                ", sampleExtractions=" + sampleExtractions +
                ", count=" + count +
                '}';
    }
}
