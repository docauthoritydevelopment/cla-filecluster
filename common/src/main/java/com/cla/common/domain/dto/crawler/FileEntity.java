package com.cla.common.domain.dto.crawler;

/**
 * File entity for use in key-value db
 */
public class FileEntity {
    private boolean file = true;
    private boolean folder = false;
    private String aclSignature;
    private Long lastModified;
    private Long size;
    private Long fileId;
    private Long folderId;
    private Long contentMdId;
    private Long lastScanned;
    private boolean deleted = false;
    private boolean inaccessible = false;
    private String path;

    public boolean isFile() {
        return file;
    }

    public void setFile(boolean file) {
        this.file = file;
    }

    public boolean isFolder() {
        return folder;
    }

    public void setFolder(boolean folder) {
        this.folder = folder;
    }

    public String getAclSignature() {
        return aclSignature;
    }

    public void setAclSignature(String aclSignature) {
        this.aclSignature = aclSignature;
    }

    public Long getLastModified() {
        return lastModified;
    }

    public void setLastModified(Long lastModified) {
        this.lastModified = lastModified;
    }

    public Long getLastScanned() {
        return lastScanned;
    }

    public void setLastScanned(Long lastScanned) {
        this.lastScanned = lastScanned;
    }

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public Long getContentMdId() {
        return contentMdId;
    }

    public void setContentMdId(Long contentMdId) {
        this.contentMdId = contentMdId;
    }

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    public Long getFolderId() {
        return folderId;
    }

    public void setFolderId(Long folderId) {
        this.folderId = folderId;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public boolean isInaccessible() {
        return inaccessible;
    }

    public void setInaccessible(boolean inaccessible) {
        this.inaccessible = inaccessible;
    }

    public String getPath() {
        return path;
    }

    public FileEntity setPath(String path) {
        this.path = path;
        return this;
    }

    @Override
    public String toString() {
        return "FileEntity{" +
                "file=" + file +
                ", folder=" + folder +
                ", aclSignature='" + aclSignature + '\'' +
                ", lastModified=" + lastModified +
                ", size=" + size +
                ", fileId=" + fileId +
                ", folderId=" + folderId +
                ", contentMdId=" + contentMdId +
                ", lastScanned=" + lastScanned +
                ", deleted=" + deleted +
                ", inaccessible=" + inaccessible +
                ", path='" + path + '\'' +
                '}';
    }
}
