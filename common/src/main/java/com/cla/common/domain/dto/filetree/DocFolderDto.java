package com.cla.common.domain.dto.filetree;

import com.cla.common.domain.dto.bizlist.SimpleBizListItemDto;
import com.cla.common.domain.dto.department.DepartmentAssociationDto;
import com.cla.common.domain.dto.department.DepartmentDto;
import com.cla.common.domain.dto.doctype.DocTypeAssociationDto;
import com.cla.common.domain.dto.doctype.DocTypeDto;
import com.cla.common.domain.dto.file.FolderType;
import com.cla.common.domain.dto.filetag.FileTagAssociationDto;
import com.cla.common.domain.dto.filetag.FileTagDto;
import com.cla.common.domain.dto.security.AssociableDto;
import com.cla.common.domain.dto.security.FunctionalItemDto;
import com.cla.common.domain.dto.security.FunctionalRoleDto;
import com.cla.common.domain.dto.security.SystemRoleDto;
import com.cla.connector.domain.dto.media.MediaType;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

/**
 * Created by uri on 13/08/2015.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DocFolderDto implements FunctionalItemDto, AssociableDto,Serializable {

    private Long id;
    private String name;
    private String path;
    private String realPath;

    private String mediaItemId;
    private Integer depthFromRoot;
    private Long parentFolderId;
    private String rootFolderNickName;

    private int numOfSubFolders;  // Number of direct sub folders
    private int numOfDirectFiles; // Number of files under the folder (directly)
    private int numOfAllFiles;    // Number of files under the folder and any sub-folder.

    private Set<FileTagDto> fileTagDtos = Sets.newHashSet();
    private List<FileTagAssociationDto> fileTagAssociations = Lists.newLinkedList();

    private DepartmentDto departmentDto;
    private List<DepartmentAssociationDto> departmentAssociationDtos = Lists.newLinkedList();

    private Set<DocTypeDto> docTypeDtos = Sets.newHashSet();
    private List<SimpleBizListItemDto> associatedBizListItems = Lists.newLinkedList();
    private Set<FunctionalRoleDto> associatedFunctionalRoles = Sets.newHashSet();
    private Long rootFolderId;
    private int rootFolderLength;

    private DocStoreDto docStore;
    private Set<SystemRoleDto> permittedOperations = Sets.newHashSet();
    private List<DocTypeAssociationDto> docTypeAssociations;

    private FolderType folderType;

    private String rootFolderPath;

    private MediaType mediaType;

    private String mailboxUpn;

    private String mailboxGroup;

    private List<String> parentsInfo;

    private List<RootFolderSharePermissionDto> rootFolderSharePermissions;

    /**
     * default constructor needed for automation
     */
    public DocFolderDto() {
    }

    public DocFolderDto(Long id, String name, String path, Integer numOfSubFolders) {
        this.id = id;
        this.name = name;
        this.path = path;
        if (numOfSubFolders != null) {
            this.numOfSubFolders = numOfSubFolders;
        }
    }

    @Override
    public Set<SystemRoleDto> getPermittedOperations() {
        return permittedOperations;
    }

    @Override
    public Set<FunctionalRoleDto> getAssociatedFunctionalRoles() {
        return associatedFunctionalRoles;
    }

    public void setFunctionalRoles(Set<FunctionalRoleDto> associatedFunctionalRoles) {
        this.associatedFunctionalRoles = associatedFunctionalRoles;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the canonical path of the folder. No trailing slash
     */
    public String getPath() {
        return getRootFolderPathMailbox() + path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getPathNoPrefix() {
        return path;
    }

    public Integer getDepthFromRoot() {
        return depthFromRoot;
    }

    public void setDepthFromRoot(Integer depthFromRoot) {
        this.depthFromRoot = depthFromRoot;
    }

    public Long getParentFolderId() {
        return parentFolderId;
    }

    public void setParentFolderId(Long parentFolderId) {
        this.parentFolderId = parentFolderId;
    }

    public int getNumOfSubFolders() {
        return numOfSubFolders;
    }

    public void setNumOfSubFolders(int numOfSubFolders) {
        this.numOfSubFolders = numOfSubFolders;
    }

    public int getNumOfDirectFiles() {
        return numOfDirectFiles;
    }

    public void setNumOfDirectFiles(int numOfDirectFiles) {
        this.numOfDirectFiles = numOfDirectFiles;
    }

    public int getNumOfAllFiles() {
        return numOfAllFiles;
    }

    public void setNumOfAllFiles(int numOfAllFiles) {
        this.numOfAllFiles = numOfAllFiles;
    }

    public Set<FileTagDto> getFileTagDtos() {
        return fileTagDtos;
    }

    public void setFileTagDtos(Set<FileTagDto> fileTagDtos) {
        this.fileTagDtos = fileTagDtos;
    }

    public List<FileTagAssociationDto> getFileTagAssociations() {
        return fileTagAssociations;
    }

    public void setFileTagAssociations(List<FileTagAssociationDto> fileTagAssociations) {
        this.fileTagAssociations = fileTagAssociations;
    }

    public Set<DocTypeDto> getDocTypeDtos() {
        return docTypeDtos;
    }

    public void setDocTypeDtos(Set<DocTypeDto> docTypeDtos) {
        this.docTypeDtos = docTypeDtos;
    }

    public void setAssociatedBizListItems(List<SimpleBizListItemDto> associatedBizListItems) {
        this.associatedBizListItems = associatedBizListItems;
    }

    public List<SimpleBizListItemDto> getAssociatedBizListItems() {
        return associatedBizListItems;
    }

    public String getRealPathNoPrefix() {
        return realPath;
    }

    public String getRealPath() {
        return getRootFolderPathMailbox() + realPath;
    }

    private String getRootFolderPathMailbox() {
        if (!Strings.isNullOrEmpty(rootFolderPath)) {
            return rootFolderPath;
        } else if (!Strings.isNullOrEmpty(mailboxGroup)) {
            return mailboxGroup;
        } else {
            return "";
        }
    }

    public void setRealPath(String realPath) {
        this.realPath = realPath;
    }

    public String getRootFolderNickName() {
        return rootFolderNickName;
    }

    public void setRootFolderNickName(String rootFolderNickName) {
        this.rootFolderNickName = rootFolderNickName;
    }

    public void setRootFolderId(Long rootFolderId) {
        this.rootFolderId = rootFolderId;
    }

    public Long getRootFolderId() {
        return rootFolderId;
    }

    public void setRootFolderLength(int rootFolderLength) {
        this.rootFolderLength = rootFolderLength;
    }

    public int getRootFolderLength() {
        return rootFolderLength;
    }

    public String getMediaItemId() {
        if (mediaItemId != null && mediaType != null && rootFolderPath != null && mediaType.isPathAsMediaEntity()) {
            return rootFolderPath + mediaItemId;
        }
        return mediaItemId;
    }

    public String getMediaItemIdNoPrefix() {
        return mediaItemId;
    }

    public void setMediaItemId(String mediaItemId) {
        this.mediaItemId = mediaItemId;
    }

    public DocStoreDto getDocStore() {
        return docStore;
    }

    public void setDocStore(DocStoreDto docStore) {
        this.docStore = docStore;
    }

    public FolderType getFolderType() {
        return folderType;
    }

    public void setFolderType(FolderType folderType) {
        this.folderType = folderType;
    }

    @Override
    public String toString() {
        return "DocFolderDto{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", path='" + path + '\'' +
                ", associatedFunctionalRoles=" + associatedFunctionalRoles +
                '}';
    }

    public void setDocTypeAssociations(List<DocTypeAssociationDto> docTypeAssociations) {
        this.docTypeAssociations = docTypeAssociations;
    }

    public List<DocTypeAssociationDto> getDocTypeAssociations() {
        return docTypeAssociations;
    }

    public String getRootFolderPath() {
        return rootFolderPath;
    }

    public void setRootFolderPath(String rootFolderPath) {
        this.rootFolderPath = rootFolderPath;
    }

    public void setMediaType(MediaType mediaType) {
        this.mediaType = mediaType;
    }

    public MediaType getMediaType() {
        return mediaType;
    }

    public String getMailboxUpn() {
        return mailboxUpn;
    }

    public void setMailboxUpn(String mailboxUpn) {
        this.mailboxUpn = mailboxUpn;
    }

    public String getMailboxGroup() {
        return mailboxGroup;
    }

    public void setMailboxGroup(String mailboxGroup) {
        this.mailboxGroup = mailboxGroup;
    }

    public DepartmentDto getDepartmentDto() {
        return departmentDto;
    }

    public void setDepartmentDto(DepartmentDto departmentDto) {
        this.departmentDto = departmentDto;
    }

    public List<DepartmentAssociationDto> getDepartmentAssociationDtos() {
        return departmentAssociationDtos;
    }

    public void setDepartmentAssociationDtos(List<DepartmentAssociationDto> departmentAssociationDtos) {
        this.departmentAssociationDtos = departmentAssociationDtos;
    }

    public List<RootFolderSharePermissionDto> getRootFolderSharePermissions() {
        return rootFolderSharePermissions;
    }

    public void setRootFolderSharePermissions(List<RootFolderSharePermissionDto> rootFolderSharePermissions) {
        this.rootFolderSharePermissions = rootFolderSharePermissions;
    }

    public List<String> getParentsInfo() {
        return parentsInfo;
    }

    public void setParentsInfo(List<String> parentsInfo) {
        this.parentsInfo = parentsInfo;
    }
}
