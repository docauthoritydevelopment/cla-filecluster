package com.cla.common.domain.dto.mediaproc;

import com.cla.common.domain.dto.extraction.SearchPatternCountDto;
import com.cla.common.domain.dto.messages.IngestMessagePayload;
import com.cla.common.domain.dto.SearchPatternCounting;
import com.cla.common.utils.json.ZippingStringDeserializer;
import com.cla.common.utils.json.ZippingStringSerializer;
import com.cla.connector.domain.dto.file.FileType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.List;
import java.util.Set;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class OtherFile extends IngestMessagePayload<OtherFileMetadata> implements TextualResult, SearchPatternCounting {

	private String extractedContent;
	private String hashedContent;
	private String mime;
	private FileType type;
	private Set<SearchPatternCountDto> searchPatternsCounting = null;
	private int textLength;

	@JsonSerialize(using = ZippingStringSerializer.class)
	public String getExtractedContent() {
		return extractedContent;
	}

	@Override
	@JsonIgnore
	public String getOriginalContent() {
		return extractedContent;
	}

	@JsonDeserialize(using = ZippingStringDeserializer.class)
	public void setExtractedContent(String extractedContent) {
		this.extractedContent = extractedContent;
	}

	public String getMime() {
		return mime;
	}

	public void setMime(String mime) {
		this.mime = mime;
	}

	public FileType getType() {
		return type;
	}

	public void setType(FileType type) {
		this.type = type;
	}

	public Set<SearchPatternCountDto> getSearchPatternsCounting() {
		return searchPatternsCounting;
	}

	public void setSearchPatternsCounting(Set<SearchPatternCountDto> searchPatternsCounting) {
		this.searchPatternsCounting = searchPatternsCounting;
	}

	public int getTextLength() {
		return textLength;
	}

	public void setTextLength(int textLength) {
		this.textLength = textLength;
	}

	@JsonSerialize(using = ZippingStringSerializer.class)
	public String getHashedContent() {
		return hashedContent;
	}

	@JsonDeserialize(using = ZippingStringDeserializer.class)
	public void setHashedContent(String hashedContent) {
		this.hashedContent = hashedContent;
	}

	public void handleSearchPatterns(String requestId, SearchPatternServiceInterface searchPatternService) {
		if (getExtractedContent() != null) {
			Set<SearchPatternCountDto> patternCountDtos = searchPatternService.getSearchPatternsCounting(getExtractedContent());
			setSearchPatternsCounting(patternCountDtos);
			if (requestId == null) {
				setExtractedContent(null);
			}
		}
	}

	@Override
	public boolean isMetadataOnly() {
		return super.isMetadataOnly() ||
				(getTextLength() <= 0 &&
					(getExtractedContent() == null || getExtractedContent().length() <= 0));
	}

	@Override
	public List<String> getCombinedProposedTitles() {
		return null;
	}

	@Override
	public String getCsvValue() {
		return null;
	}
}
