package com.cla.common.domain.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


@SuppressWarnings("serial")
public class AggregationCountDTO<T extends Serializable> implements Serializable {
	private final List<AggregationCountItemDTO<T>> histogram;
	private String itemType;

	public AggregationCountDTO(final Class<T> itemClass) {
		this(itemClass.toString());
	}
	public AggregationCountDTO(final String itemType) {
		histogram = new ArrayList<>();
		this.itemType = itemType;
	}
	
	public AggregationCountDTO(final Class<T> itemClass, final List<Object[]> queryResults) {
		this(itemClass.toString(), queryResults);
	}
	public AggregationCountDTO(final String itemType, final List<Object[]> queryResults) {
		histogram = new ArrayList<>(queryResults.size());
		this.itemType = itemType;
		try {
			boolean checked = false;
			for (Object[] item:queryResults) {
				if (checked || (item[0].getClass().toString().equals(itemType) && 
						(item[1] instanceof Integer || item[1] instanceof Long))) {
					checked = true;
					histogram.add(new AggregationCountItemDTO<T>((T)item[0] , ((Long)item[1])));
				}
				else {
					throw new RuntimeException("Wrong result set type: ("+item[0].getClass().toString() + ", "+item[1].getClass().toString() 
							+ "). expecting ("+itemType+", Integer/Long)");
				}
			}
		} catch (Exception e) {
			throw new RuntimeException("Unexpected error during result set conversion.", e);
		}
	}
	
	public int addItem(final T item, long count) {
		histogram.add(new AggregationCountItemDTO<T>(item, count));
		return histogram.size();
	}

	public List<AggregationCountItemDTO<T>> getHistogram() {
		return histogram;
	}

	public String getItemType() {
		return itemType;
	}

	@Override
	public String toString() {
		return "{itemType="+itemType+" ,histogram=" + histogram.toString() + "}";
	}
	
}
