package com.cla.common.domain.dto.messages;

import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;

public class IngestResultMessage<T extends IngestMessagePayload> extends ProcessingPhaseMessage<T, IngestError>
        implements ResponseMessage{

    private Long claFileId;
    private boolean duplicateContentFound = false;
    private ClaFilePropertiesDto fileProperties;
    private Long contentMetadataId;
    private String requestId;

    // ---------------------- constructors -----------------------------------------------------------

    public IngestResultMessage() {
        super();
    }

    public IngestResultMessage(T extractedContent) {
        super(extractedContent);
    }

    public static IngestResultMessage create(){
        return new IngestResultMessage();
    }

    public static <R extends IngestMessagePayload> IngestResultMessage<R> create(R extractedContent){
        return new IngestResultMessage<>(extractedContent);
    }

    // ---------------------- getters / setters -------------------------------------------------------

    public Long getClaFileId() {
        return claFileId;
    }

    public IngestResultMessage<T> setClaFileId(Long claFileId) {
        this.claFileId = claFileId;
        return this;
    }

    public boolean getDuplicateContentFound() {
        return duplicateContentFound;
    }

    public ClaFilePropertiesDto getFileProperties() {
        return fileProperties;
    }

    public IngestResultMessage<T> setFileProperties(ClaFilePropertiesDto fileProperties) {
        this.fileProperties = fileProperties;
        return this;
    }

    public Long getContentMetadataId() {
        return contentMetadataId;
    }

    public IngestResultMessage<T> setContentMetadataId(Long contentMetadataId) {
        this.contentMetadataId = contentMetadataId;
        return this;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public void setDuplicateContentFound(boolean duplicateContentFound) {
        this.duplicateContentFound = duplicateContentFound;
    }

    @Override
    public boolean hasErrors() {
        return super.hasErrors() || (hasPayload() && getPayload().hasError());
    }

    @Override
    public String toString() {
        return "IngestResultMessage{" +
                "claFileId=" + claFileId +
                ", duplicateContentFound=" + duplicateContentFound +
                " [" + super.toString() + "]"+
                '}';
    }
}
