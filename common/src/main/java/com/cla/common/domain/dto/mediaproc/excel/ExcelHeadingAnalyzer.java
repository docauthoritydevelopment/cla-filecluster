package com.cla.common.domain.dto.mediaproc.excel;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class ExcelHeadingAnalyzer {
	private static final int maxColForHeadingStart = 8;
	private static final int minNumberOfHeading = 4;
	private static final int maxRowForHeadings = 15;
	
	@JsonProperty
	private Map<String, List<String>> sheetsHeadings = new LinkedHashMap<>();
	
	private int headingTopCandidateRowIndex = -1;
	private int headingCurrentRowIndex = -1;
	private boolean headingDisqualifyRow = false;
	private int headingLatestColInRow = -1;
	private ExcelCell headingLatestCellInRow = null;
	private List<String> headingTopCandidateValues = null;
	private List<String> headingCurrentValues = null;
	private String headingSheetName = null;
	private ExcelCell headingLatestCellInRow_old = null;
	
	public void headingProcessCell(final String sheetName, final int columnIndex, final int rowNum, final ExcelCell excelCell) {
		if (!sheetName.equals(headingSheetName))
			headingSheetStart(sheetName);
		if (rowNum != headingCurrentRowIndex) {
			if (rowNum >= maxRowForHeadings)
				return;
			headingRowStart(rowNum);
		}
		
		if (headingDisqualifyRow)
			return;
		if (excelCell.isFormula() || !(excelCell.getType() == ExcelCell.CELL_TYPE_STRING ||
				(excelCell.getType() == ExcelCell.CELL_TYPE_NUMERIC && 		// match dates as well, if in bold font
				 excelCell.isDate() &&
				 excelCell.getCellStyle().isBold())))
			return;
		if (headingLatestCellInRow != null) {
			// Looking for continuity and style boldness similarity - if sequence is broken, start again.
			if ((headingLatestColInRow+1) != columnIndex ||
					headingLatestCellInRow.getCellStyle().isBold() != excelCell.getCellStyle().isBold()) {
				if (columnIndex <= maxColForHeadingStart) {
					headingCurrentValues = new LinkedList<>();
				}
				else {
					headingDisqualifyRow = true;	// no more values to process
				}
			}
		}
		headingLatestCellInRow = excelCell;
		headingCurrentValues.add(excelCell.getStringValue());
		headingLatestColInRow = columnIndex;
	}

	private void headingRowStart(final int rowNum) {
		if (headingCurrentValues != null)
			headingRowEnd();

		headingCurrentRowIndex = rowNum;
		headingCurrentValues = new LinkedList<>();
		headingLatestCellInRow = null;
		headingLatestCellInRow_old = null;
		headingLatestColInRow = -1;
		headingDisqualifyRow = false;
	}
	
	private void headingRowEnd() {
		if (headingCurrentValues == null)
			return;
		if (headingTopCandidateValues == null ||
				headingTopCandidateValues.size() < headingCurrentValues.size()) {
			headingTopCandidateValues = headingCurrentValues;
			headingTopCandidateRowIndex = headingCurrentRowIndex;
		}
		headingCurrentValues = null;
		headingCurrentRowIndex = -1;
	}
	
	private void headingSheetStart(final String sheetName) {
		if (headingSheetName != null)
			headingSheetEnd();
		headingSheetName = sheetName;
	}
	
	private void headingSheetEnd() {
		headingRowEnd();
		if (headingTopCandidateValues != null &&
				headingTopCandidateValues.size() >= minNumberOfHeading) {
			sheetsHeadings.put(headingSheetName, headingTopCandidateValues);
			headingTopCandidateValues = null;
		}
		headingSheetName = null;
		headingTopCandidateRowIndex = -1;
	}
	
	public void headingFinalize() {
		headingSheetEnd();
	}
	
	public Map<String, List<String>> getSheetsHeadings() {
		return sheetsHeadings;
	}

	public List<String> getHeadingsForSheet(final String sheet) {
		return sheetsHeadings.get(sheet);
	}

	public void setHeadingsForSheet(final String sheet, final List<String> sheetHeadings) {
		this.sheetsHeadings.put(sheet, sheetHeadings);
	}

	public void setSheetsHeadings(final Map<String, List<String>> sheetHeadings) {
		this.sheetsHeadings = sheetHeadings;
	}

	@Override
	public String toString() {
		return "ExcelHeadingLocator [sheetsHeadings:" + sheetsHeadings.toString() + "]";
	}
	
}
