package com.cla.common.domain.dto.messages;

/**
 * Request message for the remote content metadata service
 * Created by vladi on 2/23/2017.
 */
public class ContentMetadataRequestMessage implements RequestMessage{
    private String signature;
    private int fileSize;
    private long rootFolderId;
    private String requestId = null;
    private String mediaProcessorId;

    public ContentMetadataRequestMessage() {
    }

    public ContentMetadataRequestMessage(String signature, int fileSize, long rootFolderId) {
        this.signature = signature;
        this.fileSize = fileSize;
        this.rootFolderId = rootFolderId;
    }

    public static ContentMetadataRequestMessage create(){
        return new ContentMetadataRequestMessage();
    }

    public String getSignature() {
        return signature;
    }

    public ContentMetadataRequestMessage setSignature(String signature) {
        this.signature = signature;
        return this;
    }

    public int getFileSize() {
        return fileSize;
    }

    public ContentMetadataRequestMessage setFileSize(int fileSize) {
        this.fileSize = fileSize;
        return this;
    }

    public long getRootFolderId() {
        return rootFolderId;
    }

    public ContentMetadataRequestMessage setRootFolderId(long rootFolderId) {
        this.rootFolderId = rootFolderId;
        return this;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getMediaProcessorId() {
        return mediaProcessorId;
    }

    public void setMediaProcessorId(String mediaProcessorId) {
        this.mediaProcessorId = mediaProcessorId;
    }

    @Override
    public String toString() {
        return "ContentMetadataRequestMessage{" +
                "signature='" + signature + '\'' +
                ", fileSize=" + fileSize +
                ", rootFolderId=" + rootFolderId +
                '}';
    }
}
