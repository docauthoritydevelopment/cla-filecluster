package com.cla.common.domain.dto.security;

import java.util.List;

/**
 * Created by Itai Marko.
 */
public class BizRoleSummaryInfoDto {

    private BizRoleDto bizRoleDto;
    private List<LdapGroupMappingDto> ldapGroupMappingDtos;

    public BizRoleDto getBizRoleDto() {
        return bizRoleDto;
    }

    public void setBizRoleDto(BizRoleDto bizRoleDto) {
        this.bizRoleDto = bizRoleDto;
    }

    public List<LdapGroupMappingDto> getLdapGroupMappingDtos() {
        return ldapGroupMappingDtos;
    }

    public void setLdapGroupMappingDtos(List<LdapGroupMappingDto> ldapGroupMappingDtos) {
        this.ldapGroupMappingDtos = ldapGroupMappingDtos;
    }

    @Override
    public String toString() {
        return "BizRoleSummaryInfoDto{" +
                "bizRoleDto=" + bizRoleDto +
                ", ldapGroupMappingDtos=" + ldapGroupMappingDtos +
                '}';
    }
}

