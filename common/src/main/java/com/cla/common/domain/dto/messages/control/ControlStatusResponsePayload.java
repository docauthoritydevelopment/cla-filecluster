package com.cla.common.domain.dto.messages.control;

/**
 * Created by liora on 10/07/2017.
 */
public class ControlStatusResponsePayload extends ControlMessagePayload {

    private long startScanTaskRequests;
    private long startScanTaskRequestsFinished;
    private long scanTaskResponsesFinished;
    private long scanTaskFailedResponsesFinished;
    private Long activeScanTasks;

    private long startScanTaskRequestsPaused;

    private long ingestTaskRequests;
    private long ingestTaskRequestsFinished;
    private long ingestTaskRequestsFinishedWithError;
    private long ingestTaskRequestFailure;

    private double averageIngestThroughput =0;
    private float scanRunningThroughput = 0f;
    private double ingestRunningThroughput = 0d;
    private double averageScanThroughput = 0f;

    private long failedSignatureCheck;

    private long committedVirtualMemorySize;
    private long totalSwapSpaceSize;
    private long freeSwapSpaceSize;
    private long processCpuTime;
    private long freePhysicalMemorySize;
    private long totalPhysicalMemorySize;
    private double systemCpuLoad;
    private double processCpuLoad;

    private boolean needSearchPatterns = false;

    private String version;

    public long getCommittedVirtualMemorySize() {
        return committedVirtualMemorySize;
    }

    public void setCommittedVirtualMemorySize(long committedVirtualMemorySize) {
        this.committedVirtualMemorySize = committedVirtualMemorySize;
    }

    public long getTotalSwapSpaceSize() {
        return totalSwapSpaceSize;
    }

    public void setTotalSwapSpaceSize(long totalSwapSpaceSize) {
        this.totalSwapSpaceSize = totalSwapSpaceSize;
    }

    public long getFreeSwapSpaceSize() {
        return freeSwapSpaceSize;
    }

    public void setFreeSwapSpaceSize(long freeSwapSpaceSize) {
        this.freeSwapSpaceSize = freeSwapSpaceSize;
    }

    public long getProcessCpuTime() {
        return processCpuTime;
    }

    public void setProcessCpuTime(long processCpuTime) {
        this.processCpuTime = processCpuTime;
    }

    public long getFreePhysicalMemorySize() {
        return freePhysicalMemorySize;
    }

    public void setFreePhysicalMemorySize(long freePhysicalMemorySize) {
        this.freePhysicalMemorySize = freePhysicalMemorySize;
    }

    public long getTotalPhysicalMemorySize() {
        return totalPhysicalMemorySize;
    }

    public void setTotalPhysicalMemorySize(long totalPhysicalMemorySize) {
        this.totalPhysicalMemorySize = totalPhysicalMemorySize;
    }

    public double getSystemCpuLoad() {
        return systemCpuLoad;
    }

    public void setSystemCpuLoad(double systemCpuLoad) {
        this.systemCpuLoad = systemCpuLoad;
    }

    public double getProcessCpuLoad() {
        return processCpuLoad;
    }

    public void setProcessCpuLoad(double processCpuLoad) {
        this.processCpuLoad = processCpuLoad;
    }

    public double getAverageIngestThroughput() {
        return averageIngestThroughput;
    }

    public void setAverageIngestThroughput(double averageIngestThroughput) {
        this.averageIngestThroughput = averageIngestThroughput;
    }

    public double getAverageScanThroughput() {
        return averageScanThroughput;
    }

    public void setAverageScanThroughput(double averageScanThroughput) {
        this.averageScanThroughput = averageScanThroughput;
    }

    public long getStartScanTaskRequests() {
        return startScanTaskRequests;
    }

    public void setStartScanTaskRequests(long startScanTaskRequests) {
        this.startScanTaskRequests = startScanTaskRequests;
    }

    public long getStartScanTaskRequestsFinished() {
        return startScanTaskRequestsFinished;
    }

    public void setStartScanTaskRequestsFinished(long startScanTaskRequestsFinished) {
        this.startScanTaskRequestsFinished = startScanTaskRequestsFinished;
    }

    public long getScanTaskResponsesFinished() {
        return scanTaskResponsesFinished;
    }

    public void setScanTaskResponsesFinished(long scanTaskResponsesFinished) {
        this.scanTaskResponsesFinished = scanTaskResponsesFinished;
    }

    public long getScanTaskFailedResponsesFinished() {
        return scanTaskFailedResponsesFinished;
    }

    public void setScanTaskFailedResponsesFinished(long scanTaskFailedResponsesFinished) {
        this.scanTaskFailedResponsesFinished = scanTaskFailedResponsesFinished;
    }

    public long getIngestTaskRequests() {
        return ingestTaskRequests;
    }

    public void setIngestTaskRequests(long ingestTaskRequests) {
        this.ingestTaskRequests = ingestTaskRequests;
    }

    public long getIngestTaskRequestsFinished() {
        return ingestTaskRequestsFinished;
    }

    public void setIngestTaskRequestsFinished(long ingestTaskRequestsFinished) {
        this.ingestTaskRequestsFinished = ingestTaskRequestsFinished;
    }

    public long getIngestTaskRequestsFinishedWithError() {
        return ingestTaskRequestsFinishedWithError;
    }

    public void setIngestTaskRequestsFinishedWithError(long ingestTaskRequestsFinishedWithError) {
        this.ingestTaskRequestsFinishedWithError = ingestTaskRequestsFinishedWithError;
    }

    public long getIngestTaskRequestFailure() {
        return ingestTaskRequestFailure;
    }

    public void setIngestTaskRequestFailure(long ingestTaskRequestFailure) {
        this.ingestTaskRequestFailure = ingestTaskRequestFailure;
    }
    public float getScanRunningThroughput() {
        return scanRunningThroughput;
    }

    public void setScanRunningThroughput(float scanRunningThroughput) {
        this.scanRunningThroughput = scanRunningThroughput;
    }

    public double getIngestRunningThroughput() {
        return ingestRunningThroughput;
    }

    public void setIngestRunningThroughput(double ingestRunningThroughput) {
        this.ingestRunningThroughput = ingestRunningThroughput;
    }

    public long getStartScanTaskRequestsPaused() {
        return startScanTaskRequestsPaused;
    }

    public void setStartScanTaskRequestsPaused(long startScanTaskRequestsPaused) {
        this.startScanTaskRequestsPaused = startScanTaskRequestsPaused;
    }

    public long getActiveScanTasks() {
        return activeScanTasks==null?0:activeScanTasks.longValue();
    }

    public void setActiveScanTasks(long activeScanTasks) {
        this.activeScanTasks = activeScanTasks;
    }

    public long getFailedSignatureCheck() {
        return failedSignatureCheck;
    }

    public void setFailedSignatureCheck(long failedSignatureCheck) {
        this.failedSignatureCheck = failedSignatureCheck;
    }

    public boolean isNeedSearchPatterns() {
        return needSearchPatterns;
    }

    public void setNeedSearchPatterns(boolean needSearchPatterns) {
        this.needSearchPatterns = needSearchPatterns;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    @Override
    public String toString() {
        return "ServerHandShakeRequestPayload{" +
                " ingestTaskRequestFailure='" + ingestTaskRequestFailure + '\'' +
                " ingestTaskRequestsFinishedWithError='" + ingestTaskRequestsFinishedWithError + '\'' +
                " ingestTaskRequestsFinished='" + ingestTaskRequestsFinished + '\'' +
                " ingestTaskRequests='" + ingestTaskRequests + '\'' +
                " activeScanTasks='" + activeScanTasks + '\'' +
                " startScanTaskRequests='" + startScanTaskRequests + '\'' +
                " startScanTaskRequestsFinished='" + startScanTaskRequestsFinished + '\'' +
                " scanTaskFailedResponsesFinished='" + scanTaskFailedResponsesFinished + '\'' +
                " scanTaskResponsesFinished='" + scanTaskResponsesFinished + '\'' +
                " startScanTaskRequestsPaused='" + startScanTaskRequestsPaused + '\'' +
                " ingestRunningThroughput='" + ingestRunningThroughput + '\'' +
                " scanRunningThroughput='" + scanRunningThroughput + '\'' +
                " averageIngestThroughput='" + averageIngestThroughput + '\'' +
                " freePhysicalMemorySize='" + freePhysicalMemorySize + '\'' +
                " totalPhysicalMemorySize='" + totalPhysicalMemorySize + '\'' +
                " systemCpuLoad='" + systemCpuLoad + '\'' +
                " processCpuLoad='" + processCpuLoad + '\'' +
                " failedSignatureCheck='" + failedSignatureCheck + '\'' +
                " version='" + version + '\'' +
                '}';
    }
}
