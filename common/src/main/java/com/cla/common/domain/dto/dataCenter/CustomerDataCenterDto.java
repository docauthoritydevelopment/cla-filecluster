package com.cla.common.domain.dto.dataCenter;



import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.common.base.Objects;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by liora on 28/05/2017.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CustomerDataCenterDto implements Serializable {
    private long id;

    private String name;
    private String location;
    private String description;
    private Date createdOnDB;    // Time when was added to the system
    private boolean defaultDataCenter;
    private String publicKey;
    private Integer ingestTasksQueueLimit;
    private Integer ingestTaskRetryTime; // in seconds
    private Integer ingestTaskExpireTime; // in seconds

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Date getCreatedOnDB() {
        return createdOnDB;
    }

    public boolean getDefaultDataCenter() {
        return defaultDataCenter;
    }

    public void setDefaultDataCenter(boolean isDefault) {
        this.defaultDataCenter = isDefault;
    }

    public void setCreatedOnDB(Date createdOnDB) {
        this.createdOnDB = createdOnDB;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }

    public Integer getIngestTasksQueueLimit() {
        return ingestTasksQueueLimit;
    }

    public void setIngestTasksQueueLimit(Integer ingestTasksQueueLimit) {
        this.ingestTasksQueueLimit = ingestTasksQueueLimit;
    }

    public Integer getIngestTaskRetryTime() {
        return ingestTaskRetryTime;
    }

    public void setIngestTaskRetryTime(Integer ingestTaskRetryTime) {
        this.ingestTaskRetryTime = ingestTaskRetryTime;
    }

    public Integer getIngestTaskExpireTime() {
        return ingestTaskExpireTime;
    }

    public void setIngestTaskExpireTime(Integer ingestTaskExpireTime) {
        this.ingestTaskExpireTime = ingestTaskExpireTime;
    }

    @Override
    public String toString() {
        return "CustomerDataCenterDto{" +
                "id=" + id +
                    ", name=" + name +
                    ", location='" + location + '\'' +
                    ", description='" + description + '\'' +
                    ", createdOnDB='" + createdOnDB + '\'' +
                    ", defaultDataCenter='" + defaultDataCenter + '\'' +
                    ", publicKey='" + publicKey + '\'' +
                '}';
    }

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof CustomerDataCenterDto))
			return false;
		CustomerDataCenterDto that = (CustomerDataCenterDto) o;
		return getId() == that.getId();
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId(), getName(), getLocation(), getDescription(), getCreatedOnDB(), getDefaultDataCenter(), getPublicKey(), getIngestTasksQueueLimit(), getIngestTaskRetryTime(), getIngestTaskExpireTime());
	}
}
