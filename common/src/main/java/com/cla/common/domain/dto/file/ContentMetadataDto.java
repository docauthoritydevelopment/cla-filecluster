package com.cla.common.domain.dto.file;

import com.cla.connector.domain.dto.file.FileType;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ContentMetadataDto implements Serializable {

    private long contentId;
    private FileType type;
    private String contentSignature;
    private String userContentSignature;

    private String analysisGroupId;
    private String userGroupId;

    private String hint;
    private AnalyzeHint analyzeHint;

    private int fileCount;
    private Long sampleFileId;
    private Long fsFileSize;
    private ClaFileState state;

    private Long itemsCountL1;      // in Solr CatFile
    private Long itemsCountL2;      // in Solr CatFile
    private Long itemsCountL3;      // in Solr CatFile

    private List<String> proposedTitles;

    private String author;      // in Solr CatFile
    private String company;     // in Solr CatFile
    private String subject;     // in Solr CatFile
    private String title;       // in Solr CatFile
    private String keywords;        // NOT in Solr
    private String generatingApp;   // NOT in Solr
    private String template;        // NOT in Solr

    private Date appCreationDate;

    public ContentMetadataDto() {
    }

    public ContentMetadataDto(long contentId,
                              String analysisGroupId,
                              String userGroupId,
                              int typeInx,
                              Long fsFileSize,
                              Long sampleFileId,
                              ClaFileState claFileState,
                              int fileCount,
                              AnalyzeHint analyzeHint,
                              Long itemsCountL3) {
        this.contentId = contentId;
        this.analysisGroupId = analysisGroupId;
        this.userGroupId = userGroupId;
        this.type = FileType.valueOf(typeInx);
        this.fileCount = fileCount;
        this.fsFileSize = fsFileSize;
        this.state = claFileState;
        this.sampleFileId = sampleFileId;
        this.analyzeHint = analyzeHint;
        if (itemsCountL3 != null) {
            this.itemsCountL3 = itemsCountL3;
        }
    }

    public ContentMetadataDto(long contentId,
                              String analysisGroupId,
                              String userGroupId,
                              int typeInx,
                              Long fsFileSize,
                              Long sampleFileId,
                              ClaFileState claFileState,
                              int fileCount,
                              AnalyzeHint analyzeHint,
                              Long itemsCountL3,
                              String hint) {
        this.contentId = contentId;
        this.analysisGroupId = analysisGroupId;
        this.userGroupId = userGroupId;
        this.type = FileType.valueOf(typeInx);
        this.fileCount = fileCount;
        this.fsFileSize = fsFileSize;
        this.state = claFileState;
        this.sampleFileId = sampleFileId;
        this.analyzeHint = analyzeHint;
        this.hint = hint;
        if (itemsCountL3 != null) {
            this.itemsCountL3 = itemsCountL3;
        }
    }

    public long getContentId() {
        return contentId;
    }

    public void setContentId(long contentId) {
        this.contentId = contentId;
    }

    public String getAnalysisGroupId() {
        return analysisGroupId;
    }

    public void setAnalysisGroupId(String analysisGroupId) {
        this.analysisGroupId = analysisGroupId;
    }

    public FileType getType() {
        return type;
    }

    public void setType(FileType type) {
        this.type = type;
    }

    public String getHint() {
        return hint;
    }

    public void setHint(String hint) {
        this.hint = hint;
    }

    public int getFileCount() {
        return fileCount;
    }

    public void setFileCount(int fileCount) {
        this.fileCount = fileCount;
    }

    public Long getSampleFileId() {
        return sampleFileId;
    }

    public void setSampleFileId(Long sampleFileId) {
        this.sampleFileId = sampleFileId;
    }

    public Long getFsFileSize() {
        return fsFileSize;
    }

    public void setFsFileSize(Long fsFileSize) {
        this.fsFileSize = fsFileSize;
    }

    public ClaFileState getState() {
        return state;
    }

    public void setState(ClaFileState state) {
        this.state = state;
    }

    public String getContentSignature() {
        return contentSignature;
    }

    public void setContentSignature(String contentSignature) {
        this.contentSignature = contentSignature;
    }

    public String getUserContentSignature() { return userContentSignature; }

    public void setUserContentSignature(String userContentSignature) { this.userContentSignature = userContentSignature; }

    public List<String> getProposedTitles() { return proposedTitles; }

    public void setProposedTitles(List<String> proposedTitles) { this.proposedTitles = proposedTitles; }

    public Long getItemsCountL1() {
        return itemsCountL1;
    }

    public void setItemsCountL1(Long itemsCountL1) {
        this.itemsCountL1 = itemsCountL1;
    }

    public Long getItemsCountL2() {
        return itemsCountL2;
    }

    public void setItemsCountL2(Long itemsCountL2) {
        this.itemsCountL2 = itemsCountL2;
    }

    public void setItemsCountL3(Long itemsCountL3) {
        this.itemsCountL3 = itemsCountL3;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getGeneratingApp() {
        return generatingApp;
    }

    public void setGeneratingApp(String generatingApp) {
        this.generatingApp = generatingApp;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public Date getAppCreationDate() {
        return appCreationDate;
    }

    public void setAppCreationDate(Date appCreationDate) {
        this.appCreationDate = appCreationDate;
    }

    @Override
    public String toString() {
        return "ContentMetadataDto{" +
                "contentId=" + contentId +
                ", analysisGroupId='" + analysisGroupId + '\'' +
                ", userGroupId='" + userGroupId + '\'' +
                ", type=" + type +
                ", hint='" + hint + '\'' +
                ", fileCount=" + fileCount +
                ", sampleFileId=" + sampleFileId +
                ", fsFileSize=" + fsFileSize +
                ", state=" + state +
                ", proposedTitles=" + proposedTitles +
                '}';
    }

    public AnalyzeHint getAnalyzeHint() {
        return analyzeHint;
    }

    public void setAnalyzeHint(AnalyzeHint analyzeHint) {
        this.analyzeHint = analyzeHint;
    }

    public Long getItemsCountL3() {
        return itemsCountL3;
    }

    public String getUserGroupId() {
        return userGroupId;
    }

    public void setUserGroupId(String userGroupId) {
        this.userGroupId = userGroupId;
    }
}
