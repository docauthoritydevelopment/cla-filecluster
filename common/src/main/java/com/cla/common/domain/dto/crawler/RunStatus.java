package com.cla.common.domain.dto.crawler;

/**
 * Created by uri on 27/10/2015.
 */
public enum RunStatus {
    FINISHED_SUCCESSFULLY("Finished Successfully"),
    NA("N/A"),
    RUNNING("Running"),
    FAILED("Failed"),
    STOPPED("Stopped"),
    UN_USED1("N/A"),   // Left to preserve emum ordinals
    PAUSED("Paused");

    private static RunStatus[] cachedValues;

    static {
        cachedValues = values();
    }

    private final String readableValue;


    RunStatus(String readableValue) {
        this.readableValue = readableValue;
    }

    public static RunStatus of(int ordinal) {
        if (ordinal < 0 || ordinal >= cachedValues.length) {
            throw new IndexOutOfBoundsException("Ordinal value is out of bounds: " + ordinal);
        }
        return cachedValues[ordinal];
    }

    public boolean isActive() {
        return RunStatus.NA == this || RunStatus.RUNNING == this;
    }

    public boolean isFinished() {
        return RunStatus.FINISHED_SUCCESSFULLY == this || RunStatus.FAILED == this || RunStatus.STOPPED == this;
    }

    public static int numInUseStatuses() {
        return RunStatus.values().length - 1; // ignore UN_USED1
    }

    public String getReadableValue() {
        return readableValue;
    }

	public static boolean isProcessing(RunStatus runStatus) {
		return RUNNING == runStatus || PAUSED == runStatus;
	}
}
