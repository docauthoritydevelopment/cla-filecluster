package com.cla.common.domain.dto.mediaproc.excel;

import com.cla.common.domain.dto.extraction.SearchPatternCountDto;
import com.cla.common.domain.dto.mediaproc.SearchPatternServiceInterface;
import com.cla.common.domain.dto.mediaproc.TextualResult;
import com.cla.common.domain.dto.mediaproc.word.WordFile;
import com.cla.common.domain.dto.messages.IngestMessagePayload;
import com.cla.common.utils.signature.SigningStringBuilder;
import com.cla.common.utils.tokens.DocAuthorityTokenizer;
import com.cla.common.utils.tokens.HashedTokensConsumer;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import java.security.MessageDigest;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringJoiner;
import java.util.stream.Collectors;

public class ExcelWorkbook extends IngestMessagePayload<ExcelWorkbookMetaData> implements TextualResult {

    protected static DocAuthorityTokenizer nGramTokenizerHashed = null;

    private HashedTokensConsumer hashedTokensConsumer;

    private List<String> phaseDetails = Lists.newArrayList();
    private final Map<String, ExcelSheet> sheets = Maps.newLinkedHashMap();
    private final Map<String, String> sheetFullNames = Maps.newLinkedHashMap();

    private MessageDigest userSignatureDigest;
    private boolean userContentSignatureEnabled;
    private ExcelParseParams excelParseParams;

    public ExcelWorkbook(ExcelParseParams excelParseParams) {
        this.excelParseParams = excelParseParams;
        this.metadata = new ExcelWorkbookMetaData();
        nGramTokenizerHashed = ExcelParseParams.getDocAuthorityTokenizer();
        if (nGramTokenizerHashed != null) {
            nGramTokenizerHashed.setDebugHashMapper(WordFile.getDebugHashMapper());
        }
    }

    /**
     * Add filename and optional doc properties to the potential titles
     */
    public void processSheetName(final String sheetName) {
        // TODO: implement
    }

    public void addExcelCellContentOnly(String sheetName, String fullSheetName, String cellRefStr, ExcelCell cellValue) {
        ExcelSheet sheet = getExcelSheet(sheetName, fullSheetName);
        sheet.addExcelCellContentOnly(cellRefStr, cellValue, ExcelCell.CELL_TYPE_STRING);
    }

    private ExcelSheet getExcelSheet(String sheetName, String fullSheetName) {
        ExcelSheet sheet = sheets.get(sheetName);
        if (sheet == null) {
            sheet = new ExcelSheet(sheetName, fullSheetName, hashedTokensConsumer, excelParseParams);
            sheet.setUserSignature(SigningStringBuilder.create(userSignatureDigest, userContentSignatureEnabled));
            sheets.put(sheetName, sheet);
            sheetFullNames.put(fullSheetName, sheetName);
        }
        return sheet;
    }

    public void addExcelCell(final String sheetName, final int columnIndex, final int rowNum, final ExcelCell excelCell,
                             final String fullSheetName, final boolean detailedAnalysis, final String cellRefStr, final boolean isTokenMatcherActive) {
        ExcelSheet sheet = getExcelSheet(sheetName, fullSheetName);
        sheet.addExcelCell(columnIndex, rowNum, excelCell, detailedAnalysis, cellRefStr, isTokenMatcherActive);
    }

    // To be called after last cell was added.
    public void processStyle() {
        //getNumOfSkippedCells();
        sheets.values().forEach((sheet) -> {
            sheet.processStyle();
            sheet.finalizeIngestion();
        });
    }

    public Map<String, ExcelSheet> getSheets() {
        return sheets;
    }

    public int getNumOfCells() {
        return metadata.getNumOfCells();
    }

    public Map<String, String> getSheetFullNamesMap() {
        return sheetFullNames;
    }

    public MessageDigest getUserSignatureDigest() {
        return userSignatureDigest;
    }

    public void setUserSignatureDigest(MessageDigest userSignatureDigest) {
        this.userSignatureDigest = userSignatureDigest;
    }

    public void setUserContentSignatureEnabled(boolean userContentSignatureEnabled) {
        this.userContentSignatureEnabled = userContentSignatureEnabled;
    }

    @Override
    public String toString() {
        return "ExcelWorkbook [sheets=" + sheets.keySet() + " numCells=" + getNumOfCells() + "]";
    }

    public static String getMetaDataStringTitle(final String suffix) {
        return "NumCells" + suffix + ",NumFormulas" + suffix;
    }

    public static String toCsvStringTitle(final String sep) {
        return "shInx" + sep + "shName" + sep + ExcelSheet.toCsvStringTitle(sep);
    }

    /**
     * Aggregate Sheet per column
     * @return CSV string
     */
    public String toCsvString() {
        return toCsvString(",");
    }

    public String toCsvString(final String sep) {
        return sheets.values().stream().map((sheet) -> sheet.getSheetName() +
                ",\"" +
                sheet.getFullSheetName() +
                "\"," +
                sheet.toCsvString(",")).collect(Collectors.joining(sep));
    }

    public void setHashedTokensConsumer(final HashedTokensConsumer hashedTokensConsumer) {
        this.hashedTokensConsumer = hashedTokensConsumer;
    }

    public void addPhaseDetail(String phase, Long duration) {
        phaseDetails.add(phase + ":" + duration);
    }

    public String getPhasesDetails() {
        StringJoiner sj = new StringJoiner(",");
        for (String phaseDetail : phaseDetails) {
            sj.add(phaseDetail);
        }
        return sj.toString();

    }

    public static DocAuthorityTokenizer getNGramTokenizer() {
        return nGramTokenizerHashed;
    }

    public static void setNGramTokenizer(final DocAuthorityTokenizer nGramTokenizer) {
        nGramTokenizerHashed = nGramTokenizer;
    }

    public void handleSearchPatterns(String requestId, SearchPatternServiceInterface searchPatternService) {
        if (getSheets() != null) {
            getSheets().values().forEach(sheet -> {
                if (sheet.getOrigContentTokens() != null) {
                    Set<SearchPatternCountDto> patternCountDtos = searchPatternService.getSearchPatternsCounting(sheet.getOrigContentTokens());
                    sheet.setSearchPatternsCounting(patternCountDtos);
                    if (requestId == null) {
                        sheet.removeOrigContentTokens();
                    }
                }
            });
        }
    }

    @Override
    public List<String> getCombinedProposedTitles() {
        return null;
    }

    @Override
    public String getCsvValue() {
        return null;
    }
}
