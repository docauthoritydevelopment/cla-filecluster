package com.cla.common.domain.dto.email;

/**
 * Created by uri on 12-Apr-17.
 */
public class MailNotificationConfigurationDto {

    SmtpConfigurationDto smtpConfigurationDto;

    String addresses;

    boolean enabled;

    public SmtpConfigurationDto getSmtpConfigurationDto() {
        return smtpConfigurationDto;
    }

    public void setSmtpConfigurationDto(SmtpConfigurationDto smtpConfigurationDto) {
        this.smtpConfigurationDto = smtpConfigurationDto;
    }

    public String getAddresses() {
        return addresses;
    }

    public void setAddresses(String addresses) {
        this.addresses = addresses;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public String toString() {
        return "MailNotificationConfigurationDto{" +
                "smtpConfigurationDto=" + smtpConfigurationDto +
                ", addresses='" + addresses + '\'' +
                ", enabled=" + enabled +
                '}';
    }
}
