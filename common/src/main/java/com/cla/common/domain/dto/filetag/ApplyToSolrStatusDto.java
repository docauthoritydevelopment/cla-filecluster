package com.cla.common.domain.dto.filetag;

/**
 * Created by uri on 22/10/2015.
 */
public class ApplyToSolrStatusDto {

    private ApplyToSolrStatus applyToSolrStatus;

    private int applyCounter;
    private int applyCounterMaxMark;

    public ApplyToSolrStatus getApplyToSolrStatus() {
        return applyToSolrStatus;
    }

    public void setApplyToSolrStatus(ApplyToSolrStatus applyToSolrStatus) {
        this.applyToSolrStatus = applyToSolrStatus;
    }

    public int getApplyCounter() {
        return applyCounter;
    }

    public void setApplyCounter(int applyCounter) {
        this.applyCounter = applyCounter;
    }

    public void setApplyCounterMaxMark(int applyCounterMaxMark) {
        this.applyCounterMaxMark = applyCounterMaxMark;
    }

    public int getApplyCounterMaxMark() {
        return applyCounterMaxMark;
    }
}
