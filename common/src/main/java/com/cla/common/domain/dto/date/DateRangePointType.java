package com.cla.common.domain.dto.date;

/**
 * Created by uri on 05/06/2016.
 */
public enum DateRangePointType {
    ABSOLUTE,   // Point contains an absolut date value - e.g. May 22, 2003
    RELATIVE,   // Point contains a value indicating difference from now/today - e.g. 3 years ago
    EVER,       // Special value indicating beginning of time
    UNAVAILABLE  //for files without date
}
