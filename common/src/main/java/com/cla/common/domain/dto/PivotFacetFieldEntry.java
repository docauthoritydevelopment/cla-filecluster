package com.cla.common.domain.dto;

import java.util.LinkedList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PivotFacetFieldEntry extends FacetFieldEntry {
	
	
	public PivotFacetFieldEntry() {
	}

	public PivotFacetFieldEntry(final String field, final String value, final int count) {
		super(field, value, count);
	}

	@JsonProperty
	private List<PivotFacetFieldEntry> pivotFacetFieldEntries = new LinkedList<>();

	public List<PivotFacetFieldEntry> getPivotFacetFieldEntries() {
		return pivotFacetFieldEntries;
	}

	public void setPivotFacetFieldEntries(final List<PivotFacetFieldEntry> pivotFacetFieldEntries) {
		this.pivotFacetFieldEntries = pivotFacetFieldEntries;
	}
	
}
