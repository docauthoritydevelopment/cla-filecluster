package com.cla.common.domain.dto.license;

import java.io.Serializable;

/**
 * Created by uri on 28/06/2016.
 */
public class LicenseImportResultDto implements Serializable {

    DocAuthorityLicenseDto docAuthorityLicenseDto;

    boolean success;

    String reason;

    public LicenseImportResultDto(DocAuthorityLicenseDto docAuthorityLicenseDto, boolean success, String reason) {
        this.docAuthorityLicenseDto = docAuthorityLicenseDto;
        this.success = success;
        this.reason = reason;
    }

    public LicenseImportResultDto(boolean success, String reason) {
        this.docAuthorityLicenseDto = null;
        this.success = success;
        this.reason = reason;
    }

    public LicenseImportResultDto(DocAuthorityLicenseDto docAuthorityLicenseDto) {
        this.docAuthorityLicenseDto = docAuthorityLicenseDto;
        success = true;
    }

    public DocAuthorityLicenseDto getDocAuthorityLicenseDto() {
        return docAuthorityLicenseDto;
    }

    public void setDocAuthorityLicenseDto(DocAuthorityLicenseDto docAuthorityLicenseDto) {
        this.docAuthorityLicenseDto = docAuthorityLicenseDto;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
