package com.cla.common.domain.dto.generic;

import java.io.Serializable;

/**
 * Created by uri on 30/08/2016.
 */
public class TestResultDto implements Serializable {

    boolean result;
    String reason;

    public TestResultDto() {
    }

    public TestResultDto(boolean result) {
        this.result = result;
    }

    public TestResultDto(boolean result, String reason) {
        this.result = result;
        this.reason = reason;
    }

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
