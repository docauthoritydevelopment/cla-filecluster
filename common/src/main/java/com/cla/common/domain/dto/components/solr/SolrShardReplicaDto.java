package com.cla.common.domain.dto.components.solr;

/**
 * Created by uri on 01-Jun-17.
 */
public class SolrShardReplicaDto {
    private String baseName;

    private String coreName;

    private String nodeName;

    private String active;

    private String leader;

    public String getBaseName() {
        return baseName;
    }

    public void setBaseName(String baseName) {
        this.baseName = baseName;
    }

    public String getCoreName() {
        return coreName;
    }

    public void setCoreName(String coreName) {
        this.coreName = coreName;
    }

    public String getNodeName() {
        return nodeName;
    }

    public void setNodeName(String nodeName) {
        this.nodeName = nodeName;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getLeader() {
        return leader;
    }

    public void setLeader(String leader) {
        this.leader = leader;
    }

    @Override
    public String toString() {
        return "SolrShardReplicaDto{" +
                "baseName='" + baseName + '\'' +
                ", coreName='" + coreName + '\'' +
                ", nodeName='" + nodeName + '\'' +
                ", active='" + active + '\'' +
                ", leader='" + leader + '\'' +
                '}';
    }
}
