package com.cla.common.domain.dto.event;

import com.google.common.collect.Maps;

import java.util.Arrays;
import java.util.Map;

/**
 * Created by: yael
 * Created on: 1/10/2018
 * Represent which type of events we're supporting to execute defined policies.
 */
public enum EventType {
    SCAN_DIFF_UPDATED(ScanDiffPayload.class, "File content updated"),
    SCAN_DIFF_ACL_UPDATED(ScanDiffPayload.class, "File ACL updated"),
    SCAN_DIFF_DELETED(ScanDiffPayload.class, "File deleted"),
    /*
     IMPORTANT: Currently support action on this event only on alert tags that are attached to the file folder
     In case of a group we do not have that information yet
      */
    SCAN_DIFF_UNDELETED(ScanDiffPayload.class, "File restored"),
    SCAN_DIFF_NEW(ScanDiffPayload.class, "Rescan new file");

    private static Map<Class<?>, EventType> mappings = Maps.newHashMap();
    private final String value;
    private Class<?> realType;

    static {
        Arrays.stream(EventType.values()).forEach(type -> mappings.put(type.getRealType(), type));
    }

    public static EventType typeOf(Object obj) {
        EventType mpt = mappings.get(obj.getClass());
        if (mpt == null) {
            throw new RuntimeException("Unsupported type " + obj.getClass());
        }
        return mpt;
    }

    EventType(Class<?> realType, String value) {
        this.realType = realType;
        this.value = value;
    }

    public Class<?> getRealType() {
        return realType;
    }

    public boolean in(EventType... types) {
        for (EventType type : types) {
            if (this.equals(type)) {
                return true;
            }
        }
        return false;
    }

    public String getValue() {
        return value;
    }
}
