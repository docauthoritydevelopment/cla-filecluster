package com.cla.common.domain.dto.filetag;

/**
 * Created by uri on 26/08/2015.
 */
public enum ApplyToSolrStatus {
    NOT_STARTED,IN_PROGRESS, CANCELED, DONE
}
