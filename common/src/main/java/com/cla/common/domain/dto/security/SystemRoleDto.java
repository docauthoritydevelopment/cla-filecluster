package com.cla.common.domain.dto.security;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.common.collect.Sets;

import java.io.Serializable;
import java.util.Set;

/**
 * Created by uri on 17/07/2016.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SystemRoleDto implements Serializable {

    private Integer id;
    private String name;
    private String displayName;
    private String description;
    private Set<Authority> authorities = Sets.newHashSet();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setAuthorities(Set<Authority> authorities) {
        this.authorities = authorities;
    }

    public Set<Authority> getAuthorities() {
        return authorities;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    @Override
    public String toString() {
        return "SystemRoleDto{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", authorities=" + authorities +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SystemRoleDto systemRoleDto = (SystemRoleDto) o;

        if (id != null ? !id.equals(systemRoleDto.id) : systemRoleDto.id != null) return false;
        return name != null ? name.equals(systemRoleDto.name) : systemRoleDto.name == null;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }
}
