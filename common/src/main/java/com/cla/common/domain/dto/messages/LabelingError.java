package com.cla.common.domain.dto.messages;

import com.cla.connector.domain.dto.messages.ProcessingError;

/**
 * Created by: yael
 * Created on: 7/4/2019
 */
public class LabelingError extends ProcessingError {
    @Override
    public String toString() {
        return "LabelingError{} " + super.toString();
    }
}
