package com.cla.common.domain.dto.mcf.mcf_api.job;

import java.io.Serializable;

/**
 * Created by uri on 18/07/2016.
 */
public class McfScheduleObjectDto implements Serializable {

    String timezone;

    String duration;

    String dayofweek;

    String monthofyear;
    String dayofmonth;
    String year;

    String hourofday;

    String minutesofhour;

    String requestminimum;

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getDayofweek() {
        return dayofweek;
    }

    public void setDayofweek(String dayofweek) {
        this.dayofweek = dayofweek;
    }

    public String getMonthofyear() {
        return monthofyear;
    }

    public void setMonthofyear(String monthofyear) {
        this.monthofyear = monthofyear;
    }

    public String getDayofmonth() {
        return dayofmonth;
    }

    public void setDayofmonth(String dayofmonth) {
        this.dayofmonth = dayofmonth;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getHourofday() {
        return hourofday;
    }

    public void setHourofday(String hourofday) {
        this.hourofday = hourofday;
    }

    public String getMinutesofhour() {
        return minutesofhour;
    }

    public void setMinutesofhour(String minutesofhour) {
        this.minutesofhour = minutesofhour;
    }

    public String getRequestminimum() {
        return requestminimum;
    }

    public void setRequestminimum(String requestminimum) {
        this.requestminimum = requestminimum;
    }
}
