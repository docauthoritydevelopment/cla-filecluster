package com.cla.common.domain.dto.category;

/**
 * Created by uri on 26/07/2015.
 */
public enum FileCategoryScope {
    ACCOUNT("account_categories"),
    GROUP("group_categories"),
    USER("user_categories");

    private String solrField;

    FileCategoryScope(String solrField) {
        this.solrField = solrField;
    }

    public String getSolrField() {
        return solrField;
    }
}
