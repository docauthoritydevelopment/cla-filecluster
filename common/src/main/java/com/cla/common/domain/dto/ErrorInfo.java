package com.cla.common.domain.dto;

import com.cla.connector.domain.dto.error.BadRequestType;

import java.io.Serializable;

/**
 * Created by uri on 10/05/2016.
 */
public class ErrorInfo implements Serializable {

    public final String url;

    public final String message;

    public BadRequestType type;

    public ErrorInfo(String url, Exception ex) {
        this.url = url;
        this.message = ex.getLocalizedMessage();
    }

    public String getUrl() {
        return url;
    }

    public String getMessage() {
        return message;
    }

    public BadRequestType getType() {
        return type;
    }

    public void setType(BadRequestType type) {
        this.type = type;
    }
}
