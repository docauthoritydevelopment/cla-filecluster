package com.cla.common.domain.dto.workflow;

public enum WorkflowState {
    NEW, CLOSED, GDPR_DISCOVER, GDPR_RESOLVE, GDPR_PENDING_APPROVAL, GDPR_APPROVED, GDPR_PENDING_ACTION, GDPR_ACTION_DONE;
}
