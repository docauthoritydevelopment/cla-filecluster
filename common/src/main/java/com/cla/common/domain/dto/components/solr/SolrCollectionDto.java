package com.cla.common.domain.dto.components.solr;

/**
 * Created by uri on 04-Jun-17.
 */
public class SolrCollectionDto {

    private String name;

    public SolrCollectionDto() {
    }

    public SolrCollectionDto(String name) {

        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "SolrCollectionDto{" +
                "name='" + name + '\'' +
                '}';
    }
}
