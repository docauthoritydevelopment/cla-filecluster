package com.cla.common.domain.dto.action;

import java.io.Serializable;

/**
 * Created by uri on 28/01/2016.
 */
public class ActionDefinitionDto implements Serializable {

    private String id;
    private String name;
    private String javaMethodName;
    private String description;
    private ActionClass actionClass;

    public ActionDefinitionDto() {
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setJavaMethodName(String javaMethodName) {
        this.javaMethodName = javaMethodName;
    }

    public String getJavaMethodName() {
        return javaMethodName;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setActionClass(ActionClass actionClass) {
        this.actionClass = actionClass;
    }

    public ActionClass getActionClass() {
        return actionClass;
    }

    @Override
    public String toString() {
        return "ActionDefinitionDto{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", javaMethodName='" + javaMethodName + '\'' +
                ", actionClass=" + actionClass +
                '}';
    }

    public ActionDefinitionDto(ActionDefinitionDto other) {
        this.id = other.id;
        this.name = other.name;
        this.javaMethodName = other.javaMethodName;
        this.description = other.description;
        this.actionClass = other.actionClass;
    }
}
