package com.cla.common.domain.dto.filesearchpatterns;

import java.io.Serializable;
import java.util.List;

public class PatternCategoryDto implements Serializable {

    private int id;

    private String name;

    private boolean active;

    private Integer parentId;

    private List<TextSearchPatternDto> patterns;

    public List<TextSearchPatternDto> getPatterns() {
        return patterns;
    }

    public void setPatterns(List<TextSearchPatternDto> patterns) {
        this.patterns = patterns;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public String toString() {
        String sb = "RegulationDto{" + "id=" + id +
                ", parentId=" + parentId +
                ", name='" + name + '\'' +
                ", active=" + active +
                "}";
        return sb;
    }
}
