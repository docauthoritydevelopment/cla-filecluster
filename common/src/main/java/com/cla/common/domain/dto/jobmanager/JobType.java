package com.cla.common.domain.dto.jobmanager;

import java.util.ArrayList;
import java.util.List;

public enum JobType {
    UNKNOWN("N/A"),
    SCAN("Map"),
    SCAN_FINALIZE("Map finalize"),
    INGEST("Ingest"),
    INGEST_FINALIZE("Ingest finalize"),
    OPTIMIZE_INDEXES("Optimize indexes"),
    ANALYZE("Analyze"),
    ANALYZE_FINALIZE("Analyze finalize"),
    EXTRACT("Extract"),
    EXTRACT_FINALIZE("Extract finalize"),
    LABELING("Labeling");

    private static final JobType[] values = values();

    private String name;

    JobType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static JobType valueOf(int ordinal) {
        return (ordinal < 0 || ordinal >= values.length)? null : values[ordinal];
    }

    /**
     * Returns a list of JobType objects that come after the given one.
     *
     * @param jobType   the JobType for which to return following JobTypes
     * @return  a list of all JobTypes that follow the given argument
     */
    public static List<JobType> getJobTypesFollowing(JobType jobType) {
        List<JobType> result  = new ArrayList<>();
        for (int i = jobType.ordinal()+1; i < values.length; i++) {
            result.add(values[i]);

        }
        return result;
    }
}
