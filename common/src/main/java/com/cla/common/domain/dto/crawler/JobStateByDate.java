package com.cla.common.domain.dto.crawler;

import com.cla.common.domain.dto.jobmanager.JobState;

/**
 * Created by: yael
 * Created on: 6/16/2019
 */
public class JobStateByDate {

    private Long jobId;
    private JobState state;
    private Long timstamp;

    public static JobStateByDate of(Long jobId, JobState state, Long timstamp){
        return new JobStateByDate(jobId, state, timstamp);
    }

    public JobStateByDate() {
    }

    public JobStateByDate(Long jobId, JobState state, Long timstamp) {
        this.jobId = jobId;
        this.state = state;
        this.timstamp = timstamp;
    }

    public Long getJobId() {
        return jobId;
    }

    public void setJobId(Long jobId) {
        this.jobId = jobId;
    }

    public JobState getState() {
        return state;
    }

    public void setState(JobState state) {
        this.state = state;
    }

    public Long getTimstamp() {
        return timstamp;
    }

    public void setTimstamp(Long timstamp) {
        this.timstamp = timstamp;
    }

    @Override
    public String toString() {
        return "JobStateByDate{" +
                "jobId=" + jobId +
                ", state=" + state +
                ", timstamp=" + timstamp +
                '}';
    }
}
