package com.cla.common.domain.dto.category;

/**
 * Created by uri on 26/07/2015.
 */
public enum FileCategoryType {

    BUSINESS_ID_FILTER("BID");

    private String solrName;

    FileCategoryType(String solrName) {
        this.solrName = solrName;
    }

    public String getSolrName() {
        return solrName;
    }
}
