package com.cla.common.domain.dto.file;

import com.google.common.collect.Maps;

import java.util.Map;

/**
 * Created by uri on 19/08/2015.
 */
public enum ClaFileState {
    SCANNED,
    SCANNED_AND_STOPPED,
    STARTED_INGESTING, // deprecated
    INGESTED,
    STARTED_ANALYSING,
    ANALYSED,
    SKIP, // deprecated
    ERROR,
    DELETED, // deprecated
    OFFLINE; // deprecated


    private static Map<Integer, ClaFileState> map = Maps.newHashMap();

    static {
        for (ClaFileState state : ClaFileState.values()) {
            map.put(state.ordinal(), state);
        }
    }

    public static ClaFileState valueOf(int state) {
        return map.get(state);
    }

    public boolean equals(String name){
        return this.name().equals(name);
    }

    public boolean shouldHaveContent() {
        return this.equals(INGESTED) || this.equals(STARTED_ANALYSING) || this.equals(ANALYSED);
    }

    public boolean alreadyIngested() {
        return this.equals(INGESTED) || this.equals(STARTED_ANALYSING) || this.equals(ANALYSED);
    }
}
