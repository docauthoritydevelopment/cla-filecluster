package com.cla.common.domain.dto.mediaproc.excel;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.beans.Transient;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class ExcelCell {
	public static final int CELL_TYPE_NUMERIC = 0;
	public static final int CELL_TYPE_STRING = 1;
	public static final int CELL_TYPE_BLANK = 3;
	public static final int CELL_TYPE_BOOLEAN = 4;
	public static final int CELL_TYPE_ERROR = 5;

	@JsonProperty
	private String stringValue;

	@JsonIgnore
	private transient Double numericValue;

	@JsonProperty
	private String formula;

	@JsonProperty
	private String formulaIngestion;

	@JsonProperty
	private int type;

	@JsonProperty
	private ExcellCellStyle cellStyle;

	@JsonProperty
	private boolean ingestionError;

	@JsonIgnore
	private transient boolean isDate = false;
	
	/**
     * This method is private but it is called using reflection by java
     * serialization mechanism. It overwrites the default object serialization.
     *
     * <br/><br/><b>IMPORTANT</b>
     * The access modifier for this method MUST be set to <b>private</b> otherwise {@link java.io.StreamCorruptedException}
     * will be thrown.
     *
     */
    public void MyWriteObject(final ObjectOutputStream oos) throws IOException {
        //System.out.println(ExcelSheet.class.getName() + ".writeObject");

        //oos.defaultWriteObject();
        
        // scalars
        oos.writeObject(stringValue);
        oos.writeObject(formula);
        oos.writeObject(formulaIngestion);
        oos.writeInt(type);
        //oos.writeObject(cellStyle);
        cellStyle.MyWriteObject(oos);
    }
 
    /**
     * This method is private but it is called using reflection by java
     * serialization mechanism. It overwrites the default object serialization.
     *
     * <br/><br/><b>IMPORTANT</b>
     * The access modifier for this method MUST be set to <b>private</b> otherwise {@link java.io.StreamCorruptedException}
     * will be thrown.
     *
     */
    public void MyReadObject(final ObjectInputStream ois) throws IOException, ClassNotFoundException {
        //System.out.println(ExcelSheet.class.getName() + ".readObject");

        //ois.defaultReadObject();
        
        // scalars
        stringValue = (String) ois.readObject();
        formula = (String) ois.readObject();
        formulaIngestion = (String) ois.readObject();
        type = ois.readInt();
        //cellStyle = (ExcellCellStyle) ois.readObject();
        cellStyle = new ExcellCellStyle();
        cellStyle.MyReadObject(ois);
    }
    

	public ExcelCell() {
	}

	public Double getNumericValue() {
		return numericValue;
	} // transient - should not be used in app server only in mp

	public void setNumericValue(final double numericValue) {
		this.numericValue = numericValue;
		if (stringValue == null) {
			this.stringValue = Double.valueOf(numericValue).toString();
		}
	}

	public String getStringValue() {
		return stringValue;
	}

	public void setStringValue(final String stringValue) {
		this.stringValue = stringValue;
	}

	public boolean isBooleanValue() {
		return (type == CELL_TYPE_BOOLEAN);
	}

	public boolean getBooleanValue() {
		return getBooleanValue(stringValue);
	}

    public static boolean getBooleanValue(String stringValue) {
        return (Integer.valueOf(stringValue) != 0);
    }

    public void setBooleanValue(final boolean booleanValue) {
		this.stringValue = booleanValue?"1":"0";
	}

	public boolean isBlank() {
		return (type == CELL_TYPE_BLANK);
	}

	public boolean isError() {
		return (type == CELL_TYPE_ERROR);
	}

	public boolean isIngestionError() {
		return ingestionError;
	}

	public void setIngestionError(final boolean ingestionError) {
		this.ingestionError = ingestionError;
	}

	public String getFormula() {
		return formula;
	}

	public void setFormula(final String formula) {
		this.formula = formula;
//		if (formula.length() > 25) {
//			this.formula = "" + IngestionUtils.MD5Long(formula);
//		}
	}

	public String getFormulaIngestion() {
		return formulaIngestion;
	}

	public void setFormulaIngestion(final String formulaIngestion) {
		this.formulaIngestion = formulaIngestion;
//		if (formulaIngestion.length() > 25) {
//			this.formulaIngestion = "" + IngestionUtils.MD5Long(formulaIngestion);
//		}
	}

	public int getType() {
		return type;
	}

	public void setType(final int type) {
		this.type = type;
	}

	public boolean isDate() {
		return isDate;
	} // transient - should not be used in app server only in mp

	public void setDate(boolean date) {
		isDate = date;
	}

	public ExcellCellStyle getCellStyle() {
		return cellStyle;
	}

	public void setCellStyle(final ExcellCellStyle cellStyle) {
		this.cellStyle = cellStyle;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		//long temp;
		//temp = Double.doubleToLongBits(numericValue);
		//result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((stringValue == null) ? 0 : stringValue.hashCode());
		result = prime * result + ((cellStyle == null) ? 0 : cellStyle.hashCode());
		result = prime * result + type;
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final ExcelCell other = (ExcelCell) obj;
		if (type != other.type)
			return false;
		//if (Double.doubleToLongBits(numericValue) != Double.doubleToLongBits(other.numericValue))
		//	return false;
		if (stringValue == null) {
			if (other.stringValue != null)
				return false;
		} else if (!stringValue.equals(other.stringValue))
			return false;
		// Ignore cellStyle comparison for now
		return true;
	}

	public boolean valueEquals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final ExcelCell other = (ExcelCell) obj;
		if (type != other.type)
			return false;
		//if (Double.doubleToLongBits(numericValue) != Double.doubleToLongBits(other.numericValue))
		//	return false;
		if (stringValue == null) {
			if (other.stringValue != null)
				return false;
		} else if (!stringValue.equals(other.stringValue))
			return false;
		return true;
	}

	public boolean formulaEquals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final ExcelCell other = (ExcelCell) obj;		
		if (formula == null || other.formula==null) {
			// if (other.formulla != null)
			return false;
		} else if (!formula.equals(other.formula))
			return false;
		return true;
	}


	public boolean styleEquals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final ExcelCell other = (ExcelCell) obj;		
		if (cellStyle == null) {
			if (other.cellStyle != null)
				return false;
		} else if (!cellStyle.equals(other.cellStyle))
			return false;
		return true;
	}

	@Override
	public String toString() {
		final String prefix = (formula==null)?"V->":"F("+formula+")->";
		return doToString(type,stringValue,prefix)+" "+((cellStyle==null)?"":cellStyle.toString());
	}

	public static String doToString(int cellType, String stringValue, final String prefix) {
		switch (cellType) {
		case CELL_TYPE_BLANK:
			return prefix + cellType + ":" + "BLANK";
		case CELL_TYPE_BOOLEAN:
			return prefix +cellType + ":" + getBooleanValue(stringValue);
		case CELL_TYPE_ERROR:
			return prefix +cellType + ":" + "ERROR";
		case CELL_TYPE_NUMERIC:
			return prefix +cellType + ":" + stringValue;
		case CELL_TYPE_STRING:
			return prefix +cellType + ":" + stringValue;
		}
		throw new RuntimeException("no mapping for "+cellType);
	}
	
	public String toStringValue() { // numericValue is transient - should not be used in app server only in mp
		final String prefix ="";
		if (type == CELL_TYPE_NUMERIC) {
			return doToString(type,numericValue.toString(),prefix);
		}
		return doToString(type,stringValue,prefix);
	}
	
	public boolean isFormula(){
		return formula != null;
	}
}
