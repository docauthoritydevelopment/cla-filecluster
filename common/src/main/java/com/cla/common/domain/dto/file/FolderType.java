package com.cla.common.domain.dto.file;

import com.cla.connector.utils.FileNamingUtils;
import com.google.common.collect.Maps;

import java.util.Map;
import java.util.Objects;

import static com.cla.common.domain.pst.PstPath.PST_PSEUDO_FOLDER_MARKER;

/**
 * Created by: yael
 * Created on: 5/30/2018
 */
public enum FolderType {
    REGULAR(0, null),
    ZIP(1, ".zip*"),
    PST(2, PST_PSEUDO_FOLDER_MARKER),
    EML(3, ".eml"),
    MAILBOX(4, null)
    ;

    private static final Map<Integer, FolderType> intMapping = Maps.newHashMap();
    private static final Map<String, FolderType> extMapping = Maps.newHashMap();
    private final int value;
    private final String extension;

    FolderType(final int value, final String extension) {
        this.value = value;
        this.extension = extension;
    }

    static {
        for (final FolderType t : FolderType.values()) {
            intMapping.put(t.value, t);
            if (t.extension != null) {
                extMapping.put(t.extension, t);
            }
        }
    }

    public int toInt() {
        return value;
    }

    public static FolderType valueOf(final int value) {
        final FolderType mt = FolderType.intMapping.get(value);
        if (mt == null) {
            throw new RuntimeException("Invalid value: " + value);
        }
        return mt;
    }

    public static FolderType getFromName(final String folderName) {
        Objects.requireNonNull(folderName, "folderName can't be null");
        String folderPseudoExt = "." + FileNamingUtils.getFilenameExtension(folderName).toLowerCase();
        if (extMapping.keySet().contains(folderPseudoExt)) {
            return extMapping.get(folderPseudoExt);
        }
        return FolderType.REGULAR;
    }
}
