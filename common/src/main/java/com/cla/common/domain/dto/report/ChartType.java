package com.cla.common.domain.dto.report;

public enum ChartType {
    YEARLY_TOP_EXTENSIONS,
    SUNBURST_CHART,
    MULTIPLE_FIXED_HISTOGRAM,
    SINGLE_VALUE_CHART
}
