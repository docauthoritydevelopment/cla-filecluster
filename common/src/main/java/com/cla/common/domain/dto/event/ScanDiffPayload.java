package com.cla.common.domain.dto.event;

/**
 * Created by: yael
 * Created on: 1/11/2018
 */
public class ScanDiffPayload implements EventPayload {
    private Long fileId;
    private String path;

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public String toString() {
        return "ScanDiffPayload{" +
                "fileId=" + fileId +
                ", path='" + path + '\'' +
                '}';
    }
}
