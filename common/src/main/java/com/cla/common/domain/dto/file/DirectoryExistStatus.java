package com.cla.common.domain.dto.file;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by liron on 7/27/2017.
 */
public enum DirectoryExistStatus {
    YES(0, "YES"),
    NO(1, "NO"),
    UNKNOWN(2, "UNKNOWN");

    DirectoryExistStatus(int value, String string) {
        this.value = value;
        this.string = string;
    }

    private static final Map<Integer, DirectoryExistStatus> intMapping = new HashMap<Integer, DirectoryExistStatus>();
    private int value;
    private String string;

    static {
        for (final DirectoryExistStatus v : DirectoryExistStatus.values()) {
            intMapping.put(v.value, v);
        }
    }

    @Override
    public String toString() {
        return string;
    }

    public int getValue() {
        return value;
    }

    public static DirectoryExistStatus valueOf(final int value) {
        final DirectoryExistStatus mt = DirectoryExistStatus.intMapping.get(value);
        if (mt == null) {
            throw new RuntimeException("Invalid value: " + value);
        }
        return mt;
    }
}
