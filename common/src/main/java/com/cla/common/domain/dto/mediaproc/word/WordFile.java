package com.cla.common.domain.dto.mediaproc.word;

import com.cla.common.domain.dto.histogram.ClaHistogramCollection;
import com.cla.common.domain.dto.histogram.ClaSuperCollection;
import com.cla.common.domain.dto.histogram.HistogramType;
import com.cla.common.domain.dto.mediaproc.excel.IngestConstants;
import com.cla.common.domain.dto.messages.ExtractionResult;
import com.cla.common.domain.dto.pv.PVType;
import com.cla.common.domain.dto.word.WordfileMetaData;
import com.cla.common.utils.tokens.DocAuthorityTokenizer;
import com.cla.common.utils.tokens.HashedTokensConsumer;
import com.google.common.collect.Maps;
import org.apache.commons.io.FilenameUtils;

import java.io.PrintStream;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;


public class WordFile extends ExtractionResult {


    private static Map<Long, String> debugHashMapper = null;
    private static DocAuthorityTokenizer nGramTokenizerHashed = null;


    // Fields used for Entity extraction tokenization
    private String body;
    private String header;
    private String footer;

    private String origBody;
    private String origHeader;
    private String origFooter;

    private boolean isScannedDoc = false;

    // Set to true only in development controlled builds/runs
    private static final boolean isStyleDebugActive = false;

    private Integer id;

    private WordfileMetaData metaData = new WordfileMetaData();

    private final ClaSuperCollection<Long> longHistograms = new ClaSuperCollection<>();

    private Map<String, String> proposedTitles = Maps.newHashMap();

    transient private String lastStyleSignature = null;
    transient private String lastAbsoluteStyleSignature = null;

    transient private boolean bodyLongTextProcessed = false;

    transient private FontStats fontSizeStats = new FontStats();

    private HashedTokensConsumer hashedTokensConsumer;

    public WordFile() {
        init();
    }

    public WordFile(final String fileName) {
        init();
        this.fileName = fileName;
    }

    public WordFile(final int id, final String fileName) {
        init();
        this.id = id;
        this.fileName = fileName;
    }

    public String getBody() {
        return body;
    }

    public void setBody(final String body) {
        this.body = body;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(final String header) {
        this.header = header;
    }

    private String getOrigBody() {
        return origBody;
    }

    public void setOrigBody(final String origBody) {
        this.origBody = origBody;
    }

    private String getOrigHeader() {
        return origHeader;
    }

    public void setOrigHeader(final String origHeader) {
        this.origHeader = origHeader;
    }

    private String getOrigFooter() {
        return origFooter;
    }

    public void setOrigFooter(final String origFooter) {
        this.origFooter = origFooter;
    }

    public String getFooter() {
        return footer;
    }

    public void setFooter(final String footer) {
        this.footer = footer;
    }

    public ClaSuperCollection<Long> getLongHistograms() {
        return longHistograms;
    }

    private void init() {

        createHistograms();

        getLongHistogram(PVType.PV_StyleSignatures).setItemCounterLimit(3);
        getLongHistogram(PVType.PV_StyleSignatures).setEnforceDuringInsert(true);    // better performance

        getLongHistogram(PVType.PV_StyleSequence).setItemCounterLimit(3);
        getLongHistogram(PVType.PV_StyleSequence).setEnforceDuringInsert(true);    // better performance

        getLongHistogram(PVType.PV_LetterHeadLabels).setItemCounterLimit(1);
        getLongHistogram(PVType.PV_LetterHeadLabels).setEnforceDuringInsert(true);    // better performance; maintain items order
        getLongHistogram(PVType.PV_SubBodyNgrams).setType(HistogramType.MARKER);

//		if (tokenMatcherActive) {
//			tokensCollection = new ClaTokensCollection<Long>();
//		}
    }

    private void createHistograms() {
//		for (int i=0;i<strHistogramNames.length;++i) {
//			strHistograms.getCollections().put(strHistogramNames[i], new ClaHistogramCollection<String>());
//		}
        for (PVType longHistogramName : ClaHistogramCollection.wordLongHistogramNames) {
            longHistograms.getCollections().put(longHistogramName.name(), new ClaHistogramCollection<>());
        }
    }

    public ClaHistogramCollection<Long> getLongHistogram(final PVType name) {
        return longHistograms.getCollections().get(name.name());
    }

//	public ClaHistogramCollection<String> getStrHistogram(final String name) {
//		return strHistograms.getCollections().get(name);
//	}

    public void histogramAddItem(final PVType name, final String item) {
//		final ClaHistogramCollection<String> hist = getStrHistogram(name);
//		if (hist == null)
//			throw new RuntimeException("Histogram name not found: " + name);
//		hist.addItem(item);
        histogramAddItem(name, nGramTokenizerHashed.MD5Long(item));
    }

    private void histogramAddItem(final PVType name, final Long item) {
        final ClaHistogramCollection<Long> hist = getLongHistogram(name);
        if (hist == null) {
            throw new RuntimeException("Histogram name not found: " + name);
        }
        hist.addItem(item);
    }

    public void histogramAddStrItems(final PVType name, final List<String> items) {
//		final ClaHistogramCollection<String> hist = getStrHistogram(name);
//		if (hist == null)
//			throw new RuntimeException("Histogram name not found: " + name);
//		hist.addItems(items);
        final List<Long> longItems = items.stream().map(i -> nGramTokenizerHashed.MD5Long(i)).collect(toList());
        histogramAddLongItems(name, longItems);
    }

    public void histogramAddLongItems(final PVType name, final Collection<Long> items) {
        final ClaHistogramCollection<Long> hist = getLongHistogram(name);
        if (hist == null) {
            throw new RuntimeException("Histogram name not found: " + name);
        }
        hist.addItems(items);
    }

    public float getSimilarity(final WordFile other, final PVType name) {
//		final ClaHistogramCollection<String> strHist = getStrHistogram(name);
//		if (strHist != null) {
//			return strHist.getNormalizedSimilarityRatio(other.getStrHistogram(name));
//		}
        return getLongHistogram(name).getNormalizedSimilarityRatio(other.getLongHistogram(name));
    }

    public int getOrderedMatchCount(final WordFile other, final PVType name) {
//		final ClaHistogramCollection<String> strHist = getStrHistogram(name);
//		if (strHist != null) {
//			return strHist.getOrderedSimilarityCount(other.getStrHistogram(name));
//		}
        return getLongHistogram(name).getOrderedSimilarityCount(other.getLongHistogram(name));
    }

    public Integer getId() {
        return id;
    }

    public void setId(final Integer id) {
        this.id = id;
    }

    public boolean isBodyLongTextProcessed() {
        return bodyLongTextProcessed;
    }

    public void setBodyLongTextProcessed(final boolean bodyLongTextProcessed) {
        this.bodyLongTextProcessed = bodyLongTextProcessed;
    }

    private void addStyleSignature(final String styleSignature, final String absuluteStyleSignature, final boolean countInPairs) {
//		if (isStyleDebugActive && styleDebFile!=null) {
//			styleDebFile.println("abs-st=" + absuluteStyleSignature);
//		}
        if (styleSignature != null) {
            histogramAddItem(PVType.PV_StyleSignatures, styleSignature);
            histogramAddItem(PVType.PV_AllStyleSignatures, styleSignature);
            if (countInPairs) {
                if (lastStyleSignature != null) {
                    final String pair = lastStyleSignature + "|" + styleSignature;
                    histogramAddItem(PVType.PV_StyleSequence, pair);
                    histogramAddItem(PVType.PV_AllStyleSequence, pair);
                }
                lastStyleSignature = styleSignature;
            }
        }
        if (absuluteStyleSignature != null) {
            histogramAddItem(PVType.PV_AbsoluteStyleSignatures, absuluteStyleSignature);
            if (countInPairs) {
                if (lastAbsoluteStyleSignature != null) {
                    final String pair = lastAbsoluteStyleSignature + "|" + absuluteStyleSignature;
                    histogramAddItem(PVType.PV_AbsoluteStyleSequence, pair);
                }
                lastAbsoluteStyleSignature = absuluteStyleSignature;
            }
        }
    }

    transient private List<String> delatedStyleSigFontNames = new ArrayList<>(1000);
    transient private List<String> delatedStyleSigSemiSignature = new ArrayList<>(1000);
    transient private List<Boolean> delatedStyleSigCountInPairs = new ArrayList<>(1000);
    transient private FontStats delatedStyleFontStats = new FontStats();

//    public void addDelayedStyleSignature(final String fontName, final String semiStyleSignature, final int textLength) {
//        addDelayedStyleSignature(fontName, semiStyleSignature, textLength, true);
//    }

    // Store added items and add them during ingestion=finalization when fonts stats is ready.
    public void addDelayedStyleSignature(final String fontName, final String semiStyleSignature, final int textLength, final boolean countInPairs) {
        delatedStyleSigFontNames.add(fontName);
        delatedStyleSigSemiSignature.add(semiStyleSignature);
        delatedStyleSigCountInPairs.add(countInPairs);
        delatedStyleFontStats.addRun(fontName, 0, textLength);
    }

    private PrintStream styleDebFile = null;

    private void processDelayedStyles(final boolean absStylesOnly) {
        List<String> debStyles = null;
        if (isStyleDebugActive) {
            System.out.println("WordFile.java: isStyleDebugActive=true !!!");
            if (fileName.contains("/test/")) {
                throw new RuntimeException("WordFile.java: isStyleDebugActive=true");
            }
            try {
                styleDebFile = new PrintStream("./logs/ingestStyleDeb_"
                        + FilenameUtils.getBaseName(fileName)
                        + "_"+FilenameUtils.getExtension(fileName)
                        + ".log");
                debStyles = new ArrayList<>();
            } catch (Exception e) {
                // do nothing
            }
        }

        final Map<String, String> repFontsMap = new HashMap<>();
        for (int i = 0; i < delatedStyleSigFontNames.size(); ++i) {
            final String origFont = delatedStyleSigFontNames.get(i);
            final String semiSig = delatedStyleSigSemiSignature.get(i);
            final boolean countInPairs = delatedStyleSigCountInPairs.get(i);
            String repFont = repFontsMap.get(origFont);
            //noinspection Java8ReplaceMapGet
            if (repFont == null) {
                repFont = String.format("F%03d", delatedStyleFontStats.getFontNameOrderedIndex(origFont));
                repFontsMap.put(origFont, repFont);
            }
            addStyleSignature(absStylesOnly ? null : (origFont + semiSig),
                    repFont + semiSig, countInPairs);
            if (debStyles!=null) {
                debStyles.add(repFont+semiSig);
            }
        }
        delatedStyleSigFontNames.clear();
        delatedStyleSigSemiSignature.clear();
        delatedStyleSigCountInPairs.clear();

        if (styleDebFile!=null) {
            if (debStyles!=null) {
                Map<String, Long> counted = debStyles.stream()
                        .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
                counted.entrySet().stream()
                        .sorted((a,b) -> (a.getValue().equals(b.getValue())) ?
                                a.getKey().compareTo(b.getKey()) : Long.compare(b.getValue(), a.getValue()))
                        .forEach(e -> styleDebFile.println("abs-style " + e.getKey() + " : " + e.getValue()));
            }
            try {
                styleDebFile.close();
            } catch (Exception e) {
                // do nothing
            }
        }
    }

    public void addHeaderStyleSignature(final String styleSignature) {

        histogramAddItem(PVType.PV_HeaderStyles, styleSignature);
    }

    public void finalizeIngestion() {
        finalizeIngestion(false);
    }

    public void finalizeIngestion(final boolean absStylesOnly) {
        Arrays.asList(ClaHistogramCollection.wordLongHistogramNames)
                .forEach(name -> getLongHistogram(name).applyLimitations());

        processDelayedStyles(absStylesOnly);
    }

    public String getCsvString() {
        return Arrays.stream(ClaHistogramCollection.wordLongHistogramNames)
                        .map(name -> name.toString().substring(0, 3) + ":" + getLongHistogram(name).getPropertiesString())
                        .collect(Collectors.joining(","));
    }

    /**
     * Return a new object with property histograms containing the difference between the two docs.
     * Used for debug ...
     */
    public WordFile diff(final WordFile other) {
        if (other == null) {
            return null;
        }

        final WordFile result = new WordFile();
        result.setFileName("@Diff:" + getFileName() + "-" + other.getFileName());

//        final ClaSuperCollection<String> newStrHistograms = new ClaSuperCollection<>();
        final ClaSuperCollection<Long> newLongHistograms = new ClaSuperCollection<>();

//		Arrays.asList(WordFile.strHistogramNames).stream()
//		.forEach(name->newStrHistograms.setCollection(name,
//				new ClaHistogramCollection<String>(this.getStrHistogram(name).diff(other.getStrHistogram(name)))));
        Arrays.stream(ClaHistogramCollection.wordLongHistogramNames)
                .forEach(name -> newLongHistograms.setCollection(name.name(),
                        new ClaHistogramCollection<>(this.getLongHistogram(name).diff(other.getLongHistogram(name)))));

//		result.strHistograms.setCollections(newStrHistograms.getCollections());
        result.longHistograms.setCollections(newLongHistograms.getCollections());

        return result;
    }

//	public static void setTokenMatcher(final boolean activate) {
//		tokenMatcherActive = activate;
//	}

//	public static boolean isTokenMatcherActive() {
//		return tokenMatcherActive;
//	}

    public static Map<Long, String> getDebugHashMapper() {
        return debugHashMapper;
    }

    public static void setDebugHashMapper(final Map<Long, String> debugHashMapper) {
        WordFile.debugHashMapper = debugHashMapper;
    }

    public WordfileMetaData getMetaData() {
        return metaData;
    }

    public void setMetaData(final WordfileMetaData metaData) {
        this.metaData = metaData;
    }

    public Map<String, String> getProposedTitles() {
        return proposedTitles;
    }


    public List<String> getProposedTitlesCombined() {
        return proposedTitles.entrySet().stream()
                .sorted(Comparator.comparing(Map.Entry::getKey))    // sort ascending
                .map(e -> (e.getKey() + IngestConstants.PROPOSED_TITLE_KEY_SEPARATOR + e.getValue()))
                .collect(Collectors.toList());
    }

    public void addProposedTitles(final String keyPrefix, final int titleRate, String proposedTitle, final boolean joinPrev) {
        proposedTitle = DocAuthorityTokenizer.resetWhiteSpaces(proposedTitle);
        if (joinPrev) {
            // If prev title was with same key/rate then join the two
            final String key = String.format(IngestConstants.TITLE_KEY_FORMAT_STRING, keyPrefix, titleRate, getProposedTitlesCount());
            final String prevTitle = proposedTitles.get(key);
            if (prevTitle != null && (prevTitle.length() + proposedTitle.length()) < IngestConstants.MAX_TITLE_LENGTH) {
                final String newTitle = prevTitle + " " + proposedTitle;
                if (proposedTitles.values().contains(newTitle)) {
                    proposedTitles.remove(key);     // remove pre-joined previous title
                } else {
                    proposedTitles.put(key, newTitle);
                }
                return;
            }
        }
        // No duplications .. (overcome duplicate header/footer processing)
        if (!proposedTitles.values().contains(proposedTitle)) {
            proposedTitles.put(String.format(IngestConstants.TITLE_KEY_FORMAT_STRING, keyPrefix, titleRate, 1 + getProposedTitlesCount()), proposedTitle);
        }
    }

    public int getProposedTitlesCount() {
        return proposedTitles.size();
    }

    public void processFontStats(final String fontName, final int size, final int lenght) {
        fontSizeStats.addRun(fontName, size, lenght);
    }

    public void setHashedTokensConsumer(final HashedTokensConsumer hashedTokensConsumer) {
        this.hashedTokensConsumer = hashedTokensConsumer;
    }

    public HashedTokensConsumer getHashedTokensConsumer() {
        return hashedTokensConsumer;
    }

    public String getOrigContent() {
        return getOrigHeader() + "\n" + getOrigBody() + "\n" + getOrigFooter();
    }

    public boolean isScannedDoc() {
        return isScannedDoc;
    }

    public void setScannedDoc(final boolean isScannedDoc) {
        this.isScannedDoc = isScannedDoc;
    }

    public static DocAuthorityTokenizer getNGramTokenizer() {
        return nGramTokenizerHashed;
    }

    public static void setNGramTokenizer(final DocAuthorityTokenizer nGramTokenizer) {
        nGramTokenizerHashed = nGramTokenizer;
    }

}
