package com.cla.common.domain.dto.messages;

import com.cla.common.domain.dto.SearchPatternCounting;
import com.cla.common.domain.dto.extraction.SearchPatternCountDto;
import com.cla.common.utils.json.ZippingStringDeserializer;
import com.cla.common.utils.json.ZippingStringSerializer;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.io.Serializable;
import java.util.Set;

/**
 * Excel sheet data acquired during ingestion and relevant for persistence / analysis
 * @see ExcelIngestResult
 *
 * Created by vladi on 3/7/2017.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ExcelSheetIngestResult implements Serializable, SearchPatternCounting {

    private String fullSheetName;
    private String contentTokens;
    private String origContentTokens;
    private Set<SearchPatternCountDto> searchPatternsCounting = null;

    @JsonProperty("full-sheet-name")
    public String getFullSheetName() {
        return fullSheetName;
    }

    public void setFullSheetName(String fullSheetName) {
        this.fullSheetName = fullSheetName;
    }

    @JsonProperty("content-tokens")
    @JsonSerialize(using = ZippingStringSerializer.class)
    public String getContentTokens() {
        return contentTokens;
    }

    @JsonDeserialize(using = ZippingStringDeserializer.class)
    public void setContentTokens(String contentTokens) {
        this.contentTokens = contentTokens;
    }

    @JsonProperty("original-content-tokens")
    @JsonSerialize(using = ZippingStringSerializer.class)
    public String getOrigContentTokens() {
        return origContentTokens;
    }

    @Override
    @JsonIgnore
    public String getOriginalContent() {
        return origContentTokens;
    }

    @JsonDeserialize(using = ZippingStringDeserializer.class)
    public void setOrigContentTokens(String origContentTokens) {
        this.origContentTokens = origContentTokens;
    }

    public Set<SearchPatternCountDto> getSearchPatternsCounting() {
        return searchPatternsCounting;
    }

    public void setSearchPatternsCounting(Set<SearchPatternCountDto> searchPatternsCounting) {
        this.searchPatternsCounting = searchPatternsCounting;
    }
}
