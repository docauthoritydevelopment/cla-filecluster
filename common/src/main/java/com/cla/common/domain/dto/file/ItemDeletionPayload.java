package com.cla.common.domain.dto.file;

import com.cla.connector.domain.dto.error.ProcessingErrorType;
import com.cla.connector.domain.dto.messages.PhaseMessagePayload;
import com.google.common.base.Strings;

/**
 * Created by Itai Marko.
 */
public class ItemDeletionPayload implements PhaseMessagePayload {

    private String filename;

    @SuppressWarnings("unused")
    public ItemDeletionPayload() {
        // Default Ctor for JSON serialization
    }

    public ItemDeletionPayload(String filename) {
        if (Strings.isNullOrEmpty(filename)) {
            throw new IllegalArgumentException("Got null filename in ItemDeletionPayload constructor");
        }
        this.filename = filename;
    }

    public String getFilename() {
        return filename;
    }

    @SuppressWarnings("unused")
    public void setFilename(String filename) {
        if (Strings.isNullOrEmpty(filename)) {
            throw new IllegalArgumentException("Got null filename in ItemDeletionPayload setter");
        }
        this.filename = filename;
    }

    @Override
    public ProcessingErrorType calculateProcessErrorType() {
        return ProcessingErrorType.ERR_NONE;
    }
}
