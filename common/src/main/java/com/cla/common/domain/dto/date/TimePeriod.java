package com.cla.common.domain.dto.date;

import java.util.concurrent.TimeUnit;

/**
 * Created by uri on 05/06/2016.
 */
public enum TimePeriod {
    MINUTES(TimeUnit.MINUTES.toMillis(1)),
    HOURS(TimeUnit.HOURS.toMillis(1)),
    DAYS(TimeUnit.DAYS.toMillis(1)),
    WEEKS(TimeUnit.DAYS.toMillis(7)),
    MONTHS(TimeUnit.DAYS.toMillis(30)),
    YEARS(TimeUnit.DAYS.toMillis(365));

    private long durationMsEstimate;

    TimePeriod(long durationMsEstimate) {
        this.durationMsEstimate = durationMsEstimate;
    }

    public static long periodsInMs(TimePeriod tp) {
        return tp.durationMsEstimate;
    }
}
