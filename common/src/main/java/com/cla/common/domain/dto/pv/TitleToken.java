package com.cla.common.domain.dto.pv;

import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Pattern;

@SuppressWarnings("serial")
public class TitleToken implements Comparable<TitleToken>,Serializable {

	private final String token;
	private Double tfidf;
	
	private int tf;

	private Double nTfidf;	// Normalised 
	private Double nTf;	// Normalised
	private Double nLen;	// Normalised
	private Double nWords;	// Normalised

	// 
	private final int titleRate;		// high value -> higher likelihood to be a doc title
	private final int titleLen;			// length of extracted title text
	private final int titleOrder;		// Order found in the doc in ascending order (zero origin)
	private final int words;

	private double tokenScore;		// high value -> higher likelihood to be a doc title

	transient private double boost = 0;
	
	private static int titleTokenFactorTfidf;
	private static int titleTokenFactorTf;
	private static int titleTokenFactorLength;
	private static int titleTokenFactorNumWords;
	private static int titleBoostFactor;
	private static int titleScoreFactor;
	private static int titleOrderFactor;
	private static int optimalTitleLength;
	private static int minRatedTitleLength;
	private static int maxRatedTitleLength;

	public static void setTitleTokenFactorTfidf(final int titleTokenFactorTfidf) {
		TitleToken.titleTokenFactorTfidf = titleTokenFactorTfidf;
	}

	public static void setTitleTokenFactorTf(final int titleTokenFactorTf) {
		TitleToken.titleTokenFactorTf = titleTokenFactorTf;
	}

	public static void setTitleBoostFactorTf(final int titleBoostFactor) {
		TitleToken.titleBoostFactor = titleBoostFactor;
	}

	public static void setTitleScoreFactor(final int titleScoreFactor) {
		TitleToken.titleScoreFactor = titleScoreFactor;
	}

	public static void setTitleOrderFactor(final int titleOrderFactor) {
		TitleToken.titleOrderFactor = titleOrderFactor;
	}

	public static void setTitleTokenFactorLength(final int titleTokenFactorLength) {
		TitleToken.titleTokenFactorLength = titleTokenFactorLength;
	}
	
	public static void setTitleTokenFactorNumWords(final int titleTokenFactorNumWords) {
		TitleToken.titleTokenFactorNumWords = titleTokenFactorNumWords;
	}
	
	public static int getTitleBoostFactor() {
		return titleBoostFactor;
	}

	public static void setTitleBoostFactor(int titleBoostFactor) {
		TitleToken.titleBoostFactor = titleBoostFactor;
	}

	public static int getOptimalTitleLength() {
		return optimalTitleLength;
	}

	public static void setOptimalTitleLength(int optimalTitleLength) {
		TitleToken.optimalTitleLength = optimalTitleLength;
	}

	public static int getMinRatedTitleLength() {
		return minRatedTitleLength;
	}

	public static void setMinRatedTitleLength(int minRatedTitleLength) {
		TitleToken.minRatedTitleLength = minRatedTitleLength;
	}

	public static int getMaxRatedTitleLength() {
		return maxRatedTitleLength;
	}

	public static void setMaxRatedTitleLength(int maxRatedTitleLength) {
		TitleToken.maxRatedTitleLength = maxRatedTitleLength;
	}

	public static int getTitleTokenFactorTfidf() {
		return titleTokenFactorTfidf;
	}

	public static int getTitleTokenFactorTf() {
		return titleTokenFactorTf;
	}

	public static int getTitleTokenFactorLength() {
		return titleTokenFactorLength;
	}

	public static int getTitleTokenFactorNumWords() {
		return titleTokenFactorNumWords;
	}

	public static int getTitleScoreFactor() {
		return titleScoreFactor;
	}

	public static int getTitleOrderFactor() {
		return titleOrderFactor;
	}

	public static void setTitleTokenFactors(final int titleTokenFactorTfidf, final int titleTokenFactorTf, final int titleTokenFactorLength, final int titleTokenFactorNumWords, 
			final int titleBoostFactor, final int titleScoreFactor, final int titleOrderFactor, 
			final int optimalTitleLength, final int minRatedTitleLength, final int maxRatedTitleLength) {
		TitleToken.titleTokenFactorTfidf = titleTokenFactorTfidf;
		TitleToken.titleTokenFactorTf = titleTokenFactorTf;
		TitleToken.titleTokenFactorLength = titleTokenFactorLength;
		TitleToken.titleTokenFactorNumWords = titleTokenFactorNumWords;
		TitleToken.titleBoostFactor = titleBoostFactor;
		TitleToken.titleScoreFactor = titleScoreFactor;
		TitleToken.titleOrderFactor = titleOrderFactor;
		TitleToken.optimalTitleLength = optimalTitleLength;
		TitleToken.minRatedTitleLength = minRatedTitleLength;
		TitleToken.maxRatedTitleLength = maxRatedTitleLength;
	}

	
	private final static Comparator<TitleToken> comparator = ((t1,t2)->Double.valueOf(t2.getTokenScore()).compareTo(t1.getTokenScore()));
	
//	private final static Comparator<TitleToken> comparator = ((t1,t2)->(t1.compareTo(t2)));
	
//	private final static Comparator<TitleToken> comparator = ((t1,t2)->((int)(2d*Math.signum(
//			(t2.getnTfidf()-t1.getnTfidf())*titleTokenFactorTfidf
//			+(t2.getnTf()-t1.getnTf())*titleTokenFactorTf
////			+(t2.getnLen()-t1.getnLen())*titleTokenFactorLength
//			+((double)(t2.getToken().length()-t1.getToken().length()))/Math.max(t2.getToken().length(),t1.getToken().length())*titleTokenFactorLength
//			))));

	public TitleToken(final TitleToken o) {
		this.token = o.token;
		this.tfidf = o.tfidf;
		this.tf = o.tf;
		this.nTfidf = o.nTfidf;
		this.nTf = o.nTf;
		this.nLen = o.nLen;
		this.titleRate = o.titleRate;
		this.titleLen = o.titleLen;
		this.titleOrder = o.titleOrder;
		this.tokenScore = o.tokenScore;
		this.words = o.words;
		this.nWords = o.nWords;
		this.boost = o.boost;
	}
	
	@Override
	public int compareTo(final TitleToken o) {
		return Double.valueOf(this.getTokenScore()).compareTo(o.getTokenScore());
//		if (tfidf==null) {
//			return (o==null || o.tfidf==null)?0:-1;
//		}
//		if (o==null) {
//			return 1;
//		}
//		if (nTf > 0 || nTfidf > 0 || o.nTf > 0 || o.nTfidf > 0) {
//			return (int) Math.signum(nTf+nTfidf-o.nTf-o.nTfidf);
//		}
//		final int tfidfCmp = tfidf.compareTo(o.tfidf);
//		return tfidfCmp==0?(token.length()-o.token.length()):tfidfCmp;
		
//		final TitleToken t1 = this;
//		final TitleToken t2 = o;
//		return
//				((int)(2d*Math.signum(
//						(t2.getnTfidf()-t1.getnTfidf())*titleTokenFactorTfidf
//						+(t2.getnTf()-t1.getnTf())*titleTokenFactorTf
//						//				+(t2.getnLen()-t1.getnLen())*titleTokenFactorLength
//						+((double)(t2.getToken().length()-t1.getToken().length()))/Math.max(t2.getToken().length(),t1.getToken().length())*titleTokenFactorLength
//						+(t2.getBoost()-t1.getBoost())*titleBoostFactor
//						)));
	}

	@Override
	public boolean equals(final Object o) {
		if (o == null || !(o instanceof TitleToken)) {
			return false;
		}
		return tfidf.equals(((TitleToken)o).tfidf);
	}

	public TitleToken(final String token, final Double tfidf, final int tf) {
		this(token, tfidf, tf, 10, 0, 0);
	}
	public TitleToken(final String token, final Double tfidf, final int tf, final int titleRate, final int titleLen, final int titleOrder) {
		this.token = token;
		this.tfidf = tfidf;
		this.tf = tf;
		this.nTfidf = 0d;
		this.nTf = 0d;
		this.nLen = 0d;
		this.titleRate = titleRate;
		this.titleLen = titleLen;
		this.titleOrder = titleOrder;
		this.tokenScore = 0d;
		// Count number of words by counting the spaces - should be accurate for output of Shigle tokenizer.
		this.words = 1+org.apache.commons.lang3.StringUtils.countMatches(token, " ");
	}

	public String getToken() {
		return token;
	}

	public Double getTfidf() {
		return tfidf;
	}
	
	public void setTfidf(final Double tfidf) {
		this.tfidf = tfidf;
	}

	

	public int getTf() {
		return tf;
	}

	public void setTf(final int tf) {
		this.tf = tf;
	}

	
	public int getTitleRate() {
		return titleRate;
	}

	public int getTitleLen() {
		return titleLen;
	}

	public int getTitleOrder() {
		return titleOrder;
	}

	public Double getnTfidf() {
		return nTfidf;
	}

	public void setnTfidf(final Double nTfidf) {
		this.nTfidf = nTfidf;
	}

	public Double getnTf() {
		return nTf;
	}

	public void setnTf(final Double nTf) {
		this.nTf = nTf;
	}

	public Double getnLen() {
		return nLen;
	}

	public void setnLen(final Double nLen) {
		this.nLen = nLen;
	}
	
	public Double getnWords() {
		return nWords;
	}

	public void setnWords(final Double nWords) {
		this.nWords = nWords;
	}
	
	public static String toStringHeader() {
		return "tf,nTf,idf,nIdf, len,nlen,wrd,nwrd, rate,ord,score";
	}

	public static String toStringDescription() {
		return String.format("token,tf,nTf,tfidf,nTfidf, token.length(),nLen,words,nWords, titleRate,titleOrder,tokenScore");
	}


	@Override
	public String toString() {
		return String.format("{%s,%d,%.2f,%.2f,%.2f, %d,%.2f,%d,%.2f, %d,%d,%.2f}",
				token,tf,nTf,tfidf,nTfidf, token.length(),nLen,words,nWords, titleRate,titleOrder,tokenScore);
//		return String.format("{%s,tfidf:%.2f,tf:%d,nTfidf:%.2f,nTf:%.2f,nLen:%.2f}",token,tfidf,tf,nTfidf,nTf,nLen);
//		return "TitleToken [token=" + token + ", tfidf=" + String.format("%.2f", tfidf) + ", tf=" + tf + "]";
	}

	public static void normalizeTitleTokensList(final List<TitleToken> tokens) {
		TitleToken.normalizeTitleTokensList(tokens, 0);
	}
	public static void normalizeTitleTokensList(final List<TitleToken> tokens, final long numOfFiles) {
		if (tokens==null || tokens.isEmpty()) {
			return;
		}
		final double maxTfidf = tokens.stream().mapToDouble(t->t.getTfidf()).max().orElse(0);
		final double minTfidf = tokens.stream().mapToDouble(t->t.getTfidf()).min().orElse(0);
		final double tfidfRange = (maxTfidf==minTfidf)?1:(maxTfidf-minTfidf); 
		final int maxTf = tokens.stream().mapToInt(t->t.getTf()).max().orElse(0);
		final int minTf = tokens.stream().mapToInt(t->t.getTf()).min().orElse(0);
		final double tfRange = (maxTf==minTf)?1:(maxTf-minTf); 
		final int maxLen = tokens.stream().mapToInt(t->t.getToken().length()).max().orElse(0);
		final int minLen = tokens.stream().mapToInt(t->t.getToken().length()).min().orElse(0);
//		final double lenRange = (maxLen==minLen)?1:(maxLen-minLen); 
		final int maxWords = Math.min(30, tokens.stream().mapToInt(t->t.words).max().orElse(0));
		tokens.forEach(t->{
			t.setnTfidf((t.getTfidf()-minTfidf)/tfidfRange);
			if (numOfFiles == 0) {
				if (t.getTf() == 0) {
					t.setnTf((t.getTf()-minTf)/tfRange);
				}
			}
			else {
				t.setnTf(((double)t.getTf())/numOfFiles);
			}
//			t.setnLen((t.getToken().length()-minLen)/lenRange + t.getToken().length()/100f);	// Favor strings of large groups
			final double nLen = (t.getToken().length() > optimalTitleLength)?
					(((double)(t.getToken().length() - maxRatedTitleLength))/(optimalTitleLength - maxRatedTitleLength)):
					(((double)(t.getToken().length() - minRatedTitleLength))/(optimalTitleLength - minRatedTitleLength));
			t.setnLen(Math.max(0d, nLen));	// Favor strings around optimal length
			t.setnWords(Math.min(1d, ((double)t.words)/maxWords));
//			t.getTokenScore();
		});
	}
	
	public double getTokenScore() {
		if (tokenScore==0d) {
			tokenScore =
					nTfidf*titleTokenFactorTfidf
					+ nTf*titleTokenFactorTf
					+ nLen*titleTokenFactorLength
					+ nWords*titleTokenFactorNumWords
					+ boost*titleBoostFactor
					+ titleRate*titleScoreFactor
					+ (10-((titleOrder>9)?10:titleOrder))*titleOrderFactor	// lower order -> higher score
				;
		}
		return tokenScore;
	}

	static private final Pattern allNumericPattern = Pattern.compile("[\\d\\s]*");
	private static boolean disqualifyToken(final TitleToken token) {
		final String s = allNumericPattern.matcher(token.getToken()).replaceFirst("");
		return s.isEmpty();
	}

	public static TitleToken copyFirstQualified(final List<TitleToken> tokens) {
//		return (tokens==null || tokens.isEmpty())?null:(new TitleToken(tokens.get(0)));
		
		if (tokens==null) {
			return null;
		}
		TitleToken firstToken = tokens.stream().filter(t->!disqualifyToken(t)).findFirst().orElse(null);
		return (firstToken==null)?null:(new TitleToken(firstToken));
	}
	
	// Reverse order of weighted field differences
	public static void sort(final List<TitleToken> tokens) {
		if (tokens==null || tokens.isEmpty()) {
			return;
		}
		
		try {
			Collections.sort(tokens, comparator);
		} catch (final Exception e) {
		// TODO: add error indication	
		}
	}

	private double getBoost() {
		return boost;
	}
	public void setBoost(final double boost) {
		this.boost = boost;
		this.tokenScore = 0d;	// invalidate cached value
	}

	public static void setBoostIfNotNull(final TitleToken titleToken, final double boost) {
		if (titleToken!=null) {
			titleToken.setBoost(boost);
		}
	}

	
}
