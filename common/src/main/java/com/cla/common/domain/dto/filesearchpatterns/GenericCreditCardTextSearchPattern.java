package com.cla.common.domain.dto.filesearchpatterns;

import java.util.function.Predicate;
import java.util.regex.Pattern;

public class GenericCreditCardTextSearchPattern extends TextSearchPatternImpl {

//	public static final String GENERIC_PATERN = "\\b\\d{12,16}\\b";
//	public static final String GENERIC_PATERN = "\\b(?:3[47]\\d{2}([\\ \\-]?)\\d{6}\\1\\d|(?:(?:4\\d|5[1-5]|65)\\d{2}|6011)([\\ \\-]?)\\d{4}\\2\\d{4}\\2)\\d{4}\\b";
	public static final String GENERIC_PATERN = "(?:[^.\\-\\w])(?:3[47]\\d{2}([\\ \\-]?)\\d{6}\\1\\d|(?:(?:4\\d|5[1-5]|65)\\d{2}|6011)([\\ \\-]?)\\d{4}\\2\\d{4}\\2)\\d{4}(?![\\-\\.\\w])";

	private static final Pattern separator = Pattern.compile("[\\ \\-]");

	public GenericCreditCardTextSearchPattern() {
	}

	public GenericCreditCardTextSearchPattern(final Integer id, final String name, final String patern) {
		super(id, name, patern);
	}

	public GenericCreditCardTextSearchPattern(final Integer id, final String name) {
		this(id, name, GENERIC_PATERN);
	}

	@Override
	public Predicate<String> getPredicate() {
		return this::checkLuhn;
	}

	private boolean checkLuhn(final String number) {
		final int[] digits = separator.matcher(number).replaceAll("").chars().toArray();
		int sum = 0;
		final int length = digits.length;
		for (int i = 0; i < length; i++) {

			// get digits in reverse order
			int digit = digits[length - i - 1] - '0';
			
			// every 2nd number multiply with 2
			if (i % 2 == 1) {
				digit *= 2;
			}
			sum += digit > 9 ? digit - 9 : digit;
		}
		return sum % 10 == 0;
	}

}
