package com.cla.common.domain.dto.classification;

import com.cla.common.domain.dto.crawler.RunStatus;

import java.io.Serializable;

/**
 * Created by uri on 14/06/2016.
 */
public class EvaluationProgressDto implements Serializable {

    private RunStatus runStatus;

    public RunStatus getRunStatus() {
        return runStatus;
    }

    public void setRunStatus(RunStatus runStatus) {
        this.runStatus = runStatus;
    }
}
