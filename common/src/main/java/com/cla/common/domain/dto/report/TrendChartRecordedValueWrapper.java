package com.cla.common.domain.dto.report;

/**
 * Created by uri on 03/08/2016.
 */
public class TrendChartRecordedValueWrapper {
    TrendChartRecordedValue trendChartRecordedValue;

    String name;

    public TrendChartRecordedValueWrapper(TrendChartRecordedValue trendChartRecordedValue) {
        this.trendChartRecordedValue = trendChartRecordedValue;
    }

    public TrendChartRecordedValue getTrendChartRecordedValue() {
        return trendChartRecordedValue;
    }

    public void setTrendChartRecordedValue(TrendChartRecordedValue trendChartRecordedValue) {
        this.trendChartRecordedValue = trendChartRecordedValue;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
