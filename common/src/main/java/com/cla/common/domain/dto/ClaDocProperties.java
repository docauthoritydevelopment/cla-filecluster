package com.cla.common.domain.dto;

import java.util.Date;

public class ClaDocProperties {

	private String creator;
	private String company;
	private String subject;
	private String title;
	private String keywords;
	private String appNameVer;
	private String template;
	private Date created;
	private Date modified;
	private int words;
	private int pages;
	private int paragraphs;
	private int chars;
	private int lines;
	private int secAppCode;
	private boolean secPasswordProtected;
	private boolean secReadonlyRecommended;
	private boolean secReadonlyEnforced;
	private boolean secLockedForAnnotations;
	
	public ClaDocProperties() {
	}
	
	
	public String getCreator() {
		return creator;
	}


	public void setCreator(final String creator) {
		this.creator = creator;
	}


	public String getCompany() {
		return company;
	}


	public void setCompany(final String company) {
		this.company = company;
	}


	public String getSubject() {
		return subject;
	}


	public void setSubject(final String subject) {
		this.subject = subject;
	}


	public String getTitle() {
		return title;
	}


	public void setTitle(final String title) {
		this.title = title;
	}


	public String getKeywords() {
		return keywords;
	}


	public void setKeywords(final String keywords) {
		this.keywords = keywords;
	}


	public String getAppNameVer() {
		return appNameVer;
	}


	public void setAppNameVer(final String appNameVer) {
		this.appNameVer = appNameVer;
	}


	public String getTemplate() {
		return template;
	}


	public void setTemplate(final String template) {
		this.template = template;
	}


	public Date getCreated() {
		return created;
	}


	public void setCreated(final Date created) {
		this.created = created;
	}


	public Date getModified() {
		return modified;
	}


	public void setModified(final Date modified) {
		this.modified = modified;
	}


	public int getWords() {
		return words;
	}


	public void setWords(final int words) {
		this.words = words;
	}


	public int getPages() {
		return pages;
	}


	public void setPages(final int pages) {
		this.pages = pages;
	}

	public int getParagraphs() {
		return paragraphs;
	}


	public void setParagraphs(final int paragraphs) {
		this.paragraphs = paragraphs;
	}


	public int getChars() {
		return chars;
	}


	public void setChars(final int chars) {
		this.chars = chars;
	}


	public int getLines() {
		return lines;
	}


	public void setLines(final int lines) {
		this.lines = lines;
	}


	public int getSecAppCode() {
		return secAppCode;
	}


	public void setSecAppCode(final int secAppCode) {
		this.secAppCode = secAppCode;
	}


	public boolean isSecPasswordProtected() {
		return secPasswordProtected;
	}


	public void setSecPasswordProtected(final boolean secPasswordProtected) {
		this.secPasswordProtected = secPasswordProtected;
	}


	public boolean isSecReadonlyRecommended() {
		return secReadonlyRecommended;
	}


	public void setSecReadonlyRecommended(final boolean secReadonlyRecommended) {
		this.secReadonlyRecommended = secReadonlyRecommended;
	}


	public boolean isSecReadonlyEnforced() {
		return secReadonlyEnforced;
	}


	public void setSecReadonlyEnforced(final boolean secReadonlyEnforced) {
		this.secReadonlyEnforced = secReadonlyEnforced;
	}


	public boolean isSecLockedForAnnotations() {
		return secLockedForAnnotations;
	}


	public void setSecLockedForAnnotations(final boolean secLockedForAnnotations) {
		this.secLockedForAnnotations = secLockedForAnnotations;
	}


	@Override
	public String toString() {
		return "ClaDocProperties{creator="+creator
				+" company="+company
				+" subject="+subject
				+" title="+title
				+" keywords="+keywords
				+" appNameVer="+appNameVer
				+" template="+template
				+" created="+(created!=null?created.toString():"null")
				+" modified="+(modified!=null?modified.toString():"null")
				+" secCode="+secAppCode
				+" pages="+pages
				+" paragraphs="+paragraphs
				+" lines="+lines
				+" words="+words
				+" chars="+chars
				+"}";
	}


	public int getSheets() {
		return 0;
	}

	public int getCells() {
		return 0;
	}

}
