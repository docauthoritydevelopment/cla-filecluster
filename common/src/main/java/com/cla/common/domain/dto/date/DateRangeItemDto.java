package com.cla.common.domain.dto.date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by uri on 05/06/2016.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class DateRangeItemDto implements Serializable {

    private static String pattern = "MM/dd/yyyy";
    private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

    private Long id;
    private String name;
    private DateRangePointDto start; // From
    private DateRangePointDto end; // To

    private String dateFilter;

    private Integer totalCount;

    public DateRangeItemDto() {
    }

    public DateRangeItemDto(String name, DateRangePointDto start, DateRangePointDto end) {
        this.name = name;
        this.start = start;
        this.end = end;
    }

    public DateRangeItemDto(DateRangeItemDto other) {
        this.id = other.id;
        this.name = String.valueOf(other.name);
        this.start = other.start == null ? null : new DateRangePointDto(other.start);
        this.end = other.end == null ? null : new DateRangePointDto(other.end);
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setStart(DateRangePointDto start) {
        this.start = start;
    }

    public DateRangePointDto getStart() {
        return start;
    }

    public void setEnd(DateRangePointDto end) {
        this.end = end;
    }

    public DateRangePointDto getEnd() {
        return end;
    }

    @Override
    public String toString() {
        return "DateRangeItemDto{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", start=" + start +
                ", end=" + end +
                '}';
    }

    public String getDateFilter() {
        return dateFilter;
    }

    public void setDateFilter(String dateFilter) {
        this.dateFilter = dateFilter;
    }

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    public String getStartAbsoluteDate() {
        if (start != null && start.getAbsoluteTime() != null) {
            return simpleDateFormat.format(new Date(start.getAbsoluteTime()));
        }
        return null;
    }
}
