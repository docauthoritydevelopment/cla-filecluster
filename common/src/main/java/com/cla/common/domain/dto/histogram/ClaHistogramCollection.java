package com.cla.common.domain.dto.histogram;

import com.cla.common.domain.dto.pv.PVType;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.*;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.joining;

/**
 * @param <T>
 * @author itay
 */
public class ClaHistogramCollection<T> implements Serializable {


    static public final PVType[] wordLongHistogramNames = {
            PVType.PV_BodyNgrams
            , PVType.PV_SubBodyNgrams
            , PVType.PV_DocstartNgrams
            , PVType.PV_HeaderNgrams

            , PVType.PV_Headings
            , PVType.PV_StyleSignatures
            , PVType.PV_StyleSequence
            , PVType.PV_AllStyleSignatures
            , PVType.PV_AllStyleSequence
            , PVType.PV_AbsoluteStyleSignatures
            , PVType.PV_AbsoluteStyleSequence
            , PVType.PV_HeaderStyles
            , PVType.PV_ParagraphLabels
            , PVType.PV_LetterHeadLabels
            , PVType.PV_Pictures
    };

    static public final PVType[] excelLongHistogramNames = {
            PVType.PV_RowHeadings
            , PVType.PV_ExcelStyles
            , PVType.PV_CellValues
            , PVType.PV_Formulas
            , PVType.PV_ConcreteCellValues
            //,PVType.PV_Pictures				// TODO:
            , PVType.PV_TextNgrams
            , PVType.PV_TitlesNgrams
            , PVType.PV_SheetLayoutSignatures
    };
    private static final long serialVersionUID = 1L;
    private static final int DEFAULT_LIMIT = 63900;

    private Map<T, Integer> histogram = new LinkedHashMap<>();
    private Map<T, Float> weights = new LinkedHashMap<>();
    private Set<T> markedItems = null;
    private int globalCount = 0;
    private int skippedCount = 0;

    private int globalCountLimit = 0;
    private int membersCountLimit = 0;
    private int itemCounterLimit = 0;
    private boolean enforceDuringInsert = false;

//    private int markedSingleItemsCount = 0;
//    private boolean markedSingleItemsCountDirty = true;

    private HistogramType type;

    /**
     * Prepare for custom serialization ...
     * <p>
     * This method is private but it is called using reflection by java
     * serialization mechanism. It overwrites the default object serialization.
     * The access modifier for this method MUST be set to <b>private</b> otherwise {@link java.io.StreamCorruptedException}
     * will be thrown.
     */
    @SuppressWarnings("unused")
    private void MyWriteObject(final ObjectOutputStream oos) throws IOException {
        calcWeights();
        //System.out.println(ExcelSheet.class.getName() + ".writeObject");

        //oos.defaultWriteObject();

        // scalars
        oos.writeLong(serialVersionUID);
        oos.writeObject(type);
        oos.writeInt(globalCount);
        oos.writeInt(skippedCount);
        oos.writeInt(globalCountLimit);
        oos.writeInt(membersCountLimit);
        oos.writeInt(itemCounterLimit);
        // Collection
        if (histogram == null)
            oos.writeInt(-1);
        else {
            oos.writeInt(histogram.size());
            for (final Map.Entry<T, Integer> entry : histogram.entrySet()) {
                oos.writeObject(entry.getKey());
                oos.writeInt(entry.getValue());
            }
        }

        if (weights == null)
            oos.writeFloat(-1f);
        else {
            oos.writeInt(weights.size());
            for (final Map.Entry<T, Float> entry : weights.entrySet()) {
                oos.writeObject(entry.getKey());
                oos.writeFloat(entry.getValue());
            }
        }
    }

    /**
     * This method is private but it is called using reflection by java
     * serialization mechanism. It overwrites the default object serialization.
     * The access modifier for this method MUST be set to <b>private</b> otherwise {@link java.io.StreamCorruptedException}
     * will be thrown.
     */
    @SuppressWarnings("unused")
    private void MyReadObject(final ObjectInputStream ois) throws IOException, ClassNotFoundException {
        //System.out.println(ExcelSheet.class.getName() + ".readObject");

        //ois.defaultReadObject();

        int i;
        int count;

        // scalars
        final long version = ois.readLong();
        if (version != serialVersionUID)
            throw new IOException("Bad type hash " + version + "!=" + serialVersionUID);
        type = (HistogramType) ois.readObject();
        globalCount = ois.readInt();
        skippedCount = ois.readInt();
        globalCountLimit = ois.readInt();
        membersCountLimit = ois.readInt();
        itemCounterLimit = ois.readInt();
        // Collection
        count = ois.readInt();
        if (count < 0)
            histogram = null;
        else {
            histogram = new LinkedHashMap<>();
            for (i = 0; i < count; ++i) {
                @SuppressWarnings("unchecked")
                final
                T key = (T) ois.readObject();
                final int value = ois.readInt();
                histogram.put(key, value);
            }
        }

        if (count < 0)
            weights = null;
        else {
            weights = new LinkedHashMap<>();
            for (i = 0; i < count; ++i) {
                @SuppressWarnings("unchecked")
                final
                T key = (T) ois.readObject();
                final float value = ois.readFloat();
                weights.put(key, value);
            }
        }
    }

    public ClaHistogramCollection() {
        this(HistogramType.NORMAL);
    }

    public ClaHistogramCollection(final HistogramType type) {
        this.type = type;
        init();
    }

    public ClaHistogramCollection(final Map<T, Integer> histogram) {
        this(histogram, HistogramType.NORMAL);
    }

    // no limitation enforcement
    public ClaHistogramCollection(final Map<T, Integer> histogram, final HistogramType type) {
        this.type = type;
        init();
        if (histogram != null) {
            this.histogram.putAll(histogram);
            globalCount = histogram.values().stream().mapToInt(v -> v).sum();
        }
        else {
            globalCount = 0;
        }
    }

    private void init() {
        markedItems = (type == HistogramType.MARKER) ?
                (new HashSet<>()) :
                null;
    }

    public boolean markItem(final T item) {
        if (type != HistogramType.MARKER)
            throw new RuntimeException("Wrong histogram type " + type.name());

        return markedItems.add(item);
    }

    public boolean isItemMarked(final T item) {
        return markedItems != null && markedItems.contains(item);
    }

    /**
     * @param item
     * @return true if item is marked and has a count of 1
     */
    public boolean isItemMarkedSingleCount(final T item) {
        return markedItems != null && (markedItems.contains(item) && getCount(item) == 1);
    }


    private void calcWeights() {
        weights.clear();
        if (type == HistogramType.MARKER) {
            // Balance weights to be comparable to the 'calcWeight()' value.
            final float denom = globalCount - markedItems.size();
            histogram.forEach((k, v) -> weights.put(k, ((float) v) / denom));
        } else {
            histogram.forEach((k, v) -> weights.put(k, ((float) v) / ((float) globalCount)));
        }
    }


    /**
     * @return external int value based on the histogram type. Equal to getCount() in most cases
     */
    public int getValue(final T item) {
        return
                getCount(item);
    }

//    public Set<T> getMarkedItems() {
//        return markedItems;
//    }

    public int getMarkedItemsSize() {
        return (markedItems == null) ? 0 : markedItems.size();
    }

    public int addItem(final T item) {
        Integer count = histogram.get(item);
        if (count == null)
            count = 0;
        // Skip addition if limit is set and is reached
        if (enforceDuringInsert && ((globalCountLimit > 0 && globalCount >= globalCountLimit) ||
                (membersCountLimit > 0 && count == 0 && histogram.size() >= membersCountLimit) ||
                (itemCounterLimit > 0 && count >= itemCounterLimit))) {
            skippedCount++;
            return count;
        }
        count++;
        histogram.put(item, count);
        globalCount++;
        return count;
    }

    /**
     * Enforce count limitations.
     * This should be done after items insertion to make sure that the proper items are trimmed.
     * globalCountLimit : supported only during insert
     */
    public void applyLimitations() {
        if (enforceDuringInsert || histogram.size() == 0)
            return;

        // Enforce itemCounterLimit
        if (itemCounterLimit > 0) {
            histogram.entrySet()
                    .forEach((e) -> {
                        final int v = e.getValue();
                        if (v > itemCounterLimit) {
                            skippedCount += v - itemCounterLimit;
                            e.setValue(itemCounterLimit);
                            globalCount -= v - itemCounterLimit;
                        }
                    });
        }

        // Enforce membersCountLimit - drop least popular items
        if (membersCountLimit > 0 && histogram.size() > membersCountLimit) {
            final Map<T, Integer> newHist = new LinkedHashMap<>();
            histogram.entrySet().stream()
                    .sorted(Comparator.comparingInt(Entry::getValue))
                    .skip(histogram.size() - membersCountLimit).forEach(e -> newHist.put(e.getKey(), e.getValue()));
            final int newCount = newHist.values().stream().mapToInt(i -> i).sum();
            skippedCount += globalCount - newCount;
            globalCount = newCount;
            histogram = newHist;
        }

    }

    public void addItems(final Collection<T> items) {
        if (items != null)
            items.forEach(this::addItem);
    }

    public int getCount(final T item) {
        Integer count = histogram.get(item);
        if (count == null) {
            count = 0;
        }
        return count;
    }

//    public int getGlobalCountLimit() {
//        return globalCountLimit;
//    }
//
//    public void setGlobalCountLimit(final int globalCountLimit) {
//        this.globalCountLimit = globalCountLimit;
//    }
//
//    public int getMembersCountLimit() {
//        return membersCountLimit;
//    }
//
//    public void setMembersCountLimit(final int membersCountLimit) {
//        this.membersCountLimit = membersCountLimit;
//    }
//
//    public int getItemCounterLimit() {
//        return itemCounterLimit;
//    }

    public void setItemCounterLimit(final int itemCounterLimit) {
        this.itemCounterLimit = itemCounterLimit;
    }

//    public boolean isEnforceDuringInsert() {
//        return enforceDuringInsert;
//    }

    public void setEnforceDuringInsert(final boolean enforceDuringInsert) {
        this.enforceDuringInsert = enforceDuringInsert;
    }

    public Map<T, Integer> getCollection() {
        return histogram;
    }

    public HistogramType getType() {
        return type;
    }

    public void setType(final HistogramType type) {
        if (histogram.size() > 0 && !this.type.equals(type))
            throw new RuntimeException("Cannot change type after collection is set.");

        this.type = type;
        init();
    }

    public int getGlobalCount() {
        return globalCount;
    }

    public int getItemsCount() {
        return histogram.size();
    }

    public int getSkippedCount() {
        return skippedCount;
    }

    public int getTotalCount() {
        return globalCount + skippedCount;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;

        for (final Map.Entry<T, Integer> entry : histogram.entrySet()) {
            result = prime * result + entry.getKey().hashCode();
            result = prime * result + entry.getValue();
        }
        result = prime * result + globalCount;
        result = prime * result + skippedCount;
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        @SuppressWarnings("unchecked")
        final
        ClaHistogramCollection<T> other = (ClaHistogramCollection<T>) obj;
        if (globalCount != other.globalCount) {
            return false;
        }
        if (skippedCount != other.skippedCount) {
            return false;
        }
        //noinspection SimplifiableIfStatement
        if (histogram.size() != other.histogram.size()) {
            return false;
        }
        return histogram.equals(other.histogram);
    }

    public float getSimilarityRatio(final ClaHistogramCollection<T> other) {
        if (other instanceof ClaLongHistogramCollectionArray) {
            return
                ((ClaLongHistogramCollectionArray)ClaLongHistogramCollectionArray.createIfNeeded((ClaHistogramCollection<Long>) this))
                        .getSimilarityRatio2((ClaHistogramCollection<Long>) other);
        }

        final int denom = globalCount + other.globalCount;
        if (denom == 0 || histogram.size() == 0 || other.histogram.size() == 0)
            return 0.0f;
        int equalsCount = 0;
        for (final Map.Entry<T, Integer> entry : histogram.entrySet()) {
            final int countA = entry.getValue();
            final int countB = other.getCount(entry.getKey());
            equalsCount += Math.min(countA, countB);
        }
        return (2.0f * equalsCount) / (denom);
    }

    public float getNormalizedSimilarityRatio(final ClaHistogramCollection<T> other) {
        if (other instanceof ClaLongHistogramCollectionArray) {
            return
                ((ClaLongHistogramCollectionArray)ClaLongHistogramCollectionArray.createIfNeeded((ClaHistogramCollection<Long>) this))
                        .getNormalizedSimilarityRatio2((ClaHistogramCollection<Long>) other);
        }

        if (globalCount == 0 || other.globalCount == 0)
            return 0.0f;
        if (histogram.size() > other.histogram.size())            // For better performance loop on the other histogram entries.
            return other.getNormalizedSimilarityRatio(this);
        float equalsCount = 0.0f;
        for (final Map.Entry<T, Integer> entry : histogram.entrySet()) {
            final float countA = ((float) entry.getValue()) / (float) globalCount;
            final float countB = ((float) other.getCount(entry.getKey())) / (float) other.globalCount;
            equalsCount += Float.min(countA, countB);
        }
        return equalsCount;
    }

    /**
     * Compare collections taking into account items order
     * Assuming histogram preserve insertion order.
     *
     */
    public Set<T> getOrderedCommonKeys(final ClaHistogramCollection<T> other) {
        if (other instanceof ClaLongHistogramCollectionArray) {
            return (Set<T>)
                    ((ClaLongHistogramCollectionArray)(ClaLongHistogramCollectionArray.createIfNeeded((ClaHistogramCollection<Long>) this)))
                            .getOrderedCommonKeys2((ClaLongHistogramCollectionArray) other);
        }

        if (globalCount == 0 || other.globalCount == 0)
            return null;
        final Set<T> res = new LinkedHashSet<>();

        Map<T, Integer> histA = this.histogram;
        Map<T, Integer> histB = other.histogram;
        if (histA.size() > histB.size()) {
            // switch
            histA = other.histogram;
            histB = this.histogram;
        }
        final Set<T> keySetA = histA.keySet();
        final Set<T> keySetB = histB.keySet();
        final Iterator<T> iterA = keySetA.iterator();
        final Iterator<T> iterB = keySetB.iterator();
        while (iterA.hasNext() && iterB.hasNext()) {
            T keyA = iterA.next();
            T keyB = iterB.next();
            // Skip items that are not present in the other collection, then compare items by order.
            while ((!keySetB.contains(keyA)) && iterA.hasNext())
                keyA = iterA.next();
            while (((!keySetA.contains(keyB)) || (!keyA.equals(keyB))) && iterB.hasNext())
                keyB = iterB.next();
            if (keyA.equals(keyB))
                res.add(keyA);
        }    // while ()
        return res;
    }

//    public float getOrderedNormSimilarityRatio(final ClaHistogramCollection<T> other) {
//        final Set<T> commonKeys = getOrderedCommonKeys(other);
//        if (commonKeys == null)
//            return 0.0f;
//
//        final Map<T, Integer> histA = this.histogram;
//        final Map<T, Integer> histB = other.histogram;
//
//        final float equalsCount = (float) commonKeys.stream()
//                .mapToDouble(key -> Float.min(((float) histA.get(key)) / (float) this.globalCount,
//                        ((float) histB.get(key)) / (float) other.globalCount))
//                .sum();
//        return equalsCount;
//    }

    public int getOrderedSimilarityCount(final ClaHistogramCollection<T> other) {
        if (other instanceof ClaLongHistogramCollectionArray) {
            return ((ClaLongHistogramCollectionArray)(ClaLongHistogramCollectionArray.createIfNeeded((ClaHistogramCollection<Long>) this)))
                            .getOrderedSimilarityCount2((ClaLongHistogramCollectionArray) other);
        }

        final Set<T> commonKeys = getOrderedCommonKeys(other);
        if (commonKeys == null)
            return 0;

        return commonKeys.size();
    }

//    /**
//     * Return a copy of the histogram map sorted by item-count value (descending order).
//     *
//     * @return
//     */
//    public Map<T, Integer> sortByValue() {
//        return sortByValue(histogram);
//    }

    /**
     * Return a new linked map where elements are sorted by value reveres order.
     *
     * @param map
     * @return
     */
    public static <T> Map<T, Integer> sortByValue(final Map<T, Integer> map) {
        final List<Map.Entry<T, Integer>> list = new LinkedList<>(map.entrySet());
        list.sort((o1, o2) -> {
            return (o2.getValue()).compareTo(o1.getValue());    // get reverse order
        });

        final Map<T, Integer> result = new LinkedHashMap<>();
        for (final Map.Entry<T, Integer> entry : list) {
            result.put(entry.getKey(), entry.getValue());
        }
        return result;
    }

    public String getPropertiesString() {
        return "cnt=" + globalCount +
                " itms=" + histogram.size() +
                ((markedItems == null) ? "" : (" #mrkd=" + markedItems.size())) +
                " max=" + ((histogram.size() == 0) ? 0 : histogram.values().stream().max(Integer::compare).get());
    }

    @Override
    public String toString() {
        return this.toString(',');
    }

    public String toString(final char seperator) {
        return histogram.entrySet().stream().map(e -> e.getKey().toString() + ":" + e.getValue().toString()).collect(Collectors.joining(String.valueOf(seperator)));
    }

//    public String toStringOrderedValueDesc(final String seperator) {
//        return histogram.entrySet().stream()
//                .sorted((a, b) -> Integer.compare(b.getValue(), a.getValue()))    // Descending by count
//                .map(e -> e.getKey().toString() + ":" + e.getValue().toString()).collect(Collectors.joining(seperator));
//    }

    public String toStringOrderedKeyAsc(final String separator) {
        return histogram.entrySet().stream()
                .sorted((a, b) -> compareKeys(a.getKey(), b.getKey()))    // Ascending by key
                .map(e -> e.getKey().toString() + ":" + e.getValue().toString())
                .collect(Collectors.joining(separator));
    }

    private int compareKeys(T key1, T key2) {
        if (key1.getClass().equals(key2.getClass())) {
            if (key1 instanceof Long)
                return Long.compare((Long) key1, (Long) key2);
            if (key1 instanceof Integer)
                return Integer.compare((Integer) key1, (Integer) key2);
        }
        return key1.toString().compareTo(key2.toString());
    }

    /**
     * For debug purposes ...
     *
     * @return a collection with the diff items between the two compared collections.
     */
    public Map<T, Integer> diff(final ClaHistogramCollection<T> other) {
        if (histogram.size() > other.histogram.size())            // For better performance loop on the other histogram entries.
            return other.diff(this);
        final Map<T, Integer> result = new LinkedHashMap<>(other.histogram);
        if (histogram.size() == 0)
            return result;

        for (final Map.Entry<T, Integer> entry : histogram.entrySet()) {
            final T key = entry.getKey();
            final Integer valA = entry.getValue();
            final Integer valB = other.getCount(key);
//			if (valB == null) {
//				result.put(key, valA);
//			}
//			else {
            {
                if (valA != null && valA.equals(valB)) {
                    result.remove(key);
                } else {
                    result.put(key, valB - valA);
                }
            }
        }
        return result;
    }

    public List<T> getOutliersByAverage(final float factorThreshold, final int minCount) {

        if (histogram.size() == 0)
            return null;

        final Comparator<? super Entry<T, Integer>> sortByCount = (a, b) -> {
            return b.getValue() - a.getValue();
        };

        final int threshold = (int) (0.5 + factorThreshold * globalCount / histogram.size());
        final List<T> result = new ArrayList<>();

        histogram.entrySet().stream().filter(e -> e.getValue() > minCount)
                .sorted(sortByCount).anyMatch((e) -> {
            if (e.getValue() > threshold) {
                result.add(e.getKey());
                return false;    // anyMatch continues
            }
            return true;    // When below threshold, stop the iteration
        });

        return result;
    }

    public T getOutlierByDiff(final int factorThreshold, final int minCount) {

        if (histogram.size() < 3)
            return null;

        final Comparator<? super Entry<T, Integer>> sortByCountDesc = (a, b) -> {
            return b.getValue() - a.getValue();
        };

        final List<Entry<T, Integer>> lastEnt = new ArrayList<>();
        ;
        final List<T> result = new ArrayList<>();

        histogram.entrySet().stream().filter(e -> e.getValue() > minCount)
                .sorted(sortByCountDesc).anyMatch((e) -> {
            if (lastEnt.size() == 0) {
                lastEnt.add(e);
                return false;    // anyMatch continues
            } else if (lastEnt.get(0).getValue() >= e.getValue() * factorThreshold) {
                result.add(lastEnt.get(0).getKey());
            }
            return true;    // Terminate anyMatch
        });

        return (result.size() > 0) ? result.get(0) : null;
    }

    public void removeItem(final T outLier) {
        final Integer count = histogram.remove(outLier);
        if (count != null) {
            globalCount -= count;
        }
    }

    public String getJoinedKeys() {
        return getJoinedKeys("");
    }

    // Used to generate encoded doc PV field value string
    private String getJoinedKeys(final String prefix) {
        return getCollection().entrySet().stream()
                .flatMap(e -> Stream.generate(() -> String.format("%X", e.getKey())).limit(e.getValue()))
                .collect(joining(" ", prefix, ""));
    }

//    public String getJoinedKeysWithBoost() {
//        return getJoinedKeysWithBoost("");
//    }
    // Used to generate encoded query PV field value string - tokens are set with term-boost value of square-root of their frequency

    public String getJoinedKeysWithBoost(final String prefix) {
        return getJoinedKeysWithBoost(prefix, DEFAULT_LIMIT);
    }

    public String getJoinedKeysWithBoost(final String prefix, int limit) {
//        StringJoiner stringJoiner = new StringJoiner(" ", prefix + "(", ")");
//		for (Entry<T, Integer> entry : getCollection().entrySet()) {
//			T key = entry.getKey();
//			stringJoiner.add(entry.getValue() == 1 ? String.format("%X", key) :
//					String.format("%X^%.3f", key, (float) Math.sqrt(entry.getValue())));
//		}
//		return stringJoiner.toString();

        return getCollection().entrySet().stream()
                .map(e ->
                        ((e.getValue() == 1) ?
                                String.format("%X", e.getKey()) :
                                String.format("%X^%.3f", e.getKey(), (float) Math.sqrt(e.getValue()))))
                .limit(limit)
                .collect(joining(" ", prefix + "(", ")"));
    }


//	// Used to generate encoded query PV field value string - tokens are set with term-boost value of square-root of their frequency
//	// This method is used to create sub-queries for long histograms.
//	public List<String> getJoinedKeysWithBoostSubList(final int listSize){
//		return getJoinedKeysWithBoostSubList("", listSize);
//	}
//	
//	public List<String> getJoinedKeysWithBoostSubList(final String prefix, final int listSize){
//		final List<List<String>> lists = new ArrayList<>();
//		List<String> currList = new ArrayList<>(); 
//		for (final Entry<T, Integer> e : getCollection().entrySet()) {
//			final String token = (e.getValue()==1)?
//					String.format("%X",e.getKey()):
//					String.format("%X^%.3f",e.getKey(),(float)Math.sqrt(e.getValue()));
//			currList.add(token);
//			if (currList.size() == listSize) {
//				lists.add(currList);
//				currList = new ArrayList<>();
//			}
//		}
//		if (!currList.isEmpty()) {
//			lists.add(currList);
//		}
//		return lists.stream().map(l->l.stream().collect(joining(" ",prefix+"(",")"))).collect(Collectors.toList());
//	}

    public List<ClaHistogramCollection<T>> split(final int listSize) {
        final List<ClaHistogramCollection<T>> lists = new ArrayList<>();
        Map<T, Integer> histogram = new HashMap<>();
        for (final Entry<T, Integer> e : getCollection().entrySet()) {
            histogram.put(e.getKey(), e.getValue());
            if (histogram.size() == listSize) {
                lists.add(new ClaHistogramCollection<>(histogram, getType()));
                histogram = new HashMap<>();
            }
        }
        if (!histogram.isEmpty()) {
            lists.add(new ClaHistogramCollection<>(histogram, getType()));
        }
        return lists;
    }

//    public long sumSquareCounts() {
//        return getCollection().values().stream().mapToLong(v -> (Long.valueOf(v) * v)).sum();
//    }

}
