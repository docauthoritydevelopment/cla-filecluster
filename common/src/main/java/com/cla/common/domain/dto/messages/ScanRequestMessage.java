package com.cla.common.domain.dto.messages;

import com.cla.connector.domain.dto.messages.ScanErrorDto;
import com.cla.connector.domain.dto.messages.ScanTaskParameters;

/**
 *
 * Created by vladi on 2/21/2017.
 */
public class ScanRequestMessage extends ProcessingPhaseMessage<ScanTaskParameters, ScanErrorDto> implements RequestMessage{

    // true indicates that the MP should ignore its scan-cache
    private boolean forceScanFromScratch = false;

    public ScanRequestMessage() {
    }

    public ScanRequestMessage(ScanTaskParameters payload) {
        super(payload);
        setRunContext(payload.getRunId());
    }

    public boolean isForceScanFromScratch() {
        return forceScanFromScratch;
    }

    public void setForceScanFromScratch(boolean forceScanFromScratch) {
        this.forceScanFromScratch = forceScanFromScratch;
    }

    @Override
    public String toString() {
        return "ScanRequestMessage{" +
                "forceScanFromScratch=" + forceScanFromScratch +
                "} " + super.toString();
    }
}
