package com.cla.common.domain.dto.filetree;

/**
 * Created by uri on 11/08/2015.
 */
public enum FileRepositoryType {
    FILESHARE_NATIVE, SHAREPOINT_LOCAL, SHAREPOINT_365, BOX, DROPBOX, ONEDRIVE, GOOGLE_DRIVE
}
