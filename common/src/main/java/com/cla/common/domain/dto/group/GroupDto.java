package com.cla.common.domain.dto.group;

import com.cla.common.domain.dto.bizlist.SimpleBizListItemDto;
import com.cla.common.domain.dto.doctype.DocTypeAssociationDto;
import com.cla.common.domain.dto.doctype.DocTypeDto;
import com.cla.common.domain.dto.filetag.FileTagAssociationDto;
import com.cla.common.domain.dto.filetag.FileTagDto;
import com.cla.common.domain.dto.security.AssociableDto;
import com.cla.common.domain.dto.security.FunctionalItemDto;
import com.cla.common.domain.dto.security.FunctionalRoleDto;
import com.cla.common.domain.dto.security.SystemRoleDto;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

/**
 * Created by uri on 05/07/2015.
 * <p>
 * id: { type: 'string', editable: false, nullable: true },
 * sgid: { type: 'string', editable: false, nullable: true },
 * numOfFiles: { type: 'number', editable: false},
 * firstMemberName: { type: 'string', editable: false},
 * firstMemberBasename: { type: 'string', editable: false},
 * shortName: { type: 'string', editable: false}
 */
public class GroupDto implements FunctionalItemDto, AssociableDto, Serializable {

    private String id;
    private String groupName;
    private Long numOfFiles;
    private String firstMemberPath;
    private String firstMemberName;
    private Long firstMemberId;
    private Boolean deleted;
    private String parentGroupName;
    private GroupType groupType;
    private String parentGroupId;
    private String rawAnalysisGroupId;
    private boolean hasSubgroups = false;
    private Boolean isGroupProcessing = null;
    private GroupDto parentGroup;

    private Set<FileTagDto> fileTagDtos = Sets.newHashSet();
    private List<FileTagAssociationDto> fileTagAssociations = Lists.newLinkedList();

    private Set<DocTypeDto> docTypeDtos = Sets.newHashSet();
    private List<SimpleBizListItemDto> associatedBizListItems = Lists.newLinkedList();
    private Set<FunctionalRoleDto> associatedFunctionalRoles = Sets.newHashSet();
    private Set<SystemRoleDto> permittedOperations = Sets.newHashSet();
    private List<DocTypeAssociationDto> docTypeAssociations;

    public GroupDto() {
    }

    public GroupDto(final String id, final String groupName, final Long numOfFiles, Long firstMemberId, final String alternativeName, GroupType groupType) {
        this.id = id;
        this.groupName = groupName;
        this.numOfFiles = numOfFiles;
        this.firstMemberId = firstMemberId;
        this.groupType = groupType;
        if (groupName == null) {
            this.groupName = alternativeName;
        }
    }

    public GroupDto(final String id, final String groupName, final Long numOfFiles, Long firstMemberId, final String firstMemberPath, final String firstMemberName) {
        this.id = id;
        this.groupName = groupName;
        this.numOfFiles = numOfFiles;
        this.firstMemberId = firstMemberId;
        this.firstMemberName = firstMemberName;
        this.firstMemberPath = firstMemberPath;
    }

    public GroupDto(final String id, final String groupName, final Long numOfFiles, Long firstMemberId, final String fullPath, final String basename, final String alternativeName) {
        this(id, groupName, numOfFiles, firstMemberId, fullPath, basename);
        if (groupName == null) {
            this.groupName = alternativeName;
        }
    }

    public GroupDto(final String id, final String groupName, final Long numOfFiles, Long firstMemberId, final String fullPath, final String basename, final String alternativeName, GroupType groupType) {
        this(id, groupName, numOfFiles, firstMemberId, fullPath, basename, alternativeName);
        this.groupType = groupType;
    }

    @Override
    public Set<SystemRoleDto> getPermittedOperations() {
        return permittedOperations;
    }

    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(final String groupName) {
        this.groupName = groupName;
    }

    public long getNumOfFiles() {
        return numOfFiles;
    }

    public void setNumOfFiles(final long numOfFiles) {
        this.numOfFiles = numOfFiles;
    }

    public String getFirstMemberPath() {
        return firstMemberPath;
    }

    public void setFirstMemberPath(final String firstMemberPath) {
        this.firstMemberPath = firstMemberPath;
    }

    public String getFirstMemberName() {
        return firstMemberName;
    }

    public void setFirstMemberName(final String firstMemberName) {
        this.firstMemberName = firstMemberName;
    }

    public Long getFirstMemberId() {
        return firstMemberId;
    }

    @Override
    public String toString() {
        return "{id=" + id + ", groupName=" + groupName
                + ", groupType=" + groupType
                + ", numOfFiles=" + numOfFiles + ", firstMemberPath="
                + firstMemberPath + ", firstMemberName=" + firstMemberName +
                ", associatedFunctionalRoles=" + associatedFunctionalRoles +
                 "}";
    }

    public Set<FileTagDto> getFileTagDtos() {
        return fileTagDtos;
    }

    public void setFileTagDtos(Set<FileTagDto> fileTagDtos) {
        this.fileTagDtos = fileTagDtos;
    }

    public List<FileTagAssociationDto> getFileTagAssociations() {
        return fileTagAssociations;
    }

    public void setFileTagAssociations(List<FileTagAssociationDto> fileTagAssociations) {
        this.fileTagAssociations = fileTagAssociations;
    }

    public Set<DocTypeDto> getDocTypeDtos() {
        return docTypeDtos;
    }

    public void setDocTypeDtos(Set<DocTypeDto> docTypeDtos) {
        this.docTypeDtos = docTypeDtos;
    }

    public void setAssociatedBizListItems(List<SimpleBizListItemDto> associatedBizListItems) {
        this.associatedBizListItems = associatedBizListItems;
    }

    public List<SimpleBizListItemDto> getAssociatedBizListItems() {
        return associatedBizListItems;
    }

    @Override
    public Set<FunctionalRoleDto> getAssociatedFunctionalRoles() {
        return associatedFunctionalRoles;
    }

    public void setFunctionalRoles(Set<FunctionalRoleDto> associatedFunctionalRoles) {
        this.associatedFunctionalRoles = associatedFunctionalRoles;
    }

    public void setGroupType(GroupType groupType) {
        this.groupType = groupType;
    }

    public GroupType getGroupType() {
        return groupType;
    }

    public String getParentGroupName() {
        return parentGroupName;
    }

    public void setParentGroupName(String parentGroupName) {
        this.parentGroupName = parentGroupName;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public void setParentGroupId(String parentGroupId) {
        this.parentGroupId = parentGroupId;
    }

    public String getParentGroupId() {
        return parentGroupId;
    }

    public String getRawAnalysisGroupId() {
        return rawAnalysisGroupId;
    }

    public void setRawAnalysisGroupId(String rawAnalysisGroupId) {
        this.rawAnalysisGroupId = rawAnalysisGroupId;
    }

    public boolean isHasSubgroups() {
        return hasSubgroups;
    }

    public void setHasSubgroups(boolean hasSubgroups) {
        this.hasSubgroups = hasSubgroups;
    }

    public void setDocTypeAssociations(List<DocTypeAssociationDto> docTypeAssociations) {
        this.docTypeAssociations = docTypeAssociations;
    }

    public List<DocTypeAssociationDto> getDocTypeAssociations() {
        return docTypeAssociations;
    }

    public Boolean getGroupProcessing() {
        return isGroupProcessing;
    }

    public void setGroupProcessing(Boolean groupProcessing) {
        isGroupProcessing = groupProcessing;
    }

    public GroupDto getParentGroup() {
        return parentGroup;
    }

    public void setParentGroup(GroupDto parentGroup) {
        this.parentGroup = parentGroup;
    }
}
