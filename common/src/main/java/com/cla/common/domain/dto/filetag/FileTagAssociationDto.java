package com.cla.common.domain.dto.filetag;

import com.cla.common.domain.dto.scope.Scope;
import com.cla.common.domain.dto.scope.ScopedEntity;
import com.cla.common.domain.dto.security.EntityAssociationDetails;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

/**
 * Created by uri on 26/08/2015.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FileTagAssociationDto implements Serializable, EntityAssociationDetails, ScopedEntity {
    private FileTagDto fileTagDto;
    private long associationTimeStamp;
    private FileTagSource fileTagSource;
    private Scope scope;
    private Long originalFolderId;
    private boolean deleted;
    private Integer originDepthFromRoot;

    public FileTagAssociationDto() {
    }

    public FileTagAssociationDto(FileTagDto fileTagDto, long associationTimeStamp, FileTagSource fileTagSource,
                                 Scope scope, Long originalFolderId, boolean deleted, Integer originDepthFromRoot) {
        this.fileTagDto = fileTagDto;
        this.associationTimeStamp = associationTimeStamp;
        this.fileTagSource = fileTagSource;
        this.scope = scope;
        this.originalFolderId = originalFolderId;
        this.deleted = deleted;
        this.originDepthFromRoot = originDepthFromRoot;
    }

    public FileTagDto getFileTagDto() {
        return fileTagDto;
    }

    public void setFileTagDto(FileTagDto fileTagDto) {
        this.fileTagDto = fileTagDto;
    }

    public long getAssociationTimeStamp() {
        return associationTimeStamp;
    }

    @Override
    public boolean isSingleValue() {
        return fileTagDto.getType().isSingleValueTag();
    }

    @Override
    public long getCategoryId() {
        return fileTagDto.getType().getId();
    }

    public FileTagSource getFileTagSource() {
        return fileTagSource;
    }

    @Override
    public Long getEntityId() {
        return fileTagDto.getId();
    }

    public Scope getScope() {
        return scope;
    }

    public void setScope(Scope scope) {
        this.scope = scope;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    @Override
    public String toString() {
        return "FileTagAssociationDto{" +
                "fileTagDto=" + fileTagDto +
                ", fileTagSource=" + fileTagSource +
                '}';
    }

    @Override
    public boolean isImplicit() {
        return fileTagDto.isImplicit();
    }

    @Override
    public Long getOriginalFolderId() {
        return originalFolderId;
    }

    @Override
    public Integer getOriginDepthFromRoot() {
        return originDepthFromRoot;
    }

    public void setOriginDepthFromRoot(Integer originDepthFromRoot) {
        this.originDepthFromRoot = originDepthFromRoot;
    }
}
