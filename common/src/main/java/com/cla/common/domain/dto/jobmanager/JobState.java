package com.cla.common.domain.dto.jobmanager;

import java.util.*;

public enum JobState {
    NEW("New"),
    PENDING("PendingMap FinalizeMap Finalize"),
    WAITING("Waiting"),
    IN_PROGRESS("In progress"),
    PAUSED("Paused"),
    DONE("OK"),
    READY("Ready");

    private String displayName;

    JobState(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public static final Set<JobState> ACTIVE_STATES;
    public static final Set<JobState> STARTED_STATES;
    static {
        ACTIVE_STATES = Collections.unmodifiableSet(EnumSet.of(NEW, PENDING, IN_PROGRESS, READY, WAITING));

        EnumSet<JobState> tempStartedStates = EnumSet.copyOf(ACTIVE_STATES);
        tempStartedStates.remove(NEW);
        STARTED_STATES = Collections.unmodifiableSet(tempStartedStates);
    }

    public boolean isActive() {
        return ACTIVE_STATES.contains(this);
    }

    public boolean isStarted() {
        return STARTED_STATES.contains(this);
    }

    public static Collection<JobState> getActiveStates() {
        return ACTIVE_STATES;
    }

    public static Collection<JobState> getStartedStates() {
        return STARTED_STATES;
    }
}
