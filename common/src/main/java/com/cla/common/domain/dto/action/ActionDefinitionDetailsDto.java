package com.cla.common.domain.dto.action;

import java.util.List;

/**
 * Created by uri on 25/02/2016.
 */
public class ActionDefinitionDetailsDto {

    String actionId;

    List<ActionParameterDto> actionParameters;

    public String getActionId() {
        return actionId;
    }

    public void setActionId(String actionId) {
        this.actionId = actionId;
    }

    public List<ActionParameterDto> getActionParameters() {
        return actionParameters;
    }

    public void setActionParameters(List<ActionParameterDto> actionParameters) {
        this.actionParameters = actionParameters;
    }
}
