package com.cla.common.domain.dto.filetree;

/**
 * Created by uri on 07/07/2016.
 */
public enum CrawlerType {
    INTERNAL, MCF, MEDIA_PROCESSOR
}
