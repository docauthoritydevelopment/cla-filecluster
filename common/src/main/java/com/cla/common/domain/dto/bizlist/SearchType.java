package com.cla.common.domain.dto.bizlist;

/**
 * Created by uri on 23/08/2015.
 */
public enum SearchType {
    PATTERN,TEXT_SEARCH,ENTITY
}
