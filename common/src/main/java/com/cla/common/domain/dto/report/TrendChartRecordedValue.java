package com.cla.common.domain.dto.report;

import java.io.Serializable;

/**
 *
 * Created by uri on 31/07/2016.
 */
public class TrendChartRecordedValue implements Serializable {

    String id;

    Double value;

    int filterIndex;

    public TrendChartRecordedValue() {
    }

    public TrendChartRecordedValue(String id, Double value, int filterIndex) {
        this.id = id;
        this.value = value;
        this.filterIndex = filterIndex;
    }

    public int getFilterIndex() {
        return filterIndex;
    }

    public void setFilterIndex(int filterIndex) {
        this.filterIndex = filterIndex;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }
}
