package com.cla.common.domain.dto.mediaproc;

import com.cla.common.domain.dto.ClaDocProperties;
import com.cla.common.domain.dto.FileMetadata;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.util.List;

/**
 * Created by: yael
 * Created on: 9/25/2019
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OtherFileMetadata extends FileMetadata implements Serializable {

    private static final long serialVersionUID = 1L;

    public OtherFileMetadata() {
    }

    public OtherFileMetadata(ClaDocProperties fileProps) {
        super(fileProps);
    }

    @Override
    public List<String> toCsvInner() {
        return null;
    }

    @Override
    public List<String> toCsvTitleInner() {
        return null;
    }
}
