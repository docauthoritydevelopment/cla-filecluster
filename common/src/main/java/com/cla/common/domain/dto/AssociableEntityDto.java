package com.cla.common.domain.dto;

/**
 * Created by: yael
 * Created on: 5/5/2019
 */
public interface AssociableEntityDto {
    boolean isIdMatch(Object idToCheck);
    String createIdentifierString();
    String getName();
}
