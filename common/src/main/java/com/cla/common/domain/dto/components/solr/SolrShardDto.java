package com.cla.common.domain.dto.components.solr;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by uri on 01-Jun-17.
 */
public class SolrShardDto implements Serializable {
    private String name;
    private String state;

    private List<SolrShardReplicaDto> replicaDtos;

    public SolrShardDto() {
        replicaDtos = new ArrayList<>();
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getState() {
        return state;
    }

    public List<SolrShardReplicaDto> getReplicaDtos() {
        return replicaDtos;
    }

    public void setReplicaDtos(List<SolrShardReplicaDto> replicaDtos) {
        this.replicaDtos = replicaDtos;
    }

    public void addReplica(SolrShardReplicaDto solrShardReplicaDto) {
        replicaDtos.add(solrShardReplicaDto);
    }

    @Override
    public String toString() {
        return "SolrShardDto{" +
                "name='" + name + '\'' +
                ", state='" + state + '\'' +
                ", replicaDtos=" + replicaDtos +
                '}';
    }
}
