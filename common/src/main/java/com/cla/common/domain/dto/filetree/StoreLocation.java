package com.cla.common.domain.dto.filetree;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by uri on 08/03/2016.
 */
public enum StoreLocation {
    UNDEFINED(0),
    LOCAL_DATACENTER(1),
    REMOTE_DATACENTER(2),
    CLOUD_FILESHARE(3),
    CLOUD_SAAS(4),
    CLOUD_PASS(5),
    OTHER(6)
    ;

    private static Map<Integer, StoreLocation> map = new HashMap<Integer, StoreLocation>();

    static {
        for (StoreLocation state : StoreLocation.values()) {
            map.put(state.ordinal(), state);
        }
    }

    public static StoreLocation valueOf(int state) {
        return map.get(state);
    }

    private int value;

    StoreLocation(int value) {
        this.value = value;
    }
}
