package com.cla.common.domain.dto.schedule;

import com.cla.common.domain.dto.crawler.RunStatus;
import com.cla.common.domain.dto.filetree.CrawlRunDetailsDto;

/**
 * Created by uri on 04/05/2016.
 */
public class ScheduleGroupSummaryInfoDto {

    private ScheduleGroupDto scheduleGroupDto;

    private RunStatus runStatus;

    private Long missedScheduleTimeStamp;

    private Long nextPlannedWakeup;

    private CrawlRunDetailsDto lastCrawlRunDetails;

    public ScheduleGroupDto getScheduleGroupDto() {
        return scheduleGroupDto;
    }

    public void setScheduleGroupDto(ScheduleGroupDto scheduleGroupDto) {
        this.scheduleGroupDto = scheduleGroupDto;
    }

    public RunStatus getRunStatus() {
        return runStatus;
    }

    public void setRunStatus(RunStatus runStatus) {
        this.runStatus = runStatus;
    }

     public Long getMissedScheduleTimeStamp() {
        return missedScheduleTimeStamp;
    }

    public void setMissedScheduleTimeStamp(Long missedScheduleTimeStamp) {
        this.missedScheduleTimeStamp = missedScheduleTimeStamp;
    }

    public CrawlRunDetailsDto getLastCrawlRunDetails() {
        return lastCrawlRunDetails;
    }

    public void setLastCrawlRunDetails(CrawlRunDetailsDto lastCrawlRunDetails) {
        this.lastCrawlRunDetails = lastCrawlRunDetails;
    }

    public Long getNextPlannedWakeup() {
        return nextPlannedWakeup;
    }

    public void setNextPlannedWakeup(Long nextPlannedWakeup) {
        this.nextPlannedWakeup = nextPlannedWakeup;
    }

    @Override
    public String toString() {
        return "ScheduleGroupSummaryInfoDto{" +
                "scheduleGroupDto=" + scheduleGroupDto +
                ", runStatus=" + runStatus +
                ", missedScheduleTimeStamp=" + missedScheduleTimeStamp +
                ", lastCrawlRunDetails=" + lastCrawlRunDetails +
                '}';
    }
}
