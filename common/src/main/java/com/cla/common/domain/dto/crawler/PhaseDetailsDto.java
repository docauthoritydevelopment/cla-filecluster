package com.cla.common.domain.dto.crawler;


import com.cla.common.domain.dto.jobmanager.JobState;
import com.cla.common.domain.dto.jobmanager.JobType;

/**
 * Created by uri on 28/10/2015.
 */
public class PhaseDetailsDto {

    private String name;
    private JobType jobType;
    private Double tasksPerSecRate;

    private Double tasksPerSecRunningRate;
    private long runningRateDurationInMsc;

    private Integer phaseCounter;
    private Long phaseMaxMark;


    private Long rootFolderId;
    private Long bizListId;
    private String rootFolderName;
    private JobCompletionStatus completionStatus;

    private Long phaseStart;
    private Long phaseLastStopTime;
    private Long pauseDuration;
    private String phaseError;
    private Integer failedTasksCount;

    private String stateDescription;
    private JobState jobState;
    private Long jobId;

    private String hint;

    private Long totalTaskRetriesCount;

    public Long getTotalTaskRetriesCount() {
        return totalTaskRetriesCount;
    }

    public void setTotalTaskRetriesCount(Long totalTaskRetriesCount) {
        this.totalTaskRetriesCount = totalTaskRetriesCount;
    }

    public String getName() {
        return name;
    }

    public Long getRootFolderId() {
        return rootFolderId;
    }

    public void setRootFolderId(Long rootFolderId) {
        this.rootFolderId = rootFolderId;
    }

    public String getRootFolderName() {
        return rootFolderName;
    }

    public void setRootFolderName(String rootFolderName) {
        this.rootFolderName = rootFolderName;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getTasksPerSecRate() {
        return tasksPerSecRate;
    }

    public void setTasksPerSecRate(Double tasksPerSecRate) {
        this.tasksPerSecRate = tasksPerSecRate;
    }


    public Integer getPhaseCounter() {
        return phaseCounter;
    }

    public void setPhaseCounter(Integer phaseCounter) {
        this.phaseCounter = phaseCounter;
    }

    public Long getPhaseMaxMark() {
        return phaseMaxMark;
    }

    public void setPhaseMaxMark(Long phaseMaxMark) {
        this.phaseMaxMark = phaseMaxMark;
    }

    public Long getPhaseStart() {
        return phaseStart;
    }

    public void setPhaseStart(Long phaseStart) {
        this.phaseStart = phaseStart;
    }

    public Long getPhaseLastStopTime() {
        return phaseLastStopTime;
    }

    public void setPhaseLastStopTime(Long phaseLastStopTime) {
        this.phaseLastStopTime = phaseLastStopTime;
    }

    public String getPhaseError() {
        return phaseError;
    }

    public void setPhaseError(String phaseError) {
        this.phaseError = phaseError;
    }


    public String getStateDescription() {
        return stateDescription;
    }

    public void setStateDescription(String stateDescription) {
        this.stateDescription = stateDescription;
    }

    public JobState getJobState() {
        return jobState;
    }

    public void setJobState(JobState jobState) {
        this.jobState = jobState;
    }

    public String getHint() {
        return hint;
    }

    public void setHint(String hint) {
        this.hint = hint;
    }

    public Integer getFailedTasksCount() {
        return failedTasksCount;
    }

    public void setFailedTasksCount(Integer failedTasksCount) {
        this.failedTasksCount = failedTasksCount;
    }


    public Long getJobId() {
        return jobId;
    }

    public void setJobId(Long jobId) {
        this.jobId = jobId;
    }

    public JobCompletionStatus getCompletionStatus() {
        return completionStatus;
    }

    public void setCompletionStatus(JobCompletionStatus completionStatus) {
        this.completionStatus = completionStatus;
    }
    public Double getTasksPerSecRunningRate() {
        return tasksPerSecRunningRate;
    }

    public void setTasksPerSecRunningRate(Double tasksPerSecRunningRate) {
        this.tasksPerSecRunningRate = tasksPerSecRunningRate;
    }

    public JobType getJobType() {
        return jobType;
    }

    public void setJobType(JobType jobType) {
        this.jobType = jobType;
    }



    @Override
    public String toString() {
        return "PhaseDetailsDto{" +
                ", name='" + name + '\'' +
                ", jobType='" + jobType + '\'' +
                ", tasksPerSecRate=" + tasksPerSecRate +
                ", phaseCounter=" + phaseCounter +
                ", phaseMaxMark=" + phaseMaxMark +
                ", phaseStart=" + phaseStart +
                ", phaseLastStopTime=" + phaseLastStopTime +
                ", jobState=" + jobState +
                ", jobId=" + jobId +
                ", failedTasksCount=" + failedTasksCount +
                ", totalTaskRetriesCount=" + totalTaskRetriesCount +
                ", stateDescription='" + stateDescription + '\'' +
                ", bizListId='" + bizListId + '\'' +
                '}';
    }


    public Long getPauseDuration() {
        return pauseDuration;
    }

    public void setPauseDuration(Long pauseDuration) {
        this.pauseDuration = pauseDuration;
    }

    public long getRunningRateDurationInMsc() {
        return runningRateDurationInMsc;
    }

    public void setRunningRateDurationInMsc(long runningRateDurationInMsc) {
        this.runningRateDurationInMsc = runningRateDurationInMsc;
    }

    public Long getBizListId() {
        return bizListId;
    }

    public void setBizListId(Long bizListId) {
        this.bizListId = bizListId;
    }
}
