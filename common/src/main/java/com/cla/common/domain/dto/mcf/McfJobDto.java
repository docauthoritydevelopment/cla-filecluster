package com.cla.common.domain.dto.mcf;

/**
 * Created by uri on 05/07/2016.
 */
public class McfJobDto {

    private String id;

    private String name;

    private String mcfJobId;

    private boolean isFileFetcher;

    private Long rootFolderId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMcfJobId() {
        return mcfJobId;
    }

    public void setMcfJobId(String mcfJobId) {
        this.mcfJobId = mcfJobId;
    }

    public boolean isFileFetcher() {
        return isFileFetcher;
    }

    public void setFileFetcher(boolean fileFetcher) {
        isFileFetcher = fileFetcher;
    }

    public Long getRootFolderId() {
        return rootFolderId;
    }

    public void setRootFolderId(Long rootFolderId) {
        this.rootFolderId = rootFolderId;
    }
}
