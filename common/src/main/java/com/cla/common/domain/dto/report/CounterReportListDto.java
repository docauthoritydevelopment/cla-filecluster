package com.cla.common.domain.dto.report;

import java.io.Serializable;
import java.util.List;

/**
 * Created by uri on 11/10/2015.
 */
public class CounterReportListDto implements Serializable {

    List<CounterReportDto> counterReportDtos;

    private String name;

    public CounterReportListDto(String name, List<CounterReportDto> counterReportDtos) {
        this.name = name;
        this.counterReportDtos = counterReportDtos;
    }

    public List<CounterReportDto> getCounterReportDtos() {
        return counterReportDtos;
    }

    public void setCounterReportDtos(List<CounterReportDto> counterReportDtos) {
        this.counterReportDtos = counterReportDtos;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
