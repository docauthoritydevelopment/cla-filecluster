package com.cla.common.domain.dto.report;

import java.io.Serializable;

/**
 * Created by uri on 27/07/2016.
 */
public class TrendChartSeriesDto implements Serializable {

    private String id;

    private String name;

    private String filter;

    private double[] values;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public double[] getValues() {
        return values;
    }

    public void setValues(double[] values) {
        this.values = values;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFilter() {
        return filter;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    @Override
    public String toString() {
        return "TrendChartSeriesDto{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", filter='" + filter + '\'' +
                '}';
    }
}
