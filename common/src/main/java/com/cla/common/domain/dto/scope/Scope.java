package com.cla.common.domain.dto.scope;

import com.cla.common.domain.dto.Identifiable;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "@type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = WorkGroupsScopeDto.class, name = "workgroup"),
        @JsonSubTypes.Type(value = UserScopeDto.class, name = "user")
})
/**
 * Scope provides a separation level for view & work assignments
 * It is used for now on our tagging infra
 */
public interface Scope<T> extends Identifiable<Long> {

    T getScopeBindings();

    void setScopeBindings(T scopeBindings);
}
