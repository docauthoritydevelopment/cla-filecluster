package com.cla.common.domain.dto.messages;

import com.cla.common.domain.dto.file.FileContentHolder;
import com.cla.common.domain.dto.file.DirectoryExistStatus;
import com.cla.common.utils.json.*;
import com.cla.connector.domain.dto.file.FileType;
import com.cla.connector.domain.dto.error.ProcessingErrorType;
import com.cla.connector.domain.dto.messages.PhaseMessagePayload;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import java.util.List;
import java.util.Map;

/**
 *
 * Created by liron on 04/18/2017
 */
public class FileContentMessagePayload extends ExtractionResult implements PhaseMessagePayload {

    private String content;
    private String mime;
    private FileType fileType;
    private String requestId;
    private long modTimeMilli;
    private Long fileLength;
    private String baseName;
    private FileContentHolder fileContentHolder;
    private Map<String, List<String>> locations;
    private DirectoryExistStatus directoryExistsStatus;


    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getMime() {
        return mime;
    }

    public void setMime(String mime) {
        this.mime = mime;
    }

    public FileType getFileType() {
        return fileType;
    }

    public void setFileType(FileType fileType) {
        this.fileType = fileType;
    }

    public String getRequestId() { return requestId; }

    public void setRequestId(String requestId) { this.requestId = requestId; }

    public long getModTimeMilli() {
        return modTimeMilli;
    }

    public void setModTimeMilli(long modTimeMilli) {
        this.modTimeMilli = modTimeMilli;
    }

    public Long getFileLength() { return fileLength; }

    public void setFileLength(Long fileLength) { this.fileLength = fileLength; }

    public String getBaseName() { return baseName; }

    public void setBaseName(String baseName) { this.baseName = baseName; }

    public DirectoryExistStatus getDirectoryExistsStatus() {
        return directoryExistsStatus;
    }

    public void setDirectoryExistsStatus(DirectoryExistStatus directoryExists) {
        directoryExistsStatus = directoryExists;
    }

    @JsonSerialize(using = FileContentHolderSerializer.class)
    public FileContentHolder getFileContentHolder() {
        return fileContentHolder;
    }

    @JsonDeserialize(using = FileContentHolderDeserializer.class)
    public void setFileContentHolder(FileContentHolder fileContentHolder) {
        this.fileContentHolder = fileContentHolder;
    }

    public Map<String, List<String>> getLocations() {
        return locations;
    }

    public void setLocations(Map<String, List<String>> locations) {
        this.locations = locations;
    }

    @Override
    @JsonIgnore
    public ProcessingErrorType calculateProcessErrorType() {
        return getErrorType();
    }

    public FileContentMessagePayload copy(){
        FileContentMessagePayload payload = new FileContentMessagePayload();
        payload.setErrorType(this.getErrorType());
        payload.setFileName(this.getFileName());
        payload.setContent(this.content);
        payload.setMime(this.mime);
        payload.setFileType(this.fileType);
        payload.setRequestId(this.requestId);
        payload.setModTimeMilli(this.modTimeMilli);
        payload.setFileLength(this.fileLength);
        payload.setBaseName(baseName);
        payload.setFileContentHolder(this.fileContentHolder);
        payload.setDirectoryExistsStatus(this.directoryExistsStatus);

        if (locations != null) {
            Map<String, List<String>> locations = Maps.newHashMap();
            this.locations.forEach((key, value) -> {
                if (value != null) {
                    locations.put(key, Lists.newArrayList(value));
                }
            });
            payload.setLocations(locations);
        }

        return payload;
    }
}
