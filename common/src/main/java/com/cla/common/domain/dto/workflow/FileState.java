package com.cla.common.domain.dto.workflow;

public enum FileState {
    NONE, ACTION_PENDING_APPROVAL, APPROVED, DISAPPROVED
}
