package com.cla.common.domain.dto.messages.control;

import com.cla.common.domain.dto.filesearchpatterns.TextSearchPatternImpl;
import com.fasterxml.jackson.annotation.JsonUnwrapped;

import java.util.Collection;

/**
 * Created by: yael
 * Created on: 8/5/2018
 */
public class ControlPatternRequestPayload extends ControlMessagePayload {

    private Integer maxSingleSearchPatternMatches;

    private boolean searchPatternStopSearchAfterMulti = false;

    private int searchPatternCountThresholdMulti;

    @JsonUnwrapped
    private Collection<TextSearchPatternImpl> searchPatterns;

    public Collection<TextSearchPatternImpl> getSearchPatterns() {
        return searchPatterns;
    }

    public void setSearchPatterns(Collection<TextSearchPatternImpl> searchPatterns) {
        this.searchPatterns = searchPatterns;
    }

    public Integer getMaxSingleSearchPatternMatches() {
        return maxSingleSearchPatternMatches;
    }

    public boolean isSearchPatternStopSearchAfterMulti() {
        return searchPatternStopSearchAfterMulti;
    }

    public void setMaxSingleSearchPatternMatches(Integer maxSingleSearchPatternMatches) {
        this.maxSingleSearchPatternMatches = maxSingleSearchPatternMatches;
    }

    public void setSearchPatternStopSearchAfterMulti(boolean searchPatternStopSearchAfterMulti) {
        this.searchPatternStopSearchAfterMulti = searchPatternStopSearchAfterMulti;
    }

    public int getSearchPatternCountThresholdMulti() {
        return searchPatternCountThresholdMulti;
    }

    public void setSearchPatternCountThresholdMulti(int searchPatternCountThresholdMulti) {
        this.searchPatternCountThresholdMulti = searchPatternCountThresholdMulti;
    }

    @Override
    public String toString() {
        return "ControlPatternRequestPayload{" +
                "searchPatterns=" + searchPatterns +
                '}';
    }
}
