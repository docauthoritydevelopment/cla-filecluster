package com.cla.common.domain.dto.messages;

import com.cla.connector.domain.dto.error.ProcessingErrorType;
import com.cla.connector.domain.dto.messages.PhaseMessagePayload;
import com.cla.connector.domain.dto.messages.ProcessingError;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;

import java.util.List;

/**
 * Abstract class for all the processing phases results.
 * Common handling of the errors
 *
 * Created by vladi on 2/20/2017.
 */
public abstract class ProcessingPhaseMessage<T extends PhaseMessagePayload, S extends ProcessingError>{

    private Long runContext;
    private Long jobId;
    private Long taskId;


    private T payload = null;

    private MessagePayloadType payloadType;
    private String payloadStr;

    private List<S> errors;

    private Long sendTime;

    private Long expirationTime;

    public ProcessingPhaseMessage() {
        errors = Lists.newLinkedList();
        sendTime = System.currentTimeMillis();
    }

    public ProcessingPhaseMessage(T payload) {
        this();
        this.payload = payload;
    }

    public ProcessingPhaseMessage(String payloadStr, MessagePayloadType payloadType) {
        this();
        this.payloadStr = payloadStr;
        this.payloadType = payloadType;
    }

    public Long getRunContext() {
        return runContext;
    }

    public void setRunContext(Long runContext) {
        this.runContext = runContext;
    }

    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public Long getJobId() {
        return jobId;
    }

    public void setJobId(Long jobId) {
        this.jobId = jobId;
    }

    public T getPayload() {
        return payload;
    }

    public void setPayload(T payload) {
        this.payload = payload;
    }

    public boolean hasPayload(){
        return payload != null || (!Strings.isNullOrEmpty(payloadStr) && !"null".equals(payloadStr));
    }

    public void setPayloadStr(String payloadStr) {
        this.payloadStr = payloadStr;
    }

    public String getPayloadStr() {
        return payloadStr;
    }

    public void setPayloadType(MessagePayloadType payloadType) {
        this.payloadType = payloadType;
    }

    public MessagePayloadType getPayloadType() {
        return payloadType;
    }

    public void addError(S error){
        errors.add(error);
    }

    public List<S> getErrors(){
        return errors;
    }

    @SuppressWarnings("SimplifiableIfStatement")
    public boolean hasErrors(){
        if (!errors.isEmpty()){
            return true;
        }

        if (payload != null) {
            return payload.calculateProcessErrorType() != ProcessingErrorType.ERR_NONE;
        }

        return false;
    }

    public Long getExpirationTime() {
        return expirationTime;
    }

    public void setExpirationTime(Long expirationTime) {
        this.expirationTime = expirationTime;
    }

    @Override
    public String toString() {
        return "ProcessingPhaseMessage{" +
                "runContext=" + runContext +
                ", jobId=" + jobId +
                ", taskId=" + taskId +
                ", payload=" + payload +
                ", payloadType=" + payloadType +
                ", payloadStr='" + payloadStr + '\'' +
                ", errors=" + errors +
                ", sendTime=" + sendTime +
                ", expirationTime=" + expirationTime +
                '}';
    }

    public Long getSendTime() {
        return sendTime;
    }

    public void setSendTime(Long sendTime) {
        this.sendTime = sendTime;
    }
}
