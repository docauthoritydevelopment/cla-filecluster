package com.cla.common.domain.dto.mcf.mcf_api.job;

/**
 * Created by uri on 18/07/2016.
 */
public class McfJobStatusDto {

    /**
     * job_id"	The job identifier
     */
    String job_id;
    /**
     * "status"	The job status, having the possible values: "not yet run", "running", "paused", "done", "waiting", "stopping", "resuming", "starting up", "cleaning up", "error", "aborting", "restarting", "running no connector", and "terminating"
     */
    String status;
    /**
     * "error_text"	The error text, if the status is "error"
     */
    String error_text;
    /**
     * "start_time"	The job start time, in milliseconds since Jan 1, 1970
     */
    String start_time;
    /**
     * "end_time"	The job end time, in milliseconds since Jan 1, 1970
     */
    String end_time;
    /**
     * "documents_in_queue"	The total number of documents in the queue for the job
     */
    String documents_in_queue;
    /**
     * "documents_outstanding"	The number of documents for the job that are currently considered 'active'
     */
    String documents_outstanding;
    /**
     * "documents_processed"	The number of documents that in the queue for the job that have been processed at least once
     */
    String documents_processed;

    String queue_exact;

    String outstanding_exact;

    String processed_exact;

    public String getJob_id() {
        return job_id;
    }

    public void setJob_id(String job_id) {
        this.job_id = job_id;
    }

    /**
     * "status"	The job status, having the possible values: "not yet run", "running", "paused", "done", "waiting", "stopping", "resuming", "starting up", "cleaning up", "error", "aborting", "restarting", "running no connector", and "terminating"
     */
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getError_text() {
        return error_text;
    }

    public void setError_text(String error_text) {
        this.error_text = error_text;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public String getDocuments_in_queue() {
        return documents_in_queue;
    }

    public void setDocuments_in_queue(String documents_in_queue) {
        this.documents_in_queue = documents_in_queue;
    }

    public String getDocuments_outstanding() {
        return documents_outstanding;
    }

    public void setDocuments_outstanding(String documents_outstanding) {
        this.documents_outstanding = documents_outstanding;
    }

    public String getDocuments_processed() {
        return documents_processed;
    }

    public void setDocuments_processed(String documents_processed) {
        this.documents_processed = documents_processed;
    }

    @Override
    public String toString() {
        return "McfJobStatusDto{" +
                "job_id='" + job_id + '\'' +
                ", status='" + status + '\'' +
                ", error_text='" + error_text + '\'' +
                ", start_time='" + start_time + '\'' +
                ", end_time='" + end_time + '\'' +
                ", documents_in_queue='" + documents_in_queue + '\'' +
                ", documents_outstanding='" + documents_outstanding + '\'' +
                ", documents_processed='" + documents_processed + '\'' +
                '}';
    }


    public String getQueue_exact() {
        return queue_exact;
    }

    public void setQueue_exact(String queue_exact) {
        this.queue_exact = queue_exact;
    }

    public String getOutstanding_exact() {
        return outstanding_exact;
    }

    public void setOutstanding_exact(String outstanding_exact) {
        this.outstanding_exact = outstanding_exact;
    }

    public String getProcessed_exact() {
        return processed_exact;
    }

    public void setProcessed_exact(String processed_exact) {
        this.processed_exact = processed_exact;
    }


}
