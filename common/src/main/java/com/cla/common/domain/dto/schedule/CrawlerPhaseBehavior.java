package com.cla.common.domain.dto.schedule;

/**
 * Created by uri on 02/05/2016.
 */
public enum CrawlerPhaseBehavior {
    ONCE,EVERY_ROOT_FOLDER
}
