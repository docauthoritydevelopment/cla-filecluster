package com.cla.common.domain.dto.mediaproc.excel;

import com.cla.common.domain.dto.ClaDocProperties;
import com.cla.common.domain.dto.FileMetadata;
import com.cla.common.utils.CsvUtils;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.common.collect.Lists;

import java.util.*;
import java.util.stream.Collectors;

@SuppressWarnings("unused")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ExcelWorkbookMetaData extends FileMetadata {

    private int numOfSheets;
    private int numOfCells;
    private int numOfFormulas;
    private int numOfBlanks;
    private int numOfErrors;
    private int cellsSkipped;
    private List<String> sheetNames;

    public ExcelWorkbookMetaData() {
    }

    public ExcelWorkbookMetaData(final ClaDocProperties fileProps) {
        super(fileProps);
        if (fileProps != null) {
            this.numOfSheets = fileProps.getSheets();
            this.numOfCells = fileProps.getCells();
        }
    }

    public List<String> toCsvTitleInner() {
        final List<String> sb = new ArrayList<>();
        sb.add("numOfSheets");
        sb.add("numOfCells");
        sb.add("numOfFormulas");
        sb.add("numOfBlanks");
        sb.add("numOfErrors");
        sb.add("cellsSkipped");
        sb.add("sheetNames");
        return sb;
    }

    public List<String> toCsvInner(){
        final List<String> sb = Lists.newArrayList();
        sb.add(String.valueOf(numOfSheets));
        sb.add(String.valueOf(numOfCells));
        sb.add(String.valueOf(numOfFormulas));
        sb.add(String.valueOf(numOfBlanks));
        sb.add(String.valueOf(numOfErrors));
        sb.add(String.valueOf(cellsSkipped));
        sb.add(CsvUtils.notNull(sheetNames).stream().collect(Collectors.joining("|")));
        return sb;
    }

    public int getNumOfSheets() {
        return numOfSheets;
    }

    public void setNumOfSheets(final int numOfSheets) {
        this.numOfSheets = numOfSheets;
    }

    public int getNumOfCells() {
        return numOfCells;
    }

    public void setNumOfCells(final int numOfCells) {
        this.numOfCells = numOfCells;
    }

    public int getNumOfFormulas() {
        return numOfFormulas;
    }

    public void setNumOfFormulas(final int numOfFormulas) {
        this.numOfFormulas = numOfFormulas;
    }

    public int getNumOfBlanks() {
        return numOfBlanks;
    }

    public void setNumOfBlanks(final int numOfBlanks) {
        this.numOfBlanks = numOfBlanks;
    }

    public int getNumOfErrors() {
        return numOfErrors;
    }

    public void setNumOfErrors(final int numOfErrors) {
        this.numOfErrors = numOfErrors;
    }

    public int getCellsSkipped() {
        return cellsSkipped;
    }

    public void setCellsSkipped(final int cellsSkipped) {
        this.cellsSkipped = cellsSkipped;
    }

    public List<String> getSheetNames() {
        return sheetNames;
    }

    public void setSheetNames(final List<String> sheetNames) {
        this.sheetNames = sheetNames;
    }

}
