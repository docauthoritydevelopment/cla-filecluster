package com.cla.common.domain.dto.histogram;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author itay
 * 
 * Allow storage of tokens together with a set of values that may imply to their location 
 * 	within the originating source (eg. doc position).
 *
 * @param <T>
 */
public class ClaTokensCollection<T> implements Serializable {

	
	private static final long serialVersionUID = 1L;
	
	private Map<T, PositionSet> pCollection = new LinkedHashMap<>();
	private int globalCount = 0;
	private int skippedCount = 0;
	
	private int globalCountLimit = 0;
	private int membersCountLimit = 0;
	private boolean enforceDuringInsert = false;
	
	private class PositionSet implements Serializable {
		private List<Integer> positions = new ArrayList<>();

		public int getSize() {
			return positions.size();
		}

		public List<Integer> getPositions() {
			return positions;
		}

		public void setPositions(final List<Integer> positions) {
			this.positions = positions;
		}

		public void addPosition(final int pos) {
			positions.add(pos);
		}

		public void addPosition(final Collection<Integer> posCol) {
			positions.addAll(posCol);
		}
		
		@Override
		public boolean equals(final Object obj) {
			if (obj == null)
				return false;
			if (!(obj instanceof ClaTokensCollection.PositionSet))
				return false;
			final PositionSet other = (PositionSet) obj;
			if (this.positions == null)
				return (other.positions == null);
			
			return this.positions.equals(other.positions);
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + positions.hashCode();
			return result;
		}

		@Override
		public String toString() {
			return 
				positions.stream().map(p->p.toString()).collect(Collectors.joining(",","[","]"));
		}

	}		// class PositionSet

	/**
	 * Prepare for custom serialization ...
	 * 
     * This method is private but it is called using reflection by java
     * serialization mechanism. It overwrites the default object serialization.
     * The access modifier for this method MUST be set to <b>private</b> otherwise {@link java.io.StreamCorruptedException}
     * will be thrown.
     */
    @SuppressWarnings("unused")
	private void MyWriteObject(final ObjectOutputStream oos) throws IOException {
        //System.out.println(ExcelSheet.class.getName() + ".writeObject");

        //oos.defaultWriteObject();
        
        // scalars
    	oos.writeLong(serialVersionUID);
		oos.writeInt(globalCount);
		oos.writeInt(skippedCount);
		oos.writeInt(globalCountLimit);
		oos.writeInt(membersCountLimit);
        // Collection
        if (pCollection== null)
        	oos.writeInt(-1);
        else {
	        oos.writeInt(pCollection.size());
			for (final Map.Entry<T, PositionSet> entry : pCollection.entrySet()) {
				oos.writeObject(entry.getKey());
				final List<Integer> ps = entry.getValue().getPositions();
				oos.writeInt(ps.size());
				for(final Integer p : ps) {
					oos.writeInt(p);
				}
			}
        }
    }
 
    /**
     * This method is private but it is called using reflection by java
     * serialization mechanism. It overwrites the default object serialization.
     * The access modifier for this method MUST be set to <b>private</b> otherwise {@link java.io.StreamCorruptedException}
     * will be thrown.
     */
    @SuppressWarnings("unused")
	private void MyReadObject(final ObjectInputStream ois) throws IOException, ClassNotFoundException {
        //System.out.println(ExcelSheet.class.getName() + ".readObject");

        //ois.defaultReadObject();
        
        int i;
        int count;
        
        // scalars
        final long version  = ois.readLong();
        if (version != serialVersionUID)
        	throw new IOException("Bad type hash " + version + "!=" + serialVersionUID);
        globalCount = ois.readInt();
        skippedCount = ois.readInt();
        globalCountLimit = ois.readInt();
        membersCountLimit = ois.readInt();
        // Collection
		count = ois.readInt();
        if (count < 0)
        	pCollection = null;
        else {
        	pCollection = new LinkedHashMap<>(); 
			for (i=0;i<count;++i) {
				@SuppressWarnings("unchecked")
				final T key = (T) ois.readObject();
				final int  psSize = ois.readInt();
				final PositionSet ps = new PositionSet();
				for (int k=0;k<psSize;++k) {
					ps.addPosition(ois.readInt());
				}
				pCollection.put(key, ps);
			}
        }
    }

	public ClaTokensCollection() {
	}
	
	// no limitation enforcement
	public ClaTokensCollection(final Map<T, PositionSet> pCol) {
		if (pCol != null) {
			this.pCollection.putAll(pCol);
		}
		globalCount = pCol.values().stream().mapToInt(ps->ps.getSize()).sum();
	}
	
	public void addItem(final T item, final int pos) {
		PositionSet ps = pCollection.get(item);
		final boolean newItem = (ps == null);
		if (newItem)
			ps = new PositionSet();
		// Skip addition if limit is set and is reached
		if (enforceDuringInsert && ((globalCountLimit > 0 && globalCount >= globalCountLimit) ||
				(membersCountLimit > 0 && newItem && pCollection.size() >= membersCountLimit))) {
			skippedCount++;
		}
		else {
			if (newItem) {
				pCollection.put(item, ps);
			}
			ps.addPosition(pos);
			globalCount++;
		}
	}
	
	public void addItems(final Map<T, Integer> items) {
		if (items != null) {
			items.entrySet().forEach(e -> addItem(e.getKey(), e.getValue()));
		}
	}
	
	public void addItems(final Collection<T> items, final int pos) {
		if (items != null) {
			items.forEach(i -> addItem(i, pos));
		}
	}

	/**
	 * Enforce count limitations.
	 * This should be done after items insertion to make sure that the proper items are trimmed.
	 * globalCountLimit : supported only during insert
	 */
	public void applyLimitations()
	{
		if (enforceDuringInsert || pCollection.size() == 0)
			return;
		
		// Enforce membersCountLimit - drop least popular items
		if (membersCountLimit > 0 && pCollection.size() > membersCountLimit) {
			final Map<T, PositionSet> newHist = new LinkedHashMap<>();
			pCollection.entrySet().stream()
			.skip(pCollection.size()-membersCountLimit).forEach(e->newHist.put(e.getKey(), e.getValue()));
			final int newCount = newHist.values().stream().mapToInt(ps->ps.getSize()).sum();
			skippedCount += globalCount-newCount;
			globalCount = newCount;
			pCollection = newHist;
		}

	}

	public boolean contains(final T item) {
		final PositionSet ps = pCollection.get(item);
		return (ps != null);
	}

	public int getCount(final T item) {
		final PositionSet ps = pCollection.get(item);
		return (ps == null)?0:ps.getSize();
	}

	public List<Integer> getPositionsSet(final T item) {
		final PositionSet ps = pCollection.get(item);
		return (ps == null)?null:ps.getPositions();
	}

	public int getGlobalCountLimit() {
		return globalCountLimit;
	}

	public void setGlobalCountLimit(final int globalCountLimit) {
		this.globalCountLimit = globalCountLimit;
	}

	public int getMembersCountLimit() {
		return membersCountLimit;
	}
	
	public void setMembersCountLimit(final int membersCountLimit) {
		this.membersCountLimit = membersCountLimit;
	}

	public boolean isEnforceDuringInsert() {
		return enforceDuringInsert;
	}

	public void setEnforceDuringInsert(final boolean enforceDuringInsert) {
		this.enforceDuringInsert = enforceDuringInsert;
	}

	public int getGlobalCount() {
		return globalCount;
	}

	public int getSkippedCount() {
		return skippedCount;
	}

	public int getTotalCount() {
		return globalCount+skippedCount;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		
		for (final Map.Entry<T, PositionSet> entry : pCollection.entrySet()) {
			result = prime * result + entry.getKey().hashCode();
			result = prime * result + entry.getValue().hashCode();
		}
		result = prime * result + globalCount;
		result = prime * result + skippedCount;
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		@SuppressWarnings("unchecked")
		final
		ClaTokensCollection<T> other = (ClaTokensCollection<T>) obj;
		if (globalCount != other.globalCount)
			return false;
		if (skippedCount != other.skippedCount)
			return false;
		if (pCollection.size() != other.pCollection.size())
			return false;
		return pCollection.equals(other.pCollection);
	}

	public String getPropertiesString() {
		return  "count="+globalCount+
				"skipd="+skippedCount+
				"items="+pCollection.size()+
				"mxCnt="+((pCollection.size()==0)?0:pCollection.values().stream().mapToInt(ps->ps.getSize()).max().orElse(0));
	}

	@Override
	public String toString() {
		return 
				pCollection.entrySet().stream()
				.map(e->e.getKey().toString()+":"+e.getValue().toString())
				.collect(Collectors.joining(",","{","}"));
	}

	public Object toString(final Map<Long, String> debugHashMapper) {
		if (debugHashMapper == null) {
			return toString();
		}
		return 
				pCollection.entrySet().stream()
				.map(e->debugHashMapper.get(e.getKey())+":"+e.getValue().toString())
				.collect(Collectors.joining(",","{","}"));
	}

}
