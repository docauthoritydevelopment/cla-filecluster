package com.cla.common.domain.dto.report;

/**
 * Created by Itay on 24/8/2016.
 */
public enum TimeResolutionType {
    TR_miliSec,
    TR_hour,
    TR_day,
    TR_week,
    TR_month,
    TR_quarter,
    TR_year
}
