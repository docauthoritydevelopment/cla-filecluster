package com.cla.common.domain.dto.board;

import lombok.*;
import lombok.experimental.Accessors;

@NoArgsConstructor
@Getter
@Setter
@Accessors(chain = true)
@ToString(callSuper = true)
@EqualsAndHashCode
public class BoardDto {

    private Long id;

    private String name;

    private String description;

    private long ownerId;

    private boolean isPublic;

    private BoardType boardType;

    private Long dateCreated;

    private Long dateModified;
}
