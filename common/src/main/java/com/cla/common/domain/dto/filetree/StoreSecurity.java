package com.cla.common.domain.dto.filetree;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by uri on 08/03/2016.
 */
public enum StoreSecurity {
    UNDEFINED(0),
    CLEAR(1),
    ENCRYPTED(2)
    ;

    private static Map<Integer, StoreSecurity> map = new HashMap<Integer, StoreSecurity>();

    static {
        for (StoreSecurity state : StoreSecurity.values()) {
            map.put(state.ordinal(), state);
        }
    }

    public static StoreSecurity valueOf(int state) {
        return map.get(state);
    }

    private int value;

    StoreSecurity(int value) {
        this.value = value;
    }
}
