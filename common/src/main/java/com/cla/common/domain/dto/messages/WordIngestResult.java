package com.cla.common.domain.dto.messages;

import com.cla.common.domain.dto.SearchPatternCounting;
import com.cla.common.domain.dto.extraction.SearchPatternCountDto;
import com.cla.common.domain.dto.histogram.ClaSuperCollection;
import com.cla.common.domain.dto.mediaproc.SearchPatternServiceInterface;
import com.cla.common.domain.dto.mediaproc.TextualResult;
import com.cla.common.domain.dto.word.WordfileMetaData;
import com.cla.common.utils.json.ClaSuperCollectionDeserializer;
import com.cla.common.utils.json.ClaSuperCollectionSerializer;
import com.cla.common.utils.json.ZippingStringDeserializer;
import com.cla.common.utils.json.ZippingStringSerializer;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.List;
import java.util.Set;

/**
 * Post-processed ingested word file, ready for persistence and analysis
 * @see com.cla.common.domain.dto.mediaproc.word.WordFile
 *
 * Created by vladi on 3/8/2017.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class WordIngestResult extends IngestMessagePayload<WordfileMetaData> implements TextualResult, SearchPatternCounting {

    private List<String> combinedProposedTitles;
    private String csvValue;
    private ClaSuperCollection<Long> longHistograms;
    private String content;
    private String originalContent;
    private boolean scanned = false;
    private Set<SearchPatternCountDto> searchPatternsCounting = null;


    public List<String> getCombinedProposedTitles() {
        return combinedProposedTitles;
    }

    public void setCombinedProposedTitles(List<String> combinedProposedTitles) {
        this.combinedProposedTitles = combinedProposedTitles;
    }

    public String getCsvValue() {
        return csvValue;
    }

    public void setCsvValue(String csvValue) {
        this.csvValue = csvValue;
    }

    @JsonProperty("long-histograms")
    @JsonSerialize(using = ClaSuperCollectionSerializer.class)
    public ClaSuperCollection<Long> getLongHistograms() {
        return longHistograms;
    }

    @JsonDeserialize(using = ClaSuperCollectionDeserializer.class)
    public void setLongHistograms(ClaSuperCollection<Long> longHistograms) {
        this.longHistograms = longHistograms;
    }

    @JsonSerialize(using = ZippingStringSerializer.class)
    public String getContent() {
        return content;
    }

    @JsonDeserialize(using = ZippingStringDeserializer.class)
    public void setContent(String content) {
        this.content = content;
    }

    @JsonSerialize(using = ZippingStringSerializer.class)
    public String getOriginalContent() {
        return originalContent;
    }

    @JsonDeserialize(using = ZippingStringDeserializer.class)
    public void setOriginalContent(String originalContent) {
        this.originalContent = originalContent;
    }

    public boolean isScanned() {
        return scanned;
    }

    public void setScanned(boolean scanned) {
        this.scanned = scanned;
    }

    public Set<SearchPatternCountDto> getSearchPatternsCounting() {
        return searchPatternsCounting;
    }

    public void setSearchPatternsCounting(Set<SearchPatternCountDto> searchPatternsCounting) {
        this.searchPatternsCounting = searchPatternsCounting;
    }

    public void handleSearchPatterns(String requestId, SearchPatternServiceInterface searchPatternService) {
        if (getOriginalContent() != null) {
            Set<SearchPatternCountDto> patternCountDtos = searchPatternService.getSearchPatternsCounting(getOriginalContent());
            setSearchPatternsCounting(patternCountDtos);
            if (requestId == null) {
                setOriginalContent(null);
            }
        }
    }
}
