package com.cla.common.domain.dto;

import com.cla.common.domain.dto.extraction.SearchPatternCountDto;

import java.util.Set;

public interface SearchPatternCounting {

    Set<SearchPatternCountDto> getSearchPatternsCounting();

    String getOriginalContent();

}
