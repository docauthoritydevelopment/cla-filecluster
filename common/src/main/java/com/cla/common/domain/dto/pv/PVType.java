package com.cla.common.domain.dto.pv;

import java.util.HashMap;
import java.util.Map;

public enum PVType {
	// types for Word processing 
	PV_Headings(1, "heading")
	,PV_StyleSignatures(2, "styleSig3")
	,PV_StyleSequence(3, "ssqSyleSeq3")
	,PV_AllStyleSignatures(4, "astAllStyleSig")
	,PV_AllStyleSequence(5, "asqAllStyleSeq")
	,PV_AbsoluteStyleSignatures(6, "absStyleSig")
	,PV_AbsoluteStyleSequence(7, "abqStyleSeq")
	,PV_HeaderStyles(8, "hfsHFtStyle")
	,PV_Pictures(9, "picture")
	,PV_LetterHeadLabels(10, "letterHLabel")
	,PV_ParagraphLabels(11, "pLabel")
	,PV_BodyNgrams(12, "bodyNg")
	,PV_SubBodyNgrams(13, "subBdNg")
	,PV_DocstartNgrams(14, "docstartNg")
	,PV_HeaderNgrams(15, "hfnHFtNgr")
	// types for Workbooks 
	,PV_RowHeadings(21, "rhdRowHead")
	,PV_ExcelStyles(22, "estExcelStyle")
//	,PV_Pictures
	,PV_CellValues(23, "value")
	,PV_Formulas(24, "frmFormula")
	,PV_ConcreteCellValues(25, "cnvConcVal")
	,PV_TextNgrams(26, "etnTextNg")
	,PV_TitlesNgrams(27, "ttnTitleNg")
	,PV_SheetLayoutSignatures(28, "layout")
	;
	
	private static final Map<Integer, PVType> intMapping = new HashMap<Integer, PVType>();
	private final int value;
	private final String name;

	PVType(final int value) {
		this(value, "_"+value);
	}

	PVType(final int value, final String name) {
		this.value = value;
		this.name = name;
	}

	static {
		for (final PVType v : PVType.values()) {
			intMapping.put(v.value, v);
		}
	}

	public int toInt() {
		return value;
	}

	@Override
	public String toString() {
		return name;
	}

	public static PVType valueOf(final int value) {
		final PVType mt = PVType.intMapping.get(value);
		if (mt == null) {
			throw new RuntimeException("Invalid value: " + value);
		}
		return mt;
	}
}
