package com.cla.common.domain.dto.filesearchpatterns;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

public class UkNationalInsuranceTextSearchPattern extends TextSearchPatternImpl {

//	public static final String UK_NI_PATERN = "\\b[A-CEGHJ-PR-TW-Z][A-CEGHJ-NPR-TW-Z]\\s?[0-9]{2}\\s?[0-9]{2}\\s?[0-9]{2}\\s?[A-D]?\\b";
	public static final String UK_NI_PATERN = "\\b(?!([Bb][Gg]|[Gg][Bb]|[Kk][Nn]|[Nn][Kk]|[Nn][Tt]|[Tt][Nn]|[Zz]{2}))[ABCEGHJ-PRSTW-Zabceghj-prstw-z][ABCEGHJ-NPRSTW-Zabceghj-nprstw-z][ ]?\\d{2}[ ]?\\d{2}[ ]?\\d{2}[ ]?[A-Da-d]\\b";
	public static final List<String> disallowedPrefixes = Arrays.asList("BG", "GB", "NK", "KN", "TN", "NT", "ZZ");

	public UkNationalInsuranceTextSearchPattern() {}

	public UkNationalInsuranceTextSearchPattern(final Integer id, final String name) {
		super(id, name, UK_NI_PATERN);
	}

	@Override
	public Predicate<String> getPredicate() {
		return this::validate;
	}

	private boolean validate(final String id) {
		String id2Test = id.replaceAll("\\s", "");
		int origLen = id.trim().length();
		int newLen = id2Test.length();
		if (origLen != newLen && (origLen - newLen) != (newLen==9?4:3)) {
			// If white space included it should be in full: XX NN NN NN C or XX NN NN NN
			return false;
		}

		String prefix = id2Test.substring(0,2);
		return !disallowedPrefixes.contains(prefix);
	}
}
