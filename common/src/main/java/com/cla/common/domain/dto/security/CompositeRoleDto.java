package com.cla.common.domain.dto.security;

public abstract class CompositeRoleDto {

    protected Long id;

    protected String name;

    protected String description;

    protected String displayName;

    protected RoleTemplateDto template;

    protected boolean deleted;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public RoleTemplateDto getTemplate() {
        return template;
    }

    public void setTemplate(RoleTemplateDto template) {
        this.template = template;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CompositeRoleDto that = (CompositeRoleDto) o;

        if (id != null ? !id.equals(that.getId()) : that.getId() != null) return false;
        return name != null ? name.equals(that.getName()) : that.getName() == null;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "BizRoleDto{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", displayName='" + displayName + '\'' +
                ", template='" + template + '\'' +
                '}';
    }

    public void copy(CompositeRoleDto other) {
        this.id = other.id;
        this.name = other.name;
        this.description = other.description;
        this.displayName = other.displayName;
        this.template = other.template;
        this.deleted = other.deleted;
    }
}
