package com.cla.common.domain.dto;

import java.util.HashMap;
import java.util.Map;

public enum FileMediaType {
	UNKNOWN(0),EXCEL(1),EXCELXML(2),WORD(3),WORD6(4),WORDXML(5),;
	private static final Map<Integer, FileMediaType> mapping = new HashMap<Integer, FileMediaType>();
	private int value;

	FileMediaType(int value) {
		this.value = value;
	}

	static {
		for (FileMediaType provider : FileMediaType.values())
			mapping.put(provider.value, provider);
	}

	public int toInt() {
		return value;
	}

	public static FileMediaType valueOf(int value) {
		FileMediaType mt = FileMediaType.mapping.get(value);
		if (mt == null)
			throw new RuntimeException("Invalid value:" + value);
		return mt;
	}
}
