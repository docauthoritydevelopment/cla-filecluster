package com.cla.common.domain.dto.upgrade;

import com.google.common.io.Files;

import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by uri on 03/03/2016.
 */
public class UpgradeSchemaDto implements Serializable, Comparable<UpgradeSchemaDto> {


    private long executionTime;

    Long version;

    String description;

    Path file;

    private String nameWithoutExtension;

    public UpgradeSchemaDto() {
    }

    public UpgradeSchemaDto(Path path) {
        this.file = path;
        nameWithoutExtension = Files.getNameWithoutExtension(file.getFileName().toString());
        Long version = extractVersionFromFileName(nameWithoutExtension);
        this.version = version;
        this.description = extractDescriptionFromFileName(nameWithoutExtension);
    }

    public Long extractVersionFromFileName(String fileNameWithoutExtension) {
        String temp = nameWithoutExtension;
        int i = nameWithoutExtension.indexOf("_");
        if (i >0) {
            temp = nameWithoutExtension.substring(0,i);
        }
        try {
            return Long.valueOf(temp);
        } catch (NumberFormatException e) {
            return null;
        }
    }

    public String extractDescriptionFromFileName(String fileNameWithoutExtension) {
        String temp = nameWithoutExtension;
        int i = nameWithoutExtension.indexOf("_");
        if (i >0) {
            temp = nameWithoutExtension.substring(i+1);
            return temp;
        }
        return null;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(long version) {
        this.version = version;
    }

    public Path getFile() {
        return file;
    }

    public void setFile(Path file) {
        this.file = file;
    }

    public String getNameWithoutExtension() {
        return nameWithoutExtension;
    }

    public void setNameWithoutExtension(String nameWithoutExtension) {
        this.nameWithoutExtension = nameWithoutExtension;
    }

    @Override
    public int compareTo(UpgradeSchemaDto o) {
        return Long.compare(version,o.getVersion());
    }

    public String prettyString() {
        StringBuilder sb = new StringBuilder();
        if (version != null) {
            sb.append(version);
        }
        if (description != null) {
            sb.append(" ").append(description).append("");
        }
        else {
            sb.append(" (").append(nameWithoutExtension).append(")");
        }
        return sb.toString();
    }

    @Override
    public String toString() {
        return "UpgradeSchemaDto{" +
                "version=" + version +
                ", description='" + description + '\'' +
                ", file=" + file +
                '}';
    }

    public void setExecutionTime(long executionTime) {
        this.executionTime = executionTime;
    }

    public long getExecutionTime() {
        return executionTime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public static List<UpgradeSchemaDto> findSqlScriptsInFolder(Path sqlFolder) throws IOException {
        try(Stream<Path> list = java.nio.file.Files.list(sqlFolder)) {
            return list.filter(f -> f.toString().endsWith(".sql"))
                    .map(UpgradeSchemaDto::new)
                    .filter(f -> f.getVersion() != null)
                    .collect(Collectors.toList());
        }
    }

}
