package com.cla.common.domain.dto.email;

import java.io.Serializable;

/**
 * Created by: yael
 * Created on: 2/3/2019
 */
public class MailAddressDto implements Serializable {

    private String address;
    private String name;
    private String domain;
    private MailAddressType mailAddressType;

    public MailAddressDto() {
    }

    public MailAddressDto(String address, String name, String domain, MailAddressType mailAddressType) {
        this.address = address;
        this.name = name;
        this.domain = domain;
        this.mailAddressType = mailAddressType;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public MailAddressType getMailAddressType() {
        return mailAddressType;
    }

    public void setMailAddressType(MailAddressType mailAddressType) {
        this.mailAddressType = mailAddressType;
    }
}
