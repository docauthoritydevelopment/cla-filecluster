package com.cla.common.domain.dto.filetree;

import com.cla.common.domain.dto.crawler.PhaseDetailsDto;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by: yael
 * Created on: 10/21/2018
 */
public class RootFolderRunSummaryInfo {

    private RootFolderSummaryInfo rootFolderSummaryInfo = new RootFolderSummaryInfo();
    private List<PhaseDetailsDto> jobs = new ArrayList<>();
    private PhaseDetailsDto currentJob;

    public RootFolderSummaryInfo getRootFolderSummaryInfo() {
        return rootFolderSummaryInfo;
    }

    public void setRootFolderSummaryInfo(RootFolderSummaryInfo rootFolderSummaryInfo) {
        this.rootFolderSummaryInfo = rootFolderSummaryInfo;
    }

    public List<PhaseDetailsDto> getJobs() {
        return jobs;
    }

    public void setJobs(List<PhaseDetailsDto> jobs) {
        this.jobs = jobs;
    }

    public PhaseDetailsDto getCurrentJob() {
        return currentJob;
    }

    public void setCurrentJob(PhaseDetailsDto currentJob) {
        this.currentJob = currentJob;
    }

    @Override
    public String toString() {
        return "RootFolderRunSummaryInfo{" +
                "rootFolderSummaryInfo=" + rootFolderSummaryInfo +
                ", jobs=" + jobs +
                ", currentJob=" + currentJob +
                '}';
    }
}
