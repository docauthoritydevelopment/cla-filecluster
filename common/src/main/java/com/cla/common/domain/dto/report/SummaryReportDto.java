package com.cla.common.domain.dto.report;

import java.io.Serializable;
import java.util.List;

/**
 * Created by uri on 07/10/2015.
 */
public class SummaryReportDto implements Serializable {
    public List<CounterReportDto> counterReportDtos;

    public List<StringReportDto> stringReportDtos;

    public List<StringListReportDto> stringListReportDtos;

    private MultiValueListDto multiValueListDto;

    public SummaryReportDto() {
    }


    public SummaryReportDto(List<CounterReportDto> counterReportDtos) {
        this.counterReportDtos = counterReportDtos;
    }

    public List<CounterReportDto> getCounterReportDtos() {
        return counterReportDtos;
    }

    public void setCounterReportDtos(List<CounterReportDto> counterReportDtos) {
        this.counterReportDtos = counterReportDtos;
    }

    public List<StringListReportDto> getStringListReportDtos() {
        return stringListReportDtos;
    }

    public void setStringListReportDtos(List<StringListReportDto> stringListReportDtos) {
        this.stringListReportDtos = stringListReportDtos;
    }

    public List<StringReportDto> getStringReportDtos() {
        return stringReportDtos;
    }

    public void setStringReportDtos(List<StringReportDto> stringReportDtos) {
        this.stringReportDtos = stringReportDtos;
    }

    public MultiValueListDto getMultiValueListDto() {
        return multiValueListDto;
    }

    public void setMultiValueListDto(MultiValueListDto multiValueListDto) {
        this.multiValueListDto = multiValueListDto;
    }
}
