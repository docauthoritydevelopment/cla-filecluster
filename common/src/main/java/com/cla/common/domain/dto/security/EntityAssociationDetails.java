package com.cla.common.domain.dto.security;

import com.cla.common.domain.dto.filetag.FileTagSource;
import com.cla.common.domain.dto.scope.ScopedEntity;

/**
 * Provides crucial details on the association in order to filter out on results and prioritize correctly
 */
public interface EntityAssociationDetails {

    /**
     * @return if the association is bound to be single on entities or multiple of the same category can apply
     */
    boolean isSingleValue();

    /**
     * @return the category id which this association entity belongs to
     */
    long getCategoryId();

    /**
     * @return source of the association, is it filegroup/file/folder(explicit|implicit)
     */
    FileTagSource getFileTagSource();

    /**
     * @return the entity id  of the association (i.e tagId or docTypeId)
     */
    Long getEntityId();

    /**
     * Returns the original folder ID, used for getting
     * depth from root if relevant to the association (only for folders)
     *
     * @return ID of the folder
     */
    Long getOriginalFolderId();

    Integer getOriginDepthFromRoot();

    boolean isImplicit();
}
