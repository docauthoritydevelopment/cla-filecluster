package com.cla.common.domain.dto.messages;

import com.cla.connector.domain.dto.error.ProcessingErrorType;

public abstract class ExtractionResult {

    protected String fileName;
    private ProcessingErrorType errorType = ProcessingErrorType.ERR_NONE;
    private String exceptionText;

    public ExtractionResult(ExtractionResult other) {
        this.fileName = other.fileName;
        this.errorType = other.errorType;
        this.exceptionText = other.exceptionText;
    }

    public ExtractionResult(){}

    // ------------------------  getters / setters -----------------------------------------

    public String getFileName() {
        return fileName;
    }

    public void setFileName(final String fileName) {
        this.fileName = fileName;
    }

    public ProcessingErrorType getErrorType() {
        return errorType;
    }

    public void setErrorType(ProcessingErrorType errorType) {
        this.errorType = errorType;
    }

    public boolean hasError(){
        return (errorType != null && errorType != ProcessingErrorType.ERR_NONE);
    }

    public void setErrorTypeIfEmpty(ProcessingErrorType errorType) {
        if (!hasError()) {
            this.errorType = errorType;
        }
    }

    public String getExceptionText() {
        return exceptionText;
    }

    public void setExceptionText(String exceptionText) {
        this.exceptionText = exceptionText;
    }
}
