package com.cla.common.domain.dto.filetree;

import java.io.Serializable;

/**
 * Entity credential is a record of username/password used to access an entity (currently only root folder)
 * Created by yael on 11/9/2017.
 */
public class EntityCredentialsDto implements Serializable {

    private long id;
    private EntityType entityType;
    private long entityId;
    private String username;
    private String password;
    private boolean shouldEncryptPassword = false;
    private long createdTimeStampMs;
    private long modifiedTimeStampMs;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public EntityType getEntityType() {
        return entityType;
    }

    public void setEntityType(EntityType entityType) {
        this.entityType = entityType;
    }

    public long getEntityId() {
        return entityId;
    }

    public void setEntityId(long entityId) {
        this.entityId = entityId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isShouldEncryptPassword() {
        return shouldEncryptPassword;
    }

    public void setShouldEncryptPassword(boolean shouldEncryptPassword) {
        this.shouldEncryptPassword = shouldEncryptPassword;
    }

    public long getCreatedTimeStampMs() {
        return createdTimeStampMs;
    }

    public void setCreatedTimeStampMs(long createdTimeStampMs) {
        this.createdTimeStampMs = createdTimeStampMs;
    }

    public long getModifiedTimeStampMs() {
        return modifiedTimeStampMs;
    }

    public void setModifiedTimeStampMs(long modifiedTimeStampMs) {
        this.modifiedTimeStampMs = modifiedTimeStampMs;
    }

    @Override
    public String toString() {
        return "EntityCredentialsDto{" +
                "id=" + id +
                ", entityType=" + entityType +
                ", entityId=" + entityId +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", shouldEncryptPassword=" + shouldEncryptPassword +
                ", createdTimeStampMs=" + createdTimeStampMs +
                ", modifiedTimeStampMs=" + modifiedTimeStampMs +
                '}';
    }
}
