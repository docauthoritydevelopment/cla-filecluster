package com.cla.common.domain.dto.word;

import com.cla.common.domain.dto.ClaDocProperties;
import com.cla.common.domain.dto.FileMetadata;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.common.collect.Lists;

import java.io.Serializable;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class WordfileMetaData extends FileMetadata implements Serializable {

    private static final long serialVersionUID = 1L;

    private int numWords;
    private int numPages;
    private int numParagraphs;
    private int numChars;
    private int numLines;


    public WordfileMetaData() {
    }

    public WordfileMetaData(final ClaDocProperties fileProps) {
        super(fileProps);
        if (fileProps != null) {
            this.numWords = fileProps.getWords();
            this.numPages = fileProps.getPages();
            this.numParagraphs = fileProps.getParagraphs();
            this.numChars = fileProps.getChars();
            this.numLines = fileProps.getLines();
        }
    }


    public List<String> toCsvTitleInner() {
        final List<String> sb = Lists.newArrayList();
        sb.add("numWords");
        sb.add("numPages");
        sb.add("numParagraphs");
        sb.add("numChars");
        sb.add("numLines");
        return sb;
    }

    public List<String> toCsvInner(){
        final List<String> sb = Lists.newArrayList();
        sb.add(String.valueOf(numWords));
        sb.add(String.valueOf(numPages));
        sb.add(String.valueOf(numParagraphs));
        sb.add(String.valueOf(numChars));
        sb.add(String.valueOf(numLines));
        return sb;
    }

    public int getNumWords() {
        return numWords;
    }

    public void setNumWords(final int numWords) {
        this.numWords = numWords;
    }

    public int getNumPages() {
        return numPages;
    }

    public void setNumPages(final int numPages) {
        this.numPages = numPages;
    }

    public int getNumParagraphs() {
        return numParagraphs;
    }

    public void setNumParagraphs(final int numParagraphs) {
        this.numParagraphs = numParagraphs;
    }

    public int getNumChars() {
        return numChars;
    }

    public void setNumChars(final int numChars) {
        this.numChars = numChars;
    }

    public int getNumLines() {
        return numLines;
    }

    public void setNumLines(final int numLines) {
        this.numLines = numLines;
    }

}
