package com.cla.common.domain.dto.mediaproc.excel;



import com.google.common.collect.Maps;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Excel workbook processing tools
 * Created by vladi on 3/6/2017.
 */
public class ExcelWorkbookHelper {

    private static final int MAX_NUM_HEADINGS_IN_PROPOSED_TITLE = 4;
    private static final String PROPOSED_TITLE_KEY_SEPARATOR = ":";

    public static List<String> getProposedTitlesCombined(ExcelWorkbook workbook) {
        // get keys sorted by map values (desc)
        final Map<String, String> proposedTitles = getProposedTitles(workbook);
        return proposedTitles.entrySet().stream()
                // ascending order - lower rate-string <-> higher in rate value
                .sorted(Comparator.comparing(Map.Entry::getValue))
                .map(e -> (e.getValue() + PROPOSED_TITLE_KEY_SEPARATOR + e.getKey()))
                .collect(Collectors.toList());
    }

    public static Map<String, String> getProposedTitles(ExcelWorkbook workbook) {

        Map<String, String> result = Maps.newLinkedHashMap();
        Map<String, ExcelSheet> sheets = workbook.getSheets();

        for (Map.Entry<String, ExcelSheet> sheetEntry : sheets.entrySet()) {
            final Map<String, String> sheetTitles = sheetEntry.getValue().getProposedTitles();
            for (final Map.Entry<String, String> titleEntry : sheetTitles.entrySet()) {
                final String title = titleEntry.getKey();
                final String existingTitle = result.get(title);
                if (existingTitle == null || existingTitle.compareTo(titleEntry.getValue()) < 0) {
                    result.put(title, titleEntry.getValue());
                }
            }
        }

        if (result.size() == 0) {
            // If no proposed titles and there are headings, use headings as a proposed title
            int order = 0;
            for (final Map.Entry<String, ExcelSheet> sheetEntry : sheets.entrySet()) {
                final List<String> sheetHeadings = sheetEntry.getValue().getSheetHeadings();
                if (sheetHeadings != null) {
                    final String pTitle = sheetHeadings.stream().limit(MAX_NUM_HEADINGS_IN_PROPOSED_TITLE)
                            .collect(Collectors.joining(", "));
                    result.put(pTitle, String.format(IngestConstants.TITLE_KEY_FORMAT_STRING, "P0", 10, ++order));
                }
            }
        }

        return result;
    }

    public static Map<String, String> getProposedTitles(ExcelWorkbook workbook, String sheetName) {
        Map<String, ExcelSheet> sheets = workbook.getSheets();
        ExcelSheet sheet = sheets.get(sheetName);
        if (sheet == null) {
            return null;
        }
        return sheet.getProposedTitles();
    }

    // Aggregate PV per column
//	public String toCsvString() {
//
//		final Map<String, StringBuffer> sbMap = new HashMap<>();
//		ExcelSheet.pvKeyList.forEach(name->sbMap.put(name, new StringBuffer()));
//		StringBuffer namesSb = new StringBuffer();
//		StringBuffer fullNamesSb = new StringBuffer();
//
//		boolean first = true;
//		for (final ExcelSheet sheet : sheets.values()) {
//			if (first) {
//				first = false;
//			}
//			else {
//				sbMap.keySet().forEach(n -> sbMap.get(n).append("|"));
//				namesSb.append("|");
//				fullNamesSb.append("|");
//			}
//			sbMap.keySet().forEach(n -> sbMap.get(n).append(sheet.getHistPropertiesString(n)));
//			namesSb.append(sheet.getSheetName());
//			fullNamesSb.append(sheet.getFullSheetName());
//		}
//
//		// Use pvKeyList to assure order of strings
//		final String result = ExcelSheet.pvKeyList.stream().map(n->sbMap.get(n).toString()).collect(Collectors.joining(","));
//		return namesSb.toString()+",\""+fullNamesSb.toString()+"\","+result;
//	}

    //	public ExcelSimilarity findSimilarity(final ExcelWorkbook other) throws IllegalStateException {
//		return findSimilarity(this, other, false);
//	}

//	private static ExcelSimilarity findSimilarity(final ExcelWorkbook book1, final ExcelWorkbook book2,
//			final boolean fullHeadingMatch) throws IllegalStateException {
//		if (book1 == null || book2 == null)
//			return null;
//
//		final Map<String, ExcelSheet> sheetsA = new LinkedHashMap<>(book1.sheets);
//		final Map<String, ExcelSheet> sheetsB = new LinkedHashMap<>(book2.sheets);
//
//		final ExcelSimilarity excelSimilarity =
//				new ExcelSimilarity(book1.getNumOfCells(), book2.getNumOfCells(),
//						book1.getNumOfFormulas(), book2.getNumOfFormulas(),
//						book1.getNumOfBlanks(), book2.getNumOfBlanks(),
//						book1.getNumOfSheets(), book2.getNumOfSheets());
//		/*
//		 * Look for sheets with matching names (exact match for now)
//		 * Compare sheets with matching names and then remove them from the sheets lists to allow iteration on remaining sheets.
//		 */
//		final Map<String, String> book1SheetNamesMap = book1.getSheetFullNamesMap();
//		final Map<String, String> book2SheetNamesMap = book2.getSheetFullNamesMap();
//		final Iterator<String> book1SheetNamesIter = book1SheetNamesMap.keySet().iterator();
//		while (book1SheetNamesIter.hasNext()) {
//			final String fullNameA = book1SheetNamesIter.next();
//			final String sheetNameB = book2SheetNamesMap.get(fullNameA);
//			if (sheetNameB != null) {
//				final String sheetNameA = book1SheetNamesMap.get(fullNameA);
//				final ExcelSheet sheetA = sheetsA.get(sheetNameA);
//				final ExcelSheet sheetB = sheetsB.get(sheetNameB);
//				final ExcelSheetSimilarity sheetSimilarity = new ExcelSheetSimilarity(sheetA, sheetB);
//				// If sheets seem strangers, then ignore the named matched sheets.
//				// TODO: take the threshold out to config file
//				if (sheetSimilarity.getCombinedCountSimilarityRate() > 0.5 ||
//						sheetSimilarity.getStyleSimilarityRate() > 0.5 ||
//						sheetNameA.equals(sheetNameB)) {
//					excelSimilarity.addSheetsSimilarity(sheetNameA+","+sheetNameB, sheetSimilarity);
//					// remove the matched sheets from the sheets-lists
//					sheetsA.remove(sheetNameA);
//					sheetsB.remove(sheetNameB);
//				}
//			}
//		}	// Iterating book1SheetNamesIter
//
//		// Perform sheet by sheet comparison
//		// 	Naive approach: direct positional match - no attempt for skip or look for best match
//		final Iterator<String> i1 = sheetsA.keySet().iterator();
//		final Iterator<String> i2 = sheetsB.keySet().iterator();
//		boolean b1 = i1.hasNext();
//		boolean b2 = i2.hasNext();
//		while (b1 && b2) {
//			final String nameA = i1.next();
//			final String nameB = i2.next();
//			final ExcelSheet sheetA = sheetsA.get(nameA);
//			final ExcelSheet sheetB = sheetsB.get(nameB);
//
//			final ExcelSheetSimilarity sheetSimilarity = new ExcelSheetSimilarity(sheetA, sheetB);
//			excelSimilarity.addSheetsSimilarity(nameA+","+nameB, sheetSimilarity);
//
//			b1 = i1.hasNext();
//			b2 = i2.hasNext();
//		}
//		excelSimilarity.finalizeGlobalMetrics();
//		excelSimilarity.setProposedTitlesA(book1.getProposedTitles());
//		excelSimilarity.setProposedTitlesB(book2.getProposedTitles());
//
//		return excelSimilarity;
//	}

}
