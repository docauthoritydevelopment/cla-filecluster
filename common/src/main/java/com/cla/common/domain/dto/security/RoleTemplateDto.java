package com.cla.common.domain.dto.security;

import com.cla.connector.domain.dto.JsonSerislizableVisible;

import java.util.Set;

/**
 * Role template holds the individual roles connected to a composite role
 * Created by: yael
 * Created on: 11/30/2017
 */
public class RoleTemplateDto implements JsonSerislizableVisible {
    protected Long id;

    protected String name;

    protected String description;

    protected String displayName;

    private RoleTemplateType templateType;

    private Set<SystemRoleDto> systemRoleDtos;

    private boolean deleted;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public Set<SystemRoleDto> getSystemRoleDtos() {
        return systemRoleDtos;
    }

    public void setSystemRoleDtos(Set<SystemRoleDto> systemRoleDtos) {
        this.systemRoleDtos = systemRoleDtos;
    }

    public RoleTemplateType getTemplateType() {
        return templateType;
    }

    public void setTemplateType(RoleTemplateType templateType) {
        this.templateType = templateType;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    @Override
    public String toString() {
        return "RoleTemplateDto{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", displayName='" + displayName + '\'' +
                ", templateType='" + templateType + '\'' +
                ", systemRoleDtos=" + systemRoleDtos +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RoleTemplateDto that = (RoleTemplateDto) o;

        if (id != null ? !id.equals(that.getId()) : that.getId() != null) return false;
        return name != null ? name.equals(that.getName()) : that.getName() == null;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }
}
