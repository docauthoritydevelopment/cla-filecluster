package com.cla.common.domain.dto.messages;

import com.cla.common.constants.CatFileFieldType;
import com.cla.connector.domain.dto.media.MediaConnectionDetailsDto;
import com.cla.connector.domain.dto.messages.ScanTypeSpecification;

/**
 *
 * Created by vladi on 2/22/2017.
 */
public class IngestTaskParameters extends TaskParameters implements MediaConnectionDetailsProvider {

    public static final String METADATA_ONLY_PARAM = "metadataOnly";
    public static final String FIRST_SCAN = "firstScan";
    public static final String CONTENT_ID = CatFileFieldType.CONTENT_ID.getSolrName();

    private boolean ingestContents = true;
    private long jobId = 0;
    private ScanTypeSpecification packageScanTypeSpecification;
    private Long allocatedContentId;
    private MediaConnectionDetailsDto mediaConnectionDetailsDto;
    private boolean firstScan = true;
    private String username;
    private String password;
    private boolean getReverseDictionary = false;
    private Long jobStateIdentifier;

    public static IngestTaskParameters create() {
        return new IngestTaskParameters();
    }

    private IngestTaskParameters() {
    }

    public boolean getIngestContents() {
        return ingestContents;
    }

    public IngestTaskParameters setIngestContents(boolean ingestContents) {
        this.ingestContents = ingestContents;
        return this;
    }

    public IngestTaskParameters setJobId(long jobId) {
        this.jobId = jobId;
        return this;
    }

    public long getJobId() {
        return jobId;
    }

    public String getUsername() {
        return username;
    }

    public IngestTaskParameters setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public IngestTaskParameters setPassword(String password) {
        this.password = password;
        return this;
    }
    public Long getAllocatedContentId() {
        return allocatedContentId;
    }

    public IngestTaskParameters setAllocatedContentId(Long allocatedContentId) {
        this.allocatedContentId = allocatedContentId;
        return this;
    }

    public MediaConnectionDetailsDto getMediaConnectionDetailsDto() {
        return mediaConnectionDetailsDto;
    }

    public IngestTaskParameters setMediaConnectionDetailsDto(MediaConnectionDetailsDto mediaConnectionDetailsDto) {
        this.mediaConnectionDetailsDto = mediaConnectionDetailsDto;
        return this;
    }

    public IngestTaskParameters setPackageScanTypeSpecification(ScanTypeSpecification packageScanTypeSpecification) {
        this.packageScanTypeSpecification = packageScanTypeSpecification;
        return this;
    }

    public ScanTypeSpecification getPackageScanTypeSpecification() {
        return packageScanTypeSpecification;
    }

    public boolean isFirstScan() {
        return firstScan;
    }

    public IngestTaskParameters setFirstScan(boolean firstScan) {
        this.firstScan = firstScan;
        return this;
    }

    public boolean isGetReverseDictionary() {
        return getReverseDictionary;
    }

    public IngestTaskParameters setGetReverseDictionary(boolean getReverseDictionary) {
        this.getReverseDictionary = getReverseDictionary;
        return this;
    }

    public Long getJobStateIdentifier() {
        return jobStateIdentifier;
    }

    public IngestTaskParameters setJobStateIdentifier(Long jobStateIdentifier) {
        this.jobStateIdentifier = jobStateIdentifier;
        return this;
    }

    @Override
	public String toString() {
		return "IngestTaskParameters{" +
				"ingestContents=" + ingestContents +
				", jobId=" + jobId +
				", packageScanTypeSpecification=" + packageScanTypeSpecification +
				", mediaConnectionDetailsDto=" + getMediaConnectionDetailsDto() +
				", firstScan=" + firstScan +
				", username='" + username + '\'' +
				", password='" + password + '\'' +
				", mediaEntityId='" + getMediaEntityId()+ '\'' +
				", getReverseDictionary=" + getReverseDictionary +
				'}';
	}
}
