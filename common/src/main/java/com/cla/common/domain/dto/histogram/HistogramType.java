package com.cla.common.domain.dto.histogram;

/**
 * Created by uri on 05/02/2017.
 */
public enum HistogramType {
    NORMAL,
    MARKER,
    ORDERED
}
