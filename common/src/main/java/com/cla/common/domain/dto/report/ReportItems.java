package com.cla.common.domain.dto.report;

/**
 * Created by uri on 08/10/2015.
 */
public interface ReportItems {

    String SCANNED_FILES = "scanned files";
    String ANALYZED_FILES = "analyzed files";
    String INGESTED_FILES = "ingested files";
    String SKIPPED_FILES = "skipped files";
    String ERROR_FILES = "files with ingestion errors";
    String SENSITIVE_DATA_FILES = "sensitive data files";
    String SENSITIVE_DATA_GROUPS = "sensitive data groups";
    String DISTINCT_OWNERS = "distinct owners";
    String DISTINCT_ACL_READ = "distinct acl read";
    String DISTINCT_ACL_WRITE = "distinct acl write";

    String SCANNED_ROOT_FOLDERS = "scanned root folders";
    String SCAN_USER_ACCOUNT = "scan user account";

    String ENTITY_MANAGEMENT_DETAILS = "entity management details";

    String FILE_COUNT = "file count";

    String AGG_SIZE = "aggregated processed size";

	/**
	 * number of total files that were ingested.
	 */
    String PROCESSED_FILES_COUNT = "processed files";  // found in Solr
    /**
     * Size of total files that were ingested.
     */
    String AGG_SIZE_DOCUMENTS = "documents aggregated processed size";
    /**
     * Count of total files that are marked as deleted.
     */
    String AGG_COUNT_DELETED = "documents aggregated deleted count";
    /**
     * Size of total files that were deleted.
     */
    String AGG_SIZE_DELETED = "documents aggregated deleted size";
	/**
	 * number of total files that can be analyzed: word, excel, document-pdf. excluding scanned pdf and files in various error states.
	 * TODO [12.16.2018]: currently files with state ERROR are included because the Front-End have a hardcoded formula scope.analysisMaxFilesCount.counter - scope.ingestionErrorsCount.counter,
	 * when they remove the  'minus errors counter'  from the  analyzedFilesPercentStr calculation, we will exclude it from the filter.
	 */
    String PROCESSED_FILES_COUNT_DOCUMENTS = "documents files with content";  // found in Solr

    String ERROR_DOCUMENTS = "documents with errors";

    String GROUP_SIZE = "group size";
    String FILE_TYPE = "file type";
    String LAST_MODIFY_DATE = "last modify date";

    String PROCESSED_EXCEL_FILES_COUNT = "processed excel files";  // found in Solr
    String EXCEL_SHEETS_AVG = "Excel_sheets_average";
    String EXCEL_SHEETS_MAX = "Excel_sheets_max";
    String EXCEL_CELLS_AVG = "Excel_cells_average";
    String EXCEL_CELLS_MAX = "Excel_cells_max";
}
