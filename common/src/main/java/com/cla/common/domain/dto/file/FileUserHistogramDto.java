package com.cla.common.domain.dto.file;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.cla.common.domain.dto.FileUserType;

@Deprecated
@SuppressWarnings("serial")
public class FileUserHistogramDto implements Serializable {
	private final List<FileUserHistogramItemDto> histogram;
	private final FileUserType userType;

	public FileUserHistogramDto(final FileUserType userType) {
		histogram = new ArrayList<>();
		this.userType = userType;
	}
	
	public FileUserHistogramDto(final FileUserType itemType, final List<Object[]> queryResults) {
		histogram = new ArrayList<>(queryResults.size());
		this.userType = itemType;
		try {
			boolean checked = false;
			for (Object[] item:queryResults) {
				if (checked || (item[0] instanceof String && 
						(item[1] instanceof Integer || item[1] instanceof Long))) {
					checked = true;
					histogram.add(new FileUserHistogramItemDto((String)item[0] , ((Long)item[1]).intValue()));
				}
				else {
					throw new RuntimeException("Wrong result set type: ("+item[0].getClass().toString() + ", "+item[1].getClass().toString() 
							+ "). expecting (String, Integer/Long)");
				}
			}
		} catch (Exception e) {
			throw new RuntimeException("Unexpected error during result set conversion.", e);
		}
	}
	
	public int addItem(String user, int count) {
		histogram.add(new FileUserHistogramItemDto(user, count));
		return histogram.size();
	}

	public List<FileUserHistogramItemDto> getHistogram() {
		return histogram;
	}

	public FileUserType getUserType() {
		return userType;
	}

	@Override
	public String toString() {
		return "{userType="+userType.name()+" ,histogram=" + histogram + "}";
	}
	
}
