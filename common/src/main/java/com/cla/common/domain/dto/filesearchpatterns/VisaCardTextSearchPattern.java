package com.cla.common.domain.dto.filesearchpatterns;


public class VisaCardTextSearchPattern extends GenericCreditCardTextSearchPattern {

	public static final String VISA_PATERN = "\\b4\\d{3}([ -]?)\\d{4}\\1\\d{4}\\1\\d{4}\\b";

	public VisaCardTextSearchPattern() {}

	public VisaCardTextSearchPattern(final Integer id, final String name) {
		super(id, name, VISA_PATERN);
	}
}
