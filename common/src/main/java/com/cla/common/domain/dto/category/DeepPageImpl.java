package com.cla.common.domain.dto.category;

import java.util.List;

/**
 * Created by uri on 07/01/2016.
 */
public class DeepPageImpl<T> {

    private String cursorMark;

    private int pageSize;

    private List<T> content;
    private long totalCount;

    public DeepPageImpl(String cursorMark, int pageSize, List<T> content, long totalCount) {
        this.cursorMark = cursorMark;
        this.pageSize = pageSize;
        this.content = content;
        this.totalCount = totalCount;
    }

    public String getCursorMark() {
        return cursorMark;
    }

    public void setCursorMark(String cursorMark) {
        this.cursorMark = cursorMark;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public List<T> getContent() {
        return content;
    }

    public void setContent(List<T> content) {
        this.content = content;
    }

    public long getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(long totalCount) {
        this.totalCount = totalCount;
    }
}
