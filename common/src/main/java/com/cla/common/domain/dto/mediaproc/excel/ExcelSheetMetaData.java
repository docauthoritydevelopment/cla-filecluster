package com.cla.common.domain.dto.mediaproc.excel;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.common.collect.Lists;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ExcelSheetMetaData implements Serializable {

	private String sheetName;
	private String fullSheetName;
	private int numOfCells;
	private int numOfFormulas;
	private int numOfBlanks;
	private int numOfErrors;
	private int cellsSkipped;

	private final List<String> proposedTitles = Lists.newArrayList();

	public String getSheetName() {
		return sheetName;
	}
	public void setSheetName(final String sheetName) {
		this.sheetName = sheetName;
	}
	public String getFullSheetName() {
		return fullSheetName;
	}
	public void setFullSheetName(final String fullSheetName) {
		this.fullSheetName = fullSheetName;
	}
	public int getNumOfCells() {
		return numOfCells;
	}
	public void setNumOfCells(final int numOfCells) {
		this.numOfCells = numOfCells;
	}
	public int getNumOfFormulas() {
		return numOfFormulas;
	}
	public void setNumOfFormulas(final int numOfFormulas) {
		this.numOfFormulas = numOfFormulas;
	}
	public int getNumOfBlanks() {
		return numOfBlanks;
	}
	public void setNumOfBlanks(final int numOfBlanks) {
		this.numOfBlanks = numOfBlanks;
	}
	public int getNumOfErrors() {
		return numOfErrors;
	}
	public void setNumOfErrors(final int numOfErrors) {
		this.numOfErrors = numOfErrors;
	}
	public int getCellsSkipped() {
		return cellsSkipped;
	}
	public void setCellsSkipped(final int cellsSkipped) {
		this.cellsSkipped = cellsSkipped;
	}
	public List<String> getProposedTitles() {
		return proposedTitles;
	}
	public void addProposedTitle(final String title) {
		proposedTitles.add(title);
	}
	public void addProposedTitles(final Collection<String> titles) {
		proposedTitles.addAll(titles);
	}

	@JsonIgnore
	public String getStringTitle() {
		return "sheetName,numCells,numFormulas";
	}

	@Override
	@JsonIgnore
	public String toString() {
		return ""+sheetName+","+fullSheetName+","+numOfCells+","+numOfFormulas;
	}
}
