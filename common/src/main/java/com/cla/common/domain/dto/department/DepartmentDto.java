package com.cla.common.domain.dto.department;

import com.cla.common.domain.dto.AssociableEntityDto;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.util.List;


/**
 * Created by ginat on 11/02/2019.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DepartmentDto implements Serializable, AssociableEntityDto {

    public static final String ID_PREFIX = "dept.";
    public final static String SEPARATOR = "\\";

    private Long id;

    private String name;

    private Long parentId;

    private boolean deleted;

    private List<Long> parents;

    private Boolean implicit;

    private String uniqueName;

    private String description;

    private String path;

    private List<String> childrenName;

    public DepartmentDto() {}

    public DepartmentDto(String name) { this.name = name; }

    public DepartmentDto(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public DepartmentDto(DepartmentDto other) {
        this.id = other.id;
        this.name = other.name;
        this.parentId = other.parentId;
        this.deleted = other.deleted;
        this.parents = other.parents;
        this.uniqueName = other.uniqueName;
        this.description = other.description;
        this.path = other.path;
        this.childrenName = other.childrenName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) { this.id = id; }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public List<Long> getParents() {
        return parents;
    }

    public void setParents(List<Long> parents) {
        this.parents = parents;
    }

    public Boolean isImplicit() {
        return implicit;
    }

    public void setImplicit(Boolean implicit) {
        this.implicit = implicit;
    }

    public String getUniqueName() {
        return uniqueName;
    }

    public void setUniqueName(String uniqueName) {
        this.uniqueName = uniqueName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @JsonIgnore
    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public List<String> getChildrenName() {
        return childrenName;
    }

    public void setChildrenName(List<String> childrenName) {
        this.childrenName = childrenName;
    }

    public String getFullName() {
        return path == null ? name : path + SEPARATOR + name;
    }

    @Override
    public String toString() {
        return "DepartmentDto{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", parentId=" + parentId +
                ", deleted=" + deleted +
                ", implicit=" + implicit +
                ", uniqueName='" + uniqueName + '\'' +
                '}';
    }

    @Override
    public boolean isIdMatch(Object idToCheck) {
        return idToCheck != null && idToCheck instanceof Long && idToCheck.equals(id);
    }

    @Override
    public String createIdentifierString() {
        return DepartmentDto.ID_PREFIX + String.valueOf(id);
    }
}
