package com.cla.common.domain.dto.filetag;

/**
 * Created by uri on 26/08/2015.
 */
public enum FileTagSource {
    FILE,FOLDER_EXPLICIT,FOLDER_IMPLICIT,BUSINESS_ID
}
