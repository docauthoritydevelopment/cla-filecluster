package com.cla.common.domain.dto.filesearchpatterns;

import java.util.function.Predicate;

public class EmailTextSearchPattern extends TextSearchPatternImpl {

	public static final String EMAIL_PATTERN = "\\b[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})\\b";

	public EmailTextSearchPattern(final Integer id, final String name) {
		super(id, name, EMAIL_PATTERN);
	}

	public EmailTextSearchPattern() {
	}

	@Override
	public Predicate<String> getPredicate() {
		return (str) -> str.length() < 100;
	}

}
