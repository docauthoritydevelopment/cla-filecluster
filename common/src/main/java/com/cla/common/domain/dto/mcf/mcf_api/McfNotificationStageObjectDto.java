package com.cla.common.domain.dto.mcf.mcf_api;

import java.io.Serializable;

/**
 * Created by uri on 18/07/2016.
 */
public class McfNotificationStageObjectDto implements Serializable {

    /**"stage_id"	The unique identifier for the notification stage */
    String stage_id;
     /**"stage_connectionname"	The connection name for the notification stage*/
    String connectionname;
     /**"stage_description"	A description of the notification stage*/
    String stage_description;
     /**"stage_specification"	The specification string for the notification stage*/

    String stage_specification;


}
