package com.cla.common.domain.dto.file;

import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.file.ItemType;
import com.cla.connector.domain.dto.media.EmailAddress;
import com.cla.connector.domain.dto.media.MailPropertiesDto;

import java.util.Date;
import java.util.List;

public class MailEntryPropertiesDto extends ClaFilePropertiesDto implements MailPropertiesDto {

    private EmailAddress sender;
    private List<EmailAddress> recipients;
    private Date sentDate;
    private ItemType itemType;
    private String emailSubject;

    public EmailAddress getSender() {
        return sender;
    }

    public void setSender(EmailAddress sender) {
        this.sender = sender;
    }

    public List<EmailAddress> getRecipients() {
        return recipients;
    }

    public void setRecipients(List<EmailAddress> recipients) {
        this.recipients = recipients;
    }

    public Date getSentDate() {
        return sentDate;
    }

    public void setSentDate(Date sentDate) {
        this.sentDate = sentDate;
    }

    public ItemType getItemType() {
        return itemType;
    }

    public void setItemType(ItemType itemType) {
        this.itemType = itemType;
    }

    public String getEmailSubject() {
        return emailSubject;
    }

    public void setEmailSubject(String emailSubject) {
        this.emailSubject = emailSubject;
    }
}
