package com.cla.common.domain.dto.action;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by uri on 25/01/2016.
 */
public enum ActionClass {

    ACCESS_MANAGEMENT
    , BUSINESS_PROCESS
    , TAGGING
    , DLP
    , ENCRYPTION
    , RETENTION
    , STORAGE_POLICY
    , REPORTING
}
