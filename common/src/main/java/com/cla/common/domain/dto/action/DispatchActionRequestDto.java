package com.cla.common.domain.dto.action;

import java.io.Serializable;
import java.util.Properties;

/**
 *
 * Created by uri on 22/02/2016.
 */
public class DispatchActionRequestDto implements Serializable {
    private long fileId;
    private String baseName;
    private String fullPath;

    private String actionDefinitionId;
    private Properties parameters;

    public long getFileId() {
        return fileId;
    }

    public void setFileId(long fileId) {
        this.fileId = fileId;
    }

    public String getBaseName() {
        return baseName;
    }

    public void setBaseName(String baseName) {
        this.baseName = baseName;
    }

    public String getFullPath() {
        return fullPath;
    }

    public void setFullPath(String fullPath) {
        this.fullPath = fullPath;
    }

    public String getActionDefinitionId() {
        return actionDefinitionId;
    }

    public void setActionDefinitionId(String actionDefinitionId) {
        this.actionDefinitionId = actionDefinitionId;
    }

    public Properties getParameters() {
        return parameters;
    }

    public void setParameters(Properties parameters) {
        this.parameters = parameters;
    }

}
