package com.cla.common.domain.dto;

public class AggregationCountItemDTO<T extends Object> {
    private T item;
    private long count;
    private long count2;
    private long subtreeCount;
    private long subtreeCountUnfiltered;
    private double totalSize;

    public AggregationCountItemDTO() {
    }

    public AggregationCountItemDTO(final T item, final long count) {
        this(item, count, -1);
    }

    public AggregationCountItemDTO(final T item, final long count, final long count2) {
        this(item, count, count2, -1);
    }

    public AggregationCountItemDTO(T item, long count, long count2, long subtreeCount) {
        this(item, count, count2, subtreeCount, -1);
    }

    public AggregationCountItemDTO(T item, long count, long count2, long subtreeCount, long subtreeCountUnfiltered) {
        this.item = item;
        this.count = count;
        this.count2 = count2;
        this.subtreeCount = subtreeCount;
        this.subtreeCountUnfiltered = subtreeCountUnfiltered;
    }

    public T getItem() {
        return item;
    }

    public long getCount() {
        return count;
    }

    public long getCount2() {
        return count2;
    }

    public void setCount2(long count2) {
        this.count2 = count2;
    }

    public void incrementCount2(long size) {
        if (count2 < 0) {
            count2 = size;
        } else {
            count2 += size;
        }
    }

    public void incrementCount(long size) {
        count += size;
    }

    public void incrementTotalSize(double size) {
        totalSize += size;
    }

    public double getTotalSize() {
        return totalSize;
    }

    public void setTotalSize(double totalSize) {
        this.totalSize = totalSize;
    }

    public long getSubtreeCount() {
        return subtreeCount;
    }

    public void setSubtreeCount(int subtreeCount) {
        this.subtreeCount = subtreeCount;
    }

    public void incrementSubtreeCount(long size) {
        if (subtreeCount < 0) {
            subtreeCount = size;
        } else {
            subtreeCount += size;
        }
    }

    public long getSubtreeCountUnfiltered() {
        return subtreeCountUnfiltered;
    }

    public void setSubtreeCountUnfiltered(int subtreeCountUnfiltered) {
        this.subtreeCountUnfiltered = subtreeCountUnfiltered;
    }

    public void incrementSubtreeCountUnfiltered(long size) {
        if (subtreeCountUnfiltered < 0) {
            subtreeCountUnfiltered = size;
        } else {
            subtreeCountUnfiltered += size;
        }
    }

    @Override
    public String toString() {
        return "{item=" + item.toString() +
                ", count=" + count +
                (count2 == (-1) ? "" : (", count2=" + count2)) +
                (subtreeCount == (-1) ? "" : (", subtreeCount=" + subtreeCount)) +
                (subtreeCountUnfiltered == (-1) ? "" : (", subtreeCountUnfiltered=" + subtreeCountUnfiltered)) +
                "}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AggregationCountItemDTO<?> that = (AggregationCountItemDTO<?>) o;

        return item.equals(that.item);

    }

    @Override
    public int hashCode() {
        return item.hashCode();
    }
}
