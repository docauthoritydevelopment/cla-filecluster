package com.cla.common.domain.dto.report;

public enum ChartQueryType {
    DEFAULT,
    PERIOD,
    CATEGORY
}
