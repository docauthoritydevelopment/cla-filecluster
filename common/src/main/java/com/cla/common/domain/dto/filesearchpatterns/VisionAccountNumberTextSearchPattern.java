package com.cla.common.domain.dto.filesearchpatterns;

import java.util.function.Predicate;

/**
 * Created by: itay
 * Created on: 25/06/2019
 *
 * This time it is for a ‘Vision Account Number’ which is 14 digits with a mod 11 weighted check. The specification is attached.
 * Sample valid account numbers are:  10100100001147 and 10100100001155
 *
 */
public class VisionAccountNumberTextSearchPattern extends TextSearchPatternImpl {

	public static final String VAN_PATERN = "\\b\\d{14}\\b";

	public VisionAccountNumberTextSearchPattern() {}

	public VisionAccountNumberTextSearchPattern(final Integer id, final String name) {
		super(id, name, VAN_PATERN);
	}

	@Override
	public Predicate<String> getPredicate() {
		return this::checkMod11Weighted;
	}

	public static final int[] VISION_ACCOUNT_NUMBER_WEIGHTING = new int[]{2,7,6,5,4,3,2,7,6,5,4,3,2,1};

	private boolean checkMod11Weighted(final String number) {
		final int[] digits = number.chars().toArray();
		if (digits.length > VISION_ACCOUNT_NUMBER_WEIGHTING.length) {
			return false;
		}
		int sum = 0;
		final int length = digits.length;
		for (int i = 0; i < length; i++) {
			int digit = digits[i] - '0';
			int weight = digit * VISION_ACCOUNT_NUMBER_WEIGHTING[i];
			sum= sum + weight;
		}
		return sum % 11 == 0;
	}
}
