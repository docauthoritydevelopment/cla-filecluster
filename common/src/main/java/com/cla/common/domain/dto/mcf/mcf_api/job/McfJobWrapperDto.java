package com.cla.common.domain.dto.mcf.mcf_api.job;

import com.google.common.collect.Lists;

import java.io.Serializable;
import java.util.List;

/**
 * Created by uri on 18/07/2016.
 */
public class McfJobWrapperDto implements Serializable {

    List<McfJobObjectDto> job;

    public McfJobWrapperDto(McfJobObjectDto newJob) {
        this.job = Lists.newArrayList(newJob);
    }

    public McfJobWrapperDto() {
    }

    public List<McfJobObjectDto> getJob() {
        return job;
    }

    public void setJob(List<McfJobObjectDto> job) {
        this.job = job;
    }
}
