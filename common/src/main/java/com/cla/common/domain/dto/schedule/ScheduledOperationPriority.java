package com.cla.common.domain.dto.schedule;

public enum ScheduledOperationPriority {
    HIGH(0),
    MEDIUM(1),
    LOW(2)
    ;

    private int value;

    ScheduledOperationPriority(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
