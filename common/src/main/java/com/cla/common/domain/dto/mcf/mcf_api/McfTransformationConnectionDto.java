package com.cla.common.domain.dto.mcf.mcf_api;

import java.util.Map;

/**
 * Created by uri on 07/07/2016.
 */
public class McfTransformationConnectionDto extends McfBaseObjectDto {

    String description;

    String class_name;

    Boolean isnew;

    int max_connections;

    Map<String,McfConfigurationParameterDto[]> configuration;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getClass_name() {
        return class_name;
    }

    public void setClass_name(String class_name) {
        this.class_name = class_name;
    }

    public int getMax_connections() {
        return max_connections;
    }

    public void setMax_connections(int max_connections) {
        this.max_connections = max_connections;
    }

    public Map<String, McfConfigurationParameterDto[]> getConfiguration() {
        return configuration;
    }

    public void setConfiguration(Map<String, McfConfigurationParameterDto[]> configuration) {
        this.configuration = configuration;
    }

    public Boolean getIsnew() {
        return isnew;
    }

    public void setIsnew(Boolean isnew) {
        this.isnew = isnew;
    }
}
