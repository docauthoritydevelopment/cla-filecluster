package com.cla.common.domain.dto.scope;

/**
 * Represent an entity within a scope
 * @param <S>
 */
public interface ScopedEntity<S extends Scope> {
    S getScope();
}
