package com.cla.common.domain.dto.messages;

import com.cla.common.domain.dto.FileMetadata;
import com.cla.common.domain.dto.extraction.SearchPatternCountDto;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.error.ProcessingErrorType;
import com.cla.connector.domain.dto.messages.PhaseMessagePayload;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by vladi on 2/21/2017.
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "@class")
public abstract class IngestMessagePayload<T extends FileMetadata> extends ExtractionResult implements PhaseMessagePayload, Serializable {

    protected T metadata;
    protected boolean metadataOnly = false;
    protected ClaFilePropertiesDto claFileProperties;
    protected Map<Long, String> reverseDictionary;

    public IngestMessagePayload() {

    }

    public IngestMessagePayload(IngestMessagePayload<T> other) {
        super(other);
        this.metadata = other.metadata;
        this.claFileProperties = other.claFileProperties;
    }

    @Override
    @JsonIgnore
    public ProcessingErrorType calculateProcessErrorType() {
        return getErrorType();
    }

    public ClaFilePropertiesDto getClaFileProperties() {
        return claFileProperties;
    }

    public abstract List<String> getCombinedProposedTitles();

    public abstract String getCsvValue();

    public void setClaFileProperties(ClaFilePropertiesDto claFileProperties) {
        this.claFileProperties = claFileProperties;
    }

    public boolean isMetadataOnly() {
        return metadataOnly;
    }

    public void setMetadataOnly(boolean metadataOnly) {
        this.metadataOnly = metadataOnly;
    }

    @JsonProperty("metadata")
    public T getMetadata() {
        return metadata;
    }

    public void setMetadata(T metadata) {
        this.metadata = metadata;
    }

    public Map<Long, String> getReverseDictionary() {
        return reverseDictionary;
    }

    public void setReverseDictionary(Map<Long, String> reverseDictionary) {
        this.reverseDictionary = reverseDictionary;
    }
}
