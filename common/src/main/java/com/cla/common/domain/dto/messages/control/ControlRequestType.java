package com.cla.common.domain.dto.messages.control;

/**
 * Types of control messages sent to Media Processors
 * Created by uri on 16-May-17.
 */
public enum ControlRequestType {
    FETCH_LOGS,
    JOBS_STATES,
    HANDSHAKE, //(ServerHandShakeRequestPayload.class);
    STATE_CHANGE, //(ServerHandShakeRequestPayload.class) - indicate change isActive change not part of handshake
    LIMIT_REACHED,
    REFRESH_CONNECTOR,
    SEARCH_PATTERNS
}
