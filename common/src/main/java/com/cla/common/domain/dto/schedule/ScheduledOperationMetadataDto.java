package com.cla.common.domain.dto.schedule;

import com.cla.common.predicate.KeyValuePredicate;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ScheduledOperationMetadataDto {

    private Long id;

    private ScheduledOperationType operationType;

    private int step;

    private Map<String,String> opData;

    private Long createTime;

    private Long startTime;

    private Long updateTime;

    private boolean isUserOperation;

    private ScheduledOperationPriority priority;

    private Integer retryLeft;

    private KeyValuePredicate opCondition;

    private Long ownerId;

    private String description;

    private ScheduledOperationState state;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ScheduledOperationType getOperationType() {
        return operationType;
    }

    public void setOperationType(ScheduledOperationType operationType) {
        this.operationType = operationType;
    }

    public int getStep() {
        return step;
    }

    public void setStep(int step) {
        this.step = step;
    }

    public Map<String, String> getOpData() {
        return opData;
    }

    public void setOpData(Map<String, String> opData) {
        this.opData = opData;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public boolean isUserOperation() {
        return isUserOperation;
    }

    public void setUserOperation(boolean userOperation) {
        isUserOperation = userOperation;
    }

    public ScheduledOperationPriority getPriority() {
        return priority;
    }

    public void setPriority(ScheduledOperationPriority priority) {
        this.priority = priority;
    }

    public Integer getRetryLeft() {
        return retryLeft;
    }

    public void setRetryLeft(Integer retryLeft) {
        this.retryLeft = retryLeft;
    }

    public KeyValuePredicate getOpCondition() {
        return opCondition;
    }

    public void setOpCondition(KeyValuePredicate opCondition) {
        this.opCondition = opCondition;
    }

    public Long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(Long ownerId) {
        this.ownerId = ownerId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ScheduledOperationState getState() {
        return state;
    }

    public void setState(ScheduledOperationState state) {
        this.state = state;
    }

    public Long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Long updateTime) {
        this.updateTime = updateTime;
    }
}
