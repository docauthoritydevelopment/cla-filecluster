package com.cla.common.domain.dto;

import com.cla.connector.domain.dto.JsonSerislizableVisible;
import com.fasterxml.jackson.annotation.JsonProperty;

public class FacetFieldEntry implements JsonSerislizableVisible {
	@JsonProperty
	private String field;
	
	@JsonProperty
	private String value;
	
	@JsonProperty
	private long count;

	public FacetFieldEntry(final String field,final String value, final long count) {
		this.field=field;
		this.value = value;
		this.count = count;
	}

	public FacetFieldEntry() {
	}

	@Override
	public String toString() {
		return "FacetFieldEntry:{field=" + field + ", value=" + value + ", count=" + count + "}";
	}

}
