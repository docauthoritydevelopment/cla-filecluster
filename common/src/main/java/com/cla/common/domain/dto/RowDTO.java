package com.cla.common.domain.dto;

import java.io.Serializable;
import java.util.Arrays;
import java.util.stream.Stream;

public class RowDTO implements Serializable {
	private String[] fieldsValues;
	private int lineNumber;

	public RowDTO() { // for qa
	}

	public RowDTO(final Object[] fieldsValues, final int lineNumber) {
		this.fieldsValues = Stream.of(fieldsValues).map(o->o==null?null:o.toString()).toArray(String[]::new);
		this.lineNumber = lineNumber;
	}

	public String[] getFieldsValues() {
		return fieldsValues;
	}

	public int getLineNumber() {
		return lineNumber;
	}

	@Override
	public String toString() {
		return "RowDTO [fieldsValues=" + Arrays.toString(fieldsValues) + ", lineNumber=" + lineNumber + "]";
	}

	public void setFieldsValues(String[] fieldsValues) {
		this.fieldsValues = fieldsValues;
	}

	public void setLineNumber(int lineNumber) {
		this.lineNumber = lineNumber;
	}
}
