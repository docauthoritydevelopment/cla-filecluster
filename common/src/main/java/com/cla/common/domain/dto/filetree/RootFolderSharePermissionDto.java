package com.cla.common.domain.dto.filetree;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.nio.file.attribute.AclEntryType;
import java.util.List;
import java.util.Set;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class RootFolderSharePermissionDto {

    private Long id;

    private Long rootFolderId;

    private String user;

    private AclEntryType aclEntryType;

    private Long mask;

    private Set<String> textPermissions;

    private List<Long> rootFolderIds;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getRootFolderId() {
        return rootFolderId;
    }

    public void setRootFolderId(Long rootFolderId) {
        this.rootFolderId = rootFolderId;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public AclEntryType getAclEntryType() {
        return aclEntryType;
    }

    public void setAclEntryType(AclEntryType aclEntryType) {
        this.aclEntryType = aclEntryType;
    }

    public Long getMask() {
        return mask;
    }

    public void setMask(Long mask) {
        this.mask = mask;
    }

    public Set<String> getTextPermissions() {
        return textPermissions;
    }

    public void setTextPermissions(Set<String> textPermissions) {
        this.textPermissions = textPermissions;
    }

    public List<Long> getRootFolderIds() {
        return rootFolderIds;
    }

    public void setRootFolderIds(List<Long> rootFolderIds) {
        this.rootFolderIds = rootFolderIds;
    }

    @Override
    public String toString() {
        return "RootFolderSharePermissionDto{" +
                "id=" + id +
                ", rootFolderId=" + rootFolderId +
                ", user='" + user + '\'' +
                ", aclEntryType=" + aclEntryType +
                ", mask=" + mask +
                '}';
    }

    // returns identifier for permission regardless of root folder assigned
    public String toItemIdentifier() {
        return this.getUser() + "___" + this.getAclEntryType().name();
    }
}
