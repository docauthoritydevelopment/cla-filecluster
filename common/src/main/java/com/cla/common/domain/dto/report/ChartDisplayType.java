package com.cla.common.domain.dto.report;

/**
 * Created by uri on 09/11/2015.
 */
public enum ChartDisplayType {
    PIE,BAR_HORIZONTAL,BAR_VERTICAL,TREND_BAR,TREND_BAR_STACKED,TREND_LINE
}
