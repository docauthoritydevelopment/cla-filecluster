package com.cla.common.domain.dto.messages.control;

import com.cla.connector.domain.dto.error.ProcessingErrorType;
import com.cla.connector.domain.dto.messages.PhaseMessagePayload;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Payload for control messages
 * Created by uri on 21-May-17.
 */
public class ControlMessagePayload implements PhaseMessagePayload {

    @Override
    @JsonIgnore
    public ProcessingErrorType calculateProcessErrorType() {
        return ProcessingErrorType.ERR_NONE;
    }

}
