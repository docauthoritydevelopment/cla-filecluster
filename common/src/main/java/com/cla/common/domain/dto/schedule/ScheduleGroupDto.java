package com.cla.common.domain.dto.schedule;

import com.cla.connector.domain.dto.JsonSerislizableVisible;

import java.util.List;

/**
 * Created by uri on 02/05/2016.
 */
public class ScheduleGroupDto implements JsonSerislizableVisible {

    private Long id;

    private String name;

    private CrawlerPhaseBehavior analyzePhaseBehavior;

    private List<Long> rootFolderIds;

    private ScheduleConfigDto scheduleConfigDto;

    private Boolean active;

    private String cronTriggerString;

    private Long missedScheduleTimeStamp;

    private String schedulingDescription;

    private Long lastRunId;

    // not part of actual object in repository - for ui display only
    private Long nextPlannedWakeup;

    public Long getLastRunId() {
        return lastRunId;
    }

    public void setLastRunId(Long lastRunId) {
        this.lastRunId = lastRunId;
    }



    public Boolean getActive() {
        return active;
    }

    public ScheduleGroupDto() {
    }

    public ScheduleGroupDto(String name, CrawlerPhaseBehavior analyzePhaseBehavior) {
        this.name = name;
        this.analyzePhaseBehavior = analyzePhaseBehavior;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CrawlerPhaseBehavior getAnalyzePhaseBehavior() {
        return analyzePhaseBehavior;
    }

    public void setAnalyzePhaseBehavior(CrawlerPhaseBehavior analyzePhaseBehavior) {
        this.analyzePhaseBehavior = analyzePhaseBehavior;
    }

    public List<Long> getRootFolderIds() {
        return rootFolderIds;
    }

    public void setRootFolderIds(List<Long> rootFolderIds) {
        this.rootFolderIds = rootFolderIds;
    }

    public String getCronTriggerString() {
        return cronTriggerString;
    }

    public void setCronTriggerString(String cronTriggerString) {
        this.cronTriggerString = cronTriggerString;
    }

    public Long getMissedScheduleTimeStamp() {
        return missedScheduleTimeStamp;
    }

    public void setMissedScheduleTimeStamp(Long missedScheduleTimeStamp) {
        this.missedScheduleTimeStamp = missedScheduleTimeStamp;
    }

    public Boolean isActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public void setSchedulingDescription(String schedulingDescription) {
        this.schedulingDescription = schedulingDescription;
    }

    public String getSchedulingDescription() {
        return schedulingDescription;
    }

    public ScheduleConfigDto getScheduleConfigDto() {
        return scheduleConfigDto;
    }

    public void setScheduleConfigDto(ScheduleConfigDto scheduleConfigDto) {
        this.scheduleConfigDto = scheduleConfigDto;
    }

    public Long getNextPlannedWakeup() {
        return nextPlannedWakeup;
    }

    public void setNextPlannedWakeup(Long nextPlannedWakeup) {
        this.nextPlannedWakeup = nextPlannedWakeup;
    }

    @Override
    public String toString() {
        return "ScheduleGroupDto{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", active=" + active +
                ", cronTriggerString='" + cronTriggerString + '\'' +
                '}';
    }
}
