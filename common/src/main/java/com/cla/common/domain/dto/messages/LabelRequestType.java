package com.cla.common.domain.dto.messages;

/**
 * Created by: yael
 * Created on: 7/15/2019
 */
public enum LabelRequestType {
    READ_LABELS,
    WRITE_LABEL
    ;
}
