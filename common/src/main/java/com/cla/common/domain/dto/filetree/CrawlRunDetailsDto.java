package com.cla.common.domain.dto.filetree;

import com.cla.common.domain.dto.crawler.RunStatus;
import com.cla.common.domain.dto.crawler.RunType;
import com.cla.common.domain.dto.jobmanager.PauseReason;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 *
 * Created by uri on 01/05/2016.
 */
public class CrawlRunDetailsDto implements Serializable {

    private Long id;
    private Long scanStartTime;
    private Long scanEndTime;
    private RunStatus runOutcomeState;
    private Integer numberOfFoldersFound;
    private Long totalProcessedFiles;
    private Long processedPdfFiles;
    private Long processedWordFiles;
    private Long processedExcelFiles;
    private Long processedOtherFiles;
    private String stateString;

    private RunType runType;

    private long newFiles = 0;
    private long updatedContentFiles = 0;
    private long updatedMetadataFiles = 0;
    private long deletedFiles = 0;
    private Long scanErrors;
    private Long scanProcessingErrors;
    private boolean manual;
    private boolean accessible;

    private RootFolderDto rootFolderDto;

    private boolean scanFiles;
    private boolean ingestFiles;
    private List<Long> ingestedRootFolders;
    private List<Long> analyzedRootFolders;
    private boolean analyzeFiles;
    private Long scheduleGroupId;
    private String scheduleGroupName;
    private Boolean hasChildRuns;

    private Long parentRunId;
    private PauseReason pauseReason;
    private Date stopTime;
    private Boolean gracefulStop;

    public Long getParentRunId() {
        return parentRunId;
    }

    public void setParentRunId(Long parentRunId) {
        this.parentRunId = parentRunId;
    }

    public Boolean getHasChildRuns() {
        return hasChildRuns;
    }

    public void setHasChildRuns(Boolean hasChildRuns) {
        this.hasChildRuns = hasChildRuns;
    }

    public Long getScanStartTime() {
        return scanStartTime;
    }

    public void setScanStartTime(Long scanStartTime) {
        this.scanStartTime = scanStartTime;
    }

    public Long getScanEndTime() {
        return scanEndTime;
    }

    public void setScanEndTime(Long scanEndTime) {
        this.scanEndTime = scanEndTime;
    }

    public RunStatus getRunOutcomeState() {
        return runOutcomeState;
    }

    public void setRunOutcomeState(RunStatus runOutcomeState) {
        this.runOutcomeState = runOutcomeState;
    }

    public Integer getNumberOfFoldersFound() {
        return numberOfFoldersFound;
    }

    public void setNumberOfFoldersFound(Integer numberOfFoldersFound) {
        this.numberOfFoldersFound = numberOfFoldersFound;
    }

    public Long getTotalProcessedFiles() {
        return totalProcessedFiles;
    }

    public void setTotalProcessedFiles(Long totalProcessedFiles) {
        this.totalProcessedFiles = totalProcessedFiles;
    }

    public Long getProcessedPdfFiles() {
        return processedPdfFiles;
    }

    public void setProcessedPdfFiles(Long processedPdfFiles) {
        this.processedPdfFiles = processedPdfFiles;
    }

    public Long getProcessedWordFiles() {
        return processedWordFiles;
    }

    public void setProcessedWordFiles(Long processedWordFiles) {
        this.processedWordFiles = processedWordFiles;
    }

    public Long getProcessedExcelFiles() {
        return processedExcelFiles;
    }

    public void setProcessedExcelFiles(Long processedExcelFiles) {
        this.processedExcelFiles = processedExcelFiles;
    }

    public Long getProcessedOtherFiles() {
        return processedOtherFiles;
    }

    public void setProcessedOtherFiles(Long processedOtherFiles) {
        this.processedOtherFiles = processedOtherFiles;
    }

    public Long getScanErrors() {
        return scanErrors;
    }

    public void setScanErrors(Long scanErrors) {
        this.scanErrors = scanErrors;
    }

    public Long getScanProcessingErrors() {
        return scanProcessingErrors;
    }

    public void setScanProcessingErrors(Long scanProcessingErrors) {
        this.scanProcessingErrors = scanProcessingErrors;
    }

    public boolean isAccessible() {
        return accessible;
    }

    public void setAccessible(boolean accessible) {
        this.accessible = accessible;
    }

    public RootFolderDto getRootFolderDto() {
        return rootFolderDto;
    }

    public void setRootFolderDto(RootFolderDto rootFolderDto) {
        this.rootFolderDto = rootFolderDto;
    }

    public void setScanFiles(boolean scanFiles) {
        this.scanFiles = scanFiles;
    }

    public boolean isScanFiles() {
        return scanFiles;
    }

    public void setIngestFiles(boolean ingestFiles) {
        this.ingestFiles = ingestFiles;
    }

    public boolean isIngestFiles() {
        return ingestFiles;
    }

    public List<Long> getIngestedRootFolders() {
        return ingestedRootFolders;
    }

    public void setIngestedRootFolders(List<Long> ingestedRootFolders) {
        this.ingestedRootFolders = ingestedRootFolders;
    }

    public List<Long> getAnalyzedRootFolders() {
        return analyzedRootFolders;
    }

    public void setAnalyzedRootFolders(List<Long> analyzedRootFolders) {
        this.analyzedRootFolders = analyzedRootFolders;
    }

    public void setAnalyzeFiles(boolean analyzeFiles) {
        this.analyzeFiles = analyzeFiles;
    }

    public boolean isAnalyzeFiles() {
        return analyzeFiles;
    }

    public Long getScheduleGroupId() {
        return scheduleGroupId;
    }

    public void setScheduleGroupId(Long scheduleGroupId) {
        this.scheduleGroupId = scheduleGroupId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getScheduleGroupName() {
        return scheduleGroupName;
    }

    public void setScheduleGroupName(String scheduleGroupName) {
        this.scheduleGroupName = scheduleGroupName;
    }

    public boolean isManual() {
        return manual;
    }

    public void setManual(boolean manual) {
        this.manual = manual;
    }

    public long getNewFiles() {
        return newFiles;
    }

    public void setNewFiles(long newFiles) {
        this.newFiles = newFiles;
    }

    public long getUpdatedContentFiles() {
        return updatedContentFiles;
    }

    public void setUpdatedContentFiles(long updatedContentFiles) {
        this.updatedContentFiles = updatedContentFiles;
    }

    public long getUpdatedMetadataFiles() {
        return updatedMetadataFiles;
    }

    public void setUpdatedMetadataFiles(long updatedMetadataFiles) {
        this.updatedMetadataFiles = updatedMetadataFiles;
    }

    public long getDeletedFiles() {
        return deletedFiles;
    }

    public void setDeletedFiles(long deletedFiles) {
        this.deletedFiles = deletedFiles;
    }

    public String getStateString() {
        return stateString;
    }

    public void setStateString(String stateString) {
        this.stateString = stateString;
    }


    public RunType getRunType() {
        return runType;
    }

    public void setRunType(RunType runType) {
        this.runType = runType;
    }

    public PauseReason getPauseReason() {
        return pauseReason;
    }

    public void setPauseReason(PauseReason pauseReason) {
        this.pauseReason = pauseReason;
    }

    public Date getStopTime() {
        return stopTime;
    }

    public void setStopTime(Date stopTime) {
        this.stopTime = stopTime;
    }

    public Long getStopTimeAsLong() {
        return getStopTime() == null ? null : getStopTime().getTime();
    }

    public Boolean getGracefulStop() {
        return gracefulStop;
    }

    public void setGracefulStop(Boolean gracefulStop) {
        this.gracefulStop = gracefulStop;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("CrawlRunDetailsDto{");
        sb.append("id=").append(id);
        sb.append(", scanStartTime=").append(scanStartTime);
        sb.append(", scanEndTime=").append(scanEndTime);
        sb.append(", runOutcomeState=").append(runOutcomeState);
        sb.append(", numberOfFoldersFound=").append(numberOfFoldersFound);
        sb.append(", totalProcessedFiles=").append(totalProcessedFiles);
        sb.append(", processedPdfFiles=").append(processedPdfFiles);
        sb.append(", processedWordFiles=").append(processedWordFiles);
        sb.append(", processedExcelFiles=").append(processedExcelFiles);
        sb.append(", processedOtherFiles=").append(processedOtherFiles);
        sb.append(", stateString='").append(stateString).append('\'');
        sb.append(", runType=").append(runType);
        sb.append(", newFiles=").append(newFiles);
        sb.append(", updatedContentFiles=").append(updatedContentFiles);
        sb.append(", updatedMetadataFiles=").append(updatedMetadataFiles);
        sb.append(", deletedFiles=").append(deletedFiles);
        sb.append(", scanErrors=").append(scanErrors);
        sb.append(", scanProcessingErrors=").append(scanProcessingErrors);
        sb.append(", manual=").append(manual);
        sb.append(", accessible=").append(accessible);
        sb.append(", rootFolderDto=").append(rootFolderDto);
        sb.append(", scanFiles=").append(scanFiles);
        sb.append(", ingestFiles=").append(ingestFiles);
        sb.append(", ingestedRootFolders=").append(ingestedRootFolders);
        sb.append(", analyzedRootFolders=").append(analyzedRootFolders);
        sb.append(", analyzeFiles=").append(analyzeFiles);
        sb.append(", scheduleGroupId=").append(scheduleGroupId);
        sb.append(", scheduleGroupName='").append(scheduleGroupName).append('\'');
        sb.append(", hasChildRuns=").append(hasChildRuns);
        sb.append(", parentRunId=").append(parentRunId);
        sb.append('}');
        return sb.toString();
    }
}
