package com.cla.common.domain.dto.messages.control;

/**
 * Created by uri on 16-May-17.
 */
public enum ControlResultType {
    STATUS_REPORT,
    HANDSHAKE
}
