package com.cla.common.domain.dto.group;

import com.cla.common.domain.dto.pv.TitleToken;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


@SuppressWarnings("serial")
public class GroupNamingInfoDto implements Serializable {

	public class Factors {
		private int titleTokenFactorTfidf;
		private int titleTokenFactorTf;
		private int titleTokenFactorLength;
		private int titleTokenFactorNumWords;
		private int titleBoostFactor;
		private int titleScoreFactor;
		private int titleOrderFactor;
		private int optimalTitleLength;
		private int minRatedTitleLength;
		private int maxRatedTitleLength;
		private double topTitlesBoostValue;
		private double fileNameBoostValue;
		private double parentDirBoostValue;
		private double grandParentDirBoostValue;
		
		public double getTopTitlesBoostValue() {
			return topTitlesBoostValue;
		}
		public void setTopTitlesBoostValue(double topTitlesBoostValue) {
			this.topTitlesBoostValue = topTitlesBoostValue;
		}
		public double getFileNameBoostValue() {
			return fileNameBoostValue;
		}
		public void setFileNameBoostValue(double fileNameBoostValue) {
			this.fileNameBoostValue = fileNameBoostValue;
		}
		public double getParentDirBoostValue() {
			return parentDirBoostValue;
		}
		public void setParentDirBoostValue(double parentDirBoostValue) {
			this.parentDirBoostValue = parentDirBoostValue;
		}
		public double getGrandParentDirBoostValue() {
			return grandParentDirBoostValue;
		}
		public void setGrandParentDirBoostValue(double grandParentDirBoostValue) {
			this.grandParentDirBoostValue = grandParentDirBoostValue;
		}
		public int getTitleTokenFactorTfidf() {
			return titleTokenFactorTfidf;
		}
		public int getTitleTokenFactorTf() {
			return titleTokenFactorTf;
		}
		public int getTitleTokenFactorLength() {
			return titleTokenFactorLength;
		}
		public int getTitleTokenFactorNumWords() {
			return titleTokenFactorNumWords;
		}
		public int getTitleBoostFactor() {
			return titleBoostFactor;
		}
		public int getTitleScoreFactor() {
			return titleScoreFactor;
		}
		public int getTitleOrderFactor() {
			return titleOrderFactor;
		}
		public int getOptimalTitleLength() {
			return optimalTitleLength;
		}
		public int getMinRatedTitleLength() {
			return minRatedTitleLength;
		}
		public int getMaxRatedTitleLength() {
			return maxRatedTitleLength;
		}

		Factors() {
			
		}
		
		Factors(final int titleTokenFactorTfidf, final int titleTokenFactorTf, final int titleTokenFactorLength, final int titleTokenFactorNumWords, 
				final int titleBoostFactor, final int titleScoreFactor, final int titleOrderFactor, 
				final int optimalTitleLength, final int minRatedTitleLength, final int maxRatedTitleLength) {
			setFactors(titleTokenFactorTfidf, titleTokenFactorTf, titleTokenFactorLength, titleTokenFactorNumWords, titleBoostFactor, titleScoreFactor, titleOrderFactor, optimalTitleLength, minRatedTitleLength, maxRatedTitleLength);
		}
		
		public void setFactors(final int titleTokenFactorTfidf, final int titleTokenFactorTf, final int titleTokenFactorLength, final int titleTokenFactorNumWords, 
				final int titleBoostFactor, final int titleScoreFactor, final int titleOrderFactor, 
				final int optimalTitleLength, final int minRatedTitleLength, final int maxRatedTitleLength) {
			this.titleTokenFactorTfidf = titleTokenFactorTfidf;
			this.titleTokenFactorTf = titleTokenFactorTf;
			this.titleTokenFactorLength = titleTokenFactorLength;
			this.titleTokenFactorNumWords = titleTokenFactorNumWords;
			this.titleBoostFactor = titleBoostFactor;
			this.titleScoreFactor = titleScoreFactor;
			this.titleOrderFactor = titleOrderFactor;
			this.optimalTitleLength = optimalTitleLength;
			this.minRatedTitleLength = minRatedTitleLength;
			this.maxRatedTitleLength = maxRatedTitleLength;
		}
	}

	private final GroupDto groupDto;
	private String newName;
	private String debugInfo;

	private Factors factors;
	
	private String status;
	private boolean useFilename;
	private TitleToken topToken;
	private List<TitleToken> titleBasedTokens;
	private List<TitleToken> filenameBasedTokens;
	private List<TitleToken> parentDirBasedTokens;
	private List<TitleToken> grandparentDirBasedTokens;
	private List<TitleToken> combinedListTokens;

	public GroupNamingInfoDto(String id, String genName, int filesNum, String altNames) {
		factors = new Factors();
		this.groupDto = new GroupDto(id, genName, Integer.valueOf(filesNum).longValue(), 0L, "", "", altNames);
	}

	public Factors getFactors() {
		return factors;
	}

	public void setFactors(Factors factors) {
		this.factors = factors;
	}

	public void setNewName(String newName) {
		this.newName = newName;
	}

	public void setTopToken(TitleToken topToken) {
		this.topToken = new TitleToken(topToken);
	}

	private List<TitleToken> duplicateTokensList(List<TitleToken> titleTokens) {
		List<TitleToken> res = new ArrayList<>(titleTokens.size());
		titleTokens.forEach(t->res.add(new TitleToken(t)));
		return res;
	}

	public void setTitleBasedTokens(List<TitleToken> newProposedTitles) {
		titleBasedTokens = duplicateTokensList(newProposedTitles);
	}
	
	public void setFilenameBasedTokens(List<TitleToken> fnTv) {
		filenameBasedTokens = duplicateTokensList(fnTv);
	}


	public void setParentDirBasedTokens(List<TitleToken> parentDirTv) {
		parentDirBasedTokens = duplicateTokensList(parentDirTv);
	}

	public void setGrandparentDirBasedTokens(List<TitleToken> grandParentDirTv) {
		grandparentDirBasedTokens = duplicateTokensList(grandParentDirTv);
	}

	public void setCombinedListTokens(List<TitleToken> newTopTitles) {
		combinedListTokens = duplicateTokensList(newTopTitles);
	}

	public void setUseFilename(boolean useFilename) {
		this.useFilename = useFilename;
	}

	public void setStatus(String statusStr) {
		this.status  = statusStr;
	}
	
	public void setDebugInfo(String debugInfo) {
		this.debugInfo  = debugInfo;
	}

	public GroupDto getGroupDto() {
		return groupDto;
	}

	public String getNewName() {
		return newName;
	}

	public String getDebugInfo() {
		return debugInfo;
	}

	public String getStatus() {
		return status;
	}

	public boolean isUseFilename() {
		return useFilename;
	}

	public TitleToken getTopToken() {
		return topToken;
	}

	public List<TitleToken> getTitleBasedTokens() {
		return titleBasedTokens;
	}

	public List<TitleToken> getFilenameBasedTokens() {
		return filenameBasedTokens;
	}

	public List<TitleToken> getParentDirBasedTokens() {
		return parentDirBasedTokens;
	}

	public List<TitleToken> getGrandparentDirBasedTokens() {
		return grandparentDirBasedTokens;
	}

	public List<TitleToken> getCombinedListTokens() {
		return combinedListTokens;
	}
	
}
