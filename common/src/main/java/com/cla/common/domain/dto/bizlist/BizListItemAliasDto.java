package com.cla.common.domain.dto.bizlist;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

/**
 * Created by uri on 31/12/2015.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BizListItemAliasDto implements Serializable {

    Long id;

    String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String alias) {
        this.name= alias;
    }

    public BizListItemAliasDto(String name) {
        this.name= name;
    }

    public BizListItemAliasDto() {
    }

    public BizListItemAliasDto(long id, String alias) {
        this.id = id;
        this.name= alias;
    }

    @Override
    public String toString() {
        return "BizListItemAliasDto{" +
                "name='" + name + '\'' +
                ", id=" + id +
                '}';
    }
}
