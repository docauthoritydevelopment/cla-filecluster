package com.cla.common.domain.dto.bizlist;

import com.cla.common.domain.dto.AssociableEntityDto;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 * Created by uri on 31/12/2015.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SimpleBizListItemDto implements Serializable, AssociableEntityDto, SimpleBizListItemInterface {

    public static final String ID_PREFIX = "bli.";

    private String id;

    private String businessId;

    private String name;

    private Long bizListId;

    private String bizListName;

    private BizListItemType type;

    private List<BizListItemAliasDto> entityAliases;

    private String ruleName;

    private BizListItemState state;

    private Long importId;

    private Boolean implicit;

    private boolean aggregated;

    private String encryptedFieldsJson;

    public String getBusinessId() {
        return businessId;
    }

    public void setBusinessId(String entityId) {
        this.businessId = entityId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getBizListId() {
        return bizListId;
    }

    public void setBizListId(Long bizListId) {
        this.bizListId = bizListId;
    }

    public List<BizListItemAliasDto> getEntityAliases() {
        return entityAliases;
    }

    public void setEntityAliases(List<BizListItemAliasDto> entityAliases) {
        this.entityAliases = entityAliases;
    }

    public BizListItemState getState() {
        return state;
    }

    public void setState(BizListItemState state) {
        this.state = state;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getImportId() {
        return importId;
    }

    public void setImportId(Long importId) {
        this.importId = importId;
    }

    public BizListItemType getType() {
        return type;
    }

    public void setType(BizListItemType type) {
        this.type = type;
    }

    public Boolean isImplicit() {
        return implicit;
    }

    public void setImplicit(Boolean implicit) {
        this.implicit = implicit;
    }

    public boolean isAggregated() {
        return aggregated;
    }

    public void setAggregated(boolean aggregated) {
        this.aggregated = aggregated;
    }

    public String getEncryptedFieldsJson() {
        return encryptedFieldsJson;
    }

    public void setEncryptedFieldsJson(String encryptedFieldsJson) {
        this.encryptedFieldsJson = encryptedFieldsJson;
    }

    public String getRuleName() {
        return ruleName;
    }

    public void setRuleName(String ruleName) {
        this.ruleName = ruleName;
    }

    public String getBizListName() {
        return bizListName;
    }

    public void setBizListName(String bizListName) {
        this.bizListName = bizListName;
    }

    @Override
    public String toString() {
        return "SimpleBizListItemDto{" +
                "id='" + id + '\'' +
                ", businessId='" + businessId + '\'' +
                ", name='" + name + '\'' +
                ", bizListId=" + bizListId +
                ", type=" + type +
                ", entityAliases=" + entityAliases +
                ", state=" + state +
                ", importId=" + importId +
                ", implicit=" + implicit +
                ", aggregated=" + aggregated +
                ", encryptedFieldsJson='" + encryptedFieldsJson + '\'' +
                '}';
    }

    @Override
    public boolean isIdMatch(Object idToCheck) {
        return idToCheck != null && idToCheck instanceof String && idToCheck.equals(id);
    }

    @Override
    @JsonIgnore
    public BizListItemType getListType() {
        return type;
    }

    @Override
    @JsonIgnore
    public String getSid() {
        return id;
    }

    @Override
    @JsonIgnore
    public String createIdentifierString() {
        return createBizListIdentifierString();
    }

    public SimpleBizListItemDto() {
    }

    public SimpleBizListItemDto(SimpleBizListItemDto other) {
        this.id = other.id;
        this.businessId = other.businessId;
        this.name = other.name;
        this.bizListId = other.bizListId;
        this.bizListName = other.bizListName;
        this.type = other.type;
        this.entityAliases = other.entityAliases;
        this.ruleName = other.ruleName;
        this.state = other.state;
        this.importId = other.importId;
        this.implicit = other.implicit;
        this.aggregated = other.aggregated;
        this.encryptedFieldsJson = other.encryptedFieldsJson;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SimpleBizListItemDto that = (SimpleBizListItemDto) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(businessId, that.businessId) &&
                Objects.equals(name, that.name) &&
                Objects.equals(bizListId, that.bizListId) &&
                Objects.equals(bizListName, that.bizListName) &&
                type == that.type &&
                Objects.equals(ruleName, that.ruleName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, businessId, name, bizListId, bizListName, type, ruleName);
    }
}
