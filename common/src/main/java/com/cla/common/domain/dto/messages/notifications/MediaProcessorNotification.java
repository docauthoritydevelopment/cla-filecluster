package com.cla.common.domain.dto.messages.notifications;

import java.io.Serializable;

/**
 * Created by uri on 14-Mar-17.
 */
public class MediaProcessorNotification implements Serializable {

    private Long runContext;

    MediaProcessorNotificationType type;

    String payload;

    public MediaProcessorNotification() {
    }

    public MediaProcessorNotification(Long runContext, MediaProcessorNotificationType type) {
        this.runContext = runContext;
        this.type = type;
    }

    public MediaProcessorNotification(Long runContext, MediaProcessorNotificationType type, String payload) {
        this.runContext = runContext;
        this.type = type;
        this.payload = payload;
    }

    public Long getRunContext() {
        return runContext;
    }

    public void setRunContext(Long runContext) {
        this.runContext = runContext;
    }

    public MediaProcessorNotificationType getType() {
        return type;
    }

    public void setType(MediaProcessorNotificationType type) {
        this.type = type;
    }

    public String getPayload() {
        return payload;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }
}
