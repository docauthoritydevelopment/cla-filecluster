package com.cla.common.domain.dto.mediaproc.word;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by uri on 05/02/2017.
 */
public class FontStats {
    private int commonFontSize = -1;
    private int minBodyFontSize = 99999;
    private int maxBodyFontSize = 0;
    private int sumProdBodyFontSize = 0;
    private int countChars = 0;
    private int countRuns = 0;
    private final Map<Integer, Integer> fontSizeCounts = new HashMap<>();
    private final Map<String, Integer> fontNameCounts = new HashMap<>();
    private Map<String, Integer> fontNameIndexMap = null;

    public void addRun(final String fontName, final int size, final int lenght) {
        countRuns++;
        countChars += lenght;
        sumProdBodyFontSize += size * lenght;
        if (maxBodyFontSize < size) {
            maxBodyFontSize = size;
        }
        if (minBodyFontSize > size) {
            minBodyFontSize = size;
        }
        // Update font-size histogram
        Integer c;
        if (size > 0) {
            final Integer k = Integer.valueOf(size);
            c = fontSizeCounts.get(k);
            if (c == null) {
                c = 0;
            }
            fontSizeCounts.put(k, Integer.valueOf(c + lenght));
        }
        // Update font-name histogram
        c = fontNameCounts.get(fontName);
        if (c == null) {
            c = 0;
        }
        fontNameCounts.put(fontName, Integer.valueOf(c + lenght));

        commonFontSize = -1;
        fontNameIndexMap = null;
    }

    public int getMmaxBodyFontSize() {
        return maxBodyFontSize;
    }

    public int getMinBodyFontSize() {
        return minBodyFontSize;
    }

    public int getAverageFontSize() {
        return (countChars == 0) ? 0 :
                (int) (0.5f + ((float) sumProdBodyFontSize) / countChars);
    }

    public int getCommonFontSize() {
        if (countChars == 0) {
            return 0;
        }
        if (commonFontSize < 0) {
            commonFontSize = fontSizeCounts.entrySet().stream()
                    .max((e1, e2) -> (e1.getValue() - e2.getValue())).map(e -> e.getKey()).orElse(0);
        }
        return commonFontSize;
    }

    public int getFontNameCount(final String fontNmae) {
        final Integer c = fontNameCounts.get(fontNmae);
        return (c == null) ? 0 : c.intValue();
    }

    public int getFontNameOrderedIndex(final String fontName) {
        final Integer c = getFontNameIndexMap().get(fontName);
        return (c == null) ? -1 : c.intValue();
    }

    public Map<String, Integer> getFontNameIndexMap() {
        if (fontNameIndexMap == null) {
            final List<String> sortedFontNames = fontNameCounts.entrySet().stream().sorted((e1, e2) -> (e2.getValue() - e1.getValue()))
                    .map(e -> e.getKey()).collect(Collectors.toList());
            fontNameIndexMap = IntStream.range(0, sortedFontNames.size()).mapToObj(i -> Integer.valueOf(i))
                    .collect(Collectors.toMap(i -> sortedFontNames.get(i), i -> Integer.valueOf(i)));
        }
        return fontNameIndexMap;
    }

}
