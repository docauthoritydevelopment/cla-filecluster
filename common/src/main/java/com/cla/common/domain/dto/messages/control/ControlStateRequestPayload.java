package com.cla.common.domain.dto.messages.control;

import com.cla.common.domain.dto.crawler.JobStateByDate;
import com.cla.connector.domain.dto.error.ProcessingErrorType;

import java.util.List;

/**
 * Created by liora on 21/06/2017.
 */
public class ControlStateRequestPayload extends ControlMessagePayload {
    private List<JobStateByDate> jobStates;

    @Override
    public ProcessingErrorType calculateProcessErrorType() {
        return ProcessingErrorType.ERR_NONE;
    }

    public List<JobStateByDate> getJobStates() {
        return jobStates;
    }

    public void setJobStates(List<JobStateByDate> jobStates) {
        this.jobStates = jobStates;
    }

    @Override
    public String toString() {
        return "ControlStateRequestPayload{" +
                " jobStates='" + jobStates +
                '}';
    }

}
