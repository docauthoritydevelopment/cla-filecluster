package com.cla.common.domain.dto.file;

import com.cla.common.domain.dto.histogram.ClaLongSuperCollectionArray;
import com.cla.common.domain.dto.histogram.ClaSuperCollection;
import com.cla.common.domain.dto.pv.PropertyVector;
import com.google.common.base.Strings;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public class FileCollections implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private final Map<String, ClaSuperCollection<Long>> collections = new LinkedHashMap<>();

	public FileCollections(final ClaSuperCollection<Long> histogram) {
		collections.put(PropertyVector.DEF_SYS_PART, histogram);
	}
	
	public FileCollections() {
	}

	public void addPartHistogram(final String part, final ClaSuperCollection<Long> histogram) {
		collections.put(part, histogram);
	}

	public ClaSuperCollection<Long> getPartHistogram(final String part) {
		return (Strings.isNullOrEmpty(part)) ? null : collections.get(part);
	}

	public void removePartHistogram(final String part) {
		if (!Strings.isNullOrEmpty(part)) {
			collections.remove(part);
		}
	}

	public Map<String, ClaSuperCollection<Long>> getCollections() {
		return collections;
	}

	public Set<String> getParts() {
		return collections.keySet(); 
	}

	public int size() {
		return collections.size(); 
	}

	public FileCollections getArraysCollections() {
		FileCollections fc = new FileCollections();
		collections.forEach((p, sc) ->
				fc.addPartHistogram(p, (sc instanceof ClaLongSuperCollectionArray) ?
						sc : ClaLongSuperCollectionArray.create(sc)));
		return fc;
	}

}
