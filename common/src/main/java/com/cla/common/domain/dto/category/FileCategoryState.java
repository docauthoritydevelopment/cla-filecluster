package com.cla.common.domain.dto.category;

/**
 * Created by uri on 11/08/2015.
 */
public enum FileCategoryState {
    ACTIVE,MERGED,CREATING
}
