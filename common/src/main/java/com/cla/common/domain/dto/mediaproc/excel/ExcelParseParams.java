package com.cla.common.domain.dto.mediaproc.excel;

import com.cla.common.utils.tokens.DocAuthorityTokenizer;

import java.util.Set;

/**
 * Created by uri on 05/02/2017.
 */
public class ExcelParseParams {

    private boolean useTableHeadingsAsTitles;

    private Set<String> ignoredSheetNamePrefixesAsTitlesSet;
    private int singleTokenNumberedTitleRateReduction;

    private boolean patternSearchActive;
    private boolean useFormattedPatterns;

    private static int tokenMatcherExcelCellMinStringLength = 3;
    private static int tokenMatcherExcelCellMinNumberLength = 4;
    private static boolean tokenMatcherExcelIgnoreComplexNumbers = true;

    private static DocAuthorityTokenizer nGramTokenizerHashed = null;

    public static void setTokenMatchedParameters(final int tokenMatcherExcelCellMinStringLength,final int tokenMatcherExcelCellMinNumberLength,final boolean tokenMatcherExcelIgnoreComplexNumbers) {
        ExcelParseParams.tokenMatcherExcelCellMinStringLength = tokenMatcherExcelCellMinStringLength;
        ExcelParseParams.tokenMatcherExcelCellMinNumberLength = tokenMatcherExcelCellMinNumberLength;
        ExcelParseParams.tokenMatcherExcelIgnoreComplexNumbers = tokenMatcherExcelIgnoreComplexNumbers;
    }

    public ExcelParseParams(boolean useTableHeadingsAsTitles,
                            int singleTokenNumberedTitleRateReduction,
                            Set<String> ignoredSheetNamePrefixesAsTitlesSet,
                            boolean patternSearchActive,
                            boolean useFormattedPatterns) {
        this.useTableHeadingsAsTitles = useTableHeadingsAsTitles;
        this.singleTokenNumberedTitleRateReduction = singleTokenNumberedTitleRateReduction;
        this.ignoredSheetNamePrefixesAsTitlesSet = ignoredSheetNamePrefixesAsTitlesSet;
        this.patternSearchActive = patternSearchActive;
        this.useFormattedPatterns = useFormattedPatterns;
    }

    public boolean isUseTableHeadingsAsTitles() {
        return useTableHeadingsAsTitles;
    }

    public Set<String> getIgnoredSheetNamePrefixesAsTitlesSet() {
        return ignoredSheetNamePrefixesAsTitlesSet;
    }

    public int getSingleTokenNumberedTitleRateReduction() {
        return singleTokenNumberedTitleRateReduction;
    }

    public boolean isPatternSearchActive() {
        return patternSearchActive;
    }

    public boolean isUseFormattedPatterns() {
        return useFormattedPatterns;
    }

    public static int getTokenMatcherExcelCellMinStringLength() {
        return tokenMatcherExcelCellMinStringLength;
    }

    public static int getTokenMatcherExcelCellMinNumberLength() {
        return tokenMatcherExcelCellMinNumberLength;
    }

    public static boolean getTokenMatcherExcelIgnoreComplexNumbers() {
        return tokenMatcherExcelIgnoreComplexNumbers;
    }

    public static void setNGramTokenizer(
            final DocAuthorityTokenizer nGramTokenizer) {
        nGramTokenizerHashed = nGramTokenizer;
    }

    public static DocAuthorityTokenizer getDocAuthorityTokenizer() {
        return nGramTokenizerHashed;
    }
}
