package com.cla.common.domain.dto.pv;


public class SimilarityRates {
	private float similarity = 0f;
	private float inclusion = 0f;
	
	
	public SimilarityRates() {
	}


	public SimilarityRates(final float similarity, final float inclusion) {
		this.similarity = similarity;
		this.inclusion = inclusion;
	}

	

	public float getSimilarity() {
		return similarity;
	}


	public void setSimilarity(final float similarity) {
		this.similarity = similarity;
	}


	public float getInclusion() {
		return inclusion;
	}


	public void setInclusion(final float inclusion) {
		this.inclusion = inclusion;
	}


	@Override
	public String toString() {
		return "Rates:{sim:" + similarity + ", incl:" + inclusion + "}";
	}


	public void add(final float similarityDelta, final float incusionDelta) {
		similarity+=similarityDelta;
		inclusion += incusionDelta;		
	}
}
