package com.cla.common.domain.dto.pv;

/**
 * Created by uri on 05/02/2017.
 */
public enum MaturityClass {
    MC_STYLE, MC_CONTENT, MC_MAX_VALUE                //, MC_STRUCTURE
}
