package com.cla.common.domain.dto.components;

/**
 * Created by itay on 22/02/2017.
 */
public enum ComponentState {
    OK(0),
    STOPPED(1),
    SHUTTING_DOWN(2),
    STARTING(3),
    FAULTY(4),
    REMOVED(5),
    PENDING_INIT(6),
    UNKNOWN(7),
    WARNING(8);

    private int value;

    ComponentState(int value) {
        this.value = value;
    }

    private static ComponentState[] cachedValues;
    static {
        cachedValues = values();
    }

    public static ComponentState of(int ordinal){
        if (ordinal < 0 || ordinal >= cachedValues.length){
            throw new IndexOutOfBoundsException("Ordinal value is out of bounds: " + ordinal);
        }
        return cachedValues[ordinal];
    }
}
