package com.cla.common.domain.dto.messages.control;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class DriveDto implements Serializable {
	@JsonProperty("folder_sizes")
	private Map<String,Integer> folderSizes;
	@JsonProperty("drive_capacities")
	private List<DriveCapacityMetrics> driveCapacities;
	@JsonProperty("machine_id")
	private String machineId;
	private Date timestamp = new Date();

	//region ...getters & setters
	public Map<String, Integer> getFolderSizes() {
		return folderSizes;
	}

	public DriveDto setFolderSizes(Map<String, Integer> folderSizes) {
		this.folderSizes = folderSizes;
		return this;
	}

	public List<DriveCapacityMetrics> getDriveCapacities() {
		return driveCapacities;
	}

	public DriveDto setDriveCapacities(List<DriveCapacityMetrics> driveCapacities) {
		this.driveCapacities = driveCapacities;
		return this;
	}

	public String getMachineId() {
		return machineId;
	}

	public DriveDto setMachineId(String machineId) {
		this.machineId = machineId;
		return this;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public DriveDto setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
		return this;
	}

	//endregion

	//region ...hash-toString-equals
	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
				.add("folderSizes", folderSizes)
				.add("driveCapacities", driveCapacities)
				.add("machineId", machineId)
				.add("timeStamp", timestamp)
				.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof DriveDto))
			return false;
		DriveDto driveDto = (DriveDto) o;
		return Objects.equal(getFolderSizes(), driveDto.getFolderSizes()) &&
				Objects.equal(getDriveCapacities(), driveDto.getDriveCapacities()) &&
				Objects.equal(getMachineId(), driveDto.getMachineId()) &&
				Objects.equal(getTimestamp(), driveDto.getTimestamp());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getFolderSizes(), getDriveCapacities(), getMachineId(), getTimestamp());
	}

	//endregion

	public static class DriveCapacityMetrics {
		@JsonProperty(value = "drive_id",required = true)
		private String driveId;
		@JsonProperty(value = "total_space",required = true)
		private int totalSpace;
		@JsonProperty(value = "free_space",required = true)
		private int freeSpace;

		public DriveCapacityMetrics() {
		}

		public DriveCapacityMetrics(String driveId, int totalSpace, int freeSpace) {
			this.driveId = driveId;
			this.totalSpace = totalSpace;
			this.freeSpace = freeSpace;
		}

		public String getDriveId() {
			return driveId;
		}

		public DriveCapacityMetrics setDriveId(String driveId) {
			this.driveId = driveId;
			return this;
		}

		public int getTotalSpace() {
			return totalSpace;
		}

		public DriveCapacityMetrics setTotalSpace(int totalSpace) {
			this.totalSpace = totalSpace;
			return this;
		}

		public int getFreeSpace() {
			return freeSpace;
		}

		public DriveCapacityMetrics setFreeSpace(int freeSpace) {
			this.freeSpace = freeSpace;
			return this;
		}

		@Override
		public String toString() {
			return MoreObjects.toStringHelper(this)
					.add("driveId", driveId)
					.add("totalSpace", totalSpace)
					.add("freeSpace", freeSpace)
					.toString();
		}

		@Override
		public boolean equals(Object o) {
			if (this == o)
				return true;
			if (!(o instanceof DriveCapacityMetrics))
				return false;
			DriveCapacityMetrics that = (DriveCapacityMetrics) o;
			return getTotalSpace() == that.getTotalSpace() &&
					getFreeSpace() == that.getFreeSpace() &&
					Objects.equal(getDriveId(), that.getDriveId());
		}

		@Override
		public int hashCode() {
			return Objects.hashCode(getDriveId(), getTotalSpace(), getFreeSpace());
		}
	}
}
