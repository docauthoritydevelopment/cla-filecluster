package com.cla.common.domain.dto.workflow;

/**
 * Created by: yael
 * Created on: 2/21/2018
 */
public enum ActionType {
    COMMENT, STATE_CHANGE, CREATE, UPDATE_FIELD
}
