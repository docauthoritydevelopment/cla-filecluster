package com.cla.common.domain.dto.schedule;

/**
 * Created by uri on 15/05/2016.
 */
public enum ScheduleType {
    HOURLY,DAILY,WEEKLY,CUSTOM
}
