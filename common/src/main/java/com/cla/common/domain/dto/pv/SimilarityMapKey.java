package com.cla.common.domain.dto.pv;


public class SimilarityMapKey {

	private long docIdFromKey;
	private String partFromKey;
	private int internalId;

	public SimilarityMapKey() {
	}

	public SimilarityMapKey(long docIdFromKey, String partFromKey, int internalId) {
		this.docIdFromKey = docIdFromKey;
		this.partFromKey = partFromKey;
		this.internalId = internalId;
	}

	public SimilarityMapKey(final Long docIdFromKey, final String partFromKey) {
		this.docIdFromKey = docIdFromKey;
		this.partFromKey = partFromKey;
	}

	public Long getDocIdFromKey() {
		return docIdFromKey;
	}

	public String getPartFromKey() {
		return partFromKey;
	}

	public int getInternalId() {
		return internalId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) docIdFromKey;
		result = prime * result + partFromKey.hashCode();
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final SimilarityMapKey other = (SimilarityMapKey) obj;
		if (docIdFromKey != other.docIdFromKey)
			return false;
		if (!partFromKey.equals(other.partFromKey))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "[" + docIdFromKey + partFromKey + "]";
	}

}
