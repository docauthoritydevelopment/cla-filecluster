package com.cla.common.domain.dto.mcf.mcf_api.job;

import java.io.Serializable;
import java.util.Map;

/**
 * Created by uri on 18/07/2016.
 */
public class McfPipelineStageObjectDto implements Serializable {

    /**"stage_id"	The unique identifier for the pipeline stage */
    String stage_id;

    /**"stage_prerequisite"	The unique identifier for the preceding pipeline stage; may be missing if none*/
    String stage_prerequisite;

    /**"stage_isoutput"	"true" if the stage is an output connection*/
    boolean stage_isoutput;

    /**"stage_connectionname"	The connection name for the pipeline stage*/
    String stage_connectionname;

    /**"stage_description"	A description of the pipeline stage*/
    String stage_description;

    /**"stage_specification"	The specification string for the pipeline stage*/
    Map stage_specification;

    public String getStage_id() {
        return stage_id;
    }

    public void setStage_id(String stage_id) {
        this.stage_id = stage_id;
    }

    public String getStage_prerequisite() {
        return stage_prerequisite;
    }

    public void setStage_prerequisite(String stage_prerequisite) {
        this.stage_prerequisite = stage_prerequisite;
    }

    public boolean isStage_isoutput() {
        return stage_isoutput;
    }

    public void setStage_isoutput(boolean stage_isoutput) {
        this.stage_isoutput = stage_isoutput;
    }

    public String getStage_connectionname() {
        return stage_connectionname;
    }

    public void setStage_connectionname(String stage_connectionname) {
        this.stage_connectionname = stage_connectionname;
    }

    public String getStage_description() {
        return stage_description;
    }

    public void setStage_description(String stage_description) {
        this.stage_description = stage_description;
    }

    public Map getStage_specification() {
        return stage_specification;
    }

    public void setStage_specification(Map stage_specification) {
        this.stage_specification = stage_specification;
    }
}
