package com.cla.common.domain.dto.workflow;

public enum FileAction {
    NEW,DO_NOTHING,ARCHIVE,DELETE,IRRELEVANT
}
