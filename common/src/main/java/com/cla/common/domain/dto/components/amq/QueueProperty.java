package com.cla.common.domain.dto.components.amq;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by: yael
 * Created on: 7/22/2018
 */
public enum QueueProperty {
    QueueSize("x1"),
    EnqueueCount("x2"),
    ConsumerCount("x3"),
    ExpiredCount("x4"),
    DequeueCount("x5"),
    AverageEnqueueTime("x6"),
    MinEnqueueTime("x7"),
    MaxEnqueueTime("x8"),
    DataCenterId("x9"),
    Name("x10"),
    CursorFull("x11"),
    CursorMemoryUsage("x12"),
    CursorPercentUsage("x13"),
    InFlightCount("x14"),
    MemoryPercentUsage("x15"),
    ProducerCount("x16"),
    ProducerFlowControl("x17"),
    AlwaysRetroactive("x18"),
    Options("x19"),
    MemoryUsageByteCount("x20"),
    AverageBlockedTime("x21"),
    Subscriptions("x22"),
    CacheEnabled("x23"),
    ForwardCount("x24"),
    StoreMessageSize("x25"),
    DLQ("x26"),
    BlockedSends("x27"),
    MaxAuditDepth("x28"),
    TotalBlockedTime("x29"),
    MaxPageSize("x30"),
    PrioritizedMessages("x31"),
    MemoryUsagePortion("x32"),
    Paused("x33"),
    MessageGroups("x34"),
    AverageMessageSize("x35", true), // received in bytes saved in kilo
    MaxProducersToAudit("x36"),
    MinMessageSize("x37", true),  // received in bytes saved in kilo
    MemoryLimit("x38"),
    DispatchCount("x39"),
    BlockedProducerWarningInterval("x40"),
    MessageGroupType("x41"),
    MaxMessageSize("x42", true),  // received in bytes saved in kilo
    UseCache("x43"),
    SlowConsumerStrategy("x44")
    ;

    private String initials;
    private boolean divideForKilo;

    QueueProperty(String initials) {
        this.divideForKilo = false;
        this.initials = initials;
    }

    QueueProperty(String initials, boolean divideForKilo) {
        this.divideForKilo = divideForKilo;
        this.initials = initials;
    }

    public String getInitials() {
        return initials;
    }

    public boolean isDivideForKilo() {
        return divideForKilo;
    }

    public static QueueProperty valueOf(int ordinal) {
        return QueueProperty.values()[ordinal];
    }

    private static Map<String, QueueProperty> map = new HashMap<String, QueueProperty>();

    static {
        for (QueueProperty state : QueueProperty.values()) {
            map.put(state.getInitials(), state);
        }
    }

    public static QueueProperty valueByInitials(String initials) {
        return map.get(initials);
    }
}
