package com.cla.common.domain.dto.pv;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class PartSimilarityMaps implements Map<PVType, SimilarityMap> {
	private Map<PVType, SimilarityMap> similarityMap = new HashMap<>();
	private Map<PVType, Integer> pvCounts = new HashMap<>();
	
	public PartSimilarityMaps() {
	}

	// Similarity stats counters
	private final int[] counters = new int[]{0,0,0};
	
	public int incrementCounter(final int inx) {
		if (inx<0 || inx>=counters.length)
			return -1;
		return ++(counters[inx]);
	}

	public int getCounter(final int inx) {
		if (inx<0 || inx>=counters.length)
			return -1;
		return counters[inx];
	}

	public void resetCounter(final int inx) {
		if (inx<0 || inx>=counters.length)
			return;
		counters[inx] = 0;
	}
	public void resetCounters() {
		for (int i=0;i<counters.length;++i)
			counters[i] = 0;
	}
	
	@Override
	public void clear() {
		similarityMap.clear();
		pvCounts.clear();
	}

	@Override
	public boolean containsKey(final Object key) {
		return (key==null)?null:similarityMap.containsKey(key);
	}

	@Override
	public boolean containsValue(final Object value) {
		return (value==null)?null:similarityMap.containsValue(value);
	}

	@Override
	public Set<Entry<PVType, SimilarityMap>> entrySet() {
		return similarityMap.entrySet();
	}

	public Set<Entry<PVType, Integer>> pvCountsEntrySet() {
		return pvCounts.entrySet();
	}

	@Override
	public SimilarityMap get(final Object key) {
		return similarityMap.get(key);
	}
	public Integer getPvCount(final PVType key) {
		return pvCounts.get(key);
	}

	@Override
	public boolean isEmpty() {
		return similarityMap.isEmpty();
	}

	@Override
	public Set<PVType> keySet() {
		return similarityMap.keySet();
	}
	public Set<PVType> pvCountsKeySet() {
		return pvCounts.keySet();
	}

	@Override
	public SimilarityMap put(final PVType key, final SimilarityMap value) {
		return (key==null)?null:similarityMap.put(key, value);
	}

	public Integer put(final PVType key, final Integer value) {
		return (key==null)?null:pvCounts.put(key, value);
	}
	public void put(final PVType key, final SimilarityMap sValue, final Integer cValue) {
		if (key!=null) {
			similarityMap.put(key, sValue);
			pvCounts.put(key, cValue);
		}
	}

	@Override
	public void putAll(final Map<? extends PVType, ? extends SimilarityMap> m) {
		if (m!=null)
			similarityMap.putAll(m);
	}

	public void putAllPvCounts(final Map<? extends PVType, ? extends Integer> m) {
		if (m!=null)
			pvCounts.putAll(m);
	}

	@Override
	public SimilarityMap remove(final Object key) {
		return (key==null)?null:similarityMap.remove(key);
	}

	@Override
	public int size() {
		return similarityMap.size();
	}

	public int pvCountsSize() {
		return pvCounts.size();
	}

	@Override
	public Collection<SimilarityMap> values() {
		return similarityMap.values();
	}
	public Collection<Integer> pvCountsValues() {
		return pvCounts.values();
	}

	public Map<PVType, Integer> getPvCounts() {
		return pvCounts;
	}

	public void close() {
		if (!isClosed()) {
			similarityMap.values().stream().forEach(sm->sm.close());
			similarityMap.clear();
			similarityMap = null;
			pvCounts.clear();
			pvCounts = null;
		}
	}
	
	public boolean isClosed() {
		return (similarityMap == null);
	}

}
