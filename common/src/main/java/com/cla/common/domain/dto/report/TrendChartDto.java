package com.cla.common.domain.dto.report;

import com.cla.common.domain.dto.date.DateRangePointDto;

import java.io.Serializable;

/**
 * Created by uri on 27/07/2016.
 */
public class TrendChartDto implements Serializable {

    private Long id;

    private Long trendChartCollectorId;

    private String name;

    private ChartDisplayType chartDisplayType;

    private ChartValueFormatType chartValueFormatType;

    private Long ownerId;

    private boolean hidden;

    private String seriesType;

    private String valueType;

    private SortType sortType;

    private boolean deleted;

    private String filters;

    private String viewConfiguration;

    /**
     * Timestamp for the first (earliest) point
     */
    private DateRangePointDto viewFrom;

    /**
     * Maximum time that the chart will show. Nullable
     */
    private DateRangePointDto viewTo;

    /**
     * Optional number of points to show
     */
    private Integer viewSampleCount;

    private TimeResolutionType resolutionType;      // default is TR_miliSec
    /**
     * The view resolution in milliseconds (resolutionType = TR_miliSec)
     */
    private Long viewResolutionMs;

    /**
     * The view resolution details (resolutionType != TR_miliSec)
     */
    private Integer viewResolutionCount;    // default is 1
    private Integer viewOffset;             // default is 0
    private Integer viewResolutionStep;     // default is 1

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ChartDisplayType getChartDisplayType() {
        return chartDisplayType;
    }

    public void setChartDisplayType(ChartDisplayType chartDisplayType) {
        this.chartDisplayType = chartDisplayType;
    }

    public ChartValueFormatType getChartValueFormatType() {
        return chartValueFormatType;
    }

    public void setChartValueFormatType(ChartValueFormatType chartValueFormatType) {
        this.chartValueFormatType = chartValueFormatType;
    }

    public Long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(Long ownerId) {
        this.ownerId = ownerId;
    }

    public boolean isHidden() {
        return hidden;
    }

    public void setHidden(boolean hidden) {
        this.hidden = hidden;
    }

    public String getSeriesType() {
        return seriesType;
    }

    public void setSeriesType(String seriesType) {
        this.seriesType = seriesType;
    }

    public String getValueType() {
        return valueType;
    }

    public void setValueType(String valueType) {
        this.valueType = valueType;
    }

    public SortType getSortType() {
        return sortType;
    }

    public void setSortType(SortType sortType) {
        this.sortType = sortType;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public String getFilters() {
        return filters;
    }

    public void setFilters(String filters) {
        this.filters = filters;
    }

    public String getViewConfiguration() {
        return viewConfiguration;
    }

    public void setViewConfiguration(String viewConfiguration) {
        this.viewConfiguration = viewConfiguration;
    }

    public DateRangePointDto getViewFrom() {
        return viewFrom;
    }

    public void setViewFrom(DateRangePointDto viewFrom) {
        this.viewFrom = viewFrom;
    }

    public DateRangePointDto getViewTo() {
        return viewTo;
    }

    public void setViewTo(DateRangePointDto viewTo) {
        this.viewTo = viewTo;
    }

    public Integer getViewSampleCount() {
        return viewSampleCount;
    }

    public void setViewSampleCount(Integer viewSampleCount) {
        this.viewSampleCount = viewSampleCount;
    }

    public Long getViewResolutionMs() {
        return viewResolutionMs;
    }

    public void setViewResolutionMs(Long viewResolutionMs) {
        this.viewResolutionMs = viewResolutionMs;
    }

    public Long getTrendChartCollectorId() {
        return trendChartCollectorId;
    }

    public void setTrendChartCollectorId(Long trendChartCollectorId) {
        this.trendChartCollectorId = trendChartCollectorId;
    }

    public TimeResolutionType getResolutionType() {
        return resolutionType;
    }

    public void setResolutionType(TimeResolutionType resolutionType) {
        this.resolutionType = resolutionType;
    }

    public Integer getViewResolutionCount() {
        return viewResolutionCount;
    }

    public void setViewResolutionCount(Integer viewResolutionCount) {
        this.viewResolutionCount = viewResolutionCount;
    }

    public Integer getViewOffset() {
        return viewOffset;
    }

    public void setViewOffset(Integer viewOffset) {
        this.viewOffset = viewOffset;
    }

    public Integer getViewResolutionStep() {
        return viewResolutionStep;
    }

    public void setViewResolutionStep(Integer viewResolutionStep) {
        this.viewResolutionStep = viewResolutionStep;
    }

    @Override
    public String toString() {
        return "TrendChartDto{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", chartDisplayType=" + chartDisplayType +
                ", chartValueFormatType=" + chartValueFormatType +
                ", seriesType='" + seriesType + '\'' +
                ", valueType='" + valueType + '\'' +
                ", deleted=" + deleted +
                '}';
    }
}
