package com.cla.common.domain.dto.file;

import com.cla.common.utils.FileSizeUnits;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Created by: yael
 * Created on: 5/14/2019
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FileSizePartitionDto {

    private Long id;

    private FileSizeUnits fromType;
    private FileSizeUnits toType;

    private Integer fromValue;
    private Integer toValue;

    private String solrQuery;

    public String getLabel() {
        String label = fromValue + fromType.name();
        if (toType != null) {
            label += " - " + toValue + toType.name();
        } else {
            label = "> " + label;
        }
        return label;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public FileSizeUnits getFromType() {
        return fromType;
    }

    public void setFromType(FileSizeUnits fromType) {
        this.fromType = fromType;
    }

    public FileSizeUnits getToType() {
        return toType;
    }

    public void setToType(FileSizeUnits toType) {
        this.toType = toType;
    }

    public Integer getFromValue() {
        return fromValue;
    }

    public void setFromValue(Integer fromValue) {
        this.fromValue = fromValue;
    }

    public Integer getToValue() {
        return toValue;
    }

    public void setToValue(Integer toValue) {
        this.toValue = toValue;
    }

    @JsonIgnore
    public String getSolrQuery() {
        return solrQuery;
    }

    public void setSolrQuery(String solrQuery) {
        this.solrQuery = solrQuery;
    }

    @Override
    public String toString() {
        return "FileSizePartitionDto{" +
               getLabel() + '}';
    }
}
