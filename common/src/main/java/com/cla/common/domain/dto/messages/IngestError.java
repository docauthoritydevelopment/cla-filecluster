package com.cla.common.domain.dto.messages;

import com.cla.connector.domain.dto.messages.ProcessingError;

/**
 *
 * Created by vladi on 2/23/2017.
 */
public class IngestError extends ProcessingError {

    public static IngestError create(){
        return new IngestError();
    }

    @Override
    public String toString() {
        return "IngestError{" +
                "text='" + text + '\'' +
                "exceptionText='" + exceptionText + '\'' +
                ", exceptionType='" + exceptionType + '\'' +
                ", errorType='" + getErrorType() + '\'' +
                '}';
    }
}
