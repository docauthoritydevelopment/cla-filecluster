package com.cla.common.domain.dto.category;

import java.io.Serializable;
import java.util.List;

/**
 * Created by uri on 11/08/2015.
 */
public class MergeCategoryRequestDto implements Serializable {
    private List<Long> categoriesToMerge;

    private String newCategoryName;

    public List<Long> getCategoriesToMerge() {
        return categoriesToMerge;
    }

    public void setCategoriesToMerge(List<Long> categoriesToMerge) {
        this.categoriesToMerge = categoriesToMerge;
    }

    public String getNewCategoryName() {
        return newCategoryName;
    }

    public void setNewCategoryName(String newCategoryName) {
        this.newCategoryName = newCategoryName;
    }
}
