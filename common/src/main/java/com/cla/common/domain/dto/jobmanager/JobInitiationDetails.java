package com.cla.common.domain.dto.jobmanager;

/**
 * Created by Itai Marko.
 */
public class JobInitiationDetails {

    private boolean scannedFromScratch;

    public boolean isScannedFromScratch() {
        return scannedFromScratch;
    }

    public void setScannedFromScratch(boolean scaannedFromScratch) {
        this.scannedFromScratch = scaannedFromScratch;
    }
}
