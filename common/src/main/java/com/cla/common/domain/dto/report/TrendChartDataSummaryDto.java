package com.cla.common.domain.dto.report;

import java.io.Serializable;
import java.util.List;

/**
 * Created by uri on 27/07/2016.
 */
public class TrendChartDataSummaryDto implements Serializable {

    TrendChartDto trendChartDto;

    List<TrendChartSeriesDto> trendChartSeriesDtoList;

    long startTime;    // timestamp of first data points - requested time, rounded to a ‘resolution’ point.
    long endTime;      // timestamp of last data points
    Long viewResolutionMs;
    int  sampleCount;  // redundant but should be provided for extra validation / troubleshoot
    // count = (endTime-startTime)/resolution + 1;

    private TimeResolutionType resolutionType;      // default is TR_miliSec
    private Long count;
    private long viewOffsetMs;          // default is 0
    private int viewResolutionStep;     // default is 1

    public TrendChartDataSummaryDto() {
    }

    public TrendChartDataSummaryDto(TrendChartDto trendChartDto, List<TrendChartSeriesDto> trendChartSeriesDtoList,
                                    long startTime, long endTime,
                                    TrendChartResolutionDto tcResolutionDto,
                                    int sampleCount) {
        this.trendChartDto = trendChartDto;
        this.trendChartSeriesDtoList = trendChartSeriesDtoList;
        this.startTime = (tcResolutionDto == null) ? startTime : tcResolutionDto.calcPeriodStartMs(startTime, 0);
        this.endTime = (tcResolutionDto == null) ? endTime : tcResolutionDto.calcPeriodStartMs(endTime, 0);
        this.viewResolutionMs = null;
        if (tcResolutionDto != null) {
            this.viewResolutionMs = (tcResolutionDto.getResolutionType() == TimeResolutionType.TR_miliSec) ? tcResolutionDto.getCount() : null;
            this.resolutionType = tcResolutionDto.getResolutionType();
            this.count = tcResolutionDto.getCount();
            this.viewOffsetMs = tcResolutionDto.getViewOffsetMs();
            this.viewResolutionStep = tcResolutionDto.getViewResolutionStep();
        }
        this.sampleCount = sampleCount;

    }

    public TrendChartDto getTrendChartDto() {
        return trendChartDto;
    }

    public void setTrendChartDto(TrendChartDto trendChartDto) {
        this.trendChartDto = trendChartDto;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public long getViewResolutionMs() {
        return viewResolutionMs;
    }

    public int getSampleCount() {
        return sampleCount;
    }

    public void setSampleCount(int sampleCount) {
        this.sampleCount = sampleCount;
    }

    public List<TrendChartSeriesDto> getTrendChartSeriesDtoList() {
        return trendChartSeriesDtoList;
    }

    public void setTrendChartSeriesDtoList(List<TrendChartSeriesDto> trendChartSeriesDtoList) {
        this.trendChartSeriesDtoList = trendChartSeriesDtoList;
    }

    public void setViewResolutionMs(Long viewResolutionMs) {
        this.viewResolutionMs = viewResolutionMs;
    }

    public TimeResolutionType getResolutionType() {
        return resolutionType;
    }

    public void setResolutionType(TimeResolutionType resolutionType) {
        this.resolutionType = resolutionType;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public long getViewOffsetMs() {
        return viewOffsetMs;
    }

    public void setViewOffsetMs(long viewOffsetMs) {
        this.viewOffsetMs = viewOffsetMs;
    }

    public int getViewResolutionStep() {
        return viewResolutionStep;
    }

    public void setViewResolutionStep(int viewResolutionStep) {
        this.viewResolutionStep = viewResolutionStep;
    }

    @Override
    public String toString() {
        return "TrendChartDataSummaryDto{" +
                "trendChartDto=" + trendChartDto +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                ", viewResolutionMs=" + viewResolutionMs +
                ", sampleCount=" + sampleCount +
                '}';
    }
}
