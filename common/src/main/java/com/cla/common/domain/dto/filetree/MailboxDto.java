package com.cla.common.domain.dto.filetree;

/**
 * Created by: yael
 * Created on: 2/10/2019
 */
public class MailboxDto {

    private Long id;

    private Long rootFolderId;

    private String upn;

    private String syncState;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getRootFolderId() {
        return rootFolderId;
    }

    public void setRootFolderId(Long rootFolderId) {
        this.rootFolderId = rootFolderId;
    }

    public String getUpn() {
        return upn;
    }

    public void setUpn(String upn) {
        this.upn = upn;
    }

    public String getSyncState() {
        return syncState;
    }

    public void setSyncState(String syncState) {
        this.syncState = syncState;
    }

    @Override
    public String toString() {
        return "MailboxDto{" +
                "id=" + id +
                ", rootFolderId=" + rootFolderId +
                ", upn='" + upn + '\'' +
                ", syncState='" + syncState + '\'' +
                '}';
    }
}
