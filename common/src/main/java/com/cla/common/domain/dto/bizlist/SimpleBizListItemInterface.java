package com.cla.common.domain.dto.bizlist;

import com.cla.common.utils.AssociationIdUtils;

/**
 * Created by: yael
 * Created on: 5/5/2019
 */
public interface SimpleBizListItemInterface {
    BizListItemType getListType();
    Long getBizListId();
    String getSid();
    default String createBizListIdentifierString() {
        return AssociationIdUtils.createSimpleBizListItemIdentfier(this);
    }
}
