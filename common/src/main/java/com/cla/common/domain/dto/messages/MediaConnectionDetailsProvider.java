package com.cla.common.domain.dto.messages;

import com.cla.connector.domain.dto.media.MediaConnectionDetailsDto;

/**
 * This interface is used in order to bridge the gap between media connectors which use the file path as a unique identifier and those who use a custom id (i.e. box).
 * When the only identifier used by media connectors will be the mediaItemId, this interface could be retired.
 */
public interface MediaConnectionDetailsProvider {
	MediaConnectionDetailsDto getMediaConnectionDetailsDto();
	String getMediaEntityId();
}
