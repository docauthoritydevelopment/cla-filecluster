package com.cla.common.domain.dto.bizlist;

import java.io.Serializable;

/**
 * Created by uri on 22/03/2016.
 */
public class ConsumerTemplateDto implements Serializable{

    String name;

    String description;

    public ConsumerTemplateDto(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public ConsumerTemplateDto() {
    }

    public ConsumerTemplateDto(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
