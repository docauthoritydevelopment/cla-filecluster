package com.cla.common.domain.dto.pv;

import java.util.HashMap;
import java.util.Map;

public enum MaturityLevel {
	// types for Word processing 
	ML_Low(1)
	,ML_Med(2)
	,ML_High(3)
	,Deleted(-99)
	;
	
	public static int MAX_VALUE = ML_High.toInt();
	
	private static final Map<Integer, MaturityLevel> intMapping = new HashMap<Integer, MaturityLevel>();
	private final int value;
	private final String name;

	MaturityLevel(final int value) {
		this(value, "_"+value);
	}

	MaturityLevel(final int value, final String name) {
		this.value = value;
		this.name = name;
	}

	static {
		for (final MaturityLevel v : MaturityLevel.values()) {
			intMapping.put(v.value, v);
		}
	}

	public int toInt() {
		return value;
	}

	@Override
	public String toString() {
		return name;
	}

	public static MaturityLevel valueOf(final int value) {
		final MaturityLevel mt = MaturityLevel.intMapping.get(value);
		if (mt == null) {
			throw new RuntimeException("Invalid value: " + value);
		}
		return mt;
	}
}
