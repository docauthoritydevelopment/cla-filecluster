package com.cla.common.domain.dto.mcf.mcf_api.job;

import java.io.Serializable;

/**
 * ,"include":[
 *         {"_value_":""
 *         ,"_attribute_match":"*","_attribute_type":"file"}
 *         ,{"_value_":""
 *         ,"_attribute_match":"*"
 *         ,"_attribute_type":"directory"}
 *        ]
 *        }
 */
public class McfIncludeObjectDto implements Serializable {
    private String _value_;
    private String _attribute_match;
    private String _attribute_type;

    public McfIncludeObjectDto() {
    }

    public McfIncludeObjectDto(String _value_, String _attribute_match, String _attribute_type) {
        this._value_ = _value_;
        this._attribute_match = _attribute_match;
        this._attribute_type = _attribute_type;
    }

    public String get_value_() {
        return _value_;
    }

    public void set_value_(String _value_) {
        this._value_ = _value_;
    }

    public String get_attribute_match() {
        return _attribute_match;
    }

    public void set_attribute_match(String _attribute_match) {
        this._attribute_match = _attribute_match;
    }

    public String get_attribute_type() {
        return _attribute_type;
    }

    public void set_attribute_type(String _attribute_type) {
        this._attribute_type = _attribute_type;
    }
}
