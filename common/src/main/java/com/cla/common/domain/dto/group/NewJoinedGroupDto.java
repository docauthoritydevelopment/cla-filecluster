package com.cla.common.domain.dto.group;

import com.google.common.base.Joiner;

import java.io.Serializable;
import java.util.List;

/**
 * Created by uri on 28/06/2016.
 */
public class NewJoinedGroupDto implements Serializable {

    private String groupName;

    private List<String> childrenGroupIds;

    public NewJoinedGroupDto(String groupName) {
        this.groupName = groupName;
    }

    public NewJoinedGroupDto() {
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public List<String> getChildrenGroupIds() {
        return childrenGroupIds;
    }

    public void setChildrenGroupIds(List<String> childrenGroupIds) {
        this.childrenGroupIds = childrenGroupIds;
    }

    @Override
    public String toString() {
        return "NewJoinedGroupDto{" +
                "groupName='" + groupName + '\'' +
                ", childrenGroupIds:[" + Joiner.on(", ").join(childrenGroupIds) + "]}";
    }
}
