package com.cla.common.domain.dto.dataCenter;

import com.cla.common.domain.dto.filetree.RootFolderDto;

/**
 * Created by liora on 09/07/2017.
 */
public class CustomerDataCenterSummaryInfo {

    private CustomerDataCenterDto customerDataCenterDto;
    private Long rootFolderCount;
    private Integer mediaProcessorCount;

    public CustomerDataCenterDto getCustomerDataCenterDto() {
        return customerDataCenterDto;
    }

    public void setCustomerDataCenterDto(CustomerDataCenterDto customerDataCenterDto) {
        this.customerDataCenterDto = customerDataCenterDto;
    }

    public Long getRootFolderCount() {
        return rootFolderCount;
    }

    public void setRootFolderCount(Long rootFolderCount) {
        this.rootFolderCount = rootFolderCount;
    }

    public Integer getMediaProcessorCount() {
        return mediaProcessorCount;
    }

    public void setMediaProcessorCount(Integer mediaProcessorCount) {
        this.mediaProcessorCount = mediaProcessorCount;
    }

    @Override
    public String toString() {
        return "CustomerDataCenterSummaryInfo{" +
                ", customerDataCenterDto='" + customerDataCenterDto + '\'' +
                ", rootFolderCount='" + rootFolderCount + '\'' +
                ", mediaProcessorCount='" + mediaProcessorCount + '\'' +
                '}';
    }
}
