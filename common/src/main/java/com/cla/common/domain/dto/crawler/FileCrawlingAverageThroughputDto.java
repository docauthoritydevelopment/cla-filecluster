package com.cla.common.domain.dto.crawler;

/**
 *
 * Created by uri on 06/10/2015.
 */
public class FileCrawlingAverageThroughputDto {

    private final double averageScanThroughput;
    private double averageAnalyzeThroughput;

    private double averageWordIngestThroughput = 0;
    private double averageExcelIngestThroughput = 0;
    private double averagePdfIngestThroughput = 0;

    public FileCrawlingAverageThroughputDto(double averageScanThroughput, double averageAnalyzeThroughput) {
        this.averageScanThroughput = averageScanThroughput;
        this.averageAnalyzeThroughput = averageAnalyzeThroughput;

    }

    public double getAverageAnalyzeThroughput() {
        return averageAnalyzeThroughput;
    }

    public void setAverageAnalyzeThroughput(double averageAnalyzeThroughput) {
        this.averageAnalyzeThroughput = averageAnalyzeThroughput;
    }

    public double getAverageScanThroughput() {
        return averageScanThroughput;
    }

    public double getAverageWordIngestThroughput() {
        return averageWordIngestThroughput;
    }

    public double getAverageExcelIngestThroughput() {
        return averageExcelIngestThroughput;
    }

    public double getAveragePdfIngestThroughput() {
        return averagePdfIngestThroughput;
    }
}
