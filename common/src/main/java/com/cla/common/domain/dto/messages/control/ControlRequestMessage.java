package com.cla.common.domain.dto.messages.control;

import com.cla.common.domain.dto.messages.ProcessingPhaseMessage;
import com.cla.common.domain.dto.messages.RequestMessage;

/**
 *
 * Created by uri on 16-May-17.
 */
public class ControlRequestMessage extends ProcessingPhaseMessage<ControlMessagePayload, ControlProcessingError> implements RequestMessage {


    private ControlRequestType type;

    private String mediaProcessorId;

    public ControlRequestMessage() {
        super();
    }


    public ControlRequestMessage(ControlMessagePayload payload, ControlRequestType type) {
        super(payload);
        this.type = type;
    }


    public ControlRequestType getType() {
        return type;
    }

    public void setType(ControlRequestType type) {
        this.type = type;
    }

    public String getMediaProcessorId() {
        return mediaProcessorId;
    }

    public void setMediaProcessorId(String mediaProcessorId) {
        this.mediaProcessorId = mediaProcessorId;
    }
}
