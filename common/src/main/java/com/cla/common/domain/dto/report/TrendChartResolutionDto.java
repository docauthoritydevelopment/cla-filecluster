package com.cla.common.domain.dto.report;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

/**
 *
 * Created by Itay on 28/08/2016.
 */
public class TrendChartResolutionDto implements Serializable {

    private TimeResolutionType resolutionType;      // default is TR_miliSec
    private long count;
    private long viewOffsetMs;          // default is 0
    private int viewResolutionStep;     // default is 1

    public TrendChartResolutionDto(long count) {
        this(TimeResolutionType.TR_miliSec, count, 0L, 1);
    }

    public TrendChartResolutionDto(TimeResolutionType resolutionType, long count) {
        this(resolutionType, count, 0L, 1);
    }

    public TrendChartResolutionDto(TimeResolutionType resolutionType, long count, long viewOffsetMs) {
        this(resolutionType, count, viewOffsetMs, 1);
    }

    public TrendChartResolutionDto(TimeResolutionType resolutionType, long count, Long viewOffsetMs, Integer viewResolutionStep) {
        this.resolutionType = resolutionType;
        this.count = count;
        this.viewOffsetMs = (viewOffsetMs==null)?0:viewOffsetMs;
        this.viewResolutionStep = (viewResolutionStep==null)?1:viewResolutionStep;
    }

    public TrendChartResolutionDto(TrendChartDto trendChartDto) {
        resolutionType  = trendChartDto.getResolutionType();
        if (resolutionType ==null) {
            resolutionType = TimeResolutionType.TR_miliSec;
        }
        count = (resolutionType==TimeResolutionType.TR_miliSec)?trendChartDto.getViewResolutionMs():trendChartDto.getViewSampleCount();
        viewOffsetMs = (trendChartDto.getViewOffset()==null)?0:trendChartDto.getViewOffset();
        viewResolutionStep = (trendChartDto.getViewResolutionStep()==null)?1:trendChartDto.getViewResolutionStep();
        if (viewResolutionStep==0) {
            throw new RuntimeException("Zero viewResolutionStep. " + toString());
        }
    }

    public TimeResolutionType getResolutionType() {
        return resolutionType;
    }

    public void setResolutionType(TimeResolutionType resolutionType) {
        this.resolutionType = resolutionType;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    public long getViewOffsetMs() {
        return viewOffsetMs;
    }

    public void setViewOffsetMs(long viewOffsetMs) {
        this.viewOffsetMs = viewOffsetMs;
    }

    public Integer getViewResolutionStep() {
        return viewResolutionStep;
    }

    public void setViewResolutionStep(Integer viewResolutionStep) {
        this.viewResolutionStep = viewResolutionStep;
    }

    @Override
    public String toString() {
        return "TrendChartResolutionDto{" +
                "resolutionType=" + resolutionType +
                ", count=" + count +
                ", viewOffsetMs=" + viewOffsetMs +
                ", viewResolutionStep=" + viewResolutionStep +
                '}';
    }

    public long calcPeriodStartMs(long epocTimeMs, int periods) {
        long res = 0;
        LocalDateTime lt;
        int year;
        int month;
        switch (resolutionType) {
            case TR_miliSec:
                res = epocTimeMs + periods * count;
                break;
            case TR_hour:
            case TR_day:
            case TR_week:
                long periodMs = periodTypicalMS(resolutionType);
                long alignMs = periodAlignMS(resolutionType);
                res = epocTimeMs - (epocTimeMs % alignMs) + periods * periodMs * viewResolutionStep + viewOffsetMs;
                break;

            case TR_month:
                lt = LocalDateTime.ofEpochSecond(epocTimeMs / 1000, 0, ZoneOffset.UTC);
                year = lt.getYear();
                month = lt.getMonthValue() + periods * viewResolutionStep;
                res = LocalDateTime.of(year + ((month-1)/12),((month-1)%12)+1,1,0,0).toEpochSecond(ZoneOffset.UTC)*1000 + viewOffsetMs;
                break;

            case TR_quarter:
                lt = LocalDateTime.ofEpochSecond(epocTimeMs / 1000, 0, ZoneOffset.UTC);
                year = lt.getYear();
                month = (lt.getMonthValue() - 1) % 3 + 1 + 3 * periods * viewResolutionStep;
                res = LocalDateTime.of(year + ((month-1)/12),((month-1)%12)+1,1,0,0).toEpochSecond(ZoneOffset.UTC)*1000 + viewOffsetMs;
                break;

            case TR_year:
                lt = LocalDateTime.ofEpochSecond(epocTimeMs / 1000, 0, ZoneOffset.UTC);
                year = lt.getYear();
                month = lt.getMonthValue() + 12 * periods * viewResolutionStep;
                res = LocalDateTime.of(year + ((month-1)/12),((month-1)%12)+1,1,0,0).toEpochSecond(ZoneOffset.UTC)*1000 + viewOffsetMs;
                break;
        }
        return res;
    }

    public int calcSampleCount(long startTimeMs, long endTimeMs) {
        int res = 0;
        LocalDateTime lts;
        LocalDateTime lte;

        switch (resolutionType) {
            case TR_miliSec:
                res = (endTimeMs <= startTimeMs)?1:(int)((endTimeMs - startTimeMs - 1) / count + 2);
                break;
            case TR_hour:
            case TR_day:
            case TR_week:
                long periodMs = periodTypicalMS(resolutionType);
                startTimeMs = calcPeriodStartMs(startTimeMs, 0);
                endTimeMs = calcPeriodStartMs(endTimeMs, 0);
                res = (endTimeMs <= startTimeMs)?1:(int)((endTimeMs - startTimeMs - 1) / periodMs + 2);
                break;

            case TR_month:
                lts = LocalDateTime.ofEpochSecond(startTimeMs / 1000, 0, ZoneOffset.UTC);
                lte = LocalDateTime.ofEpochSecond(endTimeMs / 1000, 0, ZoneOffset.UTC);
                res = (lte.getYear() - lts.getYear()) * 12 + lte.getMonthValue() - lts.getMonthValue() + 1;
                break;

            case TR_quarter:
                lts = LocalDateTime.ofEpochSecond(startTimeMs / 1000, 0, ZoneOffset.UTC);
                lte = LocalDateTime.ofEpochSecond(endTimeMs / 1000, 0, ZoneOffset.UTC);
                res = ((lte.getYear() - lts.getYear()) * 12 + lte.getMonthValue() - lts.getMonthValue()) / 3 + 1;
                break;

            case TR_year:
                lts = LocalDateTime.ofEpochSecond(startTimeMs / 1000, 0, ZoneOffset.UTC);
                lte = LocalDateTime.ofEpochSecond(endTimeMs / 1000, 0, ZoneOffset.UTC);
//                res = lt3e.getYear() - lt3s.getYear() + 1;
                res = ((lte.getYear() - lts.getYear()) * 12 + lte.getMonthValue() - lts.getMonthValue()) / 12 + 1;
                break;
        }
        return res;
    }

    private long periodTypicalMS(TimeResolutionType resolutionType) {
        long res = 1;
        switch (resolutionType) {
            case TR_hour:
                res = 3600_000;
                break;
            case TR_day:
                res = 24 * 3600_000;
                break;
            case TR_week:
                res = 7 * 24 * 3600_000;
                break;
            case TR_month:
                res = 30 * 24 * 3600_000;
                break;
            case TR_quarter:
                res = 90 * 24 * 3600_000;
                break;
            case TR_year:
                res = 365 * 24 * 3600_000;
                break;
        }
        return res;
    }

    private long periodAlignMS(TimeResolutionType resolutionType) {
        switch (resolutionType) {
            case TR_hour:
            case TR_day:
                return periodTypicalMS(resolutionType);
            case TR_week:
            case TR_month:
            case TR_quarter:
                return periodTypicalMS(TimeResolutionType.TR_day);
            case TR_year:
                return periodTypicalMS(TimeResolutionType.TR_month);
        }
        return 1;
    }

    long calcPeriodEndMs(long epocTimeMs, int periods) {
        return calcPeriodStartMs(epocTimeMs, periods + 1) - 1;
    }
}
