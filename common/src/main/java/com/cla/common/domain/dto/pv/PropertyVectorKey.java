package com.cla.common.domain.dto.pv;

import java.io.Serializable;


@SuppressWarnings("serial")
public class PropertyVectorKey implements Serializable {
	private Long docId;

	private String part=PropertyVector.DEF_SYS_PART;
	
	private int pvType;


	public PropertyVectorKey() {
	}

	public PropertyVectorKey(final Long docId, final int pvType) {
		this.docId = docId;
		this.pvType = pvType;
	}
	
	public PropertyVectorKey(final Long docId, final String part,final int pvType) {
		this.docId = docId;
		this.pvType = pvType;
		this.part=part;
	}

	public PropertyVectorKey(final PropertyVectorKey other) {
		this.docId = other.docId;
		this.pvType = other.pvType;
		this.part=other.part;
	}

	// @PrimaryKeyColumn(ordinal = 1, type = PrimaryKeyType.CLUSTERED)
	// private long key;

	public int getPvType() {
		return pvType;
	}

	public void setPv(final int pv) {
		this.pvType = pv;
	}


	public long getDocId() {
		return docId;
	}

	public void setDocId(final long docId) {
		this.docId = docId;
	}

	public String getPart() {
		return part;
	}
	
	

	public void setPart(final String part) {
		this.part = part;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((docId == null) ? 0 : docId.hashCode());
		result = prime * result + ((part == null) ? 0 : part.hashCode());
		result = prime * result + pvType;
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final PropertyVectorKey other = (PropertyVectorKey) obj;
		if (docId == null) {
			if (other.docId != null)
				return false;
		} else if (!docId.equals(other.docId))
			return false;
		if (part == null) {
			if (other.part != null)
				return false;
		} else if (!part.equals(other.part))
			return false;
		if (pvType != other.pvType)
			return false;
		return true;
	}

    @Override
    public String toString() {
        return "pvk{" +
                "docId=" + docId +
                ", part='" + part + '\'' +
                ", pvType=" + pvType +
                '}';
    }
}
