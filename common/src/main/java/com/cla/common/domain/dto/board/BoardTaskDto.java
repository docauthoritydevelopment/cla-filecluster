package com.cla.common.domain.dto.board;

import lombok.*;
import lombok.experimental.Accessors;

@NoArgsConstructor
@Getter
@Setter
@Accessors(chain = true)
@ToString
@EqualsAndHashCode
public class BoardTaskDto {

    private Long id;

    private long boardId;

    private long ownerId;

    private long assignee;

    private long dueDate;

    private BoardTaskType boardTaskType;

    private BoardTaskState boardTaskState;

    private String entityId;

    private BoardEntityType boardEntityType;

    private Long dateCreated;

    private Long dateModified;
}
