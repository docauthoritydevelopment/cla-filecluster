package com.cla.common.domain.dto.kendo;

/**
 * Created by uri on 07/07/2015.
 */
public class SortDescriptor {
    private String field;
    private String dir;

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getDir() {
        return dir;
    }

    public void setDir(String dir) {
        this.dir = dir;
    }
}
