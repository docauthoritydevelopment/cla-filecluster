package com.cla.common.domain.dto.filetree;

/**
 * Created by yael on 11/9/2017.
 */
public enum EntityType {
    ROOT_FOLDER("ROOT_FOLDER")
    ;

    private String name;

    EntityType(String s) {
        name = s;
    }

    public String getName() {
        return name;
    }
}
