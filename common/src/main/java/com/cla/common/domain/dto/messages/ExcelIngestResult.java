package com.cla.common.domain.dto.messages;

import com.cla.common.domain.dto.extraction.SearchPatternCountDto;
import com.cla.common.domain.dto.file.ExcelAnalysisMetadata;
import com.cla.common.domain.dto.file.FileCollections;
import com.cla.common.domain.dto.mediaproc.SearchPatternServiceInterface;
import com.cla.common.domain.dto.mediaproc.TextualResult;
import com.cla.common.domain.dto.mediaproc.excel.ExcelWorkbook;
import com.cla.common.domain.dto.mediaproc.excel.ExcelWorkbookMetaData;
import com.cla.common.utils.json.ExcelAnalysisMetadataDeserializer;
import com.cla.common.utils.json.ExcelAnalysisMetadataSerializer;
import com.cla.common.utils.json.FileCollectionsDeserializer;
import com.cla.common.utils.json.FileCollectionsSerializer;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.List;
import java.util.Set;

/**
 * Post-processed ingested excel workbook, ready for persistence and analysis
 * @see ExcelWorkbook
 * Created by vladi on 3/7/2017.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ExcelIngestResult extends IngestMessagePayload<ExcelWorkbookMetaData> implements TextualResult {

    private List<String> combinedProposedTitles;
    private String csvValue;
    private FileCollections fileCollections;
    private ExcelAnalysisMetadata sheetsMetadata;
    private List<ExcelSheetIngestResult> sheets;

    public List<String> getCombinedProposedTitles() {
        return combinedProposedTitles;
    }

    public void setCombinedProposedTitles(List<String> combinedProposedTitles) {
        this.combinedProposedTitles = combinedProposedTitles;
    }

    public List<ExcelSheetIngestResult> getSheets() {
        return sheets;
    }

    public void setSheets(List<ExcelSheetIngestResult> sheets) {
        this.sheets = sheets;
    }

    @JsonSerialize(using = FileCollectionsSerializer.class)
    public FileCollections getFileCollections() {
        return fileCollections;
    }

    @JsonDeserialize(using = FileCollectionsDeserializer.class)
    public void setFileCollections(FileCollections fileCollections) {
        this.fileCollections = fileCollections;
    }

    @JsonSerialize(using = ExcelAnalysisMetadataSerializer.class)
    public ExcelAnalysisMetadata getSheetsMetadata() {
        return sheetsMetadata;
    }

    @JsonDeserialize(using = ExcelAnalysisMetadataDeserializer.class)
    public void setSheetsMetadata(ExcelAnalysisMetadata sheetsMetadata) {
        this.sheetsMetadata = sheetsMetadata;
    }

    public String getCsvValue() {
        return csvValue;
    }

    public void setCsvValue(String csvValue) {
        this.csvValue = csvValue;
    }

    public void handleSearchPatterns(String requestId, SearchPatternServiceInterface searchPatternService) {
        if (getSheets() != null) {
            getSheets().forEach(sheet -> {
                if (sheet.getOrigContentTokens() != null) {
                    Set<SearchPatternCountDto> patternCountDtos = searchPatternService.getSearchPatternsCounting(sheet.getOrigContentTokens());
                    sheet.setSearchPatternsCounting(patternCountDtos);
                    if (requestId == null) {
                        sheet.setOrigContentTokens(null);
                    }
                }
            });
        }
    }

    @Override
    public String toString() {
        return "ExcelIngestResult{" +
                "combinedProposedTitles=" + combinedProposedTitles +
                ", csvValue='" + csvValue + '\'' +
                ", sheetsMetadata=" + sheetsMetadata +
                ", sheets=" + sheets +
                ", metadataOnly=" + metadataOnly +
                ", claFileProperties=" + claFileProperties +
                ", fileName='" + fileName + '\'' +
                '}';
    }
}
