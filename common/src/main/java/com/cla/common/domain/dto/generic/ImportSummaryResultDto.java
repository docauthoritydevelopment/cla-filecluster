package com.cla.common.domain.dto.generic;

import java.io.Serializable;
import java.util.List;

/**
 * Created by liora on 21/03/2017.
 */
public class ImportSummaryResultDto implements Serializable {

    private String resultDescription;

    private Long importId;

    private boolean importFailed;

    private int importedRowCount;

    private int duplicateRowCount;
    private int errorRowCount;
    private int warningRowCount;

    private String[] rowHeaders;
    private List<ImportRowResultDto> errorImportRowResultDtos;
    private List<ImportRowResultDto> duplicateImportRowResultDtos;



    private List<ImportRowResultDto> warningImportRowResultDtos;

    public String getResultDescription() {
        return resultDescription;
    }

    public void setResultDescription(String resultDescription) {
        this.resultDescription = resultDescription;
    }
    public List<ImportRowResultDto> getErrorImportRowResultDtos() {
        return errorImportRowResultDtos;
    }

    public void setErrorImportRowResultDtos(List<ImportRowResultDto> errorImportRowResultDtos) {
        this.errorImportRowResultDtos = errorImportRowResultDtos;
    }

    public List<ImportRowResultDto> getWarningImportRowResultDtos() {
        return warningImportRowResultDtos;
    }

    public void setWarningImportRowResultDtos(List<ImportRowResultDto> warningImportRowResultDtos) {
        this.warningImportRowResultDtos = warningImportRowResultDtos;
    }
    public List<ImportRowResultDto> getDuplicateImportRowResultDtos() {
        return duplicateImportRowResultDtos;
    }

    public void setDuplicateImportRowResultDtos(List<ImportRowResultDto> duplicateImportRowResultDtos) {
        this.duplicateImportRowResultDtos = duplicateImportRowResultDtos;
    }
    public Long getImportId() {
        return importId;
    }

    public void setImportId(Long importId) {
        this.importId = importId;
    }

    public int getImportedRowCount() {
        return importedRowCount;
    }

    public void setImportedRowCount(int importedRowCount) {
        this.importedRowCount = importedRowCount;
    }

    public int getDuplicateRowCount() {
        return duplicateRowCount;
    }

    public void setDuplicateRowCount(int duplicateRowCount) {
        this.duplicateRowCount = duplicateRowCount;
    }

    public void setErrorRowCount(int errorRowCount) {
        this.errorRowCount = errorRowCount;
    }

    public int getErrorRowCount() {
        return errorRowCount;
    }
    public int getWarningRowCount() {
        return warningRowCount;
    }

    public void setWarningRowCount(int warningRowCount) {
        this.warningRowCount = warningRowCount;
    }
    public boolean isImportFailed() {
        return importFailed;
    }

    public void setImportFailed(boolean importFailed) {
        this.importFailed = importFailed;
    }

    public String[] getRowHeaders() {
        return rowHeaders;
    }

    public void setRowHeaders(String[] rowHeaders) {
        this.rowHeaders = rowHeaders;
    }
    @Override
    public String toString() {
        return "ImportSummaryResultDto{" +
                "resultDescription='" + resultDescription + '\'' +
                ", importId=" + importId +
                ", importFailed=" + importFailed +
                ", importedRowCount=" + importedRowCount +
                ", duplicateRowCount=" + duplicateRowCount +
                ", warningRowCount=" + warningRowCount +
                ", errorRowCount=" + errorRowCount +
                ", errorImportRowResultDtos=" + errorImportRowResultDtos +
                '}';
    }


}
