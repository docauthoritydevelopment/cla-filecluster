package com.cla.common.domain.dto.components;

/**
 * Created by: yael
 * Created on: 2/21/2018
 */
public class MediaProcessorStatus extends ComponentStatus {
    private long startScanTaskRequests;
    private long startScanTaskRequestsFinished;
    private long scanTaskResponsesFinished;
    private long scanTaskFailedResponsesFinished;
    private long activeScanTasks;

    private long ingestTaskRequests;
    private long ingestTaskRequestsFinished;
    private long ingestTaskRequestsFinishedWithError;
    private long ingestTaskRequestFailure;

    private double averageIngestThroughput =0;
    private float scanRunningThroughput = 0f;
    private double ingestRunningThroughput = 0d;
    private double averageScanThroughput = 0f;

    private long failedSignatureCheck;

    private String version;

    public long getStartScanTaskRequests() {
        return startScanTaskRequests;
    }

    public void setStartScanTaskRequests(long startScanTaskRequests) {
        this.startScanTaskRequests = startScanTaskRequests;
    }

    public long getStartScanTaskRequestsFinished() {
        return startScanTaskRequestsFinished;
    }

    public void setStartScanTaskRequestsFinished(long startScanTaskRequestsFinished) {
        this.startScanTaskRequestsFinished = startScanTaskRequestsFinished;
    }

    public long getScanTaskResponsesFinished() {
        return scanTaskResponsesFinished;
    }

    public void setScanTaskResponsesFinished(long scanTaskResponsesFinished) {
        this.scanTaskResponsesFinished = scanTaskResponsesFinished;
    }

    public long getScanTaskFailedResponsesFinished() {
        return scanTaskFailedResponsesFinished;
    }

    public void setScanTaskFailedResponsesFinished(long scanTaskFailedResponsesFinished) {
        this.scanTaskFailedResponsesFinished = scanTaskFailedResponsesFinished;
    }

    public long getActiveScanTasks() {
        return activeScanTasks;
    }

    public void setActiveScanTasks(long activeScanTasks) {
        this.activeScanTasks = activeScanTasks;
    }

    public long getIngestTaskRequests() {
        return ingestTaskRequests;
    }

    public void setIngestTaskRequests(long ingestTaskRequests) {
        this.ingestTaskRequests = ingestTaskRequests;
    }

    public long getIngestTaskRequestsFinished() {
        return ingestTaskRequestsFinished;
    }

    public void setIngestTaskRequestsFinished(long ingestTaskRequestsFinished) {
        this.ingestTaskRequestsFinished = ingestTaskRequestsFinished;
    }

    public long getIngestTaskRequestsFinishedWithError() {
        return ingestTaskRequestsFinishedWithError;
    }

    public void setIngestTaskRequestsFinishedWithError(long ingestTaskRequestsFinishedWithError) {
        this.ingestTaskRequestsFinishedWithError = ingestTaskRequestsFinishedWithError;
    }

    public long getIngestTaskRequestFailure() {
        return ingestTaskRequestFailure;
    }

    public void setIngestTaskRequestFailure(long ingestTaskRequestFailure) {
        this.ingestTaskRequestFailure = ingestTaskRequestFailure;
    }

    public double getAverageIngestThroughput() {
        return averageIngestThroughput;
    }

    public void setAverageIngestThroughput(double averageIngestThroughput) {
        this.averageIngestThroughput = averageIngestThroughput;
    }

    public float getScanRunningThroughput() {
        return scanRunningThroughput;
    }

    public void setScanRunningThroughput(float scanRunningThroughput) {
        this.scanRunningThroughput = scanRunningThroughput;
    }

    public double getIngestRunningThroughput() {
        return ingestRunningThroughput;
    }

    public void setIngestRunningThroughput(double ingestRunningThroughput) {
        this.ingestRunningThroughput = ingestRunningThroughput;
    }

    public double getAverageScanThroughput() {
        return averageScanThroughput;
    }

    public void setAverageScanThroughput(double averageScanThroughput) {
        this.averageScanThroughput = averageScanThroughput;
    }

    public long getFailedSignatureCheck() {
        return failedSignatureCheck;
    }

    public void setFailedSignatureCheck(long failedSignatureCheck) {
        this.failedSignatureCheck = failedSignatureCheck;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    @Override
    public String toString() {
        return "MediaProcessorStatus{" +
                "startScanTaskRequests=" + startScanTaskRequests +
                ", startScanTaskRequestsFinished=" + startScanTaskRequestsFinished +
                ", scanTaskResponsesFinished=" + scanTaskResponsesFinished +
                ", scanTaskFailedResponsesFinished=" + scanTaskFailedResponsesFinished +
                ", activeScanTasks=" + activeScanTasks +
                ", ingestTaskRequests=" + ingestTaskRequests +
                ", ingestTaskRequestsFinished=" + ingestTaskRequestsFinished +
                ", ingestTaskRequestsFinishedWithError=" + ingestTaskRequestsFinishedWithError +
                ", ingestTaskRequestFailure=" + ingestTaskRequestFailure +
                ", averageIngestThroughput=" + averageIngestThroughput +
                ", scanRunningThroughput=" + scanRunningThroughput +
                ", ingestRunningThroughput=" + ingestRunningThroughput +
                ", averageScanThroughput=" + averageScanThroughput +
                ", failedSignatureCheck=" + failedSignatureCheck +
                ", version=" + version +
                ", ComponentStatus=" + super.toString() +
                '}';
    }
}
