package com.cla.common.domain.dto.report;

import com.google.common.base.MoreObjects;

import java.io.Serializable;

/**
 * Created by uri on 07/10/2015.
 */
public class CounterReportDto implements Serializable{
    String name;
    String description;
    long counter;

    public CounterReportDto() {
    }

    public CounterReportDto(String name, long counter) {
        this.name = name;
        this.counter = counter;
    }

    public CounterReportDto(String name, String description, long counter) {
        this.name = name;
        this.description = description;
        this.counter = counter;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getCounter() {
        return counter;
    }

    public void setCounter(long counter) {
        this.counter = counter;
    }

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
				.add("name", name)
				.add("description", description)
				.add("counter", counter)
				.toString();
	}
}
