package com.cla.common.domain.dto.excel;

import com.google.common.collect.Maps;

import java.util.Map;

/**
 *
 * Created by uri on 18/10/2015.
 */
public class ExcelSheetDimensions {

    private int maxColumn;
    private int maxRow;
    private int nonBlankRows;
    private Map<Integer,Integer> nonBlankCellCount = Maps.newHashMap();


    public void updateColumn(short column) {
        maxColumn = Math.max(maxColumn,Short.valueOf(column).intValue());
    }

    public int getMaxColumn() {
        return maxColumn;
    }

    public void updateRow(int row) {
        maxRow = Math.max(maxRow,row);
    }

    public int getMaxRow() {
        return maxRow;
    }

    public void addNonBlankRow() {
        nonBlankRows++;
    }

    public int getNonBlankRows() {
        return nonBlankRows;
    }

    public void addNonBlankCell(int row) {
        Integer count = nonBlankCellCount.get(row);
        if (count == null) {
            nonBlankCellCount.put(row,1);
        }
        else {
            nonBlankCellCount.put(row,count+1);
        }
    }

    public Map<Integer, Integer> getNonBlankCellCount() {
        return nonBlankCellCount;
    }

    @Override
    public String toString() {
        return "ExcelSheetDimensions{" +
                "maxColumn=" + maxColumn +
                ", maxRow=" + maxRow +
                ", nonBlankRows=" + nonBlankRows +
//                ", nonBlankCellCount=" + nonBlankCellCount +
                '}';
    }
}
