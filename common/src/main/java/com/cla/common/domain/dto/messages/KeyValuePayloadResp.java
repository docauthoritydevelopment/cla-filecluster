package com.cla.common.domain.dto.messages;

import com.google.common.base.MoreObjects;

import java.util.Map;

public class KeyValuePayloadResp extends FileContentResultMessage {
	Map<String,String> mapPayload;

	public KeyValuePayloadResp(Map<String, String> mapPayload) {
		super(null);
		this.mapPayload = mapPayload;
		this.setPayloadType(MessagePayloadType.KEY_VALUE_RESPONSE);
	}

	//default constructor needed by serializer
	public KeyValuePayloadResp() {
		super(null);
	}

	public Map<String, String> getMapPayload() {
		return mapPayload;
	}

	public KeyValuePayloadResp setMapPayload(Map<String, String> mapPayload) {
		this.mapPayload = mapPayload;
		return this;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
				.add("mapPayload", mapPayload)
				.toString();
	}
}
