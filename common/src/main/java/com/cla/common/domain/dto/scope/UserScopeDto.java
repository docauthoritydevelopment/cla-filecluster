package com.cla.common.domain.dto.scope;

import com.cla.common.domain.dto.security.UserDto;

import javax.validation.constraints.NotNull;

public class UserScopeDto implements Scope<UserDto> {

    private Long id;

    @NotNull
    private Long dateCreated;

    @NotNull
    private Long dateModified;

    private UserDto user;


    @Override
    public UserDto getScopeBindings() {
        return user;
    }

    public void setScopeBindings(UserDto user) {
        this.user = user;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Long dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Long getDateModified() {
        return dateModified;
    }

    public void setDateModified(Long dateModified) {
        this.dateModified = dateModified;
    }

}
