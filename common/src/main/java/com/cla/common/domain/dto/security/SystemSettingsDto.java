package com.cla.common.domain.dto.security;

import com.cla.connector.domain.dto.JsonSerislizableVisible;

public class SystemSettingsDto implements JsonSerislizableVisible {

    private Long id;

    private String name;

    private String value;

    private long createdTimeStampMs;

    private long modifiedTimeStampMs;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public long getCreatedTimeStampMs() {
        return createdTimeStampMs;
    }

    public void setCreatedTimeStampMs(long createdTimeStampMs) {
        this.createdTimeStampMs = createdTimeStampMs;
    }

    public long getModifiedTimeStampMs() {
        return modifiedTimeStampMs;
    }

    public void setModifiedTimeStampMs(long modifiedTimeStampMs) {
        this.modifiedTimeStampMs = modifiedTimeStampMs;
    }

    @Override
    public String toString() {
        return "SystemSettingsDto{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", value='" + value + '\'' +
                ", createdTimeStampMs=" + createdTimeStampMs +
                ", modifiedTimeStampMs=" + modifiedTimeStampMs +
                '}';
    }
}
