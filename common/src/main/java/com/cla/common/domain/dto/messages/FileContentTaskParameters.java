package com.cla.common.domain.dto.messages;

import com.cla.common.domain.dto.filetree.RootFolderDto;
import com.cla.connector.domain.dto.media.MediaConnectionDetailsDto;

import java.util.Set;

/**
 * Created by liron on 4/14/2017.
 */
public class FileContentTaskParameters extends TaskParameters implements MediaConnectionDetailsProvider {

	private String requestId;
    private FileContentType fileContentType;
    private MediaConnectionDetailsDto mediaConnectionDetailsDto;
    private String fileName;
    private String baseName;
    private Set<String> textTokens;
    private RootFolderDto rootFolderDto;
    private boolean test;
    private Long instanceId;
    private boolean forceAccessibilityCheck = false;

    private FileContentTaskParameters() {
    }

    public static FileContentTaskParameters create() {
        return new FileContentTaskParameters();
    }

    public FileContentType getFileContentType() {
        return fileContentType;
    }

    public void setFileContentType(FileContentType fileContentType) {
        this.fileContentType = fileContentType;
    }

    public MediaConnectionDetailsDto getMediaConnectionDetailsDto() {
        return mediaConnectionDetailsDto;
    }

    public void setMediaConnectionDetailsDto(MediaConnectionDetailsDto mediaConnectionDetailsDto) {
        this.mediaConnectionDetailsDto = mediaConnectionDetailsDto;
    }

    public String getBaseName() {
        return baseName;
    }

    public void setBaseName(String baseName) {
        this.baseName = baseName;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public Set<String> getTextTokens() {
        return textTokens;
    }

    public void setTextTokens(Set<String> textTokens) {
        this.textTokens = textTokens;
    }

    public RootFolderDto getRootFolderDto() {
        return rootFolderDto;
    }

    public void setRootFolderDto(RootFolderDto rootFolderDto) {
        this.rootFolderDto = rootFolderDto;
    }

    /**
     * Specifies task is test-connection
     * @return
     */
    public boolean isTest() {
        return test;
    }

    public void setTest(boolean test) {
        this.test = test;
    }

    public Long getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(Long instanceId) {
        this.instanceId = instanceId;
    }

    public boolean isForceAccessibilityCheck() {
        return forceAccessibilityCheck;
    }

    public void setForceAccessibilityCheck(boolean forceAccessibilityCheck) {
        this.forceAccessibilityCheck = forceAccessibilityCheck;
    }
}
