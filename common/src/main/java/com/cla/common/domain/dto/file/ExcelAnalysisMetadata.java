package com.cla.common.domain.dto.file;

import com.cla.common.domain.dto.mediaproc.AnalysisMetadata;
import com.cla.common.domain.dto.mediaproc.excel.ExcelSheetMetaData;
import com.cla.connector.domain.dto.file.FileType;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import java.util.List;
import java.util.Map;

public class ExcelAnalysisMetadata extends AnalysisMetadata {

	private List<String> sheetsFullNames = Lists.newArrayList();	// Should be ordered list
	private Map<String, ExcelSheetMetaData> sheetsMetadata = Maps.newHashMap();
	
	public ExcelAnalysisMetadata() {
		super(FileType.EXCEL);
	}

	public List<String> getSheetsFullNames() {
		return sheetsFullNames;
	}

	public void setSheetsFullNames(final List<String> sheetsFullNames) {
		this.sheetsFullNames = sheetsFullNames;
	}

	public Map<String, ExcelSheetMetaData> getSheetsMetadata() {
		return sheetsMetadata;
	}

	public void setSheetsMetadata(final Map<String, ExcelSheetMetaData> sheetsMetadata) {
		this.sheetsMetadata = sheetsMetadata;
	}
	
	public ExcelSheetMetaData getSheetMetadata(final String name) {
		return sheetsMetadata.get(name);
	}
	
	public void addSheetMetadata(final String name, final ExcelSheetMetaData md) {
		sheetsFullNames.add(name);
		sheetsMetadata.put(name, md);
	}
	
}
