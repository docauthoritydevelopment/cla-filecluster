package com.cla.common.domain.dto.crawler;

import com.cla.connector.domain.dto.file.FileType;

import java.util.Map;

/**
 *
 * Created by uri on 06/10/2015.
 */
public class FileCrawlingTimersDto {

    private final long scanStartTime;
    private long ingestWordStartTime = 0;
    private long ingestExcelStartTime = 0;
    private long ingestPdfStartTime = 0;
    private final long analyzeStartTime;

    public FileCrawlingTimersDto(long scanStartTime, Map<FileType, Long> ingestStartTimes, long analyzeStartTime) {
        this.scanStartTime = scanStartTime;
        if (ingestStartTimes.containsKey(FileType.WORD)) {
            ingestWordStartTime  = ingestStartTimes.get(FileType.WORD);
        }
        if (ingestStartTimes.containsKey(FileType.EXCEL)) {
            ingestExcelStartTime = ingestStartTimes.get(FileType.EXCEL);
        }
        if (ingestStartTimes.containsKey(FileType.PDF)) {
            ingestPdfStartTime = ingestStartTimes.get(FileType.PDF);
        }
        this.analyzeStartTime = analyzeStartTime;
    }

    public long getScanStartTime() {
        return scanStartTime;
    }

    public long getAnalyzeStartTime() {
        return analyzeStartTime;
    }

    public long getIngestWordStartTime() {
        return ingestWordStartTime;
    }

    public long getIngestExcelStartTime() {
        return ingestExcelStartTime;
    }

    public long getIngestPdfStartTime() {
        return ingestPdfStartTime;
    }
}
