package com.cla.common.domain.dto.histogram;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created to work around sql @LOB cast problem
 * @author itay
 *
 * @param <T>
 */
public class ClaSuperCollection<T> implements Serializable {

	
	private static final long serialVersionUID = 1L;
	
	private Map<String, ClaHistogramCollection<T>> collections = new LinkedHashMap<>();

	public Map<String, ClaHistogramCollection<T>> getCollections() {
		return collections;
	}

	public void setCollections(final Map<String, ClaHistogramCollection<T>> collections) {
		this.collections = collections;
	}

	public ClaHistogramCollection<T> getCollection(final String key) {
		return collections.get(key);
	}

	public void setCollection(final String key, final ClaHistogramCollection<T> collection) {
		this.collections.put(key, collection);
	}

}
