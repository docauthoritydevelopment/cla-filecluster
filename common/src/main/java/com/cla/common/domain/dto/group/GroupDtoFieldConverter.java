package com.cla.common.domain.dto.group;

/**
 * Created by uri on 07/07/2015.
 */
public class GroupDtoFieldConverter {

    static enum DtoFields {
        id,
        groupName("generatedName"),
        numOfFiles,
        firstMemberPath,
        firstMemberName;



        String modelField;

        private DtoFields() {
            this.modelField = this.name();
        }

        private DtoFields(String modelField) {
            this.modelField = modelField;
        }

        public String getModelField() {
            return modelField;
        }
    }

    public static String convertDtoField(String dtoField) {
        return DtoFields.valueOf(dtoField).getModelField();
    }

}
