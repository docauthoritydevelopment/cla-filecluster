package com.cla.common.domain.dto.crawler;

import com.cla.common.domain.dto.jobmanager.JobState;

public enum JobCompletionStatus {
    UNKNOWN,
    OK,
    CANCELLED,
    ERROR,
    DONE_WITH_ERRORS,
    TIMED_OUT,
    STOPPED,
    SKIPPED;

    public boolean isFinishedSuccessfully(){
        return JobCompletionStatus.OK == this || JobCompletionStatus.SKIPPED == this || JobCompletionStatus.DONE_WITH_ERRORS == this || JobCompletionStatus.CANCELLED == this;
    }
}
