package com.cla.common.domain.dto.group;

import java.io.Serializable;
import java.util.List;

/**
 * Created by uri on 20/11/2016.
 */
public class AttachFilesToGroupDto implements Serializable {

    List<Long> fileIds;

    String groupName;

    String groupId;

    public List<Long> getFileIds() {
        return fileIds;
    }

    public void setFileIds(List<Long> fileIds) {
        this.fileIds = fileIds;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    @Override
    public String toString() {
        return "AttachFilesToGroupDto{" +
                "fileIds=" + fileIds +
                ", groupName='" + groupName + '\'' +
                ", groupId='" + groupId + '\'' +
                '}';
    }
}
