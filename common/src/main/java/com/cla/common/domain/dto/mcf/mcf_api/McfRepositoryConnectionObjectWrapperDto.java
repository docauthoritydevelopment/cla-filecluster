package com.cla.common.domain.dto.mcf.mcf_api;

import com.google.common.collect.Lists;

import java.io.Serializable;
import java.util.List;

/**
 * Created by uri on 20/07/2016.
 {"repositoryconnection":{"isnew":"false","name":"FileSystem","class_name":"org.apache.manifoldcf.crawler.connectors.filesystem.FileConnector","max_connections":"10","description":"","configuration":{}}}
 */
public class McfRepositoryConnectionObjectWrapperDto implements Serializable {

    List<McfRepositoryConnectionObjectDto> repositoryconnection;

    public McfRepositoryConnectionObjectWrapperDto(McfRepositoryConnectionObjectDto newRepository) {
        repositoryconnection = Lists.newArrayList(newRepository);
    }

    public McfRepositoryConnectionObjectWrapperDto() {
    }

    public List<McfRepositoryConnectionObjectDto> getRepositoryconnection() {
        return repositoryconnection;
    }

    public void setRepositoryconnection(List<McfRepositoryConnectionObjectDto> repositoryconnection) {
        this.repositoryconnection = repositoryconnection;
    }
}
