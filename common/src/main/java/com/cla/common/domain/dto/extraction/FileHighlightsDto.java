package com.cla.common.domain.dto.extraction;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * Created by liora on 14/11/2016.
 */
public class FileHighlightsDto implements Serializable
{
    private List<String> snippets;
    private Map<String, List<String>> locations;


    private long fileContentLastModifiedDate;

    public List<String> getSnippets() {
        return snippets;
    }

    public void setSnippets(List<String> snippets) {
        this.snippets = snippets;
    }

    public Map<String, List<String>> getLocations() {
        return locations;
    }

    public void setLocations(Map<String, List<String>> locations) {
        this.locations = locations;
    }

    public long getFileContentLastModifiedDate() {
        return fileContentLastModifiedDate;
    }

    public void setFileContentLastModifiedDate(long fileContentLastModifiedDate) {
        this.fileContentLastModifiedDate = fileContentLastModifiedDate;
    }

    @Override
    public String toString() {
        return "FileHighlightsDto{" +
                "snippets=" + snippets +
                ", locations=" + locations +
                ", fileContentLastModifiedDate=" + fileContentLastModifiedDate +
                '}';
    }
}
