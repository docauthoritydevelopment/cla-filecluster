package com.cla.common.domain.dto.category;

import java.io.Serializable;

/**
 * Created by uri on 26/07/2015.
 */
public class FileCategoryDto implements Serializable  {
    private String fileCategoryId;
    private long numOfFiles;

    private String fileCategoryName;
    private FileCategoryType fileCategoryType;

    public FileCategoryDto(String fileCategoryId, long numOfFiles, String fileCategoryName, FileCategoryType fileCategoryType) {
        this.fileCategoryId = fileCategoryId;
        this.numOfFiles = numOfFiles;
        this.fileCategoryName = fileCategoryName;
        this.fileCategoryType = fileCategoryType;
    }

    public FileCategoryDto(String fileCategoryId, long numOfFiles) {
        this.fileCategoryId = fileCategoryId;
        this.numOfFiles = numOfFiles;
    }

    public String getFileCategoryId() {
        return fileCategoryId;
    }

    public void setFileCategoryId(String fileCategoryId) {
        this.fileCategoryId = fileCategoryId;
    }

    public long getNumOfFiles() {
        return numOfFiles;
    }

    public void setNumOfFiles(long numOfFiles) {
        this.numOfFiles = numOfFiles;
    }

    public String getFileCategoryName() {
        return fileCategoryName;
    }

    public void setFileCategoryName(String fileCategoryName) {
        this.fileCategoryName = fileCategoryName;
    }

    public FileCategoryType getFileCategoryType() {
        return fileCategoryType;
    }

    public void setFileCategoryType(FileCategoryType fileCategoryType) {
        this.fileCategoryType = fileCategoryType;
    }
}
