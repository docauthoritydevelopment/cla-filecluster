package com.cla.common.domain.dto.components;

import com.cla.common.domain.dto.dataCenter.CustomerDataCenterDto;
import com.cla.connector.domain.dto.JsonSerislizableVisible;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Created by itay on 22/02/2017.
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ClaComponentDto implements JsonSerislizableVisible {
    private long id;
    private ClaComponentType claComponentType;
    private String componentType;
    private String instanceId;  // Ordinal within the type
    private String location;
    private String externalNetAddress;
    private String localNetAddress;
    private Long createdOnDB;    // Time when was added to the system
    private Boolean active = true;
    private ComponentState state;
    private Long stateChangeDate;
    private Long lastResponseDate;
    private Long lastStatusId;

    private CustomerDataCenterDto customerDataCenterDto;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public ClaComponentType getClaComponentType() {
        return claComponentType;
    }

    public void setClaComponentType(ClaComponentType claComponentType) {
        this.claComponentType = claComponentType;
    }

    public String getComponentType() {
        return componentType;
    }

    public void setComponentType(String componentType) {
        this.componentType = componentType;
    }

    public String getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getExternalNetAddress() {
        return externalNetAddress;
    }

    public void setExternalNetAddress(String externalNetAddress) {
        this.externalNetAddress = externalNetAddress;
    }

    public String getLocalNetAddress() {
        return localNetAddress;
    }

    public void setLocalNetAddress(String localNetAddress) {
        this.localNetAddress = localNetAddress;
    }

    public Long getCreatedOnDB() {
        return createdOnDB;
    }

    public void setCreatedOnDB(Long createdOnDB) {
        this.createdOnDB = createdOnDB;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public ComponentState getState() {
        return state;
    }

    public void setState(ComponentState state) {
        this.state = state;
    }

    public Long getStateChangeDate() {
        return stateChangeDate;
    }

    public void setStateChangeDate(Long stateChangeDate) {
        this.stateChangeDate = stateChangeDate;
    }

    public Long getLastResponseDate() {
        return lastResponseDate;
    }

    public void setLastResponseDate(Long lastResponseDate) {
        this.lastResponseDate = lastResponseDate;
    }

    public CustomerDataCenterDto getCustomerDataCenterDto() {
        return customerDataCenterDto;
    }

    public void setCustomerDataCenterDto(CustomerDataCenterDto customerDataCenterDto) {
        this.customerDataCenterDto = customerDataCenterDto;
    }

    public Long getLastStatusId() {
        return lastStatusId;
    }

    public void setLastStatusId(Long lastStatusId) {
        this.lastStatusId = lastStatusId;
    }

    @Override
    public String toString() {
        return "ClaComponentDto{" +
                "id=" + id +
                ", claComponentType=" + claComponentType +
                ", componentType='" + componentType + '\'' +
                ", instanceId='" + instanceId + '\'' +
                ", lastResponseDate='" + lastResponseDate+ '\'' +
                ", location='" + location + '\'' +
                ", externalNetAddress='" + externalNetAddress + '\'' +
                ", localNetAddress='" + localNetAddress + '\'' +
                ", createdOnDB=" + createdOnDB +
                ", active=" + active +
                ", state=" + state +
                ", stateChangeDate=" + stateChangeDate +
                '}';
    }
}
