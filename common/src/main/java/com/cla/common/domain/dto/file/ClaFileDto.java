package com.cla.common.domain.dto.file;

import com.cla.common.domain.dto.bizlist.ExtractionSummaryDto;
import com.cla.common.domain.dto.bizlist.SimpleBizListItemDto;
import com.cla.common.domain.dto.department.DepartmentAssociationDto;
import com.cla.common.domain.dto.department.DepartmentDto;
import com.cla.common.domain.dto.doctype.DocTypeAssociationDto;
import com.cla.common.domain.dto.doctype.DocTypeDto;
import com.cla.common.domain.dto.email.MailAddressDto;
import com.cla.common.domain.dto.email.MailAddressType;
import com.cla.common.domain.dto.extraction.SearchPatternCountDto;
import com.cla.common.domain.dto.filetag.FileTagAssociationDto;
import com.cla.common.domain.dto.filetag.FileTagDto;
import com.cla.common.domain.dto.security.AssociableDto;
import com.cla.common.domain.dto.security.FunctionalItemDto;
import com.cla.common.domain.dto.security.FunctionalRoleDto;
import com.cla.common.domain.dto.security.SystemRoleDto;
import com.cla.common.domain.dto.workflow.WorkflowFileData;
import com.cla.common.utils.EmailUtils;
import com.cla.connector.domain.dto.file.FileType;
import com.cla.connector.domain.dto.file.ItemType;
import com.cla.connector.domain.dto.file.label.LabelDto;
import com.cla.connector.domain.dto.media.EmailAddress;
import com.cla.connector.domain.dto.media.MediaType;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.cla.common.domain.dto.filetree.DocFolderDto;

import java.io.Serializable;
import java.util.*;

/**
 *
 * Created by uri on 06/07/2015.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ClaFileDto implements FunctionalItemDto,AssociableDto, Serializable {
    private static final int DEFAULT_STR_FIELD_SIZE = 256;

    private Long id;
    private String fileName;
    private String fullPath;
    private String extension;
    private String baseName;
    private String fileNameHashed;
    private String mediaEntityId;

    private FileType type;

    private Long fsFileSize;
    private Date fsLastModified;
    private Date fsLastAccess;
    private Date creationDate;      // This is the app creation data. TODO - split into 2
    private Date initialScanDate;	// Time when was added to the system or un-deleted as it is found again as an active file

    private boolean deleted;
    private Date deletionDate;	// Latest time when record was marked as deleted
    private Date createdOnDB;   // created in DB date
    private Date updatedOnDB;   // last time it was updated in DB

    private ClaFileState state;
    private FileIngestionState ingestionState;

    private String analysisGroupId;
    private AnalyzeHint analyzeHint;
    private String groupId; // user group ID
    private String groupName;

    private Long rootFolderId;
    private Long docFolderId;
    private DocFolderDto docFolder;

    private Long contentId;

    private List<Integer> matchedPatternsSingle = Lists.newArrayList();
    private List<Integer> matchedPatternsMulti = Lists.newArrayList();
    private List<String> matchedPatternsCounts = Lists.newArrayList();

    private Set<FileTagDto> fileTagDtos = Sets.newHashSet();
    private List<FileTagAssociationDto> fileTagAssociations = Lists.newLinkedList();
    private Set<DocTypeDto> docTypeDtos = Sets.newHashSet();
    private DepartmentDto departmentDto;
    private List<DepartmentAssociationDto> departmentAssociationDtos = Lists.newLinkedList();

    private String owner;
    private String mime;

    private ContentMetadataDto contentMetadataDto;

    private List<ExtractionSummaryDto> bizListExtractionSummary = Lists.newLinkedList();
    private Set<FunctionalRoleDto> associatedFunctionalRoles = Sets.newHashSet();
    private List<SimpleBizListItemDto> associatedBizListItems = Lists.newLinkedList();
    private String docFolderRealPath;

    private Set<SystemRoleDto> permittedOperations = Sets.newHashSet();
    private Long packageTopFileId;

    private transient Set<SearchPatternCountDto> searchPatternsCounting = null;

    private Long lastMetadataChangeDate;
    private WorkflowFileData workflowFileData;
    private List<DocTypeAssociationDto> docTypeAssociations;

    private String senderName;
    private String senderAddress;
    private String senderDomain;
    private String senderFullAddress;
    private List<String> recipientsNames;
    private List<String> recipientsAddresses;
    private List<String> recipientsDomains;
    private List<String> recipientsFullAddresses;
    private Date sentDate;

    private String rootFolderPath;

    private String rootFolderRealPath;

    private ItemType itemType;

    private Integer numOfAttachments;

    private Integer totalRecipients;

    private MailAddressDto sender;

    private List<MailAddressDto> recipients;

    private Map<Long, String> containerIds;

    private String mailboxUpn;

    private String itemSubject;

    private MediaType mediaType;

    private String mailboxGroup;

    private String sortName;

    private List<Long> daLabels;

    private List<String> externalMetadata;

    private List<String> generalMetadata;

    private List<String> actionsExpected;

    private List<String> actionConfirmation;

    private List<String> daMetadata;

    private List<String> daMetadataType;

    private String rawMetadata;

    private Long labelProcessDate;

    private List<LabelDto> daLabelDtos;

    public ClaFileDto() {
    }

    public ClaFileDto(long id, String fullPath, int typeInx, String baseName,
                      Long fsFileSize, Date fsLastModified, Date creationDate, Date fsLastAccess,
                      Date initialScanDate, Date deletionDate,
                      String analysisGroupId, String userGroupId, String groupName, Long contentId, Long rootFolderId) {
		super();
		this.id = id;
        this.analysisGroupId = analysisGroupId;
        this.groupId = userGroupId;
        this.groupName = groupName;
        this.rootFolderId = rootFolderId;
        this.fileName = fullPath+baseName;
        this.fullPath = fullPath;
		this.type = FileType.valueOf(typeInx);
		this.baseName = baseName;
		this.fsFileSize = fsFileSize;
		this.fsLastModified = fsLastModified;
        this.creationDate = creationDate;
        this.fsLastAccess = fsLastAccess;
        this.initialScanDate = initialScanDate;
        this.deletionDate = deletionDate;
        this.contentId = contentId;
        this.analyzeHint = AnalyzeHint.NORMAL;


        if (contentId != null) {
            contentMetadataDto = new ContentMetadataDto(contentId,
                    analysisGroupId, userGroupId, typeInx, fsFileSize,null,null,0,null,0L,null);
            contentMetadataDto.setHint(fileName);
        }
	}

	public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public void setType(FileType type) {
        this.type = type;
    }

    public void setBaseName(String baseName) {
        this.baseName = baseName;
    }

    public void setFsFileSize(Long fsFileSize) {
        this.fsFileSize = fsFileSize;
    }

    public void setFsLastModified(Date fsLastModified) {
        this.fsLastModified = fsLastModified;
    }

    public void setFsLastAccess(Date fsLastAccess) {
        this.fsLastAccess = fsLastAccess;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getInitialScanDate() { return initialScanDate; }

    public void setInitialScanDate(Date initialScanDate) { this.initialScanDate = initialScanDate; }

    public Date getCreatedOnDB() {
        return createdOnDB;
    }

    public void setCreatedOnDB(Date createdOnDB) {
        this.createdOnDB = createdOnDB;
    }

    public Date getUpdatedOnDB() {
        return updatedOnDB;
    }

    public void setUpdatedOnDB(Date updatedOnDB) {
        this.updatedOnDB = updatedOnDB;
    }

    public Date getDeletionDate() { return deletionDate; }

    public void setDeletionDate(Date deletionDate) { this.deletionDate = deletionDate; }

    public Date getAppCreationDate() {
        if (contentMetadataDto != null) {
            return contentMetadataDto.getAppCreationDate();
        }
        return null;
    }

    public String getFileName() {
        return getRootFolderRealPathMailbox() + fileName;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public FileType getType() {
        return type;
    }

    public String getMime() {
        return mime;
    }

    public void setMime(String mime) {
        this.mime = mime;
    }

    /**
     *
     * @return the name of the file (no path)
     */
    public String getBaseName() {
        return baseName;
    }

    public Long getFsFileSize() {
        return fsFileSize;
    }

    public Date getFsLastModified() {
        return fsLastModified;
    }

    public Date getFsLastAccess() {
        return fsLastAccess;
    }

    public Long getFsLastAccessAsLong() {
        return fsLastAccess == null ? null : fsLastAccess.getTime();
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public ClaFileState getState() {
        return state;
    }

    public void setState(ClaFileState state) {
        this.state = state;
    }

    public FileIngestionState getIngestionState() {
        return ingestionState;
    }

    public void setIngestionState(FileIngestionState ingestionState) {
        this.ingestionState = ingestionState;
    }

    public Set<FileTagDto> getFileTagDtos() {
        return fileTagDtos;
    }

    public void setFileTagDtos(Set<FileTagDto> fileTagDtos) {
        this.fileTagDtos = fileTagDtos;
    }

    public Set<DocTypeDto> getDocTypeDtos() {
        return docTypeDtos;
    }

    public void setDocTypeDtos(Set<DocTypeDto> docTypeDtos) {
        this.docTypeDtos = docTypeDtos;
    }

    public List<FileTagAssociationDto> getFileTagAssociations() {
        return fileTagAssociations;
    }

    public void setFileTagAssociations(List<FileTagAssociationDto> fileTagAssociations) {
        this.fileTagAssociations = fileTagAssociations;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public List<SimpleBizListItemDto> getAssociatedBizListItems() {
        return associatedBizListItems;
    }

    public void setAssociatedBizListItems(List<SimpleBizListItemDto> associatedBizListItems) {
        this.associatedBizListItems = associatedBizListItems;
    }

    @Override
    public Set<SystemRoleDto> getPermittedOperations() {
        return permittedOperations;
    }

    @Override
    public Set<FunctionalRoleDto> getAssociatedFunctionalRoles() {
        return associatedFunctionalRoles;
    }

    public void setAssociatedFunctionalRoles(Set<FunctionalRoleDto> associatedFunctionalRoles) {
        this.associatedFunctionalRoles = associatedFunctionalRoles;
    }

    public String getAnalysisGroupId() {
        return analysisGroupId;
    }

    public void setAnalysisGroupId(String analysisGroupId) {
        this.analysisGroupId = analysisGroupId;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getFullPath() {
        return getRootFolderPathMailbox() + fullPath;
    }

    public void setFullPath(String fullPath) {
        this.fullPath = fullPath;
    }

    public List<ExtractionSummaryDto> getBizListExtractionSummary() {
        return bizListExtractionSummary;
    }

    public void setBizListExtractionSummary(List<ExtractionSummaryDto> bizListExtractionSummary) {
        this.bizListExtractionSummary = bizListExtractionSummary;
    }

    public Long getContentId() {
        return contentId;
    }

    public void setContentId(Long contentId) {
        this.contentId = contentId;
    }

    public ContentMetadataDto getContentMetadataDto() {
        return contentMetadataDto;
    }

    public void setContentMetadataDto(ContentMetadataDto contentMetadataDto) {
        this.contentMetadataDto = contentMetadataDto;
    }

    public Long getRootFolderId() {
        return rootFolderId;
    }

    public void setRootFolderId(Long rootFolderId) {
        this.rootFolderId = rootFolderId;
    }

    public AnalyzeHint getAnalyzeHint() {
        return analyzeHint;
    }

    public void setAnalyzeHint(AnalyzeHint analyzeHint) {
        this.analyzeHint = analyzeHint;
    }

    public Long getDocFolderId() {
        return docFolderId;
    }

    public void setDocFolderId(Long docFolderId) {
        this.docFolderId = docFolderId;
    }

    public void setDocFolderRealPath(String docFolderRealPath) {
        this.docFolderRealPath = docFolderRealPath;
    }

    public String getDocFolderRealPath() {
        return rootFolderRealPath!=null ? rootFolderRealPath + docFolderRealPath : docFolderRealPath;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(final String str) {
        if (Strings.isNullOrEmpty(str)) {
            this.extension = str;
        } else {
            final int len = str.length();
            this.extension = (len < DEFAULT_STR_FIELD_SIZE) ? str : str.substring(0, DEFAULT_STR_FIELD_SIZE - 1);
        }
    }

    public DocFolderDto getDocFolder() {
        return docFolder;
    }

    public void setDocFolder(DocFolderDto docFolder) {
        this.docFolder = docFolder;
    }

    public Long getPackageTopFileId() {
        return packageTopFileId;
    }

    public void setPackageTopFileId(Long packageTopFileId) {
        this.packageTopFileId = packageTopFileId;
    }

    public String getFileNameHashed() {
        return fileNameHashed;
    }

    public void setFileNameHashed(String fileNameHashed) {
        this.fileNameHashed = fileNameHashed;
    }

    public String getMediaEntityId() {
        return (mediaEntityId != null && rootFolderRealPath!=null) ? rootFolderRealPath+mediaEntityId : mediaEntityId;
    }

    public void setMediaEntityId(String mediaEntityId) {
        this.mediaEntityId = mediaEntityId;
    }

    public Set<SearchPatternCountDto> getSearchPatternsCounting() {
        return searchPatternsCounting;
    }

    public void setSearchPatternsCounting(Set<SearchPatternCountDto> searchPatternsCounting) {
        this.searchPatternsCounting = searchPatternsCounting;
    }

    public Long getLastMetadataChangeDate() {
        return lastMetadataChangeDate;
    }

    public void setLastMetadataChangeDate(Long lastMetadataChangeDate) {
        this.lastMetadataChangeDate = lastMetadataChangeDate;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getSenderAddress() {
        return senderAddress;
    }

    public void setSenderAddress(String senderAddress) {
        this.senderAddress = senderAddress;
    }

    public String getSenderDomain() {
        return senderDomain;
    }

    public void setSenderDomain(String senderDomain) {
        this.senderDomain = senderDomain;
    }

    public List<String> getRecipientsNames() {
        return recipientsNames;
    }

    public void setRecipientsNames(List<String> recipientsNames) {
        this.recipientsNames = recipientsNames;
    }

    public List<String> getRecipientsAddresses() {
        return recipientsAddresses;
    }

    public void setRecipientsAddresses(List<String> recipientsAddresses) {
        this.recipientsAddresses = recipientsAddresses;
    }

    public List<String> getRecipientsDomains() {
        return recipientsDomains;
    }

    public void setRecipientsDomains(List<String> recipientsDomains) {
        this.recipientsDomains = recipientsDomains;
    }

    public String getSenderFullAddress() {
        return senderFullAddress;
    }

    public void setSenderFullAddress(String senderFullAddress) {
        this.senderFullAddress = senderFullAddress;
    }

    public List<String> getRecipientsFullAddresses() {
        return recipientsFullAddresses;
    }

    public void setRecipientsFullAddresses(List<String> recipientsFullAddresses) {
        this.recipientsFullAddresses = recipientsFullAddresses;
    }

    public Date getSentDate() {
        return sentDate;
    }

    public void setSentDate(Date sentDate) {
        this.sentDate = sentDate;
    }

    public List<Integer> getMatchedPatternsSingle() {
        return matchedPatternsSingle;
    }

    public void setMatchedPatternsSingle(List<Integer> matchedPatternsSingle) {
        this.matchedPatternsSingle = matchedPatternsSingle;
    }

    public List<Integer> getMatchedPatternsMulti() {
        return matchedPatternsMulti;
    }

    public void setMatchedPatternsMulti(List<Integer> matchedPatternsMulti) {
        this.matchedPatternsMulti = matchedPatternsMulti;
    }

    public List<String> getMatchedPatternsCounts() {
        return matchedPatternsCounts;
    }

    public void setMatchedPatternsCounts(List<String> matchedPatternsCounts) {
        this.matchedPatternsCounts = matchedPatternsCounts;
    }

    @Override
    public String toString() {
        return "ClaFileDto{" +
                "id=" + id +
                ", fileName='" + fileName + '\'' +
                ", type=" + type +
                ", rootFolderId='" + rootFolderId + '\'' +
                ", contentId='" + (contentId!=null?contentId.toString():"-") + '\'' +
                ", baseName='" + baseName + '\'' +
                ", fsFileSize=" + fsFileSize +
                ", fsLastModified=" + fsLastModified +
                ", creationDate=" + creationDate +
                ", fsLastAccess=" + fsLastAccess +
                ", state=" + state +
                ", analysisGroupId='" + analysisGroupId + '\'' +
                ", userGroupId='" + groupId+ '\'' +
                ", groupName='" + groupName + '\'' +
                ", fileTagDtos=" + fileTagDtos +
                ", fileTagAssociations=" + fileTagAssociations +
                ", associatedFunctionalRoles=" + associatedFunctionalRoles +
                ", owner='" + owner + '\'' +
                '}';
    }

    public void setWorkflowFileData(WorkflowFileData workflowFileData) {
        this.workflowFileData = workflowFileData;
    }

    public WorkflowFileData getWorkflowFileData() {
        return workflowFileData;
    }

    public void setFileId(long fileId) {
        this.id = fileId;
    }

    public void setDocTypeAssociations(List<DocTypeAssociationDto> docTypeAssociations) {
        this.docTypeAssociations = docTypeAssociations;
    }

    public List<DocTypeAssociationDto> getDocTypeAssociations() {
        return docTypeAssociations;
    }

    public String getRootFolderPath() {
        return rootFolderPath;
    }

    public void setRootFolderPath(String rootFolderPath) {
        if (rootFolderPath != null) {
            if (rootFolderPath.endsWith("/") || rootFolderPath.endsWith("\\")) {
                rootFolderPath = rootFolderPath.substring(0, rootFolderPath.length() - 1);
            }
        }
        this.rootFolderPath = rootFolderPath;
    }

    public String getRootFolderRealPath() {
        return rootFolderRealPath;
    }

    public void setRootFolderRealPath(String rootFolderRealPath) {
        this.rootFolderRealPath = rootFolderRealPath;
    }

    public ItemType getItemType() {
        return itemType;
    }

    public void setItemType(ItemType itemType) {
        this.itemType = itemType;
    }

    public Integer getNumOfAttachments() {
        return numOfAttachments;
    }

    public void setNumOfAttachments(Integer numOfAttachments) {
        this.numOfAttachments = numOfAttachments;
    }

    public Integer getTotalRecipients() {
        return totalRecipients;
    }

    public MailAddressDto getSender() {
        return sender;
    }

    public List<MailAddressDto> getRecipients() {
        return recipients;
    }

    public Map<Long, String> getContainerIds() {
        return containerIds;
    }

    public void setContainerIds(Map<Long, String> containerIds) {
        this.containerIds = containerIds;
    }

    public void setContainerIdsByList(List<String> containerIds) {
        if (containerIds != null) {
            this.containerIds = new LinkedHashMap<>();
            containerIds.forEach(container -> {
                String[] containerData = container.split("[.]");
                this.containerIds.put(Long.parseLong(containerData[1]), containerData[0]);
            });
        }
    }

    public Long getMailContainerId() {
        if (type.equals(FileType.MAIL)) {
            return id;
        } else if (containerIds != null) {
            for (Map.Entry<Long, String> entry : containerIds.entrySet()) {
                if (entry.getValue().equals(FileType.MAIL.name())) {
                    return entry.getKey();
                }
            }
        }
        return null;
    }

    public void fixEmailDataForDtoDisplay(int recipientDisplayLimit, EmailAddress senderParam, List<EmailAddress> recipientsParam, boolean parseX500Address, boolean extractNonDomainCN) {
        totalRecipients = (recipientsAddresses == null ? 0 : recipientsAddresses.size());

        if (senderParam != null) {
            sender = new MailAddressDto(senderParam.getAddress(), senderParam.getName(),
                    EmailUtils.extractDomainFromEmailAddress(senderParam.getAddress(), parseX500Address, extractNonDomainCN), MailAddressType.SENDER);
            senderFullAddress = null; senderAddress = null; senderName = null; senderDomain = null;
        }

        if (recipientsParam != null && !recipientsParam.isEmpty()) {
            int total = Math.min(recipientsParam.size(), recipientDisplayLimit);
            recipients = new ArrayList<>();
            for (int i = 0; i < total; ++i) {
                EmailAddress current = recipientsParam.get(i);
                MailAddressDto recp = new MailAddressDto(
                        current.getAddress(),
                        current.getName(),
                        EmailUtils.extractDomainFromEmailAddress(current.getAddress(), parseX500Address, extractNonDomainCN),
                        MailAddressType.RECIPIENT);
                recipients.add(recp);
            }
            recipientsNames = null; recipientsAddresses = null; recipientsDomains = null; recipientsFullAddresses = null;
        }
    }

    public String getMailboxUpn() {
        return mailboxUpn;
    }

    public void setMailboxUpn(String mailboxUpn) {
        this.mailboxUpn = mailboxUpn;
    }

    public String getItemSubject() {
        return itemSubject;
    }

    public void setItemSubject(String itemSubject) {
        this.itemSubject = itemSubject;
    }

    public MediaType getMediaType() {
        return mediaType;
    }

    public void setMediaType(MediaType mediaType) {
        this.mediaType = mediaType;
    }

    public String getMailboxGroup() {
        return mailboxGroup;
    }

    public void setMailboxGroup(String mailboxGroup) {
        this.mailboxGroup = mailboxGroup;
    }

    public DepartmentDto getDepartmentDto() {
        return departmentDto;
    }

    public void setDepartmentDto(DepartmentDto departmentDto) {
        this.departmentDto = departmentDto;
    }

    public List<DepartmentAssociationDto> getDepartmentAssociationDtos() {
        return departmentAssociationDtos;
    }

    public void setDepartmentAssociationDtos(List<DepartmentAssociationDto> departmentAssociationDtos) {
        this.departmentAssociationDtos = departmentAssociationDtos;
    }

    public String getSortName() {
        return sortName;
    }

    public void setSortName(String sortName) {
        this.sortName = sortName;
    }

    public List<Long> getDaLabels() {
        return daLabels;
    }

    public void setDaLabels(List<Long> daLabels) {
        this.daLabels = daLabels;
    }

    public List<String> getExternalMetadata() {
        return externalMetadata;
    }

    public void setExternalMetadata(List<String> externalMetadata) {
        this.externalMetadata = externalMetadata;
    }

    public List<String> getActionsExpected() {
        return actionsExpected;
    }

    public void setActionsExpected(List<String> actionsExpected) {
        this.actionsExpected = actionsExpected;
    }

    public String getRawMetadata() {
        return rawMetadata;
    }

    public void setRawMetadata(String rawMetadata) {
        this.rawMetadata = rawMetadata;
    }

    public List<String> getGeneralMetadata() {
        return generalMetadata;
    }

    public void setGeneralMetadata(List<String> generalMetadata) {
        this.generalMetadata = generalMetadata;
    }

    public List<String> getActionConfirmation() {
        return actionConfirmation;
    }

    public void setActionConfirmation(List<String> actionConfirmation) {
        this.actionConfirmation = actionConfirmation;
    }

    public Long getLabelProcessDate() {
        return labelProcessDate;
    }

    public void setLabelProcessDate(Long labelProcessDate) {
        this.labelProcessDate = labelProcessDate;
    }

    public List<LabelDto> getDaLabelDtos() {
        return daLabelDtos;
    }

    public void setDaLabelDtos(List<LabelDto> daLabelDtos) {
        this.daLabelDtos = daLabelDtos;
    }

    public List<String> getDaMetadata() {
        return daMetadata;
    }

    public void setDaMetadata(List<String> daMetadata) {
        this.daMetadata = daMetadata;
    }

    public List<String> getDaMetadataType() {
        return daMetadataType;
    }

    public void setDaMetadataType(List<String> daMetadataType) {
        this.daMetadataType = daMetadataType;
    }

    private String getRootFolderRealPathMailbox() {
        if (!Strings.isNullOrEmpty(rootFolderRealPath)) {
            return rootFolderRealPath;
        } else if (!Strings.isNullOrEmpty(mailboxGroup)) {
            return mailboxGroup;
        } else {
            return "";
        }
    }

    private String getRootFolderPathMailbox() {
        if (!Strings.isNullOrEmpty(rootFolderPath)) {
            return rootFolderPath;
        } else if (!Strings.isNullOrEmpty(mailboxGroup)) {
            return mailboxGroup;
        } else {
            return "";
        }
    }
}
