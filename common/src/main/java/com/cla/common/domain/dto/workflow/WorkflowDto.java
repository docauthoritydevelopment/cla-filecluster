package com.cla.common.domain.dto.workflow;

import com.cla.connector.domain.dto.JsonSerislizableVisible;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by: yael
 * Created on: 2/21/2018
 */
@SuppressWarnings("serial")
public class WorkflowDto implements JsonSerislizableVisible {

    private Long id;

    private Long dateCreated;

    private Long dateModified;

    private WorkflowType workflowType;

    private Long dateClosed;

    private Long dueDate;

    private Long currentStateDueDate;

    private Long dateLastStateChange;

    private WorkflowPriority priority;

    private WorkflowState state;

    private Long assignee;

    private Long assignedWorkgroup;

    private Map<String, String> requestDetails;

    private String contactInfo;

    private List<WorkflowFilterDescriptorDto> filters = new LinkedList<>();
    private WorkflowSummaryDto workflowSummary;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Long dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Long getDateModified() {
        return dateModified;
    }

    public void setDateModified(Long dateModified) {
        this.dateModified = dateModified;
    }

    public WorkflowType getWorkflowType() {
        return workflowType;
    }

    public void setWorkflowType(WorkflowType workflowType) {
        this.workflowType = workflowType;
    }

    public Long getDateClosed() {
        return dateClosed;
    }

    public void setDateClosed(Long dateClosed) {
        this.dateClosed = dateClosed;
    }

    public Long getDueDate() {
        return dueDate;
    }

    public void setDueDate(Long dueDate) {
        this.dueDate = dueDate;
    }

    public Long getCurrentStateDueDate() {
        return currentStateDueDate;
    }

    public void setCurrentStateDueDate(Long currentStateDueDate) {
        this.currentStateDueDate = currentStateDueDate;
    }

    public Long getDateLastStateChange() {
        return dateLastStateChange;
    }

    public void setDateLastStateChange(Long dateLastStateChange) {
        this.dateLastStateChange = dateLastStateChange;
    }

    public WorkflowPriority getPriority() {
        return priority;
    }

    public void setPriority(WorkflowPriority priority) {
        this.priority = priority;
    }

    public WorkflowState getState() {
        return state;
    }

    public void setState(WorkflowState state) {
        this.state = state;
    }

    public Long getAssignee() {
        return assignee;
    }

    public void setAssignee(Long assignee) {
        this.assignee = assignee;
    }

    public Long getAssignedWorkgroup() {
        return assignedWorkgroup;
    }

    public void setAssignedWorkgroup(Long assignedWorkgroup) {
        this.assignedWorkgroup = assignedWorkgroup;
    }

    public Map<String, String> getRequestDetails() {
        return requestDetails;
    }

    public void setRequestDetails(Map<String, String> requestDetails) {
        this.requestDetails = requestDetails;
    }

    public String getContactInfo() {
        return contactInfo;
    }

    public void setContactInfo(String contactInfo) {
        this.contactInfo = contactInfo;
    }

    public List<WorkflowFilterDescriptorDto> getFilters() {
        return filters;
    }

    public void setFilters(List<WorkflowFilterDescriptorDto> filters) {
        this.filters = filters;
    }

    public void setWorkflowSummary(WorkflowSummaryDto workflowSummary) {
        this.workflowSummary = workflowSummary;
    }

    public WorkflowSummaryDto getWorkflowSummary() {
        return workflowSummary;
    }

    @Override
    public String toString() {
        return "WorkflowDto{" +
                "id=" + id +
                ", dateCreated=" + dateCreated +
                ", dateModified=" + dateModified +
                ", workflowType=" + workflowType +
                ", dateClosed=" + dateClosed +
                ", dueDate=" + dueDate +
                ", currentStateDueDate=" + currentStateDueDate +
                ", dateLastStateChange=" + dateLastStateChange +
                ", priority=" + priority +
                ", state=" + state +
                ", assignee=" + assignee +
                ", assignedWorkgroup=" + assignedWorkgroup +
                ", requestDetails=" + requestDetails +
                ", contactInfo='" + contactInfo + '\'' +
                ", filters=" + filters +
                ", workflowSummary=" + workflowSummary +
                '}';
    }
}
