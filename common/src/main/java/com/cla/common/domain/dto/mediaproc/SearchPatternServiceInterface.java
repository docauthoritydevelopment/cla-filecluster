package com.cla.common.domain.dto.mediaproc;

import com.cla.common.domain.dto.extraction.SearchPatternCountDto;
import com.cla.common.domain.dto.filesearchpatterns.TextSearchPatternImpl;

import java.util.Collection;
import java.util.Set;

/**
 * Created by: yael
 * Created on: 8/8/2018
 */
public interface SearchPatternServiceInterface {
    Set<SearchPatternCountDto> getSearchPatternsCounting(String fileText);
	Collection<TextSearchPatternImpl> getAllFileSearchPatternImpls();
}
