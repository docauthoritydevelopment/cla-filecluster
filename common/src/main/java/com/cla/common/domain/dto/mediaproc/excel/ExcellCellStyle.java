package com.cla.common.domain.dto.mediaproc.excel;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class ExcellCellStyle {
	
	private static final short fontSzFactor = 1;	// TODO: set it right
	private static final byte BITS_bold = 0x01; 
	private static final byte BITS_underlined = 0x02; 
	private static final byte BITS_normalColor = 0x04; 
	
	@JsonProperty
	private short ftSz;		// fontSize

	//@JsonProperty
	//private boolean isDefault;

	@JsonProperty
	private byte bits;
	
	@JsonProperty
	private byte alignment;			// Not used for the moment
	
	//@JsonProperty
	//private short numOfRuns;

	@JsonProperty
	private short color;

	@JsonProperty
	private short frLen;		// First Run Length

	@JsonProperty
	private String format;		// Data format string

	@JsonProperty
	private short patternHash;

	@JsonProperty
	private short borderHash;

	/**
     * This method is private but it is called using reflection by java
     * serialization mechanism. It overwrites the default object serialization.
     *
     * <br/><br/><b>IMPORTANT</b>
     * The access modifier for this method MUST be set to <b>private</b> otherwise {@link java.io.StreamCorruptedException}
     * will be thrown.
     *
     */
    public void MyWriteObject(final ObjectOutputStream oos) throws IOException {
        //System.out.println(ExcelSheet.class.getName() + ".writeObject");

        //oos.defaultWriteObject();
        
        // scalars
    	oos.writeShort(ftSz);
    	//oos.writeBoolean(isDefault);
    	oos.writeByte(bits);
    	oos.writeByte(alignment);
    	//oos.writeShort(numOfRuns);
    	oos.writeShort(color);
    	oos.writeShort(frLen);
    	oos.writeObject(format);
    	oos.writeShort(patternHash);
    	oos.writeShort(borderHash);
    }
 
    /**
     * This method is private but it is called using reflection by java
     * serialization mechanism. It overwrites the default object serialization.
     *
     * <br/><br/><b>IMPORTANT</b>
     * The access modifier for this method MUST be set to <b>private</b> otherwise {@link java.io.StreamCorruptedException}
     * will be thrown.
     *
     */
    public void MyReadObject(final ObjectInputStream ois) throws IOException, ClassNotFoundException {
        //System.out.println(ExcelSheet.class.getName() + ".readObject");

        //ois.defaultReadObject();
        
        // scalars
    	ftSz = ois.readShort();
    	//isDefault = ois.readBoolean();
    	bits = ois.readByte();
    	alignment = ois.readByte();
    	//numOfRuns = ois.readShort();
    	color = ois.readShort();
    	frLen = ois.readShort();
    	format = (String) ois.readObject();
    	patternHash = ois.readShort();
    	borderHash = ois.readShort();
    }
    
	public ExcellCellStyle() {
	}

	public ExcellCellStyle(final short fontSize, final boolean isDefaultStyle, final boolean isBold, final boolean isUnderlined, final int numOfRuns, final int sizeOfFirstRun,
			final short color, final boolean isNormalColor, final String dataFormat, final short patternHash, final short borderHash, final byte alignment) {
		this.ftSz = fontSize;
		this.bits = 0;
		//this.isDefault = isDefaultStyle;
		setBold(isBold); 
		setUnderlined(isUnderlined);
		setNormalColor(isNormalColor);
		//this.numOfRuns = (short)numOfRuns;
		this.frLen = (short)sizeOfFirstRun;
		this.color = color;
		this.format = new String(("General".equals(dataFormat))?"":dataFormat);
		this.patternHash = patternHash;
		this.borderHash = borderHash;
		this.alignment = alignment;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		//result = prime * result + (isDefault ? 1231 : 1237);
		result = prime * result + bits*prime;
		result = prime * result + ftSz;
		//result = prime * result + numOfRuns;
		result = prime * result + frLen;
		result = prime * result + color;
		result = prime * result + patternHash;
		result = prime * result + borderHash;
		result = prime * result + format.hashCode();
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final ExcellCellStyle other = (ExcellCellStyle) obj;
		if (ftSz != other.ftSz) {
			return false;
		}
		if (bits != other.bits) {
			return false;
		}
		if (color != other.color) {
			return false;
		}
		if (patternHash != other.patternHash) {
			return false;
		}
		if (borderHash != other.borderHash) {
			return false;
		}
		if (format == null) {
			if (other.format != null) {
				return false;
			}
		} else if (!format.equals(other.format)) {
			return false;
		}
		return true;
	}

	public short getFontSize() {
		return ftSz;
	}

	public void setFontSize(final short fontSize) {
		this.ftSz = fontSize;
	}

	/*
	public boolean isDefaultStyle() {
		return isDefault;
	}

	public void setDefaultStyle(boolean isDefaultStyle) {
		this.isDefault = isDefaultStyle;
	}
	*/


	public boolean isBold() {
		return (bits & BITS_bold) != 0;
	}
	public void setBold(final boolean isBold) {
		if (isBold != isBold()) {
			bits ^= BITS_bold;
		}
	}
	public boolean isUnderlined() {
		return (bits & BITS_underlined) != 0;
	}
	public void setUnderlined(final boolean isUnderlined) {
		if (isUnderlined != isUnderlined()) {
			bits ^= BITS_underlined;
		}
	}
	public boolean isNormalColor() {
		return (bits & BITS_normalColor) != 0;
	}
	public void setNormalColor(final boolean isNormalColor) {
		if (isNormalColor != isNormalColor()) {
			bits ^= BITS_normalColor;
		}
	}


	/*
	public short getNumOfRuns() {
		return numOfRuns;
	}

	public void setNumOfRuns(int numOfRuns) {
		this.numOfRuns = (short)numOfRuns;
	}
	*/

	public short getColor() {
		return color;
	}

	public void setColor(final int color) {
		this.color = (short)color;
	}

	public short getSizeOfFirstRun() {
		return frLen;
	}


	public void setSizeOfFirstRun(final int sizeOfFirstRun) {
		this.frLen = (short)sizeOfFirstRun;
	}


	public String getFormat() {
		return format;
	}

	public short getPatternHash() {
		return patternHash;
	}

	public short getBorderHash() {
		return borderHash;
	}
	
	// Reduce sensitivity for small changes in font size
	private short blureFontSize(final short ftSz2) {
		return 
				(short) (1+4*((ftSz2-1)/4));
	}

	public String getStringSignature() {
		final StringBuffer sb = new StringBuffer();
		sb.append("fs:"+blureFontSize(ftSz));
		sb.append(" c:"+color);
		if (isNormalColor()) {
			sb.append("N");
		}
		if (isBold()) {
			sb.append("B");
		}
		sb.append(" p:"+patternHash);
		sb.append(" b:"+borderHash);
		if (format != null) {
			sb.append(" fmt:"+format);
		}
		
		return sb.toString();
	}

	public int getLayoutHashCode() {
		final int prime = 103;
		int result = 1;
		result = prime * result + bits*prime;
		result = prime * result + ftSz;
		result = prime * result + color;
		result = prime * result + patternHash;
		result = prime * result + borderHash;
		result = prime * result + format.hashCode();
		return result;
	}
	
	public boolean notStandard() {
		if (ftSz > 12 || !isNormalColor() || isBold() || patternHash !=0 || borderHash !=0) {
			return true;
		}
		return false;
	}

	public boolean isHeading() {
		if ((isBold() || isUnderlined() || !isNormalColor() || patternHash !=0 || borderHash !=0) && ftSz >= 12*fontSzFactor) {
			return true;
		}
		if (ftSz > 12*fontSzFactor) {
			return true;
		}
		
		return false;
	}
	
	public int getStyleHeadingPotentialRate()
	{
		int rate = 0;
		rate += Math.max(8, ftSz/fontSzFactor) - 8;
		if (isBold()) {
			rate += 2;
		}
		if (isUnderlined()) {
			rate += 2;
		}
		if (!isNormalColor()) {
			rate += 2;
		}
//		return 10-Math.max(0, rate);	// low number -> higher rate
		return rate;	// higher number -> higher rate
	}

	@Override
	public String toString() {
		return "ExcelCellStyle: [fontSize:"+ftSz+/*", isDefault:"+ isDefault+*/", isBold:"+isBold()+", isUL:"+isUnderlined()+", color:"+color
				+", isNormalColor:"+isNormalColor()+/*", numOfRuns:"+numOfRuns+*/ ", firstRunLen:"+frLen+", dataFormat:"+format + "]";
	}

}
