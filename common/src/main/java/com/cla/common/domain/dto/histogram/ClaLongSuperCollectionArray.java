package com.cla.common.domain.dto.histogram;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created to work around sql @LOB cast problem
 * @author itay
 *
 */
public class ClaLongSuperCollectionArray extends ClaSuperCollection<Long> {

	private static final long serialVersionUID = 1L;

	private Map<String, ClaHistogramCollection<Long>> collections = new LinkedHashMap<>();

	public Map<String, ClaHistogramCollection<Long>> getCollections() {
		return collections;
	}

	public ClaLongSuperCollectionArray() {}

	public static ClaSuperCollection<Long> create(ClaSuperCollection<Long> claSuperCollection) {
		ClaLongSuperCollectionArray clsca = new ClaLongSuperCollectionArray();
		clsca.setCollections(claSuperCollection.getCollections());
		return clsca;
	}

	public void setCollections(final Map<String, ClaHistogramCollection<Long>> colls) {
//		this.collections = collections;
		colls.forEach((s, c) -> this.setCollection(s, c));
	}

	public ClaHistogramCollection<Long> getCollection(final String key) {
		return collections.get(key);
	}

	public void setCollection(final String key, final ClaHistogramCollection<Long> collection) {
		this.collections.put(key,
				(collection instanceof ClaLongHistogramCollectionArray) ? collection :
						ClaLongHistogramCollectionArray.create(collection));
	}

	public ClaSuperCollection<Long> getClaSuperCollection() {
		ClaSuperCollection<Long> csc = new ClaSuperCollection<Long>();
		collections.forEach((s, c) -> csc.setCollection(s,
				(c instanceof ClaLongHistogramCollectionArray) ?
						((ClaLongHistogramCollectionArray) c).getClaHistogramCollection() : c));
		return csc;
	}

}