package com.cla.common.domain.dto.messages;

import com.cla.common.domain.dto.file.ItemDeletionPayload;
import com.cla.common.domain.dto.file.PstEntryPropertiesDto;
import com.cla.common.domain.dto.file.ScanProgressPayload;
import com.cla.common.domain.dto.mediaproc.OtherFile;
import com.cla.common.domain.dto.messages.control.*;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.media.Exchange365FoldersChangesDto;
import com.cla.connector.domain.dto.media.Exchange365ItemDto;
import com.cla.connector.domain.dto.media.MediaChangeLogDto;
import com.cla.connector.domain.dto.messages.DirListingPayload;
import com.cla.connector.domain.dto.messages.ScanSharePermissions;
import com.cla.connector.domain.dto.messages.ScanTaskParameters;
import com.google.common.collect.Maps;

import java.util.Arrays;
import java.util.Map;

/**
 * Payload types for inter-process messages serialization
 *
 * @see ProcessingPhaseMessage
 * @see com.cla.connector.domain.dto.messages.PhaseMessagePayload
 * Created by vladi on 3/15/2017.
 */
public enum MessagePayloadType {

    SCAN(ClaFilePropertiesDto.class),
    SCAN_PST_ENTRY(PstEntryPropertiesDto.class),
    SCAN_CHANGES(MediaChangeLogDto.class),
    SCAN_STARTED(ScanStartedPayload.class),
    SCAN_DELETION(ItemDeletionPayload.class),
    SCAN_PROGRESS(ScanProgressPayload.class),
    SCAN_DIR_LISTING(DirListingPayload.class),
    SCAN_CREATE_TASK(ScanTaskParameters.class),
    SCAN_DONE(String.class),
    SCAN_FAILED(String.class),
    SCAN_SHARE_PERMISSIONS(ScanSharePermissions.class),
    INGEST_WORD(WordIngestResult.class),
    INGEST_EXCEL(ExcelIngestResult.class),
    INGEST_PDF(WordIngestResult.class),
    INGEST_OTHER(OtherFile.class),
    INGEST_ERROR(String.class),
    FILE_CONTENT_OTHER(FileContentMessagePayload.class),
    LIST_FOLDERS_REQUEST(FileContentMessagePayload.class),
    LIST_FOLDERS_RESPONSE(FoldersListResultMessage.class),
    STATUS_REPORT(ControlStatusResponsePayload.class),
    HANDSHAKE_REQUEST(ServerHandShakeRequestPayload.class),
    HANDSHAKE_RESPONSE(ServerHandShakeResponsePayload.class),
    JOB_STATES(ControlStateRequestPayload.class),
    SEARCH_PATTERNS(ControlPatternRequestPayload.class),
    STATE_CHANGE(ServerStateChangeRequestPayload.class),
    LIMIT_REACHED(String.class),
    REFRESH_CONNECTION_DETAILS(RefreshConnectionDetailsPayload.class),
	KEY_VALUE_RESPONSE(KeyValuePayloadResp.class),
    EXCHANGE365_ITEM_CHANGE(Exchange365ItemDto.class),
    EXCHANGE365_FOLDER_CHANGES(Exchange365FoldersChangesDto.class),
    LABELING(LabelTaskParameters.class)
    ;

    private static Map<Class<?>, MessagePayloadType> mappings = Maps.newHashMap();
    private Class<?> realType;

    static {
        Arrays.stream(MessagePayloadType.values()).forEach(type -> mappings.put(type.getRealType(), type));
    }

    public static MessagePayloadType typeOf(Object obj) {
        MessagePayloadType mpt = mappings.get(obj.getClass());
        if (mpt == null) {
            throw new RuntimeException("Unsupported type " + obj.getClass());
        }
        return mpt;
    }

    MessagePayloadType(Class<?> realType) {
        this.realType = realType;
    }

    public Class<?> getRealType() {
        return realType;
    }

    public boolean in(MessagePayloadType... types) {
        for (MessagePayloadType type : types) {
            if (this.equals(type)) {
                return true;
            }
        }
        return false;
    }

}
