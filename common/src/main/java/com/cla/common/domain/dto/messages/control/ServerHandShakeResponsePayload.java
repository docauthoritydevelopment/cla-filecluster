package com.cla.common.domain.dto.messages.control;

import java.util.List;

/**
 * Created by liora on 10/07/2017.
 */
public class ServerHandShakeResponsePayload extends ControlMessagePayload{
    private String hostName;
    private boolean needSearchPatterns = false;

    public List<String> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<String> addresses) {
        this.addresses = addresses;
    }

    private List<String> addresses;

    public String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    public boolean isNeedSearchPatterns() {
        return needSearchPatterns;
    }

    public void setNeedSearchPatterns(boolean needSearchPatterns) {
        this.needSearchPatterns = needSearchPatterns;
    }

    @Override
    public String toString() {
        return "ServerHandShakeResponsePayload{" +
                " hostName='" + hostName + '\'' +
                '}';
    }


}
