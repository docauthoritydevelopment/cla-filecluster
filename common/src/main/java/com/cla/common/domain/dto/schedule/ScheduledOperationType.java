package com.cla.common.domain.dto.schedule;

/**
 * The types of operations to be done offline
 * using steps and db to allow recovery and avoid db inconsistency
 *
 * Created by: yael
 * Created on: 5/31/2018
 */
public enum ScheduledOperationType {
    REFINEMENT_RULE_APPLY(true, false, true, true),
    CREATE_JOINED_GROUP(true, false, true),
    DELETE_JOINED_GROUP(true, false, true),
    UPDATE_JOINED_GROUP(true, false, true),
    ADD_TO_JOINED_GROUP(true, false, true),
    DEL_FROM_JOINED_GROUP(true, false, true),
    CREATE_DEPARTMENT_ASSOCIATIONS_FOR_ROOT_FOLDER(false, false, true),
    APPLY_DOC_TYPES_TAGS_TO_SOLR(false, true, true),
    DOC_TYPE_MOVE(false, false, true),
    DELETE_ROOT_FOLDER(false, false, true),
    STARTUP_FILE_STATUS_CNG(false, false, false),
    APPLY_FOLDER_ASSOCIATIONS_TO_SOLR(false, true, true),
    APPLY_GROUP_ASSOCIATIONS_TO_SOLR(false, true, true),
    RE_TRANSLATE_FILE_METADATA(false, false, false, true),
    ASSOCIATION_FOR_FILES_BY_FILTER(false, true, true, true),
    ACL_RE_TRANSLATE_DOMAIN(false, true, true, true)
    ;

    private boolean isGroupOperation;
    private boolean isSolrApplyOperation;
    private boolean hasParams;
    private boolean canBeDeleted = false;

    ScheduledOperationType(boolean isGroupOperation, boolean isSolrApplyOperation, boolean hasParams) {
        this.isGroupOperation = isGroupOperation;
        this.isSolrApplyOperation = isSolrApplyOperation;
        this.hasParams = hasParams;
    }

    ScheduledOperationType(boolean isGroupOperation, boolean isSolrApplyOperation, boolean hasParams, boolean canBeDeleted) {
        this.isGroupOperation = isGroupOperation;
        this.isSolrApplyOperation = isSolrApplyOperation;
        this.hasParams = hasParams;
        this.canBeDeleted = canBeDeleted;
    }

    public boolean isGroupOperation() {
        return isGroupOperation;
    }

    public boolean isSolrApplyOperation() {
        return isSolrApplyOperation;
    }

    public boolean hasParams() {
        return hasParams;
    }

    public boolean canBeDeleted() {
        return canBeDeleted;
    }
}
