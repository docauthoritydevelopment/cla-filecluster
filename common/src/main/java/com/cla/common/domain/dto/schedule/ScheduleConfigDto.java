package com.cla.common.domain.dto.schedule;

import java.io.Serializable;
import java.util.Arrays;

/**
 * Created by uri on 15/05/2016.
 */
public class ScheduleConfigDto implements Serializable {
    private ScheduleType scheduleType;

    private int hour;

    private int minutes;

    private int every;

    private String[] daysOfTheWeek;

    private String customCronConfig;

    public ScheduleConfigDto() {
    }

    public ScheduleConfigDto(ScheduleType scheduleType, int minutes, int every) {
        this.scheduleType = scheduleType;
        this.minutes = minutes;
        this.every = every;
    }

    public ScheduleType getScheduleType() {
        return scheduleType;
    }

    public void setScheduleType(ScheduleType scheduleType) {
        this.scheduleType = scheduleType;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getMinutes() {
        return minutes;
    }

    public void setMinutes(int minutes) {
        this.minutes = minutes;
    }

    public int getEvery() {
        return every;
    }

    public void setEvery(int every) {
        this.every = every;
    }

    public String[] getDaysOfTheWeek() {
        return daysOfTheWeek;
    }

    public void setDaysOfTheWeek(String[] daysOfTheWeek) {
        this.daysOfTheWeek = daysOfTheWeek;
    }

    public String getCustomCronConfig() {
        return customCronConfig;
    }

    public void setCustomCronConfig(String customCronConfig) {
        this.customCronConfig = customCronConfig;
    }

    @Override
    public String toString() {
        return "ScheduleConfigDto{" +
                "scheduleType=" + scheduleType +
                ", hour=" + hour +
                ", minutes=" + minutes +
                ", every=" + every +
                ", daysOfTheWeek=" + Arrays.toString(daysOfTheWeek) +
                ", customCronConfig=" + customCronConfig +
                '}';
    }
}
