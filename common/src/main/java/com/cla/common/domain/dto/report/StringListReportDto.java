package com.cla.common.domain.dto.report;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by uri on 11/10/2015.
 */
public class StringListReportDto implements Serializable{

    private List<String> stringList;

    String name;

    String description;

    public StringListReportDto() {
        stringList = new ArrayList<>();
    }

    public StringListReportDto(String name, List<String> stringList) {
        this.name = name;
        this.stringList = stringList;
    }

    public List<String> getStringList() {
        return stringList;
    }

    public void setStringList(List<String> stringList) {
        this.stringList = stringList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
