package com.cla.common.domain.dto.extraction;

import java.io.Serializable;
import java.util.List;

/**
 * Created by uri on 24/11/2015.
 */
public class SearchPatternCountDto implements Serializable {
    private int patternId;
    private String patternName;
    private int patternCount;
    private List<String> matches;
    private boolean multi;

    public SearchPatternCountDto() {
    }

    public SearchPatternCountDto(int patternId, String patternName, int patternCount, List<String> matches) {
        this.patternId = patternId;
        this.patternName = patternName;
        this.patternCount = patternCount;
        this.matches = matches;
    }

    public int getPatternId() {
        return patternId;
    }

    public void setPatternId(int patternId) {
        this.patternId = patternId;
    }

    public String getPatternName() {
        return patternName;
    }

    public void setPatternName(String patternName) {
        this.patternName = patternName;
    }

    public int getPatternCount() {
        return patternCount;
    }

    public void setPatternCount(int patternCount) {
        this.patternCount = patternCount;
    }

    public List<String> getMatches() {
        return matches;
    }

    public void setMatches(List<String> matches) {
        this.matches = matches;
    }

    public boolean isMulti() {
        return multi;
    }

    public void setMulti(boolean multi) {
        this.multi = multi;
    }

    @Override
    public String toString() {
        return "SearchPatternCountDto{" +
                "patternId=" + patternId +
                ", patternName='" + patternName + '\'' +
                ", patternCount=" + patternCount +
                ", matches=" + matches +
                '}';
    }

    public String toExternalString() {
        StringBuilder sb = new StringBuilder();
        sb.append(patternName).append("(").append(patternCount).append("):");
        boolean[] first = {false};
        matches.forEach(m->{
            if (!first[0]) {
                first[0] = true;
            } else {
                sb.append(",");
            }
            sb.append(m);
        });
        return sb.toString();
    }
}
