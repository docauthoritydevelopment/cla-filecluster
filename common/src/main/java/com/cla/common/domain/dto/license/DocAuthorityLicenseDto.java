package com.cla.common.domain.dto.license;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by uri on 27/06/2016.
 */
public class DocAuthorityLicenseDto implements Serializable {

    public DocAuthorityLicenseDto() {
        restrictions = new HashMap<>();
    }

    private long id;

    private long creationDate;

    private long applyDate;

    private String registeredTo = "";

    private String issuer = "";

    private String serverId = "";

    private boolean loginExpired;

    private Map<String,String> restrictions;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(long creationDate) {
        this.creationDate = creationDate;
    }

    public long getApplyDate() {
        return applyDate;
    }

    public void setApplyDate(long applyDate) {
        this.applyDate = applyDate;
    }

    public String getRegisteredTo() {
        return registeredTo;
    }

    public void setRegisteredTo(String registeredTo) {
        this.registeredTo = registeredTo;
    }

    public String getIssuer() {
        return issuer;
    }

    public void setIssuer(String issuer) {
        this.issuer = issuer;
    }

    public String getServerId() { return serverId; }

    public void setServerId(String serverId) { this.serverId = serverId; }

    public Map<String, String> getRestrictions() {
        return restrictions;
    }

    public void setRestrictions(Map<String, String> restrictions) {
        this.restrictions = restrictions;
    }

    public boolean isLoginExpired() {
        return loginExpired;
    }

    public void setLoginExpired(boolean loginExpired) {
        this.loginExpired = loginExpired;
    }

    @Override
    public String toString() {
        return "DocAuthorityLicenseDto{" +
                "id=" + id +
                ", creationDate=" + creationDate + "/"+LocalDateTime.ofEpochSecond(creationDate / 1000, 0, ZoneOffset.UTC).format(DateTimeFormatter.BASIC_ISO_DATE) +
                ", applyDate=" + applyDate+"/"+LocalDateTime.ofEpochSecond(applyDate / 1000, 0, ZoneOffset.UTC).format(DateTimeFormatter.BASIC_ISO_DATE) +
                ", registeredTo='" + registeredTo + '\'' +
                ", issuer='" + issuer + '\'' +
                ", serverId='" + serverId + '\'' +
                ", restrictions=" + (restrictions==null?"<null>":restrictions.toString()) +
                '}';
    }
}
