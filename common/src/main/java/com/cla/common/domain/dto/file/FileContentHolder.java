package com.cla.common.domain.dto.file;

import com.cla.common.domain.dto.histogram.ClaHistogramCollection;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author liron
 *
 *
 */
public class FileContentHolder implements Serializable {

    private byte[] bytes;

    public byte[] getBytes() { return bytes;  }

    public FileContentHolder setBytes(byte[] bytes) { this.bytes = bytes;
    return this;
    }

}
