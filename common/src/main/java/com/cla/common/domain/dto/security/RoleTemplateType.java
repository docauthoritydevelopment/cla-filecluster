package com.cla.common.domain.dto.security;

/**
 * Created by: yael
 * Created on: 12/12/2017
 */
public enum RoleTemplateType {
    BIZROLE,
    FUNCROLE,
    NONE
}
