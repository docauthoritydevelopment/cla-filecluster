package com.cla.common.domain.dto.messages;

/**
 * Created by liron on 4/18/2017.
 */
public enum FileContentType {
    EXTRACT_HIGHLIGHT_FILE,
    EXTRACT_DOWNLOAD_FILE,
    IS_DIRECTORY_EXISTS,
    GET_LOGS,
    ERROR
}
