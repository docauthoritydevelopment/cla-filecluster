package com.cla.common.domain.dto.security;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Itai Marko.
 */
public class LdapGroupMappingDto {

    private Long id;

    private String groupName;

    private String groupDN;

    private Set<Long> daRolesIds = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getGroupDN() {
        return groupDN;
    }

    public void setGroupDN(String groupDN) {
        this.groupDN = groupDN;
    }

    public Set<Long> getDaRolesIds() {
        return daRolesIds;
    }

    public void setDaRolesIds(Set<Long> daRolesIds) {
        this.daRolesIds = daRolesIds;
    }

    @Override
    public String toString() {
        return "LdapGroupMappingDto{" +
                "id=" + id +
                ", groupName='" + groupName + '\'' +
                ", groupDN='" + groupDN + '\'' +
                ", daRolesIds=" + daRolesIds +
                '}';
    }
}
