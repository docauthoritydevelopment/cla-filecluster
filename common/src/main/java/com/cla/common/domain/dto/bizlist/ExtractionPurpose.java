package com.cla.common.domain.dto.bizlist;

/**
 * Created by uri on 23/08/2015.
 */
public enum ExtractionPurpose {
    PRIVACY,ENTITY_LOOKUP
}
