package com.cla.common.domain.dto.bizlist;

import java.io.Serializable;

/**
 * Created by uri on 23/08/2015.
 */
public class BizListDto implements Serializable{

    public BizListDto() {
    }

    public BizListDto(String name, BizListItemType bizListItemType) {
        this.name = name;
        this.bizListItemType = bizListItemType;
    }

    private long id;

    private String name;

    private String description;

    private BizListItemType bizListItemType;

    private String template;

    private Long lastSuccessfulRunDate;

    private Boolean active;

    private long extractionSessionId;

    private Long creationTimeStamp;

    private Long lastUpdateTimeStamp;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BizListItemType getBizListItemType() {
        return bizListItemType;
    }

    public void setBizListItemType(BizListItemType bizListItemType) {
        this.bizListItemType = bizListItemType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    @Override
    public String toString() {
        return "BizListDto{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", bizListItemType=" + bizListItemType +
                ", template='" + template + '\'' +
                ", active='" + active + '\'' +
                ", lastSuccessfulRunDate='" + lastSuccessfulRunDate + '\'' +
                ", extractionSessionId='" + extractionSessionId + '\'' +
                '}';
    }

    public Long getLastSuccessfulRunDate() {
        return lastSuccessfulRunDate;
    }

    public void setLastSuccessfulRunDate(Long lastSuccessfulRunDate) {
        this.lastSuccessfulRunDate = lastSuccessfulRunDate;
    }

    public Long getCreationTimeStamp() {
        return creationTimeStamp;
    }

    public void setCreationTimeStamp(Long creationTimeStamp) {
        this.creationTimeStamp = creationTimeStamp;
    }

    public Long getLastUpdateTimeStamp() {
        return lastUpdateTimeStamp;
    }

    public void setLastUpdateTimeStamp(Long lastUpdateTimeStamp) {
        this.lastUpdateTimeStamp = lastUpdateTimeStamp;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public long getExtractionSessionId() {
        return extractionSessionId;
    }

    public void setExtractionSessionId(long extractionSessionId) {
        this.extractionSessionId = extractionSessionId;
    }
}
