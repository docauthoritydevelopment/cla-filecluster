package com.cla.common.domain.dto.bizlist;

/**
 * Created by uri on 31/12/2015.
 *
 * Items go from PENDING_IMPORT to PENDING to ACTIVE to ARCHIVED to DELETED.
 * Any step is optional
 */
public enum BizListItemState {
    ACTIVE,DELETED,ARCHIVED,PENDING,PENDING_IMPORT
}
