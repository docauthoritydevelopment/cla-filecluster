package com.cla.common.domain.dto.crawler;

import com.cla.connector.domain.dto.file.FileType;

import java.io.Serializable;

/**
 *
 * Created by uri on 08/09/2016.
 */
public class FileProcessorOperationDto implements Serializable {
    private String operation;
    private String rootFolderString;
    private Long runId;
    private FileType fileType;

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public String getRootFolderString() {
        return rootFolderString;
    }

    public void setRootFolderString(String rootFolderString) {
        this.rootFolderString = rootFolderString;
    }

    public Long getRunId() {
        return runId;
    }

    public void setRunId(Long runId) {
        this.runId = runId;
    }

    public FileType getFileType() {
        return fileType;
    }

    public void setFileType(FileType fileType) {
        this.fileType = fileType;
    }

    @Override
    public String toString() {
        return "FileProcessorOperationDto{" +
                "operation='" + operation + '\'' +
                ", rootFolderString='" + rootFolderString + '\'' +
                ", runId=" + runId +
                ", fileType=" + fileType +
                '}';
    }
}
