package com.cla.common.domain.dto.mediaproc.excel;

import com.cla.common.domain.dto.extraction.SearchPatternCountDto;
import com.cla.common.domain.dto.histogram.ClaHistogramCollection;
import com.cla.common.domain.dto.histogram.ClaSuperCollection;
import com.cla.common.domain.dto.histogram.ClaTokensCollection;
import com.cla.common.domain.dto.pv.PVType;
import com.cla.common.utils.signature.SigningStringBuilder;
import com.cla.common.utils.tokens.DocAuthorityTokenizer;
import com.cla.common.utils.tokens.HashedTokensConsumer;
import com.cla.common.utils.tokens.TokenizerObject;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.Maps;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.stream.Collectors;

import static com.cla.common.domain.dto.mediaproc.excel.IngestConstants.*;

public class ExcelSheet{

	private static final Logger logger = LoggerFactory.getLogger(ExcelSheet.class);


    private ExcelParseParams excelParseParams;

    private ClaHistogramCollection<String> styleDebHist = null;
	
	private HashedTokensConsumer hashedTokensConsumer;
	
	private TokenizerObject tokenizerObject = null;

	private static List<String> s_valueIgnoreList = null;
	private static List<String> s_formulaIgnoreList = null;

	private String sheetName;
	private String fullSheetName;
	private List<String> sheetHeadings = null;

	private int numOfCells;
	private int numOfFormulas;
	private int numOfBlanks;
	private int numOfErrors;
	private int cellsSkipped;

	// internal state members (transient)
	//--------------------------------------------------------
	private ExcelCell potencialTitleCell = null;

	private int potencialTitleRate = 0;
	private int potencialTitleRowIndex = -1;
	private int potencialTitleColIndex = -1;
	private int lastRowIndex = -1;
	private int rowCountInSheet = 0;
	private int colCountInRow = 0;
	private int selectedCellStyleRepeatCount = 0;
	private ExcelHeadingAnalyzer excelHeadingAnalyzer = null;
	private boolean rowLayoutStarted = false;
	private int rowLayoutHash = 0;
	private int lastCellLayout = 0;
	private int lastCellLayoutRepeat = 0;
	private int lastSpecialRowLayoutHash = 0;
	private int lastRowLayout = 0;
	private int lastRowLayoutRepeat = 0;
	private int cellsInRowLayoutCount = 0;
	private int cellsInRowLayoutCountSnapshot = 0;
	private int numOfSignificantLayoutRows = 0;

	//--------------------------------------------------------

	private boolean patternSearchActive;

	private boolean useFormattedPatterns;

	private Set<SearchPatternCountDto> searchPatternsCounting = null;

	@JsonProperty
	private Integer id;

	@JsonProperty
	private final ClaSuperCollection<String> strHistograms = new ClaSuperCollection<>();

	@JsonProperty
	private final ClaSuperCollection<Long> longHistograms = new ClaSuperCollection<>();

	@JsonProperty
	private final ClaTokensCollection<Long> tokensCollection = null;

	@JsonProperty
	private final ExcelSheetMetaData metaData = new ExcelSheetMetaData();

	@JsonProperty
	private Map<String, String> proposedTitles = Maps.newLinkedHashMap();

	@JsonProperty
	private int sheetLayoutHash;	// redundant with sheetLayoutCounts - to be decided after field tests
	
	// Used for Entity extraction tokenization
	private SigningStringBuilder origContentTokens = SigningStringBuilder.create();
	final private StringBuilder contentTokens = new StringBuilder();
	
	public String getContentTokens() {
		return contentTokens.toString();
	}
	public String getOrigContentTokens() {
		return origContentTokens.toString();
	}
	public void removeOrigContentTokens() {
		origContentTokens = null;
	}

	private DocAuthorityTokenizer docAuthorityTokenizer;

	public static String getMetaDataStringTitle() {
		return getMetaDataStringTitle("");
	}

	public static String getMetaDataStringTitle(final String suffix) {
		return "shInx" + suffix + ",shName" + suffix + ",numCells" + suffix + ",numFormulas" + suffix;
	}

	public void appendContentTokens(final String tokens, final String cellRefStr) {
		if (tokenizerObject==null) {
			tokenizerObject = docAuthorityTokenizer.getTokenizerObject();
		}
		
		// Testing the tokenizer reuse implementation
//		final String t1 = nGramTokenizerHashed.getHash(tokens,hashedTokensConsumer,cellRefStr);
//		final String t2 = nGramTokenizerHashed.getHashReuseTokenizer(tokens,hashedTokensConsumer,cellRefStr,tokenizerObject);
//		if (t1!=null && !t1.equals(t2)) {
//			logger.debug("Tokenizer bug !!! {} -> {} {}", tokens, t1, t2);
//		}
		
//		contentTokens.append(nGramTokenizerHashed.getHash(tokens,hashedTokensConsumer,cellRefStr)).append(" ");
		contentTokens.append(docAuthorityTokenizer.getHashReuseTokenizer(tokens,hashedTokensConsumer,cellRefStr,tokenizerObject)).append(" ");
	}
	private void appendOrigContentTokens(final String tokens) {
		origContentTokens
				.append(tokens)
				.append("\t");
	}

	private void appendOrigContentNewRow() {
		origContentTokens.append("\n");
	}

	public ExcelSheet (ExcelParseParams excelParseParams) {
        init(excelParseParams);
	}

	public ExcelSheet(final String sheetName, final String fullSheetName, final HashedTokensConsumer hashedTokensConsumer, ExcelParseParams excelParseParams) {
		init(excelParseParams);
		this.patternSearchActive = excelParseParams.isPatternSearchActive();
		this.useFormattedPatterns = excelParseParams.isUseFormattedPatterns();
		this.sheetName = sheetName;
		this.fullSheetName = fullSheetName;
		this.hashedTokensConsumer=hashedTokensConsumer;
	}
	
	private void init(ExcelParseParams excelParseParams) {
        this.excelParseParams = excelParseParams;
        this.docAuthorityTokenizer = ExcelParseParams.getDocAuthorityTokenizer();
        if (docAuthorityTokenizer == null) {
            logger.warn("DocAuthorityTokenizer is missing");
        }
        createHistograms();
		
		if (logger.isTraceEnabled()) {
			styleDebHist = new ClaHistogramCollection<>();
		}
	}

	private void createHistograms() {
//		for (int i=0;i<strHistogramNames.length;++i) {
//			strHistograms.getCollections().put(strHistogramNames[i], new ClaHistogramCollection<String>());
//		}
		for (int i = 0; i < ClaHistogramCollection.excelLongHistogramNames.length; ++i) {
			longHistograms.getCollections().put(ClaHistogramCollection.excelLongHistogramNames[i].name(), new ClaHistogramCollection<>());
		}
	}

	public Integer getId() {
		return id;
	}

	public ExcelSheetMetaData getMetaData() {
		return metaData;
	}

//	public ClaSuperCollection<String> getStrHistograms() {
//		return strHistograms;
//	}

	public ClaSuperCollection<Long> getLongHistograms() {
		return longHistograms;
	}

	public ClaHistogramCollection<Long> getLongHistogram(final PVType name) {
		return longHistograms.getCollections().get(name.name());
	}

//	public ClaHistogramCollection<String> getStrHistogram(final PVType name) {
//		return strHistograms.getCollections().get(name);
//	}
	
	// Convert string items to their MD5 hash value.
	private void histogramAddItem(final PVType name, final String item) {
//		final ClaHistogramCollection<String> hist = getStrHistogram(name);
//		if (hist == null)
//			throw new RuntimeException("Histogram name not found: " + name);
//		hist.addItem(item);

		histogramAddItem(name, docAuthorityTokenizer.MD5Long(item));
	}

	private void histogramAddItem(final PVType name, final Long item) {
		final ClaHistogramCollection<Long> hist = getLongHistogram(name);
		if (hist == null) {
			throw new RuntimeException("Histogram name not found: " + name);
		}
		hist.addItem(item);
	}

	// Convert string items to their MD5 hash value.
	public void histogramAddStrItems(final PVType name, final List<String> items) {
//		final ClaHistogramCollection<String> hist = getStrHistogram(name);
//		if (hist == null)
//			throw new RuntimeException("Histogram name not found: " + name);
//		hist.addItems(items);

		if (items != null) {
			items.forEach(item -> histogramAddItem(name, docAuthorityTokenizer.MD5Long(item)));
		}
	}

	private void histogramAddLongItems(final PVType name, final Collection<Long> items) {
		final ClaHistogramCollection<Long> hist = getLongHistogram(name);
		if (hist == null) {
			throw new RuntimeException("Histogram name not found: " + name);
		}
		hist.addItems(items);
	}
	
	public float getSimilarity(final ExcelSheet other, final PVType name) {
		final ClaHistogramCollection<Long> longHist = getLongHistogram(name);
		if (longHist != null) {
			return longHist.getNormalizedSimilarityRatio(other.getLongHistogram(name));
		}
//		final ClaHistogramCollection<String> strHist = getStrHistogram(name);
//		if (strHist != null) {
//			return strHist.getNormalizedSimilarityRatio(other.getStrHistogram(name));
//		}
		throw new RuntimeException("Histogram name not found: " + name);
	}

	@Deprecated
	// This function can not be supported in big-data similarity architecture
	public int getOrderedMatchCount(final ExcelSheet other, final PVType name) {
		final ClaHistogramCollection<Long> longHist = getLongHistogram(name);
		if (longHist != null) {
			return longHist.getOrderedSimilarityCount(other.getLongHistogram(name));
		}
//		final ClaHistogramCollection<String> strHist = getStrHistogram(name);
//		if (strHist != null) {
//			return strHist.getOrderedSimilarityCount(other.getStrHistogram(name));
//		}
		throw new RuntimeException("Histogram name not found: " + name);
	}


	public void finalizeIngestion() {
		// TODO: make sure this is called
		metaData.setSheetName(sheetName);
		metaData.setFullSheetName(fullSheetName);
		metaData.setNumOfBlanks(numOfBlanks);
		metaData.setCellsSkipped(cellsSkipped);
		metaData.setNumOfCells(numOfCells);
		metaData.setNumOfErrors(numOfErrors);
		metaData.setNumOfFormulas(numOfFormulas);
		metaData.addProposedTitles(getProposedTitlesList());

		ingestTitles();
		
//		Arrays.asList(ExcelSheet.strHistogramNames).stream()
//		.forEach(name->getStrHistogram(name).applyLimitations());
		Arrays.stream(ClaHistogramCollection.excelLongHistogramNames)
			.map(this::getLongHistogram)
			.forEach(ClaHistogramCollection::applyLimitations);

		final PVType[] valueOrientedPVTypes = {
			PVType.PV_CellValues
			,PVType.PV_ConcreteCellValues
			,PVType.PV_TextNgrams
		};

		Arrays.stream(valueOrientedPVTypes)
			.map(this::getLongHistogram).filter(Objects::nonNull)
			.forEach(this::trimOutlier);
		
		// Add sheet full name as a proposed title (in low prio)
		final String pTitle = getFullSheetName();
		if (isValidSheetName(pTitle)) {
			setProposedTitle("S8", SHEET_NAME_RATE_VALUE, pTitle);
		}
		
		if (logger.isTraceEnabled()) {
			logger.trace("Styles deb for sheet {}: {}", sheetName, styleDebHist.toString());
		}
	}
	
	private boolean isValidSheetName(String pTitle) {
		final String lcTitle = pTitle.toLowerCase();
		return excelParseParams.getIgnoredSheetNamePrefixesAsTitlesSet().stream().noneMatch(lcTitle::startsWith);
	}
	
	private List<String> getProposedTitlesList() {
		return proposedTitles.entrySet().stream()
				.sorted((e1,e2)->e2.getValue().compareTo(e1.getValue()))
				.map(Map.Entry::getKey).collect(Collectors.toList());
	}



	private void trimOutlier(final ClaHistogramCollection<Long> hist) {
		if (hist.getItemsCount() > OUT_LIER_TRIM_MIN_NUM_ITEMS) {
			final Long outLier = hist.getOutlierByDiff(OUT_LIER_TRIM_DIFF_FACTOR, OUT_LIER_TRIM_MIN_ITEM_COUNT);
			if (outLier != null) {
				final int count = hist.getCount(outLier);
				logger.debug("Trimming value {} with count {}. GlobCount={} #items={}",
						outLier, count, hist.getGlobalCount(), hist.getItemsCount());
				
				hist.removeItem(outLier);
			}
		}
	}
	
	private void ingestTitles() {
		if (docAuthorityTokenizer == null) {
			return;
		}
		
		final List<String> titles = metaData.getProposedTitles();
		
		titles.stream().filter(t-> (t!=null && t.length()> MIN_TEXT_LENGTH_FOR_NG_CALC))
		.forEach((t) -> {
			final List<Long> nGrams = docAuthorityTokenizer.getHashlist(t, TITLE_NGRAM_SIZE);
			histogramAddLongItems(PVType.PV_TitlesNgrams, nGrams);
		});
	}

    public void addExcelCellContentOnly(final String cellRefStr, ExcelCell excelCell, int type) {
        if(excelCell.isBlank()) {
            numOfBlanks++;
            return;
        }
        if(excelCell.isError()) {
            numOfErrors++;
            return;
        }
        numOfCells++;
        if(excelCell.isFormula()) {
            numOfFormulas++;
        }
        cellsSkipped++;
        addExcelCellTokenValues(cellRefStr,excelCell,type, true);
    }

	/**
	 * Add cell to the sheet
	 * @param columnIndex			column index
	 * @param rowNum				row index
	 * @param excelCell				cell object
	 * @param detailedAnalysis		whether to perform detailed analysis
	 * @param cellRefStr			cell reference string
	 * @param isTokenMatcherActive	whether the token matcher is active
	 */
	public void addExcelCell(final int columnIndex, final int rowNum, final ExcelCell excelCell, final boolean detailedAnalysis,
			final String cellRefStr, final boolean isTokenMatcherActive){
		if(excelCell.isBlank()) {
			numOfBlanks++;
			return;
		}
		if(excelCell.isError()) {
			numOfErrors++;
			return;
		}
		numOfCells++;

		if(excelCell.isFormula()) {
			numOfFormulas++;
		}

		if (!detailedAnalysis){
			cellsSkipped++;
		}
		
		final String val = excelCell.toStringValue();
		if ((!excelCell.isBlank()) && !excelCell.isError()) {
			if (detailedAnalysis) {
				histogramAddItem(PVType.PV_CellValues, val);
				if(!excelCell.isFormula()) {
					histogramAddItem(PVType.PV_ConcreteCellValues, val);
				}
				if (excelCell.getType() == ExcelCell.CELL_TYPE_STRING &&
						val != null && val.length() > MIN_TEXT_LENGTH_FOR_NG_CALC &&
                        docAuthorityTokenizer != null) {
					final List<Long> nGrams = docAuthorityTokenizer.getHashlist(val, TEXT_NGRAM_SIZE);
					histogramAddLongItems(PVType.PV_TextNgrams, nGrams);
				}
			}
		}
		final ExcellCellStyle cellStyle = excelCell.getCellStyle();
        if (cellStyle == null) {
            throw new RuntimeException("Missing cellStyle on excel cell - problem when parsing cell");
        }
		// count formulas
		if(excelCell.isFormula()) {
            final String dataFormat = cellStyle.getFormat();
//            logger.info(excelCell.getFormula()+":"+excelCell.getFormulaIngestion());
            final String formulaStr = dataFormat+"!"+excelCell.getFormulaIngestion();
			histogramAddItem(PVType.PV_Formulas, formulaStr);
		}
		if (cellStyle.notStandard()) {
			final String styleSig = cellStyle.getStringSignature();
			histogramAddItem(PVType.PV_ExcelStyles, styleSig);
			if (logger.isTraceEnabled()) {
				styleDebHist.addItem(styleSig);
			}
		}
		
		
		analyzeCellStyle(columnIndex, rowNum, excelCell);
		
		// Update row layout
		processCellLayout(excelCell.getCellStyle());

		if (excelHeadingAnalyzer == null) {
			excelHeadingAnalyzer = new ExcelHeadingAnalyzer();
		}
		
		excelHeadingAnalyzer.headingProcessCell(sheetName, columnIndex, rowNum, excelCell);
		// Process tokens for entity-extraction mechanism
		if (isTokenMatcherActive) {
			addExcelCellTokens(excelCell, cellRefStr);
		}
	}
	

    private void addExcelCellTokenValues(String cellRefStr, ExcelCell excelCell, int type, boolean isContentOnly) {
		String strVal;
        switch (type) {
		case ExcelCell.CELL_TYPE_STRING:
			strVal = isContentOnly ? excelCell.toStringValue() : excelCell.getStringValue();
			if (strVal.length() >= ExcelParseParams.getTokenMatcherExcelCellMinStringLength()) {
				if(patternSearchActive) {
					appendOrigContentTokens(strVal);
				}
				appendContentTokens(strVal,cellRefStr);
			}
			break;
		case ExcelCell.CELL_TYPE_NUMERIC:
			strVal = isContentOnly ? excelCell.toStringValue() : excelCell.getNumericValue().toString();
			// skip number that are not all digits or '-,./' - basically ignore signtific notation + dates + currency notation.
			if (((!ExcelParseParams.getTokenMatcherExcelIgnoreComplexNumbers()) ||
					(ALL_DIGITS_PATTERN.matcher(strVal).matches()) ||
					(!isContentOnly && useFormattedPatterns && ALL_DIGITS_PATTERN.matcher(excelCell.getStringValue()).matches())
			) &&
					// skip short numbers (set config to zero to ignore
					strVal.length() >= ExcelParseParams.getTokenMatcherExcelCellMinNumberLength()) {
				if (patternSearchActive) {
					appendOrigContentTokens(isContentOnly || !useFormattedPatterns ? strVal : excelCell.getStringValue());
				}
				if (!isContentOnly && useFormattedPatterns) {
					appendContentTokens(excelCell.getStringValue(), cellRefStr);
				} else {
					appendContentTokens(strVal, cellRefStr);
				}
			}
			break;
		}
    }

    // To be called after last cell was added.
	public void processStyle() {
		// Finalize heading identification
		if (excelHeadingAnalyzer != null) {
			excelHeadingAnalyzer.headingFinalize();
			sheetHeadings = excelHeadingAnalyzer.getHeadingsForSheet(sheetName);
			if (sheetHeadings!=null) {
				sheetHeadings.forEach( h -> histogramAddItem(PVType.PV_RowHeadings, h) );
			}
			excelHeadingAnalyzer = null;	// free some memory from the workbook object.
		}
		
		// Title processing
		String titleModifier = "";
		if (potencialTitleCell != null) {
			if (!potencialTitleCell.getCellStyle().isHeading()) {
				// down rate if cell holds default/regular worksheet style
				titleModifier = ":def";
				potencialTitleRate = potencialTitleCell.getCellStyle().getStyleHeadingPotentialRate() + 1;
			}
			else
			if (selectedCellStyleRepeatCount >= 2) {
				// down rate if same style was repeated in row
				titleModifier = ":rep" + selectedCellStyleRepeatCount;
				potencialTitleRate = potencialTitleCell.getCellStyle().getStyleHeadingPotentialRate() + 2;
			}

			final String pTitle = potencialTitleCell.getStringValue();

			if (sheetHeadings == null) {
				setProposedTitle("P3", potencialTitleRate, pTitle);
			}
			else {
				List<String> headSl = sheetHeadings.subList(0,Math.min(MAX_NUM_HEADINGS_IN_PROPOSED_TITLE, sheetHeadings.size()));
				if (headSl.contains(pTitle)) {
					if (excelParseParams.isUseTableHeadingsAsTitles()) {
						setProposedTitle("H3", potencialTitleRate, headSl.stream().collect(Collectors.joining(", ")));
					}
				}
				else {
					setProposedTitle("P3", potencialTitleRate, pTitle);
				}
			}
		}
		potencialTitleCell = null;
		
		// If no proposed titles and there are headings, use headings as a proposed title
//		if (getProposedTitles().size()==0 && sheetHeadings!=null) {
//			final String pTitle = sheetHeadings.stream().limit(MAX_NUM_HEADINGS_IN_PROPOSED_TITLE).collect(Collectors.joining(", "));
//			setProposedTitle("P8", getProposedTitles().size(), pTitle);
//		}

		// process sheet layout
		processSheetLayout();
	}

	private void addExcelCellTokens(final ExcelCell excelCell, final String cellRefStr) {
		if (excelCell.isBlank() || excelCell.isError()) {
			return;
		}

		int type = excelCell.getType();
		addExcelCellTokenValues(cellRefStr, excelCell, type, false);
	}
	
	private void processCellLayout(final ExcellCellStyle style) {
		if (rowLayoutStarted || style.notStandard()) {
			rowLayoutStarted = true;
			final int cellLayout = style.getLayoutHashCode(); 

			if (cellLayout == lastCellLayout) {
				lastCellLayoutRepeat++;
			}
			else {
				lastCellLayoutRepeat = 0;
				lastCellLayout = cellLayout;
			}
			if (lastCellLayoutRepeat < MAX_CELL_LAYOUT_REPEAT_COUNT) {
				rowLayoutHash = 773 * rowLayoutHash + (style.notStandard()?cellLayout:31);	// account all 'standard' format cells as same layout
				cellsInRowLayoutCount++;
			}
			if (style.notStandard()) {
				lastSpecialRowLayoutHash = rowLayoutHash;	// keep hash value for last special cell to ignore tail of non-special cells in row.
				cellsInRowLayoutCountSnapshot = cellsInRowLayoutCount;
			}
		}
	}

	private void processRowLayout() {
		if (rowLayoutHash != 0) {

			if (rowLayoutHash == lastRowLayout) {
				lastRowLayoutRepeat++;
			}
			else {
				lastRowLayoutRepeat = 0;
				lastRowLayout = rowLayoutHash;
			}

			if (lastRowLayoutRepeat < MAX_ROW_LAYOUT_REPEAT_COUNT) {
				// use the hash calculated for the last special cell in the row.
				histogramAddItem(PVType.PV_SheetLayoutSignatures, Long.valueOf(lastSpecialRowLayoutHash));
				
				sheetLayoutHash = sheetLayoutHash * 877 + lastSpecialRowLayoutHash;
			}

			if (cellsInRowLayoutCountSnapshot >= MIN_CELLS_IN_SIGNIFICANT_LAYOUT_ROW) {
				numOfSignificantLayoutRows++;
			}
		}

		rowLayoutStarted = false;
		rowLayoutHash = 0;
		lastCellLayout = 0;
		lastCellLayoutRepeat = 0;
		lastSpecialRowLayoutHash = 0;
		cellsInRowLayoutCount = 0;
		cellsInRowLayoutCountSnapshot = 0;
	}

	private void processSheetLayout() {
		processRowLayout();
		
		// no need for additional processing
	}

	private void analyzeCellStyle(final int columnIndex, final int rowNum, final ExcelCell excelCell) {

		/*
		 * Assuming cells are added by order they appear in the workbook - sheet by sheet, iterated by rows and then by cell within row.
		 */
		if (rowNum != lastRowIndex){
			// Starting a new row ...
			rowCountInSheet++;
			lastRowIndex = rowNum;
			colCountInRow = 0;
			
			processRowLayout();

			if (patternSearchActive) {
				appendOrigContentNewRow();
			}
		}
		colCountInRow++;

		final ExcellCellStyle newStyle = excelCell.getCellStyle();
		if (potencialTitleRowIndex == rowNum &&
				potencialTitleCell != null &&
				newStyle.equals(potencialTitleCell.getCellStyle())) {
			selectedCellStyleRepeatCount++;
		}
		
		// Check if cell meets title candidate criteria
		final String value = excelCell.getStringValue();
		if (rowCountInSheet <= ROW_COUNT_THRESHOLD &&
				colCountInRow <= COL_COUNT_THRESHOLD &&
				excelCell.getType() == ExcelCell.CELL_TYPE_STRING && 
				value.length() >= MIN_TITLE_LENGTH &&
				value.length() <= MAX_TITLE_LENGTH &&
				newStyle.getFontSize() >= MIN_TITLE_FONT_SIZE_IN_POINTS) {
			
			if (potencialTitleCell == null) {
				potencialTitleCell = excelCell;
				selectedCellStyleRepeatCount = 0;
				potencialTitleRate = newStyle.getStyleHeadingPotentialRate();
				potencialTitleRowIndex = rowNum;
				potencialTitleColIndex = columnIndex;
				selectedCellStyleRepeatCount = 0;
			}
			else {
				final ExcellCellStyle lastStyle = potencialTitleCell.getCellStyle();
				
				if (lastStyle.isBold() && newStyle.isBold() && newStyle.getFontSize() > lastStyle.getFontSize()) {
					potencialTitleCell = excelCell;
					selectedCellStyleRepeatCount = 0;
					potencialTitleRate = newStyle.getStyleHeadingPotentialRate();
					potencialTitleRowIndex = rowNum;
					potencialTitleColIndex = columnIndex;
				}
				else if ((!lastStyle.isBold()) && newStyle.isBold()) {
					potencialTitleCell = excelCell;
					selectedCellStyleRepeatCount = 0;
					potencialTitleRate = newStyle.getStyleHeadingPotentialRate() + 1;
					potencialTitleRowIndex = rowNum;
					potencialTitleColIndex = columnIndex;
				}
				else if ((!lastStyle.isBold()) && (!newStyle.isBold()) && newStyle.getFontSize() > lastStyle.getFontSize()) {
					potencialTitleCell = excelCell;
					selectedCellStyleRepeatCount = 0;
					potencialTitleRate = newStyle.getStyleHeadingPotentialRate() + 2;
					potencialTitleRowIndex = rowNum;
					potencialTitleColIndex = columnIndex;
				}
				// Look for subTitle
				if (potencialTitleCell != null && 
						lastStyle.getFontSize() > MIN_SUPER_TITLE_FONT_SIZE &&
						lastStyle.isBold() == newStyle.isBold() &&
						lastStyle.getColor() == newStyle.getColor() &&
						(potencialTitleRowIndex == rowNum || potencialTitleColIndex == columnIndex)) {
					// In case of potential subtitle, add it to the pool in any case
					// There is a chance for potential false positives
					setProposedTitle("P5", potencialTitleRate, value);
				}
			}
		} // cell meets title candidate criteria
	}	// void analyzeCellStyle


	public String getSheetName() {
		return metaData.getSheetName();
	}

	public String getFullSheetName() {
		return metaData.getFullSheetName();
	}

	public Map<String, String> getProposedTitles() {
		return proposedTitles;
	}

	public List<String> getSheetHeadings() {
		return sheetHeadings;
	}
	private void setProposedTitle(final String keyPref, int rate, final String proposedTitle) {
		
		// Reduce title rate if it is a single token with a digit
		if ((!proposedTitle.contains(" ")) && ANY_DIGIT_PATTERN.matcher(proposedTitle).find()) {
			rate -= excelParseParams.getSingleTokenNumberedTitleRateReduction();
			if (rate<1) rate = 1;
		}

		setProposedTitle(String.format(TITLE_KEY_FORMAT_STRING, keyPref, rate, proposedTitles.size()), proposedTitle);
	}
	private void setProposedTitle(final String key, final String proposedTitle) {
		final String oldRate = proposedTitles.get(proposedTitle);
		if (oldRate==null || oldRate.compareTo(key)<0) {
			proposedTitles.put(proposedTitle, key);
		}
	}

	public static List<String> getS_valueIgnoreList() {
		return s_valueIgnoreList;
	}

	public static void setS_valueIgnoreList(final List<String> valueIgnoreList) {
		s_valueIgnoreList = valueIgnoreList;
	}

	public static List<String> getS_formulaIgnoreList() {
		return s_formulaIgnoreList;
	}

	public static void setS_formulaIgnoreList(final List<String> formulaIgnoreList) {
		s_formulaIgnoreList = formulaIgnoreList;
	}


	private static int compareHeadingsExact(final List<String> headings1, final List<String> headings2) {
		if (headings1 == null || headings2 == null) {
			return 0;
		}
		
		// Start with simple streamline comparison.
		int count = 0;
		final Iterator<String> i1 = headings1.iterator();
		final Iterator<String> i2 = headings2.iterator();
		boolean b1 = i1.hasNext(); 
		boolean b2 = i2.hasNext(); 
		while (b1 && b2) {
			if (!i1.next().equals(i2.next())) {
				break;
			}
			count++;
			b1 = i1.hasNext(); 
			b2 = i2.hasNext(); 
		}
		return count;
	}

	private static int compareHeadingsOrdered(final List<String> headings1, final List<String> headings2) {
		if (headings1 == null || headings2 == null) {
			return 0;
		}

		// Count all ordered item match
		int count = 0;
		final List<String> h2Copy = new LinkedList<>(headings2);
		final Iterator<String> i1 = headings1.iterator();
		while (i1.hasNext() && !h2Copy.isEmpty()) {
			final int i = h2Copy.indexOf(i1.next());
			if (i >= 0) {
				count++;
				h2Copy.remove(i);
			}
		}
		return count;
	}

	@Override
	public String toString() {
		return "ExcelSheet [#cells=" + metaData.getNumOfCells() + " #formulas=" + metaData.getNumOfFormulas() + "]";
	}

	public static String toCsvStringTitle() {
		return toCsvStringTitle(",");
	}

	public static String toCsvStringTitle(final String sep) {
		return 
				Arrays.stream(ClaHistogramCollection.excelLongHistogramNames)
						.map(PVType::toString)
						.collect(Collectors.joining(sep)) +
				sep + 
				"title";
	}

	public String toCsvString() {
		return toCsvString(",");
	}
	
	public String toCsvString(final String sep) {
		return 
			Arrays.stream(ClaHistogramCollection.excelLongHistogramNames)
				.map(name->name.toString().substring(0, 3)+":"+String.valueOf(getLongHistogram(name).getPropertiesString()))
				.collect(Collectors.joining(sep)) +
				sep +
				((metaData.getProposedTitles()==null)?"":
					("\"" + metaData.getProposedTitles().stream().collect(Collectors.joining("|"))+
							"\""));
	}
	
	public String getHistPropertiesString(final PVType name) {
//		final ClaHistogramCollection<String> sh = getStrHistogram(name);
//		if (sh != null) {
//			return sh.getPropertiesString();
//		}
		final ClaHistogramCollection<Long> lh = getLongHistogram(name);
		if (lh != null) {
			return lh.getPropertiesString();
		}
		throw new RuntimeException("Histogram name not found: " + name);
	}

	void setUserSignature(SigningStringBuilder signing) {
		this.origContentTokens = signing;
	}

	public Set<SearchPatternCountDto> getSearchPatternsCounting() {
		return searchPatternsCounting;
	}

	public void setSearchPatternsCounting(Set<SearchPatternCountDto> searchPatternsCounting) {
		this.searchPatternsCounting = searchPatternsCounting;
	}
}
