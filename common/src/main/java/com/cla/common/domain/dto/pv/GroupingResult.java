package com.cla.common.domain.dto.pv;

import java.util.UUID;

/**
 * Created by uri on 02/02/2017.
 */
public class GroupingResult {
    private char groupingDeb;
    private boolean match = false;
    private String simCalc;
    private int simCalc2;
    private UUID sourceFileGroup;

    public GroupingResult(char groupingDeb) {
        this.groupingDeb = groupingDeb;
        this.match = true;
    }

    public GroupingResult(String simCalc, int simCalc2, char groupingDeb, boolean match) {
        this.match = match;
        this.simCalc = simCalc;
        this.simCalc2 = simCalc2;
        this.groupingDeb = groupingDeb;
    }

    public GroupingResult(char groupingDeb, UUID sourceFileGroup) {
        this.sourceFileGroup = sourceFileGroup;
        this.groupingDeb = groupingDeb;
        this.match = true;
    }

    public static GroupingResult falseGroupingResult() {
        char newGrpInd = ' ';
        return new GroupingResult(null, 0, newGrpInd, false);
    }

    public char getGroupingDeb() {
        return groupingDeb;
    }

    public void setSimCalc(String simCalc) {
        this.simCalc = simCalc;
    }

    public String getSimCalc() {
        return simCalc;
    }

    public void setSimCalc2(int simCalc2) {
        this.simCalc2 = simCalc2;
    }

    public int getSimCalc2() {
        return simCalc2;
    }

    public boolean isMatch() {
        return match;
    }

    public void setMatch(boolean match) {
        this.match = match;
    }

    public UUID getSourceFileGroup() {
        return sourceFileGroup;
    }

    public void setSourceFileGroup(UUID sourceFileGroup) {
        this.sourceFileGroup = sourceFileGroup;
    }

    public static GroupingResult falseGroupingResult(String simCalc, int simCalc2) {
        char newGrpInd = ' ';
        return new GroupingResult(simCalc, simCalc2, newGrpInd, false);
    }
}
