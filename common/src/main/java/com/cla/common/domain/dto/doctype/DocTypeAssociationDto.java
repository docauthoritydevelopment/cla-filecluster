package com.cla.common.domain.dto.doctype;

import com.cla.common.domain.dto.filetag.FileTagSource;
import com.cla.common.domain.dto.scope.Scope;
import com.cla.common.domain.dto.scope.ScopedEntity;
import com.cla.common.domain.dto.security.EntityAssociationDetails;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class DocTypeAssociationDto implements EntityAssociationDetails, ScopedEntity {

    private static final long SINGLE_CATEGORY_FIXED_ID = -1;
    private Long id;
    private DocTypeDto docTypeDto;
    private long associationTimeStamp;
    private FileTagSource fileTagSource;
    private Scope scope;
    private Long originalFolderId;
    private boolean deleted = false;
    private Integer originDepthFromRoot;

    public DocTypeAssociationDto() {

    }

    public DocTypeAssociationDto(Long id, DocTypeDto docTypeDto, long associationTimeStamp, FileTagSource fileTagSource,
                                 Scope scope, Long originalFolderId, Integer originDepthFromRoot) {
        this.id = id;
        this.docTypeDto = docTypeDto;
        this.associationTimeStamp = associationTimeStamp;
        this.fileTagSource = fileTagSource;
        this.scope = scope;
        this.originalFolderId = originalFolderId;
        this.originDepthFromRoot = originDepthFromRoot;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public boolean isSingleValue() {
        return docTypeDto.isSingleValue();
    }

    /**
     * The is no categories for doc types, they are category by themselves, so we return fixed id
     * @return
     */
    @Override
    public long getCategoryId() {
        return SINGLE_CATEGORY_FIXED_ID;
    }

    @Override
    public FileTagSource getFileTagSource() {
        return fileTagSource;
    }

    @Override
    public Long getEntityId() {
        return docTypeDto.getId();
    }

    @Override
    public Long getOriginalFolderId() {
        return originalFolderId;
    }

    public void setOriginalFolderId(Long originalFolderId) {
        this.originalFolderId = originalFolderId;
    }

    @Override
    public boolean isImplicit() {
        return docTypeDto.isImplicit();
    }

    @Override
    public Scope getScope() {
        return scope;
    }

    public long getAssociationTimeStamp() {
        return associationTimeStamp;
    }

    public DocTypeDto getDocTypeDto() {
        return docTypeDto;
    }

    public void setDocTypeDto(DocTypeDto docTypeDto) {
        this.docTypeDto = docTypeDto;
    }

    public void setAssociationTimeStamp(long associationTimeStamp) {
        this.associationTimeStamp = associationTimeStamp;
    }

    public void setFileTagSource(FileTagSource fileTagSource) {
        this.fileTagSource = fileTagSource;
    }

    public void setScope(Scope scope) {
        this.scope = scope;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    @Override
    public Integer getOriginDepthFromRoot() {
        return originDepthFromRoot;
    }

    public void setOriginDepthFromRoot(Integer originDepthFromRoot) {
        this.originDepthFromRoot = originDepthFromRoot;
    }
}
