package com.cla.common.domain.dto.export;

import lombok.Data;

import java.util.Map;

@Data
public class FileExportRequestDto {

    private String fileNamePrefix;
    private Map<String, String> exportParams;
    private Map<String, String> userParams;

}
