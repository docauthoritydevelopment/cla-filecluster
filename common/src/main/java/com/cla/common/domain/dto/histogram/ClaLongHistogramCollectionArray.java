package com.cla.common.domain.dto.histogram;


import org.apache.commons.lang3.tuple.Pair;

import java.util.*;
import java.util.stream.IntStream;

public class ClaLongHistogramCollectionArray extends ClaHistogramCollection<Long> {

    private static final long serialVersionUID = 1L;
    private static final int DEFAULT_LIMIT = 63900;

//    transient private Map<Long, Integer> histogram = new LinkedHashMap<>(); // TODO: remove
    private long[] histogramArrKey = null;
    private int[] histogramArrValue = null;
    private int[] histogramOrigKeyIndex = null;     // Indexes to the original key list before sorting
    private float[] weightsArrValue = null;

    private long[] perfSubArrKey = null;
    private int[] perfSubArrLeft = null;
    private int[] perfSubArrRight = null;
    private static final int perfSubArrThreshold = 128; // Min size to activate this mechanism
    private static final int perfSubSize = 15; // Min size to activate this mechanism

    private int globalCount = 0;
    private int skippedCount = 0;
    private int markItemsSize = 0;
    private HistogramType type;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public ClaLongHistogramCollectionArray() {}

    public ClaLongHistogramCollectionArray(final HistogramType type, final long[] histogramArrKey, final int[] histogramArrValue,
                                           final int[] histogramOrigKeyIndex, final float[] weightsArrValue,
                                           final long[] perfSubArrKey, final int[] perfSubArrLeft, final int[] perfSubArrRight) {
        this.histogramArrKey = histogramArrKey;
        this.histogramArrValue = histogramArrValue;
        this.histogramOrigKeyIndex = histogramOrigKeyIndex;
        this.weightsArrValue = weightsArrValue;
        this.perfSubArrKey = perfSubArrKey;
        this.perfSubArrLeft = perfSubArrLeft;
        this.perfSubArrRight = perfSubArrRight;
        this.globalCount = IntStream.range(0, histogramArrValue.length).map(i->histogramArrValue[i]).sum();
        this.type = type;
    }

    private ClaLongHistogramCollectionArray(final ClaHistogramCollection<Long> claHistogramCollection) {
        this.type = claHistogramCollection.getType();
        this.globalCount = claHistogramCollection.getGlobalCount();
        this.skippedCount = claHistogramCollection.getSkippedCount();
        this.markItemsSize = claHistogramCollection.getMarkedItemsSize();
        buildArrays(claHistogramCollection.getCollection());
    }

    public static ClaHistogramCollection<Long> createIfNeeded(final ClaHistogramCollection<Long> claHistogramCollection) {
        return (claHistogramCollection instanceof ClaLongHistogramCollectionArray) ? claHistogramCollection :
                create(claHistogramCollection);
    }

    public static ClaHistogramCollection<Long> create(final ClaHistogramCollection<Long> claHistogramCollection) {
        if (claHistogramCollection instanceof ClaLongHistogramCollectionArray) {
            ClaLongHistogramCollectionArray clhca = (ClaLongHistogramCollectionArray)claHistogramCollection;
            return new ClaLongHistogramCollectionArray(clhca.type, clhca.histogramArrKey, clhca.histogramArrValue,
                    clhca.histogramOrigKeyIndex, clhca.weightsArrValue, clhca.perfSubArrKey, clhca.perfSubArrLeft, clhca.perfSubArrRight);
        }
        return new ClaLongHistogramCollectionArray(claHistogramCollection);
    }

    private void buildArrays(final Map<Long,Integer> histogram) {
        if (histogram == null) {
            return;
        }
        // Init arrays
        histogramArrKey = new long[histogram.size()];
        histogramArrValue = new int[histogram.size()];
        histogramOrigKeyIndex= new int[histogram.size()];
        if (histogramArrKey.length > perfSubArrThreshold) {
            perfSubArrKey = new long[perfSubSize];
            perfSubArrLeft = new int[perfSubSize];
            perfSubArrRight = new int[perfSubSize];
        }
        // Fill arrays content
        LinkedHashMap<Long, Pair<Integer,Integer>> sortInxMap = new LinkedHashMap<>(histogram.size());
        final int[] counter = {0};
        histogram.forEach((k, v) -> sortInxMap.put(k,Pair.of(v, counter[0]++)));
        counter[0] = 0;
        int[] perfSubNextLeft = {0};
        int[] perfSubPoint = {0};
        final float perfPageSize = histogramArrKey.length / (float)(perfSubSize + 1);
        List<Pair<Integer,Integer>> sortList2 = new ArrayList<>(histogram.size());
        sortInxMap.entrySet().stream().sorted((a,b)->a.getKey().compareTo(b.getKey())).forEach(e->{
            histogramArrKey[counter[0]] = e.getKey();
            Pair<Integer,Integer> p = e.getValue();
            histogramArrValue[counter[0]] = p.getKey().intValue();
            sortList2.add(Pair.of(p.getValue().intValue(), counter[0]));
            // Looking for the point that falls between even splits of the keyArray
            if (perfSubArrKey != null &&
                    perfSubPoint[0] < perfSubArrKey.length &&
                    (counter[0] + 1) < ((perfSubPoint[0] + 1) * perfPageSize) &&
                    (counter[0] + 2) >= ((perfSubPoint[0] + 1) * perfPageSize)) {
                // Set a point
                int subPoint = perfSubPoint[0];
                perfSubArrKey[subPoint] = histogramArrKey[counter[0]];
                perfSubArrLeft[subPoint] = perfSubNextLeft[0];
                perfSubNextLeft[0] = counter[0] + 1;
                perfSubArrRight[subPoint] = perfSubNextLeft[0];
                perfSubPoint[0]++;
            }
            counter[0]++;
        });
        counter[0] = 0;
        // Create a reversed index array by double sorting the initial references
        //      histogramOrigKeyIndex[i] -> is the index of the i'th key in the original histogram within the sorted keys array.
        sortList2.stream().sorted((a,b)->a.getKey().compareTo(b.getKey())).forEach(p->{
            histogramOrigKeyIndex[counter[0]++] = p.getValue().intValue();
        });
        calcWeightsArrays();
    }

    private void calcWeightsArrays() {
        weightsArrValue = new float[histogramArrKey.length];
        if (type == HistogramType.MARKER) {
            // Balance weights to be comparable to the 'calcWeight()' value.
            final float denom = Float.max(globalCount - markItemsSize,1f);
            IntStream.range(0, weightsArrValue.length).forEach(i -> weightsArrValue[i] = ((float) histogramArrValue[i]) / denom);
        } else {
            // histogram.forEach((k, v) -> weights.put(k, ((float) v) / ((float) globalCount)));
            IntStream.range(0, weightsArrValue.length).forEach(i -> weightsArrValue[i] = ((float) histogramArrValue[i]) / ((float) globalCount));
        }
    }

//    /**
//     * @return external int value based on the histogram type. Equal to getCount() in most cases
//     */
//    public int getValue(final long item) {
//        return getCount(item, histogramArrKey, histogramArrValue);
//    }
//    public int getCount(final long item, long[] keyArray, int[] valueArray) {
//        return getCount(item, 0, keyArray, valueArray);
//    }
//    public int getCount(final long item, int start, long[] keyArray, int[] valueArray) {
//        int inx = binarySearch(keyArray, start, keyArray.length, item);
//        return (inx<0)? 0 : valueArray[inx];
//    }

    /**
     *
     * @param keyArray          Array with keys to search in
     * @param key               Searched item
     * @param perfSubArrKey     sub array key values
     * @param perfSubArrLeft    sub array left-index referenced
     * @param perfSubArrRight   sub array right-index referenced
     * @return true if key value found within the keyArray values; false otherwise.
     */
    private boolean contains(long[] keyArray, long key, long[] perfSubArrKey, int[] perfSubArrLeft, int[] perfSubArrRight) {
        int left = 0;
        int right = keyArray.length;

        if (perfSubArrKey != null) {
            int i = 0;
            if (key >= perfSubArrKey[perfSubArrKey.length-1]) {
                if (key == perfSubArrKey[perfSubArrKey.length-1]) {
                    return true;
                }
                left = perfSubArrRight[perfSubArrKey.length - 1];
            } else {
                while (i < perfSubArrKey.length && key > perfSubArrKey[i]) i++;
                if (i < perfSubArrKey.length) {
                    if (key == perfSubArrKey[i]) {
                        return true;
                    }
                    left = perfSubArrLeft[i];
                    right = perfSubArrRight[i];
                }
            }
        }
        int inx = binarySearch(keyArray, left, right, key);
        return inx >= 0;
    }

    private boolean contains(long[] keyArray, long key) {
        return contains(keyArray, key, null);
    }
    private boolean contains(long[] keyArray, long key, long[] searchHint) {
        int l = 0;
        int r = keyArray.length;
        if (searchHint != null && searchHint.length == 2 && searchHint[1] >= 0 && searchHint[1] <= keyArray.length) {
            if (searchHint[0] == key) {
                if (keyArray[(int) searchHint[1]] != key) {
                    throw new RuntimeException(String.format("Unexpected parameters: contains(keyArray[%d], key=%d, searchHint={%d,%d}) keyArray[%d]=%d",
                            keyArray.length, key, searchHint[0], searchHint[1], searchHint[1], keyArray[(int) searchHint[1]]));
                }
                return true;
            }
            if (searchHint[0] > key) {  // based on prev search value set a partial search
                l = (int)searchHint[1];
            } else {
                r = (int)searchHint[1];
            }
        }
        int inx = binarySearch(keyArray, 0, keyArray.length, key);
        searchHint[0] = key;
        searchHint[1] = (inx < 0) ? (-inx - 1) : inx;
        return inx >= 0;
    }

    /**
     *
     * @param arr   Array to search
     * @param l     array left bound index, inclusive
     * @param r     array right bound index, exclusive
     * @param x     value to be searched
     * @return The index of x within arr if found; A negative value of recent -(l+1) if not found. This allows
     *         an effective search of a set of sorted numbers where each search starts when the previous one ended.
     */
    private static int binarySearch(long[] arr, int l, int r, long x) {
        if (l < 0 || r > arr.length) {
            throw new RuntimeException(String.format("Wrong parameters binarySearch(arr[%d],%d,%d,%d)", arr.length, l, r, x));
        }
        while (r > l) {     // was >= when r was inclusive
            int mid = l + (r - l)/2;
            if (arr[mid] == x)
                return mid;
            if (arr[mid] > x) {
                r = mid;    // was mid-1 when r was inclusive
            } else {
                l = mid+1;
            }
        }
        return -(l+1);
    }

    public void setItemCounterLimit(final int itemCounterLimit) {
        throw new RuntimeException("Function not supported during analysis: setItemCounterLimit");
    }

    public void setEnforceDuringInsert(final boolean enforceDuringInsert) {
        throw new RuntimeException("Function not supported during analysis: setEnforceDuringInsert");
    }

    public Map<Long, Integer> getCollection() {
        throw new RuntimeException("Function not supported during analysis: getCollection");
    }

    public HistogramType getType() {
        return type;
    }

    public void setType(final HistogramType type) {
        throw new RuntimeException("Function not supported during analysis: setType");
    }

    public int getGlobalCount() {
        return globalCount;
    }

    public int getItemsCount() {
        return histogramArrKey.length;
    }

    public int getSkippedCount() {
        return skippedCount;
    }

    public int getTotalCount() {
        return globalCount + skippedCount;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + histogramArrKey.hashCode();
        result = prime * result + histogramArrValue.hashCode();
        result = prime * result + histogramOrigKeyIndex.hashCode();
        result = prime * result + globalCount;
        result = prime * result + skippedCount;
        result = prime * result + type.ordinal();
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        @SuppressWarnings("unchecked")
        final ClaLongHistogramCollectionArray other = (ClaLongHistogramCollectionArray) obj;
        if (globalCount != other.globalCount) {
            return false;
        }
        if (skippedCount != other.skippedCount) {
            return false;
        }
        //noinspection SimplifiableIfStatement
        return (histogramArrKey.equals(other.histogramArrKey) && histogramArrValue.equals(other.histogramArrValue));
    }

    public ClaHistogramCollection<Long> getClaHistogramCollection() {
        Map<Long, Integer> newHistogram = new LinkedHashMap<>();
        // Reconstruct sub-histogram based on the original list order
        for (int i=0; i<histogramOrigKeyIndex.length; ++i) {
            int inx = histogramOrigKeyIndex[i];
            newHistogram.put(histogramArrKey[inx], histogramArrValue[inx]);
        }
        return new ClaHistogramCollection<Long>(newHistogram, this.type);
    }

//    public float getSimilarityRatio(final ClaLongHistogramCollectionArray other) {
//        if (histogramArrKey != null) {
//            return getSimilarityRatioArray(other);
//        }
//        final int denom = globalCount + other.globalCount;
//        if (denom == 0 || histogram.size() == 0 || other.histogram.size() == 0)
//            return 0.0f;
//        int equalsCount = 0;
//        for (final Entry<Long, Integer> entry : histogram.entrySet()) {
//            final int countA = entry.getValue();
//            final int countB = other.getCount(entry.getKey());
//            equalsCount += Math.min(countA, countB);
//        }
//        return (2.0f * equalsCount) / (denom);
//    }
//
//    public float getNormalizedSimilarityRatio(final ClaLongHistogramCollectionArray other) {
//        if (globalCount == 0 || other.globalCount == 0)
//            return 0.0f;
//        if (histogram.size() > other.histogram.size())            // For better performance loop on the other histogram entries.
//            return other.getNormalizedSimilarityRatio(this);
//        float equalsCount = 0.0f;
//        for (final Entry<Long, Integer> entry : histogram.entrySet()) {
//            final float countA = ((float) entry.getValue()) / (float) globalCount;
//            final float countB = ((float) other.getCount(entry.getKey())) / (float) other.globalCount;
//            equalsCount += Float.min(countA, countB);
//        }
//        return equalsCount;
//    }

    // Name change to allow super to call this without confusion.
    public float getSimilarityRatio2(final ClaHistogramCollection<Long> other) {
        return getSimilarityRatio(other);
    }

    public float getSimilarityRatio(final ClaHistogramCollection<Long> other) {
        if (!(other instanceof ClaLongHistogramCollectionArray)) {
            return getSimilarityRatio(new ClaLongHistogramCollectionArray(other));
//            return getClaHistogramCollection().getSimilarityRatio(other);
        }

        ClaLongHistogramCollectionArray otherA = (ClaLongHistogramCollectionArray)other;
        if (histogramArrKey == null || otherA.histogramArrKey == null) {
            throw new RuntimeException("Both histogramArrKey must not be null");
        }
        final int denom = globalCount + otherA.globalCount;
        if (denom == 0 || histogramArrKey.length == 0 || otherA.histogramArrKey.length == 0)
            return 0.0f;
        int equalsCount = 0;
        int lastSearchIndex = 0;    // keys array is sorted so can limit search based on last found index
        for (int i=0; i<histogramArrKey.length; ++i) {
            final int countA = histogramArrValue[i];
            final int countBInx = binarySearch(otherA.histogramArrKey,
                    lastSearchIndex, otherA.histogramArrKey.length,
                    histogramArrKey[i]);
            final int countB = (countBInx < 0)? 0 : otherA.histogramArrValue[countBInx];
            lastSearchIndex = (countBInx < 0) ? (-countBInx - 1) : countBInx;   // use search hint from previous search
            equalsCount += Math.min(countA, countB);
        }
        return (2.0f * equalsCount) / (denom);
    }

    public float getNormalizedSimilarityRatio2(final ClaHistogramCollection<Long> other) {
        return getNormalizedSimilarityRatio(other);
    }
    public float getNormalizedSimilarityRatio(final ClaHistogramCollection<Long> other) {
        if (!(other instanceof ClaLongHistogramCollectionArray)) {
            return getNormalizedSimilarityRatio(new ClaLongHistogramCollectionArray(other));
//            return getClaHistogramCollection().getNormalizedSimilarityRatio(other);
        }

        ClaLongHistogramCollectionArray otherA = (ClaLongHistogramCollectionArray)other;
        if (globalCount == 0 || otherA.globalCount == 0)
            return 0.0f;
        if (histogramArrKey.length > otherA.histogramArrKey.length)            // For better performance loop on the other histogram entries.
            return other.getNormalizedSimilarityRatio(this);
        float equalsCount = 0.0f;
        int lastSearchIndex = 0;    // keys array is sorted so can limit search based on last found index
        for (int i=0; i<histogramArrKey.length; ++i) {
            final int countA = histogramArrValue[i];
            final int countBInx = binarySearch(otherA.histogramArrKey,
                    lastSearchIndex, otherA.histogramArrKey.length,
                    histogramArrKey[i]);
            final int countB = (countBInx < 0)? 0 : otherA.histogramArrValue[countBInx];
            lastSearchIndex = (countBInx < 0) ? (-countBInx - 1) : countBInx+1;   // use search hint from previous search. If found add 1 as keys has no repetitions

            final float countAf = ((float) countA) / (float) globalCount;
            final float countBf = ((float) countB) / (float) otherA.globalCount;
            equalsCount += Float.min(countAf, countBf);
        }
        return equalsCount;
    }

    /**
     * Compare collections taking into account items order
     * Assuming histogram preserve insertion order.
     *
     */
//    private Set<Long> getOrderedCommonKeys(final ClaLongHistogramCollectionArray other) {
//        if (globalCount == 0 || other.globalCount == 0)
//            return null;
//        final Set<Long> res = new LinkedHashSet<>();
//
//        Map<Long, Integer> histA = this.histogram;
//        Map<Long, Integer> histB = other.histogram;
//        if (histA.size() > histB.size()) {
//            // switch
//            histA = other.histogram;
//            histB = this.histogram;
//        }
//        final Set<Long> keySetA = histA.keySet();
//        final Set<Long> keySetB = histB.keySet();
//        final Iterator<Long> iterA = keySetA.iterator();
//        final Iterator<Long> iterB = keySetB.iterator();
//        while (iterA.hasNext() && iterB.hasNext()) {
//            Long keyA = iterA.next();
//            Long keyB = iterB.next();
//            // Skip items that are not present in the other collection, then compare items by order.
//            while ((!keySetB.contains(keyA)) && iterA.hasNext())
//                keyA = iterA.next();
//            while (((!keySetA.contains(keyB)) || (!keyA.equals(keyB))) && iterB.hasNext())
//                keyB = iterB.next();
//            if (keyA.equals(keyB))
//                res.add(keyA);
//        }    // while ()
//        return res;
//    }

    // Name change to allow super to call this without confusion.
    public Set<Long> getOrderedCommonKeys2(final ClaLongHistogramCollectionArray other) {
        return getOrderedCommonKeys(other);
    }

    /**
     * Compare collections taking into account items order
     * Assuming histogram preserve insertion order.
     *
     */
    public Set<Long> getOrderedCommonKeys(final ClaHistogramCollection<Long> other) {
        if (!(other instanceof ClaLongHistogramCollectionArray)) {
            return getOrderedCommonKeys(new ClaLongHistogramCollectionArray(other));
        }

        ClaLongHistogramCollectionArray otherArr = (ClaLongHistogramCollectionArray)other;
        if (globalCount == 0 || otherArr.globalCount == 0)
            return null;
        final Set<Long> res = new LinkedHashSet<>();

        long[] histKeyA = this.histogramArrKey;
        int[] histValA = this.histogramArrValue;
        int[] histInxA = this.histogramOrigKeyIndex;
        long[] histKeyB = otherArr.histogramArrKey;
        int[] histValB = otherArr.histogramArrValue;
        int[] histInxB = otherArr.histogramOrigKeyIndex;

        long[] pSubArrKeyA = this.perfSubArrKey;
        int[] pSubArrLeftA = this.perfSubArrLeft;
        int[] pSubArrRightA = this.perfSubArrRight;
        long[] pSubArrKeyB = otherArr.perfSubArrKey;
        int[] pSubArrLeftB = otherArr.perfSubArrLeft;
        int[] pSubArrRightB = otherArr.perfSubArrRight;

        if (histKeyA.length > histKeyB.length) {
            // switch
            histKeyA = otherArr.histogramArrKey;
            histValA = otherArr.histogramArrValue;
            histInxA = otherArr.histogramOrigKeyIndex;
            histKeyB = this.histogramArrKey;
            histValB = this.histogramArrValue;
            histInxB = this.histogramOrigKeyIndex;
            pSubArrKeyA = otherArr.perfSubArrKey;
            pSubArrLeftA = otherArr.perfSubArrLeft;
            pSubArrRightA = otherArr.perfSubArrRight;
            pSubArrKeyB = this.perfSubArrKey;
            pSubArrLeftB = this.perfSubArrLeft;
            pSubArrRightB = this.perfSubArrRight;
        }
        int iterA = 0;
        int iterB = 0;
        while (iterA < histKeyA.length && iterB < histKeyB.length) {
            long keyA = histKeyA[histInxA[iterA++]];
            long keyB = histKeyB[histInxB[iterB++]];
            // Skip items that are not present in the other collection, then compare items by order.
            while (iterA < histKeyA.length && !contains(histKeyB, keyA, pSubArrKeyB, pSubArrLeftB, pSubArrRightB)) {
                keyA = histKeyA[histInxA[iterA++]];
            }
            while (iterB < histKeyB.length && (keyA != keyB || !contains(histKeyA, keyB, pSubArrKeyA, pSubArrLeftA, pSubArrRightA))) {
                keyB = histKeyB[histInxB[iterB++]];
            }
            if (keyA == keyB) {
                res.add(keyA);
            }
        }    // while ()
        return res;
    }

    private Set<Long> getOrderedCommonKeys_Alternate(final ClaHistogramCollection<Long> other) {
        if (!(other instanceof ClaLongHistogramCollectionArray)) {
            return getOrderedCommonKeys_Alternate(new ClaLongHistogramCollectionArray(other));
        }

        ClaLongHistogramCollectionArray otherArr = (ClaLongHistogramCollectionArray)other;
        if (globalCount == 0 || otherArr.globalCount == 0)
            return null;
        final Set<Long> res = new LinkedHashSet<>();

        long[] histKeyA = this.histogramArrKey;
        int[] histValA = this.histogramArrValue;
        int[] histInxA = this.histogramOrigKeyIndex;
        long[] histKeyB = otherArr.histogramArrKey;
        int[] histValB = otherArr.histogramArrValue;
        int[] histInxB = otherArr.histogramOrigKeyIndex;
        if (histKeyA.length > histKeyB.length) {
            // switch
            histKeyA = otherArr.histogramArrKey;
            histValA = otherArr.histogramArrValue;
            histInxA = otherArr.histogramOrigKeyIndex;
            histKeyB = this.histogramArrKey;
            histValB = this.histogramArrValue;
            histInxB = this.histogramOrigKeyIndex;
        }
        // TODO: consider using binary search with some performance impact over the alternative of using Sets and its impact on GC
        int iterA = 0;
        int iterB = 0;
        long[] searchHintA = {0, -1};
        long[] searchHintB = {0, -1};
        while (iterA < histKeyA.length && iterB < histKeyB.length) {
            long keyA = histKeyA[histInxA[iterA++]];
            long keyB = histKeyB[histInxB[iterB++]];
            // Skip items that are not present in the other collection, then compare items by order.
            while (iterA < histKeyA.length && !contains(histKeyB, keyA, searchHintB)) {
                keyA = histKeyA[histInxA[iterA++]];
            }
            while (iterB < histKeyB.length && (keyA != keyB || !contains(histKeyA, keyB, searchHintA))) {
                keyB = histKeyB[histInxB[iterB++]];
            }
            if (keyA == keyB) {
                res.add(keyA);
            }
        }    // while ()
        return res;
    }

//    public int getOrderedSimilarityCount(final ClaLongHistogramCollectionArray other) {
//        final Set<Long> commonKeys = getOrderedCommonKeys(other);
//        if (commonKeys == null)
//            return 0;
//
//        return commonKeys.size();
//    }


    public int getOrderedSimilarityCount2(final ClaLongHistogramCollectionArray other) {
        return getOrderedSimilarityCount(other);
    }
    public int getOrderedSimilarityCount(final ClaLongHistogramCollectionArray other) {
        final Set<Long> commonKeys = getOrderedCommonKeys(other);
        if (commonKeys == null)
            return 0;

        return commonKeys.size();
    }

    public String getPropertiesString() {
        return "cnt=" + globalCount +
                " itms=" + histogramArrKey.length +
                " max=" + ((histogramArrKey.length == 0) ? 0 : histogramArrValue[histogramArrValue.length-1]);
    }

    @Override
    public String toString() {
        return this.toString(',');
    }

    public String toString(final char separator) {
        StringBuilder sb = new StringBuilder();
        for (int i=0; i<histogramArrKey.length; ++i) {
            if (i>0) sb.append(separator);
            int inx = histogramOrigKeyIndex[i];
            sb.append(histogramArrKey[inx]).append(":'").append(histogramArrValue[inx]);
        }
        return sb.toString();
    }

    public String toStringOrderedKeyAsc(final String separator) {
        StringBuilder sb = new StringBuilder();
        for (int i=0; i<histogramArrKey.length; ++i) {
            if (i>0) sb.append(separator);
            sb.append(histogramArrKey[i]).append(":'").append(histogramArrValue[i]);
        }
        return sb.toString();
    }

    public Long getOutlierByDiff(final int factorThreshold, final int minCount) {
        throw new RuntimeException("Function not supported during analysis: getOutlierByDiff");
    }

    public void removeItem(final Long outLier) {
        throw new RuntimeException("Function not supported during analysis: removeItem");
    }

    public String getJoinedKeys() {
        return getJoinedKeys("");
    }

    // Used to generate encoded doc PV field value string
    private String getJoinedKeys(final String prefix) {
        StringBuilder sb = new StringBuilder();
        sb.append(prefix);
        for (int i=0; i<histogramArrKey.length; ++i) {
            if (i>0) sb.append(" ");
            int inx = histogramOrigKeyIndex[i];
            sb.append(String.format("%X", histogramArrKey[inx]));
        }
        return sb.toString();
    }

    public String getJoinedKeysWithBoost(final String prefix) {
        return getJoinedKeysWithBoost(prefix, DEFAULT_LIMIT);
    }

    public String getJoinedKeysWithBoost(final String prefix, int limit) {
        StringBuilder sb = new StringBuilder();
        if (histogramArrKey.length < limit) {
            limit = histogramArrKey.length;
        }
        sb.append(prefix).append("(");
        for (int i=0; i<limit; ++i) {
            if (i>0) sb.append(" ");
            int inx = histogramOrigKeyIndex[i];
            long key = histogramArrKey[inx];
            int val = histogramArrValue[inx];
            sb.append((val == 1) ?
                    String.format("%X", key) :
                    String.format("%X^%.3f", key, (float) Math.sqrt(val)));
        }
        sb.append(")");
        return sb.toString();
    }

    public List<ClaHistogramCollection<Long>> split(final int listSize) {
        final List<ClaHistogramCollection<Long>> lists = new ArrayList<>();
        int count = histogramArrKey.length;
        int index = 0;
        do {
            int batchSize = Integer.min(listSize, count);
            Map<Long, Integer> newHistogram = new LinkedHashMap<>();
            // Reconstruct sub-histogram based on the original list order
            for (int i=0; i<batchSize; ++i) {
                int inx = histogramOrigKeyIndex[index+i];
                newHistogram.put(histogramArrKey[inx], histogramArrValue[inx]);
            }
            lists.add(new ClaLongHistogramCollectionArray(new ClaHistogramCollection<Long>(newHistogram, this.type)));
            count -= batchSize;
            index += batchSize;
        } while (count > 0);
        return lists;
    }

}
