package com.cla.common.domain.dto.file;

import com.cla.common.domain.pst.PstPath;
import com.cla.connector.domain.dto.acl.AclInheritanceType;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.media.MailPropertiesDto;

import java.nio.file.Path;
import java.util.Collection;
import java.util.Objects;

/**
 * Created By Itai Marko
 */
@SuppressWarnings("unused")
public class PstEntryPropertiesDto extends MailEntryPropertiesDto implements MailPropertiesDto {

    private ClaFilePropertiesDto pstFileProps;
    private long pstEntryId;
    private PstPath pstPath;
    private boolean file;
    private Integer numOfAttachments;
    private String emailPath;

    public ClaFilePropertiesDto getPstFileProps() {
        return pstFileProps;
    }

    public void setPstFileProps(ClaFilePropertiesDto pstFileProps) {
        Objects.requireNonNull(pstFileProps);
        this.pstFileProps = pstFileProps;
    }

    public long getPstEntryId() {
        return pstEntryId;
    }

    public void setPstEntryId(long pstEntryId) {
        this.pstEntryId = pstEntryId;
    }

    public PstPath getPstPath() {
        return pstPath;
    }

    public void setPstPath(PstPath pstPath) {
        this.pstPath = pstPath;
    }


    /**
     * Returns true iff this instance represents a PST entry that should be considered as a file.
     * <p>
     * Note that this implementation overrides the behaviour of {@link ClaFilePropertiesDto#isFile()} and
     * that for an instance of PstEntryPropertiesDto it is possible to be both a file and a folder (email for example).
     */
    @Override
    public boolean isFile() {
        return file;
    }

    /**
     * Set's whether this instance represents a PST entry that should be considered as a file.
     * <p>
     * Note that this implementation overrides the behaviour of {@link ClaFilePropertiesDto#setFile(boolean)} in
     * that it does not set the {@link #isFolder()} property to the opposite of {@link #isFile()}.
     * For an instance of PstEntryPropertiesDto it is possible to be both a file and a folder (email for example).
     */
    @Override
    public void setFile(boolean file) {
        this.file = file;
    }

    // <editor-fold desc="Getters delegated to the ClaFilePropertiesDto of the containing PST file">

    @Override
    public Path getFilePath() {
        return pstFileProps.getFilePath();
    }

    @Override
    public long getModTimeMilli() {
        return pstFileProps.getModTimeMilli();
    }

    @Override
    public long getAccessTimeMilli() {
        return pstFileProps.getAccessTimeMilli();
    }

    @Override
    public long getCreationTimeMilli() {
        return pstFileProps.getCreationTimeMilli();
    }

    @Override
    public Collection<String> getAclReadAllowed() {
        return pstFileProps.getAclReadAllowed();
    }

    @Override
    public Collection<String> getAclReadDenied() {
        return pstFileProps.getAclReadDenied();
    }

    @Override
    public Collection<String> getAclWriteAllowed() {
        return pstFileProps.getAclWriteAllowed();
    }

    @Override
    public Collection<String> getAclWriteDenied() {
        return pstFileProps.getAclWriteDenied();
    }

    @Override
    public String getOwnerName() {
        return pstFileProps.getOwnerName();
    }

    @Override
    public String getAclSignature() {
        return pstFileProps.getAclSignature();
    }

    @Override
    public AclInheritanceType getAclInheritanceType() {
        return pstFileProps.getAclInheritanceType();
    }

    @Override
    public String calculateAclSignature() {
        // Nothing needs to be recalculated as we're delegating the aclSignature to the containing PST file.
        // Just return the containing PST file's aclSignature
        return getAclSignature();
    }

    //</editor-fold>

    // <editor-fold desc="Setters with empty impl as to not change the state of the delegate">

    @Override
    public void setFilePath(Path filePath) {
        // Empty impl. There's no use case for changing the pst file's property via one of the entries.
    }

    @Override
    public ClaFilePropertiesDto setPath(Path path) {
        // Empty impl. There's no use case for changing the pst file's property via one of the entries.
        return pstFileProps;
    }

    @Override
    public void setModTimeMilli(long modTimeMilli) {
        // Empty impl. There's no use case for changing the pst file's property via one of the entries.
    }

    @Override
    public void setAccessTimeMilli(long accessTimeMilli) {
        // Empty impl. There's no use case for changing the pst file's property via one of the entries.
    }

    @Override
    public void setCreationTimeMilli(long creationTimeMilli) {
        // Empty impl. There's no use case for changing the pst file's property via one of the entries.
    }

    @Override
    public void addAllowedReadPrincipalName(String principalsName) {
        // Empty impl. There's no use case for changing the pst file's property via one of the entries.
    }

    @Override
    public void addDeniedReadPrincipalName(String principalsName) {
        // Empty impl. There's no use case for changing the pst file's property via one of the entries.
    }

    @Override
    public void addAllowedWritePrincipalName(String principalsName) {
        // Empty impl. There's no use case for changing the pst file's property via one of the entries.
    }

    @Override
    public void addDeniedWritePrincipalName(String principalsName) {
        // Empty impl. There's no use case for changing the pst file's property via one of the entries.
    }

    @Override
    public void setOwnerName(String ownerName) {
        // Empty impl. There's no use case for changing the pst file's property via one of the entries.
    }

    @Override
    public void setAclSignature(String aclSignature) {
        // Empty impl. There's no use case for changing the pst file's property via one of the entries.
    }

    @Override
    public void setAclInheritanceType(AclInheritanceType aclInheritanceType) {
        // Empty impl. There's no use case for changing the pst file's property via one of the entries.
    }

    public Integer getNumOfAttachments() {
        return numOfAttachments;
    }

    public void setNumOfAttachments(Integer numOfAttachments) {
        this.numOfAttachments = numOfAttachments;
    }

    public String getEmailPath() {
        return emailPath;
    }

    public void setEmailPath(String emailPath) {
        this.emailPath = emailPath;
    }

    // </editor-fold>

    @Override
    public String toString() {
        return "PstEntryPropertiesDto{" + "claFileId=" + getClaFileId() +
                ", pstEntryId=" + pstEntryId +
                ", pstPath=" + pstPath +
                ", file=" + file +
                ", folder=" + isFolder() +
                ", fileSize=" + getFileSize() +
                ", allowedReadPrincipalsNames=" + getAclReadAllowed() +
                ", deniedReadPrincipalsNames=" + getAclReadDenied() +
                ", allowedWritePrincipalsNames=" + getAclWriteAllowed() +
                ", deniedWritePrincipalsNames=" + getAclWriteDenied() +
                ", contentMetadataId=" + getContentMetadataId() +
                ", modTimeMilli=" + getModTimeMilli() +
                ", accessTimeMilli=" + getAccessTimeMilli() +
                ", creationTimeMilli=" + getCreationTimeMilli() +
                ", ownerName='" + getOwnerName() + '\'' +
                ", aclSignature='" + getAclSignature() + '\'' +
                ", mediaItemId='" + getMediaItemId() + '\'' +
                ", fileName='" + getFileName() + '\'' +
                ", aclInheritanceType=" + getAclInheritanceType() +
                ", scanErrorDtos=" + getErrors() +
                ", pstFileProps=" + pstFileProps.toString() +
                ", itemType=" + getItemType() +
                '}';
    }
}
