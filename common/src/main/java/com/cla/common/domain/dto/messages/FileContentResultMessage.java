package com.cla.common.domain.dto.messages;

import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;

public class  FileContentResultMessage extends ProcessingPhaseMessage<FileContentMessagePayload, IngestError>
        implements ResponseMessage{

    private Long claFileId;
    private boolean duplicateContentFound = false;
    private ClaFilePropertiesDto fileProperties;
    private Long contentMetadataId;
    private Long rootFolderId;


    private FileContentType requestType;



    // ---------------------- constructors -----------------------------------------------------------

    private FileContentResultMessage() {
        super();
    }

    public FileContentResultMessage(FileContentMessagePayload extractedContent) {
        super(extractedContent);
    }

    public static FileContentResultMessage create(){
        return new FileContentResultMessage();
    }

    public static FileContentResultMessage create(FileContentMessagePayload extractedContent){
        return new FileContentResultMessage(extractedContent);
    }

    // ---------------------- getters / setters -------------------------------------------------------

    public Long getClaFileId() {
        return claFileId;
    }

    public void setRequestType(FileContentType requestType) {
        this.requestType = requestType;
    }

    public FileContentType getRequestType() {
        return requestType;
    }

    public Long getRootFolderId() {
        return rootFolderId;
    }

    public void setRootFolderId(Long rootFolderId) {
        this.rootFolderId = rootFolderId;
    }

    public FileContentResultMessage setClaFileId(Long claFileId) {
        this.claFileId = claFileId;
        return this;
    }

    public boolean duplicateContentFound() {
        return duplicateContentFound;
    }

    public ClaFilePropertiesDto getFileProperties() {
        return fileProperties;
    }

    public FileContentResultMessage setFileProperties(ClaFilePropertiesDto fileProperties) {
        this.fileProperties = fileProperties;
        return this;
    }

    public Long getContentMetadataId() {
        return contentMetadataId;
    }

    public FileContentResultMessage setContentMetadataId(Long contentMetadataId) {
        if (contentMetadataId != null) {
            this.contentMetadataId = contentMetadataId;
            this.duplicateContentFound = true;
        }
        return this;
    }



    @Override
    public String toString() {
        return "FileContentResultMessage{" +
                "claFileId=" + claFileId +
                ", duplicateContentFound=" + duplicateContentFound +
                '}';
    }
}
