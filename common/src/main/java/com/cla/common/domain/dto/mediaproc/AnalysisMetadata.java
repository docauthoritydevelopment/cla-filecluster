package com.cla.common.domain.dto.mediaproc;

import com.cla.connector.domain.dto.file.FileType;

import java.io.Serializable;


public class AnalysisMetadata implements Serializable {

    // Added for backward competability with old serializer
    private static final long serialVersionUID =  -4694392389041098994L;

    private FileType type;

    public AnalysisMetadata() {
    }

    public AnalysisMetadata(final FileType type) {
            super();
        this.type = type;
    }

    public FileType getType() {
        return type;
    }

    public void setType(final FileType type) {
        this.type = type;
    }

}
