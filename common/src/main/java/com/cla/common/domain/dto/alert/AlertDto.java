package com.cla.common.domain.dto.alert;

import com.cla.common.domain.dto.event.EventType;
import com.cla.connector.domain.dto.JsonSerislizableVisible;

/**
 * Created by: yael
 * Created on: 1/15/2018
 */
public class AlertDto implements JsonSerislizableVisible {

    private Long id;

    private Long dateCreated;

    private String message;

    private String description;

    private AlertSeverity severity;

    private boolean acknowledged = false;

    private EventType eventType;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Long dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public AlertSeverity getSeverity() {
        return severity;
    }

    public void setSeverity(AlertSeverity severity) {
        this.severity = severity;
    }

    public boolean isAcknowledged() {
        return acknowledged;
    }

    public void setAcknowledged(boolean acknowledged) {
        this.acknowledged = acknowledged;
    }

    public EventType getEventType() {
        return eventType;
    }

    public void setEventType(EventType eventType) {
        this.eventType = eventType;
    }

    @Override
    public String toString() {
        return "AlertDto{" +
                "id=" + id +
                ", dateCreated=" + dateCreated +
                ", message='" + message + '\'' +
                ", description='" + description + '\'' +
                ", severity=" + severity +
                ", acknowledged=" + acknowledged +
                ", eventType=" + eventType +
                '}';
    }
}
