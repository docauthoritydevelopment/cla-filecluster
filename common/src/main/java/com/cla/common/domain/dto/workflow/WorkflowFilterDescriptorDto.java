package com.cla.common.domain.dto.workflow;

import com.cla.common.domain.dto.kendo.FilterDescriptor;

public class WorkflowFilterDescriptorDto {

    private Long id;

    private FilterDescriptor filter;

    private WorkflowFileData workflowFileData;

    private Long dateCreated;

    private Long dateModified;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public FilterDescriptor getFilter() {
        return filter;
    }

    public void setFilter(FilterDescriptor filter) {
        this.filter = filter;
    }

    public Long getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Long dateCreated) {
        this.dateCreated = dateCreated;
    }


    public Long getDateModified() {
        return dateModified;
    }

    public void setDateModified(Long dateModified) {
        this.dateModified = dateModified;
    }

    @Override
    public String toString() {
        return "WorkflowFilterDescriptorDto{" +
                "id=" + id +
                ", filter=" + filter +
                ", dateCreated=" + dateCreated +
                ", dateModified=" + dateModified +
                '}';
    }

    public WorkflowFileData getWorkflowFileData() {
        return workflowFileData;
    }

    public void setWorkflowFileData(WorkflowFileData workflowFileData) {
        this.workflowFileData = workflowFileData;
    }
}
