package com.cla.common.domain.dto.mcf.mcf_api.job;

import com.cla.common.domain.dto.mcf.mcf_api.McfNotificationStageObjectDto;

import java.util.List;
import java.util.Map;

/**
 * Created by uri on 07/07/2016.
 */
public class McfJobObjectDto {

    /**
     * "id"	The job's identifier, if present. If not present, ManifoldCF will create one (and will also create the job when saved).
     */
    String id;

    /**
     * "description"	Text describing the job
     */
    String description;

    /**
     * "repository_connection"	The name of the repository connection to use with the job
     */
    String repository_connection;

    /**
     * "document_specification"	The document specification object for the job, whose format is repository-connection specific
     */

    Map document_specification;
    /**
     * "start_mode"	The start mode for the job, which can be one of "schedule window start", "schedule window anytime", or "manual"
     */

    String start_mode;

    /**
     * "run_mode"	The run mode for the job, which can be either "continuous" or "scan once"
     */
    String run_mode;


    /**            "hopcount_mode"	The hopcount mode for the job, which can be either "accurate", "no delete", "never delete"*/
    String hopcount_mode;
    /**       "priority"	The job's priority, typically "5"*/
    Integer priority;

    /**            "recrawl_interval"	The default time between recrawl of documents (if the job is "continuous"), in milliseconds, or "infinite" for infinity*/
    String recrawl_interval;

    /**"expiration_interval"	The time until a document expires (if the job is "continuous"), in milliseconds, or "infinite" for infinity*/
    String expiration_interval;

    /**"reseed_interval"	The time between reseeding operations (if the job is "continuous"), in milliseconds, or "infinite" for infinity*/
    String reseed_interval;

    /**"hopcount"	An array of hopcount objects, describing the link types and associated maximum hops permitted for the job*/
    List<HopCountObjectDto> hopcount;

    /**"schedule"	An array of schedule objects, describing when the job should be started and run*/
    List<McfScheduleObjectDto> schedule;

    /**"pipelinestage"	An array of pipelinestage objects, describing what the transformation pipeline is*/
    List<McfPipelineStageObjectDto> pipelinestage;

    /**"notificationstage"	An array of notificationstage objects, describing what the notifications are*/
    List<McfNotificationStageObjectDto> notificationstage;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRepository_connection() {
        return repository_connection;
    }

    public void setRepository_connection(String repository_connection) {
        this.repository_connection = repository_connection;
    }

    public Map getDocument_specification() {
        return document_specification;
    }

    public void setDocument_specification(Map document_specification) {
        this.document_specification = document_specification;
    }

    public String getStart_mode() {
        return start_mode;
    }

    public void setStart_mode(String start_mode) {
        this.start_mode = start_mode;
    }

    public String getRun_mode() {
        return run_mode;
    }

    public void setRun_mode(String run_mode) {
        this.run_mode = run_mode;
    }

    public String getHopcount_mode() {
        return hopcount_mode;
    }

    public void setHopcount_mode(String hopcount_mode) {
        this.hopcount_mode = hopcount_mode;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public String getRecrawl_interval() {
        return recrawl_interval;
    }

    public void setRecrawl_interval(String recrawl_interval) {
        this.recrawl_interval = recrawl_interval;
    }

    public String getExpiration_interval() {
        return expiration_interval;
    }

    public void setExpiration_interval(String expiration_interval) {
        this.expiration_interval = expiration_interval;
    }

    public String getReseed_interval() {
        return reseed_interval;
    }

    public void setReseed_interval(String reseed_interval) {
        this.reseed_interval = reseed_interval;
    }

    public List<HopCountObjectDto> getHopcount() {
        return hopcount;
    }

    public void setHopcount(List<HopCountObjectDto> hopcount) {
        this.hopcount = hopcount;
    }

    public List<McfScheduleObjectDto> getSchedule() {
        return schedule;
    }

    public void setSchedule(List<McfScheduleObjectDto> schedule) {
        this.schedule = schedule;
    }

    public List<McfPipelineStageObjectDto> getPipelinestage() {
        return pipelinestage;
    }

    public void setPipelinestage(List<McfPipelineStageObjectDto> pipelinestage) {
        this.pipelinestage = pipelinestage;
    }

    public List<McfNotificationStageObjectDto> getNotificationstage() {
        return notificationstage;
    }

    public void setNotificationstage(List<McfNotificationStageObjectDto> notificationstage) {
        this.notificationstage = notificationstage;
    }

    @Override
    public String toString() {
        return "McfJobObjectDto{" +
                "id='" + id + '\'' +
                ", description='" + description + '\'' +
                ", repository_connection='" + repository_connection + '\'' +
                ", document_specification=" + document_specification +
                '}';
    }
}
