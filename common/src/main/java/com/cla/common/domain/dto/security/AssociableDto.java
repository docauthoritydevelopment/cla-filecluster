package com.cla.common.domain.dto.security;

import com.cla.common.domain.dto.doctype.DocTypeAssociationDto;
import com.cla.common.domain.dto.doctype.DocTypeDto;
import com.cla.common.domain.dto.filetag.FileTagAssociationDto;
import com.cla.common.domain.dto.filetag.FileTagDto;

import java.util.List;
import java.util.Set;

public interface AssociableDto {

    Set<FileTagDto> getFileTagDtos();

    List<FileTagAssociationDto> getFileTagAssociations();

    Set<DocTypeDto> getDocTypeDtos();

    List<DocTypeAssociationDto> getDocTypeAssociations();
}
