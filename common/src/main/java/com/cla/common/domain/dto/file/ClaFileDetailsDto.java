package com.cla.common.domain.dto.file;

import com.cla.common.domain.dto.filetree.RootFolderDto;
import com.cla.common.domain.dto.pv.FileProcessingErrorDto;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.util.List;

/**
 * Created by uri on 02/12/2015.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ClaFileDetailsDto implements Serializable {
    private ClaFileDto fileDto;

    private RootFolderDto fileRootFolder;
    private String fileOwner;
    private ClaFileState fileProcessingState;
    private FileProcessingErrorDto ingestionError;
    private List<String> proposedTitles;
    private String subject;
    private String title;
    private String author;
    private String template;

    private FileAclDataDto aclDataDto;

    public void setFileDto(ClaFileDto fileDto) {
        this.fileDto = fileDto;
    }

    public ClaFileDto getFileDto() {
        return fileDto;
    }

    public RootFolderDto getFileRootFolder() {
        return fileRootFolder;
    }

    public void setFileRootFolder(RootFolderDto fileRootFolder) {
        this.fileRootFolder = fileRootFolder;
    }

    public void setFileOwner(String fileOwner) {
        this.fileOwner = fileOwner;
    }

    public String getFileOwner() {
        return fileOwner;
    }

    public void setFileProcessingState(ClaFileState fileProcessingState) {
        this.fileProcessingState = fileProcessingState;
    }

    public ClaFileState getFileProcessingState() {
        return fileProcessingState;
    }

    public FileProcessingErrorDto getIngestionError() {
        return ingestionError;
    }

    public void setIngestionError(FileProcessingErrorDto ingestionError) {
        this.ingestionError = ingestionError;
    }

    public void setProposedTitles(List<String> proposedTitles) {
        this.proposedTitles = proposedTitles;
    }

    public List<String> getProposedTitles() {
        return proposedTitles;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getSubject() {
        return subject;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getAuthor() {
        return author;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public String getTemplate() {
        return template;
    }

    public FileAclDataDto getAclDataDto() {
        return aclDataDto;
    }

    public void setAclDataDto(FileAclDataDto aclDataDto) {
        this.aclDataDto = aclDataDto;
    }
}
