package com.cla.common.domain.dto.kendo;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by uri on 07/07/2015.
 */
public class FilterDescriptor implements Serializable {

    public static final String CONTENT_FILTER = "contentFilter";

    private String logic;

    private String field;

    private Object value;

    private String operator;

    private boolean ignoreCase = true;

    private String name;

    private List<FilterDescriptor> filters;

    public FilterDescriptor(String field, Object value, String operator) {
        this.field = field;
        this.value = value;
        this.operator = operator;
    }

    public FilterDescriptor() {
        filters = new ArrayList<>();
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    @JsonIgnore
    public Optional<String> getOptionalOperator() {
        return Optional.ofNullable(operator);
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getLogic() {
        return logic;
    }

    public void setLogic(String logic) {
        this.logic = logic;
    }

    public boolean isIgnoreCase() {
        return ignoreCase;
    }

    public void setIgnoreCase(boolean ignoreCase) {
        this.ignoreCase = ignoreCase;
    }

    public List<FilterDescriptor> getFilters() {
        return filters;
    }

    public void addFilter(FilterDescriptor filter) {
        filters.add(filter);
    }

	public void setFilters(List<FilterDescriptor> filters) {
		this.filters = filters;
	}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "FilterDescriptor{" +
                "name='" + name + '\'' +
                ", logic='" + logic + '\'' +
                ", filters=" + filters +
                ", field='" + field + '\'' +
                ", value=" + value +
                ", operator='" + operator + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FilterDescriptor that = (FilterDescriptor) o;

        if (ignoreCase != that.ignoreCase) return false;
        if (logic != null ? !logic.equals(that.logic) : that.logic != null) return false;
        if (field != null ? !field.equals(that.field) : that.field != null) return false;
        if (value != null ? !value.equals(that.value) : that.value != null) return false;
        if (operator != null ? !operator.equals(that.operator) : that.operator != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        return filters != null ? filters.equals(that.filters) : that.filters == null;
    }


    public boolean containsContentFilter() {
        if (CONTENT_FILTER.equalsIgnoreCase(this.getField())) {
            return true;
        }
        if (this.getFilters() != null) {
            for (FilterDescriptor innerFilter : this.getFilters()) {
                boolean innerAns = innerFilter.containsContentFilter();
                if (innerAns) {
                    return true;
                }
            }
        }
        return false;
    }

}
