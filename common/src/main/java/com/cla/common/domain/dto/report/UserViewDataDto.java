package com.cla.common.domain.dto.report;

import com.cla.common.domain.dto.filter.SavedFilterDto;
import com.cla.common.domain.dto.scope.Scope;
import com.cla.common.domain.dto.security.UserDto;

import java.io.Serializable;

/**
 * Created by: yael
 * Created on: 3/8/2018
 */
public class UserViewDataDto implements Serializable {

    private Long id;

    private String name;

    private String displayData;

    private SavedFilterDto filter;

    private DisplayType displayType;

    private UserDto owner;

    private Scope scope;

    private boolean globalFilter;

    private Long containerId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDisplayData() {
        return displayData;
    }

    public void setDisplayData(String displayData) {
        this.displayData = displayData;
    }

    public SavedFilterDto getFilter() {
        return filter;
    }

    public void setFilter(SavedFilterDto filter) {
        this.filter = filter;
    }

    public DisplayType getDisplayType() {
        return displayType;
    }

    public void setDisplayType(DisplayType displayType) {
        this.displayType = displayType;
    }

    public UserDto getOwner() {
        return owner;
    }

    public void setOwner(UserDto owner) {
        this.owner = owner;
    }

    public Scope getScope() {
        return scope;
    }

    public void setScope(Scope scope) {
        this.scope = scope;
    }

    public boolean isGlobalFilter() {
        return globalFilter;
    }

    public void setGlobalFilter(boolean globalFilter) {
        this.globalFilter = globalFilter;
    }

    public Long getContainerId() {
        return containerId;
    }

    public void setContainerId(Long containerId) {
        this.containerId = containerId;
    }
}
