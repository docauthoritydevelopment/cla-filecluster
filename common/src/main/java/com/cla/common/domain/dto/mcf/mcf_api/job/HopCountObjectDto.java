package com.cla.common.domain.dto.mcf.mcf_api.job;

import java.io.Serializable;

/**
 * Created by uri on 18/07/2016.
 */
public class HopCountObjectDto implements Serializable {

    String link_type;

    int count;

    public String getLink_type() {
        return link_type;
    }

    public void setLink_type(String link_type) {
        this.link_type = link_type;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
