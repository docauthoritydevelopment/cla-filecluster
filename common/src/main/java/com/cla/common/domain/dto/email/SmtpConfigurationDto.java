package com.cla.common.domain.dto.email;

import java.io.Serializable;

/**
 * Created by uri on 12-Apr-17.
 */
public class SmtpConfigurationDto implements Serializable {
    private String host;
    private Integer port;
    private String username;
    private String password;
    private boolean passwordEncrypted;
    private boolean smtpAuthentication;
    private boolean useTls;
    private boolean useSsl;

    public SmtpConfigurationDto() {
    }

    public SmtpConfigurationDto(SmtpConfigurationDto smtpConfigurationDto) {
        if (smtpConfigurationDto == null) {
            return;
        }
        this.host = smtpConfigurationDto.getHost();
        this.port = smtpConfigurationDto.getPort();
        this.username = smtpConfigurationDto.getUsername();
        this.password = smtpConfigurationDto.getPassword();
        this.passwordEncrypted = smtpConfigurationDto.isPasswordEncrypted();
        this.smtpAuthentication = smtpConfigurationDto.getSmtpAuthentication();
        this.useTls = smtpConfigurationDto.isUseTls();
        this.useSsl = smtpConfigurationDto.isUseSsl();
    }
    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean getSmtpAuthentication() {
        return smtpAuthentication;
    }

    public void setSmtpAuthentication(boolean smtpAuthentication) {
        this.smtpAuthentication = smtpAuthentication;
    }

    public boolean isUseTls() {
        return useTls;
    }

    public void setUseTls(boolean useTls) {
        this.useTls = useTls;
    }

    public boolean isUseSsl() {
        return useSsl;
    }

    public void setUseSsl(boolean useSsl) {
        this.useSsl = useSsl;
    }

    public boolean isPasswordEncrypted() {
        return passwordEncrypted;
    }

    public void setPasswordEncrypted(boolean passwordEncrypted) {
        this.passwordEncrypted = passwordEncrypted;
    }

    @Override
    public String toString() {
        return "SmtpConfigurationDto{" +
                "host='" + host + '\'' +
                ", port=" + port +
                ", username='" + username + '\'' +
                ", smtpAuthentication=" + smtpAuthentication +
                ", useTls=" + useTls +
                ", useSsl=" + useSsl +
                '}';
    }
}
