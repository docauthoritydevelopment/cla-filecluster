package com.cla.common.domain.dto.security;

import java.util.Set;

/**
 * Interface for DTOs that represent FunctionalItems.
 */
public interface FunctionalItemDto {

    /**
     * Return set of system roles, which are relevant for currently logged in user.
     * If user has business roles (global roles), the corresponding system roles will be added to this set.
     * If user has FunctionalRoles that intersect with functional roles of the functional item,
     * the corresponding system roles will be also added to this set.
     *
     * @return set of all permitted operations that the currently logged in user is allowed to apply to this item
     */
    Set<SystemRoleDto> getPermittedOperations();

    /**
     * Get functional role DTOs associated with this item
     * @return set of functional role DTOs
     */
    Set<FunctionalRoleDto> getAssociatedFunctionalRoles();
}
