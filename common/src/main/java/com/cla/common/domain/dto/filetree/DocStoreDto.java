package com.cla.common.domain.dto.filetree;

import java.io.Serializable;
import java.util.List;

/**
 * Created by uri on 12/08/2015.
 */
public class DocStoreDto implements Serializable {

    private Long id;

    private PathDescriptorType pathDescriptorType;

    private String description;

    private List<RootFolderDto> rootFolders;

    private List<RootFolderDto> ignoredRootFolders;

    public DocStoreDto() {
    }

    public DocStoreDto(Long id, String description, PathDescriptorType pathDescriptorType) {
        setId(id);
        setDescription(description);
        setPathDescriptorType(pathDescriptorType);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PathDescriptorType getPathDescriptorType() {
        return pathDescriptorType;
    }

    public void setPathDescriptorType(PathDescriptorType pathDescriptorType) {
        this.pathDescriptorType = pathDescriptorType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<RootFolderDto> getRootFolders() {
        return rootFolders;
    }

    public void setRootFolders(List<RootFolderDto> rootFolders) {
        this.rootFolders = rootFolders;
    }

    public List<RootFolderDto> getIgnoredRootFolders() {
        return ignoredRootFolders;
    }

    public void setIgnoredRootFolders(List<RootFolderDto> ignoredRootFolders) {
        this.ignoredRootFolders = ignoredRootFolders;
    }
}
