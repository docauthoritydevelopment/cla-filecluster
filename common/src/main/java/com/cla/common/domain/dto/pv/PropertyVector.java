package com.cla.common.domain.dto.pv;

import com.cla.common.domain.dto.histogram.ClaHistogramCollection;
import com.cla.common.domain.dto.histogram.ClaLongHistogramCollectionArray;
import org.apache.commons.lang3.SerializationUtils;

import java.nio.ByteBuffer;

public class PropertyVector {
    public static final String DEF_SYS_PART = "_";    // default value (for word, etc.)

    private PropertyVectorKey pk;

    //private String fileName;

    private java.nio.ByteBuffer data;

    private ClaHistogramCollection<Long> claHistogramCollection;

    public PropertyVector() {
    }

    public PropertyVector(final Long docId, final int histogramCode, final String fileName, final ClaHistogramCollection<Long> claHistogramCollection) {
        this.pk = new PropertyVectorKey(docId, histogramCode);
        //this.fileName = fileName;
        this.claHistogramCollection = claHistogramCollection;
        this.data = ByteBuffer.wrap(SerializationUtils.serialize(ClaLongHistogramCollectionArray.create(claHistogramCollection)));
    }

    public PropertyVector(final Long docId, final String part, final int histogramCode, final String fileName, final ClaHistogramCollection<Long> claHistogramCollection) {
        this.pk = new PropertyVectorKey(docId, part, histogramCode);
        //this.fileName = fileName;
        this.claHistogramCollection = claHistogramCollection;
        this.data = ByteBuffer.wrap(SerializationUtils.serialize(claHistogramCollection));
    }

    public PropertyVector(final PropertyVectorKey key, final String fileName, final ClaHistogramCollection<Long> claHistogramCollection) {
        this.pk = new PropertyVectorKey(key);
        //this.fileName = fileName;
        this.claHistogramCollection = claHistogramCollection;
        this.data = ByteBuffer.wrap(SerializationUtils.serialize(ClaLongHistogramCollectionArray.create(claHistogramCollection)));
    }

    public int getDataSize() {
        return (data == null) ? -1 : data.limit();
    }

    public PropertyVector(final Long docId, final String part, final int histogramCode, final String fileName, final ByteBuffer data) {
        this.pk = new PropertyVectorKey(docId, part, histogramCode);
        //this.fileName = fileName;
        this.data = data;
    }

    public java.nio.ByteBuffer getData() {
        return data;
    }

    @SuppressWarnings("unchecked")
    public ClaHistogramCollection<Long> getClaHistogramCollection() {
        if (claHistogramCollection == null) {
            final byte[] bytes = new byte[data.remaining()];
            data.get(bytes, 0, bytes.length);
            claHistogramCollection = (ClaHistogramCollection<Long>) SerializationUtils.deserialize(bytes);
        }
        return claHistogramCollection;
    }

    public void setData(final java.nio.ByteBuffer data) {
        this.data = data;
    }

    public PropertyVectorKey getPk() {
        return pk;
    }

    public void setPk(final PropertyVectorKey pk) {
        this.pk = pk;
    }

    /*public String getFileName() {
        return fileName;
    }

    public void setFileName(final String fileName) {
        this.fileName = fileName;
    }*/

}
