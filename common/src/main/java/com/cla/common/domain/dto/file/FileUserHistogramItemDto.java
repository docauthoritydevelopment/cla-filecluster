package com.cla.common.domain.dto.file;

import java.io.Serializable;

@Deprecated
@SuppressWarnings("serial")
public class FileUserHistogramItemDto implements Serializable {
	private final String user;
	private final int count;
	
	public FileUserHistogramItemDto(final String user,final int count) {
		super();
		this.user = user;
		this.count = count;
	}
	public String getUser() {
		return user;
	}
	public int getCount() {
		return count;
	}
	
	@Override
	public String toString() {
		return "{user=" + user + ", count=" + count + "}";
	}

}
