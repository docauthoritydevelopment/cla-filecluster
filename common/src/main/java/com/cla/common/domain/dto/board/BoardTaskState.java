package com.cla.common.domain.dto.board;

public enum BoardTaskState {
    NEW,
    ASSIGNED,
    IN_PROGRESS,
    DONE
    ;
}
