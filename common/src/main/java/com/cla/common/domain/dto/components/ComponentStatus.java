package com.cla.common.domain.dto.components;

/**
 * Created by: yael
 * Created on: 2/21/2018
 */
public class ComponentStatus {

    private double processCpuLoad;
    private double systemCpuLoad;
    private long totalPhysicalMemorySize;
    private long freePhysicalMemorySize;

    public double getProcessCpuLoad() {
        return processCpuLoad;
    }

    public void setProcessCpuLoad(double processCpuLoad) {
        this.processCpuLoad = processCpuLoad;
    }

    public double getSystemCpuLoad() {
        return systemCpuLoad;
    }

    public void setSystemCpuLoad(double systemCpuLoad) {
        this.systemCpuLoad = systemCpuLoad;
    }

    public long getTotalPhysicalMemorySize() {
        return totalPhysicalMemorySize;
    }

    public void setTotalPhysicalMemorySize(long totalPhysicalMemorySize) {
        this.totalPhysicalMemorySize = totalPhysicalMemorySize;
    }

    public long getFreePhysicalMemorySize() {
        return freePhysicalMemorySize;
    }

    public void setFreePhysicalMemorySize(long freePhysicalMemorySize) {
        this.freePhysicalMemorySize = freePhysicalMemorySize;
    }

    @Override
    public String toString() {
        return "ComponentStatus{" +
                "processCpuLoad=" + processCpuLoad +
                ", systemCpuLoad=" + systemCpuLoad +
                ", totalPhysicalMemorySize=" + totalPhysicalMemorySize +
                ", freePhysicalMemorySize=" + freePhysicalMemorySize +
                '}';
    }
}
