package com.cla.common.domain.dto.crawler;

import com.cla.common.domain.dto.file.ClaFileState;
import com.cla.connector.domain.dto.file.FileType;

import java.io.Serializable;

/**
 *
 * Created by uri on 18/11/2015.
 */
public class FileStateDto implements Serializable {

    private ClaFileState fileState;
    private FileType fileType;

    public void setFileState(ClaFileState fileState) {
        this.fileState = fileState;
    }

    public ClaFileState getFileState() {
        return fileState;
    }

    public void setFileType(FileType fileType) {
        this.fileType = fileType;
    }

    public FileType getFileType() {
        return fileType;
    }

    @Override
    public String toString() {
        return "state=" + fileState +", type=" + fileType;
    }
}
