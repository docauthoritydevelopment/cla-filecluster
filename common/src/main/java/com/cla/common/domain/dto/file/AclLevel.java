package com.cla.common.domain.dto.file;

/**
 * Created by uri on 25/08/2016.
 */
public enum AclLevel {
    share, parent, document
}
