package com.cla.common.domain.dto.messages;

import com.cla.connector.domain.dto.file.FileType;
import com.cla.connector.domain.dto.file.label.LabelingVendor;
import com.cla.connector.domain.dto.media.MediaConnectionDetailsDto;

/**
 * Created by: yael
 * Created on: 7/4/2019
 */
public class LabelTaskParameters extends TaskParameters implements MediaConnectionDetailsProvider {

    private Long fileId;
    private Long rootFolderId;
    private String filePath;
    private FileType fileType;
    private String mediaEntityId;
    private String username;
    private String password;
    private MediaConnectionDetailsDto mediaConnectionDetailsDto;
    private LabelingVendor vendor;
    private String nativeId;
    private MediaConnectionDetailsDto labelingConnection;
    private LabelRequestType requestType;
    private long labelId;

    public static LabelTaskParameters create(){
        return new LabelTaskParameters();
    }

    public Long getFileId() {
        return fileId;
    }

    public LabelTaskParameters setFileId(Long fileId) {
        this.fileId = fileId;
        return this;
    }

    public Long getRootFolderId() {
        return rootFolderId;
    }

    public LabelTaskParameters setRootFolderId(Long rootFolderId) {
        this.rootFolderId = rootFolderId;
        return this;
    }

    public String getFilePath() {
        return filePath;
    }

    public LabelTaskParameters setFilePath(String filePath) {
        this.filePath = filePath;
        return this;
    }

    public FileType getFileType() {
        return fileType;
    }

    public LabelTaskParameters setFileType(FileType fileType) {
        this.fileType = fileType;
        return this;
    }

    public String getMediaEntityId() {
        return mediaEntityId;
    }

    public LabelTaskParameters setMediaEntityId(String mediaEntityId) {
        this.mediaEntityId = mediaEntityId;
        return this;
    }

    public String getUsername() {
        return username;
    }

    public LabelTaskParameters setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public LabelTaskParameters setPassword(String password) {
        this.password = password;
        return this;
    }

    public MediaConnectionDetailsDto getMediaConnectionDetailsDto() {
        return mediaConnectionDetailsDto;
    }

    public LabelTaskParameters setMediaConnectionDetailsDto(MediaConnectionDetailsDto mediaConnectionDetailsDto) {
        this.mediaConnectionDetailsDto = mediaConnectionDetailsDto;
        return this;
    }

    public LabelingVendor getLabelingVendor() {
        return vendor;
    }

    public LabelTaskParameters setLabelingVendor(LabelingVendor vendor) {
        this.vendor = vendor;
        return this;
    }

    public String getNativeId() {
        return nativeId;
    }

    public LabelTaskParameters setNativeId(String nativeId) {
        this.nativeId = nativeId;
        return this;
    }

    public MediaConnectionDetailsDto getLabelingConnection() {
        return labelingConnection;
    }

    public LabelTaskParameters setLabelingConnection(MediaConnectionDetailsDto labelingConnection) {
        this.labelingConnection = labelingConnection;
        return this;
    }

    public LabelRequestType getRequestType() {
        return requestType;
    }

    public LabelTaskParameters setRequestType(LabelRequestType requestType) {
        this.requestType = requestType;
        return this;
    }

    public long getLabelId() {
        return labelId;
    }

    public LabelTaskParameters setLabelId(long labelId) {
        this.labelId = labelId;
        return this;
    }

    @Override
    public String toString() {
        return "LabelTaskParameters{" +
                "fileId=" + fileId +
                ", rootFolderId=" + rootFolderId +
                ", filePath='" + filePath + '\'' +
                ", fileType=" + fileType +
                ", mediaEntityId='" + mediaEntityId + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", mediaConnectionDetailsDto=" + mediaConnectionDetailsDto +
                ", vendor=" + vendor +
                ", nativeId='" + nativeId + '\'' +
                ", labelingConnection=" + labelingConnection +
                ", requestType=" + requestType +
                ", labelId=" + labelId +
                "} " + super.toString();
    }
}
