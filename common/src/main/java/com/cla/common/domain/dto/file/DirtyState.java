package com.cla.common.domain.dto.file;

/**
 * Created by uri on 23/12/2015.
 * DirtyState is cumulative. For example, If ACL_DIRTY then assumed GROUP and TAG are also dirty
 */
@Deprecated
public enum DirtyState {
    OK,DIRTY,ACL_DIRTY, GROUP_DIRTY, TAG_DIRTY, EXTRACTION_DIRTY
}
