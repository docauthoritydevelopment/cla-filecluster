package com.cla.common.domain.dto.histogram;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

public class ClaStyle implements Serializable {

	private static final long serialVersionUID = 1L;
	public enum Justification {LEFT_JUST,CENTER_JUST,RIGHT_JUST,JUSTIFIED_JUST}

	private String styleName;
	private float fontSize;
	private String fontName;
	private boolean isBold;
	private boolean isUnderlined;
	private int headingLevel;	// 0 = not heading
	private boolean isListItem;	// Bullet or numbered
	private int tableLevel;		// 0 = not in table	
	private int color;	// 0 = auto-color
	private int border;	// 0 = no border
	private short firstLineIndent;
	private float indentFromLeft;
	private float indentFromRight;
	private Justification justification;
	private short spacingBefore;
	private short spacingAfter;
	private String text;

	private int selectedRunLength;

	private static short[] colorLookup = null;
	private static Map<Short,Short> fontSizeLookup = null;

	public static void setFontSizeLookup(Map<Short,Short> fontSizeLookup) {
		ClaStyle.fontSizeLookup = fontSizeLookup;
	}

	private final static Pattern commaSeparatorPattern = Pattern.compile(",");
	private final static Pattern colonSeparatorPattern = Pattern.compile(":");

	public static void setFontSizeLookup(String fontSizeLookupStr) throws RuntimeException {
		if (fontSizeLookupStr==null || fontSizeLookupStr.trim().isEmpty()) {
			return;
		}
		try {
			Map<Short,Short> map = new HashMap<>();
			// s1:t1,s2:t2 . For example: 10:12,13:12
			String[] sp = commaSeparatorPattern.split(fontSizeLookupStr);
			for (int i=0; i<sp.length; ++i) {
				String[] innerSp = colonSeparatorPattern.split(sp[i]);
				if (innerSp.length!=2) {
					throw new RuntimeException("Bad fontSizeLookupString " + fontSizeLookupStr);
				}
				Short fromVal = Short.valueOf(innerSp[0]);
				Short toVal = Short.valueOf(innerSp[1]);
				map.put(fromVal, toVal);
			}
			fontSizeLookup = map;
		} catch (NumberFormatException e) {
			throw new RuntimeException("Could not parse fontSizeLookup string", e);
		}
	}

	private static short[] calcColorLookup(float gamma, int bits) {
		if (gamma==1f || gamma<=0f || bits>=8 || bits<1) {
			return null;
		}
		short[] lookup = new short[256];
		for (short i=0;i<lookup.length;++i) {
			lookup[i] = convertColorComponent(i, gamma, bits);
		}
		return lookup;
	}

	public static short convertColorComponent(short c, float gamma, int bits) {
		if (gamma==1f || gamma<=0f || bits>=8 || bits<1) {
			return c;
		}
		bits = 8 - bits;
		short v = (short) Integer.min((short) (256 * (Math.pow((double) c / (double) 256, 1f/gamma))),255);
		return (short) ((v >> bits) << bits);
	}

	public static void setColorLookupParams(float gamma, int bits) {
		colorLookup = calcColorLookup(gamma, bits);
	}

	public static int translateColor(int color, float gamma, int bits) {
		if (gamma==1f || gamma<=0f || bits>=8 || bits<1 || color==0) {
			return color;
		}
		return convertColorComponent((short) (color&0xff),gamma,bits) |
				(convertColorComponent((short) ((color>>8)&0xff),gamma,bits)<<8) |
				(convertColorComponent((short) ((color>>16)&0xff),gamma,bits)<<16);
	}

	public static int translateColor(int color) {
		if (colorLookup==null) {
			return color;
		}
		return colorLookup[color&0xff] | ((colorLookup[(color>>8)&0xff])<<8) | ((colorLookup[(color>>16)&0xff])<<16);
	}

	private static int translateFontSize(float fontSize) {
		// Old (good) transformation
		int res = ((int) ((1f + fontSize) / 2f)) * 2;
		if (fontSizeLookup == null) {
			return res;
		}
		Short newVal = fontSizeLookup.get((short) res);
		if (newVal!=null) {
			res = newVal;
		}
		return res;
	}

	/**
	 * Prepare for custom serialization ...
	 *
	 * This method is private but it is called using reflection by java
	 * serialization mechanism. It overwrites the default object serialization.
	 * The access modifier for this method MUST be set to <b>private</b> otherwise {@link java.io.StreamCorruptedException}
	 * will be thrown.
	 */
	@SuppressWarnings("unused")
	private void MyWriteObject(final ObjectOutputStream oos) throws IOException {
		//System.out.println(ExcelSheet.class.getName() + ".writeObject");

		//oos.defaultWriteObject();

		// scalars
		oos.writeObject(styleName);
		oos.writeFloat(fontSize);
		oos.writeObject(fontName);
		oos.writeInt(headingLevel);
		oos.writeInt(color);
		oos.writeInt(border);
		final byte mask = (byte) ((isBold?1:0) | (isUnderlined?2:0) | (isListItem?4:0));
		oos.writeByte(mask);
		oos.writeShort(firstLineIndent);
		oos.writeFloat(indentFromLeft);
		oos.writeFloat(indentFromRight);
		oos.writeByte(justification.ordinal());
		oos.writeShort(spacingBefore);
		oos.writeShort(spacingAfter);
		oos.writeInt(selectedRunLength);
		oos.writeObject(text);
	}

	/**
	 * This method is private but it is called using reflection by java
	 * serialization mechanism. It overwrites the default object serialization.
	 * The access modifier for this method MUST be set to <b>private</b> otherwise {@link java.io.StreamCorruptedException}
	 * will be thrown.
	 */
	@SuppressWarnings("unused")
	private void MyReadObject(final ObjectInputStream ois) throws IOException, ClassNotFoundException {
		//System.out.println(ExcelSheet.class.getName() + ".readObject");

		//ois.defaultReadObject();
		styleName = (String) ois.readObject();
		fontSize = ois.readFloat();
		fontName = (String) ois.readObject();
		headingLevel = ois.readInt();
		color = ois.readInt();
		border = ois.readInt();
		final byte mask = ois.readByte();
		isBold = (mask & 1) != 0;
		isUnderlined = (mask & 2) != 0;
		isListItem = (mask & 4) != 0;
		firstLineIndent = ois.readShort();
		indentFromLeft = ois.readFloat();
		indentFromRight = ois.readFloat();
		justification = Justification.values()[ois.readByte()];
		spacingBefore = ois.readShort();
		spacingAfter = ois.readShort();
		selectedRunLength = ois.readInt();
		text = (String) ois.readObject();
	}

	public ClaStyle() {
		super();
	}

	public ClaStyle(final String styleName, final String fontName, final float fontSize, final int color,
					final boolean isBold, final boolean isUnderlined, final int selectedRunLength) {
		super();
		this.styleName = styleName;
		this.fontSize = fontSize;
		this.fontName = fontName;
		this.isBold = isBold;
		this.isUnderlined = isUnderlined;
		this.color = color & 0x00ffffff;	// drop alpha value
		this.selectedRunLength = selectedRunLength;
	}

	public static ClaStyle getAbsoluteStyle(final ClaStyle baseStyle, final float leftMargine, final float rightMargin) {
		final ClaStyle res = new ClaStyle();

		res.border = 0;
		res.color = baseStyle.color;
		res.firstLineIndent = baseStyle.firstLineIndent;
		res.fontName = "";
		res.fontSize = baseStyle.fontSize;
		res.headingLevel = baseStyle.headingLevel;
		res.indentFromLeft = baseStyle.indentFromLeft;
		res.indentFromRight = baseStyle.indentFromRight;
		res.isBold = baseStyle.isBold;
		res.isListItem = baseStyle.isListItem;
		res.isUnderlined = baseStyle.isUnderlined;
		res.justification = baseStyle.justification;
		res.selectedRunLength = baseStyle.selectedRunLength;
		res.spacingAfter = 0;
		res.spacingBefore = 0;
		res.styleName = "";
		res.tableLevel = (baseStyle.border==0)?0:baseStyle.tableLevel;
		if (baseStyle.tableLevel>0) {
			res.justification = Justification.LEFT_JUST;
			res.indentFromLeft = 0;
			res.indentFromRight = 0;
			res.firstLineIndent = 0;
		}

		res.indentFromLeft += leftMargine;
		res.indentFromRight += rightMargin;
		if (res.justification==Justification.LEFT_JUST) {
			res.indentFromRight = 0;
		} else if (res.justification==Justification.RIGHT_JUST) {
			res.indentFromLeft = 0;
			res.firstLineIndent = 0;
		} else if (res.justification==Justification.CENTER_JUST) {
			res.indentFromLeft = 0;
			res.indentFromRight = 0;
			res.firstLineIndent = 0;
		}

		return res;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + border;
		result = prime * result + color;
		result = prime * result + firstLineIndent;
		result = prime * result + ((fontName == null) ? 0 : fontName.hashCode());
		result = prime * result + Float.floatToIntBits(fontSize);
		result = prime * result + headingLevel;
		result = prime * result + Float.floatToIntBits(indentFromLeft);
		result = prime * result + Float.floatToIntBits(indentFromRight);
		result = prime * result + (isBold ? 1231 : 1237);
		result = prime * result + (isListItem ? 1231 : 1237);
		result = prime * result + (isUnderlined ? 1231 : 1237);
		result = prime * result + ((justification == null) ? 0 : justification.hashCode());
		result = prime * result + selectedRunLength;
		result = prime * result + spacingAfter;
		result = prime * result + spacingBefore;
		result = prime * result + ((styleName == null) ? 0 : styleName.hashCode());
		result = prime * result + tableLevel;
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final ClaStyle other = (ClaStyle) obj;
		if (border != other.border) {
			return false;
		}
		if (color != other.color) {
			return false;
		}
		if (firstLineIndent != other.firstLineIndent) {
			return false;
		}
		if (fontName == null) {
			if (other.fontName != null) {
				return false;
			}
		} else if (!fontName.equals(other.fontName)) {
			return false;
		}
		if (fontSize != other.fontSize) {
			return false;
		}
		if (headingLevel != other.headingLevel) {
			return false;
		}
		if (indentFromLeft != other.indentFromLeft) {
			return false;
		}
		if (indentFromRight != other.indentFromRight) {
			return false;
		}
		if (isBold != other.isBold) {
			return false;
		}
		if (isListItem != other.isListItem) {
			return false;
		}
		if (isUnderlined != other.isUnderlined) {
			return false;
		}
		if (justification != other.justification) {
			return false;
		}
		if (selectedRunLength != other.selectedRunLength) {
			return false;
		}
		if (spacingAfter != other.spacingAfter) {
			return false;
		}
		if (spacingBefore != other.spacingBefore) {
			return false;
		}
		if (styleName == null) {
			if (other.styleName != null) {
				return false;
			}
		} else if (!styleName.equals(other.styleName)) {
			return false;
		}
		if (tableLevel != other.tableLevel) {
			return false;
		}
		return true;
	}

	public String getStyleName() {
		return styleName;
	}

	public void setStyleName(final String styleName) {
		this.styleName = styleName;
	}

	public float getFontSize() {
		return fontSize;
	}

	public void setFontSize(final float fontSize) {
		this.fontSize = fontSize;
	}

	public String getFontName() {
		return fontName;
	}

	public void setFontName(final String fontName) {
		this.fontName = fontName;
	}

	public boolean isBold() {
		return isBold;
	}

	public void setBold(final boolean isBold) {
		this.isBold = isBold;
	}

	public boolean isUnderlined() {
		return isUnderlined;
	}

	public void setUnderlined(final boolean isUnderlined) {
		this.isUnderlined = isUnderlined;
	}

	public int getHeadingLevel() {
		return headingLevel;
	}

	public void setHeadingLevel(final int headingLevel) {
		this.headingLevel = headingLevel;
	}

	public boolean isListItem() {
		return isListItem;
	}

	public void setListItem(final boolean isListItem) {
		this.isListItem = isListItem;
	}

	public boolean isInTable() {
		return tableLevel > 0;
	}

	public int getTableLevel() {
		return tableLevel;
	}

	public void setTableLevel(final int tableLevel) {
		this.tableLevel = tableLevel;
	}

	public int getColor() {
		return color;
	}

	public void setColor(final int color) {
		this.color = color;
	}

	public int getBorder() {
		return border;
	}

	public void setBorder(final int borderTop, final int borderRight, final int borderBottom, final int borderLeft) {
		if (borderTop == 0 &&
				borderRight == 0 &&
				borderBottom == 0 &&
				borderLeft == 0) {
			this.border = 0;
		}
		else {
			final int prime = 31;
			int res = borderTop;
			res = res * prime + borderRight;
			res = res * prime + borderBottom;
			res = res * prime + borderLeft;
			this.border = res;
		}
	}

	public short getFirstLineIndent() {
		return firstLineIndent;
	}

	public void setFirstLineIndent(final short firstLineIndent) {
		this.firstLineIndent = firstLineIndent;
	}

	public float getIndentFromLeft() {
		return indentFromLeft;
	}

	public void setIndentFromLeft(final float indentFromLeft) {
		this.indentFromLeft = indentFromLeft;
	}

	public float getIndentFromRight() {
		return indentFromRight;
	}

	public void setIndentFromRight(final float indentFromRight) {
		this.indentFromRight = indentFromRight;
	}

	public Justification getJustification() {
		return justification;
	}

	public void setJustification(final Justification justification) {
		this.justification = justification;
	}

	public short getSpacingBefore() {
		return spacingBefore;
	}

	public void setSpacingBefore(final short spacingBefore) {
		this.spacingBefore = spacingBefore;
	}

	public short getSpacingAfter() {
		return spacingAfter;
	}

	public void setSpacingAfter(final short spacingAfter) {
		this.spacingAfter = spacingAfter;
	}

	public String getText() {
		return text;
	}

	public void setText(final String text) {
		this.text = text;
	}

	public boolean isHeading() {
		if ((isBold || isUnderlined) && fontSize >= 8f && getSelectedRunDominance()>0.8) {
			return true;
		}
		if ((isBold || isUnderlined) && fontSize >= 12f) {
			return true;
		}
		if ((isBold || isUnderlined) && justification==Justification.CENTER_JUST && fontSize >= 8) {
			return true;
		}
		if (justification==Justification.CENTER_JUST && fontSize >= 10f) {
			return true;
		}
		if (fontSize >= 14f) {
			return true;
		}

		return false;
	}

	public String getSimpleSignature()
	{
		// TODO: Justification DocX-Val = DocVal + 1; Color: DocVal should be matched to DocX (RGB).
		return /*styleName+*/fontName+"S"+ translateFontSize(fontSize)
				+"C"+((color==0)?"0":Integer.toHexString(translateColor(color)))
				+(isListItem?"LI":("FI"+firstLineIndent))
				+"R"+((int)indentFromRight)
				+"L"+((int)indentFromLeft)
				+"J"+justification.ordinal()
				+"pB"+spacingBefore
				+"pA"+spacingAfter
				+(isBold?"B":"")+(isUnderlined?"U":"")
				+(isInTable()?"T":"")
				;
	}

	public String getAbsoluteSignatureNoFont(final float leftMargine, final float rightMargin) {
		return ClaStyle.getAbsoluteStyle(this, leftMargine, rightMargin).getSimpleSignature();
	}

	public String getStyleDescriptionToken()
	{
		return String.format("%02d", getStyleHeadingPotentialRate())+"F"+fontSize
				+(isBold?"B+":"B-")+(isUnderlined?"U+":"U-")+(isInTable()?"T+":"T-")
				+(headingLevel>0?("H"+headingLevel):"H-")+"J"+justification.ordinal()
				+"R"+indentFromRight+"L"+indentFromLeft+(isListItem?"I+":"I-")
				+"pB"+spacingBefore+"pA"+spacingAfter;
	}

	public int getStyleHeadingPotentialRate()
	{
		int rate = 0;
		rate += Math.max(8, fontSize) - 8;
		if (isBold) {
			rate += 2;
		}
		if (isUnderlined) {
			rate += 2;
		}

		if (justification == Justification.CENTER_JUST) {
			rate += 2;
		}
		if (justification == Justification.RIGHT_JUST && !isInTable()) {
			rate -= 4;
		}
		if (isListItem) {
			rate -= 4;
		}
//		return 10-Math.max(0, rate);	// low number -> higher rate
		return rate;	// higher number -> higher rate
	}

	public float getSelectedRunDominance() {
		return
				(text==null || text.isEmpty() || selectedRunLength == 0)?1f:
						((float)selectedRunLength)/text.length();
	}

	@Override
	public String toString() {
		final StringBuffer sb = new StringBuffer("ClaStyle{");
		sb.append("styleName='").append(styleName).append('\'');
		sb.append(", fontSize=").append(fontSize).append('/').append(translateFontSize(fontSize));
		sb.append(", fontName='").append(fontName).append('\'');
		sb.append(", isBold=").append(isBold);
		sb.append(", isUnderlined=").append(isUnderlined);
		sb.append(", headingLevel=").append(headingLevel);
		sb.append(", isListItem=").append(isListItem);
		sb.append(", tableLevel=").append(tableLevel);
		sb.append(", color=").append(color).append('/').append(translateColor(color));
		sb.append(", border=").append(border);
		sb.append(", firstLineIndent=").append(firstLineIndent);
		sb.append(", indentFromLeft=").append(indentFromLeft);
		sb.append(", indentFromRight=").append(indentFromRight);
		sb.append(", justification=").append(justification);
		sb.append(", spacingBefore=").append(spacingBefore);
		sb.append(", spacingAfter=").append(spacingAfter);
		sb.append(", text='").append(text).append('\'');
		sb.append(", selectedRunLength=").append(selectedRunLength);
		sb.append('}');
		return sb.toString();
	}
}
