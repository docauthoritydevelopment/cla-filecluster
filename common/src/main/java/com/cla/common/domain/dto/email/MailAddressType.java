package com.cla.common.domain.dto.email;

/**
 * Created by: yael
 * Created on: 2/3/2019
 */
public enum MailAddressType {
    SENDER,
    RECIPIENT
    ;
}
