package com.cla.common.domain.dto.filetree;

import com.cla.common.domain.dto.dataCenter.CustomerDataCenterDto;
import com.cla.common.domain.dto.file.DirectoryExistStatus;
import com.cla.common.utils.CollectionsUtils;
import com.cla.connector.domain.dto.file.FileType;
import com.cla.connector.domain.dto.JsonSerislizableVisible;
import com.cla.connector.domain.dto.media.MediaType;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.common.base.Objects;
import com.google.common.base.Strings;

import java.util.Arrays;
import java.util.List;

/**
 * NOTE - this DTO is serialized to the database (run history CrawlRun) as json.
 * This DTO should be backward compatible. Only add fields, and rename only if really really necessary.
 *
 * Created by uri on 12/08/2015.
 */
@SuppressWarnings("serial")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RootFolderDto implements JsonSerislizableVisible {

    private Long id;

    private String path;

    private String realPath;

    private String nickName;

    private String description;

    private Long docStoreId;

    private Integer numberOfFoldersFound;

    private Long lastRunId;

    private StoreLocation storeLocation;

    private StorePurpose storePurpose;

    private StoreSecurity storeSecurity;

    private StoreRescanMethod storeRescanMethod;

    private boolean rescanActive;

    private FileType[] fileTypes;

    private FileType[] ingestFileTypes;

    private int folderExcludeRulesCount;

    private MediaType mediaType;

    private CrawlerType crawlerType;

    private Integer scannedFilesCountCap;
    private Integer scannedMeaningfulFilesCountCap;
    private Long mediaConnectionDetailsId;

    private String mediaConnectionName;

    private CustomerDataCenterDto customerDataCenterDto;

    private long reingestTimeStampMs;

    private Integer scanTaskDepth;

    private String mediaEntityId;

    private DirectoryExistStatus isAccessible;

    private Long accessabilityLastCngDate;

    private Boolean isFirstScan;

    private Long fromDateScanFilter;

    private String mailboxGroup;

    private Long departmentId;

    private String departmentName;

    private List<RootFolderSharePermissionDto> rootFolderSharePermissions;
    private boolean shareFolderPermissionError = false;

    private boolean deleted = false;

    public RootFolderDto() {
    }

    public RootFolderDto(Long id, String path, Long docStoreId) {
        setId(id);
        setPath(path);
        setDocStoreId(docStoreId);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Long getDocStoreId() {
        return docStoreId;
    }

    public void setDocStoreId(Long docStoreId) {
        this.docStoreId = docStoreId;
    }

    public Integer getNumberOfFoldersFound() {
        return numberOfFoldersFound;
    }

    public void setNumberOfFoldersFound(Integer numberOfFoldersFound) {
        this.numberOfFoldersFound = numberOfFoldersFound;
    }

    public StoreLocation getStoreLocation() {
        return storeLocation;
    }

    public void setStoreLocation(StoreLocation storeLocation) {
        this.storeLocation = storeLocation;
    }

    public StorePurpose getStorePurpose() {
        return storePurpose;
    }

    public void setStorePurpose(StorePurpose storePurpose) {
        this.storePurpose = storePurpose;
    }

    public StoreSecurity getStoreSecurity() {
        return storeSecurity;
    }

    public void setStoreSecurity(StoreSecurity storeSecurity) {
        this.storeSecurity = storeSecurity;
    }

    public boolean isRescanActive() {
        return rescanActive;
    }

    public void setRescanActive(boolean rescanActive) {
        this.rescanActive = rescanActive;
    }

    public FileType[] getFileTypes() {
        return fileTypes;
    }

    public void setFileTypes(FileType[] fileTypes) {
        this.fileTypes = fileTypes;
    }

    public FileType[] getIngestFileTypes() {
        return ingestFileTypes;
    }

    public void setIngestFileTypes(FileType[] ingestFileTypes) {
        this.ingestFileTypes = ingestFileTypes;
    }

    //    @Override
//    public String toString() {
//        return "RootFolderDto{" +
//                "id=" + id +
//                ", path='" + path + '\'' +
//                ", realpath='" + realPath + '\'' +
//                ", mediaType=" + mediaType +
//                ", crawlerType=" + crawlerType+
//                '}';
//    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("RootFolderDto{");
        sb.append("id=").append(id);
        sb.append(", path='").append(path).append('\'');
        sb.append(", realPath='").append(realPath).append('\'');
        sb.append(", nickName='").append(nickName).append('\'');
//        sb.append(", description='").append(description).append('\'');
        sb.append(", docStoreId=").append(docStoreId);
        sb.append(", numberOfFoldersFound=").append(numberOfFoldersFound);
        sb.append(", lastRunId=").append(lastRunId);
//        sb.append(", storeLocation=").append(storeLocation);
//        sb.append(", storePurpose=").append(storePurpose);
//        sb.append(", storeSecurity=").append(storeSecurity);
        sb.append(", storeRescanMethod=").append(storeRescanMethod);
        sb.append(", rescanActive=").append(rescanActive);
        sb.append(", fileTypes=").append(fileTypes == null ? "null" : Arrays.asList(fileTypes).toString());
        sb.append(", folderExcludeRulesCount=").append(folderExcludeRulesCount);
        sb.append(", mediaType=").append(mediaType);
        sb.append(", crawlerType=").append(crawlerType);
        sb.append(", scannedFilesCountCap=").append(scannedFilesCountCap);
        sb.append(", mediaConnectionDetailsId=").append(mediaConnectionDetailsId);
        sb.append(", mediaConnectionName='").append(mediaConnectionName).append('\'');
        sb.append(", customerDataCenterDto=").append(customerDataCenterDto);
        sb.append(", reingestTimeStampMs=").append(reingestTimeStampMs);
        sb.append(", mediaEntityId=").append(mediaEntityId);
        sb.append('}');
        return sb.toString();
    }

   	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof RootFolderDto))
			return false;
		RootFolderDto that = (RootFolderDto) o;
		return
                CollectionsUtils.compareArrayAsSet(fileTypes, that.getFileTypes()) &&
                CollectionsUtils.compareArrayAsSet(ingestFileTypes, that.getIngestFileTypes()) &&
		 		isRescanActive() == that.isRescanActive() &&
				getFolderExcludeRulesCount() == that.getFolderExcludeRulesCount() &&
				getReingestTimeStampMs() == that.getReingestTimeStampMs() &&
				Objects.equal(getId(), that.getId()) &&
				Objects.equal(getPath(), that.getPath()) &&
				Objects.equal(getRealPath(), that.getRealPath()) &&
                Objects.equal(getMailboxGroup(), that.getMailboxGroup()) &&
				Objects.equal(getNickName(), that.getNickName()) &&
				Objects.equal(getDescription(), that.getDescription()) &&
				Objects.equal(getDocStoreId(), that.getDocStoreId()) &&
				Objects.equal(getNumberOfFoldersFound(), that.getNumberOfFoldersFound()) &&
				Objects.equal(getLastRunId(), that.getLastRunId()) &&
				getStoreLocation() == that.getStoreLocation() &&
				getStorePurpose() == that.getStorePurpose() &&
				getStoreSecurity() == that.getStoreSecurity() &&
				getStoreRescanMethod() == that.getStoreRescanMethod() &&
				getMediaType() == that.getMediaType() &&
				getCrawlerType() == that.getCrawlerType() &&
				Objects.equal(getScannedFilesCountCap(), that.getScannedFilesCountCap()) &&
				Objects.equal(getScannedMeaningfulFilesCountCap(), that.getScannedMeaningfulFilesCountCap()) &&
				Objects.equal(getMediaConnectionDetailsId(), that.getMediaConnectionDetailsId()) &&
				Objects.equal(getMediaConnectionName(), that.getMediaConnectionName()) &&
				Objects.equal(getCustomerDataCenterDto(), that.getCustomerDataCenterDto()) &&
				Objects.equal(getScanTaskDepth(), that.getScanTaskDepth()) &&
                Objects.equal(getDepartmentId(), that.getDepartmentId()) &&
				Objects.equal(getMediaEntityId(), that.getMediaEntityId()) &&
                Objects.equal(getFromDateScanFilter(), that.getFromDateScanFilter()) &&
				getIsAccessible() == that.getIsAccessible() &&
				Objects.equal(getAccessabilityLastCngDate(), that.getAccessabilityLastCngDate()) &&
				Objects.equal(isFirstScan, that.isFirstScan);
	}

	@Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    public int getFolderExcludeRulesCount() {
        return folderExcludeRulesCount;
    }

    public void setFolderExcludeRulesCount(int folderExcludeRulesCount) {
        this.folderExcludeRulesCount = folderExcludeRulesCount;
    }

    public StoreRescanMethod getStoreRescanMethod() {
        return storeRescanMethod;
    }

    public void setStoreRescanMethod(StoreRescanMethod storeRescanMethod) {
        this.storeRescanMethod = storeRescanMethod;
    }

    public MediaType getMediaType() {
        return mediaType;
    }

    public void setMediaType(MediaType mediaType) {
        this.mediaType = mediaType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getLastRunId() {
        return lastRunId;
    }

    public void setLastRunId(Long lastRunId) {
        this.lastRunId = lastRunId;
    }

    public Integer getScannedFilesCountCap() {
        return scannedFilesCountCap;
    }

    public void setScannedFilesCountCap(Integer scannedFilesCountCap) {
        this.scannedFilesCountCap = scannedFilesCountCap;
    }

    public Integer getScannedMeaningfulFilesCountCap() {
        return scannedMeaningfulFilesCountCap;
    }

    public void setScannedMeaningfulFilesCountCap(Integer scannedMeaningfulFilesCountCap) {
        this.scannedMeaningfulFilesCountCap = scannedMeaningfulFilesCountCap;
    }

    public void setCrawlerType(CrawlerType crawlerType) {
        this.crawlerType = crawlerType;
    }

    public CrawlerType getCrawlerType() {
        return crawlerType;
    }

    public void setMediaConnectionDetailsId(Long mediaConnectionDetailsId) {
        this.mediaConnectionDetailsId = mediaConnectionDetailsId;
    }

    public Long getMediaConnectionDetailsId() {
        return mediaConnectionDetailsId;
    }

    public String getRealPath() {
        return realPath;
    }

    public void setRealPath(String realPath) {
        this.realPath = realPath;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getMediaConnectionName() {
        return mediaConnectionName;
    }

    public void setMediaConnectionName(String mediaConnectionName) {
        this.mediaConnectionName = mediaConnectionName;
    }

    public CustomerDataCenterDto getCustomerDataCenterDto() {
        return customerDataCenterDto;
    }

    public void setCustomerDataCenterDto(CustomerDataCenterDto customerDataCenterDto) {
        this.customerDataCenterDto = customerDataCenterDto;
    }

    public long getReingestTimeStampMs() {
        return reingestTimeStampMs;
    }

    public void setReingestTimeStampMs(long reingestTimeStampMs) {
        this.reingestTimeStampMs = reingestTimeStampMs;
    }

    public void setReingestOn() {
        this.reingestTimeStampMs = System.currentTimeMillis();
    }

    public void setReingestOff() {
        this.reingestTimeStampMs = 0L;
    }

    public boolean isReingest() {
        return reingestTimeStampMs > 0;
    }

    public Integer getScanTaskDepth() {
        return scanTaskDepth;
    }

    public void setScanTaskDepth(Integer scanTaskDepth) {
        this.scanTaskDepth = scanTaskDepth;
    }

	public String getMediaEntityId() {
		return mediaEntityId;
	}

	public RootFolderDto setMediaEntityId(String mediaEntityId) {
		this.mediaEntityId = mediaEntityId;
		return this;
	}

    public DirectoryExistStatus getIsAccessible() {
        return isAccessible;
    }

    public void setIsAccessible(DirectoryExistStatus isAccessible) {
        this.isAccessible = isAccessible;
    }

    public Long getAccessabilityLastCngDate() {
        return accessabilityLastCngDate;
    }

    public void setAccessabilityLastCngDate(Long accessabilityLastCngDate) {
        this.accessabilityLastCngDate = accessabilityLastCngDate;
    }

    public Boolean getFirstScan() {
        return isFirstScan;
    }

    public void setFirstScan(Boolean firstScan) {
        isFirstScan = firstScan;
    }

    public Long getFromDateScanFilter() {
        return fromDateScanFilter;
    }

    public void setFromDateScanFilter(Long fromDateScanFilter) {
        this.fromDateScanFilter = fromDateScanFilter;
    }

    public String getMailboxGroup() {
        return mailboxGroup;
    }

    public void setMailboxGroup(String mailboxGroup) {
        this.mailboxGroup = mailboxGroup;
    }

    public Long getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Long departmentId) {
        this.departmentId = departmentId;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public List<RootFolderSharePermissionDto> getRootFolderSharePermissions() {
        return rootFolderSharePermissions;
    }

    public void setRootFolderSharePermissions(List<RootFolderSharePermissionDto> rootFolderSharePermissions) {
        this.rootFolderSharePermissions = rootFolderSharePermissions;
    }

    public String getIdentifier() {
        if (!Strings.isNullOrEmpty(getNickName())) {
            return getNickName();
        }
        if (!Strings.isNullOrEmpty(getPath())) {
            return getPath();
        }
        if (!Strings.isNullOrEmpty(getMailboxGroup())) {
            return getMailboxGroup();
        }
        if (getId() != null) {
            return String.valueOf(getId());
        }
        return null;
    }

    public boolean isShareFolderPermissionError() {
        return shareFolderPermissionError;
    }

    public void setShareFolderPermissionError(boolean shareFolderPermissionError) {
        this.shareFolderPermissionError = shareFolderPermissionError;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
}
