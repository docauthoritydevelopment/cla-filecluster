package com.cla.common.domain.dto.action;

import java.util.Properties;

/**
 * Created by uri on 14/02/2016.
 */
public class ActionInstanceDto {

    public ActionInstanceDto() {
    }

    public ActionInstanceDto(String name, ActionDefinitionDto action) {
        this.name = name;
        this.action = action;
    }

    private Properties parameters;

    private long id;
    private ActionDefinitionDto action;

    private String name;

    public void setId(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setAction(ActionDefinitionDto action) {
        this.action = action;
    }

    public ActionDefinitionDto getAction() {
        return action;
    }

    public Properties getParameters() {
        return parameters;
    }

    public void setParameters(Properties parameters) {
        this.parameters = parameters;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @Override
    public String toString() {
        return "ActionInstanceDto{" +
                "id=" + id +
                ", action=" + action +
                ", name='" + name + '\'' +
                '}';
    }
}
