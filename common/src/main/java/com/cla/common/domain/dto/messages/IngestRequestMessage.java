package com.cla.common.domain.dto.messages;

import java.util.Collections;
import java.util.List;

/**
 *
 * Created by vladi on 2/26/2017.
 */

public class IngestRequestMessage extends ProcessingPhaseMessage<IngestTaskParameters, IngestError> implements RequestMessage {

    private List<IngestTaskParameters> containedFilesPayloads = Collections.emptyList();

    private String requestId;

    public IngestRequestMessage() {
    }

    public IngestRequestMessage(IngestTaskParameters payload) {
        super(payload);
    }

    public List<IngestTaskParameters> getContainedFilesPayloads() {
        return containedFilesPayloads;
    }

    public void setContainedFilesPayloads(List<IngestTaskParameters> containedFilesPayloads) {
        this.containedFilesPayloads = containedFilesPayloads;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }
}
