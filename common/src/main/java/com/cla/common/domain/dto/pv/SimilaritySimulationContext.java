package com.cla.common.domain.dto.pv;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

public class SimilaritySimulationContext {

	private AnalyzeType analyzeType;
	
    Queue<Long> simulationMatchesQueue = null;
    Map<Long, Integer> simulationMatchesResultMap = null;
    List<Long> simulationDisqualifiedList = null;
    int simulationDisqualifiedCount = 0;

    private boolean writeSimilarityInfoSimulation;

	public SimilaritySimulationContext(AnalyzeType analyzeType, boolean showDisqualifiedList, boolean writeSimilarityInfoSimulation) {
		super();
		this.analyzeType = analyzeType;
		createSimulationMatchesQueue();
		createSimulationMatchesResultMap();
		if (showDisqualifiedList) {
			createSimulationDisqualifiedList();
		}
		this.simulationDisqualifiedCount = 0;
		this.writeSimilarityInfoSimulation = writeSimilarityInfoSimulation;
	}
	
	public SimilaritySimulationContext(AnalyzeType analyzeType, Queue<Long> simulationMatchesQueue,
			Map<Long, Integer> simulationMatchesResultMap, List<Long> simulationDisqualifiedList,
			boolean writeSimilarityInfoSimulation) {
		super();
		
		this.analyzeType = analyzeType;
		this.simulationMatchesQueue = simulationMatchesQueue;
		this.simulationMatchesResultMap = simulationMatchesResultMap;
		this.simulationDisqualifiedList = simulationDisqualifiedList;
		this.simulationDisqualifiedCount = 0;
		this.writeSimilarityInfoSimulation = writeSimilarityInfoSimulation;
	}
	
	public void clear() {
		removeSimulationMatchesQueue();
		removeSimulationMatchesResultMap();
		removeSimulationDisqualifiedList();
		this.simulationDisqualifiedCount = 0;
	}

	public void reset() {
		createSimulationMatchesQueue();
		createSimulationMatchesResultMap();
		if (simulationDisqualifiedList!=null) {
			createSimulationDisqualifiedList();
		}
		this.simulationDisqualifiedCount = 0;
	}

	@Override
	public String toString() {
		return "SimilaritySimulationContext [analyzeType=" + analyzeType + ", simulationMatchesQueue="
				+ simulationMatchesQueue + ", simulationMatchesResultMap=" + simulationMatchesResultMap
				+ ", simulationDisqualifiedList=" + simulationDisqualifiedList + ", simulationDisqualifiedCount="
				+ simulationDisqualifiedCount + ", writeSimilarityInfoSimulation=" + writeSimilarityInfoSimulation
				+ "]";
	}

	public AnalyzeType getAnalyzeType() {
		return analyzeType;
	}

	public void setAnalyzeType(AnalyzeType analyzeType) {
		this.analyzeType = analyzeType;
	}

    public void createSimulationMatchesQueue() {
    	if (simulationMatchesQueue!=null) {
    		simulationMatchesQueue.clear();
    	}
    	else {
    		simulationMatchesQueue = new LinkedList<>();
    	}
    }

    public void createSimulationDisqualifiedList() {
    	if (simulationDisqualifiedList!=null) {
    		simulationDisqualifiedList.clear();
    	}
    	else {
    		simulationDisqualifiedList = new ArrayList<>();
    	}
    }
    
    public void resetSimulationDisqualifiedCount() {
    	simulationDisqualifiedCount = 0;
    }
    
    public void createSimulationMatchesResultMap() {
    	if (simulationMatchesResultMap!=null) {
    		simulationMatchesResultMap.clear();
    	}
    	else {
    		simulationMatchesResultMap = new HashMap<>();
    	}
    }
    
    public void removeSimulationMatchesQueue() {
    	if (simulationMatchesQueue!=null) {
    		simulationMatchesQueue.clear();
    	}
    	simulationMatchesQueue = null;
    }
    
    public void removeSimulationDisqualifiedList() {
    	if (simulationDisqualifiedList!=null) {
    		simulationDisqualifiedList.clear();
    	}
    	simulationDisqualifiedList = null;
    }

    public void removeSimulationMatchesResultMap() {
    	if (simulationMatchesResultMap!=null) {
    		simulationMatchesResultMap.clear();
    	}
    	simulationMatchesResultMap = null;
    }

    public Collection<Long> getSimulationMatchesQueue() {
    	return simulationMatchesQueue;
    }

    public Collection<Long> getSimulationDisqualifiedList() {
    	return simulationDisqualifiedList;
    }
    
    public int getSimulationDisqualifiedCount() {
    	return simulationDisqualifiedCount;
    }
    
    public Map<Long, Integer> getSimulationMatchesResultMap() {
    	return simulationMatchesResultMap;
    }

    public  void  addToSimulationMatchesQueue(Long id) {
    	if (simulationMatchesQueue!=null) {
    		boolean status = simulationMatchesQueue.offer(id);
    		if (!status) {
//    			logger.debug("Simulation failure: could not add {} into simulationMatchesQueue", id);    			
    		}
    	}
    }

    public  void  addToSimulationDisqualifiedList(Long id) {
    	simulationDisqualifiedCount++;
    	if (simulationDisqualifiedList!=null) {
    		simulationDisqualifiedList.add(id);
    	}
    }

    public  void  addToSimulationMatchesResultMap(Long id, int simCalc) {
    	if (simulationMatchesResultMap!=null) {
    		simulationMatchesResultMap.put(id, Integer.valueOf(simCalc));
    	}
    }

	public boolean isWriteSimilarityInfoSimulation() {
		return writeSimilarityInfoSimulation;
	}

	public void setWriteSimilarityInfoSimulation(boolean writeSimilarityInfoSimulation) {
		this.writeSimilarityInfoSimulation = writeSimilarityInfoSimulation;
	}

}
