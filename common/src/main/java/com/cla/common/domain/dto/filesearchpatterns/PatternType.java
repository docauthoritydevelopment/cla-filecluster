package com.cla.common.domain.dto.filesearchpatterns;

/**
 * Created by uri on 23/06/2016.
 */
public enum PatternType {
    REGEX,PREDEFINED
}
