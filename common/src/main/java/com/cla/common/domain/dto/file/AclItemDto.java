package com.cla.common.domain.dto.file;

import com.cla.connector.domain.dto.acl.AclType;

import java.io.Serializable;

/**
 * Created by uri on 06/07/2015.
 */
public class AclItemDto implements Serializable {

	private Long claFileId;
	private String entity;
	private AclType type;
	private AclLevel level;

	public AclItemDto(Long claFileId, String entity, AclType type, AclLevel level) {
		this.claFileId = claFileId;
		this.entity = entity;
		this.type = type;
		this.level = level;
	}


	public Long getClaFileId() {
		return claFileId;
	}

	public void setClaFileId(Long claFileId) {
		this.claFileId = claFileId;
	}

	public String getEntity() {
		return entity;
	}

	public void setEntity(String entity) {
		this.entity = entity;
	}

	public AclType getType() {
		return type;
	}

	public void setType(AclType type) {
		this.type = type;
	}

	public AclLevel getLevel() {
		return level;
	}

	public void setLevel(AclLevel level) {
		this.level = level;
	}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AclItemDto that = (AclItemDto) o;

        if (claFileId != null ? !claFileId.equals(that.claFileId) : that.claFileId != null) return false;
        if (entity != null ? !entity.equals(that.entity) : that.entity != null) return false;
        if (type != that.type) return false;
        if (level != that.level) return false;

        return true;
    }

    @Override
    public int hashCode() {
		int result = (claFileId != null ? claFileId.hashCode() : 0);
        result = 31 * result + (entity != null ? entity.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (level != null ? level.hashCode() : 0);
        return result;
    }

    @Override
	public String toString() {
		return "{claFileId=" + claFileId + ", entity=" + entity + ", type=" + type + ", level=" + level + "}";
	}
	
}
