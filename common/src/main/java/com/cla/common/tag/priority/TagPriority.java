package com.cla.common.tag.priority;

import org.apache.commons.lang3.math.NumberUtils;

import java.util.HashMap;
import java.util.Map;

public final class TagPriority implements Comparable<TagPriority> {

    public static final String VIA_DOC_TYPE_INDICATION = "vdt";

    public static final TagPriority FILE = new TagPriority("file", 0);

    public static final TagPriority FILE_GROUP = new TagPriority("fileGroup", 1);

    public static final int PRIORITIES_SIZE = 32;

    private static Map<String, TagPriority> reverseLookup = new HashMap<>();

    static {

        reverseLookup.put(FILE.priority, FILE);
        reverseLookup.put(FILE_GROUP.priority, FILE_GROUP);

        for (int i = 0; i < PRIORITIES_SIZE; i++) {
            String folderDepthPriority = folderKey(i);
            reverseLookup.put(folderDepthPriority, new TagPriority(folderDepthPriority, PRIORITIES_SIZE - 1 - i));
        }
    }

    private static String folderKey(int i) {
        return "f" + i;
    }

    private final Integer index;
    private final String priority;

    private TagPriority(String priority, Integer index) {
        this.priority = priority;
        this.index = index;

    }

    public static TagPriority byValue(String value) {
        if (NumberUtils.isNumber(value)) {
            return folder(Integer.valueOf(value));
        }
        return reverseLookup.get(value);
    }

    public static boolean isViaDocType(String value) {
        return value != null && value.contains(VIA_DOC_TYPE_INDICATION);
    }

    public String getPriority() {
        return priority;
    }

    public Integer getIndex() {
        return index;
    }

    public static TagPriority folder(int depthFromRoot) {
        return reverseLookup.get(folderKey(depthFromRoot));
    }

    public boolean isFolder() {
        return this != FILE && this != FILE_GROUP;
    }

    @Override
    public int compareTo(TagPriority o) {
        if (this == o) {
            return 0;
        }

        if (this == FILE) { // File comes first
            return -1;
        }

        if (this == FILE_GROUP) {
            if (o == FILE) { // File comes first
                return 1;
            }
            return -1;
        }

        if (this.isFolder()) {
            if (o.isFolder()) {
                return Integer.compare(this.getIndex(), o.getIndex());
            }
            return 1;
        }

        // Should never get here
        return 0;
    }


}
