package com.cla.common.tag.scope;

import com.cla.common.domain.dto.department.DepartmentDto;
import com.cla.common.domain.dto.doctype.DocTypeDto;
import com.google.common.base.Strings;
import org.apache.commons.lang3.ArrayUtils;

public class PriorityScopedValueResolver {

    public static final int SCOPE_IDENTIFIER_INDEX = 0;
    public static final int VALUE_TYPE_IDENTIFIER_INDEX = 1;

    public static final String VALUE_DELIMITER = ".";
    public static final String VALUE_DELIMETER_PATTERN = "\\" + VALUE_DELIMITER;
    private final String scopedTagNewValue;
    private final String tagGroupingPrefix;


    public PriorityScopedValueResolver(String scopedTagNewValue) {
        this.scopedTagNewValue = scopedTagNewValue;
        String[] parts = scopedTagNewValue.split(VALUE_DELIMETER_PATTERN);
        this.tagGroupingPrefix = parts[SCOPE_IDENTIFIER_INDEX] + VALUE_DELIMITER + parts[VALUE_TYPE_IDENTIFIER_INDEX] + VALUE_DELIMITER;
    }

    public String[] resolveNewScopedValues(String[] scopedTagValues, boolean addCommand) {
        String[] result = scopedTagValues;
        PriorityScopedTag priorityScopedTag = extractMatchingScopedTag(scopedTagValues);

        // Means value to remove not found, then ignore it
        if (!addCommand && priorityScopedTag.getIndex() == scopedTagValues.length) {
            return result;
        }

        if (addCommand) {
            priorityScopedTag.add(scopedTagNewValue);
        } else {
            priorityScopedTag.remove(scopedTagNewValue);
        }

        // Handle new value (i.e. either remove/update/add)
        String value = priorityScopedTag.value();
        if (Strings.isNullOrEmpty(priorityScopedTag.value())) {// Value is empty then remove the entire scoped tag
            result = ArrayUtils.remove(scopedTagValues, priorityScopedTag.getIndex());
        } else if (priorityScopedTag.getIndex() < scopedTagValues.length) { // already exists then Update
            result[priorityScopedTag.getIndex()] = value;
        } else { // Otherwise add it
            result = ArrayUtils.add(scopedTagValues, value);
        }

        return result;
    }

    private PriorityScopedTag extractMatchingScopedTag(String[] scopedTagValues) {
        for (int i = 0; i < scopedTagValues.length; i++) {
            if (scopedTagValues[i].startsWith(DocTypeDto.ID_PREFIX)) {
                if (scopedTagValues[i].contains("..") && scopedTagValues[i].startsWith(tagGroupingPrefix)) {
                    return new PriorityScopedTag(i, scopedTagValues[i]);
                }
            } else if (scopedTagValues[i].startsWith(DepartmentDto.ID_PREFIX)) {
                return new PriorityScopedTag(i, scopedTagValues[i]);
            } else if (scopedTagValues[i].startsWith(tagGroupingPrefix)) {
                return new PriorityScopedTag(i, scopedTagValues[i]);
            }
        }
        return new PriorityScopedTag(scopedTagValues.length, "");
    }
}
