package com.cla.common.tag.scope;

import com.cla.common.tag.priority.TagPriority;
import com.google.common.base.Strings;

import java.util.stream.Collectors;
import java.util.stream.Stream;

public class PriorityScopedTag {

    private int index;
    private String[] slots = new String[TagPriority.PRIORITIES_SIZE];

    public PriorityScopedTag(int index, String scopedTagValue) {
        this.index = index;
        if (!Strings.isNullOrEmpty(scopedTagValue)) {
            try {
                String[] tagValues = scopedTagValue.split(";");
                initSlots(tagValues);
            } catch (Exception e) {
                throw new RuntimeException("scopedTagValue:" + scopedTagValue, e);
            }
        }
    }

    private void initSlots(String[] tagValues) {
        for (String tagValue : tagValues) {
            add(tagValue);
        }
    }

    public void add(String tagNewValue) {
        TagPriority tagPriority = extractTagPriority(tagNewValue);
        String currentValue = slots[tagPriority.getIndex()];
        if (currentValue == null) {
            slots[tagPriority.getIndex()] = tagNewValue;
            return;
        }

        boolean tagNewValueViaDocType = TagPriority.isViaDocType(tagNewValue);
        String[] internalSlots = currentValue.split(";");
        if (internalSlots.length == 1) {
            boolean currentValueViaDocType = TagPriority.isViaDocType(internalSlots[0]);
            if (currentValueViaDocType == tagNewValueViaDocType) {
                slots[tagPriority.getIndex()] = tagNewValue;
            } else if (currentValueViaDocType) {
                slots[tagPriority.getIndex()] = tagNewValue + ";" + internalSlots[0];
            } else {
                slots[tagPriority.getIndex()] = internalSlots[0] + ";" + tagNewValue;
            }
        } else { // means the length is 2
            if (tagNewValueViaDocType) {
                slots[tagPriority.getIndex()] = internalSlots[0] + ";" + tagNewValue;
            } else {
                slots[tagPriority.getIndex()] = tagNewValue + ";" + internalSlots[1];
            }
        }
    }

    public int getIndex() {
        return index;
    }

    public String value() {
        return Stream.of(slots)
                .filter(s -> s != null)
                .collect(Collectors.joining(";"));
    }

    public void remove(String scopedTagValue) {
        TagPriority tagPriority = extractTagPriority(scopedTagValue);
        String currentValue = slots[tagPriority.getIndex()];
        if (currentValue == null || !currentValue.contains(scopedTagValue)) {
            return;
        }
        if (!TagPriority.isViaDocType(currentValue)) {
            slots[tagPriority.getIndex()] = null;
            return;
        }

        boolean tagToRemoveViaDocType = TagPriority.isViaDocType(scopedTagValue);
        String[] internalSlots = currentValue.split(";");
        if (internalSlots.length == 2) { // Meaning doc type tag & actual tags were assigned
            if (tagToRemoveViaDocType) {
                slots[tagPriority.getIndex()] = internalSlots[0]; // Choose to set the actual tag
            } else {
                slots[tagPriority.getIndex()] = internalSlots[1]; // Choose to set the doc type tag
            }
        } else { // Only one singe tag is assigned, just remove it
            slots[tagPriority.getIndex()] = null;
        }
    }

    private TagPriority extractTagPriority(String scopedTagValue) {
        return TagPriority.byValue(scopedTagValue.substring(scopedTagValue.lastIndexOf(".") + 1));
    }
}
