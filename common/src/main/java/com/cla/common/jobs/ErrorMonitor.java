package com.cla.common.jobs;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by oren on 2/21/2018.
 */
public class ErrorMonitor {

    private AtomicLong beginningOfErrorBurstTimestamp;

    private AtomicInteger lastBurstErrorCount;

    private final long errorInterval;

    /**
     * Error count per time interval threshold.
     */
    private final int errorThreshold;

    /**
     *
     * @param errorInterval in ms
     * @param errorThreshold    error count
     */
    public ErrorMonitor(int errorInterval, TimeUnit errorTimeIntervalUnit, int errorThreshold) {
        this.errorInterval = errorTimeIntervalUnit.toMillis(errorInterval);
        this.errorThreshold = errorThreshold;
        beginningOfErrorBurstTimestamp = new AtomicLong();
        lastBurstErrorCount = new AtomicInteger(0);
    }

    public boolean addErrorAndGetIsValveTriggered() {
        resetStateIfNeeded();
        int lastCount = lastBurstErrorCount.incrementAndGet();
        return lastCount > errorThreshold;
    }

    private void resetStateIfNeeded() {
        Instant lastBurstInst = Instant.ofEpochMilli(beginningOfErrorBurstTimestamp.get());
        if (Instant.now().minus(errorInterval, ChronoUnit.MILLIS).compareTo(lastBurstInst) > 0) {
            synchronized (beginningOfErrorBurstTimestamp) {
                if (Instant.now().minusMillis(errorInterval).compareTo(lastBurstInst) > 0) {
                    beginningOfErrorBurstTimestamp.set(Instant.now().toEpochMilli());
                    lastBurstErrorCount.set(0);
                }
            }
        }
    }
}
