package com.cla.common.workflow;

import com.cla.common.constants.SolrFieldOp;
import com.cla.common.exceptions.NotSupportedException;
import org.apache.commons.lang3.ArrayUtils;

public class WorkflowDataResolver {


    public static final String CATEGORY_SEPARATOR = ".";
    public static final String WF_DATA_SEPARATOR_PATTERN = "\\" + CATEGORY_SEPARATOR;


    private WorkflowDataResolver() {
    }

    public static String[] resolveNewWorkflowDataValues(String[] existingWorkflowValues, SolrFieldOp fieldOp, String workflowDataValue) {
        switch (fieldOp) {
            case SET:
                return addWfData(existingWorkflowValues, workflowDataValue, false);
            case ADD:
                return addWfData(existingWorkflowValues, workflowDataValue, true);
            case REMOVE:
                return removeWorkflowDataValue(existingWorkflowValues, workflowDataValue);
        }
        throw new NotSupportedException(String.format("Not supported operation %s for workflow data", fieldOp));
    }


    private static String[] addWfData(String[] existingWorkflowValues, String workflowDataValue, boolean increaseRefCount) {
        String[] newWfDataParts = workflowDataValue.split(WF_DATA_SEPARATOR_PATTERN);
        int workflowSlot = findWorkflowSlot(existingWorkflowValues, workflowDataValue);
        if (workflowSlot < 0) {
            String newValue = String.join(CATEGORY_SEPARATOR,
                    newWfDataParts[0],
                    "1",
                    newWfDataParts[1],
                    newWfDataParts[2]);
            return ArrayUtils.add(existingWorkflowValues, newValue);
        } else {
            String existingWorkflowValue = existingWorkflowValues[workflowSlot];
            String[] existingWfDataParts = existingWorkflowValue.split(WF_DATA_SEPARATOR_PATTERN);
            String action = newWfDataParts[1].equals("null") ? existingWfDataParts[2] : newWfDataParts[1];
            String state = newWfDataParts[2].equals("null") ? existingWfDataParts[3] : newWfDataParts[2];
            String refCount = increaseRefCount ?
                    String.valueOf(Integer.valueOf(existingWfDataParts[1]) + 1) : existingWfDataParts[1];
            existingWorkflowValues[workflowSlot] = String.join(CATEGORY_SEPARATOR,
                    existingWfDataParts[0],
                    refCount,
                    action,
                    state);
            return existingWorkflowValues;
        }
    }

    private static String[] removeWorkflowDataValue(String[] existingWorkflowValues, String workflowDataValue) {
        String[] valuesToWorkOn = existingWorkflowValues == null ? new String[0] : existingWorkflowValues;
        int slot = findWorkflowSlot(valuesToWorkOn, workflowDataValue);
        if (slot > -1) {
            String[] existingWfDataParts = valuesToWorkOn[slot].split(WF_DATA_SEPARATOR_PATTERN);
            Integer refCount = Integer.valueOf(existingWfDataParts[1]);
            if (refCount == 1) {
                valuesToWorkOn = ArrayUtils.remove(valuesToWorkOn, slot);
            } else {
                valuesToWorkOn[slot] = String.join(CATEGORY_SEPARATOR,
                        existingWfDataParts[0],
                        String.valueOf(--refCount),
                        existingWfDataParts[2],
                        existingWfDataParts[3]);
            }
        }

        return valuesToWorkOn;
    }

    private static int findWorkflowSlot(String[] existingWorkflowValues, String workflowDataValue) {
        int wfIdIndex = workflowDataValue.indexOf(".");
        String workFlowIdPrefix = wfIdIndex < 0 ?
                workflowDataValue : workflowDataValue.substring(0, wfIdIndex + 1);
        for (int i = 0; i < existingWorkflowValues.length; i++) {
            if (existingWorkflowValues[i].startsWith(workFlowIdPrefix)) {
                return i;
            }
        }

        return -1;
    }
}
