package com.cla.common.exceptions;

/**
 * Created by liora on 12/07/2017.
 */
public class BreakStreamException  extends RuntimeException  {

    public BreakStreamException() {
    }

    public BreakStreamException(String message) {
        super(message);
    }
}
