package com.cla.common.serialization;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.Map;

/**
 * This class is to used instead of the plain ObjectInputStream in cae class versioning
 * is not crucial for you.
 *
 * Also can be  provided with a compatability map to support backward compatability of
 * classes that were refactored to different path.
 */
public class BackwardCompatabilityInputStream extends ObjectInputStream {

    private static final String INCOMPATIBLE_CLASSES_VERSIONS_MESSAGE = "Overriding serialized class version mismatch: local serialVersionUID = %d" +
            " stream serialVersionUID = %d";

    private static Logger logger = LoggerFactory.getLogger(BackwardCompatabilityInputStream.class);

    private final Map<String, String> compatibilityMap;

    public BackwardCompatabilityInputStream(InputStream in, Map<String, String> compatibilityMap) throws IOException {
        super(in);
        this.compatibilityMap = compatibilityMap;
    }

    /**
     * Ignore the case of classes version incompatibility and post a trace message
     * @return
     * @throws IOException
     * @throws ClassNotFoundException
     */
    protected ObjectStreamClass readClassDescriptor() throws IOException, ClassNotFoundException {
        ObjectStreamClass resultClassDescriptor = super.readClassDescriptor(); // initially streams descriptor
        Class localClass; // the class in the local JVM that this descriptor represents.
        try {
            String concreteClass = compatibilityMap.getOrDefault(resultClassDescriptor.getName(), resultClassDescriptor.getName());
            localClass = Class.forName(concreteClass);
        } catch (ClassNotFoundException e) {
            logger.error("No local class for " + resultClassDescriptor.getName(), e);
            return resultClassDescriptor;
        }
        ObjectStreamClass localClassDescriptor = ObjectStreamClass.lookup(localClass);
        if (localClassDescriptor != null) { // only if class implements serializable
            final long localSUID = localClassDescriptor.getSerialVersionUID();
            final long streamSUID = resultClassDescriptor.getSerialVersionUID();
            if (streamSUID != localSUID) { // check for serialVersionUID mismatch.
                String s = "Overriding serialized class version mismatch: " + "local serialVersionUID = " + localSUID +
                        " stream serialVersionUID = " + streamSUID;
                Exception e = new InvalidClassException(
                        String.format(INCOMPATIBLE_CLASSES_VERSIONS_MESSAGE, localSUID, streamSUID));
                logger.trace("Potentially Fatal Deserialization Operation.", e);
                resultClassDescriptor = localClassDescriptor; // Use local class descriptor for deserialization
            }
        }
        return resultClassDescriptor;
    }

    @Override
    protected Class<?> resolveClass(ObjectStreamClass desc) throws IOException, ClassNotFoundException {
        String className = compatibilityMap.getOrDefault(desc.getName(), desc.getName());
        if (!className.equals(desc.getName())) {
            desc = ObjectStreamClass.lookup(Class.forName(className));
        }
        return super.resolveClass(desc);
    }
}