package com.cla.filecluster.util;

import com.cla.connector.domain.dto.media.MediaType;
import org.apache.commons.io.ByteOrderMark;
import org.apache.commons.io.input.BOMInputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 *
 * Created by uri on 28/02/2016.
 */
public class LocalFileUtils {

    private static Logger logger = LoggerFactory.getLogger(LocalFileUtils.class);

    public static String createFullPath(MediaType mediaType, String rootFolderPath, String filePath) {
        return mediaType.isPathAsMediaEntity() ? rootFolderPath + filePath : filePath;
    }

    public static void addFileToZip(Path f, ZipOutputStream zipOutputStream) throws IOException {
        String fileName = f.getFileName().toString();
        logger.debug("Adding file {} to zip",fileName);
        ZipEntry ze= new ZipEntry(fileName);
        zipOutputStream.putNextEntry(ze);
        FileInputStream in = new FileInputStream(f.toFile());
        int len;
        byte[] buffer = new byte[10000];
        while ((len = in.read(buffer)) > 0) {
            zipOutputStream.write(buffer, 0, len);
        }
        in.close();
        zipOutputStream.closeEntry();
    }

    public static byte[] getFileBytesWithoutBOM(MultipartFile file) throws IOException {
        InputStream is = file.getInputStream();
        if (is == null) {
            return new byte[0];
        }
        try (BOMInputStream bomInputStream = new BOMInputStream(is, false, ByteOrderMark.UTF_8)) {
            return FileCopyUtils.copyToByteArray(bomInputStream);
        }
    }
}
