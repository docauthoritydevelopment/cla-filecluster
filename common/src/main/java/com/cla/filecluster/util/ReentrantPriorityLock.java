package com.cla.filecluster.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Comparator;
import java.util.Objects;
import java.util.PriorityQueue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 *
 * A reentrant mutex which under contention grants access to threads based on priority.
 *
 * <p> Thread priority is specified by a {@link Comparator} provided at construction time.
 *
 * <p> The constructor for this class accepts an optional fairness parameter.
 * When set to true, in case of contention of two waiting threads that have the same priority, a FIFO order
 * will be used to break the tie. Otherwise this lock does not guarantee any particular access order (beyond the
 * order dictated by the provided Comparator).
 *
 * <p> This lock does not maintain any hold count. If the lock was acquired by the same thread more than once,
 * one call to this method releases the lock.
 *
 * <p> Created by Itai Marko.
 *
 * @param <T> the type of objects that will be used as priority keys
 */
public class ReentrantPriorityLock<T> {
// TODO Itai: test this class
    private static final Logger logger = LoggerFactory.getLogger(ReentrantPriorityLock.class);

    private static final AtomicInteger enqueueSeq = new AtomicInteger(0);

    private PrioritizedThread executingThread;
    private final PriorityQueue<PrioritizedThread> waitingThreads;
    private final ReentrantLock lock = new ReentrantLock(true);

    /**
     * Creates an instance of ReentrantPriorityLock with priority based on the provided Comparator.
     * This is equivalent to using ReentrantPriorityLock(true).
     *
     * @param prioritizer used to specify which thread has priority to acquire this lock.
     */
    @SuppressWarnings("unused")
    public ReentrantPriorityLock(Comparator<? super T> prioritizer) {
        this(prioritizer, true);
    }

    /**
     * Creates an instance of ReentrantPriorityLock with priority based on the provided Comparator and
     * the provided fairness policy.
     *
     * @param prioritizer used to specify which thread has priority to acquire this lock.
     * @param fair specifies whether this lock should use FIFO order to break priority ties.
     */
    public ReentrantPriorityLock(Comparator<? super T> prioritizer, boolean fair) {
        Objects.requireNonNull(prioritizer);

        Comparator<PrioritizedThread> threadPrioritizer = new ThreadPrioritizer(prioritizer);
        if (fair) {
            threadPrioritizer = threadPrioritizer.thenComparing(new SequentialThreadPrioritizer());
        }
        this.waitingThreads = new PriorityQueue<>(threadPrioritizer);
    }

    /**
     * Acquires the lock unless the current thread is {@linkplain Thread#interrupt interrupted}.
     *
     * <p> Acquires the lock if it is not held by another thread.
     *
     * If the lock is held by another thread then the current thread becomes disabled for thread scheduling
     * purposes and is added to a queue of waiting threads. When a thread holding this lock releases it, the
     * next-in-line thread is taken from the queue and returns from this method, holding the lock.
     *
     * The threads waiting in the queue are ordered by the {@code prioritizer} provided to the constructor of
     * this lock. Each thread's priority is determined by the {@code priorityKey} parameter.
     *
     * <p> In case an {@code InterruptedException} is thrown, the current thread does not hold the lock and is
     * removed from the waiting threads queue.
     *
     * @param priorityKey specifies the priority of the current thread when contending for the lock
     * @throws InterruptedException if the current thread is interrupted
     */
    public void lockInterruptibly(T priorityKey) throws InterruptedException {
        int enqueueSeqNum = enqueueSeq.getAndIncrement();
        PrioritizedThread newPrioritizedThread =
                new PrioritizedThread(priorityKey, Thread.currentThread(), enqueueSeqNum);
        lock.lockInterruptibly();
        try {
            if (executingThread == null) {
                executingThread = newPrioritizedThread;
            } else if (!executingThread.thread.equals(Thread.currentThread())) {
                enqueue(newPrioritizedThread);
            }
        } finally {
            lock.unlock();
        }
    }

    /**
     * Releases this lock if it is held by the current thread.
     *
     * <p> This method has no effect if the current thread is not the holder of this lock.
     *
     * <p> This lock does not maintain any hold count. If the lock was acquired by the same thread more than once,
     * one call to this method releases the lock.
     */
    public void unlock() {
        lock.lock();
        try {
            releaseAndDequeue();
        } finally {
            lock.unlock();
        }
    }

    /**
     * Releases the lock if there are other threads with higher priority waiting for it and goes back to wait
     * in the queue.
     *
     * <p> If there are no such threads, the calling thread retains the lock.
     *
     * <p> If this lock was constructed to use a fair ordering policy then when going back to the queue, this thread
     * will pass all threads of similar (or lesser) priority that were enqueued after this thread. Otherwise, threads
     * with similar priority will take arbitrary order in the queue.
     *
     * @throws InterruptedException if the current thread is interrupted
     */
    public void yield() throws InterruptedException {
        lock.lockInterruptibly();
        try {
            if (hasQueuedThreads()){
                PrioritizedThread releasedThread = releaseAndDequeue();
                if (releasedThread != null) {
                    enqueue(releasedThread);
                }
            }
        } finally {
            lock.unlock();
        }
    }

    /**
     * @return true iff there are other threads waiting to acquire the lock
     */
    public boolean hasQueuedThreads() {
        lock.lock();
        try {
            return waitingThreads.size() > 0;
        } finally {
            lock.unlock();
        }
    }

    /**
     * @return true iff true if any thread holds this lock
     */
    public boolean isLocked() {
        lock.lock();
        try {
            return executingThread != null;
        } finally {
            lock.unlock();
        }
    }

    private void enqueue(PrioritizedThread waitingThread) throws InterruptedException {
        try {
            waitingThreads.add(waitingThread);
            logger.debug("ReentrantPriorityLock enqueuing PrioritizedThread: {}", waitingThread);
            while (executingThread != waitingThread) {
                waitingThread.canExecute.await(); // assuming lock is held
            }
            logger.debug("PrioritizedThread: {} acquired the ReentrantPriorityLock", waitingThread);
        } catch (InterruptedException ie) {
            waitingThreads.remove(waitingThread);
            throw ie;
        }
    }

    private PrioritizedThread releaseAndDequeue() {
        PrioritizedThread releasedThread = null;
        if (executingThread != null && executingThread.thread.equals(Thread.currentThread())) {
            releasedThread = executingThread;
            executingThread = waitingThreads.poll();
            logger.debug("PrioritizedThread: {} released hold of the ReentrantPriorityLock", releasedThread);
            if (executingThread != null) {
                logger.debug("Signaling PrioritizedThread: {} to wake up", executingThread);
                executingThread.canExecute.signal(); // assuming lock is held
            } else {
                enqueueSeq.set(0);
            }
        }
        return releasedThread;
    }

    /**
     * A helper class to represent a thread waiting in the queue.
     */
    private final class PrioritizedThread {

        private final int seqNum;
        private final Condition canExecute;
        private final T priorityKey;
        private final Thread thread;

        PrioritizedThread(T priorityKey, Thread thread, int seqNum) {
            Objects.requireNonNull(priorityKey);
            Objects.requireNonNull(thread);
            this.seqNum = seqNum;
            this.priorityKey = priorityKey;
            this.thread = thread;
            this.canExecute = lock.newCondition();
        }

        @Override
        public String toString() {
            return "PrioritizedThread{" +
                    "seqNum=" + seqNum +
                    ", priorityKey=" + priorityKey +
                    ", thread=" + thread +
                    '}';
        }
    }

    /**
     * An adapter to compare {@link PrioritizedThread PrioritizedThread} objects by
     * their {@link PrioritizedThread#priorityKey priorityKey} field.
     */
    private final class ThreadPrioritizer implements Comparator<PrioritizedThread> {

        private final Comparator<? super T> innerPrioritizer;

        ThreadPrioritizer(Comparator<? super T> innerPrioritizer) {
            this.innerPrioritizer = innerPrioritizer;
        }

        @Override
        public int compare(PrioritizedThread t1, PrioritizedThread t2) {
            return innerPrioritizer.compare(t1.priorityKey, t2.priorityKey);
        }
    }

    /**
     * A comparator that orders {@link PrioritizedThread PrioritizedThread} objects
     * by their {@link PrioritizedThread#seqNum seqNum} field.
     */
    private final class SequentialThreadPrioritizer implements Comparator<PrioritizedThread> {

        @Override
        public int compare(PrioritizedThread t1, PrioritizedThread t2) {
            return t1.seqNum - t2.seqNum;
        }
    }
}
