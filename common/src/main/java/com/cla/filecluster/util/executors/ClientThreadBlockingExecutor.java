package com.cla.filecluster.util.executors;

import java.util.concurrent.Callable;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;


public class ClientThreadBlockingExecutor  extends BlockingExecutor{
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(ClientThreadBlockingExecutor.class);
	private Consumer<Integer> postRunProgressHandler = null;
    private int errorsCounter = 0;

    public ClientThreadBlockingExecutor() {
		super(1, 1, Long.MAX_VALUE, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>());
	}
	
	@Override
	public void  execute(final Runnable task)
	{
		try {
			task.run();
			if (postRunProgressHandler!=null) {
				postRunProgressHandler.accept(100);
			}
		} catch (Exception e) {
			logger.error("Exception during worker thread execution. {}", e.getMessage(), e);
//                    throw new RuntimeException("Error during worker thread execution.", e);
            errorsCounter++;
		} catch (Error e) {
			logger.error("Irrecoverable error during worker thread execution. {}", e.getMessage(), e);
            errorsCounter++;
		}
	}

	@Override
	public void blockUntilProcessingFinished() {
		setPostRunHandler(null);
	}

	@Override
	public void blockUntilProcessingFinished(Consumer<Integer> progressHandler) {
        if (progressHandler!=null) {
            progressHandler.accept(100);
        }
		setPostRunHandler(null);
	}

	// Allow progress reporting when task ends - parameter is progress percentage (0-100)
	// Progress handler is running in the context of the worker threads
	@Override
	public Consumer<Integer> setPostRunHandler(final Consumer<Integer> newHandler) {
		final Consumer<Integer> prevHandler = postRunProgressHandler;
		postRunProgressHandler = newHandler;
		return prevHandler;
	}

    @Override
    public int getErrorCounter() {
        return errorsCounter;
    }
}
