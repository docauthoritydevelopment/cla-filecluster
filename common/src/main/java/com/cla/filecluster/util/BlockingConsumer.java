package com.cla.filecluster.util;

/**
 * A  a <a href="package-summary.html">functional interface</a> similar to
 * {@link java.util.function.Consumer Consumer} which represents an operation that accepts a single input
 * argument, returns no result and may block (thus, may throw {@link InterruptedException})
 *
 * <p>
 * Created by Itai Marko.
 *
 * @param <T> the type of the input to the operation
 */
@FunctionalInterface
public interface BlockingConsumer<T> {

    void accept(T t) throws InterruptedException;
}
