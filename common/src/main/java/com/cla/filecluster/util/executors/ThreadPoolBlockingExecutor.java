package com.cla.filecluster.util.executors;

import com.cla.filecluster.domain.exceptions.StoppedByUserException;

import java.util.concurrent.*;
import java.util.function.Consumer;

public class ThreadPoolBlockingExecutor extends BlockingExecutor {
    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(ThreadPoolBlockingExecutor.class);
    private Consumer<Integer> postRunProgressHandler = null;
    private final Semaphore semaphore;
    private long lastExecutionTime;
    private final long idleTimeBeforeShutdown;
    private int executionCounter = 0;
    private int errorCounter = 0;

    private final int totalPermits;
    private ThreadFactory threadFactory;
    private ThreadFactory defaultThreadFactory;
    private String name;

    private long masterThreadId = 0;
    private String masterThreadName;

    private final long queueSize;

    private final boolean printMasterThreadWarning;


    public ThreadPoolBlockingExecutor(final int poolSize, final int queueSize, final long idleTimeBeforeShutdown) {
        this(null, poolSize, queueSize, idleTimeBeforeShutdown, false);
    }

    public ThreadPoolBlockingExecutor(final String name, final int poolSize, final int queueSize, final long idleTimeBeforeShutdown,
                                      boolean printMasterThreadWarning) {
        super(poolSize, poolSize, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>());
        this.queueSize = queueSize;
        this.name = name;
        this.printMasterThreadWarning = printMasterThreadWarning;
        logger.info("Create ThreadPoolBlockingExecutor name={}, poolSize={}, queueSize={}. idleTime={}",
                getNameNotNull(), poolSize, queueSize, idleTimeBeforeShutdown);
        this.totalPermits = poolSize + queueSize;
        semaphore = new Semaphore(this.totalPermits);
        this.idleTimeBeforeShutdown = idleTimeBeforeShutdown;
        if (name != null) {
            this.threadFactory = createThreadFactory(name);
            this.setThreadFactory(this.threadFactory);
        }
    }

    public ThreadPoolBlockingExecutor(final int poolSize, final int queueSize, final long idleTimeBeforeShutdown, ThreadFactory threadFactory
            , boolean printMasterThreadWarning) {
        super(poolSize, poolSize, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>());
        this.queueSize = queueSize;
        this.printMasterThreadWarning = printMasterThreadWarning;
        logger.info("Create ThreadPoolBlockingExecutor poolSize={}, queueSize={}. idleTime={}", poolSize, queueSize, idleTimeBeforeShutdown);
        this.totalPermits = poolSize + queueSize;
        semaphore = new Semaphore(this.totalPermits);
        this.idleTimeBeforeShutdown = idleTimeBeforeShutdown;
        this.threadFactory = threadFactory;
        this.setThreadFactory(this.threadFactory);
    }

    private final static String threadNameLeteral = "thread-";
    private final static String threadNameFormat = "pool-%s-%d";

    private ThreadFactory createThreadFactory(String name) {
        defaultThreadFactory = this.getThreadFactory();
        ThreadFactory tf = new ThreadFactory() {
            @Override
            public Thread newThread(Runnable r) {
                Thread t = defaultThreadFactory.newThread(r);
                String oldNmae = t.getName();
                int strInx = oldNmae.indexOf(threadNameLeteral);
                if (strInx >= 0) {
                    String inxStr = oldNmae.substring(strInx + threadNameLeteral.length());
                    Integer inx = Integer.parseInt(oldNmae.substring(strInx + threadNameLeteral.length()));
                    if (inx != null) {
                        String newName = String.format(threadNameFormat, name, inx);
                        t.setName(newName);
                    }
                }
                return t;
            }
        };
        return tf;
    }

    @Override
    public void execute(final Runnable task) {
        logger.trace("Current {} queue size: {}", name, this.getQueue().size());
        boolean acquired = false;
        long threadId = Thread.currentThread().getId();
        if (masterThreadId == 0) {
            masterThreadId = threadId;
            masterThreadName = Thread.currentThread().getName();
        } else if (printMasterThreadWarning && masterThreadId != threadId) {
            logger.warn("Executing thread pool workers from multiple master threads. Master thread is {}/{} other is {} for executor {}",
                    masterThreadId, masterThreadName, threadId, getNameNotNull());
            masterThreadId = threadId;
            masterThreadName = Thread.currentThread().getName();
        }
        do {
            try {
                semaphore.acquire();
                acquired = true;
            } catch (final InterruptedException e) {
                logger.warn("InterruptedException whilst aquiring semaphore ({})", getNameNotNull(), e);
                throw new StoppedByUserException("Got an InterruptedException while waiting to execute task - stopping execution (" + getNameNotNull() + ")");
            }
        } while (!acquired);

        try {
            executionCounter++;
            super.execute(() -> {
                try {
                    task.run();
                } catch (Exception e) {
                    errorCounter++;
                    logger.error("Exception during worker thread execution({}) #{}. {}",
                            getNameNotNull(), errorCounter, e.getMessage(), e);
//                    throw new RuntimeException("Error during worker thread execution.", e);
                } catch (Error e) {
                    errorCounter++;
                    logger.error("Irrecoverable error during worker thread execution({}) #{}. {}",
                            getNameNotNull(), errorCounter, e.getMessage(), e);
                }
            });
        } catch (final RejectedExecutionException e) {
            semaphore.release();
            throw e;
        }
    }

    @Override
    protected void afterExecute(final Runnable r, final Throwable t) {
        super.afterExecute(r, t);
        lastExecutionTime = System.currentTimeMillis();
        semaphore.release();
        if (postRunProgressHandler != null) {
            try {
                postRunProgressHandler.accept(getBlockWaitProgressPercentage());
            } catch (Exception e) {
                logger.info("Error during postRunProgressHandler({}). {}", getNameNotNull(), e.getMessage(), e);
                postRunProgressHandler = null;
            }
        }
    }

    public long getLastExecutionTime() {
        return lastExecutionTime;
    }

    int getBlockWaitProgressPercentage() {
        return ((1000 * semaphore.availablePermits()) / totalPermits + 4) / 10;
    }

    @Override
    public void blockUntilProcessingFinished() {
        blockUntilProcessingFinished(null);
    }

    @Override
    public void blockUntilProcessingFinished(Consumer<Integer> progressHandler) {
        setPostRunHandler(null);
        long threadId = Thread.currentThread().getId();
        if (masterThreadId == 0) {
            logger.info("Blocking without any execution ({})", getNameNotNull());
        } else if (masterThreadId != threadId) {
            logger.warn("Blocking thread differ from executor. Other thread is {}/{} {}", masterThreadId, masterThreadName, getNameNotNull());
        }
        long diff = (System.currentTimeMillis() - lastExecutionTime) / 1000;
        boolean interruptedExceptionThrown = false;
        int internalQueueSize = this.getQueue().size();
        if (lastExecutionTime > 0) {
            int queueLength = semaphore.getQueueLength();
            int availablePermits = semaphore.availablePermits();
            logger.debug("({}) - Last execution was {} seconds ago." +
                            " Number of active-threads is {}. " +
                            "QueueSize={} S.QueueLength={} S.av={} IQ.size={}",
                    getNameNotNull(), diff, getActiveCount(), queueSize, queueLength, availablePermits, internalQueueSize);
        }
        while (waitToStartExecutions() || getActiveCount() > 0 || diff < idleTimeBeforeShutdown || internalQueueSize > 0) {
            if (waitToStartExecutions()) {
                if (executionCounter == 0) {
                    logger.debug("Nothing was executed here. No reason to wait ({})", getNameNotNull());
                    break;
                }
                logger.debug("Keep blocking since no execution started yet ({})", getNameNotNull());
            } else {
                int queueLength = semaphore.getQueueLength();
                int availablePermits = semaphore.availablePermits();
                internalQueueSize = this.getQueue().size();
                logger.debug("Keep blocking ({}) - Last execution was {} seconds ago." +
                                " Number of active-threads is {}. " +
                                "QueueSize={} S.QueueLength={} S.av={} IQ.size={}",
                        getNameNotNull(), diff, getActiveCount(), queueSize, queueLength, availablePermits, internalQueueSize);
            }
            diff = waitToStartExecutions() ? 0 : ((System.currentTimeMillis() - lastExecutionTime) / 1000);

            // Call progress reporting handler
            if (progressHandler != null) {
                progressHandler.accept(getBlockWaitProgressPercentage());
            }

            try {
                Thread.sleep(1000);
            } catch (final InterruptedException e) {
                logger.warn("Got an interrupted exception while blocking until processing finished ({})", getNameNotNull());
                int queueLength = semaphore.getQueueLength();
                int availablePermits = semaphore.availablePermits();
                internalQueueSize = this.getQueue().size();
                logger.debug("Keep blocking ({})  - Last execution was {} seconds ago." +
                                " Number of active-threads is {}. " +
                                "QueueSize={} S.QueueLength={} S.av={} IQ.size={}",
                        getNameNotNull(), diff, getActiveCount(), queueSize, queueLength, availablePermits, internalQueueSize);
                interruptedExceptionThrown = true;
                //Ignore it ;
            }
        }
        // Clear last using master thread
        masterThreadId = 0;
        masterThreadName = null;

        if (interruptedExceptionThrown) {
            // Clear execution start indications
            executionCounter = 0;
            lastExecutionTime = 0;
            throw new StoppedByUserException("Got an InterruptedException while waiting until processing is finished - stopping execution (" + getNameNotNull() + ")");
        }
        if (executionCounter > 0) {
            int queueLength = semaphore.getQueueLength();
            int availablePermits = semaphore.availablePermits();
            internalQueueSize = this.getQueue().size();
            logger.debug("Stop blocking ({}). Last execution was {} seconds ago." +
                            " Number of active-threads is {}. " +
                            "QueueSize={} S.QueueLength={} S.av={} IQ.size={}",
                    getNameNotNull(), diff, getActiveCount(), queueSize, queueLength, availablePermits, internalQueueSize);
        }
        // Clear execution start indications
        executionCounter = 0;
        lastExecutionTime = 0;
        // Call progress reporting handler
        if (progressHandler != null) {
            progressHandler.accept(100);
        }
//		shutdown();
    }

    // Allow progress reporting when task ends - parameter is progress percentage (0-100)
    // Progress handler is running in the context of the worker threads
    @Override
    public Consumer<Integer> setPostRunHandler(final Consumer<Integer> newHandler) {
        final Consumer<Integer> prevHandler = postRunProgressHandler;
        postRunProgressHandler = newHandler;
        return prevHandler;
    }

    private boolean waitToStartExecutions() {
        return lastExecutionTime == 0;
    }

    private String getNameNotNull() {
        return (name == null) ? "" : name;
    }

    @Override
    public int getErrorCounter() {
        return errorCounter;
    }

    @Override
    public String toString() {
        long diff = (System.currentTimeMillis() - lastExecutionTime) / 1000;
        int queueLength = semaphore.getQueueLength();
        int availablePermits = semaphore.availablePermits();
        int internalQueueSize = this.getQueue().size();
        StringBuilder stringBuilder = new StringBuilder("ThreadPoolBlocking Executor ");
        stringBuilder.append(", ").append(getNameNotNull())
                .append(" Last execution was ").append(diff).append("seconds ago.")
                .append(" Number of active-threads is ").append(getActiveCount())
                .append(" QueueSize=").append(queueSize)
                .append(" QueueLength=").append(queueLength)
                .append(" S.av=").append(availablePermits)
                .append(" IQ.size=").append(internalQueueSize);
        return stringBuilder.toString();
    }
}
