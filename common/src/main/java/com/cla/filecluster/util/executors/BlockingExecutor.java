package com.cla.filecluster.util.executors;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;


public abstract class BlockingExecutor  extends ThreadPoolExecutor{
	

	public BlockingExecutor(final int corePoolSize, final int maximumPoolSize, final long keepAliveTime, final TimeUnit unit,
			final BlockingQueue<Runnable> workQueue) {
		super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue);
	}

    public BlockingExecutor(final int corePoolSize, final int maximumPoolSize, final long keepAliveTime, final TimeUnit unit,
                            final BlockingQueue<Runnable> workQueue, ThreadFactory threadFactory) {
        super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue,threadFactory);
    }

    public abstract void blockUntilProcessingFinished();

    // Allow periodic progress reporting while blocking - parameter is progress percentage (0-100)
    public abstract void blockUntilProcessingFinished(Consumer<Integer> progressHandler);

    // Allow progress reporting when task ends - parameter is progress percentage (0-100)
    // Progress handler is running in the context of the worker threads
    public abstract Consumer<Integer> setPostRunHandler(Consumer<Integer> postRunHandler);

    public abstract int getErrorCounter();
}
