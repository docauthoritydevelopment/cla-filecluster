package com.cla.filecluster.domain.exceptions;

/**
 * Created by uri on 17/05/2016.
 */
public class StoppedByUserException extends RuntimeException {
    public StoppedByUserException(String message) {
        super(message);
    }

    public StoppedByUserException(Throwable cause) {
        super(cause);
    }

    public StoppedByUserException(String message, Throwable cause) {
        super(message, cause);
    }
}
