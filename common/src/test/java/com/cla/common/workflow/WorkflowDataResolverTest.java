package com.cla.common.workflow;

import com.cla.common.constants.SolrFieldOp;
import org.junit.Assert;
import org.junit.Test;

public class WorkflowDataResolverTest {

    @Test
    public void testAddNewWfData() {
        testResolveWfData(new String[]{},
                SolrFieldOp.ADD,
                "1.NEW.NONE",
                new String[]{"1.1.NEW.NONE"});
    }

    @Test
    public void testAddNewWfDataAdditionalFilterMatch() {
        testResolveWfData(new String[]{"1.1.NEW.NONE"},
                SolrFieldOp.ADD,
                "1.NEW.NONE",
                new String[]{"1.2.NEW.NONE"});
    }

    @Test
    public void testAddNewWfDataNewFilterPerm1() {
        testResolveWfData(new String[]{"1.1.NEW.NONE"},
                SolrFieldOp.ADD,
                "2.NEW.NONE",
                new String[]{"1.1.NEW.NONE", "2.1.NEW.NONE"});
    }

    @Test
    public void testAddNewWfDataNewFilterPerm2() {
        testResolveWfData(new String[]{"1.1.NEW.NONE", "2.1.NEW.NONE"},
                SolrFieldOp.ADD,
                "2.NEW.NONE",
                new String[]{"1.1.NEW.NONE", "2.2.NEW.NONE"});
    }

    @Test
    public void testRemoveWfDataHasOneFilterMatch() {
        testResolveWfData(new String[]{"1.1.NEW.NONE"},
                SolrFieldOp.REMOVE,
                "1",
                new String[]{});
    }

    @Test
    public void testRemoveWfDataHasTwoFilterMatch() {
        testResolveWfData(new String[]{"1.2.NEW.NONE"},
                SolrFieldOp.REMOVE,
                "1",
                new String[]{"1.1.NEW.NONE"});
    }

    @Test
    public void testRemoveWfDataHasTwoFiltersPerm1() {
        testResolveWfData(new String[]{"1.1.NEW.NONE", "2.1.NEW.NONE"},
                SolrFieldOp.REMOVE,
                "1",
                new String[]{"2.1.NEW.NONE"});
    }

    @Test
    public void testRemoveWfDataHasTwoFiltersPrem2() {
        testResolveWfData(new String[]{"1.2.NEW.NONE", "2.2.NEW.NONE"},
                SolrFieldOp.REMOVE,
                "2",
                new String[]{"1.2.NEW.NONE", "2.1.NEW.NONE"});
    }

    @Test
    public void testSetWfData() {
        testResolveWfData(new String[]{"1.2.NEW.NONE"},
                SolrFieldOp.SET,
                "1.DELETE.APPROVED",
                new String[]{"1.2.DELETE.APPROVED"});
    }


    private void testResolveWfData(String[] existingWfDataValues, SolrFieldOp solrFieldOp, String wfDataValue, String[] expectedWfDataValues) {
        String[] workflowDataValues = WorkflowDataResolver.resolveNewWorkflowDataValues(
                existingWfDataValues,
                solrFieldOp,
                wfDataValue);
        Assert.assertArrayEquals(expectedWfDataValues, workflowDataValues);
    }
}
