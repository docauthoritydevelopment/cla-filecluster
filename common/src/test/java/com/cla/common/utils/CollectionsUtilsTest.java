package com.cla.common.utils;

import com.google.common.collect.Lists;
import org.junit.Assert;
import org.junit.Test;

import java.util.Map;
import java.util.TreeMap;

import static org.junit.Assert.*;

public class CollectionsUtilsTest {

    @Test
    public void getSubMap() {
        TreeMap<Long, Long> map = new TreeMap<>();
        for (int i = 0; i < 500; i++) {
            map.put((long) i, (long) i);
        }

        Map<Long, Long> firstPage = CollectionsUtils.getSubMap(map, 0, 30);
        Assert.assertEquals(30, firstPage.size());
        Assert.assertTrue(containsRange(firstPage, 0L, 29L));

        Map<Long, Long> sndPage = CollectionsUtils.getSubMap(map, 30, 30);
        Assert.assertEquals(30, sndPage.size());
        Assert.assertTrue(containsRange(sndPage, 30L, 59L));

        Map<Long, Long> aPage = CollectionsUtils.getSubMap(map, 120, 30);
        Assert.assertEquals(30, aPage.size());
        Assert.assertTrue(containsRange(aPage, 120L, 149L));

        Map<Long, Long> lastPage = CollectionsUtils.getSubMap(map, 490, 30);
        Assert.assertEquals(10, lastPage.size());
        Assert.assertTrue(containsRange(lastPage, 490L, 499L));
    }

    private boolean containsRange(Map<Long, Long> map, long start, long end) {
        for (long i = start; i <= end; ++i) {
            if (!map.containsKey(i)) {
                return false;
            }
        }
        return true;
    }

    @Test
    public void testCompareAsSetCollection() {
        Assert.assertTrue(CollectionsUtils.compareAsSet(null, null));
        Assert.assertFalse(CollectionsUtils.compareAsSet(null, Lists.newArrayList()));
        Assert.assertTrue(CollectionsUtils.compareAsSet(Lists.newArrayList(), Lists.newArrayList()));
        Assert.assertTrue(CollectionsUtils.compareAsSet(Lists.newArrayList("aaa"), Lists.newArrayList("aaa")));
        Assert.assertFalse(CollectionsUtils.compareAsSet(Lists.newArrayList("bbb"), Lists.newArrayList("aaa")));
    }

    @Test
    public void testCompareAsSetArray() {
        Assert.assertTrue(CollectionsUtils.compareArrayAsSet(null, null));
        Assert.assertFalse(CollectionsUtils.compareArrayAsSet(null, new String[]{}));
        Assert.assertTrue(CollectionsUtils.compareArrayAsSet(new String[]{}, new String[]{}));
        Assert.assertTrue(CollectionsUtils.compareArrayAsSet(new String[]{"aaa"}, new String[]{"aaa"}));
        Assert.assertFalse(CollectionsUtils.compareArrayAsSet(new String[]{"aaa"}, new String[]{"bbb"}));
    }
}