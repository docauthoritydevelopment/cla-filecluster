package com.cla.common.utils;


import com.cla.common.constants.ContentMetadataFieldType;
import org.junit.Before;
import org.junit.Test;

import javax.xml.bind.JAXBException;

/**
 * Test ContentMetadataFieldType enum against the actual SOLR managed-schema and make sure all the fields are covered
 */
public class TestContentMetadataSchema extends TestSolrSchema<ContentMetadataFieldType>{

    private static final String SCHEMA_PATH = "/solr-cores-config/ContentMetadata/conf/managed-schema";

    @Override
    protected String getSchemaPath() {
        return SCHEMA_PATH;
    }

    @Override
    protected Class<ContentMetadataFieldType> getEnumClass() {
        return ContentMetadataFieldType.class;
    }

    @Before
    public void init() throws JAXBException{
        super.init();
    }

    @Test
    public void testSchemaAgainstEnum() throws JAXBException {
        super.testSchemaAgainstEnum();
    }

}
