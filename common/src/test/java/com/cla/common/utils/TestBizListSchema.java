package com.cla.common.utils;

import com.cla.common.constants.BizListFieldType;
import org.junit.Before;
import org.junit.Test;

import javax.xml.bind.JAXBException;

/**
 * Created by: yael
 * Created on: 7/2/2018
 */
public class TestBizListSchema extends TestSolrSchema<BizListFieldType> {

    private static final String SCHEMA_PATH = "/solr-cores-config/bizList/conf/managed-schema";

    @Override
    protected String getSchemaPath() {
        return SCHEMA_PATH;
    }

    @Override
    protected Class<BizListFieldType> getEnumClass() {
        return BizListFieldType.class;
    }

    @Before
    public void init() throws JAXBException {
        super.init();
    }

    @Test
    public void testSchemaAgainstEnum() throws JAXBException {
        super.testSchemaAgainstEnum();
    }
}
