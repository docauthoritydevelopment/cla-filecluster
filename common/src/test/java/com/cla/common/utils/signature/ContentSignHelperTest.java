package com.cla.common.utils.signature;

import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

import static org.junit.Assert.assertTrue;

/**
 * Created by: yael
 * Created on: 11/29/2017
 */
public class ContentSignHelperTest {

    @Test
    public void testSign() {
        ContentSignHelper instance = new ContentSignHelper(50);
        byte[] data = {0,0,0,0,0};
        String signature = instance.sign(data);
        assertTrue(signature.equals("oQkJws3K9a235rCSpPq6VYtivZY="));
    }

    @Test
    public void testReadAndSignBelowMax() throws Exception {
        ContentSignHelper instance = new ContentSignHelper(50);
        InputStream is = new InputStream() {
            @Override
            public int read() throws IOException {
                return 0;
            }
        };
        ContentAndSignature res = instance.readAndSign(is, 5, "dd.txt");
        assertTrue(res.contentLength() == 5);
        assertTrue(!res.contentReadFully());
        byte[] compare = {0,0,0,0,0};
        assertTrue(Arrays.equals(res.getContent(), compare));
        assertTrue(res.getSignature().equals("oQkJws3K9a235rCSpPq6VYtivZY="));
        assertTrue(res.hasContent());
        assertTrue(res.hasSignature());
    }

    @Test
    public void testReadAndSignAboveMax() throws Exception {
        ContentSignHelper instance = new ContentSignHelper(2);
        InputStream is = new InputStream() {
            @Override
            public int read() throws IOException {
                return 0;
            }
        };
        ContentAndSignature res = instance.readAndSign(is, 5, "dd.txt");
        assertTrue(res.contentLength() == 2);
        assertTrue(!res.contentReadFully());
        byte[] compare = {0,0};
        assertTrue(Arrays.equals(res.getContent(), compare));
        assertTrue(res.getSignature().equals("FIn5I8TcpykXiz4yM0WFUNjd3yk="));
        assertTrue(res.hasContent());
        assertTrue(res.hasSignature());

        instance.readRestOfFile(res, is, 5);
        assertTrue(res.contentLength() == 5);
        byte[] compareAll = {0,0,0,0,0};
        assertTrue(Arrays.equals(res.getContent(), compareAll));
        assertTrue(res.getSignature().equals("FIn5I8TcpykXiz4yM0WFUNjd3yk="));
    }
}
