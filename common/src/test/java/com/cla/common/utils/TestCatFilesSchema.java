package com.cla.common.utils;


import com.cla.common.constants.CatFileFieldType;
import org.junit.Before;
import org.junit.Test;

import javax.xml.bind.JAXBException;

/**
 * Test CatFileFieldType enum against the actual SOLR managed-schema and make sure all the fields are covered
 */
public class TestCatFilesSchema extends TestSolrSchema<CatFileFieldType>{

    private static final String SCHEMA_PATH = "/solr-cores-config/catFiles/conf/managed-schema";

    @Override
    protected String getSchemaPath() {
        return SCHEMA_PATH;
    }

    @Override
    protected Class<CatFileFieldType> getEnumClass() {
        return CatFileFieldType.class;
    }

    @Before
    public void init() throws JAXBException{
        super.init();
    }

    @Test
    public void testSchemaAgainstEnum() throws JAXBException {
        super.testSchemaAgainstEnum();
    }

}
