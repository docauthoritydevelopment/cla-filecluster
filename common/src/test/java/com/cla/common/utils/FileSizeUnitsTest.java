package com.cla.common.utils;

import org.junit.Test;

import static com.cla.common.utils.FileSizeUnits.ROUND_DOWN;
import static com.cla.common.utils.FileSizeUnits.ROUND_UP;
import static org.junit.Assert.assertTrue;

public class FileSizeUnitsTest {

    @Test
    public void testToUnit() {
        assertTrue(FileSizeUnits.BYTE.toMegabytes(1000000) == 1);
        assertTrue(FileSizeUnits.KB.toBytes(10) == 10240);
        assertTrue(FileSizeUnits.KB.toMegabytes(50000) == 49); // result is rounded -> actually around 48.8
        assertTrue(FileSizeUnits.MB.toBytes(10) == 10485760);
        assertTrue(FileSizeUnits.MB.toKilobytes(10) == 10240);
        assertTrue(FileSizeUnits.MB.toGigabytes(100000) == 98); // result is rounded -> actually around 97.6
        assertTrue(FileSizeUnits.GB.toKilobytes(10) == 10485760);
    }

    @Test
    public void testRoundUnit() {
        assertTrue(FileSizeUnits.KB.toMegabytes(50000, ROUND_UP) == 49); // result is rounded -> actually around 48.8
        assertTrue(FileSizeUnits.KB.toMegabytes(50000, ROUND_DOWN) == 48);
        assertTrue(FileSizeUnits.KB.toMegabytes(50000) == 49);
        assertTrue(FileSizeUnits.MB.toGigabytes(100000, ROUND_UP) == 98); // result is rounded -> actually around 97.6
        assertTrue(FileSizeUnits.MB.toGigabytes(100000, ROUND_DOWN) == 97);
    }

    @Test
    public void testRoundScaleUnit() {
        assertTrue(FileSizeUnits.BYTE.toKilobytes(50000, FileSizeUnits.ROUND,2) == 48.83);
        assertTrue(FileSizeUnits.BYTE.toKilobytes(50000, FileSizeUnits.ROUND,1) == 48.8);
        assertTrue(FileSizeUnits.BYTE.toKilobytes(50000, FileSizeUnits.ROUND,0) == 49);
        assertTrue(FileSizeUnits.BYTE.toKilobytes(50000, FileSizeUnits.ROUND_UP,0) == 49);
        assertTrue(FileSizeUnits.BYTE.toKilobytes(50000, FileSizeUnits.ROUND_DOWN,0) == 48);

        assertTrue(FileSizeUnits.KB.toMegabytes(50000, FileSizeUnits.ROUND_DOWN,0) == 48);
        assertTrue(FileSizeUnits.MB.toGigabytes(50000, FileSizeUnits.ROUND_DOWN,0) == 48);
    }
}
