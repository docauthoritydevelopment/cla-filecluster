package com.cla.common.utils;

import com.google.common.cache.CacheLoader;
import com.google.common.collect.Maps;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.Assert.*;

/**
 * Persistable safe cache tests
 * Created by vladi on 5/18/2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class PersistableSafeCacheTest {

    private Map<Integer, String> database;
    private int loadCounter = 0;

    @Before
    public void init(){
        loadCounter = 0;

        database = Maps.newHashMap();
        database.put(1, "Apple");
        database.put(2, "Orange");
        database.put(3, "Guava");
        database.put(4, "Kiwi");
        database.put(5, "Pear");

    }

    @Test
    public void testLoader(){
        PersistableSafeCache<Integer, String> cache = setupCache();
        String val1 = cache.getForRead(1);
        String val2 = cache.getForRead(2);
        assertEquals("Apple", val1);
        assertEquals("Orange", val2);
        assertEquals(2, loadCounter);
        cache.getForRead(1);
        assertEquals(2, loadCounter);
    }

    @Test
    public void testEvict(){
        PersistableSafeCache<Integer, String> cache = setupCache();
        cache.getForRead(1);
        cache.getForRead(2);
        cache.getForRead(2);
        cache.getForRead(3);
        assertEquals(3, loadCounter);

        cache.evict(1);
        cache.getForRead(1);
        assertEquals(4, loadCounter);

        cache.getForRead(1);
        assertEquals(4, loadCounter);

        cache.evictBy(x -> x.equals("Guava"));
        cache.getForRead(3);
        assertEquals(5, loadCounter);

        cache.evictAll();
        cache.getForRead(1);
        cache.getForRead(2);
        cache.getForRead(2);
        cache.getForRead(3);
        assertEquals(8, loadCounter);
    }


    private PersistableSafeCache<Integer, String> setupCache(){
        return PersistableSafeCache.Builder
                .create(Integer.class, String.class)
                .maximumSize(200)
                .setLoader(new CacheLoader<Integer, String>() {
                    @Override
                    public String load(Integer key) throws Exception {
                        loadCounter++;
                        return database.get(key);
                    }
                })
                .setPersister(this::persister)
                .build();
    }

    private void persister(Integer key, String value){
        database.put(key, value);
    }

    @Test
    public void testPersistOnEvict() {

        testPersistCounter = new AtomicInteger();

        PersistableSafeCache<Long, Long> testCache = PersistableSafeCache.Builder
                .create(Long.class, Long.class)
                .maximumSize(2)
                .setLoader(new Loader())
                .setPersister((key, value) -> updateKey(value))
                .build();

        testCache.getForRead(1L);
        testCache.getForRead(2L);
        testCache.lockForUpdate(2L);
        testCache.unlockUpdate(2L);
        testCache.getForRead(3L);
        testCache.getForRead(4L);

        assertEquals(1, testPersistCounter.get());

        testCache.lockForUpdate(3L);
        testCache.unlockUpdate(3L);
        testCache.getForRead(5L);
        testCache.getForRead(6L);

        assertEquals(2, testPersistCounter.get());

        testPersistCounter = null;
    }

    private class Loader extends CacheLoader<Long, Long> {
        public Long load(Long key) {
            return 7L;
        }
    }

    private void updateKey(Long key) {
        if (testPersistCounter != null) {
            testPersistCounter.incrementAndGet();
        }
    }

    private AtomicInteger testPersistCounter = null;
}
