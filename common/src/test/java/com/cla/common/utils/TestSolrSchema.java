package com.cla.common.utils;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.constants.SolrCoreSchema;
import com.google.common.collect.Maps;
import org.junit.Assert;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.File;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Map;

public abstract class TestSolrSchema<T extends Enum<T>> {

    private static final String PROJECT_PREFIX = "..";

    private ManagedSchema schema;

    protected abstract String getSchemaPath();

    protected abstract Class<T> getEnumClass();

    public void init() throws JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance(ManagedSchema.class);
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        File file = new File(PROJECT_PREFIX + getSchemaPath());

        schema = (ManagedSchema) jaxbUnmarshaller.unmarshal(file);
    }

    protected void testSchemaAgainstEnum() throws JAXBException {
        Map<String, SolrCoreSchema> enumMap = Maps.newHashMap();
        Enum.valueOf(getEnumClass(), "ID");
        T[] values = getEnumClass().getEnumConstants();
        for (T entry : values){
            enumMap.put(((SolrCoreSchema)entry).getSolrName(), (SolrCoreSchema)entry);
        }

        for (Field field : schema.getFields()){
            if (field.getName().startsWith("_") || field.getName().equals("id")){
                continue;
            }
            SolrCoreSchema enumEntry = enumMap.get(field.getName());
            Assert.assertNotNull("No corresponding enum field [" + field.getName() + "]", enumEntry);
            switch(field.getType()){
                case "plong":
                    if (enumEntry instanceof CatFileFieldType && CatFileFieldType.ROOT_FOLDER_NAME_LIKE == enumEntry) {
                        // ignore this, we get the ids from db but the parameter is str
                    } else {
                        assertTypeMatch(field, Long.class, enumEntry);
                    }
                    break;
                case "daString":
                case "string":
                case "string_ci":
                case "text_general":
                case "text_exact":
                    assertTypeMatch(field, String.class, enumEntry);
                    break;
                case "pdate":
                    Assert.assertTrue("Type mismatch for field " + field.getName(),
                            LocalDateTime.class.equals(enumEntry.getType()) ||
                                    Date.class.equals(enumEntry.getType()));
                    break;
                case "pint":
                    assertTypeMatch(field, Integer.class, enumEntry);
                    break;
                case "boolean":
                    assertTypeMatch(field, Boolean.class, enumEntry);
                    break;
                default:
                    Assert.fail("Unrecognized type " + field.getType() + " for field " + field.getName());
            }
        }
    }

    private void assertTypeMatch(Field field, Class expectedType, SolrCoreSchema enumEntry){
        Assert.assertEquals("Type mismatch for field " + field.getName(), expectedType, enumEntry.getFieldType());
    }

    @SuppressWarnings("unused")
    @XmlRootElement(name = "schema")
    public static class ManagedSchema{

        private List<Field> fields;

        @XmlElement(name = "field")
        public List<Field> getFields() {
            return fields;
        }

        public void setFields(List<Field> fields) {
            this.fields = fields;
        }
    }

    @SuppressWarnings("unused")
    public static class Field{
        private String name;
        private String type;
        private Boolean indexed;
        private Boolean stored;
        private Boolean required;
        private Boolean multiValued;

        @XmlAttribute
        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        @XmlAttribute
        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        @XmlAttribute
        public Boolean getIndexed() {
            return indexed;
        }

        public void setIndexed(Boolean indexed) {
            this.indexed = indexed;
        }

        @XmlAttribute
        public Boolean getStored() {
            return stored;
        }

        public void setStored(Boolean stored) {
            this.stored = stored;
        }

        @XmlAttribute
        public Boolean getRequired() {
            return required;
        }

        public void setRequired(Boolean required) {
            this.required = required;
        }

        @XmlAttribute
        public Boolean getMultiValued() {
            return multiValued;
        }

        public void setMultiValued(Boolean multiValued) {
            this.multiValued = multiValued;
        }
    }
}
