package com.cla.common.utils;

import com.cla.common.constants.CatFoldersFieldType;
import org.junit.Before;
import org.junit.Test;

import javax.xml.bind.JAXBException;

public class TestCatFoldersSchema extends TestSolrSchema<CatFoldersFieldType>{

    private static final String SCHEMA_PATH = "/solr-cores-config/catFolders/conf/managed-schema";

    @Override
    protected String getSchemaPath() {
        return SCHEMA_PATH;
    }

    @Override
    protected Class<CatFoldersFieldType> getEnumClass() {
        return CatFoldersFieldType.class;
    }

    @Before
    public void init() throws JAXBException {
        super.init();
    }

    @Test
    public void testSchemaAgainstEnum() throws JAXBException {
        super.testSchemaAgainstEnum();
    }
}
