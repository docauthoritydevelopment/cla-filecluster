package com.cla.common.utils;

import com.cla.common.domain.dto.filetree.RsaKey;
import org.junit.Assert;
import org.junit.Test;

import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Arrays;

/**
 * Test of EncryptionUtils class
 * Created by yael on 11/9/2017.
 */
public class EncryptionUtilsTest {

    @Test
    public void testCreateKeys() throws Exception {
        EncryptionUtils.generateNewKeys();
    }

    @Test
    public void testCreateKeyEncryptAndDecrypt() throws Exception {
        KeyPair pair = EncryptionUtils.generateNewKeys();
        PublicKey pub = pair.getPublic();
        PrivateKey priv = pair.getPrivate();

        String str = "toenc";
        byte[] toEncode = str.getBytes();

        byte[] res = EncryptionUtils.encryptUsingPublicKey(pub, toEncode);
        byte[] res2 = EncryptionUtils.decryptUsingPrivateKey(priv, res);

        Assert.assertTrue(Arrays.equals(toEncode, res2));
        Assert.assertTrue(!Arrays.equals(toEncode, res));
        Assert.assertTrue(str.equals(new String(res2, "UTF-8")));
    }

    @Test
    public void testCreateKeyEncryptAndDecryptKeyStr() throws Exception {

        RsaKey keys = EncryptionUtils.generateNewKeysComplete();

        String str = "toenc";

        String res = EncryptionUtils.encryptUsingPublicKeyStr(keys.getPublicKey(), str);
        String res2 = EncryptionUtils.decryptUsingPrivateKeyStr(keys.getPrivateKey(), res);

        Assert.assertTrue(str.equals(res2));
        Assert.assertTrue(!str.equals(res));
    }

    @Test
    public void testCreateKeyEncryptAndDecryptKeyStrLong() throws Exception {

        RsaKey keys = EncryptionUtils.generateNewKeysComplete();

        String str = "toenctoenctoenctoenctoenctoenctoenctoenctoenctoenctoenctoenc";

        String res = EncryptionUtils.encryptUsingPublicKeyStr(keys.getPublicKey(), str);
        String res2 = EncryptionUtils.decryptUsingPrivateKeyStr(keys.getPrivateKey(), res);

        Assert.assertTrue(str.equals(res2));
        Assert.assertTrue(!str.equals(res));
    }

    @Test
    public void testCertificateToAndFromString() throws Exception {
        RsaKey keys = EncryptionUtils.generateNewKeysComplete();
        EncryptionUtils.getCertificateFromString(keys.getCertificate());
    }
}
