package com.cla.common.utils;

import org.junit.Test;

import static org.junit.Assert.*;

public class PatternSearcherTest {

    @Test
    public void testLimitMatchCount() {
        PatternSearcher patternSearcher = new PatternSearcher(true, true, null);

        patternSearcher.setMaxSingleSearchPatternMatches(50);
        patternSearcher.setSearchPatternStopSearchAfterMulti(false);
        patternSearcher.setSearchPatternCountThresholdMulti(10);
        assertTrue(50 == patternSearcher.calcActualLimitForPattern());

        patternSearcher.setMaxSingleSearchPatternMatches(50);
        patternSearcher.setSearchPatternStopSearchAfterMulti(false);
        patternSearcher.setSearchPatternCountThresholdMulti(70);
        assertTrue(70 == patternSearcher.calcActualLimitForPattern());

        patternSearcher.setMaxSingleSearchPatternMatches(50);
        patternSearcher.setSearchPatternStopSearchAfterMulti(false);
        patternSearcher.setSearchPatternCountThresholdMulti(null);
        assertTrue(50 == patternSearcher.calcActualLimitForPattern());

        patternSearcher.setMaxSingleSearchPatternMatches(null);
        patternSearcher.setSearchPatternStopSearchAfterMulti(false);
        patternSearcher.setSearchPatternCountThresholdMulti(70);
        assertTrue(null == patternSearcher.calcActualLimitForPattern());

        patternSearcher.setMaxSingleSearchPatternMatches(50);
        patternSearcher.setSearchPatternStopSearchAfterMulti(true);
        patternSearcher.setSearchPatternCountThresholdMulti(10);
        assertTrue(10 == patternSearcher.calcActualLimitForPattern());

        patternSearcher.setMaxSingleSearchPatternMatches(5);
        patternSearcher.setSearchPatternStopSearchAfterMulti(true);
        patternSearcher.setSearchPatternCountThresholdMulti(10);
        assertTrue(10 == patternSearcher.calcActualLimitForPattern());

        patternSearcher.setMaxSingleSearchPatternMatches(5);
        patternSearcher.setSearchPatternStopSearchAfterMulti(true);
        patternSearcher.setSearchPatternCountThresholdMulti(null);
        assertTrue(5 == patternSearcher.calcActualLimitForPattern());
    }
}