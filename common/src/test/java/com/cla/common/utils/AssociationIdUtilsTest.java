package com.cla.common.utils;

import com.cla.common.domain.dto.bizlist.BizListItemType;
import com.cla.common.domain.dto.bizlist.SimpleBizListItemDto;
import com.cla.common.domain.dto.filetag.*;
import com.cla.common.domain.dto.security.FunctionalRoleDto;
import com.cla.common.tag.priority.TagPriority;
import org.junit.Test;

import static org.junit.Assert.*;

public class AssociationIdUtilsTest {

    @Test
    public void testTagIdentifier() {
        FileTagTypeDto type = new FileTagTypeDto();
        type.setId(1);
        FileTagDto tag = new FileTagDto();
        tag.setId(2L);
        tag.setType(type);
        tag.setFileTagAction(FileTagAction.MANUAL);
        String id = AssociationIdUtils.createTagIdentifier(tag);
        assertEquals("1.2.MANUAL", id);

        tag.setFileTagAction(FileTagAction.SUGGESTION);
        id = AssociationIdUtils.createTagIdentifier(tag);
        assertEquals("1.2.SUGGESTION", id);

        id = AssociationIdUtils.createPriorityScopedTagIdentifier(tag, null, TagPriority.FILE);
        assertEquals("g.1.2.SUGGESTION.file", id);

        id = AssociationIdUtils.createPriorityScopedTagIdentifier(tag, null, TagPriority.folder(0));
        assertEquals("g.1.2.SUGGESTION.f0", id);

        id = AssociationIdUtils.createPriorityScopedTagIdentifier(tag, null, TagPriority.FILE_GROUP);
        assertEquals("g.1.2.SUGGESTION.fileGroup", id);

        id = AssociationIdUtils.createPriorityScopedTagIdentifier(tag, 5L, TagPriority.FILE_GROUP);
        assertEquals("5.1.2.SUGGESTION.fileGroup", id);

        id = AssociationIdUtils.createViaDocTypePriorityScopedTagIdentifier(tag, null, TagPriority.FILE_GROUP);
        assertEquals("g.1.2.SUGGESTION.vdt.fileGroup", id);

        id = AssociationIdUtils.createTag2Identifier(tag);
        assertEquals("type.1 tag.2.SUGGESTION", id);

        id = AssociationIdUtils.createScopedTagIdentifier(tag, null);
        assertEquals("g.1.2.SUGGESTION", id);
    }

    @Test
    public void testFuncRoleIdentifier() {
        String id = AssociationIdUtils.createFunctionalRoleIdentfier(8L);
        assertEquals("frole.8", id);

        FunctionalRoleDto fr = new FunctionalRoleDto();
        fr.setId(8L);

        id = AssociationIdUtils.createFunctionalRoleIdentfier(fr);
        assertEquals("frole.8", id);

        id = AssociationIdUtils.createFunctionalRoleIdentfier(fr, TagPriority.FILE_GROUP);
        assertEquals("frole.8.fileGroup", id);
    }

    @Test
    public void testBizListClientIdentifier() {
        SimpleBizListItemDto simpleBizListItem = new SimpleBizListItemDto();
        simpleBizListItem.setId("sid");
        simpleBizListItem.setType(BizListItemType.CLIENTS);
        simpleBizListItem.setBizListId(5L);

        String id = AssociationIdUtils.createSimpleBizListItemIdentfier(simpleBizListItem);
        assertEquals("bli.1.5.sid", id);

        id = AssociationIdUtils.createSimpleBizListItemIdentfier(simpleBizListItem, TagPriority.folder(0));
        assertEquals("bli.1.5.sid.f0", id);
    }

}