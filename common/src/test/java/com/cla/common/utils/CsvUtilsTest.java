package com.cla.common.utils;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;

import static org.junit.Assert.assertTrue;

public class CsvUtilsTest {

    @Test
    public void testNotNullString() {
        assertTrue(CsvUtils.notNull((String)null).equals(""));
        assertTrue(CsvUtils.notNull("").equals("\"\""));
        assertTrue(CsvUtils.notNull("asfafff").equals("\"asfafff\""));
    }

    @Test
    public void testNotNullDate() {
        assertTrue(CsvUtils.notNull((Date)null).equals(""));
        Date d = new Date();
        assertTrue(CsvUtils.notNull(d).equals(d.toString()));
    }

    @Test
    public void testNotNullCollection() {
        assertTrue(CsvUtils.notNull((Collection<String>)null).equals(Collections.emptyList()));
        Collection<String> c = new ArrayList<>();
        assertTrue(CsvUtils.notNull(c).equals(c));
    }
}
