package com.cla.common.utils.signature;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by: yael
 * Created on: 11/29/2017
 */
public class SigningStringBuilderTest {

    @Test
    public void testAppend() {
        SigningStringBuilder instance = SigningStringBuilder.create();
        instance.append("dfgdfgd");
        assertTrue(instance.toString().equals("dfgdfgd"));
        instance.append("eee");
        assertTrue(instance.toString().equals("dfgdfgdeee"));
        instance.append("12345678",2,4);
        assertTrue(instance.toString().equals("dfgdfgdeee34"));
        instance.append('c');
        assertTrue(instance.toString().equals("dfgdfgdeee34c"));
    }

    @Test
    public void testDigest() throws Exception {
        SigningStringBuilder instance = SigningStringBuilder.create();
        instance.append("dfgdfgdeee34c");
        byte[] digest = instance.getSignature();
        byte[] compare = {89, 74, -63, 107, -33, -107, -22, -89, 40, 39, -122, 17, 14, 104, -108, -17, -47, -49, -13, -75};
        assertTrue(Arrays.equals(digest, compare));
        digest = instance.getSignature();
        String digest64 = instance.getSignatureAsBase64();
        assertTrue(digest64.equals("WUrBa9+V6qcoJ4YRDmiU79HP87U="));
        byte[] res = Base64.decodeBase64(digest64);
        assertTrue(Arrays.equals(res, digest));
    }

    @Test(expected = IllegalStateException.class)
    public void testAppendAfterDigest() {
        SigningStringBuilder instance = SigningStringBuilder.create();
        instance.append("dfgdfgdeee34c");
        instance.getSignature();
        instance.append("dfgdfgdeee34c");
    }

    @Test
    public void testSigningDisabled() {
        SigningStringBuilder instance = SigningStringBuilder.create(false);
        instance.append("dfgdfgdeee34c");
        assertTrue(instance.getSignature() == null);
        assertTrue(instance.getSignatureAsBase64() == null);
    }

    @Test
    public void testGetSigNoAppend() {
        SigningStringBuilder instance = SigningStringBuilder.create();
        byte[] compare = {-38, 57, -93, -18, 94, 107, 75, 13, 50, 85, -65, -17, -107, 96, 24, -112, -81, -40, 7, 9};
        assertTrue(Arrays.equals(instance.getSignature(), compare));
        assertEquals("2jmj7l5rSw0yVb/vlWAYkK/YBwk=", instance.getSignatureAsBase64());
    }

    @Test
    public void testDigestSettingMD5() {
        SigningStringBuilder instance = SigningStringBuilder.create(DigestUtils.getMd5Digest());
        instance.append("dfgdfgdeee34c");
        byte[] digest = instance.getSignature();
        byte[] compare = {-64, -55, 77, 88, -126, 22, 8, 57, 110, 98, 79, 34, 17, -105, 114, -78};
        assertTrue(Arrays.equals(digest, compare));
        String digest64 = instance.getSignatureAsBase64();
        assertTrue(digest64.equals("wMlNWIIWCDluYk8iEZdysg=="));
    }
}
