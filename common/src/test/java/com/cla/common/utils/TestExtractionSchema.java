package com.cla.common.utils;

import com.cla.common.constants.ExtractionFieldType;
import org.junit.Before;
import org.junit.Test;

import javax.xml.bind.JAXBException;

public class TestExtractionSchema extends TestSolrSchema<ExtractionFieldType>{

    private static final String SCHEMA_PATH = "/solr-cores-config/Extraction/conf/managed-schema";

    @Override
    protected String getSchemaPath() {
        return SCHEMA_PATH;
    }

    @Override
    protected Class<ExtractionFieldType> getEnumClass() {
        return ExtractionFieldType.class;
    }

    @Before
    public void init() throws JAXBException {
        super.init();
    }

    @Test
    public void testSchemaAgainstEnum() throws JAXBException {
        super.testSchemaAgainstEnum();
    }

}
