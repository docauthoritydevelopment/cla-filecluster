package com.cla.common.domain.dto.word;

import com.cla.common.domain.dto.histogram.ClaHistogramCollection;
import com.cla.common.domain.dto.mediaproc.word.WordFile;
import com.cla.common.domain.dto.pv.PVType;
import com.cla.common.utils.tokens.DocAuthorityTokenizer;
import com.cla.common.utils.tokens.HashedTokensConsumer;
import com.cla.common.utils.tokens.TokenizerObject;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertTrue;

/**
 * Created by: yael
 * Created on: 12/3/2017
 */
public class WordFilesSimilarityTest {

    @Before
    public void init() {
        WordFile.setNGramTokenizer(new DocAuthorityTokenizer() {
            @Override
            public long MD5Long(String item) {
                return 55;
            }

            @Override
            public String getHashReuseTokenizer(String text, HashedTokensConsumer hashedTokensConsumer, String locationDesc, TokenizerObject tokenizerObject) {
                return text;
            }

            @Override
            public TokenizerObject getTokenizerObject() {
                return null;
            }

            @Override
            public List<Long> getHashlist(String t, int titleNgramSize) {
                return null;
            }

            @Override
            public Map<Long, String> getDebugHashMapper() {
                return null;
            }

            @Override
            public void setDebugHashMapper(Map<Long, String> debugHashMapper) {

            }
        });
    }

    @Test
    public void testFile() {
        WordFile a = new WordFile();
        a.histogramAddItem(PVType.PV_BodyNgrams, "66");
        a.histogramAddItem(PVType.PV_BodyNgrams, "66");
        WordFile b = new WordFile();
        b.histogramAddItem(PVType.PV_BodyNgrams, "66");
        b.histogramAddItem(PVType.PV_BodyNgrams, "55");
        final WordFilesSimilarity wfs = new WordFilesSimilarity(a, b);
        assertTrue(1 == wfs.getSimilarityValue(PVType.PV_BodyNgrams));
        assertTrue(1 == wfs.getPotentialSimilarityRate());
        assertTrue(wfs.passThreshold(0.1f, 0.05f, 0.2f));
    }

    @Test
    public void testHistograms() {
        Map<String, ClaHistogramCollection<Long>> fileAHistograms = new HashMap<>();
        ClaHistogramCollection<Long> a = new ClaHistogramCollection<>();
        a.addItem(5L);
        a.addItem(5L);
        a.addItem(27L);
        fileAHistograms.put("PV_BodyNgrams", a);
        Map<String, ClaHistogramCollection<Long>> fileBHistograms = new HashMap<>();
        ClaHistogramCollection<Long> b = new ClaHistogramCollection<>();
        b.addItem(7L);
        b.addItem(5L);
        fileBHistograms.put("PV_BodyNgrams", b);
        final WordFilesSimilarity wfs = new WordFilesSimilarity(fileAHistograms, fileBHistograms);
        assertTrue(0.5 == wfs.getSimilarityValue(PVType.PV_BodyNgrams));
        assertTrue(0.5 == wfs.getPotentialSimilarityRate());
        assertTrue(wfs.passThreshold(0.1f, 0.05f, 0.2f));
    }

    @Test
    public void testMultiHistograms() {
        Map<String, ClaHistogramCollection<Long>> fileAHistograms = new HashMap<>();
        ClaHistogramCollection<Long> a = new ClaHistogramCollection<>();
        a.addItem(5L);
        a.addItem(5L);
        a.addItem(27L);
        ClaHistogramCollection<Long> a2 = new ClaHistogramCollection<>();
        a2.addItem(27L);
        a2.addItem(27L);
        fileAHistograms.put("PV_BodyNgrams", a);
        fileAHistograms.put("PV_Headings", a2);
        Map<String, ClaHistogramCollection<Long>> fileBHistograms = new HashMap<>();
        ClaHistogramCollection<Long> b = new ClaHistogramCollection<>();
        b.addItem(7L);
        b.addItem(5L);
        ClaHistogramCollection<Long> b2 = new ClaHistogramCollection<>();
        b2.addItem(27L);
        b2.addItem(27L);
        b2.addItem(7L);
        fileBHistograms.put("PV_BodyNgrams", b);
        fileBHistograms.put("PV_Headings", b2);
        final WordFilesSimilarity wfs = new WordFilesSimilarity(fileAHistograms, fileBHistograms);
        assertTrue(0.5 == wfs.getSimilarityValue(PVType.PV_BodyNgrams));
        assertTrue(0.7 > wfs.getPotentialSimilarityRate() && 0.6 < wfs.getPotentialSimilarityRate());
        assertTrue(0.7 > wfs.getSimilarityValue(PVType.PV_Headings) && 0.6 < wfs.getSimilarityValue(PVType.PV_Headings));
        assertTrue(wfs.passThreshold(0.1f, 0.05f, 0.2f));
    }
}
