package com.cla.common.domain.dto.filesearchpatterns;

import org.junit.Test;

import java.util.function.Predicate;
import java.util.regex.Pattern;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by: yael
 * Created on: 11/29/2017
 */
public class VisaCardTextSearchPatternTest {

    @Test
    public void test() {
        VisaCardTextSearchPattern ccp = new VisaCardTextSearchPattern(5, "name");

        Pattern pattern = ccp.getCompiledPattern();

        assertFalse(pattern.matcher("ffff").find());
        assertFalse(pattern.matcher("5631111111111111").find());
        assertTrue(pattern.matcher("4444333322221111").find());
        assertTrue(pattern.matcher("4111111111111111").find());
        assertTrue(pattern.matcher("asdasda /n/t dsfsdf 4111111111111111 sdgsdgf").find());

        Predicate<String> p =  ccp.getPredicate();
        assertFalse(p.test("ffff"));
        assertFalse(p.test("2353"));
        assertFalse(p.test("3454 5676 5676 6754"));
        assertFalse(p.test("4454567656766754"));
        assertTrue(p.test("4444333322221111"));
        assertTrue(p.test("4111111111111111"));
    }
}
