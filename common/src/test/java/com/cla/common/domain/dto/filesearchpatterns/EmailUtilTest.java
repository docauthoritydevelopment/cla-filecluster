package com.cla.common.domain.dto.filesearchpatterns;

import org.junit.Test;

import java.util.function.Predicate;
import java.util.regex.Pattern;
import com.cla.common.utils.EmailUtils;

import static org.junit.Assert.*;

/**
 * Created by: Itay
 * Created on: 24/9/2019
 */
public class EmailUtilTest {
    @Test
    public void test() {

        assertEquals(EmailUtils.getEmailAddressValue("/o=Organisation/ou=Administrative Group/cn=Recipients/cn=tester@mydomain.com", true, true),
                "tester@mydomain.com");
        assertEquals(EmailUtils.getEmailAddressValue("/o=Organisation/ou=Administrative Group/cn=Recipients/cn=tester@mydomain.comdef", true, true),
                "tester@mydomain.com");
        assertEquals(EmailUtils.getEmailAddressValue("tester@mydomain.com", true, true),
                "tester@mydomain.com");

        assertEquals(EmailUtils.extractDomainFromEmailAddress("/o=Organisation/ou=Administrative Group/cn=Recipients/cn=tester@mydomain.com", true, true),
                "mydomain.com");
        assertEquals(EmailUtils.extractDomainFromEmailAddress("/o=Organisation/ou=Administrative Group/cn=Recipients/cn=tester@mydomain.comdef", true, true),
                "mydomain.com");
        assertEquals(EmailUtils.extractDomainFromEmailAddress("tester@mydomain.com", true, true),
                "mydomain.com");

        assertEquals(EmailUtils.getEmailAddressValue("/o=Organisation/ou=Administrative Group/cn=Recipients/cn=tester@mydomain.com", false, false),
                "/o=Organisation/ou=Administrative Group/cn=Recipients/cn=tester@mydomain.com".toLowerCase());
        assertEquals(EmailUtils.getEmailAddressValue("/o=Organisation/ou=Administrative Group/cn=Recipients/cn=tester@mydomain.comdef", false, false),
                "/o=Organisation/ou=Administrative Group/cn=Recipients/cn=tester@mydomain.comdef".toLowerCase());
        assertEquals(EmailUtils.getEmailAddressValue("tester@mydomain.com", false, false),
                "tester@mydomain.com");
        assertEquals(EmailUtils.getEmailAddressValue("/o=enron/ou=na/cn=recipients/cn=dbracke", true, false),
                "/o=enron/ou=na/cn=recipients/cn=dbracke");
        assertEquals(EmailUtils.getEmailAddressValue("/o=enron/ou=na/cn=recipients/cn=dbracke", false, false),
                "/o=enron/ou=na/cn=recipients/cn=dbracke");
        assertEquals(EmailUtils.getEmailAddressValue("/o=enron/ou=na/cn=recipients/cn=dbracke", false, true),
                "/o=enron/ou=na/cn=recipients/cn=dbracke");
        assertEquals(EmailUtils.getEmailAddressValue("/o=enron/ou=na/cn=recipients/cn=dbracke", true, true),
                "dbracke");

    }
}
