package com.cla.common.domain.dto.histogram;

import org.junit.Test;

import java.awt.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by: yael
 * Created on: 12/3/2017
 */
public class ClaStyleTest {

    @Test
    public void testStatic() {
        ClaStyle.setFontSizeLookup("10:12,11:12,13:12");
        ClaStyle.setColorLookupParams(2, 2);
        assertEquals(192, ClaStyle.translateColor(500));
        assertEquals(4644, ClaStyle.convertColorComponent((short)4644, 6.99f, 67));
        assertEquals(252, ClaStyle.convertColorComponent((short)555, 0.99f, 6));
        assertEquals(40, ClaStyle.translateColor(555, 0.99f, 6));
    }

    @Test
    public void testInstanceFontSizeChangeByMap() {
        ClaStyle.setFontSizeLookup("10:12,11:12,13:12");
        final ClaStyle cStyle = new ClaStyle("", "ariel", 11, Color.BLACK.getRGB(), true, false, 0 /*@@@*/);
        cStyle.setJustification(ClaStyle.Justification.LEFT_JUST);
        assertEquals("arielS12C0FI0R0L0J0pB0pA0B", cStyle.getSimpleSignature());
    }

    @Test
    public void testInstance() {
        final ClaStyle cStyle = new ClaStyle("", "ariel", 14, Color.BLACK.getRGB(), true, false, 0 /*@@@*/);
        cStyle.setFirstLineIndent((short) 0);
        cStyle.setHeadingLevel(1);
        cStyle.setIndentFromLeft((short) (5/144));
        cStyle.setIndentFromRight((short) (5/144));
        cStyle.setJustification(ClaStyle.Justification.LEFT_JUST);
        cStyle.setListItem(false);
        cStyle.setSpacingBefore((short) 0);
        cStyle.setSpacingAfter((short) 0);
        cStyle.setTableLevel(0);
        assertEquals("arielS14C0FI0R0L0J0pB0pA0B", cStyle.getSimpleSignature());
        assertEquals("S14C0FI0R0L5J0pB0pA0B", cStyle.getAbsoluteSignatureNoFont(5,5));
        assertEquals("08F14.0B+U-T-H1J0R0.0L0.0I-pB0pA0", cStyle.getStyleDescriptionToken());
        assertEquals(8, cStyle.getStyleHeadingPotentialRate());
        assertTrue(1F == cStyle.getSelectedRunDominance());
        assertTrue(cStyle.isHeading());
        cStyle.setBorder(4, 6, 7, 2);
        assertEquals(125149, cStyle.getBorder());
    }
}
