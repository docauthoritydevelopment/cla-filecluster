package com.cla.common.domain.dto.histogram;

import org.junit.Assert;
import org.junit.Test;

/**
 * Tests for ClaTokensCollection class
 * Created by yael on 11/16/2017.
 */
public class ClaTokensCollectionTest {

    @Test
    public void testStrTokensCollection1(){
        final ClaTokensCollection<String> tc1 = new ClaTokensCollection<>();

        tc1.addItem("t1", 1);
        tc1.addItem("t2", 2);
        tc1.addItem("t3", 3);
        tc1.addItem("t4", 4);
        tc1.addItem("t1", 111);
        tc1.addItem("t1", 11);
        tc1.addItem("t1", 1111);
        tc1.addItem("t2", 22);
        tc1.addItem("t2", 222);
        tc1.addItem("t3", 33);
        Assert.assertEquals(tc1.getCount("t1"),4);
        Assert.assertEquals(tc1.getCount("t4"),1);
        Assert.assertEquals(tc1.getGlobalCount(),10);
    }
}
