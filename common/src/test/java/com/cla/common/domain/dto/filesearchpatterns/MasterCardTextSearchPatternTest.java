package com.cla.common.domain.dto.filesearchpatterns;

import org.junit.Test;

import java.util.function.Predicate;
import java.util.regex.Pattern;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by: yael
 * Created on: 11/29/2017
 */
public class MasterCardTextSearchPatternTest {
    @Test
    public void test() {
        MasterCardTextSearchPattern ccp = new MasterCardTextSearchPattern(5, "name");

        Pattern pattern = ccp.getCompiledPattern();

        assertFalse(pattern.matcher("ffff").find());
        assertFalse(pattern.matcher("5631111111111111").find());
        assertTrue(pattern.matcher("5431-1111-1111-1111").find());
        assertTrue(pattern.matcher("sdsf 5431-1111-1111-1111 sdgdfgd").find());

        Predicate<String> p =  ccp.getPredicate();
        assertFalse(p.test("ffff"));
        assertFalse(p.test("2353"));
        assertFalse(p.test("5631-1111-1111-1111"));
        assertTrue(p.test("5431-1111-1111-1111"));
    }
}
