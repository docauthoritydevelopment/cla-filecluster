package com.cla.common.domain.dto;

import com.cla.common.domain.dto.security.Authority;
import org.junit.Assert;
import org.junit.Test;

public class AuthorityConsistencyTest {

    @Test
    public void testConsistency(){
        for (Authority value : Authority.values()){
            Assert.assertEquals(value.name(), value.getName());
        }
    }

}
