package com.cla.common.domain.dto.histogram;

import org.junit.Assert;
import org.junit.Test;

/**
 * Tests for ClaHistogramCollection class
 * Created by yael on 11/16/2017.
 */
public class ClaHistogramCollectionTest {

    @Test
    public void testStringHistogram(){
        final ClaHistogramCollection<String> h1 = new ClaHistogramCollection<>();
        final ClaHistogramCollection<String> h2 = new ClaHistogramCollection<>();

        h1.addItem("A");
        h1.addItem("B");
        h1.addItem("C");
        h1.addItem("B");
        h1.addItem("C");
        Assert.assertEquals(3, h1.addItem("C"));
        Assert.assertEquals(6, h1.getGlobalCount());

        h2.addItem("A");
        h2.addItem("B");
        h2.addItem("B");
        h2.addItem("B");
        h2.addItem("D");
        h2.addItem("D");
        h2.addItem("D");
        Assert.assertEquals(4, h2.addItem("D"));
        Assert.assertEquals(8, h2.getGlobalCount());

        Assert.assertEquals((int)(1000*h1.getSimilarityRatio(h2)),(int)(1000*h2.getSimilarityRatio(h1)));
        Assert.assertEquals(1000*6/14, (int)(1000*h1.getSimilarityRatio(h2)));
        Assert.assertEquals(1000, (int)(1000*h1.getSimilarityRatio(h1)));
    }

    @Test
    public void testIntegerHistogram(){
        final ClaHistogramCollection<Integer> h1 = new ClaHistogramCollection<>();
        final ClaHistogramCollection<Integer> h2 = new ClaHistogramCollection<>();

        h1.addItem(11);
        h1.addItem(22);
        h1.addItem(33);
        h1.addItem(22);
        h1.addItem(33);
        Assert.assertEquals(h1.addItem(33),3);
        Assert.assertEquals(h1.getGlobalCount(),6);

        h2.addItem(11);
        h2.addItem(22);
        h2.addItem(22);
        h2.addItem(22);
        h2.addItem(44);
        h2.addItem(44);
        h2.addItem(44);
        Assert.assertEquals(h2.addItem(44),4);
        Assert.assertEquals(h2.getGlobalCount(),8);

        Assert.assertEquals((int)(1000*h1.getSimilarityRatio(h2)),(int)(1000*h2.getSimilarityRatio(h1)));
        Assert.assertEquals((int)(1000*h1.getSimilarityRatio(h2)),1000*6/14);
        Assert.assertEquals((int)(1000*h1.getSimilarityRatio(h1)),1000);
    }

    @Test
    public void testOrderdMatch(){
        final ClaHistogramCollection<String> h1 = new ClaHistogramCollection<>();
        final ClaHistogramCollection<String> h2 = new ClaHistogramCollection<>();

        h1.setItemCounterLimit(1);
        h1.setEnforceDuringInsert(true);
        h1.addItem("A");
        h1.addItem("B");
        h1.addItem("C");
        h1.addItem("B");
        h1.addItem("C");
        h1.addItem("E");

        h2.setItemCounterLimit(1);
        h2.setEnforceDuringInsert(true);
        h2.addItem("A");
        h2.addItem("B");
        h2.addItem("B");
        h2.addItem("B");
        h2.addItem("X");
        h2.addItem("D");
        h2.addItem("D");
        h2.addItem("D");
        Assert.assertEquals(2, h1.getOrderedSimilarityCount(h2));
        h2.addItem("E");
        Assert.assertEquals(3, h1.getOrderedSimilarityCount(h2));
        h2.addItem("C");
        Assert.assertEquals(3, h1.getOrderedSimilarityCount(h2));
        h1.addItem("X");
        Assert.assertEquals(3, h1.getOrderedSimilarityCount(h2));
    }

}
