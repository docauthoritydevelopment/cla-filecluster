package com.cla.common.domain.dto.histogram;

import org.junit.Assert;
import org.junit.Test;

/**
 * Tests for ClaHistogramCollection class
 * Created by yael on 11/16/2017.
 */
public class ClaLongHistogramCollectionArrayTest {

    @Test
    public void testIntegerHistogram(){
        final ClaHistogramCollection<Long> h1 = new ClaHistogramCollection<>();
        final ClaHistogramCollection<Long> h2 = new ClaHistogramCollection<>();

        h1.addItem(22L);
        h1.addItem(33L);
        h1.addItem(22L);
        h1.addItem(33L);
        h1.addItem(11L);
        h1.addItem(33L);
        final ClaHistogramCollection h1Arr = ClaLongHistogramCollectionArray.create(h1);
        Assert.assertEquals(h1.getJoinedKeysWithBoost("pre"),h1Arr.getJoinedKeysWithBoost("pre"));

        h2.addItem(44L);
        h2.addItem(11L);
        h2.addItem(22L);
        h2.addItem(22L);
        h2.addItem(22L);
        h2.addItem(44L);
        h2.addItem(44L);
        h2.addItem(44L);
        final ClaHistogramCollection h2Arr = ClaLongHistogramCollectionArray.create(h2);
        Assert.assertEquals(h2.getJoinedKeysWithBoost("pre"),h2Arr.getJoinedKeysWithBoost("pre"));

        Assert.assertEquals((int)(1000*h1Arr.getSimilarityRatio(h2Arr)),(int)(1000*h2Arr.getSimilarityRatio(h1Arr)));
        Assert.assertEquals((int)(1000*h1.getSimilarityRatio(h2)),1000*6/14);
        Assert.assertEquals((int)(1000*h1Arr.getSimilarityRatio(h2)),1000*6/14);
        Assert.assertEquals((int)(1000*h1.getSimilarityRatio(h2Arr)),1000*6/14);
        Assert.assertEquals((int)(1000*h1.getSimilarityRatio(h1Arr)),1000);
        Assert.assertEquals((int)(1000*h1Arr.getSimilarityRatio(h1)),1000);

        Assert.assertEquals((int)(1000*h1.getSimilarityRatio(h1Arr)),1000);
        Assert.assertEquals((int)(1000*h1Arr.getSimilarityRatio(h1)),1000);
        Assert.assertEquals((int)(1000*h2.getSimilarityRatio(h2Arr)),1000);
        Assert.assertEquals((int)(1000*h2Arr.getSimilarityRatio(h2)),1000);

    }
}
