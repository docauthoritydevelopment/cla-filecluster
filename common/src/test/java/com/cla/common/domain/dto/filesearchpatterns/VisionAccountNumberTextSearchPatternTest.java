package com.cla.common.domain.dto.filesearchpatterns;

import org.junit.Test;

import java.util.function.Predicate;
import java.util.regex.Pattern;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by: itay
 * Created on: 04/07/2019
 */
public class VisionAccountNumberTextSearchPatternTest {
    @Test
    public void test() {
        VisionAccountNumberTextSearchPattern ccp = new VisionAccountNumberTextSearchPattern(5, "name");

        Pattern pattern = ccp.getCompiledPattern();

        assertFalse(pattern.matcher("ffff").find());
        assertFalse(pattern.matcher("2353").find());
        assertFalse(pattern.matcher("3454 5676 5676 6754").find());
        assertTrue(pattern.matcher("10100100001147").find());
        assertTrue(pattern.matcher("10100100001155").find());
        assertTrue(pattern.matcher("10100108001155").find());
        assertTrue(pattern.matcher("This is an ID 10100100001147. yea").find());
        assertTrue(pattern.matcher("sadsa \n\t 10100100001155 sdfsdfds").find());

        Predicate<String> p =  ccp.getPredicate();
        assertFalse(p.test("3454 5676 5676 6754"));

        assertTrue(p.test("10100100001147"));    // ok
        assertFalse(p.test("10200100001147"));   // modified, should fail

        assertTrue(p.test("10100100001155"));    // ok
        assertFalse(p.test("10100100001156"));   // modified, should fail
        assertFalse(p.test("99060130891147"));   // modified, should fail
    }
}
