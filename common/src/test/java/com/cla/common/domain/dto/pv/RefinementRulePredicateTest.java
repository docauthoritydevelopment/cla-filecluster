package com.cla.common.domain.dto.pv;

import com.google.common.base.Strings;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class RefinementRulePredicateTest {

    private Function<String, Boolean> tokenizerValidator = input -> !Strings.isNullOrEmpty(input);

    @Test
    public void validateRule() {

        // and rule with values and no predicates
        RefinementRulePredicate p = new RefinementRulePredicate();
        p.setValues(Collections.singletonList("gsdfgs"));
        p.setOperator(RuleOperator.AND);
        assertFalse(p.validateRule(tokenizerValidator));

        // or rule with values and no predicates
        p = new RefinementRulePredicate();
        p.setValues(Collections.singletonList("gsdfgs"));
        p.setOperator(RuleOperator.OR);
        assertFalse(p.validateRule(tokenizerValidator));

        // contains rule with no field
        p = new RefinementRulePredicate();
        p.setValues(Collections.singletonList("gsdfgs"));
        p.setOperator(RuleOperator.CONTAINS);
        assertFalse(p.validateRule(tokenizerValidator));

        // valid contains rule
        p.setField(RefinementRuleField.CONTENT);
        assertTrue(p.validateRule(tokenizerValidator));

        // or rule with only 1  predicate
        RefinementRulePredicate pOr = new RefinementRulePredicate();
        pOr.setOperator(RuleOperator.OR);
        pOr.setPredicates(new ArrayList<>());
        pOr.getPredicates().add(p);
        assertFalse(pOr.validateRule(tokenizerValidator));

        // valid or rule
        pOr.getPredicates().add(p);
        assertTrue(p.validateRule(tokenizerValidator));
    }

    @Test
    public void testSimpleContainsRule() {
        RefinementRulePredicate p = new RefinementRulePredicate();
        p.setValues(Collections.singletonList("gsdfgs"));
        p.setOperator(RuleOperator.CONTAINS);
        p.setField(RefinementRuleField.CONTENT);

        Map<RefinementRulePredicate, Boolean> map = new HashMap<>();
        map.put(p, true);
        assertTrue(p.testRule(map));

        map.put(p, false);
        assertFalse(p.testRule(map));
    }

    @Test
    public void testEmptyContainsRule() {
        RefinementRulePredicate p = new RefinementRulePredicate();
        p.setValues(Collections.singletonList(""));
        p.setOperator(RuleOperator.CONTAINS);
        p.setField(RefinementRuleField.CONTENT);
        assertFalse(p.validateRule(tokenizerValidator));
    }

    @Test
    public void testOrRule() {
        RefinementRulePredicate p = new RefinementRulePredicate();
        p.setValues(Collections.singletonList("gsdfgs"));
        p.setOperator(RuleOperator.CONTAINS);
        p.setField(RefinementRuleField.CONTENT);

        RefinementRulePredicate p2 = new RefinementRulePredicate();
        p2.setValues(Collections.singletonList("gddddddgs"));
        p2.setOperator(RuleOperator.CONTAINS);
        p2.setField(RefinementRuleField.CONTENT);

        Map<RefinementRulePredicate, Boolean> map = new HashMap<>();
        map.put(p, true);
        map.put(p2, false);

        RefinementRulePredicate pOr = new RefinementRulePredicate();
        pOr.setOperator(RuleOperator.OR);
        pOr.setPredicates(new ArrayList<>());
        pOr.getPredicates().add(p);
        pOr.getPredicates().add(p2);

        assertTrue(pOr.validateRule(tokenizerValidator));

        assertTrue(pOr.testRule(map));

        map.put(p, false);
        assertFalse(pOr.testRule(map));
    }

    @Test
    public void testAndRule() {
        RefinementRulePredicate p = new RefinementRulePredicate();
        p.setValues(Collections.singletonList("gsdfgs"));
        p.setOperator(RuleOperator.CONTAINS);
        p.setField(RefinementRuleField.CONTENT);

        RefinementRulePredicate p2 = new RefinementRulePredicate();
        p2.setValues(Collections.singletonList("gddddddgs"));
        p2.setOperator(RuleOperator.CONTAINS);
        p2.setField(RefinementRuleField.CONTENT);

        Map<RefinementRulePredicate, Boolean> map = new HashMap<>();
        map.put(p, true);
        map.put(p2, true);

        RefinementRulePredicate pOr = new RefinementRulePredicate();
        pOr.setOperator(RuleOperator.AND);
        pOr.setPredicates(new ArrayList<>());
        pOr.getPredicates().add(p);
        pOr.getPredicates().add(p2);

        assertTrue(pOr.validateRule(tokenizerValidator));

        assertTrue(pOr.testRule(map));

        map.put(p, false);
        assertFalse(pOr.testRule(map));
    }
}