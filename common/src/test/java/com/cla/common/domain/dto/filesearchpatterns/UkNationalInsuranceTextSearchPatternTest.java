package com.cla.common.domain.dto.filesearchpatterns;

import org.junit.Test;

import java.util.function.Predicate;
import java.util.regex.Pattern;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by: yael
 * Created on: 11/29/2017
 */
public class UkNationalInsuranceTextSearchPatternTest {

    @Test
    public void test() {
        UkNationalInsuranceTextSearchPattern ccp = new UkNationalInsuranceTextSearchPattern(5, "name");

        Pattern pattern = ccp.getCompiledPattern();

        assertFalse(pattern.matcher("ffff").find());
        assertFalse(pattern.matcher("2353").find());
        assertFalse(pattern.matcher("QQ 12 34 56 C").find());
        assertTrue(pattern.matcher("RR 12 34 56 C").find());
        assertFalse(pattern.matcher("RR 12 34 56").find());
        assertTrue(pattern.matcher("sadad \n\t dfsf RR 12 34 56 C dfgsdg").find());

        Predicate<String> p =  ccp.getPredicate();
        assertFalse(p.test("BG 12 34 56 C"));
        assertTrue(p.test("RR 12 34 56 C"));
        assertTrue(p.test("RR 12 34 56"));
    }
}
