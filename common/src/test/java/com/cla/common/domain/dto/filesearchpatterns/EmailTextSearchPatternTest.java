package com.cla.common.domain.dto.filesearchpatterns;

import org.junit.Test;

import java.util.function.Predicate;
import java.util.regex.Pattern;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by: yael
 * Created on: 12/3/2017
 */
public class EmailTextSearchPatternTest {
    @Test
    public void test() {
        EmailTextSearchPattern ccp = new EmailTextSearchPattern(5, "mail");

        Pattern pattern = ccp.getCompiledPattern();

        assertFalse(pattern.matcher("ffff").find());
        assertFalse(pattern.matcher("2353").find());
        assertFalse(pattern.matcher("30569@309025.904").find());
        assertFalse(pattern.matcher("30dd69@.904").find());
        assertFalse(pattern.matcher("yee5@.com").find());
        assertFalse(pattern.matcher("yee5@com").find());
        assertTrue(pattern.matcher("yee5@gg.com").find());
        assertTrue(pattern.matcher("yee5@gg.org.il").find());
        assertTrue(pattern.matcher("dsfs\t yee5@gg.org.il \ndsgsdgsdg").find());

        Predicate<String> p =  ccp.getPredicate();
        assertTrue(p.test("yee5@gg.org.il"));
        assertFalse(p.test("yefgfdtgdfgdfgfdgdfgdfgdfgfdgdfgdfgfdgfdgfdgfdgfdgfdgfdgfdgdfgdfgdfgvcdrfgdfbdfdfbdfgbdfgbfdgfdgfdgdfgdfgde5@gg.org.il"));
    }
}
