package com.cla.common.domain.dto.filesearchpatterns;

import org.junit.Test;

import java.util.function.Predicate;
import java.util.regex.Pattern;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by: yael
 * Created on: 11/29/2017
 */
public class GenericCreditCardTextSearchPatternTest {

    @Test
    public void test() {
        GenericCreditCardTextSearchPattern ccp = new GenericCreditCardTextSearchPattern(5, "name");

        Pattern pattern = ccp.getCompiledPattern();

        assertFalse(pattern.matcher(" ffff").find());
        assertFalse(pattern.matcher(" 5631111111111111 ").find());
        assertTrue(pattern.matcher(" 4580280101570651 ").find());
        assertTrue(pattern.matcher(" 5431-1111-1111-1111\n").find());
        assertTrue(pattern.matcher("dsfsdf 5431-1111-1111-1111 sdgsdgf").find());

        Predicate<String> p =  ccp.getPredicate();
        assertFalse(p.test("ffff"));
        assertFalse(p.test("2353"));
        assertTrue(p.test("30569309025904"));
        assertTrue(p.test("4580-2801-0157-0651"));
        assertTrue(p.test("5431-1111-1111-1111"));
        assertFalse(p.test("5631111111111111"));
    }
}
