package com.cla.common.domain.dto.filesearchpatterns;

import org.junit.Test;

import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by: yael
 * Created on: 11/29/2017
 */
public class IsraeliIdTextSearchPatternTest {
    @Test
    public void test() {
        IsraeliIdTextSearchPattern ccp = new IsraeliIdTextSearchPattern(5, "name");

        Pattern pattern = ccp.getCompiledPattern();

        assertFalse(pattern.matcher("ffff").find());
        assertFalse(pattern.matcher("2353").find());
        assertFalse(pattern.matcher("3454 5676 5676 6754").find());
        assertTrue(pattern.matcher("345456764").find());
        assertTrue(pattern.matcher("453456766").find());
        assertTrue(pattern.matcher("sadsa \n\t 453456766 sdfsdfds").find());

        Predicate<String> p =  ccp.getPredicate();
        assertFalse(p.test("3454 5676 5676 6754"));
        assertFalse(p.test("345456764"));
        assertTrue(p.test("453456766"));
    }
}
