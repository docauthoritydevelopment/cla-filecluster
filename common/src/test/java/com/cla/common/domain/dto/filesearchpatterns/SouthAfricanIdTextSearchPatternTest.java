package com.cla.common.domain.dto.filesearchpatterns;

import org.junit.Test;

import java.util.function.Predicate;
import java.util.regex.Pattern;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by: itay
 * Created on: 25/06/2019
 */
public class SouthAfricanIdTextSearchPatternTest {
    @Test
    public void test() {
        SouthAfricanIdTextSearchPattern ccp = new SouthAfricanIdTextSearchPattern(5, "name");

        Pattern pattern = ccp.getCompiledPattern();

        assertFalse(pattern.matcher("ffff").find());
        assertFalse(pattern.matcher("2353").find());
        assertFalse(pattern.matcher("3454 5676 5676 6754").find());
        assertTrue(pattern.matcher("7207255800082").find());
        assertTrue(pattern.matcher("7312164555189").find());
        assertTrue(pattern.matcher("9906295013089").find());
        assertTrue(pattern.matcher("This is an ID 9906295013083. yea").find());
        assertTrue(pattern.matcher("sadsa \n\t 7312164555182 sdfsdfds").find());

        Predicate<String> p =  ccp.getPredicate();
        assertFalse(p.test("3454 5676 5676 6754"));

        assertTrue(p.test("7207255800082"));    // ok
        assertFalse(p.test("7207255800081"));   // modified, should fail

        assertTrue(p.test("7312164555189"));    // ok
        assertFalse(p.test("7312164555182"));   // modified, should fail

        assertTrue(p.test("9906295013089"));    // ok
        assertFalse(p.test("9906295013086"));   // modified, should fail
    }
}
