package com.cla.common.domain.dto.filesearchpatterns;

import org.junit.Test;

import java.util.function.Predicate;
import java.util.regex.Pattern;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by: yael
 * Created on: 11/29/2017
 */
public class SSNTextSearchPatternTest {

    @Test
    public void test() {
        SSNTextSearchPattern ccp = new SSNTextSearchPattern(5, "name");

        Pattern pattern = ccp.getCompiledPattern();

        assertFalse(pattern.matcher("ffff").find());
        assertFalse(pattern.matcher("2353").find());
        assertFalse(pattern.matcher("333 1 5555").find());
        assertTrue(pattern.matcher("333 12 5555").find());
        assertTrue(pattern.matcher("sadad \n\t dfsf 333 12 5555 dfgsdg").find());

        Predicate<String> p =  ccp.getPredicate();
        assertFalse(p.test("666 12 5555"));
        assertFalse(p.test("999 12 5555"));
        assertTrue(p.test("333 12 5555"));
    }
}
