package com.cla.common.tag.scope;


import org.junit.Assert;
import org.junit.Test;

public class PriorityScopedTagResolverTest {

    @Test
    public void testAddFileAttachedTagPermutations() {
        testResolveScopedTag(
                new String[]{},
                "1.2.1.MANUAL.file",
                true,
                new String[]{"1.2.1.MANUAL.file"});

        testResolveScopedTag(
                new String[]{"1.2.1.MANUAL.file"},
                "1.2.2.MANUAL.file",
                true,
                new String[]{"1.2.2.MANUAL.file"});

        testResolveScopedTag(
                new String[]{"1.2.1.MANUAL.file;1.2.2.MANUAL.fileGroup"},
                "1.2.2.MANUAL.file",
                true,
                new String[]{"1.2.2.MANUAL.file;1.2.2.MANUAL.fileGroup"});

        testResolveScopedTag(
                new String[]{"1.2.1.MANUAL.file;1.2.2.MANUAL.f0"},
                "1.2.2.MANUAL.file",
                true,
                new String[]{"1.2.2.MANUAL.file;1.2.2.MANUAL.f0"});

        testResolveScopedTag(
                new String[]{"1.2.1.MANUAL.file;1.2.2.MANUAL.fileGroup;1.2.2.MANUAL.f0"},
                "1.2.2.MANUAL.file",
                true,
                new String[]{"1.2.2.MANUAL.file;1.2.2.MANUAL.fileGroup;1.2.2.MANUAL.f0"});
    }

    @Test
    public void testCombineDocTypeAndTagsSingleAndMulti() {
        testResolveScopedTag(
                new String[]{"dt.g.59"},
                "dt.1.4..file",
                true,
                new String[]{"dt.g.59", "dt.1.4..file"});

        testResolveScopedTag(
                new String[]{"dt.g.59", "dt.1.4..file"},
                "dt.1.3..file",
                true,
                new String[]{"dt.g.59", "dt.1.3..file"});

        testResolveScopedTag(
                new String[]{"1.2.1.MANUAL.file;1.2.2.MANUAL.fileGroup", "dt.g.59", "dt.1.4..file"},
                "dt.1.3..f0",
                true,
                new String[]{"1.2.1.MANUAL.file;1.2.2.MANUAL.fileGroup", "dt.g.59", "dt.1.4..file;dt.1.3..f0"});

        testResolveScopedTag(
                new String[]{"1.2.1.MANUAL.file;1.2.2.MANUAL.fileGroup", "dt.g.59", "dt.1.4..file;dt.1.3..f0"},
                "dt.1.4..f0",
                true,
                new String[]{"1.2.1.MANUAL.file;1.2.2.MANUAL.fileGroup", "dt.g.59", "dt.1.4..file;dt.1.4..f0"});

        testResolveScopedTag(
                new String[]{"1.2.1.MANUAL.file;1.2.2.MANUAL.fileGroup", "dt.g.59", "dt.1.4..file;dt.1.3..f0"},
                "1.2.2.MANUAL.file",
                true,
                new String[]{"1.2.2.MANUAL.file;1.2.2.MANUAL.fileGroup", "dt.g.59", "dt.1.4..file;dt.1.3..f0"});

        testResolveScopedTag(
                new String[]{"1.2.1.MANUAL.file;1.2.2.MANUAL.fileGroup", "1.3.4", "dt.g.59", "dt.1.4..file;dt.1.3..f0"},
                "1.4.1.MANUAL.file",
                true,
                new String[]{"1.2.1.MANUAL.file;1.2.2.MANUAL.fileGroup", "1.3.4", "dt.g.59", "dt.1.4..file;dt.1.3..f0", "1.4.1.MANUAL.file"});
    }

    @Test
    public void testDocTypesAndTagsMultiAndSingleRemove() {
        testResolveScopedTag(
                new String[]{"1.2.1.MANUAL.file;1.2.2.MANUAL.fileGroup", "1.3.4", "dt.g.59", "dt.1.4..file;dt.1.3..f0", "1.4.1.MANUAL.file"},
                "dt.1.4..file",
                false,
                new String[]{"1.2.1.MANUAL.file;1.2.2.MANUAL.fileGroup", "1.3.4", "dt.g.59", "dt.1.3..f0", "1.4.1.MANUAL.file"});

        testResolveScopedTag(
                new String[]{"1.2.1.MANUAL.file;1.2.2.MANUAL.fileGroup", "1.3.4", "dt.g.59", "dt.1.3..f0", "1.4.1.MANUAL.file"},
                "dt.1.3..f0",
                false,
                new String[]{"1.2.1.MANUAL.file;1.2.2.MANUAL.fileGroup", "1.3.4", "dt.g.59", "1.4.1.MANUAL.file"});

        testResolveScopedTag(
                new String[]{"1.2.1.MANUAL.file;1.2.2.MANUAL.fileGroup", "1.3.4", "dt.g.59", "dt.1.3..f0", "1.4.1.MANUAL.file"},
                "1.2.1.MANUAL.file",
                false,
                new String[]{"1.2.2.MANUAL.fileGroup", "1.3.4", "dt.g.59", "dt.1.3..f0", "1.4.1.MANUAL.file"});

        testResolveScopedTag(
                new String[]{"1.2.2.MANUAL.fileGroup", "1.3.4", "dt.g.59", "dt.1.3..f0", "1.4.1.MANUAL.file"},
                "1.2.2.MANUAL.fileGroup",
                false,
                new String[]{"1.3.4", "dt.g.59", "dt.1.3..f0", "1.4.1.MANUAL.file"});
    }

    @Test
    public void testRemoveFileAttachedTagPermutations() {
        testResolveScopedTag(
                new String[]{},
                "1.2.1.MANUAL.file",
                false,
                new String[]{});

        testResolveScopedTag(
                new String[]{"1.2.1.MANUAL.file;1.2.2.MANUAL.fileGroup"},
                "1.2.1.MANUAL.file",
                false,
                new String[]{"1.2.2.MANUAL.fileGroup"});

        testResolveScopedTag(
                new String[]{"1.2.1.MANUAL.file;1.2.2.MANUAL.f0"},
                "1.2.1.MANUAL.file",
                false,
                new String[]{"1.2.2.MANUAL.f0"});

        testResolveScopedTag(
                new String[]{"1.2.1.MANUAL.file;1.2.2.MANUAL.fileGroup;1.2.2.MANUAL.f0"},
                "1.2.1.MANUAL.file",
                false,
                new String[]{"1.2.2.MANUAL.fileGroup;1.2.2.MANUAL.f0"});
    }

    @Test
    public void testAddFileGroupAttachedTagPermutations() {
        testResolveScopedTag(
                new String[]{},
                "1.2.1.MANUAL.fileGroup",
                true,
                new String[]{"1.2.1.MANUAL.fileGroup"});

        testResolveScopedTag(
                new String[]{"1.2.1.MANUAL.file;1.2.1.MANUAL.fileGroup"},
                "1.2.2.MANUAL.fileGroup",
                true,
                new String[]{"1.2.1.MANUAL.file;1.2.2.MANUAL.fileGroup"});

        testResolveScopedTag(
                new String[]{"1.2.1.MANUAL.fileGroup;1.2.1.MANUAL.f0"},
                "1.2.2.MANUAL.fileGroup",
                true,
                new String[]{"1.2.2.MANUAL.fileGroup;1.2.1.MANUAL.f0"});

        testResolveScopedTag(
                new String[]{"1.2.1.MANUAL.file;1.2.1.MANUAL.fileGroup;1.2.1.MANUAL.f0"},
                "1.2.2.MANUAL.fileGroup",
                true,
                new String[]{"1.2.1.MANUAL.file;1.2.2.MANUAL.fileGroup;1.2.1.MANUAL.f0"});
    }

    @Test
    public void testRemoveFileGroupAttachedTagPermutations() {
        testResolveScopedTag(
                new String[]{},
                "1.2.1.MANUAL.fileGroup",
                false,
                new String[]{});

        testResolveScopedTag(
                new String[]{"1.2.1.MANUAL.file;1.2.1.MANUAL.fileGroup"},
                "1.2.1.MANUAL.fileGroup",
                false,
                new String[]{"1.2.1.MANUAL.file"});

        testResolveScopedTag(
                new String[]{"1.2.1.MANUAL.fileGroup;1.2.1.MANUAL.f0"},
                "1.2.1.MANUAL.fileGroup",
                false,
                new String[]{"1.2.1.MANUAL.f0"});

        testResolveScopedTag(
                new String[]{"1.2.1.MANUAL.file;1.2.1.MANUAL.fileGroup;1.2.1.MANUAL.f0"},
                "1.2.1.MANUAL.fileGroup",
                false,
                new String[]{"1.2.1.MANUAL.file;1.2.1.MANUAL.f0"});
    }

    @Test
    public void testAddFolderAttachedTagPermutations() {
        testResolveScopedTag(
                new String[]{},
                "1.2.1.MANUAL.f0",
                true,
                new String[]{"1.2.1.MANUAL.f0"});

        testResolveScopedTag(
                new String[]{"1.2.1.MANUAL.file"},
                "1.2.2.MANUAL.f0",
                true,
                new String[]{"1.2.1.MANUAL.file;1.2.2.MANUAL.f0"});

        testResolveScopedTag(
                new String[]{"1.2.1.MANUAL.fileGroup;1.2.1.MANUAL.f0"},
                "1.2.2.MANUAL.f0",
                true,
                new String[]{"1.2.1.MANUAL.fileGroup;1.2.2.MANUAL.f0"});

        testResolveScopedTag(
                new String[]{"1.2.1.MANUAL.file;1.2.1.MANUAL.fileGroup"},
                "1.2.2.MANUAL.f0",
                true,
                new String[]{"1.2.1.MANUAL.file;1.2.1.MANUAL.fileGroup;1.2.2.MANUAL.f0"});

        testResolveScopedTag(
                new String[]{"1.2.1.MANUAL.file;1.2.1.MANUAL.fileGroup;1.2.1.MANUAL.f1;1.2.1.MANUAL.f0"},
                "1.2.2.MANUAL.f0",
                true,
                new String[]{"1.2.1.MANUAL.file;1.2.1.MANUAL.fileGroup;1.2.1.MANUAL.f1;1.2.2.MANUAL.f0"});
    }

    @Test
    public void testFolderDepthAdd() {
        testResolveScopedTag(
                new String[]{"1.2.1.MANUAL.file;1.2.1.MANUAL.fileGroup"},
                "1.2.2.MANUAL.f0",
                true,
                new String[]{"1.2.1.MANUAL.file;1.2.1.MANUAL.fileGroup;1.2.2.MANUAL.f0"});

        testResolveScopedTag(
                new String[]{"1.2.1.MANUAL.file;1.2.1.MANUAL.fileGroup;1.2.2.MANUAL.f0"},
                "1.2.2.MANUAL.f1",
                true,
                new String[]{"1.2.1.MANUAL.file;1.2.1.MANUAL.fileGroup;1.2.2.MANUAL.f1;1.2.2.MANUAL.f0"});

        testResolveScopedTag(
                new String[]{"1.2.1.MANUAL.file;1.2.1.MANUAL.fileGroup;1.2.2.MANUAL.f5;1.2.2.MANUAL.f0"},
                "1.2.2.MANUAL.f4",
                true,
                new String[]{"1.2.1.MANUAL.file;1.2.1.MANUAL.fileGroup;1.2.2.MANUAL.f5;1.2.2.MANUAL.f4;1.2.2.MANUAL.f0"});

    }

    @Test
    public void testFolderDepthRemove() {
        testResolveScopedTag(
                new String[]{"1.2.1.MANUAL.file;1.2.1.MANUAL.fileGroup;1.2.2.MANUAL.f0"},
                "1.2.2.MANUAL.f0",
                false,
                new String[]{"1.2.1.MANUAL.file;1.2.1.MANUAL.fileGroup"});

        testResolveScopedTag(
                new String[]{"1.2.1.MANUAL.file;1.2.1.MANUAL.fileGroup;1.2.2.MANUAL.f5;1.2.2.MANUAL.f0"},
                "1.2.2.MANUAL.f5",
                false,
                new String[]{"1.2.1.MANUAL.file;1.2.1.MANUAL.fileGroup;1.2.2.MANUAL.f0"});

        testResolveScopedTag(
                new String[]{"1.2.1.MANUAL.file;1.2.1.MANUAL.fileGroup;1.2.2.MANUAL.f5;1.2.2.MANUAL.f4;1.2.2.MANUAL.f0"},
                "1.2.2.MANUAL.f4",
                false,
                new String[]{"1.2.1.MANUAL.file;1.2.1.MANUAL.fileGroup;1.2.2.MANUAL.f5;1.2.2.MANUAL.f0"});
    }

    @Test
    public void testAddMultiValueScopeMatchPermutations() {
        testResolveScopedTag(
                new String[]{"1.2.1.MANUAL.file;1.2.1.MANUAL.fileGroup;1.2.2.MANUAL.f5;1.2.2.MANUAL.f4;1.2.2.MANUAL.f0"},
                "2.2.2.MANUAL.f4",
                true,
                new String[]{"1.2.1.MANUAL.file;1.2.1.MANUAL.fileGroup;1.2.2.MANUAL.f5;1.2.2.MANUAL.f4;1.2.2.MANUAL.f0",
                        "2.2.2.MANUAL.f4"});

        testResolveScopedTag(
                new String[]{"1.2.1.MANUAL.file;1.2.1.MANUAL.fileGroup;1.2.2.MANUAL.f5;1.2.2.MANUAL.f4;1.2.2.MANUAL.f0",
                        "2.2.2.MANUAL.f4"},
                "2.2.2.MANUAL.file",
                true,
                new String[]{"1.2.1.MANUAL.file;1.2.1.MANUAL.fileGroup;1.2.2.MANUAL.f5;1.2.2.MANUAL.f4;1.2.2.MANUAL.f0",
                        "2.2.2.MANUAL.file;2.2.2.MANUAL.f4"});

        testResolveScopedTag(
                new String[]{"1.2.1.MANUAL.file;1.2.1.MANUAL.fileGroup;1.2.2.MANUAL.f5;1.2.2.MANUAL.f4;1.2.2.MANUAL.f0",
                        "2.2.2.MANUAL.file;2.2.2.MANUAL.f4"},
                "2.2.2.MANUAL.fileGroup",
                true,
                new String[]{"1.2.1.MANUAL.file;1.2.1.MANUAL.fileGroup;1.2.2.MANUAL.f5;1.2.2.MANUAL.f4;1.2.2.MANUAL.f0",
                        "2.2.2.MANUAL.file;2.2.2.MANUAL.fileGroup;2.2.2.MANUAL.f4"});
    }

    @Test
    public void testRemoveMultiValueScopeMatchPermutations() {
        testResolveScopedTag(
                new String[]{"1.2.1.MANUAL.file;1.2.1.MANUAL.fileGroup;1.2.2.MANUAL.f5;1.2.2.MANUAL.f4;1.2.2.MANUAL.f0",
                        "2.2.2.MANUAL.f4"},
                "2.2.2.MANUAL.f4",
                false,
                new String[]{"1.2.1.MANUAL.file;1.2.1.MANUAL.fileGroup;1.2.2.MANUAL.f5;1.2.2.MANUAL.f4;1.2.2.MANUAL.f0"});

        testResolveScopedTag(
                new String[]{"1.2.1.MANUAL.file;1.2.1.MANUAL.fileGroup;1.2.2.MANUAL.f5;1.2.2.MANUAL.f4;1.2.2.MANUAL.f0",
                        "2.2.2.MANUAL.file;2.2.2.MANUAL.f4"},
                "2.2.2.MANUAL.file",
                false,
                new String[]{"1.2.1.MANUAL.file;1.2.1.MANUAL.fileGroup;1.2.2.MANUAL.f5;1.2.2.MANUAL.f4;1.2.2.MANUAL.f0",
                        "2.2.2.MANUAL.f4"});

        testResolveScopedTag(
                new String[]{"1.2.1.MANUAL.file;1.2.1.MANUAL.fileGroup;1.2.2.MANUAL.f5;1.2.2.MANUAL.f4;1.2.2.MANUAL.f0",
                        "2.2.2.MANUAL.file;2.2.2.MANUAL.fileGroup;2.2.2.MANUAL.f4"},
                "2.2.2.MANUAL.fileGroup",
                false,
                new String[]{"1.2.1.MANUAL.file;1.2.1.MANUAL.fileGroup;1.2.2.MANUAL.f5;1.2.2.MANUAL.f4;1.2.2.MANUAL.f0",
                        "2.2.2.MANUAL.file;2.2.2.MANUAL.f4"});
    }

    @Test
    public void testAddTagViaDocTypeFile() {
        testResolveScopedTag(
                new String[]{},
                "1.2.1.MANUAL.vdt.file",
                true,
                new String[]{"1.2.1.MANUAL.vdt.file"});

        testResolveScopedTag(
                new String[]{"1.2.1.MANUAL.file"},
                "1.2.1.MANUAL.vdt.file",
                true,
                new String[]{"1.2.1.MANUAL.file;1.2.1.MANUAL.vdt.file"});

        testResolveScopedTag(
                new String[]{"1.2.1.MANUAL.file"},
                "1.2.2.MANUAL.vdt.file",
                true,
                new String[]{"1.2.1.MANUAL.file;1.2.2.MANUAL.vdt.file"});

        testResolveScopedTag(
                new String[]{"1.2.1.MANUAL.vdt.file"},
                "1.2.2.MANUAL.file",
                true,
                new String[]{"1.2.2.MANUAL.file;1.2.1.MANUAL.vdt.file"});

        testResolveScopedTag(
                new String[]{"1.2.1.MANUAL.file;1.2.1.MANUAL.vdt.file"},
                "1.2.2.MANUAL.file",
                true,
                new String[]{"1.2.2.MANUAL.file;1.2.1.MANUAL.vdt.file"});

        testResolveScopedTag(
                new String[]{"1.2.1.MANUAL.file;1.2.1.MANUAL.vdt.file"},
                "1.2.2.MANUAL.vdt.file",
                true,
                new String[]{"1.2.1.MANUAL.file;1.2.2.MANUAL.vdt.file"});

        testResolveScopedTag(
                new String[]{"1.2.1.MANUAL.vdt.file"},
                "1.2.1.MANUAL.fileGroup",
                true,
                new String[]{"1.2.1.MANUAL.vdt.file;1.2.1.MANUAL.fileGroup"});

        testResolveScopedTag(
                new String[]{"1.2.1.MANUAL.file;1.2.1.MANUAL.fileGroup"},
                "1.2.1.MANUAL.vdt.file",
                true,
                new String[]{"1.2.1.MANUAL.file;1.2.1.MANUAL.vdt.file;1.2.1.MANUAL.fileGroup"});

        testResolveScopedTag(
                new String[]{"1.2.1.MANUAL.file;1.2.1.MANUAL.vdt.file;1.2.1.MANUAL.f0"},
                "1.2.1.MANUAL.vdt.file",
                true,
                new String[]{"1.2.1.MANUAL.file;1.2.1.MANUAL.vdt.file;1.2.1.MANUAL.f0"});
    }
    @Test
    public void testAddTagViaDocTypeFileGroup(){
        testResolveScopedTag(
                new String[]{},
                "1.2.1.MANUAL.vdt.fileGroup",
                true,
                new String[]{"1.2.1.MANUAL.vdt.fileGroup"});

        testResolveScopedTag(
                new String[]{"1.2.1.MANUAL.fileGroup"},
                "1.2.1.MANUAL.vdt.fileGroup",
                true,
                new String[]{"1.2.1.MANUAL.fileGroup;1.2.1.MANUAL.vdt.fileGroup"});

        testResolveScopedTag(
                new String[]{"1.2.1.MANUAL.fileGroup"},
                "1.2.2.MANUAL.vdt.fileGroup",
                true,
                new String[]{"1.2.1.MANUAL.fileGroup;1.2.2.MANUAL.vdt.fileGroup"});

        testResolveScopedTag(
                new String[]{"1.2.1.MANUAL.vdt.fileGroup"},
                "1.2.2.MANUAL.fileGroup",
                true,
                new String[]{"1.2.2.MANUAL.fileGroup;1.2.1.MANUAL.vdt.fileGroup"});

        testResolveScopedTag(
                new String[]{"1.2.1.MANUAL.fileGroup;1.2.1.MANUAL.vdt.fileGroup"},
                "1.2.2.MANUAL.fileGroup",
                true,
                new String[]{"1.2.2.MANUAL.fileGroup;1.2.1.MANUAL.vdt.fileGroup"});

        testResolveScopedTag(
                new String[]{"1.2.1.MANUAL.fileGroup;1.2.1.MANUAL.vdt.fileGroup"},
                "1.2.2.MANUAL.vdt.fileGroup",
                true,
                new String[]{"1.2.1.MANUAL.fileGroup;1.2.2.MANUAL.vdt.fileGroup"});

        testResolveScopedTag(
                new String[]{"1.2.1.MANUAL.file;1.2.1.MANUAL.vdt.file"},
                "1.2.1.MANUAL.fileGroup",
                true,
                new String[]{"1.2.1.MANUAL.file;1.2.1.MANUAL.vdt.file;1.2.1.MANUAL.fileGroup"});


        testResolveScopedTag(
                new String[]{"1.2.1.MANUAL.file;1.2.1.MANUAL.vdt.file;1.2.1.MANUAL.fileGroup"},
                "1.2.1.MANUAL.vdt.fileGroup",
                true,
                new String[]{"1.2.1.MANUAL.file;1.2.1.MANUAL.vdt.file;1.2.1.MANUAL.fileGroup;1.2.1.MANUAL.vdt.fileGroup"});

        testResolveScopedTag(
                new String[]{"1.2.1.MANUAL.file;1.2.1.MANUAL.vdt.file;1.2.1.MANUAL.fileGroup;1.2.1.MANUAL.vdt.f0"},
                "1.2.1.MANUAL.vdt.fileGroup",
                true,
                new String[]{"1.2.1.MANUAL.file;1.2.1.MANUAL.vdt.file;1.2.1.MANUAL.fileGroup;1.2.1.MANUAL.vdt.fileGroup;1.2.1.MANUAL.vdt.f0"});

        testResolveScopedTag(
                new String[]{"1.2.1.MANUAL.file;1.2.1.MANUAL.vdt.file;1.2.1.MANUAL.vdt.fileGroup;1.2.1.MANUAL.vdt.f0"},
                "1.2.1.MANUAL.fileGroup",
                true,
                new String[]{"1.2.1.MANUAL.file;1.2.1.MANUAL.vdt.file;1.2.1.MANUAL.fileGroup;1.2.1.MANUAL.vdt.fileGroup;1.2.1.MANUAL.vdt.f0"});

        testResolveScopedTag(
                new String[]{"1.2.1.MANUAL.file;1.2.1.MANUAL.vdt.file;1.2.1.MANUAL.vdt.fileGroup;1.2.1.MANUAL.vdt.f0"},
                "1.2.1.MANUAL.vdt.fileGroup",
                true,
                new String[]{"1.2.1.MANUAL.file;1.2.1.MANUAL.vdt.file;1.2.1.MANUAL.vdt.fileGroup;1.2.1.MANUAL.vdt.f0"});

        testResolveScopedTag(
                new String[]{"1.2.1.MANUAL.file;1.2.1.MANUAL.vdt.file;1.2.1.MANUAL.fileGroup;1.2.1.MANUAL.vdt.fileGroup;1.2.1.MANUAL.vdt.f0"},
                "1.2.1.MANUAL.vdt.fileGroup",
                true,
                new String[]{"1.2.1.MANUAL.file;1.2.1.MANUAL.vdt.file;1.2.1.MANUAL.fileGroup;1.2.1.MANUAL.vdt.fileGroup;1.2.1.MANUAL.vdt.f0"});
    }


    @Test
    public void testAddTagViaDocTypeFolder(){
        testResolveScopedTag(
                new String[]{},
                "1.2.1.MANUAL.vdt.f0",
                true,
                new String[]{"1.2.1.MANUAL.vdt.f0"});

        testResolveScopedTag(
                new String[]{"1.2.1.MANUAL.f0"},
                "1.2.1.MANUAL.vdt.f0",
                true,
                new String[]{"1.2.1.MANUAL.f0;1.2.1.MANUAL.vdt.f0"});

        testResolveScopedTag(
                new String[]{"1.2.1.MANUAL.f0"},
                "1.2.2.MANUAL.vdt.f0",
                true,
                new String[]{"1.2.1.MANUAL.f0;1.2.2.MANUAL.vdt.f0"});

        testResolveScopedTag(
                new String[]{"1.2.1.MANUAL.vdt.f0"},
                "1.2.2.MANUAL.f0",
                true,
                new String[]{"1.2.2.MANUAL.f0;1.2.1.MANUAL.vdt.f0"});

        testResolveScopedTag(
                new String[]{"1.2.1.MANUAL.f0;1.2.1.MANUAL.vdt.f0"},
                "1.2.2.MANUAL.f0",
                true,
                new String[]{"1.2.2.MANUAL.f0;1.2.1.MANUAL.vdt.f0"});

        testResolveScopedTag(
                new String[]{"1.2.1.MANUAL.f0;1.2.1.MANUAL.vdt.f0"},
                "1.2.2.MANUAL.vdt.f0",
                true,
                new String[]{"1.2.1.MANUAL.f0;1.2.2.MANUAL.vdt.f0"});

        testResolveScopedTag(
                new String[]{"1.2.1.MANUAL.vdt.f0"},
                "1.2.1.MANUAL.f0",
                true,
                new String[]{"1.2.1.MANUAL.f0;1.2.1.MANUAL.vdt.f0"});

        testResolveScopedTag(
                new String[]{"1.2.1.MANUAL.file;1.2.1.MANUAL.vdt.file"},
                "1.2.1.MANUAL.f0",
                true,
                new String[]{"1.2.1.MANUAL.file;1.2.1.MANUAL.vdt.file;1.2.1.MANUAL.f0"});

        testResolveScopedTag(
                new String[]{"1.2.1.MANUAL.f0;1.2.1.MANUAL.vdt.f0"},
                "1.2.2.MANUAL.vdt.f0",
                true,
                new String[]{"1.2.1.MANUAL.f0;1.2.2.MANUAL.vdt.f0"});

        testResolveScopedTag(
                new String[]{"1.2.1.MANUAL.f0;1.2.1.MANUAL.vdt.f0"},
                "1.2.2.MANUAL.f0",
                true,
                new String[]{"1.2.2.MANUAL.f0;1.2.1.MANUAL.vdt.f0"});

        testResolveScopedTag(
                new String[]{"1.2.1.MANUAL.file;1.2.1.MANUAL.vdt.file;1.2.1.MANUAL.f0"},
                "1.2.1.MANUAL.vdt.f0",
                true,
                new String[]{"1.2.1.MANUAL.file;1.2.1.MANUAL.vdt.file;1.2.1.MANUAL.f0;1.2.1.MANUAL.vdt.f0"});

        testResolveScopedTag(
                new String[]{"1.2.1.MANUAL.file;1.2.1.MANUAL.vdt.file;1.2.1.MANUAL.vdt.f0"},
                "1.2.1.MANUAL.f1",
                true,
                new String[]{"1.2.1.MANUAL.file;1.2.1.MANUAL.vdt.file;1.2.1.MANUAL.f1;1.2.1.MANUAL.vdt.f0"});

        testResolveScopedTag(
                new String[]{"1.2.1.MANUAL.file;1.2.1.MANUAL.vdt.file;1.2.1.MANUAL.f0"},
                "1.2.1.MANUAL.vdt.f1",
                true,
                new String[]{"1.2.1.MANUAL.file;1.2.1.MANUAL.vdt.file;1.2.1.MANUAL.vdt.f1;1.2.1.MANUAL.f0"});

        testResolveScopedTag(
                new String[]{"1.2.1.MANUAL.file;1.2.1.MANUAL.vdt.file;1.2.1.MANUAL.f1;1.2.1.MANUAL.f0"},
                "1.2.1.MANUAL.vdt.f1",
                true,
                new String[]{"1.2.1.MANUAL.file;1.2.1.MANUAL.vdt.file;1.2.1.MANUAL.f1;1.2.1.MANUAL.vdt.f1;1.2.1.MANUAL.f0"});

        testResolveScopedTag(
                new String[]{"1.2.1.MANUAL.file;1.2.1.MANUAL.vdt.file;1.2.1.MANUAL.vdt.f1;1.2.1.MANUAL.f0"},
                "1.2.1.MANUAL.f1",
                true,
                new String[]{"1.2.1.MANUAL.file;1.2.1.MANUAL.vdt.file;1.2.1.MANUAL.f1;1.2.1.MANUAL.vdt.f1;1.2.1.MANUAL.f0"});

        testResolveScopedTag(
                new String[]{"1.2.1.MANUAL.file;1.2.1.MANUAL.vdt.file;1.2.1.MANUAL.vdt.f1;1.2.1.MANUAL.f0"},
                "1.2.1.MANUAL.vdt.f1",
                true,
                new String[]{"1.2.1.MANUAL.file;1.2.1.MANUAL.vdt.file;1.2.1.MANUAL.vdt.f1;1.2.1.MANUAL.f0"});

        testResolveScopedTag(
                new String[]{"1.2.1.MANUAL.file;1.2.1.MANUAL.vdt.file;1.2.1.MANUAL.f1;1.2.1.MANUAL.vdt.f1;1.2.1.MANUAL.f0"},
                "1.2.1.MANUAL.vdt.f1",
                true,
                new String[]{"1.2.1.MANUAL.file;1.2.1.MANUAL.vdt.file;1.2.1.MANUAL.f1;1.2.1.MANUAL.vdt.f1;1.2.1.MANUAL.f0"});

        testResolveScopedTag(
                new String[]{"1.2.1.MANUAL.file;1.2.1.MANUAL.vdt.file;1.2.1.MANUAL.f1;1.2.1.MANUAL.vdt.f1;1.2.1.MANUAL.f0"},
                "1.2.2.MANUAL.vdt.f1",
                true,
                new String[]{"1.2.1.MANUAL.file;1.2.1.MANUAL.vdt.file;1.2.1.MANUAL.f1;1.2.2.MANUAL.vdt.f1;1.2.1.MANUAL.f0"});

        testResolveScopedTag(
                new String[]{"1.2.1.MANUAL.file;1.2.1.MANUAL.vdt.file;1.2.1.MANUAL.f1;1.2.1.MANUAL.vdt.f1;1.2.1.MANUAL.f0"},
                "1.2.2.MANUAL.f1",
                true,
                new String[]{"1.2.1.MANUAL.file;1.2.1.MANUAL.vdt.file;1.2.2.MANUAL.f1;1.2.1.MANUAL.vdt.f1;1.2.1.MANUAL.f0"});
    }

    @Test
    public void testRemoveDocTypeTag(){
        testResolveScopedTag(
                new String[]{"1.2.1.MANUAL.file;1.2.1.MANUAL.vdt.file;1.2.1.MANUAL.fileGroup;1.2.1.MANUAL.vdt.fileGroup;1.2.1.MANUAL.vdt.f0"},
                "1.2.1.MANUAL.vdt.file",
                false,
                new String[]{"1.2.1.MANUAL.file;1.2.1.MANUAL.fileGroup;1.2.1.MANUAL.vdt.fileGroup;1.2.1.MANUAL.vdt.f0"});

        testResolveScopedTag(
                new String[]{"1.2.1.MANUAL.file;1.2.1.MANUAL.vdt.file;1.2.1.MANUAL.fileGroup;1.2.1.MANUAL.vdt.fileGroup;1.2.1.MANUAL.vdt.f0"},
                "1.2.1.MANUAL.file",
                false,
                new String[]{"1.2.1.MANUAL.vdt.file;1.2.1.MANUAL.fileGroup;1.2.1.MANUAL.vdt.fileGroup;1.2.1.MANUAL.vdt.f0"});

        testResolveScopedTag(
                new String[]{"1.2.1.MANUAL.file;1.2.1.MANUAL.vdt.file;1.2.1.MANUAL.fileGroup;1.2.1.MANUAL.vdt.fileGroup;1.2.1.MANUAL.vdt.f0"},
                "1.2.1.MANUAL.vdt.fileGroup",
                false,
                new String[]{"1.2.1.MANUAL.file;1.2.1.MANUAL.vdt.file;1.2.1.MANUAL.fileGroup;1.2.1.MANUAL.vdt.f0"});

        testResolveScopedTag(
                new String[]{"1.2.1.MANUAL.file;1.2.1.MANUAL.vdt.file;1.2.1.MANUAL.fileGroup;1.2.1.MANUAL.vdt.fileGroup;1.2.1.MANUAL.vdt.f0"},
                "1.2.1.MANUAL.vdt.f0",
                false,
                new String[]{"1.2.1.MANUAL.file;1.2.1.MANUAL.vdt.file;1.2.1.MANUAL.fileGroup;1.2.1.MANUAL.vdt.fileGroup"});

        testResolveScopedTag(
                new String[]{"1.2.1.MANUAL.file;1.2.1.MANUAL.vdt.file;1.2.1.MANUAL.f1;1.2.1.MANUAL.vdt.f1;1.2.1.MANUAL.f0"},
                "1.2.1.MANUAL.f1",
                false,
                new String[]{"1.2.1.MANUAL.file;1.2.1.MANUAL.vdt.file;1.2.1.MANUAL.vdt.f1;1.2.1.MANUAL.f0"});
    }

    private void testResolveScopedTag(String[] existingValuesBefore, String newScopedValue, boolean addCommand, String[] expectedResult) {
        PriorityScopedValueResolver priorityScopedValueResolver = new PriorityScopedValueResolver(newScopedValue);
        String[] resolveNewScopedTagValues = priorityScopedValueResolver.resolveNewScopedValues(existingValuesBefore, addCommand);
        Assert.assertArrayEquals(expectedResult, resolveNewScopedTagValues);
    }


}
