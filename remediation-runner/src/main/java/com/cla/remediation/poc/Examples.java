package com.cla.remediation.poc;

import com.cla.common.ms_graph.MSGraphClient;
import com.microsoft.graph.content.MSBatchRequestContent;
import com.microsoft.graph.content.MSBatchRequestStep;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class Examples {

    private MSGraphClient graphClient;

    public void listFiles(String path) {
        // drive by user id
        // https://graph.microsoft.com/v1.0/users/feed4508-60b1-49b8-98a8-b2468446353c/drive

        // children by drive
        // https://graph.microsoft.com/v1.0/users/feed4508-60b1-49b8-98a8-b2468446353c/drive/root/children

        // item item by drive
        // https://graph.microsoft.com/v1.0/users/feed4508-60b1-49b8-98a8-b2468446353c/drive/items/[ID]

        // site by path: (get the site id)
        // "id": "docauthority-my.sharepoint.com,1db5cdda-1d73-42f7-9300-d7e04292c883,0d2998b9-8f51-4462-9f7b-a15fc6f87720",
        // the site id is the mid one
        //https://graph.microsoft.com/v1.0/sites/docauthority-my.sharepoint.com:/personal/doug1_docauthority_onmicrosoft_com

        // drive by site id
        //https://graph.microsoft.com/v1.0/sites/1db5cdda-1d73-42f7-9300-d7e04292c883/drive

        // item id by path
        //  /me/drive/root:/path/to/file - Access a DriveItem by path relative to the user's OneDrive root folder.
        //  /me/drive/items/{item-id}:/path/to/file - Access a DriveItem by path relative to another item (a DriveItem with a folder facet).
        //  /me/drive/root:/path/to/folder:/children - List the children of a DriveItem by path relative to the root of the user's OneDrive.
        //  /me/drive/items/{item-id}:/path/to/folder:/children - List the children of a DriveItem by path relative to another item.


        Request request1 = new Request.Builder().url("https://graph.microsoft.com/v1.0/users/feed4508-60b1-49b8-98a8-b2468446353c/drive/root/children").build();

        //executeAndPrintBody(request1);


        System.out.println("------------------------------------------------------------------------------------------");

        // move item request.

        Request request2 = new Request.Builder().url("https://graph.microsoft.com/v1.0" +
                "/users" + // users api
                "/feed4508-60b1-49b8-98a8-b2468446353c" + //user id
                "/drive" + // drive api (of the selected user)
                "/items" + // items api
                "/01LGVSDW26KIQZBFTOGZFLAFUOJ6BUCJOT") // item id
                .patch(RequestBody.create(MediaType.get("application/json"), "{\"parentReference\": {\"id\": \"01LGVSDW4GCPOUBQ7BXJC3P6LEJIUJOSSL\"}}"))
                .header("Content-Type", "application/json")
                .build();

        //executeAndPrintBody(request2);

        List<MSBatchRequestStep> steps = Arrays.asList(
                new MSBatchRequestStep("1", request1, null),
                new MSBatchRequestStep("2", request2, null));

        MSBatchRequestContent requestContent = new MSBatchRequestContent(steps);
        String content = requestContent.getBatchRequestContent();

        Request batchRequest = new Request
                .Builder()
                .url("https://graph.microsoft.com/v1.0/$batch")
                .post(RequestBody.create(MediaType.parse("application/json"), content))
                .build();

        graphClient.executeRequest(batchRequest, response -> {
            try {
                System.out.println(response.body().string());
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }



    private void batchRequestExample() throws IOException {
        Request requestGetMe = new Request.Builder().url("https://graph.microsoft.com/v1.0/me/").build();
        List<String> arrayOfDependsOnIdsGetMe = null;
        MSBatchRequestStep stepGetMe = new MSBatchRequestStep("1", requestGetMe, arrayOfDependsOnIdsGetMe);

        Request requestGetMePlannerTasks = new Request.Builder().url("https://graph.microsoft.com/v1.0/me/planner/tasks").build();
        //List<String> arrayOfDependsOnIdsGetMePlannerTasks = Arrays.asList("1");
        MSBatchRequestStep stepMePlannerTasks = new MSBatchRequestStep("2", requestGetMePlannerTasks, null /*arrayOfDependsOnIdsGetMePlannerTasks*/);

        String body = "{" +
                "\"displayName\": \"My Notebook\"" +
                "}";
        RequestBody postBody = RequestBody.create(MediaType.parse("application/json"), body);
        Request requestCreateNotebook = new Request
                .Builder()
                .addHeader("Content-Type", "application/json")
                .url("https://graph.microsoft.com/v1.0/me/onenote/notebooks")
                .post(postBody)
                .build();
        MSBatchRequestStep stepCreateNotebook = new MSBatchRequestStep("3", requestCreateNotebook, Arrays.asList("2"));

        List<MSBatchRequestStep> steps = Arrays.asList(stepGetMe, stepMePlannerTasks, stepCreateNotebook);
        MSBatchRequestContent requestContent = new MSBatchRequestContent(steps);
        String content = requestContent.getBatchRequestContent();

        Request batchRequest = new Request
                .Builder()
                .url("https://graph.microsoft.com/v1.0/$batch")
                .post(RequestBody.create(MediaType.parse("application/json"), content))
                .build();

        //Response batchResponse = httpClient.newCall(batchRequest).execute();
    }
}
