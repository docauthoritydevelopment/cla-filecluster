package com.cla.remediation;

import com.cla.common.ms_graph.MsGraphParams;
import com.cla.common.ms_graph.throttling.ThrottlingStartledTurtle;
import com.cla.remediation.params.RemediationParams;
import com.cla.remediation.processor.RemediationProcessor;
import picocli.CommandLine;

import java.util.concurrent.Callable;

import static picocli.CommandLine.*;

@Command(name = "policy-applier", mixinStandardHelpOptions = true, version = "1.0",
        description = "run remediation operations on files...")
public class RemediationCli implements Callable<Integer> {

    private static final int DEFAULT_BATCH_SIZE = 20;

    public static int DEFAULT_CONNECTION_TIMEOUT = 20;

    public static int DEFAULT_CONNECTION_READ_TIMEOUT = 20;

    @Parameters(index = "0", description = "Input instructions file.")
    private String policyFile;

    @Option(names = {"-t", "--tenant"}, description = "Tenant Id for OAuth connection", required = true)
    private String tenantId;

    @Option(names = {"-a", "--app"}, description = "Application Id for OAuth connection", required = true)
    private String applicationId;

    @Option(names = {"-p", "--password"}, description = "Password for OAuth connection", required = true)
    private String password;

    @Option(names = {"-dd", "--destination-drive"},
            description = "the destination drive to copy the files to ex. \"/personal/user_name_domain_onmicrosoft_com\"",
            required = true)
    private String destinationDrive;

    @Option(names = {"-dp", "--destination-path"},
            description = "the destination path to copy the files to ex.\"aaa\"",
            required = true)
    private String destinationPath;

    @Option(names = {"-ct", "--connection-timeout"},
            description = "the timeout of construction a connection to the client")
    private long connectionTimeout = DEFAULT_CONNECTION_TIMEOUT;

    @Option(names = {"-crt", "--connection-read-timeout"},
            description = "the timeout of executing an api call to the client")
    private long connectionReadTimeout = DEFAULT_CONNECTION_READ_TIMEOUT;

    @Option(names = {"-bs", "--batch-size"},
            description = "the batch size of bulk operation")
    private int batchSize = DEFAULT_BATCH_SIZE;

    @Option(names = {"-ln", "--line-number"},
            description = "the line number from where to continue")
    private int lineNumber = 0;


    public static void main(String[] args) {
        int exitCode = new CommandLine(new RemediationCli()).execute(args);
        System.exit(exitCode);
    }

    @Override
    public Integer call() throws Exception {
        // get params from args (OAuth - AppId, Password, TenantId)
        ThrottlingStartledTurtle throttler = new ThrottlingStartledTurtle();
        MsGraphParams msGraphParams = MsGraphParams.builder()
                .addConnectionParams()
                .withApplicationId(applicationId)
                .withConnectionConnectTimeout(connectionTimeout)
                .withConnectionReadTimeout(connectionReadTimeout)
                .withPassword(password)
                .withTenantId(tenantId)
                .done()
                .addThrottlingParams()
                .withMaxRetriesOnThrottling(5)
                .withThrottler(throttler)
                .done()
                .addAuthConfig()
                .withConnectTimeout(connectionTimeout)
                .withMaxAttempts(3)
                .withPrefetchTokenBeforeExpiry(10000)
                .withReadTimeout(10000)
                .withRetrySleepInterval(10000)
                .done()
                .withGraphBatchMaxSize(batchSize)
                .build();

        RemediationParams remediationParams = RemediationParams.builder()
                .withBatchSize(batchSize) // currently must be set to graph max batch size (20)
                .withMaxOpenConnections(5)
                .withDestinationDrive(destinationDrive)
                .withDestinationPath(destinationPath)
                .build();


        RemediationProcessor processor = new RemediationProcessor(msGraphParams, remediationParams, lineNumber);
        processor.processListFile(policyFile);

        return 0;
    }
}
