package com.cla.remediation.params;

public class RemediationParams {

    private final int batchSize;
    private final int maxOpenConnections;
    private final String destinationDrive;
    private final String destinationPath;

    private RemediationParams(Builder builder) {
        this.batchSize = builder.batchSize;
        this.maxOpenConnections = builder.maxOpenConnections;
        this.destinationDrive = builder.destinationDrive;
        this.destinationPath = builder.destinationPath;
    }

    public static RemediationParams.Builder builder() {
        return new RemediationParams.Builder();
    }

    public int getBatchSize() {
        return batchSize;
    }

    public int getMaxOpenConnections() {
        return maxOpenConnections;
    }

    public String getDestinationDrive() {
        return destinationDrive;
    }

    public String getDestinationPath() {
        return destinationPath;
    }

    public static final class Builder {
        public int maxOpenConnections;
        private int batchSize;
        private String destinationDrive;
        private String destinationPath;

        public RemediationParams.Builder withBatchSize(int batchSize) {
            this.batchSize = batchSize;
            return this;
        }

        public RemediationParams.Builder withMaxOpenConnections(int maxOpenConnections) {
            this.maxOpenConnections = maxOpenConnections;
            return this;
        }

        public RemediationParams.Builder withDestinationDrive(String destination) {
            this.destinationDrive = destination;
            return this;
        }

        public RemediationParams.Builder withDestinationPath(String destinationPath) {
            this.destinationPath = destinationPath;
            return this;
        }

        public RemediationParams build() {
            return new RemediationParams(this);
        }
    }
}
