package com.cla.remediation.util;

import org.apache.commons.lang3.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PathUtils {

    private static final String SUBSITE_REGEX = "^.*?/personal/[^/]+";

    public static String extractDomain(String baseUrl) {
        Matcher matcher = Pattern.compile("https?://([a-zA-Z0-9@:%._\\\\+~#=\\-]{1,256})").matcher(baseUrl);
        if (matcher.find()) {
            return matcher.group(1);
        }
        throw new RuntimeException(String.format("could not extract domain from url: %s. Aborting!", baseUrl));
    }

    public static String extractRelativePath(String path, String domain) {
        int index = StringUtils.lastIndexOfIgnoreCase(path, domain);
        return index >=0 ? path.substring(index + domain.length()) : "";
    }

    public static String combinePaths(String... subPaths) {
        if (subPaths.length == 0) {
            return "";
        }
        StringBuilder output = new StringBuilder();
        String subPath = StringUtils.stripEnd(subPaths[0], "/");
        output.append(subPath);
        String separator = subPath.isEmpty() ? "" : "/";
        for (int i = 1 ; i < subPaths.length ; i++) {
            subPath = StringUtils.stripStart(subPaths[i], "/");
            output.append(separator).append(subPath);
            separator = subPath.isEmpty() ? "" : "/";
        }
        return output.toString();
    }

    public static String extractSiteUrl(String sourcePath) {
        Matcher matcher = Pattern.compile(SUBSITE_REGEX).matcher(sourcePath);
        if (matcher.find()) {
            return matcher.group(0);
        }
        throw new RuntimeException(String.format("error extracting sub-site for: %s", sourcePath));
    }

    public static String shortPathToActual(String path, String baseUrl) {
        path = StringUtils.removeStart(path, "/");
        path = StringUtils.removeStart(path, "personal/");
        String localPath = StringUtils.substringAfter(path, "/");
        String user = StringUtils.substringBefore(path, "/");
        user = user.replaceAll("@", "_").replaceAll("\\.", "_");
        baseUrl = baseUrl.endsWith("/") ? baseUrl : baseUrl + "/";
        return baseUrl +
                "personal/" +
                user +
                "/Documents/" +
                localPath;
    }

    public static String getDirectoryName(String path) {
        if (path.endsWith("/")) {
            path = path.substring(0, path.length() - 2);
        }
        return StringUtils.substringAfterLast(path, "/");
    }
}
