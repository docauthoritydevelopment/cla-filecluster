package com.cla.remediation.ms_graph;

import com.cla.common.ms_graph.MSGraphClient;
import com.cla.common.ms_graph.MsGraphParams;
import com.cla.remediation.processor.operation.GlobalItemId;
import com.cla.remediation.util.tree.TreeNode;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.cla.remediation.util.PathUtils.*;

public class MSGraphRemediationConnector {

    private static final long ENSURE_COPIED_TIMEOUT_MILLIS = 20000;

    private static final Logger log = LoggerFactory.getLogger(MSGraphRemediationConnector.class);
    private final int graphBatchMaxSize;

    private MSGraphClient graphClient;

    public MSGraphRemediationConnector(MsGraphParams graphParams) {
        this.graphClient = new MSGraphClient(graphParams);
        this.graphBatchMaxSize = graphParams.getGraphBatchMaxSize();
    }

    public Map<String, String> getSiteIdsByPaths(Set<String> paths) {
        if (paths.isEmpty()) {
            return Maps.newHashMap();
        }

        // generate a list of requests for path -> site
        List<String> pathsList = new ArrayList<>(paths);

        List<Request> sitesByPathsRequests = pathsList.stream()
                .map(this::getSiteByPathRequest)
                .collect(Collectors.toList());

        Map<String, String> result = Maps.newHashMap();

        // execute and get site id's
        graphClient.createBatchRequest(sitesByPathsRequests).ifPresent(request ->
                graphClient.executeRequestJsonResponse(request)
                        .forEach(response -> {
                            int responseIndex = Integer.parseInt(response.get("id").getAsString());
                            int status = response.get("status").getAsInt();
                            if (status >= 200 && status < 300) {
                                String siteId = response.get("body").getAsJsonObject().get("id").getAsString();
                                result.put(pathsList.get(responseIndex), siteId);
                            } else {
                                log.warn("error extracting site id for {}", pathsList.get(responseIndex));
                            }

                        }));

        return result;
    }

    private Request getDriveBySiteIdRequest(String siteId) {
        //https://graph.microsoft.com/v1.0/sites/1db5cdda-1d73-42f7-9300-d7e04292c883/drive

        return new Request.Builder()
                .url(MSGraphClient.MS_GRAPH_URL +
                        "/sites/" + // sites api
                        siteId +
                        "/drive")
                .get()
                .build();
    }

    private Request getSiteByPathRequest(String path) {
        // site by path: (get the site id)
        // "id": "docauthority-my.sharepoint.com,1db5cdda-1d73-42f7-9300-d7e04292c883,0d2998b9-8f51-4462-9f7b-a15fc6f87720",
        // the site id is the mid one
        //https://graph.microsoft.com/v1.0/sites/docauthority-my.sharepoint.com:/personal/doug1_docauthority_onmicrosoft_com
        String domain = extractDomain(path);
        String relativePath = extractRelativePath(path, domain);

        return new Request.Builder()
                .url(MSGraphClient.MS_GRAPH_URL +
                        "/sites/" + // sites api
                        domain +
                        ":" + //
                        relativePath)
                .get()
                .build();
    }

    /**
     * @param pathsAndSites pair of path, site-id
     * @return
     */
    public Map<Pair<String, String>, String> getItemIdsByPathsAndSites(List<Pair<String, String>> pathsAndSites) {
        // create batch request
        List<Request> getItemRequests = pathsAndSites.stream()
                .map(pair -> getItemIdByPathAndSite(pair.getLeft(), pair.getRight()))
                .collect(Collectors.toList());

        // create a map of the results
        Map<Pair<String, String>, String> result = Maps.newHashMap();

        // execute
        graphClient.createBatchRequest(getItemRequests).ifPresent(batchRequest -> {

            Stream<JsonObject> responseStream = graphClient.executeRequestJsonResponse(batchRequest);

            responseStream.forEach(responseObj -> {
                int responseId = Integer.parseInt(responseObj.get("id").getAsString());
                Pair<String, String> pathSite = pathsAndSites.get(responseId);
                int status = responseObj.get("status").getAsInt();
                if (status >= 200 && status < 300 && responseObj.has("body")) {
                    String itemId = responseObj.get("body").getAsJsonObject().get("id").getAsString();
                    result.put(pathSite, itemId);
                } else {
                    log.warn("error getting Item-id for site {}, path: {} status: {}", pathSite.getRight(), pathSite.getLeft(), status);
                }
            });
        });

        return result;
    }

    private Request getItemIdByPathAndSite(String relativePath, String siteId) {
        //https://graph.microsoft.com/v1.0/sites/1db5cdda-1d73-42f7-9300-d7e04292c883/drive
        //  /me/drive/root:/path/to/file - Access a DriveItem by path relative to the user's OneDrive root folder.
        return new Request.Builder()
                .url(MSGraphClient.MS_GRAPH_URL +
                        "/sites/" + // sites api
                        siteId +
                        "/drive/root:" +
                        relativePath)
                .get()
                .build();

    }

    /**
     * get's or create a target id
     * try to get the specified path, if not available, try to create it.
     *
     * @param targetPath the path to create/get
     * @return a GlobalItemId of the needed target.
     */
    public GlobalItemId getOrCreateTargetId(String targetPath) {
        //https://graph.microsoft.com/v1.0/me/drive/root:/path/to/destination
        String sitePath = extractSiteUrl(targetPath);

        String siteId = getSiteId(sitePath);

        String relativePath = extractRelativePath(targetPath, combinePaths(sitePath, "Documents"));

        GlobalItemId target = getTarget(siteId, relativePath).map(Optional::of)
                .orElseGet(() -> createTarget(siteId, null, relativePath))
                .orElseThrow(() -> new RuntimeException("Error resolving target:" + targetPath));

        target.setName(relativePath);

        return target;
    }

    private void createMissingTargets(String siteId, List<TreeNode<Pair<String, GlobalItemId>>> targetPaths) {
        if (targetPaths == null || targetPaths.isEmpty()) {
            log.warn("got an empty target list");
        }

        List<Request> requests = targetPaths.stream()
                .map(targetPath -> {
                    if (targetPath.data.getValue() == null) {
                        // target not created - proceed to create
                        if (targetPath.parent != null && targetPath.parent.data.getValue() != null) {
                            // parent exists and has item-id
                            return createDirectoryRequest(siteId, targetPath.parent.data.getValue().getItemId(), targetPath.data.getKey());
                        } else {
                            log.error("Skipping node: parent does not exist or does not have item id: {}", targetPath.data);
                        }
                    }
                    return null;
                })
                .collect(Collectors.toList());

        graphClient.createBatchRequest(requests).ifPresent(batchRequest ->
                graphClient.executeRequestJsonResponse(batchRequest).forEach(response -> {
                    int status = response.get("status").getAsInt();
                    int index = response.get("id").getAsInt();

                    if (status >= 200 && status < 300) {
                        TreeNode<Pair<String, GlobalItemId>> node = targetPaths.get(index);
                        extractGlobalItemId(response).ifPresent(item -> node.data = Pair.of(node.data.getKey(), item));
                    } else {
                        log.error("Error creating target for index: {} - error {} - response {}", index, status, response);
                    }
                }));
    }

    /**
     * try to get the target ids of the paths specified in the provided list
     *
     * @param relativePaths a list of target paths
     * @return a map of index(on the original list) -> target-id
     */
    private Map<Integer, GlobalItemId> getTargets(String siteId, List<String> relativePaths) {
        if (relativePaths == null || relativePaths.isEmpty()) {
            log.warn("got an empty target list");
            return Maps.newHashMap();
        }

        Map<Integer, GlobalItemId> result = Maps.newHashMap();

        List<Request> requests = relativePaths.stream()
                .map(relativePath ->
                        getItemIdByPathAndSite(relativePath, siteId))
                .collect(Collectors.toList());

        graphClient.createBatchRequest(requests).ifPresent(batchRequsest ->
                graphClient.executeRequestJsonResponse(batchRequsest).forEach(response -> {
                    int index = response.get("id").getAsInt();
                    int status = response.get("status").getAsInt();

                    switch (status) {
                        case 404:
                            // not found - ignore
                            break;
                        case 200:
                            extractGlobalItemId(response).ifPresent(item -> result.put(index, item));
                            break;
                        default:
                            log.error("error getting target for request no {} - error: {} - response: {}", index, status, response);
                    }
                }));

        return result;
    }

    private String getSiteId(String sitePath) {
        Request getSiteRequest = getSiteByPathRequest(sitePath);
        JsonObject siteResponseObj = graphClient.executeRequestJsonSingleResponse(getSiteRequest);
        graphClient.checkResponse(siteResponseObj, String.format("Could not get siteId for path: %s", sitePath));
        String siteIdCompositeId = siteResponseObj.get("body").getAsJsonObject().get("id").getAsString();
        return siteIdCompositeId.split(",")[1];
    }

    private Optional<GlobalItemId> getTarget(String siteId, String relativePath) {
        // get taretId by site and relative path
        //https://graph.microsoft.com/v1.0/me/drive/root:/path/to/destination
        Request getTargetRequest = getItemIdByPathAndSite(relativePath, siteId);
        JsonObject targetResponseObj = graphClient.executeRequestJsonSingleResponse(getTargetRequest);
        switch (targetResponseObj.get("status").getAsInt()) {
            case 404:
                return Optional.empty();
            case 200:
                return extractGlobalItemId(targetResponseObj);
            default:
                graphClient.checkResponse(targetResponseObj,
                        "Error while resolving target: " + relativePath + " for site: " + siteId);
        }
        throw new RuntimeException("unexpected Exception");
    }

    public Optional<GlobalItemId> createTarget(String siteId, String parentId, String directoryName) {
        Request request = createDirectoryRequest(siteId, parentId, directoryName);

        JsonObject response = graphClient.executeRequestJsonSingleResponse(request);
        int status = response.get("status").getAsInt();
        if (status >= 200 && status < 300) {
            return extractGlobalItemId(response);
        }
        return Optional.empty();
    }

    private Request createDirectoryRequest(String siteId, String parentItemId, String newDirName) {
    /*POST https://graph.microsoft.com/v1.0/sites/1db5cdda-1d73-42f7-9300-d7e04292c883/drive/root/children
    Content-Type: application/json

    {
        "name": "New Folder",
        "folder": { },
        "@microsoft.graph.conflictBehavior": "rename"
    }*/
        newDirName = StringUtils.strip(newDirName, "/");
        String body = "{" +
                "\"name\": \"" + newDirName + "\", " +
                "\"folder\": { }, " +
                "\"@microsoft.graph.conflictBehavior\": \"rename\"" +
                "}";
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), body);

        String parent = parentItemId == null ? "root" : "items/" + parentItemId;

        return new Request.Builder()
                .url(MSGraphClient.MS_GRAPH_URL +
                        "/sites/" +
                        siteId +
                        "/drive/" +
                        parent +
                        "/children")
                .addHeader("Content-Type", "application/json")
                .post(requestBody)
                .build();
    }

    private Optional<GlobalItemId> extractGlobalItemId(JsonObject driveItemResponse) {
        JsonObject responseBody = driveItemResponse.get("body").getAsJsonObject();
        String itemId = responseBody.get("id").getAsString();
        String driveId = responseBody.get("parentReference").getAsJsonObject().get("driveId").getAsString();
        return Optional.of(GlobalItemId.of(itemId, driveId));
    }

    public void moveItems(List<Pair<String, String>> itemsToMove, String targetId) {
        List<Request> requests = itemsToMove.stream()
                .map(item -> moveItemRequest(item, targetId))
                .collect(Collectors.toList());

        graphClient.createBatchRequest(requests).ifPresent(batchRequest ->
                graphClient.executeRequestJsonResponse(batchRequest)
                        .forEach(response -> {
                            int id = response.get("id").getAsInt();
                            System.out.println("file: " + itemsToMove.get(id));
                            int status = response.get("status").getAsInt();
                            System.out.println("Status: " + status);
                            if (status >= 200 && status < 300) {
                                System.out.println(response.get("body").getAsJsonObject().toString());
                            }
                        }));
    }

    private Request moveItemRequest(Pair<String, String> item, String targetId) {
        // move item request.
        return new Request.Builder().url(MSGraphClient.MS_GRAPH_URL +
                "/sites/" + // sites api
                item.getLeft() + // site id
                "/drive/items/" + // items api
                item.getRight()) // item id
                .patch(RequestBody.create(MediaType.get("application/json"), "{\"parentReference\": {\"id\": \"" + targetId + "\"}}"))
                .header("Content-Type", "application/json")
                .build();
    }

    /**
     * @param itemsToCopy list of pair of <source, destination>
     * @return sources of copied items
     */
    public List<GlobalItemId> copyItems(List<Pair<GlobalItemId, GlobalItemId>> itemsToCopy) {

        List<Request> copyRequests = itemsToCopy.stream()
                .map(item -> createCopyItemRequest(item.getLeft(), item.getRight()))
                .collect(Collectors.toList());

        Request batchRequest = graphClient.createBatchRequest(copyRequests)
                .orElseThrow(() ->
                        new RuntimeException(String.format("Could not create batch request for targets list: %s", itemsToCopy)));

        Map<GlobalItemId, String> monitors = graphClient.executeRequestJsonResponse(batchRequest)
                .map(response -> {
                    int status = response.get("status").getAsInt();
                    int batchId = Integer.parseInt(response.get("id").getAsString());
                    GlobalItemId item = itemsToCopy.get(batchId).getLeft();
                    if (status >= 200 && status < 300) {
                        String location = response.get("headers").getAsJsonObject().get("Location").getAsString();
                        return Pair.of(batchId, location);
                    }
                    String messageBody = response.has("body") ? response.get("body").toString() : "";
                    log.warn("batch item {} for item - name: {}, DriveId: {}, ItemId: {} wasn't able to be copied. error {} - {}",
                            batchId, item.getName(), item.getDriveId(), item.getItemId(), status, messageBody);
                    return null;

                })
                .filter(Objects::nonNull)
                // a map of full item-id to monitor link
                .collect(Collectors.toMap(pair -> itemsToCopy.get(pair.getLeft()).getLeft(), Pair::getValue));

        return ensureCopied(monitors);
    }

    /**
     * @param monitors
     * @return
     */
    private List<GlobalItemId> ensureCopied(Map<GlobalItemId, String> monitors) {
        List<GlobalItemId> result = new ArrayList<>();
        Map<GlobalItemId, String> monitorsCopy = Maps.newHashMap(monitors);
        long startTime = System.currentTimeMillis();
        while (!monitorsCopy.isEmpty()) {
            // check timeout
            if (System.currentTimeMillis() - startTime > ENSURE_COPIED_TIMEOUT_MILLIS) {
                log.warn("Warning! Ensure copied timeout - returning partial results");
                break;
            }
            Iterator<Map.Entry<GlobalItemId, String>> monitorsIterator = monitorsCopy.entrySet().iterator();
            while (monitorsIterator.hasNext()) {
                Map.Entry<GlobalItemId, String> monitor = monitorsIterator.next();
                try {
                    if (monitorCompleted(monitor.getValue())) {
                        // copy done, add current file to results and remove current monitor
                        result.add(monitor.getKey());
                        monitorsIterator.remove();
                    }
                } catch (Exception e) {
                    // monitor encountered an error  - report, remove from monitors, do not add to results
                    log.warn("Error ensuring file was copied - File will be skipped. Item ID: {} {}", monitor.getKey().getDriveId(), monitor.getKey().getItemId(), e);
                    monitorsIterator.remove();
                }
            }
            // sleep.... but only if we will re-enter on the next loop...
            if (!monitorsCopy.isEmpty() && System.currentTimeMillis() - startTime < ENSURE_COPIED_TIMEOUT_MILLIS) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    log.warn("ensureCopied interrupted - quitting");
                    break;
                }
            }
        }

        return result;
    }

    private boolean monitorCompleted(String url) throws Exception {
        Request request = new Request.Builder()
                .url(url)
                .addHeader("Content-Type", "application/json")
                .get()
                .build();

        Response response = graphClient.executeUnauthenticated(request);
        if (response.code() >= 200 && response.code() < 300) {
            JsonObject body = new JsonParser().parse(response.body().string()).getAsJsonObject();
            String progressStatus = body.has("status") ? body.get("status").getAsString() : "";
            if ("failed".equals(progressStatus)) {
                //TODO log on more errors?
                log.error("monitor indicates a failed copy operation: {}", body.toString());
            }
            return "completed".equals(progressStatus);
        } else {
            // TODO problem getting monitor... throw?
        }
        return false;
    }


    private Request createCopyItemRequest(GlobalItemId source, GlobalItemId target) {
        // workaround - renaming the file due to ms-graph api issue (bug): https://github.com/OneDrive/onedrive-api-docs/issues/980
        // workaround - use "Sites.ReadWrite.All" and "Sites.Read.All" permissions instead of "Files.ReadWrite.All" and "Files.Read.All"
        String body = "{\n" +
                "  \"parentReference\": {\n" +
                "    \"driveId\": \"" + target.getDriveId() + "\",\n" +
                "    \"id\": \"" + target.getItemId() + "\"\n" +
                "  }\n" + // "  },\n" +
                //"  \"name\": \"copied-" + source.getName() + "\" " +
                "}";

        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), body);

        return new Request.Builder()
                .url(MSGraphClient.MS_GRAPH_URL +
                        "/drives/" + // sites api
                        source.getDriveId() + // site id
                        "/items/" + // items api
                        source.getItemId() +  // item id)
                        "/copy")
                .addHeader("Content-Type", "application/json")
                .post(requestBody)
                .build();
    }

    /**
     * deletes the supplied list of items
     *
     * @param itemsToDelete Pair of <site-id, item-id> of items to delete
     */
    public void deleteItems(List<GlobalItemId> itemsToDelete) {
        if (itemsToDelete.isEmpty()) {
            return;
        }

        List<Request> requests = itemsToDelete.stream().map(this::deleteItemRequest).collect(Collectors.toList());

        Request batchRequest = graphClient.createBatchRequest(requests)
                .orElseThrow(() ->
                        new RuntimeException(String.format("Could not create batch request for list: %s", itemsToDelete)));

        graphClient.executeRequestJsonResponse(batchRequest).forEach(response -> {
            int status = response.get("status").getAsInt();
            int batchId = Integer.parseInt(response.get("id").getAsString());
            GlobalItemId item = itemsToDelete.get(batchId);
            if (status < 200 || status >= 300) {
                String messageBody = response.has("body") ? response.get("body").toString() : "";
                log.error("error deleting item: {} {} error {} body {}",
                        item.getDriveId(), item.getItemId(), status, messageBody);
            }
        });
    }

    private Request deleteItemRequest(GlobalItemId item) {
        return new Request.Builder()
                .url(MSGraphClient.MS_GRAPH_URL +
                        "/drives/" + // drives api
                        item.getDriveId() + // drive id
                        "/items/" + // items api
                        item.getItemId()) // item id
                .delete(RequestBody.create(MediaType.parse("application/json"), "{}"))
                .header("Content-Type", "application/json")
                .build();
    }

    public Map<String, String> getDriveIdsBySites(Set<String> sites) {
        if (sites == null || sites.isEmpty()) {
            log.warn("got empty sites collection");
            return Maps.newHashMap();
        }

        List<String> sitesList = new ArrayList<>(sites);

        Map<String, String> result = Maps.newHashMap();

        List<Request> requests = sitesList.stream()
                .map(this::driveBySiteRequest)
                .collect(Collectors.toList());

        Request batchRequest = graphClient.createBatchRequest(requests)
                .orElseThrow(() ->
                        new RuntimeException(String.format("Could not create batch request for list: %s", sitesList)));

        graphClient.executeRequestJsonResponse(batchRequest).forEach(response -> {
            int batchId = response.get("id").getAsInt();
            int status = response.get("status").getAsInt();
            String siteId = sitesList.get(batchId);
            if (status >= 200 && status < 300) {
                String driveId = response.get("body").getAsJsonObject().get("id").getAsString();
                result.put(siteId, driveId);
            } else {
                log.warn("Error processing batch {} site {} status {} error {}",
                        batchId, siteId, status, response.has("body") ? response.get("body").getAsJsonObject().toString() : "");
            }
        });

        return result;
    }

    private Request driveBySiteRequest(String siteId) {
        return new Request.Builder()
                .url(MSGraphClient.MS_GRAPH_URL +
                        "/sites/" + // sites api
                        siteId +
                        "/drive") // get drive
                .get()
                .build();
    }

    public Map<String, GlobalItemId> getOrCreateDestinations(final Set<String> sourcePaths, String targetPath) {

        // create a destination list
        TreeNode<Pair<String, GlobalItemId>> destinationsTree = createInitialDestinationsTree(targetPath);
        fillDestinationsTree(sourcePaths, destinationsTree);

        // assuming same site for all targets - get the target site id
        String sitePath = extractSiteUrl(targetPath);
        String siteId = getSiteId(sitePath);

        // create initial target path
        GlobalItemId targetId = getOrCreateTargetId(targetPath);
        destinationsTree.data = Pair.of(destinationsTree.data.getKey(), targetId);

        List<TreeNode<Pair<String, GlobalItemId>>> currDepth = Collections.singletonList(destinationsTree);

        while (!currDepth.isEmpty()) {
            // divide to batch size
            Iterable<List<TreeNode<Pair<String, GlobalItemId>>>> batches = Iterables.partition(currDepth, graphBatchMaxSize);
            batches.forEach(batch -> {
                // get or create...
                getOrCreateBatch(siteId, batch);
            });

            // get next depth
            List<TreeNode<Pair<String, GlobalItemId>>> nextDepth = new LinkedList<>();
            currDepth.forEach(node -> nextDepth.addAll(node.children));

            currDepth = nextDepth;

        }

        return buildSourcePathToTarget(sourcePaths, destinationsTree);
    }

    private Map<String, GlobalItemId> buildSourcePathToTarget(Set<String> sourcePaths, TreeNode<Pair<String, GlobalItemId>> destinationsTree) {
        Map<String, GlobalItemId> result = Maps.newHashMap();
        Map<TreeNode<Pair<String, GlobalItemId>>, String> nodeToPath = new HashMap<>();
        destinationsTree.children.forEach(pairTreeNode ->
                pairTreeNode.forEach(child -> {
                    String parentPath = nodeToPath.get(child.parent);
                    String sourcePath = parentPath == null ? child.data.getKey() : parentPath + child.data.getKey();
                    nodeToPath.put(child, sourcePath);
                    // Add to result if sourcePath is contained (with "/" or without it) within the listed sourcePaths
                    if (sourcePaths.contains(sourcePath)) {
                        result.put(sourcePath, child.data.getValue());
                    } else if (sourcePaths.contains(sourcePath + "/")) {
                        result.put(sourcePath + "/", child.data.getValue());
                    }
                }));
        return result;
    }

    private void getOrCreateBatch(String siteId, List<TreeNode<Pair<String, GlobalItemId>>> batch) {

        List<String> reconstructedPaths = batch.stream().map(this::reconstructPath).collect(Collectors.toList());

        // get and update ids on tree
        Map<Integer, GlobalItemId> foundTargets = getTargets(siteId, reconstructedPaths);
        foundTargets.forEach((index, itemId) -> {
            TreeNode<Pair<String, GlobalItemId>> node = batch.get(index);
            node.data = Pair.of(node.data.getKey(), itemId);
        });

        // crate missing
        createMissingTargets(siteId, batch);
    }

    private String reconstructPath(TreeNode<Pair<String, GlobalItemId>> node, boolean includeRoot) {
        StringBuilder path = new StringBuilder(node.data.getKey());
        while (node.parent != null && (includeRoot || node.parent.parent != null)) {
            path.insert(0, node.parent.data.getKey());
            node = node.parent;
        }
        return path.toString();
    }

    private String reconstructPath(TreeNode<Pair<String, GlobalItemId>> node) {
        return reconstructPath(node, true);
    }

    private TreeNode<Pair<String, GlobalItemId>> createInitialDestinationsTree(String targetBasePath) {
        if (targetBasePath.endsWith("/")) {
            targetBasePath = targetBasePath.substring(0, targetBasePath.length() - 1);
        }
        String targetDir = StringUtils.substringAfterLast(targetBasePath, "/");
        return new TreeNode<>(Pair.of("/" + targetDir, null));
    }

    /**
     * fill in and creates a tree structure to store directory names
     *
     * @param sourcePaths      the source paths to copy
     * @param destinationsTree a root node of a tree to fill
     */
    private void fillDestinationsTree(Set<String> sourcePaths, TreeNode<Pair<String, GlobalItemId>> destinationsTree) {
        if (sourcePaths == null || sourcePaths.isEmpty()) {
            log.warn("got empty sources list");
            return;
        }

        // multimap of - direct child -> rest-of-path
        Multimap<String, String> pathsByRoot = HashMultimap.create();

        sourcePaths.forEach(sourcePath -> {
            if (!sourcePath.startsWith("/")) {
                sourcePath = "/" + sourcePath;
            }
            int nextPosition = sourcePath.indexOf("/", 1);
            if (nextPosition >= 0) {
                String directChild = sourcePath.substring(0, nextPosition);
                String restOfPath = sourcePath.substring(nextPosition);

                pathsByRoot.put(directChild, restOfPath);
            }
        });

        pathsByRoot.asMap().forEach((rootPath, childPaths) -> {

            // fill in direct child
            TreeNode<Pair<String, GlobalItemId>> child = destinationsTree.addChild(Pair.of(rootPath, null));

            Set<String> newSourcePahts = childPaths.stream()
                    .filter(StringUtils::isNotEmpty)
                    .collect(Collectors.toSet());

            // call recursively to fill sub-paths
            fillDestinationsTree(newSourcePahts, child);
        });
    }
}
