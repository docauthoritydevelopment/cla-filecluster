package com.cla.remediation.processor.operation;

import com.cla.connector.domain.dto.media.MediaType;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class RemediationOperation {
    private final String operation;
    private final String fullOperationLine;
    private final int lineIndex;
    private final MediaType mediaType;
    private final String baseUrl;
    private final String sourcePath;
    private final String sourceFileName;
    private final String targetPath;
    private String siteHostName;
    private String siteCollectionId;
    private String siteId;
    private String driveId;
    private String itemId;
    private GlobalItemId resolvedTargetId;
}
