package com.cla.remediation.processor.operation;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.ws.rs.GET;

@Builder
@RequiredArgsConstructor(staticName="of")
@AllArgsConstructor
public class GlobalItemId {
    @Getter private final String itemId;
    @Getter private final String driveId;
    @Getter @Setter private String name;
}
