package com.cla.remediation.processor;

import com.cla.common.ms_graph.MsGraphParams;
import com.cla.connector.domain.dto.media.MediaType;
import com.cla.remediation.ms_graph.MSGraphRemediationConnector;
import com.cla.remediation.params.RemediationParams;
import com.cla.remediation.processor.operation.GlobalItemId;
import com.cla.remediation.processor.operation.RemediationOperation;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Flux;
import reactor.core.scheduler.Schedulers;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.cla.remediation.util.PathUtils.*;

public class RemediationProcessor {

    private static final Logger log = LoggerFactory.getLogger(RemediationProcessor.class);
    private static final String FIELD_SEPARATOR = ":::";
    private static final int CACHE_MAX_ELEMENTS = 1000;
    public static final String MOVE_OPERATION = "MOVE";
    public static final String COPY_OPERATION = "COPY";
    public static final List<String> SUPPORTED_OPERATIONS = Lists.newArrayList(COPY_OPERATION, MOVE_OPERATION);

    private final RemediationParams remediationParams;
    private final MSGraphRemediationConnector graphConnector;

    /**
     * Cache of - path -> Composite-Site-ID
     */
    private final Cache<String, String> siteIdCache;

    /**
     * Cache of - Site-Collection-ID -> Drive-ID
     */
    private final Cache<String, String> driveIdCache;

    /**
     * Cache of - Source-Path -> Destination-Item-ID
     */
    private final Cache<String, GlobalItemId> destinationCache;
    private final int lineNumber;
    private AtomicInteger lineIndex;


    public RemediationProcessor(MsGraphParams msGraphParams, RemediationParams remediationParams, int lineNumber) {
        this.graphConnector = new MSGraphRemediationConnector(msGraphParams);
        this.remediationParams = remediationParams;

        siteIdCache = CacheBuilder
                .newBuilder()
                .maximumSize(CACHE_MAX_ELEMENTS)
                .build();

        driveIdCache = CacheBuilder
                .newBuilder()
                .maximumSize(CACHE_MAX_ELEMENTS)
                .build();

        destinationCache = CacheBuilder
                .newBuilder()
                .maximumSize(CACHE_MAX_ELEMENTS)
                .build();
        this.lineNumber = lineNumber;
    }

    /**
     * resolve the destination to move the files to -
     * target drive - from cli parameters
     * target folder - gets or creates the destination folder     *
     *
     * @param targetPath the target path to resolve (get or create)
     * @return pair of <drive-id, item-id>
     */
    private GlobalItemId resolveDestination2(String targetPath) {
        GlobalItemId destination = null; // TEMP - was a field
        // get destination id
        // if doesn't exist, create one
        if (destination == null) {
            synchronized (this) {
                if (destination == null) {
                    destination = Optional.of(graphConnector.getOrCreateTargetId(targetPath))
                            .orElseThrow(() -> new RuntimeException(
                                    String.format("could not resolve destination: " +
                                            "drive: %s, directory: %s - Aborting!", targetPath)));
                }
            }
        }
        return destination;
    }

    public void processListFile(String pathStr) {
        // reactive... operation -> stream of lines -> getInfo -> perform action -> aggregate result
        try {
            Stream<String> lineStream = Files.lines(Paths.get(pathStr));
            lineIndex = new AtomicInteger(0);
            Flux.fromStream(lineStream)
                    .doOnNext(line -> lineIndex.getAndIncrement())
                    .filter(this::needToProcess)
                    .filter(StringUtils::isNotEmpty)
                    .map(this::toOperation)
                    .filter(Objects::nonNull)
                    .filter(this::isSupportedOperation)
                    //.groupBy(RemediationOperation::getMediaType) // TODO: POC Stage only! assuming all with the same media_type and same action
                    //.groupBy(RemediationOperation::getOperation)
                    .buffer(remediationParams.getBatchSize())
                    .limitRate(remediationParams.getMaxOpenConnections())
                    .map(withLogResume(this::enrichWithSiteInfo))
                    .map(withLogResume(this::enrichWithDriveId))
                    .map(withLogResume(this::enrichWithItemId))
                    .map(list -> list.stream() // filter the ones that didn't succeed in item-id extraction
                            .filter(operation -> Objects.nonNull(operation.getItemId()))
                            .collect(Collectors.toList()))
                    .map(withLogResume(this::resolveDestination))
                    .doOnNext(withLogResume(this::performRemediation))
                    .subscribeOn(Schedulers.elastic())
                    .blockLast();
        } catch (IOException e) {
            log.error("Error processing script file file: {}", pathStr, e);
        }
    }

    private boolean needToProcess(String line) {
        boolean needToProcess = lineIndex.get() >= lineNumber;
        if (!needToProcess) {
            log.trace("Skipping operation at line {} as it is before specified line {}, operation skipped: {}",
                    lineIndex.get(), lineNumber, line);
        }
        return needToProcess;
    }

    private Function<List<RemediationOperation>, List<RemediationOperation>> withLogResume(
            Function<List<RemediationOperation>, List<RemediationOperation>> mapper) {
        return remediationOperations -> {
            try {
                return mapper.apply(remediationOperations);
            } catch (Exception e) {
                RemediationOperation startOperation = remediationOperations.iterator().next();
                log.error("Batch failed, resume script at line {}, operation {}", startOperation.getLineIndex(),
                        startOperation.getFullOperationLine());
                throw e;
            }
        };
    }

    private Consumer<List<RemediationOperation>> withLogResume(
            Consumer<List<RemediationOperation>> consumer) {
        return remediationOperations -> {
            try {
                consumer.accept(remediationOperations);
            } catch (Exception e) {
                RemediationOperation startOperation = remediationOperations.iterator().next();
                log.error("Batch failed, resume script at line {}, operation {}", startOperation.getLineIndex(),
                        startOperation.getFullOperationLine());
                throw e;
            }
        };
    }

    private boolean isSupportedOperation(RemediationOperation remediationOperation) {
        boolean operationSupported = SUPPORTED_OPERATIONS.contains(remediationOperation.getOperation());
        if (!operationSupported) {
            log.warn("Skipping {} operation, it is not supported. {}", remediationOperation.getOperation(),
                    remediationOperation);
        }
        return operationSupported;
    }

    private RemediationOperation toOperation(String operationLine) {
        String[] splits = operationLine.split(FIELD_SEPARATOR);

        try {
            return RemediationOperation.builder()
                    .operation(splits[0])
                    .fullOperationLine(operationLine)
                    .lineIndex(lineIndex.get())
                    .mediaType(MediaType.valueOf(splits[1]))
                    .baseUrl(splits[2])
                    .sourcePath(splits[3])
                    .sourceFileName(splits[4])
                    .targetPath(splits[5])
                    .build();
        } catch (Exception e) {
            log.error("Error processing input line, Skipping {}", operationLine, e);
        }
        return null;
    }

    private List<RemediationOperation> enrichWithSiteInfo(List<RemediationOperation> batch) {

        Function<RemediationOperation, String> pathExtractor = operation ->
                extractSiteUrl(shortPathToActual(operation.getSourcePath(), operation.getBaseUrl()));

        Set<String> paths = batch.stream()
                .map(pathExtractor)
                .collect(Collectors.toSet());

        // path -> site-id
        // check local site-id cache for paths
        Map<String, String> pathSiteIdMap = getCached(paths, siteIdCache);

        // create a batch request for the remaining paths
        Set<String> missingPaths = getMissingKeys(pathSiteIdMap);
        Map<String, String> siteIds = graphConnector.getSiteIdsByPaths(missingPaths);

        // update cache
        siteIdCache.putAll(siteIds);
        pathSiteIdMap.putAll(siteIds);

        // enrich batch
        batch.forEach(operation -> {
            String siteUrl = pathExtractor.apply(operation);
            String siteId = pathSiteIdMap.get(siteUrl);
            String[] splits = siteId.split(",");
            operation.setSiteHostName(splits[0]);
            operation.setSiteCollectionId(splits[1]);
            operation.setSiteId(splits[2]);
        });

        return batch;
    }

    private List<RemediationOperation> enrichWithDriveId(List<RemediationOperation> batch) {
        if (batch == null || batch.isEmpty()) {
            log.warn("Got Empty Batch");
            return Collections.emptyList();
        }

        Set<String> keys = batch
                .stream()
                .map(RemediationOperation::getSiteCollectionId)
                .filter(Objects::nonNull)
                .collect(Collectors.toSet());

        Map<String, String> cachedDriveBySite = getCached(keys, driveIdCache);

        // create a batch request for the remaining paths
        Set<String> missingKeys = getMissingKeys(cachedDriveBySite);
        Map<String, String> driveIds = graphConnector.getDriveIdsBySites(missingKeys);

        // update cache
        driveIdCache.putAll(driveIds);
        cachedDriveBySite.putAll(driveIds);

        // enrich
        batch.forEach(operation -> {
            String siteId = operation.getSiteCollectionId();
            if (siteId != null) {
                operation.setDriveId(cachedDriveBySite.get(siteId));
            }
        });

        return batch;
    }

    private List<RemediationOperation> enrichWithItemId(List<RemediationOperation> batch) {

        // Pair<Relative-Path, Site-Id>
        List<Pair<String, String>> pathsAndSites = batch.stream()
                .map(operation -> Pair.of(extractRelativeFilePath(operation), operation.getSiteCollectionId()))
                .collect(Collectors.toList());

        Map<Pair<String, String>, String> itemsByPathAndSite = graphConnector.getItemIdsByPathsAndSites(pathsAndSites);

        batch.forEach(operation -> {
            Pair<String, String> key = Pair.of(extractRelativeFilePath(operation), operation.getSiteCollectionId());
            String itemId = itemsByPathAndSite.get(key);
            if (itemId == null) {
                log.error("Skipping line {}, item-id was not resolved, line: {}",
                        operation.getLineIndex(), operation.getFullOperationLine());
            }
            operation.setItemId(itemId);
        });

        return batch;
    }

    private List<RemediationOperation> resolveDestination(List<RemediationOperation> batch) {
        if (batch == null || batch.isEmpty()) {
            log.warn("Got Empty Batch");
            return Collections.emptyList();
        }

        String baseUrl = batch.get(0).getBaseUrl();

        Set<String> sourcePaths = batch.stream().map(RemediationOperation::getSourcePath).collect(Collectors.toSet());

        // get cached destinations - non cached will have null values
        // source path -> destination item id
        Map<String, GlobalItemId> cached = getCached(sourcePaths, destinationCache);

        Set<String> missingKeys = getMissingKeys(cached);

        String targetPath = shortPathToActual(
                combinePaths(remediationParams.getDestinationDrive(), remediationParams.getDestinationPath()),
                baseUrl);

        // get (or create and get) non-cached destinations (api)
        Map<String, GlobalItemId> resolvedDestinations = graphConnector.getOrCreateDestinations(missingKeys, targetPath);

        // update cache
        destinationCache.putAll(resolvedDestinations);
        cached.putAll(resolvedDestinations);

        // enrich batch with destination info
        batch.forEach(operation ->
                operation.setResolvedTargetId(cached.get(operation.getSourcePath())));

        return batch;
    }


    private void performRemediation(List<RemediationOperation> batch) {
        if (batch.isEmpty()) {
            log.warn("empty batch");
            return;
        }

        // Split items by their operation type
        Map<String, List<Pair<GlobalItemId, GlobalItemId>>> remediationByOperationType = batch.stream()
                .collect(Collectors.groupingBy(
                        RemediationOperation::getOperation,
                        Collectors.mapping(operation -> {
                            GlobalItemId source = GlobalItemId.builder()
                                    .itemId(operation.getItemId())
                                    .driveId(operation.getDriveId())
                                    .name(operation.getSourceFileName())
                                    .build();
                            GlobalItemId destination = operation.getResolvedTargetId();
                            return Pair.of(source, destination);
                        }, Collectors.toList())));

        // Copy all items
        List<Pair<GlobalItemId, GlobalItemId>> itemsToCopy = remediationByOperationType.values().stream().
                flatMap(Collection::stream).collect(Collectors.toList());
        log.info("Running copy operation on {} items", itemsToCopy.size());
        graphConnector.copyItems(itemsToCopy);
        log.info("Copy operation on {} items done", itemsToCopy.size());

        // Delete items that were intended for move
        Optional.ofNullable(remediationByOperationType.get(MOVE_OPERATION))
                .map(itemsToCompleteMove -> itemsToCompleteMove.stream()
                        .map(Pair::getKey).collect(Collectors.toList()))
                .ifPresent(itemsToDelete -> {
                    log.info("Operation is move for {} items out of {}, deleting matching sources",
                            itemsToDelete.size(), itemsToCopy.size());
                    graphConnector.deleteItems(itemsToDelete);
                    log.info("Deletion of {} items done", itemsToDelete.size());
                });
    }

    private String extractRelativeFilePath(RemediationOperation operation) {
        // remove the the user part of the path
        String sourcePath = StringUtils.removeStart(operation.getSourcePath(), "/");
        sourcePath = StringUtils.substringAfter(sourcePath, "/");

        // format path + filename into full path
        return "/" + combinePaths(sourcePath, operation.getSourceFileName());
    }


    private <T> Set<String> getMissingKeys(Map<String, T> entries) {
        return entries.entrySet()
                .stream()
                .filter(entry -> entry.getValue() == null)
                .map(Map.Entry::getKey)
                .collect(Collectors.toSet());
    }

    private <T> Map<String, T> getCached(Set<String> keys, Cache<String, T> cache) {
        Map<String, T> pathSiteIdMap = Maps.newHashMap();
        keys.forEach(key -> pathSiteIdMap.put(key, cache.getIfPresent(key)));
        return pathSiteIdMap;
    }


}
