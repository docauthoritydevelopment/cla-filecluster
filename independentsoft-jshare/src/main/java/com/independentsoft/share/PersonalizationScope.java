package com.independentsoft.share;

/**
 * The Enum PersonalizationScope.
 */
public enum PersonalizationScope {

    USER,
    SHARED
}
