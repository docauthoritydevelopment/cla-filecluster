package com.independentsoft.share;

enum ServerVersionInfo {

    SHARE_POINT_ONLINE,
    SHARE_POINT_ON_PREMISE,
    NONE
}
