package com.independentsoft.share;

/**
 * The Class SiteCreationInfo.
 */
public class SiteCreationInfo {

    private String description;
    private Locale language = Locale.NONE;
    private String title;
    private String url;
    private boolean useSamePermissionsAsParentSite;
    private String webTemplate;

    /**
     * Instantiates a new site creation info.
     */
    public SiteCreationInfo()
    {
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        String stringValue = "{ 'parameters': { '__metadata':{ 'type': 'SP.WebCreationInformation' }";

        if (description != null && description.length() > 0)
        {
            stringValue += ", 'Description':'" + Util.encodeJson(description) + "'";
        }

        if (language != Locale.NONE)
        {
            stringValue += ", 'Language':'" + EnumUtil.parseLocale(language) + "'";
        }

        if (title != null && title.length() > 0)
        {
            stringValue += ", 'Title':'" + Util.encodeJson(title) + "'";
        }

        if (url != null && url.length() > 0)
        {
            stringValue += ", 'Url':'" + Util.encodeJson(url) + "'";
        }

        if (useSamePermissionsAsParentSite)
        {
            stringValue += ", 'UseSamePermissionsAsParentSite': true";
        }

        if (webTemplate != null && webTemplate.length() > 0)
        {
            stringValue += ", 'WebTemplate':'" + Util.encodeJson(webTemplate) + "'";
        }

        stringValue += " } }";

        return stringValue;
    }

    /**
     * Gets the description.
     *
     * @return the description
     */
    public String getDescription()
    {
        return description;
    }

    /**
     * Sets the description.
     *
     * @param description the new description
     */
    public void setDescription(String description)
    {
        this.description = description;
    }

    /**
     * Gets the language.
     *
     * @return the language
     */
    public Locale getLanguage()
    {
        return language;
    }

    /**
     * Sets the language.
     *
     * @param language the new language
     */
    public void setLanguage(Locale language)
    {
        this.language = language;
    }

    /**
     * Gets the title.
     *
     * @return the title
     */
    public String getTitle()
    {
        return title;
    }

    /**
     * Sets the title.
     *
     * @param title the new title
     */
    public void setTitle(String title)
    {
        this.title = title;
    }

    /**
     * Gets the url.
     *
     * @return the url
     */
    public String getUrl()
    {
        return url;
    }

    /**
     * Sets the url.
     *
     * @param url the new url
     */
    public void setUrl(String url)
    {
        this.url = url;
    }

    /**
     * Use same permissions as parent site.
     *
     * @return true, if successful
     */
    public boolean useSamePermissionsAsParentSite()
    {
        return useSamePermissionsAsParentSite;
    }

    /**
     * Sets the same permissions as parent site.
     *
     * @param useSamePermissionsAsParentSite the new same permissions as parent site
     */
    public void setSamePermissionsAsParentSite(boolean useSamePermissionsAsParentSite)
    {
        this.useSamePermissionsAsParentSite = useSamePermissionsAsParentSite;
    }

    /**
     * Gets the web template.
     *
     * @return the web template
     */
    public String getWebTemplate()
    {
        return webTemplate;
    }

    /**
     * Sets the web template.
     *
     * @param webTemplate the new web template
     */
    public void setWebTemplate(String webTemplate)
    {
        this.webTemplate = webTemplate;
    }
}
