package com.independentsoft.share;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;

import org.apache.http.HttpHost;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.protocol.HttpContext;

   class AndroidSSLSocketFactory implements ConnectionSocketFactory {

	private SSLContext sslContext;
	
	AndroidSSLSocketFactory(SSLContext context) throws IOException
	{
		this.sslContext = context;
	}


    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    public boolean equals( Object obj )
    {
        return ( ( obj != null ) && obj.getClass().equals( AndroidSSLSocketFactory.class ) );
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    public int hashCode()
    {
        return AndroidSSLSocketFactory.class.hashCode();
    }


	/* (non-Javadoc)
	 * @see org.apache.http.conn.socket.ConnectionSocketFactory#connectSocket(int, java.net.Socket, org.apache.http.HttpHost, java.net.InetSocketAddress, java.net.InetSocketAddress, org.apache.http.protocol.HttpContext)
	 */
	@Override
	public Socket connectSocket(
            final int connectTimeout,
            final Socket socket,
            final HttpHost host,
            final InetSocketAddress remoteAddress,
            final InetSocketAddress localAddress,
            final HttpContext context)
			throws IOException {

		SSLSocket sslSocket = (SSLSocket) ((socket != null) ? socket : createSocket(context));
		
		if (localAddress != null) 
		{
			sslSocket.bind(localAddress);
		}

		sslSocket.connect(remoteAddress, connectTimeout);
		sslSocket.setSoTimeout(connectTimeout);		
		
		return sslSocket;
    }


	/* (non-Javadoc)
	 * @see org.apache.http.conn.socket.ConnectionSocketFactory#createSocket(org.apache.http.protocol.HttpContext)
	 */
	@Override
	public Socket createSocket(HttpContext context) throws IOException {
		
		return sslContext.getSocketFactory().createSocket();
		
	}
}

