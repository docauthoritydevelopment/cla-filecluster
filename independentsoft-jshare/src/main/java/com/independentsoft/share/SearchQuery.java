package com.independentsoft.share;

import java.util.ArrayList;
import java.util.List;

/**
 * The Class SearchQuery.
 */
public class SearchQuery {

    private String query;
    private String template;
    private boolean enableInterleaving = true;
    private String sourceId;
    private String rankingModelId;
    private int startRow;
    private int rowLimit;
    private int rowsPerPage;
    private List<String> selectProperties = new ArrayList<String>();
    private Locale culture = Locale.NONE;
    private List<String> refinementFilters = new ArrayList<String>();
    private List<String> refiners = new ArrayList<String>();
    private String hiddenConstraints;
    private List<Sort> sortList = new ArrayList<Sort>();
    private boolean enableStemming = true;
    private boolean trimDuplicates = true;
    private int timeout;
    private boolean enableNicknames;
    private boolean enablePhonetic;
    private boolean enableFql;
    private List<String> hitHighlightedProperties = new ArrayList<String>();
    private boolean bypassResultTypes;
    private boolean processBestBets;
    private QueryLogClientType clientType = QueryLogClientType.NONE;
    private String personalizationData;
    private String resultsUrl;
    private List<String> tags = new ArrayList<String>();
    private List<SearchQueryProperty> properties = new ArrayList<SearchQueryProperty>();
    private boolean enableQueryRules = true;
    private List<ReorderingRule> reorderingRules = new ArrayList<ReorderingRule>();
    private boolean processPersonalFavorites;
    private String queryTemplatePropertiesUrl;
    private int hitHighlightedMultivaluePropertyLimit;
    private boolean enableOrderingHitHighlightedProperty;
    private String collapseSpecification;
    private boolean enableSorting;
    private boolean generateBlockRankLog;
    private Locale uiLanguage = Locale.NONE;
    private int desiredSnippetLength;
    private int maxSnippetLength;
    private int summaryLength;

    /**
     * Instantiates a new search query.
     */
    public SearchQuery()
    {
        this.query = "*";
    }

    /**
     * Instantiates a new search query.
     *
     * @param query the query
     */
    public SearchQuery(String query)
    {
        this.query = query;
    }

    /**
     * Instantiates a new search query.
     *
     * @param restriction the restriction
     */
    public SearchQuery(com.independentsoft.share.kql.IRestriction restriction)
    {
        if(restriction == null)
        {
            throw new IllegalArgumentException("restriction");
        }

        this.query = restriction.toString();
    }

    /**
     * Instantiates a new search query.
     *
     * @param restriction the restriction
     */
    public SearchQuery(com.independentsoft.share.fql.IRestriction restriction)
    {
        if (restriction == null)
        {
            throw new IllegalArgumentException("restriction");
        }

        this.refinementFilters.add(restriction.toString());
        this.query = "*";
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        String stringValue = "{ 'request': {";

        if (query != null && query.length() > 0)
        {
            stringValue += " 'Querytext':'" + Util.encodeJson(query) + "'";
        }

        if (template != null && template.length() > 0)
        {
            stringValue += ", 'QueryTemplate':'" + Util.encodeJson(template) + "'";
        }

        if (!enableInterleaving)
        {
            stringValue += ", 'EnableInterleaving': false";
        }

        if (sourceId != null && sourceId.length() > 0)
        {
            stringValue += ", 'SourceId':'" + Util.encodeJson(sourceId) + "'";
        }

        if (rankingModelId != null && rankingModelId.length() > 0)
        {
            stringValue += ", 'RankingModelId':'" + Util.encodeJson(rankingModelId) + "'";
        }

        if (startRow > 0)
        {
            stringValue += ", 'StartRow':'" + startRow + "'";
        }

        if (rowLimit > 0)
        {
            stringValue += ", 'RowLimit':'" + rowLimit + "'";
        }

        if (rowsPerPage > 0)
        {
            stringValue += ", 'RowsPerPage':'" + rowsPerPage + "'";
        }

        if (selectProperties.size() > 0)
        {
            String value = "";

            for (int i = 0; i < selectProperties.size(); i++)
            {
                value += "'" + Util.encodeJson(selectProperties.get(i).toString()) + "'";

                if( i < selectProperties.size() - 1)
                {
                    value += ",";
                }
            }

            stringValue += ", 'SelectProperties': { 'results' : [ " + value + " ] }";
        }

        if (culture != Locale.NONE)
        {
            stringValue += ", 'Culture':'" + EnumUtil.parseLocale(culture) + "'";
        }

        if (refinementFilters.size() > 0)
        {
            String value = "";

            for (int i = 0; i < refinementFilters.size(); i++)
            {
                value += "'" + Util.encodeJson(refinementFilters.get(i).toString()) + "'";

                if (i < refinementFilters.size() - 1)
                {
                    value += ",";
                }
            }

            stringValue += ", 'RefinementFilters': { 'results' : [ " + value + " ] }";
        }

        if (refiners.size() > 0)
        {
            String value = "";

            for (int i = 0; i < refiners.size(); i++)
            {
                value += "'" + Util.encodeJson(refiners.get(i).toString()) + "'";

                if (i < refiners.size() - 1)
                {
                    value += ",";
                }
            }

            stringValue += ", 'Refiners': { 'results' : [ " + value + " ] }";
        }

        if (hiddenConstraints != null && hiddenConstraints.length() > 0)
        {
            stringValue += ", 'HiddenConstraints':'" + Util.encodeJson(hiddenConstraints) + "'";
        }

        if (sortList.size() > 0)
        {
            String value = "";

            for (int i = 0; i < sortList.size(); i++)
            {
                value += "'" + Util.encodeJson(sortList.get(i).toString()) + "'";

                if (i < sortList.size() - 1)
                {
                    value += ",";
                }
            }

            stringValue += ", 'SortList': { 'results' : [ " + value + " ] }";
        }

        if (!enableStemming)
        {
            stringValue += ", 'EnableStemming': false";
        }

        if (!trimDuplicates)
        {
            stringValue += ", 'TrimDuplicates': false";
        }

        if (timeout > 0)
        {
            stringValue += ", 'Timeout':'" + timeout + "'";
        }

        if (enableNicknames)
        {
            stringValue += ", 'EnableNicknames': true";
        }

        if (enablePhonetic)
        {
            stringValue += ", 'EnablePhonetic': true";
        }

        if (enableFql)
        {
            stringValue += ", 'EnableFql': true";
        }

        if (hitHighlightedProperties.size() > 0)
        {
            String value = "";

            for (int i = 0; i < hitHighlightedProperties.size(); i++)
            {
                value += "'" + Util.encodeJson(hitHighlightedProperties.get(i).toString()) + "'";

                if (i < hitHighlightedProperties.size() - 1)
                {
                    value += ",";
                }
            }

            stringValue += ", 'HitHighlightedProperties': { 'results' : [ " + value + " ] }";
        }

        if (bypassResultTypes)
        {
            stringValue += ", 'BypassResultTypes': true";
        }

        if (processBestBets)
        {
            stringValue += ", 'ProcessBestBets': true";
        }

        if (clientType != QueryLogClientType.NONE)
        {
            stringValue += ", 'ClientType':'" + EnumUtil.parseQueryLogClientType(clientType) + "'";
        }

        if (personalizationData != null && personalizationData.length() > 0)
        {
            stringValue += ", 'PersonalizationData':'" + Util.encodeJson(personalizationData) + "'";
        }

        if (resultsUrl != null && resultsUrl.length() > 0)
        {
            stringValue += ", 'ResultsURL':'" + Util.encodeJson(resultsUrl) + "'";
        }

        if (tags.size() > 0)
        {
            String value = "";

            for (int i = 0; i < tags.size(); i++)
            {
                value += "'" + Util.encodeJson(tags.get(i).toString()) + "'";

                if (i < tags.size() - 1)
                {
                    value += ",";
                }
            }

            stringValue += ", 'QueryTag': { 'results' : [ " + value + " ] }";
        }

        if (properties.size() > 0)
        {
            String value = "";

            for (int i = 0; i < properties.size(); i++)
            {
                value += "'" + properties.get(i).toString() + "'";

                if (i < properties.size() - 1)
                {
                    value += ",";
                }
            }

            stringValue += ", 'Properties': { 'results' : [ " + value + " ] }";
        }

        if (!enableQueryRules)
        {
            stringValue += ", 'EnableQueryRules': false";
        }

        if (reorderingRules.size() > 0)
        {
            String value = "";

            for (int i = 0; i < reorderingRules.size(); i++)
            {
                value += "'" + reorderingRules.get(i).toString() + "'";

                if (i < reorderingRules.size() - 1)
                {
                    value += ",";
                }
            }

            stringValue += ", 'ReorderingRules': { 'results' : [ " + value + " ] }";
        }

        if (processPersonalFavorites)
        {
            stringValue += ", 'ProcessPersonalFavorites': true";
        }

        if (queryTemplatePropertiesUrl != null && queryTemplatePropertiesUrl.length() > 0)
        {
            stringValue += " 'QueryTemplatePropertiesUrl':'" + Util.encodeJson(queryTemplatePropertiesUrl) + "'";
        }

        if (hitHighlightedMultivaluePropertyLimit > 0)
        {
            stringValue += ", 'HitHighlightedMultivaluePropertyLimit':'" + hitHighlightedMultivaluePropertyLimit + "'";
        }

        if (enableOrderingHitHighlightedProperty)
        {
            stringValue += ", 'EnableOrderingHitHighlightedProperty': true";
        }

        if (collapseSpecification != null && collapseSpecification.length() > 0)
        {
            stringValue += " 'CollapseSpecification':'" + Util.encodeJson(collapseSpecification) + "'";
        }

        if (enableSorting)
        {
            stringValue += ", 'EnableSorting': true";
        }

        if (generateBlockRankLog)
        {
            stringValue += ", 'GenerateBlockRankLog': true";
        }

        if (uiLanguage != Locale.NONE)
        {
            stringValue += ", 'UIlanguage':'" + EnumUtil.parseLocale(uiLanguage) + "'";
        }

        if (desiredSnippetLength > 0)
        {
            stringValue += ", 'DesiredSnippetLength':'" + desiredSnippetLength + "'";
        }

        if (maxSnippetLength > 0)
        {
            stringValue += ", 'MaxSnippetLength':'" + maxSnippetLength + "'";
        }

        if (summaryLength > 0)
        {
            stringValue += ", 'SummaryLength':'" + summaryLength + "'";
        }

        stringValue +=" } }";

        return stringValue;
    }

    /**
     * Gets the query.
     *
     * @return the query
     */
    public String getQuery()
    {
        return query;
    }

    /**
     * Sets the query.
     *
     * @param query the new query
     */
    public void setQuery(String query)
    {
        this.query = query;
    }

    /**
     * Gets the template.
     *
     * @return the template
     */
    public String getTemplate()
    {
        return template;
    }

    /**
     * Sets the template.
     *
     * @param template the new template
     */
    public void setTemplate(String template)
    {
        this.template = template;
    }

    /**
     * Checks if is interleaving enabled.
     *
     * @return true, if is interleaving enabled
     */
    public boolean isInterleavingEnabled()
    {
        return enableInterleaving;
    }

    /**
     * Enable interleaving.
     *
     * @param enableInterleaving the enable interleaving
     */
    public void enableInterleaving(boolean enableInterleaving)
    {
        this.enableInterleaving = enableInterleaving;
    }

    /**
     * Gets the source id.
     *
     * @return the source id
     */
    public String getSourceId()
    {
        return sourceId;
    }

    /**
     * Sets the source id.
     *
     * @param sourceId the new source id
     */
    public void setSourceId(String sourceId)
    {
        this.sourceId = sourceId;
    }

    /**
     * Gets the ranking model id.
     *
     * @return the ranking model id
     */
    public String getRankingModelId()
    {
        return rankingModelId;
    }

    /**
     * Sets the ranking model id.
     *
     * @param rankingModelId the new ranking model id
     */
    public void setRankingModelId(String rankingModelId)
    {
        this.rankingModelId = rankingModelId;
    }

    /**
     * Gets the start row.
     *
     * @return the start row
     */
    public int getStartRow()
    {
        return startRow;
    }

    /**
     * Sets the start row.
     *
     * @param startRow the new start row
     */
    public void setStartRow(int startRow)
    {
        this.startRow = startRow;
    }

    /**
     * Gets the row limit.
     *
     * @return the row limit
     */
    public int getRowLimit()
    {
        return rowLimit;
    }

    /**
     * Sets the row limit.
     *
     * @param rowLimit the new row limit
     */
    public void setRowLimit(int rowLimit)
    {
        this.rowLimit = rowLimit;
    }

    /**
     * Gets the rows per page.
     *
     * @return the rows per page
     */
    public int getRowsPerPage()
    {
        return rowsPerPage;
    }

    /**
     * Sets the rows per page.
     *
     * @param rowsPerPage the new rows per page
     */
    public void setRowsPerPage(int rowsPerPage)
    {
        this.rowsPerPage = rowsPerPage;
    }

    /**
     * Gets the select properties.
     *
     * @return the select properties
     */
    public List<String> getSelectProperties()
    {
        return selectProperties;
    }

    /**
     * Gets the culture.
     *
     * @return the culture
     */
    public Locale getCulture()
    {
        return culture;
    }

    /**
     * Sets the culture.
     *
     * @param culture the new culture
     */
    public void setCulture(Locale culture)
    {
        this.culture = culture;
    }

    /**
     * Gets the refinement filters.
     *
     * @return the refinement filters
     */
    public List<String> getRefinementFilters()
    {
        return refinementFilters;
    }

    /**
     * Gets the refiners.
     *
     * @return the refiners
     */
    public List<String> getRefiners()
    {
        return refiners;
    }

    /**
     * Gets the hidden constraints.
     *
     * @return the hidden constraints
     */
    public String getHiddenConstraints()
    {
        return hiddenConstraints;
    }

    /**
     * Sets the hidden constraints.
     *
     * @param hiddenConstraints the new hidden constraints
     */
    public void setHiddenConstraints(String hiddenConstraints)
    {
        this.hiddenConstraints = hiddenConstraints;
    }

    /**
     * Gets the sort list.
     *
     * @return the sort list
     */
    public List<Sort> getSortList()
    {
        return sortList;
    }

    /**
     * Checks if is stemming enabled.
     *
     * @return true, if is stemming enabled
     */
    public boolean isStemmingEnabled()
    {
        return enableStemming;
    }

    /**
     * Enable stemming.
     *
     * @param enableStemming the enable stemming
     */
    public void enableStemming(boolean enableStemming)
    {
        this.enableStemming = enableStemming;
    }

    /**
     * Checks if is trim duplicates.
     *
     * @return true, if is trim duplicates
     */
    public boolean isTrimDuplicates()
    {
        return trimDuplicates;
    }

    /**
     * Sets the trim duplicates.
     *
     * @param trimDuplicates the new trim duplicates
     */
    public void setTrimDuplicates(boolean trimDuplicates)
    {
        this.trimDuplicates = trimDuplicates;
    }

    /**
     * Gets the timeout.
     *
     * @return the timeout
     */
    public int getTimeout()
    {
        return timeout;
    }

    /**
     * Sets the timeout.
     *
     * @param timeout the new timeout
     */
    public void setTimeout(int timeout)
    {
        this.timeout = timeout;
    }

    /**
     * Checks for nicknames enabled.
     *
     * @return true, if successful
     */
    public boolean hasNicknamesEnabled()
    {
        return enableNicknames;
    }

    /**
     * Enable nicknames.
     *
     * @param enableNicknames the enable nicknames
     */
    public void enableNicknames(boolean enableNicknames)
    {
        this.enableNicknames = enableNicknames;
    }

    /**
     * Checks if is phonetic enabled.
     *
     * @return true, if is phonetic enabled
     */
    public boolean isPhoneticEnabled()
    {
        return enablePhonetic;
    }

    /**
     * Enable phonetic.
     *
     * @param enablePhonetic the enable phonetic
     */
    public void enablePhonetic(boolean enablePhonetic)
    {
        this.enablePhonetic = enablePhonetic;
    }

    /**
     * Checks if is fql enabled.
     *
     * @return true, if is fql enabled
     */
    public boolean isFqlEnabled()
    {
        return enableFql;
    }

    /**
     * Enable fql.
     *
     * @param enableFql the enable fql
     */
    public void enableFql(boolean enableFql)
    {
        this.enableFql = enableFql;
    }

    /**
     * Gets the hit highlighted properties.
     *
     * @return the hit highlighted properties
     */
    public List<String> getHitHighlightedProperties()
    {
        return hitHighlightedProperties;
    }

    /**
     * Checks if is bypass result types.
     *
     * @return true, if is bypass result types
     */
    public boolean isBypassResultTypes()
    {
        return bypassResultTypes;
    }

    /**
     * Sets the bypass result types.
     *
     * @param bypassResultTypes the new bypass result types
     */
    public void setBypassResultTypes(boolean bypassResultTypes)
    {
        this.bypassResultTypes = bypassResultTypes;
    }

    /**
     * Checks if is process best bets.
     *
     * @return true, if is process best bets
     */
    public boolean isProcessBestBets()
    {
        return processBestBets;
    }

    /**
     * Sets the process best bets.
     *
     * @param processBestBets the new process best bets
     */
    public void setProcessBestBets(boolean processBestBets)
    {
        this.processBestBets = processBestBets;
    }

    /**
     * Gets the client type.
     *
     * @return the client type
     */
    public QueryLogClientType getClientType()
    {
        return clientType;
    }

    /**
     * Sets the client type.
     *
     * @param clientType the new client type
     */
    public void setClientType(QueryLogClientType clientType)
    {
        this.clientType = clientType;
    }

    /**
     * Gets the personalization data.
     *
     * @return the personalization data
     */
    public String getPersonalizationData()
    {
        return personalizationData;
    }

    /**
     * Sets the personalization data.
     *
     * @param personalizationData the new personalization data
     */
    public void setPersonalizationData(String personalizationData)
    {
        this.personalizationData = personalizationData;
    }

    /**
     * Gets the results url.
     *
     * @return the results url
     */
    public String getResultsUrl()
    {
        return resultsUrl;
    }

    /**
     * Sets the results url.
     *
     * @param resultsUrl the new results url
     */
    public void setResultsUrl(String resultsUrl)
    {
        this.resultsUrl = resultsUrl;
    }

    /**
     * Gets the tags.
     *
     * @return the tags
     */
    public List<String> getTags()
    {
        return tags;
    }

    /**
     * Gets the properties.
     *
     * @return the properties
     */
    public List<SearchQueryProperty> getProperties()
    {
        return properties;
    }

    /**
     * Checks for query rules enabled.
     *
     * @return true, if successful
     */
    public boolean hasQueryRulesEnabled()
    {
        return enableQueryRules;
    }

    /**
     * Enable query rules.
     *
     * @param enableQueryRules the enable query rules
     */
    public void enableQueryRules(boolean enableQueryRules)
    {
        this.enableQueryRules = enableQueryRules;
    }

    /**
     * Gets the reordering rules.
     *
     * @return the reordering rules
     */
    public List<ReorderingRule> getReorderingRules()
    {
        return reorderingRules;
    }

    /**
     * Checks if is process personal favorites.
     *
     * @return true, if is process personal favorites
     */
    public boolean isProcessPersonalFavorites()
    {
        return processPersonalFavorites;
    }

    /**
     * Sets the process personal favorites.
     *
     * @param processPersonalFavorites the new process personal favorites
     */
    public void setProcessPersonalFavorites(boolean processPersonalFavorites)
    {
        this.processPersonalFavorites = processPersonalFavorites;
    }

    /**
     * Gets the query template properties url.
     *
     * @return the query template properties url
     */
    public String getQueryTemplatePropertiesUrl()
    {
        return queryTemplatePropertiesUrl;
    }

    /**
     * Sets the query template properties url.
     *
     * @param queryTemplatePropertiesUrl the new query template properties url
     */
    public void setQueryTemplatePropertiesUrl(String queryTemplatePropertiesUrl)
    {
        this.queryTemplatePropertiesUrl = queryTemplatePropertiesUrl;
    }

    /**
     * Gets the hit highlighted multivalue property limit.
     *
     * @return the hit highlighted multivalue property limit
     */
    public int getHitHighlightedMultivaluePropertyLimit()
    {
        return hitHighlightedMultivaluePropertyLimit;
    }

    /**
     * Sets the hit highlighted multivalue property limit.
     *
     * @param hitHighlightedMultivaluePropertyLimit the new hit highlighted multivalue property limit
     */
    public void setHitHighlightedMultivaluePropertyLimit(int hitHighlightedMultivaluePropertyLimit)
    {
        this.hitHighlightedMultivaluePropertyLimit = hitHighlightedMultivaluePropertyLimit;
    }

    /**
     * Checks if is ordering hit highlighted property enabled.
     *
     * @return true, if is ordering hit highlighted property enabled
     */
    public boolean isOrderingHitHighlightedPropertyEnabled()
    {
        return enableOrderingHitHighlightedProperty;
    }

    /**
     * Enable ordering hit highlighted property.
     *
     * @param enableOrderingHitHighlightedProperty the enable ordering hit highlighted property
     */
    public void enableOrderingHitHighlightedProperty(boolean enableOrderingHitHighlightedProperty)
    {
        this.enableOrderingHitHighlightedProperty = enableOrderingHitHighlightedProperty;
    }

    /**
     * Gets the collapse specification.
     *
     * @return the collapse specification
     */
    public String getCollapseSpecification()
    {
        return collapseSpecification;
    }

    /**
     * Sets the collapse specification.
     *
     * @param collapseSpecification the new collapse specification
     */
    public void setCollapseSpecification(String collapseSpecification)
    {
        this.collapseSpecification = collapseSpecification;
    }

    /**
     * Checks if is sorting enabled.
     *
     * @return true, if is sorting enabled
     */
    public boolean isSortingEnabled()
    {
        return enableSorting;
    }

    /**
     * Enable sorting.
     *
     * @param enableSorting the enable sorting
     */
    public void enableSorting(boolean enableSorting)
    {
        this.enableSorting = enableSorting;
    }

    /**
     * Checks if is generate block rank log.
     *
     * @return true, if is generate block rank log
     */
    public boolean isGenerateBlockRankLog()
    {
        return generateBlockRankLog;
    }

    /**
     * Sets the generate block rank log.
     *
     * @param generateBlockRankLog the new generate block rank log
     */
    public void setGenerateBlockRankLog(boolean generateBlockRankLog)
    {
        this.generateBlockRankLog = generateBlockRankLog;
    }

    /**
     * Gets the UI language.
     *
     * @return the UI language
     */
    public Locale getUILanguage()
    {
        return uiLanguage;
    }

    /**
     * Sets the UI language.
     *
     * @param uiLanguage the new UI language
     */
    public void setUILanguage(Locale uiLanguage)
    {
        this.uiLanguage = uiLanguage;
    }

    /**
     * Gets the desired snippet length.
     *
     * @return the desired snippet length
     */
    public int getDesiredSnippetLength()
    {
        return desiredSnippetLength;
    }

    /**
     * Sets the desired snippet length.
     *
     * @param desiredSnippetLength the new desired snippet length
     */
    public void setDesiredSnippetLength(int desiredSnippetLength)
    {
        this.desiredSnippetLength = desiredSnippetLength;
    }

    /**
     * Gets the max snippet length.
     *
     * @return the max snippet length
     */
    public int getMaxSnippetLength()
    {
        return maxSnippetLength;
    }

    /**
     * Sets the max snippet length.
     *
     * @param maxSnippetLength the new max snippet length
     */
    public void setMaxSnippetLength(int maxSnippetLength)
    {
        this.maxSnippetLength = maxSnippetLength;
    }

    /**
     * Gets the summary length.
     *
     * @return the summary length
     */
    public int getSummaryLength()
    {
        return summaryLength;
    }

    /**
     * Sets the summary length.
     *
     * @param summaryLength the new summary length
     */
    public void setSummaryLength(int summaryLength)
    {
        this.summaryLength = summaryLength;
    }
}
