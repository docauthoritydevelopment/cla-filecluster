package com.independentsoft.share;

/**
 * The Enum DraftVisibilityType.
 */
public enum DraftVisibilityType {

    READER,
    AUTHOR,
    APPROVER,
    NONE
}
