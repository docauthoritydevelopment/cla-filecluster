package com.independentsoft.share;

/**
 * The Enum RoleType.
 */
public enum RoleType {

    GUEST,
    READER,
    CONTRIBUTOR,
    WEB_DESIGNER,
    ADMINISTRATOR,
    EDITOR,
    NONE
}
