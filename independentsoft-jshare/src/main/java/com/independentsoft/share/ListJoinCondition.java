package com.independentsoft.share;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

/**
 * The Class ListJoinCondition.
 */
public class ListJoinCondition {

    private FieldReference fieldRef1;
    private FieldReference fieldRef2;

    /**
     * Instantiates a new list join condition.
     */
    public ListJoinCondition()
    {
    }

    ListJoinCondition(XMLStreamReader reader) throws XMLStreamException
    {
        parse(reader);
    }

    private void parse(XMLStreamReader reader) throws XMLStreamException
    {
        while (reader.hasNext())
        {
            if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("FieldRef") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                if(fieldRef1 == null)
                {
                    fieldRef1 = new FieldReference(reader);
                }
                else
                {
                    fieldRef2 = new FieldReference(reader);
                }
            }

            if (reader.isEndElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Eq") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                break;
            }
            else
            {
                reader.next();
            }
        }
    }

    /**
     * Gets the field ref1.
     *
     * @return the field ref1
     */
    public FieldReference getFieldRef1()
    {
        return fieldRef1;
    }

    /**
     * Gets the field ref2.
     *
     * @return the field ref2
     */
    public FieldReference getFieldRef2()
    {
        return fieldRef2;
    }
}
