package com.independentsoft.share;

import java.util.Date;

/**
 * The Class Change.
 */
public abstract class Change {

    protected ChangeToken token;
    protected ChangeType type = ChangeType.NONE;
    protected String siteId;
    protected Date time;

    /**
     * Gets the token.
     *
     * @return the token
     */
    public ChangeToken getToken()
    {
        return token;
    }

    /**
     * Gets the type.
     *
     * @return the type
     */
    public ChangeType getType()
    {
        return type;
    }

    /**
     * Gets the site id.
     *
     * @return the site id
     */
    public String getSiteId()
    {
        return siteId;
    }

    /**
     * Gets the time.
     *
     * @return the time
     */
    public Date getTime()
    {
        return time;
    }
}
