package com.independentsoft.share;

import java.io.InputStream;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

/**
 * The Class User.
 */
public class User extends Principal {

    private String email;
    private boolean isSiteAdmin;
    private UserId userId;

    /**
     * Instantiates a new user.
     */
    public User()
    {
    }

    User(InputStream inputStream) throws XMLStreamException
    {
        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        XMLStreamReader reader = xmlInputFactory.createXMLStreamReader(inputStream);

        parse(reader);
    }

    User(XMLStreamReader reader) throws XMLStreamException
    {
        parse(reader);
    }

    private void parse(XMLStreamReader reader) throws XMLStreamException
    {
        while (reader.hasNext())
        {
            if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("properties") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices/metadata"))
            {
                while (reader.hasNext())
                {
                    if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Email") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        email = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Id") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            id = Integer.parseInt(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("IsHiddenInUI") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            isHiddenInUI = Boolean.parseBoolean(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("IsSiteAdmin") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            isSiteAdmin = Boolean.parseBoolean(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("LoginName") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        loginName = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("PrincipalType") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            type = EnumUtil.parsePrincipalType(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("UserId") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        userId = new UserId(reader);
                    }

                    if (reader.isEndElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("properties") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices/metadata"))
                    {
                        break;
                    }
                    else
                    {
                        reader.next();
                    }
                }
            }

            if (reader.isEndElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("entry") && reader.getNamespaceURI().equals("http://www.w3.org/2005/Atom"))
            {
                break;
            }
            else
            {
                reader.next();
            }
        }
    }

    String toCreateJSon()
    {
        String stringValue = "{ '__metadata':{ 'type': 'SP.User' }";

        if (loginName != null && loginName.length() > 0)
        {
            stringValue += ", 'LoginName': '" + Util.encodeJson(loginName) + "'";
        }

        if (email != null && email.length() > 0)
        {
            stringValue += ", 'Email': '" + Util.encodeJson(email) + "'";
        }

        if (isSiteAdmin)
        {
            stringValue += ", 'IsSiteAdmin': true";
        }

        if (title != null && title.length() > 0)
        {
            stringValue += ", 'Title': '" + Util.encodeJson(title) + "'";
        }

        stringValue += " }";

        return stringValue;
    }

    String toUpdateJSon()
    {
        String stringValue = "{ '__metadata':{ 'type': 'SP.User' }";

        if (email != null && email.length() > 0)
        {
            stringValue += ", 'Email': '" + Util.encodeJson(email) + "'";
        }

        if (isSiteAdmin)
        {
            stringValue += ", 'IsSiteAdmin': true";
        }

        if (title != null && title.length() > 0)
        {
            stringValue += ", 'Title': '" + Util.encodeJson(title) + "'";
        }

        stringValue += " }";

        return stringValue;
    }

    /**
     * Gets the email.
     *
     * @return the email
     */
    public String getEmail()
    {
        return email;
    }

    /**
     * Sets the email.
     *
     * @param email the new email
     */
    public void setEmail(String email)
    {
        this.email = email;
    }

    /**
     * Checks if is site admin.
     *
     * @return true, if is site admin
     */
    public boolean isSiteAdmin()
    {
        return isSiteAdmin;
    }

    /**
     * Sets the site admin.
     *
     * @param isSiteAdmin the new site admin
     */
    public void setSiteAdmin(boolean isSiteAdmin)
    {
        this.isSiteAdmin = isSiteAdmin;
    }

    /**
     * Gets the user id.
     *
     * @return the user id
     */
    public UserId getUserId()
    {
        return userId;
    }
}
