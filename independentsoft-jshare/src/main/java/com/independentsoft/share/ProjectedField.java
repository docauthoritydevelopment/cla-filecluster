package com.independentsoft.share;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

/**
 * The Class ProjectedField.
 */
public class ProjectedField {

    private String name;
    private String showField;
    private String type;
    private String list;
    private String fieldRef;

    /**
     * Instantiates a new projected field.
     */
    public ProjectedField()
    {
    }

    ProjectedField(XMLStreamReader reader) throws XMLStreamException
    {
        parse(reader);
    }

    private void parse(XMLStreamReader reader) throws XMLStreamException
    {
        name = reader.getAttributeValue(null, "Name");
        showField = reader.getAttributeValue(null, "ShowField");
        type = reader.getAttributeValue(null, "Type");
        list = reader.getAttributeValue(null, "List");
        fieldRef = reader.getAttributeValue(null, "FieldRef");

        while (reader.hasNext())
        {
            if (reader.isEndElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Field") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                break;
            }
            else
            {
                reader.next();
            }
        }
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName()
    {
        return name;
    }

    /**
     * Gets the show field.
     *
     * @return the show field
     */
    public String getShowField()
    {
        return showField;
    }

    /**
     * Gets the type.
     *
     * @return the type
     */
    public String getType()
    {
        return type;
    }

    /**
     * Gets the list.
     *
     * @return the list
     */
    public String getList()
    {
        return list;
    }

    /**
     * Gets the field ref.
     *
     * @return the field ref
     */
    public String getFieldRef()
    {
        return fieldRef;
    }
}
