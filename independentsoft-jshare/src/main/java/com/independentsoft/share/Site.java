package com.independentsoft.share;

import java.io.InputStream;
import java.text.ParseException;
import java.util.Date;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

/**
 * The Class Site.
 */
public class Site {

    private boolean allowRssFeeds;
    private String appInstanceId;
    private int configuration;
    private Date createdTime;
    private String customMasterUrl;
    private String description;
    private boolean isDocumentLibraryCalloutOfficeWebAppPreviewersDisabled;
    private boolean enableMinimalDownload;
    private String id;
    private Locale language = Locale.NONE;
    private Date lastItemModifiedTime;
    private String masterUrl;
    private boolean isQuickLaunchEnabled;
    private boolean isRecycleBinEnabled;
    private String serverRelativeUrl;
    private boolean isSyndicationEnabled;
    private String title;
    private boolean isTreeViewEnabled;
    private int uiVersion;
    private boolean isUIVersionConfigurationEnabled;
    private String url;
    private String webTemplate;

    /**
     * Instantiates a new site.
     */
    public Site()
    {
    }

    Site(InputStream inputStream) throws XMLStreamException, ParseException
    {
        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        XMLStreamReader reader = xmlInputFactory.createXMLStreamReader(inputStream);

        parse(reader);
    }

    Site(XMLStreamReader reader) throws XMLStreamException, ParseException
    {
        parse(reader);
    }

    private void parse(XMLStreamReader reader) throws XMLStreamException, ParseException
    {
        while (reader.hasNext())
        {
            if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("properties") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices/metadata"))
            {
                while (reader.hasNext())
                {
                    if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("AllowRssFeeds") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            allowRssFeeds = Boolean.parseBoolean(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("AppInstanceId") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        appInstanceId = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Configuration") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            configuration = Integer.parseInt(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Created") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            createdTime = Util.parseDate(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("CustomMasterUrl") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        customMasterUrl = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Description") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        description = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("DocumentLibraryCalloutOfficeWebAppPreviewersDisabled") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            isDocumentLibraryCalloutOfficeWebAppPreviewersDisabled = Boolean.parseBoolean(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("EnableMinimalDownload") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            enableMinimalDownload = Boolean.parseBoolean(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Id") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        id = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Language") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            language = EnumUtil.parseLocale(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("LastItemModifiedDate") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            lastItemModifiedTime = Util.parseDate(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("MasterUrl") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        masterUrl = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("QuickLaunchEnabled") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            isQuickLaunchEnabled = Boolean.parseBoolean(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("RecycleBinEnabled") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            isRecycleBinEnabled = Boolean.parseBoolean(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("ServerRelativeUrl") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        serverRelativeUrl = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("SyndicationEnabled") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            isSyndicationEnabled = Boolean.parseBoolean(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Title") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        title = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("TreeViewEnabled") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            isTreeViewEnabled = Boolean.parseBoolean(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("UIVersion") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            uiVersion = Integer.parseInt(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("UIVersionConfigurationEnabled") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            isUIVersionConfigurationEnabled = Boolean.parseBoolean(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Url") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        url = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("WebTemplate") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        webTemplate = reader.getElementText();
                    }

                    if (reader.isEndElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("properties") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices/metadata"))
                    {
                        break;
                    }
                    else
                    {
                        reader.next();
                    }
                }
            }

            if (reader.isEndElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("entry") && reader.getNamespaceURI().equals("http://www.w3.org/2005/Atom"))
            {
                break;
            }
            else
            {
                reader.next();
            }
        }
    }
    
    String toUpdateJSon()
    {
        String stringValue = "{ '__metadata': { 'type': 'SP.Web' }";

        if (customMasterUrl != null && customMasterUrl.length() > 0)
        {
            stringValue += ", 'CustomMasterUrl': '" + Util.encodeJson(customMasterUrl) + "'";
        }
        
        if (description != null && description.length() > 0)
        {
            stringValue += ", 'Description': '" + Util.encodeJson(description) + "'";
        }
                
        if (enableMinimalDownload)
        {
            stringValue += ", 'EnableMinimalDownload': true";
        }
        else        	
        {
            stringValue += ", 'EnableMinimalDownload': false";
        }
        
        if (masterUrl != null && masterUrl.length() > 0)
        {
            stringValue += ", 'MasterUrl': '" + Util.encodeJson(masterUrl) + "'";
        }

        if (isQuickLaunchEnabled)
        {
            stringValue += ", 'QuickLaunchEnabled': true";
        }
        else        	
        {
            stringValue += ", 'QuickLaunchEnabled': false";
        }
        
        if (serverRelativeUrl != null && serverRelativeUrl.length() > 0)
        {
            stringValue += ", 'ServerRelativeUrl': '" + Util.encodeJson(serverRelativeUrl) + "'";
        }

        if (isSyndicationEnabled)
        {
            stringValue += ", 'SyndicationEnabled': true";
        }
        else        	
        {
            stringValue += ", 'SyndicationEnabled': false";
        }
        
        if (title != null && title.length() > 0)
        {
            stringValue += ", 'Title': '" + Util.encodeJson(title) + "'";
        }

        if (isTreeViewEnabled)
        {
            stringValue += ", 'TreeViewEnabled': true";
        }
        else        	
        {
            stringValue += ", 'TreeViewEnabled': false";
        }
        
        if (uiVersion > 0)
        {
            stringValue += ", 'UIVersion': '" + uiVersion + "'";
        }

        if (isUIVersionConfigurationEnabled)
        {
            stringValue += ", 'UIVersionConfigurationEnabled': true";
        }
        else        	
        {
            stringValue += ", 'UIVersionConfigurationEnabled': false";
        }

        stringValue += " }";

        return stringValue;
    }

    /**
     * Checks if is rss feeds allowed.
     *
     * @return true, if is rss feeds allowed
     */
    public boolean isRssFeedsAllowed()
    {
        return allowRssFeeds;
    }

    /**
     * Gets the app instance id.
     *
     * @return the app instance id
     */
    public String getAppInstanceId()
    {
        return appInstanceId;
    }

    /**
     * Gets the configuration.
     *
     * @return the configuration
     */
    public int getConfiguration()
    {
        return configuration;
    }

    /**
     * Gets the created time.
     *
     * @return the created time
     */
    public Date getCreatedTime()
    {
        return createdTime;
    }

    /**
     * Gets the custom master url.
     *
     * @return the custom master url
     */
    public String getCustomMasterUrl()
    {
        return customMasterUrl;
    }
    
    /**
     * Sets the custom master url.
     *
     * @param customMasterUrl the new custom master url
     */
    public void setCustomMasterUrl(String customMasterUrl)
    {
        this.customMasterUrl = customMasterUrl;
    }

    /**
     * Gets the description.
     *
     * @return the description
     */
    public String getDescription()
    {
        return description;
    }
    
    /**
     * Sets the description.
     *
     * @param description the new description
     */
    public void setDescription(String description)
    {
        this.description = description;
    }

    /**
     * Checks if is document library callout office web app previewers disabled.
     *
     * @return true, if is document library callout office web app previewers disabled
     */
    public boolean isDocumentLibraryCalloutOfficeWebAppPreviewersDisabled()
    {
        return isDocumentLibraryCalloutOfficeWebAppPreviewersDisabled;
    }

    /**
     * Checks if is minimal download enabled.
     *
     * @return true, if is minimal download enabled
     */
    public boolean isMinimalDownloadEnabled()
    {
        return enableMinimalDownload;
    }
    
    /**
     * Enable minimal download.
     *
     * @param enableMinimalDownload the enable minimal download
     */
    public void enableMinimalDownload(boolean enableMinimalDownload)
    {
        this.enableMinimalDownload = enableMinimalDownload;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public String getId()
    {
        return id;
    }

    /**
     * Gets the language.
     *
     * @return the language
     */
    public Locale getLanguage()
    {
        return language;
    }

    /**
     * Gets the last item modified time.
     *
     * @return the last item modified time
     */
    public Date getLastItemModifiedTime()
    {
        return lastItemModifiedTime;
    }

    /**
     * Gets the master url.
     *
     * @return the master url
     */
    public String getMasterUrl()
    {
        return masterUrl;
    }
    
    /**
     * Sets the master url.
     *
     * @param masterUrl the new master url
     */
    public void setMasterUrl(String masterUrl)
    {
        this.masterUrl = masterUrl;
    }

    /**
     * Checks if is quick launch enabled.
     *
     * @return true, if is quick launch enabled
     */
    public boolean isQuickLaunchEnabled()
    {
        return isQuickLaunchEnabled;
    }
    
    /**
     * Enable quick launch.
     *
     * @param isQuickLaunchEnabled the is quick launch enabled
     */
    public void enableQuickLaunch(boolean isQuickLaunchEnabled)
    {
        this.isQuickLaunchEnabled = isQuickLaunchEnabled;
    }

    /**
     * Checks if is recycle bin enabled.
     *
     * @return true, if is recycle bin enabled
     */
    public boolean isRecycleBinEnabled()
    {
        return isRecycleBinEnabled;
    }

    /**
     * Gets the server relative url.
     *
     * @return the server relative url
     */
    public String getServerRelativeUrl()
    {
        return serverRelativeUrl;
    }
    
    /**
     * Sets the server relative url.
     *
     * @param serverRelativeUrl the new server relative url
     */
    public void setServerRelativeUrl(String serverRelativeUrl)
    {
        this.serverRelativeUrl = serverRelativeUrl;
    }

    /**
     * Checks if is syndication enabled.
     *
     * @return true, if is syndication enabled
     */
    public boolean isSyndicationEnabled()
    {
        return isSyndicationEnabled;
    }
    
    /**
     * Enable syndication.
     *
     * @param isSyndicationEnabled the is syndication enabled
     */
    public void enableSyndication(boolean isSyndicationEnabled)
    {
        this.isSyndicationEnabled = isSyndicationEnabled;
    }

    /**
     * Gets the title.
     *
     * @return the title
     */
    public String getTitle()
    {
        return title;
    }

    /**
     * Sets the title.
     *
     * @param title the new title
     */
    public void setTitle(String title)
    {
        this.title = title;
    }
    
    /**
     * Checks if is tree view enabled.
     *
     * @return true, if is tree view enabled
     */
    public boolean isTreeViewEnabled()
    {
        return isTreeViewEnabled;
    }
    
    /**
     * Enable tree view.
     *
     * @param isTreeViewEnabled the is tree view enabled
     */
    public void enableTreeView(boolean isTreeViewEnabled)
    {
        this.isTreeViewEnabled = isTreeViewEnabled;
    }

    /**
     * Gets the UI version.
     *
     * @return the UI version
     */
    public int getUIVersion()
    {
        return uiVersion;
    }
    
    /**
     * Sets the UI version.
     *
     * @param uiVersion the new UI version
     */
    public void setUIVersion(int uiVersion)
    {
        this.uiVersion = uiVersion;
    }

    /**
     * Checks if is UI version configuration enabled.
     *
     * @return true, if is UI version configuration enabled
     */
    public boolean isUIVersionConfigurationEnabled()
    {
        return isUIVersionConfigurationEnabled;
    }
    
    /**
     * Enable ui version configuration.
     *
     * @param isUIVersionConfigurationEnabled the is ui version configuration enabled
     */
    public void enableUIVersionConfiguration(boolean isUIVersionConfigurationEnabled)
    {
        this.isUIVersionConfigurationEnabled = isUIVersionConfigurationEnabled;
    }

    /**
     * Gets the url.
     *
     * @return the url
     */
    public String getUrl()
    {
        return url;
    }

    /**
     * Gets the web template.
     *
     * @return the web template
     */
    public String getWebTemplate()
    {
        return webTemplate;
    }
}
