package com.independentsoft.share;

import java.util.ArrayList;
import java.util.List;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

/**
 * The Class QueryResult.
 */
public class QueryResult {

    private List<ResultTable> customResults = new ArrayList<ResultTable>();
    private String queryId;
    private String queryRuleId;
    private ResultTable refinementResult;
    private ResultTable relevantResult;
    private ResultTable specialTermResult;

    /**
     * Instantiates a new query result.
     */
    public QueryResult()
    {
    }

    QueryResult(XMLStreamReader reader, String localName) throws XMLStreamException
    {
        parse(reader, localName);
    }

    private void parse(XMLStreamReader reader, String localName) throws XMLStreamException
    {
        while (reader.hasNext())
        {
            if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("CustomResults") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                String innerXml = reader.getElementText();

                if (innerXml != null && innerXml.length() > 64)
                {
                    throw new UnsupportedOperationException("CustomResults response:" + innerXml);
                }
            }
            else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("QueryId") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                queryId = reader.getElementText();
            }
            else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("QueryRuleId") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                queryRuleId = reader.getElementText();
            }
            else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("RefinementResults") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                refinementResult = new ResultTable(reader, "RefinementResults");
            }
            else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("RelevantResults") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                relevantResult = new ResultTable(reader, "RelevantResults");
            }
            else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("SpecialTermResults") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                specialTermResult = new ResultTable(reader, "SpecialTermResults");
            }

            if (reader.isEndElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals(localName) && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                break;
            }
            else
            {
                reader.next();
            }
        }
    }

    /**
     * Gets the custom results.
     *
     * @return the custom results
     */
    public List<ResultTable> getCustomResults()
    {
        return customResults;
    }

    /**
     * Gets the query id.
     *
     * @return the query id
     */
    public String getQueryId()
    {
        return queryId;
    }

    /**
     * Gets the query rule id.
     *
     * @return the query rule id
     */
    public String getQueryRuleId()
    {
        return queryRuleId;
    }

    /**
     * Gets the refinement result.
     *
     * @return the refinement result
     */
    public ResultTable getRefinementResult()
    {
        return refinementResult;
    }

    /**
     * Gets the relevant result.
     *
     * @return the relevant result
     */
    public ResultTable getRelevantResult()
    {
        return relevantResult;
    }

    /**
     * Gets the special term result.
     *
     * @return the special term result
     */
    public ResultTable getSpecialTermResult()
    {
        return specialTermResult;
    }
}
