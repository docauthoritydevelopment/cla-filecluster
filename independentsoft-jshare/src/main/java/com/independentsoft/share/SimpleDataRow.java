package com.independentsoft.share;

import java.util.ArrayList;
import java.util.List;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

/**
 * The Class SimpleDataRow.
 */
public class SimpleDataRow {

    private List<KeyValue> cells = new ArrayList<KeyValue>();

    /**
     * Instantiates a new simple data row.
     */
    public SimpleDataRow()
    {
    }

    SimpleDataRow(XMLStreamReader reader) throws XMLStreamException
    {
        parse(reader);
    }

    private void parse(XMLStreamReader reader) throws XMLStreamException
    {
        while (reader.hasNext())
        {
            if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("element") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                KeyValue property = new KeyValue(reader);
                cells.add(property);
            }

            if (reader.isEndElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Cells") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                break;
            }
            else
            {
                reader.next();
            }
        }
    }

    /**
     * Gets the cells.
     *
     * @return the cells
     */
    public List<KeyValue> getCells()
    {
        return cells;
    }
}
