package com.independentsoft.share;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

/**
 * The Class PersonalResultSuggestion.
 */
public class PersonalResultSuggestion {

    private String highlightedTitle;
    private boolean isBestBet;
    private String title;
    private String url;

    /**
     * Instantiates a new personal result suggestion.
     */
    public PersonalResultSuggestion()
    {
    }

    PersonalResultSuggestion(XMLStreamReader reader) throws XMLStreamException
    {
        parse(reader);
    }

    private void parse(XMLStreamReader reader) throws XMLStreamException
    {
        while (reader.hasNext())
        {
            if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("HighlightedTitle") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                highlightedTitle = reader.getElementText();
            }
            else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("IsBestBet") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                String stringValue = reader.getElementText();

                if(stringValue != null && stringValue.length() > 0)
                {
                    isBestBet = Boolean.parseBoolean(stringValue);
                }
            }
            else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Title") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                title = reader.getElementText();
            }
            else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Url") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                url = reader.getElementText();
            }

            if (reader.isEndElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("PersonalResultSuggestion") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                break;
            }
            else
            {
                reader.next();
            }
        }
    }

    /**
     * Gets the highlighted title.
     *
     * @return the highlighted title
     */
    public String getHighlightedTitle()
    {
        return highlightedTitle;
    }

    /**
     * Checks if is best bet.
     *
     * @return true, if is best bet
     */
    public boolean isBestBet()
    {
        return isBestBet;
    }

    /**
     * Gets the title.
     *
     * @return the title
     */
    public String getTitle()
    {
        return title;
    }

    /**
     * Gets the url.
     *
     * @return the url
     */
    public String getUrl()
    {
        return url;
    }
}
