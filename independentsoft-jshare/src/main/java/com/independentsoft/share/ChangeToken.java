package com.independentsoft.share;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * The Class ChangeToken.
 */
public class ChangeToken {
	
    private int version = 1;
    private ChangeTokenScope scope = ChangeTokenScope.SITE;
    private String scopeId;
    private Date changeTime;
    private int changeNumber = -1;

    /**
     * Instantiates a new change token.
     */
    public ChangeToken()
    {
    }

    /**
     * Instantiates a new change token.
     *
     * @param scope the scope
     * @param scopeId the scope id
     * @param changeTime the change time
     */
    public ChangeToken(ChangeTokenScope scope, String scopeId, Date changeTime)
    {
        this.scope = scope;
        this.scopeId = scopeId;
        this.changeTime = changeTime;
    }

    /**
     * Instantiates a new change token.
     *
     * @param scope the scope
     * @param scopeId the scope id
     * @param changeTime the change time
     * @param changeNumber the change number
     */
    public ChangeToken(ChangeTokenScope scope, String scopeId, Date changeTime, int changeNumber)
    {
        this.scope = scope;
        this.scopeId = scopeId;
        this.changeTime = changeTime;
        this.changeNumber = changeNumber;
    }

    /**
     * Instantiates a new change token.
     *
     * @param changeToken the change token
     * @throws ParseException the parse exception
     */
    public ChangeToken(String changeToken) throws ParseException
    {
        parse(changeToken);
    }

    private void parse(String changeToken) throws ParseException
    {
        if(changeToken != null)
        {
            String[] splitted = changeToken.split(";");

            if(splitted.length == 5)
            {
                version = Integer.parseInt(splitted[0]);

                scope = EnumUtil.parseChangeTokenScope(splitted[1]);
                scopeId = splitted[2];

                long ticks = Long.parseLong(splitted[3]);
                
                changeTime = new Date((ticks - 621355968000000000L)/10000);
           	
                changeNumber = Integer.parseInt(splitted[4]);
            }
        }
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
       	long millis = changeTime.getTime();    	
    	long ticks = 621355968000000000L + millis * 10000;
    	
        String value = "1" + ";" + EnumUtil.parseChangeTokenScope(scope) + ";" + scopeId + ";" + ticks + ";" + changeNumber;

        String stringValue = "{ '__metadata':{ 'type': 'SP.ChangeToken' }";

        stringValue += ", 'StringValue':'" + value + "'";

        stringValue +=" }";

        return stringValue;
    }

    /**
     * Gets the version.
     *
     * @return the version
     */
    public int getVersion()
    {
    	return version;
    }
    
    /**
     * Gets the scope.
     *
     * @return the scope
     */
    public ChangeTokenScope getScope()
    {
    	return scope;
    }

    /**
     * Sets the scope.
     *
     * @param scope the new scope
     */
    public void setScope(ChangeTokenScope scope)
    {
    	this.scope = scope;
    }
   
    /**
     * Gets the scope id.
     *
     * @return the scope id
     */
    public String getScopeId()
    {
    	return scopeId;
    }

    /**
     * Sets the scope id.
     *
     * @param scopeId the new scope id
     */
    public void setScopeId(String scopeId)
    {
    	this.scopeId = scopeId;
    }

    /**
     * Gets the change time.
     *
     * @return the change time
     */
    public Date getChangeTime()
    {
    	return changeTime;
    }

    /**
     * Sets the change time.
     *
     * @param changeTime the new change time
     */
    public void setChangeTime(Date changeTime)
    {
    	this.changeTime = changeTime;
    }

    /**
     * Gets the change number.
     *
     * @return the change number
     */
    public int getChangeNumber()
    {
    	return changeNumber;
    }

    /**
     * Sets the change number.
     *
     * @param changeNumber the new change number
     */
    public void setChangeNumber(int changeNumber)
    {
    	this.changeNumber = changeNumber;
    }    
}
