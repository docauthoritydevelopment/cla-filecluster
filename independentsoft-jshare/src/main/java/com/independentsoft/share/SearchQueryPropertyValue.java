package com.independentsoft.share;

/**
 * The Class SearchQueryPropertyValue.
 */
public class SearchQueryPropertyValue {

    private int index;
    private boolean isBooleanValueSet;
    private boolean booleanValue;
    private int integerValue = Integer.MIN_VALUE;
    private String stringValue;
    private String[] stringArrayValue;

    /**
     * Instantiates a new search query property value.
     *
     * @param value the value
     * @param index the index
     */
    public SearchQueryPropertyValue(boolean value, int index)
    {
        this.booleanValue = value;
        this.isBooleanValueSet = true;
        this.index = index;
    }

    /**
     * Instantiates a new search query property value.
     *
     * @param value the value
     * @param index the index
     */
    public SearchQueryPropertyValue(int value, int index)
    {
        this.integerValue = value;
        this.index = index;
    }

    /**
     * Instantiates a new search query property value.
     *
     * @param value the value
     * @param index the index
     */
    public SearchQueryPropertyValue(String value, int index)
    {
        this.stringValue = value;
        this.index = index;
    }

    /**
     * Instantiates a new search query property value.
     *
     * @param value the value
     * @param index the index
     */
    public SearchQueryPropertyValue(String[] value, int index)
    {
        this.stringArrayValue = value;
        this.index = index;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        String json = "{ ";

        if(isBooleanValueSet)
        {
            json += "'BoolVal':'" + Boolean.toString(booleanValue) + "'";
        }
        else if(integerValue > Integer.MIN_VALUE)
        {
            json += "'IntVal':'" + integerValue + "'";
        }
        else if (stringValue != null && stringValue.length() > 0)
        {
            json += "'StrVal':'" + Util.encodeJson(stringValue) + "'";
        }
        else if (stringArrayValue != null && stringArrayValue.length > 0)
        {
            String value = "";

            for (int i = 0; i < stringArrayValue.length; i++)
            {
                value += "'" + Util.encodeJson(stringArrayValue[i]) + "'";

                if (i < stringArrayValue.length - 1)
                {
                    value += ",";
                }
            }

            json += "'StrArray':'" + Util.encodeJson(value) + "'";
        }

        json += ", 'QueryPropertyValueTypeIndex':'" + index + "' }";

        return json;
    }

    /**
     * Gets the index.
     *
     * @return the index
     */
    public int getIndex()
    {
        return index;
    }

    /**
     * Gets the boolean value.
     *
     * @return the boolean value
     */
    public boolean getBooleanValue()
    {
        return booleanValue;
    }

    /**
     * Gets the integer value.
     *
     * @return the integer value
     */
    public int getIntegerValue()
    {
        return integerValue;
    }

    /**
     * Gets the string value.
     *
     * @return the string value
     */
    public String getStringValue()
    {
        return stringValue;
    }

    /**
     * Gets the string array value.
     *
     * @return the string array value
     */
    public String[] getStringArrayValue()
    {
        return stringArrayValue;
    }
}
