package com.independentsoft.share;

/**
 * The Enum RecurrencePatternXmlVersion.
 */
public enum RecurrencePatternXmlVersion {

    V3,
    NONE
}
