package com.independentsoft.share;

/**
 * The Enum CheckInType.
 */
public enum CheckInType {
	
    MINOR,
    MAJOR,
    OVERWRITE    
}
