package com.independentsoft.share;

import java.io.InputStream;
import java.text.ParseException;
import java.util.Date;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

/**
 * The Class ListItem.
 */
public class ListItem {

    private FileSystemObjectType fileSystemObjectType = FileSystemObjectType.INVALID;
    private int id;
    private String contentTypeId;
    private String title;
    private String description;
    private Date modifiedTime;
    private Date createdTime;
    private int authorId;
    private int editorId;
    private String uiVersion;
    private boolean hasAttachments;
    private String guid;

    /**
     * Instantiates a new list item.
     */
    public ListItem()
    {
    }

    /**
     * Instantiates a new list item.
     *
     * @param title the title
     */
    public ListItem(String title)
    {
        this.title = title;
    }

    ListItem(InputStream inputStream) throws XMLStreamException, ParseException
    {
        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        XMLStreamReader reader = xmlInputFactory.createXMLStreamReader(inputStream);

        parse(reader);
    }

    ListItem(XMLStreamReader reader) throws XMLStreamException, ParseException
    {
        parse(reader);
    }

    private void parse(XMLStreamReader reader) throws XMLStreamException, ParseException
    {
        while (reader.hasNext())
        {
            if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("properties") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices/metadata"))
            {
                while (reader.hasNext())
                {
                    if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("FileSystemObjectType") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            fileSystemObjectType = EnumUtil.parseFileSystemObjectType(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Id") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            id = Integer.parseInt(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("ContentTypeId") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        contentTypeId = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Title") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        title = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("OData__x0064_qt5") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        description = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Modified") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            modifiedTime = Util.parseDate(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Created") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            createdTime = Util.parseDate(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("AuthorId") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            authorId = Integer.parseInt(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("EditorId") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            editorId = Integer.parseInt(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("OData__UIVersionString") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        uiVersion = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Attachments") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            hasAttachments = Boolean.parseBoolean(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("GUID") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        guid = reader.getElementText();
                    }

                    if (reader.isEndElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("properties") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices/metadata"))
                    {
                        break;
                    }
                    else
                    {
                        reader.next();
                    }
                }
            }

            if (reader.isEndElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("entry") && reader.getNamespaceURI().equals("http://www.w3.org/2005/Atom"))
            {
                break;
            }
            else
            {
                reader.next();
            }
        }
    }

    String toCreateJSon(String listItemEntityTypeFullName)
    {
        String stringValue = "{ '__metadata': { 'type': '" + listItemEntityTypeFullName + "' }";

        if (title != null && title.length() > 0)
        {
            stringValue += ", 'Title':'" + Util.encodeJson(title) + "'";
        }

        stringValue += " }";

        return stringValue;
    }

    String toUpdateJSon(String listItemEntityTypeFullName)
    {
        String stringValue = "{ '__metadata': { 'type': '" + listItemEntityTypeFullName + "' }";

        if (title != null && title.length() > 0)
        {
            stringValue += ", 'Title':'" + Util.encodeJson(title) + "'";
        }

        stringValue += " }";

        return stringValue;
    }

    /**
     * Gets the file system object type.
     *
     * @return the file system object type
     */
    public FileSystemObjectType getFileSystemObjectType()
    {
        return fileSystemObjectType;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public int getId()
    {
        return id;
    }

    /**
     * Gets the content type id.
     *
     * @return the content type id
     */
    public String getContentTypeId()
    {
        return contentTypeId;
    }

    /**
     * Gets the title.
     *
     * @return the title
     */
    public String getTitle()
    {
        return title;
    }

    /**
     * Sets the title.
     *
     * @param title the new title
     */
    public void setTitle(String title)
    {
        this.title = title;
    }

    /**
     * Gets the description.
     *
     * @return the description
     */
    public String getDescription()
    {
        return description;
    }

    /**
     * Gets the modified time.
     *
     * @return the modified time
     */
    public Date getModifiedTime()
    {
        return modifiedTime;
    }

    /**
     * Gets the created time.
     *
     * @return the created time
     */
    public Date getCreatedTime()
    {
        return createdTime;
    }

    /**
     * Gets the author id.
     *
     * @return the author id
     */
    public int getAuthorId()
    {
        return authorId;
    }

    /**
     * Gets the editor id.
     *
     * @return the editor id
     */
    public int getEditorId()
    {
        return editorId;
    }

    /**
     * Gets the UI version.
     *
     * @return the UI version
     */
    public String getUIVersion()
    {
        return uiVersion;
    }

    /**
     * Checks for attachments.
     *
     * @return true, if successful
     */
    public boolean hasAttachments()
    {
        return hasAttachments;
    }

    /**
     * Gets the guid.
     *
     * @return the guid
     */
    public String getGuid()
    {
        return guid;
    }
}
