package com.independentsoft.share;

import java.io.InputStream;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

/**
 * The Class EventReceiver.
 */
public class EventReceiver {
    
    private String receiverAssembly;
    private String receiverClass;
    private String receiverId;
    private String receiverName;
    private String receiverUrl;
    private int sequenceNumber;
    private int synchronization;
    private EventReceiverType type = EventReceiverType.NONE;

    /**
     * Instantiates a new event receiver.
     */
    public EventReceiver()
    {
    }

    EventReceiver(InputStream inputStream) throws XMLStreamException
    {
        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        XMLStreamReader reader = xmlInputFactory.createXMLStreamReader(inputStream);

        parse(reader);
    }

    EventReceiver(XMLStreamReader reader) throws XMLStreamException
    {
        parse(reader);
    }

    private void parse(XMLStreamReader reader) throws XMLStreamException
    {
        while (reader.hasNext())
        {
            if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("properties") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices/metadata"))
            {
                while (reader.hasNext())
                {
                    if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("ReceiverAssembly") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        receiverAssembly = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("ReceiverClass") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        receiverClass = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("ReceiverId") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        receiverId = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("ReceiverName") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        receiverName = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("ReceiverUrl") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        receiverUrl = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("SequenceNumber") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            sequenceNumber = Integer.parseInt(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Synchronization") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            synchronization = Integer.parseInt(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("EventType") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            type = EnumUtil.parseEventReceiverType(stringValue);
                        }
                    }

                    if (reader.isEndElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("properties") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices/metadata"))
                    {
                        break;
                    }
                    else
                    {
                        reader.next();
                    }
                }
            }

            if (reader.isEndElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("entry") && reader.getNamespaceURI().equals("http://www.w3.org/2005/Atom"))
            {
                break;
            }
            else
            {
                reader.next();
            }
        }
    }

    /**
     * Gets the receiver assembly.
     *
     * @return the receiver assembly
     */
    public String getReceiverAssembly()
    {
        return receiverAssembly;
    }

    /**
     * Gets the receiver class.
     *
     * @return the receiver class
     */
    public String getReceiverClass()
    {
        return receiverClass;
    }

    /**
     * Gets the receiver id.
     *
     * @return the receiver id
     */
    public String getReceiverId()
    {
        return receiverId;
    }

    /**
     * Gets the receiver name.
     *
     * @return the receiver name
     */
    public String getReceiverName()
    {
        return receiverName;
    }

    /**
     * Gets the receiver url.
     *
     * @return the receiver url
     */
    public String getReceiverUrl()
    {
        return receiverUrl;
    }

    /**
     * Gets the sequence number.
     *
     * @return the sequence number
     */
    public int getSequenceNumber()
    {
        return sequenceNumber;
    }

    /**
     * Gets the synchronization.
     *
     * @return the synchronization
     */
    public int getSynchronization()
    {
        return synchronization;
    }

    /**
     * Gets the type.
     *
     * @return the type
     */
    public EventReceiverType getType()
    {
        return type;
    }
}
