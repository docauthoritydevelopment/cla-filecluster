package com.independentsoft.share;

/**
 * The Class ChangeQuery.
 */
public class ChangeQuery {

    private boolean add;
    private boolean alert;
    private ChangeToken changeTokenEnd;
    private ChangeToken changeTokenStart;
    private boolean contentType;
    private boolean delete;
    private boolean field;
    private boolean file;
    private boolean folder;
    private boolean group;
    private boolean groupMembershipAdd;
    private boolean groupMembershipDelete;
    private boolean item;
    private boolean list;
    private boolean move;
    private boolean navigation;
    private boolean rename;
    private boolean restore;
    private boolean roleAssignmentAdd;
    private boolean roleAssignmentDelete;
    private boolean roleDefinitionAdd;
    private boolean roleDefinitionDelete;
    private boolean roleDefinitionUpdate;
    private boolean securityPolicy;
    private boolean site;
    private boolean systemUpdate;
    private boolean update;
    private boolean user;
    private boolean view;
    private boolean web;

    /**
     * Instantiates a new change query.
     */
    public ChangeQuery()
    {
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        String stringValue = "{ 'query': { '__metadata':{ 'type': 'SP.ChangeQuery' }";

        if (add)
        {
            stringValue += ", 'Add': true";
        }

        if (alert)
        {
            stringValue += ", 'Alert': true";
        }

        if (changeTokenEnd != null)
        {
            stringValue += ", 'ChangeTokenEnd':" + changeTokenEnd.toString();
        }

        if (changeTokenStart != null)
        {
            stringValue += ", 'ChangeTokenStart':" + changeTokenStart.toString();
        }

        if (contentType)
        {
            stringValue += ", 'ContentType': true";
        }

        if (delete)
        {
            stringValue += ", 'DeleteObject': true";
        }

        if (field)
        {
            stringValue += ", 'Field': true";
        }

        if (file)
        {
            stringValue += ", 'File': true";
        }

        if (folder)
        {
            stringValue += ", 'Folder': true";
        }

        if (group)
        {
            stringValue += ", 'Group': true";
        }

        if (groupMembershipAdd)
        {
            stringValue += ", 'GroupMembershipAdd': true";
        }

        if (groupMembershipDelete)
        {
            stringValue += ", 'GroupMembershipDelete': true";
        }

        if (item)
        {
            stringValue += ", 'Item': true";
        }

        if (list)
        {
            stringValue += ", 'List': true";
        }

        if (move)
        {
            stringValue += ", 'Move': true";
        }

        if (navigation)
        {
            stringValue += ", 'Navigation': true";
        }

        if (rename)
        {
            stringValue += ", 'Rename': true";
        }

        if (restore)
        {
            stringValue += ", 'Restore': true";
        }

        if (roleAssignmentAdd)
        {
            stringValue += ", 'RoleAssignmentAdd': true";
        }

        if (roleAssignmentDelete)
        {
            stringValue += ", 'RoleAssignmentDelete': true";
        }

        if (roleDefinitionAdd)
        {
            stringValue += ", 'RoleDefinitionAdd': true";
        }

        if (roleDefinitionDelete)
        {
            stringValue += ", 'RoleDefinitionDelete': true";
        }

        if (roleDefinitionUpdate)
        {
            stringValue += ", 'RoleDefinitionUpdate': true";
        }

        if (securityPolicy)
        {
            stringValue += ", 'SecurityPolicy': true";
        }

        if (site)
        {
            stringValue += ", 'Site': true";
        }

        if (systemUpdate)
        {
            stringValue += ", 'SystemUpdate': true";
        }

        if (update)
        {
            stringValue += ", 'Update': true";
        }

        if (user)
        {
            stringValue += ", 'User': true";
        }

        if (view)
        {
            stringValue += ", 'View': true";
        }

        if (web)
        {
            stringValue += ", 'Web': true";
        }

        stringValue +=" } }";

        return stringValue;
    }

    /**
     * Checks if is adds the.
     *
     * @return true, if is adds the
     */
    public boolean isAdd()
    {
        return add;
    }

    /**
     * Sets the adds the.
     *
     * @param add the new adds the
     */
    public void setAdd(boolean add)
    {
        this.add = add;
    }

    /**
     * Checks if is alert.
     *
     * @return true, if is alert
     */
    public boolean isAlert()
    {
        return alert;
    }

    /**
     * Sets the alert.
     *
     * @param alert the new alert
     */
    public void setAlert(boolean alert)
    {
        this.alert = alert;
    }

    /**
     * Gets the change token end.
     *
     * @return the change token end
     */
    public ChangeToken getChangeTokenEnd()
    {
        return changeTokenEnd;
    }

    /**
     * Sets the change token end.
     *
     * @param changeTokenEnd the new change token end
     */
    public void setChangeTokenEnd(ChangeToken changeTokenEnd)
    {
        this.changeTokenEnd = changeTokenEnd;
    }

    /**
     * Gets the change token start.
     *
     * @return the change token start
     */
    public ChangeToken getChangeTokenStart()
    {
        return changeTokenStart;
    }

    /**
     * Sets the change token start.
     *
     * @param changeTokenStart the new change token start
     */
    public void setChangeTokenStart(ChangeToken changeTokenStart)
    {
        this.changeTokenStart = changeTokenStart;
    }

    /**
     * Checks if is content type.
     *
     * @return true, if is content type
     */
    public boolean isContentType()
    {
        return contentType;
    }

    /**
     * Sets the content type.
     *
     * @param contentType the new content type
     */
    public void setContentType(boolean contentType)
    {
        this.contentType = contentType;
    }

    /**
     * Checks if is delete.
     *
     * @return true, if is delete
     */
    public boolean isDelete()
    {
        return delete;
    }

    /**
     * Sets the delete.
     *
     * @param delete the new delete
     */
    public void setDelete(boolean delete)
    {
        this.delete = delete;
    }

    /**
     * Checks if is field.
     *
     * @return true, if is field
     */
    public boolean isField()
    {
        return field;
    }

    /**
     * Sets the field.
     *
     * @param field the new field
     */
    public void setField(boolean field)
    {
        this.field = field;
    }

    /**
     * Checks if is file.
     *
     * @return true, if is file
     */
    public boolean isFile()
    {
        return file;
    }

    /**
     * Sets the file.
     *
     * @param file the new file
     */
    public void setFile(boolean file)
    {
        this.file = file;
    }

    /**
     * Checks if is folder.
     *
     * @return true, if is folder
     */
    public boolean isFolder()
    {
        return folder;
    }

    /**
     * Sets the folder.
     *
     * @param folder the new folder
     */
    public void setFolder(boolean folder)
    {
        this.folder = folder;
    }

    /**
     * Checks if is group.
     *
     * @return true, if is group
     */
    public boolean isGroup()
    {
        return group;
    }

    /**
     * Sets the group.
     *
     * @param group the new group
     */
    public void setGroup(boolean group)
    {
        this.group = group;
    }

    /**
     * Checks if is group membership add.
     *
     * @return true, if is group membership add
     */
    public boolean isGroupMembershipAdd()
    {
        return groupMembershipAdd;
    }

    /**
     * Sets the group membership add.
     *
     * @param groupMembershipAdd the new group membership add
     */
    public void setGroupMembershipAdd(boolean groupMembershipAdd)
    {
        this.groupMembershipAdd = groupMembershipAdd;
    }

    /**
     * Checks if is group membership delete.
     *
     * @return true, if is group membership delete
     */
    public boolean isGroupMembershipDelete()
    {
        return groupMembershipDelete;
    }

    /**
     * Sets the group membership delete.
     *
     * @param groupMembershipDelete the new group membership delete
     */
    public void setGroupMembershipDelete(boolean groupMembershipDelete)
    {
        this.groupMembershipDelete = groupMembershipDelete;
    }

    /**
     * Checks if is item.
     *
     * @return true, if is item
     */
    public boolean isItem()
    {
        return item;
    }

    /**
     * Sets the item.
     *
     * @param item the new item
     */
    public void setItem(boolean item)
    {
        this.item = item;
    }

    /**
     * Checks if is list.
     *
     * @return true, if is list
     */
    public boolean isList()
    {
        return list;
    }

    /**
     * Sets the list.
     *
     * @param list the new list
     */
    public void setList(boolean list)
    {
        this.list = list;
    }

    /**
     * Checks if is move.
     *
     * @return true, if is move
     */
    public boolean isMove()
    {
        return move;
    }

    /**
     * Sets the move.
     *
     * @param move the new move
     */
    public void setMove(boolean move)
    {
        this.move = move;
    }

    /**
     * Checks if is navigation.
     *
     * @return true, if is navigation
     */
    public boolean isNavigation()
    {
        return navigation;
    }

    /**
     * Sets the navigation.
     *
     * @param navigation the new navigation
     */
    public void setNavigation(boolean navigation)
    {
        this.navigation = navigation;
    }

    /**
     * Checks if is rename.
     *
     * @return true, if is rename
     */
    public boolean isRename()
    {
        return rename;
    }

    /**
     * Sets the rename.
     *
     * @param rename the new rename
     */
    public void setRename(boolean rename)
    {
        this.rename = rename;
    }

    /**
     * Checks if is restore.
     *
     * @return true, if is restore
     */
    public boolean isRestore()
    {
        return restore;
    }

    /**
     * Sets the restore.
     *
     * @param restore the new restore
     */
    public void setRestore(boolean restore)
    {
        this.restore = restore;
    }

    /**
     * Checks if is role assignment add.
     *
     * @return true, if is role assignment add
     */
    public boolean isRoleAssignmentAdd()
    {
        return roleAssignmentAdd;
    }

    /**
     * Sets the role assignment add.
     *
     * @param roleAssignmentAdd the new role assignment add
     */
    public void setRoleAssignmentAdd(boolean roleAssignmentAdd)
    {
        this.roleAssignmentAdd = roleAssignmentAdd;
    }

    /**
     * Checks if is role assignment delete.
     *
     * @return true, if is role assignment delete
     */
    public boolean isRoleAssignmentDelete()
    {
        return roleAssignmentDelete;
    }

    /**
     * Sets the role assignment delete.
     *
     * @param roleAssignmentDelete the new role assignment delete
     */
    public void setRoleAssignmentDelete(boolean roleAssignmentDelete)
    {
        this.roleAssignmentDelete = roleAssignmentDelete;
    }

    /**
     * Checks if is role definition add.
     *
     * @return true, if is role definition add
     */
    public boolean isRoleDefinitionAdd()
    {
        return roleDefinitionAdd;
    }

    /**
     * Sets the role definition add.
     *
     * @param roleDefinitionAdd the new role definition add
     */
    public void setRoleDefinitionAdd(boolean roleDefinitionAdd)
    {
        this.roleDefinitionAdd = roleDefinitionAdd;
    }

    /**
     * Checks if is role definition delete.
     *
     * @return true, if is role definition delete
     */
    public boolean isRoleDefinitionDelete()
    {
        return roleDefinitionDelete;
    }

    /**
     * Sets the role definition delete.
     *
     * @param roleDefinitionDelete the new role definition delete
     */
    public void setRoleDefinitionDelete(boolean roleDefinitionDelete)
    {
        this.roleDefinitionDelete = roleDefinitionDelete;
    }

    /**
     * Checks if is role definition update.
     *
     * @return true, if is role definition update
     */
    public boolean isRoleDefinitionUpdate()
    {
        return roleDefinitionUpdate;
    }

    /**
     * Sets the role definition update.
     *
     * @param roleDefinitionUpdate the new role definition update
     */
    public void setRoleDefinitionUpdate(boolean roleDefinitionUpdate)
    {
        this.roleDefinitionUpdate = roleDefinitionUpdate;
    }

    /**
     * Checks if is security policy.
     *
     * @return true, if is security policy
     */
    public boolean isSecurityPolicy()
    {
        return securityPolicy;
    }

    /**
     * Sets the security policy.
     *
     * @param securityPolicy the new security policy
     */
    public void setSecurityPolicy(boolean securityPolicy)
    {
        this.securityPolicy = securityPolicy;
    }

    /**
     * Checks if is site.
     *
     * @return true, if is site
     */
    public boolean isSite()
    {
        return site;
    }

    /**
     * Sets the site.
     *
     * @param site the new site
     */
    public void setSite(boolean site)
    {
        this.site = site;
    }

    /**
     * Checks if is system update.
     *
     * @return true, if is system update
     */
    public boolean isSystemUpdate()
    {
        return systemUpdate;
    }

    /**
     * Sets the system update.
     *
     * @param systemUpdate the new system update
     */
    public void setSystemUpdate(boolean systemUpdate)
    {
        this.systemUpdate = systemUpdate;
    }

    /**
     * Checks if is update.
     *
     * @return true, if is update
     */
    public boolean isUpdate()
    {
        return update;
    }

    /**
     * Sets the update.
     *
     * @param update the new update
     */
    public void setUpdate(boolean update)
    {
        this.update = update;
    }

    /**
     * Checks if is user.
     *
     * @return true, if is user
     */
    public boolean isUser()
    {
        return user;
    }

    /**
     * Sets the user.
     *
     * @param user the new user
     */
    public void setUser(boolean user)
    {
        this.user = user;
    }

    /**
     * Checks if is view.
     *
     * @return true, if is view
     */
    public boolean isView()
    {
        return view;
    }

    /**
     * Sets the view.
     *
     * @param view the new view
     */
    public void setView(boolean view)
    {
        this.view = view;
    }

    /**
     * Checks if is web.
     *
     * @return true, if is web
     */
    public boolean isWeb()
    {
        return web;
    }

    /**
     * Sets the web.
     *
     * @param web the new web
     */
    public void setWeb(boolean web)
    {
        this.web = web;
    }
}
