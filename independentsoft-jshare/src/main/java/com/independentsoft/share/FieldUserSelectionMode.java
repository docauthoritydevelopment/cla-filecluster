package com.independentsoft.share;

/**
 * The Enum FieldUserSelectionMode.
 */
public enum FieldUserSelectionMode {

    PEOPLE,
    PEOPLE_AND_GROUPS,
    NONE
}
