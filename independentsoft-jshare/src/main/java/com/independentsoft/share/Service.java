package com.independentsoft.share;

import com.google.common.base.Strings;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.independentsoft.share.fql.IRestriction;
import com.independentsoft.share.queryoptions.IQueryOption;
import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.StatusLine;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.Credentials;
import org.apache.http.auth.NTCredentials;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CookieStore;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.AuthSchemes;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.*;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.HttpClientConnectionManager;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContexts;
import org.apache.http.entity.BufferedHttpEntity;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.auth.BasicSchemeFactory;
import org.apache.http.impl.auth.DigestSchemeFactory;
import org.apache.http.impl.auth.KerberosSchemeFactory;
import org.apache.http.impl.auth.SPNegoSchemeFactory;
import org.apache.http.impl.client.*;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.protocol.HttpContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.*;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamReader;
import java.io.*;
import java.net.*;
import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.List;
import java.util.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.zip.GZIPInputStream;

//import android.util.Log; //Uncomment on Android

/**
 * Service class contains the methods to perform operations on SharePoint server.
 */
public class Service {
    private static final Logger logger = LoggerFactory.getLogger(Service.class);

    /* DA */
    private static final int VERSION_DEFAULT = 2016;
    static final int VERSION_2013 = 2013;

    // 2013>=
    private static final String GET_FILE_RESTRICTED_CHARS = "_api/web/GetFileByServerRelativeUrl('";

    // 2013<
    private static final String GET_FILE_ALL_CHARS = "_api/web/GetFileByServerRelativePath(decodedurl='";

    private static final int DEFAULT_TIMEOUT = 60000;

    private String userAgent = "JShare 1.0, www.independentsoft.com";
    /* DA- End */

    private String siteUrl;
    private String username;
    private String password;
    private String domain;
    private boolean acceptGzipEncoding = false;
    private CookieStore cookieStore;
    private List<String> urlConnectionCookies;
    private LoadingCache<String, ContextInfo> contextInfoCache;

    private ServerVersionInfo serverVersionInfo = ServerVersionInfo.NONE;
    private boolean sharePointOnlineRegisteredApp = false;

    //HttpURLConnection
    private boolean useHttpURLConnection = false;
    private Proxy httpURLConnectionProxy = Proxy.NO_PROXY;
    private int connectTimeout;
    private int readTimeout;

    //Apache HttpClient
    private CloseableHttpClient httpClient;
    private Credentials proxyCredentials;
    private HttpHost proxy;
    private HttpClientConnectionManager connectionManager;
    private RequestConfig requestConfig;
    private Function<CloseableHttpClient, Header[]> appSignInHeadersRetriever = null;

    private SSLConnectionSocketFactory socketFactory; //Remove on Android
    //private AndroidSSLSocketFactory socketFactory; //Uncomment on Android

    private Header[] customHeaders;

    /* DA */
    private final Lock httpClientInitLock = new ReentrantLock();

    private final String getFileApiCall;
    private final String getFolderApiCall = "_api/web/GetFolderByServerRelativeUrl('";

    private String defaultSubsite;

    /* DA- End */

    /**
     * Initializes a new instance of the Service class.
     */
   /* DA public Service()
    {
        init();
    }*/

    // DA
    public Service() {
        this(null, null, VERSION_DEFAULT, false, null, null, DEFAULT_TIMEOUT, DEFAULT_TIMEOUT);
    }

    /**
     * Initializes a new instance of the Service class.
     *
     * @param siteUrl  The SharePoint site url. Example: "https://servername/site".
     * @param username Username
     * @param password Password
     */
   /* DA
   public Service(String siteUrl, String username, String password)
    {
        this.siteUrl = siteUrl;
        this.username = username;
        this.password = password;

        init();
    }*/

    //DA
    public Service(String siteUrl, String defaultSubsite, int spInstanceVersion, boolean isSpecialCharsSupported, String username, String password, int connectTimeout, int readTimeout) {
        this(siteUrl, defaultSubsite, spInstanceVersion, isSpecialCharsSupported, username, password, null, connectTimeout, readTimeout);
    }

    /**
     * Initializes a new instance of the Service class.
     *
     * @param siteUrl  The SharePoint site url. Example: "https://servername/site".
     * @param username Username
     * @param password Password
     * @param domain   User' domain
     */
// DA    public Service(String siteUrl, String username, String password, String domain) {
    public Service(String siteUrl, String defaultSubsite, int spInstanceVersion, boolean isSpecialCharsSupported, String username,
                   String password, String domain, int connectTimeout, int readTimeout) { // DA
        this.siteUrl = siteUrl;
        this.defaultSubsite = defaultSubsite;
        this.username = username;
        this.password = password;
        this.domain = domain;
        this.connectTimeout = connectTimeout; // DA
        this.readTimeout = readTimeout; // DA

        init();
        getFileApiCall = isSpecialCharsSupported && VERSION_DEFAULT <= spInstanceVersion ? GET_FILE_ALL_CHARS : GET_FILE_RESTRICTED_CHARS; // DA

        contextInfoCache = CacheBuilder.newBuilder()
                .maximumSize(1000)
                .build(new CacheLoader<String, ContextInfo>() {
                    @Override
                    public ContextInfo load(String key) throws Exception {
                        return getContextInfo(key);
                    }
                });
    }

    private void init() {
        try {
            //Uncomment on Android
            //System.setProperty("javax.xml.stream.XMLInputFactory", "com.sun.xml.stream.ZephyrParserFactory");
            //System.setProperty("javax.xml.stream.XMLOutputFactory", "com.sun.xml.stream.ZephyrWriterFactory");
            //System.setProperty("javax.xml.stream.XMLEventFactory", "com.sun.xml.stream.events.ZephyrEventFactory");
            //Thread.currentThread().setContextClassLoader(getClass().getClassLoader());
            //HttpClientParams.setRedirecting(httpParams, false);

            HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {

                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });

            SSLContext context = SSLContexts.custom().useTLS().build();

            context.init(null, new X509TrustManager[]
                    {
                            new X509TrustManager() {

                                @Override
                                public void checkClientTrusted(X509Certificate[] chain, String authType) {
                                }

                                @Override
                                public void checkServerTrusted(X509Certificate[] chain, String authType) {
                                }

                                @Override
                                public X509Certificate[] getAcceptedIssuers() {
                                    return new X509Certificate[0];
                                }
                            }
                    }, new SecureRandom());

            socketFactory = new SSLConnectionSocketFactory(context, SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);//Remove on Android
            //socketFactory = new AndroidSSLSocketFactory();//Uncomment on Android

            Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder.<ConnectionSocketFactory>create()
                    .register("http", PlainConnectionSocketFactory.INSTANCE)
                    .register("https", socketFactory)
                    .build();

            connectionManager = new PoolingHttpClientConnectionManager(socketFactoryRegistry);

            HttpsURLConnection.setDefaultSSLSocketFactory(context.getSocketFactory());

            java.util.logging.Logger.getLogger("org.apache.http.impl.client.DefaultRequestDirector").setLevel(Level.OFF);

            requestConfig = RequestConfig.custom()
// DA                    .setSocketTimeout(60000) //60 seconds
// DA                   .setConnectTimeout(60000) //60 seconds
                    .setSocketTimeout(readTimeout) //DA
                    .setConnectTimeout(connectTimeout) //DA
                    .setProxyPreferredAuthSchemes(Arrays.asList(AuthSchemes.BASIC, AuthSchemes.DIGEST, AuthSchemes.NTLM, AuthSchemes.SPNEGO, AuthSchemes.KERBEROS))
                    .setTargetPreferredAuthSchemes(Arrays.asList(AuthSchemes.BASIC, AuthSchemes.DIGEST, AuthSchemes.NTLM, AuthSchemes.SPNEGO, AuthSchemes.KERBEROS))
                    .build();
        } catch (Exception e) {
            logger.error("Error initializing JShare service", e);
        }
    }

    // DA
    private InputStream sendRequest(String method, String requestUrl) throws Exception {
        return sendRequest(method, null, requestUrl, null);
    }

    // DA    private InputStream sendRequest(String method, String subSite, String requestUrl) throws Exception {
    private InputStream sendRequest(String method, String subSite, String requestUrl) throws Exception {// DA
        return sendRequest(method, subSite, requestUrl, null);
    }

    // DA    private InputStream sendRequest(String method, String requestUrl, String requestBody) throws Exception {
    private InputStream sendRequest(String method, String subSite, String requestUrl, String requestBody) throws Exception {// DA
        return sendRequest(method, subSite, requestUrl, requestBody, null);
    }

    // DA    private InputStream sendRequest(String method, String requestUrl, String requestBody, String xHttpMethod) throws Exception {
    private InputStream sendRequest(String method, String subSite, String requestUrl, String requestBody, String xHttpMethod) throws Exception {// DA
        return sendRequest(method, subSite, requestUrl, requestBody, xHttpMethod, null);
    }

    // DA    private InputStream sendRequest(String method, String requestUrl, String requestBody, String xHttpMethod, String ifMatch) throws Exception {
    private InputStream sendRequest(String method, String subSite, String requestUrl, String requestBody, String xHttpMethod, String ifMatch) throws Exception {// DA
        return sendRequest(method, subSite, requestUrl, requestBody, xHttpMethod, ifMatch, null, true, false);
    }

    // DA    private InputStream sendRequest(String method, String requestUrl, String requestBody, String xHttpMethod, String ifMatch, InputStream requestStream, boolean readResponseStream, boolean contextInfoCall) throws Exception {
    private InputStream sendRequest(String method, String subSite, String requestUrl, String requestBody, String xHttpMethod, String ifMatch, InputStream requestStream, boolean readResponseStream, boolean contextInfoCall) throws Exception {// DA
        System.setProperty("http.auth.preference", "basic");
        System.setProperty("sun.security.ssl.allowUnsafeRenegotiation", "true");
        logger.trace("sendRequest({},{},{}..) serverVersionInfo={} sharePointOnlineRegisteredApp={}", method, subSite, requestUrl, serverVersionInfo, sharePointOnlineRegisteredApp);

        synchronized (this) { // This is to login only once and associate 1 token to http client
            if (serverVersionInfo == ServerVersionInfo.NONE) {
                setSharePointVersionInfo();

                if (serverVersionInfo == ServerVersionInfo.SHARE_POINT_ONLINE && !sharePointOnlineRegisteredApp) {
                    loginSharePointOnline();
                }
            }
        }

        String adjustedSubSite = adjustSitePath(subSite);
// DA        String serviceUrl = this.siteUrl.endsWith("/") ? this.siteUrl + requestUrl : this.siteUrl + "/" + requestUrl;
        String serviceUrl = (this.siteUrl.endsWith("/") ? this.siteUrl + adjustedSubSite : this.siteUrl + "/" + adjustedSubSite)
                + requestUrl;// DA

        logger.trace("url {}", serviceUrl);

        if (!useHttpURLConnection) {
//            HttpResponse response = null; // DA
            CloseableHttpResponse response = null; // DA
            try {
                boolean redirect = true;

                while (redirect) {
                    URI uri = new URI(serviceUrl);

                    HttpRequestBase httpMethod = getHttpMethod(uri, method, requestBody, requestStream);
// DA           httpMethod.setHeader("User-Agent", "JShare 1.0, www.independentsoft.com");
                    httpMethod.setHeader("User-Agent", userAgent); // DA
                    httpMethod.setHeader("Accept", "application/atom+xml");
                    httpMethod.setHeader("Content-Type", "application/json;odata=verbose");

                    if (acceptGzipEncoding) {
                        httpMethod.setHeader("Accept-Encoding", "gzip");
                    }

                    if (customHeaders != null) {
                        Stream.of(customHeaders).forEach(h -> httpMethod.setHeader(h));
                    }

                    if (xHttpMethod != null && xHttpMethod.length() > 0) {
                        httpMethod.setHeader("X-HTTP-Method", xHttpMethod);
                    }

                    if (ifMatch != null && ifMatch.length() > 0) {
                        httpMethod.setHeader("IF-MATCH", ifMatch);
                    }

                    ContextInfo contextInfo = null;

                    if (!contextInfoCall) {
                        contextInfo = resolveContextInfo(subSite);
                    }

                    if (contextInfo != null) {
                        httpMethod.setHeader("X-RequestDigest", contextInfo.getFormDigestValue());
                    }

                    createHttpClientIfNeeded(uri);

                    if (appSignInHeadersRetriever != null) {
                        Header[] appSignInHeaders = appSignInHeadersRetriever.apply(httpClient);
                        if (appSignInHeaders != null) {
                            Stream.of(appSignInHeaders).forEach(h -> httpMethod.setHeader(h));
                            if (logger.isTraceEnabled()) {
                                logger.trace("Retrieved appSignInHeaders: {}", Stream.of(appSignInHeaders).map(h -> h.getName() + ":" + h.getValue()).collect(Collectors.joining(";")));
                            }
                        }
                    }

                    response = httpClient.execute(httpMethod, getContext());

                    logger.trace("response {}", response);
                    logger.trace("statusLine {}", response.getStatusLine());

                    StatusLine statusLine = response.getStatusLine();

                    if (statusLine.getStatusCode() >= 300 && statusLine.getStatusCode() < 400) {
                        Header[] locationHeader = response.getHeaders("Location");

                        if (locationHeader.length > 0) {
                            serviceUrl = locationHeader[0].getValue();
                            continue;
                        }
                    } else if (statusLine.getStatusCode() == 500) {
                        Header[] contentTypeHeader = response.getHeaders("Content-Type");

                        if (contentTypeHeader != null && contentTypeHeader.length > 0) {
                            String contentType = contentTypeHeader[0].getValue();

                            if (contentType != null) {
                                HttpEntity responseEntity = response.getEntity();
//                                InputStream inputStream = responseEntity.getContent(); // DA
                                InputStream inputStream = IOUtils.toBufferedInputStream(responseEntity.getContent()); // DA

                                Header[] contentEncodingHeader = response.getHeaders("Content-Encoding");

                                if (contentEncodingHeader != null && contentEncodingHeader.length > 0) {
                                    String contentEncoding = contentEncodingHeader[0].getValue();

                                    if (contentEncoding != null && contentEncoding.equals("gzip")) {
//                                        inputStream = new GZIPInputStream(new BufferedInputStream(inputStream)); // DA
                                        inputStream = new GZIPInputStream(new BufferedInputStream(inputStream)); // DA
                                    }
                                }

                                throw new ServiceException(inputStream, requestUrl, requestBody);
                            }
                        }
                    } else if (statusLine.getStatusCode() >= 400) {
                        throw new ServiceException(Integer.toString(statusLine.getStatusCode()) + " " + statusLine.getReasonPhrase(), null, requestUrl, requestBody);
                    }

                    redirect = false;
                }

                HttpEntity responseEntity = response.getEntity();

                if (responseEntity != null) {
                    InputStream inputStream = responseEntity.getContent();

                    Header[] contentEncodingHeader = response.getHeaders("Content-Encoding");

                    if (contentEncodingHeader != null && contentEncodingHeader.length > 0) {
                        String contentEncoding = contentEncodingHeader[0].getValue();

                        if (contentEncoding != null && contentEncoding.equals("gzip")) {
                            inputStream = new GZIPInputStream(new BufferedInputStream(inputStream));
                        }
                    }

                    return readResponseStream ? getInputStream(inputStream) : IOUtils.toBufferedInputStream(inputStream); //DA

                /*if (readResponseStream) { // DA
                    inputStream = getInputStream(inputStream);
                }

                    return inputStream;
                    */
                } else {
                    return null;
                }
            } finally {
                finalizeHttpResponse(response);
            }
        } else {
            OutputStream outputStream = null;
            InputStream inputStream = null;

            try {
                if (serverVersionInfo == ServerVersionInfo.SHARE_POINT_ON_PREMISE && username != null && password != null) {
                    //AuthCacheValue.setAuthCache(new AuthCacheImpl());
                    Authenticator.setDefault(new Authenticator() {

                        @Override
                        protected PasswordAuthentication getPasswordAuthentication() {
                            //System.out.println("Authentication: " + this. getRequestingScheme());

                            if (domain != null) {
                                return new PasswordAuthentication(domain + "\\" + username, password.toCharArray());
                            } else {
                                return new PasswordAuthentication(username, password.toCharArray());
                            }
                        }
                    });
                }

                final URL url = new URL(serviceUrl);

                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection(httpURLConnectionProxy);
                urlConnection.setInstanceFollowRedirects(true);
                urlConnection.setDoInput(true);
                urlConnection.setDoOutput(true);
                urlConnection.setRequestMethod(method);
                urlConnection.setUseCaches(false);
                urlConnection.setConnectTimeout(connectTimeout);
                urlConnection.setReadTimeout(readTimeout);
                urlConnection.setRequestProperty("Content-Type", "application/json;odata=verbose");
                urlConnection.setRequestProperty("Accept", "application/atom+xml");
                //urlConnection.setRequestProperty("Connection", "close"); //http://download.oracle.com/javase/1.5.0/docs/guide/net/http-keepalive.html

// DA           urlConnection.setRequestProperty("User-Agent", "JShare 1.0, www.independentsoft.com");
                urlConnection.setRequestProperty("User-Agent", userAgent); // DA

                if (acceptGzipEncoding) {
                    urlConnection.setRequestProperty("Accept-Encoding", "gzip");
                }

                if (xHttpMethod != null && xHttpMethod.length() > 0) {
                    urlConnection.setRequestProperty("X-HTTP-Method", xHttpMethod);
                }

                if (ifMatch != null && ifMatch.length() > 0) {
                    urlConnection.setRequestProperty("IF-MATCH", ifMatch);
                }

                if (urlConnectionCookies != null && urlConnectionCookies.size() > 0) {
                    String cookiesString = "";

                    for (int i = 0; i < urlConnectionCookies.size(); i++) {
                        cookiesString += urlConnectionCookies.get(i);

                        if (i < urlConnectionCookies.size() - 1) {
                            cookiesString += ";";
                        }
                    }

                    urlConnection.setRequestProperty("Cookie", cookiesString);
                }

                if (ifMatch != null && ifMatch.length() > 0) {
                    urlConnection.setRequestProperty("IF-MATCH", ifMatch);
                }

                ContextInfo contextInfo = resolveContextInfo(subSite);

                urlConnection.setRequestProperty("X-RequestDigest", contextInfo.getFormDigestValue());

                urlConnection.connect();

                if (requestBody != null) {
                    urlConnection.setRequestProperty("Content-Length", "" + requestBody.length());

                    outputStream = urlConnection.getOutputStream();
                    outputStream.write(requestBody.getBytes("UTF-8"));
                    outputStream.flush();
                } else if (requestStream != null) {
                    urlConnection.setRequestProperty("Content-Length", "" + requestStream.available());
                    outputStream = urlConnection.getOutputStream();

                    BufferedInputStream bufferedInputStream = new BufferedInputStream(requestStream);
                    byte[] tempBuffer = new byte[2048];

                    try {
                        int count = bufferedInputStream.read(tempBuffer);

                        while (count != -1) {
                            outputStream.write(tempBuffer, 0, count);
                            count = bufferedInputStream.read(tempBuffer);
                        }
                    } finally {
                        bufferedInputStream.close();
                        outputStream.flush();
                        outputStream.close();
                    }
                }

                int responseCode = urlConnection.getResponseCode();

                if (responseCode == HttpURLConnection.HTTP_INTERNAL_ERROR) {
                    inputStream = urlConnection.getErrorStream();

                    String contentEncoding = urlConnection.getContentEncoding();

                    if (contentEncoding != null && contentEncoding.equals("gzip")) {
                        inputStream = new GZIPInputStream(new BufferedInputStream(inputStream));
                    }

                    throw new ServiceException(inputStream, requestUrl, requestBody);
                }

                inputStream = urlConnection.getInputStream();

                if (inputStream != null) {
                    String contentEncoding = urlConnection.getContentEncoding();

                    if (contentEncoding != null && contentEncoding.equals("gzip")) {
                        inputStream = new GZIPInputStream(new BufferedInputStream(inputStream));
                    }

                    if (readResponseStream) {
                        inputStream = getInputStream(inputStream);
                    }
                }
            } finally {
                if (outputStream != null) {
                    try {
                        outputStream.close();
                    } catch (IOException ex) {
                    }
                }
            }

            return inputStream;
        }
    }


    private ContextInfo resolveContextInfo(String subsite) throws ServiceException {

        String site = Optional.ofNullable(subsite).orElseGet(() -> Optional.ofNullable(defaultSubsite).orElse(""));

        ContextInfo contextInfo = contextInfoCache.getIfPresent(site);

        logContextInfoState(site, contextInfo);

        if (contextInfo == null || isInvalidContextInfo(contextInfo)) {
            try {
                logger.debug("Fetching new context info for site {} ...", site);
                contextInfoCache.invalidate(site);
                contextInfoCache.get(site);
                logger.debug("Updated new context info for site {}", site);
            } catch (Exception e) {
                throw new ServiceException("Error getting context info", e, "_api/contextinfo");
            }
        }
        return contextInfo;
    }

    private void logContextInfoState(String site, ContextInfo contextInfo) {
        if (logger.isTraceEnabled()) {
            if (contextInfo == null) {
                logger.trace("Context info is null for site {}", site);
            } else {
                logger.trace("Context info exists for site {}, taking from cache", site);
            }
        }
    }


    private boolean isInvalidContextInfo(ContextInfo contextInfo) {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.SECOND, -60);
        boolean expired = contextInfo.getFormDigestValueExpireTime().compareTo(cal.getTime()) < 0;
        if (logger.isDebugEnabled() && expired) {
            logger.debug("Digest expired, context info is not valid anymore");
        }
        return expired;
    }

    // DA
    private String adjustSitePath(String site) {
        if (Strings.isNullOrEmpty(site)) {
            return "";
        }
        if (site.startsWith("/")) {
            site = site.substring(1);
        }
        if (!site.endsWith("/")) {
            site += "/";
        }

        return site;
    }

    // DA
    private void finalizeHttpResponse(CloseableHttpResponse response) {
        if (response != null) {
            try {
                response.close();
            } catch (IOException e) {
                logger.warn("Failed to close http-response", e);
            }
        }
    }

    // Fixme: thread safe
    private void createHttpClientIfNeeded(URI uri) {
        if (httpClient != null) {
            return;
        }

        try {
            httpClientInitLock.lock();
            if (httpClient != null) {
                return;
            }
            HttpClientBuilder builder = HttpClients.custom();

            CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
            builder.setDefaultCredentialsProvider(credentialsProvider);

            if (serverVersionInfo == ServerVersionInfo.SHARE_POINT_ON_PREMISE) {
                credentialsProvider.setCredentials(new AuthScope(uri.getHost(), uri.getPort(), AuthScope.ANY_REALM), new UsernamePasswordCredentials(username, password));
                credentialsProvider.setCredentials(new AuthScope(uri.getHost(), uri.getPort(), AuthScope.ANY_REALM), new NTCredentials(username, password, uri.getHost(), (domain != null ? domain : "")));
            } else {
                if (cookieStore != null) {
                    builder.setDefaultCookieStore(cookieStore);
                }
            }

            if (proxy != null && proxyCredentials != null) {
                credentialsProvider.setCredentials(new AuthScope(proxy.getHostName(), proxy.getPort(), AuthScope.ANY_REALM), proxyCredentials);
            }

            if (proxy != null) {
                builder.setProxy(proxy);
            }

            if (requestConfig != null) {
                builder.setDefaultRequestConfig(requestConfig);
            }

            if (connectionManager != null) {
                builder.setConnectionManager(connectionManager);
            }

            createAndSetAuthSchemeRegistery(builder);
            this.httpClient = builder.build();
        } finally {
            httpClientInitLock.unlock();
        }
    }

    private HttpRequestBase getHttpMethod(URI uri, String method, String requestBody, InputStream requestStream) throws UnsupportedEncodingException, IOException {
        if (method.equals("GET")) {
            HttpGet httpMethod = new HttpGet(uri);
            return httpMethod;
        } else if (method.equals("DELETE")) {
            HttpDelete httpMethod = new HttpDelete(uri);
            return httpMethod;
        } else if (method.equals("PUT")) {
            HttpPut httpMethod = new HttpPut(uri);
            return httpMethod;
        } else {
            HttpPost httpMethod = new HttpPost(uri);

            if (requestBody != null) {
                StringEntity requestEntity = new StringEntity(requestBody, "UTF-8");
                requestEntity.setContentType("application/json;odata=verbose");
                httpMethod.setEntity(requestEntity);
            } else if (requestStream != null) {
                InputStreamEntity inputStreamEntity = new InputStreamEntity(requestStream, requestStream.available());
                BufferedHttpEntity httpEntity = new BufferedHttpEntity(inputStreamEntity);
                httpMethod.setEntity(httpEntity);
            }

            return httpMethod;
        }
    }

    private InputStream sendLoginSharePointOnlineRequest(String requestUrl, String requestBody) throws Exception {
        if (!useHttpURLConnection) {
            System.setProperty("http.auth.preference", "basic");
            System.setProperty("sun.security.ssl.allowUnsafeRenegotiation", "true");


            StringEntity requestEntity = new StringEntity(requestBody, "UTF-8");
            requestEntity.setContentType("application/x-www-form-urlencoded");
// DA       HttpResponse response = null;
            URI uri = new URI(requestUrl);

            HttpPost httpPost = new HttpPost(uri);

// DA       httpPost.setHeader("User-Agent", "JShare 1.0, www.independentsoft.com");
            httpPost.setHeader("User-Agent", userAgent); // DA
            httpPost.setEntity(requestEntity);

            if (acceptGzipEncoding) {
                httpPost.setHeader("Accept-Encoding", "gzip");
            }

            CredentialsProvider credentialsProvider = new BasicCredentialsProvider();

            credentialsProvider.setCredentials(new AuthScope(uri.getHost(), uri.getPort(), AuthScope.ANY_REALM), new UsernamePasswordCredentials(username, password));
            credentialsProvider.setCredentials(new AuthScope(uri.getHost(), uri.getPort(), AuthScope.ANY_REALM), new NTCredentials(username, password, uri.getHost(), (domain != null ? domain : "")));

            if (proxy != null && proxyCredentials != null) {
                credentialsProvider.setCredentials(new AuthScope(proxy.getHostName(), proxy.getPort(), AuthScope.ANY_REALM), proxyCredentials);
            }

            HttpClientBuilder builder = HttpClients.custom();

            if (proxy != null) {
                builder.setProxy(proxy);
            }

            if (requestConfig != null) {
                builder.setDefaultRequestConfig(requestConfig);
            }

            if (connectionManager != null) {
                builder.setConnectionManager(connectionManager);
            }
            /* DA
            Registry<AuthSchemeProvider> authSchemeRegistry = RegistryBuilder.<AuthSchemeProvider>create()
                    .register(AuthSchemes.NTLM, new NTLMSchemeFactory())
                    .register(AuthSchemes.BASIC, new BasicSchemeFactory())
                    .register(AuthSchemes.DIGEST, new DigestSchemeFactory())
                    .register(AuthSchemes.SPNEGO, new SPNegoSchemeFactory())
                    .register(AuthSchemes.KERBEROS, new KerberosSchemeFactory())
                    .build();
            builder.setDefaultAuthSchemeRegistry(authSchemeRegistry);
            httpClient = builder.setDefaultCredentialsProvider(credentialsProvider).build();
            */

            createAndSetAuthSchemeRegistery(builder); //DA

            cookieStore = new BasicCookieStore();
            HttpClientContext context = HttpClientContext.create();
            context.setCookieStore(cookieStore);

            CloseableHttpClient client = builder.setDefaultCredentialsProvider(credentialsProvider).build(); // DA
            CloseableHttpResponse response = null; //DA
            response = client.execute(httpPost, context);
            try {
                cookieStore = context.getCookieStore();
                StatusLine statusLine = response.getStatusLine();


                response = client.execute(httpPost, context); // DA

                if (statusLine.getStatusCode() == 500) {
                    Header[] contentTypeHeader = response.getHeaders("Content-Type");

                    if (contentTypeHeader != null && contentTypeHeader.length > 0) {
                        String contentType = contentTypeHeader[0].getValue();

                        if (contentType != null) {
                            HttpEntity responseEntity = response.getEntity();
                            InputStream inputStream = IOUtils.toBufferedInputStream(responseEntity.getContent());

                            Header[] contentEncodingHeader = response.getHeaders("Content-Encoding");

                            if (contentEncodingHeader != null && contentEncodingHeader.length > 0) {
                                String contentEncoding = contentEncodingHeader[0].getValue();

                                if (contentEncoding != null && contentEncoding.equals("gzip")) {
                                    inputStream = new GZIPInputStream(new BufferedInputStream(inputStream));
                                }
                            }

                            throw new ServiceException(inputStream, requestUrl, requestBody);
                        }
                    }
                } else if (statusLine.getStatusCode() >= 400) {
                    throw new ServiceException(Integer.toString(statusLine.getStatusCode()) + " " + statusLine.getReasonPhrase(), null, requestUrl, null);
                }

                HttpEntity responseEntity = response.getEntity();
                if (responseEntity != null) {
// DA                    InputStream inputStream = responseEntity.getContent();
                    InputStream inputStream = IOUtils.toBufferedInputStream(responseEntity.getContent());// DA

                    Header[] contentEncodingHeader = response.getHeaders("Content-Encoding");

                    if (contentEncodingHeader != null && contentEncodingHeader.length > 0) {
                        String contentEncoding = contentEncodingHeader[0].getValue();

                        if (contentEncoding != null && contentEncoding.equals("gzip")) {
                            inputStream = new GZIPInputStream(new BufferedInputStream(inputStream));
                        }
                    }

                    inputStream = getInputStream(inputStream);

                    return inputStream;
                } else {
                    return null;
                }
            } finally {// DA
                finalizeHttpResponse(response);
            }
        } else {
            OutputStream outputStream = null;
            InputStream inputStream = null;

            try {
                final URL url = new URL(requestUrl);

                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection(httpURLConnectionProxy);
                urlConnection.setInstanceFollowRedirects(false); //important
                urlConnection.setDoInput(true);
                urlConnection.setDoOutput(true);
                urlConnection.setRequestMethod("POST");
                urlConnection.setUseCaches(false);
                urlConnection.setConnectTimeout(connectTimeout);
                urlConnection.setReadTimeout(readTimeout);
                urlConnection.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
                urlConnection.setRequestProperty("Content-Length", "" + requestBody.length());
                //urlConnection.setRequestProperty("Connection", "close"); //http://download.oracle.com/javase/1.5.0/docs/guide/net/http-keepalive.html
// DA           urlConnection.setRequestProperty("User-Agent", "JShare 1.0, www.independentsoft.com");
                urlConnection.setRequestProperty("User-Agent", userAgent); // DA

                if (acceptGzipEncoding) {
                    urlConnection.setRequestProperty("Accept-Encoding", "gzip");
                }

                urlConnection.connect();

                outputStream = urlConnection.getOutputStream();
                outputStream.write(requestBody.getBytes("UTF-8"));
                outputStream.flush();

                int responseCode = urlConnection.getResponseCode();

                //get cookies
                String headerName = null;
                urlConnectionCookies = new ArrayList<String>();

                for (int i = 1; (headerName = urlConnection.getHeaderFieldKey(i)) != null; i++) {
                    if (headerName.equals("Set-Cookie")) {
                        String cookie = urlConnection.getHeaderField(i);
                        urlConnectionCookies.add(cookie);
                    }
                }

                if (responseCode == HttpURLConnection.HTTP_INTERNAL_ERROR) {
                    inputStream = urlConnection.getErrorStream();

                    String contentEncoding = urlConnection.getContentEncoding();

                    if (contentEncoding != null && contentEncoding.equals("gzip")) {
                        inputStream = new GZIPInputStream(new BufferedInputStream(inputStream));
                    }

                    throw new ServiceException(inputStream, requestUrl, requestBody);
                }

                inputStream = urlConnection.getInputStream();

                if (inputStream != null) {
                    String contentEncoding = urlConnection.getContentEncoding();

                    if (contentEncoding != null && contentEncoding.equals("gzip")) {
                        inputStream = new GZIPInputStream(new BufferedInputStream(inputStream));
                    }

                    inputStream = getInputStream(inputStream);
                }
            } finally {
                if (outputStream != null) {
                    try {
                        outputStream.close();
                    } catch (IOException ex) {
                    }
                }
            }

            return inputStream;
        }
    }

    // DA
    private void createAndSetAuthSchemeRegistery(HttpClientBuilder builder) {
        Registry registery = RegistryBuilder.create()
                .register(AuthSchemes.NTLM, new NTLMSchemeFactory())
                .register(AuthSchemes.BASIC, new BasicSchemeFactory())
                .register(AuthSchemes.DIGEST, new DigestSchemeFactory())
                .register(AuthSchemes.SPNEGO, new SPNegoSchemeFactory())
                .register(AuthSchemes.KERBEROS, new KerberosSchemeFactory())
                .build();

        builder.setDefaultAuthSchemeRegistry(registery);
    }

    private void loginSharePointOnline() throws Exception {
        URL url = new URL(this.siteUrl);

        String hostName = url.getProtocol() + "://" + url.getHost();

        String requestBody = "<S:Envelope xmlns:S=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:wsa=\"http://www.w3.org/2005/08/addressing\" xmlns:wsp=\"http://schemas.xmlsoap.org/ws/2004/09/policy\" xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\" xmlns:wst=\"http://schemas.xmlsoap.org/ws/2005/02/trust\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\">" +
                "<S:Header>" +
                "<wsa:Action S:mustUnderstand=\"1\">http://schemas.xmlsoap.org/ws/2005/02/trust/RST/Issue</wsa:Action>" +
                "<wsa:To S:mustUnderstand=\"1\">https://login.microsoftonline.com/rst2.srf</wsa:To>" +
                "<wsse:Security>" +
                "<wsse:UsernameToken wsu:Id=\"user\">" +
                "<wsse:Username>" + this.username + "</wsse:Username>" +
                "<wsse:Password>" + this.password + "</wsse:Password>" +
                "</wsse:UsernameToken>" +
                "</wsse:Security>" +
                "</S:Header>" +
                "<S:Body>" +
                "<wst:RequestSecurityToken Id=\"RST0\">" +
                "<wst:RequestType>http://schemas.xmlsoap.org/ws/2005/02/trust/Issue</wst:RequestType>" +
                "<wsp:AppliesTo>" +
                "<wsa:EndpointReference>" +
                "<wsa:Address>" + hostName + "</wsa:Address>" +
                "</wsa:EndpointReference>" +
                "</wsp:AppliesTo>" +
                "<wsp:PolicyReference URI=\"MBI\" />" +
                "</wst:RequestSecurityToken>" +
                "</S:Body>" +
                "</S:Envelope>";

        String token = null;

        InputStream inputStream = sendLoginSharePointOnlineRequest("https://login.microsoftonline.com/rst2.srf", requestBody);

        //debug(inputStream);

        token = parseLoginSharePointOnlineResponse(inputStream);

        if (token != null) {
            String loginRequestUrl = hostName.endsWith("/") ? hostName + "_forms/default.aspx?wa=wsignin1.0" : hostName + "/" + "_forms/default.aspx?wa=wsignin1.0";

            inputStream = sendLoginSharePointOnlineRequest(loginRequestUrl, token);
        }
    }

//    private void loginSharePointOnline() throws Exception
//    {
//    	URL url = new URL(this.siteUrl);
//
//    	String hostName = url.getProtocol() + "://" + url.getHost();
//
//        String requestBody =  "<s:Envelope xmlns:s=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:a=\"http://www.w3.org/2005/08/addressing\" xmlns:u=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\">" +
//                "<s:Header>" +
//                "<a:Action s:mustUnderstand=\"1\">http://schemas.xmlsoap.org/ws/2005/02/trust/RST/Issue</a:Action>" +
//                "<a:ReplyTo>" +
//                "<a:Address>http://www.w3.org/2005/08/addressing/anonymous</a:Address>" +
//                "</a:ReplyTo>" +
//                "<a:To s:mustUnderstand=\"1\">https://login.microsoftonline.com/extSTS.srf</a:To>" +
//                "<o:Security s:mustUnderstand=\"1\" xmlns:o=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\">" +
//                "<o:UsernameToken>" +
//                "<o:Username>" + this.username + "</o:Username>" +
//                "<o:Password>" + this.password + "</o:Password>" +
//                "</o:UsernameToken>" +
//                "</o:Security>" +
//                "</s:Header>" +
//                "<s:Body>" +
//                "<t:RequestSecurityToken xmlns:t=\"http://schemas.xmlsoap.org/ws/2005/02/trust\">" +
//                "<wsp:AppliesTo xmlns:wsp=\"http://schemas.xmlsoap.org/ws/2004/09/policy\">" +
//                "<a:EndpointReference>" +
//                "<a:Address>" + hostName + "</a:Address>" +
//                "</a:EndpointReference>" +
//                "</wsp:AppliesTo>" +
//                "<t:KeyType>http://schemas.xmlsoap.org/ws/2005/05/identity/NoProofKey</t:KeyType>" +
//                "<t:RequestType>http://schemas.xmlsoap.org/ws/2005/02/trust/Issue</t:RequestType>" +
//                "<t:TokenType>urn:oasis:names:tc:SAML:1.0:assertion</t:TokenType>" +
//                "</t:RequestSecurityToken>" +
//                "</s:Body>" +
//                "</s:Envelope>";
//
//        String token = null;
//
//        InputStream inputStream = sendLoginSharePointOnlineRequest("https://login.microsoftonline.com/extSTS.srf", requestBody);
//
//        //debug(inputStream);
//
//        token = parseLoginSharePointOnlineResponse(inputStream);
//
//        if (token != null)
//        {
//            String loginRequestUrl = hostName.endsWith("/") ? hostName + "_forms/default.aspx?wa=wsignin1.0" : hostName + "/" + "_forms/default.aspx?wa=wsignin1.0";
//
//            inputStream = sendLoginSharePointOnlineRequest(loginRequestUrl, token);
//        }
//    }

    private void setSharePointVersionInfo() throws MalformedURLException {
        if (this.username != null && this.username.endsWith(".onmicrosoft.com")) {
            serverVersionInfo = ServerVersionInfo.SHARE_POINT_ONLINE;
            sharePointOnlineRegisteredApp = false;
        } else if (this.siteUrl != null && this.siteUrl.length() > 0) {
            URL uri = new URL(this.siteUrl);

            if (uri.getHost() != null && (uri.getHost().toLowerCase().endsWith(".sharepoint.com") || uri.getHost().toLowerCase().endsWith(".sharepointonline.com") || uri.getHost().toLowerCase().endsWith(".office365.com"))) {
                serverVersionInfo = ServerVersionInfo.SHARE_POINT_ONLINE;
                sharePointOnlineRegisteredApp = true;
            } else {
                serverVersionInfo = ServerVersionInfo.SHARE_POINT_ON_PREMISE;
            }
        } else {
            serverVersionInfo = ServerVersionInfo.SHARE_POINT_ON_PREMISE;
        }
        logger.trace("setSharePointVersionInfo: serverVersionInfo={} sharePointOnlineRegisteredApp={}", serverVersionInfo, sharePointOnlineRegisteredApp);
    }

    /**
     * Gets the context info.
     *
     * @return the context info
     * @throws ServiceException the service exception
     */
    public ContextInfo getContextInfo(String subsite) throws ServiceException {
        return getContextInfoImplementation(subsite);
    }

    private ContextInfo getContextInfoImplementation(String subsite) throws ServiceException {
        InputStream inputStream = null;


        String requestUrl = "_api/contextinfo";

        try {
//DA            inputStream = sendRequest("POST", requestUrl, null, null, null, null, true, true);
            inputStream = sendRequest("POST", subsite, requestUrl, null, null, null, null, true, true);

            //debug(inputStream);
            return new ContextInfo(inputStream);
        } catch (ServiceException e) {
            logger.error("FAIL requestUrl {}", requestUrl, e);
            throw e;
        } catch (Exception e) {
            logger.error("FAIL requestUrl {}", requestUrl, e);
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }
    }

    /**
     * Gets the folders.
     *
     * @param parentFolder the parent folder
     * @return the folders
     * @throws ServiceException the service exception
     */
// DA    public List<Folder> getFolders(String parentFolder) throws ServiceException {
    public List<Folder> getFolders(String subSite, String parentFolder) throws ServiceException {// DA
        return getFolders(subSite, parentFolder, null);
    }

    /**
     * Gets the folders.
     *
     * @param parentFolder the parent folder
     * @param queryOptions the query options
     * @return the folders
     * @throws ServiceException the service exception
     */
//DA     public List<Folder> getFolders(String parentFolder, List<IQueryOption> queryOptions) throws ServiceException {
    public List<Folder> getFolders(String subSite, String parentFolder, List<IQueryOption> queryOptions) throws ServiceException {//DA
        return getFoldersImplementation(subSite, parentFolder, queryOptions);
    }

    //DA    private List<Folder> getFoldersImplementation(String parentFolder, List<IQueryOption> queryOptions) throws ServiceException {
    private List<Folder> getFoldersImplementation(String subSite, String parentFolder, List<IQueryOption> queryOptions) throws ServiceException {//DA
        if (parentFolder == null) {
            throw new IllegalArgumentException("parentFolder");
        }

        InputStream inputStream = null;
        List<Folder> folders = new ArrayList<Folder>();

        String queryOptionsString = Util.queryOptionsToString(queryOptions);

// DA        String requestUrl = "_api/web/GetFolderByServerRelativeUrl('" + Util.encodeUrl(parentFolder) + "')/folders" + queryOptionsString;
        String requestUrl = getFolderApiCall + Util.encodeUrl(parentFolder) + "')/folders" + queryOptionsString; // DA

        try {
            inputStream = sendRequest("GET", subSite, requestUrl);

            //debug(inputStream);

            folders = parseGetFolders(inputStream);
            if (logger.isTraceEnabled()) {
                logger.trace("Found {} folders. subsite={} url={}", (folders==null?"null":folders.size()), subSite, requestUrl);
            }
        } catch (ServiceException e) {
            logger.debug("Failed execution of GET subsite={} url={} serverVersionInfo={} sharePointOnlineRegisteredApp={}", subSite, requestUrl, serverVersionInfo, sharePointOnlineRegisteredApp);
            throw e;
        } catch (Exception e) {
            logger.debug("Failed execution of GET subsite={} url={} serverVersionInfo={} sharePointOnlineRegisteredApp={}", subSite, requestUrl, serverVersionInfo, sharePointOnlineRegisteredApp);
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return folders;
    }

    /**
     * Gets the files.
     *
     * @param folderPath the folder path
     * @return the files
     * @throws ServiceException the service exception
     */
//DA    public List<File> getFiles(String folderPath) throws ServiceException {
    public List<File> getFiles(String subSite, String folderPath) throws ServiceException { //DA
        return getFiles(subSite, folderPath, null);
    }

    /**
     * Gets the files.
     *
     * @param folderPath   the folder path
     * @param queryOptions the query options
     * @return the files
     * @throws ServiceException the service exception
     */
//DA    public List<File> getFiles(String folderPath, List<IQueryOption> queryOptions) throws ServiceException {
    public List<File> getFiles(String subSite, String folderPath, List<IQueryOption> queryOptions) throws ServiceException {//DA
        return getFilesImplementation(subSite, folderPath, queryOptions);
    }

    //DA    private List<File> getFilesImplementation(String folderPath, List<IQueryOption> queryOptions) throws ServiceException {
    private List<File> getFilesImplementation(String subSite, String folderPath, List<IQueryOption> queryOptions) throws ServiceException {//DA
        if (folderPath == null) {
            throw new IllegalArgumentException("folderPath");
        }

        InputStream inputStream = null;
        List<File> files = new ArrayList<File>();

        String queryOptionsString = Util.queryOptionsToString(queryOptions);

// DA        String requestUrl = "_api/web/GetFolderByServerRelativeUrl('" + Util.encodeUrl(folderPath) + "')/files" + queryOptionsString;
        String requestUrl = getFolderApiCall + Util.encodeUrl(folderPath) + "')/files" + queryOptionsString; // DA

        try {
            inputStream = sendRequest("GET", subSite, requestUrl);

            //debug(inputStream);

            files = parseGetFiles(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return files;
    }

    /**
     * Delete folder.
     *
     * @param folderPath the folder path
     * @throws ServiceException the service exception
     */
    public void deleteFolder(String folderPath) throws ServiceException {
        deleteFolderImplementation(folderPath);
    }

    private void deleteFolderImplementation(String folderPath) throws ServiceException {
        if (folderPath == null) {
            throw new IllegalArgumentException("folderPath");
        }

        InputStream inputStream = null;

// DA        String requestUrl = "_api/web/GetFolderByServerRelativeUrl('" + Util.encodeUrl(folderPath) + "')";
        String requestUrl = getFolderApiCall + Util.encodeUrl(folderPath) + "')"; // DA

        try {
            inputStream = sendRequest("POST", null, requestUrl, null, "DELETE");

            //debug(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }
    }

    /**
     * Creates the folder.
     *
     * @param folderPath the folder path
     * @return the folder
     * @throws ServiceException the service exception
     */
    public Folder createFolder(String folderPath) throws ServiceException {
        return createFolderImplementation(folderPath);
    }

    private Folder createFolderImplementation(String folderPath) throws ServiceException {
        if (folderPath == null) {
            throw new IllegalArgumentException("folderPath");
        }

        Folder folder = null;
        InputStream inputStream = null;

        String requestUrl = "_api/web/folders/add('" + Util.encodeUrl(folderPath) + "')";

        try {
            inputStream = sendRequest("POST", requestUrl);

            //debug(inputStream);

            folder = new Folder(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return folder;
    }

    /**
     * Gets the list.
     *
     * @param listId the list id
     * @return the list
     * @throws ServiceException the service exception
     */

//DA    public com.independentsoft.share.List getList(String listId) throws ServiceException {
    public com.independentsoft.share.List getList(String subSite, String listId) throws ServiceException {//DA
        return getListImplementation(subSite, listId);
    }

    //DA private com.independentsoft.share.List getListImplementation(String listId) throws ServiceException {
    private com.independentsoft.share.List getListImplementation(String subSite, String listId) throws ServiceException {//DA
        if (listId == null) {
            throw new IllegalArgumentException("listId");
        }

        InputStream inputStream = null;
        com.independentsoft.share.List list = null;

        String requestUrl = "_api/web/lists('" + listId + "')";

        try {
            inputStream = sendRequest("GET", subSite, requestUrl);

            //debug(inputStream);

            list = new com.independentsoft.share.List(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return list;
    }

    /**
     * Gets the list by title.
     *
     * @param title the title
     * @return the list by title
     * @throws ServiceException the service exception
     */
//DA    public com.independentsoft.share.List getListByTitle(String title) throws ServiceException {
    public com.independentsoft.share.List getListByTitle(String subSite, String title) throws ServiceException {//DA
        return getListByTitleImplementation(subSite, title);
    }

    //DA    private com.independentsoft.share.List getListByTitleImplementation(String title) throws ServiceException {
    private com.independentsoft.share.List getListByTitleImplementation(String subSite, String title) throws ServiceException {//DA
        if (title == null) {
            throw new IllegalArgumentException("title");
        }

        InputStream inputStream = null;
        com.independentsoft.share.List list = null;

        String requestUrl = "_api/web/lists/GetByTitle('" + Util.encodeUrl(title) + "')";

        try {
            inputStream = sendRequest("GET", subSite, requestUrl);

            //debug(inputStream);

            list = new com.independentsoft.share.List(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return list;
    }

    /**
     * Gets the list items.
     *
     * @param listId the list id
     * @return the list items
     * @throws ServiceException the service exception
     */
//DA    public List<ListItem> getListItems(String String listId) throws ServiceException {//DA
    public List<ListItem> getListItems(String subSite, String listId) throws ServiceException {//DA
        return getListItems(subSite, listId, null);
    }

    /**
     * Gets the list items.
     *
     * @param listId       the list id
     * @param queryOptions the query options
     * @return the list items
     * @throws ServiceException the service exception
     */
//DA     public List<ListItem> getListItems(String listId, List<IQueryOption> queryOptions) throws ServiceException {
    public List<ListItem> getListItems(String subSite, String listId, List<IQueryOption> queryOptions) throws ServiceException {//DA
        return getListItemsImplementation(subSite, listId, queryOptions);
    }

    //DA    private List<ListItem> getListItemsImplementation(String subSite, String listId, List<IQueryOption> queryOptions) throws ServiceException {
    private List<ListItem> getListItemsImplementation(String subSite, String listId, List<IQueryOption> queryOptions) throws ServiceException {//DA
        if (listId == null) {
            throw new IllegalArgumentException("listId");
        }

        InputStream inputStream = null;
        List<ListItem> items = new ArrayList<ListItem>();

        String queryOptionsString = Util.queryOptionsToString(queryOptions);

        String requestUrl = "_api/web/lists('" + listId + "')/items" + queryOptionsString;

        try {
            inputStream = sendRequest("GET", subSite, requestUrl);

            //debug(inputStream);

            items = parseGetListItems(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return items;
    }

    /**
     * Suggest.
     *
     * @param query the query
     * @return the suggest result
     * @throws ServiceException the service exception
     */
    public SuggestResult suggest(String query) throws ServiceException {
        return suggest(new SearchQuerySuggestion(query));
    }

    /**
     * Suggest.
     *
     * @param suggestion the suggestion
     * @return the suggest result
     * @throws ServiceException the service exception
     */
    public SuggestResult suggest(SearchQuerySuggestion suggestion) throws ServiceException {
        return suggestImplementation(suggestion);
    }

    private SuggestResult suggestImplementation(SearchQuerySuggestion suggestion) throws ServiceException {
        if (suggestion == null) {
            throw new IllegalArgumentException("suggestion");
        }

        SuggestResult result = null;
        InputStream inputStream = null;

        String requestUrl = "/_api/search/suggest?" + suggestion.toString();

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream);

            result = new SuggestResult(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return result;
    }

    /**
     * Search.
     *
     * @param restriction the restriction
     * @return the search result
     * @throws ServiceException the service exception
     */
    public SearchResult search(IRestriction restriction) throws ServiceException {
        return search(new SearchQuery(restriction));
    }

    /**
     * Search.
     *
     * @param restriction the restriction
     * @return the search result
     * @throws ServiceException the service exception
     */
    public SearchResult search(com.independentsoft.share.kql.IRestriction restriction) throws ServiceException {
        return search(new SearchQuery(restriction));
    }

    /**
     * Search.
     *
     * @param query the query
     * @return the search result
     * @throws ServiceException the service exception
     */
    public SearchResult search(String query) throws ServiceException {
        return search(new SearchQuery(query));
    }

    /**
     * Search.
     *
     * @param query the query
     * @return the search result
     * @throws ServiceException the service exception
     */
    public SearchResult search(SearchQuery query) throws ServiceException {
        return searchImplementation(query);
    }

    private SearchResult searchImplementation(SearchQuery query) throws ServiceException {
        if (query == null) {
            throw new IllegalArgumentException("query");
        }

        SearchResult result = null;
        InputStream inputStream = null;

        String requestUrl = "/_api/search/postquery";

        String requestBody = query.toString();

        try {
            inputStream = sendRequest("POST", null, requestUrl, requestBody);

            //debug(inputStream);

            result = new SearchResult(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl, requestBody);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl, requestBody);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestBody);
                }
            }
        }

        return result;
    }

    /**
     * Gets the list item.
     *
     * @param listId the list id
     * @param itemId the item id
     * @return the list item
     * @throws ServiceException the service exception
     */
    public ListItem getListItem(String listId, int itemId) throws ServiceException {
        return getListItemImplementation(listId, itemId);
    }

    private ListItem getListItemImplementation(String listId, int itemId) throws ServiceException {
        if (listId == null) {
            throw new IllegalArgumentException("listId");
        }

        if (itemId < 0) {
            throw new IllegalArgumentException("The parameter itemId must be non-negative.");
        }

        ListItem item = null;
        InputStream inputStream = null;

        String requestUrl = "_api/web/lists('" + listId + "')/items(" + itemId + ")";

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream);

            item = new ListItem(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return item;
    }

    /**
     * Gets the list item property as xml string.
     *
     * @param listId       the list id
     * @param itemId       the item id
     * @param propertyName the property name
     * @return the list item
     * @throws ServiceException the service exception
     */
//DA    public String getListItemProperty(String listId, int itemId, String propertyName) throws ServiceException {
    public String getListItemProperty(String subSite, String listId, int itemId, String propertyName) throws ServiceException {//DA
        return getListItemPropertyImplementation(subSite, listId, itemId, propertyName);
    }

    //DA    private String getListItemPropertyImplementation(String listId, int itemId, String propertyName) throws ServiceException {
    private String getListItemPropertyImplementation(String subSite, String listId, int itemId, String propertyName) throws ServiceException {//DA
        if (listId == null) {
            throw new IllegalArgumentException("listId");
        }

        if (propertyName == null) {
            throw new IllegalArgumentException("propertyName");
        }

        if (itemId < 0) {
            throw new IllegalArgumentException("The parameter itemId must be non-negative.");
        }

        String responseText = null;
        InputStream inputStream = null;

        String requestUrl = "_api/web/lists('" + listId + "')/items(" + itemId + ")/" + Util.encodeUrl(propertyName);

        try {
            inputStream = sendRequest("GET", subSite, requestUrl);

            //debug(inputStream);

            responseText = convertInputStreamToString(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return responseText;
    }

    /**
     * Gets the field values.
     *
     * @param listId the list id
     * @param itemId the item id
     * @return the field values
     * @throws ServiceException the service exception
     */
    public List<FieldValue> getFieldValuesAsHtml(String listId, int itemId) throws ServiceException {
        return getFieldValuesAsHtml(listId, itemId, null);
    }

    /**
     * Gets the field values.
     *
     * @param listId       the list id
     * @param itemId       the item id
     * @param queryOptions the query options
     * @return the field values
     * @throws ServiceException the service exception
     */
    public List<FieldValue> getFieldValuesAsHtml(String listId, int itemId, List<IQueryOption> queryOptions) throws ServiceException {
        return getFieldValuesAsHtmlImplementation(listId, itemId, queryOptions);
    }

    private List<FieldValue> getFieldValuesAsHtmlImplementation(String listId, int itemId, List<IQueryOption> queryOptions) throws ServiceException {
        if (listId == null) {
            throw new IllegalArgumentException("listId");
        }

        if (itemId < 0) {
            throw new IllegalArgumentException("The parameter itemId must be non-negative.");
        }

        InputStream inputStream = null;
        List<FieldValue> values = new ArrayList<FieldValue>();

        String queryOptionsString = Util.queryOptionsToString(queryOptions);

        String requestUrl = "_api/web/lists('" + listId + "')/items(" + itemId + ")/FieldValuesAsHTML" + queryOptionsString;

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream);

            values = parseGetFieldValues(inputStream);

        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return values;
    }

    /**
     * Gets the field values.
     *
     * @param listId the list id
     * @param itemId the item id
     * @return the field values
     * @throws ServiceException the service exception
     */
//DA    public List<FieldValue> getFieldValues(String listId, int itemId) throws ServiceException {
    public List<FieldValue> getFieldValues(String subSite, String listId, int itemId) throws ServiceException {//DA
        return getFieldValues(subSite, listId, itemId, null);
    }

    /**
     * Gets the field values.
     *
     * @param listId       the list id
     * @param itemId       the item id
     * @param queryOptions the query options
     * @return the field values
     * @throws ServiceException the service exception
     */
//DA    public List<FieldValue> getFieldValues(String listId, int itemId, List<IQueryOption> queryOptions) throws ServiceException {
    public List<FieldValue> getFieldValues(String subSite, String listId, int itemId, List<IQueryOption> queryOptions) throws ServiceException {//DA
        return getFieldValuesImplementation(subSite, listId, itemId, queryOptions);
    }

    //DA private List<FieldValue> getFieldValuesImplementation(String listId, int itemId, List<IQueryOption> queryOptions) throws ServiceException {
    private List<FieldValue> getFieldValuesImplementation(String subSite, String listId, int itemId, List<IQueryOption> queryOptions) throws ServiceException {//DA
        if (listId == null) {
            throw new IllegalArgumentException("listId");
        }

        if (itemId < 0) {
            throw new IllegalArgumentException("The parameter itemId must be non-negative.");
        }

        InputStream inputStream = null;
        List<FieldValue> values = new ArrayList<FieldValue>();

        String queryOptionsString = Util.queryOptionsToString(queryOptions);

        String requestUrl = "_api/web/lists('" + listId + "')/items(" + itemId + ")/FieldValuesAsText" + queryOptionsString;

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream);

            values = parseGetFieldValues(inputStream);

        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return values;
    }

    /**
     * Gets the list item attachments.
     *
     * @param listId the list id
     * @param itemId the item id
     * @return the list item attachments
     * @throws ServiceException the service exception
     */
    public List<Attachment> getListItemAttachments(String listId, int itemId) throws ServiceException {
        return getListItemAttachments(listId, itemId, null);
    }

    /**
     * Gets the list item attachments.
     *
     * @param listId       the list id
     * @param itemId       the item id
     * @param queryOptions the query options
     * @return the list item attachments
     * @throws ServiceException the service exception
     */
    public List<Attachment> getListItemAttachments(String listId, int itemId, List<IQueryOption> queryOptions) throws ServiceException {
        return getListItemAttachmentsImplementation(listId, itemId, queryOptions);
    }

    private List<Attachment> getListItemAttachmentsImplementation(String listId, int itemId, List<IQueryOption> queryOptions) throws ServiceException {
        if (listId == null) {
            throw new IllegalArgumentException("listId");
        }

        if (itemId < 0) {
            throw new IllegalArgumentException("The parameter itemId must be non-negative.");
        }

        InputStream inputStream = null;
        List<Attachment> attachments = new ArrayList<Attachment>();

        String queryOptionsString = Util.queryOptionsToString(queryOptions);

        String requestUrl = "_api/web/lists('" + listId + "')/items(" + itemId + ")/AttachmentFiles" + queryOptionsString;

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream);

            attachments = parseGetAttachments(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return attachments;
    }

    /***
     * Creates the list item attachment.
     *
     * @param listId
     * @param itemId
     * @param fileName
     * @param buffer
     * @return
     * @throws ServiceException
     */
//DA    public Attachment createListItemAttachment(String listId, int itemId, String fileName, byte[] buffer) throws ServiceException {
    public Attachment createListItemAttachment(String subSite, String listId, int itemId, String fileName, byte[] buffer) throws ServiceException {//DA
        ByteArrayInputStream stream = new ByteArrayInputStream(buffer);

        return createListItemAttachment(subSite, listId, itemId, fileName, stream);
    }

    /***
     * Creates the list item attachment.
     *
     * @param listId
     * @param itemId
     * @param fileName
     * @param stream
     * @return
     * @throws ServiceException
     */
//DA    public Attachment createListItemAttachment(String listId, int itemId, String fileName, InputStream stream) throws ServiceException {
    public Attachment createListItemAttachment(String subSite, String listId, int itemId, String fileName, InputStream stream) throws ServiceException {//DA
        return createListItemAttachmentImplementation(subSite, listId, itemId, fileName, stream);
    }

    //DA    private Attachment createListItemAttachmentImplementation(String listId, int itemId, String fileName, InputStream stream) throws ServiceException {
    private Attachment createListItemAttachmentImplementation(String subSite, String listId, int itemId, String fileName, InputStream stream) throws ServiceException {//DA
        if (listId == null) {
            throw new IllegalArgumentException("listId");
        }

        if (itemId < 0) {
            throw new IllegalArgumentException("The parameter itemId must be non-negative.");
        }

        if (fileName == null) {
            throw new IllegalArgumentException("filePath");
        }

        if (stream == null) {
            throw new IllegalArgumentException("stream");
        }

        Attachment attachment = null;
        InputStream inputStream = null;

        String requestUrl = "_api/web/lists('" + listId + "')/items(" + itemId + ")/AttachmentFiles/Add(filename='" + Util.encodeUrl(fileName) + "')";

        try {
// DA inputStream = sendRequest("POST", requestUrl, null, null, null, stream, true, false);
            inputStream = sendRequest("POST", subSite, requestUrl, null, null, null, stream, true, false);

            //debug(inputStream);

            attachment = new Attachment(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return attachment;
    }

    /***
     * Updates the list item attachment.
     *
     * @param listId
     * @param itemId
     * @param fileName
     * @param buffer
     * @throws ServiceException
     */
    public void updateListItemAttachment(String listId, int itemId, String fileName, byte[] buffer) throws ServiceException {
        ByteArrayInputStream stream = new ByteArrayInputStream(buffer);

        updateListItemAttachment(listId, itemId, fileName, stream);
    }

    /***
     * Updates the list item attachment.
     *
     * @param listId
     * @param itemId
     * @param fileName
     * @param stream
     * @throws ServiceException
     */
    public void updateListItemAttachment(String listId, int itemId, String fileName, InputStream stream) throws ServiceException {
        updateListItemAttachmentImplementation(listId, itemId, fileName, stream);
    }

    private void updateListItemAttachmentImplementation(String listId, int itemId, String fileName, InputStream stream) throws ServiceException {
        if (listId == null) {
            throw new IllegalArgumentException("listId");
        }

        if (itemId < 0) {
            throw new IllegalArgumentException("The parameter itemId must be non-negative.");
        }

        if (fileName == null) {
            throw new IllegalArgumentException("filePath");
        }

        if (stream == null) {
            throw new IllegalArgumentException("stream");
        }

        InputStream inputStream = null;

        String requestUrl = "_api/web/lists('" + listId + "')/items(" + itemId + ")/AttachmentFiles(filename='" + Util.encodeUrl(fileName) + "')/$value";

        try {
//DA            inputStream = sendRequest("POST", requestUrl, null, "PUT", null, stream, true, false);
            inputStream = sendRequest("POST", null, requestUrl, null, "PUT", null, stream, true, false);

            //debug(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }
    }

    /**
     * Gets the list item content type.
     *
     * @param listId the list id
     * @param itemId the item id
     * @return the list item content type
     * @throws ServiceException the service exception
     */
    public ContentType getListItemContentType(String listId, int itemId) throws ServiceException {
        return getListItemContentTypeImplementation(listId, itemId);
    }

    private ContentType getListItemContentTypeImplementation(String listId, int itemId) throws ServiceException {
        if (listId == null) {
            throw new IllegalArgumentException("listId");
        }

        if (itemId < 0) {
            throw new IllegalArgumentException("The parameter itemId must be non-negative.");
        }

        InputStream inputStream = null;
        ContentType contentType = null;

        String requestUrl = "_api/web/lists('" + listId + "')/items(" + itemId + ")/ContentType";

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream);

            contentType = new ContentType(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return contentType;
    }

    /**
     * Gets the user effective permissions.
     *
     * @param listId    the list id
     * @param loginName the login name
     * @return the user effective permissions
     * @throws ServiceException the service exception
     */
    public List<BasePermission> getUserEffectivePermissions(String listId, String loginName) throws ServiceException {
        return getUserEffectivePermissionsImplementation(listId, loginName);
    }

    private List<BasePermission> getUserEffectivePermissionsImplementation(String listId, String loginName) throws ServiceException {
        if (listId == null) {
            throw new IllegalArgumentException("listId");
        }

        if (loginName == null) {
            throw new IllegalArgumentException("loginName");
        }

        InputStream inputStream = null;
        List<BasePermission> basePermissions = new ArrayList<BasePermission>();

        String requestUrl = "_api/web/lists('" + listId + "')/getusereffectivepermissions(@v)?@v='" + Util.encodeUrl(loginName) + "'";

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream);

            basePermissions = Util.parseBasePermissions(inputStream, "GetUserEffectivePermissions");
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return basePermissions;
    }

    /**
     * Gets the user effective permissions.
     *
     * @param listId    the list id
     * @param itemId    the item id
     * @param loginName the login name
     * @return the user effective permissions
     * @throws ServiceException the service exception
     */
    public List<BasePermission> getUserEffectivePermissions(String listId, int itemId, String loginName) throws ServiceException {
        return getUserEffectivePermissionsImplementation(listId, itemId, loginName);
    }

    private List<BasePermission> getUserEffectivePermissionsImplementation(String listId, int itemId, String loginName) throws ServiceException {
        if (listId == null) {
            throw new IllegalArgumentException("listId");
        }

        if (itemId < 0) {
            throw new IllegalArgumentException("The parameter itemId must be non-negative.");
        }

        if (loginName == null) {
            throw new IllegalArgumentException("loginName");
        }

        InputStream inputStream = null;
        List<BasePermission> basePermissions = new ArrayList<BasePermission>();

        String requestUrl = "_api/web/lists('" + listId + "')/items(" + itemId + ")/getusereffectivepermissions(@v)?@v='" + Util.encodeUrl(loginName) + "'";

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream);

            basePermissions = Util.parseBasePermissions(inputStream, "GetUserEffectivePermissions");
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return basePermissions;
    }

    /**
     * Delete list item.
     *
     * @param listId the list id
     * @param itemId the item id
     * @throws ServiceException the service exception
     */
    public void deleteListItem(String listId, int itemId) throws ServiceException {
        deleteListItemImplementation(listId, itemId);
    }

    private void deleteListItemImplementation(String listId, int itemId) throws ServiceException {
        if (listId == null) {
            throw new IllegalArgumentException("listId");
        }

        if (itemId < 0) {
            throw new IllegalArgumentException("The parameter itemId must be non-negative.");
        }

        InputStream inputStream = null;

        String requestUrl = "_api/web/lists('" + listId + "')/items(" + itemId + ")";

        try {
            inputStream = sendRequest("POST", null, requestUrl, null, "DELETE", "*");

            //debug(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }
    }

    /**
     * Gets the site.
     *
     * @return the site
     * @throws ServiceException the service exception
     */
//DA    public Site getSite(String subSite) throws ServiceException {
    public Site getSite(String subSite) throws ServiceException {//DA
        return getSiteImplementation(subSite);
    }

    //DA    private Site getSiteImplementation() throws ServiceException {
    private Site getSiteImplementation(String subSite) throws ServiceException {//DA
        Site site = null;
        InputStream inputStream = null;

        String requestUrl = "_api/web";

        try {
            inputStream = sendRequest("GET", subSite, requestUrl);

            //debug(inputStream);

            site = new Site(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return site;
    }

    /**
     * Gets the sites.
     *
     * @return the sites
     * @throws ServiceException the service exception
     */
    public List<Site> getSites() throws ServiceException {
        return getSites(null);
    }

    /**
     * Gets the sites.
     *
     * @param queryOptions the query options
     * @return the sites
     * @throws ServiceException the service exception
     */
    public List<Site> getSites(List<IQueryOption> queryOptions) throws ServiceException {
        return getSitesImplementation(queryOptions);
    }

    private List<Site> getSitesImplementation(List<IQueryOption> queryOptions) throws ServiceException {
        List<Site> sites = new ArrayList<Site>();

        String queryOptionsString = Util.queryOptionsToString(queryOptions);

        String requestUrl = "_api/web/Webs" + queryOptionsString;

        InputStream inputStream = null;

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream);

            sites = parseGetSites(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return sites;
    }

    /**
     * Gets the site infos.
     *
     * @return the site infos
     * @throws ServiceException the service exception
     */
    public List<SiteInfo> getSiteInfos() throws ServiceException {
        return getSiteInfos(null);
    }

    /**
     * Gets the site infos.
     *
     * @param queryOptions the query options
     * @return the site infos
     * @throws ServiceException the service exception
     */
    public List<SiteInfo> getSiteInfos(List<IQueryOption> queryOptions) throws ServiceException {
        return getSiteInfosImplementation(queryOptions);
    }

    private List<SiteInfo> getSiteInfosImplementation(List<IQueryOption> queryOptions) throws ServiceException {
        List<SiteInfo> sites = new ArrayList<SiteInfo>();

        String queryOptionsString = Util.queryOptionsToString(queryOptions);

        String requestUrl = "_api/web/WebInfos" + queryOptionsString;

        InputStream inputStream = null;

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream);

            sites = parseGetSiteInfos(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return sites;
    }


    /**
     * Apply theme.
     *
     * @param colorPaletteUrl the color palette url
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean applyTheme(String colorPaletteUrl) throws ServiceException {
        return applyTheme(colorPaletteUrl, null);
    }

    /**
     * Apply theme.
     *
     * @param colorPaletteUrl the color palette url
     * @param shareGenerated  the share generated
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean applyTheme(String colorPaletteUrl, boolean shareGenerated) throws ServiceException {
        return applyTheme(colorPaletteUrl, null, shareGenerated);
    }

    /**
     * Apply theme.
     *
     * @param colorPaletteUrl the color palette url
     * @param fontSchemeUrl   the font scheme url
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean applyTheme(String colorPaletteUrl, String fontSchemeUrl) throws ServiceException {
        return applyTheme(colorPaletteUrl, fontSchemeUrl, null);
    }

    /**
     * Apply theme.
     *
     * @param colorPaletteUrl the color palette url
     * @param fontSchemeUrl   the font scheme url
     * @param shareGenerated  the share generated
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean applyTheme(String colorPaletteUrl, String fontSchemeUrl, boolean shareGenerated) throws ServiceException {
        return applyTheme(colorPaletteUrl, fontSchemeUrl, null, shareGenerated);
    }

    /**
     * Apply theme.
     *
     * @param colorPaletteUrl    the color palette url
     * @param fontSchemeUrl      the font scheme url
     * @param backgroundImageUrl the background image url
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean applyTheme(String colorPaletteUrl, String fontSchemeUrl, String backgroundImageUrl) throws ServiceException {
        return applyTheme(colorPaletteUrl, fontSchemeUrl, backgroundImageUrl, false);
    }

    /**
     * Apply theme.
     *
     * @param colorPaletteUrl    the color palette url
     * @param fontSchemeUrl      the font scheme url
     * @param backgroundImageUrl the background image url
     * @param shareGenerated     the share generated
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean applyTheme(String colorPaletteUrl, String fontSchemeUrl, String backgroundImageUrl, boolean shareGenerated) throws ServiceException {
        return applyThemeImplementation(colorPaletteUrl, fontSchemeUrl, backgroundImageUrl, shareGenerated);
    }

    private boolean applyThemeImplementation(String colorPaletteUrl, String fontSchemeUrl, String backgroundImageUrl, boolean shareGenerated) throws ServiceException {
        if (colorPaletteUrl == null) {
            throw new IllegalArgumentException("colorPaletteUrl");
        }

        boolean success = false;
        InputStream inputStream = null;

        String fontSchemeUrlParameter = "";
        String backgroundImageUrlParameter = "";

        if (fontSchemeUrl != null && fontSchemeUrl.length() > 0) {
            fontSchemeUrlParameter = ", fontschemeurl='" + fontSchemeUrl + "'";
        }

        if (backgroundImageUrl != null && backgroundImageUrl.length() > 0) {
            backgroundImageUrlParameter = ", backgroundimageurl='" + backgroundImageUrl + "'";
        }

        String requestUrl = "_api/web/ApplyTheme(colorpaletteurl='" + colorPaletteUrl + "'" + fontSchemeUrlParameter + backgroundImageUrlParameter + ",sharegenerated=" + Boolean.toString(shareGenerated).toLowerCase() + ")";

        requestUrl = Util.encodeUrl(requestUrl);

        try {
            inputStream = sendRequest("POST", requestUrl);

            //debug(inputStream);

            success = parseBooleanResponse(inputStream, "ApplyTheme");
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return success;
    }

    /**
     * Apply site template.
     *
     * @param name the name
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean applySiteTemplate(String name) throws ServiceException {
        return applySiteTemplateImplementation(name);
    }

    private boolean applySiteTemplateImplementation(String name) throws ServiceException {
        if (name == null) {
            throw new IllegalArgumentException("name");
        }

        boolean success = false;
        InputStream inputStream = null;

        String requestUrl = "_api/web/ApplyWebTemplate(" + Util.encodeUrl(name) + ")";

        try {
            inputStream = sendRequest("POST", requestUrl);

            //debug(inputStream);

            success = parseBooleanResponse(inputStream, "ApplyWebTemplate");
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return success;
    }

    /**
     * Break role inheritance.
     *
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean breakRoleInheritance() throws ServiceException {
        return breakRoleInheritance(false);
    }

    /**
     * Break role inheritance.
     *
     * @param copyRoleAssignments the copy role assignments
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean breakRoleInheritance(boolean copyRoleAssignments) throws ServiceException {
        return breakRoleInheritance(copyRoleAssignments, false);
    }

    /**
     * Break role inheritance.
     *
     * @param copyRoleAssignments the copy role assignments
     * @param clearSubscopes      the clear subscopes
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean breakRoleInheritance(boolean copyRoleAssignments, boolean clearSubscopes) throws ServiceException {
        return breakRoleInheritanceImplementation(copyRoleAssignments, clearSubscopes);
    }

    private boolean breakRoleInheritanceImplementation(boolean copyRoleAssignments, boolean clearSubscopes) throws ServiceException {
        boolean success = false;
        InputStream inputStream = null;

        String requestUrl = "_api/web/BreakRoleInheritance(copyroleassignments=" + Boolean.toString(copyRoleAssignments).toLowerCase() + ", clearsubscopes=" + Boolean.toString(clearSubscopes).toLowerCase() + ")";

        try {
            inputStream = sendRequest("POST", requestUrl);

            //debug(inputStream);

            success = parseBooleanResponse(inputStream, "BreakRoleInheritance");
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return success;
    }

    /**
     * Break role inheritance.
     *
     * @param listId the list id
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean breakRoleInheritance(String listId) throws ServiceException {
        return breakRoleInheritance(listId, false);
    }

    /**
     * Break role inheritance.
     *
     * @param listId              the list id
     * @param copyRoleAssignments the copy role assignments
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean breakRoleInheritance(String listId, boolean copyRoleAssignments) throws ServiceException {
        return breakRoleInheritance(listId, copyRoleAssignments, false);
    }

    /**
     * Break role inheritance.
     *
     * @param listId              the list id
     * @param copyRoleAssignments the copy role assignments
     * @param clearSubscopes      the clear subscopes
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean breakRoleInheritance(String listId, boolean copyRoleAssignments, boolean clearSubscopes) throws ServiceException {
        return breakRoleInheritanceImplementation(listId, copyRoleAssignments, clearSubscopes);
    }

    private boolean breakRoleInheritanceImplementation(String listId, boolean copyRoleAssignments, boolean clearSubscopes) throws ServiceException {
        boolean success = false;
        InputStream inputStream = null;

        String requestUrl = "_api/web/lists('" + listId + "')/BreakRoleInheritance(copyroleassignments=" + Boolean.toString(copyRoleAssignments).toLowerCase() + ", clearsubscopes=" + Boolean.toString(clearSubscopes).toLowerCase() + ")";

        try {
            inputStream = sendRequest("POST", requestUrl);

            //debug(inputStream);

            success = parseBooleanResponse(inputStream, "BreakRoleInheritance");
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return success;
    }

    /**
     * Break role inheritance.
     *
     * @param listId the list id
     * @param itemId the item id
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean breakRoleInheritance(String listId, int itemId) throws ServiceException {
        return breakRoleInheritance(listId, itemId, false);
    }

    /**
     * Break role inheritance.
     *
     * @param listId              the list id
     * @param itemId              the item id
     * @param copyRoleAssignments the copy role assignments
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean breakRoleInheritance(String listId, int itemId, boolean copyRoleAssignments) throws ServiceException {
        return breakRoleInheritance(listId, itemId, copyRoleAssignments, false);
    }

    /**
     * Break role inheritance.
     *
     * @param listId              the list id
     * @param itemId              the item id
     * @param copyRoleAssignments the copy role assignments
     * @param clearSubscopes      the clear subscopes
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean breakRoleInheritance(String listId, int itemId, boolean copyRoleAssignments, boolean clearSubscopes) throws ServiceException {
        return breakRoleInheritanceImplementation(listId, itemId, copyRoleAssignments, clearSubscopes);
    }

    private boolean breakRoleInheritanceImplementation(String listId, int itemId, boolean copyRoleAssignments, boolean clearSubscopes) throws ServiceException {
        boolean success = false;
        InputStream inputStream = null;

        String requestUrl = "_api/web/lists('" + listId + "')/items(" + itemId + ")/BreakRoleInheritance(copyroleassignments=" + Boolean.toString(copyRoleAssignments).toLowerCase() + ", clearsubscopes=" + Boolean.toString(clearSubscopes).toLowerCase() + ")";

        try {
            inputStream = sendRequest("POST", requestUrl);

            //debug(inputStream);

            success = parseBooleanResponse(inputStream, "BreakRoleInheritance");
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return success;
    }

    /**
     * Ensure user.
     *
     * @param loginName the login name
     * @return the user
     * @throws ServiceException the service exception
     */
    public User ensureUser(String loginName) throws ServiceException {
        return ensureUserImplementation(loginName);
    }

    private User ensureUserImplementation(String loginName) throws ServiceException {
        if (loginName == null) {
            throw new IllegalArgumentException("loginName");
        }

        User user = null;
        InputStream inputStream = null;

        String requestUrl = "_api/web/EnsureUser";

        String requestBody = "{ 'logonName': '" + Util.encodeJson(loginName) + "' }";

        try {
            inputStream = sendRequest("POST", null, requestUrl, requestBody);

            //debug(inputStream);

            user = new User(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl, requestBody);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl, requestBody);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestBody);
                }
            }
        }

        return user;
    }

    /**
     * Gets the catalog.
     *
     * @param listTemplate the list template
     * @return the catalog
     * @throws ServiceException the service exception
     */
    public com.independentsoft.share.List getCatalog(ListTemplateType listTemplate) throws ServiceException {
        return getCatalogImplementation(listTemplate);
    }

    private com.independentsoft.share.List getCatalogImplementation(ListTemplateType listTemplate) throws ServiceException {
        com.independentsoft.share.List list = null;
        InputStream inputStream = null;

        String requestUrl = "_api/web/GetCatalog(" + EnumUtil.parseListTemplateType(listTemplate) + ")";

        try {
            inputStream = sendRequest("POST", requestUrl);

            //debug(inputStream);

            list = new com.independentsoft.share.List(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return list;
    }

    /**
     * Gets the changes.
     *
     * @param query the query
     * @return the changes
     * @throws ServiceException the service exception
     */
//DA    public List<Change> getChanges(ChangeQuery query) throws ServiceException {
    public List<Change> getChanges(String subSite, ChangeQuery query) throws ServiceException {//DA
        return getChanges(subSite, query, new ArrayList<IQueryOption>());
    }

    /**
     * Gets the changes.
     *
     * @param query        the query
     * @param queryOptions the query options
     * @return the changes
     * @throws ServiceException the service exception
     */
//DA    public List<Change> getChanges(ChangeQuery query, List<IQueryOption> queryOptions) throws ServiceException {
    public List<Change> getChanges(String subSite, ChangeQuery query, List<IQueryOption> queryOptions) throws ServiceException {//DA
        return getChangesImplementation(subSite, query, queryOptions);
    }

    //DA    private List<Change> getChangesImplementation(ChangeQuery query, List<IQueryOption> queryOptions) throws ServiceException {
    private List<Change> getChangesImplementation(String subSite, ChangeQuery query, List<IQueryOption> queryOptions) throws ServiceException {//DA
        if (query == null) {
            throw new IllegalArgumentException("query");
        }

        InputStream inputStream = null;
        List<Change> changes = new ArrayList<Change>();

        String queryOptionsString = Util.queryOptionsToString(queryOptions);

        String requestUrl = "_api/web/GetChanges" + queryOptionsString;

        String requestBody = query.toString();

        try {
            inputStream = sendRequest("POST", subSite, requestUrl, requestBody);

            //debug(inputStream);

            changes = parseGetChanges(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl, requestBody);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl, requestBody);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestBody);
                }
            }
        }

        return changes;
    }

    /**
     * Gets the changes.
     *
     * @param query  the query
     * @param listId the list id
     * @return the changes
     * @throws ServiceException the service exception
     */
//DA    public List<Change> getChanges(ChangeQuery query, String listId) throws ServiceException {
    public List<Change> getChanges(String subSite, ChangeQuery query, String listId) throws ServiceException {//DA
        return getChanges(subSite, query, listId, null);
    }

    /**
     * Gets the changes.
     *
     * @param query        the query
     * @param listId       the list id
     * @param queryOptions the query options
     * @return the changes
     * @throws ServiceException the service exception
     */
//DA    public List<Change> getChanges(ChangeQuery query, String listId, List<IQueryOption> queryOptions) throws ServiceException {
    public List<Change> getChanges(String subSite, ChangeQuery query, String listId, List<IQueryOption> queryOptions) throws ServiceException {//DA
        return getChangesImplementation(subSite, query, listId, queryOptions);
    }

    //DA    private List<Change> getChangesImplementation(ChangeQuery query, String listId, List<IQueryOption> queryOptions) throws ServiceException {
    private List<Change> getChangesImplementation(String subSite, ChangeQuery query, String listId, List<IQueryOption> queryOptions) throws ServiceException {//DA
        if (query == null) {
            throw new IllegalArgumentException("query");
        }

        if (listId == null) {
            throw new IllegalArgumentException("listId");
        }

        InputStream inputStream = null;
        List<Change> changes = new ArrayList<Change>();

        String queryOptionsString = Util.queryOptionsToString(queryOptions);

        String requestUrl = "_api/web/lists('" + listId + "')/GetChanges" + queryOptionsString;

        String requestBody = query.toString();

        try {
            inputStream = sendRequest("POST", null, requestUrl, requestBody);

            //debug(inputStream);

            changes = parseGetChanges(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl, requestBody);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl, requestBody);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestBody);
                }
            }
        }

        return changes;
    }

    /**
     * Gets the list item changes.
     *
     * @param listId the list id
     * @param query  the query
     * @return the list item changes
     * @throws ServiceException the service exception
     */
//DA    public List<Change> getListItemChanges(String listId, ChangeLogItemQuery query) throws ServiceException {
    public List<Change> getListItemChanges(String subSite, String listId, ChangeLogItemQuery query) throws ServiceException {//DA
        return getListItemChanges(subSite, listId, query, null);
    }

    /**
     * Gets the list item changes.
     *
     * @param listId       the list id
     * @param query        the query
     * @param queryOptions the query options
     * @return the list item changes
     * @throws ServiceException the service exception
     */
//DA    public List<Change> getListItemChanges(String listId, ChangeLogItemQuery query, List<IQueryOption> queryOptions) throws ServiceException {
    public List<Change> getListItemChanges(String subSite, String listId, ChangeLogItemQuery query, List<IQueryOption> queryOptions) throws ServiceException {//DA
        return getListItemChangesImplementation(subSite, listId, query, queryOptions);
    }

    //DA       private List<Change> getListItemChangesImplementation(String listId, ChangeLogItemQuery query, List<IQueryOption> queryOptions) throws ServiceException {
    private List<Change> getListItemChangesImplementation(String subSite, String listId, ChangeLogItemQuery query, List<IQueryOption> queryOptions) throws ServiceException {//DA
        if (listId == null) {
            throw new IllegalArgumentException("listId");
        }

        if (query == null) {
            throw new IllegalArgumentException("query");
        }

        InputStream inputStream = null;
        List<Change> changes = new ArrayList<Change>();

        String queryOptionsString = Util.queryOptionsToString(queryOptions);

        String requestUrl = "_api/web/lists('" + listId + "')/getlistitemchangessincetoken" + queryOptionsString;

        String requestBody = query.toString();

        try {
            inputStream = sendRequest("POST", subSite, requestUrl, requestBody);

            //debug(inputStream);

            changes = parseGetChanges(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl, requestBody);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl, requestBody);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestBody);
                }
            }
        }

        return changes;
    }

    /**
     * Render list data.
     *
     * @param listId  the list id
     * @param viewXml the view xml
     * @return the string
     * @throws ServiceException the service exception
     */
    public String renderListData(String listId, String viewXml) throws ServiceException {
        return renderListDataImplementation(listId, viewXml);
    }

    private String renderListDataImplementation(String listId, String viewXml) throws ServiceException {
        if (listId == null) {
            throw new IllegalArgumentException("listId");
        }

        if (viewXml == null) {
            throw new IllegalArgumentException("viewXml");
        }

        InputStream inputStream = null;
        String renderString = null;

        String requestUrl = "_api/web/lists('" + listId + "')/RenderListData(@viewXml)?@viewXml='" + Util.encodeUrl(viewXml) + "'";

        try {
            inputStream = sendRequest("POST", requestUrl);

            //debug(inputStream);

            renderString = parseRenderListData(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return renderString;
    }

    /**
     * Render list form data.
     *
     * @param listId the list id
     * @param itemId the item id
     * @param formId the form id
     * @param mode   the mode
     * @return the string
     * @throws ServiceException the service exception
     */
    public String renderListFormData(String listId, int itemId, String formId, ControlMode mode) throws ServiceException {
        return renderListFormDataImplementation(listId, itemId, formId, mode);
    }

    private String renderListFormDataImplementation(String listId, int itemId, String formId, ControlMode mode) throws ServiceException {
        if (listId == null) {
            throw new IllegalArgumentException("listId");
        }

        if (itemId < 0) {
            throw new IllegalArgumentException("The itemId must be non-negative.");
        }

        if (formId == null) {
            throw new IllegalArgumentException("formId");
        }

        InputStream inputStream = null;
        String renderString = null;

        String requestUrl = "_api/web/lists('" + listId + "')/renderlistformdata(itemid=" + itemId + ", formid='" + formId + "', mode=" + EnumUtil.parseControlMode(mode) + ")";

        try {
            inputStream = sendRequest("POST", requestUrl);

            //debug(inputStream);

            renderString = parseRenderListData(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return renderString;
    }

    /**
     * Reserve list item id.
     *
     * @param listId the list id
     * @return the int
     * @throws ServiceException the service exception
     */
    public int reserveListItemId(String listId) throws ServiceException {
        return reserveListItemIdImplementation(listId);
    }

    private int reserveListItemIdImplementation(String listId) throws ServiceException {
        if (listId == null) {
            throw new IllegalArgumentException("listId");
        }

        int itemId = -1;
        InputStream inputStream = null;

        String requestUrl = "_api/web/lists('" + listId + "')/ReserveListItemId";

        try {
            inputStream = sendRequest("POST", requestUrl);

            //debug(inputStream);

            itemId = parseReserveListItemId(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return itemId;
    }

    /**
     * Reset role inheritance.
     *
     * @param listId the list id
     * @param itemId the item id
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean resetRoleInheritance(String listId, int itemId) throws ServiceException {
        return resetRoleInheritanceImplementation(listId, itemId);
    }

    private boolean resetRoleInheritanceImplementation(String listId, int itemId) throws ServiceException {
        if (listId == null) {
            throw new IllegalArgumentException("listId");
        }

        if (itemId < 0) {
            throw new IllegalArgumentException("The itemId must be non-negative.");
        }

        boolean success = false;
        InputStream inputStream = null;

        String requestUrl = "_api/web/lists('" + listId + "')/items(" + itemId + ")/ResetRoleInheritance";

        try {
            inputStream = sendRequest("POST", requestUrl);

            //debug(inputStream);

            success = parseBooleanResponse(inputStream, "ResetRoleInheritance");
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return success;
    }

    /**
     * Gets the related fields.
     *
     * @param listId the list id
     * @return the related fields
     * @throws ServiceException the service exception
     */
    public List<Field> getRelatedFields(String listId) throws ServiceException {
        return getRelatedFields(listId, null);
    }

    /**
     * Gets the related fields.
     *
     * @param listId       the list id
     * @param queryOptions the query options
     * @return the related fields
     * @throws ServiceException the service exception
     */
    public List<Field> getRelatedFields(String listId, List<IQueryOption> queryOptions) throws ServiceException {
        return getRelatedFieldsImplementation(listId, queryOptions);
    }

    private List<Field> getRelatedFieldsImplementation(String listId, List<IQueryOption> queryOptions) throws ServiceException {
        if (listId == null) {
            throw new IllegalArgumentException("listId");
        }

        InputStream inputStream = null;
        List<Field> fields = new ArrayList<Field>();

        String queryOptionsString = Util.queryOptionsToString(queryOptions);

        String requestUrl = "_api/web/lists('" + listId + "')/GetRelatedFields" + queryOptionsString;

        try {
            inputStream = sendRequest("POST", requestUrl);

            //debug(inputStream);

            fields = parseGetFields(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return fields;
    }

    /**
     * Gets the site templates.
     *
     * @param locale the locale
     * @return the site templates
     * @throws ServiceException the service exception
     */
    public List<SiteTemplate> getSiteTemplates(Locale locale) throws ServiceException {
        return getSiteTemplates(locale, false);
    }

    /**
     * Gets the site templates.
     *
     * @param locale       the locale
     * @param queryOptions the query options
     * @return the site templates
     * @throws ServiceException the service exception
     */
    public List<SiteTemplate> getSiteTemplates(Locale locale, List<IQueryOption> queryOptions) throws ServiceException {
        return getSiteTemplates(locale, false, queryOptions);
    }

    /**
     * Gets the site templates.
     *
     * @param locale               the locale
     * @param includeCrossLanguage the include cross language
     * @return the site templates
     * @throws ServiceException the service exception
     */
    public List<SiteTemplate> getSiteTemplates(Locale locale, boolean includeCrossLanguage) throws ServiceException {
        return getSiteTemplates(locale, includeCrossLanguage, null);
    }

    /**
     * Gets the site templates.
     *
     * @param locale               the locale
     * @param includeCrossLanguage the include cross language
     * @param queryOptions         the query options
     * @return the site templates
     * @throws ServiceException the service exception
     */
    public List<SiteTemplate> getSiteTemplates(Locale locale, boolean includeCrossLanguage, List<IQueryOption> queryOptions) throws ServiceException {
        return getSiteTemplatesImplementation(locale, includeCrossLanguage, queryOptions);
    }

    private List<SiteTemplate> getSiteTemplatesImplementation(Locale locale, boolean includeCrossLanguage, List<IQueryOption> queryOptions) throws ServiceException {
        List<SiteTemplate> templates = new ArrayList<SiteTemplate>();

        String includeCrossLanguageParameter = includeCrossLanguage ? ", doincludecrosslanguage=true" : "";

        String queryOptionsString = Util.queryOptionsToString(queryOptions);

        String requestUrl = "_api/web/GetAvailableWebTemplates(lcid=" + EnumUtil.parseLocale(locale) + includeCrossLanguageParameter + ")" + queryOptionsString;

        InputStream inputStream = null;

        try {
            inputStream = sendRequest("POST", requestUrl);

            //debug(inputStream);

            templates = parseGetSiteTemplates(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return templates;
    }

    /**
     * Gets the effective base permissions.
     *
     * @return the effective base permissions
     * @throws ServiceException the service exception
     */
    public List<BasePermission> getEffectiveBasePermissions() throws ServiceException {
        return getEffectiveBasePermissionsImplementation();
    }

    private List<BasePermission> getEffectiveBasePermissionsImplementation() throws ServiceException {
        InputStream inputStream = null;
        List<BasePermission> permissions = new ArrayList<BasePermission>();

        String requestUrl = "_api/web/EffectiveBasePermissions";

        try {
            inputStream = sendRequest("POST", requestUrl);

            //debug(inputStream);

            permissions = Util.parseBasePermissions(inputStream, "EffectiveBasePermissions");
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return permissions;
    }

    /**
     * Creates the site.
     *
     * @param siteInfo the site info
     * @return the site
     * @throws ServiceException the service exception
     */
    public Site createSite(SiteCreationInfo siteInfo) throws ServiceException {
        return createSiteImplementation(siteInfo);
    }

    private Site createSiteImplementation(SiteCreationInfo siteInfo) throws ServiceException {
        if (siteInfo == null) {
            throw new IllegalArgumentException("siteInfo");
        }

        Site returnValue = null;
        InputStream inputStream = null;

        String requestUrl = "_api/web/webs/Add";

        String requestBody = siteInfo.toString();

        try {
            inputStream = sendRequest("POST", null, requestUrl, requestBody);

            //debug(inputStream);

            returnValue = new Site(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl, requestBody);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl, requestBody);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestBody);
                }
            }
        }

        return returnValue;
    }

    /**
     * Update site.
     *
     * @param site the site
     * @throws ServiceException the service exception
     */
    public void updateSite(Site site) throws ServiceException {
        updateSiteImplementation(site);
    }

    private void updateSiteImplementation(Site site) throws ServiceException {
        if (site == null) {
            throw new IllegalArgumentException("site");
        }

        String requestUrl = "_api/web";

        String requestBody = site.toUpdateJSon();

        InputStream inputStream = null;

        try {
            inputStream = sendRequest("POST", null, requestUrl, requestBody, "MERGE");

            //debug(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl, requestBody);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl, requestBody);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestBody);
                }
            }
        }
    }

    /**
     * Delete site.
     *
     * @throws ServiceException the service exception
     */
    public void deleteSite() throws ServiceException {
        deleteSiteImplementation();
    }

    private void deleteSiteImplementation() throws ServiceException {
        String requestUrl = "_api/web";

        InputStream inputStream = null;

        try {
            inputStream = sendRequest("POST", null, requestUrl, null, "DELETE");

            //debug(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }
    }

    /**
     * Gets the users.
     *
     * @param groupId the group id
     * @return the users
     * @throws ServiceException the service exception
     */
    public List<User> getUsers(String subsite, int groupId) throws ServiceException { // DA - added subsite
        return getUsers(subsite, groupId, null);  // DA - added subsite
    }

    /**
     * Gets the users.
     *
     * @param groupId      the group id
     * @param queryOptions the query options
     * @return the users
     * @throws ServiceException the service exception
     */
    public List<User> getUsers(String subsite, int groupId, List<IQueryOption> queryOptions) throws ServiceException { // DA - added subsite
        return getUsersImplementation(subsite, groupId, queryOptions);  // DA - added subsite
    }

    private List<User> getUsersImplementation(String subsite, int groupId, List<IQueryOption> queryOptions) throws ServiceException {  // DA - added subsite
        if (groupId <= 0) {
            throw new IllegalArgumentException("groupId");
        }

        InputStream inputStream = null;
        List<User> users = new ArrayList<User>();

        String queryOptionsString = Util.queryOptionsToString(queryOptions);

        String requestUrl = "_api/web/SiteGroups(" + groupId + ")/Users" + queryOptionsString;

        try {
            inputStream = sendRequest("GET", subsite, requestUrl);  // DA - added subsite

            //debug(inputStream);

            users = parseGetUsers(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return users;
    }

    /**
     * Gets the user.
     *
     * @param userId  the user id
     * @param groupId the group id
     * @return the user
     * @throws ServiceException the service exception
     */
    public User getUser(int userId, int groupId) throws ServiceException {
        return getUserImplementation(userId, groupId);
    }

    private User getUserImplementation(int userId, int groupId) throws ServiceException {
        if (userId <= 0) {
            throw new IllegalArgumentException("userId");
        }

        if (groupId <= 0) {
            throw new IllegalArgumentException("groupId");
        }

        User user = null;
        InputStream inputStream = null;

        String requestUrl = "_api/web/SiteGroups(" + groupId + ")/Users/GetById(" + userId + ")";

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream);

            user = new User(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return user;
    }


    /**
     * Gets the users.
     *
     * @return the users
     * @throws ServiceException the service exception
     */
    public List<User> getUsers(String subsite) throws ServiceException {  // DA - added subsite
        return getUsers(subsite, null);  // DA - added subsite
    }

    /**
     * Gets the users.
     *
     * @param queryOptions the query options
     * @return the users
     * @throws ServiceException the service exception
     */
    public List<User> getUsers(String subsite, List<IQueryOption> queryOptions) throws ServiceException { // DA - added subsite
        return getUsersImplementation(subsite, queryOptions); // DA - added subsite
    }

    private List<User> getUsersImplementation(String subsite, List<IQueryOption> queryOptions) throws ServiceException { // DA - added subsite
        InputStream inputStream = null;
        List<User> users = new ArrayList<User>();

        String queryOptionsString = Util.queryOptionsToString(queryOptions);

        String requestUrl = "_api/web/SiteUsers" + queryOptionsString;

        try {
            inputStream = sendRequest("GET", subsite, requestUrl); // DA - added subsite

            //debug(inputStream);

            users = parseGetUsers(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return users;
    }

    /**
     * Gets the user.
     *
     * @param userId the user id
     * @return the user
     * @throws ServiceException the service exception
     */
    public User getUser(int userId) throws ServiceException {
        return getUserImplementation(userId);
    }

    private User getUserImplementation(int userId) throws ServiceException {
        User user = null;
        InputStream inputStream = null;

        String requestUrl = "_api/web/GetUserById(" + userId + ")";

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream);

            user = new User(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return user;
    }

    /**
     * Gets the user groups.
     *
     * @param userId the user id
     * @return the user groups
     * @throws ServiceException the service exception
     */
    public List<Group> getUserGroups(int userId) throws ServiceException {
        return getUserGroups(userId, null);
    }

    /**
     * Gets the user groups.
     *
     * @param userId       the user id
     * @param queryOptions the query options
     * @return the user groups
     * @throws ServiceException the service exception
     */
    public List<Group> getUserGroups(int userId, List<IQueryOption> queryOptions) throws ServiceException {
        return getUserGroupsImplementation(userId, queryOptions);
    }

    private List<Group> getUserGroupsImplementation(int userId, List<IQueryOption> queryOptions) throws ServiceException {
        InputStream inputStream = null;
        List<Group> groups = new ArrayList<Group>();

        String queryOptionsString = Util.queryOptionsToString(queryOptions);

        String requestUrl = "_api/web/GetUserById(" + userId + ")/Groups" + queryOptionsString;

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream);

            groups = parseGetGroups(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return groups;
    }

    /**
     * Gets the user.
     *
     * @param loginName the login name
     * @param groupId   the group id
     * @return the user
     * @throws ServiceException the service exception
     */
    public User getUser(String loginName, int groupId) throws ServiceException {
        return getUserImplementation(loginName, groupId);
    }

    private User getUserImplementation(String loginName, int groupId) throws ServiceException {
        if (loginName == null) {
            throw new IllegalArgumentException("loginName");
        }

        if (groupId <= 0) {
            throw new IllegalArgumentException("groupId");
        }

        User user = null;
        InputStream inputStream = null;

        String requestUrl = "_api/web/SiteGroups(" + groupId + ")/Users/GetByLoginName(@v)?@v='" + Util.encodeUrl(loginName) + "'";

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream);

            user = new User(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return user;
    }

    /**
     * Gets the user by email.
     *
     * @param email   the email
     * @param groupId the group id
     * @return the user by email
     * @throws ServiceException the service exception
     */
    public User getUserByEmail(String email, int groupId) throws ServiceException {
        return getUserByEmailImplementation(email, groupId);
    }

    private User getUserByEmailImplementation(String email, int groupId) throws ServiceException {
        if (email == null) {
            throw new IllegalArgumentException("email");
        }

        if (groupId <= 0) {
            throw new IllegalArgumentException("groupId");
        }

        User user = null;
        InputStream inputStream = null;

        String requestUrl = "_api/web/SiteGroups(" + groupId + ")/Users/GetByEmail('" + Util.encodeUrl(email) + "')";

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream);

            user = new User(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return user;
    }

    /**
     * Delete user.
     *
     * @param userId  the user id
     * @param groupId the group id
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean deleteUser(int userId, int groupId) throws ServiceException {
        return deleteUserImplementation(userId, groupId);
    }

    private boolean deleteUserImplementation(int userId, int groupId) throws ServiceException {
        if (userId <= 0) {
            throw new IllegalArgumentException("userId");
        }

        if (groupId <= 0) {
            throw new IllegalArgumentException("groupId");
        }

        boolean success = false;
        InputStream inputStream = null;

        String requestUrl = "_api/web/SiteGroups(" + groupId + ")/Users/RemoveById(" + userId + ")";

        try {
            inputStream = sendRequest("POST", requestUrl);

            //debug(inputStream);

            success = parseBooleanResponse(inputStream, "RemoveById");
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return success;
    }


    /**
     * Delete user.
     *
     * @param loginName the login name
     * @param groupId   the group id
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean deleteUser(String loginName, int groupId) throws ServiceException {
        return deleteUserImplementation(loginName, groupId);
    }

    private boolean deleteUserImplementation(String loginName, int groupId) throws ServiceException {
        if (loginName == null) {
            throw new IllegalArgumentException("loginName");
        }

        if (groupId <= 0) {
            throw new IllegalArgumentException("groupId");
        }

        boolean success = false;
        InputStream inputStream = null;

        String requestUrl = "_api/web/SiteGroups(" + groupId + ")/Users/RemoveByLoginName(@v)?@v='" + Util.encodeUrl(loginName) + "'";

        try {
            inputStream = sendRequest("POST", requestUrl);

            //debug(inputStream);

            success = parseBooleanResponse(inputStream, "RemoveByLoginName");
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return success;
    }

    /**
     * Gets the groups.
     *
     * @return the groups
     * @throws ServiceException the service exception
     */
    public List<Group> getGroups() throws ServiceException {
        return getGroups(null);
    }

    /**
     * Gets the groups.
     *
     * @param queryOptions the query options
     * @return the groups
     * @throws ServiceException the service exception
     */
    public List<Group> getGroups(List<IQueryOption> queryOptions) throws ServiceException {
        return getGroupsImplementation(queryOptions);
    }

    private List<Group> getGroupsImplementation(List<IQueryOption> queryOptions) throws ServiceException {
        InputStream inputStream = null;
        List<Group> groups = new ArrayList<Group>();

        String queryOptionsString = Util.queryOptionsToString(queryOptions);

        String requestUrl = "_api/web/SiteGroups" + queryOptionsString;

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream);

            groups = parseGetGroups(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return groups;
    }

    /**
     * Creates the user.
     *
     * @param user    the user
     * @param groupId the group id
     * @return the user
     * @throws ServiceException the service exception
     */
    public User createUser(User user, int groupId) throws ServiceException {
        return createUserImplementation(user, groupId);
    }

    private User createUserImplementation(User user, int groupId) throws ServiceException {
        if (user == null) {
            throw new IllegalArgumentException("user");
        }

        if (groupId <= 0) {
            throw new IllegalArgumentException("groupId");
        }

        User returnValue = null;
        InputStream inputStream = null;

        String requestUrl = "_api/web/SiteGroups(" + groupId + ")/Users";

        String requestBody = user.toCreateJSon();

        try {
            inputStream = sendRequest("POST", null, requestUrl, requestBody);

            //debug(inputStream);

            returnValue = new User(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl, requestBody);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl, requestBody);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestBody);
                }
            }
        }

        return returnValue;
    }

    /**
     * Update user.
     *
     * @param user    the user
     * @param groupId the group id
     * @return the user
     * @throws ServiceException the service exception
     */
    public User updateUser(User user, int groupId) throws ServiceException {
        return updateUserImplementation(user, groupId);
    }

    private User updateUserImplementation(User user, int groupId) throws ServiceException {
        if (user == null) {
            throw new IllegalArgumentException("user");
        }

        if (user.getLoginName() == null) {
            throw new IllegalArgumentException("LoginName");
        }

        if (groupId <= 0) {
            throw new IllegalArgumentException("groupId");
        }

        User returnValue = null;
        InputStream inputStream = null;

        String requestUrl = "_api/web/SiteGroups(" + groupId + ")/Users(@v)?@v='" + user.getLoginName() + "'";

        String requestBody = user.toUpdateJSon();

        try {
            inputStream = sendRequest("POST", null, requestUrl, requestBody, "MERGE");

            //debug(inputStream);

            returnValue = new User(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl, requestBody);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl, requestBody);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestBody);
                }
            }
        }

        return returnValue;
    }

    /**
     * Creates the role.
     *
     * @param role the role
     * @return the role
     * @throws ServiceException the service exception
     */
    public Role createRole(Role role) throws ServiceException {
        return createRoleImplementation(role);
    }

    private Role createRoleImplementation(Role role) throws ServiceException {
        if (role == null) {
            throw new IllegalArgumentException("role");
        }

        Role returnValue = null;
        InputStream inputStream = null;

        String requestUrl = "_api/web/RoleDefinitions";

        String requestBody = role.toCreateJSon();

        try {
            inputStream = sendRequest("POST", null, requestUrl, requestBody);

            //debug(inputStream);

            returnValue = new Role(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl, requestBody);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl, requestBody);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestBody);
                }
            }
        }

        return returnValue;
    }

    /**
     * Delete role.
     *
     * @param roleId the role id
     * @throws ServiceException the service exception
     */
    public void deleteRole(int roleId) throws ServiceException {
        deleteRoleImplementation(roleId);
    }

    private void deleteRoleImplementation(int roleId) throws ServiceException {
        if (roleId <= 0) {
            throw new IllegalArgumentException("roleId");
        }

        InputStream inputStream = null;

        String requestUrl = "_api/web/RoleDefinitions(" + roleId + ")";

        try {
            inputStream = sendRequest("POST", null, requestUrl, null, "DELETE");

            //debug(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }
    }

    /**
     * Update role.
     *
     * @param role the role
     * @throws ServiceException the service exception
     */
    public void updateRole(Role role) throws ServiceException {
        updateRoleImplementation(role);
    }

    private void updateRoleImplementation(Role role) throws ServiceException {
        if (role == null) {
            throw new IllegalArgumentException("role");
        }

        if (role.getId() <= 0) {
            throw new IllegalArgumentException("Invalid Role Id");
        }

        InputStream inputStream = null;

        String requestUrl = "_api/web/RoleDefinitions(" + role.getId() + ")";

        String requestBody = role.toUpdateJSon();

        try {
            inputStream = sendRequest("POST", null, requestUrl, requestBody, "MERGE");

            //debug(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }
    }

    /**
     * Creates the group.
     *
     * @param group the group
     * @return the group
     * @throws ServiceException the service exception
     */
    public Group createGroup(Group group) throws ServiceException {
        return createGroupImplementation(group);
    }

    private Group createGroupImplementation(Group group) throws ServiceException {
        if (group == null) {
            throw new IllegalArgumentException("group");
        }

        Group returnValue = null;
        InputStream inputStream = null;

        String requestUrl = "_api/web/SiteGroups";

        String requestBody = group.toCreateJSon();

        try {
            inputStream = sendRequest("POST", null, requestUrl, requestBody);

            //debug(inputStream);

            returnValue = new Group(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl, requestBody);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl, requestBody);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl, requestBody);
                }
            }
        }

        return returnValue;
    }

    /**
     * Update group.
     *
     * @param group the group
     * @throws ServiceException the service exception
     */
    public void updateGroup(Group group) throws ServiceException {
        updateGroupImplementation(group);
    }

    private void updateGroupImplementation(Group group) throws ServiceException {
        if (group == null) {
            throw new IllegalArgumentException("group");
        }

        if (group.getId() == 0) {
            throw new IllegalArgumentException("Id");
        }

        InputStream inputStream = null;

        String requestUrl = "_api/web/SiteGroups(" + group.getId() + ")";

        String requestBody = group.toUpdateJSon();

        try {
            inputStream = sendRequest("POST", null, requestUrl, requestBody, "MERGE");

            //debug(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl, requestBody);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl, requestBody);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl, requestBody);
                }
            }
        }
    }

    /**
     * Delete group.
     *
     * @param groupId the group id
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean deleteGroup(int groupId) throws ServiceException {
        return deleteGroupImplementation(groupId);
    }

    private boolean deleteGroupImplementation(int groupId) throws ServiceException {
        boolean success = false;
        InputStream inputStream = null;

        String requestUrl = "_api/web/SiteGroups/RemoveById(" + groupId + ")";

        try {
            inputStream = sendRequest("POST", requestUrl);

            //debug(inputStream);

            success = parseBooleanResponse(inputStream, "RemoveById");
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return success;
    }

    /**
     * Delete group.
     *
     * @param loginName the login name
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean deleteGroup(String loginName) throws ServiceException {
        return deleteGroupImplementation(loginName);
    }

    private boolean deleteGroupImplementation(String loginName) throws ServiceException {
        if (loginName == null) {
            throw new IllegalArgumentException("loginName");
        }

        boolean success = false;
        InputStream inputStream = null;

        String requestUrl = "_api/web/SiteGroups/RemoveByLoginName('" + Util.encodeUrl(loginName) + "')";

        try {
            inputStream = sendRequest("POST", requestUrl);

            //debug(inputStream);

            success = parseBooleanResponse(inputStream, "RemoveByLoginName");
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return success;
    }

    /**
     * Gets the group.
     *
     * @param groupId the group id
     * @return the group
     * @throws ServiceException the service exception
     */
    public Group getGroup(int groupId) throws ServiceException {
        return getGroupImplementation(groupId);
    }

    private Group getGroupImplementation(int groupId) throws ServiceException {
        Group group = null;
        InputStream inputStream = null;

        String requestUrl = "_api/web/SiteGroups(" + groupId + ")";

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream);

            group = new Group(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return group;
    }

    /**
     * Gets the group owner.
     *
     * @param groupId the group id
     * @return the group owner
     * @throws ServiceException the service exception
     */
    public User getGroupOwner(int groupId) throws ServiceException {
        return getGroupOwnerImplementation(groupId);
    }

    private User getGroupOwnerImplementation(int groupId) throws ServiceException {
        User owner = null;
        InputStream inputStream = null;

        String requestUrl = "_api/web/SiteGroups(" + groupId + ")/Owner";

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream);

            owner = new User(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return owner;
    }

    /**
     * Gets the group users.
     *
     * @param groupId the group id
     * @return the group users
     * @throws ServiceException the service exception
     */
    public List<User> getGroupUsers(int groupId) throws ServiceException {
        return getGroupUsers(groupId, null);
    }

    /**
     * Gets the group users.
     *
     * @param groupId      the group id
     * @param queryOptions the query options
     * @return the group users
     * @throws ServiceException the service exception
     */
    public List<User> getGroupUsers(int groupId, List<IQueryOption> queryOptions) throws ServiceException {
        return getGroupUsersImplementation(groupId, queryOptions);
    }

    private List<User> getGroupUsersImplementation(int groupId, List<IQueryOption> queryOptions) throws ServiceException {
        InputStream inputStream = null;
        List<User> users = new ArrayList<User>();

        String queryOptionsString = Util.queryOptionsToString(queryOptions);

        String requestUrl = "_api/web/SiteGroups(" + groupId + ")/Users" + queryOptionsString;

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream);

            users = parseGetUsers(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return users;
    }

    /**
     * Gets the group.
     *
     * @param loginName the login name
     * @return the group
     * @throws ServiceException the service exception
     */
    public Group getGroup(String loginName) throws ServiceException {
        return getGroupImplementation(loginName);
    }

    private Group getGroupImplementation(String loginName) throws ServiceException {
        if (loginName == null) {
            throw new IllegalArgumentException("loginName");
        }

        Group group = null;
        InputStream inputStream = null;

        String requestUrl = "_api/web/SiteGroups/GetByName('" + Util.encodeUrl(loginName) + "')";

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream);

            group = new Group(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return group;
    }

    /**
     * Gets the group owner.
     *
     * @param loginName the login name
     * @return the group owner
     * @throws ServiceException the service exception
     */
    public User getGroupOwner(String loginName) throws ServiceException {
        return getGroupOwnerImplementation(loginName);
    }

    private User getGroupOwnerImplementation(String loginName) throws ServiceException {
        if (loginName == null) {
            throw new IllegalArgumentException("loginName");
        }

        User owner = null;
        InputStream inputStream = null;

        String requestUrl = "_api/web/SiteGroups/GetByName('" + Util.encodeUrl(loginName) + "')/Owner";

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream);

            owner = new User(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return owner;
    }

    /**
     * Gets the group users.
     *
     * @param loginName the login name
     * @return the group users
     * @throws ServiceException the service exception
     */
    public List<User> getGroupUsers(String loginName) throws ServiceException {
        return getGroupUsers(loginName, null);
    }

    /**
     * Gets the group users.
     *
     * @param loginName    the login name
     * @param queryOptions the query options
     * @return the group users
     * @throws ServiceException the service exception
     */
    public List<User> getGroupUsers(String loginName, List<IQueryOption> queryOptions) throws ServiceException {
        return getGroupUsersImplementation(loginName, queryOptions);
    }

    private List<User> getGroupUsersImplementation(String loginName, List<IQueryOption> queryOptions) throws ServiceException {
        if (loginName == null) {
            throw new IllegalArgumentException("loginName");
        }

        InputStream inputStream = null;
        List<User> users = new ArrayList<User>();

        String queryOptionsString = Util.queryOptionsToString(queryOptions);

        String requestUrl = "_api/web/SiteGroups/GetByName('" + Util.encodeUrl(loginName) + "')/Users" + queryOptionsString;

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream);

            users = parseGetUsers(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return users;
    }

    /**
     * Gets the role assignments.
     *
     * @return the role assignments
     * @throws ServiceException the service exception
     */
    public List<Integer> getRoleAssignments() throws ServiceException {
        return getRoleAssignments(null);
    }

    /**
     * Gets the role assignments.
     *
     * @param queryOptions the query options
     * @return the role assignments
     * @throws ServiceException the service exception
     */
    public List<Integer> getRoleAssignments(List<IQueryOption> queryOptions) throws ServiceException {
        return getRoleAssignmentsImplementation(queryOptions);
    }

    private List<Integer> getRoleAssignmentsImplementation(List<IQueryOption> queryOptions) throws ServiceException {
        InputStream inputStream = null;
        List<Integer> list = new ArrayList<Integer>();

        String queryOptionsString = Util.queryOptionsToString(queryOptions);

        String requestUrl = "_api/web/roleassignments" + queryOptionsString;

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream);

            list = parseGetRoleAssignments(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return list;
    }

    /**
     * Gets the role assignments.
     *
     * @param principalId the principal id
     * @return the role assignments
     * @throws ServiceException the service exception
     */
    public List<Integer> getRoleAssignments(int principalId) throws ServiceException {
        return getRoleAssignmentsImplementation(principalId);
    }

    private List<Integer> getRoleAssignmentsImplementation(int principalId) throws ServiceException {
        InputStream inputStream = null;
        List<Integer> list = new ArrayList<Integer>();

        String requestUrl = "_api/web/roleassignments/getbyprincipalid(" + principalId + ")";

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream);

            list = parseGetRoleAssignments(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return list;
    }

    /**
     * Adds the role assignment.
     *
     * @param principalId the principal id
     * @param roleId      the role id
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean addRoleAssignment(int principalId, int roleId) throws ServiceException {
        return addRoleAssignmentImplementation(principalId, roleId);
    }

    private boolean addRoleAssignmentImplementation(int principalId, int roleId) throws ServiceException {
        InputStream inputStream = null;
        boolean success = false;

        String requestUrl = "_api/web/roleassignments/addroleassignment(principalid=" + principalId + ",roledefid=" + roleId + ")";

        try {
            inputStream = sendRequest("POST", requestUrl);

            //debug(inputStream);

            success = parseBooleanResponse(inputStream, "AddRoleAssignment");
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return success;
    }

    /**
     * Removes the role assignment.
     *
     * @param principalId the principal id
     * @param roleId      the role id
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean removeRoleAssignment(int principalId, int roleId) throws ServiceException {
        return removeRoleAssignmentImplementation(principalId, roleId);
    }

    private boolean removeRoleAssignmentImplementation(int principalId, int roleId) throws ServiceException {
        InputStream inputStream = null;
        boolean success = false;

        String requestUrl = "_api/web/roleassignments/removeroleassignment(principalid=" + principalId + ",roledefid=" + roleId + ")";

        try {
            inputStream = sendRequest("POST", requestUrl);

            //debug(inputStream);

            success = parseBooleanResponse(inputStream, "RemoveRoleAssignment");
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return success;
    }

    /**
     * Gets the regional settings.
     *
     * @return the regional settings
     * @throws ServiceException the service exception
     */
    public RegionalSettings getRegionalSettings() throws ServiceException {
        return getRegionalSettingsImplementation();
    }

    private RegionalSettings getRegionalSettingsImplementation() throws ServiceException {
        InputStream inputStream = null;
        RegionalSettings regionalSettings = null;

        String requestUrl = "_api/web/RegionalSettings";

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug (inputStream);

            regionalSettings = new RegionalSettings(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return regionalSettings;
    }

    /**
     * Gets the time zones.
     *
     * @return the time zones
     * @throws ServiceException the service exception
     */
    public List<TimeZone> getTimeZones() throws ServiceException {
        return getTimeZones(null);
    }

    /**
     * Gets the time zones.
     *
     * @param queryOptions the query options
     * @return the time zones
     * @throws ServiceException the service exception
     */
    public List<TimeZone> getTimeZones(List<IQueryOption> queryOptions) throws ServiceException {
        return getTimeZonesImplementation(queryOptions);
    }

    private List<TimeZone> getTimeZonesImplementation(List<IQueryOption> queryOptions) throws ServiceException {
        InputStream inputStream = null;
        List<TimeZone> timeZones = new ArrayList<TimeZone>();

        String queryOptionsString = Util.queryOptionsToString(queryOptions);

        String requestUrl = "_api/web/RegionalSettings/TimeZones" + queryOptionsString;

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream);

            timeZones = parseGetTimeZones(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return timeZones;
    }

    /**
     * Gets the time zone.
     *
     * @return the time zone
     * @throws ServiceException the service exception
     */
    public TimeZone getTimeZone() throws ServiceException {
        return getTimeZoneImplementation();
    }

    private TimeZone getTimeZoneImplementation() throws ServiceException {
        TimeZone timeZone = null;
        InputStream inputStream = null;

        String requestUrl = "_api/web/RegionalSettings/TimeZone";

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream);

            timeZone = new TimeZone(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return timeZone;
    }

    /**
     * Gets the theme info.
     *
     * @return the theme info
     * @throws ServiceException the service exception
     */
    public ThemeInfo getThemeInfo() throws ServiceException {
        return getThemeInfoImplementation();
    }

    private ThemeInfo getThemeInfoImplementation() throws ServiceException {
        ThemeInfo themeInfo = null;
        InputStream inputStream = null;

        String requestUrl = "_api/web/ThemeInfo";

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream);

            themeInfo = new ThemeInfo(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return themeInfo;
    }

    /**
     * Gets the workflow templates.
     *
     * @return the workflow templates
     * @throws ServiceException the service exception
     */
    public List<WorkflowTemplate> getWorkflowTemplates() throws ServiceException {
        return getWorkflowTemplates(null);
    }

    /**
     * Gets the workflow templates.
     *
     * @param queryOptions the query options
     * @return the workflow templates
     * @throws ServiceException the service exception
     */
    public List<WorkflowTemplate> getWorkflowTemplates(List<IQueryOption> queryOptions) throws ServiceException {
        return getWorkflowTemplatesImplementation(queryOptions);
    }

    private List<WorkflowTemplate> getWorkflowTemplatesImplementation(List<IQueryOption> queryOptions) throws ServiceException {
        InputStream inputStream = null;
        List<WorkflowTemplate> workflowTemplates = new ArrayList<WorkflowTemplate>();

        String queryOptionsString = Util.queryOptionsToString(queryOptions);

        String requestUrl = "_api/web/WorkflowTemplates" + queryOptionsString;

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream);

            workflowTemplates = parseGetWorkflowTemplates(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return workflowTemplates;
    }

    /**
     * Gets the user info list.
     *
     * @return the user info list
     * @throws ServiceException the service exception
     */
    public com.independentsoft.share.List getUserInfoList() throws ServiceException {
        return getUserInfoListImplementation();
    }

    private com.independentsoft.share.List getUserInfoListImplementation() throws ServiceException {
        InputStream inputStream = null;
        com.independentsoft.share.List userInfoList = null;

        String requestUrl = "_api/web/SiteUserInfoList";

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream);

            userInfoList = new com.independentsoft.share.List(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return userInfoList;
    }

    /**
     * Gets the root folder.
     *
     * @return the root folder
     * @throws ServiceException the service exception
     */
    public Folder getRootFolder() throws ServiceException {
        return getRootFolderImplementation();
    }

    private Folder getRootFolderImplementation() throws ServiceException {
        Folder folder = null;
        InputStream inputStream = null;

        String requestUrl = "_api/web/RootFolder";

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream);

            folder = new Folder(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return folder;
    }

    /**
     * Gets the top navigation bar.
     *
     * @return the top navigation bar
     * @throws ServiceException the service exception
     */
    public List<NavigationNode> getTopNavigationBar() throws ServiceException {
        return getTopNavigationBar(null);
    }

    /**
     * Gets the top navigation bar.
     *
     * @param queryOptions the query options
     * @return the top navigation bar
     * @throws ServiceException the service exception
     */
    public List<NavigationNode> getTopNavigationBar(List<IQueryOption> queryOptions) throws ServiceException {
        return getTopNavigationBarImplementation(queryOptions);
    }

    private List<NavigationNode> getTopNavigationBarImplementation(List<IQueryOption> queryOptions) throws ServiceException {
        InputStream inputStream = null;
        List<NavigationNode> items = new ArrayList<NavigationNode>();

        String queryOptionsString = Util.queryOptionsToString(queryOptions);

        String requestUrl = "_api/web/Navigation/TopNavigationBar" + queryOptionsString;

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream);

            items = parseGetNavigationNode(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return items;
    }

    /**
     * Gets the quick launch.
     *
     * @return the quick launch
     * @throws ServiceException the service exception
     */
    public List<NavigationNode> getQuickLaunch() throws ServiceException {
        return getQuickLaunch(null);
    }

    /**
     * Gets the quick launch.
     *
     * @param queryOptions the query options
     * @return the quick launch
     * @throws ServiceException the service exception
     */
    public List<NavigationNode> getQuickLaunch(List<IQueryOption> queryOptions) throws ServiceException {
        return getQuickLaunchImplementation(queryOptions);
    }

    private List<NavigationNode> getQuickLaunchImplementation(List<IQueryOption> queryOptions) throws ServiceException {
        InputStream inputStream = null;
        List<NavigationNode> items = new ArrayList<NavigationNode>();

        String queryOptionsString = Util.queryOptionsToString(queryOptions);

        String requestUrl = "_api/web/Navigation/QuickLaunch" + queryOptionsString;

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream);

            items = parseGetNavigationNode(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return items;
    }

    /**
     * Gets the navigation node.
     *
     * @param id the id
     * @return the navigation node
     * @throws ServiceException the service exception
     */
    public NavigationNode getNavigationNode(int id) throws ServiceException {
        return getNavigationNodeImplementation(id);
    }

    private NavigationNode getNavigationNodeImplementation(int id) throws ServiceException {
        if (id <= 0) {
            throw new IllegalArgumentException("The parameter id must be non-negative.");
        }

        NavigationNode node = null;
        InputStream inputStream = null;

        String requestUrl = "_api/web/Navigation/GetNodeById(" + id + ")";

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream);

            node = new NavigationNode(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return node;
    }

    /**
     * Gets the navigation node children.
     *
     * @param id the id
     * @return the navigation node children
     * @throws ServiceException the service exception
     */
    public List<NavigationNode> getNavigationNodeChildren(int id) throws ServiceException {
        return getNavigationNodeChildren(id, null);
    }

    /**
     * Gets the navigation node children.
     *
     * @param id           the id
     * @param queryOptions the query options
     * @return the navigation node children
     * @throws ServiceException the service exception
     */
    public List<NavigationNode> getNavigationNodeChildren(int id, List<IQueryOption> queryOptions) throws ServiceException {
        return getNavigationNodeChildrenImplementation(id, queryOptions);
    }

    private List<NavigationNode> getNavigationNodeChildrenImplementation(int id, List<IQueryOption> queryOptions) throws ServiceException {
        if (id <= 0) {
            throw new IllegalArgumentException("Invalid node id");
        }

        InputStream inputStream = null;

        List<NavigationNode> nodes = new ArrayList<NavigationNode>();

        String queryOptionsString = Util.queryOptionsToString(queryOptions);

        String requestUrl = "_api/web/Navigation/GetNodeById(" + id + ")/Children" + queryOptionsString;

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream);

            nodes = parseGetNavigationNode(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return nodes;
    }

    /**
     * Checks if is shared navigation.
     *
     * @return true, if is shared navigation
     * @throws ServiceException the service exception
     */
    public boolean isSharedNavigation() throws ServiceException {
        return isSharedNavigationImplementation();
    }

    private boolean isSharedNavigationImplementation() throws ServiceException {
        boolean useShared;
        InputStream inputStream = null;

        String requestUrl = "_api/web/Navigation";

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream);

            useShared = parseBooleanResponse(inputStream, "UseShared");
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return useShared;
    }

    /**
     * Gets the recycle bin items.
     *
     * @return the recycle bin items
     * @throws ServiceException the service exception
     */
    public List<RecycleBinItem> getRecycleBinItems() throws ServiceException {
        return getRecycleBinItems(null);
    }

    /**
     * Gets the recycle bin items.
     *
     * @param queryOptions the query options
     * @return the recycle bin items
     * @throws ServiceException the service exception
     */
    public List<RecycleBinItem> getRecycleBinItems(List<IQueryOption> queryOptions) throws ServiceException {
        return getRecycleBinItemsImplementation(queryOptions);
    }

    private List<RecycleBinItem> getRecycleBinItemsImplementation(List<IQueryOption> queryOptions) throws ServiceException {
        InputStream inputStream = null;
        List<RecycleBinItem> items = new ArrayList<RecycleBinItem>();

        String queryOptionsString = Util.queryOptionsToString(queryOptions);

        String requestUrl = "_api/web/RecycleBin" + queryOptionsString;

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream);

            items = parseGetRecycleBinItems(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return items;
    }

    /**
     * Gets the recycle bin item.
     *
     * @param id the id
     * @return the recycle bin item
     * @throws ServiceException the service exception
     */
    public RecycleBinItem getRecycleBinItem(String id) throws ServiceException {
        return getRecycleBinItemImplementation(id);
    }

    private RecycleBinItem getRecycleBinItemImplementation(String id) throws ServiceException {
        if (id == null) {
            throw new IllegalArgumentException("id");
        }

        RecycleBinItem item = null;
        InputStream inputStream = null;

        String requestUrl = "_api/web/RecycleBin('" + id + "')";

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream);

            item = new RecycleBinItem(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return item;
    }

    /**
     * Gets the recycle bin item author.
     *
     * @param id the id
     * @return the recycle bin item author
     * @throws ServiceException the service exception
     */
    public User getRecycleBinItemAuthor(String id) throws ServiceException {
        return getRecycleBinItemAuthorImplementation(id);
    }

    private User getRecycleBinItemAuthorImplementation(String id) throws ServiceException {
        User author = null;
        InputStream inputStream = null;

        String requestUrl = "_api/web/RecycleBin('" + id + "')/Author";

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream);

            author = new User(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return author;
    }

    /**
     * Gets the recycle bin item deleted by.
     *
     * @param id the id
     * @return the recycle bin item deleted by
     * @throws ServiceException the service exception
     */
    public User getRecycleBinItemDeletedBy(String id) throws ServiceException {
        return getRecycleBinItemDeletedByImplementation(id);
    }

    private User getRecycleBinItemDeletedByImplementation(String id) throws ServiceException {
        if (id == null) {
            throw new IllegalArgumentException("id");
        }

        User deletedBy = null;
        InputStream inputStream = null;

        String requestUrl = "_api/web/RecycleBin('" + id + "')/DeletedBy";

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream);

            deletedBy = new User(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return deletedBy;
    }

    /**
     * Gets the user custom actions.
     *
     * @return the user custom actions
     * @throws ServiceException the service exception
     */
    public List<Role> getUserCustomActions() throws ServiceException {
        return getUserCustomActions(null);
    }

    /**
     * Gets the user custom actions.
     *
     * @param queryOptions the query options
     * @return the user custom actions
     * @throws ServiceException the service exception
     */
    public List<Role> getUserCustomActions(List<IQueryOption> queryOptions) throws ServiceException {
        return getUserCustomActionsImplementation(queryOptions);
    }

    private List<Role> getUserCustomActionsImplementation(List<IQueryOption> queryOptions) throws ServiceException {
        InputStream inputStream = null;
        List<Role> roles = new ArrayList<Role>();

        String queryOptionsString = Util.queryOptionsToString(queryOptions);

        String requestUrl = "_api/web/UserCustomActions" + queryOptionsString;

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream);

            roles = parseGetRoles(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return roles;
    }

    /**
     * Gets the roles.
     *
     * @return the roles
     * @throws ServiceException the service exception
     */
    public List<Role> getRoles() throws ServiceException {
        return getRoles(null);
    }

    /**
     * Gets the roles.
     *
     * @param queryOptions the query options
     * @return the roles
     * @throws ServiceException the service exception
     */
    public List<Role> getRoles(List<IQueryOption> queryOptions) throws ServiceException {
        return getRolesImplementation(queryOptions);
    }

    private List<Role> getRolesImplementation(List<IQueryOption> queryOptions) throws ServiceException {
        InputStream inputStream = null;
        List<Role> roles = new ArrayList<Role>();

        String queryOptionsString = Util.queryOptionsToString(queryOptions);

        String requestUrl = "_api/web/RoleDefinitions" + queryOptionsString;

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream);

            roles = parseGetRoles(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return roles;
    }

    /**
     * Gets the role.
     *
     * @param roleId the role id
     * @return the role
     * @throws ServiceException the service exception
     */
    public Role getRole(int roleId) throws ServiceException {
        return getRoleImplementation(roleId);
    }

    private Role getRoleImplementation(int roleId) throws ServiceException {
        if (roleId <= 0) {
            throw new IllegalArgumentException("The parameter roleId must be non-negative.");
        }

        Role role = null;
        InputStream inputStream = null;

        String requestUrl = "_api/web/RoleDefinitions(" + roleId + ")";

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream);

            role = new Role(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return role;
    }

    /**
     * Gets the role.
     *
     * @param name the name
     * @return the role
     * @throws ServiceException the service exception
     */
    public Role getRole(String name) throws ServiceException {
        return getRoleImplementation(name);
    }

    private Role getRoleImplementation(String name) throws ServiceException {
        if (name == null) {
            throw new IllegalArgumentException("name");
        }

        Role role = null;
        InputStream inputStream = null;

        String requestUrl = "_api/web/RoleDefinitions/GetByName('" + Util.encodeUrl(name) + "')";

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream);

            role = new Role(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return role;
    }

    /**
     * Gets the role.
     *
     * @param type the type
     * @return the role
     * @throws ServiceException the service exception
     */
    public Role getRole(RoleType type) throws ServiceException {
        return getRoleImplementation(type);
    }

    private Role getRoleImplementation(RoleType type) throws ServiceException {
        Role role = null;
        InputStream inputStream = null;

        String requestUrl = "_api/web/RoleDefinitions/GetByType('" + EnumUtil.parseRoleType(type) + "')";

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream);

            role = new Role(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return role;
    }

    /**
     * Gets the field.
     *
     * @param id the id
     * @return the field
     * @throws ServiceException the service exception
     */
    public Field getField(String id) throws ServiceException {
        return getFieldImplementation(id);
    }

    private Field getFieldImplementation(String id) throws ServiceException {
        if (id == null) {
            throw new IllegalArgumentException("id");
        }

        Field field = null;
        InputStream inputStream = null;

        String requestUrl = "_api/web/fields('" + id + "')";

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream);

            field = new Field(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return field;
    }

    /**
     * Gets the field by title.
     *
     * @param title the title
     * @return the field by title
     * @throws ServiceException the service exception
     */
    public Field getFieldByTitle(String title) throws ServiceException {
        return getFieldByTitleImplementation(title);
    }

    private Field getFieldByTitleImplementation(String title) throws ServiceException {
        if (title == null) {
            throw new IllegalArgumentException("title");
        }

        Field field = null;
        InputStream inputStream = null;

        String requestUrl = "_api/web/fields/getbytitle('" + title + "')";

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream);

            field = new Field(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return field;
    }

    /**
     * Gets the field by internal name or title.
     *
     * @param name the name
     * @return the field by internal name or title
     * @throws ServiceException the service exception
     */
    public Field getFieldByInternalNameOrTitle(String name) throws ServiceException {
        return getFieldByInternalNameOrTitleImplementation(name);
    }

    private Field getFieldByInternalNameOrTitleImplementation(String name) throws ServiceException {
        if (name == null) {
            throw new IllegalArgumentException("name");
        }

        Field field = null;
        InputStream inputStream = null;

        String requestUrl = "_api/web/fields/getbyinternalnameortitle('" + name + "')";

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream);

            field = new Field(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return field;
    }

    /**
     * Sets the show in display form.
     *
     * @param id     the id
     * @param listId the list id
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean setShowInDisplayForm(String id, String listId) throws ServiceException {
        return setShowInDisplayForm(id, listId, true);
    }

    /**
     * Sets the show in display form.
     *
     * @param id     the id
     * @param listId the list id
     * @param show   the show
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean setShowInDisplayForm(String id, String listId, boolean show) throws ServiceException {
        return setShowInDisplayFormImplementation(id, listId, show);
    }

    private boolean setShowInDisplayFormImplementation(String id, String listId, boolean show) throws ServiceException {
        if (id == null) {
            throw new IllegalArgumentException("id");
        }

        if (listId == null) {
            throw new IllegalArgumentException("listId");
        }

        boolean success = false;
        InputStream inputStream = null;

        String requestUrl = "_api/web/lists('" + listId + "')/fields('" + id + "')/setshowindisplayform(" + Boolean.toString(show).toLowerCase() + ")";

        try {
            inputStream = sendRequest("POST", requestUrl);

            //debug(inputStream);

            success = parseBooleanResponse(inputStream, "SetShowInDisplayForm");
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return success;
    }

    /**
     * Sets the show in edit form.
     *
     * @param id     the id
     * @param listId the list id
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean setShowInEditForm(String id, String listId) throws ServiceException {
        return setShowInEditForm(id, listId, true);
    }

    /**
     * Sets the show in edit form.
     *
     * @param id     the id
     * @param listId the list id
     * @param show   the show
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean setShowInEditForm(String id, String listId, boolean show) throws ServiceException {
        return setShowInEditFormImplementation(id, listId, show);
    }

    private boolean setShowInEditFormImplementation(String id, String listId, boolean show) throws ServiceException {
        if (id == null) {
            throw new IllegalArgumentException("id");
        }

        if (listId == null) {
            throw new IllegalArgumentException("listId");
        }

        boolean success = false;
        InputStream inputStream = null;

        String requestUrl = "_api/web/lists('" + listId + "')/fields('" + id + "')/setshowineditform(" + Boolean.toString(show).toLowerCase() + ")";

        try {
            inputStream = sendRequest("POST", requestUrl);

            //debug(inputStream);

            success = parseBooleanResponse(inputStream, "SetShowInEditForm");
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return success;
    }

    /**
     * Sets the show in new form.
     *
     * @param id     the id
     * @param listId the list id
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean setShowInNewForm(String id, String listId) throws ServiceException {
        return setShowInNewForm(id, listId, true);
    }

    /**
     * Sets the show in new form.
     *
     * @param id     the id
     * @param listId the list id
     * @param show   the show
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean setShowInNewForm(String id, String listId, boolean show) throws ServiceException {
        return setShowInNewFormImplementation(id, listId, show);
    }

    private boolean setShowInNewFormImplementation(String id, String listId, boolean show) throws ServiceException {
        if (id == null) {
            throw new IllegalArgumentException("id");
        }

        if (listId == null) {
            throw new IllegalArgumentException("listId");
        }

        boolean success = false;
        InputStream inputStream = null;

        String requestUrl = "_api/web/lists('" + listId + "')/fields('" + id + "')/setshowinnewform(" + Boolean.toString(show).toLowerCase() + ")";

        try {
            inputStream = sendRequest("POST", requestUrl);

            //debug(inputStream);

            success = parseBooleanResponse(inputStream, "SetShowInNewForm");
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return success;
    }

    /**
     * Gets the field.
     *
     * @param id     the id
     * @param listId the list id
     * @return the field
     * @throws ServiceException the service exception
     */
    public Field getField(String id, String listId) throws ServiceException {
        return getFieldImplementation(id, listId);
    }

    private Field getFieldImplementation(String id, String listId) throws ServiceException {
        if (id == null) {
            throw new IllegalArgumentException("id");
        }

        if (listId == null) {
            throw new IllegalArgumentException("listId");
        }

        Field field = null;
        InputStream inputStream = null;


        String requestUrl = "_api/web/lists('" + listId + "')/fields('" + id + "')";

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream);

            field = new Field(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return field;
    }

    /**
     * Gets the field by title.
     *
     * @param title  the title
     * @param listId the list id
     * @return the field by title
     * @throws ServiceException the service exception
     */
    public Field getFieldByTitle(String title, String listId) throws ServiceException {
        return getFieldByTitleImplementation(title, listId);
    }

    private Field getFieldByTitleImplementation(String title, String listId) throws ServiceException {
        if (title == null) {
            throw new IllegalArgumentException("title");
        }

        if (listId == null) {
            throw new IllegalArgumentException("listId");
        }

        Field field = null;
        InputStream inputStream = null;

        String requestUrl = "_api/web/lists('" + listId + "')/fields/getbytitle('" + title + "')";

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream);

            field = new Field(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return field;
    }

    /**
     * Gets the field by internal name or title.
     *
     * @param name   the name
     * @param listId the list id
     * @return the field by internal name or title
     * @throws ServiceException the service exception
     */
    public Field getFieldByInternalNameOrTitle(String name, String listId) throws ServiceException {
        return getFieldByInternalNameOrTitleImplementation(name, listId);
    }

    private Field getFieldByInternalNameOrTitleImplementation(String name, String listId) throws ServiceException {
        if (name == null) {
            throw new IllegalArgumentException("name");
        }

        if (listId == null) {
            throw new IllegalArgumentException("listId");
        }

        Field field = null;
        InputStream inputStream = null;

        String requestUrl = "_api/web/lists('" + listId + "')/fields/getbyinternalnameortitle('" + name + "')";

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream);

            field = new Field(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return field;
    }

    /**
     * Creates the field.
     *
     * @param field the field
     * @return the field
     * @throws ServiceException the service exception
     */
    public Field createField(FieldSchemaXml field) throws ServiceException {
        return createFieldImplementation(field);
    }

    private Field createFieldImplementation(FieldSchemaXml field) throws ServiceException {
        if (field == null) {
            throw new IllegalArgumentException("field");
        }

        Field returnValue = null;
        InputStream inputStream = null;

        String requestUrl = "_api/web/fields/createfieldasxml";

        String requestBody = "{ 'parameters': { '__metadata': { 'type': 'SP.XmlSchemaFieldCreationInformation' }, 'SchemaXml': '" + field.toString() + "' } }";

        try {
            inputStream = sendRequest("POST", null, requestUrl, requestBody);

            //debug(inputStream);

            returnValue = new Field(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl, requestBody);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl, requestBody);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl, requestBody);
                }
            }
        }

        return returnValue;
    }

    /**
     * Adds the dependent lookup field.
     *
     * @param displayName          the display name
     * @param primaryLookupFieldId the primary lookup field id
     * @param showField            the show field
     * @param listId               the list id
     * @return the field
     * @throws ServiceException the service exception
     */
    public Field addDependentLookupField(String displayName, String primaryLookupFieldId, String showField, String listId) throws ServiceException {
        return addDependentLookupFieldImplementation(displayName, primaryLookupFieldId, showField, listId);
    }

    private Field addDependentLookupFieldImplementation(String displayName, String primaryLookupFieldId, String showField, String listId) throws ServiceException {
        if (displayName == null) {
            throw new IllegalArgumentException("displayName");
        }

        if (primaryLookupFieldId == null) {
            throw new IllegalArgumentException("primaryLookupFieldId");
        }

        if (showField == null) {
            throw new IllegalArgumentException("showField");
        }

        if (listId == null) {
            throw new IllegalArgumentException("listId");
        }

        Field returnValue = null;
        InputStream inputStream = null;

        String requestUrl = "_api/web/lists('" + listId + "')/fields/adddependentlookupfield(displayname='" + Util.encodeUrl(displayName) + "', primarylookupfieldid='" + Util.encodeUrl(primaryLookupFieldId) + "', showfield='" + Util.encodeUrl(showField) + "')";

        try {
            inputStream = sendRequest("POST", requestUrl);

            //debug(inputStream);

            returnValue = new Field(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return returnValue;
    }

    /**
     * Update field.
     *
     * @param field  the field
     * @param listId the list id
     * @return the field
     * @throws ServiceException the service exception
     */
    public Field updateField(Field field, String listId) throws ServiceException {
        return updateFieldImplementation(field, listId);
    }

    private Field updateFieldImplementation(Field field, String listId) throws ServiceException {
        if (field == null) {
            throw new IllegalArgumentException("field");
        }

        if (field.getId() == null) {
            throw new IllegalArgumentException("Id");
        }

        if (listId == null) {
            throw new IllegalArgumentException("listId");
        }

        Field returnValue = null;
        InputStream inputStream = null;

        String requestUrl = "_api/web/lists('" + listId + "')/fields('" + field.getId() + "')";

        String requestBody = field.toUpdateJSon();

        try {
            inputStream = sendRequest("POST", null, requestUrl, requestBody, "MERGE");

            //debug(inputStream);

            returnValue = new Field(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl, requestBody);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl, requestBody);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl, requestBody);
                }
            }
        }

        return returnValue;
    }

    /**
     * Creates the field.
     *
     * @param field the field
     * @return the field
     * @throws ServiceException the service exception
     */
    public Field createField(Field field) throws ServiceException {
        return createFieldImplementation(field);
    }

    private Field createFieldImplementation(Field field) throws ServiceException {
        if (field == null) {
            throw new IllegalArgumentException("field");
        }

        Field returnValue = null;
        InputStream inputStream = null;

        String requestUrl = "_api/web/fields";

        String requestBody = field.toCreateJSon();

        try {
            inputStream = sendRequest("POST", null, requestUrl, requestBody);

            //debug(inputStream);

            returnValue = new Field(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl, requestBody);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl, requestBody);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl, requestBody);
                }
            }
        }

        return returnValue;
    }

    /**
     * Creates the field.
     *
     * @param field  the field
     * @param listId the list id
     * @return the field
     * @throws ServiceException the service exception
     */
    public Field createField(FieldSchemaXml field, String listId) throws ServiceException {
        return createFieldImplementation(field, listId);
    }

    private Field createFieldImplementation(FieldSchemaXml field, String listId) throws ServiceException {
        if (field == null) {
            throw new IllegalArgumentException("field");
        }

        if (listId == null) {
            throw new IllegalArgumentException("listId");
        }

        Field returnValue = null;
        InputStream inputStream = null;

        String requestUrl = "_api/web/lists('" + listId + "')/fields/createfieldasxml";

        String requestBody = "{ 'parameters': { '__metadata': { 'type': 'SP.XmlSchemaFieldCreationInformation' }, 'SchemaXml': '" + field.toString() + "' } }";

        try {
            inputStream = sendRequest("POST", null, requestUrl, requestBody);

            //debug(inputStream);

            returnValue = new Field(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl, requestBody);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl, requestBody);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl, requestBody);
                }
            }
        }

        return returnValue;
    }

    /**
     * Creates the field.
     *
     * @param field  the field
     * @param listId the list id
     * @return the field
     * @throws ServiceException the service exception
     */
    public Field createField(FieldCreationInfo field, String listId) throws ServiceException {
        return createFieldImplementation(field, listId);
    }

    private Field createFieldImplementation(FieldCreationInfo field, String listId) throws ServiceException {
        if (field == null) {
            throw new IllegalArgumentException("field");
        }

        if (listId == null) {
            throw new IllegalArgumentException("listId");
        }

        Field returnValue = null;
        InputStream inputStream = null;

        String requestUrl = "_api/web/lists('" + listId + "')/fields/addfield";

        String requestBody = field.toCreateJSon();

        try {
            inputStream = sendRequest("POST", null, requestUrl, requestBody);

            //debug(inputStream);

            returnValue = new Field(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl, requestBody);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl, requestBody);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl, requestBody);
                }
            }
        }

        return returnValue;
    }

    /**
     * Creates the field.
     *
     * @param field  the field
     * @param listId the list id
     * @return the field
     * @throws ServiceException the service exception
     */
    public Field createField(Field field, String listId) throws ServiceException {
        return createFieldImplementation(field, listId);
    }

    private Field createFieldImplementation(Field field, String listId) throws ServiceException {
        if (field == null) {
            throw new IllegalArgumentException("field");
        }

        if (listId == null) {
            throw new IllegalArgumentException("listId");
        }

        Field returnValue = null;
        InputStream inputStream = null;

        String requestUrl = "_api/web/lists('" + listId + "')/fields";

        String requestBody = field.toCreateJSon();

        try {
            inputStream = sendRequest("POST", null, requestUrl, requestBody);

            //debug(inputStream);

            returnValue = new Field(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl, requestBody);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl, requestBody);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl, requestBody);
                }
            }
        }

        return returnValue;
    }

    /**
     * Delete field.
     *
     * @param id     the id
     * @param listId the list id
     * @throws ServiceException the service exception
     */
    public void deleteField(String id, String listId) throws ServiceException {
        deleteFieldImplementation(id, listId);
    }

    private void deleteFieldImplementation(String id, String listId) throws ServiceException {
        if (id == null) {
            throw new IllegalArgumentException("id");
        }

        if (listId == null) {
            throw new IllegalArgumentException("listId");
        }

        InputStream inputStream = null;

        String requestUrl = "_api/web/lists('" + listId + "')/fields('" + id + "')";

        try {
            inputStream = sendRequest("POST", null, requestUrl, null, "DELETE");

            //debug(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }
    }

    /**
     * Gets the features.
     *
     * @return the features
     * @throws ServiceException the service exception
     */
    public List<String> getFeatures() throws ServiceException {
        return getFeatures(null);
    }

    /**
     * Gets the features.
     *
     * @param queryOptions the query options
     * @return the features
     * @throws ServiceException the service exception
     */
    public List<String> getFeatures(List<IQueryOption> queryOptions) throws ServiceException {
        return getFeaturesImplementation(queryOptions);
    }

    private List<String> getFeaturesImplementation(List<IQueryOption> queryOptions) throws ServiceException {
        InputStream inputStream = null;
        List<String> ids = new ArrayList<String>();

        String queryOptionsString = Util.queryOptionsToString(queryOptions);

        String requestUrl = "_api/web/Features" + queryOptionsString;

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream);

            ids = parseGetFeatures(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return ids;
    }

    /**
     * Gets the fields.
     *
     * @return the fields
     * @throws ServiceException the service exception
     */
    public List<Field> getFields() throws ServiceException {
        return getFields(null);
    }

    /**
     * Gets the fields.
     *
     * @param queryOptions the query options
     * @return the fields
     * @throws ServiceException the service exception
     */
    public List<Field> getFields(List<IQueryOption> queryOptions) throws ServiceException {
        return getFieldsImplementation(queryOptions);
    }

    private List<Field> getFieldsImplementation(List<IQueryOption> queryOptions) throws ServiceException {
        InputStream inputStream = null;
        List<Field> fields = new ArrayList<Field>();

        String queryOptionsString = Util.queryOptionsToString(queryOptions);

        String requestUrl = "_api/web/Fields" + queryOptionsString;

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream);

            fields = parseGetFields(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return fields;
    }

    /**
     * Gets the list fields.
     *
     * @param listId the list id
     * @return the list fields
     * @throws ServiceException the service exception
     */
    public List<Field> getListFields(String listId) throws ServiceException {
        return getListFields(listId, null);
    }

    /**
     * Gets the list fields.
     *
     * @param listId       the list id
     * @param queryOptions the query options
     * @return the list fields
     * @throws ServiceException the service exception
     */
    public List<Field> getListFields(String listId, List<IQueryOption> queryOptions) throws ServiceException {
        return getListFieldsImplementation(listId, queryOptions);
    }

    private List<Field> getListFieldsImplementation(String listId, List<IQueryOption> queryOptions) throws ServiceException {
        if (listId == null) {
            throw new IllegalArgumentException("listId");
        }

        InputStream inputStream = null;
        List<Field> fields = new ArrayList<Field>();

        String queryOptionsString = Util.queryOptionsToString(queryOptions);

        String requestUrl = "_api/web/lists('" + listId + "')/Fields" + queryOptionsString;

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream);

            fields = parseGetFields(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return fields;
    }

    /**
     * Gets the list forms.
     *
     * @param listId the list id
     * @return the list forms
     * @throws ServiceException the service exception
     */
    public List<Form> getListForms(String listId) throws ServiceException {
        return getListForms(listId, null);
    }

    /**
     * Gets the list forms.
     *
     * @param listId       the list id
     * @param queryOptions the query options
     * @return the list forms
     * @throws ServiceException the service exception
     */
    public List<Form> getListForms(String listId, List<IQueryOption> queryOptions) throws ServiceException {
        return getListFormsImplementation(listId, queryOptions);
    }

    private List<Form> getListFormsImplementation(String listId, List<IQueryOption> queryOptions) throws ServiceException {
        if (listId == null) {
            throw new IllegalArgumentException("listId");
        }

        InputStream inputStream = null;
        List<Form> forms = new ArrayList<Form>();

        String queryOptionsString = Util.queryOptionsToString(queryOptions);

        String requestUrl = "_api/web/lists('" + listId + "')/Forms" + queryOptionsString;

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream);

            forms = parseGetForms(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return forms;
    }

    /**
     * Gets the list form.
     *
     * @param listId the list id
     * @param formId the form id
     * @return the list form
     * @throws ServiceException the service exception
     */
    public Form getListForm(String listId, String formId) throws ServiceException {
        return getListFormImplementation(listId, formId);
    }

    private Form getListFormImplementation(String listId, String formId) throws ServiceException {
        if (listId == null) {
            throw new IllegalArgumentException("listId");
        }

        if (formId == null) {
            throw new IllegalArgumentException("formId");
        }

        Form form = null;
        InputStream inputStream = null;

        String requestUrl = "_api/web/lists('" + listId + "')/Forms('" + formId + "')";

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream);

            form = new Form(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return form;
    }

    /**
     * Gets the list content types.
     *
     * @param listId the list id
     * @return the list content types
     * @throws ServiceException the service exception
     */
    public List<ContentType> getListContentTypes(String listId) throws ServiceException {
        return getListContentTypes(listId, null);
    }

    /**
     * Gets the list content types.
     *
     * @param listId       the list id
     * @param queryOptions the query options
     * @return the list content types
     * @throws ServiceException the service exception
     */
    public List<ContentType> getListContentTypes(String listId, List<IQueryOption> queryOptions) throws ServiceException {
        return getListContentTypesImplementation(listId, queryOptions);
    }

    private List<ContentType> getListContentTypesImplementation(String listId, List<IQueryOption> queryOptions) throws ServiceException {
        if (listId == null) {
            throw new IllegalArgumentException("listId");
        }

        InputStream inputStream = null;
        List<ContentType> contentTypes = new ArrayList<ContentType>();

        String queryOptionsString = Util.queryOptionsToString(queryOptions);

        String requestUrl = "_api/web/lists('" + listId + "')/ContentTypes" + queryOptionsString;

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream)

            contentTypes = parseGetContentTypes(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return contentTypes;
    }

    /**
     * Gets the list default view.
     *
     * @param listId the list id
     * @return the list default view
     * @throws ServiceException the service exception
     */
    public View getListDefaultView(String listId) throws ServiceException {
        return getListDefaultViewImplementation(listId);
    }

    private View getListDefaultViewImplementation(String listId) throws ServiceException {
        if (listId == null) {
            throw new IllegalArgumentException("listId");
        }

        View view = null;
        InputStream inputStream = null;

        String requestUrl = "_api/web/lists('" + listId + "')/DefaultView";

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream);

            view = new View(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return view;
    }

    /**
     * Gets the list event receivers.
     *
     * @param listId the list id
     * @return the list event receivers
     * @throws ServiceException the service exception
     */
    public List<EventReceiver> getListEventReceivers(String listId) throws ServiceException {
        return getListEventReceivers(listId, null);
    }

    /**
     * Gets the list event receivers.
     *
     * @param listId       the list id
     * @param queryOptions the query options
     * @return the list event receivers
     * @throws ServiceException the service exception
     */
    public List<EventReceiver> getListEventReceivers(String listId, List<IQueryOption> queryOptions) throws ServiceException {
        return getListEventReceiversImplementation(listId, queryOptions);
    }

    private List<EventReceiver> getListEventReceiversImplementation(String listId, List<IQueryOption> queryOptions) throws ServiceException {
        if (listId == null) {
            throw new IllegalArgumentException("listId");
        }

        InputStream inputStream = null;
        List<EventReceiver> eventReceivers = new ArrayList<EventReceiver>();

        String queryOptionsString = Util.queryOptionsToString(queryOptions);

        String requestUrl = "_api/web/lists('" + listId + "')/EventReceivers" + queryOptionsString;

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream);

            eventReceivers = parseGetEventReceivers(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return eventReceivers;
    }

    /**
     * Gets the list schema xml.
     *
     * @param listId the list id
     * @return the list schema xml
     * @throws ServiceException the service exception
     */
    public String getListSchemaXml(String listId) throws ServiceException {
        return getListSchemaXmlImplementation(listId);
    }

    private String getListSchemaXmlImplementation(String listId) throws ServiceException {
        if (listId == null) {
            throw new IllegalArgumentException("listId");
        }

        String innerXml = null;
        InputStream inputStream = null;

        String requestUrl = "_api/web/lists('" + listId + "')/SchemaXml";

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream);

            innerXml = parseSchemaXml(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return innerXml;
    }

    /**
     * Gets the list server settings.
     *
     * @param listId the list id
     * @return the list server settings
     * @throws ServiceException the service exception
     */
    public ServerSettings getListServerSettings(String listId) throws ServiceException {
        if (listId == null) {
            throw new IllegalArgumentException("listId");
        }

        InputStream inputStream = null;
        ServerSettings serverSettings = null;

        String requestUrl = "_api/web/lists('" + listId + "')/SchemaXml";

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream);

            String innerXml = parseSchemaXml(inputStream);

            serverSettings = new ServerSettings(innerXml);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return serverSettings;
    }

    /**
     * Gets the list regional settings.
     *
     * @param listId the list id
     * @return the list regional settings
     * @throws ServiceException the service exception
     */
    public RegionalSettings getListRegionalSettings(String listId) throws ServiceException {
        if (listId == null) {
            throw new IllegalArgumentException("listId");
        }

        InputStream inputStream = null;
        RegionalSettings regionalSettings = null;

        String requestUrl = "_api/web/lists('" + listId + "')/SchemaXml";

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream);

            String innerXml = parseSchemaXml(inputStream);

            regionalSettings = new RegionalSettings(innerXml);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return regionalSettings;
    }

    /**
     * Delete list.
     *
     * @param listId the list id
     * @throws ServiceException the service exception
     */
    public void deleteList(String listId) throws ServiceException {
        deleteListImplementation(listId);
    }

    private void deleteListImplementation(String listId) throws ServiceException {
        if (listId == null) {
            throw new IllegalArgumentException("listId");
        }

        InputStream inputStream = null;

        String requestUrl = "_api/web/lists('" + listId + "')";

        try {
            inputStream = sendRequest("POST", null, requestUrl, null, "DELETE", "*");

            //debug(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }
    }

    /**
     * Gets the content type field links.
     *
     * @param contentTypeId the content type id
     * @return the content type field links
     * @throws ServiceException the service exception
     */
    public List<FieldLink> getContentTypeFieldLinks(String contentTypeId) throws ServiceException {
        return getContentTypeFieldLinks(contentTypeId, null);
    }

    /**
     * Gets the content type field links.
     *
     * @param contentTypeId the content type id
     * @param queryOptions  the query options
     * @return the content type field links
     * @throws ServiceException the service exception
     */
    public List<FieldLink> getContentTypeFieldLinks(String contentTypeId, List<IQueryOption> queryOptions) throws ServiceException {
        return getContentTypeFieldLinksImplementation(contentTypeId, queryOptions);
    }

    private List<FieldLink> getContentTypeFieldLinksImplementation(String contentTypeId, List<IQueryOption> queryOptions) throws ServiceException {
        if (contentTypeId == null) {
            throw new IllegalArgumentException("contentTypeId");
        }

        InputStream inputStream = null;
        List<FieldLink> fieldLinks = new ArrayList<FieldLink>();

        String queryOptionsString = Util.queryOptionsToString(queryOptions);

        String requestUrl = "_api/web/ContentTypes('" + Util.encodeUrl(contentTypeId) + "')/FieldLinks" + queryOptionsString;

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream);

            fieldLinks = parseGetFieldLinks(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return fieldLinks;
    }

    /**
     * Gets the content type fields.
     *
     * @param contentTypeId the content type id
     * @return the content type fields
     * @throws ServiceException the service exception
     */
    public List<Field> getContentTypeFields(String contentTypeId) throws ServiceException {
        return getContentTypeFields(contentTypeId, null);
    }

    /**
     * Gets the content type fields.
     *
     * @param contentTypeId the content type id
     * @param queryOptions  the query options
     * @return the content type fields
     * @throws ServiceException the service exception
     */
    public List<Field> getContentTypeFields(String contentTypeId, List<IQueryOption> queryOptions) throws ServiceException {
        return getContentTypeFieldsImplementation(contentTypeId, queryOptions);
    }

    private List<Field> getContentTypeFieldsImplementation(String contentTypeId, List<IQueryOption> queryOptions) throws ServiceException {
        if (contentTypeId == null) {
            throw new IllegalArgumentException("contentTypeId");
        }

        InputStream inputStream = null;
        List<Field> fields = new ArrayList<Field>();

        String queryOptionsString = Util.queryOptionsToString(queryOptions);

        String requestUrl = "_api/web/ContentTypes('" + Util.encodeUrl(contentTypeId) + "')/Fields" + queryOptionsString;

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream);

            fields = parseGetFields(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return fields;
    }

    /**
     * Gets the content type.
     *
     * @param contentTypeId the content type id
     * @return the content type
     * @throws ServiceException the service exception
     */
    public ContentType getContentType(String contentTypeId) throws ServiceException {
        return getContentTypeImplementation(contentTypeId);
    }

    private ContentType getContentTypeImplementation(String contentTypeId) throws ServiceException {
        if (contentTypeId == null) {
            throw new IllegalArgumentException("contentTypeId");
        }

        ContentType contentType = null;
        InputStream inputStream = null;

        String requestUrl = "_api/web/ContentTypes('" + Util.encodeUrl(contentTypeId) + "')";

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream);

            contentType = new ContentType(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return contentType;
    }

    /**
     * Gets the content types.
     *
     * @return the content types
     * @throws ServiceException the service exception
     */
    public List<ContentType> getContentTypes() throws ServiceException {
        return getContentTypes(null);
    }

    /**
     * Gets the content types.
     *
     * @param queryOptions the query options
     * @return the content types
     * @throws ServiceException the service exception
     */
    public List<ContentType> getContentTypes(List<IQueryOption> queryOptions) throws ServiceException {
        return getContentTypesImplementation(queryOptions);
    }

    private List<ContentType> getContentTypesImplementation(List<IQueryOption> queryOptions) throws ServiceException {
        InputStream inputStream = null;
        List<ContentType> contentTypes = new ArrayList<ContentType>();

        String queryOptionsString = Util.queryOptionsToString(queryOptions);

        String requestUrl = "_api/web/ContentTypes" + queryOptionsString;

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream);

            contentTypes = parseGetContentTypes(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return contentTypes;
    }

    /**
     * Gets the associated member group.
     *
     * @return the associated member group
     * @throws ServiceException the service exception
     */
    public Group getAssociatedMemberGroup() throws ServiceException {
        return getAssociatedMemberGroupImplementation();
    }

    private Group getAssociatedMemberGroupImplementation() throws ServiceException {
        Group group = null;
        InputStream inputStream = null;

        String requestUrl = "_api/web/AssociatedMemberGroup";

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream);

            group = new Group(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return group;
    }

    /**
     * Gets the associated owner group.
     *
     * @return the associated owner group
     * @throws ServiceException the service exception
     */
    public Group getAssociatedOwnerGroup() throws ServiceException {
        return getAssociatedOwnerGroupImplementation();
    }

    private Group getAssociatedOwnerGroupImplementation() throws ServiceException {
        Group group = null;
        InputStream inputStream = null;

        String requestUrl = "_api/web/AssociatedOwnerGroup";

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream);

            group = new Group(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return group;
    }

    /**
     * Gets the associated visitor group.
     *
     * @return the associated visitor group
     * @throws ServiceException the service exception
     */
    public Group getAssociatedVisitorGroup() throws ServiceException {
        return getAssociatedVisitorGroupImplementation();
    }

    private Group getAssociatedVisitorGroupImplementation() throws ServiceException {
        Group group = null;
        InputStream inputStream = null;

        String requestUrl = "_api/web/AssociatedVisitorGroup";

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream);

            group = new Group(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return group;
    }

    /**
     * Gets the available content types.
     *
     * @return the available content types
     * @throws ServiceException the service exception
     */
    public List<ContentType> getAvailableContentTypes() throws ServiceException {
        return getAvailableContentTypes(null);
    }

    /**
     * Gets the available content types.
     *
     * @param queryOptions the query options
     * @return the available content types
     * @throws ServiceException the service exception
     */
    public List<ContentType> getAvailableContentTypes(List<IQueryOption> queryOptions) throws ServiceException {
        return getAvailableContentTypesImplementation(queryOptions);
    }

    private List<ContentType> getAvailableContentTypesImplementation(List<IQueryOption> queryOptions) throws ServiceException {
        InputStream inputStream = null;
        List<ContentType> contentTypes = new ArrayList<ContentType>();

        String queryOptionsString = Util.queryOptionsToString(queryOptions);

        String requestUrl = "_api/web/AvailableContentTypes" + queryOptionsString;

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream);

            contentTypes = parseGetContentTypes(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return contentTypes;
    }

    /**
     * Gets the current user.
     *
     * @return the current user
     * @throws ServiceException the service exception
     */
    public User getCurrentUser() throws ServiceException {
        return getCurrentUserImplementation();
    }

    private User getCurrentUserImplementation() throws ServiceException {
        User user = null;
        InputStream inputStream = null;

        String requestUrl = "_api/web/CurrentUser";

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream);

            user = new User(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return user;
    }

    /**
     * Gets the lists.
     *
     * @return the lists
     * @throws ServiceException the service exception
     */
//DA        public List<com.independentsoft.share.List> getLists(String subSite) throws ServiceException {
    public List<com.independentsoft.share.List> getLists(String subSite) throws ServiceException {//DA
        return getLists(subSite, null);
    }

    /**
     * Gets the lists.
     *
     * @param queryOptions the query options
     * @return the lists
     * @throws ServiceException the service exception
     */
//DA    public List<com.independentsoft.share.List> getLists(List<IQueryOption> queryOptions) throws ServiceException {
    public List<com.independentsoft.share.List> getLists(String subSite, List<IQueryOption> queryOptions) throws ServiceException {//DA
        return getListsImplementation(subSite, queryOptions);
    }

    //DA    private List<com.independentsoft.share.List> getListsImplementation(List<IQueryOption> queryOptions) throws ServiceException {
    private List<com.independentsoft.share.List> getListsImplementation(String subSite, List<IQueryOption> queryOptions) throws ServiceException {//DA
        InputStream inputStream = null;
        List<com.independentsoft.share.List> lists = new ArrayList<com.independentsoft.share.List>();

        String queryOptionsString = Util.queryOptionsToString(queryOptions);

        String requestUrl = "_api/web/lists" + queryOptionsString;

        try {
            inputStream = sendRequest("GET", subSite, requestUrl);

            //debug(inputStream);

            lists = parseGetLists(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return lists;
    }

    /**
     * Creates the view.
     *
     * @param listId the list id
     * @param view   the view
     * @return the view
     * @throws ServiceException the service exception
     */
    public View createView(String listId, View view) throws ServiceException {
        return createViewImplementation(listId, view);
    }

    private View createViewImplementation(String listId, View view) throws ServiceException {
        if (listId == null) {
            throw new IllegalArgumentException("listId");
        }

        if (view == null) {
            throw new IllegalArgumentException("view");
        }

        View returnValue = null;
        InputStream inputStream = null;

        String requestUrl = "_api/web/Lists('" + listId + "')/Views";

        String requestBody = view.toCreateJSon();

        try {
            inputStream = sendRequest("POST", null, requestUrl, requestBody);

            //debug(inputStream);

            returnValue = new View(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return returnValue;
    }

    /**
     * Update view.
     *
     * @param listId the list id
     * @param view   the view
     * @throws ServiceException the service exception
     */
    public void updateView(String listId, View view) throws ServiceException {
        updateViewImplementation(listId, view);
    }

    private void updateViewImplementation(String listId, View view) throws ServiceException {
        if (listId == null) {
            throw new IllegalArgumentException("listId");
        }

        if (view == null) {
            throw new IllegalArgumentException("view");
        }

        if (view.getId() == null) {
            throw new IllegalArgumentException("view.Id");
        }

        String requestUrl = "_api/web/Lists('" + listId + "')/Views('" + view.getId() + "')";

        String requestBody = view.toUpdateJSon();

        InputStream inputStream = null;

        try {
            inputStream = sendRequest("POST", null, requestUrl, requestBody, "MERGE");

            //debug(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl, requestBody);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl, requestBody);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl, requestBody);
                }
            }
        }
    }

    /**
     * Gets the views.
     *
     * @param listId the list id
     * @return the views
     * @throws ServiceException the service exception
     */
    public List<View> getViews(String listId) throws ServiceException {
        return getViews(listId, null);
    }

    /**
     * Gets the views.
     *
     * @param listId       the list id
     * @param queryOptions the query options
     * @return the views
     * @throws ServiceException the service exception
     */
    public List<View> getViews(String listId, List<IQueryOption> queryOptions) throws ServiceException {
        return getViewsImplementation(listId, queryOptions);
    }

    private List<View> getViewsImplementation(String listId, List<IQueryOption> queryOptions) throws ServiceException {
        if (listId == null) {
            throw new IllegalArgumentException("listId");
        }

        InputStream inputStream = null;

        List<View> views = new ArrayList<View>();

        String queryOptionsString = Util.queryOptionsToString(queryOptions);

        String requestUrl = "_api/web/Lists('" + listId + "')/Views" + queryOptionsString;

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream);

            views = parseGetViews(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return views;
    }

    /**
     * Gets the view.
     *
     * @param listId the list id
     * @param viewId the view id
     * @return the view
     * @throws ServiceException the service exception
     */
    public View getView(String listId, String viewId) throws ServiceException {
        return getViewImplementation(listId, viewId);
    }

    private View getViewImplementation(String listId, String viewId) throws ServiceException {
        if (listId == null) {
            throw new IllegalArgumentException("listId");
        }

        if (viewId == null) {
            throw new IllegalArgumentException("viewId");
        }

        View view = null;
        InputStream inputStream = null;

        String requestUrl = "_api/web/Lists('" + listId + "')/Views('" + viewId + "')";

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream);

            view = new View(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return view;
    }

    /**
     * Gets the view by title.
     *
     * @param listId the list id
     * @param title  the title
     * @return the view by title
     * @throws ServiceException the service exception
     */
    public View getViewByTitle(String listId, String title) throws ServiceException {
        return getViewByTitleImplementation(listId, title);
    }

    private View getViewByTitleImplementation(String listId, String title) throws ServiceException {
        if (listId == null) {
            throw new IllegalArgumentException("listId");
        }

        if (title == null) {
            throw new IllegalArgumentException("title");
        }

        View view = null;
        InputStream inputStream = null;

        String requestUrl = "_api/web/Lists('" + listId + "')/Views/GetByTitle('" + Util.encodeUrl(title) + "')";

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream);

            view = new View(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return view;
    }

    /**
     * Gets the view html.
     *
     * @param listId the list id
     * @param viewId the view id
     * @return the view html
     * @throws ServiceException the service exception
     */
    public String getViewHtml(String listId, String viewId) throws ServiceException {
        return getViewHtmlImplementation(listId, viewId);
    }

    private String getViewHtmlImplementation(String listId, String viewId) throws ServiceException {
        if (listId == null) {
            throw new IllegalArgumentException("listId");
        }

        if (viewId == null) {
            throw new IllegalArgumentException("viewId");
        }

        String html = null;
        InputStream inputStream = null;

        String requestUrl = "_api/web/Lists('" + listId + "')/Views('" + viewId + "')/RenderAsHtml";

        try {
            inputStream = sendRequest("GET", requestUrl);

            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream), 2048);
            StringBuilder responseText = new StringBuilder();
            String line;

            while ((line = reader.readLine()) != null) {
                responseText.append(line);
            }

            html = responseText.toString();
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return html;
    }

    /**
     * Delete view.
     *
     * @param listId the list id
     * @param viewId the view id
     * @throws ServiceException the service exception
     */
    public void deleteView(String listId, String viewId) throws ServiceException {
        deleteViewImplementation(listId, viewId);
    }

    private void deleteViewImplementation(String listId, String viewId) throws ServiceException {
        if (listId == null) {
            throw new IllegalArgumentException("listId");
        }

        if (viewId == null) {
            throw new IllegalArgumentException("viewId");
        }

        InputStream inputStream = null;

        String requestUrl = "_api/web/Lists('" + listId + "')/Views('" + viewId + "')";

        try {
            inputStream = sendRequest("POST", null, requestUrl, null, "DELETE");

            //debug(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }
    }

    /**
     * Gets the view fields.
     *
     * @param listId the list id
     * @param viewId the view id
     * @return the view fields
     * @throws ServiceException the service exception
     */
    public List<String> getViewFields(String listId, String viewId) throws ServiceException {
        return getViewFields(listId, viewId, null);
    }

    /**
     * Gets the view fields.
     *
     * @param listId       the list id
     * @param viewId       the view id
     * @param queryOptions the query options
     * @return the view fields
     * @throws ServiceException the service exception
     */
    public List<String> getViewFields(String listId, String viewId, List<IQueryOption> queryOptions) throws ServiceException {
        return getViewFieldsImplementation(listId, viewId, queryOptions);
    }

    private List<String> getViewFieldsImplementation(String listId, String viewId, List<IQueryOption> queryOptions) throws ServiceException {
        if (listId == null) {
            throw new IllegalArgumentException("listId");
        }

        if (viewId == null) {
            throw new IllegalArgumentException("viewId");
        }

        InputStream inputStream = null;
        List<String> fields = new ArrayList<String>();

        String queryOptionsString = Util.queryOptionsToString(queryOptions);

        String requestUrl = "_api/web/Lists('" + listId + "')/Views('" + viewId + "')/ViewFields" + queryOptionsString;

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream);

            fields = parseGetViewFields(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return fields;
    }

    /**
     * Gets the view fields schema xml.
     *
     * @param listId the list id
     * @param viewId the view id
     * @return the view fields schema xml
     * @throws ServiceException the service exception
     */
    public String getViewFieldsSchemaXml(String listId, String viewId) throws ServiceException {
        return getViewFieldsSchemaXmlImplementation(listId, viewId);
    }

    private String getViewFieldsSchemaXmlImplementation(String listId, String viewId) throws ServiceException {
        if (listId == null) {
            throw new IllegalArgumentException("listId");
        }

        if (viewId == null) {
            throw new IllegalArgumentException("viewId");
        }

        String schemaXml = null;
        InputStream inputStream = null;

        String requestUrl = "_api/web/Lists('" + listId + "')/Views('" + viewId + "')/ViewFields";

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream);

            schemaXml = parseSchemaXml(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return schemaXml;
    }

    /**
     * Creates the view field.
     *
     * @param listId    the list id
     * @param viewId    the view id
     * @param fieldName the field name
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean createViewField(String listId, String viewId, String fieldName) throws ServiceException {
        return createViewFieldImplementation(listId, viewId, fieldName);
    }

    private boolean createViewFieldImplementation(String listId, String viewId, String fieldName) throws ServiceException {
        if (listId == null) {
            throw new IllegalArgumentException("listId");
        }

        if (viewId == null) {
            throw new IllegalArgumentException("viewId");
        }

        if (fieldName == null) {
            throw new IllegalArgumentException("fieldName");
        }

        boolean success = false;
        InputStream inputStream = null;

        String requestUrl = "_api/web/Lists('" + listId + "')/Views('" + viewId + "')/ViewFields/AddViewField('" + Util.encodeUrl(fieldName) + "')";

        try {
            inputStream = sendRequest("POST", requestUrl);

            //debug(inputStream);

            success = parseBooleanResponse(inputStream, "AddViewField");
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return success;
    }

    /**
     * Delete view field.
     *
     * @param listId    the list id
     * @param viewId    the view id
     * @param fieldName the field name
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean deleteViewField(String listId, String viewId, String fieldName) throws ServiceException {
        return deleteViewFieldImplementation(listId, viewId, fieldName);
    }

    private boolean deleteViewFieldImplementation(String listId, String viewId, String fieldName) throws ServiceException {
        if (listId == null) {
            throw new IllegalArgumentException("listId");
        }

        if (viewId == null) {
            throw new IllegalArgumentException("viewId");
        }

        if (fieldName == null) {
            throw new IllegalArgumentException("fieldName");
        }

        boolean success = false;
        InputStream inputStream = null;

        String requestUrl = "_api/web/Lists('" + listId + "')/Views('" + viewId + "')/ViewFields/RemoveViewField('" + Util.encodeUrl(fieldName) + "')";

        try {
            inputStream = sendRequest("POST", requestUrl);

            //debug(inputStream);

            success = parseBooleanResponse(inputStream, "RemoveViewField");
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return success;
    }

    /**
     * Delete all view fields.
     *
     * @param listId the list id
     * @param viewId the view id
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean deleteAllViewFields(String listId, String viewId) throws ServiceException {
        return deleteAllViewFieldsImplementation(listId, viewId);
    }

    private boolean deleteAllViewFieldsImplementation(String listId, String viewId) throws ServiceException {
        if (listId == null) {
            throw new IllegalArgumentException("listId");
        }

        if (viewId == null) {
            throw new IllegalArgumentException("viewId");
        }

        boolean success = false;
        InputStream inputStream = null;

        String requestUrl = "_api/web/Lists('" + listId + "')/Views('" + viewId + "')/ViewFields/RemoveAllViewFields";

        try {
            inputStream = sendRequest("POST", requestUrl);

            //debug(inputStream);

            success = parseBooleanResponse(inputStream, "RemoveAllViewFields");
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return success;
    }

    /**
     * Move view field.
     *
     * @param listId    the list id
     * @param viewId    the view id
     * @param fieldName the field name
     * @param index     the index
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean moveViewField(String listId, String viewId, String fieldName, int index) throws ServiceException {
        return moveViewFieldImplementation(listId, viewId, fieldName, index);
    }

    private boolean moveViewFieldImplementation(String listId, String viewId, String fieldName, int index) throws ServiceException {
        if (listId == null) {
            throw new IllegalArgumentException("listId");
        }

        if (viewId == null) {
            throw new IllegalArgumentException("viewId");
        }

        if (fieldName == null) {
            throw new IllegalArgumentException("fieldName");
        }

        boolean success = false;
        InputStream inputStream = null;

        String requestUrl = "_api/web/Lists('" + listId + "')/Views('" + viewId + "')/ViewFields/MoveViewFieldTo";

        String requestBody = "{ 'field': '" + Util.encodeJson(fieldName) + "', 'index': " + index + " }";

        try {
            inputStream = sendRequest("POST", null, requestUrl, requestBody);

            //debug(inputStream);

            success = parseBooleanResponse(inputStream, "MoveViewFieldTo");
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return success;
    }


    /**
     * Gets the event receivers.
     *
     * @return the event receivers
     * @throws ServiceException the service exception
     */
    public List<EventReceiver> getEventReceivers() throws ServiceException {
        return getEventReceivers(null);
    }

    /**
     * Gets the event receivers.
     *
     * @param queryOptions the query options
     * @return the event receivers
     * @throws ServiceException the service exception
     */
    public List<EventReceiver> getEventReceivers(List<IQueryOption> queryOptions) throws ServiceException {
        return getEventReceiversImplementation(queryOptions);
    }

    private List<EventReceiver> getEventReceiversImplementation(List<IQueryOption> queryOptions) throws ServiceException {
        InputStream inputStream = null;
        List<EventReceiver> receivers = new ArrayList<EventReceiver>();

        String queryOptionsString = Util.queryOptionsToString(queryOptions);

        String requestUrl = "_api/web/EventReceivers" + queryOptionsString;

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream);

            receivers = parseGetEventReceivers(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return receivers;
    }

    /**
     * Gets the event receiver.
     *
     * @param receiverId the receiver id
     * @return the event receiver
     * @throws ServiceException the service exception
     */
    public EventReceiver getEventReceiver(String receiverId) throws ServiceException {
        return getEventReceiverImplementation(receiverId);
    }

    private EventReceiver getEventReceiverImplementation(String receiverId) throws ServiceException {
        if (receiverId == null) {
            throw new IllegalArgumentException("receiverId");
        }

        EventReceiver receiver = null;
        InputStream inputStream = null;

        String requestUrl = "_api/web/EventReceivers('" + receiverId + "')";

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream);

            receiver = new EventReceiver(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return receiver;
    }

    /**
     * Gets the list templates.
     *
     * @return the list templates
     * @throws ServiceException the service exception
     */
    public List<ListTemplate> getListTemplates() throws ServiceException {
        return getListTemplates(null);
    }

    /**
     * Gets the list templates.
     *
     * @param queryOptions the query options
     * @return the list templates
     * @throws ServiceException the service exception
     */
    public List<ListTemplate> getListTemplates(List<IQueryOption> queryOptions) throws ServiceException {
        return getListTemplatesImplementation(queryOptions);
    }

    private List<ListTemplate> getListTemplatesImplementation(List<IQueryOption> queryOptions) throws ServiceException {
        InputStream inputStream = null;
        List<ListTemplate> listTemplates = new ArrayList<ListTemplate>();

        String queryOptionsString = Util.queryOptionsToString(queryOptions);

        String requestUrl = "_api/web/ListTemplates" + queryOptionsString;

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream);

            listTemplates = parseGetListTemplates(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return listTemplates;
    }

    /**
     * Gets the list template.
     *
     * @param name the name
     * @return the list template
     * @throws ServiceException the service exception
     */
    public ListTemplate getListTemplate(String name) throws ServiceException {
        return getListTemplateImplementation(name);
    }

    private ListTemplate getListTemplateImplementation(String name) throws ServiceException {
        InputStream inputStream = null;
        ListTemplate listTemplate = null;

        String requestUrl = "_api/web/ListTemplates('" + Util.encodeUrl(name) + "')";

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream);

            listTemplate = new ListTemplate(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return listTemplate;
    }

    /**
     * Gets the custom list templates.
     *
     * @return the custom list templates
     * @throws ServiceException the service exception
     */
    public List<ListTemplate> getCustomListTemplates() throws ServiceException {
        return getCustomListTemplates(null);
    }

    /**
     * Gets the custom list templates.
     *
     * @param queryOptions the query options
     * @return the custom list templates
     * @throws ServiceException the service exception
     */
    public List<ListTemplate> getCustomListTemplates(List<IQueryOption> queryOptions) throws ServiceException {
        return getCustomListTemplatesImplementation(queryOptions);
    }

    private List<ListTemplate> getCustomListTemplatesImplementation(List<IQueryOption> queryOptions) throws ServiceException {
        InputStream inputStream = null;
        List<ListTemplate> listTemplates = new ArrayList<ListTemplate>();

        String queryOptionsString = Util.queryOptionsToString(queryOptions);

        String requestUrl = "_api/web/GetCustomListTemplates" + queryOptionsString;

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream);

            listTemplates = parseGetListTemplates(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return listTemplates;
    }

    /**
     * Gets the file stream.
     *
     * @param filePath the file path
     * @return the file stream
     * @throws ServiceException the service exception
     */
//DA     public InputStream getFileStream(String filePath) throws ServiceException {
    public InputStream getFileStream(String subSite, String filePath) throws ServiceException {//DA
        return getFileStreamImplementation(subSite, filePath);
    }

    //DA    private InputStream getFileStreamImplementation(String filePath) throws ServiceException {
    private InputStream getFileStreamImplementation(String subSite, String filePath) throws ServiceException {//DA
        if (filePath == null) {
            throw new IllegalArgumentException("filePath");
        }

        InputStream inputStream = null;

// DA        String requestUrl = "_api/web/GetFileByServerRelativeUrl('" + Util.encodeUrl(filePath) + "')/$value";
        String requestUrl = getFileApiCall + Util.encodeUrl(filePath) + "')/$value"; // DA

        try {
// DA             inputStream = sendRequest("GET", requestUrl, null, null, null, null, false, false);
            inputStream = sendRequest("GET", subSite, requestUrl, null, null, null, null, false, false);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        }

        return inputStream;
    }

    /**
     * Gets the input stream.
     * <p>
     * Please use {@link Service#getFileStream(String, String)}
     *
     * @param url the url
     * @return the input stream
     * @throws ServiceException the service exception
     */
    @Deprecated // DA
//DA    public InputStream getInputStream(String url) throws ServiceException {
    public InputStream getInputStream(String subSite, String url) throws ServiceException {//DA
        return getInputStreamImplementation(subSite, url);
    }

    //DA    private InputStream getInputStreamImplementation(String url) throws ServiceException {
    private InputStream getInputStreamImplementation(String subSite, String url) throws ServiceException {//DA
        if (url == null) {
            throw new IllegalArgumentException("url");
        }

        InputStream inputStream = null;

        String requestUrl = Util.encodeUrlInputStream(url);

        try {
            inputStream = sendRequest("GET", subSite, requestUrl, null, null, null, null, false, false);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        }

        return inputStream;
    }


    /**
     * Gets the file content.
     *
     * @param filePath the file path
     * @return the file content
     * @throws ServiceException the service exception
     */
    public byte[] getFileContent(String filePath) throws ServiceException {
        return getFileContentImplementation(filePath);
    }

    private byte[] getFileContentImplementation(String filePath) throws ServiceException {
        if (filePath == null) {
            throw new IllegalArgumentException("filePath");
        }

        byte[] buffer = null;
        InputStream inputStream = null;

// DA        String requestUrl = "_api/web/GetFileByServerRelativeUrl('" + Util.encodeUrl(filePath) + "')/$value";
        String requestUrl = getFileApiCall + Util.encodeUrl(filePath) + "')/$value"; // DA

        try {
            inputStream = sendRequest("GET", requestUrl);

            buffer = getInputStreamBuffer(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return buffer;
    }

    /**
     * Delete file.
     *
     * @param filePath the file path
     * @throws ServiceException the service exception
     */
    public void deleteFile(String filePath) throws ServiceException {
        deleteFileImplementation(filePath);
    }

    private void deleteFileImplementation(String filePath) throws ServiceException {
        if (filePath == null) {
            throw new IllegalArgumentException("filePath");
        }

// DA        String requestUrl = "_api/web/GetFileByServerRelativeUrl('" + Util.encodeUrl(filePath) + "')";
        String requestUrl = getFileApiCall + Util.encodeUrl(filePath) + "')"; // DA

        InputStream inputStream = null;

        try {
            inputStream = sendRequest("POST", null, requestUrl, null, "DELETE");

            //debug(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }
    }

    /**
     * Creates the file.
     *
     * @param filePath the file path
     * @param buffer   the buffer
     * @return the file
     * @throws ServiceException the service exception
     */

//DA    public File createFile(String filePath, byte[] buffer) throws ServiceException {
    public File createFile(String subSite, String filePath, byte[] buffer) throws ServiceException {//DA
        return createFile(subSite, filePath, buffer, false);
    }

    /**
     * Creates the file.
     *
     * @param filePath  the file path
     * @param buffer    the buffer
     * @param overwrite the overwrite
     * @return the file
     * @throws ServiceException the service exception
     */
//DA    public File createFile(String filePath, byte[] buffer, boolean overwrite) throws ServiceException {
    public File createFile(String subSite, String filePath, byte[] buffer, boolean overwrite) throws ServiceException {//DA
        ByteArrayInputStream stream = new ByteArrayInputStream(buffer);

        return createFile(subSite, filePath, stream, overwrite);
    }

    /**
     * Creates the file.
     *
     * @param filePath the file path
     * @param stream   the stream
     * @return the file
     * @throws ServiceException the service exception
     */
//DA    public File createFile(String filePath, InputStream stream) throws ServiceException {
    public File createFile(String subSite, String filePath, InputStream stream) throws ServiceException {//DA
        return createFile(subSite, filePath, stream, false);
    }

    /**
     * Creates the file.
     *
     * @param filePath  the file path
     * @param stream    the stream
     * @param overwrite the overwrite
     * @return the file
     * @throws ServiceException the service exception
     */
//DA    public File createFile(String filePath, InputStream stream, boolean overwrite) throws ServiceException {
    public File createFile(String subSite, String filePath, InputStream stream, boolean overwrite) throws ServiceException {//DA
        return createFileImplementation(subSite, filePath, stream, overwrite);
    }

    //DA    private File createFileImplementation(String filePath, InputStream stream, boolean overwrite) throws ServiceException {
    private File createFileImplementation(String subSite, String filePath, InputStream stream, boolean overwrite) throws ServiceException {//DA
        if (filePath == null) {
            throw new IllegalArgumentException("filePath");
        }

        if (stream == null) {
            throw new IllegalArgumentException("stream");
        }

        File file = null;
        InputStream inputStream = null;

        int index = filePath.lastIndexOf("/");

        String folderPath = index > 0 ? filePath.substring(0, index) : "/";

        String fileName = filePath.substring(index + 1);

// DA        String requestUrl = "_api/web/GetFolderByServerRelativeUrl('" + Util.encodeUrl(folderPath) + "')/files/Add(overwrite=" + Boolean.toString(overwrite).toLowerCase() + ",url='" + Util.encodeUrl(fileName) + "')";
        String requestUrl = getFolderApiCall + Util.encodeUrl(folderPath) + "')/files/Add(overwrite=" + Boolean.toString(overwrite).toLowerCase() + ",url='" + Util.encodeUrl(fileName) + "')"; // DA

        try {
//DA             inputStream = sendRequest("POST", requestUrl, null, null, null, stream, true, false);
            inputStream = sendRequest("POST", subSite, requestUrl, null, null, null, stream, true, false); // DA

            //debug(inputStream);

            file = new File(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return file;
    }

    /**
     * Creates the template file.
     *
     * @param filePath the file path
     * @return the file
     * @throws ServiceException the service exception
     */
    public File createTemplateFile(String filePath) throws ServiceException {
        return createTemplateFile(filePath, TemplateFileType.NONE);
    }

    /**
     * Creates the template file.
     *
     * @param filePath the file path
     * @param type     the type
     * @return the file
     * @throws ServiceException the service exception
     */
    public File createTemplateFile(String filePath, TemplateFileType type) throws ServiceException {
        return createTemplateFileImplementation(filePath, type);
    }

    private File createTemplateFileImplementation(String filePath, TemplateFileType type) throws ServiceException {
        if (filePath == null) {
            throw new IllegalArgumentException("filePath");
        }

        File file = null;
        InputStream inputStream = null;

        int index = filePath.lastIndexOf("/");

        String folderPath = index > 0 ? filePath.substring(0, index) : "/";

        String templateFileTypeString = type != TemplateFileType.NONE ? ",templatefiletype=" + EnumUtil.parseTemplateFileType(type) : "";

// DA        String requestUrl = "_api/web/GetFolderByServerRelativeUrl('" + Util.encodeUrl(folderPath) + "')/files/AddTemplateFile(urloffile='" + Util.encodeUrl(filePath) + "'" + templateFileTypeString + ")";
        String requestUrl = getFolderApiCall + Util.encodeUrl(folderPath) + "')/files/AddTemplateFile(urloffile='" + Util.encodeUrl(filePath) + "'" + templateFileTypeString + ")"; // DA

        try {
            inputStream = sendRequest("POST", requestUrl);

            //debug(inputStream);

            file = new File(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return file;
    }

    /**
     * Update file content.
     *
     * @param filePath the file path
     * @param buffer   the buffer
     * @throws ServiceException the service exception
     */
//DA    public void updateFileContent(String subSite, String filePath, byte[] buffer) throws ServiceException {
    public void updateFileContent(String subSite, String filePath, byte[] buffer) throws ServiceException {//DA
        ByteArrayInputStream stream = new ByteArrayInputStream(buffer);

        updateFileContent(subSite, filePath, stream);
    }

    /**
     * Update file content.
     *
     * @param filePath the file path
     * @param stream   the stream
     * @throws ServiceException the service exception
     */
//DA    public void updateFileContent(String filePath, InputStream stream) throws ServiceException {
    public void updateFileContent(String subSite, String filePath, InputStream stream) throws ServiceException {//DA
        updateFileContentImplementation(subSite, filePath, stream);
    }

    //DA    private void updateFileContentImplementation(String filePath, InputStream stream) throws ServiceException {
    private void updateFileContentImplementation(String subSite, String filePath, InputStream stream) throws ServiceException {
        if (filePath == null) {
            throw new IllegalArgumentException("filePath");
        }

        if (stream == null) {
            throw new IllegalArgumentException("stream");
        }

        InputStream inputStream = null;

// DA        String requestUrl = "_api/web/GetFileByServerRelativeUrl('" + Util.encodeUrl(filePath) + "')/$value";
        String requestUrl = getFileApiCall + Util.encodeUrl(filePath) + "')/$value"; // DA

        try {
// DA inputStream = sendRequest("POST", requestUrl, null, "PUT", null, stream, true, false);
            inputStream = sendRequest("POST", subSite, requestUrl, null, "PUT", null, stream, true, false); //DA

            //debug(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }
    }

    /**
     * Gets the file.
     *
     * @param filePath the file path
     * @return the file
     * @throws ServiceException the service exception
     */
//DA    public File getFile(String filePath) throws ServiceException {
    public File getFile(String subSite, String filePath) throws ServiceException {//DA
        return getFileImplementation(subSite, filePath);
    }

    //DA    private File getFileImplementation(String subSite, String filePath) throws ServiceException {
    private File getFileImplementation(String subSite, String filePath) throws ServiceException {//DA
        if (filePath == null) {
            throw new IllegalArgumentException("filePath");
        }

        File file = null;
        InputStream inputStream = null;

// DA        String requestUrl = "_api/web/GetFileByServerRelativeUrl('" + Util.encodeUrl(filePath) + "')";
        String requestUrl = getFileApiCall + Util.encodeUrl(filePath) + "')"; // DA

        try {
            inputStream = sendRequest("GET", subSite, requestUrl);

            //debug(inputStream);

            file = new File(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return file;
    }

    /**
     * Recycle file.
     *
     * @param filePath the file path
     * @return the string
     * @throws ServiceException the service exception
     */
    public String recycleFile(String filePath) throws ServiceException {
        return recycleFileImplementation(filePath);
    }

    private String recycleFileImplementation(String filePath) throws ServiceException {
        if (filePath == null) {
            throw new IllegalArgumentException("filePath");
        }

        String id = null;
        InputStream inputStream = null;

// DA        String requestUrl = "_api/web/GetFileByServerRelativeUrl('" + Util.encodeUrl(filePath) + "')/recycle";
        String requestUrl = getFileApiCall + Util.encodeUrl(filePath) + "')/recycle";  // DA

        try {
            inputStream = sendRequest("POST", requestUrl);

            //debug(inputStream);

            id = parseRecycle(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return id;
    }

    /**
     * Recycle list.
     *
     * @param listId the list id
     * @return the string
     * @throws ServiceException the service exception
     */
    public String recycleList(String listId) throws ServiceException {
        return recycleListImplementation(listId);
    }

    private String recycleListImplementation(String listId) throws ServiceException {
        if (listId == null) {
            throw new IllegalArgumentException("listId");
        }

        String id = null;
        InputStream inputStream = null;

        String requestUrl = "_api/web/lists('" + listId + "')/Recycle";

        try {
            inputStream = sendRequest("POST", requestUrl);

            //debug(inputStream);

            id = parseRecycle(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return id;
    }

    /**
     * Recycle list item.
     *
     * @param listId the list id
     * @param itemId the item id
     * @return the string
     * @throws ServiceException the service exception
     */
    public String recycleListItem(String listId, int itemId) throws ServiceException {
        return recycleListImplementation(listId, itemId);
    }

    private String recycleListImplementation(String listId, int itemId) throws ServiceException {
        if (listId == null) {
            throw new IllegalArgumentException("listId");
        }

        if (itemId < 0) {
            throw new IllegalArgumentException("The parameter itemId must be non-negative.");
        }

        String id = null;
        InputStream inputStream = null;

        String requestUrl = "_api/web/lists('" + listId + "')/items(" + itemId + ")/Recycle";

        try {
            inputStream = sendRequest("POST", requestUrl);

            //debug(inputStream);

            id = parseRecycle(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return id;
    }

    /**
     * Unpublish.
     *
     * @param filePath the file path
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean unpublish(String filePath) throws ServiceException {
        return unpublish(filePath, null);
    }

    /**
     * Unpublish.
     *
     * @param filePath the file path
     * @param comment  the comment
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean unpublish(String filePath, String comment) throws ServiceException {
        return unpublishImplementation(filePath, comment);
    }

    private boolean unpublishImplementation(String filePath, String comment) throws ServiceException {
        if (filePath == null) {
            throw new IllegalArgumentException("filePath");
        }

        boolean success = false;
        InputStream inputStream = null;

// DA        String requestUrl = "_api/web/GetFileByServerRelativeUrl('" + Util.encodeUrl(filePath) + "')/Unpublish";
        String requestUrl = getFileApiCall + Util.encodeUrl(filePath) + "')/Unpublish"; // DA

        if (comment != null && comment.length() > 0) {
            requestUrl += "('" + Util.encodeUrl(comment) + "')";
        }

        try {
            inputStream = sendRequest("POST", requestUrl);

            //debug(inputStream);

            success = parseBooleanResponse(inputStream, "UnPublish");
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return success;
    }

    /**
     * Publish.
     *
     * @param filePath the file path
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean publish(String filePath) throws ServiceException {
        return publish(filePath, null);
    }

    /**
     * Publish.
     *
     * @param filePath the file path
     * @param comment  the comment
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean publish(String filePath, String comment) throws ServiceException {
        return publishImplementation(filePath, comment);
    }

    private boolean publishImplementation(String filePath, String comment) throws ServiceException {
        if (filePath == null) {
            throw new IllegalArgumentException("filePath");
        }

        boolean success = false;
        InputStream inputStream = null;

// DA        String requestUrl = "_api/web/GetFileByServerRelativeUrl('" + Util.encodeUrl(filePath) + "')/Publish";
        String requestUrl = getFileApiCall + Util.encodeUrl(filePath) + "')/Publish"; // DA

        if (comment != null && comment.length() > 0) {
            requestUrl += "('" + Util.encodeUrl(comment) + "')";
        }

        try {
            inputStream = sendRequest("POST", requestUrl);

            //debug(inputStream);

            success = parseBooleanResponse(inputStream, "Publish");
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return success;
    }

    /**
     * Approve.
     *
     * @param filePath the file path
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean approve(String filePath) throws ServiceException {
        return approve(filePath, null);
    }

    /**
     * Approve.
     *
     * @param filePath the file path
     * @param comment  the comment
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean approve(String filePath, String comment) throws ServiceException {
        return approveImplementation(filePath, comment);
    }

    private boolean approveImplementation(String filePath, String comment) throws ServiceException {
        if (filePath == null) {
            throw new IllegalArgumentException("filePath");
        }

        boolean success = false;
        InputStream inputStream = null;

// DA        String requestUrl = "_api/web/GetFileByServerRelativeUrl('" + Util.encodeUrl(filePath) + "')/Approve";
        String requestUrl = getFileApiCall + Util.encodeUrl(filePath) + "')/Approve"; // DA

        if (comment != null && comment.length() > 0) {
            requestUrl += "('" + Util.encodeUrl(comment) + "')";
        }

        try {
            inputStream = sendRequest("POST", requestUrl);

            //debug(inputStream);

            success = parseBooleanResponse(inputStream, "Approve");
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return success;
    }

    /**
     * Deny.
     *
     * @param filePath the file path
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean deny(String filePath) throws ServiceException {
        return deny(filePath, null);
    }

    /**
     * Deny.
     *
     * @param filePath the file path
     * @param comment  the comment
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean deny(String filePath, String comment) throws ServiceException {
        return denyImplementation(filePath, comment);
    }

    private boolean denyImplementation(String filePath, String comment) throws ServiceException {
        if (filePath == null) {
            throw new IllegalArgumentException("filePath");
        }

        boolean success = false;
        InputStream inputStream = null;

// DA        String requestUrl = "_api/web/GetFileByServerRelativeUrl('" + Util.encodeUrl(filePath) + "')/Deny";
        String requestUrl = getFileApiCall + Util.encodeUrl(filePath) + "')/Deny"; // DA

        if (comment != null && comment.length() > 0) {
            requestUrl += "('" + Util.encodeUrl(comment) + "')";
        }

        try {
            inputStream = sendRequest("POST", requestUrl);

            //debug(inputStream);

            success = parseBooleanResponse(inputStream, "Deny");
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return success;
    }

    /**
     * Undo check out.
     *
     * @param filePath the file path
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean undoCheckOut(String filePath) throws ServiceException {
        return undoCheckOutImplementation(filePath);
    }

    private boolean undoCheckOutImplementation(String filePath) throws ServiceException {
        if (filePath == null) {
            throw new IllegalArgumentException("filePath");
        }

        boolean success = false;
        InputStream inputStream = null;

// DA        String requestUrl = "_api/web/GetFileByServerRelativeUrl('" + Util.encodeUrl(filePath) + "')/UndoCheckOut()";
        String requestUrl = getFileApiCall + Util.encodeUrl(filePath) + "')/UndoCheckOut()"; // DA

        try {
            inputStream = sendRequest("POST", requestUrl);

            //debug(inputStream);

            success = parseBooleanResponse(inputStream, "UndoCheckOut");
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return success;
    }

    /**
     * Check out.
     *
     * @param filePath the file path
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean checkOut(String filePath) throws ServiceException {
        return checkOutImplementation(filePath);
    }

    private boolean checkOutImplementation(String filePath) throws ServiceException {
        if (filePath == null) {
            throw new IllegalArgumentException("filePath");
        }

        boolean success = false;
        InputStream inputStream = null;

// DA        String requestUrl = "_api/web/GetFileByServerRelativeUrl('" + Util.encodeUrl(filePath) + "')/Checkout()";
        String requestUrl = getFileApiCall + Util.encodeUrl(filePath) + "')/Checkout()"; // DA

        try {
            inputStream = sendRequest("POST", requestUrl);

            //debug(inputStream);

            success = parseBooleanResponse(inputStream, "CheckOut");
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return success;
    }

    /**
     * Check in.
     *
     * @param filePath the file path
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean checkIn(String filePath) throws ServiceException {
        return checkIn(filePath, CheckInType.MINOR);
    }

    /**
     * Check in.
     *
     * @param filePath the file path
     * @param comment  the comment
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean checkIn(String filePath, String comment) throws ServiceException {
        return checkIn(filePath, CheckInType.MINOR, comment);
    }

    /**
     * Check in.
     *
     * @param filePath the file path
     * @param type     the type
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean checkIn(String filePath, CheckInType type) throws ServiceException {
        return checkIn(filePath, type, null);
    }

    /**
     * Check in.
     *
     * @param filePath the file path
     * @param type     the type
     * @param comment  the comment
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean checkIn(String filePath, CheckInType type, String comment) throws ServiceException {
        return checkInImplementation(filePath, type, comment);
    }

    private boolean checkInImplementation(String filePath, CheckInType type, String comment) throws ServiceException {
        if (filePath == null) {
            throw new IllegalArgumentException("filePath");
        }

        boolean success = false;
        InputStream inputStream = null;

// DA        String requestUrl = "_api/web/GetFileByServerRelativeUrl('" + Util.encodeUrl(filePath) + "')/Checkin";
        String requestUrl = getFileApiCall + Util.encodeUrl(filePath) + "')/Checkin"; // DA

        String encodedComment = comment != null ? "comment='" + Util.encodeUrl(comment) + "'," : "";

        requestUrl += "(" + encodedComment + "checkintype=" + EnumUtil.parseCheckInType(type) + ")";

        try {
            inputStream = sendRequest("POST", requestUrl);

            //debug(inputStream);

            success = parseBooleanResponse(inputStream, "CheckIn");
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return success;
    }

    /**
     * Copy file.
     *
     * @param sourceFilePath      the source file path
     * @param destinationFilePath the destination file path
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean copyFile(String sourceFilePath, String destinationFilePath) throws ServiceException {
        return copyFile(sourceFilePath, destinationFilePath, false);
    }

    /**
     * Copy file.
     *
     * @param sourceFilePath      the source file path
     * @param destinationFilePath the destination file path
     * @param overwrite           the overwrite
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean copyFile(String sourceFilePath, String destinationFilePath, boolean overwrite) throws ServiceException {
        return copyFileImplementation(sourceFilePath, destinationFilePath, overwrite);
    }

    private boolean copyFileImplementation(String sourceFilePath, String destinationFilePath, boolean overwrite) throws ServiceException {
        if (sourceFilePath == null) {
            throw new IllegalArgumentException("sourceFilePath");
        }

        if (destinationFilePath == null) {
            throw new IllegalArgumentException("destinationFilePath");
        }

        boolean success = false;
        InputStream inputStream = null;

// DA        String requestUrl = "_api/web/GetFileByServerRelativeUrl('" + Util.encodeUrl(sourceFilePath) + "')/CopyTo";
        String requestUrl = getFileApiCall + Util.encodeUrl(sourceFilePath) + "')/CopyTo"; // DA

        requestUrl += "(strnewurl='" + Util.encodeUrl(destinationFilePath) + "',boverwrite=" + Boolean.toString(overwrite).toLowerCase() + ")";

        try {
            inputStream = sendRequest("POST", requestUrl);

            //debug(inputStream);

            success = parseBooleanResponse(inputStream, "CopyTo");
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return success;
    }

    /**
     * Move file.
     *
     * @param sourceFilePath      the source file path
     * @param destinationFilePath the destination file path
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean moveFile(String sourceFilePath, String destinationFilePath) throws ServiceException {
        return moveFile(sourceFilePath, destinationFilePath, MoveOperation.NONE);
    }

    /**
     * Move file.
     *
     * @param sourceFilePath      the source file path
     * @param destinationFilePath the destination file path
     * @param operation           the operation
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean moveFile(String sourceFilePath, String destinationFilePath, MoveOperation operation) throws ServiceException {
        return moveFileImplementation(sourceFilePath, destinationFilePath, operation);
    }

    private boolean moveFileImplementation(String sourceFilePath, String destinationFilePath, MoveOperation operation) throws ServiceException {
        if (sourceFilePath == null) {
            throw new IllegalArgumentException("sourceFilePath");
        }

        if (destinationFilePath == null) {
            throw new IllegalArgumentException("destinationFilePath");
        }

        boolean success = false;
        InputStream inputStream = null;

// DA        String requestUrl = "_api/web/GetFileByServerRelativeUrl('" + Util.encodeUrl(sourceFilePath) + "')/MoveTo";
        String requestUrl = getFileApiCall + Util.encodeUrl(sourceFilePath) + "')/MoveTo"; // DA

        String moveOperationString = operation != MoveOperation.NONE ? ",flags=" + EnumUtil.parseMoveOperation(operation) : "";

        requestUrl += "(newurl='" + Util.encodeUrl(destinationFilePath) + "'" + moveOperationString + ")";

        try {
            inputStream = sendRequest("POST", requestUrl);

            //debug(inputStream);

            success = parseBooleanResponse(inputStream, "MoveTo");
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return success;
    }

    /**
     * Gets the limited web part manager.
     *
     * @param filePath the file path
     * @return the limited web part manager
     * @throws ServiceException the service exception
     */
    public LimitedWebPartManager getLimitedWebPartManager(String filePath) throws ServiceException {
        return getLimitedWebPartManager(filePath, PersonalizationScope.USER);
    }

    /**
     * Gets the limited web part manager.
     *
     * @param filePath the file path
     * @param scope    the scope
     * @return the limited web part manager
     * @throws ServiceException the service exception
     */
    public LimitedWebPartManager getLimitedWebPartManager(String filePath, PersonalizationScope scope) throws ServiceException {
        return getLimitedWebPartManagerImplementation(filePath, scope);
    }

    private LimitedWebPartManager getLimitedWebPartManagerImplementation(String filePath, PersonalizationScope scope) throws ServiceException {
        if (filePath == null) {
            throw new IllegalArgumentException("filePath");
        }

        InputStream inputStream = null;
        LimitedWebPartManager limitedWebPartManager = null;

// DA        String requestUrl = "_api/web/GetFileByServerRelativeUrl('" + Util.encodeUrl(filePath) + "')/GetLimitedWebPartManager(scope=" + EnumUtil.parsePersonalizationScope(scope) + ")";
        String requestUrl = getFileApiCall + Util.encodeUrl(filePath) + "')/GetLimitedWebPartManager(scope=" + EnumUtil.parsePersonalizationScope(scope) + ")"; // DA

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream);

            limitedWebPartManager = new LimitedWebPartManager(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return limitedWebPartManager;
    }

    /**
     * Gets the file author.
     *
     * @param filePath the file path
     * @return the file author
     * @throws ServiceException the service exception
     */
//DA public User getFileAuthor(String filePath) throws ServiceException {
    public User getFileAuthor(String subSite, String filePath) throws ServiceException {//DA
        return getFileAuthorImplementation(subSite, filePath);
    }

    //DA    private User getFileAuthorImplementation(String filePath) throws ServiceException {
    private User getFileAuthorImplementation(String subSite, String filePath) throws ServiceException {//DA
        if (filePath == null) {
            throw new IllegalArgumentException("filePath");
        }

        User user = null;
        InputStream inputStream = null;

// DA        String requestUrl = "_api/web/GetFileByServerRelativeUrl('" + Util.encodeUrl(filePath) + "')/Author";
        String requestUrl = getFileApiCall + Util.encodeUrl(filePath) + "')/Author"; // DA

        try {
            inputStream = sendRequest("GET", subSite, requestUrl);

            //debug(inputStream);

            user = new User(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return user;
    }

    /**
     * Checked out by user.
     *
     * @param filePath the file path
     * @return the user
     * @throws ServiceException the service exception
     */
    public User checkedOutByUser(String filePath) throws ServiceException {
        return checkedOutByUserImplementation(filePath);
    }

    private User checkedOutByUserImplementation(String filePath) throws ServiceException {
        if (filePath == null) {
            throw new IllegalArgumentException("filePath");
        }

        User user = null;
        InputStream inputStream = null;

// DA        String requestUrl = "_api/web/GetFileByServerRelativeUrl('" + Util.encodeUrl(filePath) + "')/CheckedOutByUser";
        String requestUrl = getFileApiCall + Util.encodeUrl(filePath) + "')/CheckedOutByUser"; // DA

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream);

            user = new User(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return user;
    }

    /**
     * Gets the modified by user.
     *
     * @param filePath the file path
     * @return the modified by user
     * @throws ServiceException the service exception
     */
    public User getModifiedByUser(String filePath) throws ServiceException {
        return getModifiedByUserImplementation(filePath);
    }

    private User getModifiedByUserImplementation(String filePath) throws ServiceException {
        if (filePath == null) {
            throw new IllegalArgumentException("filePath");
        }

        User user = null;
        InputStream inputStream = null;

// DA        String requestUrl = "_api/web/GetFileByServerRelativeUrl('" + Util.encodeUrl(filePath) + "')/ModifiedBy";
        String requestUrl = getFileApiCall + Util.encodeUrl(filePath) + "')/ModifiedBy"; //DA

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream);

            user = new User(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return user;
    }

    /**
     * Gets the locked by user.
     *
     * @param filePath the file path
     * @return the locked by user
     * @throws ServiceException the service exception
     */
    public User getLockedByUser(String filePath) throws ServiceException {
        return getLockedByUserImplementation(filePath);
    }

    private User getLockedByUserImplementation(String filePath) throws ServiceException {
        if (filePath == null) {
            throw new IllegalArgumentException("filePath");
        }

        User user = null;
        InputStream inputStream = null;

// DA        String requestUrl = "_api/web/GetFileByServerRelativeUrl('" + Util.encodeUrl(filePath) + "')/LockedByUser";
        String requestUrl = getFileApiCall + Util.encodeUrl(filePath) + "')/LockedByUser"; // DA

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream);

            user = new User(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return user;
    }

    /**
     * Gets the created by user.
     *
     * @param filePath  the file path
     * @param versionId the version id
     * @return the created by user
     * @throws ServiceException the service exception
     */
    public User getCreatedByUser(String filePath, int versionId) throws ServiceException {
        return getCreatedByUserImplementation(filePath, versionId);
    }

    private User getCreatedByUserImplementation(String filePath, int versionId) throws ServiceException {
        if (filePath == null) {
            throw new IllegalArgumentException("filePath");
        }

        User user = null;
        InputStream inputStream = null;

// DA        String requestUrl = "_api/web/GetFileByServerRelativeUrl('" + Util.encodeUrl(filePath) + "')/Versions(" + Integer.toString(versionId) + ")/CreatedBy";
        String requestUrl = getFileApiCall + Util.encodeUrl(filePath) + "')/Versions(" + Integer.toString(versionId) + ")/CreatedBy"; // DA

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream);

            user = new User(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return user;
    }

    /**
     * Gets the file versions.
     *
     * @param filePath the file path
     * @return the file versions
     * @throws ServiceException the service exception
     */
    public List<FileVersion> getFileVersions(String filePath) throws ServiceException {
        return getFileVersions(filePath, null);
    }

    /**
     * Gets the file versions.
     *
     * @param filePath     the file path
     * @param queryOptions the query options
     * @return the file versions
     * @throws ServiceException the service exception
     */
    public List<FileVersion> getFileVersions(String filePath, List<IQueryOption> queryOptions) throws ServiceException {
        return getFileVersionsImplementation(filePath, queryOptions);
    }

    private List<FileVersion> getFileVersionsImplementation(String filePath, List<IQueryOption> queryOptions) throws ServiceException {
        if (filePath == null) {
            throw new IllegalArgumentException("filePath");
        }

        InputStream inputStream = null;
        List<FileVersion> fileVersions = new ArrayList<FileVersion>();

        String queryOptionsString = Util.queryOptionsToString(queryOptions);

// DA        String requestUrl = "_api/web/GetFileByServerRelativeUrl('" + Util.encodeUrl(filePath) + "')/Versions" + queryOptionsString;
        String requestUrl = getFileApiCall + Util.encodeUrl(filePath) + "')/Versions" + queryOptionsString; //DA

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream);

            fileVersions = parseGetFileVersions(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return fileVersions;
    }

    /**
     * Gets the file version.
     *
     * @param filePath  the file path
     * @param versionId the version id
     * @return the file version
     * @throws ServiceException the service exception
     */
    public FileVersion getFileVersion(String filePath, int versionId) throws ServiceException {
        return getFileVersionImplementation(filePath, versionId);
    }

    private FileVersion getFileVersionImplementation(String filePath, int versionId) throws ServiceException {
        if (filePath == null) {
            throw new IllegalArgumentException("filePath");
        }

        FileVersion fileVersion = null;
        InputStream inputStream = null;

// DA        String requestUrl = "_api/web/GetFileByServerRelativeUrl('" + Util.encodeUrl(filePath) + "')/Versions(" + Integer.toString(versionId) + ")";
        String requestUrl = getFileApiCall + Util.encodeUrl(filePath) + "')/Versions(" + Integer.toString(versionId) + ")"; // DA

        try {
            inputStream = sendRequest("GET", requestUrl);

            //debug(inputStream);

            fileVersion = parseGetFileVersion(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return fileVersion;
    }

    /**
     * Delete file version.
     *
     * @param filePath  the file path
     * @param versionId the version id
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean deleteFileVersion(String filePath, int versionId) throws ServiceException {
        return deleteFileVersionImplementation(filePath, versionId);
    }

    private boolean deleteFileVersionImplementation(String filePath, int versionId) throws ServiceException {
        if (filePath == null) {
            throw new IllegalArgumentException("filePath");
        }

        boolean success = false;
        InputStream inputStream = null;

// DA        String requestUrl = "_api/web/GetFileByServerRelativeUrl('" + Util.encodeUrl(filePath) + "')/Versions/DeleteById(vid=" + Integer.toString(versionId) + ")";
        String requestUrl = getFileApiCall + Util.encodeUrl(filePath) + "')/Versions/DeleteById(vid=" + Integer.toString(versionId) + ")"; // DA

        try {
            inputStream = sendRequest("POST", requestUrl);

            //debug(inputStream);

            success = parseBooleanResponse(inputStream, "DeleteByID");
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return success;
    }

    /**
     * Delete all file versions.
     *
     * @param filePath the file path
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean deleteAllFileVersions(String filePath) throws ServiceException {
        return deleteAllFileVersionsImplementation(filePath);
    }

    private boolean deleteAllFileVersionsImplementation(String filePath) throws ServiceException {
        if (filePath == null) {
            throw new IllegalArgumentException("filePath");
        }

        boolean success = false;
        InputStream inputStream = null;

// DA        String requestUrl = "_api/web/GetFileByServerRelativeUrl('" + Util.encodeUrl(filePath) + "')/Versions/DeleteAll";
        String requestUrl = getFileApiCall + Util.encodeUrl(filePath) + "')/Versions/DeleteAll"; // DA

        try {
            inputStream = sendRequest("POST", null, requestUrl, null, "DELETE");

            //debug(inputStream);

            success = parseBooleanResponse(inputStream, "DeleteAll");
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return success;
    }

    /**
     * Delete file version.
     *
     * @param filePath     the file path
     * @param versionLabel the version label
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean deleteFileVersion(String filePath, String versionLabel) throws ServiceException {
        return deleteFileVersionImplementation(filePath, versionLabel);
    }

    private boolean deleteFileVersionImplementation(String filePath, String versionLabel) throws ServiceException {
        if (filePath == null) {
            throw new IllegalArgumentException("filePath");
        }

        boolean success = false;
        InputStream inputStream = null;

// DA        String requestUrl = "_api/web/GetFileByServerRelativeUrl('" + Util.encodeUrl(filePath) + "')/Versions/DeleteByLabel(versionlabel=" + versionLabel + ")";
        String requestUrl = getFileApiCall + Util.encodeUrl(filePath) + "')/Versions/DeleteByLabel(versionlabel=" + versionLabel + ")"; // DA

        try {
            inputStream = sendRequest("POST", null, requestUrl, null, "DELETE");

            //debug(inputStream);

            success = parseBooleanResponse(inputStream, "DeleteByLabel");
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return success;
    }

    /**
     * Restore file version.
     *
     * @param filePath     the file path
     * @param versionLabel the version label
     * @return true, if successful
     * @throws ServiceException the service exception
     */
    public boolean restoreFileVersion(String filePath, String versionLabel) throws ServiceException {
        return restoreFileVersionImplementation(filePath, versionLabel);
    }

    private boolean restoreFileVersionImplementation(String filePath, String versionLabel) throws ServiceException {
        if (filePath == null) {
            throw new IllegalArgumentException("filePath");
        }

        boolean success = false;
        InputStream inputStream = null;

// DA        String requestUrl = "_api/web/GetFileByServerRelativeUrl('" + Util.encodeUrl(filePath) + "')/Versions/RestoreByLabel(versionlabel=" + versionLabel + ")";
        String requestUrl = getFileApiCall + Util.encodeUrl(filePath) + "')/Versions/RestoreByLabel(versionlabel=" + versionLabel + ")"; // DA

        try {
            inputStream = sendRequest("POST", requestUrl);

            //debug(inputStream);

            success = parseBooleanResponse(inputStream, "RestoreByLabel");
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return success;
    }

    /**
     * Gets the folder.
     *
     * @param folderPath the folder path
     * @return the folder
     * @throws ServiceException the service exception
     */
//DA    public Folder getFolder(String folderPath) throws ServiceException {
    public Folder getFolder(String subSite, String folderPath) throws ServiceException {//DA
        return getFolderImplementation(subSite, folderPath);
    }

    //DA    private Folder getFolderImplementation(String folderPath) throws ServiceException {
    private Folder getFolderImplementation(String subSite, String folderPath) throws ServiceException {//DA
        if (folderPath == null) {
            throw new IllegalArgumentException("folderPath");
        }

        Folder folder = null;
        InputStream inputStream = null;

// DA        String requestUrl = "_api/web/GetFolderByServerRelativeUrl('" + Util.encodeUrl(folderPath) + "')";
        String requestUrl = getFolderApiCall + Util.encodeUrl(folderPath) + "')"; // DA

        try {
            inputStream = sendRequest("GET", subSite, requestUrl);

            //debug(inputStream);

            folder = new Folder(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return folder;
    }

    /**
     * Update folder.
     *
     * @param folder the folder
     * @throws ServiceException the service exception
     */
    public void updateFolder(Folder folder) throws ServiceException {
        updateFolderImplementation(folder);
    }

    private void updateFolderImplementation(Folder folder) throws ServiceException {
        if (folder == null) {
            throw new IllegalArgumentException("folder");
        }

        if (folder.getServerRelativeUrl() == null) {
            throw new IllegalArgumentException("The ServerRelativeUrl property must be set in order to update folder.");
        }

// DA        String requestUrl = "_api/web/GetFolderByServerRelativeUrl('" + Util.encodeUrl(folder.getServerRelativeUrl()) + "')";
        String requestUrl = getFolderApiCall + Util.encodeUrl(folder.getServerRelativeUrl()) + "')"; // DA

        String requestBody = folder.toString();

        InputStream inputStream = null;

        try {
            inputStream = sendRequest("POST", null, requestUrl, requestBody, "MERGE");

            //debug(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl, requestBody);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl, requestBody);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl, requestBody);
                }
            }
        }
    }

    /**
     * Recycle folder.
     *
     * @param folderPath the folder path
     * @return the string
     * @throws ServiceException the service exception
     */
    public String recycleFolder(String folderPath) throws ServiceException {
        return recycleFolderImplementation(folderPath);
    }

    private String recycleFolderImplementation(String folderPath) throws ServiceException {
        if (folderPath == null) {
            throw new IllegalArgumentException("folderPath");
        }

        String id = null;
        InputStream inputStream = null;

// DA        String requestUrl = "_api/web/GetFolderByServerRelativeUrl('" + Util.encodeUrl(folderPath) + "')/recycle";
        String requestUrl = getFolderApiCall + Util.encodeUrl(folderPath) + "')/recycle"; // DA

        try {
            inputStream = sendRequest("POST", requestUrl);

            //debug(inputStream);

            id = parseRecycle(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl);
                }
            }
        }

        return id;
    }

    /**
     * Creates the list.
     *
     * @param list the list
     * @return the com.independentsoft.share. list
     * @throws ServiceException the service exception
     */
    public com.independentsoft.share.List createList(com.independentsoft.share.List list) throws ServiceException {
        return createListImplementation(list);
    }

    private com.independentsoft.share.List createListImplementation(com.independentsoft.share.List list) throws ServiceException {
        if (list == null) {
            throw new IllegalArgumentException("list");
        }

        InputStream inputStream = null;
        com.independentsoft.share.List returnValue = null;

        String requestUrl = "_api/web/lists";

        String requestBody = list.toCreateJSon();

        try {
            inputStream = sendRequest("POST", null, requestUrl, requestBody);

            //debug(inputStream);

            returnValue = new com.independentsoft.share.List(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl, requestBody);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl, requestBody);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl, requestBody);
                }
            }
        }

        return returnValue;
    }

    /**
     * Update list.
     *
     * @param list the list
     * @throws ServiceException the service exception
     */
    public void updateList(com.independentsoft.share.List list) throws ServiceException {
        updateListImplementation(list);
    }

    private void updateListImplementation(com.independentsoft.share.List list) throws ServiceException {
        if (list == null) {
            throw new IllegalArgumentException("list");
        }

        if (list.getId() == null) {
            throw new IllegalArgumentException("The list.getId() must not be null.");
        }

        InputStream inputStream = null;

        String requestUrl = "_api/web/lists('" + list.getId() + "')";

        String requestBody = list.toCreateJSon();

        try {
            inputStream = sendRequest("POST", null, requestUrl, requestBody, "MERGE", "*");

            //debug(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl, requestBody);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl, requestBody);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl, requestBody);
                }
            }
        }
    }

    /**
     * Creates the list item.
     *
     * @param listId   the list id
     * @param listItem the list item
     * @return the list item
     * @throws ServiceException the service exception
     */
//DA    public ListItem createListItem(String listId, ListItem listItem) throws ServiceException {
    public ListItem createListItem(String subSite, String listId, ListItem listItem) throws ServiceException {//DA
        return createListImplementation(subSite, listId, listItem);
    }

    //DA    private ListItem createListImplementation(String listId, ListItem listItem) throws ServiceException {
    private ListItem createListImplementation(String subSite, String listId, ListItem listItem) throws ServiceException {//DA
        if (listId == null) {
            throw new IllegalArgumentException("listId");
        }

        if (listItem == null) {
            throw new IllegalArgumentException("listItem");
        }

        ListItem returnValue = null;
        InputStream inputStream = null;

        String requestUrl = "_api/web/lists('" + listId + "')/items";

        com.independentsoft.share.List list = getList(subSite, listId);

        String requestBody = listItem.toCreateJSon(list.getListItemEntityTypeFullName());

        try {
            inputStream = sendRequest("POST", null, requestUrl, requestBody);

            //debug(inputStream);

            returnValue = new ListItem(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl, requestBody);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl, requestBody);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl, requestBody);
                }
            }
        }

        return returnValue;
    }

    /**
     * Update list item.
     *
     * @param listId   the list id
     * @param listItem the list item
     * @throws ServiceException the service exception
     */
//DA    public void updateListItem(String listId, ListItem listItem) throws ServiceException {
    public void updateListItem(String subSite, String listId, ListItem listItem) throws ServiceException {//DA
        updateListItemImplementation(subSite, listId, listItem);
    }

    //DA    private void updateListItemImplementation(String listId, ListItem listItem) throws ServiceException {
    private void updateListItemImplementation(String subSite, String listId, ListItem listItem) throws ServiceException {//DA
        if (listId == null) {
            throw new IllegalArgumentException("listId");
        }

        if (listItem == null) {
            throw new IllegalArgumentException("listItem");
        }

        InputStream inputStream = null;

        String requestUrl = "_api/web/lists('" + listId + "')/items(" + listItem.getId() + ")";

        com.independentsoft.share.List list = getList(subSite, listId);

        String requestBody = listItem.toUpdateJSon(list.getListItemEntityTypeFullName());

        try {
            inputStream = sendRequest("POST", null, requestUrl, requestBody, "MERGE", "*");

            //debug(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl, requestBody);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl, requestBody);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl, requestBody);
                }
            }
        }
    }

    /**
     * Sets the field value.
     *
     * @param listId     the list id
     * @param listItemId the list item id
     * @param fieldValue the field value
     * @throws ServiceException the service exception
     */
//DA    public void setFieldValue(String listId, int listItemId, FieldValue fieldValue) throws ServiceException {
    public void setFieldValue(String subSite, String listId, int listItemId, FieldValue fieldValue) throws ServiceException {//DA
        List<FieldValue> fieldValues = new ArrayList<FieldValue>();
        fieldValues.add(fieldValue);

        setFieldValuesImplementation(subSite, listId, listItemId, fieldValues);
    }

    /**
     * Sets the field values.
     *
     * @param listId      the list id
     * @param listItemId  the list item id
     * @param fieldValues the field values
     * @throws ServiceException the service exception
     */
//DA    public void setFieldValues(String listId, int listItemId, List<FieldValue> fieldValues) throws ServiceException {
    public void setFieldValues(String subSite, String listId, int listItemId, List<FieldValue> fieldValues) throws ServiceException {//DA
        setFieldValuesImplementation(subSite, listId, listItemId, fieldValues);
    }

    //DA    private void setFieldValuesImplementation(String listId, int listItemId, List<FieldValue> fieldValues) throws ServiceException {
    private void setFieldValuesImplementation(String subSite, String listId, int listItemId, List<FieldValue> fieldValues) throws ServiceException {//DA
        if (listId == null) {
            throw new IllegalArgumentException("listId");
        }

        if (fieldValues == null) {
            throw new IllegalArgumentException("fieldValues");
        }

        if (fieldValues.size() == 0) {
            throw new IllegalArgumentException("Collection fieldValues is emtpy");
        }

        InputStream inputStream = null;

        String requestUrl = "_api/web/lists('" + listId + "')/items(" + listItemId + ")";

        com.independentsoft.share.List list = getList(subSite, listId);

        String requestBody = "{ '__metadata': { 'type': '" + list.getListItemEntityTypeFullName() + "' }";

        for (int i = 0; i < fieldValues.size(); i++) {
            if (fieldValues.get(i).getName() != null && fieldValues.get(i).getName().length() > 0 && fieldValues.get(i).getValue() != null && fieldValues.get(i).getValue().length() > 0) {
                String fieldName = fieldValues.get(i).getName();
                fieldName = fieldName.replaceAll(" ", "_x0020_");

                requestBody += ", '" + Util.encodeJson(fieldName) + "':'" + Util.encodeJson(fieldValues.get(i).getValue()) + "'";
            }
        }

        requestBody += " }";

        try {
            inputStream = sendRequest("POST", null, requestUrl, requestBody, "MERGE", "*");

            //debug(inputStream);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e, requestUrl, requestBody);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl, requestBody);
                }
            }

            if (httpClient != null && connectionManager == null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    throw new ServiceException(e.getMessage(), e, requestUrl, requestBody);
                }
            }
        }
    }

    private List<Change> parseGetChanges(InputStream inputStream) throws Exception {
        List<Change> changes = new ArrayList<Change>();

        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        XMLStreamReader reader = xmlInputFactory.createXMLStreamReader(inputStream);

        while (reader.hasNext() && reader.next() > 0) {
            if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("entry") && reader.getNamespaceURI().equals("http://www.w3.org/2005/Atom")) {
                String changeClass = null;

                while (reader.hasNext()) {
                    if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("category") && reader.getNamespaceURI().equals("http://www.w3.org/2005/Atom")) {
                        changeClass = reader.getAttributeValue(null, "term");
                    }

                    if (reader.isEndElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("category") && reader.getNamespaceURI().equals("http://www.w3.org/2005/Atom")) {
                        break;
                    } else {
                        reader.next();
                    }
                }

                if (changeClass.equals("SP.ChangeAlert")) {
                    ChangeAlert change = new ChangeAlert(reader);
                    changes.add(change);
                } else if (changeClass.equals("SP.ChangeContentType")) {
                    ChangeContentType change = new ChangeContentType(reader);
                    changes.add(change);
                } else if (changeClass.equals("SP.ChangeField")) {
                    ChangeField change = new ChangeField(reader);
                    changes.add(change);
                } else if (changeClass.equals("SP.ChangeFile")) {
                    ChangeFile change = new ChangeFile(reader);
                    changes.add(change);
                } else if (changeClass.equals("SP.ChangeFolder")) {
                    ChangeFolder change = new ChangeFolder(reader);
                    changes.add(change);
                } else if (changeClass.equals("SP.ChangeGroup")) {
                    ChangeGroup change = new ChangeGroup(reader);
                    changes.add(change);
                } else if (changeClass.equals("SP.ChangeItem")) {
                    ChangeItem change = new ChangeItem(reader);
                    changes.add(change);
                } else if (changeClass.equals("SP.ChangeList")) {
                    ChangeList change = new ChangeList(reader);
                    changes.add(change);
                } else if (changeClass.equals("SP.ChangeUser")) {
                    ChangeUser change = new ChangeUser(reader);
                    changes.add(change);
                } else if (changeClass.equals("SP.ChangeView")) {
                    ChangeView change = new ChangeView(reader);
                    changes.add(change);
                } else if (changeClass.equals("SP.ChangeWeb")) {
                    ChangeWeb change = new ChangeWeb(reader);
                    changes.add(change);
                }
            }
        }

        return changes;
    }

    private List<FileVersion> parseGetFileVersions(InputStream inputStream) throws Exception {
        List<FileVersion> fileVersions = new ArrayList<FileVersion>();

        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        XMLStreamReader reader = xmlInputFactory.createXMLStreamReader(inputStream);

        while (reader.hasNext() && reader.next() > 0) {
            if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("entry") && reader.getNamespaceURI().equals("http://www.w3.org/2005/Atom")) {
                FileVersion fileVersion = new FileVersion(reader);
                fileVersions.add(fileVersion);
            }
        }

        return fileVersions;
    }

    private FileVersion parseGetFileVersion(InputStream inputStream) throws Exception {
        FileVersion fileVersion = null;

        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        XMLStreamReader reader = xmlInputFactory.createXMLStreamReader(inputStream);

        while (reader.hasNext() && reader.next() > 0) {
            if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("entry") && reader.getNamespaceURI().equals("http://www.w3.org/2005/Atom")) {
                fileVersion = new FileVersion(reader);
            }
        }

        return fileVersion;
    }

    private String parseRecycle(InputStream inputStream) throws Exception {
        String id = null;

        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        XMLStreamReader reader = xmlInputFactory.createXMLStreamReader(inputStream);

        while (reader.hasNext() && reader.next() > 0) {
            if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Recycle") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices")) {
                id = reader.getElementText();
            }
        }

        return id;
    }

    private List<ListTemplate> parseGetListTemplates(InputStream inputStream) throws Exception {
        List<ListTemplate> listTemplates = new ArrayList<ListTemplate>();

        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        XMLStreamReader reader = xmlInputFactory.createXMLStreamReader(inputStream);

        while (reader.hasNext() && reader.next() > 0) {
            if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("entry") && reader.getNamespaceURI().equals("http://www.w3.org/2005/Atom")) {
                ListTemplate listTemplate = new ListTemplate(reader);
                listTemplates.add(listTemplate);
            }
        }

        return listTemplates;
    }

    private List<View> parseGetViews(InputStream inputStream) throws Exception {
        List<View> views = new ArrayList<View>();

        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        XMLStreamReader reader = xmlInputFactory.createXMLStreamReader(inputStream);

        while (reader.hasNext() && reader.next() > 0) {
            if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("entry") && reader.getNamespaceURI().equals("http://www.w3.org/2005/Atom")) {
                View view = new View(reader);
                views.add(view);
            }
        }

        return views;
    }

    private List<String> parseGetViewFields(InputStream inputStream) throws Exception {
        List<String> fields = new ArrayList<String>();

        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        XMLStreamReader reader = xmlInputFactory.createXMLStreamReader(inputStream);

        while (reader.hasNext()) {
            if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Items") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices")) {
                while (reader.hasNext()) {
                    if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("element") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices")) {
                        String viewFieldName = reader.getElementText();
                        fields.add(viewFieldName);
                    }

                    if (reader.isEndElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Items") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices")) {
                        break;
                    } else {
                        reader.next();
                    }
                }
            }

            if (reader.isEndElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("entry") && reader.getNamespaceURI().equals("http://www.w3.org/2005/Atom")) {
                break;
            } else {
                reader.next();
            }
        }

        return fields;
    }

    private List<com.independentsoft.share.List> parseGetLists(InputStream inputStream) throws Exception {
        List<com.independentsoft.share.List> lists = new ArrayList<com.independentsoft.share.List>();

        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        XMLStreamReader reader = xmlInputFactory.createXMLStreamReader(inputStream);

        while (reader.hasNext() && reader.next() > 0) {
            if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("entry") && reader.getNamespaceURI().equals("http://www.w3.org/2005/Atom")) {
                com.independentsoft.share.List list = new com.independentsoft.share.List(reader);
                lists.add(list);
            }
        }

        return lists;
    }

    private List<FieldLink> parseGetFieldLinks(InputStream inputStream) throws Exception {
        List<FieldLink> fieldLinks = new ArrayList<FieldLink>();

        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        XMLStreamReader reader = xmlInputFactory.createXMLStreamReader(inputStream);

        while (reader.hasNext() && reader.next() > 0) {
            if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("entry") && reader.getNamespaceURI().equals("http://www.w3.org/2005/Atom")) {
                FieldLink fieldLink = new FieldLink(reader);
                fieldLinks.add(fieldLink);
            }
        }

        return fieldLinks;
    }

    private String parseSchemaXml(InputStream inputStream) throws Exception {
        String xml = null;

        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        XMLStreamReader reader = xmlInputFactory.createXMLStreamReader(inputStream);

        while (reader.hasNext() && reader.next() > 0) {
            if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("SchemaXml") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices")) {
                xml = reader.getElementText();
            }
        }

        return xml;
    }

    private List<EventReceiver> parseGetEventReceivers(InputStream inputStream) throws Exception {
        List<EventReceiver> receivers = new ArrayList<EventReceiver>();

        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        XMLStreamReader reader = xmlInputFactory.createXMLStreamReader(inputStream);

        while (reader.hasNext() && reader.next() > 0) {
            if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("entry") && reader.getNamespaceURI().equals("http://www.w3.org/2005/Atom")) {
                EventReceiver receiver = new EventReceiver(reader);
                receivers.add(receiver);
            }
        }

        return receivers;
    }

    private List<ContentType> parseGetContentTypes(InputStream inputStream) throws Exception {
        List<ContentType> contentTypes = new ArrayList<ContentType>();

        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        XMLStreamReader reader = xmlInputFactory.createXMLStreamReader(inputStream);

        while (reader.hasNext() && reader.next() > 0) {
            if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("entry") && reader.getNamespaceURI().equals("http://www.w3.org/2005/Atom")) {
                ContentType contentType = new ContentType(reader);
                contentTypes.add(contentType);
            }
        }

        return contentTypes;
    }

    private List<Form> parseGetForms(InputStream inputStream) throws Exception {
        List<Form> forms = new ArrayList<Form>();

        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        XMLStreamReader reader = xmlInputFactory.createXMLStreamReader(inputStream);

        while (reader.hasNext() && reader.next() > 0) {
            if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("entry") && reader.getNamespaceURI().equals("http://www.w3.org/2005/Atom")) {
                Form form = new Form(reader);
                forms.add(form);
            }
        }

        return forms;
    }

    private List<Role> parseGetRoles(InputStream inputStream) throws Exception {
        List<Role> roles = new ArrayList<Role>();

        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        XMLStreamReader reader = xmlInputFactory.createXMLStreamReader(inputStream);

        while (reader.hasNext() && reader.next() > 0) {
            if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("entry") && reader.getNamespaceURI().equals("http://www.w3.org/2005/Atom")) {
                Role role = new Role(reader);
                roles.add(role);
            }
        }

        return roles;
    }

    private List<RecycleBinItem> parseGetRecycleBinItems(InputStream inputStream) throws Exception {
        List<RecycleBinItem> items = new ArrayList<RecycleBinItem>();

        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        XMLStreamReader reader = xmlInputFactory.createXMLStreamReader(inputStream);

        while (reader.hasNext() && reader.next() > 0) {
            if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("entry") && reader.getNamespaceURI().equals("http://www.w3.org/2005/Atom")) {
                RecycleBinItem item = new RecycleBinItem(reader);
                items.add(item);
            }
        }

        return items;
    }

    private List<NavigationNode> parseGetNavigationNode(InputStream inputStream) throws Exception {
        List<NavigationNode> nodes = new ArrayList<NavigationNode>();

        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        XMLStreamReader reader = xmlInputFactory.createXMLStreamReader(inputStream);

        while (reader.hasNext() && reader.next() > 0) {
            if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("entry") && reader.getNamespaceURI().equals("http://www.w3.org/2005/Atom")) {
                NavigationNode node = new NavigationNode(reader);
                nodes.add(node);
            }
        }

        return nodes;
    }

    private List<WorkflowTemplate> parseGetWorkflowTemplates(InputStream inputStream) throws Exception {
        List<WorkflowTemplate> workflowTemplates = new ArrayList<WorkflowTemplate>();

        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        XMLStreamReader reader = xmlInputFactory.createXMLStreamReader(inputStream);

        while (reader.hasNext() && reader.next() > 0) {
            if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("entry") && reader.getNamespaceURI().equals("http://www.w3.org/2005/Atom")) {
                WorkflowTemplate workflowTemplate = new WorkflowTemplate(reader);
                workflowTemplates.add(workflowTemplate);
            }
        }

        return workflowTemplates;
    }

    private List<TimeZone> parseGetTimeZones(InputStream inputStream) throws Exception {
        List<TimeZone> timeZones = new ArrayList<TimeZone>();

        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        XMLStreamReader reader = xmlInputFactory.createXMLStreamReader(inputStream);

        while (reader.hasNext() && reader.next() > 0) {
            if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("entry") && reader.getNamespaceURI().equals("http://www.w3.org/2005/Atom")) {
                TimeZone timeZone = new TimeZone(reader);
                timeZones.add(timeZone);
            }
        }

        return timeZones;
    }

    private List<Group> parseGetGroups(InputStream inputStream) throws Exception {
        List<Group> groups = new ArrayList<Group>();

        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        XMLStreamReader reader = xmlInputFactory.createXMLStreamReader(inputStream);

        while (reader.hasNext() && reader.next() > 0) {
            if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("entry") && reader.getNamespaceURI().equals("http://www.w3.org/2005/Atom")) {
                Group group = new Group(reader);
                groups.add(group);
            }
        }

        return groups;
    }

    private List<User> parseGetUsers(InputStream inputStream) throws Exception {
        List<User> users = new ArrayList<User>();

        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        XMLStreamReader reader = xmlInputFactory.createXMLStreamReader(inputStream);

        while (reader.hasNext() && reader.next() > 0) {
            if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("entry") && reader.getNamespaceURI().equals("http://www.w3.org/2005/Atom")) {
                User user = new User(reader);
                users.add(user);
            }
        }

        return users;
    }

    private List<SiteTemplate> parseGetSiteTemplates(InputStream inputStream) throws Exception {
        List<SiteTemplate> templates = new ArrayList<SiteTemplate>();

        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        XMLStreamReader reader = xmlInputFactory.createXMLStreamReader(inputStream);

        while (reader.hasNext() && reader.next() > 0) {
            if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("entry") && reader.getNamespaceURI().equals("http://www.w3.org/2005/Atom")) {
                SiteTemplate template = new SiteTemplate(reader);
                templates.add(template);
            }
        }

        return templates;
    }

    private List<SiteInfo> parseGetSiteInfos(InputStream inputStream) throws Exception {
        List<SiteInfo> sites = new ArrayList<SiteInfo>();

        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        XMLStreamReader reader = xmlInputFactory.createXMLStreamReader(inputStream);

        while (reader.hasNext() && reader.next() > 0) {
            if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("entry") && reader.getNamespaceURI().equals("http://www.w3.org/2005/Atom")) {
                SiteInfo site = new SiteInfo(reader);
                sites.add(site);
            }
        }

        return sites;
    }

    private List<Site> parseGetSites(InputStream inputStream) throws Exception {
        List<Site> sites = new ArrayList<Site>();

        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        XMLStreamReader reader = xmlInputFactory.createXMLStreamReader(inputStream);

        while (reader.hasNext() && reader.next() > 0) {
            if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("entry") && reader.getNamespaceURI().equals("http://www.w3.org/2005/Atom")) {
                Site site = new Site(reader);
                sites.add(site);
            }
        }

        return sites;
    }

    private List<Attachment> parseGetAttachments(InputStream inputStream) throws Exception {
        List<Attachment> attachments = new ArrayList<Attachment>();

        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        XMLStreamReader reader = xmlInputFactory.createXMLStreamReader(inputStream);

        while (reader.hasNext() && reader.next() > 0) {
            if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("entry") && reader.getNamespaceURI().equals("http://www.w3.org/2005/Atom")) {
                Attachment attachment = new Attachment(reader);
                attachments.add(attachment);
            }
        }

        return attachments;
    }

    private List<FieldValue> parseGetFieldValues(InputStream inputStream) throws Exception {
        List<FieldValue> values = new ArrayList<FieldValue>();

        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        XMLStreamReader reader = xmlInputFactory.createXMLStreamReader(inputStream);

        while (reader.hasNext() && reader.next() > 0) {
            if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("properties") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices/metadata")) {
                while (reader.hasNext()) {
                    if (reader.isStartElement() && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices")) {
                        String name = reader.getLocalName();
                        String value = reader.getElementText();

                        FieldValue fieldValue = new FieldValue(name, value);
                        values.add(fieldValue);
                    }

                    if (reader.isEndElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("properties") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices/metadata")) {
                        break;
                    } else {
                        reader.next();
                    }
                }
            }
        }

        return values;
    }

    private List<ListItem> parseGetListItems(InputStream inputStream) throws Exception {
        List<ListItem> items = new ArrayList<ListItem>();

        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        XMLStreamReader reader = xmlInputFactory.createXMLStreamReader(inputStream);

        while (reader.hasNext() && reader.next() > 0) {
            if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("entry") && reader.getNamespaceURI().equals("http://www.w3.org/2005/Atom")) {
                ListItem item = new ListItem(reader);
                items.add(item);
            }
        }

        return items;
    }

    private List<Field> parseGetFields(InputStream inputStream) throws Exception {
        List<Field> fields = new ArrayList<Field>();

        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        XMLStreamReader reader = xmlInputFactory.createXMLStreamReader(inputStream);

        while (reader.hasNext() && reader.next() > 0) {
            if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("entry") && reader.getNamespaceURI().equals("http://www.w3.org/2005/Atom")) {
                Field field = new Field(reader);
                fields.add(field);
            }
        }

        return fields;
    }

    private List<Folder> parseGetFolders(InputStream inputStream) throws Exception {
        List<Folder> folders = new ArrayList<>();
        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        XMLStreamReader reader;
        try {
            reader = xmlInputFactory.createXMLStreamReader(inputStream);
            while (reader.hasNext() && reader.next() > 0) {
                if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("entry") && reader.getNamespaceURI().equals("http://www.w3.org/2005/Atom")) {
                    Folder folder = new Folder(reader);
                    folders.add(folder);
                }
            }
            return folders;
        } catch (Exception e) {
            safePrintFailedParseResponse(inputStream, e);
            throw e;
        }
    }

    private void safePrintFailedParseResponse(InputStream inputStream, Exception parsingExp) {
        try {
            inputStream.reset();
            logger.error("Get Folders response parsing has failed {}", parsingExp);
            logger.error("Failed response is {}", IOUtils.toString(inputStream, StandardCharsets.UTF_8));
        } catch (Exception printExp) {
            logger.error("Did not succeed logging the failed response", printExp);
        }
    }

    private List<String> parseGetFeatures(InputStream inputStream) throws Exception {
        List<String> ids = new ArrayList<String>();

        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        XMLStreamReader reader = xmlInputFactory.createXMLStreamReader(inputStream);

        while (reader.hasNext() && reader.next() > 0) {
            if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("DefinitionId") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices")) {
                String id = reader.getElementText();
                ids.add(id);
            }
        }

        return ids;
    }

    private List<File> parseGetFiles(InputStream inputStream) throws Exception {
        List<File> files = new ArrayList<File>();

        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        XMLStreamReader reader = xmlInputFactory.createXMLStreamReader(inputStream);

        while (reader.hasNext() && reader.next() > 0) {
            if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("entry") && reader.getNamespaceURI().equals("http://www.w3.org/2005/Atom")) {
                File file = new File(reader);
                files.add(file);
            }
        }

        return files;
    }

    private boolean parseBooleanResponse(InputStream inputStream, String localName) throws Exception {
        boolean success = false;

        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        XMLStreamReader reader = xmlInputFactory.createXMLStreamReader(inputStream);

        while (reader.hasNext() && reader.next() > 0) {
            if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals(localName) && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices")) {
                success = true;
            }
        }

        return success;
    }

    private List<Integer> parseGetRoleAssignments(InputStream inputStream) throws Exception {
        List<Integer> list = new ArrayList<Integer>();

        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        XMLStreamReader reader = xmlInputFactory.createXMLStreamReader(inputStream);

        while (reader.hasNext() && reader.next() > 0) {
            if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("PrincipalId") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices")) {
                String stringValue = reader.getElementText();

                if (stringValue != null && stringValue.length() > 0) {
                    int id = Integer.parseInt(stringValue);
                    list.add(id);
                }
            }
        }

        return list;
    }

    private int parseReserveListItemId(InputStream inputStream) throws Exception {
        int itemId = -1;

        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        XMLStreamReader reader = xmlInputFactory.createXMLStreamReader(inputStream);

        while (reader.hasNext() && reader.next() > 0) {
            if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("ReserveListItemId") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices")) {
                String stringValue = reader.getElementText();

                if (stringValue != null && stringValue.length() > 0) {
                    itemId = Integer.parseInt(stringValue);
                }
            }
        }

        return itemId;
    }

    private String parseRenderListData(InputStream inputStream) throws Exception {
        String value = null;

        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        XMLStreamReader reader = xmlInputFactory.createXMLStreamReader(inputStream);

        while (reader.hasNext() && reader.next() > 0) {
            if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("RenderListData") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices")) {
                value = reader.getElementText();
            }
        }

        return value;
    }

    private String parseLoginSharePointOnlineResponse(InputStream inputStream) throws Exception {
        String token = null;

        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        XMLStreamReader reader = xmlInputFactory.createXMLStreamReader(inputStream);

        while (reader.hasNext() && reader.next() > 0) {
            if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("BinarySecurityToken") && reader.getNamespaceURI().equals("http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd")) {
                token = reader.getElementText();
            }
        }

        return token;
    }

    private byte[] getInputStreamBuffer(InputStream inputStream) throws IOException {
        BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

        byte[] tempBuffer = new byte[2048];
        byte[] buffer = null;

        try {
            int count = bufferedInputStream.read(tempBuffer);

            while (count != -1) {
                byteArrayOutputStream.write(tempBuffer, 0, count);
                count = bufferedInputStream.read(tempBuffer);
            }

            buffer = byteArrayOutputStream.toByteArray();
        } finally {
            bufferedInputStream.close();
            byteArrayOutputStream.close();
        }

        return buffer;
    }

    private static void debug(InputStream inputStream) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream), 2048);
        StringBuilder responseText = new StringBuilder();
        String line;

        try {
            while ((line = reader.readLine()) != null) {
                responseText.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println(responseText.toString()); //Remove on Android
        //Log.w("Response:", responseText.toString()); //Uncomment on Android
    }

    private static String convertInputStreamToString(InputStream inputStream) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream), 2048);
        StringBuilder responseText = new StringBuilder();
        String line;

        try {
            while ((line = reader.readLine()) != null) {
                responseText.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return responseText.toString();
    }

    /**
     * Reads entire input stream and correct not allowed xml characters.
     *
     * @param inputStream
     * @return
     * @throws IOException
     */
    private static InputStream getInputStream(InputStream inputStream) throws IOException {
        BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

        byte[] tempBuffer = new byte[2048];
        byte[] buffer = null;

        try {
            int count = bufferedInputStream.read(tempBuffer);

            while (count != -1) {
                byteArrayOutputStream.write(tempBuffer, 0, count);
                count = bufferedInputStream.read(tempBuffer);
            }

            buffer = byteArrayOutputStream.toByteArray();
        } finally {
            if (bufferedInputStream != null) {
                bufferedInputStream.close();
            }

            if (byteArrayOutputStream != null) {
                byteArrayOutputStream.close();
            }
        }

        for (int i = 0; i < buffer.length; i++) {
            if (buffer[i] == 0x19) {
                buffer[i] = 0x20;
            } else if (buffer[i] == 38 && i < buffer.length - 3) //allowed are only &#x9, &#xA, &#xD  (9, 10 i 13 ASCII characters)
            {
                if (buffer[i + 1] == 35 && buffer[i + 2] == 120) {
                    if (buffer[i + 3] != 9 && buffer[i + 3] != 65 && buffer[i + 3] != 68) {
                        buffer[i] = 0x20;
                    }
                }
            }
        }

        ByteArrayInputStream outputStream = new ByteArrayInputStream(buffer);

        return outputStream;
    }

    /**
     * Gets SharePoint site url.
     *
     * @return
     */
    public String getSiteUrl() {
        return siteUrl;
    }

    /**
     * Sets SharePoint site url.
     *
     * @param siteUrl
     */
    public void setSiteUrl(String siteUrl) {
        this.siteUrl = siteUrl;
    }

    /**
     * Gets username.
     *
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Sets username.
     *
     * @param username the new username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Gets user's password.
     *
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets user's password.
     *
     * @param password the new password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Gets user's domain.
     *
     * @return the domain
     */
    public String getDomain() {
        return domain;
    }

    /**
     * Sets user's domain.
     *
     * @param domain the new domain
     */
    public void setDomain(String domain) {
        this.domain = domain;
    }

    /**
     * Gets HttpURLConnection proxy server.
     *
     * @return the http url connection proxy
     */
    public Proxy getHttpURLConnectionProxy() {
        return httpURLConnectionProxy;
    }

    /**
     * Sets HttpURLConnection proxy server.
     *
     * @param httpURLConnectionProxy the new http url connection proxy
     */
    public void setHttpURLConnectionProxy(Proxy httpURLConnectionProxy) {
        this.httpURLConnectionProxy = httpURLConnectionProxy;
    }

    /**
     * Gets HttpClient proxy.
     *
     * @return the proxy
     */
    public HttpHost getProxy() {
        return proxy;
    }

    /**
     * Sets HttpClient proxy.
     *
     * @param proxy the new proxy
     */
    public void setProxy(HttpHost proxy) {
        this.proxy = proxy;
    }

    /**
     * Gets proxy credentials.
     *
     * @return the proxy credentials
     */
    public Credentials getProxyCredentials() {
        return proxyCredentials;
    }

    /**
     * Sets proxy credentials.
     *
     * @param proxyCredentials the new proxy credentials
     */
    public void setProxyCredentials(Credentials proxyCredentials) {
        this.proxyCredentials = proxyCredentials;
    }

    /**
     * Gets connection manager.
     *
     * @return the client connection manager
     */
    public HttpClientConnectionManager getClientConnectionManager() {
        return connectionManager;
    }

    /**
     * Sets connection manager.
     *
     * @param connectionManager the new client connection manager
     */
    public void setClientConnectionManager(HttpClientConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }

    /**
     * Gets request configuration.
     *
     * @return the request configuration
     */
    public RequestConfig getRequestConfig() {
        return requestConfig;
    }

    /**
     * Sets request configuration.
     *
     * @param requestConfig the request configuration
     */
    public void setRequestConfig(RequestConfig requestConfig) {
        this.requestConfig = requestConfig;
    }

    /**
     * Sets the http url connection.
     *
     * @param useHttpURLConnection the new http url connection
     */
    public void setHttpURLConnection(boolean useHttpURLConnection) {
        this.useHttpURLConnection = useHttpURLConnection;
    }

    /**
     * Sets the specified timeout value, in milliseconds, to be used when opening a communications link to the resource referenced by the HttpURLConnection. If the timeout expires before the connection can be established, a java.net.SocketTimeoutException is raised. A timeout of zero is interpreted as an infinite timeout.
     *
     * @param connectTimeout the new connect timeout
     */
    public void setConnectTimeout(int connectTimeout) {
        this.connectTimeout = connectTimeout;
    }

    /**
     * Returns setting for connect timeout. 0 return implies that the option is disabled (i.e., timeout of infinity).
     *
     * @return the connect timeout
     */
    public int getConnectTimeout() {
        return this.connectTimeout;
    }

    /**
     * Sets the read timeout to the specified timeout, in milliseconds. A non-zero value specifies the timeout when reading from Input stream when a connection is established to a resource. If the timeout expires before there is data available for read, a java.net.SocketTimeoutException is raised. A timeout of zero is interpreted as an infinite timeout.
     *
     * @param readTimeout the new read timeout
     */
    public void setReadTimeout(int readTimeout) {
        this.readTimeout = readTimeout;
    }

    /**
     * Returns setting for read timeout. 0 return implies that the option is disabled (i.e., timeout of infinity).
     *
     * @return the read timeout
     */
    public int getReadTimeout() {
        return this.readTimeout;
    }

    /**
     * Set whether to accept GZIP encoding, that is, whether to send the HTTP "Accept-Encoding" header with "gzip" as value.
     *
     * @param acceptGzipEncoding the new accept gzip encoding
     */
    public void setAcceptGzipEncoding(boolean acceptGzipEncoding) {
        this.acceptGzipEncoding = acceptGzipEncoding;
    }

    /**
     * Return whether to accept GZIP encoding, that is, whether to send the HTTP "Accept-Encoding" header with "gzip" as value.
     *
     * @return true, if is accept gzip encoding
     */
    public boolean isAcceptGzipEncoding() {
        return this.acceptGzipEncoding;
    }

    public void setCustomHeaders(Header[] customHeaders) {
        this.customHeaders = customHeaders;
    }

    public Header[] getCustomHeaders() {
        return this.customHeaders;
    }

    // DA
    private HttpContext getContext() {
        logger.trace("getContext() serverVersionInfo={} sharePointOnlineRegisteredApp={}", serverVersionInfo, sharePointOnlineRegisteredApp);
        return (serverVersionInfo == ServerVersionInfo.SHARE_POINT_ONLINE && !sharePointOnlineRegisteredApp) ?
                HttpClientContext.create() : getContext(siteUrl, username);
    }

    public static HttpContext getContext(String url, String username) {
        HttpContext context = HttpClientContext.create();
        String token = Optional.ofNullable(url)
                .map(dom -> dom + "\\")
                .orElse("")
                + username;

        ((HttpClientContext) context).setUserToken(token);
        if (logger.isTraceEnabled()) {
            logger.trace("getContext({},{})=#{}", url, username, context == null ? "<null>" : context.hashCode());
        }
        return context;
    }

    // DA
    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public Function<CloseableHttpClient, Header[]> getAppSignInHeadersRetriever() {
        return appSignInHeadersRetriever;
    }

    public void setAppSignInHeadersRetriever(Function<CloseableHttpClient, Header[]> appSignInHeadersRetriever) {
        this.appSignInHeadersRetriever = appSignInHeadersRetriever;
    }

    public interface HttpContextProvider {
        HttpContext getHttpContext(String url, String userName);
    }
}
