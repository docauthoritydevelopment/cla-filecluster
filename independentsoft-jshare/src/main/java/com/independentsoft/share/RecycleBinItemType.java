package com.independentsoft.share;

/**
 * The Enum RecycleBinItemType.
 */
public enum RecycleBinItemType {

    FILE,
    FILE_VERSION,
    LIST_ITEM,
    LIST,
    FOLDER,
    FOLDER_WITH_LISTS,
    ATTACHMENT,
    LIST_ITEM_VERSION,
    CASCADE_PARENT,
    NONE
}
