package com.independentsoft.share;

/**
 * The Class SearchResultPropertyName.
 */
public class SearchResultPropertyName {
	
    public static final String RANK = "Rank";
    public static final String DOC_ID = "DocId";
    public static final String WORK_ID = "WorkId";
    public static final String TITLE = "Title";
    public static final String AUTHOR = "Author";
    public static final String SIZE = "Size";
    public static final String PATH = "Path";
    public static final String DESCRIPTION = "Description";
    public static final String CREATED_TIME = "Write";
    public static final String COLLAPSING_STATUS = "CollapsingStatus";
    public static final String HIT_HIGHLIGHTED_SUMMARY = "HitHighlightedSummary";
    public static final String HIT_HIGHLIGHTED_PROPERTIES = "HitHighlightedProperties";
    public static final String CONTENT_CLASS = "contentclass";
    public static final String PICTURE_TUMBNAIL_URL = "PictureThumbnailURL";
    public static final String SERVER_REDIRECTED_URL = "ServerRedirectedURL";
    public static final String SERVER_REDIRECTED_EMBED_URL = "ServerRedirectedEmbedURL";
    public static final String SERVER_REDIRECTED_PREVIEW_URL = "ServerRedirectedPreviewURL";
    public static final String FILE_EXTENSION = "FileExtension";
    public static final String CONTENT_TYPE_ID = "ContentTypeId";
    public static final String PARENT_LINK = "ParentLink";
    public static final String VIEWS_LIFE_TIME = "ViewsLifeTime";
    public static final String VIEWS_RECENT = "ViewsRecent";
    public static final String SECTION_NAMES = "SectionNames";
    public static final String SECTION_INDEXES = "SectionIndexes";
    public static final String SITE_LOGO = "SiteLogo";
    public static final String SITE_DESCRIPTION = "SiteDescription";
    public static final String DEEP_LINKS = "deeplinks";
    public static final String IMPORTANCE = "importance";
    public static final String SITE_NAME = "SiteName";
    public static final String IS_DOCUMENT = "IsDocument";
    public static final String LAST_MODIFIED_TIME = "LastModifiedTime";
    public static final String FILE_TYPE = "FileType";
    public static final String IS_CONTAINER = "IsContainer";
    public static final String WEB_TEMPLATE = "WebTemplate";
    public static final String SECONDARY_FILE_EXTENSION = "SecondaryFileExtension";
    public static final String DOC_ACL_META = "docaclmeta";
    public static final String ORIGINAL_PATH = "OriginalPath";
    public static final String PARTITION_ID = "PartitionId";
    public static final String URL_ZONE = "UrlZone";
    public static final String RESULT_TYPE_ID = "ResultTypeId";
    public static final String RENDER_TEMPLATE_ID = "RenderTemplateId";
    public static final String PI_SEARCH_RESULT_ID = "piSearchResultId";

}
