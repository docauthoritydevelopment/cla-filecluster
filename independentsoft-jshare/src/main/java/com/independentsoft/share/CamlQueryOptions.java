package com.independentsoft.share;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

/**
 * The Class CamlQueryOptions.
 */
public class CamlQueryOptions {
    
    private boolean dateInUtc;
    private String folder;
    private String paging;
    private boolean includeMandatoryColumns;
    private int meetingInstanceId = Integer.MIN_VALUE;
    private ViewScope viewScope = ViewScope.NONE;
    private RecurrencePatternXmlVersion recurrencePatternXmlVersion = RecurrencePatternXmlVersion.NONE;
    private boolean includePermissions;
    private boolean expandUserField;
    private boolean recurrenceOrderBy;
    private boolean includeAttachmentUrls;
    private boolean includeAttachmentVersion;
    private boolean removeInvalidXmlCharacters;
    private OptimizeFor optimizeFor = OptimizeFor.NONE;
    private String extraIds;
    private boolean optimizeLookups;
    private boolean includeFragmentChanges;

    /**
     * Instantiates a new caml query options.
     */
    public CamlQueryOptions()
    {
    }

    CamlQueryOptions(XMLStreamReader reader) throws XMLStreamException
    {
        parse(reader);
    }

    private void parse(XMLStreamReader reader) throws XMLStreamException
    {
        while (reader.hasNext())
        {
            if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("DateInUtc") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                String dateInUtcString = reader.getElementText();

                if (dateInUtcString != null && dateInUtcString.length() > 0)
                {
                    dateInUtc = Boolean.parseBoolean(dateInUtcString);
                }
            }
            else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Folder") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                folder = reader.getElementText();
            }
            else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Paging") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                paging = reader.getAttributeValue(null, "ListItemCollectionPositionNext");
            }
            else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("IncludeMandatoryColumns") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                String includeMandatoryColumnsString = reader.getElementText();

                if (includeMandatoryColumnsString != null && includeMandatoryColumnsString.length() > 0)
                {
                    includeMandatoryColumns = Boolean.parseBoolean(includeMandatoryColumnsString);
                }
            }
            else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("MeetingInstanceID") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                String meetingInstanceIdString = reader.getElementText();

                if (meetingInstanceIdString != null && meetingInstanceIdString.length() > 0)
                {
                    meetingInstanceId = Integer.parseInt(meetingInstanceIdString);
                }
            }
            else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("ViewAttributes") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                String viewScopeString = reader.getAttributeValue(null, "Scope");

                if (viewScopeString != null && viewScopeString.length() > 0)
                {
                    viewScope = EnumUtil.parseViewScope(viewScopeString);
                }
            }
            else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("RecurrencePatternXMLVersion") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                String recurrencePatternXmlVersionString = reader.getElementText();

                if (recurrencePatternXmlVersionString != null && recurrencePatternXmlVersionString.length() > 0)
                {
                    recurrencePatternXmlVersion = EnumUtil.parseRecurrencePatternXmlVersion(recurrencePatternXmlVersionString);
                }
            }
            else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("IncludePermissions") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                String includePermissionsString = reader.getElementText();

                if (includePermissionsString != null && includePermissionsString.length() > 0)
                {
                    includePermissions = Boolean.parseBoolean(includePermissionsString);
                }
            }
            else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("ExpandUserField") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                String expandUserFieldString = reader.getElementText();

                if (expandUserFieldString != null && expandUserFieldString.length() > 0)
                {
                    expandUserField = Boolean.parseBoolean(expandUserFieldString);
                }
            }
            else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("RecurrenceOrderBy") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                String recurrenceOrderByString = reader.getElementText();

                if (recurrenceOrderByString != null && recurrenceOrderByString.length() > 0)
                {
                    recurrenceOrderBy = Boolean.parseBoolean(recurrenceOrderByString);
                }
            }
            else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("IncludeAttachmentUrls") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                String includeAttachmentUrlsString = reader.getElementText();

                if (includeAttachmentUrlsString != null && includeAttachmentUrlsString.length() > 0)
                {
                    includeAttachmentUrls = Boolean.parseBoolean(includeAttachmentUrlsString);
                }
            }
            else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("IncludeAttachmentVersion") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                String includeAttachmentVersionString = reader.getElementText();

                if (includeAttachmentVersionString != null && includeAttachmentVersionString.length() > 0)
                {
                    includeAttachmentVersion = Boolean.parseBoolean(includeAttachmentVersionString);
                }
            }
            else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("RemoveInvalidXmlCharacters") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                String removeInvalidXmlCharactersString = reader.getElementText();

                if (removeInvalidXmlCharactersString != null && removeInvalidXmlCharactersString.length() > 0)
                {
                    removeInvalidXmlCharacters = Boolean.parseBoolean(removeInvalidXmlCharactersString);
                }
            }
            else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("OptimizeFor") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                String optimizeForString = reader.getElementText();

                if (optimizeForString != null && optimizeForString.length() > 0)
                {
                    optimizeFor = EnumUtil.parseOptimizeFor(optimizeForString);
                }
            }
            else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("ExtraIds") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                extraIds = reader.getElementText();
            }
            else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("OptimizeLookups") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                String optimizeLookupsString = reader.getElementText();

                if (optimizeLookupsString != null && optimizeLookupsString.length() > 0)
                {
                    optimizeLookups = Boolean.parseBoolean(optimizeLookupsString);
                }
            }
            else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("IncludeFragmentChanges") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                String includeFragmentChangesString = reader.getElementText();

                if (includeFragmentChangesString != null && includeFragmentChangesString.length() > 0)
                {
                    includeFragmentChanges = Boolean.parseBoolean(includeFragmentChangesString);
                }
            }

            if (reader.isEndElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("queryOptions") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                break;
            }
            else
            {
                reader.next();
            }
        }
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        String stringValue = "<QueryOptions>";

        if (dateInUtc)
        {
            stringValue += "<DateInUtc>true</DateInUtc>";
        }

        if (folder != null && folder.length() > 0)
        {
            stringValue += "<Folder>" + Util.encodeJson(folder) + "</Folder>";
        }

        if (paging != null)
        {
            stringValue += "<Paging ListItemCollectionPositionNext=\"" + paging + "\"/>";
        }

        if (includeMandatoryColumns)
        {
            stringValue += "<IncludeMandatoryColumns>true</IncludeMandatoryColumns>";
        }

        if (meetingInstanceId > Integer.MIN_VALUE)
        {
            stringValue += "<MeetingInstanceID>" + meetingInstanceId + "</MeetingInstanceID>";
        }

        if (viewScope != ViewScope.NONE)
        {
            stringValue += "<ViewAttributes Scope=\"" + EnumUtil.parseViewScope(viewScope) + "\"/>";
        }

        if (recurrencePatternXmlVersion != RecurrencePatternXmlVersion.NONE)
        {
            stringValue += "<RecurrencePatternXMLVersion>" + EnumUtil.parseRecurrencePatternXmlVersion(recurrencePatternXmlVersion) + "</RecurrencePatternXMLVersion>";
        }

        if (includePermissions)
        {
            stringValue += "<IncludePermissions>true</IncludePermissions>";
        }

        if (expandUserField)
        {
            stringValue += "<ExpandUserField>true</ExpandUserField>";
        }

        if (recurrenceOrderBy)
        {
            stringValue += "<RecurrenceOrderBy>true</RecurrenceOrderBy>";
        }

        if (includeAttachmentUrls)
        {
            stringValue += "<IncludeAttachmentUrls>true</IncludeAttachmentUrls>";
        }

        if (includeAttachmentVersion)
        {
            stringValue += "<IncludeAttachmentVersion>true</IncludeAttachmentVersion>";
        }

        if (removeInvalidXmlCharacters)
        {
            stringValue += "<RemoveInvalidXmlCharacters>true</RemoveInvalidXmlCharacters>";
        }

        if (optimizeFor != OptimizeFor.NONE)
        {
            stringValue += "<OptimizeFor>" + EnumUtil.parseOptimizeFor(optimizeFor) + "</OptimizeFor>";
        }

        if (extraIds != null && extraIds.length() > 0)
        {
            stringValue += "<ExtraIds>" + Util.encodeJson(extraIds) + "</ExtraIds>";
        }

        if (optimizeLookups)
        {
            stringValue += "<OptimizeLookups>true</OptimizeLookups>";
        }

        if (includeFragmentChanges)
        {
            stringValue += "<IncludeFragmentChanges>true</IncludeFragmentChanges>";
        }

        stringValue += "</QueryOptions>";

        return stringValue;
    }

    /**
     * Checks if is date in utc.
     *
     * @return true, if is date in utc
     */
    public boolean isDateInUtc()
    {
        return dateInUtc;
    }

    /**
     * Sets the date in utc.
     *
     * @param dateInUtc the new date in utc
     */
    public void setDateInUtc(boolean dateInUtc)
    {
        this.dateInUtc = dateInUtc;
    }

    /**
     * Gets the folder.
     *
     * @return the folder
     */
    public String getFolder()
    {
        return folder;
    }

    /**
     * Sets the folder.
     *
     * @param folder the new folder
     */
    public void setFolder(String folder)
    {
        this.folder = folder;
    }

    /**
     * Gets the paging.
     *
     * @return the paging
     */
    public String getPaging()
    {
        return paging;
    }

    /**
     * Sets the paging.
     *
     * @param paging the new paging
     */
    public void setPaging(String paging)
    {
        this.paging = paging;
    }

    /**
     * Checks if is include mandatory columns.
     *
     * @return true, if is include mandatory columns
     */
    public boolean isIncludeMandatoryColumns()
    {
        return includeMandatoryColumns;
    }

    /**
     * Sets the include mandatory columns.
     *
     * @param includeMandatoryColumns the new include mandatory columns
     */
    public void setIncludeMandatoryColumns(boolean includeMandatoryColumns)
    {
        this.includeMandatoryColumns = includeMandatoryColumns;
    }

    /**
     * Gets the meeting instance id.
     *
     * @return the meeting instance id
     */
    public int getMeetingInstanceId()
    {
        return meetingInstanceId;
    }

    /**
     * Sets the meeting instance id.
     *
     * @param meetingInstanceId the new meeting instance id
     */
    public void setMeetingInstanceId(int meetingInstanceId)
    {
        this.meetingInstanceId = meetingInstanceId;
    }

    /**
     * Gets the view scope.
     *
     * @return the view scope
     */
    public ViewScope getViewScope()
    {
        return viewScope;
    }

    /**
     * Sets the view scope.
     *
     * @param viewScope the new view scope
     */
    public void setViewScope(ViewScope viewScope)
    {
        this.viewScope = viewScope;
    }

    /**
     * Gets the recurrence pattern xml version.
     *
     * @return the recurrence pattern xml version
     */
    public RecurrencePatternXmlVersion getRecurrencePatternXmlVersion()
    {
        return recurrencePatternXmlVersion;
    }

    /**
     * Sets the recurrence pattern xml version.
     *
     * @param recurrencePatternXmlVersion the new recurrence pattern xml version
     */
    public void setRecurrencePatternXmlVersion(RecurrencePatternXmlVersion recurrencePatternXmlVersion)
    {
        this.recurrencePatternXmlVersion = recurrencePatternXmlVersion;
    }

    /**
     * Checks if is include permissions.
     *
     * @return true, if is include permissions
     */
    public boolean isIncludePermissions()
    {
        return includePermissions;
    }

    /**
     * Sets the include permissions.
     *
     * @param includePermissions the new include permissions
     */
    public void setIncludePermissions(boolean includePermissions)
    {
        this.includePermissions = includePermissions;
    }

    /**
     * Checks if is expand user field.
     *
     * @return true, if is expand user field
     */
    public boolean isExpandUserField()
    {
        return expandUserField;
    }

    /**
     * Sets the expand user field.
     *
     * @param expandUserField the new expand user field
     */
    public void setExpandUserField(boolean expandUserField)
    {
        this.expandUserField = expandUserField;
    }

    /**
     * Checks if is recurrence order by.
     *
     * @return true, if is recurrence order by
     */
    public boolean isRecurrenceOrderBy()
    {
        return recurrenceOrderBy;
    }

    /**
     * Sets the recurrence order by.
     *
     * @param recurrenceOrderBy the new recurrence order by
     */
    public void setRecurrenceOrderBy(boolean recurrenceOrderBy)
    {
        this.recurrenceOrderBy = recurrenceOrderBy;
    }

    /**
     * Checks if is include attachment urls.
     *
     * @return true, if is include attachment urls
     */
    public boolean isIncludeAttachmentUrls()
    {
        return includeAttachmentUrls;
    }

    /**
     * Sets the include attachment urls.
     *
     * @param includeAttachmentUrls the new include attachment urls
     */
    public void setIncludeAttachmentUrls(boolean includeAttachmentUrls)
    {
        this.includeAttachmentUrls = includeAttachmentUrls;
    }

    /**
     * Checks if is include attachment version.
     *
     * @return true, if is include attachment version
     */
    public boolean isIncludeAttachmentVersion()
    {
        return includeAttachmentVersion;
    }

    /**
     * Sets the include attachment version.
     *
     * @param includeAttachmentVersion the new include attachment version
     */
    public void setIncludeAttachmentVersion(boolean includeAttachmentVersion)
    {
        this.includeAttachmentVersion = includeAttachmentVersion;
    }

    /**
     * Checks if is removes the invalid xml characters.
     *
     * @return true, if is removes the invalid xml characters
     */
    public boolean isRemoveInvalidXmlCharacters()
    {
        return removeInvalidXmlCharacters;
    }

    /**
     * Sets the removes the invalid xml characters.
     *
     * @param removeInvalidXmlCharacters the new removes the invalid xml characters
     */
    public void setRemoveInvalidXmlCharacters(boolean removeInvalidXmlCharacters)
    {
        this.removeInvalidXmlCharacters = removeInvalidXmlCharacters;
    }

    /**
     * Gets the optimize for.
     *
     * @return the optimize for
     */
    public OptimizeFor getOptimizeFor()
    {
        return optimizeFor;
    }

    /**
     * Sets the optimize for.
     *
     * @param optimizeFor the new optimize for
     */
    public void setOptimizeFor(OptimizeFor optimizeFor)
    {
        this.optimizeFor = optimizeFor;
    }

    /**
     * Gets the extra ids.
     *
     * @return the extra ids
     */
    public String getExtraIds()
    {
        return extraIds;
    }

    /**
     * Sets the extra ids.
     *
     * @param extraIds the new extra ids
     */
    public void setExtraIds(String extraIds)
    {
        this.extraIds = extraIds;
    }

    /**
     * Checks if is optimize lookups.
     *
     * @return true, if is optimize lookups
     */
    public boolean isOptimizeLookups()
    {
        return optimizeLookups;
    }

    /**
     * Sets the optimize lookups.
     *
     * @param optimizeLookups the new optimize lookups
     */
    public void setOptimizeLookups(boolean optimizeLookups)
    {
        this.optimizeLookups = optimizeLookups;
    }

    /**
     * Checks if is include fragment changes.
     *
     * @return true, if is include fragment changes
     */
    public boolean isIncludeFragmentChanges()
    {
        return includeFragmentChanges;
    }

    /**
     * Sets the include fragment changes.
     *
     * @param includeFragmentChanges the new include fragment changes
     */
    public void setIncludeFragmentChanges(boolean includeFragmentChanges)
    {
        this.includeFragmentChanges = includeFragmentChanges;
    }
}
