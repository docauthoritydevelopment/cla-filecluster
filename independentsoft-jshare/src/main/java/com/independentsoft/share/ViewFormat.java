package com.independentsoft.share;

import java.util.ArrayList;
import java.util.List;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

/**
 * The Class ViewFormat.
 */
public class ViewFormat {

    private List<FormatDefinition> definitions = new ArrayList<FormatDefinition>();
    private List<Format> formats = new ArrayList<Format>();

    /**
     * Instantiates a new view format.
     */
    public ViewFormat()
    {
    }

    ViewFormat(XMLStreamReader reader) throws XMLStreamException
    {
        parse(reader);
    }

    private void parse(XMLStreamReader reader) throws XMLStreamException
    {
        while (reader.hasNext())
        {
            if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("FormatDef") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                FormatDefinition definition = new FormatDefinition(reader);
                definitions.add(definition);
            }
            else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Format") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                Format format = new Format(reader);
                formats.add(format);
            }

            if (reader.isEndElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Formats") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                break;
            }
            else
            {
                reader.next();
            }
        }
    }

    /**
     * Gets the definitions.
     *
     * @return the definitions
     */
    public List<FormatDefinition> getDefinitions()
    {
        return definitions;
    }

    /**
     * Gets the formats.
     *
     * @return the formats
     */
    public List<Format> getFormats()
    {
        return formats;
    }
}
