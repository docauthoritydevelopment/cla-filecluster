package com.independentsoft.share;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

/**
 * The Class SuggestResult.
 */
public class SuggestResult {

    private List<String> peopleNames = new ArrayList<String>();
    private List<PersonalResultSuggestion> personalResults = new ArrayList<PersonalResultSuggestion>();
    private List<SuggestionQuery> queries = new ArrayList<SuggestionQuery>();

    /**
     * Instantiates a new suggest result.
     */
    public SuggestResult()
    {
    }

    SuggestResult(InputStream inputStream) throws XMLStreamException
    {
        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        XMLStreamReader reader = xmlInputFactory.createXMLStreamReader(inputStream);

        parse(reader);
    }

    private void parse(XMLStreamReader reader) throws XMLStreamException
    {
        while (reader.hasNext() && reader.next() > 0)
        {
            if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("PeopleNames") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                String innerXml = reader.getElementText();

                if (innerXml != null && innerXml.length() > 64)
                {
                    throw new UnsupportedOperationException("PeopleNames response:" + innerXml);
                }
            }
            else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("PersonalResults") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                String innerXml = reader.getElementText();

                if (innerXml != null && innerXml.length() > 64)
                {
                    throw new UnsupportedOperationException("PersonalResults response:" + innerXml); //see class PersonalResultSuggestion
                }
            }
            else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("TriggeredRules") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                String innerXml = reader.getElementText();

                if (innerXml != null && innerXml.length() > 64)
                {
                    throw new UnsupportedOperationException("Queries response:" + innerXml); // see class SuggestionQuery
                }
            }
        }
    }

    /**
     * Gets the people names.
     *
     * @return the people names
     */
    public List<String> getPeopleNames()
    {
        return peopleNames;
    }

    /**
     * Gets the personal results.
     *
     * @return the personal results
     */
    public List<PersonalResultSuggestion> getPersonalResults()
    {
        return personalResults;
    }

    /**
     * Gets the queries.
     *
     * @return the queries
     */
    public List<SuggestionQuery> getQueries()
    {
        return queries;
    }
}
