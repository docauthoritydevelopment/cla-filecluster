package com.independentsoft.share;

/**
 * The Enum Direction.
 */
public enum Direction {

    LEFT_TO_RIGHT,
    RIGHT_TO_LEFT,
    NONE
}
