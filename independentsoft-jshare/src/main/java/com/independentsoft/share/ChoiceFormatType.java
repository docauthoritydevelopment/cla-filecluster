package com.independentsoft.share;

/**
 * The Enum ChoiceFormatType.
 */
public enum ChoiceFormatType {

    DROPDOWN,
    RADIO_BUTTONS,
    NONE
}
