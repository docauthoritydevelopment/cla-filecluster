package com.independentsoft.share.queryoptions;

import java.util.Date;

import com.independentsoft.share.Util;

/**
 * The Class IsLessThanOrEqualTo.
 */
public class IsLessThanOrEqualTo implements IFilterRestriction {

    private String propertyName;
    private String value;

    /**
     * Instantiates a new checks if is less than or equal to.
     *
     * @param propertyName the property name
     * @param value the value
     */
    public IsLessThanOrEqualTo(String propertyName, long value)
    {
        if (propertyName == null)
        {
            throw new IllegalArgumentException("propertyName");
        }

        this.propertyName = propertyName;
        this.value = Long.toString(value);
    }

    /**
     * Instantiates a new checks if is less than or equal to.
     *
     * @param propertyName the property name
     * @param value the value
     */
    public IsLessThanOrEqualTo(String propertyName, double value)
    {
        if (propertyName == null)
        {
            throw new IllegalArgumentException("propertyName");
        }

        this.propertyName = propertyName;
        this.value = Double.toString(value);
    }

    /**
     * Instantiates a new checks if is less than or equal to.
     *
     * @param propertyName the property name
     * @param value the value
     */
    public IsLessThanOrEqualTo(String propertyName, Date value)
    {
        if (propertyName == null)
        {
            throw new IllegalArgumentException("propertyName");
        }

        if (value == null)
        {
            throw new IllegalArgumentException("value");
        }

        this.propertyName = propertyName;
        this.value = "datetime'" + Util.toUniversalTime(value) + "'";
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        String stringValue = propertyName + " le " + value;

        return stringValue;
    }
}
