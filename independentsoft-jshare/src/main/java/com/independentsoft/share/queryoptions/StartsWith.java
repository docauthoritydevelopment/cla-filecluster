package com.independentsoft.share.queryoptions;

import com.independentsoft.share.Util;

/**
 * The Class StartsWith.
 */
public class StartsWith implements IFilterRestriction {

    private String propertyName;
    private String value;

    /**
     * Instantiates a new starts with.
     *
     * @param propertyName the property name
     * @param value the value
     */
    public StartsWith(String propertyName, String value)
    {
        if (propertyName == null)
        {
            throw new IllegalArgumentException("propertyName");
        }

        if (value == null)
        {
            throw new IllegalArgumentException("value");
        }

        this.propertyName = propertyName;
        this.value = "\'" + value + "\'";
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        String stringValue = "startswith(" + Util.encodeUrlInputStream(propertyName) + ", " + Util.encodeUrlInputStream(value) + ")";

        return stringValue;
    }
}
