package com.independentsoft.share.queryoptions;

/**
 * The Class IsNotNull.
 */
public class IsNotNull implements IFilterRestriction {

    private String expression;

    /**
     * Instantiates a new checks if is not null.
     *
     * @param propertyName the property name
     */
    public IsNotNull(String propertyName)
    {
        if (propertyName == null)
        {
            throw new IllegalArgumentException("propertyName");
        }

        this.expression = propertyName;
    }

    /**
     * Instantiates a new checks if is not null.
     *
     * @param filterRestriction the filter restriction
     */
    public IsNotNull(IFilterRestriction filterRestriction)
    {
        if (filterRestriction == null)
        {
            throw new IllegalArgumentException("filterRestriction");
        }

        this.expression = filterRestriction.toString();
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        String stringValue = expression + " ne null";

        return stringValue;
    }
}
