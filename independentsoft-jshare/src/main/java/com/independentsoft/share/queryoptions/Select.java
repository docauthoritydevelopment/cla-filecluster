package com.independentsoft.share.queryoptions;

import java.util.ArrayList;
import java.util.List;

import com.independentsoft.share.Util;

/**
 * The Class Select.
 */
public class Select implements IQueryOption {

    private List<String> properties = new ArrayList<String>();

    /**
     * Instantiates a new select.
     */
    public Select()
    {
    }

    /**
     * Instantiates a new select.
     *
     * @param property the property
     */
    public Select(String property)
    {
        if (property == null)
        {
            throw new IllegalArgumentException("property");
        }

        properties.add(property);
    }

    /**
     * Instantiates a new select.
     *
     * @param property1 the property1
     * @param property2 the property2
     */
    public Select(String property1, String property2)
    {
        if (property1 == null)
        {
            throw new IllegalArgumentException("property1");
        }

        if (property2 == null)
        {
            throw new IllegalArgumentException("property2");
        }

        properties.add(property1);
        properties.add(property2);
    }

    /**
     * Instantiates a new select.
     *
     * @param property1 the property1
     * @param property2 the property2
     * @param property3 the property3
     */
    public Select(String property1, String property2, String property3)
    {
        if (property1 == null)
        {
            throw new IllegalArgumentException("property1");
        }

        if (property2 == null)
        {
            throw new IllegalArgumentException("property2");
        }

        if (property3 == null)
        {
            throw new IllegalArgumentException("property3");
        }

        properties.add(property1);
        properties.add(property2);
        properties.add(property3);
    }

    /**
     * Instantiates a new select.
     *
     * @param property1 the property1
     * @param property2 the property2
     * @param property3 the property3
     * @param property4 the property4
     */
    public Select(String property1, String property2, String property3, String property4)
    {
        if (property1 == null)
        {
            throw new IllegalArgumentException("property1");
        }

        if (property2 == null)
        {
            throw new IllegalArgumentException("property2");
        }

        if (property3 == null)
        {
            throw new IllegalArgumentException("property3");
        }

        if (property4 == null)
        {
            throw new IllegalArgumentException("property4");
        }

        properties.add(property1);
        properties.add(property2);
        properties.add(property3);
        properties.add(property4);
    }

    /**
     * Instantiates a new select.
     *
     * @param properties the properties
     */
    public Select(String[] properties)
    {
        if (properties == null)
        {
            throw new IllegalArgumentException("properties");
        }

        for (int i = 0; i < properties.length; i++)
        {
            this.properties.add(properties[i]);
        }
    }

    /**
     * Instantiates a new select.
     *
     * @param properties the properties
     */
    public Select(List<String> properties)
    {
        if (properties == null)
        {
            throw new IllegalArgumentException("properties");
        }

        for (int i = 0; i < properties.size(); i++)
        {
            this.properties.add(properties.get(i));
        }
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        if (properties.size() > 0)
        {
            String stringValue = "$select=";

            for (int i = 0; i < properties.size(); i++)
            {
                if (properties.get(i) != null)
                {
                    stringValue += Util.encodeUrlInputStream(properties.get(i));
                }

                if (i < properties.size() - 1)
                {
                    stringValue += ",";
                }
            }

            return stringValue;
        }
        else
        {
            return "";
        }
    }

    /**
     * Gets the properties.
     *
     * @return the properties
     */
    public List<String> getProperties()
    {
        return properties;
    }
}
