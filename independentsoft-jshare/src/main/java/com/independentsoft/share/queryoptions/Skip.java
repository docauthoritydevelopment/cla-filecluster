package com.independentsoft.share.queryoptions;

/**
 * The Class Skip.
 */
public class Skip implements IQueryOption {

    private int value;

    /**
     * Instantiates a new skip.
     */
    public Skip()
    {
    }

    /**
     * Instantiates a new skip.
     *
     * @param value the value
     */
    public Skip(int value)
    {
        if (value < 0)
        {
            throw new IllegalArgumentException("The value must be an integer greater than zero.");
        }

        this.value = value;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        String stringValue = "$skip=" + Integer.toString(value);

        return stringValue;
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public int getValue()
    {
        return value;
    }

    /**
     * Sets the value.
     *
     * @param value the new value
     */
    public void setValue(int value)
    {
        if (value < 0)
        {
            throw new IllegalArgumentException("The value must be an integer greater than zero.");
        }

        this.value = value;
    }
}
