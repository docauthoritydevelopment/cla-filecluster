package com.independentsoft.share.queryoptions;

/**
 * The Class IsNull.
 */
public class IsNull implements IFilterRestriction {

    private String expression;

    /**
     * Instantiates a new checks if is null.
     *
     * @param propertyName the property name
     */
    public IsNull(String propertyName)
    {
        if (propertyName == null)
        {
            throw new IllegalArgumentException("propertyName");
        }

        this.expression = propertyName;
    }

    /**
     * Instantiates a new checks if is null.
     *
     * @param filterRestriction the filter restriction
     */
    public IsNull(IFilterRestriction filterRestriction)
    {
        if (filterRestriction == null)
        {
            throw new IllegalArgumentException("filterRestriction");
        }

        this.expression = filterRestriction.toString();
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        String stringValue = expression + " eq null";

        return stringValue;
    }
}
