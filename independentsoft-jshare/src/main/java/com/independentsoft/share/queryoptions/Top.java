package com.independentsoft.share.queryoptions;

/**
 * The Class Top.
 */
public class Top implements IQueryOption {

    private int value;

    /**
     * Instantiates a new top.
     */
    public Top()
    {
    }

    /**
     * Instantiates a new top.
     *
     * @param value the value
     */
    public Top(int value)
    {
        if (value < 0)
        {
            throw new IllegalArgumentException("The value must be an integer greater than zero.");
        }

        this.value = value;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        String stringValue = "$top=" + Integer.toString(value);

        return stringValue;
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public int getValue()
    {
        return value;
    }

    /**
     * Sets the value.
     *
     * @param value the new value
     */
    public void setValue(int value)
    {
        if (value < 0)
        {
            throw new IllegalArgumentException("The value must be an integer greater than zero.");
        }

        this.value = value;
    }
}
