package com.independentsoft.share.queryoptions;

import java.util.ArrayList;
import java.util.List;

/**
 * The Class Or.
 */
public class Or implements IFilterRestriction {

    private List<IFilterRestriction> restrictions = new ArrayList<IFilterRestriction>();

    /**
     * Instantiates a new or.
     *
     * @param restriction1 the restriction1
     * @param restriction2 the restriction2
     */
    public Or(IFilterRestriction restriction1, IFilterRestriction restriction2)
    {
        if (restriction1 == null)
        {
            throw new IllegalArgumentException("restriction1");
        }

        if (restriction2 == null)
        {
            throw new IllegalArgumentException("restriction2");
        }

        this.restrictions.add(restriction1);
        this.restrictions.add(restriction2);
    }

    /**
     * Instantiates a new or.
     *
     * @param restriction1 the restriction1
     * @param restriction2 the restriction2
     * @param restriction3 the restriction3
     */
    public Or(IFilterRestriction restriction1, IFilterRestriction restriction2, IFilterRestriction restriction3)
    {
        if (restriction1 == null)
        {
            throw new IllegalArgumentException("restriction1");
        }

        if (restriction2 == null)
        {
            throw new IllegalArgumentException("restriction2");
        }

        if (restriction3 == null)
        {
            throw new IllegalArgumentException("restriction3");
        }

        this.restrictions.add(restriction1);
        this.restrictions.add(restriction2);
        this.restrictions.add(restriction3);
    }

    /**
     * Instantiates a new or.
     *
     * @param restriction1 the restriction1
     * @param restriction2 the restriction2
     * @param restriction3 the restriction3
     * @param restriction4 the restriction4
     */
    public Or(IFilterRestriction restriction1, IFilterRestriction restriction2, IFilterRestriction restriction3, IFilterRestriction restriction4)
    {
        if (restriction1 == null)
        {
            throw new IllegalArgumentException("restriction1");
        }

        if (restriction2 == null)
        {
            throw new IllegalArgumentException("restriction2");
        }

        if (restriction3 == null)
        {
            throw new IllegalArgumentException("restriction3");
        }

        if (restriction4 == null)
        {
            throw new IllegalArgumentException("restriction4");
        }

        this.restrictions.add(restriction1);
        this.restrictions.add(restriction2);
        this.restrictions.add(restriction3);
        this.restrictions.add(restriction4);
    }

    /**
     * Instantiates a new or.
     *
     * @param restrictions the restrictions
     */
    public Or(List<IFilterRestriction> restrictions)
    {
        if (restrictions == null)
        {
            throw new IllegalArgumentException("restrictions");
        }

        this.restrictions = restrictions;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        if (restrictions.size() > 1)
        {
            String stringValue = "";

            for (int i = 0; i < restrictions.size(); i++)
            {
                stringValue += "(" + restrictions.get(i).toString() + ")";

                if (i < restrictions.size() - 1)
                {
                    stringValue += " or ";
                }
            }

            return stringValue;
        }
        else
        {
            return "";
        }
    }
}
