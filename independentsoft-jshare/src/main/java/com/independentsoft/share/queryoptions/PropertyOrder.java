package com.independentsoft.share.queryoptions;

import com.independentsoft.share.Util;

/**
 * The Class PropertyOrder.
 */
public class PropertyOrder {

    private String property;
    private boolean isDescending;

    /**
     * Instantiates a new property order.
     */
    public PropertyOrder()
    {
    }

    /**
     * Instantiates a new property order.
     *
     * @param property the property
     */
    public PropertyOrder(String property)
    {
        if (property == null)
        {
            throw new IllegalArgumentException("property");
        }

        this.property = property;
    }

    /**
     * Instantiates a new property order.
     *
     * @param property the property
     * @param isDescending the is descending
     */
    public PropertyOrder(String property, boolean isDescending)
    {
        if (property == null)
        {
            throw new IllegalArgumentException("property");
        }

        this.property = property;
        this.isDescending = isDescending;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        String stringValue = Util.encodeUrlInputStream(property);

        if(isDescending)
        {
            stringValue += " desc";
        }

        return stringValue;
    }

    /**
     * Gets the property.
     *
     * @return the property
     */
    public String getProperty()
    {
        return property;
    }

    /**
     * Sets the property.
     *
     * @param property the new property
     */
    public void setProperty(String property)
    {
        this.property = property;
    }

    /**
     * Checks if is descending.
     *
     * @return true, if is descending
     */
    public boolean isDescending()
    {
        return isDescending;
    }

    /**
     * Sets the descending.
     *
     * @param isDescending the new descending
     */
    public void setDescending(boolean isDescending)
    {
        this.isDescending = isDescending;
    }
}
