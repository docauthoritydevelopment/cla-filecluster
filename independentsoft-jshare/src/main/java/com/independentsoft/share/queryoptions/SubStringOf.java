package com.independentsoft.share.queryoptions;

import com.independentsoft.share.Util;

/**
 * The Class SubStringOf.
 */
public class SubStringOf implements IFilterRestriction {

    private String propertyName;
    private String value;

    /**
     * Instantiates a new sub string of.
     *
     * @param propertyName the property name
     * @param value the value
     */
    public SubStringOf(String propertyName, String value)
    {
        if (propertyName == null)
        {
            throw new IllegalArgumentException("propertyName");
        }

        if (value == null)
        {
            throw new IllegalArgumentException("value");
        }

        this.propertyName = propertyName;
        this.value = "\'" + value + "\'";
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        String stringValue = "substringof(" + Util.encodeUrlInputStream(value) + ", " + Util.encodeUrlInputStream(propertyName) + ")";

        return stringValue;
    }
}
