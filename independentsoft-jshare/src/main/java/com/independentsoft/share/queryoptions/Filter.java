package com.independentsoft.share.queryoptions;

import com.independentsoft.share.Util;

import java.util.Objects;

/**
 * The Class Filter.
 */
public class Filter implements IQueryOption {

    private IFilterRestriction filterRestriction;

    /**
     * Instantiates a new filter.
     *
     * @param filterRestriction the filter restriction
     */
    public Filter(IFilterRestriction filterRestriction)
    {
        if (filterRestriction == null)
        {
            throw new IllegalArgumentException("filterRestriction");
        }

        this.filterRestriction = filterRestriction;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        String stringValue = "$filter=" + Util.encodeUrlInputStream(filterRestriction.toString());

        return stringValue;
    }


    // DA
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Filter filter = (Filter) o;
        return Objects.equals(filterRestriction, filter.filterRestriction);
    }

    // DA
    @Override
    public int hashCode() {
        return Objects.hash(filterRestriction);
    }
}
