package com.independentsoft.share.queryoptions;

import java.util.ArrayList;
import java.util.List;

import com.independentsoft.share.Util;

/**
 * The Class OrderBy.
 */
public class OrderBy implements IQueryOption {

    private List<PropertyOrder> propertyOrders = new ArrayList<PropertyOrder>();

    /**
     * Instantiates a new order by.
     */
    public OrderBy()
    {
    }

    /**
     * Instantiates a new order by.
     *
     * @param propertyOrder the property order
     */
    public OrderBy(PropertyOrder propertyOrder)
    {
        if (propertyOrder == null)
        {
            throw new IllegalArgumentException("propertyOrder");
        }

        propertyOrders.add(propertyOrder);
    }

    /**
     * Instantiates a new order by.
     *
     * @param propertyOrder1 the property order1
     * @param propertyOrder2 the property order2
     */
    public OrderBy(PropertyOrder propertyOrder1, PropertyOrder propertyOrder2)
    {
        if (propertyOrder1 == null)
        {
            throw new IllegalArgumentException("propertyOrder1");
        }

        if (propertyOrder2 == null)
        {
            throw new IllegalArgumentException("propertyOrder2");
        }

        propertyOrders.add(propertyOrder1);
        propertyOrders.add(propertyOrder2);
    }

    /**
     * Instantiates a new order by.
     *
     * @param propertyOrder1 the property order1
     * @param propertyOrder2 the property order2
     * @param propertyOrder3 the property order3
     */
    public OrderBy(PropertyOrder propertyOrder1, PropertyOrder propertyOrder2, PropertyOrder propertyOrder3)
    {
        if (propertyOrder1 == null)
        {
            throw new IllegalArgumentException("propertyOrder1");
        }

        if (propertyOrder2 == null)
        {
            throw new IllegalArgumentException("propertyOrder2");
        }

        if (propertyOrder3 == null)
        {
            throw new IllegalArgumentException("propertyOrder3");
        }

        propertyOrders.add(propertyOrder1);
        propertyOrders.add(propertyOrder2);
        propertyOrders.add(propertyOrder3);
    }

    /**
     * Instantiates a new order by.
     *
     * @param propertyOrders the property orders
     */
    public OrderBy(PropertyOrder[] propertyOrders)
    {
        if (propertyOrders == null)
        {
            throw new IllegalArgumentException("propertyOrders");
        }

        for (int i = 0; i < propertyOrders.length; i++)
        {
            if (propertyOrders[i] != null)
            {
                this.propertyOrders.add(propertyOrders[i]);
            }
        }
    }

    /**
     * Instantiates a new order by.
     *
     * @param propertyOrders the property orders
     */
    public OrderBy(List<PropertyOrder> propertyOrders)
    {
        if (propertyOrders == null)
        {
            throw new IllegalArgumentException("propertyOrders");
        }

        for (int i = 0; i < propertyOrders.size(); i++)
        {
            if (propertyOrders.get(i) != null)
            {
                this.propertyOrders.add(propertyOrders.get(i));
            }
        }
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        if (propertyOrders.size() > 0)
        {
            String stringValue = "$orderby=";

            for (int i = 0; i < propertyOrders.size(); i++)
            {
                if (propertyOrders.get(i) != null)
                {
                    stringValue += Util.encodeUrlInputStream(propertyOrders.get(i).toString());
                }

                if (i < propertyOrders.size() - 1)
                {
                    stringValue += ",";
                }
            }

            return stringValue;
        }
        else
        {
            return "";
        }
    }

    /**
     * Gets the property orders.
     *
     * @return the property orders
     */
    public List<PropertyOrder> getPropertyOrders()
    {
        return propertyOrders;
    }
}
