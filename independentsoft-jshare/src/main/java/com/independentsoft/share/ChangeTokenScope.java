package com.independentsoft.share;

/**
 * The Enum ChangeTokenScope.
 */
public enum ChangeTokenScope {
	
    CONTENT_DB,
    SITE_COLLECTION,
    SITE,
    LIST
}
