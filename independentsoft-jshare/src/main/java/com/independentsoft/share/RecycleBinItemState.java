package com.independentsoft.share;

/**
 * The Enum RecycleBinItemState.
 */
public enum RecycleBinItemState {

    FIRST_STAGE_RECYCLE_BIN,
    SECOND_STAGE_RECYCLE_BIN,
    NONE
}
