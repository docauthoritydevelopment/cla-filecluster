package com.independentsoft.share;

/**
 * The Enum ControlMode.
 */
public enum ControlMode {

    DISPLAY,
    EDIT,
    NEW
}
