package com.independentsoft.share;

/**
 * The Enum EdmType.
 */
public enum EdmType {

    BINARY,
    BOOLEAN,
    BYTE,
    DATE_TIME,
    DATE_TIME_OFFSET,
    DECIMAL,
    DOUBLE,
    GUID,
    INT_16,
    INT_32,
    INT_64,
    S_BYTE,
    SINGLE,
    STRING,
    TIME
}
