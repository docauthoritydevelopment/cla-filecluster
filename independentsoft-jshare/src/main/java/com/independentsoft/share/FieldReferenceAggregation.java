package com.independentsoft.share;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

/**
 * The Class FieldReferenceAggregation.
 */
public class FieldReferenceAggregation {

    private String name;
    private FieldAggregation type = FieldAggregation.NONE;

    /**
     * Instantiates a new field reference aggregation.
     */
    public FieldReferenceAggregation()
    {
    }

    FieldReferenceAggregation(XMLStreamReader reader) throws XMLStreamException
    {
        parse(reader);
    }

    private void parse(XMLStreamReader reader) throws XMLStreamException
    {
        name = reader.getAttributeValue(null, "Name");

        String typeAttribute = reader.getAttributeValue(null, "Type");

        if(typeAttribute != null && typeAttribute.length() > 0)
        {
            type = EnumUtil.parseFieldAggregation(typeAttribute);
        }

        while (reader.hasNext())
        {
            if (reader.isEndElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Aggregations") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                break;
            }
            else
            {
                reader.next();
            }
        }
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName()
    {
        return name;
    }

    /**
     * Gets the type.
     *
     * @return the type
     */
    public FieldAggregation getType()
    {
        return type;
    }
}
