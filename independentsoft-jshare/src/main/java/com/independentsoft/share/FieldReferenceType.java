package com.independentsoft.share;

/**
 * The Enum FieldReferenceType.
 */
public enum FieldReferenceType {

    RECURRENCE,
    EVENT_TYPE,
    UID,
    RECURRENCE_ID,
    EVENT_CANCEL,
    START_DATE,
    END_DATE,
    RECURRENCE_DATA,
    DURATION,
    TIME_ZONE,
    XML_TIME_ZONE,
    CP_LINK,
    LINK_URL,
    MASTER_SERIES_ITEM_ID,
    ALL_DAY_EVENT,
    NONE
}
