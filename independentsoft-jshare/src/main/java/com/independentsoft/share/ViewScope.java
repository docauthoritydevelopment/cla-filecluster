package com.independentsoft.share;

/**
 * The Enum ViewScope.
 */
public enum ViewScope {

    RECURSIVE,
    RECURSIVE_ALL,
    FILES_ONLY,
    NONE
}
