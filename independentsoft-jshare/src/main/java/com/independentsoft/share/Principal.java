package com.independentsoft.share;

/**
 * The Class Principal.
 */
public abstract class Principal {

    protected int id;
    protected boolean isHiddenInUI;
    protected String loginName;
    protected PrincipalType type = PrincipalType.NONE;
    protected String title;

    /**
     * Gets the id.
     *
     * @return the id
     */
    public int getId()
    {
        return id;
    }

    /**
     * Checks if is hidden in ui.
     *
     * @return true, if is hidden in ui
     */
    public boolean isHiddenInUI()
    {
        return isHiddenInUI;
    }

    /**
     * Gets the login name.
     *
     * @return the login name
     */
    public String getLoginName()
    {
        return loginName;
    }

    /**
     * Sets the login name.
     *
     * @param loginName the new login name
     */
    public void setLoginName(String loginName)
    {
        this.loginName = loginName;
    }

    /**
     * Gets the type.
     *
     * @return the type
     */
    public PrincipalType getType()
    {
        return type;
    }

    /**
     * Gets the title.
     *
     * @return the title
     */
    public String getTitle()
    {
        return title;
    }

    /**
     * Sets the title.
     *
     * @param title the new title
     */
    public void setTitle(String title)
    {
        this.title = title;
    }
}
