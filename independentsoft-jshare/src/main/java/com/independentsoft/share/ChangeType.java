package com.independentsoft.share;

/**
 * The Enum ChangeType.
 */
public enum ChangeType {

    ADD,
    UPDATE,
    DELETE_OBJECT,
    RENAME,
    MOVE_AWAY,
    MOVE_INTO,
    RESTORE,
    ROLE_ADD,
    ROLE_DELETE,
    ROLE_UPDATE,
    ASSIGNMENT_ADD,
    ASSIGNMENT_DELETE,
    MEMBER_ADD,
    MEMBER_DELETE,
    SYSTEM_UPDATE,
    NAVIGATION,
    SCOPE_ADD,
    SCOPE_DELETE,
    NONE    
}
