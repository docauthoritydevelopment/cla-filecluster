package com.independentsoft.share;

/**
 * The Enum UrlFieldDisplayFormat.
 */
public enum UrlFieldDisplayFormat {

    HYPERLINK,
    IMAGE,
    NONE
}
