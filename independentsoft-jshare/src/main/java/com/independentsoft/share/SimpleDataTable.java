package com.independentsoft.share;

import java.util.ArrayList;
import java.util.List;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

/**
 * The Class SimpleDataTable.
 */
public class SimpleDataTable {

    private List<SimpleDataRow> rows = new ArrayList<SimpleDataRow>();

    /**
     * Instantiates a new simple data table.
     */
    public SimpleDataTable()
    {
    }

    SimpleDataTable(XMLStreamReader reader) throws XMLStreamException
    {
        parse(reader);
    }

    private void parse(XMLStreamReader reader) throws XMLStreamException
    {
        while (reader.hasNext())
        {
            if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("element") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                SimpleDataRow row = new SimpleDataRow(reader);
                rows.add(row);
            }

            if (reader.isEndElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Rows") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                break;
            }
            else
            {
                reader.next();
            }
        }
    }

    /**
     * Gets the rows.
     *
     * @return the rows
     */
    public List<SimpleDataRow> getRows()
    {
        return rows;
    }
}
