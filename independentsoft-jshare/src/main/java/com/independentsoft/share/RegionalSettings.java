package com.independentsoft.share;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

/**
 * The Class RegionalSettings.
 */
public class RegionalSettings {

    private int adjustHijriDays = Integer.MIN_VALUE;
    private CalendarType alternateCalendarType = CalendarType.NONE;
    private String am;
    private int collation;
    private Locale collationLocale = Locale.NONE;
    private int dateFormat = Integer.MIN_VALUE;
    private String dateSeparator;
    private String decimalSeparator;
    private String digitGrouping;
    private int firstDayOfWeek = Integer.MIN_VALUE;
    private int firstWeekOfYear = Integer.MIN_VALUE;
    private boolean isEastAsia;
    private boolean isRightToLeft;
    private boolean isUIRightToLeft;
    private String listSeparator;
    private Locale locale = Locale.NONE;  //LocaleId
    private String negativeSign;
    private int negativeNumberMode = -1;
    private String pm;
    private String positiveSign;
    private boolean showWeeks;
    private String thousandSeparator;
    private int timeMarkerPosition = -1;
    private String timeSeparator;
    private int workDayEndHour = -1;
    private int workDays = -1;
    private int workDayStartHour = -1;

    private String language;
    private String advanceHijri;
    private CalendarType calendarType = CalendarType.NONE;
    private boolean isTime24;
    private int timeZone;
    private String sortOrder;
    private boolean presence;

    /**
     * Instantiates a new regional settings.
     */
    public RegionalSettings()
    {
    }

    RegionalSettings(InputStream inputStream) throws XMLStreamException
    {
        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        XMLStreamReader reader = xmlInputFactory.createXMLStreamReader(inputStream);

        parse(reader);
    }

    RegionalSettings(String xml) throws XMLStreamException
    {
        parse(xml);
    }

    private void parse(XMLStreamReader reader) throws XMLStreamException
    {
        while (reader.hasNext())
        {
            if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("properties") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices/metadata"))
            {
                while (reader.hasNext())
                {
                    if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("AdjustHijriDays") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            adjustHijriDays = Integer.parseInt(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("AlternateCalendarType") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            alternateCalendarType = EnumUtil.parseCalendarType(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("AM") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        am = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("CalendarType") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            calendarType = EnumUtil.parseCalendarType(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Collation") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            collation = Integer.parseInt(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("CollationLCID") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            collationLocale = EnumUtil.parseLocale(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("DateFormat") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            dateFormat = Integer.parseInt(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("DateSeparator") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        dateSeparator = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("DecimalSeparator") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        decimalSeparator = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("DigitGrouping") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        digitGrouping = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("FirstDayOfWeek") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            firstDayOfWeek = Integer.parseInt(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("FirstWeekOfYear") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            firstWeekOfYear = Integer.parseInt(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("IsEastAsia") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            isEastAsia = Boolean.parseBoolean(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("IsRightToLeft") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            isRightToLeft = Boolean.parseBoolean(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("IsUIRightToLeft") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            isUIRightToLeft = Boolean.parseBoolean(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("ListSeparator") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        listSeparator = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("LocaleId") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            locale = EnumUtil.parseLocale(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("NegativeSign") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        negativeSign = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("NegNumberMode") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            negativeNumberMode = Integer.parseInt(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("PM") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        pm = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("PositiveSign") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        positiveSign = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("ShowWeeks") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            showWeeks = Boolean.parseBoolean(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("ThousandSeparator") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        thousandSeparator = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Time24") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            isTime24 = Boolean.parseBoolean(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("TimeMarkerPosition") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            timeMarkerPosition = Integer.parseInt(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("TimeSeparator") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        timeSeparator = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("WorkDayEndHour") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            workDayEndHour = Integer.parseInt(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("WorkDays") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            workDays = Integer.parseInt(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("WorkDayStartHour") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            workDayStartHour = Integer.parseInt(stringValue);
                        }
                    }

                    if (reader.isEndElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("properties") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices/metadata"))
                    {
                        break;
                    }
                    else
                    {
                        reader.next();
                    }
                }
            }

            if (reader.isEndElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("entry") && reader.getNamespaceURI().equals("http://www.w3.org/2005/Atom"))
            {
                break;
            }
            else
            {
                reader.next();
            }
        }
    }

    private void parse(String xml) throws XMLStreamException
    {
        byte[] buffer = null;

        try
        {
            buffer = xml.getBytes("UTF-8");
        }
        catch(UnsupportedEncodingException ex)
        {
            System.err.println(ex.getMessage());
        }

        ByteArrayInputStream inputStream = new ByteArrayInputStream(buffer);

        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        XMLStreamReader reader = xmlInputFactory.createXMLStreamReader(inputStream);

        while (reader.hasNext() && reader.next() > 0)
        {
            if (reader.isStartElement() && reader.getLocalName() != null && reader.getLocalName().equals("RegionalSettings"))
            {
                while (reader.hasNext())
                {
                    if (reader.isStartElement() && reader.getLocalName() != null && reader.getLocalName().equals("Language"))
                    {
                        language = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getLocalName().equals("Locale"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            locale = EnumUtil.parseLocale(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getLocalName().equals("AdvanceHijri"))
                    {
                        advanceHijri = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getLocalName().equals("CalendarType"))
                    {
                        String calendarTypeString = reader.getElementText();

                        if (calendarTypeString != null && calendarTypeString.length() > 0)
                        {
                            calendarType = EnumUtil.parseCalendarType(calendarTypeString);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getLocalName().equals("Time24"))
                    {
                        String time24String = reader.getElementText();

                        if (time24String != null && time24String.length() > 0)
                        {
                            isTime24 = Boolean.parseBoolean(time24String);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getLocalName().equals("TimeZone"))
                    {
                        String timeZoneString = reader.getElementText();

                        if (timeZoneString != null && timeZoneString.length() > 0)
                        {
                            timeZone = Integer.parseInt(timeZoneString);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getLocalName().equals("SortOrder"))
                    {
                        sortOrder = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getLocalName().equals("Presence"))
                    {
                        String presenceString = reader.getElementText();

                        if (presenceString != null && presenceString.length() > 0)
                        {
                            presence = Boolean.parseBoolean(presenceString);
                        }
                    }

                    if (reader.isEndElement() && reader.getLocalName() != null && reader.getLocalName().equals("RegionalSettings"))
                    {
                        break;
                    }
                    else
                    {
                        reader.next();
                    }
                }
            }
        }
    }

    /**
     * Gets the adjust hijri days.
     *
     * @return the adjust hijri days
     */
    public int getAdjustHijriDays()
    {
        return adjustHijriDays;
    }

    /**
     * Gets the alternate calendar type.
     *
     * @return the alternate calendar type
     */
    public CalendarType getAlternateCalendarType()
    {
        return alternateCalendarType;
    }

    /**
     * Gets the am.
     *
     * @return the am
     */
    public String getAm()
    {
        return am;
    }

    /**
     * Gets the collation.
     *
     * @return the collation
     */
    public int getCollation()
    {
        return collation;
    }

    /**
     * Gets the collation locale.
     *
     * @return the collation locale
     */
    public Locale getCollationLocale()
    {
        return collationLocale;
    }

    /**
     * Gets the date format.
     *
     * @return the date format
     */
    public int getDateFormat()
    {
        return dateFormat;
    }

    /**
     * Gets the date separator.
     *
     * @return the date separator
     */
    public String getDateSeparator()
    {
        return dateSeparator;
    }

    /**
     * Gets the decimal separator.
     *
     * @return the decimal separator
     */
    public String getDecimalSeparator()
    {
        return decimalSeparator;
    }

    /**
     * Gets the digit grouping.
     *
     * @return the digit grouping
     */
    public String getDigitGrouping()
    {
        return digitGrouping;
    }

    /**
     * Gets the first day of week.
     *
     * @return the first day of week
     */
    public int getFirstDayOfWeek()
    {
        return firstDayOfWeek;
    }

    /**
     * Gets the first week of year.
     *
     * @return the first week of year
     */
    public int getFirstWeekOfYear()
    {
        return firstWeekOfYear;
    }

    /**
     * Checks if is east asia.
     *
     * @return true, if is east asia
     */
    public boolean isEastAsia()
    {
        return isEastAsia;
    }

    /**
     * Checks if is right to left.
     *
     * @return true, if is right to left
     */
    public boolean isRightToLeft()
    {
        return isRightToLeft;
    }

    /**
     * Checks if is UI right to left.
     *
     * @return true, if is UI right to left
     */
    public boolean isUIRightToLeft()
    {
        return isUIRightToLeft;
    }

    /**
     * Gets the list separator.
     *
     * @return the list separator
     */
    public String getListSeparator()
    {
        return listSeparator;
    }

    /**
     * Gets the locale.
     *
     * @return the locale
     */
    public Locale getLocale()
    {
        return locale;
    }

    /**
     * Gets the negative sign.
     *
     * @return the negative sign
     */
    public String getNegativeSign()
    {
        return negativeSign;
    }

    /**
     * Gets the negative number mode.
     *
     * @return the negative number mode
     */
    public int getNegativeNumberMode()
    {
        return negativeNumberMode;
    }

    /**
     * Gets the pm.
     *
     * @return the pm
     */
    public String getPm()
    {
        return pm;
    }

    /**
     * Gets the positive sign.
     *
     * @return the positive sign
     */
    public String getPositiveSign()
    {
        return positiveSign;
    }

    /**
     * Show weeks.
     *
     * @return true, if successful
     */
    public boolean showWeeks()
    {
        return showWeeks;
    }

    /**
     * Gets the thousand separator.
     *
     * @return the thousand separator
     */
    public String getThousandSeparator()
    {
        return thousandSeparator;
    }

    /**
     * Gets the time marker position.
     *
     * @return the time marker position
     */
    public int getTimeMarkerPosition()
    {
        return timeMarkerPosition;
    }

    /**
     * Gets the time separator.
     *
     * @return the time separator
     */
    public String getTimeSeparator()
    {
        return timeSeparator;
    }

    /**
     * Gets the work day end hour.
     *
     * @return the work day end hour
     */
    public int getWorkDayEndHour()
    {
        return workDayEndHour;
    }

    /**
     * Gets the work days.
     *
     * @return the work days
     */
    public int getWorkDays()
    {
        return workDays;
    }

    /**
     * Gets the work day start hour.
     *
     * @return the work day start hour
     */
    public int getWorkDayStartHour()
    {
        return workDayStartHour;
    }

    /**
     * Gets the language.
     *
     * @return the language
     */
    public String getLanguage()
    {
        return language;
    }

    /**
     * Gets the advance hijri.
     *
     * @return the advance hijri
     */
    public String getAdvanceHijri()
    {
        return advanceHijri;
    }

    /**
     * Gets the calendar type.
     *
     * @return the calendar type
     */
    public CalendarType getCalendarType()
    {
        return calendarType;
    }

    /**
     * Checks if is time24.
     *
     * @return true, if is time24
     */
    public boolean isTime24()
    {
        return isTime24;
    }

    /**
     * Gets the time zone.
     *
     * @return the time zone
     */
    public int getTimeZone()
    {
        return timeZone;
    }

    /**
     * Gets the sort order.
     *
     * @return the sort order
     */
    public String getSortOrder()
    {
        return sortOrder;
    }

    /**
     * Gets the presence.
     *
     * @return the presence
     */
    public boolean getPresence()
    {
        return presence;
    }
}
