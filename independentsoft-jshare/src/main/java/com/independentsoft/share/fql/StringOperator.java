package com.independentsoft.share.fql;

/**
 * The Class StringOperator.
 */
public class StringOperator implements IRestriction {

    private String text;
    private StringOperatorMode mode = StringOperatorMode.NONE;
    private int maximumDistance = -1;
    private int weight = -1;
    private boolean enableLinguistics = true;
    private boolean enableWildcard = true;

    /**
     * Instantiates a new string operator.
     *
     * @param text the text
     */
    public StringOperator(String text)
    {
        this.text = text;
    }

    /**
     * Instantiates a new string operator.
     *
     * @param text the text
     * @param mode the mode
     */
    public StringOperator(String text, StringOperatorMode mode)
    {
        this.text = text;
        this.mode = mode;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        String stringValue = "";

        if (text != null && text.length() > 0)
        {
            String parameters = "";

            if (mode != StringOperatorMode.NONE)
            {
                if (parameters.length() > 0)
                {
                    parameters += ", ";
                }

                parameters += "mode=\"" + parseStringOperatorMode(mode) + "\"";
            }

            if (maximumDistance > -1)
            {
                if (parameters.length() > 0)
                {
                    parameters += ", ";
                }

                parameters += "N=" + maximumDistance;
            }

            if (weight > -1)
            {
                if (parameters.length() > 0)
                {
                    parameters += ", ";
                }

                parameters += "weight=" + weight;
            }

            if (!enableLinguistics)
            {
                if (parameters.length() > 0)
                {
                    parameters += ", ";
                }

                parameters += "linguistics=off";
            }
            else
            {
                if (parameters.length() > 0)
                {
                    parameters += ", ";
                }

                parameters += "linguistics=on";
            }

            if (!enableWildcard)
            {
                if (parameters.length() > 0)
                {
                    parameters += ", ";
                }

                parameters += "wildcard=off";
            }
            else
            {
                if (parameters.length() > 0)
                {
                    parameters += ", ";
                }

                parameters += "wildcard=on";
            }

            stringValue += "STRING(\"" + text + "\"" + parameters + ")";
        }

        return stringValue;
    }

    private String parseStringOperatorMode(StringOperatorMode parameter)
    {
        if (parameter == StringOperatorMode.PHRASE)
        {
            return "phrase";
        }
        else if (parameter == StringOperatorMode.AND)
        {
            return "and";
        }
        else if (parameter == StringOperatorMode.OR)
        {
            return "or";
        }
        else if (parameter == StringOperatorMode.ANY)
        {
            return "any";
        }
        else if (parameter == StringOperatorMode.NEAR)
        {
            return "near";
        }
        else if (parameter == StringOperatorMode.ORDERED_NEAR)
        {
            return "onear";
        }
        else
        {
            return "none";
        }
    }

    /**
     * Gets the mode.
     *
     * @return the mode
     */
    public StringOperatorMode getMode()
    {
        return mode;
    }

    /**
     * Sets the mode.
     *
     * @param mode the new mode
     */
    public void setMode(StringOperatorMode mode)
    {
        this.mode = mode;
    }

    /**
     * Gets the maximum distance.
     *
     * @return the maximum distance
     */
    public int getMaximumDistance()
    {
        return maximumDistance;
    }

    /**
     * Sets the maximum distance.
     *
     * @param maximumDistance the new maximum distance
     */
    public void setMaximumDistance(int maximumDistance)
    {
        this.maximumDistance = maximumDistance;
    }

    /**
     * Gets the weight.
     *
     * @return the weight
     */
    public int getWeight()
    {
        return weight;
    }

    /**
     * Sets the weight.
     *
     * @param weight the new weight
     */
    public void setWeight(int weight)
    {
        this.weight = weight;
    }

    /**
     * Checks if is linguistics enabled.
     *
     * @return true, if is linguistics enabled
     */
    public boolean isLinguisticsEnabled()
    {
        return enableLinguistics;
    }

    /**
     * Enable linguistics.
     *
     * @param enableLinguistics the enable linguistics
     */
    public void enableLinguistics(boolean enableLinguistics)
    {
        this.enableLinguistics = enableLinguistics;
    }

    /**
     * Checks if is wildcard enabled.
     *
     * @return true, if is wildcard enabled
     */
    public boolean isWildcardEnabled()
    {
        return enableWildcard;
    }

    /**
     * Enable wildcard.
     *
     * @param enableWildcard the enable wildcard
     */
    public void enableWildcard(boolean enableWildcard)
    {
        this.enableWildcard = enableWildcard;
    }
}
