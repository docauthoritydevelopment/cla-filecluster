package com.independentsoft.share.fql;

/**
 * The Class Not.
 */
public class Not implements IRestriction {

    private String expression;

    /**
     * Instantiates a new not.
     *
     * @param expression the expression
     */
    public Not(String expression)
    {
        if (expression == null)
        {
            throw new IllegalArgumentException("expression");
        }

        this.expression = expression;
    }

    /**
     * Instantiates a new not.
     *
     * @param restriction the restriction
     */
    public Not(IRestriction restriction)
    {
        if(restriction == null)
        {
            throw new IllegalArgumentException("restriction");
        }

        this.expression = restriction.toString();
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        String stringValue = "NOT(" + expression + ")";

        return stringValue;
    }
}
