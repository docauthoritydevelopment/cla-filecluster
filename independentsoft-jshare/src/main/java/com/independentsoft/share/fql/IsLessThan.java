package com.independentsoft.share.fql;

import java.util.Date;

import com.independentsoft.share.Util;

/**
 * The Class IsLessThan.
 */
public class IsLessThan extends IsWithinRange {

    /**
     * Instantiates a new checks if is less than.
     *
     * @param propertyName the property name
     * @param value the value
     */
    public IsLessThan(String propertyName, long value)
    {
        if (propertyName == null)
        {
            throw new IllegalArgumentException("propertyName");
        }

        this.propertyName = propertyName;
        this.start = Long.toString(value);
    }

    /**
     * Instantiates a new checks if is less than.
     *
     * @param propertyName the property name
     * @param value the value
     */
    public IsLessThan(String propertyName, double value)
    {
        if (propertyName == null)
        {
            throw new IllegalArgumentException("propertyName");
        }

        this.propertyName = propertyName;
        this.start = Double.toString(value);
    }

    /**
     * Instantiates a new checks if is less than.
     *
     * @param propertyName the property name
     * @param value the value
     */
    public IsLessThan(String propertyName, Date value)
    {
        if (propertyName == null)
        {
            throw new IllegalArgumentException("propertyName");
        }

        if (value == null)
        {
            throw new IllegalArgumentException("value");
        }

        this.propertyName = propertyName;
        this.start = Util.toUniversalTime(value);
    }

    /* (non-Javadoc)
     * @see com.independentsoft.share.fql.IsWithinRange#toString()
     */
    @Override
    public String toString()
    {
        String stringValue = propertyName + ":RANGE(min, " + end + ", to=LT)";

        return stringValue;
    }
}
