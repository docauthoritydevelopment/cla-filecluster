package com.independentsoft.share.fql;

import java.util.ArrayList;
import java.util.List;

/**
 * The Class Phrase.
 */
public class Phrase implements IRestriction {

    private List<String> terms = new ArrayList<String>();

    /**
     * Instantiates a new phrase.
     *
     * @param term the term
     */
    public Phrase(String term)
    {
        if (term == null)
        {
            throw new IllegalArgumentException("term");
        }

        this.terms.add(term);
    }

    /**
     * Instantiates a new phrase.
     *
     * @param term1 the term1
     * @param term2 the term2
     */
    public Phrase(String term1, String term2)
    {
        if (term1 == null)
        {
            throw new IllegalArgumentException("term1");
        }

        if (term2 == null)
        {
            throw new IllegalArgumentException("term2");
        }

        this.terms.add(term1);
        this.terms.add(term2);
    }

    /**
     * Instantiates a new phrase.
     *
     * @param term1 the term1
     * @param term2 the term2
     * @param term3 the term3
     */
    public Phrase(String term1, String term2, String term3)
    {
        if (term1 == null)
        {
            throw new IllegalArgumentException("term1");
        }

        if (term2 == null)
        {
            throw new IllegalArgumentException("term2");
        }

        if (term3 == null)
        {
            throw new IllegalArgumentException("term3");
        }

        this.terms.add(term1);
        this.terms.add(term2);
        this.terms.add(term3);
    }

    /**
     * Instantiates a new phrase.
     *
     * @param term1 the term1
     * @param term2 the term2
     * @param term3 the term3
     * @param term4 the term4
     */
    public Phrase(String term1, String term2, String term3, String term4)
    {
        if (term1 == null)
        {
            throw new IllegalArgumentException("term1");
        }

        if (term2 == null)
        {
            throw new IllegalArgumentException("term2");
        }

        if (term3 == null)
        {
            throw new IllegalArgumentException("term3");
        }

        if (term4 == null)
        {
            throw new IllegalArgumentException("term4");
        }

        this.terms.add(term1);
        this.terms.add(term2);
        this.terms.add(term3);
        this.terms.add(term4);
    }

    /**
     * Instantiates a new phrase.
     *
     * @param terms the terms
     */
    public Phrase(List<String> terms)
    {
        if (terms == null)
        {
            throw new IllegalArgumentException("terms");
        }

        this.terms = terms;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        String stringValue = "";

        if (terms.size() > 1)
        {
            String termsString = "";

            for (int i = 0; i < terms.size(); i++)
            {
                stringValue += terms.get(i).toString();

                if (i < terms.size() - 1)
                {
                    termsString += ", ";
                }
            }

            stringValue += "PHRASE(" + termsString + ")";
        }

        return stringValue;
    }
}
