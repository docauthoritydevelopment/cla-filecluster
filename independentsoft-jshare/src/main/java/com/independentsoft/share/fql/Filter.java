package com.independentsoft.share.fql;

/**
 * The Class Filter.
 */
public class Filter implements IRestriction {

    private String expression;

    /**
     * Instantiates a new filter.
     *
     * @param expression the expression
     */
    public Filter(String expression)
    {
        if (expression == null)
        {
            throw new IllegalArgumentException("expression");
        }

        this.expression = "\"" + expression + "\"";
    }

    /**
     * Instantiates a new filter.
     *
     * @param restriction the restriction
     */
    public Filter(IRestriction restriction)
    {
        if(restriction == null)
        {
            throw new IllegalArgumentException("restriction");
        }

        this.expression = restriction.toString();
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        String stringValue = "FILTER(" + expression + ")";

        return stringValue;
    }
}
