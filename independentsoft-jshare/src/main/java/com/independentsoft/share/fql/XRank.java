package com.independentsoft.share.fql;

/**
 * The Class XRank.
 */
public class XRank implements IRestriction {

    private String matchExpression;
    private String rankExpression;
    private int count = -1;
    private double normalizedBoost = -1;
    private double constantBoost = -1;
    private double standardDeviationBoost = -1;
    private double averageBoost = -1;
    private double rangeBoost = -1;
    private double percentageBoost = -1;

    /**
     * Instantiates a new x rank.
     *
     * @param matchExpression the match expression
     * @param rankExpression the rank expression
     */
    public XRank(String matchExpression, String rankExpression)
    {
        if (matchExpression == null)
        {
            throw new IllegalArgumentException("matchExpression");
        }

        if (rankExpression == null)
        {
            throw new IllegalArgumentException("rankExpression");
        }

        this.matchExpression = matchExpression;
        this.rankExpression = rankExpression;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        String parameters = "";

        if(count > -1)
        {
            if (parameters.length() > 0)
            {
                parameters += ", ";
            }

            parameters += "N=" + count;
        }

        if (normalizedBoost > -1)
        {
            if (parameters.length() > 0)
            {
                parameters += ", ";
            }

            parameters += "Nb=" + normalizedBoost;
        }

        if (constantBoost > -1)
        {
            if (parameters.length() > 0)
            {
                parameters += ", ";
            }

            parameters += "cb=" + constantBoost;
        }

        if (standardDeviationBoost > -1)
        {
            if (parameters.length() > 0)
            {
                parameters += ", ";
            }

            parameters += "stdb=" + standardDeviationBoost;
        }

        if (averageBoost > -1)
        {
            if (parameters.length() > 0)
            {
                parameters += ", ";
            }

            parameters += "avgb=" + averageBoost;
        }

        if (rangeBoost > -1)
        {
            if (parameters.length() > 0)
            {
                parameters += ", ";
            }

            parameters += "rb=" + rangeBoost;
        }

        if (percentageBoost > -1)
        {
            if (parameters.length() > 0)
            {
                parameters += ", ";
            }

            parameters += "pb=" + percentageBoost;
        }

        String stringValue = "XRANK(\"" + matchExpression + "\", \"" + rankExpression + "\"" + parameters + ")";

        return stringValue;
    }

    /**
     * Gets the count.
     *
     * @return the count
     */
    public int getCount()
    {
        return count;
    }

    /**
     * Sets the count.
     *
     * @param count the new count
     */
    public void setCount(int count)
    {
        this.count = count;
    }

    /**
     * Gets the normalized boost.
     *
     * @return the normalized boost
     */
    public double getNormalizedBoost()
    {
        return normalizedBoost;
    }

    /**
     * Sets the normalized boost.
     *
     * @param normalizedBoost the new normalized boost
     */
    public void setNormalizedBoost(double normalizedBoost)
    {
        this.normalizedBoost = normalizedBoost;
    }

    /**
     * Gets the constant boost.
     *
     * @return the constant boost
     */
    public double getConstantBoost()
    {
        return constantBoost;
    }

    /**
     * Sets the constant boost.
     *
     * @param constantBoost the new constant boost
     */
    public void setConstantBoost(double constantBoost)
    {
        this.constantBoost = constantBoost;
    }

    /**
     * Gets the standard deviation boost.
     *
     * @return the standard deviation boost
     */
    public double getStandardDeviationBoost()
    {
        return standardDeviationBoost;
    }

    /**
     * Sets the standard deviation boost.
     *
     * @param standardDeviationBoost the new standard deviation boost
     */
    public void setStandardDeviationBoost(double standardDeviationBoost)
    {
        this.standardDeviationBoost = standardDeviationBoost;
    }

    /**
     * Gets the average boost.
     *
     * @return the average boost
     */
    public double getAverageBoost()
    {
        return averageBoost;
    }

    /**
     * Sets the average boost.
     *
     * @param averageBoost the new average boost
     */
    public void setAverageBoost(double averageBoost)
    {
        this.averageBoost = averageBoost;
    }

    /**
     * Gets the range boost.
     *
     * @return the range boost
     */
    public double getRangeBoost()
    {
        return rangeBoost;
    }

    /**
     * Sets the range boost.
     *
     * @param rangeBoost the new range boost
     */
    public void setRangeBoost(double rangeBoost)
    {
        this.rangeBoost = rangeBoost;
    }

    /**
     * Gets the percentage boost.
     *
     * @return the percentage boost
     */
    public double getPercentageBoost()
    {
        return percentageBoost;
    }

    /**
     * Sets the percentage boost.
     *
     * @param percentageBoost the new percentage boost
     */
    public void setPercentageBoost(double percentageBoost)
    {
        this.percentageBoost = percentageBoost;
    }
}
