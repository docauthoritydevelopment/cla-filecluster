package com.independentsoft.share.fql;

import java.util.ArrayList;
import java.util.List;

/**
 * The Class IntegerOperator.
 */
public class IntegerOperator implements IRestriction {

    private long value = Long.MIN_VALUE;
    private List<Long> values = new ArrayList<Long>();

    /**
     * Instantiates a new integer operator.
     *
     * @param value the value
     */
    public IntegerOperator(long value)
    {
        this.value = value;
    }

    /**
     * Instantiates a new integer operator.
     *
     * @param values the values
     */
    public IntegerOperator(List<Long> values)
    {
        if (values == null)
        {
            throw new IllegalArgumentException("values");
        }

        this.values = values;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        String stringValue = "";

        if (values.size() > 0)
        {
            String valuesString = "";

            for (int i = 0; i < values.size(); i++)
            {
                stringValue += values.get(i).toString();

                if (i < values.size() - 1)
                {
                    valuesString += ", ";
                }
            }

            stringValue += "INT(\"" + valuesString + "\")";
        }
        else if (value > Long.MIN_VALUE)
        {
            stringValue += "INT(" + value + ")";
        }

        return stringValue;
    }
}
