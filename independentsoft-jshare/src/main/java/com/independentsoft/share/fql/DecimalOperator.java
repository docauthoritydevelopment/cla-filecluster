package com.independentsoft.share.fql;

/**
 * The Class DecimalOperator.
 */
public class DecimalOperator implements IRestriction {

    private double value = Double.MIN_VALUE;

    /**
     * Instantiates a new decimal operator.
     *
     * @param value the value
     */
    public DecimalOperator(double value)
    {
        this.value = value;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        String stringValue = "";

        if (value > Double.MIN_VALUE)
        {
            stringValue += "DECIMAL(" + Double.toString(value) + ")";
        }

        return stringValue;
    }
}
