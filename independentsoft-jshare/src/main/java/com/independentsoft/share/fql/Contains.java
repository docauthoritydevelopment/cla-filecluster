package com.independentsoft.share.fql;

import java.util.Date;

import com.independentsoft.share.Util;

/**
 * The Class Contains.
 */
public class Contains implements IRestriction {

    private String propertyName;
    private String value;

    /**
     * Instantiates a new contains.
     *
     * @param propertyName the property name
     * @param value the value
     */
    public Contains(String propertyName, long value)
    {
        if (propertyName == null)
        {
            throw new IllegalArgumentException("propertyName");
        }

        this.propertyName = propertyName;
        this.value = Long.toString(value);
    }

    /**
     * Instantiates a new contains.
     *
     * @param propertyName the property name
     * @param value the value
     */
    public Contains(String propertyName, double value)
    {
        if (propertyName == null)
        {
            throw new IllegalArgumentException("propertyName");
        }

        this.propertyName = propertyName;
        this.value = Double.toString(value);
    }

    /**
     * Instantiates a new contains.
     *
     * @param propertyName the property name
     * @param value the value
     */
    public Contains(String propertyName, boolean value)
    {
        if (propertyName == null)
        {
            throw new IllegalArgumentException("propertyName");
        }

        this.propertyName = propertyName;
        this.value = Boolean.toString(value).toLowerCase();
    }

    /**
     * Instantiates a new contains.
     *
     * @param propertyName the property name
     * @param value the value
     */
    public Contains(String propertyName, Date value)
    {
        if (propertyName == null)
        {
            throw new IllegalArgumentException("propertyName");
        }

        if (value == null)
        {
            throw new IllegalArgumentException("value");
        }

        this.propertyName = propertyName;
        this.value = Util.toUniversalTime(value);
    }

    /**
     * Instantiates a new contains.
     *
     * @param propertyName the property name
     * @param value the value
     */
    public Contains(String propertyName, String value)
    {
        if (propertyName == null)
        {
            throw new IllegalArgumentException("propertyName");
        }

        if (value == null)
        {
            throw new IllegalArgumentException("value");
        }

        this.propertyName = propertyName;
        this.value = "\"" + value + "\"";
    }

    /**
     * Instantiates a new contains.
     *
     * @param value the value
     */
    public Contains(String value)
    {
        if (value == null)
        {
            throw new IllegalArgumentException("value");
        }

        this.value = "\"" + value + "\"";
    }

    /**
     * Instantiates a new contains.
     *
     * @param propertyName the property name
     * @param restriction the restriction
     */
    public Contains(String propertyName, IRestriction restriction)
    {
        if (propertyName == null)
        {
            throw new IllegalArgumentException("propertyName");
        }

        if (restriction == null)
        {
            throw new IllegalArgumentException("restriction");
        }

        this.propertyName = propertyName;
        this.value = restriction.toString();
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        if (propertyName != null)
        {
            String stringValue = propertyName + " : " + value;

            return stringValue;
        }
        else
        {
            String stringValue = value;

            return stringValue;
        }
    }
}
