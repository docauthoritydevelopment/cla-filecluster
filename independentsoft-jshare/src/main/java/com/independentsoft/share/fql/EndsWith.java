package com.independentsoft.share.fql;

/**
 * The Class EndsWith.
 */
public class EndsWith implements IRestriction {

    private String propertyName;
    private String expression;

    /**
     * Instantiates a new ends with.
     *
     * @param expression the expression
     */
    public EndsWith(String expression)
    {
        if (expression == null)
        {
            throw new IllegalArgumentException("expression");
        }

        this.expression = "\"" + expression + "\"";
    }

    /**
     * Instantiates a new ends with.
     *
     * @param propertyName the property name
     * @param expression the expression
     */
    public EndsWith(String propertyName, String expression)
    {
        if (propertyName == null)
        {
            throw new IllegalArgumentException("propertyName");
        }

        if (expression == null)
        {
            throw new IllegalArgumentException("expression");
        }

        this.propertyName = propertyName;
        this.expression = "\"" + expression + "\"";
    }

    /**
     * Instantiates a new ends with.
     *
     * @param restriction the restriction
     */
    public EndsWith(IRestriction restriction)
    {
        if (restriction == null)
        {
            throw new IllegalArgumentException("restriction");
        }

        this.expression = restriction.toString();
    }

    /**
     * Instantiates a new ends with.
     *
     * @param propertyName the property name
     * @param restriction the restriction
     */
    public EndsWith(String propertyName, IRestriction restriction)
    {
        if (propertyName == null)
        {
            throw new IllegalArgumentException("propertyName");
        }

        if (restriction == null)
        {
            throw new IllegalArgumentException("restriction");
        }

        this.propertyName = propertyName;
        this.expression = restriction.toString();
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        if (propertyName == null)
        {
            String stringValue = "ENDS-WITH(" + expression + ")";

            return stringValue;
        }
        else
        {
            String stringValue = propertyName + ":ENDS-WITH(" + expression + ")";

            return stringValue;
        }
    }
}
