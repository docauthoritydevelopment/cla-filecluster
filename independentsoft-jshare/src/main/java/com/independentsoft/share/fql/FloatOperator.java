package com.independentsoft.share.fql;

/**
 * The Class FloatOperator.
 */
public class FloatOperator implements IRestriction {

    private float value = Float.MIN_VALUE;

    /**
     * Instantiates a new float operator.
     *
     * @param value the value
     */
    public FloatOperator(float value)
    {
        this.value = value;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        String stringValue = "";

        if (value > Float.MIN_VALUE)
        {
            stringValue += "FLOAT(" + Float.toString(value) + ")";
        }

        return stringValue;
    }
}
