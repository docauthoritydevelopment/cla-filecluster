package com.independentsoft.share.fql;

/**
 * The Class Count.
 */
public class Count implements IRestriction {

    private String propertyName;
    private String expression;
    private int from = -1;
    private int to = -1;

    /**
     * Instantiates a new count.
     *
     * @param expression the expression
     */
    public Count(String expression)
    {
        if (expression == null)
        {
            throw new IllegalArgumentException("expression");
        }

        this.expression = "\"" + expression + "\"";
    }

    /**
     * Instantiates a new count.
     *
     * @param expression the expression
     * @param from the from
     */
    public Count(String expression, int from)
    {
        if (expression == null)
        {
            throw new IllegalArgumentException("expression");
        }

        this.expression = "\"" + expression + "\"";
        this.from = from;
    }

    /**
     * Instantiates a new count.
     *
     * @param expression the expression
     * @param from the from
     * @param to the to
     */
    public Count(String expression, int from, int to)
    {
        if (expression == null)
        {
            throw new IllegalArgumentException("expression");
        }

        this.expression = "\"" + expression + "\"";
        this.from = from;
        this.to = to;
    }

    /**
     * Instantiates a new count.
     *
     * @param propertyName the property name
     * @param expression the expression
     */
    public Count(String propertyName, String expression)
    {
        if (propertyName == null)
        {
            throw new IllegalArgumentException("propertyName");
        }

        if (expression == null)
        {
            throw new IllegalArgumentException("expression");
        }

        this.propertyName = propertyName;
        this.expression = "\"" + expression + "\"";
    }

    /**
     * Instantiates a new count.
     *
     * @param propertyName the property name
     * @param expression the expression
     * @param from the from
     */
    public Count(String propertyName, String expression, int from)
    {
        if (propertyName == null)
        {
            throw new IllegalArgumentException("propertyName");
        }

        if (expression == null)
        {
            throw new IllegalArgumentException("expression");
        }

        this.propertyName = propertyName;
        this.expression = "\"" + expression + "\"";
        this.from = from;
    }

    /**
     * Instantiates a new count.
     *
     * @param propertyName the property name
     * @param expression the expression
     * @param from the from
     * @param to the to
     */
    public Count(String propertyName, String expression, int from, int to)
    {
        if (propertyName == null)
        {
            throw new IllegalArgumentException("propertyName");
        }

        if (expression == null)
        {
            throw new IllegalArgumentException("expression");
        }

        this.propertyName = propertyName;
        this.expression = "\"" + expression + "\"";
        this.from = from;
        this.to = to;
    }

    /**
     * Instantiates a new count.
     *
     * @param restriction the restriction
     */
    public Count(IRestriction restriction)
    {
        if (restriction == null)
        {
            throw new IllegalArgumentException("restriction");
        }

        this.expression = restriction.toString();
    }

    /**
     * Instantiates a new count.
     *
     * @param restriction the restriction
     * @param from the from
     */
    public Count(IRestriction restriction, int from)
    {
        if (restriction == null)
        {
            throw new IllegalArgumentException("restriction");
        }

        this.expression = restriction.toString();
        this.from = from;
    }

    /**
     * Instantiates a new count.
     *
     * @param restriction the restriction
     * @param from the from
     * @param to the to
     */
    public Count(IRestriction restriction, int from, int to)
    {
        if (restriction == null)
        {
            throw new IllegalArgumentException("restriction");
        }

        this.expression = restriction.toString();
        this.from = from;
        this.to = to;
    }

    /**
     * Instantiates a new count.
     *
     * @param propertyName the property name
     * @param restriction the restriction
     */
    public Count(String propertyName, IRestriction restriction)
    {
        if (propertyName == null)
        {
            throw new IllegalArgumentException("propertyName");
        }

        if (restriction == null)
        {
            throw new IllegalArgumentException("restriction");
        }

        this.propertyName = propertyName;
        this.expression = restriction.toString();
    }

    /**
     * Instantiates a new count.
     *
     * @param propertyName the property name
     * @param restriction the restriction
     * @param from the from
     */
    public Count(String propertyName, IRestriction restriction, int from)
    {
        if (propertyName == null)
        {
            throw new IllegalArgumentException("propertyName");
        }

        if (restriction == null)
        {
            throw new IllegalArgumentException("restriction");
        }

        this.propertyName = propertyName;
        this.expression = restriction.toString();
        this.from = from;
    }

    /**
     * Instantiates a new count.
     *
     * @param propertyName the property name
     * @param restriction the restriction
     * @param from the from
     * @param to the to
     */
    public Count(String propertyName, IRestriction restriction, int from, int to)
    {
        if (propertyName == null)
        {
            throw new IllegalArgumentException("propertyName");
        }

        if (restriction == null)
        {
            throw new IllegalArgumentException("restriction");
        }

        this.propertyName = propertyName;
        this.expression = restriction.toString();
        this.from = from;
        this.to = to;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        String parameters = "";

        if (from > -1)
        {
            parameters += ", from=" + from;
        }

        if (to > -1)
        {
            parameters += ", to=" + to;
        }

        if (propertyName == null)
        {
            String stringValue = "COUNT(" + expression + parameters + ")";

            return stringValue;
        }
        else
        {
            String stringValue = propertyName + ":COUNT(" + expression + parameters + ")";

            return stringValue;
        }
    }

    /**
     * Gets the from.
     *
     * @return the from
     */
    public int getFrom()
    {
        return from;
    }

    /**
     * Sets the from.
     *
     * @param from the new from
     */
    public void setFrom(int from)
    {
        this.from = from;
    }

    /**
     * Gets the to.
     *
     * @return the to
     */
    public int getTo()
    {
        return to;
    }

    /**
     * Sets the to.
     *
     * @param to the new to
     */
    public void setTo(int to)
    {
        this.to = to;
    }
}
