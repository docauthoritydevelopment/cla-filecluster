package com.independentsoft.share.fql;

import java.util.Date;

import com.independentsoft.share.Util;

/**
 * The Class IsWithinRange.
 */
public class IsWithinRange implements IRestriction {

    protected String propertyName;
    protected String start;
    protected String end;
    protected boolean isMinimumStartValue;
    protected boolean isMaximumEndValue;
    protected boolean isIncludeStartValue = true;
    protected boolean isIncludeEndValue = true;

    protected IsWithinRange()
    {
    }

    /**
     * Instantiates a new checks if is within range.
     *
     * @param propertyName the property name
     * @param start the start
     * @param end the end
     */
    public IsWithinRange(String propertyName, long start, long end)
    {
        if (propertyName == null)
        {
            throw new IllegalArgumentException("propertyName");
        }

        this.propertyName = propertyName;
        this.start = Long.toString(start);
        this.end = Long.toString(end);
    }

    /**
     * Instantiates a new checks if is within range.
     *
     * @param propertyName the property name
     * @param start the start
     * @param end the end
     */
    public IsWithinRange(String propertyName, double start, double end)
    {
        if (propertyName == null)
        {
            throw new IllegalArgumentException("propertyName");
        }

        this.propertyName = propertyName;
        this.start = Double.toString(start);
        this.end = Double.toString(end);
    }

    /**
     * Instantiates a new checks if is within range.
     *
     * @param propertyName the property name
     * @param start the start
     * @param end the end
     */
    public IsWithinRange(String propertyName, Date start, Date end)
    {
        if (propertyName == null)
        {
            throw new IllegalArgumentException("propertyName");
        }

        if (start == null)
        {
            throw new IllegalArgumentException("start");
        }

        if (end == null)
        {
            throw new IllegalArgumentException("end");
        }

        this.propertyName = propertyName;
        this.start = Util.toUniversalTime(start);
        this.end = Util.toUniversalTime(end);
    }

    /**
     * Instantiates a new checks if is within range.
     *
     * @param start the start
     * @param end the end
     */
    public IsWithinRange(long start, long end)
    {
        this.start = Long.toString(start);
        this.end = Long.toString(end);
    }

    /**
     * Instantiates a new checks if is within range.
     *
     * @param start the start
     * @param end the end
     */
    public IsWithinRange(double start, double end)
    {
        this.start = Double.toString(start);
        this.end = Double.toString(end);
    }

    /**
     * Instantiates a new checks if is within range.
     *
     * @param start the start
     * @param end the end
     */
    public IsWithinRange(Date start, Date end)
    {
        if (start == null)
        {
            throw new IllegalArgumentException("start");
        }

        if (end == null)
        {
            throw new IllegalArgumentException("end");
        }

        this.start = Util.toUniversalTime(start);
        this.end = Util.toUniversalTime(end);
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        if (isMinimumStartValue)
        {
            start = "min";
        }

        if (isMaximumEndValue)
        {
            end = "max";
        }

        String parameters = "";

        if (isIncludeStartValue)
        {
            parameters += ", from=GE";
        }
        else
        {
            parameters += ", from=GT";
        }

        if (isIncludeEndValue)
        {
            parameters += ", to=LE";
        }
        else
        {
            parameters += ", to=LE";
        }

        if (propertyName == null)
        {
            String stringValue = "RANGE(" + start + ", " + end + parameters + ")";

            return stringValue;
        }
        else
        {
            String stringValue = propertyName + ":RANGE(" + start + ", " + end + parameters + ")";

            return stringValue;
        }
    }

    /**
     * Checks if is minimum start value.
     *
     * @return true, if is minimum start value
     */
    public boolean isMinimumStartValue()
    {
        return isMinimumStartValue;
    }

    /**
     * Sets the minimum start value.
     *
     * @param isMinimumStartValue the new minimum start value
     */
    public void setMinimumStartValue(boolean isMinimumStartValue)
    {
        this.isMinimumStartValue = isMinimumStartValue;
    }

    /**
     * Checks if is maximum end value.
     *
     * @return true, if is maximum end value
     */
    public boolean isMaximumEndValue()
    {
        return isMaximumEndValue;
    }

    /**
     * Sets the maximum end value.
     *
     * @param isMaximumEndValue the new maximum end value
     */
    public void setMaximumEndValue(boolean isMaximumEndValue)
    {
        this.isMaximumEndValue = isMaximumEndValue;
    }

    /**
     * Checks if is include start value.
     *
     * @return true, if is include start value
     */
    public boolean isIncludeStartValue()
    {
        return isIncludeStartValue;
    }

    /**
     * Sets the include start value.
     *
     * @param isIncludeStartValue the new include start value
     */
    public void setIncludeStartValue(boolean isIncludeStartValue)
    {
        this.isIncludeStartValue = isIncludeStartValue;
    }

    /**
     * Checks if is include end value.
     *
     * @return true, if is include end value
     */
    public boolean isIncludeEndValue()
    {
        return isIncludeEndValue;
    }

    /**
     * Sets the include end value.
     *
     * @param isIncludeEndValue the new include end value
     */
    public void setIncludeEndValue(boolean isIncludeEndValue)
    {
        this.isIncludeEndValue = isIncludeEndValue;
    }
}
