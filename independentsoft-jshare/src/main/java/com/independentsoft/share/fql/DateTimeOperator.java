package com.independentsoft.share.fql;

/**
 * The Class DateTimeOperator.
 */
public class DateTimeOperator implements IRestriction {

    private String dateTime;

    /**
     * Instantiates a new date time operator.
     *
     * @param dateTime the date time
     */
    public DateTimeOperator(String dateTime)
    {
        if (dateTime == null)
        {
            throw new IllegalArgumentException("dateTime");
        }

        this.dateTime = dateTime;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        String stringValue = "DATETIME(\"" + dateTime + "\")";

        return stringValue;
    }
}
