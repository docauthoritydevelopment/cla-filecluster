package com.independentsoft.share.fql;

/**
 * The Class StartsWith.
 */
public class StartsWith implements IRestriction {

    private String propertyName;
    private String expression;

    /**
     * Instantiates a new starts with.
     *
     * @param expression the expression
     */
    public StartsWith(String expression)
    {
        if (expression == null)
        {
            throw new IllegalArgumentException("expression");
        }

        this.expression = "\"" + expression + "\"";
    }

    /**
     * Instantiates a new starts with.
     *
     * @param propertyName the property name
     * @param expression the expression
     */
    public StartsWith(String propertyName, String expression)
    {
        if (propertyName == null)
        {
            throw new IllegalArgumentException("propertyName");
        }

        if (expression == null)
        {
            throw new IllegalArgumentException("expression");
        }

        this.propertyName = propertyName;
        this.expression = "\"" + expression + "\"";
    }

    /**
     * Instantiates a new starts with.
     *
     * @param restriction the restriction
     */
    public StartsWith(IRestriction restriction)
    {
        if (restriction == null)
        {
            throw new IllegalArgumentException("restriction");
        }

        this.expression = restriction.toString();
    }

    /**
     * Instantiates a new starts with.
     *
     * @param propertyName the property name
     * @param restriction the restriction
     */
    public StartsWith(String propertyName, IRestriction restriction)
    {
        if (propertyName == null)
        {
            throw new IllegalArgumentException("propertyName");
        }

        if (restriction == null)
        {
            throw new IllegalArgumentException("restriction");
        }

        this.propertyName = propertyName;
        this.expression = restriction.toString();
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        if (propertyName == null)
        {
            String stringValue = "STARTS-WITH(" + expression + ")";

            return stringValue;
        }
        else
        {
            String stringValue = propertyName + ":STARTS-WITH(" + expression + ")";

            return stringValue;
        }
    }
}
