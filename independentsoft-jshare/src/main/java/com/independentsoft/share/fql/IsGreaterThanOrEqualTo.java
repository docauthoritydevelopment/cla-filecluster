package com.independentsoft.share.fql;

import java.util.Date;

import com.independentsoft.share.Util;

/**
 * The Class IsGreaterThanOrEqualTo.
 */
public class IsGreaterThanOrEqualTo extends IsWithinRange {

    /**
     * Instantiates a new checks if is greater than or equal to.
     *
     * @param propertyName the property name
     * @param value the value
     */
    public IsGreaterThanOrEqualTo(String propertyName, long value)
    {
        if (propertyName == null)
        {
            throw new IllegalArgumentException("propertyName");
        }

        this.propertyName = propertyName;
        this.start = Long.toString(value);
    }

    /**
     * Instantiates a new checks if is greater than or equal to.
     *
     * @param propertyName the property name
     * @param value the value
     */
    public IsGreaterThanOrEqualTo(String propertyName, double value)
    {
        if (propertyName == null)
        {
            throw new IllegalArgumentException("propertyName");
        }

        this.propertyName = propertyName;
        this.start = Double.toString(value);
    }

    /**
     * Instantiates a new checks if is greater than or equal to.
     *
     * @param propertyName the property name
     * @param value the value
     */
    public IsGreaterThanOrEqualTo(String propertyName, Date value)
    {
        if (propertyName == null)
        {
            throw new IllegalArgumentException("propertyName");
        }

        if (value == null)
        {
            throw new IllegalArgumentException("value");
        }

        this.propertyName = propertyName;
        this.start = Util.toUniversalTime(value);
    }

    /* (non-Javadoc)
     * @see com.independentsoft.share.fql.IsWithinRange#toString()
     */
    @Override
    public String toString()
    {
        String stringValue = propertyName + ":RANGE(" + start + ", max, from=GE)";

        return stringValue;
    }
}
