package com.independentsoft.share.fql;

import java.util.ArrayList;
import java.util.List;

/**
 * The Class And.
 */
public class And implements IRestriction {

    private List<String> terms = new ArrayList<String>();
    private List<IRestriction> restrictions = new ArrayList<IRestriction>();

    /**
     * Instantiates a new and.
     *
     * @param term1 the term1
     * @param term2 the term2
     */
    public And(String term1, String term2)
    {
        if (term1 == null)
        {
            throw new IllegalArgumentException("term1");
        }

        if (term2 == null)
        {
            throw new IllegalArgumentException("term2");
        }

        this.terms.add(term1);
        this.terms.add(term2);
    }

    /**
     * Instantiates a new and.
     *
     * @param term1 the term1
     * @param term2 the term2
     * @param term3 the term3
     */
    public And(String term1, String term2, String term3)
    {
        if (term1 == null)
        {
            throw new IllegalArgumentException("term1");
        }

        if (term2 == null)
        {
            throw new IllegalArgumentException("term2");
        }

        if (term3 == null)
        {
            throw new IllegalArgumentException("term3");
        }

        this.terms.add(term1);
        this.terms.add(term2);
        this.terms.add(term3);
    }

    /**
     * Instantiates a new and.
     *
     * @param term1 the term1
     * @param term2 the term2
     * @param term3 the term3
     * @param term4 the term4
     */
    public And(String term1, String term2, String term3, String term4)
    {
        if (term1 == null)
        {
            throw new IllegalArgumentException("term1");
        }

        if (term2 == null)
        {
            throw new IllegalArgumentException("term2");
        }

        if (term3 == null)
        {
            throw new IllegalArgumentException("term3");
        }

        if (term4 == null)
        {
            throw new IllegalArgumentException("term4");
        }

        this.terms.add(term1);
        this.terms.add(term2);
        this.terms.add(term3);
        this.terms.add(term4);
    }

    /**
     * Instantiates a new and.
     *
     * @param restriction1 the restriction1
     * @param restriction2 the restriction2
     */
    public And(IRestriction restriction1, IRestriction restriction2)
    {
        if (restriction1 == null)
        {
            throw new IllegalArgumentException("restriction1");
        }

        if (restriction2 == null)
        {
            throw new IllegalArgumentException("restriction2");
        }

        this.restrictions.add(restriction1);
        this.restrictions.add(restriction2);
    }

    /**
     * Instantiates a new and.
     *
     * @param restriction1 the restriction1
     * @param restriction2 the restriction2
     * @param restriction3 the restriction3
     */
    public And(IRestriction restriction1, IRestriction restriction2, IRestriction restriction3)
    {
        if (restriction1 == null)
        {
            throw new IllegalArgumentException("restriction1");
        }

        if (restriction2 == null)
        {
            throw new IllegalArgumentException("restriction2");
        }

        if (restriction3 == null)
        {
            throw new IllegalArgumentException("restriction3");
        }

        this.restrictions.add(restriction1);
        this.restrictions.add(restriction2);
        this.restrictions.add(restriction3);
    }

    /**
     * Instantiates a new and.
     *
     * @param restriction1 the restriction1
     * @param restriction2 the restriction2
     * @param restriction3 the restriction3
     * @param restriction4 the restriction4
     */
    public And(IRestriction restriction1, IRestriction restriction2, IRestriction restriction3, IRestriction restriction4)
    {
        if (restriction1 == null)
        {
            throw new IllegalArgumentException("restriction1");
        }

        if (restriction2 == null)
        {
            throw new IllegalArgumentException("restriction2");
        }

        if (restriction3 == null)
        {
            throw new IllegalArgumentException("restriction3");
        }

        if (restriction4 == null)
        {
            throw new IllegalArgumentException("restriction4");
        }

        this.restrictions.add(restriction1);
        this.restrictions.add(restriction2);
        this.restrictions.add(restriction3);
        this.restrictions.add(restriction4);
    }

    /**
     * Instantiates a new and.
     *
     * @param restrictions the restrictions
     */
    public And(List<IRestriction> restrictions)
    {
        if (restrictions == null)
        {
            throw new IllegalArgumentException("restrictions");
        }

        this.restrictions = restrictions;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        String stringValue = "";

        if (terms.size() > 0)
        {
            stringValue += "AND(";

            for (int i = 0; i < terms.size(); i++)
            {
                stringValue += "\"" + terms.get(i) + "\"";

                if (i < terms.size() - 1)
                {
                    stringValue += ", ";
                }
            }

            stringValue += ")";
        }
        else if (restrictions.size() > 0)
        {
            stringValue += "AND(";

            for(int i=0; i < restrictions.size(); i++)
            {
                stringValue += restrictions.get(i).toString();

                if (i < restrictions.size() - 1)
                {
                    stringValue += ", ";
                }
            }

            stringValue += ")";
        }

        return stringValue;
    }
}
