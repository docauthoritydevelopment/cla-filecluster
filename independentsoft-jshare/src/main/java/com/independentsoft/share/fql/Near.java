package com.independentsoft.share.fql;

import java.util.ArrayList;
import java.util.List;

/**
 * The Class Near.
 */
public class Near implements IRestriction {

    private List<String> terms = new ArrayList<String>();
    private int maximumDistance = -1;

    /**
     * Instantiates a new near.
     *
     * @param term1 the term1
     * @param term2 the term2
     */
    public Near(String term1, String term2)
    {
        if (term1 == null)
        {
            throw new IllegalArgumentException("term1");
        }

        if (term2 == null)
        {
            throw new IllegalArgumentException("term2");
        }

        this.terms.add(term1);
        this.terms.add(term2);
    }

    /**
     * Instantiates a new near.
     *
     * @param term1 the term1
     * @param term2 the term2
     * @param maximumDistance the maximum distance
     */
    public Near(String term1, String term2, int maximumDistance)
    {
        if (term1 == null)
        {
            throw new IllegalArgumentException("term1");
        }

        if (term2 == null)
        {
            throw new IllegalArgumentException("term2");
        }

        this.terms.add(term1);
        this.terms.add(term2);

        this.maximumDistance = maximumDistance;
    }

    /**
     * Instantiates a new near.
     *
     * @param term1 the term1
     * @param term2 the term2
     * @param term3 the term3
     */
    public Near(String term1, String term2, String term3)
    {
        if (term1 == null)
        {
            throw new IllegalArgumentException("term1");
        }

        if (term2 == null)
        {
            throw new IllegalArgumentException("term2");
        }

        if (term3 == null)
        {
            throw new IllegalArgumentException("term3");
        }

        this.terms.add(term1);
        this.terms.add(term2);
        this.terms.add(term3);
    }

    /**
     * Instantiates a new near.
     *
     * @param term1 the term1
     * @param term2 the term2
     * @param term3 the term3
     * @param maximumDistance the maximum distance
     */
    public Near(String term1, String term2, String term3, int maximumDistance)
    {
        if (term1 == null)
        {
            throw new IllegalArgumentException("term1");
        }

        if (term2 == null)
        {
            throw new IllegalArgumentException("term2");
        }

        if (term3 == null)
        {
            throw new IllegalArgumentException("term3");
        }

        this.terms.add(term1);
        this.terms.add(term2);
        this.terms.add(term3);

        this.maximumDistance = maximumDistance;
    }

    /**
     * Instantiates a new near.
     *
     * @param term1 the term1
     * @param term2 the term2
     * @param term3 the term3
     * @param term4 the term4
     */
    public Near(String term1, String term2, String term3, String term4)
    {
        if (term1 == null)
        {
            throw new IllegalArgumentException("term1");
        }

        if (term2 == null)
        {
            throw new IllegalArgumentException("term2");
        }

        if (term3 == null)
        {
            throw new IllegalArgumentException("term3");
        }

        if (term4 == null)
        {
            throw new IllegalArgumentException("term4");
        }

        this.terms.add(term1);
        this.terms.add(term2);
        this.terms.add(term3);
        this.terms.add(term4);
    }

    /**
     * Instantiates a new near.
     *
     * @param term1 the term1
     * @param term2 the term2
     * @param term3 the term3
     * @param term4 the term4
     * @param maximumDistance the maximum distance
     */
    public Near(String term1, String term2, String term3, String term4, int maximumDistance)
    {
        if (term1 == null)
        {
            throw new IllegalArgumentException("term1");
        }

        if (term2 == null)
        {
            throw new IllegalArgumentException("term2");
        }

        if (term3 == null)
        {
            throw new IllegalArgumentException("term3");
        }

        if (term4 == null)
        {
            throw new IllegalArgumentException("term4");
        }

        this.terms.add(term1);
        this.terms.add(term2);
        this.terms.add(term3);
        this.terms.add(term4);

        this.maximumDistance = maximumDistance;
    }

    /**
     * Instantiates a new near.
     *
     * @param terms the terms
     */
    public Near(List<String> terms)
    {
        if (terms == null)
        {
            throw new IllegalArgumentException("terms");
        }

        this.terms = terms;
    }

    /**
     * Instantiates a new near.
     *
     * @param terms the terms
     * @param maximumDistance the maximum distance
     */
    public Near(List<String> terms, int maximumDistance)
    {
        if (terms == null)
        {
            throw new IllegalArgumentException("terms");
        }

        this.terms = terms;
        this.maximumDistance = maximumDistance;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        String stringValue = "";

        if (terms.size() > 1)
        {
            String termsString = "";

            for (int i = 0; i < terms.size(); i++)
            {
                stringValue += terms.get(i).toString();

                if (i < terms.size() - 1)
                {
                    termsString += ", ";
                }
            }

            String parameters = "";

            if (maximumDistance > -1)
            {
                parameters += ", N=" + maximumDistance;
            }

            stringValue += "NEAR(" + termsString + parameters + ")";
        }

        return stringValue;
    }
}
