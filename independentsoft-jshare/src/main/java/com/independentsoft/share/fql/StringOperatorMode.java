package com.independentsoft.share.fql;

/**
 * The Enum StringOperatorMode.
 */
public enum StringOperatorMode {

    PHRASE,
    AND,
    OR,
    ANY,
    NEAR,
    ORDERED_NEAR,
    NONE
}
