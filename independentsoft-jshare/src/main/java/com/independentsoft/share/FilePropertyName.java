package com.independentsoft.share;

/**
 * The Class FilePropertyName.
 */
public class FilePropertyName {

    public static final String CHECK_IN_COMMENT = "CheckInComment";
    public static final String CHECK_OUT_TYPE = "CheckOutType";
    public static final String CONTENT_TAG = "ContentTag";
    public static final String CUSTOMIZED_PAGE_STATUS = "CustomizedPageStatus";
    public static final String E_TAG = "ETag";
    public static final String EXISTS = "Exists";
    public static final String LENGTH = "Length";
    public static final String LINKING_URL = "LinkingUrl";
    public static final String LEVEL = "Level";
    public static final String MAJOR_VERSION = "MajorVersion";
    public static final String MINOR_VERSION = "MinorVersion";
    public static final String NAME = "Name";
    public static final String SERVER_RELATIVE_URL = "ServerRelativeUrl";
    public static final String CREATED_TIME = "TimeCreated";
    public static final String LAST_MODIFIED_TIME = "TimeLastModified";
    public static final String TITLE = "Title";
    public static final String UI_VERSION = "UiVersion";
    public static final String UI_VERSION_LABEL = "UiVersionLabel";
}
