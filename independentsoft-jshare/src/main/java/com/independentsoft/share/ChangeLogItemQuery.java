package com.independentsoft.share;

import com.independentsoft.share.caml.CamlQuery;
import com.independentsoft.share.caml.Contains;

/**
 * The Class ChangeLogItemQuery.
 */
public class ChangeLogItemQuery {

    private ChangeToken token;
    private Contains contains;
    private CamlQuery query;
    private CamlQueryOptions queryOptions;
    private int rowLimit;
    private CamlViewFields viewFields;
    private String viewName;

    /**
     * Instantiates a new change log item query.
     */
    public ChangeLogItemQuery()
    {
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        String stringValue = "{ 'query': { '__metadata':{ 'type': 'SP.ChangeLogItemQuery' }";

        if (token != null)
        {
            stringValue += ", 'ChangeToken':" + token.toString();
        }

        if (contains != null)
        {
            stringValue += ", 'Contains':'" + contains.toString() + "'";
        }

        if (query != null)
        {
            stringValue += ", 'Query':'" + query.toString() + "'";
        }

        if (queryOptions != null)
        {
            stringValue += ", 'QueryOptions':'" + queryOptions.toString() + "'";
        }

        if (rowLimit > 0 )
        {
            stringValue += ", 'RowLimit':'" + rowLimit + "'";
        }

        if (viewFields != null)
        {
            stringValue += ", 'ViewFields':'" + viewFields.toString() + "'";
        }

        if (viewName != null && viewName.length() > 0)
        {
            stringValue += ", 'ViewName':'" + Util.encodeJson(viewName) + "'";
        }

        stringValue += " }}";

        return stringValue;
    }

    /**
     * Gets the token.
     *
     * @return the token
     */
    public ChangeToken getToken()
    {
        return token;
    }

    /**
     * Sets the token.
     *
     * @param token the new token
     */
    public void setToken(ChangeToken token)
    {
        this.token = token;
    }

    /**
     * Gets the contains.
     *
     * @return the contains
     */
    public Contains getContains()
    {
        return contains;
    }

    /**
     * Sets the contains.
     *
     * @param contains the new contains
     */
    public void setContains(Contains contains)
    {
        this.contains = contains;
    }

    /**
     * Gets the query.
     *
     * @return the query
     */
    public CamlQuery getQuery()
    {
        return query;
    }

    /**
     * Sets the query.
     *
     * @param query the new query
     */
    public void setQuery(CamlQuery query)
    {
        this.query = query;
    }

    /**
     * Gets the query options.
     *
     * @return the query options
     */
    public CamlQueryOptions getQueryOptions()
    {
        return queryOptions;
    }

    /**
     * Sets the query options.
     *
     * @param queryOptions the new query options
     */
    public void setQueryOptions(CamlQueryOptions queryOptions)
    {
        this.queryOptions = queryOptions;
    }

    /**
     * Gets the row limit.
     *
     * @return the row limit
     */
    public int getRowLimit()
    {
        return rowLimit;
    }

    /**
     * Sets the row limit.
     *
     * @param rowLimit the new row limit
     */
    public void setRowLimit(int rowLimit)
    {
        this.rowLimit = rowLimit;
    }

    /**
     * Gets the view fields.
     *
     * @return the view fields
     */
    public CamlViewFields getViewFields()
    {
        return viewFields;
    }

    /**
     * Sets the view fields.
     *
     * @param viewFields the new view fields
     */
    public void setViewFields(CamlViewFields viewFields)
    {
        this.viewFields = viewFields;
    }

    /**
     * Gets the view name.
     *
     * @return the view name
     */
    public String getViewName()
    {
        return viewName;
    }

    /**
     * Sets the view name.
     *
     * @param viewName the new view name
     */
    public void setViewName(String viewName)
    {
        this.viewName = viewName;
    }
}
