package com.independentsoft.share;

/**
 * The Enum CalendarType.
 */
public enum CalendarType {
	
    GREGORIAN,
    JAPANESE_EMPEROR_ERA,
    TAIWAN_CALENDAR,
    KOREAN_TANGUN_ERA,
    HIJRI,
    THAI,
    HEBREW,
    MIDDLE_EAST_FRENCH,
    ARABIC,
    TRANSLITERATED_ENGLISH,
    TRANSLITERATED_FRENCH,
    KOREAN_AND_JAPANESE,
    CHINESE,
    SAKA_ERA,
    NONE
}
