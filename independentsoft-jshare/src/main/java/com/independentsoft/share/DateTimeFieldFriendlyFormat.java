package com.independentsoft.share;

/**
 * The Enum DateTimeFieldFriendlyFormat.
 */
public enum DateTimeFieldFriendlyFormat {

    DISABLED,
    RELATIVE,
    NONE
}
