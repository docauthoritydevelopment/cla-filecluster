package com.independentsoft.share;

/**
 * The Enum SortDirection.
 */
public enum SortDirection {

    ASCENDING,
    DESCENDING,
    FQL_FORMULA
}
