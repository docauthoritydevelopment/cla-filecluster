package com.independentsoft.share;

/**
 * The Enum CheckOutType.
 */
public enum CheckOutType {
	
    ONLINE,
    OFFLINE,
    NONE
}
