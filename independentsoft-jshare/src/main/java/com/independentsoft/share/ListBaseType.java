package com.independentsoft.share;

/**
 * The Enum ListBaseType.
 */
public enum ListBaseType {

    GENERIC_LIST,
    DOCUMENT_LIBRARY,
    DISCUSSION_BOARD,
    SURVEY,
    ISSUE
}
