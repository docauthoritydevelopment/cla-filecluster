package com.independentsoft.share;

/**
 * The Enum ListJoinType.
 */
public enum ListJoinType {

	LEFT,
	INNER
}
