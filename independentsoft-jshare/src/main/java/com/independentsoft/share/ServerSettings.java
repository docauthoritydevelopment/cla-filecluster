package com.independentsoft.share;

import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

/**
 * The Class ServerSettings.
 */
public class ServerSettings {

    private String serverVersion;
    private boolean recycleBinEnabled;
    private String serverRelativeUrl;

    /**
     * Instantiates a new server settings.
     */
    public ServerSettings()
    {
    }

    ServerSettings(String xml) throws XMLStreamException
    {
        byte[] buffer = null;

        try
        {
            buffer = xml.getBytes("UTF-8");
        }
        catch(UnsupportedEncodingException ex)
        {
            System.err.println(ex.getMessage());
        }

        ByteArrayInputStream inputStream = new ByteArrayInputStream(buffer);

        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        XMLStreamReader reader = xmlInputFactory.createXMLStreamReader(inputStream);

        parse(reader);
    }

    private void parse(XMLStreamReader reader) throws XMLStreamException
    {
        while (reader.hasNext() && reader.next() > 0)
        {
            if (reader.isStartElement() && reader.getLocalName() != null && reader.getLocalName().equals("ServerSettings"))
            {
                while (reader.hasNext())
                {
                    if (reader.isStartElement() && reader.getLocalName() != null && reader.getLocalName().equals("ServerVersion"))
                    {
                        serverVersion = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getLocalName().equals("RecycleBinEnabled"))
                    {
                        String recycleBinEnabledString = reader.getElementText();

                        if (recycleBinEnabledString != null && recycleBinEnabledString.length() > 0)
                        {
                            recycleBinEnabled = Boolean.parseBoolean(recycleBinEnabledString);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getLocalName().equals("ServerRelativeUrl"))
                    {
                        serverRelativeUrl = reader.getElementText();
                    }

                    if (reader.isEndElement() && reader.getLocalName() != null && reader.getLocalName().equals("ServerSettings"))
                    {
                        break;
                    }
                    else
                    {
                        reader.next();
                    }
                }
            }
        }
    }

    /**
     * Gets the server version.
     *
     * @return the server version
     */
    public String getServerVersion()
    {
        return serverVersion;
    }

    /**
     * Checks if is recycle bin enabled.
     *
     * @return true, if is recycle bin enabled
     */
    public boolean isRecycleBinEnabled()
    {
        return recycleBinEnabled;
    }

    /**
     * Gets the server relative url.
     *
     * @return the server relative url
     */
    public String getServerRelativeUrl()
    {
        return serverRelativeUrl;
    }
}
