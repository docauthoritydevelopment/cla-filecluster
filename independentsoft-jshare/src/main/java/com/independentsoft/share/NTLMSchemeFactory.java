package com.independentsoft.share;

import org.apache.http.auth.AuthScheme;
import org.apache.http.auth.AuthSchemeProvider;
import org.apache.http.impl.auth.NTLMScheme;
import org.apache.http.protocol.HttpContext;

/**
 * A factory for creating NTLMScheme objects.
 */
public class NTLMSchemeFactory implements AuthSchemeProvider  {

	   /* (non-Javadoc)
   	 * @see org.apache.http.auth.AuthSchemeProvider#create(org.apache.http.protocol.HttpContext)
   	 */
   	public AuthScheme create(final HttpContext context) {
	        return new NTLMScheme(new JCIFSEngine());
	    }

}