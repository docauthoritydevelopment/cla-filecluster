package com.independentsoft.share;

import java.io.IOException;
import java.io.InputStream;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamReader;

/**
 * The Class ServiceException.
 */
public class ServiceException extends Exception {

    private String requestUrl;
    private String requestBody;
    private String errorString;
    private String errorCode;

    private ServiceException()
    {
    }
    
    /**
     * Instantiates a new service exception.
     *
     * @param message the message
     * @param cause the cause
     * @param requestUrl the request url
     */
    public ServiceException(String message, Throwable cause, String requestUrl)
    {
        super(message, cause);
        this.errorString = message;
        this.requestUrl = requestUrl;
    }

    /**
     * Instantiates a new service exception.
     *
     * @param message the message
     * @param cause the cause
     * @param requestUrl the request url
     * @param requestBody the request body
     */
    public ServiceException(String message, Throwable cause, String requestUrl, String requestBody)
    {
        super(message, cause);
        this.errorString = message;
        this.requestUrl = requestUrl;
        this.requestBody = requestBody;
    }

    ServiceException(InputStream inputStream, String requestUrl, String requestBody)
    {
        this.requestUrl = requestUrl;
        this.requestBody = requestBody;
        parse(inputStream);
    }
    
    private void parse(InputStream inputStream)
    {
        try
        {		         		          
         if(inputStream != null)
         {
        	 XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        	 XMLStreamReader reader = xmlInputFactory.createXMLStreamReader(inputStream);
        	 
             while (reader.hasNext() && reader.next() > 0)
             {
                 if (reader.isStartElement() && reader.getLocalName() != null && reader.getLocalName().equals("message") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices/metadata"))
                 {
                	errorString = reader.getElementText();
                 }
                 else if (reader.isStartElement() && reader.getLocalName() != null && reader.getLocalName().equals("code") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices/metadata"))
                 {
                	errorCode = reader.getElementText();
                 }
             }
         }
        }         
        catch (Exception e)
        {
            System.err.println(e);
        }
        finally
        {
            if( inputStream != null)
            {
                 try
                 {
                     inputStream.close();
                 }
                 catch (IOException e)
                 {
                     System.err.println(e);
                 }
            }
        }
    }

    /* (non-Javadoc)
     * @see java.lang.Throwable#toString()
     */
    @Override
    public String toString()
    {
        if (errorString != null)
        {
            return errorString;
        }
        else
        {
            return super.toString();
        }
    }

    /**
     * Gets the request url.
     *
     * @return the request url
     */
    public String getRequestUrl()
    {
        return requestUrl;
    }

    /**
     * Gets the request body.
     *
     * @return the request body
     */
    public String getRequestBody()
    {
   	 	return requestBody;
    }
   
    /**
     * Gets the error string.
     *
     * @return the error string
     */
    public String getErrorString()
    {
    	return errorString;
    }

    /**
     * Gets the error code.
     *
     * @return the error code
     */
    public String getErrorCode()
    {
    	return errorCode;
    }
}