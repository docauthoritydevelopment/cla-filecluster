package com.independentsoft.share;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

/**
 * The Class ListJoin.
 */
public class ListJoin {

    private ListJoinCondition condition;
    private ListJoinType type = ListJoinType.LEFT;
    private String listAlias;

    /**
     * Instantiates a new list join.
     */
    public ListJoin()
    {
    }

    ListJoin(XMLStreamReader reader) throws XMLStreamException
    {
        parse(reader);
    }

    private void parse(XMLStreamReader reader) throws XMLStreamException
    {
        listAlias = reader.getAttributeValue(null, "ListAlias");

        String typeAttribute = reader.getAttributeValue(null, "Type");

        if (typeAttribute != null && typeAttribute.length() > 0)
        {
            type = EnumUtil.parseListJoinType(typeAttribute);
        }

        while (reader.hasNext())
        {
            if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Eq") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                condition = new ListJoinCondition(reader);
            }

            if (reader.isEndElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Join") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                break;
            }
            else
            {
                reader.next();
            }
        }
    }

    /**
     * Gets the condition.
     *
     * @return the condition
     */
    public ListJoinCondition getCondition()
    {
        return condition;
    }

    /**
     * Gets the type.
     *
     * @return the type
     */
    public ListJoinType getType()
    {
        return type;
    }

    /**
     * Gets the list alias.
     *
     * @return the list alias
     */
    public String getListAlias()
    {
        return listAlias;
    }
}
