package com.independentsoft.share;

import java.util.ArrayList;
import java.util.List;

/**
 * The Class SearchQueryProperty.
 */
public class SearchQueryProperty {

    private String name;
    private List<SearchQueryPropertyValue> values = new ArrayList<SearchQueryPropertyValue>();

    /**
     * Instantiates a new search query property.
     */
    public SearchQueryProperty()
    {
    }

    /**
     * Instantiates a new search query property.
     *
     * @param name the name
     */
    public SearchQueryProperty(String name)
    {
        if (name == null)
        {
            throw new IllegalArgumentException("name");
        }
        
        this.name = name;
    }

    /**
     * Instantiates a new search query property.
     *
     * @param name the name
     * @param values the values
     */
    public SearchQueryProperty(String name, List<SearchQueryPropertyValue> values)
    {
        if (name == null)
        {
            throw new IllegalArgumentException("name");
        }
        
        this.name = name;
        this.values = values;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        String json = "{ ";

        json += "'Name':'" + Util.encodeJson(name) + "'";

        for (int i = 0; i < values.size(); i++)
        {
            json += ", 'Value':'" + values.get(i).toString() + "'";
        }

        json += " }";

        return json;
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName()
    {
        return name;
    }

    /**
     * Sets the name.
     *
     * @param name the new name
     */
    public void setName(String name)
    {
        this.name = name;
    }

    /**
     * Gets the values.
     *
     * @return the values
     */
    public List<SearchQueryPropertyValue> getValues()
    {
        return values;
    }
}
