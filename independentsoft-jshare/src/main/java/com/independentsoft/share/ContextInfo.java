package com.independentsoft.share;

import java.io.InputStream;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

/**
 * The Class ContextInfo.
 */
public class ContextInfo {

    private int formDigestTimeoutSeconds;
    private String formDigestValue;
    private String libraryVersion;
    private String siteFullUrl;
    private List<String> supportedSchemaVersions = new ArrayList<String>();
    private String webFullUrl;
    private Date formDigestValueExpireTime;

    private ContextInfo()
    {
    }

    ContextInfo(InputStream inputStream) throws XMLStreamException, ParseException
    {
        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        XMLStreamReader reader = xmlInputFactory.createXMLStreamReader(inputStream);

        parse(reader);
    }

    private void parse(XMLStreamReader reader) throws XMLStreamException, ParseException
    {
        while (reader.hasNext() && reader.next() > 0)
        {
            if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("FormDigestTimeoutSeconds") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                String stringValue = reader.getElementText();

                if(stringValue != null && stringValue.length() > 0)
                {
                    formDigestTimeoutSeconds = Integer.parseInt(stringValue);
                }
            }
            else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("FormDigestValue") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                formDigestValue = reader.getElementText();

                if (formDigestValue != null && formDigestValue.length() > 0)
                {
                    int index = formDigestValue.indexOf(",");

                    if(index > 0)
                    {
                        String dateValue = formDigestValue.substring(index + 1);

                        formDigestValueExpireTime = Util.parseDate(dateValue, "dd MMM yyyy HH:mm:ss Z");
                    }
                }
            }
            else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("LibraryVersion") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                libraryVersion = reader.getElementText();
            }
            else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("SiteFullUrl") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                siteFullUrl = reader.getElementText();
            }
            else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("SupportedSchemaVersions") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                while (reader.hasNext())
                {
                    if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("element") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String version = reader.getElementText();
                        supportedSchemaVersions.add(version);
                    }

                    if (reader.isEndElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("SupportedSchemaVersions") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        break;
                    }
                    else
                    {
                        reader.next();
                    }
                }
            }
            else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("WebFullUrl") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                webFullUrl = reader.getElementText();
            }
        }

        if(formDigestTimeoutSeconds > 0)
        {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(formDigestValueExpireTime);
            calendar.add(Calendar.SECOND, formDigestTimeoutSeconds);

            formDigestValueExpireTime = calendar.getTime();
        }
    }

    /**
     * Gets the form digest timeout seconds.
     *
     * @return the form digest timeout seconds
     */
    public int getFormDigestTimeoutSeconds()
    {
        return formDigestTimeoutSeconds;
    }

    /**
     * Gets the form digest value.
     *
     * @return the form digest value
     */
    public String getFormDigestValue()
    {
        return formDigestValue;
    }

    /**
     * Gets the library version.
     *
     * @return the library version
     */
    public String getLibraryVersion()
    {
        return libraryVersion;
    }

    /**
     * Gets the site full url.
     *
     * @return the site full url
     */
    public String getSiteFullUrl()
    {
        return siteFullUrl;
    }

    /**
     * Gets the supported schema versions.
     *
     * @return the supported schema versions
     */
    public List<String> getSupportedSchemaVersions()
    {
        return supportedSchemaVersions;
    }

    /**
     * Gets the web full url.
     *
     * @return the web full url
     */
    public String getWebFullUrl()
    {
        return webFullUrl;
    }

    /**
     * Gets the form digest value expire time.
     *
     * @return the form digest value expire time
     */
    public Date getFormDigestValueExpireTime()
    {
        return formDigestValueExpireTime;
    }
}
