package com.independentsoft.share;

/**
 * The Enum FileSystemObjectType.
 */
public enum FileSystemObjectType {

    FILE,
    FOLDER,
    WEB,
    INVALID
}
