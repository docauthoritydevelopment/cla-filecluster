package com.independentsoft.share;

/**
 * The Class ReorderingRule.
 */
public class ReorderingRule {

    private String matchValue;
    private ReorderingRuleMatchType matchType = ReorderingRuleMatchType.RESULT_CONTAINS_KEYWORD;
    private int boost;

    /**
     * Instantiates a new reordering rule.
     */
    public ReorderingRule()
    {
    }

    /**
     * Instantiates a new reordering rule.
     *
     * @param matchValue the match value
     * @param matchType the match type
     */
    public ReorderingRule(String matchValue, ReorderingRuleMatchType matchType)
    {
        if (matchValue == null)
        {
            throw new IllegalArgumentException("matchValue");
        }
        
        this.matchValue = matchValue;
        this.matchType = matchType;
    }

    /**
     * Instantiates a new reordering rule.
     *
     * @param matchValue the match value
     * @param matchType the match type
     * @param boost the boost
     */
    public ReorderingRule(String matchValue, ReorderingRuleMatchType matchType, int boost)
    {
        if (matchValue == null)
        {
            throw new IllegalArgumentException("matchValue");
        }
        
        this.matchValue = matchValue;
        this.matchType = matchType;
        this.boost = boost;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        String json = "{ ";

        json += "'MatchValue':'" + Util.encodeJson(matchValue) + "'";

        json += "'Boost':'" + boost + "'";

        json += "'MatchType':'" + EnumUtil.parseReorderingRuleMatchType(matchType) + "'";

        json += " }";

        return json;
    }

    /**
     * Gets the match value.
     *
     * @return the match value
     */
    public String getMatchValue()
    {
        return matchValue;
    }

    /**
     * Sets the match value.
     *
     * @param matchValue the new match value
     */
    public void setMatchValue(String matchValue)
    {
        this.matchValue = matchValue;
    }

    /**
     * Gets the match type.
     *
     * @return the match type
     */
    public ReorderingRuleMatchType getMatchType()
    {
        return matchType;
    }

    /**
     * Sets the match type.
     *
     * @param matchType the new match type
     */
    public void setMatchType(ReorderingRuleMatchType matchType)
    {
        this.matchType = matchType;
    }

    /**
     * Gets the boost.
     *
     * @return the boost
     */
    public int getBoost()
    {
        return boost;
    }

    /**
     * Sets the boost.
     *
     * @param boost the new boost
     */
    public void setBoost(int boost)
    {
        this.boost = boost;
    }
}
