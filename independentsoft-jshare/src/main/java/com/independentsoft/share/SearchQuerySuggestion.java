package com.independentsoft.share;

/**
 * The Class SearchQuerySuggestion.
 */
public class SearchQuerySuggestion {
    
    private String query;
    private int numberOfQuerySuggestions;
    private int numberOfResultSuggestions;
    private boolean preQuerySuggestions;
    private boolean hitHighlighting = true;
    private boolean capitalizeFirstLetters;
    private Locale culture = Locale.NONE;
    private boolean enableStemming = true;
    private boolean showPeopleNameSuggestions = true;
    private boolean enableQueryRules = true;
    private boolean prefixMatchAllTerms;

    /**
     * Instantiates a new search query suggestion.
     */
    public SearchQuerySuggestion()
    {
    }

    /**
     * Instantiates a new search query suggestion.
     *
     * @param query the query
     */
    public SearchQuerySuggestion(String query)
    {
        this.query = query;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        String stringValue = "querytext='" + Util.encodeUrlInputStream(query) + "'";

        if(numberOfQuerySuggestions > 0)
        {
            stringValue += "&inumberofquerysuggestions=" + numberOfQuerySuggestions;
        }

        if (numberOfResultSuggestions > 0)
        {
            stringValue += "&inumberofresultsuggestions=" + numberOfResultSuggestions;
        }

        if (preQuerySuggestions)
        {
            stringValue += "&fprequerysuggestions=true";
        }

        if (!hitHighlighting)
        {
            stringValue += "&fHitHighlighting=false";
        }

        if (capitalizeFirstLetters)
        {
            stringValue += "&fCapitalizeFirstLetters=true";
        }

        if (culture != Locale.NONE)
        {
            stringValue += "&culture=" + EnumUtil.parseLocale(culture);
        }

        if (!enableStemming)
        {
            stringValue += "&enablestemming=false";
        }

        if (!showPeopleNameSuggestions)
        {
            stringValue += "&showpeoplenamesuggestions=false";
        }

        if (!enableQueryRules)
        {
            stringValue += "&enablequeryrules=false";
        }

        if (prefixMatchAllTerms)
        {
            stringValue += "&fprefixmatchallterms=true";
        }

        return stringValue;
    }

    /**
     * Gets the query.
     *
     * @return the query
     */
    public String getQuery()
    {
        return query;
    }

    /**
     * Sets the query.
     *
     * @param query the new query
     */
    public void setQuery(String query)
    {
        this.query = query;
    }

    /**
     * Gets the number of query suggestions.
     *
     * @return the number of query suggestions
     */
    public int getNumberOfQuerySuggestions()
    {
        return numberOfQuerySuggestions;
    }

    /**
     * Sets the number of query suggestions.
     *
     * @param numberOfQuerySuggestions the new number of query suggestions
     */
    public void setNumberOfQuerySuggestions(int numberOfQuerySuggestions)
    {
        this.numberOfQuerySuggestions = numberOfQuerySuggestions;
    }

    /**
     * Gets the number of result suggestions.
     *
     * @return the number of result suggestions
     */
    public int getNumberOfResultSuggestions()
    {
        return numberOfResultSuggestions;
    }

    /**
     * Sets the number of result suggestions.
     *
     * @param numberOfResultSuggestions the new number of result suggestions
     */
    public void setNumberOfResultSuggestions(int numberOfResultSuggestions)
    {
        this.numberOfResultSuggestions = numberOfResultSuggestions;
    }

    /**
     * Checks if is pre query suggestions.
     *
     * @return true, if is pre query suggestions
     */
    public boolean isPreQuerySuggestions()
    {
        return preQuerySuggestions;
    }

    /**
     * Sets the pre query suggestions.
     *
     * @param preQuerySuggestions the new pre query suggestions
     */
    public void setPreQuerySuggestions(boolean preQuerySuggestions)
    {
        this.preQuerySuggestions = preQuerySuggestions;
    }

    /**
     * Checks if is hit highlighting.
     *
     * @return true, if is hit highlighting
     */
    public boolean isHitHighlighting()
    {
        return hitHighlighting;
    }

    /**
     * Sets the hit highlighting.
     *
     * @param hitHighlighting the new hit highlighting
     */
    public void setHitHighlighting(boolean hitHighlighting)
    {
        this.hitHighlighting = hitHighlighting;
    }

    /**
     * Checks if is capitalize first letters.
     *
     * @return true, if is capitalize first letters
     */
    public boolean isCapitalizeFirstLetters()
    {
        return capitalizeFirstLetters;
    }

    /**
     * Sets the capitalize first letters.
     *
     * @param capitalizeFirstLetters the new capitalize first letters
     */
    public void setCapitalizeFirstLetters(boolean capitalizeFirstLetters)
    {
        this.capitalizeFirstLetters = capitalizeFirstLetters;
    }

    /**
     * Gets the culture.
     *
     * @return the culture
     */
    public Locale getCulture()
    {
        return culture;
    }

    /**
     * Sets the culture.
     *
     * @param culture the new culture
     */
    public void setCulture(Locale culture)
    {
        this.culture = culture;
    }

    /**
     * Checks if is stemming enabled.
     *
     * @return true, if is stemming enabled
     */
    public boolean isStemmingEnabled()
    {
        return enableStemming;
    }

    /**
     * Enable stemming.
     *
     * @param enableStemming the enable stemming
     */
    public void enableStemming(boolean enableStemming)
    {
        this.enableStemming = enableStemming;
    }

    /**
     * Show people name suggestions.
     *
     * @return true, if successful
     */
    public boolean showPeopleNameSuggestions()
    {
        return showPeopleNameSuggestions;
    }

    /**
     * Sets the show people name suggestions.
     *
     * @param showPeopleNameSuggestions the new show people name suggestions
     */
    public void setShowPeopleNameSuggestions(boolean showPeopleNameSuggestions)
    {
        this.showPeopleNameSuggestions = showPeopleNameSuggestions;
    }

    /**
     * Checks if is query rules enabled.
     *
     * @return true, if is query rules enabled
     */
    public boolean isQueryRulesEnabled()
    {
        return enableQueryRules;
    }

    /**
     * Enable query rules.
     *
     * @param enableQueryRules the enable query rules
     */
    public void enableQueryRules(boolean enableQueryRules)
    {
        this.enableQueryRules = enableQueryRules;
    }

    /**
     * Checks if is prefix match all terms.
     *
     * @return true, if is prefix match all terms
     */
    public boolean isPrefixMatchAllTerms()
    {
        return prefixMatchAllTerms;
    }

    /**
     * Sets the prefix match all terms.
     *
     * @param prefixMatchAllTerms the new prefix match all terms
     */
    public void setPrefixMatchAllTerms(boolean prefixMatchAllTerms)
    {
        this.prefixMatchAllTerms = prefixMatchAllTerms;
    }
}
