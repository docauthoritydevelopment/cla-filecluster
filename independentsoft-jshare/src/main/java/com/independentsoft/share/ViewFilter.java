package com.independentsoft.share;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

/**
 * The Class ViewFilter.
 */
public class ViewFilter {

    private String name;
    private String value;

    /**
     * Instantiates a new view filter.
     */
    public ViewFilter()
    {
    }

    ViewFilter(XMLStreamReader reader) throws XMLStreamException
    {
        parse(reader);
    }

    private void parse(XMLStreamReader reader) throws XMLStreamException
    {
        name = reader.getAttributeValue(null, "Name");
        value = reader.getAttributeValue(null, "Value");

        while (reader.hasNext())
        {
            if (reader.isEndElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Filter") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                break;
            }
            else
            {
                reader.next();
            }
        }
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName()
    {
        return name;
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public String getValue()
    {
        return value;
    }
}
