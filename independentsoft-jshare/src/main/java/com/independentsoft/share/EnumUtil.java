package com.independentsoft.share;

import java.util.ArrayList;

import com.independentsoft.share.fql.StringOperatorMode;

class EnumUtil {
	
    static BasePermissionMask parseBasePermission(java.util.List<BasePermission> parameter)
    {
        BasePermissionMask mask = new BasePermissionMask();

        for (int i = 0; i < parameter.size(); i++)
        {
            if (parameter.get(i) == BasePermission.ALL_PERMISSIONS)
            {
                mask.Low = 4294967295L;
                mask.High =2147483647L;

                return mask;
            }
        }

        for (int i = 0; i < parameter.size(); i++)
        {
            if (parameter.get(i) == BasePermission.VIEW_LIST_ITEMS)
            {
                mask.Low += 0x0000000000000001;
            }
            else if (parameter.get(i) == BasePermission.ADD_LIST_ITEMS)
            {
                mask.Low += 0x0000000000000002;
            }
            else if (parameter.get(i) == BasePermission.EDIT_LIST_ITEMS)
            {
                mask.Low += 0x0000000000000004;
            }
            else if (parameter.get(i) == BasePermission.DELETE_LIST_ITEMS)
            {
                mask.Low += 0x0000000000000008;
            }
            else if (parameter.get(i) == BasePermission.APPROVE_ITEMS)
            {
                mask.Low += 0x0000000000000010;
            }
            else if (parameter.get(i) == BasePermission.OPEN_ITEMS)
            {
                mask.Low += 0x0000000000000020;
            }
            else if (parameter.get(i) == BasePermission.VIEW_VERSIONS)
            {
                mask.Low += 0x0000000000000040;
            }
            else if (parameter.get(i) == BasePermission.DELETE_VERSIONS)
            {
                mask.Low += 0x0000000000000080;
            }
            else if (parameter.get(i) == BasePermission.CANCEL_CHECKOUT)
            {
                mask.Low += 0x0000000000000100;
            }
            else if (parameter.get(i) == BasePermission.MANAGE_PERSONAL_VIEWS)
            {
                mask.Low += 0x0000000000000200;
            }
            else if (parameter.get(i) == BasePermission.MANAGE_LISTS)
            {
                mask.Low += 0x0000000000000800;
            }
            else if (parameter.get(i) == BasePermission.VIEW_FORM_PAGES)
            {
                mask.Low += 0x0000000000001000;
            }
            else if (parameter.get(i) == BasePermission.ANONYMOUS_SEARCH_ACCESS_LIST)
            {
                mask.Low += 0x0000000000002000;
            }
            else if (parameter.get(i) == BasePermission.OPEN)
            {
                mask.Low += 0x0000000000010000;
            }
            else if (parameter.get(i) == BasePermission.VIEW_PAGES)
            {
                mask.Low += 0x0000000000020000;
            }
            else if (parameter.get(i) == BasePermission.ADD_AND_CUSTOMIZE_PAGES)
            {
                mask.Low += 0x0000000000040000;
            }
            else if (parameter.get(i) == BasePermission.APPLY_THEME_AND_BORDER)
            {
                mask.Low += 0x0000000000080000;
            }
            else if (parameter.get(i) == BasePermission.APPLY_STYLE_SHEETS)
            {
                mask.Low += 0x0000000000100000;
            }
            else if (parameter.get(i) == BasePermission.VIEW_USAGE_DATA)
            {
                mask.Low += 0x0000000000200000;
            }
            else if (parameter.get(i) == BasePermission.CREATE_SSC_SITE)
            {
                mask.Low += 0x0000000000400000;
            }
            else if (parameter.get(i) == BasePermission.MANAGE_SUBWEBS)
            {
                mask.Low += 0x0000000000800000;
            }
            else if (parameter.get(i) == BasePermission.CREATE_GROUPS)
            {
                mask.Low += 0x0000000001000000;
            }
            else if (parameter.get(i) == BasePermission.MANAGE_PERMISSIONS)
            {
                mask.Low += 0x0000000002000000;
            }
            else if (parameter.get(i) == BasePermission.BROWSE_DIRECTORIES)
            {
                mask.Low += 0x0000000004000000;
            }
            else if (parameter.get(i) == BasePermission.BROWSE_USER_INFO)
            {
                mask.Low += 0x0000000008000000;
            }
            else if (parameter.get(i) == BasePermission.ADD_DEL_PRIVATE_WEB_PARTS)
            {
                mask.Low += 0x0000000010000000;
            }
            else if (parameter.get(i) == BasePermission.UPDATE_PERSONAL_WEB_PARTS)
            {
                mask.Low += 0x0000000020000000;
            }
            else if (parameter.get(i) == BasePermission.MANAGE_WEB)
            {
                mask.Low += 0x0000000040000000;
            }
            else if (parameter.get(i) == BasePermission.ANONYMOUS_SEARCH_ACCESS_WEB_LISTS)
            {
                mask.Low += 0x0000000080000000;
            }
            else if (parameter.get(i) == BasePermission.USE_CLIENT_INTEGRATION)
            {
                mask.High += 0x000000010;
            }
            else if (parameter.get(i) == BasePermission.USE_REMOTE_API)
            {
                mask.High += 0x000000020;
            }
            else if (parameter.get(i) == BasePermission.MANAGE_ALERTS)
            {
                mask.High += 0x000000040;
            }
            else if (parameter.get(i) == BasePermission.CREATE_ALERTS)
            {
                mask.High += 0x000000080;
            }
            else if (parameter.get(i) == BasePermission.EDIT_MY_USER_INFO)
            {
                mask.High += 0x000000100;
            }
            else if (parameter.get(i) == BasePermission.ENUMERATE_PERMISSIONS)
            {
                mask.High += 0x400000000L;
            }
        }

        return mask;
    }

    static java.util.List<BasePermission> parseBasePermission(long low, long high)
    {
        java.util.List<BasePermission> list = new ArrayList<BasePermission>();

        if (low == 4294967295L && high == 2147483647L)
        {
            list.add(BasePermission.ALL_PERMISSIONS);
        }
        else
        {
            if ((low & 0x0000000000000001) == 0x0000000000000001)
            {
                list.add(BasePermission.VIEW_LIST_ITEMS);
            }

            if ((low & 0x0000000000000002) == 0x0000000000000002)
            {
                list.add(BasePermission.ADD_LIST_ITEMS);
            }

            if ((low & 0x0000000000000004) == 0x0000000000000004)
            {
                list.add(BasePermission.EDIT_LIST_ITEMS);
            }

            if ((low & 0x0000000000000008) == 0x0000000000000008)
            {
                list.add(BasePermission.DELETE_LIST_ITEMS);
            }

            if ((low & 0x0000000000000010) == 0x0000000000000010)
            {
                list.add(BasePermission.APPROVE_ITEMS);
            }

            if ((low & 0x0000000000000020) == 0x0000000000000020)
            {
                list.add(BasePermission.OPEN_ITEMS);
            }

            if ((low & 0x0000000000000040) == 0x0000000000000040)
            {
                list.add(BasePermission.VIEW_VERSIONS);
            }

            if ((low & 0x0000000000000080) == 0x0000000000000080)
            {
                list.add(BasePermission.DELETE_VERSIONS);
            }

            if ((low & 0x0000000000000100) == 0x0000000000000100)
            {
                list.add(BasePermission.CANCEL_CHECKOUT);
            }

            if ((low & 0x0000000000000200) == 0x0000000000000200)
            {
                list.add(BasePermission.MANAGE_PERSONAL_VIEWS);
            }

            if ((low & 0x0000000000000800) == 0x0000000000000800)
            {
                list.add(BasePermission.MANAGE_LISTS);
            }

            if ((low & 0x0000000000001000) == 0x0000000000001000)
            {
                list.add(BasePermission.VIEW_FORM_PAGES);
            }

            if ((low & 0x0000000000002000) == 0x0000000000002000)
            {
                list.add(BasePermission.ANONYMOUS_SEARCH_ACCESS_LIST);
            }

            if ((low & 0x0000000000010000) == 0x0000000000010000)
            {
                list.add(BasePermission.OPEN);
            }

            if ((low & 0x0000000000020000) == 0x0000000000020000)
            {
                list.add(BasePermission.VIEW_PAGES);
            }

            if ((low & 0x0000000000040000) == 0x0000000000040000)
            {
                list.add(BasePermission.ADD_AND_CUSTOMIZE_PAGES);
            }

            if ((low & 0x0000000000080000) == 0x0000000000080000)
            {
                list.add(BasePermission.APPLY_THEME_AND_BORDER);
            }

            if ((low & 0x0000000000100000) == 0x0000000000100000)
            {
                list.add(BasePermission.APPLY_STYLE_SHEETS);
            }

            if ((low & 0x0000000000200000) == 0x0000000000200000)
            {
                list.add(BasePermission.VIEW_USAGE_DATA);
            }

            if ((low & 0x0000000000400000) == 0x0000000000400000)
            {
                list.add(BasePermission.CREATE_SSC_SITE);
            }

            if ((low & 0x0000000000800000) == 0x0000000000800000)
            {
                list.add(BasePermission.MANAGE_SUBWEBS);
            }

            if ((low & 0x0000000001000000) == 0x0000000001000000)
            {
                list.add(BasePermission.CREATE_GROUPS);
            }

            if ((low & 0x0000000002000000) == 0x0000000002000000)
            {
                list.add(BasePermission.MANAGE_PERMISSIONS);
            }

            if ((low & 0x0000000004000000) == 0x0000000004000000)
            {
                list.add(BasePermission.BROWSE_DIRECTORIES);
            }

            if ((low & 0x0000000008000000) == 0x0000000008000000)
            {
                list.add(BasePermission.BROWSE_USER_INFO);
            }

            if ((low & 0x0000000010000000) == 0x0000000010000000)
            {
                list.add(BasePermission.ADD_DEL_PRIVATE_WEB_PARTS);
            }

            if ((low & 0x0000000020000000) == 0x0000000020000000)
            {
                list.add(BasePermission.UPDATE_PERSONAL_WEB_PARTS);
            }

            if ((low & 0x0000000040000000) == 0x0000000040000000)
            {
                list.add(BasePermission.MANAGE_WEB);
            }

            if ((low & 0x0000000080000000) == 0x0000000080000000)
            {
                list.add(BasePermission.ANONYMOUS_SEARCH_ACCESS_WEB_LISTS);
            }

            if ((high & 0x000000010) == 0x000000010)
            {
                list.add(BasePermission.USE_CLIENT_INTEGRATION);
            }

            if ((high & 0x000000020) == 0x000000020)
            {
                list.add(BasePermission.USE_REMOTE_API);
            }

            if ((high & 0x000000040) == 0x000000040)
            {
                list.add(BasePermission.MANAGE_ALERTS);
            }

            if ((high & 0x000000080) == 0x000000080)
            {
                list.add(BasePermission.CREATE_ALERTS);
            }

            if ((high & 0x000000100) == 0x000000100)
            {
                list.add(BasePermission.EDIT_MY_USER_INFO);
            }

            if ((high & 0x400000000L) == 0x400000000L)
            {
                list.add(BasePermission.ENUMERATE_PERMISSIONS);
            }
        }

        return list;
    }

    static String parseFieldReferenceType(FieldReferenceType parameter)
    {
        if (parameter == FieldReferenceType.ALL_DAY_EVENT)
        {
            return "AllDayEvent";
        }
        else if (parameter == FieldReferenceType.CP_LINK)
        {
            return "CPLink";
        }
        else if (parameter == FieldReferenceType.DURATION)
        {
            return "Duration";
        }
        else if (parameter == FieldReferenceType.END_DATE)
        {
            return "EndDate";
        }
        else if (parameter == FieldReferenceType.EVENT_CANCEL)
        {
            return "EventCancel";
        }
        else if (parameter == FieldReferenceType.EVENT_TYPE)
        {
            return "EventType";
        }
        else if (parameter == FieldReferenceType.LINK_URL)
        {
            return "LinkURL";
        }
        else if (parameter == FieldReferenceType.MASTER_SERIES_ITEM_ID)
        {
            return "MasterSeriesItemID";
        }
        else if (parameter == FieldReferenceType.RECURRENCE)
        {
            return "Recurrence";
        }
        else if (parameter == FieldReferenceType.RECURRENCE_DATA)
        {
            return "RecurData";
        }
        else if (parameter == FieldReferenceType.RECURRENCE_ID)
        {
            return "RecurrenceId";
        }
        else if (parameter == FieldReferenceType.START_DATE)
        {
            return "StartDate";
        }
        else if (parameter == FieldReferenceType.TIME_ZONE)
        {
            return "TimeZone";
        }
        else if (parameter == FieldReferenceType.UID)
        {
            return "UID";
        }
        else if (parameter == FieldReferenceType.XML_TIME_ZONE)
        {
            return "XMLTZone";
        }
        else
        {
            return "none";
        }
    }

    static FieldReferenceType parseFieldReferenceType(String parameter)
    {
        if (parameter.equals("AllDayEvent"))
        {
            return FieldReferenceType.ALL_DAY_EVENT;
        }
        else if (parameter.equals("CPLink"))
        {
            return FieldReferenceType.CP_LINK;
        }
        else if (parameter.equals("Duration"))
        {
            return FieldReferenceType.DURATION;
        }
        else if (parameter.equals("EndDate"))
        {
            return FieldReferenceType.END_DATE;
        }
        else if (parameter.equals("EventCancel"))
        {
            return FieldReferenceType.EVENT_CANCEL;
        }
        else if (parameter.equals("EventType"))
        {
            return FieldReferenceType.EVENT_TYPE;
        }
        else if (parameter.equals("LinkURL"))
        {
            return FieldReferenceType.LINK_URL;
        }
        else if (parameter.equals("MasterSeriesItemID"))
        {
            return FieldReferenceType.MASTER_SERIES_ITEM_ID;
        }
        else if (parameter.equals("ThemeHtml"))
        {
            return FieldReferenceType.RECURRENCE;
        }
        else if (parameter.equals("RecurData"))
        {
            return FieldReferenceType.RECURRENCE_DATA;
        }
        else if (parameter.equals("RecurrenceId"))
        {
            return FieldReferenceType.RECURRENCE_ID;
        }
        else if (parameter.equals("StartDate"))
        {
            return FieldReferenceType.START_DATE;
        }
        else if (parameter.equals("TimeZone"))
        {
            return FieldReferenceType.TIME_ZONE;
        }
        else if (parameter.equals("UID"))
        {
            return FieldReferenceType.UID;
        }
        else if (parameter.equals("XMLTZone"))
        {
            return FieldReferenceType.XML_TIME_ZONE;
        }
        else
        {
            return FieldReferenceType.NONE;
        }
    }

    static ViewType parseViewType(String parameter)
    {
        if (parameter.equals("HTML"))
        {
            return ViewType.HTML;
        }
        else if (parameter.equals("GRID"))
        {
            return ViewType.GRID;
        }
        else if (parameter.equals("CALENDAR"))
        {
            return ViewType.CALENDAR;
        }
        else if (parameter.equals("RECURRENCE"))
        {
            return ViewType.RECURRENCE;
        }
        else if (parameter.equals("CHART"))
        {
            return ViewType.CHART;
        }
        else if (parameter.equals("GANTT"))
        {
            return ViewType.GANTT;
        }
        else
        {
            return ViewType.NONE;
        }
    }

    static String parseViewType(ViewType parameter)
    {
        if (parameter == ViewType.HTML)
        {
            return "HTML";
        }
        else if (parameter == ViewType.GRID)
        {
            return "GRID";
        }
        else if (parameter == ViewType.CALENDAR)
        {
            return "CALENDAR";
        }
        else if (parameter == ViewType.RECURRENCE)
        {
            return "RECURRENCE";
        }
        else if (parameter == ViewType.CHART)
        {
            return "CHART";
        }
        else if (parameter == ViewType.GANTT)
        {
            return "GANTT";
        }
        else
        {
            return "None";
        }
    }

    static ListJoinType parseListJoinType(String parameter)
    {
        if (parameter.equals("LEFT"))
        {
            return ListJoinType.LEFT;
        }
        else
        {
            return ListJoinType.INNER;
        }
    }

    static String parseListJoinType(ListJoinType parameter)
    {
        if (parameter == ListJoinType.LEFT)
        {
            return "LEFT";
        }
        else
        {
            return "INNER";
        }
    }

    static OptimizeFor parseOptimizeFor(String parameter)
    {
        if (parameter.equals("ItemIds"))
        {
            return OptimizeFor.ITEM_IDS;
        }
        else if (parameter.equals("FolderUrls"))
        {
            return OptimizeFor.FOLDER_URLS;
        }
        else
        {
            return OptimizeFor.NONE;
        }
    }

    static String parseOptimizeFor(OptimizeFor parameter)
    {
        if (parameter == OptimizeFor.ITEM_IDS)
        {
            return "ItemIds";
        }
        else if (parameter == OptimizeFor.FOLDER_URLS)
        {
            return "FolderUrls";
        }
        else
        {
            return "None";
        }
    }

    static RecurrencePatternXmlVersion parseRecurrencePatternXmlVersion(String parameter)
    {
        if (parameter.equals("v3"))
        {
            return RecurrencePatternXmlVersion.V3;
        }
        else
        {
            return RecurrencePatternXmlVersion.NONE;
        }
    }

    static String parseRecurrencePatternXmlVersion(RecurrencePatternXmlVersion parameter)
    {
        if (parameter == RecurrencePatternXmlVersion.V3)
        {
            return "v3";
        }
        else
        {
            return "None";
        }
    }

    static String parseEdmType(EdmType parameter)
    {
        if (parameter == EdmType.BINARY)
        {
            return "Edm.Binary";
        }
        else if (parameter == EdmType.BOOLEAN)
        {
            return "Edm.Boolean";
        }
        else if (parameter == EdmType.BYTE)
        {
            return "Edm.Byte";
        }
        else if (parameter == EdmType.DATE_TIME)
        {
            return "Edm.DateTime";
        }
        else if (parameter == EdmType.DATE_TIME_OFFSET)
        {
            return "Edm.DateTimeOffset";
        }
        else if (parameter == EdmType.DECIMAL)
        {
            return "Edm.Decimal";
        }
        else if (parameter == EdmType.DOUBLE)
        {
            return "Edm.Double";
        }
        else if (parameter == EdmType.GUID)
        {
            return "Edm.Guid";
        }
        else if (parameter == EdmType.INT_16)
        {
            return "Edm.Int16";
        }
        else if (parameter == EdmType.INT_32)
        {
            return "Edm.Int32";
        }
        else if (parameter == EdmType.INT_64)
        {
            return "Edm.Int64";
        }
        else if (parameter == EdmType.S_BYTE)
        {
            return "Edm.SByte";
        }
        else if (parameter == EdmType.SINGLE)
        {
            return "Edm.Single";
        }
        else if (parameter == EdmType.STRING)
        {
            return "Edm.String";
        }
        else
        {
            return "Edm.Time";
        }
    }

    static EdmType parseEdmType(String parameter)
    {
        if (parameter.equals("Edm.Binary"))
        {
            return EdmType.BINARY;
        }
        else if (parameter.equals("Edm.Boolean"))
        {
            return EdmType.BOOLEAN;
        }
        else if (parameter.equals("Edm.Byte"))
        {
            return EdmType.BYTE;
        }
        else if (parameter.equals("Edm.DateTime"))
        {
            return EdmType.DATE_TIME;
        }
        else if (parameter.equals("Edm.DateTimeOffset"))
        {
            return EdmType.DATE_TIME_OFFSET;
        }
        else if (parameter.equals("Edm.Decimal"))
        {
            return EdmType.DECIMAL;
        }
        else if (parameter.equals("Edm.Double"))
        {
            return EdmType.DOUBLE;
        }
        else if (parameter.equals("Edm.Guid"))
        {
            return EdmType.GUID;
        }
        else if (parameter.equals("Edm.Int16"))
        {
            return EdmType.INT_16;
        }
        else if (parameter.equals("Edm.Int32"))
        {
            return EdmType.INT_32;
        }
        else if (parameter.equals("Edm.Int64"))
        {
            return EdmType.INT_64;
        }
        else if (parameter.equals("Edm.SByte"))
        {
            return EdmType.S_BYTE;
        }
        else if (parameter.equals("Edm.Single"))
        {
            return EdmType.SINGLE;
        }
        else if (parameter.equals("Edm.String"))
        {
            return EdmType.STRING;
        }
        else
        {
            return EdmType.TIME;
        }
    }
    
    static String parseChangeTokenScope(ChangeTokenScope parameter)
    {
        if (parameter == ChangeTokenScope.CONTENT_DB)
        {
            return "0";
        }
        else if (parameter == ChangeTokenScope.SITE_COLLECTION)
        {
            return "1";
        }
        else if (parameter == ChangeTokenScope.SITE)
        {
            return "2";
        }
        else
        {
            return "3";
        }
    }
    
    static ChangeTokenScope parseChangeTokenScope(String parameter)
    {
        if (parameter.equals("0"))
        {
            return ChangeTokenScope.CONTENT_DB;
        }
        else if (parameter.equals("1"))
        {
            return ChangeTokenScope.SITE_COLLECTION;
        }
        else if (parameter.equals("2"))
        {
            return ChangeTokenScope.SITE;
        }
        else
        {
            return ChangeTokenScope.LIST;
        }
    }

    static String parseControlMode(ControlMode parameter)
    {
        if (parameter == ControlMode.DISPLAY)
        {
            return "1";
        }
        else if (parameter == ControlMode.EDIT)
        {
            return "2";
        }
        else
        {
            return "3";
        }
    }

    static ControlMode parseControlMode(String parameter)
    {
        if (parameter.equals("1"))
        {
            return ControlMode.DISPLAY;
        }
        else if (parameter.equals("2"))
        {
            return ControlMode.EDIT;
        }
        else
        {
            return ControlMode.NEW;
        }
    }

    static String parseViewScope(ViewScope parameter)
    {
        if (parameter == ViewScope.RECURSIVE)
        {
            return "Recursive";
        }
        else if (parameter == ViewScope.RECURSIVE_ALL)
        {
            return "RecursiveAll";
        }
        else if (parameter == ViewScope.FILES_ONLY)
        {
            return "FilesOnly";
        }
        else
        {
            return "none";
        }
    }

    static ViewScope parseViewScope(String parameter)
    {
        if (parameter.equals("Recursive"))
        {
            return ViewScope.RECURSIVE;
        }
        else if (parameter.equals("RecursiveAll"))
        {
            return ViewScope.RECURSIVE_ALL;
        }
        else if (parameter.equals("FilesOnly"))
        {
            return ViewScope.FILES_ONLY;
        }
        else
        {
            return ViewScope.NONE;
        }
    }

    static String parseLocale(Locale parameter)
    {
        if (parameter == Locale.AFRIKAANS)
        {
            return "1078";
        }
        else if (parameter == Locale.ALBANIAN)
        {
            return "1052";
        }
        else if (parameter == Locale.ARABIC_ALGERIA)
        {
            return "5121";
        }
        else if (parameter == Locale.ARABIC_BAHRAIN)
        {
            return "15361";
        }
        else if (parameter == Locale.ARABIC_EGYPT)
        {
            return "3073";
        }
        else if (parameter == Locale.ARABIC_IRAQ)
        {
            return "2049";
        }
        else if (parameter == Locale.ARABIC_JORDAN)
        {
            return "11265";
        }
        else if (parameter == Locale.ARABIC_LEBANON)
        {
            return "12289";
        }
        else if (parameter == Locale.ARABIC_LIBYA)
        {
            return "4097";
        }
        else if (parameter == Locale.ARABIC_MOROCCO)
        {
            return "6145";
        }
        else if (parameter == Locale.ARABIC_OMAN)
        {
            return "8193";
        }
        else if (parameter == Locale.ARABIC_QATAR)
        {
            return "16385";
        }
        else if (parameter == Locale.ARABIC_SAUDI_ARABIA)
        {
            return "1025";
        }
        else if (parameter == Locale.ARABIC_SYRIA)
        {
            return "10241";
        }
        else if (parameter == Locale.ARABIC_TUNISIA)
        {
            return "7169";
        }
        else if (parameter == Locale.ARABIC_UAE)
        {
            return "14337";
        }
        else if (parameter == Locale.ARABIC_YEMEN)
        {
            return "9217";
        }
        else if (parameter == Locale.ARMENIAN)
        {
            return "9217";
        }
        else if (parameter == Locale.AZERI_CYRILLIC)
        {
            return "2092";
        }
        else if (parameter == Locale.AZERI_LATIN)
        {
            return "1068";
        }
        else if (parameter == Locale.BASQUE)
        {
            return "1069";
        }
        else if (parameter == Locale.BELARUSIAN)
        {
            return "1059";
        }
        else if (parameter == Locale.BULGARIAN)
        {
            return "1026";
        }
        else if (parameter == Locale.CATALAN)
        {
            return "1027";
        }
        else if (parameter == Locale.CHINESE_HONG_KONG)
        {
            return "3076";
        }
        else if (parameter == Locale.CHINESE_MACAO)
        {
            return "5124";
        }
        else if (parameter == Locale.CHINESE_CHINA)
        {
            return "2052";
        }
        else if (parameter == Locale.CHINESE_SINGAPORE)
        {
            return "4100";
        }
        else if (parameter == Locale.CHINESE_TAIWAN)
        {
            return "1028";
        }
        else if (parameter == Locale.CROATIAN)
        {
            return "1050";
        }
        else if (parameter == Locale.CZECH)
        {
            return "1029";
        }
        else if (parameter == Locale.DANISH)
        {
            return "1030";
        }
        else if (parameter == Locale.DIVEHI)
        {
            return "1125";
        }
        else if (parameter == Locale.DUTCH_BELGIUM)
        {
            return "2067";
        }
        else if (parameter == Locale.DUTCH_NETHERLANDS)
        {
            return "1043";
        }
        else if (parameter == Locale.ENGLISH_AUSTRALIA)
        {
            return "3081";
        }
        else if (parameter == Locale.ENGLISH_BELIZE)
        {
            return "10249";
        }
        else if (parameter == Locale.ENGLISH_CANADA)
        {
            return "4105";
        }
        else if (parameter == Locale.ENGLISH_CARIBBEAN)
        {
            return "9225";
        }
        else if (parameter == Locale.ENGLISH_IRELAND)
        {
            return "6153";
        }
        else if (parameter == Locale.ENGLISH_JAMAICA)
        {
            return "8201";
        }
        else if (parameter == Locale.ENGLISH_NEW_ZEALAND)
        {
            return "5129";
        }
        else if (parameter == Locale.ENGLISH_PHILIPPINES)
        {
            return "13321";
        }
        else if (parameter == Locale.ENGLISH_SOUTH_AFRICA)
        {
            return "7177";
        }
        else if (parameter == Locale.ENGLISH_TRINIDAD)
        {
            return "11273";
        }
        else if (parameter == Locale.ENGLISH_UNITED_KINGDOM)
        {
            return "2057";
        }
        else if (parameter == Locale.ENGLISH_UNITED_STATES)
        {
            return "1033";
        }
        else if (parameter == Locale.ENGLISH_ZIMBABWE)
        {
            return "12297";
        }
        else if (parameter == Locale.ESTONIAN)
        {
            return "1061";
        }
        else if (parameter == Locale.FAEROESE)
        {
            return "1080";
        }
        else if (parameter == Locale.FINNISH)
        {
            return "1035";
        }
        else if (parameter == Locale.FRENCH_BELGIUM)
        {
            return "2060";
        }
        else if (parameter == Locale.FRENCH_CANADA)
        {
            return "3084";
        }
        else if (parameter == Locale.FRENCH_FRANCE)
        {
            return "1036";
        }
        else if (parameter == Locale.FRENCH_LUXEMBOURG)
        {
            return "5132";
        }
        else if (parameter == Locale.FRENCH_MONACO)
        {
            return "6156";
        }
        else if (parameter == Locale.FRENCH_SWITZERLAND)
        {
            return "4108";
        }
        else if (parameter == Locale.GALICIAN)
        {
            return "1110";
        }
        else if (parameter == Locale.GEORGIAN)
        {
            return "1079";
        }
        else if (parameter == Locale.GERMAN_AUSTRIA)
        {
            return "3079";
        }
        else if (parameter == Locale.GERMAN_GERMANY)
        {
            return "1031";
        }
        else if (parameter == Locale.GERMAN_LIECHTENSTEIN)
        {
            return "5127";
        }
        else if (parameter == Locale.GERMAN_LUXEMBOURG)
        {
            return "4103";
        }
        else if (parameter == Locale.GERMAN_SWITZERLAND)
        {
            return "2055";
        }
        else if (parameter == Locale.GREEK)
        {
            return "1032";
        }
        else if (parameter == Locale.GUJARTI)
        {
            return "1095";
        }
        else if (parameter == Locale.HEBREW)
        {
            return "1037";
        }
        else if (parameter == Locale.HINDI)
        {
            return "1081";
        }
        else if (parameter == Locale.HUNGARIAN)
        {
            return "1038";
        }
        else if (parameter == Locale.ICELANDIC)
        {
            return "1039";
        }
        else if (parameter == Locale.INDONESIAN)
        {
            return "1057";
        }
        else if (parameter == Locale.ITALIAN_ITALY)
        {
            return "1040";
        }
        else if (parameter == Locale.ITALIAN_SWITZERLAND)
        {
            return "2064";
        }
        else if (parameter == Locale.JAPANESE)
        {
            return "1041";
        }
        else if (parameter == Locale.KANNADA)
        {
            return "1099";
        }
        else if (parameter == Locale.KAZAKH)
        {
            return "1087";
        }
        else if (parameter == Locale.KONKANI)
        {
            return "1111";
        }
        else if (parameter == Locale.KOREAN)
        {
            return "1042";
        }
        else if (parameter == Locale.KYRGYZ_CYRILLIC)
        {
            return "1088";
        }
        else if (parameter == Locale.LATVIAN)
        {
            return "1062";
        }
        else if (parameter == Locale.LITHUANIAN)
        {
            return "1063";
        }
        else if (parameter == Locale.MACEDONIAN)
        {
            return "1071";
        }
        else if (parameter == Locale.MALAY)
        {
            return "1086";
        }
        else if (parameter == Locale.MALAY_BUNEI_DARUSSALAM)
        {
            return "2110";
        }
        else if (parameter == Locale.MARATHI)
        {
            return "1102";
        }
        else if (parameter == Locale.MONGOLIAN_CYRILLIC)
        {
            return "1104";
        }
        else if (parameter == Locale.NORWEGIAN_BOKMAL)
        {
            return "1044";
        }
        else if (parameter == Locale.NORWEGIAN_NYNORSK)
        {
            return "2068";
        }
        else if (parameter == Locale.PERSIAN)
        {
            return "1065";
        }
        else if (parameter == Locale.POLISH)
        {
            return "1045";
        }
        else if (parameter == Locale.PORTUGUESE_BRAZIL)
        {
            return "1046";
        }
        else if (parameter == Locale.PORTUGUESE_PORTUGAL)
        {
            return "2070";
        }
        else if (parameter == Locale.PUNJABI)
        {
            return "1094";
        }
        else if (parameter == Locale.ROMANIAN)
        {
            return "1048";
        }
        else if (parameter == Locale.RUSSIAN)
        {
            return "1049";
        }
        else if (parameter == Locale.SANSKRIT)
        {
            return "1103";
        }
        else if (parameter == Locale.SERBIAN_CYRILLIC)
        {
            return "3098";
        }
        else if (parameter == Locale.SERBIAN_LATIN)
        {
            return "2074";
        }
        else if (parameter == Locale.SLOVAK)
        {
            return "1051";
        }
        else if (parameter == Locale.SLOVENIAN)
        {
            return "1060";
        }
        else if (parameter == Locale.SPANISH_ARGENTINA)
        {
            return "11274";
        }
        else if (parameter == Locale.SPANISH_BOLIVIA)
        {
            return "16394";
        }
        else if (parameter == Locale.SPANISH_CHILE)
        {
            return "13322";
        }
        else if (parameter == Locale.SPANISH_COLOMBIA)
        {
            return "9226";
        }
        else if (parameter == Locale.SPANISH_COSTA_RICA)
        {
            return "5130";
        }
        else if (parameter == Locale.SPANISH_DOMINICAN_REPUBLIC)
        {
            return "7178";
        }
        else if (parameter == Locale.SPANISH_ECUADOR)
        {
            return "12298";
        }
        else if (parameter == Locale.SPANISH_EL_SALVADOR)
        {
            return "17418";
        }
        else if (parameter == Locale.SPANISH_GUATEMALA)
        {
            return "4106";
        }
        else if (parameter == Locale.SPANISH_HONDURAS)
        {
            return "18442";
        }
        else if (parameter == Locale.SPANISH_MEXICO)
        {
            return "2058";
        }
        else if (parameter == Locale.SPANISH_NICARAGUA)
        {
            return "19466";
        }
        else if (parameter == Locale.SPANISH_PANAMA)
        {
            return "6154";
        }
        else if (parameter == Locale.SPANISH_PARAGUAY)
        {
            return "15370";
        }
        else if (parameter == Locale.SPANISH_PERU)
        {
            return "10250";
        }
        else if (parameter == Locale.SPANISH_PUERTO_RICO)
        {
            return "20490";
        }
        else if (parameter == Locale.SPANISH_SPAIN)
        {
            return "3082";
        }
        else if (parameter == Locale.SPANISH_URUGUAY)
        {
            return "14346";
        }
        else if (parameter == Locale.SPANISH_VENEZUELA)
        {
            return "8202";
        }
        else if (parameter == Locale.SWAHILI)
        {
            return "1089";
        }
        else if (parameter == Locale.SWEDISH)
        {
            return "1053";
        }
        else if (parameter == Locale.SWEDISH_FINLAND)
        {
            return "2077";
        }
        else if (parameter == Locale.SYRIAC)
        {
            return "1114";
        }
        else if (parameter == Locale.TAMIL)
        {
            return "1097";
        }
        else if (parameter == Locale.TATAR)
        {
            return "1092";
        }
        else if (parameter == Locale.TELUGU)
        {
            return "1098";
        }
        else if (parameter == Locale.THAI)
        {
            return "1054";
        }
        else if (parameter == Locale.TURKISH)
        {
            return "1055";
        }
        else if (parameter == Locale.UKRAINIAN)
        {
            return "1058";
        }
        else if (parameter == Locale.URDU)
        {
            return "1056";
        }
        else if (parameter == Locale.UZBEK_CYRILLIC)
        {
            return "2115";
        }
        else if (parameter == Locale.UZBEK_LATIN)
        {
            return "1091";
        }
        else if (parameter == Locale.VIETNAMESE)
        {
            return "1066";
        }
        else
        {
            return "0";
        }
    }

    static Locale parseLocale(String parameter)
    {
        if (parameter.equals("1078"))
        {
            return Locale.AFRIKAANS;
        }
        else if (parameter.equals("1052"))
        {
            return Locale.ALBANIAN;
        }
        else if (parameter.equals("5121"))
        {
            return Locale.ARABIC_ALGERIA;
        }
        else if (parameter.equals("15361"))
        {
            return Locale.ARABIC_BAHRAIN;
        }
        else if (parameter.equals("3073"))
        {
            return Locale.ARABIC_EGYPT;
        }
        else if (parameter.equals("2049"))
        {
            return Locale.ARABIC_IRAQ;
        }
        else if (parameter.equals("11265"))
        {
            return Locale.ARABIC_JORDAN;
        }
        else if (parameter.equals("12289"))
        {
            return Locale.ARABIC_LEBANON;
        }
        else if (parameter.equals("4097"))
        {
            return Locale.ARABIC_LIBYA;
        }
        else if (parameter.equals("6145"))
        {
            return Locale.ARABIC_MOROCCO;
        }
        else if (parameter.equals("8193"))
        {
            return Locale.ARABIC_OMAN;
        }
        else if (parameter.equals("16385"))
        {
            return Locale.ARABIC_QATAR;
        }
        else if (parameter.equals("1025"))
        {
            return Locale.ARABIC_SAUDI_ARABIA;
        }
        else if (parameter.equals("10241"))
        {
            return Locale.ARABIC_SYRIA;
        }
        else if (parameter.equals("7169"))
        {
            return Locale.ARABIC_TUNISIA;
        }
        else if (parameter.equals("14337"))
        {
            return Locale.ARABIC_UAE;
        }
        else if (parameter.equals("9217"))
        {
            return Locale.ARABIC_YEMEN;
        }
        else if (parameter.equals("9217"))
        {
            return Locale.ARMENIAN;
        }
        else if (parameter.equals("2092"))
        {
            return Locale.AZERI_CYRILLIC;
        }
        else if (parameter.equals("1068"))
        {
            return Locale.AZERI_LATIN;
        }
        else if (parameter.equals("1069"))
        {
            return Locale.BASQUE;
        }
        else if (parameter.equals("1059"))
        {
            return Locale.BELARUSIAN;
        }
        else if (parameter.equals("1026"))
        {
            return Locale.BULGARIAN;
        }
        else if (parameter.equals("1027"))
        {
            return Locale.CATALAN;
        }
        else if (parameter.equals("3076"))
        {
            return Locale.CHINESE_HONG_KONG;
        }
        else if (parameter.equals("5124"))
        {
            return Locale.CHINESE_MACAO;
        }
        else if (parameter.equals("2052"))
        {
            return Locale.CHINESE_CHINA;
        }
        else if (parameter.equals("4100"))
        {
            return Locale.CHINESE_SINGAPORE;
        }
        else if (parameter.equals("1028"))
        {
            return Locale.CHINESE_TAIWAN;
        }
        else if (parameter.equals("1050"))
        {
            return Locale.CROATIAN;
        }
        else if (parameter.equals("1029"))
        {
            return Locale.CZECH;
        }
        else if (parameter.equals("1030"))
        {
            return Locale.DANISH;
        }
        else if (parameter.equals("1125"))
        {
            return Locale.DIVEHI;
        }
        else if (parameter.equals("2067"))
        {
            return Locale.DUTCH_BELGIUM;
        }
        else if (parameter.equals("1043"))
        {
            return Locale.DUTCH_NETHERLANDS;
        }
        else if (parameter.equals("3081"))
        {
            return Locale.ENGLISH_AUSTRALIA;
        }
        else if (parameter.equals("10249"))
        {
            return Locale.ENGLISH_BELIZE;
        }
        else if (parameter.equals("4105"))
        {
            return Locale.ENGLISH_CANADA;
        }
        else if (parameter.equals("9225"))
        {
            return Locale.ENGLISH_CARIBBEAN;
        }
        else if (parameter.equals("6153"))
        {
            return Locale.ENGLISH_IRELAND;
        }
        else if (parameter.equals("8201"))
        {
            return Locale.ENGLISH_JAMAICA;
        }
        else if (parameter.equals("5129"))
        {
            return Locale.ENGLISH_NEW_ZEALAND;
        }
        else if (parameter.equals("13321"))
        {
            return Locale.ENGLISH_PHILIPPINES;
        }
        else if (parameter.equals("7177"))
        {
            return Locale.ENGLISH_SOUTH_AFRICA;
        }
        else if (parameter.equals("11273"))
        {
            return Locale.ENGLISH_TRINIDAD;
        }
        else if (parameter.equals("2057"))
        {
            return Locale.ENGLISH_UNITED_KINGDOM;
        }
        else if (parameter.equals("1033"))
        {
            return Locale.ENGLISH_UNITED_STATES;
        }
        else if (parameter.equals("12297"))
        {
            return Locale.ENGLISH_ZIMBABWE;
        }
        else if (parameter.equals("1061"))
        {
            return Locale.ESTONIAN;
        }
        else if (parameter.equals("1080"))
        {
            return Locale.FAEROESE;
        }
        else if (parameter.equals("1035"))
        {
            return Locale.FINNISH;
        }
        else if (parameter.equals("2060"))
        {
            return Locale.FRENCH_BELGIUM;
        }
        else if (parameter.equals("3084"))
        {
            return Locale.FRENCH_CANADA;
        }
        else if (parameter.equals("1036"))
        {
            return Locale.FRENCH_FRANCE;
        }
        else if (parameter.equals("5132"))
        {
            return Locale.FRENCH_LUXEMBOURG;
        }
        else if (parameter.equals("6156"))
        {
            return Locale.FRENCH_MONACO;
        }
        else if (parameter.equals("4108"))
        {
            return Locale.FRENCH_SWITZERLAND;
        }
        else if (parameter.equals("1110"))
        {
            return Locale.GALICIAN;
        }
        else if (parameter.equals("1079"))
        {
            return Locale.GEORGIAN;
        }
        else if (parameter.equals("3079"))
        {
            return Locale.GERMAN_AUSTRIA;
        }
        else if (parameter.equals("1031"))
        {
            return Locale.GERMAN_GERMANY;
        }
        else if (parameter.equals("5127"))
        {
            return Locale.GERMAN_LIECHTENSTEIN;
        }
        else if (parameter.equals("4103"))
        {
            return Locale.GERMAN_LUXEMBOURG;
        }
        else if (parameter.equals("2055"))
        {
            return Locale.GERMAN_SWITZERLAND;
        }
        else if (parameter.equals("1032"))
        {
            return Locale.GREEK;
        }
        else if (parameter.equals("1095"))
        {
            return Locale.GUJARTI;
        }
        else if (parameter.equals("1037"))
        {
            return Locale.HEBREW;
        }
        else if (parameter.equals("1081"))
        {
            return Locale.HINDI;
        }
        else if (parameter.equals("1038"))
        {
            return Locale.HUNGARIAN;
        }
        else if (parameter.equals("1039"))
        {
            return Locale.ICELANDIC;
        }
        else if (parameter.equals("1057"))
        {
            return Locale.INDONESIAN;
        }
        else if (parameter.equals("1040"))
        {
            return Locale.ITALIAN_ITALY;
        }
        else if (parameter.equals("2064"))
        {
            return Locale.ITALIAN_SWITZERLAND;
        }
        else if (parameter.equals("1041"))
        {
            return Locale.JAPANESE;
        }
        else if (parameter.equals("1099"))
        {
            return Locale.KANNADA;
        }
        else if (parameter.equals("1087"))
        {
            return Locale.KAZAKH;
        }
        else if (parameter.equals("1111"))
        {
            return Locale.KONKANI;
        }
        else if (parameter.equals("1042"))
        {
            return Locale.KOREAN;
        }
        else if (parameter.equals("1088"))
        {
            return Locale.KYRGYZ_CYRILLIC;
        }
        else if (parameter.equals("1062"))
        {
            return Locale.LATVIAN;
        }
        else if (parameter.equals("1063"))
        {
            return Locale.LITHUANIAN;
        }
        else if (parameter.equals("1071"))
        {
            return Locale.MACEDONIAN;
        }
        else if (parameter.equals("1086"))
        {
            return Locale.MALAY;
        }
        else if (parameter.equals("2110"))
        {
            return Locale.MALAY_BUNEI_DARUSSALAM;
        }
        else if (parameter.equals("1102"))
        {
            return Locale.MARATHI;
        }
        else if (parameter.equals("1104"))
        {
            return Locale.MONGOLIAN_CYRILLIC;
        }
        else if (parameter.equals("1044"))
        {
            return Locale.NORWEGIAN_BOKMAL;
        }
        else if (parameter.equals("2068"))
        {
            return Locale.NORWEGIAN_NYNORSK;
        }
        else if (parameter.equals("1065"))
        {
            return Locale.PERSIAN;
        }
        else if (parameter.equals("1045"))
        {
            return Locale.POLISH;
        }
        else if (parameter.equals("1046"))
        {
            return Locale.PORTUGUESE_BRAZIL;
        }
        else if (parameter.equals("2070"))
        {
            return Locale.PORTUGUESE_PORTUGAL;
        }
        else if (parameter.equals("1094"))
        {
            return Locale.PUNJABI;
        }
        else if (parameter.equals("1048"))
        {
            return Locale.ROMANIAN;
        }
        else if (parameter.equals("1049"))
        {
            return Locale.RUSSIAN;
        }
        else if (parameter.equals("1103"))
        {
            return Locale.SANSKRIT;
        }
        else if (parameter.equals("3098"))
        {
            return Locale.SERBIAN_CYRILLIC;
        }
        else if (parameter.equals("2074"))
        {
            return Locale.SERBIAN_LATIN;
        }
        else if (parameter.equals("1051"))
        {
            return Locale.SLOVAK;
        }
        else if (parameter.equals("1060"))
        {
            return Locale.SLOVENIAN;
        }
        else if (parameter.equals("11274"))
        {
            return Locale.SPANISH_ARGENTINA;
        }
        else if (parameter.equals("16394"))
        {
            return Locale.SPANISH_BOLIVIA;
        }
        else if (parameter.equals("13322"))
        {
            return Locale.SPANISH_CHILE;
        }
        else if (parameter.equals("9226"))
        {
            return Locale.SPANISH_COLOMBIA;
        }
        else if (parameter.equals("5130"))
        {
            return Locale.SPANISH_COSTA_RICA;
        }
        else if (parameter.equals("7178"))
        {
            return Locale.SPANISH_DOMINICAN_REPUBLIC;
        }
        else if (parameter.equals("12298"))
        {
            return Locale.SPANISH_ECUADOR;
        }
        else if (parameter.equals("17418"))
        {
            return Locale.SPANISH_EL_SALVADOR;
        }
        else if (parameter.equals("4106"))
        {
            return Locale.SPANISH_GUATEMALA;
        }
        else if (parameter.equals("18442"))
        {
            return Locale.SPANISH_HONDURAS;
        }
        else if (parameter.equals("2058"))
        {
            return Locale.SPANISH_MEXICO;
        }
        else if (parameter.equals("19466"))
        {
            return Locale.SPANISH_NICARAGUA;
        }
        else if (parameter.equals("6154"))
        {
            return Locale.SPANISH_PANAMA;
        }
        else if (parameter.equals("15370"))
        {
            return Locale.SPANISH_PARAGUAY;
        }
        else if (parameter.equals("10250"))
        {
            return Locale.SPANISH_PERU;
        }
        else if (parameter.equals("20490"))
        {
            return Locale.SPANISH_PUERTO_RICO;
        }
        else if (parameter.equals("3082"))
        {
            return Locale.SPANISH_SPAIN;
        }
        else if (parameter.equals("14346"))
        {
            return Locale.SPANISH_URUGUAY;
        }
        else if (parameter.equals("8202"))
        {
            return Locale.SPANISH_VENEZUELA;
        }
        else if (parameter.equals("1089"))
        {
            return Locale.SWAHILI;
        }
        else if (parameter.equals("1053"))
        {
            return Locale.SWEDISH;
        }
        else if (parameter.equals("2077"))
        {
            return Locale.SWEDISH_FINLAND;
        }
        else if (parameter.equals("1114"))
        {
            return Locale.SYRIAC;
        }
        else if (parameter.equals("1097"))
        {
            return Locale.TAMIL;
        }
        else if (parameter.equals("1092"))
        {
            return Locale.TATAR;
        }
        else if (parameter.equals("1098"))
        {
            return Locale.TELUGU;
        }
        else if (parameter.equals("1054"))
        {
            return Locale.THAI;
        }
        else if (parameter.equals("1055"))
        {
            return Locale.TURKISH;
        }
        else if (parameter.equals("1058"))
        {
            return Locale.UKRAINIAN;
        }
        else if (parameter.equals("1056"))
        {
            return Locale.URDU;
        }
        else if (parameter.equals("2115"))
        {
            return Locale.UZBEK_CYRILLIC;
        }
        else if (parameter.equals("1091"))
        {
            return Locale.UZBEK_LATIN;
        }
        else if (parameter.equals("1066"))
        {
            return Locale.VIETNAMESE;
        }
        else
        {
            return Locale.NONE;
        }
    }

    static EventReceiverType parseEventReceiverType(String parameter)
    {
        if (parameter.equals("-1"))
        {
            return EventReceiverType.INVALID_RECEIVER;
        }
        else if (parameter.equals("1"))
        {
            return EventReceiverType.ITEM_ADDING;
        }
        else if (parameter.equals("2"))
        {
            return EventReceiverType.ITEM_UPDATING;
        }
        else if (parameter.equals("3"))
        {
            return EventReceiverType.ITEM_DELETING;
        }
        else if (parameter.equals("4"))
        {
            return EventReceiverType.ITEM_CHECKING_IN;
        }
        else if (parameter.equals("5"))
        {
            return EventReceiverType.ITEM_CHECKING_OUT;
        }
        else if (parameter.equals("6"))
        {
            return EventReceiverType.ITEM_UNCHECKED_OUT;
        }
        else if (parameter.equals("7"))
        {
            return EventReceiverType.ITEM_ATTACHMENT_ADDING;
        }
        else if (parameter.equals("8"))
        {
            return EventReceiverType.ITEM_ATTACHMENT_DELETING;
        }
        else if (parameter.equals("9"))
        {
            return EventReceiverType.ITEM_FILE_MOVING;
        }
        else if (parameter.equals("11"))
        {
            return EventReceiverType.ITEM_VERSION_DELETING;
        }
        else if (parameter.equals("101"))
        {
            return EventReceiverType.FIELD_ADDING;
        }
        else if (parameter.equals("102"))
        {
            return EventReceiverType.FIELD_UPDATING;
        }
        else if (parameter.equals("103"))
        {
            return EventReceiverType.FIELD_DELETING;
        }
        else if (parameter.equals("104"))
        {
            return EventReceiverType.LIST_ADDING;
        }
        else if (parameter.equals("105"))
        {
            return EventReceiverType.LIST_DELETING;
        }
        else if (parameter.equals("201"))
        {
            return EventReceiverType.SITE_DELETING;
        }
        else if (parameter.equals("202"))
        {
            return EventReceiverType.WEB_DELETING;
        }
        else if (parameter.equals("203"))
        {
            return EventReceiverType.WEB_MOVING;
        }
        else if (parameter.equals("204"))
        {
            return EventReceiverType.WEB_ADDING;
        }
        else if (parameter.equals("301"))
        {
            return EventReceiverType.GROUP_ADDING;
        }
        else if (parameter.equals("302"))
        {
            return EventReceiverType.GROUP_UPDATING;
        }
        else if (parameter.equals("303"))
        {
            return EventReceiverType.GROUP_DELETING;
        }
        else if (parameter.equals("304"))
        {
            return EventReceiverType.GROUP_USER_ADDING;
        }
        else if (parameter.equals("305"))
        {
            return EventReceiverType.GROUP_USER_DELETING;
        }
        else if (parameter.equals("306"))
        {
            return EventReceiverType.ROLE_DEFINITION_ADDING;
        }
        else if (parameter.equals("307"))
        {
            return EventReceiverType.ROLE_DEFINITION_UPDATING;
        }
        else if (parameter.equals("308"))
        {
            return EventReceiverType.ROLE_ASSIGNMENT_DELETING;
        }
        else if (parameter.equals("309"))
        {
            return EventReceiverType.ROLE_ASSIGNMENT_ADDING;
        }
        else if (parameter.equals("310"))
        {
            return EventReceiverType.ROLE_ASSIGNMENT_DELETING;
        }
        else if (parameter.equals("311"))
        {
            return EventReceiverType.INHERITANCE_BREAKING;
        }
        else if (parameter.equals("312"))
        {
            return EventReceiverType.INHERITANCE_RESETTING;
        }
        else if (parameter.equals("501"))
        {
            return EventReceiverType.WORKFLOW_STARTING;
        }
        else if (parameter.equals("10001"))
        {
            return EventReceiverType.ITEM_ADDED;
        }
        else if (parameter.equals("10002"))
        {
            return EventReceiverType.ITEM_UPDATED;
        }
        else if (parameter.equals("10003"))
        {
            return EventReceiverType.ITEM_DELETED;
        }
        else if (parameter.equals("10004"))
        {
            return EventReceiverType.ITEM_CHECKED_IN;
        }
        else if (parameter.equals("10005"))
        {
            return EventReceiverType.ITEM_CHECKED_OUT;
        }
        else if (parameter.equals("10006"))
        {
            return EventReceiverType.ITEM_UNCHECKED_OUT;
        }
        else if (parameter.equals("10007"))
        {
            return EventReceiverType.ITEM_ATTACHMENT_ADDED;
        }
        else if (parameter.equals("10008"))
        {
            return EventReceiverType.ITEM_ATTACHMENT_DELETED;
        }
        else if (parameter.equals("10009"))
        {
            return EventReceiverType.ITEM_FILE_MOVED;
        }
        else if (parameter.equals("10010"))
        {
            return EventReceiverType.ITEM_FILE_CONVERTED;
        }
        else if (parameter.equals("10011"))
        {
            return EventReceiverType.ITEM_VERSION_DELETED;
        }
        else if (parameter.equals("10101"))
        {
            return EventReceiverType.FIELD_ADDED;
        }
        else if (parameter.equals("10102"))
        {
            return EventReceiverType.FIELD_UPDATED;
        }
        else if (parameter.equals("10103"))
        {
            return EventReceiverType.FIELD_DELETED;
        }
        else if (parameter.equals("10104"))
        {
            return EventReceiverType.LIST_ADDED;
        }
        else if (parameter.equals("10105"))
        {
            return EventReceiverType.LIST_DELETED;
        }
        else if (parameter.equals("10201"))
        {
            return EventReceiverType.SITE_DELETED;
        }
        else if (parameter.equals("10202"))
        {
            return EventReceiverType.WEB_DELETED;
        }
        else if (parameter.equals("10203"))
        {
            return EventReceiverType.WEB_MOVED;
        }
        else if (parameter.equals("10204"))
        {
            return EventReceiverType.WEB_PROVISIONED;
        }
        else if (parameter.equals("10301"))
        {
            return EventReceiverType.GROUP_ADDED;
        }
        else if (parameter.equals("10302"))
        {
            return EventReceiverType.GROUP_UPDATED;
        }
        else if (parameter.equals("10303"))
        {
            return EventReceiverType.GROUP_DELETED;
        }
        else if (parameter.equals("10304"))
        {
            return EventReceiverType.GROUP_USER_ADDED;
        }
        else if (parameter.equals("10305"))
        {
            return EventReceiverType.GROUP_USER_DELETED;
        }
        else if (parameter.equals("10306"))
        {
            return EventReceiverType.ROLE_DEFINITION_ADDED;
        }
        else if (parameter.equals("10307"))
        {
            return EventReceiverType.ROLE_DEFINITION_UPDATED;
        }
        else if (parameter.equals("10308"))
        {
            return EventReceiverType.ROLE_DEFINITION_DELETED;
        }
        else if (parameter.equals("10309"))
        {
            return EventReceiverType.ROLE_ASSIGNMENT_ADDED;
        }
        else if (parameter.equals("10310"))
        {
            return EventReceiverType.ROLE_ASSIGNMENT_DELETED;
        }
        else if (parameter.equals("10311"))
        {
            return EventReceiverType.INHERITANCE_BROKEN;
        }
        else if (parameter.equals("10312"))
        {
            return EventReceiverType.INHERITANCE_RESET;
        }
        else if (parameter.equals("10501"))
        {
            return EventReceiverType.WORKFLOW_STARTED;
        }
        else if (parameter.equals("10502"))
        {
            return EventReceiverType.WORKFLOW_POSTPONED;
        }
        else if (parameter.equals("10503"))
        {
            return EventReceiverType.WORKFLOW_COMPLETED;
        }
        else if (parameter.equals("10601"))
        {
            return EventReceiverType.ENTITY_INSTANCE_ADDED;
        }
        else if (parameter.equals("10602"))
        {
            return EventReceiverType.ENTITY_INSTANCE_UPDATED;
        }
        else if (parameter.equals("10603"))
        {
            return EventReceiverType.ENTITY_INSTANCE_DELETED;
        }
        else if (parameter.equals("10701"))
        {
            return EventReceiverType.APP_INSTALLED;
        }
        else if (parameter.equals("10702"))
        {
            return EventReceiverType.APP_UPGRADED;
        }
        else if (parameter.equals("10703"))
        {
            return EventReceiverType.APP_UNINSTALLING;
        }
        else if (parameter.equals("20000"))
        {
            return EventReceiverType.EMAIL_RECEIVED;
        }
        else if (parameter.equals("32766"))
        {
            return EventReceiverType.CONTEXT_EVENT;
        }
        else
        {
            return EventReceiverType.NONE;
        }
    }

    static String parseEventReceiverType(EventReceiverType parameter)
    {
        if (parameter == EventReceiverType.INVALID_RECEIVER)
        {
            return "-1";
        }
        else if (parameter == EventReceiverType.ITEM_ADDING)
        {
            return "1";
        }
        else if (parameter == EventReceiverType.ITEM_UPDATING)
        {
            return "2";
        }
        else if (parameter == EventReceiverType.ITEM_DELETING)
        {
            return "3";
        }
        else if (parameter == EventReceiverType.ITEM_CHECKING_IN)
        {
            return "4";
        }
        else if (parameter == EventReceiverType.ITEM_CHECKING_OUT)
        {
            return "5";
        }
        else if (parameter == EventReceiverType.ITEM_UNCHECKING_OUT)
        {
            return "6";
        }
        else if (parameter == EventReceiverType.ITEM_ATTACHMENT_ADDING)
        {
            return "7";
        }
        else if (parameter == EventReceiverType.ITEM_ATTACHMENT_DELETING)
        {
            return "8";
        }
        else if (parameter == EventReceiverType.ITEM_FILE_MOVING)
        {
            return "9";
        }
        else if (parameter == EventReceiverType.ITEM_VERSION_DELETING)
        {
            return "11";
        }
        else if (parameter == EventReceiverType.FIELD_ADDING)
        {
            return "101";
        }
        else if (parameter == EventReceiverType.FIELD_UPDATING)
        {
            return "102";
        }
        else if (parameter == EventReceiverType.FIELD_DELETING)
        {
            return "103";
        }
        else if (parameter == EventReceiverType.LIST_ADDING)
        {
            return "104";
        }
        else if (parameter == EventReceiverType.LIST_DELETING)
        {
            return "105";
        }
        else if (parameter == EventReceiverType.SITE_DELETING)
        {
            return "201";
        }
        else if (parameter == EventReceiverType.WEB_DELETING)
        {
            return "202";
        }
        else if (parameter == EventReceiverType.WEB_MOVING)
        {
            return "203";
        }
        else if (parameter == EventReceiverType.WEB_ADDING)
        {
            return "204";
        }
        else if (parameter == EventReceiverType.GROUP_ADDING)
        {
            return "301";
        }
        else if (parameter == EventReceiverType.GROUP_UPDATING)
        {
            return "302";
        }
        else if (parameter == EventReceiverType.GROUP_DELETING)
        {
            return "303";
        }
        else if (parameter == EventReceiverType.GROUP_USER_ADDING)
        {
            return "304";
        }
        else if (parameter == EventReceiverType.GROUP_USER_DELETING)
        {
            return "305";
        }
        else if (parameter == EventReceiverType.ROLE_DEFINITION_ADDING)
        {
            return "306";
        }
        else if (parameter == EventReceiverType.ROLE_DEFINITION_UPDATING)
        {
            return "307";
        }
        else if (parameter == EventReceiverType.ROLE_DEFINITION_DELETING)
        {
            return "308";
        }
        else if (parameter == EventReceiverType.ROLE_ASSIGNMENT_ADDING)
        {
            return "309";
        }
        else if (parameter == EventReceiverType.ROLE_ASSIGNMENT_DELETING)
        {
            return "310";
        }
        else if (parameter == EventReceiverType.INHERITANCE_BREAKING)
        {
            return "311";
        }
        else if (parameter == EventReceiverType.INHERITANCE_RESETTING)
        {
            return "312";
        }
        else if (parameter == EventReceiverType.WORKFLOW_STARTING)
        {
            return "501";
        }
        else if (parameter == EventReceiverType.ITEM_ADDED)
        {
            return "10001";
        }
        else if (parameter == EventReceiverType.ITEM_UPDATED)
        {
            return "10002";
        }
        else if (parameter == EventReceiverType.ITEM_DELETED)
        {
            return "10003";
        }
        else if (parameter == EventReceiverType.ITEM_CHECKED_IN)
        {
            return "10004";
        }
        else if (parameter == EventReceiverType.ITEM_CHECKED_OUT)
        {
            return "10005";
        }
        else if (parameter == EventReceiverType.ITEM_UNCHECKED_OUT)
        {
            return "10006";
        }
        else if (parameter == EventReceiverType.ITEM_ATTACHMENT_ADDED)
        {
            return "10007";
        }
        else if (parameter == EventReceiverType.ITEM_ATTACHMENT_DELETED)
        {
            return "10008";
        }
        else if (parameter == EventReceiverType.ITEM_FILE_MOVED)
        {
            return "10009";
        }
        else if (parameter == EventReceiverType.ITEM_FILE_CONVERTED)
        {
            return "10010";
        }
        else if (parameter == EventReceiverType.ITEM_VERSION_DELETED)
        {
            return "10011";
        }
        else if (parameter == EventReceiverType.FIELD_ADDED)
        {
            return "10101";
        }
        else if (parameter == EventReceiverType.FIELD_UPDATED)
        {
            return "10102";
        }
        else if (parameter == EventReceiverType.FIELD_DELETED)
        {
            return "10103";
        }
        else if (parameter == EventReceiverType.LIST_ADDED)
        {
            return "10104";
        }
        else if (parameter == EventReceiverType.LIST_DELETED)
        {
            return "10105";
        }
        else if (parameter == EventReceiverType.SITE_DELETED)
        {
            return "10201";
        }
        else if (parameter == EventReceiverType.WEB_DELETED)
        {
            return "10202";
        }
        else if (parameter == EventReceiverType.WEB_MOVED)
        {
            return "10203";
        }
        else if (parameter == EventReceiverType.WEB_PROVISIONED)
        {
            return "10204";
        }
        else if (parameter == EventReceiverType.GROUP_ADDED)
        {
            return "10301";
        }
        else if (parameter == EventReceiverType.GROUP_UPDATED)
        {
            return "10302";
        }
        else if (parameter == EventReceiverType.GROUP_DELETED)
        {
            return "10303";
        }
        else if (parameter == EventReceiverType.GROUP_USER_ADDED)
        {
            return "10304";
        }
        else if (parameter == EventReceiverType.GROUP_USER_DELETED)
        {
            return "10305";
        }
        else if (parameter == EventReceiverType.ROLE_DEFINITION_ADDED)
        {
            return "10306";
        }
        else if (parameter == EventReceiverType.ROLE_DEFINITION_UPDATED)
        {
            return "10307";
        }
        else if (parameter == EventReceiverType.ROLE_DEFINITION_DELETED)
        {
            return "10308";
        }
        else if (parameter == EventReceiverType.ROLE_ASSIGNMENT_ADDED)
        {
            return "10309";
        }
        else if (parameter == EventReceiverType.ROLE_ASSIGNMENT_DELETED)
        {
            return "10310";
        }
        else if (parameter == EventReceiverType.INHERITANCE_BROKEN)
        {
            return "10311";
        }
        else if (parameter == EventReceiverType.INHERITANCE_RESET)
        {
            return "10312";
        }
        else if (parameter == EventReceiverType.WORKFLOW_STARTED)
        {
            return "10501";
        }
        else if (parameter == EventReceiverType.WORKFLOW_POSTPONED)
        {
            return "10502";
        }
        else if (parameter == EventReceiverType.WORKFLOW_COMPLETED)
        {
            return "10503";
        }
        else if (parameter == EventReceiverType.ENTITY_INSTANCE_ADDED)
        {
            return "10601";
        }
        else if (parameter == EventReceiverType.ENTITY_INSTANCE_UPDATED)
        {
            return "10602";
        }
        else if (parameter == EventReceiverType.ENTITY_INSTANCE_DELETED)
        {
            return "10603";
        }
        else if (parameter == EventReceiverType.APP_INSTALLED)
        {
            return "10701";
        }
        else if (parameter == EventReceiverType.APP_UPGRADED)
        {
            return "10702";
        }
        else if (parameter == EventReceiverType.APP_UNINSTALLING)
        {
            return "10703";
        }
        else if (parameter == EventReceiverType.EMAIL_RECEIVED)
        {
            return "20000";
        }
        else if (parameter == EventReceiverType.CONTEXT_EVENT)
        {
            return "32766";
        }
        else
        {
            return "-1";
        }
    }

    static UrlFieldDisplayFormat parseUrlFieldDisplayFormat(String parameter)
    {
        if (parameter.equals("0"))
        {
            return UrlFieldDisplayFormat.HYPERLINK;
        }
        else if (parameter.equals("1"))
        {
            return UrlFieldDisplayFormat.IMAGE;
        }
        else
        {
            return UrlFieldDisplayFormat.NONE;
        }
    }

    static String parseUrlFieldDisplayFormat(UrlFieldDisplayFormat parameter)
    {
        if (parameter == UrlFieldDisplayFormat.HYPERLINK)
        {
            return "0";
        }
        else if (parameter == UrlFieldDisplayFormat.IMAGE)
        {
            return "1";
        }
        else
        {
            return "-1";
        }
    }

    static String parseChangeType(ChangeType parameter)
    {
        if (parameter == ChangeType.ADD)
        {
            return "1";
        }
        else if (parameter == ChangeType.UPDATE)
        {
            return "2";
        }
        else if (parameter == ChangeType.DELETE_OBJECT)
        {
            return "3";
        }
        else if (parameter == ChangeType.RENAME)
        {
            return "4";
        }
        else if (parameter == ChangeType.MOVE_AWAY)
        {
            return "5";
        }
        else if (parameter == ChangeType.MOVE_INTO)
        {
            return "6";
        }
        else if (parameter == ChangeType.RESTORE)
        {
            return "7";
        }
        else if (parameter == ChangeType.ROLE_ADD)
        {
            return "8";
        }
        else if (parameter == ChangeType.ROLE_DELETE)
        {
            return "9";
        }
        else if (parameter == ChangeType.ROLE_UPDATE)
        {
            return "10";
        }
        else if (parameter == ChangeType.ASSIGNMENT_ADD)
        {
            return "11";
        }
        else if (parameter == ChangeType.ASSIGNMENT_DELETE)
        {
            return "12";
        }
        else if (parameter == ChangeType.MEMBER_ADD)
        {
            return "13";
        }
        else if (parameter == ChangeType.MEMBER_DELETE)
        {
            return "14";
        }
        else if (parameter == ChangeType.SYSTEM_UPDATE)
        {
            return "15";
        }
        else if (parameter == ChangeType.NAVIGATION)
        {
            return "16";
        }
        else if (parameter == ChangeType.SCOPE_ADD)
        {
            return "17";
        }
        else if (parameter == ChangeType.SCOPE_DELETE)
        {
            return "18";
        }
        else
        {
            return "0";
        }
    }

    static ChangeType parseChangeType(String parameter)
    {
        if (parameter.equals("1"))
        {
            return ChangeType.ADD;
        }
        else if (parameter.equals("2"))
        {
            return ChangeType.UPDATE;
        }
        else if (parameter.equals("3"))
        {
            return ChangeType.DELETE_OBJECT;
        }
        else if (parameter.equals("4"))
        {
            return ChangeType.RENAME;
        }
        else if (parameter.equals("5"))
        {
            return ChangeType.MOVE_AWAY;
        }
        else if (parameter.equals("6"))
        {
            return ChangeType.MOVE_INTO;
        }
        else if (parameter.equals("7"))
        {
            return ChangeType.RESTORE;
        }
        else if (parameter.equals("8"))
        {
            return ChangeType.ROLE_ADD;
        }
        else if (parameter.equals("9"))
        {
            return ChangeType.ROLE_DELETE;
        }
        else if (parameter.equals("10"))
        {
            return ChangeType.ROLE_UPDATE;
        }
        else if (parameter.equals("11"))
        {
            return ChangeType.ASSIGNMENT_ADD;
        }
        else if (parameter.equals("12"))
        {
            return ChangeType.ASSIGNMENT_DELETE;
        }
        else if (parameter.equals("13"))
        {
            return ChangeType.MEMBER_ADD;
        }
        else if (parameter.equals("14"))
        {
            return ChangeType.MEMBER_DELETE;
        }
        else if (parameter.equals("15"))
        {
            return ChangeType.SYSTEM_UPDATE;
        }
        else if (parameter.equals("16"))
        {
            return ChangeType.NAVIGATION;
        }
        else if (parameter.equals("17"))
        {
            return ChangeType.SCOPE_ADD;
        }
        else if (parameter.equals("18"))
        {
            return ChangeType.SCOPE_DELETE;
        }
        else
        {
            return ChangeType.NONE;
        }
    }

    static String parseChoiceFormatType(ChoiceFormatType parameter)
    {
        if (parameter == ChoiceFormatType.DROPDOWN)
        {
            return "0";
        }
        else if (parameter == ChoiceFormatType.RADIO_BUTTONS)
        {
            return "1";
        }
        else
        {
            return "-1";
        }
    }

    static ChoiceFormatType parseChoiceFormatType(String parameter)
    {
        if (parameter.equals("0"))
        {
            return ChoiceFormatType.DROPDOWN;
        }
        else if (parameter.equals("1"))
        {
            return ChoiceFormatType.RADIO_BUTTONS;
        }
        else
        {
            return ChoiceFormatType.NONE;
        }
    }

    static String parseSortDirection(SortDirection parameter)
    {
        if (parameter == SortDirection.ASCENDING)
        {
            return "0";
        }
        else if (parameter == SortDirection.DESCENDING)
        {
            return "1";
        }
        else
        {
            return "2";
        }
    }

    static SortDirection parseSortDirection(String parameter)
    {
        if (parameter.equals("0"))
        {
            return SortDirection.ASCENDING;
        }
        else if (parameter.equals("1"))
        {
            return SortDirection.DESCENDING;
        }
        else
        {
            return SortDirection.FQL_FORMULA;
        }
    }

    static String parseFieldUserSelectionMode(FieldUserSelectionMode parameter)
    {
        if (parameter == FieldUserSelectionMode.PEOPLE)
        {
            return "0";
        }
        else if (parameter == FieldUserSelectionMode.PEOPLE_AND_GROUPS)
        {
            return "1";
        }
        else
        {
            return "-1";
        }
    }

    static FieldUserSelectionMode parseFieldUserSelectionMode(String parameter)
    {
        if (parameter.equals("0"))
        {
            return FieldUserSelectionMode.PEOPLE;
        }
        else if (parameter.equals("1"))
        {
            return FieldUserSelectionMode.PEOPLE_AND_GROUPS;
        }
        else
        {
            return FieldUserSelectionMode.NONE;
        }
    }

    static String parseRelationshipDeleteBehaviorType(RelationshipDeleteBehaviorType parameter)
    {
        if (parameter == RelationshipDeleteBehaviorType.CASCADE)
        {
            return "1";
        }
        else if (parameter == RelationshipDeleteBehaviorType.RESTRICT)
        {
            return "2";
        }
        else
        {
            return "0";
        }
    }

    static RelationshipDeleteBehaviorType parseRelationshipDeleteBehaviorType(String parameter)
    {
        if (parameter.equals("1"))
        {
            return RelationshipDeleteBehaviorType.CASCADE;
        }
        else if (parameter.equals("2"))
        {
            return RelationshipDeleteBehaviorType.RESTRICT;
        }
        else
        {
            return RelationshipDeleteBehaviorType.NONE;
        }
    }

    static StringOperatorMode parseStringOperatorMode(String parameter)
    {
        if (parameter.equals("phrase"))
        {
            return StringOperatorMode.PHRASE;
        }
        else if (parameter.equals("and"))
        {
            return StringOperatorMode.AND;
        }
        else if (parameter.equals("or"))
        {
            return StringOperatorMode.OR;
        }
        else if (parameter.equals("any"))
        {
            return StringOperatorMode.ANY;
        }
        else if (parameter.equals("near"))
        {
            return StringOperatorMode.NEAR;
        }
        else if (parameter.equals("onear"))
        {
            return StringOperatorMode.ORDERED_NEAR;
        }
        else
        {
            return StringOperatorMode.NONE;
        }
    }

    static String parseStringOperatorMode(StringOperatorMode parameter)
    {
        if (parameter == StringOperatorMode.PHRASE)
        {
            return "phrase";
        }
        else if (parameter == StringOperatorMode.AND)
        {
            return "and";
        }
        else if (parameter == StringOperatorMode.OR)
        {
            return "or";
        }
        else if (parameter == StringOperatorMode.ANY)
        {
            return "any";
        }
        else if (parameter == StringOperatorMode.NEAR)
        {
            return "near";
        }
        else if (parameter == StringOperatorMode.ORDERED_NEAR)
        {
            return "onear";
        }
        else
        {
            return "none";
        }
    }

    static String parseDateTimeFieldFriendlyFormat(DateTimeFieldFriendlyFormat parameter)
    {
        if (parameter == DateTimeFieldFriendlyFormat.DISABLED)
        {
            return "1";
        }
        else if (parameter == DateTimeFieldFriendlyFormat.RELATIVE)
        {
            return "2";
        }
        else
        {
            return "0";
        }
    }

    static DateTimeFieldFriendlyFormat parseDateTimeFieldFriendlyFormat(String parameter)
    {
        if (parameter.equals("1"))
        {
            return DateTimeFieldFriendlyFormat.DISABLED;
        }
        else if (parameter.equals("2"))
        {
            return DateTimeFieldFriendlyFormat.RELATIVE;
        }
        else
        {
            return DateTimeFieldFriendlyFormat.NONE;
        }
    }

    static String parseQueryLogClientType(QueryLogClientType parameter)
    {
        if (parameter == QueryLogClientType.OBJECT_MODEL)
        {
            return "ObjectModel";
        }
        else if (parameter == QueryLogClientType.WEB_SERVICE)
        {
            return "WebService";
        }
        else if (parameter == QueryLogClientType.UI)
        {
            return "UI";
        }
        else if (parameter == QueryLogClientType.CUSTOM)
        {
            return "Custom";
        }
        else if (parameter == QueryLogClientType.CSOM)
        {
            return "CSOM";
        }
        else if (parameter == QueryLogClientType.RSS)
        {
            return "RSS";
        }
        else if (parameter == QueryLogClientType.OBJECT_MODEL_BACKWARDS_COMPATIBLE)
        {
            return "ObjectModelBackwardsCompatible";
        }
        else if (parameter == QueryLogClientType.ALL_RESULTS_QUERY)
        {
            return "AllResultsQuery";
        }
        else if (parameter == QueryLogClientType.PEOPLE_RESULTS_QUERY)
        {
            return "PeopleResultsQuery";
        }
        else if (parameter == QueryLogClientType.VIDEO_RESULTS_QUERY)
        {
            return "VideoResultsQuery";
        }
        else if (parameter == QueryLogClientType.SITE_RESULTS_QUERY_ALL)
        {
            return "SiteResultsQuery_All";
        }
        else if (parameter == QueryLogClientType.SITE_RESULTS_QUERY_DOCS)
        {
            return "SiteResultsQuery_Docs";
        }
        else if (parameter == QueryLogClientType.SITE_RESULTS_QUERY_SITES)
        {
            return "SiteResultsQuery_Sites";
        }
        else if (parameter == QueryLogClientType.CONTENT_SEARCH_HIGH)
        {
            return "ContentSearchHigh";
        }
        else if (parameter == QueryLogClientType.CONTENT_SEARCH_REGULAR)
        {
            return "ContentSearchRegular";
        }
        else if (parameter == QueryLogClientType.CATALOG_ITEM_REUSE_QUERY)
        {
            return "CatalogItemReuseQuery";
        }
        else if (parameter == QueryLogClientType.CONTENT_SEARCH_LOW)
        {
            return "ContentSearchLow";
        }
        else if (parameter == QueryLogClientType.SEARCH_WEB_PART_CONFIGURATION)
        {
            return "SearchWebPartConfiguration";
        }
        else if (parameter == QueryLogClientType.DISCOVERY_SEARCH)
        {
            return "DiscoverySearch";
        }
        else if (parameter == QueryLogClientType.DISCOVERY_DOWNLOAD_MANAGER)
        {
            return "DiscoveryDownloadManager";
        }
        else if (parameter == QueryLogClientType.DOCS_SHARED_WITH_ME)
        {
            return "DocsSharedWithMe";
        }
        else if (parameter == QueryLogClientType.MY_TASK_SYNC)
        {
            return "MyTaskSync";
        }
        else if (parameter == QueryLogClientType.SEO_SITE_MAP_QUERY)
        {
            return "SEOSiteMapQuery";
        }
        else if (parameter == QueryLogClientType.MY_SITE_SECURITY_TRIMMER)
        {
            return "MySiteSecurityTrimmer";
        }
        else if (parameter == QueryLogClientType.REPORTS_AND_DATA_RESULTS_QUERY)
        {
            return "ReportsAndDataResultsQuery";
        }
        else if (parameter == QueryLogClientType.INPLACE_LIST_SEARCH)
        {
            return "InplaceListSearch";
        }
        else if (parameter == QueryLogClientType.TRENDING_TAGS_QUERY)
        {
            return "TrendingTagsQuery";
        }
        else
        {
            return "Unknown";
        }
    }

    static QueryLogClientType parseQueryLogClientType(String parameter)
    {
        if (parameter.equals("ObjectModel"))
        {
            return QueryLogClientType.OBJECT_MODEL;
        }
        else if (parameter.equals("WebService"))
        {
            return QueryLogClientType.WEB_SERVICE;
        }
        else if (parameter.equals("UI"))
        {
            return QueryLogClientType.UI;
        }
        else if (parameter.equals("Custom"))
        {
            return QueryLogClientType.CUSTOM;
        }
        else if (parameter.equals("CSOM"))
        {
            return QueryLogClientType.CSOM;
        }
        else if (parameter.equals("RSS"))
        {
            return QueryLogClientType.RSS;
        }
        else if (parameter.equals("ObjectModelBackwardsCompatible"))
        {
            return QueryLogClientType.OBJECT_MODEL_BACKWARDS_COMPATIBLE;
        }
        else if (parameter.equals("AllResultsQuery"))
        {
            return QueryLogClientType.ALL_RESULTS_QUERY;
        }
        else if (parameter.equals("PeopleResultsQuery"))
        {
            return QueryLogClientType.PEOPLE_RESULTS_QUERY;
        }
        else if (parameter.equals("VideoResultsQuery"))
        {
            return QueryLogClientType.VIDEO_RESULTS_QUERY;
        }
        else if (parameter.equals("SiteResultsQuery_All"))
        {
            return QueryLogClientType.SITE_RESULTS_QUERY_ALL;
        }
        else if (parameter.equals("SiteResultsQuery_Docs"))
        {
            return QueryLogClientType.SITE_RESULTS_QUERY_DOCS;
        }
        else if (parameter.equals("SiteResultsQuery_Sites"))
        {
            return QueryLogClientType.SITE_RESULTS_QUERY_SITES;
        }
        else if (parameter.equals("ContentSearchHigh"))
        {
            return QueryLogClientType.CONTENT_SEARCH_HIGH;
        }
        else if (parameter.equals("ContentSearchRegular"))
        {
            return QueryLogClientType.CONTENT_SEARCH_REGULAR;
        }
        else if (parameter.equals("CatalogItemReuseQuery"))
        {
            return QueryLogClientType.CATALOG_ITEM_REUSE_QUERY;
        }
        else if (parameter.equals("ContentSearchLow"))
        {
            return QueryLogClientType.CONTENT_SEARCH_LOW;
        }
        else if (parameter.equals("SearchWebPartConfiguration"))
        {
            return QueryLogClientType.SEARCH_WEB_PART_CONFIGURATION;
        }
        else if (parameter.equals("DiscoverySearch"))
        {
            return QueryLogClientType.DISCOVERY_SEARCH;
        }
        else if (parameter.equals("DiscoveryDownloadManager"))
        {
            return  QueryLogClientType.DISCOVERY_DOWNLOAD_MANAGER;
        }
        else if (parameter.equals("DocsSharedWithMe"))
        {
            return QueryLogClientType.DOCS_SHARED_WITH_ME;
        }
        else if (parameter.equals("MyTaskSync"))
        {
            return QueryLogClientType.MY_TASK_SYNC;
        }
        else if (parameter.equals("SEOSiteMapQuery"))
        {
            return QueryLogClientType.SEO_SITE_MAP_QUERY;
        }
        else if (parameter.equals("MySiteSecurityTrimmer"))
        {
            return QueryLogClientType.MY_SITE_SECURITY_TRIMMER;
        }
        else if (parameter.equals("ReportsAndDataResultsQuery"))
        {
            return QueryLogClientType.REPORTS_AND_DATA_RESULTS_QUERY;
        }
        else if (parameter.equals("InplaceListSearch"))
        {
            return QueryLogClientType.INPLACE_LIST_SEARCH;
        }
        else if (parameter.equals("TrendingTagsQuery"))
        {
            return QueryLogClientType.TRENDING_TAGS_QUERY;
        }
        else
        {
            return QueryLogClientType.NONE;
        }
    }

    static String parseDateTimeFieldFormatType(DateTimeFieldFormatType parameter)
    {
        if (parameter == DateTimeFieldFormatType.DATE)
        {
            return "0";
        }
        else if (parameter == DateTimeFieldFormatType.DATE_TIME)
        {
            return "1";
        }
        else
        {
            return "none";
        }
    }

    static DateTimeFieldFormatType parseDateTimeFieldFormatType(String parameter)
    {
        if (parameter.equals("0"))
        {
            return DateTimeFieldFormatType.DATE;
        }
        else if (parameter.equals("1"))
        {
            return DateTimeFieldFormatType.DATE_TIME;
        }
        else
        {
            return DateTimeFieldFormatType.NONE;
        }
    }

    static String parseRoleType(RoleType parameter)
    {
        if (parameter == RoleType.GUEST)
        {
            return "1";
        }
        else if (parameter == RoleType.READER)
        {
            return "2";
        }
        else if (parameter == RoleType.CONTRIBUTOR)
        {
            return "3";
        }
        else if (parameter == RoleType.WEB_DESIGNER)
        {
            return "4";
        }
        else if (parameter == RoleType.ADMINISTRATOR)
        {
            return "5";
        }
        else
        {
            return "0";
        }
    }

    static RoleType parseRoleType(String parameter)
    {
        if (parameter.equals("1"))
        {
            return RoleType.GUEST;
        }
        else if (parameter.equals("2"))
        {
            return RoleType.READER;
        }
        else if (parameter.equals("3"))
        {
            return RoleType.CONTRIBUTOR;
        }
        else if (parameter.equals("4"))
        {
            return RoleType.WEB_DESIGNER;
        }
        else if (parameter.equals("5"))
        {
            return RoleType.ADMINISTRATOR;
        }
        else
        {
            return RoleType.NONE;
        }
    }

    static String parseRecycleBinItemType(RecycleBinItemType parameter)
    {
        if (parameter == RecycleBinItemType.FILE)
        {
            return "1";
        }
        else if (parameter == RecycleBinItemType.FILE_VERSION)
        {
            return "2";
        }
        else if (parameter == RecycleBinItemType.LIST_ITEM)
        {
            return "3";
        }
        else if (parameter == RecycleBinItemType.LIST)
        {
            return "4";
        }
        else if (parameter == RecycleBinItemType.FOLDER)
        {
            return "5";
        }
        else if (parameter == RecycleBinItemType.FOLDER_WITH_LISTS)
        {
            return "6";
        }
        else if (parameter == RecycleBinItemType.ATTACHMENT)
        {
            return "7";
        }
        else if (parameter == RecycleBinItemType.LIST_ITEM_VERSION)
        {
            return "8";
        }
        else if (parameter == RecycleBinItemType.CASCADE_PARENT)
        {
            return "9";
        }
        else
        {
            return "0";
        }
    }

    static RecycleBinItemType parseRecycleBinItemType(String parameter)
    {
        if (parameter.equals("1"))
        {
            return RecycleBinItemType.FILE;
        }
        else if (parameter.equals("2"))
        {
            return RecycleBinItemType.FILE_VERSION;
        }
        else if (parameter.equals("3"))
        {
            return RecycleBinItemType.LIST_ITEM;
        }
        else if (parameter.equals("4"))
        {
            return RecycleBinItemType.LIST;
        }
        else if (parameter.equals("5"))
        {
            return RecycleBinItemType.FOLDER;
        }
        else if (parameter.equals("6"))
        {
            return RecycleBinItemType.FOLDER_WITH_LISTS;
        }
        else if (parameter.equals("7"))
        {
            return RecycleBinItemType.ATTACHMENT;
        }
        else if (parameter.equals("8"))
        {
            return RecycleBinItemType.LIST_ITEM_VERSION;
        }
        else if (parameter.equals("9"))
        {
            return RecycleBinItemType.CASCADE_PARENT;
        }
        else
        {
            return RecycleBinItemType.NONE;
        }
    }

    static String parseRecycleBinItemState(RecycleBinItemState parameter)
    {
        if (parameter == RecycleBinItemState.FIRST_STAGE_RECYCLE_BIN)
        {
            return "1";
        }
        else if (parameter == RecycleBinItemState.SECOND_STAGE_RECYCLE_BIN)
        {
            return "2";
        }
        else
        {
            return "0";
        }
    }

    static RecycleBinItemState parseRecycleBinItemState(String parameter)
    {
        if (parameter.equals("1"))
        {
            return RecycleBinItemState.FIRST_STAGE_RECYCLE_BIN;
        }
        else if (parameter.equals("2"))
        {
            return RecycleBinItemState.SECOND_STAGE_RECYCLE_BIN;
        }
        else
        {
            return RecycleBinItemState.NONE;
        }
    }

    static String parseReorderingRuleMatchType(ReorderingRuleMatchType parameter)
    {
        if (parameter == ReorderingRuleMatchType.RESULT_CONTAINS_KEYWORD)
        {
            return "0";
        }
        else if (parameter == ReorderingRuleMatchType.TITLE_CONTAINS_KEYWORD)
        {
            return "1";
        }
        else if (parameter == ReorderingRuleMatchType.TITLE_MATCHES_KEYWORD)
        {
            return "2";
        }
        else if (parameter == ReorderingRuleMatchType.URL_STARTS_WITH)
        {
            return "3";
        }
        else if (parameter == ReorderingRuleMatchType.URL_EXACTLY_MATCHES)
        {
            return "4";
        }
        else if (parameter == ReorderingRuleMatchType.CONTENT_TYPE_IS)
        {
            return "5";
        }
        else if (parameter == ReorderingRuleMatchType.FILE_EXTENSION_MATCHES)
        {
            return "6";
        }
        else if (parameter == ReorderingRuleMatchType.RESULT_HAS_TAG)
        {
            return "7";
        }
        else
        {
            return "8";
        }
    }

    static ReorderingRuleMatchType parseReorderingRuleMatchType(String parameter)
    {
        if (parameter.equals("0"))
        {
            return ReorderingRuleMatchType.RESULT_CONTAINS_KEYWORD;
        }
        else if (parameter.equals("1"))
        {
            return ReorderingRuleMatchType.TITLE_CONTAINS_KEYWORD;
        }
        else if (parameter.equals("2"))
        {
            return ReorderingRuleMatchType.TITLE_MATCHES_KEYWORD;
        }
        else if (parameter.equals("3"))
        {
            return ReorderingRuleMatchType.URL_STARTS_WITH;
        }
        else if (parameter.equals("4"))
        {
            return ReorderingRuleMatchType.URL_EXACTLY_MATCHES;
        }
        else if (parameter.equals("5"))
        {
            return ReorderingRuleMatchType.CONTENT_TYPE_IS;
        }
        else if (parameter.equals("6"))
        {
            return ReorderingRuleMatchType.FILE_EXTENSION_MATCHES;
        }
        else if (parameter.equals("7"))
        {
            return ReorderingRuleMatchType.RESULT_HAS_TAG;
        }
        else
        {
            return ReorderingRuleMatchType.MANUAL_CONDITION;
        }
    }

    static String parseJoinType(JoinType parameter)
    {
        if (parameter == JoinType.INNER)
        {
            return "INNER";
        }
        else if (parameter == JoinType.LEFT_OUTER)
        {
            return "LEFT OUTER";
        }
        else if (parameter == JoinType.RIGHT_OUTER)
        {
            return "RIGHT OUTER";
        }
        else
        {
            return "none";
        }
    }

    static JoinType parseJoinType(String parameter)
    {
        if (parameter.equals("INNER"))
        {
            return JoinType.INNER;
        }
        else if (parameter.equals("LEFT OUTER"))
        {
            return JoinType.LEFT_OUTER;
        }
        else if (parameter.equals("RIGHT OUTER"))
        {
            return JoinType.RIGHT_OUTER;
        }
        else
        {
            return JoinType.NONE;
        }
    }

    static String parseImeMode(ImeMode parameter)
    {
        if (parameter == ImeMode.INACTIVE)
        {
            return "inactive";
        }
        else
        {
            return "none";
        }
    }

    static ImeMode parseImeMode(String parameter)
    {
        if (parameter.equals("inactive"))
        {
            return ImeMode.INACTIVE;
        }
        else
        {
            return ImeMode.NONE;
        }
    }

    static String parseFieldAggregation(FieldAggregation parameter)
    {
        if (parameter == FieldAggregation.AVERAGE)
        {
            return "average";
        }
        else if (parameter == FieldAggregation.COUNT)
        {
            return "count";
        }
        else if (parameter == FieldAggregation.FIRST)
        {
            return "first";
        }
        else if (parameter == FieldAggregation.LAST)
        {
            return "last";
        }
        else if (parameter == FieldAggregation.MAX)
        {
            return "max";
        }
        else if (parameter == FieldAggregation.MERGE)
        {
            return "merge";
        }
        else if (parameter == FieldAggregation.MIN)
        {
            return "min";
        }
        else if (parameter == FieldAggregation.PLAIN_TEXT)
        {
            return "plaintext";
        }
        else if (parameter == FieldAggregation.SUM)
        {
            return "sum";
        }
        else
        {
            return "none";
        }
    }

    static FieldAggregation parseFieldAggregation(String parameter)
    {
        if (parameter.equals("average"))
        {
            return FieldAggregation.AVERAGE;
        }
        else if (parameter.equals("count"))
        {
            return FieldAggregation.COUNT;
        }
        else if (parameter.equals("first"))
        {
            return FieldAggregation.FIRST;
        }
        else if (parameter.equals("last"))
        {
            return FieldAggregation.LAST;
        }
        else if (parameter.equals("max"))
        {
            return FieldAggregation.MAX;
        }
        else if (parameter.equals("merge"))
        {
            return FieldAggregation.MERGE;
        }
        else if (parameter.equals("min"))
        {
            return FieldAggregation.MIN;
        }
        else if (parameter.equals("plaintext"))
        {
            return FieldAggregation.PLAIN_TEXT;
        }
        else if (parameter.equals("sum"))
        {
            return FieldAggregation.SUM;
        }
        else
        {
            return FieldAggregation.NONE;
        }
    }

    static CalendarType parseCalendarType(String parameter)
    {
        if (parameter.equals("1"))
        {
            return CalendarType.GREGORIAN;
        }
        else if (parameter.equals("3"))
        {
            return CalendarType.JAPANESE_EMPEROR_ERA;
        }
        else if (parameter.equals("4"))
        {
            return CalendarType.TAIWAN_CALENDAR;
        }
        else if (parameter.equals("5"))
        {
            return CalendarType.KOREAN_TANGUN_ERA;
        }
        else if (parameter.equals("6"))
        {
            return CalendarType.HIJRI;
        }
        else if (parameter.equals("7"))
        {
            return CalendarType.THAI;
        }
        else if (parameter.equals("8"))
        {
            return CalendarType.HEBREW;
        }
        else if (parameter.equals("9"))
        {
            return CalendarType.MIDDLE_EAST_FRENCH;
        }
        else if (parameter.equals("10"))
        {
            return CalendarType.ARABIC;
        }
        else if (parameter.equals("11"))
        {
            return CalendarType.TRANSLITERATED_ENGLISH;
        }
        else if (parameter.equals("12"))
        {
            return CalendarType.TRANSLITERATED_FRENCH;
        }
        else if (parameter.equals("14"))
        {
            return CalendarType.KOREAN_AND_JAPANESE;
        }
        else if (parameter.equals("15"))
        {
            return CalendarType.CHINESE;
        }
        else if (parameter.equals("16"))
        {
            return CalendarType.SAKA_ERA;
        }
        else
        {
            return CalendarType.NONE;
        }
    }

    static String parseCalendarType(CalendarType parameter)
    {
        if (parameter == CalendarType.GREGORIAN)
        {
            return "1";
        }
        else if (parameter == CalendarType.JAPANESE_EMPEROR_ERA)
        {
            return "3";
        }
        else if (parameter == CalendarType.TAIWAN_CALENDAR)
        {
            return "4";
        }
        else if (parameter == CalendarType.KOREAN_TANGUN_ERA)
        {
            return "5";
        }
        else if (parameter == CalendarType.HIJRI)
        {
            return "6";
        }
        else if (parameter == CalendarType.THAI)
        {
            return "7";
        }
        else if (parameter == CalendarType.HEBREW)
        {
            return "8";
        }
        else if (parameter == CalendarType.MIDDLE_EAST_FRENCH)
        {
            return "9";
        }
        else if (parameter == CalendarType.ARABIC)
        {
            return "10";
        }
        else if (parameter == CalendarType.TRANSLITERATED_ENGLISH)
        {
            return "11";
        }
        else if (parameter == CalendarType.TRANSLITERATED_FRENCH)
        {
            return "12";
        }
        else if (parameter == CalendarType.KOREAN_AND_JAPANESE)
        {
            return "14";
        }
        else if (parameter == CalendarType.CHINESE)
        {
            return "15";
        }
        else if (parameter == CalendarType.SAKA_ERA)
        {
            return "16";
        }
        else
        {
            return "0";
        }
    }

    static String parseFieldType(FieldType parameter)
    {
        if (parameter == FieldType.INTEGER)
        {
            return "1";
        }
        else if (parameter == FieldType.TEXT)
        {
            return "2";
        }
        else if (parameter == FieldType.NOTE)
        {
            return "3";
        }
        else if (parameter == FieldType.DATE_TIME)
        {
            return "4";
        }
        else if (parameter == FieldType.COUNTER)
        {
            return "5";
        }
        else if (parameter == FieldType.CHOICE)
        {
            return "6";
        }
        else if (parameter == FieldType.LOOKUP)
        {
            return "7";
        }
        else if (parameter == FieldType.BOOLEAN)
        {
            return "8";
        }
        else if (parameter == FieldType.NUMBER)
        {
            return "9";
        }
        else if (parameter == FieldType.CURRENCY)
        {
            return "10";
        }
        else if (parameter == FieldType.URL)
        {
            return "11";
        }
        else if (parameter == FieldType.COMPUTED)
        {
            return "12";
        }
        else if (parameter == FieldType.THREADING)
        {
            return "13";
        }
        else if (parameter == FieldType.GUID)
        {
            return "14";
        }
        else if (parameter == FieldType.MULTI_CHOICE)
        {
            return "15";
        }
        else if (parameter == FieldType.GRID_CHOICE)
        {
            return "16";
        }
        else if (parameter == FieldType.CALCULATED)
        {
            return "17";
        }
        else if (parameter == FieldType.FILE)
        {
            return "18";
        }
        else if (parameter == FieldType.ATTACHMENTS)
        {
            return "19";
        }
        else if (parameter == FieldType.USER)
        {
            return "20";
        }
        else if (parameter == FieldType.RECURRENCE)
        {
            return "21";
        }
        else if (parameter == FieldType.CROSS_PROJECT_LINK)
        {
            return "22";
        }
        else if (parameter == FieldType.MOD_STAT)
        {
            return "23";
        }
        else if (parameter == FieldType.ERROR)
        {
            return "24";
        }
        else if (parameter == FieldType.CONTENT_TYPE_ID)
        {
            return "25";
        }
        else if (parameter == FieldType.PAGE_SEPARATOR)
        {
            return "26";
        }
        else if (parameter == FieldType.THREAD_INDEX)
        {
            return "27";
        }
        else if (parameter == FieldType.WORKFLOW_STATUS)
        {
            return "28";
        }
        else if (parameter == FieldType.ALL_DAY_EVENT)
        {
            return "29";
        }
        else if (parameter == FieldType.WORKFLOW_EVENT_TYPE)
        {
            return "30";
        }
        else
        {
            return "0";
        }
    }

    static FieldType parseFieldType(String parameter)
    {
        if (parameter.equals("1"))
        {
            return FieldType.INTEGER;
        }
        else if (parameter.equals("2"))
        {
            return FieldType.TEXT;
        }
        else if (parameter.equals("3"))
        {
            return FieldType.NOTE;
        }
        else if (parameter.equals("4"))
        {
            return FieldType.DATE_TIME;
        }
        else if (parameter.equals("5"))
        {
            return FieldType.COUNTER;
        }
        else if (parameter.equals("6"))
        {
            return FieldType.CHOICE;
        }
        else if (parameter.equals("7"))
        {
            return FieldType.LOOKUP;
        }
        else if (parameter.equals("8"))
        {
            return FieldType.BOOLEAN;
        }
        else if (parameter.equals("9"))
        {
            return FieldType.NUMBER;
        }
        else if (parameter.equals("10"))
        {
            return FieldType.CURRENCY;
        }
        else if (parameter.equals("11"))
        {
            return FieldType.URL;
        }
        else if (parameter.equals("12"))
        {
            return FieldType.COMPUTED;
        }
        else if (parameter.equals("13"))
        {
            return FieldType.THREADING;
        }
        else if (parameter.equals("14"))
        {
            return FieldType.GUID;
        }
        else if (parameter.equals("15"))
        {
            return FieldType.MULTI_CHOICE;
        }
        else if (parameter.equals("16"))
        {
            return FieldType.GRID_CHOICE;
        }
        else if (parameter.equals("17"))
        {
            return FieldType.CALCULATED;
        }
        else if (parameter.equals("18"))
        {
            return FieldType.FILE;
        }
        else if (parameter.equals("19"))
        {
            return FieldType.ATTACHMENTS;
        }
        else if (parameter.equals("20"))
        {
            return FieldType.USER;
        }
        else if (parameter.equals("21"))
        {
            return FieldType.RECURRENCE;
        }
        else if (parameter.equals("22"))
        {
            return FieldType.CROSS_PROJECT_LINK;
        }
        else if (parameter.equals("23"))
        {
            return FieldType.MOD_STAT;
        }
        else if (parameter.equals("24"))
        {
            return FieldType.ERROR;
        }
        else if (parameter.equals("25"))
        {
            return FieldType.CONTENT_TYPE_ID;
        }
        else if (parameter.equals("26"))
        {
            return FieldType.PAGE_SEPARATOR;
        }
        else if (parameter.equals("27"))
        {
            return FieldType.THREAD_INDEX;
        }
        else if (parameter.equals("28"))
        {
            return FieldType.WORKFLOW_STATUS;
        }
        else if (parameter.equals("29"))
        {
            return FieldType.ALL_DAY_EVENT;
        }
        else if (parameter.equals("30"))
        {
            return FieldType.WORKFLOW_EVENT_TYPE;
        }
        else
        {
            return FieldType.NONE;
        }
    }


    static String parseListTemplateType(ListTemplateType parameter)
    {
        if (parameter == ListTemplateType.GENERIC_LIST)
        {
            return "100";
        }
        else if (parameter == ListTemplateType.DOCUMENT_LIBRARY)
        {
            return "101";
        }
        else if (parameter == ListTemplateType.SURVEY)
        {
            return "102";
        }
        else if (parameter == ListTemplateType.LINKS)
        {
            return "103";
        }
        else if (parameter == ListTemplateType.ANNOUNCEMENTS)
        {
            return "104";
        }
        else if (parameter == ListTemplateType.CONTACTS)
        {
            return "105";
        }
        else if (parameter == ListTemplateType.EVENTS)
        {
            return "106";
        }
        else if (parameter == ListTemplateType.TASKS)
        {
            return "107";
        }
        else if (parameter == ListTemplateType.DISCUSSION_BOARD)
        {
            return "108";
        }
        else if (parameter == ListTemplateType.PICTURE_LIBRARY)
        {
            return "109";
        }
        else if (parameter == ListTemplateType.DATA_SOURCES)
        {
            return "110";
        }
        else if (parameter == ListTemplateType.WEB_TEMPLATE_CATALOG)
        {
            return "111";
        }
        else if (parameter == ListTemplateType.USER_INFORMATION)
        {
            return "112";
        }
        else if (parameter == ListTemplateType.WEB_PART_CATALOG)
        {
            return "113";
        }
        else if (parameter == ListTemplateType.LIST_TEMPLATE_CATALOG)
        {
            return "114";
        }
        else if (parameter == ListTemplateType.XML_FORM)
        {
            return "115";
        }
        else if (parameter == ListTemplateType.MASTER_PAGE_CATALOG)
        {
            return "116";
        }
        else if (parameter == ListTemplateType.NO_CODE_WORKFLOWS)
        {
            return "117";
        }
        else if (parameter == ListTemplateType.WORKFLOW_PROCESS)
        {
            return "118";
        }
        else if (parameter == ListTemplateType.WEB_PAGE_LIBRARY)
        {
            return "119";
        }
        else if (parameter == ListTemplateType.CUSTOM_GRID)
        {
            return "120";
        }
        else if (parameter == ListTemplateType.SOLUTION_CATALOG)
        {
            return "121";
        }
        else if (parameter == ListTemplateType.NO_CODE_PUBLIC)
        {
            return "122";
        }
        else if (parameter == ListTemplateType.THEME_CATALOG)
        {
            return "123";
        }
        else if (parameter == ListTemplateType.DESIGN_CATALOG)
        {
            return "124";
        }
        else if (parameter == ListTemplateType.APP_DATA_CATALOG)
        {
            return "125";
        }
        else if (parameter == ListTemplateType.DATA_CONNECTION_LIBRARY)
        {
            return "130";
        }
        else if (parameter == ListTemplateType.WORKFLOW_HISTORY)
        {
            return "140";
        }
        else if (parameter == ListTemplateType.GANTT_TASKS)
        {
            return "150";
        }
        else if (parameter == ListTemplateType.HELP_LIBRARY)
        {
            return "151";
        }
        else if (parameter == ListTemplateType.ACCESS_REQUEST)
        {
            return "160";
        }
        else if (parameter == ListTemplateType.TASKS_WITH_TIMELINE_AND_HIERARCHY)
        {
            return "171";
        }
        else if (parameter == ListTemplateType.MAINTENANCE_LOGS)
        {
            return "175";
        }
        else if (parameter == ListTemplateType.MEETINGS)
        {
            return "200";
        }
        else if (parameter == ListTemplateType.AGENDA)
        {
            return "201";
        }
        else if (parameter == ListTemplateType.MEETING_USER)
        {
            return "202";
        }
        else if (parameter == ListTemplateType.DECISION)
        {
            return "204";
        }
        else if (parameter == ListTemplateType.MEETING_OBJECTIVE)
        {
            return "207";
        }
        else if (parameter == ListTemplateType.TEXT_BOX)
        {
            return "210";
        }
        else if (parameter == ListTemplateType.THINGS_TO_BRING)
        {
            return "211";
        }
        else if (parameter == ListTemplateType.HOME_PAGE_LIBRARY)
        {
            return "212";
        }
        else if (parameter == ListTemplateType.POSTS)
        {
            return "301";
        }
        else if (parameter == ListTemplateType.COMMENTS)
        {
            return "302";
        }
        else if (parameter == ListTemplateType.CATEGORIES)
        {
            return "303";
        }
        else if (parameter == ListTemplateType.FACILITY)
        {
            return "402";
        }
        else if (parameter == ListTemplateType.WHEREABOUTS)
        {
            return "403";
        }
        else if (parameter == ListTemplateType.CALL_TRACK)
        {
            return "404";
        }
        else if (parameter == ListTemplateType.CIRCULATION)
        {
            return "405";
        }
        else if (parameter == ListTemplateType.TIMECARD)
        {
            return "420";
        }
        else if (parameter == ListTemplateType.HOLIDAYS)
        {
            return "421";
        }
        else if (parameter == ListTemplateType.IME_DICTIONARY)
        {
            return "499";
        }
        else if (parameter == ListTemplateType.EXTERNAL_LIST)
        {
            return "600";
        }
        else if (parameter == ListTemplateType.MY_SITE_DOCUMENT_LIBRARY)
        {
            return "700";
        }
        else if (parameter == ListTemplateType.ISSUE_TRACKING)
        {
            return "1100";
        }
        else if (parameter == ListTemplateType.ADMIN_TASKS)
        {
            return "1200";
        }
        else if (parameter == ListTemplateType.HEALTH_RULES)
        {
            return "1220";
        }
        else if (parameter == ListTemplateType.HEALTH_REPORTS)
        {
            return "1221";
        }
        else if (parameter == ListTemplateType.DEVELOPER_SITE_DRAFT_APPS)
        {
            return "1230";
        }
        else
        {
            return "0";
        }
    }

    static ListTemplateType parseListTemplateType(String parameter)
    {
        if (parameter.equals("100"))
        {
            return ListTemplateType.GENERIC_LIST;
        }
        else if (parameter.equals("101"))
        {
            return ListTemplateType.DOCUMENT_LIBRARY;
        }
        else if (parameter.equals("102"))
        {
            return ListTemplateType.SURVEY;
        }
        else if (parameter.equals("103"))
        {
            return ListTemplateType.LINKS;
        }
        else if (parameter.equals("104"))
        {
            return ListTemplateType.ANNOUNCEMENTS;
        }
        else if (parameter.equals("105"))
        {
            return ListTemplateType.CONTACTS;
        }
        else if (parameter.equals("106"))
        {
            return ListTemplateType.EVENTS;
        }
        else if (parameter.equals("107"))
        {
            return ListTemplateType.TASKS;
        }
        else if (parameter.equals("108"))
        {
            return ListTemplateType.DISCUSSION_BOARD;
        }
        else if (parameter.equals("109"))
        {
            return ListTemplateType.PICTURE_LIBRARY;
        }
        else if (parameter.equals("110"))
        {
            return ListTemplateType.DATA_SOURCES;
        }
        else if (parameter.equals("111"))
        {
            return ListTemplateType.WEB_TEMPLATE_CATALOG;
        }
        else if (parameter.equals("112"))
        {
            return ListTemplateType.USER_INFORMATION;
        }
        else if (parameter.equals("113"))
        {
            return ListTemplateType.WEB_PART_CATALOG;
        }
        else if (parameter.equals("114"))
        {
            return ListTemplateType.LIST_TEMPLATE_CATALOG;
        }
        else if (parameter.equals("115"))
        {
            return ListTemplateType.XML_FORM;
        }
        else if (parameter.equals("116"))
        {
            return ListTemplateType.MASTER_PAGE_CATALOG;
        }
        else if (parameter.equals("117"))
        {
            return ListTemplateType.NO_CODE_WORKFLOWS;
        }
        else if (parameter.equals("118"))
        {
            return ListTemplateType.WORKFLOW_PROCESS;
        }
        else if (parameter.equals("119"))
        {
            return ListTemplateType.WEB_PAGE_LIBRARY;
        }
        else if (parameter.equals("120"))
        {
            return ListTemplateType.CUSTOM_GRID;
        }
        else if (parameter.equals("121"))
        {
            return ListTemplateType.SOLUTION_CATALOG;
        }
        else if (parameter.equals("122"))
        {
            return ListTemplateType.NO_CODE_PUBLIC;
        }
        else if (parameter.equals("123"))
        {
            return ListTemplateType.THEME_CATALOG;
        }
        else if (parameter.equals("124"))
        {
            return ListTemplateType.DESIGN_CATALOG;
        }
        else if (parameter.equals("125"))
        {
            return ListTemplateType.APP_DATA_CATALOG;
        }
        else if (parameter.equals("130"))
        {
            return ListTemplateType.DATA_CONNECTION_LIBRARY;
        }
        else if (parameter.equals("140"))
        {
            return ListTemplateType.WORKFLOW_HISTORY;
        }
        else if (parameter.equals("150"))
        {
            return ListTemplateType.GANTT_TASKS;
        }
        else if (parameter.equals("151"))
        {
            return ListTemplateType.HELP_LIBRARY;
        }
        else if (parameter.equals("160"))
        {
            return ListTemplateType.ACCESS_REQUEST;
        }
        else if (parameter.equals("171"))
        {
            return ListTemplateType.TASKS_WITH_TIMELINE_AND_HIERARCHY;
        }
        else if (parameter.equals("175"))
        {
            return ListTemplateType.MAINTENANCE_LOGS;
        }
        else if (parameter.equals("200"))
        {
            return ListTemplateType.MEETINGS;
        }
        else if (parameter.equals("201"))
        {
            return ListTemplateType.AGENDA;
        }
        else if (parameter.equals("202"))
        {
            return ListTemplateType.MEETING_USER;
        }
        else if (parameter.equals("204"))
        {
            return ListTemplateType.DECISION;
        }
        else if (parameter.equals("207"))
        {
            return ListTemplateType.MEETING_OBJECTIVE;
        }
        else if (parameter.equals("210"))
        {
            return ListTemplateType.TEXT_BOX;
        }
        else if (parameter.equals("211"))
        {
            return ListTemplateType.THINGS_TO_BRING;
        }
        else if (parameter.equals("212"))
        {
            return ListTemplateType.HOME_PAGE_LIBRARY;
        }
        else if (parameter.equals("301"))
        {
            return ListTemplateType.POSTS;
        }
        else if (parameter.equals("302"))
        {
            return ListTemplateType.COMMENTS;
        }
        else if (parameter.equals("303"))
        {
            return ListTemplateType.CATEGORIES;
        }
        else if (parameter.equals("402"))
        {
            return ListTemplateType.FACILITY;
        }
        else if (parameter.equals("403"))
        {
            return  ListTemplateType.WHEREABOUTS;
        }
        else if (parameter.equals("404"))
        {
            return ListTemplateType.CALL_TRACK;
        }
        else if (parameter.equals("405"))
        {
            return ListTemplateType.CIRCULATION;
        }
        else if (parameter.equals("420"))
        {
            return ListTemplateType.TIMECARD;
        }
        else if (parameter.equals("421"))
        {
            return ListTemplateType.HOLIDAYS;
        }
        else if (parameter.equals("499"))
        {
            return ListTemplateType.IME_DICTIONARY;
        }
        else if (parameter.equals("600"))
        {
            return ListTemplateType.EXTERNAL_LIST;
        }
        else if (parameter.equals("700"))
        {
            return ListTemplateType.MY_SITE_DOCUMENT_LIBRARY;
        }
        else if (parameter.equals("1100"))
        {
            return ListTemplateType.ISSUE_TRACKING;
        }
        else if (parameter.equals("1200"))
        {
            return ListTemplateType.ADMIN_TASKS;
        }
        else if (parameter.equals("1220"))
        {
            return ListTemplateType.HEALTH_RULES;
        }
        else if (parameter.equals("1221"))
        {
            return ListTemplateType.HEALTH_REPORTS;
        }
        else if (parameter.equals("1230"))
        {
            return ListTemplateType.DEVELOPER_SITE_DRAFT_APPS;
        }
        else
        {
            return ListTemplateType.NO_LIST_TEMPLATE;
        }
    }

    static DraftVisibilityType parseDraftVisibilityType(String parameter)
    {
        if (parameter.equals("0"))
        {
            return DraftVisibilityType.READER;
        }
        else if (parameter.equals("1"))
        {
            return DraftVisibilityType.AUTHOR;
        }
        else if (parameter.equals("2"))
        {
            return DraftVisibilityType.APPROVER;
        }
        else
        {
            return DraftVisibilityType.NONE;
        }
    }

    static String parseDraftVisibilityType(DraftVisibilityType parameter)
    {
        if (parameter == DraftVisibilityType.READER)
        {
            return "0";
        }
        else if (parameter == DraftVisibilityType.AUTHOR)
        {
            return "1";
        }
        else if (parameter == DraftVisibilityType.APPROVER)
        {
            return "2";
        }
        else
        {
            return "-1";
        }
    }

    static Direction parseDirection(String parameter)
    {
        if (parameter.equals("ltr"))
        {
            return Direction.LEFT_TO_RIGHT;
        }
        else if (parameter.equals("rtl"))
        {
            return Direction.RIGHT_TO_LEFT;
        }
        else
        {
            return Direction.NONE;
        }
    }

    static String parseDirection(Direction parameter)
    {
        if (parameter == Direction.LEFT_TO_RIGHT)
        {
            return "ltr";
        }
        else if (parameter == Direction.RIGHT_TO_LEFT)
        {
            return "rtl";
        }
        else
        {
            return "none";
        }
    }

    static String parseBrowserFileHandling(BrowserFileHandling parameter)
    {
        if (parameter == BrowserFileHandling.PERMISSIVE)
        {
            return "0";
        }
        else
        {
            return "1";
        }
    }

    static BrowserFileHandling parseBrowserFileHandling(String parameter)
    {
        if (parameter.equals("0"))
        {
            return BrowserFileHandling.PERMISSIVE;
        }
        else
        {
            return BrowserFileHandling.STRICT;
        }
    }

    static String parseListBaseType(ListBaseType parameter)
    {
        if (parameter == ListBaseType.GENERIC_LIST)
        {
            return "0";
        }
        else if (parameter == ListBaseType.DOCUMENT_LIBRARY)
        {
            return "1";
        }
        else if (parameter == ListBaseType.DISCUSSION_BOARD)
        {
            return "3";
        }
        else if (parameter == ListBaseType.SURVEY)
        {
            return "4";
        }
        else
        {
            return "5";
        }
    }

    static ListBaseType parseListBaseType(String parameter)
    {
        if (parameter.equals("0"))
        {
            return ListBaseType.GENERIC_LIST;
        }
        else if (parameter.equals("1"))
        {
            return ListBaseType.DOCUMENT_LIBRARY;
        }
        else if (parameter.equals("3"))
        {
            return ListBaseType.DISCUSSION_BOARD;
        }
        else if (parameter.equals("4"))
        {
            return ListBaseType.SURVEY;
        }
        else
        {
            return ListBaseType.ISSUE;
        }
    }

    static String parseTemplateFileType(TemplateFileType parameter)
    {
        if (parameter == TemplateFileType.STANDARD_PAGE)
        {
            return "0";
        }
        else if (parameter == TemplateFileType.WIKI_PAGE)
        {
            return "1";
        }
        else if (parameter == TemplateFileType.FORM_PAGE)
        {
            return "2";
        }
        else
        {
            return "0";
        }
    }

    static TemplateFileType parseTemplateFileType(String parameter)
    {
        if (parameter.equals("0"))
        {
            return TemplateFileType.STANDARD_PAGE;
        }
        else if (parameter.equals("1"))
        {
            return TemplateFileType.WIKI_PAGE;
        }
        else if (parameter.equals("2"))
        {
            return TemplateFileType.FORM_PAGE;
        }
        else
        {
            return TemplateFileType.NONE;
        }
    }

    static String parsePersonalizationScope(PersonalizationScope parameter)
    {
        if (parameter == PersonalizationScope.USER)
        {
            return "0";
        }
        else
        {
            return "1";
        }
    }

    static PersonalizationScope parsePersonalizationScope(String parameter)
    {
        if (parameter.equals("0"))
        {
            return PersonalizationScope.USER;
        }
        else
        {
            return PersonalizationScope.SHARED;
        }

    }

    static String parseMoveOperation(MoveOperation parameter)
    {
        if (parameter == MoveOperation.OVERWRITE)
        {
            return "1";
        }
        else if (parameter == MoveOperation.ALLOW_BROKEN_THICKETS)
        {
            return "8";
        }
        else
        {
            return "0";
        }
    }

    static MoveOperation parseMoveOperation(String parameter)
    {
        if (parameter.equals("1"))
        {
            return MoveOperation.OVERWRITE;
        }
        else if (parameter.equals("8"))
        {
            return MoveOperation.ALLOW_BROKEN_THICKETS;
        }
        else
        {
            return MoveOperation.NONE;
        }
    }

    static String parseFileSystemObjectType(FileSystemObjectType parameter)
    {
        if (parameter == FileSystemObjectType.FILE)
        {
            return "0";
        }
        else if (parameter == FileSystemObjectType.FOLDER)
        {
            return "1";
        }
        else if (parameter == FileSystemObjectType.WEB)
        {
            return "2";
        }
        else
        {
            return "-1";
        }
    }

    static FileSystemObjectType parseFileSystemObjectType(String parameter)
    {
        if (parameter.equals("0"))
        {
            return FileSystemObjectType.FILE;
        }
        else if (parameter.equals("1"))
        {
            return FileSystemObjectType.FOLDER;
        }
        else if (parameter.equals("2"))
        {
            return FileSystemObjectType.WEB;
        }
        else
        {
            return FileSystemObjectType.INVALID;
        }
    }

    static String parsePrincipalType(PrincipalType parameter)
    {
        if (parameter == PrincipalType.USER)
        {
            return "1";
        }
        else if (parameter == PrincipalType.DISTRIBUTION_LIST)
        {
            return "2";
        }
        else if (parameter == PrincipalType.SECURITY_GROUP)
        {
            return "4";
        }
        else if (parameter == PrincipalType.SHARE_POINT_GROUP)
        {
            return "8";
        }
        else if (parameter == PrincipalType.ALL)
        {
            return "15";
        }
        else
        {
            return "0";
        }
    }

    static PrincipalType parsePrincipalType(String parameter)
    {
        if (parameter.equals("1"))
        {
            return PrincipalType.USER;
        }
        else if (parameter.equals("2"))
        {
            return PrincipalType.DISTRIBUTION_LIST;
        }
        else if (parameter.equals("4"))
        {
            return PrincipalType.SECURITY_GROUP;
        }
        else if (parameter.equals("8"))
        {
            return PrincipalType.SHARE_POINT_GROUP;
        }
        else if (parameter.equals("15"))
        {
            return  PrincipalType.ALL;
        }
        else
        {
            return PrincipalType.NONE;
        }
    }

    static String parseCheckInType(CheckInType parameter)
    {
        if (parameter == CheckInType.MINOR)
        {
            return "0";
        }
        else if (parameter == CheckInType.MAJOR)
        {
            return "1";
        }
        else
        {
            return "2";
        }
    }

    static CheckInType parseCheckInType(String parameter)
    {
        if (parameter.equals("0"))
        {
            return CheckInType.MINOR;
        }
        else if (parameter.equals("1"))
        {
            return CheckInType.MAJOR;
        }
        else
        {
            return CheckInType.OVERWRITE;
        }
    }

    static String parseCheckOutType(CheckOutType parameter)
    {
        if (parameter == CheckOutType.ONLINE)
        {
            return "0";
        }
        else if (parameter == CheckOutType.OFFLINE)
        {
            return "1";
        }
        else
        {
            return "2";
        }
    }

    static CheckOutType parseCheckOutType(String parameter)
    {
        if (parameter.equals("0"))
        {
            return CheckOutType.ONLINE;
        }
        else if (parameter.equals("1"))
        {
            return CheckOutType.OFFLINE;
        }
        else
        {
            return CheckOutType.NONE;
        }
    }

    static String parseFileLevel(FileLevel parameter)
    {
        if (parameter == FileLevel.PUBLISHED)
        {
            return "1";
        }
        else if (parameter == FileLevel.DRAFT)
        {
            return "2";
        }
        else
        {
            return "255";
        }
    }

    static FileLevel parseFileLevel(String parameter)
    {
        if (parameter.equals("1"))
        {
            return FileLevel.PUBLISHED;
        }
        else if (parameter.equals("2"))
        {
            return FileLevel.DRAFT;
        }
        else
        {
            return FileLevel.CHECKOUT;
        }
    }

    static String parseCustomizedPageStatus(CustomizedPageStatus parameter)
    {
        if (parameter == CustomizedPageStatus.UNCUSTOMIZED)
        {
            return "1";
        }
        else if (parameter == CustomizedPageStatus.CUSTOMIZED)
        {
            return "2";
        }
        else
        {
            return "0";
        }
    }

    static CustomizedPageStatus parseCustomizedPageStatus(String parameter)
    {
        if (parameter.equals("1"))
        {
            return CustomizedPageStatus.UNCUSTOMIZED;
        }
        else if (parameter.equals("2"))
        {
            return CustomizedPageStatus.CUSTOMIZED;
        }
        else
        {
            return CustomizedPageStatus.NONE;
        }
    }

    static String parseFieldTypeDescription(FieldType type)
    {
        if (type == FieldType.CALCULATED)
        {
            return "SP.FieldCalculated";
        }
        else if (type == FieldType.CHOICE)
        {
            return "SP.FieldChoice";
        }
        else if (type == FieldType.COMPUTED)
        {
            return "SP.FieldComputed";
        }
        else if (type == FieldType.CURRENCY)
        {
            return "SP.FieldCurrency";
        }
        else if (type == FieldType.DATE_TIME)
        {
            return "SP.FieldDateTime";
        }
        else if (type == FieldType.GUID)
        {
            return "SP.FieldGuid";
        }
        else if (type == FieldType.LOOKUP)
        {
            return "SP.FieldLookup";
        }
        else if (type == FieldType.MULTI_CHOICE)
        {
            return "SP.FieldMultiChoice";
        }
        else if (type == FieldType.NOTE)
        {
            return "SP.FieldMultiLineText";
        }
        else if (type == FieldType.NUMBER)
        {
            return "SP.FieldNumber";
        }
        else if (type == FieldType.GRID_CHOICE)
        {
            return "SP.FieldRatingScale";
        }
        else if (type == FieldType.TEXT)
        {
            return "SP.FieldText";
        }
        else if (type == FieldType.URL)
        {
            return "SP.FieldUrl";
        }
        else if (type == FieldType.USER)
        {
            return "SP.FieldUser";
        }
        else
        {
            return "SP.Field";
        }
    }
}
