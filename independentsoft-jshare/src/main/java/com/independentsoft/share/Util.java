package com.independentsoft.share;

import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import com.independentsoft.share.queryoptions.IQueryOption;

/**
 * The Class Util.
 */
public class Util {

    private static Calendar utcCalendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"));

    private static final String[] hex = {
            "%00", "%01", "%02", "%03", "%04", "%05", "%06", "%07",
            "%08", "%09", "%0a", "%0b", "%0c", "%0d", "%0e", "%0f",
            "%10", "%11", "%12", "%13", "%14", "%15", "%16", "%17",
            "%18", "%19", "%1a", "%1b", "%1c", "%1d", "%1e", "%1f",
            "%20", "%21", "%22", "%23", "%24", "%25", "%26", "%27",
            "%28", "%29", "%2a", "%2b", "%2c", "%2d", "%2e", "%2f",
            "%30", "%31", "%32", "%33", "%34", "%35", "%36", "%37",
            "%38", "%39", "%3a", "%3b", "%3c", "%3d", "%3e", "%3f",
            "%40", "%41", "%42", "%43", "%44", "%45", "%46", "%47",
            "%48", "%49", "%4a", "%4b", "%4c", "%4d", "%4e", "%4f",
            "%50", "%51", "%52", "%53", "%54", "%55", "%56", "%57",
            "%58", "%59", "%5a", "%5b", "%5c", "%5d", "%5e", "%5f",
            "%60", "%61", "%62", "%63", "%64", "%65", "%66", "%67",
            "%68", "%69", "%6a", "%6b", "%6c", "%6d", "%6e", "%6f",
            "%70", "%71", "%72", "%73", "%74", "%75", "%76", "%77",
            "%78", "%79", "%7a", "%7b", "%7c", "%7d", "%7e", "%7f",
            "%80", "%81", "%82", "%83", "%84", "%85", "%86", "%87",
            "%88", "%89", "%8a", "%8b", "%8c", "%8d", "%8e", "%8f",
            "%90", "%91", "%92", "%93", "%94", "%95", "%96", "%97",
            "%98", "%99", "%9a", "%9b", "%9c", "%9d", "%9e", "%9f",
            "%a0", "%a1", "%a2", "%a3", "%a4", "%a5", "%a6", "%a7",
            "%a8", "%a9", "%aa", "%ab", "%ac", "%ad", "%ae", "%af",
            "%b0", "%b1", "%b2", "%b3", "%b4", "%b5", "%b6", "%b7",
            "%b8", "%b9", "%ba", "%bb", "%bc", "%bd", "%be", "%bf",
            "%c0", "%c1", "%c2", "%c3", "%c4", "%c5", "%c6", "%c7",
            "%c8", "%c9", "%ca", "%cb", "%cc", "%cd", "%ce", "%cf",
            "%d0", "%d1", "%d2", "%d3", "%d4", "%d5", "%d6", "%d7",
            "%d8", "%d9", "%da", "%db", "%dc", "%dd", "%de", "%df",
            "%e0", "%e1", "%e2", "%e3", "%e4", "%e5", "%e6", "%e7",
            "%e8", "%e9", "%ea", "%eb", "%ec", "%ed", "%ee", "%ef",
            "%f0", "%f1", "%f2", "%f3", "%f4", "%f5", "%f6", "%f7",
            "%f8", "%f9", "%fa", "%fb", "%fc", "%fd", "%fe", "%ff"
    };

    /**
     * Encode url.
     *
     * @param url the url
     * @return the string
     */
    public static String encodeUrl(String url)
    {
        StringBuilder stringBuild = new StringBuilder();
        int len = url.length();

        for (int i = 0; i < len; i++)
        {
            int ch = url.charAt(i);

            if ('A' <= ch && ch <= 'Z')
            {
                // 'A'..'Z'
                stringBuild.append((char)ch);
            }
            else if ('a' <= ch && ch <= 'z')
            {
                // 'a'..'z'
                stringBuild.append((char)ch);
            }
            else if ('0' <= ch && ch <= '9')
            {
                // '0'..'9'
                stringBuild.append((char)ch);
            }
            else if (ch == ' ')
            {
                // space
                //stringBuild.Append('+');
                stringBuild.append(hex[ch]);
            }
            else if (ch == '\'')
            {
                stringBuild.append((char)'\'');
                stringBuild.append((char)'\'');
            }
            else if (ch == '-' || ch == '_' // unreserved
                    || ch == '.' || ch == '!'
                    || ch == '~' || ch == '*'
                    || ch == '\'' || ch == '('
                    || ch == ')')
            {
                stringBuild.append((char)ch);
            }
            else if (ch <= 0x007f)
            {
                // other ASCII
                stringBuild.append(hex[ch]);
            }
            else if (ch <= 0x07FF)
            {
                // non-ASCII <= 0x7FF
                stringBuild.append(hex[0xc0 | (ch >> 6)]);
                stringBuild.append(hex[0x80 | (ch & 0x3F)]);
            }
            else
            {
                // 0x7FF < ch <= 0xFFFF
                stringBuild.append(hex[0xe0 | (ch >> 12)]);
                stringBuild.append(hex[0x80 | ((ch >> 6) & 0x3F)]);
                stringBuild.append(hex[0x80 | (ch & 0x3F)]);
            }
        }

        return stringBuild.toString();
    }
    
    /**
     * Encode url.
     *
     * @param url the url
     * @return the string
     */
    public static String encodeUrlInputStream(String url)
    {
        StringBuilder stringBuild = new StringBuilder();
        int len = url.length();

        for (int i = 0; i < len; i++)
        {
            int ch = url.charAt(i);

            if ('A' <= ch && ch <= 'Z')
            {
                // 'A'..'Z'
                stringBuild.append((char)ch);
            }
            else if ('a' <= ch && ch <= 'z')
            {
                // 'a'..'z'
                stringBuild.append((char)ch);
            }
            else if ('0' <= ch && ch <= '9')
            {
                // '0'..'9'
                stringBuild.append((char)ch);
            }
            else if (ch == ' ')
            {
                // space
                //stringBuild.Append('+');
                stringBuild.append(hex[ch]);
            }
            else if (ch == '-' || ch == '_' // unreserved
                    || ch == '.' || ch == '!'
                    || ch == '~' || ch == '*'
                    || ch == '\'' || ch == '('
                    || ch == ')')
            {
                stringBuild.append((char)ch);
            }
            else if (ch <= 0x007f)
            {
                // other ASCII
                stringBuild.append(hex[ch]);
            }
            else if (ch <= 0x07FF)
            {
                // non-ASCII <= 0x7FF
                stringBuild.append(hex[0xc0 | (ch >> 6)]);
                stringBuild.append(hex[0x80 | (ch & 0x3F)]);
            }
            else
            {
                // 0x7FF < ch <= 0xFFFF
                stringBuild.append(hex[0xe0 | (ch >> 12)]);
                stringBuild.append(hex[0x80 | ((ch >> 6) & 0x3F)]);
                stringBuild.append(hex[0x80 | (ch & 0x3F)]);
            }
        }

        return stringBuild.toString();
    }

    /**
     * Encode json.
     *
     * @param s the s
     * @return the string
     */
    public static String encodeJson(String s)
    {
        if (s == null || s.length() == 0)
        {
            return "";
        }

        char c = '\0';
        int i;
        int len = s.length();
        StringBuilder sb = new StringBuilder(len + 4);
        String t;

        for (i = 0; i < len; i += 1)
        {
            c = s.charAt(i);
            switch (c)
            {
                case '\\':
                case '"':
                    sb.append('\\');
                    sb.append(c);
                    break;
                case '/':
                    sb.append('\\');
                    sb.append(c);
                    break;
                case '\b':
                    sb.append("\\b");
                    break;
                case '\t':
                    sb.append("\\t");
                    break;
                case '\n':
                    sb.append("\\n");
                    break;
                case '\f':
                    sb.append("\\f");
                    break;
                case '\r':
                    sb.append("\\r");
                    break;
                default:
                    if (c < ' ')
                    {
                        t = "000" + String.format("%X", c);
                        sb.append("\\u" + t.substring(t.length() - 4));
                    }
                    else
                    {
                        sb.append(c);
                    }
                    break;
            }
        }
        return sb.toString();
    }

    /**
     * Encode escape characters.
     *
     * @param input the input
     * @return the string
     */
    public static String encodeEscapeCharacters(String input)
    {
        if (input == null)
        {
            return null;
        }

        String output = input.replace("&", "&amp;"); //must be the first
        output = output.replace("<", "&lt;");
        output = output.replace(">", "&gt;");
        output = output.replace("'", "&apos;");
        output = output.replace("\"", "&quot;");

        return output;
    }

    /**
     * Parses the local date.
     *
     * @param localDateString the local date string
     * @return the date
     * @throws ParseException the parse exception
     */
    public static synchronized Date parseLocalDate(String localDateString) throws ParseException
    {
        String format = "yyyy-MM-dd'T'HH:mm:ss'Z'";

        if(localDateString.indexOf("Z") == -1)
        {
            format = "yyyy-MM-dd'T'HH:mm:ss";
        }
        else if (localDateString.indexOf(".") > 0 && localDateString.indexOf("Z") > localDateString.indexOf(".")) //milliseconds
        {
            format = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
        }

        DateFormat dateFormat = new SimpleDateFormat(format, java.util.Locale.US);

        Date localDate = dateFormat.parse(localDateString);

        return localDate;
    }

    /**
     * Parses the date.
     *
     * @param utcDate the utc date
     * @return the date
     * @throws ParseException the parse exception
     */
    public static synchronized Date parseDate(String utcDate) throws ParseException
    {
        String format = "yyyy-MM-dd'T'HH:mm:ss'Z'";

        if(utcDate.indexOf("Z") == -1)
        {
            format = "yyyy-MM-dd'T'HH:mm:ss";
        }
        else if (utcDate.indexOf(".") > 0 && utcDate.indexOf("Z") > utcDate.indexOf(".")) //milliseconds
        {
            format = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
        }

        DateFormat dateFormat = new SimpleDateFormat(format, java.util.Locale.US);
        dateFormat.setCalendar(utcCalendar);

        Date localDate = dateFormat.parse(utcDate);

        return localDate;
    }

    /**
     * Parses the date.
     *
     * @param utcDate the utc date
     * @param format the format
     * @return the date
     * @throws ParseException the parse exception
     */
    public static synchronized Date parseDate(String utcDate, String format) throws ParseException
    {
        DateFormat dateFormat = new SimpleDateFormat(format, java.util.Locale.US);
        dateFormat.setCalendar(utcCalendar);

        Date localDate = dateFormat.parse(utcDate);

        return localDate;
    }

    /**
     * To universal time.
     *
     * @param date the date
     * @return the string
     */
    public static synchronized String toUniversalTime(Date date)
    {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", java.util.Locale.US);
        dateFormat.setCalendar(utcCalendar);

        String utcString = dateFormat.format(date);

        return utcString;
    }
    
    /**
     * Convert from universal time to local time.
     *
     * @param utcTime the utc time
     * @return the date
     */
    public static synchronized Date convertFromUniversalTimeToLocalTime(Date utcTime)
    {
    	TimeZone localTimeZone = TimeZone.getDefault();
    	
    	long msFromEpochGmt = utcTime.getTime();

    	int offsetFromUtc = localTimeZone.getOffset(msFromEpochGmt);

        Calendar localCalendar = Calendar.getInstance();
        localCalendar.setTime(utcTime);
        localCalendar.add(Calendar.MILLISECOND, offsetFromUtc);
   	
    	Date localDate = localCalendar.getTime();
    	
    	return localDate;
    }
    
    /**
     * Convert from local time to universal time.
     *
     * @param localTime the local time
     * @return the date
     */
    public static synchronized Date convertFromLocalTimeToUniversalTime(Date localTime)
    {
    	TimeZone localTimeZone = TimeZone.getDefault();
    	TimeZone utcTimeZone = TimeZone.getTimeZone("GMT");
    	
    	long msFromEpochGmt = localTime.getTime();

    	int offsetFromUtc = localTimeZone.getOffset(msFromEpochGmt) * -1;

        Calendar utcCalendar = Calendar.getInstance(utcTimeZone);
        utcCalendar.setTime(localTime);
        utcCalendar.add(Calendar.MILLISECOND, offsetFromUtc);
   	
    	Date utcDate = utcCalendar.getTime();
    	
    	return utcDate;
    }
    
    /**
     * To string.
     *
     * @param date the date
     * @param format the format
     * @return the string
     */
    public static synchronized String toString(Date date, String format)
    {
        DateFormat dateFormat = new SimpleDateFormat(format, java.util.Locale.US);

        String dateString = dateFormat.format(date);

        return dateString;
    }

    /**
     * To local time.
     *
     * @param date the date
     * @return the string
     */
    public static synchronized String toLocalTime(Date date)
    {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", java.util.Locale.US);

        String localTimeString = dateFormat.format(date);

        return localTimeString;
    }

    static String queryOptionsToString(List<IQueryOption> queryOptions)
    {
        if (queryOptions == null || queryOptions.size() == 0)
        {
            return "";
        }
        else
        {
            String stringValue = "?";

            for (int i = 0; i < queryOptions.size(); i++)
            {
                if (queryOptions.get(i) != null)
                {
                    stringValue += queryOptions.get(i).toString();
                }

                if (i < queryOptions.size() - 1)
                {
                    stringValue += "&";
                }
            }

            return stringValue;
        }
    }
    
    static List<BasePermission> parseBasePermissions(InputStream inputStream, String localName) throws XMLStreamException
    {
        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        XMLStreamReader reader = xmlInputFactory.createXMLStreamReader(inputStream);

        return parseBasePermissions(reader, localName);
    }

    static List<BasePermission> parseBasePermissions(XMLStreamReader reader, String localName) throws XMLStreamException
    {
        long low = 0;
        long high = 0;

        while (reader.hasNext())
        {
            if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("High") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                String stringValue = reader.getElementText();

                if (stringValue != null && stringValue.length() > 0)
                {
                    high = Long.parseLong(stringValue);
                }
            }
            else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Low") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                String stringValue = reader.getElementText();

                if (stringValue != null && stringValue.length() > 0)
                {
                    low = Long.parseLong(stringValue);
                }
            }

            if (reader.isEndElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals(localName) && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                break;
            }
            else
            {
                reader.next();
            }
        }

        if (low > 0 || high > 0)
        {
            return EnumUtil.parseBasePermission(low, high);
        }
        else
        {
            return new ArrayList<BasePermission>();
        }
    }

    static String basePermissionsToJSon(List<BasePermission> basePermissions, String localName)
    {
	BasePermissionMask mask = EnumUtil.parseBasePermission(basePermissions);

        String stringValue =  ", '" + localName + "':{'__metadata':{'type':'SP.BasePermissions'}, 'High':'" + mask.High + "', 'Low':'" + mask.Low + "'}";

        return stringValue;
    }

}
