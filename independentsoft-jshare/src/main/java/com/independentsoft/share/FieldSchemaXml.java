package com.independentsoft.share;

import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

/**
 * The Class FieldSchemaXml.
 */
public class FieldSchemaXml {

    private FieldAggregation aggregation = FieldAggregation.NONE;
    private boolean allowDeletion = true;
    private boolean allowHyperlink;
    private boolean allowMultiVote;
    private boolean isAppendOnly;
    private String authoringInfo;
    private FieldType baseType = FieldType.NONE;
    private int calendarType = -1;
    private boolean canToggleHidden;
    private String classInfo;
    private String columnName;
    private String columnName2;
    private boolean hasCommas;
    private String customization;
    private int decimals = -1;
    private String description;
    private String dir;
    private Direction direction = Direction.NONE;
    private boolean isDisplaceOnUpgrade;
    private String displayImage;
    private String displayName;
    private String displayNameSourceField;
    private int displaySize = -1;
    private String div;
    private boolean enableLookup;
    private boolean enforceUniqueValues;
    private String exceptionImage;
    private String fieldRef;
    private boolean isFillInChoice;
    private boolean isFilterable = true;
    private boolean isFilterableNoRecurrence = true;
    private String forcedDisplay;
    private boolean isForcePromoteDemote;
    private String format;
    private boolean isFromBaseType;
    private String group;
    private String headerImage;
    private int height = -1;
    private boolean isHidden;
    private boolean isHtmlEncode;
    private String id;
    private ImeMode imeMode = ImeMode.NONE;
    private boolean isIndexed;
    private boolean isIsolateStyles;
    private boolean isRelationship;
    private String joinColumnName;
    private int joinRowOrdinal = -1;
    private JoinType joinType = JoinType.NONE;
    private Locale locale = Locale.NONE;
    private boolean isLinkToItem;
    private String linkToItemAllowed;
    private String list;
    private boolean isListItemMenu;
    private String listItemMenuAllowed;
    private double max = Double.MIN_VALUE;
    private int maxLength = -1;
    private double min = Double.MIN_VALUE;
    private boolean isMultiplied;
    private String name;
    private String negativeFormat;
    private String node;
    private boolean isNoEditFormBreak;
    private int numberOfLines = -1;
    private boolean isPercentage;
    private String piAttribute;
    private String piTarget;
    private boolean isPrependId;
    private boolean isPresence;
    private boolean isPrimaryKey;
    private String primaryPIAttribute;
    private String primaryPITarget;
    private boolean isReadOnly;
    private boolean isReadOnlyEnforced;
    private String relationshipDeleteBehavior;
    private boolean isRenderXmlUsingPattern;
    private boolean isRequired;
    private boolean isRestrictedMode;
    private String resultType;
    private boolean isRichText;
    private String richTextMode;
    private int rowOrdinal = -1;
    private boolean isSealed;
    private boolean isSeparateLine;
    private String setAs;
    private boolean showAddressBookButton;
    private boolean showAlways;
    private String showField;
    private boolean showInDisplayForm;
    private boolean showInEditForm = true;
    private boolean showInFileDialog;
    private boolean showInListSettings;
    private boolean showInNewForm = true;
    private boolean showInVersionHistory;
    private boolean showInViewForms;
    private boolean isSortable;
    private String sourceId;
    private String staticName;
    private String storageTimeZone;
    private boolean isStripWS;
    private boolean isSuppressNameDisplay;
    private boolean isTextOnly;
    private String title;
    private String type;
    private String uniqueId;
    private boolean isUnlimitedLengthInDocumentLibrary;
    private boolean isUrlEncode;
    private boolean isUrlEncodeAsUrl;
    private String userSelectionMode;
    private int userSelectionScope = -1;
    private String version;
    private boolean isViewable;
    private String webId;
    private int width = -1;
    private boolean isWikiLinking;
    private String xName;

    /**
     * Instantiates a new field schema xml.
     */
    public FieldSchemaXml()
    {
    }

    FieldSchemaXml(String innerXml) throws XMLStreamException
    {
        if (innerXml != null && innerXml.length() > 0)
        {
            byte[] buffer = null;

            try
            {
                buffer = innerXml.getBytes("UTF-8");
            }
            catch(UnsupportedEncodingException ex)
            {
                System.err.println(ex.getMessage());
            }

            ByteArrayInputStream inputStream = new ByteArrayInputStream(buffer);

            XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
            XMLStreamReader newReader = xmlInputFactory.createXMLStreamReader(inputStream);
            newReader.next();
            parse(newReader);
        }
    }

    private void parse(XMLStreamReader reader)
    {
        authoringInfo = reader.getAttributeValue(null, "AuthoringInfo");
        classInfo = reader.getAttributeValue(null, "ClassInfo");
        columnName = reader.getAttributeValue(null, "ColName");
        columnName2 = reader.getAttributeValue(null, "ColName2");
        customization = reader.getAttributeValue(null, "Customization");
        description = reader.getAttributeValue(null, "Description");
        dir = reader.getAttributeValue(null, "Dir");
        displayImage = reader.getAttributeValue(null, "DisplayImage");
        displayName = reader.getAttributeValue(null, "DisplayName");
        displayNameSourceField = reader.getAttributeValue(null, "DisplayNameSrcField");
        div = reader.getAttributeValue(null, "Div");
        exceptionImage = reader.getAttributeValue(null, "ExceptionImage");
        fieldRef = reader.getAttributeValue(null, "FieldRef");
        forcedDisplay = reader.getAttributeValue(null, "ForcedDisplay");
        format = reader.getAttributeValue(null, "Format");
        group = reader.getAttributeValue(null, "Group");
        headerImage = reader.getAttributeValue(null, "HeaderImage");
        id = reader.getAttributeValue(null, "ID");
        joinColumnName = reader.getAttributeValue(null, "JoinColName");
        linkToItemAllowed = reader.getAttributeValue(null, "LinkToItemAllowed");
        list = reader.getAttributeValue(null, "List");
        listItemMenuAllowed = reader.getAttributeValue(null, "ListItemMenuAllowed");
        name = reader.getAttributeValue(null, "Name");
        negativeFormat = reader.getAttributeValue(null, "NegativeFormat");
        node = reader.getAttributeValue(null, "node") != null ? reader.getAttributeValue(null, "node") : reader.getAttributeValue(null, "Node");
        piAttribute = reader.getAttributeValue(null, "PIAttribute");
        piTarget = reader.getAttributeValue(null, "PITarget");
        primaryPIAttribute = reader.getAttributeValue(null, "PrimaryPIAttribute");
        primaryPITarget = reader.getAttributeValue(null, "PrimaryPITarget");
        relationshipDeleteBehavior = reader.getAttributeValue(null, "RelationshipDeleteBehavior");
        resultType = reader.getAttributeValue(null, "ResultType");
        richTextMode = reader.getAttributeValue(null, "RichTextMode");
        setAs = reader.getAttributeValue(null, "SetAs");
        showField = reader.getAttributeValue(null, "ShowField");
        sourceId = reader.getAttributeValue(null, "SourceID");
        staticName = reader.getAttributeValue(null, "StaticName");
        storageTimeZone = reader.getAttributeValue(null, "StorageTZ");
        title = reader.getAttributeValue(null, "Title");
        type = reader.getAttributeValue(null, "Type");
        uniqueId = reader.getAttributeValue(null, "UniqueId");
        userSelectionMode = reader.getAttributeValue(null, "UserSelectionMode");
        version = reader.getAttributeValue(null, "Version");
        webId = reader.getAttributeValue(null, "WebId");
        xName = reader.getAttributeValue(null, "XName");

        String aggregationAttribute = reader.getAttributeValue(null, "Aggregation");
        String allowDeletionAttribute = reader.getAttributeValue(null, "AllowDeletion");
        String allowHyperlinkAttribute = reader.getAttributeValue(null, "AllowHyperlink");
        String allowMultiVoteAttribute = reader.getAttributeValue(null, "AllowMultiVote");
        String appendOnlyAttribute = reader.getAttributeValue(null, "AppendOnly");
        String baseRenderingTypeAttribute = reader.getAttributeValue(null, "BaseRenderingType");
        String baseTypeAttribute = reader.getAttributeValue(null, "BaseType");
        String calTypeAttribute = reader.getAttributeValue(null, "CalType");
        String calendarTypeAttribute = reader.getAttributeValue(null, "CalendarType");
        String camlRenderingAttribute = reader.getAttributeValue(null, "CAMLRendering");
        String canToggleHiddenAttribute = reader.getAttributeValue(null, "CanToggleHidden");
        String countRelatedAttribute = reader.getAttributeValue(null, "CountRelated");
        String commasAttribute = reader.getAttributeValue(null, "Commas");
        String decimalsAttribute = reader.getAttributeValue(null, "Decimals");
        String defaultListFieldAttribute = reader.getAttributeValue(null, "DefaultListField");
        String directionAttribute = reader.getAttributeValue(null, "Direction");
        String displaceOnUpgradeAttribute = reader.getAttributeValue(null, "DisplaceOnUpgrade");
        String displaySizeAttribute = reader.getAttributeValue(null, "DisplaySize");
        String enableLookupAttribute = reader.getAttributeValue(null, "EnableLookup");
        String enforceUniqueValuesAttribute = reader.getAttributeValue(null, "EnforceUniqueValues");
        String fillInChoiceAttribute = reader.getAttributeValue(null, "FillInChoice");
        String filterableAttribute = reader.getAttributeValue(null, "Filterable");
        String filterableNoRecurrenceAttribute = reader.getAttributeValue(null, "FilterableNoRecurrence");
        String forcePromoteDemoteAttribute = reader.getAttributeValue(null, "ForcePromoteDemote");
        String fromBaseTypeAttribute = reader.getAttributeValue(null, "FromBaseType");
        String gridEndNumberAttribute = reader.getAttributeValue(null, "GridEndNum");
        String gridStartNumberAttribute = reader.getAttributeValue(null, "GridStartNum");
        String heightAttribute = reader.getAttributeValue(null, "Height");
        String hiddenAttribute = reader.getAttributeValue(null, "Hidden");
        String htmlEncodeAttribute = reader.getAttributeValue(null, "HTMLEncode");
        String imeModeAttribute = reader.getAttributeValue(null, "IMEMode");
        String indexedAttribute = reader.getAttributeValue(null, "Indexed");
        String isolateStylesAttribute = reader.getAttributeValue(null, "IsolateStyles");
        String isRelationshipAttribute = reader.getAttributeValue(null, "IsRelationship");
        String joinRowOrdinalAttribute = reader.getAttributeValue(null, "JoinRowOrdinal");
        String joinTypeAttribute = reader.getAttributeValue(null, "JoinType");
        String lcidAttribute = reader.getAttributeValue(null, "LCID");
        String linkToItemAttribute = reader.getAttributeValue(null, "LinkToItem");
        String listItemMenuAttribute = reader.getAttributeValue(null, "ListItemMenu");
        String maxAttribute = reader.getAttributeValue(null, "Max");
        String maxLengthAttribute = reader.getAttributeValue(null, "MaxLength");
        String minAttribute = reader.getAttributeValue(null, "Min");
        String isMultipliedAttribute = reader.getAttributeValue(null, "Mult");
        String isNoEditFormBreakAttribute = reader.getAttributeValue(null, "NoEditFormBreak");
        String numberOfLinesAttribute = reader.getAttributeValue(null, "NumLines");
        String overwriteInChildScopesAttribute = reader.getAttributeValue(null, "OverwriteInChildScopes");
        String percentageAttribute = reader.getAttributeValue(null, "Percentage");
        String prependIdAttribute = reader.getAttributeValue(null, "PrependId");
        String presenceAttribute = reader.getAttributeValue(null, "Presence");
        String primaryKeyAttribute = reader.getAttributeValue(null, "PrimaryKey");
        String readOnlyAttribute = reader.getAttributeValue(null, "ReadOnly");
        String readOnlyEnforcedAttribute = reader.getAttributeValue(null, "ReadOnlyEnforced");
        String renderXmlUsingPatternAttribute = reader.getAttributeValue(null, "RenderXMLUsingPattern");
        String requiredAttribute = reader.getAttributeValue(null, "Required");
        String restrictedModeAttribute = reader.getAttributeValue(null, "RestrictedMode");
        String resyncOnChangeAttribute = reader.getAttributeValue(null, "ResyncOnChange");
        String richTextAttribute = reader.getAttributeValue(null, "RichText");
        String rowOrdinalAttribute = reader.getAttributeValue(null, "RowOrdinal");
        String rowOrdinal2Attribute = reader.getAttributeValue(null, "RowOrdinal2");
        String isSealedAttribute = reader.getAttributeValue(null, "Sealed");
        String separateLineAttribute = reader.getAttributeValue(null, "SeparateLine");
        String showAddressBookButtonAttribute = reader.getAttributeValue(null, "ShowAddressBookButton");
        String showAlwaysAttribute = reader.getAttributeValue(null, "ShowAlways");
        String showInDisplayFormAttribute = reader.getAttributeValue(null, "ShowInDisplayForm");
        String showInEditFormAttribute = reader.getAttributeValue(null, "ShowInEditForm");
        String showInFileDialogAttribute = reader.getAttributeValue(null, "ShowInFileDlg");
        String showInListSettingsAttribute = reader.getAttributeValue(null, "ShowInListSettings");
        String showInNewFormAttribute = reader.getAttributeValue(null, "ShowInNewForm");
        String showInVersionHistoryAttribute = reader.getAttributeValue(null, "ShowInVersionHistory");
        String showInViewFormsAttribute = reader.getAttributeValue(null, "ShowInViewForms");
        String sortableAttribute = reader.getAttributeValue(null, "Sortable");
        String suppressNameDisplayAttribute = reader.getAttributeValue(null, "SuppressNameDisplay");
        String isStripWSAttribute = reader.getAttributeValue(null, "StripWS");
        String textOnlyAttribute = reader.getAttributeValue(null, "TextOnly");
        String titleFieldAttribute = reader.getAttributeValue(null, "TitleField");
        String unlimitedLengthInDocumentLibraryAttribute = reader.getAttributeValue(null, "UnlimitedLengthInDocumentLibrary");
        String urlEncodeAttribute = reader.getAttributeValue(null, "URLEncode");
        String urlEncodeAsUrlAttribute = reader.getAttributeValue(null, "URLEncodeAsURL");
        String userSelectionScopeAttribute = reader.getAttributeValue(null, "UserSelectionScope");
        String viewableAttribute = reader.getAttributeValue(null, "Viewable");
        String widthAttribute = reader.getAttributeValue(null, "Width");
        String wikiLinkingAttribute = reader.getAttributeValue(null, "WikiLinking");

        if(aggregationAttribute != null && aggregationAttribute.length() > 0)
        {
            aggregation = EnumUtil.parseFieldAggregation(aggregationAttribute);
        }

        if(allowDeletionAttribute != null && allowDeletionAttribute.length() > 0)
        {
            allowDeletion = Boolean.parseBoolean(allowDeletionAttribute);
        }

        if(allowHyperlinkAttribute != null && allowHyperlinkAttribute.length() > 0)
        {
            allowHyperlink = Boolean.parseBoolean(allowHyperlinkAttribute);
        }

        if(allowMultiVoteAttribute != null && allowMultiVoteAttribute.length() > 0)
        {
            allowMultiVote = Boolean.parseBoolean(allowMultiVoteAttribute);
        }

        if(appendOnlyAttribute != null && appendOnlyAttribute.length() > 0)
        {
            isAppendOnly = Boolean.parseBoolean(appendOnlyAttribute);
        }

        if(baseTypeAttribute != null && baseTypeAttribute.length() > 0)
        {
            baseType = EnumUtil.parseFieldType(baseTypeAttribute);
        }

        if(calTypeAttribute != null && calTypeAttribute.length() > 0)
        {
            calendarType = Integer.parseInt(calTypeAttribute);
        }
        else if(calendarTypeAttribute != null && calendarTypeAttribute.length() > 0)
        {
            calendarType = Integer.parseInt(calendarTypeAttribute);
        }

        if(canToggleHiddenAttribute != null && canToggleHiddenAttribute.length() > 0)
        {
            canToggleHidden = Boolean.parseBoolean(canToggleHiddenAttribute);
        }

        if (commasAttribute != null && commasAttribute.length() > 0)
        {
            hasCommas = Boolean.parseBoolean(commasAttribute);
        }

        if(decimalsAttribute != null && decimalsAttribute.length() > 0)
        {
            decimals = Integer.parseInt(decimalsAttribute);
        }

        if(directionAttribute != null && directionAttribute.length() > 0)
        {
            direction = EnumUtil.parseDirection(directionAttribute);
        }

        if(displaceOnUpgradeAttribute != null && displaceOnUpgradeAttribute.length() > 0)
        {
            isDisplaceOnUpgrade = Boolean.parseBoolean(displaceOnUpgradeAttribute);
        }

        if(displaySizeAttribute != null && displaySizeAttribute.length() > 0)
        {
            displaySize = Integer.parseInt(displaySizeAttribute);
        }

        if(enableLookupAttribute != null && enableLookupAttribute.length() > 0)
        {
            enableLookup = Boolean.parseBoolean(enableLookupAttribute);
        }

        if(enforceUniqueValuesAttribute != null && enforceUniqueValuesAttribute.length() > 0)
        {
            enforceUniqueValues = Boolean.parseBoolean(enforceUniqueValuesAttribute);
        }

        if (fillInChoiceAttribute != null && fillInChoiceAttribute.length() > 0)
        {
            isFillInChoice = Boolean.parseBoolean(fillInChoiceAttribute);
        }

        if (filterableAttribute != null && filterableAttribute.length() > 0)
        {
            isFilterable = Boolean.parseBoolean(filterableAttribute);
        }

        if (filterableNoRecurrenceAttribute != null && filterableNoRecurrenceAttribute.length() > 0)
        {
            isFilterableNoRecurrence = Boolean.parseBoolean(filterableNoRecurrenceAttribute);
        }

        if (forcePromoteDemoteAttribute != null && forcePromoteDemoteAttribute.length() > 0)
        {
            isForcePromoteDemote = Boolean.parseBoolean(forcePromoteDemoteAttribute);
        }

        if (fromBaseTypeAttribute != null && fromBaseTypeAttribute.length() > 0)
        {
            isFromBaseType = Boolean.parseBoolean(fromBaseTypeAttribute);
        }

        if (heightAttribute != null && heightAttribute.length() > 0)
        {
            height = Integer.parseInt(heightAttribute);
        }

        if (hiddenAttribute != null && hiddenAttribute.length() > 0)
        {
            isHidden = Boolean.parseBoolean(hiddenAttribute);
        }

        if (htmlEncodeAttribute != null && htmlEncodeAttribute.length() > 0)
        {
            isHtmlEncode = Boolean.parseBoolean(htmlEncodeAttribute);
        }

        if (imeModeAttribute != null && imeModeAttribute.length() > 0)
        {
            imeMode = EnumUtil.parseImeMode(imeModeAttribute);
        }

        if (indexedAttribute != null && indexedAttribute.length() > 0)
        {
            isIndexed = Boolean.parseBoolean(indexedAttribute);
        }

        if (isolateStylesAttribute != null && isolateStylesAttribute.length() > 0)
        {
            isIsolateStyles = Boolean.parseBoolean(isolateStylesAttribute);
        }

        if (isRelationshipAttribute != null && isRelationshipAttribute.length() > 0)
        {
            isRelationship = Boolean.parseBoolean(isRelationshipAttribute);
        }

        if (joinRowOrdinalAttribute != null && joinRowOrdinalAttribute.length() > 0)
        {
            joinRowOrdinal = Integer.parseInt(joinRowOrdinalAttribute);
        }

        if (joinTypeAttribute != null && joinTypeAttribute.length() > 0)
        {
            joinType = EnumUtil.parseJoinType(joinTypeAttribute);
        }

        if(lcidAttribute != null && lcidAttribute.length() > 0)
        {
            locale = EnumUtil.parseLocale(lcidAttribute);
        }

        if (linkToItemAttribute != null && linkToItemAttribute.length() > 0)
        {
            isLinkToItem = Boolean.parseBoolean(linkToItemAttribute);
        }

        if (listItemMenuAttribute != null && listItemMenuAttribute.length() > 0)
        {
            isListItemMenu = Boolean.parseBoolean(listItemMenuAttribute);
        }

        if (maxAttribute != null && maxAttribute.length() > 0)
        {
            max = Double.parseDouble(maxAttribute);
        }

        if (maxLengthAttribute != null && maxLengthAttribute.length() > 0)
        {
            maxLength = Integer.parseInt(maxLengthAttribute);
        }

        if (minAttribute != null && minAttribute.length() > 0)
        {
            min = Integer.parseInt(minAttribute);
        }

        if (isMultipliedAttribute != null && isMultipliedAttribute.length() > 0)
        {
            isMultiplied = Boolean.parseBoolean(isMultipliedAttribute);
        }

        if(isNoEditFormBreakAttribute != null && isNoEditFormBreakAttribute.length() > 0)
        {
            isNoEditFormBreak = Boolean.parseBoolean(isNoEditFormBreakAttribute);
        }

        if (numberOfLinesAttribute != null && numberOfLinesAttribute.length() > 0)
        {
            numberOfLines = Integer.parseInt(numberOfLinesAttribute);
        }

        if(percentageAttribute != null && percentageAttribute.length() > 0)
        {
            isPercentage = Boolean.parseBoolean(percentageAttribute);
        }

        if(prependIdAttribute != null && prependIdAttribute.length() > 0)
        {
            isPrependId = Boolean.parseBoolean(prependIdAttribute);
        }

        if(presenceAttribute != null && presenceAttribute.length() > 0)
        {
            isPresence = Boolean.parseBoolean(presenceAttribute);
        }

        if (primaryKeyAttribute != null && primaryKeyAttribute.length() > 0)
        {
            isPrimaryKey = Boolean.parseBoolean(primaryKeyAttribute);
        }

        if (readOnlyAttribute != null && readOnlyAttribute.length() > 0)
        {
            isReadOnly = Boolean.parseBoolean(readOnlyAttribute);
        }

        if (readOnlyEnforcedAttribute != null && readOnlyEnforcedAttribute.length() > 0)
        {
            isReadOnlyEnforced = Boolean.parseBoolean(readOnlyEnforcedAttribute);
        }

        if (renderXmlUsingPatternAttribute != null && renderXmlUsingPatternAttribute.length() > 0)
        {
            isRenderXmlUsingPattern = Boolean.parseBoolean(renderXmlUsingPatternAttribute);
        }

        if (requiredAttribute != null && requiredAttribute.length() > 0)
        {
            isRequired = Boolean.parseBoolean(requiredAttribute);
        }

        if (restrictedModeAttribute != null && restrictedModeAttribute.length() > 0)
        {
            isRestrictedMode = Boolean.parseBoolean(restrictedModeAttribute);
        }

        if (richTextAttribute != null && richTextAttribute.length() > 0)
        {
            isRichText = Boolean.parseBoolean(richTextAttribute);
        }

        if (rowOrdinalAttribute != null && rowOrdinalAttribute.length() > 0)
        {
            rowOrdinal = Integer.parseInt(rowOrdinalAttribute);
        }

        if(isSealedAttribute != null && isSealedAttribute.length() > 0)
        {
            isSealed = Boolean.parseBoolean(isSealedAttribute);
        }

        if(separateLineAttribute != null && separateLineAttribute.length() > 0)
        {
            isSeparateLine = Boolean.parseBoolean(separateLineAttribute);
        }

        if(showAddressBookButtonAttribute != null && showAddressBookButtonAttribute.length() > 0)
        {
            showAddressBookButton = Boolean.parseBoolean(showAddressBookButtonAttribute);
        }

        if (showAlwaysAttribute != null && showAlwaysAttribute.length() > 0)
        {
            showAlways = Boolean.parseBoolean(showAlwaysAttribute);
        }

        if(showInDisplayFormAttribute != null && showInDisplayFormAttribute.length() > 0)
        {
            showInDisplayForm = Boolean.parseBoolean(showInDisplayFormAttribute);
        }

        if(showInEditFormAttribute != null && showInEditFormAttribute.length() > 0)
        {
            showInEditForm = Boolean.parseBoolean(showInEditFormAttribute);
        }

        if(showInFileDialogAttribute != null && showInFileDialogAttribute.length() > 0)
        {
            showInFileDialog = Boolean.parseBoolean(showInFileDialogAttribute);
        }

        if(showInListSettingsAttribute != null && showInListSettingsAttribute.length() > 0)
        {
            showInListSettings = Boolean.parseBoolean(showInListSettingsAttribute);
        }

        if(showInNewFormAttribute != null && showInNewFormAttribute.length() > 0)
        {
            showInNewForm = Boolean.parseBoolean(showInNewFormAttribute);
        }

        if(showInVersionHistoryAttribute != null && showInVersionHistoryAttribute.length() > 0)
        {
            showInVersionHistory = Boolean.parseBoolean(showInVersionHistoryAttribute);
        }

        if(showInViewFormsAttribute != null && showInViewFormsAttribute.length() > 0)
        {
            showInViewForms = Boolean.parseBoolean(showInViewFormsAttribute);
        }

        if(sortableAttribute != null && sortableAttribute.length() > 0)
        {
            isSortable = Boolean.parseBoolean(sortableAttribute);
        }

        if(suppressNameDisplayAttribute != null && suppressNameDisplayAttribute.length() > 0)
        {
            isSuppressNameDisplay = Boolean.parseBoolean(suppressNameDisplayAttribute);
        }

        if (isStripWSAttribute != null && isStripWSAttribute.length() > 0)
        {
            isStripWS = Boolean.parseBoolean(isStripWSAttribute);
        }

        if(textOnlyAttribute != null && textOnlyAttribute.length() > 0)
        {
            isTextOnly = Boolean.parseBoolean(textOnlyAttribute);
        }

        if(unlimitedLengthInDocumentLibraryAttribute != null && unlimitedLengthInDocumentLibraryAttribute.length() > 0)
        {
            isUnlimitedLengthInDocumentLibrary = Boolean.parseBoolean(unlimitedLengthInDocumentLibraryAttribute);
        }

        if(urlEncodeAttribute != null && urlEncodeAttribute.length() > 0)
        {
            isUrlEncode = Boolean.parseBoolean(urlEncodeAttribute);
        }

        if(urlEncodeAsUrlAttribute != null && urlEncodeAsUrlAttribute.length() > 0)
        {
            isUrlEncodeAsUrl = Boolean.parseBoolean(urlEncodeAsUrlAttribute);
        }

        if (userSelectionScopeAttribute != null && userSelectionScopeAttribute.length() > 0)
        {
            userSelectionScope = Integer.parseInt(userSelectionScopeAttribute);
        }

        if (viewableAttribute != null && viewableAttribute.length() > 0)
        {
            isViewable = Boolean.parseBoolean(viewableAttribute);
        }

        if (widthAttribute != null && widthAttribute.length() > 0)
        {
            width = Integer.parseInt(widthAttribute);
        }

        if (wikiLinkingAttribute != null && wikiLinkingAttribute.length() > 0)
        {
            isWikiLinking = Boolean.parseBoolean(wikiLinkingAttribute);
        }
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        String stringValue = "<Field";

        if(aggregation != FieldAggregation.NONE)
        {
            stringValue += " Aggregation=\"" + EnumUtil.parseFieldAggregation(aggregation) +"\"";
        }

        if(!allowDeletion)
        {
            stringValue += " AllowDeletion=\"FALSE\"";
        }

        if(allowHyperlink)
        {
            stringValue += " AllowHyperlink=\"TRUE\"";
        }

        if(allowMultiVote)
        {
            stringValue += " AllowMultiVote=\"TRUE\"";
        }

        if (isAppendOnly)
        {
            stringValue += " AppendOnly=\"TRUE\"";
        }

        if (authoringInfo != null && authoringInfo.length() > 0)
        {
            stringValue += " AuthoringInfo=\"" + Util.encodeJson(authoringInfo) + "\"";
        }

        if (baseType != FieldType.NONE)
        {
            stringValue += " BaseType=\"" + EnumUtil.parseFieldType(baseType) + "\"";
        }

        if (calendarType > -1)
        {
            stringValue += " CalType=\"" + calendarType + "\"";
        }

        if (canToggleHidden)
        {
            stringValue += " CanToggleHidden=\"TRUE\"";
        }

        if (classInfo != null && classInfo.length() > 0)
        {
            stringValue += " ClassInfo=\"" + Util.encodeJson(classInfo) + "\"";
        }

        if (columnName != null && columnName.length() > 0)
        {
            stringValue += " ColName=\"" + Util.encodeJson(columnName) + "\"";
        }

        if (columnName2 != null && columnName2.length() > 0)
        {
            stringValue += " ColName2=\"" + Util.encodeJson(columnName2) + "\"";
        }

        if (hasCommas)
        {
            stringValue += " Commas=\"TRUE\"";
        }

        if (customization != null && customization.length() > 0)
        {
            stringValue += " Customization=\"" + Util.encodeJson(customization) + "\"";
        }

        if (decimals > -1)
        {
            stringValue += " Decimals=\"" + decimals + "\"";
        }

        if (description != null && description.length() > 0)
        {
            stringValue += " Description=\"" + Util.encodeJson(description) + "\"";
        }

        if (dir != null && dir.length() > 0)
        {
            stringValue += " Dir=\"" + Util.encodeJson(dir) + "\"";
        }

        if (isDisplaceOnUpgrade)
        {
            stringValue += " DisplaceOnUpgrade=\"TRUE\"";
        }

        if (displayImage != null && displayImage.length() > 0)
        {
            stringValue += " DisplayImage=\"" + Util.encodeJson(displayImage) + "\"";
        }

        if (displayName != null && displayName.length() > 0)
        {
            stringValue += " DisplayName=\"" + Util.encodeJson(displayName) + "\"";
        }

        if (displayNameSourceField != null && displayNameSourceField.length() > 0)
        {
            stringValue += " DisplayNameSrcField=\"" + Util.encodeJson(displayNameSourceField) + "\"";
        }

        if (displaySize > -1)
        {
            stringValue += " DisplaySize=\"" + displaySize + "\"";
        }

        if (div != null && div.length() > 0)
        {
            stringValue += " Div=\"" + Util.encodeJson(div) + "\"";
        }

        if (enableLookup)
        {
            stringValue += " EnableLookup=\"TRUE\"";
        }

        if (enforceUniqueValues)
        {
            stringValue += " AuthoringInfo=\"TRUE\"";
        }

        if (exceptionImage != null && exceptionImage.length() > 0)
        {
            stringValue += " ExceptionImage=\"" + Util.encodeJson(exceptionImage) + "\"";
        }

        if (fieldRef != null && fieldRef.length() > 0)
        {
            stringValue += " FieldRef=\"" + Util.encodeJson(fieldRef) + "\"";
        }

        if (isFillInChoice)
        {
            stringValue += " FillInChoice=\"TRUE\"";
        }

        if (!isFilterable)
        {
            stringValue += " Filterable=\"FALSE\"";
        }

        if (!isFilterableNoRecurrence)
        {
            stringValue += " FilterableNoRecurrence=\"FALSE\"";
        }

        if (forcedDisplay != null && forcedDisplay.length() > 0)
        {
            stringValue += " ForcedDisplay=\"" + Util.encodeJson(forcedDisplay) + "\"";
        }

        if (isForcePromoteDemote)
        {
            stringValue += " ForcePromoteDemote=\"TRUE\"";
        }

        if (format != null && format.length() > 0)
        {
            stringValue += " Format=\"" + Util.encodeJson(format) + "\"";
        }

        if (isFromBaseType)
        {
            stringValue += " FromBaseType=\"TRUE\"";
        }

        if (group != null && group.length() > 0)
        {
            stringValue += " Group=\"" + Util.encodeJson(group) + "\"";
        }

        if (headerImage != null && headerImage.length() > 0)
        {
            stringValue += " HeaderImage=\"" + Util.encodeJson(headerImage) + "\"";
        }

        if (height > -1)
        {
            stringValue += " Height=\"" + height + "\"";
        }

        if (isHidden)
        {
            stringValue += " Hidden=\"TRUE\"";
        }

        if (isHtmlEncode)
        {
            stringValue += " HTMLEncode=\"TRUE\"";
        }

        if (id != null && id.length() > 0)
        {
            stringValue += " ID=\"" + Util.encodeJson(id) + "\"";
        }

        if (imeMode != ImeMode.NONE)
        {
            stringValue += " IMEMode=\"" + EnumUtil.parseImeMode(imeMode) + "\"";
        }

        if (isIndexed)
        {
            stringValue += " Indexed=\"TRUE\"";
        }

        if (isIsolateStyles)
        {
            stringValue += " IsolateStyles=\"TRUE\"";
        }

        if (isRelationship)
        {
            stringValue += " IsRelationship=\"TRUE\"";
        }

        if (joinColumnName != null && joinColumnName.length() > 0)
        {
            stringValue += " JoinColName=\"" + Util.encodeJson(joinColumnName) + "\"";
        }

        if (joinRowOrdinal > -1)
        {
            stringValue += " JoinRowOrdinal=\"" + joinRowOrdinal + "\"";
        }

        if (joinType != JoinType.NONE)
        {
            stringValue += " JoinType=\"" + EnumUtil.parseJoinType(joinType) + "\"";
        }

        if (locale != Locale.NONE)
        {
            stringValue += " LCID=\"" + EnumUtil.parseLocale(locale) + "\"";
        }

        if (isLinkToItem)
        {
            stringValue += " LinkToItem=\"TRUE\"";
        }

        if (linkToItemAllowed != null && linkToItemAllowed.length() > 0)
        {
            stringValue += " LinkToItemAllowed=\"" + Util.encodeJson(linkToItemAllowed) + "\"";
        }

        if (list != null && list.length() > 0)
        {
            stringValue += " List=\"" + Util.encodeJson(list) + "\"";
        }

        if (isListItemMenu)
        {
            stringValue += " ListItemMenu=\"TRUE\"";
        }

        if (listItemMenuAllowed != null && listItemMenuAllowed.length() > 0)
        {
            stringValue += " ListItemMenuAllowed=\"" + Util.encodeJson(listItemMenuAllowed) + "\"";
        }

        if (max > Double.MIN_VALUE)
        {
            stringValue += " Max=\"" + max + "\"";
        }

        if (maxLength > -1)
        {
            stringValue += " MaxLength =\"" + maxLength + "\"";
        }

        if (min > Double.MIN_VALUE)
        {
            stringValue += " Min=\"" + min + "\"";
        }

        if (isMultiplied)
        {
            stringValue += " Mult=\"TRUE\"";
        }

        if (name != null && name.length() > 0)
        {
            stringValue += " Name=\"" + Util.encodeJson(name) + "\"";
        }

        if (negativeFormat != null && negativeFormat.length() > 0)
        {
            stringValue += " NegativeFormat=\"" + Util.encodeJson(negativeFormat) + "\"";
        }

        if (node != null && node.length() > 0)
        {
            stringValue += " Node=\"" + Util.encodeJson(node) + "\"";
        }

        if (isNoEditFormBreak)
        {
            stringValue += " NoEditFormBreak=\"TRUE\"";
        }

        if (numberOfLines > -1)
        {
            stringValue += " NumLines=\"" + numberOfLines + "\"";
        }

        if (isPercentage)
        {
            stringValue += " Percentage=\"TRUE\"";
        }

        if (piAttribute != null && piAttribute.length() > 0)
        {
            stringValue += " PIAttribute=\"" + Util.encodeJson(piAttribute) + "\"";
        }

        if (piTarget != null && piTarget.length() > 0)
        {
            stringValue += " PITarget=\"" + Util.encodeJson(piTarget) + "\"";
        }

        if (isPrependId)
        {
            stringValue += " PrependId=\"TRUE\"";
        }

        if (isPresence)
        {
            stringValue += " Presence=\"TRUE\"";
        }

        if (isPrimaryKey)
        {
            stringValue += " PrimaryKey=\"TRUE\"";
        }

        if (primaryPIAttribute != null && primaryPIAttribute.length() > 0)
        {
            stringValue += " PrimaryPIAttribute=\"" + Util.encodeJson(primaryPIAttribute) + "\"";
        }

        if (primaryPITarget != null && primaryPITarget.length() > 0)
        {
            stringValue += " PrimaryPITarget=\"" + Util.encodeJson(primaryPITarget) + "\"";
        }

        if (isReadOnly)
        {
            stringValue += " ReadOnly=\"TRUE\"";
        }

        if (isReadOnlyEnforced)
        {
            stringValue += " ReadOnlyEnforced=\"TRUE\"";
        }

        if (relationshipDeleteBehavior != null && relationshipDeleteBehavior.length() > 0)
        {
            stringValue += " RelationshipDeleteBehavior=\"" + Util.encodeJson(relationshipDeleteBehavior) + "\"";
        }

        if (isRenderXmlUsingPattern)
        {
            stringValue += " RenderXMLUsingPattern=\"TRUE\"";
        }

        if (isRequired)
        {
            stringValue += " Required=\"TRUE\"";
        }

        if (isRestrictedMode)
        {
            stringValue += " RestrictedMode=\"TRUE\"";
        }

        if (isRichText)
        {
            stringValue += " RichText=\"TRUE\"";
        }

        if (richTextMode != null && richTextMode.length() > 0)
        {
            stringValue += " RichTextMode=\"" + Util.encodeJson(richTextMode) + "\"";
        }

        if (rowOrdinal > -1)
        {
            stringValue += " RowOrdinal=\"" + rowOrdinal + "\"";
        }

        if (isSealed)
        {
            stringValue += " Sealed=\"TRUE\"";
        }

        if (isSeparateLine)
        {
            stringValue += " SeparateLine=\"TRUE\"";
        }

        if (setAs != null && setAs.length() > 0)
        {
            stringValue += " SetAs=\"" + Util.encodeJson(setAs) + "\"";
        }

        if (showAddressBookButton)
        {
            stringValue += " ShowAddressBookButton=\"TRUE\"";
        }

        if (showAlways)
        {
            stringValue += " ShowAlways=\"TRUE\"";
        }

        if (showField != null && showField.length() > 0)
        {
            stringValue += " ShowField=\"" + Util.encodeJson(showField) + "\"";
        }

        if (showInDisplayForm)
        {
            stringValue += " ShowInDisplayForm=\"TRUE\"";
        }

        if (!showInEditForm)
        {
            stringValue += " ShowInEditForm=\"FALSE\"";
        }

        if (showInFileDialog)
        {
            stringValue += " ShowInFileDlg=\"TRUE\"";
        }

        if (showInListSettings)
        {
            stringValue += " ShowInListSettings=\"TRUE\"";
        }

        if (!showInNewForm)
        {
            stringValue += " ShowInNewForm=\"FALSE\"";
        }

        if (showInVersionHistory)
        {
            stringValue += " ShowInVersionHistory=\"TRUE\"";
        }

        if (showInViewForms)
        {
            stringValue += " ShowInViewForms=\"TRUE\"";
        }

        if (isSortable)
        {
            stringValue += " Sortable=\"TRUE\"";
        }

        if (sourceId != null && sourceId.length() > 0)
        {
            stringValue += " SourceID=\"" + Util.encodeJson(sourceId) + "\"";
        }

        if (staticName != null && staticName.length() > 0)
        {
            stringValue += " StaticName=\"" + Util.encodeJson(staticName) + "\"";
        }

        if (storageTimeZone != null && storageTimeZone.length() > 0)
        {
            stringValue += " StorageTZ=\"" + Util.encodeJson(storageTimeZone) + "\"";
        }

        if (isStripWS)
        {
            stringValue += " StripWS=\"TRUE\"";
        }

        if (isSuppressNameDisplay)
        {
            stringValue += " SuppressNameDisplay=\"TRUE\"";
        }

        if (isTextOnly)
        {
            stringValue += " TextOnly=\"TRUE\"";
        }

        if (title != null && title.length() > 0)
        {
            stringValue += " Title=\"" + Util.encodeJson(title) + "\"";
        }

        if (type != null && type.length() > 0)
        {
            stringValue += " Type=\"" + Util.encodeJson(type) + "\"";
        }

        if (uniqueId != null && uniqueId.length() > 0)
        {
            stringValue += " UniqueId=\"" + Util.encodeJson(uniqueId) + "\"";
        }

        if (isUnlimitedLengthInDocumentLibrary)
        {
            stringValue += " UnlimitedLengthInDocumentLibrary=\"TRUE\"";
        }

        if (isUrlEncode)
        {
            stringValue += " URLEncode=\"TRUE\"";
        }

        if (isUrlEncodeAsUrl)
        {
            stringValue += " URLEncodeAsUrl=\"TRUE\"";
        }

        if (userSelectionMode != null && userSelectionMode.length() > 0)
        {
            stringValue += " UserSelectionMode=\"" + Util.encodeJson(userSelectionMode) + "\"";
        }

        if (userSelectionScope > -1)
        {
            stringValue += " UserSelectionScope=\"" + userSelectionScope + "\"";
        }

        if (version != null && version.length() > 0)
        {
            stringValue += " Version=\"" + Util.encodeJson(version) + "\"";
        }

        if (isViewable)
        {
            stringValue += " Viewable=\"TRUE\"";
        }

        if (webId != null && webId.length() > 0)
        {
            stringValue += " WebId=\"" + Util.encodeJson(webId) + "\"";
        }

        if (width > -1)
        {
            stringValue += " Width=\"" + width + "\"";
        }

        if (isWikiLinking)
        {
            stringValue += " WikiLinking=\"TRUE\"";
        }

        if (xName != null && xName.length() > 0)
        {
            stringValue += " XName=\"" + Util.encodeJson(xName) + "\"";
        }

        stringValue += " />";

        return stringValue;
    }

    /**
     * Gets the aggregation.
     *
     * @return the aggregation
     */
    public FieldAggregation getAggregation()
    {
        return aggregation;
    }

    /**
     * Sets the aggregation.
     *
     * @param aggregation the new aggregation
     */
    public void setAggregation(FieldAggregation aggregation)
    {
        this.aggregation = aggregation;
    }

    /**
     * Checks if is deletion allowed.
     *
     * @return true, if is deletion allowed
     */
    public boolean isDeletionAllowed()
    {
        return allowDeletion;
    }

    /**
     * Allow deletion.
     *
     * @param allowDeletion the allow deletion
     */
    public void allowDeletion(boolean allowDeletion)
    {
        this.allowDeletion = allowDeletion;
    }

    /**
     * Checks if is hyperlink allowed.
     *
     * @return true, if is hyperlink allowed
     */
    public boolean isHyperlinkAllowed()
    {
        return allowHyperlink;
    }

    /**
     * Allow hyperlink.
     *
     * @param allowHyperlink the allow hyperlink
     */
    public void allowHyperlink(boolean allowHyperlink)
    {
        this.allowHyperlink = allowHyperlink;
    }

    /**
     * Checks if is multi vote allowed.
     *
     * @return true, if is multi vote allowed
     */
    public boolean isMultiVoteAllowed()
    {
        return allowMultiVote;
    }

    /**
     * Allow multi vote.
     *
     * @param allowMultiVote the allow multi vote
     */
    public void allowMultiVote(boolean allowMultiVote)
    {
        this.allowMultiVote = allowMultiVote;
    }

    /**
     * Checks if is append only.
     *
     * @return true, if is append only
     */
    public boolean isAppendOnly()
    {
        return isAppendOnly;
    }

    /**
     * Sets the append only.
     *
     * @param isAppendOnly the new append only
     */
    public void setAppendOnly(boolean isAppendOnly)
    {
        this.isAppendOnly = isAppendOnly;
    }

    /**
     * Gets the authoring info.
     *
     * @return the authoring info
     */
    public String getAuthoringInfo()
    {
        return authoringInfo;
    }

    /**
     * Sets the authoring info.
     *
     * @param authoringInfo the new authoring info
     */
    public void setAuthoringInfo(String authoringInfo)
    {
        this.authoringInfo = authoringInfo;
    }

    /**
     * Gets the base type.
     *
     * @return the base type
     */
    public FieldType getBaseType()
    {
        return baseType;
    }

    /**
     * Sets the base type.
     *
     * @param baseType the new base type
     */
    public void setBaseType(FieldType baseType)
    {
        this.baseType = baseType;
    }

    /**
     * Gets the calendar type.
     *
     * @return the calendar type
     */
    public int getCalendarType()
    {
        return calendarType;
    }

    /**
     * Sets the calendar type.
     *
     * @param calendarType the new calendar type
     */
    public void setCalendarType(int calendarType)
    {
        this.calendarType = calendarType;
    }

    /**
     * Can toggle hidden.
     *
     * @return true, if successful
     */
    public boolean canToggleHidden()
    {
        return canToggleHidden;
    }

    /**
     * Sets the can toggle hidden.
     *
     * @param canToggleHidden the new can toggle hidden
     */
    public void setCanToggleHidden(boolean canToggleHidden)
    {
        this.canToggleHidden = canToggleHidden;
    }

    /**
     * Gets the class info.
     *
     * @return the class info
     */
    public String getClassInfo()
    {
        return classInfo;
    }

    /**
     * Sets the class info.
     *
     * @param classInfo the new class info
     */
    public void setClassInfo(String classInfo)
    {
        this.classInfo = classInfo;
    }

    /**
     * Gets the column name.
     *
     * @return the column name
     */
    public String getColumnName()
    {
        return columnName;
    }

    /**
     * Sets the column name.
     *
     * @param columnName the new column name
     */
    public void setColumnName(String columnName)
    {
        this.columnName = columnName;
    }

    /**
     * Gets the column name2.
     *
     * @return the column name2
     */
    public String getColumnName2()
    {
        return columnName2;
    }

    /**
     * Sets the column name2.
     *
     * @param columnName2 the new column name2
     */
    public void setColumnName2(String columnName2)
    {
        this.columnName2 = columnName2;
    }

    /**
     * Checks for commas.
     *
     * @return true, if successful
     */
    public boolean hasCommas()
    {
        return hasCommas;
    }

    /**
     * Sets the checks for commas.
     *
     * @param hasCommas the new checks for commas
     */
    public void setHasCommas(boolean hasCommas)
    {
        this.hasCommas = hasCommas;
    }

    /**
     * Gets the customization.
     *
     * @return the customization
     */
    public String getCustomization()
    {
        return customization;
    }

    /**
     * Sets the customization.
     *
     * @param customization the new customization
     */
    public void setCustomization(String customization)
    {
        this.customization = customization;
    }

    /**
     * Gets the decimals.
     *
     * @return the decimals
     */
    public int getDecimals()
    {
        return decimals;
    }

    /**
     * Sets the decimals.
     *
     * @param decimals the new decimals
     */
    public void setDecimals(int decimals)
    {
        this.decimals = decimals;
    }

    /**
     * Gets the description.
     *
     * @return the description
     */
    public String getDescription()
    {
        return description;
    }

    /**
     * Sets the description.
     *
     * @param description the new description
     */
    public void setDescription(String description)
    {
        this.description = description;
    }

    /**
     * Gets the dir.
     *
     * @return the dir
     */
    public String getDir()
    {
        return dir;
    }

    /**
     * Sets the dir.
     *
     * @param dir the new dir
     */
    public void setDir(String dir)
    {
        this.dir = dir;
    }

    /**
     * Gets the direction.
     *
     * @return the direction
     */
    public Direction getDirection()
    {
        return direction;
    }

    /**
     * Sets the direction.
     *
     * @param direction the new direction
     */
    public void setDirection(Direction direction)
    {
        this.direction = direction;
    }

    /**
     * Checks if is displace on upgrade.
     *
     * @return true, if is displace on upgrade
     */
    public boolean isDisplaceOnUpgrade()
    {
        return isDisplaceOnUpgrade;
    }

    /**
     * Sets the displace on upgrade.
     *
     * @param isDisplaceOnUpgrade the new displace on upgrade
     */
    public void setDisplaceOnUpgrade(boolean isDisplaceOnUpgrade)
    {
        this.isDisplaceOnUpgrade = isDisplaceOnUpgrade;
    }

    /**
     * Gets the display image.
     *
     * @return the display image
     */
    public String getDisplayImage()
    {
        return displayImage;
    }

    /**
     * Sets the display image.
     *
     * @param displayImage the new display image
     */
    public void setDisplayImage(String displayImage)
    {
        this.displayImage = displayImage;
    }

    /**
     * Gets the display name.
     *
     * @return the display name
     */
    public String getDisplayName()
    {
        return displayName;
    }

    /**
     * Sets the display name.
     *
     * @param displayName the new display name
     */
    public void setDisplayName(String displayName)
    {
        this.displayName = displayName;
    }

    /**
     * Gets the display name source field.
     *
     * @return the display name source field
     */
    public String getDisplayNameSourceField()
    {
        return displayNameSourceField;
    }

    /**
     * Sets the display name source field.
     *
     * @param displayNameSourceField the new display name source field
     */
    public void setDisplayNameSourceField(String displayNameSourceField)
    {
        this.displayNameSourceField = displayNameSourceField;
    }

    /**
     * Gets the display size.
     *
     * @return the display size
     */
    public int getDisplaySize()
    {
        return displaySize;
    }

    /**
     * Sets the display size.
     *
     * @param displaySize the new display size
     */
    public void setDisplaySize(int displaySize)
    {
        this.displaySize = displaySize;
    }

    /**
     * Gets the div.
     *
     * @return the div
     */
    public String getDiv()
    {
        return div;
    }

    /**
     * Sets the div.
     *
     * @param div the new div
     */
    public void setDiv(String div)
    {
        this.div = div;
    }

    /**
     * Checks if is lookup enabled.
     *
     * @return true, if is lookup enabled
     */
    public boolean isLookupEnabled()
    {
        return enableLookup;
    }

    /**
     * Enable lookup.
     *
     * @param enableLookup the enable lookup
     */
    public void enableLookup(boolean enableLookup)
    {
        this.enableLookup = enableLookup;
    }

    /**
     * Checks if is enforce unique values.
     *
     * @return true, if is enforce unique values
     */
    public boolean isEnforceUniqueValues()
    {
        return enforceUniqueValues;
    }

    /**
     * Enforce unique values.
     *
     * @param enforceUniqueValues the enforce unique values
     */
    public void enforceUniqueValues(boolean enforceUniqueValues)
    {
        this.enforceUniqueValues = enforceUniqueValues;
    }

    /**
     * Gets the exception image.
     *
     * @return the exception image
     */
    public String getExceptionImage()
    {
        return exceptionImage;
    }

    /**
     * Sets the exception image.
     *
     * @param exceptionImage the new exception image
     */
    public void setExceptionImage(String exceptionImage)
    {
        this.exceptionImage = exceptionImage;
    }

    /**
     * Gets the field ref.
     *
     * @return the field ref
     */
    public String getFieldRef()
    {
        return fieldRef;
    }

    /**
     * Sets the field ref.
     *
     * @param fieldRef the new field ref
     */
    public void setFieldRef(String fieldRef)
    {
        this.fieldRef = fieldRef;
    }

    /**
     * Checks if is fill in choice.
     *
     * @return true, if is fill in choice
     */
    public boolean isFillInChoice()
    {
        return isFillInChoice;
    }

    /**
     * Sets the fill in choice.
     *
     * @param isFillInChoice the new fill in choice
     */
    public void setFillInChoice(boolean isFillInChoice)
    {
        this.isFillInChoice = isFillInChoice;
    }

    /**
     * Checks if is filterable.
     *
     * @return true, if is filterable
     */
    public boolean isFilterable()
    {
        return isFilterable;
    }

    /**
     * Sets the filterable.
     *
     * @param isFilterable the new filterable
     */
    public void setFilterable(boolean isFilterable)
    {
        this.isFilterable = isFilterable;
    }

    /**
     * Checks if is filterable no recurrence.
     *
     * @return true, if is filterable no recurrence
     */
    public boolean isFilterableNoRecurrence()
    {
        return isFilterableNoRecurrence;
    }

    /**
     * Sets the filterable no recurrence.
     *
     * @param isFilterableNoRecurrence the new filterable no recurrence
     */
    public void setFilterableNoRecurrence(boolean isFilterableNoRecurrence)
    {
        this.isFilterableNoRecurrence = isFilterableNoRecurrence;
    }

    /**
     * Gets the forced display.
     *
     * @return the forced display
     */
    public String getForcedDisplay()
    {
        return forcedDisplay;
    }

    /**
     * Sets the forced display.
     *
     * @param forcedDisplay the new forced display
     */
    public void setForcedDisplay(String forcedDisplay)
    {
        this.forcedDisplay = forcedDisplay;
    }

    /**
     * Checks if is force promote demote.
     *
     * @return true, if is force promote demote
     */
    public boolean isForcePromoteDemote()
    {
        return isForcePromoteDemote;
    }

    /**
     * Sets the force promote demote.
     *
     * @param isForcePromoteDemote the new force promote demote
     */
    public void setForcePromoteDemote(boolean isForcePromoteDemote)
    {
        this.isForcePromoteDemote = isForcePromoteDemote;
    }

    /**
     * Gets the format.
     *
     * @return the format
     */
    public String getFormat()
    {
        return format;
    }

    /**
     * Sets the format.
     *
     * @param format the new format
     */
    public void setFormat(String format)
    {
        this.format = format;
    }

    /**
     * Checks if is from base type.
     *
     * @return true, if is from base type
     */
    public boolean isFromBaseType()
    {
        return isFromBaseType;
    }

    /**
     * Sets the from base type.
     *
     * @param isFromBaseType the new from base type
     */
    public void setFromBaseType(boolean isFromBaseType)
    {
        this.isFromBaseType = isFromBaseType;
    }

    /**
     * Gets the group.
     *
     * @return the group
     */
    public String getGroup()
    {
        return group;
    }

    /**
     * Sets the group.
     *
     * @param group the new group
     */
    public void setGroup(String group)
    {
        this.group = group;
    }

    /**
     * Gets the header image.
     *
     * @return the header image
     */
    public String getHeaderImage()
    {
        return headerImage;
    }

    /**
     * Sets the header image.
     *
     * @param headerImage the new header image
     */
    public void setHeaderImage(String headerImage)
    {
        this.headerImage = headerImage;
    }

    /**
     * Gets the height.
     *
     * @return the height
     */
    public int getHeight()
    {
        return height;
    }

    /**
     * Sets the height.
     *
     * @param height the new height
     */
    public void setHeight(int height)
    {
        this.height = height;
    }

    /**
     * Checks if is hidden.
     *
     * @return true, if is hidden
     */
    public boolean isHidden()
    {
        return isHidden;
    }

    /**
     * Sets the hidden.
     *
     * @param isHidden the new hidden
     */
    public void setHidden(boolean isHidden)
    {
        this.isHidden = isHidden;
    }

    /**
     * Checks if is html encode.
     *
     * @return true, if is html encode
     */
    public boolean isHtmlEncode()
    {
        return isHtmlEncode;
    }

    /**
     * Sets the html encode.
     *
     * @param isHtmlEncode the new html encode
     */
    public void setHtmlEncode(boolean isHtmlEncode)
    {
        this.isHtmlEncode = isHtmlEncode;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public String getId()
    {
        return id;
    }

    /**
     * Gets the ime mode.
     *
     * @return the ime mode
     */
    public ImeMode getImeMode()
    {
        return imeMode;
    }

    /**
     * Sets the ime mode.
     *
     * @param imeMode the new ime mode
     */
    public void setImeMode(ImeMode imeMode)
    {
        this.imeMode = imeMode;
    }

    /**
     * Checks if is indexed.
     *
     * @return true, if is indexed
     */
    public boolean isIndexed()
    {
        return isIndexed;
    }

    /**
     * Sets the indexed.
     *
     * @param isIndexed the new indexed
     */
    public void setIndexed(boolean isIndexed)
    {
        this.isIndexed = isIndexed;
    }

    /**
     * Checks if is isolate styles.
     *
     * @return true, if is isolate styles
     */
    public boolean isIsolateStyles()
    {
        return isIsolateStyles;
    }

    /**
     * Sets the isolate styles.
     *
     * @param isIsolateStyles the new isolate styles
     */
    public void setIsolateStyles(boolean isIsolateStyles)
    {
        this.isIsolateStyles = isIsolateStyles;
    }

    /**
     * Checks if is relationship.
     *
     * @return true, if is relationship
     */
    public boolean isRelationship()
    {
        return isRelationship;
    }

    /**
     * Sets the relationship.
     *
     * @param isRelationship the new relationship
     */
    public void setRelationship(boolean isRelationship)
    {
        this.isRelationship = isRelationship;
    }

    /**
     * Gets the join column name.
     *
     * @return the join column name
     */
    public String getJoinColumnName()
    {
        return joinColumnName;
    }

    /**
     * Sets the join column name.
     *
     * @param joinColumnName the new join column name
     */
    public void setJoinColumnName(String joinColumnName)
    {
        this.joinColumnName = joinColumnName;
    }

    /**
     * Gets the join row ordinal.
     *
     * @return the join row ordinal
     */
    public int getJoinRowOrdinal()
    {
        return joinRowOrdinal;
    }

    /**
     * Sets the join row ordinal.
     *
     * @param joinRowOrdinal the new join row ordinal
     */
    public void setJoinRowOrdinal(int joinRowOrdinal)
    {
        this.joinRowOrdinal = joinRowOrdinal;
    }

    /**
     * Gets the join type.
     *
     * @return the join type
     */
    public JoinType getJoinType()
    {
        return joinType;
    }

    /**
     * Sets the join type.
     *
     * @param joinType the new join type
     */
    public void setJoinType(JoinType joinType)
    {
        this.joinType = joinType;
    }

    /**
     * Gets the locale.
     *
     * @return the locale
     */
    public Locale getLocale()
    {
        return locale;
    }

    /**
     * Sets the locale.
     *
     * @param locale the new locale
     */
    public void setLocale(Locale locale)
    {
        this.locale = locale;
    }

    /**
     * Checks if is link to item.
     *
     * @return true, if is link to item
     */
    public boolean isLinkToItem()
    {
        return isLinkToItem;
    }

    /**
     * Sets the link to item.
     *
     * @param isLinkToItem the new link to item
     */
    public void setLinkToItem(boolean isLinkToItem)
    {
        this.isLinkToItem = isLinkToItem;
    }

    /**
     * Gets the link to item allowed.
     *
     * @return the link to item allowed
     */
    public String getLinkToItemAllowed()
    {
        return linkToItemAllowed;
    }

    /**
     * Sets the link to item allowed.
     *
     * @param linkToItemAllowed the new link to item allowed
     */
    public void setLinkToItemAllowed(String linkToItemAllowed)
    {
        this.linkToItemAllowed = linkToItemAllowed;
    }

    /**
     * Gets the list.
     *
     * @return the list
     */
    public String getList()
    {
        return list;
    }

    /**
     * Sets the list.
     *
     * @param list the new list
     */
    public void setList(String list)
    {
        this.list = list;
    }

    /**
     * Checks if is list item menu.
     *
     * @return true, if is list item menu
     */
    public boolean isListItemMenu()
    {
        return isListItemMenu;
    }

    /**
     * Sets the list item menu.
     *
     * @param isListItemMenu the new list item menu
     */
    public void setListItemMenu(boolean isListItemMenu)
    {
        this.isListItemMenu = isListItemMenu;
    }

    /**
     * Gets the list item menu allowed.
     *
     * @return the list item menu allowed
     */
    public String getListItemMenuAllowed()
    {
        return listItemMenuAllowed;
    }

    /**
     * Sets the list item menu allowed.
     *
     * @param listItemMenuAllowed the new list item menu allowed
     */
    public void setListItemMenuAllowed(String listItemMenuAllowed)
    {
        this.listItemMenuAllowed = listItemMenuAllowed;
    }

    /**
     * Gets the max.
     *
     * @return the max
     */
    public double getMax()
    {
        return max;
    }

    /**
     * Sets the max.
     *
     * @param max the new max
     */
    public void setMax(double max)
    {
        this.max = max;
    }

    /**
     * Gets the max length.
     *
     * @return the max length
     */
    public int getMaxLength()
    {
        return maxLength;
    }

    /**
     * Sets the max length.
     *
     * @param maxLength the new max length
     */
    public void setMaxLength(int maxLength)
    {
        this.maxLength = maxLength;
    }

    /**
     * Gets the min.
     *
     * @return the min
     */
    public double getMin()
    {
        return min;
    }

    /**
     * Sets the min.
     *
     * @param min the new min
     */
    public void setMin(double min)
    {
        this.min = min;
    }

    /**
     * Checks if is multiplied.
     *
     * @return true, if is multiplied
     */
    public boolean isMultiplied()
    {
        return isMultiplied;
    }

    /**
     * Sets the multiplied.
     *
     * @param isMultiplied the new multiplied
     */
    public void setMultiplied(boolean isMultiplied)
    {
        this.isMultiplied = isMultiplied;
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName()
    {
        return name;
    }

    /**
     * Sets the name.
     *
     * @param name the new name
     */
    public void setName(String name)
    {
        this.name = name;
    }

    /**
     * Gets the negative format.
     *
     * @return the negative format
     */
    public String getNegativeFormat()
    {
        return negativeFormat;
    }

    /**
     * Sets the negative format.
     *
     * @param negativeFormat the new negative format
     */
    public void setNegativeFormat(String negativeFormat)
    {
        this.negativeFormat = negativeFormat;
    }

    /**
     * Gets the node.
     *
     * @return the node
     */
    public String getNode()
    {
        return node;
    }

    /**
     * Sets the node.
     *
     * @param node the new node
     */
    public void setNode(String node)
    {
        this.node = node;
    }

    /**
     * Checks if is no edit form break.
     *
     * @return true, if is no edit form break
     */
    public boolean isNoEditFormBreak()
    {
        return isNoEditFormBreak;
    }

    /**
     * Sets the no edit form break.
     *
     * @param isNoEditFormBreak the new no edit form break
     */
    public void setNoEditFormBreak(boolean isNoEditFormBreak)
    {
        this.isNoEditFormBreak = isNoEditFormBreak;
    }

    /**
     * Gets the number of lines.
     *
     * @return the number of lines
     */
    public int getNumberOfLines()
    {
        return numberOfLines;
    }

    /**
     * Sets the number of lines.
     *
     * @param numberOfLines the new number of lines
     */
    public void setNumberOfLines(int numberOfLines)
    {
        this.numberOfLines = numberOfLines;
    }

    /**
     * Checks if is percentage.
     *
     * @return true, if is percentage
     */
    public boolean isPercentage()
    {
        return isPercentage;
    }

    /**
     * Sets the percentage.
     *
     * @param isPercentage the new percentage
     */
    public void setPercentage(boolean isPercentage)
    {
        this.isPercentage = isPercentage;
    }

    /**
     * Gets the pi attribute.
     *
     * @return the pi attribute
     */
    public String getPiAttribute()
    {
        return piAttribute;
    }

    /**
     * Sets the pi attribute.
     *
     * @param piAttribute the new pi attribute
     */
    public void setPiAttribute(String piAttribute)
    {
        this.piAttribute = piAttribute;
    }

    /**
     * Gets the pi target.
     *
     * @return the pi target
     */
    public String getPiTarget()
    {
        return piTarget;
    }

    /**
     * Sets the pi target.
     *
     * @param piTarget the new pi target
     */
    public void setPiTarget(String piTarget)
    {
        this.piTarget = piTarget;
    }

    /**
     * Checks if is prepend id.
     *
     * @return true, if is prepend id
     */
    public boolean isPrependId()
    {
        return isPrependId;
    }

    /**
     * Sets the prepend id.
     *
     * @param isPrependId the new prepend id
     */
    public void setPrependId(boolean isPrependId)
    {
        this.isPrependId = isPrependId;
    }

    /**
     * Checks if is presence.
     *
     * @return true, if is presence
     */
    public boolean isPresence()
    {
        return isPresence;
    }

    /**
     * Sets the presence.
     *
     * @param isPresence the new presence
     */
    public void setPresence(boolean isPresence)
    {
        this.isPresence = isPresence;
    }

    /**
     * Checks if is primary key.
     *
     * @return true, if is primary key
     */
    public boolean isPrimaryKey()
    {
        return isPrimaryKey;
    }

    /**
     * Sets the primary key.
     *
     * @param isPrimaryKey the new primary key
     */
    public void setPrimaryKey(boolean isPrimaryKey)
    {
        this.isPrimaryKey = isPrimaryKey;
    }

    /**
     * Gets the primary pi attribute.
     *
     * @return the primary pi attribute
     */
    public String getPrimaryPIAttribute()
    {
        return primaryPIAttribute;
    }

    /**
     * Sets the primary pi attribute.
     *
     * @param primaryPIAttribute the new primary pi attribute
     */
    public void setPrimaryPIAttribute(String primaryPIAttribute)
    {
        this.primaryPIAttribute = primaryPIAttribute;
    }

    /**
     * Gets the primary pi target.
     *
     * @return the primary pi target
     */
    public String getPrimaryPITarget()
    {
        return primaryPITarget;
    }

    /**
     * Sets the primary pi target.
     *
     * @param primaryPITarget the new primary pi target
     */
    public void setPrimaryPITarget(String primaryPITarget)
    {
        this.primaryPITarget = primaryPITarget;
    }

    /**
     * Checks if is read only.
     *
     * @return true, if is read only
     */
    public boolean isReadOnly()
    {
        return isReadOnly;
    }

    /**
     * Sets the read only.
     *
     * @param isReadOnly the new read only
     */
    public void setReadOnly(boolean isReadOnly)
    {
        this.isReadOnly = isReadOnly;
    }

    /**
     * Checks if is read only enforced.
     *
     * @return true, if is read only enforced
     */
    public boolean isReadOnlyEnforced()
    {
        return isReadOnlyEnforced;
    }

    /**
     * Sets the read only enforced.
     *
     * @param isReadOnlyEnforced the new read only enforced
     */
    public void setReadOnlyEnforced(boolean isReadOnlyEnforced)
    {
        this.isReadOnlyEnforced = isReadOnlyEnforced;
    }

    /**
     * Gets the relationship delete behavior.
     *
     * @return the relationship delete behavior
     */
    public String getRelationshipDeleteBehavior()
    {
        return relationshipDeleteBehavior;
    }

    /**
     * Sets the relationship delete behavior.
     *
     * @param relationshipDeleteBehavior the new relationship delete behavior
     */
    public void setRelationshipDeleteBehavior(String relationshipDeleteBehavior)
    {
        this.relationshipDeleteBehavior = relationshipDeleteBehavior;
    }

    /**
     * Checks if is render xml using pattern.
     *
     * @return true, if is render xml using pattern
     */
    public boolean isRenderXmlUsingPattern()
    {
        return isRenderXmlUsingPattern;
    }

    /**
     * Sets the render xml using pattern.
     *
     * @param isRenderXmlUsingPattern the new render xml using pattern
     */
    public void setRenderXmlUsingPattern(boolean isRenderXmlUsingPattern)
    {
        this.isRenderXmlUsingPattern = isRenderXmlUsingPattern;
    }

    /**
     * Checks if is required.
     *
     * @return true, if is required
     */
    public boolean isRequired()
    {
        return isRequired;
    }

    /**
     * Sets the required.
     *
     * @param isRequired the new required
     */
    public void setRequired(boolean isRequired)
    {
        this.isRequired = isRequired;
    }

    /**
     * Checks if is restricted mode.
     *
     * @return true, if is restricted mode
     */
    public boolean isRestrictedMode()
    {
        return isRestrictedMode;
    }

    /**
     * Sets the restricted mode.
     *
     * @param isRestrictedMode the new restricted mode
     */
    public void setRestrictedMode(boolean isRestrictedMode)
    {
        this.isRestrictedMode = isRestrictedMode;
    }

    /**
     * Gets the result type.
     *
     * @return the result type
     */
    public String getResultType()
    {
        return resultType;
    }

    /**
     * Sets the result type.
     *
     * @param resultType the new result type
     */
    public void setResultType(String resultType)
    {
        this.resultType = resultType;
    }

    /**
     * Checks if is rich text.
     *
     * @return true, if is rich text
     */
    public boolean isRichText()
    {
        return isRichText;
    }

    /**
     * Sets the rich text.
     *
     * @param isRichText the new rich text
     */
    public void setRichText(boolean isRichText)
    {
        this.isRichText = isRichText;
    }

    /**
     * Gets the rich text mode.
     *
     * @return the rich text mode
     */
    public String getRichTextMode()
    {
        return richTextMode;
    }

    /**
     * Sets the rich text mode.
     *
     * @param richTextMode the new rich text mode
     */
    public void setRichTextMode(String richTextMode)
    {
        this.richTextMode = richTextMode;
    }

    /**
     * Gets the row ordinal.
     *
     * @return the row ordinal
     */
    public int getRowOrdinal()
    {
        return rowOrdinal;
    }

    /**
     * Sets the row ordinal.
     *
     * @param rowOrdinal the new row ordinal
     */
    public void setRowOrdinal(int rowOrdinal)
    {
        this.rowOrdinal = rowOrdinal;
    }

    /**
     * Checks if is sealed.
     *
     * @return true, if is sealed
     */
    public boolean isSealed()
    {
        return isSealed;
    }

    /**
     * Sets the sealed.
     *
     * @param isSealed the new sealed
     */
    public void setSealed(boolean isSealed)
    {
        this.isSealed = isSealed;
    }

    /**
     * Checks if is separate line.
     *
     * @return true, if is separate line
     */
    public boolean isSeparateLine()
    {
        return isSeparateLine;
    }

    /**
     * Sets the separate line.
     *
     * @param isSeparateLine the new separate line
     */
    public void setSeparateLine(boolean isSeparateLine)
    {
        this.isSeparateLine = isSeparateLine;
    }

    /**
     * Gets the sets the as.
     *
     * @return the sets the as
     */
    public String getSetAs()
    {
        return setAs;
    }

    /**
     * Sets the sets the as.
     *
     * @param setAs the new sets the as
     */
    public void setSetAs(String setAs)
    {
        this.setAs = setAs;
    }

    /**
     * Show address book button.
     *
     * @return true, if successful
     */
    public boolean showAddressBookButton()
    {
        return showAddressBookButton;
    }

    /**
     * Sets the show address book button.
     *
     * @param showAddressBookButton the new show address book button
     */
    public void setShowAddressBookButton(boolean showAddressBookButton)
    {
        this.showAddressBookButton = showAddressBookButton;
    }

    /**
     * Show always.
     *
     * @return true, if successful
     */
    public boolean showAlways()
    {
        return showAlways;
    }

    /**
     * Sets the show always.
     *
     * @param showAlways the new show always
     */
    public void setShowAlways(boolean showAlways)
    {
        this.showAlways = showAlways;
    }

    /**
     * Gets the show field.
     *
     * @return the show field
     */
    public String getShowField()
    {
        return setAs;
    }

    /**
     * Sets the show field.
     *
     * @param setAs the new show field
     */
    public void setShowField(String setAs)
    {
        this.setAs = setAs;
    }

    /**
     * Show in display form.
     *
     * @return true, if successful
     */
    public boolean showInDisplayForm()
    {
        return showInDisplayForm;
    }

    /**
     * Sets the show in display form.
     *
     * @param showInDisplayForm the new show in display form
     */
    public void setShowInDisplayForm(boolean showInDisplayForm)
    {
        this.showInDisplayForm = showInDisplayForm;
    }

    /**
     * Show in edit form.
     *
     * @return true, if successful
     */
    public boolean showInEditForm()
    {
        return showInEditForm;
    }

    /**
     * Sets the show in edit form.
     *
     * @param showInEditForm the new show in edit form
     */
    public void setShowInEditForm(boolean showInEditForm)
    {
        this.showInEditForm = showInEditForm;
    }

    /**
     * Show in file dialog.
     *
     * @return true, if successful
     */
    public boolean showInFileDialog()
    {
        return showInFileDialog;
    }

    /**
     * Sets the show in file dialog.
     *
     * @param showInFileDialog the new show in file dialog
     */
    public void setShowInFileDialog(boolean showInFileDialog)
    {
        this.showInFileDialog = showInFileDialog;
    }

    /**
     * Show in list settings.
     *
     * @return true, if successful
     */
    public boolean showInListSettings()
    {
        return showInListSettings;
    }

    /**
     * Sets the show in list settings.
     *
     * @param showInListSettings the new show in list settings
     */
    public void setShowInListSettings(boolean showInListSettings)
    {
        this.showInListSettings = showInListSettings;
    }

    /**
     * Show in new form.
     *
     * @return true, if successful
     */
    public boolean showInNewForm()
    {
        return showInNewForm;
    }

    /**
     * Sets the show in new form.
     *
     * @param showInNewForm the new show in new form
     */
    public void setShowInNewForm(boolean showInNewForm)
    {
        this.showInNewForm = showInNewForm;
    }

    /**
     * Show in version history.
     *
     * @return true, if successful
     */
    public boolean showInVersionHistory()
    {
        return showInVersionHistory;
    }

    /**
     * Sets the show in version history.
     *
     * @param showInVersionHistory the new show in version history
     */
    public void setShowInVersionHistory(boolean showInVersionHistory)
    {
        this.showInVersionHistory = showInVersionHistory;
    }

    /**
     * Show in view forms.
     *
     * @return true, if successful
     */
    public boolean showInViewForms()
    {
        return showInViewForms;
    }

    /**
     * Sets the show in view forms.
     *
     * @param showInViewForms the new show in view forms
     */
    public void setShowInViewForms(boolean showInViewForms)
    {
        this.showInViewForms = showInViewForms;
    }

    /**
     * Checks if is sortable.
     *
     * @return true, if is sortable
     */
    public boolean isSortable()
    {
        return isSortable;
    }

    /**
     * Sets the sortable.
     *
     * @param isSortable the new sortable
     */
    public void setSortable(boolean isSortable)
    {
        this.isSortable = isSortable;
    }

    /**
     * Gets the source id.
     *
     * @return the source id
     */
    public String getSourceId()
    {
        return sourceId;
    }

    /**
     * Sets the source id.
     *
     * @param sourceId the new source id
     */
    public void setSourceId(String sourceId)
    {
        this.sourceId = sourceId;
    }

    /**
     * Gets the static name.
     *
     * @return the static name
     */
    public String getStaticName()
    {
        return staticName;
    }

    /**
     * Sets the static name.
     *
     * @param staticName the new static name
     */
    public void setStaticName(String staticName)
    {
        this.staticName = staticName;
    }

    /**
     * Gets the storage time zone.
     *
     * @return the storage time zone
     */
    public String getStorageTimeZone()
    {
        return storageTimeZone;
    }

    /**
     * Sets the storage time zone.
     *
     * @param storageTimeZone the new storage time zone
     */
    public void setStorageTimeZone(String storageTimeZone)
    {
        this.storageTimeZone = storageTimeZone;
    }

    /**
     * Checks if is strip ws.
     *
     * @return true, if is strip ws
     */
    public boolean isStripWS()
    {
        return isStripWS;
    }

    /**
     * Sets the strip ws.
     *
     * @param isStripWS the new strip ws
     */
    public void setStripWS(boolean isStripWS)
    {
        this.isStripWS = isStripWS;
    }

    /**
     * Checks if is suppress name display.
     *
     * @return true, if is suppress name display
     */
    public boolean isSuppressNameDisplay()
    {
        return isSuppressNameDisplay;
    }

    /**
     * Sets the suppress name display.
     *
     * @param isSuppressNameDisplay the new suppress name display
     */
    public void setSuppressNameDisplay(boolean isSuppressNameDisplay)
    {
        this.isSuppressNameDisplay = isSuppressNameDisplay;
    }

    /**
     * Checks if is text only.
     *
     * @return true, if is text only
     */
    public boolean isTextOnly()
    {
        return isTextOnly;
    }

    /**
     * Sets the text only.
     *
     * @param isTextOnly the new text only
     */
    public void setTextOnly(boolean isTextOnly)
    {
        this.isTextOnly = isTextOnly;
    }

    /**
     * Gets the title.
     *
     * @return the title
     */
    public String getTitle()
    {
        return title;
    }

    /**
     * Sets the title.
     *
     * @param title the new title
     */
    public void setTitle(String title)
    {
        this.title = title;
    }

    /**
     * Gets the type.
     *
     * @return the type
     */
    public String getType()
    {
        return type;
    }

    /**
     * Sets the type.
     *
     * @param type the new type
     */
    public void setType(String type)
    {
        this.type = type;
    }

    /**
     * Gets the unique id.
     *
     * @return the unique id
     */
    public String getUniqueId()
    {
        return uniqueId;
    }

    /**
     * Sets the unique id.
     *
     * @param uniqueId the new unique id
     */
    public void setUniqueId(String uniqueId)
    {
        this.uniqueId = uniqueId;
    }

    /**
     * Checks if is unlimited length in document library.
     *
     * @return true, if is unlimited length in document library
     */
    public boolean isUnlimitedLengthInDocumentLibrary()
    {
        return isUnlimitedLengthInDocumentLibrary;
    }

    /**
     * Sets the unlimited length in document library.
     *
     * @param isUnlimitedLengthInDocumentLibrary the new unlimited length in document library
     */
    public void setUnlimitedLengthInDocumentLibrary(boolean isUnlimitedLengthInDocumentLibrary)
    {
        this.isUnlimitedLengthInDocumentLibrary = isUnlimitedLengthInDocumentLibrary;
    }

    /**
     * Checks if is url encode.
     *
     * @return true, if is url encode
     */
    public boolean isUrlEncode()
    {
        return isUrlEncode;
    }

    /**
     * Sets the url encode.
     *
     * @param isUrlEncode the new url encode
     */
    public void setUrlEncode(boolean isUrlEncode)
    {
        this.isUrlEncode = isUrlEncode;
    }

    /**
     * Checks if is url encode as url.
     *
     * @return true, if is url encode as url
     */
    public boolean isUrlEncodeAsUrl()
    {
        return isUrlEncodeAsUrl;
    }

    /**
     * Sets the url encode as url.
     *
     * @param isUrlEncodeAsUrl the new url encode as url
     */
    public void setUrlEncodeAsUrl(boolean isUrlEncodeAsUrl)
    {
        this.isUrlEncodeAsUrl = isUrlEncodeAsUrl;
    }

    /**
     * Gets the user selection mode.
     *
     * @return the user selection mode
     */
    public String getUserSelectionMode()
    {
        return userSelectionMode;
    }

    /**
     * Sets the user selection mode.
     *
     * @param userSelectionMode the new user selection mode
     */
    public void setUserSelectionMode(String userSelectionMode)
    {
        this.userSelectionMode = userSelectionMode;
    }

    /**
     * Gets the user selection scope.
     *
     * @return the user selection scope
     */
    public int getUserSelectionScope()
    {
        return userSelectionScope;
    }

    /**
     * Sets the user selection scope.
     *
     * @param userSelectionScope the new user selection scope
     */
    public void setUserSelectionScope(int userSelectionScope)
    {
        this.userSelectionScope = userSelectionScope;
    }

    /**
     * Gets the version.
     *
     * @return the version
     */
    public String getVersion()
    {
        return version;
    }

    /**
     * Sets the version.
     *
     * @param version the new version
     */
    public void setVersion(String version)
    {
        this.version = version;
    }

    /**
     * Checks if is viewable.
     *
     * @return true, if is viewable
     */
    public boolean isViewable()
    {
        return isViewable;
    }

    /**
     * Sets the viewable.
     *
     * @param isViewable the new viewable
     */
    public void setViewable(boolean isViewable)
    {
        this.isViewable = isViewable;
    }

    /**
     * Gets the web id.
     *
     * @return the web id
     */
    public String getWebId()
    {
        return webId;
    }

    /**
     * Sets the web id.
     *
     * @param webId the new web id
     */
    public void setWebId(String webId)
    {
        this.webId = webId;
    }

    /**
     * Gets the width.
     *
     * @return the width
     */
    public int getWidth()
    {
        return width;
    }

    /**
     * Sets the width.
     *
     * @param width the new width
     */
    public void setWidth(int width)
    {
        this.width = width;
    }

    /**
     * Checks if is wiki linking.
     *
     * @return true, if is wiki linking
     */
    public boolean isWikiLinking()
    {
        return isWikiLinking;
    }

    /**
     * Sets the wiki linking.
     *
     * @param isWikiLinking the new wiki linking
     */
    public void setWikiLinking(boolean isWikiLinking)
    {
        this.isWikiLinking = isWikiLinking;
    }

    /**
     * Gets the x name.
     *
     * @return the x name
     */
    public String getXName()
    {
        return xName;
    }

    /**
     * Sets the x name.
     *
     * @param xName the new x name
     */
    public void setXName(String xName)
    {
        this.xName = xName;
    }
}
