package com.independentsoft.share;

import java.io.InputStream;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

/**
 * The Class TimeZoneInfo.
 */
public class TimeZoneInfo {

    private int bias = Integer.MIN_VALUE;
    private int daylightBias = Integer.MIN_VALUE;
    private int standardBias = Integer.MIN_VALUE;

    /**
     * Instantiates a new time zone info.
     */
    public TimeZoneInfo()
    {
    }

    TimeZoneInfo(InputStream inputStream) throws XMLStreamException
    {
        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        XMLStreamReader reader = xmlInputFactory.createXMLStreamReader(inputStream);

        parse(reader);
    }

    TimeZoneInfo(XMLStreamReader reader) throws XMLStreamException
    {
        parse(reader);
    }

    private void parse(XMLStreamReader reader) throws XMLStreamException
    {
        while (reader.hasNext())
        {
            if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Bias") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                String stringValue = reader.getElementText();

                if (stringValue != null && stringValue.length() > 0)
                {
                    bias = Integer.parseInt(stringValue);
                }
            }
            else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("DaylightBias") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                String stringValue = reader.getElementText();

                if (stringValue != null && stringValue.length() > 0)
                {
                    daylightBias = Integer.parseInt(stringValue);
                }
            }
            else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("StandardBias") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                String stringValue = reader.getElementText();

                if (stringValue != null && stringValue.length() > 0)
                {
                    standardBias = Integer.parseInt(stringValue);
                }
            }

            if (reader.isEndElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Information") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                break;
            }
            else
            {
                reader.next();
            }
        }
    }

    /**
     * Gets the bias.
     *
     * @return the bias
     */
    public int getBias()
    {
        return bias;
    }

    /**
     * Gets the daylight bias.
     *
     * @return the daylight bias
     */
    public int getDaylightBias()
    {
        return daylightBias;
    }

    /**
     * Gets the standard bias.
     *
     * @return the standard bias
     */
    public int getStandardBias()
    {
        return standardBias;
    }
}
