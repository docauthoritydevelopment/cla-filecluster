package com.independentsoft.share.caml;

import java.util.ArrayList;
import java.util.List;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

/**
 * The Class In.
 */
public class In implements ILogicalTest {

    private FieldReferenceQueryTest fieldRef;
    private List<FieldValue> values = new ArrayList<FieldValue>();

    /**
     * Instantiates a new in.
     */
    public In()
    {
    }

    In(XMLStreamReader reader) throws XMLStreamException
    {
        parse(reader);
    }

    private void parse(XMLStreamReader reader) throws XMLStreamException
    {
        while (reader.hasNext())
        {
            if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("FieldRef") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                fieldRef = new FieldReferenceQueryTest(reader);
            }
            else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Values") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                while (reader.hasNext())
                {
                    if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Value") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        FieldValue value = new FieldValue(reader);
                        values.add(value);
                    }

                    if (reader.isEndElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Values") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        break;
                    }
                    else
                    {
                        reader.next();
                    }
                }
            }

            if (reader.isEndElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("In") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                break;
            }
            else
            {
                reader.next();
            }
        }
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        String stringValue = "<In>";

        if (fieldRef != null)
        {
            stringValue += fieldRef.toString();
        }

        if (values != null && values.size() > 0)
        {
            stringValue += "<In>";

            for(FieldValue value : values)
            {
                stringValue += value.toString();
            }

            stringValue += "</In>";
        }

        stringValue += "</In>";

        return stringValue;
    }

    /**
     * Gets the field ref.
     *
     * @return the field ref
     */
    public FieldReferenceQueryTest getFieldRef()
    {
        return fieldRef;
    }

    /**
     * Sets the field ref.
     *
     * @param fieldRef the new field ref
     */
    public void setFieldRef(FieldReferenceQueryTest fieldRef)
    {
        this.fieldRef = fieldRef;
    }

    /**
     * Gets the values.
     *
     * @return the values
     */
    public List<FieldValue> getValues()
    {
        return values;
    }
}