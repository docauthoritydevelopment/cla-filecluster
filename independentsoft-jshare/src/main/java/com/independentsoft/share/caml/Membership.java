package com.independentsoft.share.caml;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import com.independentsoft.share.Util;

/**
 * The Class Membership.
 */
public class Membership implements ILogicalOperator {

    private String type;
    private int id = Integer.MIN_VALUE;
    private String fieldRef;

    /**
     * Instantiates a new membership.
     */
    public Membership()
    {
    }

    Membership(XMLStreamReader reader) throws XMLStreamException
    {
        parse(reader);
    }

    private void parse(XMLStreamReader reader) throws XMLStreamException
    {
        type = reader.getAttributeValue(null, "Type");

        String idAttribute = reader.getAttributeValue(null, "ID");

        if (idAttribute != null && idAttribute.length() > 0)
        {
            id = Integer.parseInt(idAttribute);
        }

        while (reader.hasNext())
        {
            if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("FieldRef") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                fieldRef = reader.getAttributeValue(null, "Name");
            }

            if (reader.isEndElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Membership") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                break;
            }
            else
            {
                reader.next();
            }
        }
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        String attributes = "";

        if (type != null && type.length() > 0)
        {
            attributes += " Type=\"" + Util.encodeJson(type) + "\"";
        }

        if (id > Integer.MIN_VALUE)
        {
            attributes += " ID=\"" + id + "\"";
        }

        String stringValue = "<Membership" + attributes + ">";

        if (fieldRef != null)
        {
            stringValue += fieldRef.toString();
        }

        stringValue += "</Membership>";

        return stringValue;
    }

    /**
     * Gets the type.
     *
     * @return the type
     */
    public String getType()
    {
        return type;
    }

    /**
     * Sets the type.
     *
     * @param type the new type
     */
    public void setType(String type)
    {
        this.type = type;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public int getId()
    {
        return id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(int id)
    {
        this.id = id;
    }

    /**
     * Gets the field ref.
     *
     * @return the field ref
     */
    public String getFieldRef()
    {
        return fieldRef;
    }

    /**
     * Sets the field ref.
     *
     * @param fieldRef the new field ref
     */
    public void setFieldRef(String fieldRef)
    {
        this.fieldRef = fieldRef;
    }
}