package com.independentsoft.share.caml;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import com.independentsoft.share.Util;

/**
 * The Class WithIndex.
 */
public class WithIndex {

    private WithIndexJoin and;
    private Equal equal;
    private String id;

    /**
     * Instantiates a new with index.
     */
    public WithIndex()
    {
    }

    WithIndex(XMLStreamReader reader) throws XMLStreamException
    {
        parse(reader);
    }

    private void parse(XMLStreamReader reader) throws XMLStreamException
    {
        id = reader.getAttributeValue(null, "ID");

        while (true)
        {
            if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("And") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                and = new WithIndexJoin(reader);
            }
            else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Eq") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                equal = new Equal(reader);
            }

            if (reader.isEndElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("WithIndex") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                break;
            }
            else
            {
                reader.next();
            }
        }
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        String attributes = "";

        if (id != null && id.length() > 0)
        {
            attributes += " ID=\"" + Util.encodeJson(id) + "\"";
        }

        String stringValue = "<WithIndex" + attributes + ">";

        if (and != null)
        {
            //not implemented
        }

        if (equal != null)
        {
            stringValue += equal.toString();
        }

        stringValue += "</WithIndex>";

        return stringValue;
    }

    /**
     * Gets the and.
     *
     * @return the and
     */
    public WithIndexJoin getAnd()
    {
        return and;
    }

    /**
     * Sets the and.
     *
     * @param and the new and
     */
    public void setAnd(WithIndexJoin and)
    {
        this.and = and;
    }

    /**
     * Gets the equal.
     *
     * @return the equal
     */
    public Equal getEqual()
    {
        return equal;
    }

    /**
     * Sets the equal.
     *
     * @param equal the new equal
     */
    public void setEqual(Equal equal)
    {
        this.equal = equal;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public String getId()
    {
        return id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(String id)
    {
        this.id = id;
    }
}
