package com.independentsoft.share.caml;

/**
 * The Class LogicalNull.
 */
public abstract class LogicalNull implements ILogicalOperator {

    protected String fieldRef;

    /**
     * Gets the field ref.
     *
     * @return the field ref
     */
    public String getFieldRef()
    {
        return fieldRef;
    }

    /**
     * Sets the field ref.
     *
     * @param fieldRef the new field ref
     */
    public void setFieldRef(String fieldRef)
    {
        this.fieldRef = fieldRef;
    }
}
