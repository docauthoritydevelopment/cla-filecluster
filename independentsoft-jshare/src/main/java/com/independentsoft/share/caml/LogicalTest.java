package com.independentsoft.share.caml;

/**
 * The Class LogicalTest.
 */
public abstract class LogicalTest implements ILogicalTest {

    protected FieldReferenceQueryTest fieldRef;
    protected FieldValue value;

    /**
     * Gets the field ref.
     *
     * @return the field ref
     */
    public FieldReferenceQueryTest getFieldRef()
    {
        return fieldRef;
    }

    /**
     * Sets the field ref.
     *
     * @param fieldRef the new field ref
     */
    public void setFieldRef(FieldReferenceQueryTest fieldRef)
    {
        this.fieldRef = fieldRef;
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public FieldValue getValue()
    {
        return value;
    }

    /**
     * Sets the value.
     *
     * @param value the new value
     */
    public void setValue(FieldValue value)
    {
        this.value = value;
    }
}
