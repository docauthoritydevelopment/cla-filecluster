package com.independentsoft.share.caml;

import java.text.ParseException;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

/**
 * The Class CamlQuery.
 */
public class CamlQuery {

    private Where where;
    private OrderBy orderBy;
    private GroupBy groupBy;
    private WithIndex withIndex;

    /**
     * Instantiates a new caml query.
     */
    public CamlQuery()
    {
    }

    /**
     * Instantiates a new caml query.
     *
     * @param reader the reader
     * @param localName the local name
     * @throws XMLStreamException the XML stream exception
     * @throws ParseException the parse exception
     */
    public  CamlQuery(XMLStreamReader reader, String localName) throws XMLStreamException, ParseException
    {
        parse(reader, localName);
    }

    private void parse(XMLStreamReader reader, String localName) throws XMLStreamException, ParseException
    {
        while (true)
        {
            if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Where") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                where = new Where(reader);
            }
            else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("OrderBy") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                orderBy = new OrderBy(reader);
            }
            else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("GroupBy") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                groupBy = new GroupBy(reader);
            }
            else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("WithIndex") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                withIndex = new WithIndex(reader);
            }

            if (reader.isEndElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals(localName) && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                break;
            }
            else
            {
                reader.next();
            }
        }
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        String stringValue = "";

        if (where != null)
        {
            stringValue += where.toString();
        }

        if (orderBy != null)
        {
            stringValue += orderBy.toString();
        }

        if (groupBy != null)
        {
            stringValue += groupBy.toString();
        }

        if (withIndex != null)
        {
            stringValue += withIndex.toString();
        }

        return stringValue;
    }

    /**
     * Gets the where.
     *
     * @return the where
     */
    public Where getWhere()
    {
        return where;
    }

    /**
     * Sets the where.
     *
     * @param where the new where
     */
    public void setWhere(Where where)
    {
        this.where = where;
    }

    /**
     * Gets the order by.
     *
     * @return the order by
     */
    public OrderBy getOrderBy()
    {
        return orderBy;
    }

    /**
     * Sets the order by.
     *
     * @param orderBy the new order by
     */
    public void setOrderBy(OrderBy orderBy)
    {
        this.orderBy = orderBy;
    }

    /**
     * Gets the group by.
     *
     * @return the group by
     */
    public GroupBy getGroupBy()
    {
        return groupBy;
    }

    /**
     * Sets the group by.
     *
     * @param groupBy the new group by
     */
    public void setGroupBy(GroupBy groupBy)
    {
        this.groupBy = groupBy;
    }

    /**
     * Gets the with index.
     *
     * @return the with index
     */
    public WithIndex getWithIndex()
    {
        return withIndex;
    }

    /**
     * Sets the with index.
     *
     * @param withIndex the new with index
     */
    public void setWithIndex(WithIndex withIndex)
    {
        this.withIndex = withIndex;
    }
}
