package com.independentsoft.share.caml;

import java.util.ArrayList;
import java.util.List;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

/**
 * The Class GroupBy.
 */
public class GroupBy {

    private List<FieldReferenceGroupBy> fieldReferences = new ArrayList<FieldReferenceGroupBy>();
    private boolean isCollapse;
    private int groupLimit = -1;

    /**
     * Instantiates a new group by.
     */
    public GroupBy()
    {
    }

    GroupBy(XMLStreamReader reader) throws XMLStreamException
    {
        parse(reader);
    }

    private void parse(XMLStreamReader reader) throws XMLStreamException
    {
        String isCollapseAttribute = reader.getAttributeValue(null, "Collapse");
        String groupLimitAttribute = reader.getAttributeValue(null, "GroupLimit");

        if (isCollapseAttribute != null && isCollapseAttribute.length() > 0)
        {
            isCollapse = Boolean.parseBoolean(isCollapseAttribute);
        }

        if (groupLimitAttribute != null && groupLimitAttribute.length() > 0)
        {
            groupLimit = Integer.parseInt(groupLimitAttribute);
        }

        while (reader.hasNext())
        {
            if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("FieldRef") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                FieldReferenceGroupBy fieldRef = new FieldReferenceGroupBy(reader);
                fieldReferences.add(fieldRef);
            }

            if (reader.isEndElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("GroupBy") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                break;
            }
            else
            {
                reader.next();
            }
        }
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        String attributes = "";

        if (isCollapse)
        {
            attributes += " Collapse=\"true\"";
        }

        if (groupLimit > -1)
        {
            attributes += " GroupLimit=\"" + groupLimit + "\"";
        }

        String stringValue = "<GroupBy" + attributes + ">";

        for(FieldReferenceGroupBy fieldRef : fieldReferences)
        {
            stringValue += fieldRef.toString();
        }

        stringValue += "</GroupBy>";

        return stringValue;
    }

    /**
     * Gets the field references.
     *
     * @return the field references
     */
    public List<FieldReferenceGroupBy> getFieldReferences()
    {
        return fieldReferences;
    }

    /**
     * Checks if is collapse.
     *
     * @return true, if is collapse
     */
    public boolean isCollapse()
    {
        return isCollapse;
    }

    /**
     * Gets the group limit.
     *
     * @return the group limit
     */
    public int getGroupLimit()
    {
        return groupLimit;
    }
}
