package com.independentsoft.share.caml;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

/**
 * The Class WithIndexJoin.
 */
public class WithIndexJoin {

    private LogicalTest operator1;
    private ILogicalTest operator2;
    private LogicalTest operator3;
    private ILogicalTest operator4;

    /**
     * Instantiates a new with index join.
     */
    public WithIndexJoin()
    {
    }

    WithIndexJoin(XMLStreamReader reader) throws XMLStreamException
    {
        parse(reader);
    }

    private void parse(XMLStreamReader reader) throws XMLStreamException
    {
        while (reader.hasNext())
        {
            //not implemented

            if (reader.isEndElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("And") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                break;
            }
            else
            {
                reader.next();
            }
        }
    }
}
