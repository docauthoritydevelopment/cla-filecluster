package com.independentsoft.share.caml;

import java.text.ParseException;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

/**
 * The Class DateRangesOverlap.
 */
public class DateRangesOverlap implements ILogicalOperator {

    private FieldReferenceQueryDate fieldRef1;
    private FieldReferenceQueryDate fieldRef2;
    private FieldReferenceQueryDate fieldRef3;
    private DateRangeValue value;

    /**
     * Instantiates a new date ranges overlap.
     */
    public DateRangesOverlap()
    {
    }

    DateRangesOverlap(XMLStreamReader reader) throws XMLStreamException, ParseException
    {
        parse(reader);
    }

    private void parse(XMLStreamReader reader) throws XMLStreamException, ParseException
    {
        while (reader.hasNext())
        {
            if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("FieldRef") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                if (fieldRef1 == null)
                {
                    fieldRef1 = new FieldReferenceQueryDate(reader);
                }
                else if (fieldRef2 == null)
                {
                    fieldRef2 = new FieldReferenceQueryDate(reader);
                }
                else
                {
                    fieldRef3 = new FieldReferenceQueryDate(reader);
                }
            }
            else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Value") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                value = new DateRangeValue(reader);
            }

            if (reader.isEndElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("DateRangesOverlap") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                break;
            }
            else
            {
                reader.next();
            }
        }
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        String stringValue = "<DateRangesOverlap>";

        if (fieldRef1 != null)
        {
            stringValue += fieldRef1.toString();
        }

        if (fieldRef2 != null)
        {
            stringValue += fieldRef2.toString();
        }

        if (fieldRef3 != null)
        {
            stringValue += fieldRef3.toString();
        }

        if (value != null)
        {
            stringValue += value.toString();
        }

        stringValue += "</DateRangesOverlap>";

        return stringValue;
    }

    /**
     * Gets the field ref1.
     *
     * @return the field ref1
     */
    public FieldReferenceQueryDate getFieldRef1()
    {
        return fieldRef1;
    }

    /**
     * Sets the field ref1.
     *
     * @param fieldRef the new field ref1
     */
    public void setFieldRef1(FieldReferenceQueryDate fieldRef)
    {
        this.fieldRef1 = fieldRef;
    }

    /**
     * Gets the field ref2.
     *
     * @return the field ref2
     */
    public FieldReferenceQueryDate getFieldRef2()
    {
        return fieldRef2;
    }

    /**
     * Sets the field ref2.
     *
     * @param fieldRef the new field ref2
     */
    public void setFieldRef2(FieldReferenceQueryDate fieldRef)
    {
        this.fieldRef2 = fieldRef;
    }

    /**
     * Gets the field ref3.
     *
     * @return the field ref3
     */
    public FieldReferenceQueryDate getFieldRef3()
    {
        return fieldRef3;
    }

    /**
     * Sets the field ref3.
     *
     * @param fieldRef the new field ref3
     */
    public void setFieldRef3(FieldReferenceQueryDate fieldRef)
    {
        this.fieldRef3 = fieldRef;
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public DateRangeValue getValue()
    {
        return value;
    }

    /**
     * Sets the value.
     *
     * @param value the new value
     */
    public void setValue(DateRangeValue value)
    {
        this.value = value;
    }
}

