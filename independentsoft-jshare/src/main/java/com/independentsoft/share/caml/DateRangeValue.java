package com.independentsoft.share.caml;

import java.text.ParseException;
import java.util.Date;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import com.independentsoft.share.Util;

/**
 * The Class DateRangeValue.
 */
public class DateRangeValue {

    private String type;
    private boolean includeTimeValue;
    private boolean storageTimeZone;
    private Date value;

    /**
     * Instantiates a new date range value.
     */
    public DateRangeValue()
    {
    }

    DateRangeValue(XMLStreamReader reader) throws XMLStreamException, ParseException
    {
        parse(reader);
    }

    private void parse(XMLStreamReader reader) throws XMLStreamException, ParseException
    {
        type = reader.getAttributeValue(null, "Type");

        String includeTimeValueAttribute = reader.getAttributeValue(null, "IncludeTimeValue");
        String storageTimeZoneAttribute = reader.getAttributeValue(null, "StorageTZ");

        if (includeTimeValueAttribute != null && includeTimeValueAttribute.length() > 0)
        {
            includeTimeValue = Boolean.parseBoolean(includeTimeValueAttribute);
        }

        if (storageTimeZoneAttribute != null && storageTimeZoneAttribute.length() > 0)
        {
            storageTimeZone = Boolean.parseBoolean(storageTimeZoneAttribute);
        }

        while (reader.hasNext())
        {
            if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Month") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                String stringValue = reader.getElementText();

                if (stringValue != null && stringValue.length() > 0)
                {
                    value = Util.parseDate(stringValue);
                }
            }
            else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Now") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                String stringValue = reader.getElementText();

                if (stringValue != null && stringValue.length() > 0)
                {
                    value = Util.parseDate(stringValue);
                }
            }
            else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Today") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                String stringValue = reader.getElementText();

                if (stringValue != null && stringValue.length() > 0)
                {
                    value = Util.parseDate(stringValue);
                }
            }
            else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Week") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                String stringValue = reader.getElementText();

                if (stringValue != null && stringValue.length() > 0)
                {
                    value = Util.parseDate(stringValue);
                }
            }

            if (reader.isEndElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Value") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                break;
            }
            else
            {
                reader.next();
            }
        }
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        String attributes = "";

        if (type != null && type.length() > 0)
        {
            attributes += " Type=\"" + Util.encodeJson(type) + "\"";
        }

        if (includeTimeValue)
        {
            attributes += " IncludeTimeValue=\"true\"";
        }

        if (storageTimeZone)
        {
            attributes += " StorageTZ=\"true\"";
        }

        String stringValue = "<Value" + attributes + ">";

        if (value != null)
        {
            stringValue += "<Now>" + Util.toString(value, "EEE, dd MMM yyyy HH:mm:ss zzz") + "</Now>";
        }

        stringValue += "</Value>";

        return stringValue;
    }

    /**
     * Gets the type.
     *
     * @return the type
     */
    public String getType()
    {
        return type;
    }

    /**
     * Gets the include time value.
     *
     * @return the include time value
     */
    public boolean getIncludeTimeValue()
    {
        return includeTimeValue;
    }

    /**
     * Gets the storage time zone.
     *
     * @return the storage time zone
     */
    public boolean getStorageTimeZone()
    {
        return storageTimeZone;
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public Date getValue()
    {
        return value;
    }
}