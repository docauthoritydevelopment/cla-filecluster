package com.independentsoft.share.caml;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import com.independentsoft.share.Util;

/**
 * The Class FieldReferenceQueryTest.
 */
public class FieldReferenceQueryTest {

    private String id;
    private String name;
    private boolean lookupId;

    /**
     * Instantiates a new field reference query test.
     */
    public FieldReferenceQueryTest()
    {
    }

    FieldReferenceQueryTest(XMLStreamReader reader) throws XMLStreamException
    {
        parse(reader);
    }

    private void parse(XMLStreamReader reader) throws XMLStreamException
    {
        id = reader.getAttributeValue(null, "ID");
        name = reader.getAttributeValue(null, "Name");

        String lookupIdAttribute = reader.getAttributeValue(null, "LookupId");

        if(lookupIdAttribute != null && lookupIdAttribute.length() > 0)
        {
            lookupId = Boolean.parseBoolean(lookupIdAttribute);
        }

        while (reader.hasNext())
        {
            if (reader.isEndElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("FieldRef") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                break;
            }
            else
            {
                reader.next();
            }
        }
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        String attributes = "";

        if (id != null && id.length() > 0)
        {
            attributes += " ID=\"" + Util.encodeJson(id) + "\"";
        }

        if (name != null && name.length() > 0)
        {
            attributes += " Name=\"" + Util.encodeJson(name) + "\"";
        }

        if (lookupId)
        {
            attributes += " LookupId=\"true\"";
        }

        String stringValue = "<FieldRef" + attributes + "/>";

        return stringValue;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public String getId()
    {
        return id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(String id)
    {
        this.id = id;
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName()
    {
        return name;
    }

    /**
     * Sets the name.
     *
     * @param name the new name
     */
    public void setName(String name)
    {
        this.name = name;
    }

    /**
     * Checks if is lookup id.
     *
     * @return true, if is lookup id
     */
    public boolean isLookupId()
    {
        return lookupId;
    }

    /**
     * Sets the lookup id.
     *
     * @param lookupId the new lookup id
     */
    public void setLookupId(boolean lookupId)
    {
        this.lookupId = lookupId;
    }
}
