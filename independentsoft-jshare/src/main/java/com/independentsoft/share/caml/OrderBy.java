package com.independentsoft.share.caml;

import java.util.ArrayList;
import java.util.List;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

/**
 * The Class OrderBy.
 */
public class OrderBy {

    private List<FieldReferenceOrderBy> fieldReferences = new ArrayList<FieldReferenceOrderBy>();
    private boolean isOverride;

    /**
     * Instantiates a new order by.
     */
    public OrderBy()
    {
    }

    OrderBy(XMLStreamReader reader) throws XMLStreamException
    {
        parse(reader);
    }

    private void parse(XMLStreamReader reader) throws XMLStreamException
    {
        String isOverrideAttribute = reader.getAttributeValue(null, "Override");

        if (isOverrideAttribute != null && isOverrideAttribute.length() > 0)
        {
            isOverride = Boolean.parseBoolean(isOverrideAttribute);
        }

        while (reader.hasNext())
        {
            if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("FieldRef") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                FieldReferenceOrderBy fieldRef = new FieldReferenceOrderBy(reader);
                fieldReferences.add(fieldRef);
            }

            if (reader.isEndElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("OrderBy") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                break;
            }
            else
            {
                reader.next();
            }
        }
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        String attributes = "";

        if (isOverride)
        {
            attributes += " Override=\"true\"";
        }

        String stringValue = "<OrderBy" + attributes + ">";

        for(FieldReferenceOrderBy fieldRef : fieldReferences)
        {
            stringValue += fieldRef.toString();
        }

        stringValue += "</OrderBy>";

        return stringValue;
    }

    /**
     * Gets the field references.
     *
     * @return the field references
     */
    public List<FieldReferenceOrderBy> getFieldReferences()
    {
        return fieldReferences;
    }

    /**
     * Checks if is override.
     *
     * @return true, if is override
     */
    public boolean isOverride()
    {
        return isOverride;
    }

    /**
     * Sets the override.
     *
     * @param isOverride the new override
     */
    public void setOverride(boolean isOverride)
    {
        this.isOverride = isOverride;
    }
}