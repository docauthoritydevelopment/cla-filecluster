package com.independentsoft.share.caml;

/**
 * The Class Join.
 */
public abstract class Join implements ILogicalOperator {

    protected ILogicalOperator operator1;
    protected ILogicalOperator operator2;

    /**
     * Gets the operator1.
     *
     * @return the operator1
     */
    public ILogicalOperator getOperator1()
    {
        return operator1;
    }

    /**
     * Sets the operator1.
     *
     * @param operator the new operator1
     */
    public void setOperator1(ILogicalOperator operator)
    {
        this.operator1 = operator;
    }

    /**
     * Gets the operator2.
     *
     * @return the operator2
     */
    public ILogicalOperator getOperator2()
    {
        return operator2;
    }

    /**
     * Sets the operator2.
     *
     * @param operator the new operator2
     */
    public void setOperator2(ILogicalOperator operator)
    {
        this.operator2 = operator;
    }
}
