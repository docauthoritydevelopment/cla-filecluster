package com.independentsoft.share.caml;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import com.independentsoft.share.Util;

/**
 * The Class FieldValue.
 */
public class FieldValue {

    private String value;
    private String type;

    /**
     * Instantiates a new field value.
     */
    public FieldValue()
    {
    }

    /**
     * Instantiates a new field value.
     *
     * @param value the value
     */
    public FieldValue(String value)
    {
        this.value = value;
    }

    /**
     * Instantiates a new field value.
     *
     * @param value the value
     * @param type the type
     */
    public FieldValue(String value, String type)
    {
        this.value = value;
        this.type = type;
    }

    FieldValue(XMLStreamReader reader) throws XMLStreamException
    {
        parse(reader);
    }

    private void parse(XMLStreamReader reader) throws XMLStreamException
    {
        type = reader.getAttributeValue(null, "Type");

        while (reader.hasNext())
        {
            if(reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Value") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                value = reader.getElementText();
            }

            if (reader.isEndElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Value") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                break;
            }
            else
            {
                reader.next();
            }
        }
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    public String toString()
    {
        String attributes = "";

        if (type != null && type.length() > 0)
        {
            attributes += " Type=\"" + Util.encodeJson(type) + "\"";
        }

        String stringValue = "<Value" + attributes + ">";

        if (value != null && value.length() > 0)
        {
            stringValue += Util.encodeJson(value);
        }

        stringValue += "</Value>";

        return stringValue;
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public String getValue()
    {
        return value;
    }

    /**
     * Gets the type.
     *
     * @return the type
     */
    public String getType()
    {
        return type;
    }
}