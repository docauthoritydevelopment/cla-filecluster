package com.independentsoft.share.caml;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import com.independentsoft.share.Util;

/**
 * The Class FieldReferenceOrderBy.
 */
public class FieldReferenceOrderBy {

    private String id;
    private String name;
    private boolean ascending;

    /**
     * Instantiates a new field reference order by.
     */
    public FieldReferenceOrderBy()
    {
    }

    FieldReferenceOrderBy(XMLStreamReader reader) throws XMLStreamException
    {
        parse(reader);
    }

    private void parse(XMLStreamReader reader) throws XMLStreamException
    {
        id = reader.getAttributeValue(null, "ID");
        name = reader.getAttributeValue(null, "Name");

        String ascendingAttribute = reader.getAttributeValue(null, "Ascending");

        if(ascendingAttribute != null && ascendingAttribute.length() > 0)
        {
            ascending = Boolean.parseBoolean(ascendingAttribute);
        }

        while (reader.hasNext())
        {
            if (reader.isEndElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("FieldRef") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                break;
            }
            else
            {
                reader.next();
            }
        }
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        String attributes = "";

        if (id != null && id.length() > 0)
        {
            attributes += " ID=\"" + Util.encodeJson(id) + "\"";
        }

        if (name != null && name.length() > 0)
        {
            attributes += " Name=\"" + Util.encodeJson(name) + "\"";
        }

        if (ascending)
        {
            attributes += " Ascending=\"true\"";
        }

        String stringValue = "<FieldRef" + attributes + "/>";

        return stringValue;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public String getId()
    {
        return id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(String id)
    {
        this.id = id;
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName()
    {
        return name;
    }

    /**
     * Sets the name.
     *
     * @param name the new name
     */
    public void setName(String name)
    {
        this.name = name;
    }

    /**
     * Checks if is ascending.
     *
     * @return true, if is ascending
     */
    public boolean isAscending()
    {
        return ascending;
    }

    /**
     * Sets the ascending.
     *
     * @param ascending the new ascending
     */
    public void setAscending(boolean ascending)
    {
        this.ascending = ascending;
    }
}
