package com.independentsoft.share.caml;

import java.text.ParseException;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

/**
 * The Class And.
 */
public class And extends Join {

    /**
     * Instantiates a new and.
     */
    public And()
    {
    }

    And(XMLStreamReader reader) throws XMLStreamException, ParseException
    {
        parse(reader);
    }

    private void parse(XMLStreamReader reader) throws XMLStreamException, ParseException
    {
        while (reader.hasNext())
        {
            if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("And") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                if (operator1 == null)
                {
                    operator1 = new And(reader);
                }
                else
                {
                    operator1 = new And(reader);
                }
            }
            else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("BeginsWith") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                if (operator1 == null)
                {
                    operator1 = new BeginsWith(reader);
                }
                else
                {
                    operator1 = new BeginsWith(reader);
                }
            }
            else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Contains") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                if (operator1 == null)
                {
                    operator1 = new Contains(reader);
                }
                else
                {
                    operator1 = new Contains(reader);
                }
            }
            else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("DateRangesOverlap") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                if (operator1 == null)
                {
                    operator1 = new DateRangesOverlap(reader);
                }
                else
                {
                    operator1 = new DateRangesOverlap(reader);
                }
            }
            else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Eq") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                if (operator1 == null)
                {
                    operator1 = new Equal(reader);
                }
                else
                {
                    operator1 = new Equal(reader);
                }
            }
            else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Geq") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                if (operator1 == null)
                {
                    operator1 = new GreaterThanOrEqual(reader);
                }
                else
                {
                    operator1 = new GreaterThanOrEqual(reader);
                }
            }
            else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Gt") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                if (operator1 == null)
                {
                    operator1 = new GreaterThan(reader);
                }
                else
                {
                    operator1 = new GreaterThan(reader);
                }
            }
            else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("In") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                if (operator1 == null)
                {
                    operator1 = new In(reader);
                }
                else
                {
                    operator1 = new In(reader);
                }
            }
            else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Includes") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                if (operator1 == null)
                {
                    operator1 = new Includes(reader);
                }
                else
                {
                    operator1 = new Includes(reader);
                }
            }
            else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("IsNotNull") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                if (operator1 == null)
                {
                    operator1 = new IsNotNull(reader);
                }
                else
                {
                    operator1 = new IsNotNull(reader);
                }
            }
            else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("IsNull") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                if (operator1 == null)
                {
                    operator1 = new IsNull(reader);
                }
                else
                {
                    operator1 = new IsNull(reader);
                }
            }
            else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Leq") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                if (operator1 == null)
                {
                    operator1 = new LessThanOrEqual(reader);
                }
                else
                {
                    operator1 = new LessThanOrEqual(reader);
                }
            }
            else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Lt") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                if (operator1 == null)
                {
                    operator1 = new LessThan(reader);
                }
                else
                {
                    operator1 = new LessThan(reader);
                }
            }
            else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Membership") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                if (operator1 == null)
                {
                    operator1 = new Membership(reader);
                }
                else
                {
                    operator1 = new Membership(reader);
                }
            }
            else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Neq") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                if (operator1 == null)
                {
                    operator1 = new NotEqual(reader);
                }
                else
                {
                    operator1 = new NotEqual(reader);
                }
            }
            else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("NotIncludes") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                if (operator1 == null)
                {
                    operator1 = new NotIncludes(reader);
                }
                else
                {
                    operator1 = new NotIncludes(reader);
                }
            }
            else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Or") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                if (operator1 == null)
                {
                    operator1 = new Or(reader);
                }
                else
                {
                    operator1 = new Or(reader);
                }
            }

            if (reader.isEndElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("And") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                break;
            }
            else
            {
                reader.next();
            }
        }
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        String stringValue = "<And>";

        if (operator1 != null)
        {
            stringValue += operator1.toString();
        }

        if (operator2 != null)
        {
            stringValue += operator2.toString();
        }

        stringValue += "</And>";

        return stringValue;
    }
}
