package com.independentsoft.share;

/**
 * The Enum CustomizedPageStatus.
 */
public enum CustomizedPageStatus {

    UNCUSTOMIZED,
    CUSTOMIZED,
    NONE
}
