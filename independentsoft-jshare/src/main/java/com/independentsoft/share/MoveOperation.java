package com.independentsoft.share;

/**
 * The Enum MoveOperation.
 */
public enum MoveOperation {

    OVERWRITE,
    ALLOW_BROKEN_THICKETS,
    NONE
}
