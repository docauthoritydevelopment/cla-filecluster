package com.independentsoft.share;

/**
 * The Enum FileLevel.
 */
public enum FileLevel {

    PUBLISHED,
    DRAFT,
    CHECKOUT
}
