package com.independentsoft.share;

/**
 * The Class FolderPropertyName.
 */
public class FolderPropertyName {
	
    public static final String CONTENT_TYPE_ORDER = "ContentTypeOrder";
    public static final String ITEM_COUNT = "ItemCount";
    public static final String NAME = "Name";
    public static final String SERVER_RELATIVE_URL = "ServerRelativeUrl";
    public static final String CREATED_TIME = "TimeCreated";
    public static final String LAST_MODIFIED_TIME = "TimeLastModified";
    public static final String UNIQUE_CONTENT_TYPE_ORDER = "UniqueContentTypeOrder";
    public static final String WELCOME_PAGE = "WelcomePage";

}
