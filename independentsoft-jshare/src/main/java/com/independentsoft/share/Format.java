package com.independentsoft.share;

import java.util.ArrayList;
import java.util.List;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

/**
 * The Class Format.
 */
public class Format {

    private List<FormatDefinition> definitions = new ArrayList<FormatDefinition>();
    private String name;

    /**
     * Instantiates a new format.
     */
    public Format()
    {
    }

    Format(XMLStreamReader reader) throws XMLStreamException
    {
        parse(reader);
    }

    private void parse(XMLStreamReader reader) throws XMLStreamException
    {
        name = reader.getAttributeValue(null, "Name");

        while (true)
        {
            if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("FormatDef") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                FormatDefinition definition = new FormatDefinition(reader);
                definitions.add(definition);
            }

            if (reader.isEndElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Format") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                break;
            }
            else
            {
                reader.next();
            }
        }
    }

    /**
     * Gets the definitions.
     *
     * @return the definitions
     */
    public List<FormatDefinition> getDefinitions()
    {
        return definitions;
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName()
    {
        return name;
    }
}
