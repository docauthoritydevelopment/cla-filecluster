package com.independentsoft.share;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

/**
 * The Class FieldReference.
 */
public class FieldReference {

    private String name;
    private String id;
    private String showField;
    private FieldReferenceType refType = FieldReferenceType.NONE;
    private String createUrl;
    private String key;
    private String displayName;

    /**
     * Instantiates a new field reference.
     */
    public FieldReference()
    {
    }

    FieldReference(XMLStreamReader reader) throws XMLStreamException
    {
        parse(reader);
    }

    private void parse(XMLStreamReader reader) throws XMLStreamException
    {
        name = reader.getAttributeValue(null, "Name");
        id = reader.getAttributeValue(null, "ID");
        showField = reader.getAttributeValue(null, "ShowField");
        createUrl = reader.getAttributeValue(null, "CreateURL");
        key = reader.getAttributeValue(null, "Key");
        displayName = reader.getAttributeValue(null, "DisplayName");

        String refTypeAttribute = reader.getAttributeValue(null, "RefType");

        if (refTypeAttribute != null && refTypeAttribute.length() > 0)
        {
            refType = EnumUtil.parseFieldReferenceType(refTypeAttribute);
        }

        while (reader.hasNext())
        {
            if (reader.isEndElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("FieldRef") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                break;
            }
            else
            {
                reader.next();
            }
        }
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName()
    {
        return name;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public String getId()
    {
        return id;
    }

    /**
     * Gets the show field.
     *
     * @return the show field
     */
    public String getShowField()
    {
        return showField;
    }

    /**
     * Gets the ref type.
     *
     * @return the ref type
     */
    public FieldReferenceType getRefType()
    {
        return refType;
    }

    /**
     * Gets the creates the url.
     *
     * @return the creates the url
     */
    public String getCreateUrl()
    {
        return createUrl;
    }

    /**
     * Gets the key.
     *
     * @return the key
     */
    public String getKey()
    {
        return key;
    }

    /**
     * Gets the display name.
     *
     * @return the display name
     */
    public String getDisplayName()
    {
        return displayName;
    }
}
