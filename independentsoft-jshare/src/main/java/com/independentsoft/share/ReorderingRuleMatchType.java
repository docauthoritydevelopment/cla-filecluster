package com.independentsoft.share;

/**
 * The Enum ReorderingRuleMatchType.
 */
public enum ReorderingRuleMatchType {

    RESULT_CONTAINS_KEYWORD,
    TITLE_CONTAINS_KEYWORD,
    TITLE_MATCHES_KEYWORD,
    URL_STARTS_WITH,
    URL_EXACTLY_MATCHES,
    CONTENT_TYPE_IS,
    FILE_EXTENSION_MATCHES,
    RESULT_HAS_TAG,
    MANUAL_CONDITION
}
