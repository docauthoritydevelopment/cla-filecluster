package com.independentsoft.share;

/**
 * The Enum FieldType.
 */
public enum FieldType {

	  INTEGER,
      TEXT,
      NOTE,
      DATE_TIME,
      COUNTER,
      CHOICE,
      LOOKUP,
      BOOLEAN,
      NUMBER,
      CURRENCY,
      URL,
      COMPUTED,
      THREADING,
      GUID,
      MULTI_CHOICE,
      GRID_CHOICE,
      CALCULATED,
      FILE,
      ATTACHMENTS,
      USER,
      RECURRENCE,
      CROSS_PROJECT_LINK,
      MOD_STAT,
      ERROR,
      CONTENT_TYPE_ID,
      PAGE_SEPARATOR,
      THREAD_INDEX,
      WORKFLOW_STATUS,
      ALL_DAY_EVENT,
      WORKFLOW_EVENT_TYPE,
      NONE
}
