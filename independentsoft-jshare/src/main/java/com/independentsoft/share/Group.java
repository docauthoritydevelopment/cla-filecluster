package com.independentsoft.share;

import java.io.InputStream;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

/**
 * The Class Group.
 */
public class Group extends Principal {
    
    private boolean allowMembersEditMembership;
    private boolean allowRequestToJoinLeave;
    private boolean autoAcceptRequestToJoinLeave;
    private String description;
    private boolean allowOnlyMembersViewMembership;
    private String ownerTitle;
    private String requestToJoinLeaveEmailSetting;

    /**
     * Instantiates a new group.
     */
    public Group()
    {
    }

    Group(InputStream inputStream) throws XMLStreamException
    {
        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        XMLStreamReader reader = xmlInputFactory.createXMLStreamReader(inputStream);

        parse(reader);
    }

    Group(XMLStreamReader reader) throws XMLStreamException
    {
        parse(reader);
    }

    private void parse(XMLStreamReader reader) throws XMLStreamException
    {
        while (reader.hasNext())
        {
            if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("properties") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices/metadata"))
            {
                while (reader.hasNext())
                {
                    if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("AllowMembersEditMembership") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            allowMembersEditMembership = Boolean.parseBoolean(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("AllowRequestToJoinLeave") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            allowRequestToJoinLeave = Boolean.parseBoolean(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("AutoAcceptRequestToJoinLeave") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            autoAcceptRequestToJoinLeave = Boolean.parseBoolean(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Description") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        description = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Id") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            id = Integer.parseInt(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("IsHiddenInUI") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            isHiddenInUI = Boolean.parseBoolean(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("LoginName") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        loginName = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("OnlyAllowMembersViewMembership") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            allowOnlyMembersViewMembership = Boolean.parseBoolean(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("OwnerTitle") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        ownerTitle = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("RequestToJoinLeaveEmailSetting") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        requestToJoinLeaveEmailSetting = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("PrincipalType") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            type = EnumUtil.parsePrincipalType(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Title") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        title = reader.getElementText();
                    }

                    if (reader.isEndElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("properties") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices/metadata"))
                    {
                        break;
                    }
                    else
                    {
                        reader.next();
                    }
                }
            }

            if (reader.isEndElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("entry") && reader.getNamespaceURI().equals("http://www.w3.org/2005/Atom"))
            {
                break;
            }
            else
            {
                reader.next();
            }
        }
    }

    String toCreateJSon()
    {
        String stringValue = "{ '__metadata':{ 'type': 'SP.Group' }";

        if (allowMembersEditMembership)
        {
            stringValue += ", 'AllowMembersEditMembership': true";
        }

        if (allowRequestToJoinLeave)
        {
            stringValue += ", 'AllowRequestToJoinLeave': true";
        }

        if (autoAcceptRequestToJoinLeave)
        {
            stringValue += ", 'AutoAcceptRequestToJoinLeave': true";
        }

        if (description != null && description.length() > 0)
        {
            stringValue += ", 'Description': '" + Util.encodeJson(description) + "'";
        }

        if (allowOnlyMembersViewMembership)
        {
            stringValue += ", 'OnlyAllowMembersViewMembership': true";
        }

        if (requestToJoinLeaveEmailSetting != null && requestToJoinLeaveEmailSetting.length() > 0)
        {
            stringValue += ", 'RequestToJoinLeaveEmailSetting': '" + Util.encodeJson(requestToJoinLeaveEmailSetting) + "'";
        }

        if (title != null && title.length() > 0)
        {
            stringValue += ", 'Title': '" + Util.encodeJson(title) + "'";
        }

        stringValue += " }";

        return stringValue;
    }

    String toUpdateJSon()
    {
        String stringValue = "{ '__metadata':{ 'type': 'SP.Group' }";

        if (allowMembersEditMembership)
        {
            stringValue += ", 'AllowMembersEditMembership': true";
        }
        else        	
        {
        	stringValue += ", 'AllowMembersEditMembership': false";
        }

        if (allowRequestToJoinLeave)
        {
            stringValue += ", 'AllowRequestToJoinLeave': true";
        }
        else        	
        {
        	stringValue += ", 'AllowRequestToJoinLeave': false";
        }

        if (autoAcceptRequestToJoinLeave)
        {
            stringValue += ", 'AutoAcceptRequestToJoinLeave': true";
        }
        else        	
        {
        	stringValue += ", 'AutoAcceptRequestToJoinLeave': false";
        }

        if (description != null && description.length() > 0)
        {
            stringValue += ", 'Description': '" + Util.encodeJson(description) + "'";
        }

        if (allowOnlyMembersViewMembership)
        {
            stringValue += ", 'OnlyAllowMembersViewMembership': true";
        }
        else        	
        {
        	stringValue += ", 'OnlyAllowMembersViewMembership': false";
        }

        if (requestToJoinLeaveEmailSetting != null && requestToJoinLeaveEmailSetting.length() > 0)
        {
            stringValue += ", 'RequestToJoinLeaveEmailSetting': '" + Util.encodeJson(requestToJoinLeaveEmailSetting) + "'";
        }

        if (title != null && title.length() > 0)
        {
            stringValue += ", 'Title': '" + Util.encodeJson(title) + "'";
        }

        stringValue += " }";

        return stringValue;
    }

    /**
     * Checks if is members edit membership allowed.
     *
     * @return true, if is members edit membership allowed
     */
    public boolean isMembersEditMembershipAllowed()
    {
        return allowMembersEditMembership;
    }

    /**
     * Allow members edit membership.
     *
     * @param allowMembersEditMembership the allow members edit membership
     */
    public void allowMembersEditMembership(boolean allowMembersEditMembership)
    {
        this.allowMembersEditMembership = allowMembersEditMembership;
    }

    /**
     * Checks if is request to join leave allowed.
     *
     * @return true, if is request to join leave allowed
     */
    public boolean isRequestToJoinLeaveAllowed()
    {
        return allowRequestToJoinLeave;
    }

    /**
     * Allow request to join leave.
     *
     * @param allowRequestToJoinLeave the allow request to join leave
     */
    public void allowRequestToJoinLeave(boolean allowRequestToJoinLeave)
    {
        this.allowRequestToJoinLeave = allowRequestToJoinLeave;
    }

    /**
     * Checks if is auto accept request to join leave.
     *
     * @return true, if is auto accept request to join leave
     */
    public boolean isAutoAcceptRequestToJoinLeave()
    {
        return autoAcceptRequestToJoinLeave;
    }

    /**
     * Sets the auto accept request to join leave.
     *
     * @param autoAcceptRequestToJoinLeave the new auto accept request to join leave
     */
    public void setAutoAcceptRequestToJoinLeave(boolean autoAcceptRequestToJoinLeave)
    {
        this.autoAcceptRequestToJoinLeave = autoAcceptRequestToJoinLeave;
    }

    /**
     * Gets the description.
     *
     * @return the description
     */
    public String getDescription()
    {
        return description;
    }

    /**
     * Sets the description.
     *
     * @param description the new description
     */
    public void setDescription(String description)
    {
        this.description = description;
    }

    /**
     * Checks if is only members view membership allowed.
     *
     * @return true, if is only members view membership allowed
     */
    public boolean isOnlyMembersViewMembershipAllowed()
    {
        return allowOnlyMembersViewMembership;
    }

    /**
     * Allow only members view membership.
     *
     * @param allowOnlyMembersViewMembership the allow only members view membership
     */
    public void allowOnlyMembersViewMembership(boolean allowOnlyMembersViewMembership)
    {
        this.allowOnlyMembersViewMembership = allowOnlyMembersViewMembership;
    }

    /**
     * Gets the owner title.
     *
     * @return the owner title
     */
    public String getOwnerTitle()
    {
        return ownerTitle;
    }

    /**
     * Gets the request to join leave email setting.
     *
     * @return the request to join leave email setting
     */
    public String getRequestToJoinLeaveEmailSetting()
    {
        return requestToJoinLeaveEmailSetting;
    }

    /**
     * Sets the request to join leave email setting.
     *
     * @param requestToJoinLeaveEmailSetting the new request to join leave email setting
     */
    public void setRequestToJoinLeaveEmailSetting(String requestToJoinLeaveEmailSetting)
    {
        this.requestToJoinLeaveEmailSetting = requestToJoinLeaveEmailSetting;
    }
}
