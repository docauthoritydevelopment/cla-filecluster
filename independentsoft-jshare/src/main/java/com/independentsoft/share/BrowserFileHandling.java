package com.independentsoft.share;

/**
 * The Enum BrowserFileHandling.
 */
public enum BrowserFileHandling {
	
    PERMISSIVE,
    STRICT
}
