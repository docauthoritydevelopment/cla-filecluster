package com.independentsoft.share;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

/**
 * The Class SuggestionQuery.
 */
public class SuggestionQuery {

    private boolean isPersonal;
    private String query;

    /**
     * Instantiates a new suggestion query.
     */
    public SuggestionQuery()
    {
    }

    SuggestionQuery(XMLStreamReader reader) throws XMLStreamException
    {
        parse(reader);
    }

    private void parse(XMLStreamReader reader) throws XMLStreamException
    {
        while (reader.hasNext())
        {
            if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("IsPersonal") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                String stringValue = reader.getElementText();

                if(stringValue != null && stringValue.length() > 0)
                {
                    isPersonal = Boolean.parseBoolean(stringValue);
                }
            }
            else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Query") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                query = reader.getElementText();
            }

            if (reader.isEndElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("QuerySuggestionQuery") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                break;
            }
            else
            {
                reader.next();
            }
        }
    }

    /**
     * Checks if is personal.
     *
     * @return true, if is personal
     */
    public boolean isPersonal()
    {
        return isPersonal;
    }

    /**
     * Gets the query.
     *
     * @return the query
     */
    public String getQuery()
    {
        return query;
    }
}
