package com.independentsoft.share;

/**
 * The Enum TemplateFileType.
 */
public enum TemplateFileType {

    STANDARD_PAGE,
    WIKI_PAGE,
    FORM_PAGE,
    NONE
}
