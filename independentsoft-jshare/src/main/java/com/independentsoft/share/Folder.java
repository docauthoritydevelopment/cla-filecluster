package com.independentsoft.share;

import java.io.InputStream;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

/**
 * The Class Folder.
 */
public class Folder {

    private List<String> contentTypeOrders = new ArrayList<String>();
    private int itemCount;
    private String name;
    private String serverRelativeUrl;
    private Date createdTime;
    private Date lastModifiedTime;
    private List<String> uniqueContentTypeOrders = new ArrayList<String>();
    private String welcomePage;
    private String uniqueId;
    
    /**
     * Instantiates a new folder.
     */
    public Folder()
    {
    }

    Folder(InputStream inputStream) throws XMLStreamException, ParseException
    {
        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        XMLStreamReader reader = xmlInputFactory.createXMLStreamReader(inputStream);

        parse(reader);
    }

    Folder(XMLStreamReader reader) throws XMLStreamException, ParseException
    {
        parse(reader);
    }

    private void parse(XMLStreamReader reader) throws XMLStreamException, ParseException
    {
        while (reader.hasNext())
        {
            if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("properties") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices/metadata"))
            {
                while (reader.hasNext())
                {
                    if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("ContentTypeOrder") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String contentTypeOrder = reader.getElementText();
                        contentTypeOrders.add(contentTypeOrder);
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("ItemCount") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            itemCount = Integer.parseInt(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Name") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        name = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("ServerRelativeUrl") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        serverRelativeUrl = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("TimeCreated") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            createdTime = Util.parseDate(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("TimeLastModified") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            lastModifiedTime = Util.parseDate(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("UniqueContentTypeOrder") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String contentTypeOrder = reader.getElementText();
                        uniqueContentTypeOrders.add(contentTypeOrder);
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("WelcomePage") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        welcomePage = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("UniqueId") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        uniqueId = reader.getElementText();
                    }

                    if (reader.isEndElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("properties") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices/metadata"))
                    {
                        break;
                    }
                    else
                    {
                        reader.next();
                    }
                }
            }

            if (reader.isEndElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("entry") && reader.getNamespaceURI().equals("http://www.w3.org/2005/Atom"))
            {
                break;
            }
            else
            {
                reader.next();
            }
        }
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        String stringValue = "{ '__metadata':{ 'type': 'SP.Folder' }";

        if(welcomePage != null && welcomePage.length() > 0)
        {
            stringValue += ", 'WelcomePage':'" + Util.encodeJson(welcomePage) + "'";
        }

        stringValue +=" }";

        return stringValue;
    }

    /**
     * Gets the content type orders.
     *
     * @return the content type orders
     */
    public List<String> getContentTypeOrders()
    {
        return contentTypeOrders;
    }

    /**
     * Gets the item count.
     *
     * @return the item count
     */
    public int getItemCount()
    {
        return itemCount;
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName()
    {
        return name;
    }

    /**
     * Gets the server relative url.
     *
     * @return the server relative url
     */
    public String getServerRelativeUrl()
    {
        return serverRelativeUrl;
    }

    /**
     * Gets the created time.
     *
     * @return the created time
     */
    public Date getCreatedTime()
    {
        return createdTime;
    }

    /**
     * Gets the last modified time.
     *
     * @return the last modified time
     */
    public Date getLastModifiedTime()
    {
        return lastModifiedTime;
    }

    /**
     * Gets the unique content type orders.
     *
     * @return the unique content type orders
     */
    public List<String> getUniqueContentTypeOrders()
    {
        return uniqueContentTypeOrders;
    }

    /**
     * Gets the welcome page.
     *
     * @return the welcome page
     */
    public String getWelcomePage()
    {
        return welcomePage;
    }

    /**
     * Sets the welcome page.
     *
     * @param welcomePage the new welcome page
     */
    public void setWelcomePage(String welcomePage)
    {
        this.welcomePage = welcomePage;
    }
    
    /**
     * Gets the unique id.
     *
     * @return the unique id
     */
    public String getUniqueId()
    {
        return uniqueId;
    }
}
