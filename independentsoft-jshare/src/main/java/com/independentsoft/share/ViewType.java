package com.independentsoft.share;

/**
 * The Enum ViewType.
 */
public enum ViewType {

    HTML,
    GRID,
    CALENDAR,
    RECURRENCE,
    CHART,
    GANTT,
    NONE
}
