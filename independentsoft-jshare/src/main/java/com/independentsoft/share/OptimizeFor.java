package com.independentsoft.share;

/**
 * The Enum OptimizeFor.
 */
public enum OptimizeFor {

    ITEM_IDS,
    FOLDER_URLS,
    NONE
}
