package com.independentsoft.share;

import java.io.InputStream;
import java.text.ParseException;
import java.util.Date;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

/**
 * The Class RecycleBinItem.
 */
public class RecycleBinItem {

    private Date deletedDate;
    private String dirName;
    private String id;
    private RecycleBinItemState state = RecycleBinItemState.NONE;
    private RecycleBinItemType type = RecycleBinItemType.NONE;
    private String leafName;
    private long size;
    private String title;

    /**
     * Instantiates a new recycle bin item.
     */
    public RecycleBinItem()
    {
    }

    RecycleBinItem(InputStream inputStream) throws XMLStreamException, ParseException
    {
        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        XMLStreamReader reader = xmlInputFactory.createXMLStreamReader(inputStream);

        parse(reader);
    }

    RecycleBinItem(XMLStreamReader reader) throws XMLStreamException, ParseException
    {
        parse(reader);
    }

    private void parse(XMLStreamReader reader) throws XMLStreamException, ParseException
    {
        while (reader.hasNext())
        {
            if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("properties") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices/metadata"))
            {
                while (reader.hasNext())
                {
                    if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("DeletedDate") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            deletedDate = Util.parseDate(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("DirName") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        dirName = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Id") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        id = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("ItemState") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            state = EnumUtil.parseRecycleBinItemState(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("ItemType") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            type = EnumUtil.parseRecycleBinItemType(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("LeafName") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        leafName = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Size") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            size = Long.parseLong(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Title") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        title = reader.getElementText();
                    }

                    if (reader.isEndElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("properties") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices/metadata"))
                    {
                        break;
                    }
                    else
                    {
                        reader.next();
                    }
                }
            }

            if (reader.isEndElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("entry") && reader.getNamespaceURI().equals("http://www.w3.org/2005/Atom"))
            {
                break;
            }
            else
            {
                reader.next();
            }
        }
    }

    /**
     * Gets the deleted date.
     *
     * @return the deleted date
     */
    public Date getDeletedDate()
    {
        return deletedDate;
    }

    /**
     * Gets the dir name.
     *
     * @return the dir name
     */
    public String getDirName()
    {
        return dirName;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public String getId()
    {
        return id;
    }

    /**
     * Gets the state.
     *
     * @return the state
     */
    public RecycleBinItemState getState()
    {
        return state;
    }

    /**
     * Gets the type.
     *
     * @return the type
     */
    public RecycleBinItemType getType()
    {
        return type;
    }

    /**
     * Gets the leaf name.
     *
     * @return the leaf name
     */
    public String getLeafName()
    {
        return leafName;
    }

    /**
     * Gets the size.
     *
     * @return the size
     */
    public long getSize()
    {
        return size;
    }

    /**
     * Gets the title.
     *
     * @return the title
     */
    public String getTitle()
    {
        return title;
    }    
}
