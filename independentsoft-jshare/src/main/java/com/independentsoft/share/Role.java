package com.independentsoft.share;

import java.io.InputStream;
import java.util.ArrayList;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

/**
 * The Class Role.
 */
public class Role {

    private java.util.List<BasePermission> basePermissions = new ArrayList<BasePermission>();
    private String description;
    private boolean isHidden;
    private int id;
    private String name;
    private int order = Integer.MIN_VALUE;
    private RoleType type = RoleType.NONE;

    /**
     * Instantiates a new role.
     */
    public Role()
    {
    }

    Role(InputStream inputStream) throws XMLStreamException
    {
        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        XMLStreamReader reader = xmlInputFactory.createXMLStreamReader(inputStream);

        parse(reader);
    }

    Role(XMLStreamReader reader) throws XMLStreamException
    {
        parse(reader);
    }

    private void parse(XMLStreamReader reader) throws XMLStreamException
    {
        while (reader.hasNext())
        {
            if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("properties") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices/metadata"))
            {
                while (reader.hasNext())
                {
                    if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("BasePermissions") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        basePermissions = Util.parseBasePermissions(reader, "BasePermissions");
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Description") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        description = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Hidden") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            isHidden = Boolean.parseBoolean(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Id") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            id = Integer.parseInt(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Name") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        name = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Order") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            order = Integer.parseInt(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("RoleTypeKind") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            type = EnumUtil.parseRoleType(stringValue);
                        }
                    }

                    if (reader.isEndElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("properties") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices/metadata"))
                    {
                        break;
                    }
                    else
                    {
                        reader.next();
                    }
                }
            }

            if (reader.isEndElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("entry") && reader.getNamespaceURI().equals("http://www.w3.org/2005/Atom"))
            {
                break;
            }
            else
            {
                reader.next();
            }
        }
    }

    String toCreateJSon()
    {
        String stringValue = "{ '__metadata': { 'type': 'SP.RoleDefinition' }";

        if (basePermissions != null)
        {
            stringValue += Util.basePermissionsToJSon(basePermissions, "BasePermissions");
        }

        if (description != null && description.length() > 0)
        {
            stringValue += ", 'Description': '" + Util.encodeJson(description) + "'";
        }

        if (name != null && name.length() > 0)
        {
            stringValue += ", 'Name': '" + Util.encodeJson(name) + "'";
        }

        if (order > Integer.MIN_VALUE)
        {
            stringValue += ", 'Order': '" + order + "'";
        }

        stringValue += " }";

        return stringValue;
    }

    String toUpdateJSon()
    {
        String stringValue = "{ '__metadata': { 'type': 'SP.RoleDefinition' }";

        if (basePermissions != null)
        {
            stringValue += Util.basePermissionsToJSon(basePermissions, "BasePermissions");
        }

        if (description != null && description.length() > 0)
        {
            stringValue += ", 'Description': '" + Util.encodeJson(description) + "'";
        }

        if (name != null && name.length() > 0)
        {
            stringValue += ", 'Name': '" + Util.encodeJson(name) + "'";
        }

        if (order > Integer.MIN_VALUE)
        {
            stringValue += ", 'Order': '" + order + "'";
        }

        stringValue += " }";

        return stringValue;
    }

    /**
     * Gets the base permissions.
     *
     * @return the base permissions
     */
    public java.util.List<BasePermission> getBasePermissions()
    {
        return basePermissions;
    }

    /**
     * Gets the description.
     *
     * @return the description
     */
    public String getDescription()
    {
        return description;
    }

    /**
     * Sets the description.
     *
     * @param description the new description
     */
    public void setDescription(String description)
    {
        this.description = description;
    }

    /**
     * Checks if is hidden.
     *
     * @return true, if is hidden
     */
    public boolean isHidden()
    {
        return isHidden;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public int getId()
    {
        return id;
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName()
    {
        return name;
    }

    /**
     * Sets the name.
     *
     * @param name the new name
     */
    public void setName(String name)
    {
        this.name = name;
    }

    /**
     * Gets the order.
     *
     * @return the order
     */
    public int getOrder()
    {
        return order;
    }

    /**
     * Sets the order.
     *
     * @param order the new order
     */
    public void setOrder(int order)
    {
        this.order = order;
    }

    /**
     * Gets the type.
     *
     * @return the type
     */
    public RoleType getType()
    {
        return type;
    }
}
