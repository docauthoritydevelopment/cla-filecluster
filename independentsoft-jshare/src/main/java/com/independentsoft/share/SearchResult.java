package com.independentsoft.share;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

/**
 * The Class SearchResult.
 */
public class SearchResult {

    private int elapsedTime;
    private QueryResult primaryQueryResult;
    private List<KeyValue> properties = new ArrayList<KeyValue>();
    private List<QueryResult> secondaryQueryResults = new ArrayList<QueryResult>();
    private String spellingSuggestion;
    private List<String> triggeredRules = new ArrayList<String>();

    /**
     * Instantiates a new search result.
     */
    public SearchResult()
    {
    }

    SearchResult(InputStream inputStream) throws XMLStreamException
    {
        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        XMLStreamReader reader = xmlInputFactory.createXMLStreamReader(inputStream);

        parse(reader);
    }

    private void parse(XMLStreamReader reader) throws XMLStreamException
    {
        while (reader.hasNext() && reader.next() > 0)
        {
            if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("ElapsedTime") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                String stringValue = reader.getElementText();

                if (stringValue != null && stringValue.length() > 0)
                {
                    elapsedTime = Integer.parseInt(stringValue);
                }
            }
            else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("PrimaryQueryResult") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                primaryQueryResult = new QueryResult(reader, "PrimaryQueryResult");
            }
            else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Properties") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                while (true)
                {
                    if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("element") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        KeyValue property = new KeyValue(reader);
                        properties.add(property);
                    }

                    if (reader.isEndElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Properties") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        break;
                    }
                    else
                    {
                        reader.next();
                    }
                }
            }
            else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("SecondaryQueryResults") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                String innerXml  = reader.getElementText();

                if (innerXml != null && innerXml.length() > 64)
                {
                    throw new UnsupportedOperationException("SecondaryQueryResults response:" + innerXml);
                }
            }
            else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("SpellingSuggestion") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                spellingSuggestion = reader.getElementText();
            }
            else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("TriggeredRules") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                String innerXml = reader.getElementText();

                if (innerXml != null && innerXml.length() > 64)
                {
                    throw new UnsupportedOperationException("TriggeredRules response:" + innerXml);
                }
            }
        }
    }

    /**
     * Gets the elapsed time.
     *
     * @return the elapsed time
     */
    public int getElapsedTime()
    {
        return elapsedTime;
    }

    /**
     * Gets the primary query result.
     *
     * @return the primary query result
     */
    public QueryResult getPrimaryQueryResult()
    {
        return primaryQueryResult;
    }

    /**
     * Gets the properties.
     *
     * @return the properties
     */
    public List<KeyValue> getProperties()
    {
        return properties;
    }

    /**
     * Gets the secondary query results.
     *
     * @return the secondary query results
     */
    public List<QueryResult> getSecondaryQueryResults()
    {
        return secondaryQueryResults;
    }

    /**
     * Gets the spelling suggestion.
     *
     * @return the spelling suggestion
     */
    public String getSpellingSuggestion()
    {
        return spellingSuggestion;
    }

    /**
     * Gets the triggered rules.
     *
     * @return the triggered rules
     */
    public List<String> getTriggeredRules()
    {
        return triggeredRules;
    }
}
