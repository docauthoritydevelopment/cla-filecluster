package com.independentsoft.share;

import java.io.InputStream;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import com.independentsoft.share.caml.CamlQuery;

/**
 * The Class View.
 */
public class View {

    private FieldReferenceAggregation aggregation;
    private String aggregationsStatus;
    private String baseViewId;
    private String contentTypeId;
    private boolean isDefaultView;
    private boolean isDefaultViewForContentType;
    private boolean isEditorModified;
    private ViewFormat format;
    private boolean isHidden;
    private String htmlSchemaXml;
    private String id;
    private String imageUrl;
    private boolean isIncludeRootFolder;
    private String jsLink;
    private String listViewXml;
    private ViewMethod method;
    private boolean isMobileDefaultView;
    private boolean isMobileView;
    private String moderationType;
    private boolean isOrderedView;
    private boolean isPaged;
    private boolean isPersonalView;
    private boolean isReadOnlyView;
    private boolean requiresClientIntegration;
    private int rowLimit;
    private ViewScope scope = ViewScope.NONE;
    private String serverRelativeUrl;
    private String styleId;
    private boolean isThreaded;
    private String title;
    private String toolbar;
    private String toolbarTemplateName;
    private FieldReferenceViewData data;
    private List<ListJoin> joins = new ArrayList<ListJoin>();
    private List<ProjectedField> projectedFields = new ArrayList<ProjectedField>();
    private CamlQuery query;
    private ViewType type = ViewType.NONE;


    /**
     * Instantiates a new view.
     */
    public View()
    {
    }

    View(InputStream inputStream) throws XMLStreamException, ParseException
    {
        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        XMLStreamReader reader = xmlInputFactory.createXMLStreamReader(inputStream);

        parse(reader);
    }

    View(XMLStreamReader reader) throws XMLStreamException, ParseException
    {
        parse(reader);
    }

    private void parse(XMLStreamReader reader) throws XMLStreamException, ParseException
    {
        while (reader.hasNext())
        {
            if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("properties") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices/metadata"))
            {
                while (reader.hasNext())
                {
                    if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Aggregations") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        aggregation = new FieldReferenceAggregation(reader);
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("AggregationsStatus") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        aggregationsStatus = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("BaseViewId") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        baseViewId = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("ContentTypeId") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        while (true)
                        {
                            if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("StringValue") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                            {
                                contentTypeId = reader.getElementText();
                            }

                            if (reader.isEndElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("ContentTypeId") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                            {
                                break;
                            }
                            else
                            {
                                reader.next();
                            }
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("DefaultView") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            isDefaultView = Boolean.parseBoolean(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("DefaultViewForContentType") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            isDefaultViewForContentType = Boolean.parseBoolean(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("EditorModified") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            isEditorModified = Boolean.parseBoolean(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Formats") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        format = new ViewFormat(reader);
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Hidden") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            isHidden = Boolean.parseBoolean(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("HtmlSchemaXml") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        htmlSchemaXml = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Id") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        id = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("ImageUrl") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        imageUrl = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("IncludeRootFolder") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            isIncludeRootFolder = Boolean.parseBoolean(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("JsLink") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        jsLink = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("ListViewXml") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        listViewXml = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Method") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        method = new ViewMethod(reader);
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("MobileDefaultView") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            isMobileDefaultView = Boolean.parseBoolean(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("MobileView") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            isMobileView = Boolean.parseBoolean(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("ModerationType") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        moderationType = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("OrderedView") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            isOrderedView = Boolean.parseBoolean(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Paged") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            isPaged = Boolean.parseBoolean(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("PersonalView") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            isPersonalView = Boolean.parseBoolean(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("ReadOnlyView") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            isReadOnlyView = Boolean.parseBoolean(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("RequiresClientIntegration") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            requiresClientIntegration = Boolean.parseBoolean(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("RowLimit") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            rowLimit = Integer.parseInt(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Scope") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            scope = EnumUtil.parseViewScope(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("ServerRelativeUrl") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        serverRelativeUrl = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("StyleId") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        styleId = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Threaded") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            isThreaded = Boolean.parseBoolean(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Title") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        title = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Toolbar") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        toolbar = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("ToolbarTemplateName") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        toolbarTemplateName = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("ViewData") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        data = new FieldReferenceViewData(reader);
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("ViewJoins") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        while (true)
                        {
                            if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Join") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                            {
                                ListJoin join = new ListJoin(reader);
                                joins.add(join);
                            }

                            if (reader.isEndElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("ViewJoins") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                            {
                                break;
                            }
                            else
                            {
                                reader.next();
                            }
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("ViewProjectedFields") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        while (true)
                        {
                            if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Field") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                            {
                                ProjectedField field = new ProjectedField(reader);
                                projectedFields.add(field);
                            }

                            if (reader.isEndElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("ViewProjectedFields") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                            {
                                break;
                            }
                            else
                            {
                                reader.next();
                            }
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("ViewQuery") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        query = new CamlQuery(reader, "ViewQuery");
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("ViewType") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            type = EnumUtil.parseViewType(stringValue);
                        }
                    }

                    if (reader.isEndElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("properties") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices/metadata"))
                    {
                        break;
                    }
                    else
                    {
                        reader.next();
                    }
                }
            }

            if (reader.isEndElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("entry") && reader.getNamespaceURI().equals("http://www.w3.org/2005/Atom"))
            {
                break;
            }
            else
            {
                reader.next();
            }
        }
    }

    String toCreateJSon()
    {
        String stringValue = "{ '__metadata': { 'type': 'SP.View' }";

        if (aggregationsStatus != null)
        {
            stringValue += ", 'AggregationsStatus': '" + Util.encodeJson(aggregationsStatus) + "'";
        }

        if (isDefaultView)
        {
            stringValue += ", 'DefaultView': true";
        }

        if (isDefaultViewForContentType)
        {
            stringValue += ", 'DefaultViewForContentType': true";
        }

        if (isEditorModified)
        {
            stringValue += ", 'EditorModified': true";
        }

        if (isHidden)
        {
            stringValue += ", 'Hidden': true";
        }

        if (isIncludeRootFolder)
        {
            stringValue += ", 'IncludeRootFolder': true";
        }

        if (jsLink != null)
        {
            stringValue += ", 'JsLink': '" + Util.encodeJson(jsLink) + "'";
        }

        if (listViewXml != null)
        {
            stringValue += ", 'ListViewXml': '" + Util.encodeJson(listViewXml) + "'";
        }

        if (isMobileDefaultView)
        {
            stringValue += ", 'MobileDefaultView': true";
        }

        if (isMobileView)
        {
            stringValue += ", 'MobileView': true";
        }

        if (isPaged)
        {
            stringValue += ", 'Paged': true";
        }

        if (rowLimit > 0)
        {
            stringValue += ", 'RowLimit': '" + rowLimit + "'";
        }

        if (scope != ViewScope.NONE)
        {
            stringValue += ", 'RowLimit': '" + EnumUtil.parseViewScope(scope) + "'";
        }

        if (title != null)
        {
            stringValue += ", 'Title': '" + Util.encodeJson(title) + "'";
        }

        if (toolbar != null)
        {
            stringValue += ", 'Toolbar': '" + Util.encodeJson(toolbar) + "'";
        }

        stringValue += " }";

        return stringValue;
    }

    String toUpdateJSon()
    {
        String stringValue = "{ '__metadata': { 'type': 'SP.View' }";

        if (aggregationsStatus != null && aggregationsStatus.length() > 0)
        {
            stringValue += ", 'AggregationsStatus': '" + Util.encodeJson(aggregationsStatus) + "'";
        }

        if (isDefaultView)
        {
            stringValue += ", 'DefaultView': true";
        }

        if (isDefaultViewForContentType)
        {
            stringValue += ", 'DefaultViewForContentType': true";
        }
        else        	
        {
            stringValue += ", 'DefaultViewForContentType': false";
        }

        if (isEditorModified)
        {
            stringValue += ", 'EditorModified': true";
        }
        else        	
        {
            stringValue += ", 'EditorModified': false";
        }

        if (isHidden)
        {
            stringValue += ", 'Hidden': true";
        }
        else        	
        {
            stringValue += ", 'Hidden': false";
        }

        if (isIncludeRootFolder)
        {
            stringValue += ", 'IncludeRootFolder': true";
        }
        else        	
        {
            stringValue += ", 'IncludeRootFolder': false";
        }

        if (jsLink != null && jsLink.length() > 0)
        {
            stringValue += ", 'JsLink': '" + Util.encodeJson(jsLink) + "'";
        }

        if (isMobileDefaultView)
        {
            stringValue += ", 'MobileDefaultView': true";
        }
        else        	
        {
            stringValue += ", 'MobileDefaultView': false";
        }

        if (isMobileView)
        {
            stringValue += ", 'MobileView': true";
        }
        else        	
        {
            stringValue += ", 'MobileView': false";
        }

        if (isPaged)
        {
            stringValue += ", 'Paged': true";
        }
        else        	
        {
            stringValue += ", 'Paged': false";
        }

        if (rowLimit > 0)
        {
            stringValue += ", 'RowLimit': '" + rowLimit + "'";
        }

        if (scope != ViewScope.NONE)
        {
            stringValue += ", 'RowLimit': '" + EnumUtil.parseViewScope(scope) + "'";
        }

        if (title != null && title.length() > 0)
        {
            stringValue += ", 'Title': '" + Util.encodeJson(title) + "'";
        }

        if (toolbar != null && toolbar.length() > 0)
        {
            stringValue += ", 'Toolbar': '" + Util.encodeJson(toolbar) + "'";
        }

        stringValue += " }";

        return stringValue;
    }

    /**
     * Gets the aggregation.
     *
     * @return the aggregation
     */
    public FieldReferenceAggregation getAggregation()
    {
        return aggregation;
    }

    /**
     * Sets the aggregation.
     *
     * @param aggregation the new aggregation
     */
    public void setAggregation(FieldReferenceAggregation aggregation)
    {
        this.aggregation = aggregation;
    }

    /**
     * Gets the aggregations status.
     *
     * @return the aggregations status
     */
    public String getAggregationsStatus()
    {
        return aggregationsStatus;
    }

    /**
     * Sets the aggregations status.
     *
     * @param aggregationsStatus the new aggregations status
     */
    public void setAggregationsStatus(String aggregationsStatus)
    {
        this.aggregationsStatus = aggregationsStatus;
    }

    /**
     * Gets the base view id.
     *
     * @return the base view id
     */
    public String getBaseViewId()
    {
        return baseViewId;
    }

    /**
     * Gets the content type id.
     *
     * @return the content type id
     */
    public String getContentTypeId()
    {
        return contentTypeId;
    }

    /**
     * Sets the content type id.
     *
     * @param contentTypeId the new content type id
     */
    public void setContentTypeId(String contentTypeId)
    {
        this.contentTypeId = contentTypeId;
    }

    /**
     * Checks if is default view.
     *
     * @return true, if is default view
     */
    public boolean isDefaultView()
    {
        return isDefaultView;
    }

    /**
     * Sets the default view.
     *
     * @param isDefaultView the new default view
     */
    public void setDefaultView(boolean isDefaultView)
    {
        this.isDefaultView = isDefaultView;
    }

    /**
     * Checks if is default view for content type.
     *
     * @return true, if is default view for content type
     */
    public boolean isDefaultViewForContentType()
    {
        return isDefaultViewForContentType;
    }

    /**
     * Sets the default view for content type.
     *
     * @param isDefaultViewForContentType the new default view for content type
     */
    public void setDefaultViewForContentType(boolean isDefaultViewForContentType)
    {
        this.isDefaultViewForContentType = isDefaultViewForContentType;
    }

    /**
     * Checks if is editor modified.
     *
     * @return true, if is editor modified
     */
    public boolean isEditorModified()
    {
        return isEditorModified;
    }

    /**
     * Sets the editor modified.
     *
     * @param isEditorModified the new editor modified
     */
    public void setEditorModified(boolean isEditorModified)
    {
        this.isEditorModified = isEditorModified;
    }

    /**
     * Gets the format.
     *
     * @return the format
     */
    public ViewFormat getFormat()
    {
        return format;
    }

    /**
     * Sets the format.
     *
     * @param format the new format
     */
    public void setFormat(ViewFormat format)
    {
        this.format = format;
    }

    /**
     * Checks if is hidden.
     *
     * @return true, if is hidden
     */
    public boolean isHidden()
    {
        return isHidden;
    }

    /**
     * Sets the hidden.
     *
     * @param isHidden the new hidden
     */
    public void setHidden(boolean isHidden)
    {
        this.isHidden = isHidden;
    }

    /**
     * Gets the html schema xml.
     *
     * @return the html schema xml
     */
    public String getHtmlSchemaXml()
    {
        return htmlSchemaXml;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public String getId()
    {
        return id;
    }

    /**
     * Gets the image url.
     *
     * @return the image url
     */
    public String getImageUrl()
    {
        return imageUrl;
    }

    /**
     * Checks if is include root folder.
     *
     * @return true, if is include root folder
     */
    public boolean isIncludeRootFolder()
    {
        return isIncludeRootFolder;
    }

    /**
     * Sets the include root folder.
     *
     * @param isIncludeRootFolder the new include root folder
     */
    public void setIncludeRootFolder(boolean isIncludeRootFolder)
    {
        this.isIncludeRootFolder = isIncludeRootFolder;
    }

    /**
     * Gets the JS link.
     *
     * @return the JS link
     */
    public String getJSLink()
    {
        return jsLink;
    }

    /**
     * Sets the JS link.
     *
     * @param jsLink the new JS link
     */
    public void setJSLink(String jsLink)
    {
        this.jsLink = jsLink;
    }

    /**
     * Gets the list view xml.
     *
     * @return the list view xml
     */
    public String getListViewXml()
    {
        return listViewXml;
    }

    /**
     * Sets the list view xml.
     *
     * @param listViewXml the new list view xml
     */
    public void setListViewXml(String listViewXml)
    {
        this.listViewXml = listViewXml;
    }

    /**
     * Gets the method.
     *
     * @return the method
     */
    public ViewMethod getMethod()
    {
        return method;
    }

    /**
     * Sets the method.
     *
     * @param method the new method
     */
    public void setMethod(ViewMethod method)
    {
        this.method = method;
    }

    /**
     * Checks if is mobile default view.
     *
     * @return true, if is mobile default view
     */
    public boolean isMobileDefaultView()
    {
        return isMobileDefaultView;
    }

    /**
     * Sets the mobile default view.
     *
     * @param isMobileDefaultView the new mobile default view
     */
    public void setMobileDefaultView(boolean isMobileDefaultView)
    {
        this.isMobileDefaultView = isMobileDefaultView;
    }

    /**
     * Checks if is mobile view.
     *
     * @return true, if is mobile view
     */
    public boolean isMobileView()
    {
        return isMobileView;
    }

    /**
     * Sets the mobile view.
     *
     * @param isMobileView the new mobile view
     */
    public void setMobileView(boolean isMobileView)
    {
        this.isMobileView = isMobileView;
    }

    /**
     * Gets the moderation type.
     *
     * @return the moderation type
     */
    public String getModerationType()
    {
        return moderationType;
    }

    /**
     * Checks if is ordered view.
     *
     * @return true, if is ordered view
     */
    public boolean isOrderedView()
    {
        return isOrderedView;
    }

    /**
     * Checks if is paged.
     *
     * @return true, if is paged
     */
    public boolean isPaged()
    {
        return isPaged;
    }

    /**
     * Sets the paged.
     *
     * @param isPaged the new paged
     */
    public void setPaged(boolean isPaged)
    {
        this.isPaged = isPaged;
    }

    /**
     * Checks if is personal view.
     *
     * @return true, if is personal view
     */
    public boolean isPersonalView()
    {
        return isPersonalView;
    }

    /**
     * Checks if is read only view.
     *
     * @return true, if is read only view
     */
    public boolean isReadOnlyView()
    {
        return isReadOnlyView;
    }

    /**
     * Checks if is requires client integration.
     *
     * @return true, if is requires client integration
     */
    public boolean isRequiresClientIntegration()
    {
        return requiresClientIntegration;
    }

    /**
     * Gets the row limit.
     *
     * @return the row limit
     */
    public int getRowLimit()
    {
        return rowLimit;
    }

    /**
     * Sets the row limit.
     *
     * @param rowLimit the new row limit
     */
    public void setRowLimit(int rowLimit)
    {
        this.rowLimit = rowLimit;
    }

    /**
     * Gets the scope.
     *
     * @return the scope
     */
    public ViewScope getScope()
    {
        return scope;
    }

    /**
     * Sets the scope.
     *
     * @param scope the new scope
     */
    public void setScope(ViewScope scope)
    {
        this.scope = scope;
    }

    /**
     * Gets the server relative url.
     *
     * @return the server relative url
     */
    public String getServerRelativeUrl()
    {
        return serverRelativeUrl;
    }

    /**
     * Gets the style id.
     *
     * @return the style id
     */
    public String getStyleId()
    {
        return styleId;
    }

    /**
     * Checks if is threaded.
     *
     * @return true, if is threaded
     */
    public boolean isThreaded()
    {
        return isThreaded;
    }

    /**
     * Gets the title.
     *
     * @return the title
     */
    public String getTitle()
    {
        return title;
    }

    /**
     * Sets the title.
     *
     * @param title the new title
     */
    public void setTitle(String title)
    {
        this.title = title;
    }

    /**
     * Gets the toolbar.
     *
     * @return the toolbar
     */
    public String getToolbar()
    {
        return toolbar;
    }

    /**
     * Sets the toolbar.
     *
     * @param toolbar the new toolbar
     */
    public void setToolbar(String toolbar)
    {
        this.toolbar = toolbar;
    }

    /**
     * Gets the toolbar template name.
     *
     * @return the toolbar template name
     */
    public String getToolbarTemplateName()
    {
        return toolbarTemplateName;
    }

    /**
     * Gets the data.
     *
     * @return the data
     */
    public FieldReferenceViewData getData()
    {
        return data;
    }

    /**
     * Sets the data.
     *
     * @param data the new data
     */
    public void setData(FieldReferenceViewData data)
    {
        this.data = data;
    }

    /**
     * Gets the joins.
     *
     * @return the joins
     */
    public List<ListJoin> getJoins()
    {
        return joins;
    }

    /**
     * Gets the projected fields.
     *
     * @return the projected fields
     */
    public List<ProjectedField> getProjectedFields()
    {
        return projectedFields;
    }

    /**
     * Gets the query.
     *
     * @return the query
     */
    public CamlQuery getQuery()
    {
        return query;
    }

    /**
     * Sets the query.
     *
     * @param query the new query
     */
    public void setQuery(CamlQuery query)
    {
        this.query = query;
    }

    /**
     * Gets the type.
     *
     * @return the type
     */
    public ViewType getType()
    {
        return type;
    }
}
