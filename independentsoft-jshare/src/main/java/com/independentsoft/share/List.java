package com.independentsoft.share;

import java.io.InputStream;
import java.text.ParseException;
import java.util.Date;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

/**
 * The Class List.
 */
public class List {

    private boolean allowContentTypes;
    private ListTemplateType baseTemplate = ListTemplateType.GENERIC_LIST;
    private ListBaseType baseType = ListBaseType.GENERIC_LIST;
    private boolean hasContentTypesEnabled;
    private Date createdTime;
    private String defaultContentApprovalWorkflowId;
    private String description;
    private Direction direction = Direction.NONE;
    private String documentTemplateUrl;
    private DraftVisibilityType draftVersionVisibility = DraftVisibilityType.NONE;
    private boolean enableAttachments;
    private boolean enableFolderCreation;
    private boolean enableMinorVersions;
    private boolean enableModeration;
    private boolean enableVersioning;
    private String entityTypeName;
    private boolean forceCheckout;
    private boolean hasExternalDataSource;
    private boolean isHidden;
    private String id;
    private String imageUrl;
    private boolean isIrmEnabled;
    private boolean isIrmExpire;
    private boolean isIrmReject;
    private boolean isApplicationList;
    private boolean isCatalog;
    private boolean isPrivate;
    private int itemCount;
    private Date lastItemDeletedDate;
    private Date lastItemModifiedDate;
    private String listItemEntityTypeFullName;
    private boolean isMultipleDataList;
    private boolean isNoCrawl;
    private String parentWebUrl;
    private boolean canServerTemplateCreateFolders;
    private String templateFeatureId;
    private String title;

    /**
     * Instantiates a new list.
     */
    public List()
    {
    }

    List(InputStream inputStream) throws XMLStreamException, ParseException
    {
        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        XMLStreamReader reader = xmlInputFactory.createXMLStreamReader(inputStream);

        parse(reader);
    }

    List(XMLStreamReader reader) throws XMLStreamException, ParseException
    {
        parse(reader);
    }

    private void parse(XMLStreamReader reader) throws XMLStreamException, ParseException
    {
        while (reader.hasNext())
        {
            if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("properties") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices/metadata"))
            {
                while (reader.hasNext())
                {
                    if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("AllowContentTypes") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            allowContentTypes = Boolean.parseBoolean(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("BaseTemplate") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            baseTemplate = EnumUtil.parseListTemplateType(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("BaseType") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            baseType = EnumUtil.parseListBaseType(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("ContentTypesEnabled") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            hasContentTypesEnabled = Boolean.parseBoolean(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Created") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            createdTime = Util.parseDate(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("DefaultContentApprovalWorkflowId") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        defaultContentApprovalWorkflowId = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Description") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        description = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Direction") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            direction = EnumUtil.parseDirection(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("DocumentTemplateUrl") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        documentTemplateUrl = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("DraftVersionVisibility") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            draftVersionVisibility = EnumUtil.parseDraftVisibilityType(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("EnableAttachments") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            enableAttachments = Boolean.parseBoolean(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("EnableFolderCreation") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            enableFolderCreation = Boolean.parseBoolean(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("EnableMinorVersions") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            enableMinorVersions = Boolean.parseBoolean(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("EnableModeration") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            enableModeration = Boolean.parseBoolean(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("EnableVersioning") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            enableVersioning = Boolean.parseBoolean(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("EntityTypeName") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        entityTypeName = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("ForceCheckout") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            forceCheckout = Boolean.parseBoolean(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("HasExternalDataSource") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            hasExternalDataSource = Boolean.parseBoolean(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Hidden") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            isHidden = Boolean.parseBoolean(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Id") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        id = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("ImageUrl") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        imageUrl = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("IrmEnabled") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            isIrmEnabled = Boolean.parseBoolean(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("IrmExpire") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            isIrmExpire = Boolean.parseBoolean(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("IrmReject") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            isIrmReject = Boolean.parseBoolean(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("IsApplicationList") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            isApplicationList = Boolean.parseBoolean(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("IsCatalog") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            isCatalog = Boolean.parseBoolean(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("IsPrivate") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            isPrivate = Boolean.parseBoolean(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("ItemCount") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            itemCount = Integer.parseInt(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("LastItemDeletedDate") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            lastItemDeletedDate = Util.parseDate(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("LastItemModifiedDate") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            lastItemModifiedDate = Util.parseDate(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("ListItemEntityTypeFullName") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        listItemEntityTypeFullName = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("MultipleDataList") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            isMultipleDataList = Boolean.parseBoolean(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("NoCrawl") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            isNoCrawl = Boolean.parseBoolean(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("ParentWebUrl") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        parentWebUrl = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("ServerTemplateCanCreateFolders") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            canServerTemplateCreateFolders = Boolean.parseBoolean(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("TemplateFeatureId") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        templateFeatureId = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Title") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        title = reader.getElementText();
                    }

                    if (reader.isEndElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("properties") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices/metadata"))
                    {
                        break;
                    }
                    else
                    {
                        reader.next();
                    }
                }
            }

            if (reader.isEndElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("entry") && reader.getNamespaceURI().equals("http://www.w3.org/2005/Atom"))
            {
                break;
            }
            else
            {
                reader.next();
            }
        }
    }

    String toCreateJSon()
    {
        String stringValue = "{ '__metadata':{ 'type': 'SP.List' }";

        stringValue += ", 'BaseTemplate':'" + EnumUtil.parseListTemplateType(baseTemplate) + "'";

        if (hasContentTypesEnabled)
        {
            stringValue += ", 'ContentTypesEnabled': true";
        }

        if (defaultContentApprovalWorkflowId != null && defaultContentApprovalWorkflowId.length() > 0)
        {
            stringValue += ", 'DefaultContentApprovalWorkflowId':'" + Util.encodeJson(defaultContentApprovalWorkflowId) + "'";
        }

        if (description != null && description.length() > 0)
        {
            stringValue += ", 'Description':'" + Util.encodeJson(description) + "'";
        }

        if (direction != Direction.NONE)
        {
            stringValue += ", 'Direction':'" + EnumUtil.parseDirection(direction) + "'";
        }

        if (documentTemplateUrl != null && documentTemplateUrl.length() > 0)
        {
            stringValue += ", 'DocumentTemplateUrl':'" + Util.encodeJson(documentTemplateUrl) + "'";
        }

        if (draftVersionVisibility != DraftVisibilityType.NONE)
        {
            stringValue += ", 'DraftVersionVisibility':'" + EnumUtil.parseDraftVisibilityType(draftVersionVisibility) + "'";
        }

        if (enableAttachments)
        {
            stringValue += ", 'EnableAttachments': true";
        }

        if (enableFolderCreation)
        {
            stringValue += ", 'EnableFolderCreation': true";
        }

        if (enableMinorVersions)
        {
            stringValue += ", 'EnableMinorVersions': true";
        }

        if (enableModeration)
        {
            stringValue += ", 'EnableModeration': true";
        }

        if (enableVersioning)
        {
            stringValue += ", 'EnableVersioning': true";
        }

        if (forceCheckout)
        {
            stringValue += ", 'ForceCheckout': true";
        }

        if (isHidden)
        {
            stringValue += ", 'Hidden': true";
        }

        if (isIrmEnabled)
        {
            stringValue += ", 'IrmEnabled': true";
        }

        if (isIrmExpire)
        {
            stringValue += ", 'IrmExpire': true";
        }

        if (isIrmReject)
        {
            stringValue += ", 'IrmReject': true";
        }

        if (isApplicationList)
        {
            stringValue += ", 'IsApplicationList': true";
        }

        if (isMultipleDataList)
        {
            stringValue += ", 'MultipleDataList': true";
        }

        if (isNoCrawl)
        {
            stringValue += ", 'NoCrawl': true";
        }

        if (title != null && title.length() > 0)
        {
            stringValue += ", 'Title':'" + Util.encodeJson(title) + "'";
        }

        stringValue += " }";

        return stringValue;
    }

    /**
     * Checks for content types allowed.
     *
     * @return true, if successful
     */
    public boolean hasContentTypesAllowed()
    {
        return allowContentTypes;
    }

    /**
     * Gets the base template.
     *
     * @return the base template
     */
    public ListTemplateType getBaseTemplate()
    {
        return baseTemplate;
    }

    /**
     * Sets the base template.
     *
     * @param baseTemplate the new base template
     */
    public void setBaseTemplate(ListTemplateType baseTemplate)
    {
        this.baseTemplate = baseTemplate;
    }

    /**
     * Gets the base type.
     *
     * @return the base type
     */
    public ListBaseType getBaseType()
    {
        return baseType;
    }

    /**
     * Checks for content types enabled.
     *
     * @return true, if successful
     */
    public boolean hasContentTypesEnabled()
    {
        return hasContentTypesEnabled;
    }

    /**
     * Sets the content types enabled.
     *
     * @param hasContentTypesEnabled the new content types enabled
     */
    public void setContentTypesEnabled(boolean hasContentTypesEnabled)
    {
        this.hasContentTypesEnabled = hasContentTypesEnabled;
    }

    /**
     * Gets the created time.
     *
     * @return the created time
     */
    public Date getCreatedTime()
    {
        return createdTime;
    }

    /**
     * Gets the default content approval workflow id.
     *
     * @return the default content approval workflow id
     */
    public String getDefaultContentApprovalWorkflowId()
    {
        return defaultContentApprovalWorkflowId;
    }

    /**
     * Sets the default content approval workflow id.
     *
     * @param defaultContentApprovalWorkflowId the new default content approval workflow id
     */
    public void setDefaultContentApprovalWorkflowId(String defaultContentApprovalWorkflowId)
    {
        this.defaultContentApprovalWorkflowId = defaultContentApprovalWorkflowId;
    }

    /**
     * Gets the description.
     *
     * @return the description
     */
    public String getDescription()
    {
        return description;
    }

    /**
     * Sets the description.
     *
     * @param description the new description
     */
    public void setDescription(String description)
    {
        this.description = description;
    }

    /**
     * Gets the direction.
     *
     * @return the direction
     */
    public Direction getDirection()
    {
        return direction;
    }

    /**
     * Sets the direction.
     *
     * @param direction the new direction
     */
    public void setDirection(Direction direction)
    {
        this.direction = direction;
    }

    /**
     * Gets the document template url.
     *
     * @return the document template url
     */
    public String getDocumentTemplateUrl()
    {
        return documentTemplateUrl;
    }

    /**
     * Sets the document template url.
     *
     * @param documentTemplateUrl the new document template url
     */
    public void setDocumentTemplateUrl(String documentTemplateUrl)
    {
        this.documentTemplateUrl = documentTemplateUrl;
    }

    /**
     * Gets the draft version visibility.
     *
     * @return the draft version visibility
     */
    public DraftVisibilityType getDraftVersionVisibility()
    {
        return draftVersionVisibility;
    }

    /**
     * Sets the draft version visibility.
     *
     * @param draftVersionVisibility the new draft version visibility
     */
    public void setDraftVersionVisibility(DraftVisibilityType draftVersionVisibility)
    {
        this.draftVersionVisibility = draftVersionVisibility;
    }

    /**
     * Checks for attachments enabled.
     *
     * @return true, if successful
     */
    public boolean hasAttachmentsEnabled()
    {
        return enableAttachments;
    }

    /**
     * Enable attachments.
     *
     * @param enableAttachments the enable attachments
     */
    public void enableAttachments(boolean enableAttachments)
    {
        this.enableAttachments = enableAttachments;
    }

    /**
     * Checks if is folder creation enabled.
     *
     * @return true, if is folder creation enabled
     */
    public boolean isFolderCreationEnabled()
    {
        return enableFolderCreation;
    }

    /**
     * Enable folder creation.
     *
     * @param enableFolderCreation the enable folder creation
     */
    public void enableFolderCreation(boolean enableFolderCreation)
    {
        this.enableFolderCreation = enableFolderCreation;
    }

    /**
     * Checks for minor versions enabled.
     *
     * @return true, if successful
     */
    public boolean hasMinorVersionsEnabled()
    {
        return enableMinorVersions;
    }

    /**
     * Enable minor versions.
     *
     * @param enableMinorVersions the enable minor versions
     */
    public void enableMinorVersions(boolean enableMinorVersions)
    {
        this.enableMinorVersions = enableMinorVersions;
    }

    /**
     * Checks if is moderation enabled.
     *
     * @return true, if is moderation enabled
     */
    public boolean isModerationEnabled()
    {
        return enableModeration;
    }

    /**
     * Enable moderation.
     *
     * @param enableModeration the enable moderation
     */
    public void enableModeration(boolean enableModeration)
    {
        this.enableModeration = enableModeration;
    }

    /**
     * Checks if is versioning enabled.
     *
     * @return true, if is versioning enabled
     */
    public boolean isVersioningEnabled()
    {
        return enableVersioning;
    }

    /**
     * Enable versioning.
     *
     * @param enableVersioning the enable versioning
     */
    public void enableVersioning(boolean enableVersioning)
    {
        this.enableVersioning = enableVersioning;
    }

    /**
     * Gets the entity type name.
     *
     * @return the entity type name
     */
    public String getEntityTypeName()
    {
        return entityTypeName;
    }

    /**
     * Checks if is force checkout.
     *
     * @return true, if is force checkout
     */
    public boolean isForceCheckout()
    {
        return forceCheckout;
    }

    /**
     * Checks for external data source.
     *
     * @return true, if successful
     */
    public boolean hasExternalDataSource()
    {
        return hasExternalDataSource;
    }

    /**
     * Checks if is hidden.
     *
     * @return true, if is hidden
     */
    public boolean isHidden()
    {
        return isHidden;
    }

    /**
     * Sets the hidden.
     *
     * @param isHidden the new hidden
     */
    public void setHidden(boolean isHidden)
    {
        this.isHidden = isHidden;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public String getId()
    {
        return id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(String id)
    {
        this.id = id;
    }

    /**
     * Gets the image url.
     *
     * @return the image url
     */
    public String getImageUrl()
    {
        return imageUrl;
    }

    /**
     * Sets the image url.
     *
     * @param imageUrl the new image url
     */
    public void setImageUrl(String imageUrl)
    {
        this.imageUrl = imageUrl;
    }

    /**
     * Checks if is irm enabled.
     *
     * @return true, if is irm enabled
     */
    public boolean isIrmEnabled()
    {
        return isIrmEnabled;
    }

    /**
     * Sets the irm enabled.
     *
     * @param isIrmEnabled the new irm enabled
     */
    public void setIrmEnabled(boolean isIrmEnabled)
    {
        this.isIrmEnabled = isIrmEnabled;
    }

    /**
     * Checks if is irm expire.
     *
     * @return true, if is irm expire
     */
    public boolean isIrmExpire()
    {
        return isIrmExpire;
    }

    /**
     * Sets the irm expire.
     *
     * @param isIrmExpire the new irm expire
     */
    public void setIrmExpire(boolean isIrmExpire)
    {
        this.isIrmExpire = isIrmExpire;
    }

    /**
     * Checks if is irm reject.
     *
     * @return true, if is irm reject
     */
    public boolean isIrmReject()
    {
        return isIrmReject;
    }

    /**
     * Sets the irm reject.
     *
     * @param isIrmReject the new irm reject
     */
    public void setIrmReject(boolean isIrmReject)
    {
        this.isIrmReject = isIrmReject;
    }

    /**
     * Checks if is application list.
     *
     * @return true, if is application list
     */
    public boolean isApplicationList()
    {
        return isApplicationList;
    }

    /**
     * Sets the application list.
     *
     * @param isApplicationList the new application list
     */
    public void setApplicationList(boolean isApplicationList)
    {
        this.isApplicationList = isApplicationList;
    }

    /**
     * Checks if is catalog.
     *
     * @return true, if is catalog
     */
    public boolean isCatalog()
    {
        return isCatalog;
    }

    /**
     * Sets the catalog.
     *
     * @param isCatalog the new catalog
     */
    public void setCatalog(boolean isCatalog)
    {
        this.isCatalog = isCatalog;
    }

    /**
     * Checks if is private.
     *
     * @return true, if is private
     */
    public boolean isPrivate()
    {
        return isPrivate;
    }

    /**
     * Sets the private.
     *
     * @param isPrivate the new private
     */
    public void setPrivate(boolean isPrivate)
    {
        this.isPrivate = isPrivate;
    }

    /**
     * Gets the item count.
     *
     * @return the item count
     */
    public int getItemCount()
    {
        return itemCount;
    }

    /**
     * Gets the last item deleted date.
     *
     * @return the last item deleted date
     */
    public Date getLastItemDeletedDate()
    {
        return lastItemDeletedDate;
    }

    /**
     * Gets the last item modified date.
     *
     * @return the last item modified date
     */
    public Date getLastItemModifiedDate()
    {
        return lastItemModifiedDate;
    }

    /**
     * Gets the list item entity type full name.
     *
     * @return the list item entity type full name
     */
    public String getListItemEntityTypeFullName()
    {
        return listItemEntityTypeFullName;
    }

    /**
     * Checks if is multiple data list.
     *
     * @return true, if is multiple data list
     */
    public boolean isMultipleDataList()
    {
        return isMultipleDataList;
    }

    /**
     * Sets the multiple data list.
     *
     * @param isMultipleDataList the new multiple data list
     */
    public void setMultipleDataList(boolean isMultipleDataList)
    {
        this.isMultipleDataList = isMultipleDataList;
    }

    /**
     * Checks if is no crawl.
     *
     * @return true, if is no crawl
     */
    public boolean isNoCrawl()
    {
        return isNoCrawl;
    }

    /**
     * Sets the no crawl.
     *
     * @param isNoCrawl the new no crawl
     */
    public void setNoCrawl(boolean isNoCrawl)
    {
        this.isNoCrawl = isNoCrawl;
    }

    /**
     * Gets the parent web url.
     *
     * @return the parent web url
     */
    public String getParentWebUrl()
    {
        return parentWebUrl;
    }

    /**
     * Can server template create folders.
     *
     * @return true, if successful
     */
    public boolean canServerTemplateCreateFolders()
    {
        return canServerTemplateCreateFolders;
    }

    /**
     * Gets the template feature id.
     *
     * @return the template feature id
     */
    public String getTemplateFeatureId()
    {
        return templateFeatureId;
    }

    /**
     * Gets the title.
     *
     * @return the title
     */
    public String getTitle()
    {
        return title;
    }

    /**
     * Sets the title.
     *
     * @param title the new title
     */
    public void setTitle(String title)
    {
        this.title = title;
    }
}
