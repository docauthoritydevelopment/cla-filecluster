package com.independentsoft.share;

import java.util.ArrayList;
import java.util.List;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

/**
 * The Class CamlViewFields.
 */
public class CamlViewFields {

    private List<String> fieldReferences = new ArrayList<String>();
    private boolean properties;

    /**
     * Instantiates a new caml view fields.
     */
    public CamlViewFields()
    {
    }

    CamlViewFields(XMLStreamReader reader) throws XMLStreamException
    {
        parse(reader);
    }

    private void parse(XMLStreamReader reader) throws XMLStreamException
    {
        while (reader.hasNext())
        {
            if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("viewFields") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                String propertiesAttribute = reader.getAttributeValue(null, "Properties");

                if (propertiesAttribute != null && propertiesAttribute.length() > 0)
                {
                    properties = Boolean.parseBoolean(propertiesAttribute);
                }

                while (reader.hasNext())
                {
                    if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("FieldRef") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String fieldRef = reader.getAttributeValue(null, "Name");
                        fieldReferences.add(fieldRef);
                    }
                }
            }

            if (reader.isEndElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("viewFields") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                break;
            }
            else
            {
                reader.next();
            }
        }
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        String attributes = "";

        if (properties)
        {
            attributes += " Properties=\"true\"";
        }

        String stringValue = "<ViewFields" + attributes + ">";

        for (String fieldRef : fieldReferences)
        {
            stringValue += "<FieldRef Name=\"" + fieldRef + "\"/>";
        }

        stringValue += "</ViewFields>";

        return stringValue;
    }

    /**
     * Gets the field references.
     *
     * @return the field references
     */
    public List<String> getFieldReferences()
    {
        return fieldReferences;
    }

    /**
     * Gets the properties.
     *
     * @return the properties
     */
    public boolean getProperties()
    {
        return properties;
    }

    /**
     * Sets the properties.
     *
     * @param properties the new properties
     */
    public void setProperties(boolean properties)
    {
        this.properties = properties;
    }
}
