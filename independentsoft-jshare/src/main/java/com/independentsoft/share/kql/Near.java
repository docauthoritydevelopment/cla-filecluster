package com.independentsoft.share.kql;

/**
 * The Class Near.
 */
public class Near implements IRestriction {

    private String leftExpression;
    private String rightExpression;
    private int maximumDistance = -1;

    /**
     * Instantiates a new near.
     *
     * @param leftExpression the left expression
     * @param rightExpression the right expression
     */
    public Near(String leftExpression, String rightExpression)
    {
        if (leftExpression == null)
        {
            throw new IllegalArgumentException("leftExpression");
        }

        if (rightExpression == null)
        {
            throw new IllegalArgumentException("rightExpression");
        }

        this.leftExpression = leftExpression;
        this.rightExpression = rightExpression;
    }

    /**
     * Instantiates a new near.
     *
     * @param leftExpression the left expression
     * @param rightExpression the right expression
     * @param maximumDistance the maximum distance
     */
    public Near(String leftExpression, String rightExpression, int maximumDistance)
    {
        if (leftExpression == null)
        {
            throw new IllegalArgumentException("leftExpression");
        }

        if (rightExpression == null)
        {
            throw new IllegalArgumentException("rightExpression");
        }

        this.leftExpression = leftExpression;
        this.rightExpression = rightExpression;
        this.maximumDistance = maximumDistance;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        String stringValue = "";

        if (maximumDistance > -1)
        {
            stringValue += "(\"" + leftExpression + "\" NEAR(n=" + maximumDistance + ") \"" + rightExpression + "\")";
        }
        else
        {
            stringValue += "(\"" + leftExpression + "\" NEAR \"" + rightExpression + "\")";
        }

        return stringValue;
    }
}
