package com.independentsoft.share.kql;

import java.util.Date;

import com.independentsoft.share.Util;

/**
 * The Class IsWithinRange.
 */
public class IsWithinRange implements IRestriction {

    private String propertyName;
    private String startValue;
    private String endValue;

    /**
     * Instantiates a new checks if is within range.
     *
     * @param propertyName the property name
     * @param startValue the start value
     * @param endValue the end value
     */
    public IsWithinRange(String propertyName, long startValue, long endValue)
    {
        if (propertyName == null)
        {
            throw new IllegalArgumentException("propertyName");
        }

        this.propertyName = propertyName;
        this.startValue = Long.toString(startValue);
        this.endValue = Long.toString(endValue);
    }

    /**
     * Instantiates a new checks if is within range.
     *
     * @param propertyName the property name
     * @param startValue the start value
     * @param endValue the end value
     */
    public IsWithinRange(String propertyName, double startValue, double endValue)
    {
        if (propertyName == null)
        {
            throw new IllegalArgumentException("propertyName");
        }

        this.propertyName = propertyName;
        this.startValue = Double.toString(startValue);
        this.endValue = Double.toString(endValue);
    }

    /**
     * Instantiates a new checks if is within range.
     *
     * @param propertyName the property name
     * @param startValue the start value
     * @param endValue the end value
     */
    public IsWithinRange(String propertyName, Date startValue, Date endValue)
    {
        if (propertyName == null)
        {
            throw new IllegalArgumentException("propertyName");
        }

        if (startValue == null)
        {
            throw new IllegalArgumentException("startValue");
        }

        if (endValue == null)
        {
            throw new IllegalArgumentException("endValue");
        }

        this.propertyName = propertyName;
        this.startValue = Util.toUniversalTime(startValue);
        this.endValue = Util.toUniversalTime(endValue);
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        String stringValue = propertyName + " : " + startValue + ".." + endValue;

        return stringValue;
    }
}
