package com.independentsoft.share.kql;

import java.util.ArrayList;
import java.util.List;

/**
 * The Class Or.
 */
public class Or implements IRestriction {

    private String leftExpression;
    private String rightExpression;
    private List<IRestriction> restrictions = new ArrayList<IRestriction>();

    /**
     * Instantiates a new or.
     *
     * @param leftExpression the left expression
     * @param rightExpression the right expression
     */
    public Or(String leftExpression, String rightExpression)
    {
        if (leftExpression == null)
        {
            throw new IllegalArgumentException("leftExpression");
        }

        if (rightExpression == null)
        {
            throw new IllegalArgumentException("rightExpression");
        }

        this.leftExpression = leftExpression;
        this.rightExpression = rightExpression;
    }

    /**
     * Instantiates a new or.
     *
     * @param restriction1 the restriction1
     * @param restriction2 the restriction2
     */
    public Or(IRestriction restriction1, IRestriction restriction2)
    {
        if (restriction1 == null)
        {
            throw new IllegalArgumentException("restriction1");
        }

        if (restriction2 == null)
        {
            throw new IllegalArgumentException("restriction2");
        }

        this.restrictions.add(restriction1);
        this.restrictions.add(restriction2);
    }

    /**
     * Instantiates a new or.
     *
     * @param restriction1 the restriction1
     * @param restriction2 the restriction2
     * @param restriction3 the restriction3
     */
    public Or(IRestriction restriction1, IRestriction restriction2, IRestriction restriction3)
    {
        if (restriction1 == null)
        {
            throw new IllegalArgumentException("restriction1");
        }

        if (restriction2 == null)
        {
            throw new IllegalArgumentException("restriction2");
        }

        if (restriction3 == null)
        {
            throw new IllegalArgumentException("restriction3");
        }

        this.restrictions.add(restriction1);
        this.restrictions.add(restriction2);
        this.restrictions.add(restriction3);
    }

    /**
     * Instantiates a new or.
     *
     * @param restriction1 the restriction1
     * @param restriction2 the restriction2
     * @param restriction3 the restriction3
     * @param restriction4 the restriction4
     */
    public Or(IRestriction restriction1, IRestriction restriction2, IRestriction restriction3, IRestriction restriction4)
    {
        if (restriction1 == null)
        {
            throw new IllegalArgumentException("restriction1");
        }

        if (restriction2 == null)
        {
            throw new IllegalArgumentException("restriction2");
        }

        if (restriction3 == null)
        {
            throw new IllegalArgumentException("restriction3");
        }

        if (restriction4 == null)
        {
            throw new IllegalArgumentException("restriction4");
        }

        this.restrictions.add(restriction1);
        this.restrictions.add(restriction2);
        this.restrictions.add(restriction3);
        this.restrictions.add(restriction4);
    }

    /**
     * Instantiates a new or.
     *
     * @param restrictions the restrictions
     */
    public Or(List<IRestriction> restrictions)
    {
        if (restrictions == null)
        {
            throw new IllegalArgumentException("restrictions");
        }

        this.restrictions = restrictions;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        String stringValue = "";

        if (leftExpression != null && rightExpression != null)
        {
            stringValue += "(\"" + leftExpression + "\" OR \"" + rightExpression + "\")";
        }
        else if (restrictions.size() > 0)
        {
            stringValue += "(";

            for(int i=0; i < restrictions.size(); i++)
            {
                stringValue += restrictions.get(i).toString();

                if (i < restrictions.size() - 1)
                {
                    stringValue += " OR ";
                }
            }

            stringValue += ")";
        }

        return stringValue;
    }
}
