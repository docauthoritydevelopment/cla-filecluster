package com.independentsoft.share.kql;

/**
 * The Class OrderedNear.
 */
public class OrderedNear implements IRestriction {

    private String leftExpression;
    private String rightExpression;
    private int maximumDistance = -1;

    /**
     * Instantiates a new ordered near.
     *
     * @param leftExpression the left expression
     * @param rightExpression the right expression
     */
    public OrderedNear(String leftExpression, String rightExpression)
    {
        if (leftExpression == null)
        {
            throw new IllegalArgumentException("leftExpression");
        }

        if (rightExpression == null)
        {
            throw new IllegalArgumentException("rightExpression");
        }

        this.leftExpression = leftExpression;
        this.rightExpression = rightExpression;
    }

    /**
     * Instantiates a new ordered near.
     *
     * @param leftExpression the left expression
     * @param rightExpression the right expression
     * @param maximumDistance the maximum distance
     */
    public OrderedNear(String leftExpression, String rightExpression, int maximumDistance)
    {
        if (leftExpression == null)
        {
            throw new IllegalArgumentException("leftExpression");
        }

        if (rightExpression == null)
        {
            throw new IllegalArgumentException("rightExpression");
        }

        this.leftExpression = leftExpression;
        this.rightExpression = rightExpression;
        this.maximumDistance = maximumDistance;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        String stringValue = "";

        if (maximumDistance > -1)
        {
            stringValue += "(\"" + leftExpression + "\" ONEAR(n=" + maximumDistance + ") \"" + rightExpression + "\")";
        }
        else
        {
            stringValue += "(\"" + leftExpression + "\" ONEAR \"" + rightExpression + "\")";
        }

        return stringValue;
    }
}
