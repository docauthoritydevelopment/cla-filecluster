package com.independentsoft.share.kql;

import java.util.Date;

import com.independentsoft.share.Util;

/**
 * The Class IsLessThan.
 */
public class IsLessThan implements IRestriction {

    private String propertyName;
    private String value;

    /**
     * Instantiates a new checks if is less than.
     *
     * @param propertyName the property name
     * @param value the value
     */
    public IsLessThan(String propertyName, long value)
    {
        if (propertyName == null)
        {
            throw new IllegalArgumentException("propertyName");
        }

        this.propertyName = propertyName;
        this.value = Long.toString(value);
    }

    /**
     * Instantiates a new checks if is less than.
     *
     * @param propertyName the property name
     * @param value the value
     */
    public IsLessThan(String propertyName, double value)
    {
        if (propertyName == null)
        {
            throw new IllegalArgumentException("propertyName");
        }

        this.propertyName = propertyName;
        this.value = Double.toString(value);
    }

    /**
     * Instantiates a new checks if is less than.
     *
     * @param propertyName the property name
     * @param value the value
     */
    public IsLessThan(String propertyName, Date value)
    {
        if (propertyName == null)
        {
            throw new IllegalArgumentException("propertyName");
        }

        if (value == null)
        {
            throw new IllegalArgumentException("value");
        }

        this.propertyName = propertyName;
        this.value = Util.toUniversalTime(value);
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        String stringValue = propertyName + " < " + value;

        return stringValue;
    }
}
