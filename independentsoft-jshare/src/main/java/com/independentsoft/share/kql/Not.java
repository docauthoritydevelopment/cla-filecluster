package com.independentsoft.share.kql;

/**
 * The Class Not.
 */
public class Not implements IRestriction {

    private String expression;
    private IRestriction restriction;

    /**
     * Instantiates a new not.
     *
     * @param expression the expression
     */
    public Not(String expression)
    {
        if (expression == null)
        {
            throw new IllegalArgumentException("expression");
        }

        this.expression = expression;
    }

    /**
     * Instantiates a new not.
     *
     * @param restriction the restriction
     */
    public Not(IRestriction restriction)
    {
        if (restriction == null)
        {
            throw new IllegalArgumentException("restriction");
        }

        this.restriction = restriction;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        String stringValue = "";

        if (expression != null)
        {
            stringValue += "NOT \"" + expression + "\"";
        }
        else if (restriction != null)
        {
            stringValue += "NOT " + restriction.toString();
        }

        return stringValue;
    }
}
