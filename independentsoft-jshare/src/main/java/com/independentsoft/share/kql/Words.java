package com.independentsoft.share.kql;

/**
 * The Class Words.
 */
public class Words implements IRestriction {

    private String leftExpression;
    private String rightExpression;

    /**
     * Instantiates a new words.
     *
     * @param leftExpression the left expression
     * @param rightExpression the right expression
     */
    public Words(String leftExpression, String rightExpression)
    {
        if (leftExpression == null)
        {
            throw new IllegalArgumentException("leftExpression");
        }

        if (rightExpression == null)
        {
            throw new IllegalArgumentException("rightExpression");
        }

        this.leftExpression = leftExpression;
        this.rightExpression = rightExpression;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        String stringValue = "WORDS(\"" + leftExpression + "\", \"" + rightExpression + "\")";

        return stringValue;
    }
}
