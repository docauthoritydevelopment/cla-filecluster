package com.independentsoft.share;

import java.util.ArrayList;
import java.util.List;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

/**
 * The Class ResultTable.
 */
public class ResultTable {

    private String groupTemplateId;
    private String itemTemplateId;
    private List<KeyValue> properties = new ArrayList<KeyValue>();
    private String title;
    private String titleUrl;
    private int rowCount;
    private SimpleDataTable table;
    private int totalRows;
    private int totalRowsIncludingDuplicates;

    /**
     * Instantiates a new result table.
     */
    public ResultTable()
    {
    }

    ResultTable(XMLStreamReader reader, String localName) throws XMLStreamException
    {
        parse(reader, localName);
    }

    private void parse(XMLStreamReader reader, String localName) throws XMLStreamException
    {
        while (reader.hasNext())
        {
            if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("GroupTemplateId") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                groupTemplateId = reader.getElementText();
            }
            else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("ItemTemplateId") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                itemTemplateId = reader.getElementText();
            }
            else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Properties") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                while (reader.hasNext())
                {
                    if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("element") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        KeyValue property = new KeyValue(reader);
                        properties.add(property);
                    }

                    if (reader.isEndElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Properties") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        break;
                    }
                    else
                    {
                        reader.next();
                    }
                }
            }
            else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("ResultTitle") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                title = reader.getElementText();
            }
            else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("ResultTitleUrl") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                titleUrl = reader.getElementText();
            }
            else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("RowCount") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                String stringValue = reader.getElementText();

                if(stringValue != null && stringValue.length() > 0)
                {
                    rowCount = Integer.parseInt(stringValue);
                }
            }
            else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Table") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                table = new SimpleDataTable(reader);
            }
            else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("TotalRows") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                String stringValue = reader.getElementText();

                if (stringValue != null && stringValue.length() > 0)
                {
                    totalRows = Integer.parseInt(stringValue);
                }
            }
            else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("TotalRowsIncludingDuplicates") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                String stringValue = reader.getElementText();

                if (stringValue != null && stringValue.length() > 0)
                {
                    totalRowsIncludingDuplicates = Integer.parseInt(stringValue);
                }
            }

            if (reader.isEndElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals(localName) && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
            {
                break;
            }
            else
            {
                reader.next();
            }
        }
    }

    /**
     * Gets the group template id.
     *
     * @return the group template id
     */
    public String getGroupTemplateId()
    {
        return groupTemplateId;
    }

    /**
     * Gets the item template id.
     *
     * @return the item template id
     */
    public String getItemTemplateId()
    {
        return itemTemplateId;
    }

    /**
     * Gets the properties.
     *
     * @return the properties
     */
    public List<KeyValue> getProperties()
    {
        return properties;
    }

    /**
     * Gets the title.
     *
     * @return the title
     */
    public String getTitle()
    {
        return title;
    }

    /**
     * Gets the title url.
     *
     * @return the title url
     */
    public String getTitleUrl()
    {
        return titleUrl;
    }
    
    /**
     * Gets the row count.
     *
     * @return the row count
     */
    public int getRowCount()
    {
        return rowCount;
    }

    /**
     * Gets the table.
     *
     * @return the table
     */
    public SimpleDataTable getTable()
    {
        return table;
    }

    /**
     * Gets the total rows.
     *
     * @return the total rows
     */
    public int getTotalRows()
    {
        return totalRows;
    }

    /**
     * Gets the total rows including duplicates.
     *
     * @return the total rows including duplicates
     */
    public int getTotalRowsIncludingDuplicates()
    {
        return totalRowsIncludingDuplicates ;
    }
}
