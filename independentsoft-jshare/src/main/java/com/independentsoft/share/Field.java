package com.independentsoft.share;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

/**
 * The Class Field.
 */
public class Field {

    private boolean canBeDeleted;
    private String defaultValue;
    private String description;
    private Direction direction = Direction.NONE;
    private boolean isEnforceUniqueValues;
    private String entityPropertyName;
    private FieldType type = FieldType.NONE;
    private boolean isFilterable;
    private boolean isFromBaseType;
    private String group;
    private boolean isHidden;
    private String id;
    private boolean isIndexed;
    private String internalName;
    private String jsLink;
    private boolean isReadOnly;
    private boolean isRequired;
    private FieldSchemaXml schemaXml;
    private String scope;
    private boolean isSealed;
    private boolean isSortable;
    private String staticName;
    private String title;
    private String typeAsString;
    private String typeDisplayName;
    private String typeShortDescription;
    private String validationFormula;
    private String validationMessage;

    //field calculated
    private DateTimeFieldFormatType dateFormat = DateTimeFieldFormatType.NONE;
    private String formula;
    private FieldType outputType = FieldType.NONE;

    //field computed
    private boolean enableLookup;

    //field datetime
    private CalendarType dateTimeCalendarType = CalendarType.NONE;
    private DateTimeFieldFormatType dateTimeDisplayFormat = DateTimeFieldFormatType.NONE;
    private DateTimeFieldFriendlyFormat dateTimeFriendlyFormat = DateTimeFieldFriendlyFormat.NONE;

    //field lookup and user
    private boolean allowDisplay;
    private boolean allowMultipleValues;
    private boolean isRelationship;
    private String lookupField;
    private String lookupList;
    private String lookupWebId;
    private boolean enablePresence;
    private String primaryFieldId;
    private RelationshipDeleteBehaviorType relationshipDeleteBehavior = RelationshipDeleteBehaviorType.NONE;
    private int selectionGroup = Integer.MIN_VALUE;
    private FieldUserSelectionMode selectionMode = FieldUserSelectionMode.NONE;

    //field multiChoice, choice, ratingScale
    private List<String> choices = new ArrayList<String>();
    private ChoiceFormatType editFormat = ChoiceFormatType.NONE;
    private boolean fillInChoice;
    private int gridEndNumber = Integer.MIN_VALUE;
    private String gridNonApplicableOptionText;
    private int gridStartNumber = Integer.MIN_VALUE;
    private String gridTextRangeAverage;
    private String gridTextRangeHigh;
    private String gridTextRangeLow;
    private String mappings;
    private int rangeCount = 0;

    //field multitext
    private boolean allowHyperlink;
    private boolean appendOnly;
    private int numberOfLines = Integer.MIN_VALUE;
    private boolean isRestrictedMode;
    private boolean isRichText;
    private boolean isWikiLinking;

    //field number, currency
    private Locale currencyLocale = Locale.NONE;
    private double maximumValue = Double.MIN_VALUE;
    private double minimumValue = Double.MIN_VALUE;

    //field text
    private int maximumLength = Integer.MIN_VALUE;

    //field url
    private UrlFieldDisplayFormat urlDisplayFormat = UrlFieldDisplayFormat.NONE;

    /**
     * Instantiates a new field.
     */
    public Field()
    {
    }

    Field(InputStream inputStream) throws XMLStreamException
    {
        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        XMLStreamReader reader = xmlInputFactory.createXMLStreamReader(inputStream);

        parse(reader);
    }

    Field(XMLStreamReader reader) throws XMLStreamException
    {
        parse(reader);
    }

    private void parse(XMLStreamReader reader) throws XMLStreamException
    {
        while (reader.hasNext())
        {
            if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("properties") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices/metadata"))
            {
                while (reader.hasNext())
                {
                    if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("CanBeDeleted") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            canBeDeleted = Boolean.parseBoolean(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("DefaultValue") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        defaultValue = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Description") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        description = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Direction") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            direction = EnumUtil.parseDirection(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("EnforceUniqueValues") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            isEnforceUniqueValues = Boolean.parseBoolean(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("EntityPropertyName") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        entityPropertyName = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("FieldTypeKind") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            type = EnumUtil.parseFieldType(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Filterable") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            isFilterable = Boolean.parseBoolean(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("FromBaseType") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            isFromBaseType = Boolean.parseBoolean(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Group") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        group = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Hidden") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            isHidden = Boolean.parseBoolean(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Id") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        id = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Indexed") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            isIndexed = Boolean.parseBoolean(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("InternalName") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        internalName = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("JSLink") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        jsLink = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("ReadOnlyField") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            isReadOnly = Boolean.parseBoolean(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Required") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            isRequired = Boolean.parseBoolean(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("SchemaXml") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            schemaXml = new FieldSchemaXml(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Scope") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        scope = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Sealed") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            isSealed = Boolean.parseBoolean(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Sortable") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            isSortable = Boolean.parseBoolean(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("StaticName") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        staticName = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Title") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        title = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("TypeAsString") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        typeAsString = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("TypeDisplayName") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        typeDisplayName = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("TypeShortDescription") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        typeShortDescription = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("ValidationFormula") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        validationFormula = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("ValidationMessage") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        validationMessage = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("DateFormat") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            dateFormat = EnumUtil.parseDateTimeFieldFormatType(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Formula") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        formula = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("OutputType") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            outputType = EnumUtil.parseFieldType(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("EnableLookup") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            enableLookup = Boolean.parseBoolean(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("DateTimeCalendarType") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            dateTimeCalendarType = EnumUtil.parseCalendarType(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("DisplayFormat") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            dateTimeDisplayFormat = EnumUtil.parseDateTimeFieldFormatType(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("FriendlyDisplayFormat") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            dateTimeFriendlyFormat = EnumUtil.parseDateTimeFieldFriendlyFormat(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("AllowDisplay") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            allowDisplay = Boolean.parseBoolean(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("AllowMultipleValues") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            allowMultipleValues = Boolean.parseBoolean(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("IsRelationship") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            isRelationship = Boolean.parseBoolean(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("LookupField") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        lookupField = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("LookupList") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        lookupList = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("LookupWebId") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        lookupWebId = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Presence") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            enablePresence = Boolean.parseBoolean(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("PrimaryFieldId") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        primaryFieldId = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("RelationshipDeleteBehavior") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            relationshipDeleteBehavior = EnumUtil.parseRelationshipDeleteBehaviorType(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("SelectionGroup") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            selectionGroup = Integer.parseInt(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("SelectionMode") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            selectionMode = EnumUtil.parseFieldUserSelectionMode(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Choices") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                    	while(true)
                    	{
                            if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("element") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                            {
                                String stringValue = reader.getElementText();

                                choices.add(stringValue);
                            }
                                
                            if (reader.isEndElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Choices") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                            {
                                break;
                            }
                            else
                            {
                                reader.next();
                            }
                    	}
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("EditFormat") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            editFormat = EnumUtil.parseChoiceFormatType(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("FillInChoice") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            fillInChoice = Boolean.parseBoolean(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("GridEndNumber") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            gridEndNumber = Integer.parseInt(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("GridNAOptionText") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        gridNonApplicableOptionText = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("GridStartNumber") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            gridStartNumber = Integer.parseInt(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("GridTextRangeAverage") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        gridTextRangeAverage = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("GridTextRangeHigh") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        gridTextRangeHigh = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("GridTextRangeLow") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        gridTextRangeLow = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("Mappings") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        mappings = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("RangeCount") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            rangeCount = Integer.parseInt(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("AllowHyperlink") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            allowHyperlink = Boolean.parseBoolean(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("AppendOnly") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            appendOnly = Boolean.parseBoolean(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("NumberOfLines") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            numberOfLines = Integer.parseInt(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("RestrictedMode") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            isRestrictedMode = Boolean.parseBoolean(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("RichText") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            isRichText = Boolean.parseBoolean(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("WikiLinking") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            isWikiLinking = Boolean.parseBoolean(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("CurrencyLocaleId") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            currencyLocale = EnumUtil.parseLocale(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("MaximumValue") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            maximumValue = Double.parseDouble(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("MinimumValue") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            minimumValue = Double.parseDouble(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("MaxLength") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            maximumLength = Integer.parseInt(stringValue);
                        }
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("DisplayFormat") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        String stringValue = reader.getElementText();

                        if (stringValue != null && stringValue.length() > 0)
                        {
                            urlDisplayFormat = EnumUtil.parseUrlFieldDisplayFormat(stringValue);
                        }
                    }

                    if (reader.isEndElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("properties") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices/metadata"))
                    {
                        break;
                    }
                    else
                    {
                        reader.next();
                    }
                }
            }

            if (reader.isEndElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("entry") && reader.getNamespaceURI().equals("http://www.w3.org/2005/Atom"))
            {
                break;
            }
            else
            {
                reader.next();
            }
        }
    }

    String toCreateJSon()
    {
        String fieldType = EnumUtil.parseFieldTypeDescription(type);

        String stringValue = "{ '__metadata': { 'type': '" + fieldType + "' }";

        if (defaultValue != null && defaultValue.length() > 0)
        {
            stringValue += ", 'DefaultValue': '" + Util.encodeJson(defaultValue) + "'";
        }

        if (description != null && description.length() > 0)
        {
            stringValue += ", 'Description': '" + Util.encodeJson(description) + "'";
        }

        if (direction != Direction.NONE)
        {
            stringValue += ", 'Direction': '" + EnumUtil.parseDirection(direction) + "'";
        }

        //if (isEnforceUniqueValues)
        //{
        //    stringValue += ", 'EnforceUniqueValues': true";
        //}

        //must be set
        if (type != FieldType.NONE)
        {
            stringValue += ", 'FieldTypeKind': " + EnumUtil.parseFieldType(type);
        }

        if (group != null && group.length() > 0)
        {
            stringValue += ", 'Group': '" + Util.encodeJson(group) + "'";
        }

        if (isHidden)
        {
            stringValue += ", 'Hidden': true";
        }

        //if (isIndexed)
        //{
        //    stringValue += ", 'Indexed': true";
        //}

        if (jsLink != null && jsLink.length() > 0)
        {
            stringValue += ", 'JSLink': '" + Util.encodeJson(jsLink) + "'";
        }

        if (isReadOnly)
        {
            stringValue += ", 'ReadOnlyField': true";
        }

        if (isRequired)
        {
            stringValue += ", 'Required': true";
        }

        if (scope != null && scope.length() > 0)
        {
            stringValue += ", 'Scope': '" + Util.encodeJson(scope) + "'";
        }

        if (isSealed)
        {
            stringValue += ", 'Sealed': true";
        }

        if (isSortable)
        {
            stringValue += ", 'Sortable': true";
        }

        if (staticName != null && staticName.length() > 0)
        {
            stringValue += ", 'StaticName': '" + Util.encodeJson(staticName) + "'";
        }

        //must be set
        if (title != null && title.length() > 0)
        {
            stringValue += ", 'Title': '" + Util.encodeJson(title) + "'";
        }

        //if (typeAsString != null)
        //{
        //    stringValue += ", 'TypeAsString': '" + Util.encodeJson(typeAsString) + "'";
        //}

        //if (validationFormula != null)
        //{
        //    stringValue += ", 'ValidationFormula': '" + Util.encodeJson(validationFormula) + "'";
        //}

        //if (validationMessage != null)
        //{
        //    stringValue += ", 'ValidationMessage': '" + Util.encodeJson(validationMessage) + "";
        //}

        if (dateFormat != DateTimeFieldFormatType.NONE)
        {
            stringValue += ", 'DateFormat': " + EnumUtil.parseDateTimeFieldFormatType(dateFormat);
        }

        if (formula != null && formula.length() > 0)
        {
            stringValue += ", 'Formula': '" + Util.encodeJson(formula) + "'";
        }

        if (outputType != FieldType.NONE)
        {
            stringValue += ", 'OutputType': " + EnumUtil.parseFieldType(outputType);
        }

        if (enableLookup)
        {
            stringValue += ", 'EnableLookup': true";
        }

        if (dateTimeCalendarType != CalendarType.NONE)
        {
            stringValue += ", 'DateTimeCalendarType': " + EnumUtil.parseCalendarType(dateTimeCalendarType);
        }

        if (dateTimeDisplayFormat != DateTimeFieldFormatType.NONE)
        {
            stringValue += ", 'DisplayFormat': " + EnumUtil.parseDateTimeFieldFormatType(dateTimeDisplayFormat);
        }

        if (dateTimeFriendlyFormat != DateTimeFieldFriendlyFormat.NONE)
        {
            stringValue += ", 'FriendlyDisplayFormat': " + EnumUtil.parseDateTimeFieldFriendlyFormat(dateTimeFriendlyFormat);
        }

        if (allowDisplay)
        {
            stringValue += ", 'AllowDisplay': true";
        }

        if (allowMultipleValues)
        {
            stringValue += ", 'AllowMultipleValues': true";
        }

        if (isRelationship)
        {
            stringValue += ", 'IsRelationship': true";
        }

        if (lookupField != null && lookupField.length() > 0)
        {
            stringValue += ", 'LookupField': '" + Util.encodeJson(lookupField) + "'";
        }

        if (lookupList != null && lookupList.length() > 0)
        {
            stringValue += ", 'LookupList': '" + Util.encodeJson(lookupList) + "'";
        }

        if (lookupWebId != null && lookupWebId.length() > 0)
        {
            stringValue += ", 'LookupWebId': '" + Util.encodeJson(lookupWebId) + "'";
        }

        if (enablePresence)
        {
            stringValue += ", 'Presence': true";
        }

        if (primaryFieldId != null && primaryFieldId.length() > 0)
        {
            stringValue += ", 'PrimaryFieldId': '" + Util.encodeJson(primaryFieldId) + "'";
        }

        if (relationshipDeleteBehavior != RelationshipDeleteBehaviorType.NONE)
        {
            stringValue += ", 'RelationshipDeleteBehavior': " + EnumUtil.parseRelationshipDeleteBehaviorType(relationshipDeleteBehavior);
        }

        if (selectionGroup > Integer.MIN_VALUE)
        {
            stringValue += ", 'SelectionGroup': '" + selectionGroup + "'";
        }

        if (selectionMode != FieldUserSelectionMode.NONE)
        {
            stringValue += ", 'SelectionMode': " + EnumUtil.parseFieldUserSelectionMode(selectionMode);
        }

        if (choices.size() > 0)
        {
            String value = "";

            for (int i = 0; i < choices.size(); i++)
            {
                value += "'" + Util.encodeJson(choices.get(i)) + "'";

                if (i < choices.size() - 1)
                {
                    value += ", ";
                }
            }

            stringValue += ", 'Choices': { '__metadata': { 'type': 'Collection(Edm.String)' }, 'results': [ " + value + "] } }";
        }

        if (editFormat != ChoiceFormatType.NONE)
        {
            stringValue += ", 'EditFormat': " + EnumUtil.parseChoiceFormatType(editFormat);
        }

        if (fillInChoice)
        {
            stringValue += ", 'FillInChoice': true";
        }

        if (gridEndNumber > Integer.MIN_VALUE)
        {
            stringValue += ", 'GridEndNumber': '" + gridEndNumber + "'";
        }

        if (gridNonApplicableOptionText != null && gridNonApplicableOptionText.length() > 0)
        {
            stringValue += ", 'GridNAOptionText': '" + Util.encodeJson(gridNonApplicableOptionText) + "'";
        }

        if (gridStartNumber > Integer.MIN_VALUE)
        {
            stringValue += ", 'GridStartNumber': '" + gridStartNumber + "'";
        }

        if (gridTextRangeAverage != null && gridTextRangeAverage.length() > 0)
        {
            stringValue += ", 'GridTextRangeAverage': '" + Util.encodeJson(gridTextRangeAverage) + "'";
        }

        if (gridTextRangeHigh != null && gridTextRangeHigh.length() > 0)
        {
            stringValue += ", 'GridTextRangeHigh': '" + Util.encodeJson(gridTextRangeHigh) + "'";
        }

        if (gridTextRangeLow != null && gridTextRangeLow.length() > 0)
        {
            stringValue += ", 'GridTextRangeLow': '" + Util.encodeJson(gridTextRangeLow) + "'";
        }

        if (allowHyperlink)
        {
            stringValue += ", 'AllowHyperlink': true";
        }

        if (appendOnly)
        {
            stringValue += ", 'AppendOnly': true";
        }

        if (numberOfLines > Integer.MIN_VALUE)
        {
            stringValue += ", 'NumberOfLines': '" + numberOfLines + "'";
        }

        if (isRestrictedMode)
        {
            stringValue += ", 'RestrictedMode': true";
        }

        if (isRichText)
        {
            stringValue += ", 'RichText': true";
        }

        if (currencyLocale != Locale.NONE)
        {
            stringValue += ", 'CurrencyLocaleId': '" + EnumUtil.parseLocale(currencyLocale) + "'";
        }

        if (maximumValue > Double.MIN_VALUE)
        {
            stringValue += ", 'MaximumValue': '" + Double.toString(maximumValue) + "'";
        }

        if (minimumValue > Double.MIN_VALUE)
        {
            stringValue += ", 'MinimumValue': '" + Double.toString(minimumValue) + "'";
        }

        if (maximumLength > Integer.MIN_VALUE)
        {
            stringValue += ", 'MaxLength': '" + maximumLength + "'";
        }

        if (urlDisplayFormat != UrlFieldDisplayFormat.NONE)
        {
            stringValue += ", 'DisplayFormat': " + EnumUtil.parseUrlFieldDisplayFormat(urlDisplayFormat);
        }

        stringValue += " }";

        return stringValue;
    }

    String toUpdateJSon()
    {
        String fieldType = EnumUtil.parseFieldTypeDescription(type);

        String stringValue = "{ '__metadata': { 'type': '" + fieldType + "' }";

        if (defaultValue != null && defaultValue.length() > 0)
        {
            stringValue += ", 'DefaultValue': '" + Util.encodeJson(defaultValue) + "'";
        }

        if (description != null && description.length() > 0)
        {
            stringValue += ", 'Description': '" + Util.encodeJson(description) + "'";
        }

        if (direction != Direction.NONE)
        {
            stringValue += ", 'Direction': '" + EnumUtil.parseDirection(direction) + "'";
        }

        if (type != FieldType.NONE)
        {
            stringValue += ", 'FieldTypeKind': " + EnumUtil.parseFieldType(type);
        }

        if (group != null && group.length() > 0)
        {
            stringValue += ", 'Group': '" + Util.encodeJson(group) + "'";
        }

        if (isHidden)
        {
            stringValue += ", 'Hidden': true";
        }
        else
        {
            stringValue += ", 'Hidden': false";
        }

        if (jsLink != null && jsLink.length() > 0)
        {
            stringValue += ", 'JSLink': '" + Util.encodeJson(jsLink) + "'";
        }

        if (isReadOnly)
        {
            stringValue += ", 'ReadOnlyField': true";
        }
        else
        {
            stringValue += ", 'ReadOnlyField': false";
        }

        if (isRequired)
        {
            stringValue += ", 'Required': true";
        }
        else
        {
            stringValue += ", 'Required': false";
        }

        if (scope != null && scope.length() > 0)
        {
            stringValue += ", 'Scope': '" + Util.encodeJson(scope) + "'";
        }

        if (isSealed)
        {
            stringValue += ", 'Sealed': true";
        }
        else
        {
            stringValue += ", 'Sealed': false";
        }

        if (isSortable)
        {
            stringValue += ", 'Sortable': true";
        }
        else
        {
            stringValue += ", 'Sortable': false";
        }

        if (staticName != null && staticName.length() > 0)
        {
            stringValue += ", 'StaticName': '" + Util.encodeJson(staticName) + "'";
        }

        if (title != null && title.length() > 0)
        {
            stringValue += ", 'Title': '" + Util.encodeJson(title) + "'";
        }

        if (dateFormat != DateTimeFieldFormatType.NONE)
        {
            stringValue += ", 'DateFormat': " + EnumUtil.parseDateTimeFieldFormatType(dateFormat);
        }

        if (formula != null && formula.length() > 0)
        {
            stringValue += ", 'Formula': '" + Util.encodeJson(formula) + "'";
        }

        if (outputType != FieldType.NONE)
        {
            stringValue += ", 'OutputType': " + EnumUtil.parseFieldType(outputType);
        }

        if (enableLookup)
        {
            stringValue += ", 'EnableLookup': true";
        }
        else
        {
            stringValue += ", 'EnableLookup': false";
        }

        if (dateTimeCalendarType != CalendarType.NONE)
        {
            stringValue += ", 'DateTimeCalendarType': " + EnumUtil.parseCalendarType(dateTimeCalendarType);
        }

        if (dateTimeDisplayFormat != DateTimeFieldFormatType.NONE)
        {
            stringValue += ", 'DisplayFormat': " + EnumUtil.parseDateTimeFieldFormatType(dateTimeDisplayFormat);
        }

        if (dateTimeFriendlyFormat != DateTimeFieldFriendlyFormat.NONE)
        {
            stringValue += ", 'FriendlyDisplayFormat': " + EnumUtil.parseDateTimeFieldFriendlyFormat(dateTimeFriendlyFormat);
        }

        if (allowDisplay)
        {
            stringValue += ", 'AllowDisplay': true";
        }
        else
        {
            stringValue += ", 'AllowDisplay': false";
        }

        if (allowMultipleValues)
        {
            stringValue += ", 'AllowMultipleValues': true";
        }
        else
        {
            stringValue += ", 'AllowMultipleValues': false";
        }

        if (isRelationship)
        {
            stringValue += ", 'IsRelationship': true";
        }
        else
        {
            stringValue += ", 'IsRelationship': false";
        }

        if (lookupField != null && lookupField.length() > 0)
        {
            stringValue += ", 'LookupField': '" + Util.encodeJson(lookupField) + "'";
        }

        if (lookupList != null && lookupList.length() > 0)
        {
            stringValue += ", 'LookupList': '" + Util.encodeJson(lookupList) + "'";
        }

        if (lookupWebId != null && lookupWebId.length() > 0)
        {
            stringValue += ", 'LookupWebId': '" + Util.encodeJson(lookupWebId) + "'";
        }

        if (enablePresence)
        {
            stringValue += ", 'Presence': true";
        }
        else
        {
            stringValue += ", 'Presence': false";
        }

        if (primaryFieldId != null && primaryFieldId.length() > 0)
        {
            stringValue += ", 'PrimaryFieldId': '" + Util.encodeJson(primaryFieldId) + "'";
        }

        if (relationshipDeleteBehavior != RelationshipDeleteBehaviorType.NONE)
        {
            stringValue += ", 'RelationshipDeleteBehavior': " + EnumUtil.parseRelationshipDeleteBehaviorType(relationshipDeleteBehavior);
        }

        if (selectionGroup > -1)
        {
            stringValue += ", 'SelectionGroup': '" + selectionGroup + "'";
        }

        if (selectionMode != FieldUserSelectionMode.NONE)
        {
            stringValue += ", 'SelectionMode': " + EnumUtil.parseFieldUserSelectionMode(selectionMode);
        }

        if (choices.size() > 0)
        {
            String value = "";

            for (int i = 0; i < choices.size(); i++)
            {
                value += "'" + Util.encodeJson(choices.get(i)) + "'";

                if (i < choices.size() - 1)
                {
                    value += ", ";
                }
            }

            stringValue += ", 'Choices': { '__metadata': { 'type': 'Collection(Edm.String)' }, 'results': [ " + value + "] } }";
        }

        if (editFormat != ChoiceFormatType.NONE)
        {
            stringValue += ", 'EditFormat': " + EnumUtil.parseChoiceFormatType(editFormat);
        }

        if (fillInChoice)
        {
            stringValue += ", 'FillInChoice': true";
        }
        else
        {
            stringValue += ", 'FillInChoice': false";
        }

        if (gridEndNumber > Integer.MIN_VALUE)
        {
            stringValue += ", 'GridEndNumber': '" + gridEndNumber + "'";
        }

        if (gridNonApplicableOptionText != null && gridNonApplicableOptionText.length() > 0)
        {
            stringValue += ", 'GridNAOptionText': '" + Util.encodeJson(gridNonApplicableOptionText) + "'";
        }

        if (gridStartNumber > Integer.MIN_VALUE)
        {
            stringValue += ", 'GridStartNumber': '" + gridStartNumber + "'";
        }

        if (gridTextRangeAverage != null && gridTextRangeAverage.length() > 0)
        {
            stringValue += ", 'GridTextRangeAverage': '" + Util.encodeJson(gridTextRangeAverage) + "'";
        }

        if (gridTextRangeHigh != null && gridTextRangeHigh.length() > 0)
        {
            stringValue += ", 'GridTextRangeHigh': '" + Util.encodeJson(gridTextRangeHigh) + "'";
        }

        if (gridTextRangeLow != null && gridTextRangeLow.length() > 0)
        {
            stringValue += ", 'GridTextRangeLow': '" + Util.encodeJson(gridTextRangeLow) + "'";
        }

        if (allowHyperlink)
        {
            stringValue += ", 'AllowHyperlink': true";
        }
        else
        {
            stringValue += ", 'AllowHyperlink': false";
        }

        if (appendOnly)
        {
            stringValue += ", 'AppendOnly': true";
        }
        else
        {
            stringValue += ", 'AppendOnly': false";
        }

        if (numberOfLines > Integer.MIN_VALUE)
        {
            stringValue += ", 'NumberOfLines': '" + numberOfLines + "'";
        }

        if (isRestrictedMode)
        {
            stringValue += ", 'RestrictedMode': true";
        }
        else
        {
            stringValue += ", 'RestrictedMode': false";
        }

        if (isRichText)
        {
            stringValue += ", 'RichText': true";
        }
        else
        {
            stringValue += ", 'RichText': false";
        }

        if (currencyLocale != Locale.NONE)
        {
            stringValue += ", 'CurrencyLocaleId': '" + EnumUtil.parseLocale(currencyLocale) + "'";
        }

        if (maximumValue > Double.MIN_VALUE)
        {
            stringValue += ", 'MaximumValue': '" + Double.toString(maximumValue) + "'";
        }

        if (minimumValue > Double.MIN_VALUE)
        {
            stringValue += ", 'MinimumValue': '" + Double.toString(minimumValue) + "'";
        }

        if (maximumLength > Integer.MIN_VALUE)
        {
            stringValue += ", 'MaxLength': '" + maximumLength + "'";
        }

        if (urlDisplayFormat != UrlFieldDisplayFormat.NONE)
        {
            stringValue += ", 'DisplayFormat': " + EnumUtil.parseUrlFieldDisplayFormat(urlDisplayFormat);
        }

        stringValue += " }";

        return stringValue;
    }

    /**
     * Can be deleted.
     *
     * @return true, if successful
     */
    public boolean canBeDeleted()
    {
        return canBeDeleted;
    }

    /**
     * Gets the default value.
     *
     * @return the default value
     */
    public String getDefaultValue()
    {
        return defaultValue;
    }

    /**
     * Sets the default value.
     *
     * @param defaultValue the new default value
     */
    public void setDefaultValue(String defaultValue)
    {
        this.defaultValue = defaultValue;
    }

    /**
     * Gets the description.
     *
     * @return the description
     */
    public String getDescription()
    {
        return description;
    }

    /**
     * Sets the description.
     *
     * @param description the new description
     */
    public void setDescription(String description)
    {
        this.description = description;
    }

    /**
     * Gets the direction.
     *
     * @return the direction
     */
    public Direction getDirection()
    {
        return direction;
    }

    /**
     * Sets the direction.
     *
     * @param direction the new direction
     */
    public void setDirection(Direction direction)
    {
        this.direction = direction;
    }

    /**
     * Checks if is enforce unique values.
     *
     * @return true, if is enforce unique values
     */
    public boolean isEnforceUniqueValues()
    {
        return isEnforceUniqueValues;
    }

    /**
     * Sets the enforce unique values.
     *
     * @param isEnforceUniqueValues the new enforce unique values
     */
    public void setEnforceUniqueValues(boolean isEnforceUniqueValues)
    {
        this.isEnforceUniqueValues = isEnforceUniqueValues;
    }

    /**
     * Gets the entity property name.
     *
     * @return the entity property name
     */
    public String getEntityPropertyName()
    {
        return entityPropertyName;
    }

    /**
     * Gets the type.
     *
     * @return the type
     */
    public FieldType getType()
    {
        return type;
    }

    /**
     * Sets the type.
     *
     * @param type the new type
     */
    public void setType(FieldType type)
    {
        this.type = type;
    }

    /**
     * Checks if is filterable.
     *
     * @return true, if is filterable
     */
    public boolean isFilterable()
    {
        return isFilterable;
    }

    /**
     * Checks if is from base type.
     *
     * @return true, if is from base type
     */
    public boolean isFromBaseType()
    {
        return isFromBaseType;
    }

    /**
     * Gets the group.
     *
     * @return the group
     */
    public String getGroup()
    {
        return group;
    }

    /**
     * Sets the group.
     *
     * @param group the new group
     */
    public void setGroup(String group)
    {
        this.group = group;
    }

    /**
     * Checks if is hidden.
     *
     * @return true, if is hidden
     */
    public boolean isHidden()
    {
        return isHidden;
    }

    /**
     * Sets the hidden.
     *
     * @param isHidden the new hidden
     */
    public void setHidden(boolean isHidden)
    {
        this.isHidden = isHidden;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public String getId()
    {
        return id;
    }

    /**
     * Checks if is indexed.
     *
     * @return true, if is indexed
     */
    public boolean isIndexed()
    {
        return isIndexed;
    }

    /**
     * Sets the indexed.
     *
     * @param isIndexed the new indexed
     */
    public void setIndexed(boolean isIndexed)
    {
        this.isIndexed = isIndexed;
    }

    /**
     * Gets the internal name.
     *
     * @return the internal name
     */
    public String getInternalName()
    {
        return internalName;
    }

    /**
     * Gets the JS link.
     *
     * @return the JS link
     */
    public String getJSLink()
    {
        return jsLink;
    }

    /**
     * Sets the JS link.
     *
     * @param jsLink the new JS link
     */
    public void setJSLink(String jsLink)
    {
        this.jsLink = jsLink;
    }

    /**
     * Checks if is read only.
     *
     * @return true, if is read only
     */
    public boolean isReadOnly()
    {
        return isReadOnly;
    }

    /**
     * Sets the read only.
     *
     * @param isReadOnly the new read only
     */
    public void setReadOnly(boolean isReadOnly)
    {
        this.isReadOnly = isReadOnly;
    }

    /**
     * Checks if is required.
     *
     * @return true, if is required
     */
    public boolean isRequired()
    {
        return isRequired;
    }

    /**
     * Sets the required.
     *
     * @param isRequired the new required
     */
    public void setRequired(boolean isRequired)
    {
        this.isRequired = isRequired;
    }

    /**
     * Gets the schema xml.
     *
     * @return the schema xml
     */
    public FieldSchemaXml getSchemaXml()
    {
        return schemaXml;
    }

    /**
     * Sets the schema xml.
     *
     * @param schemaXml the new schema xml
     */
    public void setSchemaXml(FieldSchemaXml schemaXml)
    {
        this.schemaXml = schemaXml;
    }

    /**
     * Gets the scope.
     *
     * @return the scope
     */
    public String getScope()
    {
        return scope;
    }

    /**
     * Sets the scope.
     *
     * @param scope the new scope
     */
    public void setScope(String scope)
    {
        this.scope = scope;
    }

    /**
     * Checks if is sealed.
     *
     * @return true, if is sealed
     */
    public boolean isSealed()
    {
        return isSealed;
    }

    /**
     * Sets the sealed.
     *
     * @param isSealed the new sealed
     */
    public void setSealed(boolean isSealed)
    {
        this.isSealed = isSealed;
    }

    /**
     * Checks if is sortable.
     *
     * @return true, if is sortable
     */
    public boolean isSortable()
    {
        return isSortable;
    }

    /**
     * Sets the sortable.
     *
     * @param isSortable the new sortable
     */
    public void setSortable(boolean isSortable)
    {
        this.isSortable = isSortable;
    }

    /**
     * Gets the static name.
     *
     * @return the static name
     */
    public String getStaticName()
    {
        return staticName;
    }

    /**
     * Sets the static name.
     *
     * @param staticName the new static name
     */
    public void setStaticName(String staticName)
    {
        this.staticName = staticName;
    }

    /**
     * Gets the title.
     *
     * @return the title
     */
    public String getTitle()
    {
        return title;
    }

    /**
     * Sets the title.
     *
     * @param title the new title
     */
    public void setTitle(String title)
    {
        this.title = title;
    }

    /**
     * Gets the type as string.
     *
     * @return the type as string
     */
    public String getTypeAsString()
    {
        return typeAsString;
    }

    /**
     * Sets the type as string.
     *
     * @param typeAsString the new type as string
     */
    public void setTypeAsString(String typeAsString)
    {
        this.typeAsString = typeAsString;
    }

    /**
     * Gets the type display name.
     *
     * @return the type display name
     */
    public String getTypeDisplayName()
    {
        return typeDisplayName;
    }

    /**
     * Gets the type short description.
     *
     * @return the type short description
     */
    public String getTypeShortDescription()
    {
        return typeShortDescription;
    }

    /**
     * Gets the validation formula.
     *
     * @return the validation formula
     */
    public String getValidationFormula()
    {
        return validationFormula;
    }

    /**
     * Sets the validation formula.
     *
     * @param validationFormula the new validation formula
     */
    public void setValidationFormula(String validationFormula)
    {
        this.validationFormula = validationFormula;
    }

    /**
     * Gets the validation message.
     *
     * @return the validation message
     */
    public String getValidationMessage()
    {
        return validationMessage;
    }

    /**
     * Sets the validation message.
     *
     * @param validationMessage the new validation message
     */
    public void setValidationMessage(String validationMessage)
    {
        this.validationMessage = validationMessage;
    }

    /**
     * Gets the date format.
     *
     * @return the date format
     */
    public DateTimeFieldFormatType getDateFormat()
    {
        return dateFormat;
    }

    /**
     * Sets the date format.
     *
     * @param dateFormat the new date format
     */
    public void setDateFormat(DateTimeFieldFormatType dateFormat)
    {
        this.dateFormat = dateFormat;
    }

    /**
     * Gets the formula.
     *
     * @return the formula
     */
    public String getFormula()
    {
        return formula;
    }

    /**
     * Sets the formula.
     *
     * @param formula the new formula
     */
    public void setFormula(String formula)
    {
        this.formula = formula;
    }

    /**
     * Gets the output type.
     *
     * @return the output type
     */
    public FieldType getOutputType()
    {
        return outputType;
    }

    /**
     * Sets the output type.
     *
     * @param outputType the new output type
     */
    public void setOutputType(FieldType outputType)
    {
        this.outputType = outputType;
    }

    /**
     * Checks if is lookup enabled.
     *
     * @return true, if is lookup enabled
     */
    public boolean isLookupEnabled()
    {
        return enableLookup;
    }

    /**
     * Enable lookup.
     *
     * @param enableLookup the enable lookup
     */
    public void enableLookup(boolean enableLookup)
    {
        this.enableLookup = enableLookup;
    }

    /**
     * Gets the date time calendar type.
     *
     * @return the date time calendar type
     */
    public CalendarType getDateTimeCalendarType()
    {
        return dateTimeCalendarType;
    }

    /**
     * Sets the date time calendar type.
     *
     * @param dateTimeCalendarType the new date time calendar type
     */
    public void setDateTimeCalendarType(CalendarType dateTimeCalendarType)
    {
        this.dateTimeCalendarType = dateTimeCalendarType;
    }

    /**
     * Gets the date time display format.
     *
     * @return the date time display format
     */
    public DateTimeFieldFormatType getDateTimeDisplayFormat()
    {
        return dateTimeDisplayFormat;
    }

    /**
     * Sets the date time display format.
     *
     * @param dateTimeDisplayFormat the new date time display format
     */
    public void setDateTimeDisplayFormat(DateTimeFieldFormatType dateTimeDisplayFormat)
    {
        this.dateTimeDisplayFormat = dateTimeDisplayFormat;
    }

    /**
     * Gets the date time friendly format.
     *
     * @return the date time friendly format
     */
    public DateTimeFieldFriendlyFormat getDateTimeFriendlyFormat()
    {
        return dateTimeFriendlyFormat;
    }

    /**
     * Sets the date time friendly format.
     *
     * @param dateTimeFriendlyFormat the new date time friendly format
     */
    public void setDateTimeFriendlyFormat(DateTimeFieldFriendlyFormat dateTimeFriendlyFormat)
    {
        this.dateTimeFriendlyFormat = dateTimeFriendlyFormat;
    }

    /**
     * Checks if is display allowed.
     *
     * @return true, if is display allowed
     */
    public boolean isDisplayAllowed()
    {
        return allowDisplay;
    }

    /**
     * Allow display.
     *
     * @param allowDisplay the allow display
     */
    public void allowDisplay(boolean allowDisplay)
    {
        this.allowDisplay = allowDisplay;
    }

    /**
     * Checks if is multiple values allowed.
     *
     * @return true, if is multiple values allowed
     */
    public boolean isMultipleValuesAllowed()
    {
        return allowMultipleValues;
    }

    /**
     * Allow multiple values.
     *
     * @param allowMultipleValues the allow multiple values
     */
    public void allowMultipleValues(boolean allowMultipleValues)
    {
        this.allowMultipleValues = allowMultipleValues;
    }

    /**
     * Checks if is relationship.
     *
     * @return true, if is relationship
     */
    public boolean isRelationship()
    {
        return isRelationship;
    }

    /**
     * Sets the relationship.
     *
     * @param isRelationship the new relationship
     */
    public void setRelationship(boolean isRelationship)
    {
        this.isRelationship = isRelationship;
    }

    /**
     * Gets the lookup field.
     *
     * @return the lookup field
     */
    public String getLookupField()
    {
        return lookupField;
    }

    /**
     * Sets the lookup field.
     *
     * @param lookupField the new lookup field
     */
    public void setLookupField(String lookupField)
    {
        this.lookupField = lookupField;
    }

    /**
     * Gets the lookup list.
     *
     * @return the lookup list
     */
    public String getLookupList()
    {
        return lookupList;
    }

    /**
     * Sets the lookup list.
     *
     * @param lookupList the new lookup list
     */
    public void setLookupList(String lookupList)
    {
        this.lookupList = lookupList;
    }

    /**
     * Gets the lookup web id.
     *
     * @return the lookup web id
     */
    public String getLookupWebId()
    {
        return lookupWebId;
    }

    /**
     * Sets the lookup web id.
     *
     * @param lookupWebId the new lookup web id
     */
    public void setLookupWebId(String lookupWebId)
    {
        this.lookupWebId = lookupWebId;
    }

    /**
     * Checks if is presence enabled.
     *
     * @return true, if is presence enabled
     */
    public boolean isPresenceEnabled()
    {
        return enablePresence;
    }

    /**
     * Enable presence.
     *
     * @param enablePresence the enable presence
     */
    public void enablePresence(boolean enablePresence)
    {
        this.enablePresence = enablePresence;
    }

    /**
     * Gets the primary field id.
     *
     * @return the primary field id
     */
    public String getPrimaryFieldId()
    {
        return primaryFieldId;
    }

    /**
     * Sets the primary field id.
     *
     * @param primaryFieldId the new primary field id
     */
    public void setPrimaryFieldId(String primaryFieldId)
    {
        this.primaryFieldId = primaryFieldId;
    }

    /**
     * Gets the relationship delete behavior.
     *
     * @return the relationship delete behavior
     */
    public RelationshipDeleteBehaviorType getRelationshipDeleteBehavior()
    {
        return relationshipDeleteBehavior;
    }

    /**
     * Sets the relationship delete behavior.
     *
     * @param relationshipDeleteBehavior the new relationship delete behavior
     */
    public void setRelationshipDeleteBehavior(RelationshipDeleteBehaviorType relationshipDeleteBehavior)
    {
        this.relationshipDeleteBehavior = relationshipDeleteBehavior;
    }

    /**
     * Gets the selection group.
     *
     * @return the selection group
     */
    public int getSelectionGroup()
    {
        return selectionGroup;
    }

    /**
     * Sets the selection group.
     *
     * @param selectionGroup the new selection group
     */
    public void setSelectionGroup(int selectionGroup)
    {
        this.selectionGroup = selectionGroup;
    }

    /**
     * Gets the selection mode.
     *
     * @return the selection mode
     */
    public FieldUserSelectionMode getSelectionMode()
    {
        return selectionMode;
    }

    /**
     * Sets the selection mode.
     *
     * @param selectionMode the new selection mode
     */
    public void setSelectionMode(FieldUserSelectionMode selectionMode)
    {
        this.selectionMode = selectionMode;
    }

    /**
     * Gets the choices.
     *
     * @return the choices
     */
    public List<String> getChoices()
    {
        return choices;
    }

    /**
     * Gets the edits the format.
     *
     * @return the edits the format
     */
    public ChoiceFormatType getEditFormat()
    {
        return editFormat;
    }

    /**
     * Sets the edits the format.
     *
     * @param editFormat the new edits the format
     */
    public void setEditFormat(ChoiceFormatType editFormat)
    {
        this.editFormat = editFormat;
    }

    /**
     * Checks if is fill in choice.
     *
     * @return true, if is fill in choice
     */
    public boolean isFillInChoice()
    {
        return fillInChoice;
    }

    /**
     * Sets the fill in choice.
     *
     * @param fillInChoice the new fill in choice
     */
    public void setFillInChoice(boolean fillInChoice)
    {
        this.fillInChoice = fillInChoice;
    }

    /**
     * Gets the grid end number.
     *
     * @return the grid end number
     */
    public int getGridEndNumber()
    {
        return gridEndNumber;
    }

    /**
     * Sets the grid end number.
     *
     * @param gridEndNumber the new grid end number
     */
    public void setGridEndNumber(int gridEndNumber)
    {
        this.gridEndNumber = gridEndNumber;
    }

    /**
     * Gets the grid non applicable option text.
     *
     * @return the grid non applicable option text
     */
    public String getGridNonApplicableOptionText()
    {
        return gridNonApplicableOptionText;
    }

    /**
     * Sets the grid non applicable option text.
     *
     * @param gridNonApplicableOptionText the new grid non applicable option text
     */
    public void setGridNonApplicableOptionText(String gridNonApplicableOptionText)
    {
        this.gridNonApplicableOptionText = gridNonApplicableOptionText;
    }

    /**
     * Gets the grid start number.
     *
     * @return the grid start number
     */
    public int getGridStartNumber()
    {
        return gridStartNumber;
    }

    /**
     * Sets the grid start number.
     *
     * @param gridStartNumber the new grid start number
     */
    public void setGridStartNumber(int gridStartNumber)
    {
        this.gridStartNumber = gridStartNumber;
    }

    /**
     * Gets the grid text range average.
     *
     * @return the grid text range average
     */
    public String getGridTextRangeAverage()
    {
        return gridTextRangeAverage;
    }

    /**
     * Sets the grid text range average.
     *
     * @param gridTextRangeAverage the new grid text range average
     */
    public void setGridTextRangeAverage(String gridTextRangeAverage)
    {
        this.gridTextRangeAverage = gridTextRangeAverage;
    }

    /**
     * Gets the grid text range high.
     *
     * @return the grid text range high
     */
    public String getGridTextRangeHigh()
    {
        return gridTextRangeHigh;
    }

    /**
     * Sets the grid text range high.
     *
     * @param gridTextRangeHigh the new grid text range high
     */
    public void setGridTextRangeHigh(String gridTextRangeHigh)
    {
        this.gridTextRangeHigh = gridTextRangeHigh;
    }

    /**
     * Gets the grid text range low.
     *
     * @return the grid text range low
     */
    public String getGridTextRangeLow()
    {
        return gridTextRangeLow;
    }

    /**
     * Sets the grid text range low.
     *
     * @param gridTextRangeLow the new grid text range low
     */
    public void setGridTextRangeLow(String gridTextRangeLow)
    {
        this.gridTextRangeLow = gridTextRangeLow;
    }

    /**
     * Gets the mappings.
     *
     * @return the mappings
     */
    public String getMappings()
    {
        return mappings;
    }

    /**
     * Gets the range count.
     *
     * @return the range count
     */
    public int getRangeCount()
    {
        return rangeCount;
    }

    /**
     * Checks if is hyperlink allowed.
     *
     * @return true, if is hyperlink allowed
     */
    public boolean isHyperlinkAllowed()
    {
        return allowHyperlink;
    }

    /**
     * Allow hyperlink.
     *
     * @param allowHyperlink the allow hyperlink
     */
    public void allowHyperlink(boolean allowHyperlink)
    {
        this.allowHyperlink = allowHyperlink;
    }

    /**
     * Checks if is append only.
     *
     * @return true, if is append only
     */
    public boolean isAppendOnly()
    {
        return appendOnly;
    }

    /**
     * Sets the append only.
     *
     * @param appendOnly the new append only
     */
    public void setAppendOnly(boolean appendOnly)
    {
        this.appendOnly = appendOnly;
    }

    /**
     * Gets the number of lines.
     *
     * @return the number of lines
     */
    public int getNumberOfLines()
    {
        return numberOfLines;
    }

    /**
     * Sets the number of lines.
     *
     * @param numberOfLines the new number of lines
     */
    public void setNumberOfLines(int numberOfLines)
    {
        this.numberOfLines = numberOfLines;
    }

    /**
     * Checks if is restricted mode.
     *
     * @return true, if is restricted mode
     */
    public boolean isRestrictedMode()
    {
        return isRestrictedMode;
    }

    /**
     * Sets the restricted mode.
     *
     * @param isRestrictedMode the new restricted mode
     */
    public void setRestrictedMode(boolean isRestrictedMode)
    {
        this.isRestrictedMode = isRestrictedMode;
    }

    /**
     * Checks if is rich text.
     *
     * @return true, if is rich text
     */
    public boolean isRichText()
    {
        return isRichText;
    }

    /**
     * Sets the rich text.
     *
     * @param isRichText the new rich text
     */
    public void setRichText(boolean isRichText)
    {
        this.isRichText = isRichText;
    }

    /**
     * Checks if is wiki linking.
     *
     * @return true, if is wiki linking
     */
    public boolean isWikiLinking()
    {
        return isWikiLinking;
    }

    /**
     * Gets the currency locale.
     *
     * @return the currency locale
     */
    public Locale getCurrencyLocale()
    {
        return currencyLocale;
    }

    /**
     * Sets the currency locale.
     *
     * @param currencyLocale the new currency locale
     */
    public void setCurrencyLocale(Locale currencyLocale)
    {
        this.currencyLocale = currencyLocale;
    }

    /**
     * Gets the maximum value.
     *
     * @return the maximum value
     */
    public double getMaximumValue()
    {
        return maximumValue;
    }

    /**
     * Sets the maximum value.
     *
     * @param maximumValue the new maximum value
     */
    public void setMaximumValue(double maximumValue)
    {
        this.maximumValue = maximumValue;
    }

    /**
     * Gets the minimum value.
     *
     * @return the minimum value
     */
    public double getMinimumValue()
    {
        return minimumValue;
    }

    /**
     * Sets the minimum value.
     *
     * @param minimumValue the new minimum value
     */
    public void setMinimumValue(double minimumValue)
    {
        this.minimumValue = minimumValue;
    }

    /**
     * Gets the maximum length.
     *
     * @return the maximum length
     */
    public int getMaximumLength()
    {
        return maximumLength;
    }

    /**
     * Sets the maximum length.
     *
     * @param maximumLength the new maximum length
     */
    public void setMaximumLength(int maximumLength)
    {
        this.maximumLength = maximumLength;
    }

    /**
     * Gets the url display format.
     *
     * @return the url display format
     */
    public UrlFieldDisplayFormat getUrlDisplayFormat()
    {
        return urlDisplayFormat;
    }

    /**
     * Sets the url display format.
     *
     * @param urlDisplayFormat the new url display format
     */
    public void setUrlDisplayFormat(UrlFieldDisplayFormat urlDisplayFormat)
    {
        this.urlDisplayFormat = urlDisplayFormat;
    }
}
