package com.independentsoft.share;

/**
 * The Enum PrincipalType.
 */
public enum PrincipalType {

    USER,
    DISTRIBUTION_LIST,
    SECURITY_GROUP,
    SHARE_POINT_GROUP,
    ALL,
    NONE
}
