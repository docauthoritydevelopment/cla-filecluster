package com.independentsoft.share;

/**
 * The Enum RelationshipDeleteBehaviorType.
 */
public enum RelationshipDeleteBehaviorType {

    CASCADE,
    RESTRICT,
    NONE
}
