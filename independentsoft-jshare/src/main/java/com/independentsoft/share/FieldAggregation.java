package com.independentsoft.share;

/**
 * The Enum FieldAggregation.
 */
public enum FieldAggregation {

    AVERAGE,
    COUNT,
    FIRST,
    LAST,
    MAX,
    MERGE,
    MIN,
    PLAIN_TEXT,
    SUM,
    NONE
}
