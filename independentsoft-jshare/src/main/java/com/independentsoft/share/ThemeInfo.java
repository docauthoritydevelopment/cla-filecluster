package com.independentsoft.share;

import java.io.InputStream;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

/**
 * The Class ThemeInfo.
 */
public class ThemeInfo {

    private String accessibleDescription;
    private String themeBackgroundImageUri;

    /**
     * Instantiates a new theme info.
     */
    public ThemeInfo()
    {
    }

    ThemeInfo(InputStream inputStream) throws XMLStreamException
    {
        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        XMLStreamReader reader = xmlInputFactory.createXMLStreamReader(inputStream);

        parse(reader);
    }

    ThemeInfo(XMLStreamReader reader) throws XMLStreamException
    {
        parse(reader);
    }

    private void parse(XMLStreamReader reader) throws XMLStreamException
    {
        while (reader.hasNext())
        {
            if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("properties") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices/metadata"))
            {
                while (reader.hasNext())
                {
                    if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("AccessibleDescription") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        accessibleDescription = reader.getElementText();
                    }
                    else if (reader.isStartElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("ThemeBackgroundImageUri") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices"))
                    {
                        themeBackgroundImageUri = reader.getElementText();
                    }

                    if (reader.isEndElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("properties") && reader.getNamespaceURI().equals("http://schemas.microsoft.com/ado/2007/08/dataservices/metadata"))
                    {
                        break;
                    }
                    else
                    {
                        reader.next();
                    }
                }
            }

            if (reader.isEndElement() && reader.getLocalName() != null && reader.getNamespaceURI() != null && reader.getLocalName().equals("entry") && reader.getNamespaceURI().equals("http://www.w3.org/2005/Atom"))
            {
                break;
            }
            else
            {
                reader.next();
            }
        }
    }

    /**
     * Gets the accessible description.
     *
     * @return the accessible description
     */
    public String getAccessibleDescription()
    {
        return accessibleDescription;
    }

    /**
     * Gets the theme background image uri.
     *
     * @return the theme background image uri
     */
    public String getThemeBackgroundImageUri()
    {
        return themeBackgroundImageUri;
    }
}
