package com.independentsoft.share;

/**
 * The Class Sort.
 */
public class Sort {

    private String propertyName;
    private SortDirection direction = SortDirection.ASCENDING;

    /**
     * Instantiates a new sort.
     */
    public Sort()
    {
    }

    /**
     * Instantiates a new sort.
     *
     * @param propertyName the property name
     */
    public Sort(String propertyName)
    {
        if(propertyName == null)
        {
            throw new IllegalArgumentException("propertyName");
        }
        
        this.propertyName = propertyName;
    }

    /**
     * Instantiates a new sort.
     *
     * @param propertyName the property name
     * @param direction the direction
     */
    public Sort(String propertyName, SortDirection direction)
    {
        if(propertyName == null)
        {
            throw new IllegalArgumentException("propertyName");
        }
        
        this.propertyName = propertyName;
        this.direction = direction;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        String stringValue = "{ 'Property':'" + Util.encodeJson(propertyName) + "', 'Direction':'" + EnumUtil.parseSortDirection(direction) + "' }";

        return stringValue;
    }

    /**
     * Gets the property name.
     *
     * @return the property name
     */
    public String getPropertyName()
    {
        return propertyName;
    }

    /**
     * Sets the property name.
     *
     * @param propertyName the new property name
     */
    public void setPropertyName(String propertyName)
    {
        this.propertyName = propertyName;
    }

    /**
     * Gets the direction.
     *
     * @return the direction
     */
    public SortDirection getDirection()
    {
        return direction;
    }

    /**
     * Sets the direction.
     *
     * @param direction the new direction
     */
    public void setDirection(SortDirection direction)
    {
        this.direction = direction;
    }
}
