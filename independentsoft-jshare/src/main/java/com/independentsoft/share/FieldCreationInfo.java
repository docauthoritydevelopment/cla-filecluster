package com.independentsoft.share;

import java.util.ArrayList;
import java.util.List;

/**
 * The Class FieldCreationInfo.
 */
public class FieldCreationInfo {

    private List<String> choices = new ArrayList<String>();
    private boolean isCompactName;
    private String lookupFieldName;
    private String lookupListId;
    private String lookupWebId;
    private boolean isRequired;
    private String title;
    private FieldType type = FieldType.NONE;

    /**
     * Instantiates a new field creation info.
     */
    public FieldCreationInfo()
    {
    }

    String toCreateJSon()
    {
        String stringValue = "{ 'parameters': { '__metadata': { 'type': 'SP.FieldCreationInformation' }";

        if(choices.size() > 0)
        {
            String value = "";

            for(int i=0; i < choices.size(); i++)
            {
                value += "'" + Util.encodeJson(choices.get(i)) + "'";

                if(i < choices.size() - 1)
                {
                    value += ", ";
                }
            }

            stringValue += ", 'Choices': { '__metadata': { 'type': 'Collection(Edm.String)' }, 'results': [ " + value + "] } }";
        }

        if(isCompactName)
        {
            stringValue += ", 'IsCompactName': true";
        }

        if(lookupFieldName != null && lookupFieldName.length() > 0)
        {
            stringValue += ", 'LookupFieldName':'" + Util.encodeJson(lookupFieldName) + "'";
        }

        if(lookupListId != null && lookupListId.length() > 0)
        {
            stringValue += ", 'LookupListId':'" + Util.encodeJson(lookupListId) + "'";
        }

        if(lookupWebId != null && lookupWebId.length() > 0)
        {
            stringValue += ", 'LookupWebId':'" + Util.encodeJson(lookupWebId) + "'";
        }

        if(isRequired)
        {
            stringValue += ", 'Required': true";
        }

        if(title != null && title.length() > 0)
        {
            stringValue += ", 'Title':'" + Util.encodeJson(title) + "'";
        }

        if (type != FieldType.NONE)
        {
            stringValue += ", 'FieldTypeKind': " + EnumUtil.parseFieldType(type);
        }

        stringValue +=" }";

        return stringValue;
    }

    /**
     * Gets the choices.
     *
     * @return the choices
     */
    public List<String> getChoices()
    {
        return choices;
    }

    /**
     * Checks if is compact name.
     *
     * @return true, if is compact name
     */
    public boolean isCompactName()
    {
        return isCompactName;
    }

    /**
     * Sets the compact name.
     *
     * @param isCompactName the new compact name
     */
    public void setCompactName(boolean isCompactName)
    {
        this.isCompactName = isCompactName;
    }

    /**
     * Gets the lookup field name.
     *
     * @return the lookup field name
     */
    public String getLookupFieldName()
    {
        return lookupFieldName;
    }

    /**
     * Sets the lookup field name.
     *
     * @param lookupFieldName the new lookup field name
     */
    public void setLookupFieldName(String lookupFieldName)
    {
        this.lookupFieldName = lookupFieldName;
    }

    /**
     * Gets the lookup list id.
     *
     * @return the lookup list id
     */
    public String getLookupListId()
    {
        return lookupListId;
    }

    /**
     * Sets the lookup list id.
     *
     * @param lookupListId the new lookup list id
     */
    public void setLookupListId(String lookupListId)
    {
        this.lookupListId = lookupListId;
    }

    /**
     * Gets the lookup web id.
     *
     * @return the lookup web id
     */
    public String getLookupWebId()
    {
        return lookupWebId;
    }

    /**
     * Sets the lookup web id.
     *
     * @param lookupWebId the new lookup web id
     */
    public void setLookupWebId(String lookupWebId)
    {
        this.lookupWebId = lookupWebId;
    }

    /**
     * Checks if is required.
     *
     * @return true, if is required
     */
    public boolean isRequired()
    {
        return isRequired;
    }

    /**
     * Sets the required.
     *
     * @param isRequired the new required
     */
    public void setRequired(boolean isRequired)
    {
        this.isRequired = isRequired;
    }

    /**
     * Gets the title.
     *
     * @return the title
     */
    public String getTitle()
    {
        return title;
    }

    /**
     * Sets the title.
     *
     * @param title the new title
     */
    public void setTitle(String title)
    {
        this.title = title;
    }

    /**
     * Gets the type.
     *
     * @return the type
     */
    public FieldType getType()
    {
        return type;
    }

    /**
     * Sets the type.
     *
     * @param type the new type
     */
    public void setType(FieldType type)
    {
        this.type = type;
    }
}