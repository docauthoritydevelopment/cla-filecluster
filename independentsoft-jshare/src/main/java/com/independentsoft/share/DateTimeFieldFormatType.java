package com.independentsoft.share;

/**
 * The Enum DateTimeFieldFormatType.
 */
public enum DateTimeFieldFormatType {
	
	DATE,
	DATE_TIME,
	NONE
}
