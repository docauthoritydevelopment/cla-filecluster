package com.independentsoft.share;

/**
 * The Class FieldValue.
 */
public class FieldValue {

    private String name;
    private String value;

    /**
     * Instantiates a new field value.
     */
    public FieldValue()
    {
    }

    /**
     * Instantiates a new field value.
     *
     * @param name the name
     * @param value the value
     */
    public FieldValue(String name, String value)
    {
    	this.name = name;
    	this.value = value;
    }
    
    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName()
    {
        return name;
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public String getValue()
    {
        return value;
    }
}
