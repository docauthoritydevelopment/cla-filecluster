package com.independentsoft.share;

/**
 * The Enum JoinType.
 */
public enum JoinType {

    INNER,
    LEFT_OUTER,
    RIGHT_OUTER,
    NONE
}
