package com.independentsoft.json.parser;

public class JsonString extends JsonValue {

  private final String value;

  public JsonString(String value) {
    
	if (value == null) 
    {
      throw new NullPointerException("value is null");
    }
	
    this.value = value;
  }
  
  public String getValue() {
	  return value;
  }

  @Override
  public String toString() {
    return "\"" + value + "\"";
  }

  @Override
  public int hashCode() {
    return value.hashCode();
  }

  @Override
  public boolean equals(Object object) {
    if (this == object) 
    {
      return true;
    }
    if (object == null) {
      return false;
    }
    if (getClass() != object.getClass()) {
      return false;
    }
    JsonString other = (JsonString)object;
    
    return value.equals(other.value);
  }

}
