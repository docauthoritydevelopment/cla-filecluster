package com.independentsoft.json.parser;

import java.util.ArrayList;
import java.util.List;

public class JsonArray extends JsonValue {

  private List<JsonValue> values = new ArrayList<JsonValue>();

  public JsonArray() {
  }

  public List<JsonValue> getValues() {
    return values;
  }
  
  @Override
  public String toString() {

		String stringValue = new String();
		
		stringValue += "[";

		for (int i = 0; i < values.size(); i++) 
		{
			stringValue += values.get(i).toString();
			
			if(i < values.size() - 1)
			{
				stringValue += ",";
			}
		}

		stringValue += "]";
		
		return stringValue;
	}
  
  @Override
  public int hashCode() {
    return values.hashCode();
  }

  @Override
  public boolean equals(Object object) {
    if (this == object) {
      return true;
    }
    if (object == null) {
      return false;
    }
    if (getClass() != object.getClass()) {
      return false;
    }
    JsonArray other = (JsonArray)object;
    return values.equals(other.values);
  }
}
