package com.independentsoft.json.parser;

public class JsonBoolean extends JsonValue {

  private boolean value;

  public JsonBoolean(boolean value) {
    this.value = value;
  }

  public boolean getValue() {
	  return value;
  }
  
  @Override
  public String toString() {
    return Boolean.toString(value);
  }
  
  @Override
  public int hashCode() {
    return super.hashCode();
  }

  @Override
  public boolean equals(Object object) {
    if (this == object) {
      return true;
    }
    if (object == null) {
      return false;
    }
    if (getClass() != object.getClass()) {
      return false;
    }
    JsonBoolean other = (JsonBoolean)object;
    return value == other.value;
  }

}
