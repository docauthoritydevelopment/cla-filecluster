package com.independentsoft.json.parser;

public class JsonNull extends JsonValue {

  public JsonNull() {
  }

  @Override
  public String toString() {
    return "null";
  }
  
  @Override
  public int hashCode() {
    return super.hashCode();
  }

  @Override
  public boolean equals(Object object) {
	  
    if (this == object) 
    {
      return true;
    }
    else
    {
      return false;
    }
  }
}
