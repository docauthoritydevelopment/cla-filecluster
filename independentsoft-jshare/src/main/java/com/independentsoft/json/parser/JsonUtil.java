package com.independentsoft.json.parser;

import java.io.IOException;
import java.io.StringWriter;

public class JsonUtil {

  private static final int CONTROL_END = 0x001f;
  private static final char[] QUOTE = {'\\', '"'};
  private static final char[] BACKSLASH = {'\\', '\\'};
  private static final char[] LINE_FEED = {'\\', 'n'};
  private static final char[] CARRIAGE_RETURN = {'\\', 'r'};
  private static final char[] TAB = {'\\', 't'};
  private static final char[] UNICODE_2028 = {'\\', 'u', '2', '0', '2', '8'};
  private static final char[] UNICODE_2029 = {'\\', 'u', '2', '0', '2', '9'};
  private static final char[] HEX_DIGITS = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

  public static String encodeJson(String input) throws IOException {

		int length = input.length();

		StringWriter writer = new StringWriter(length);

		int start = 0;
		
		for (int index = 0; index < length; index++) 
		{
			char[] replacement = replace(input.charAt(index));
			if (replacement != null) {
				writer.write(input, start, index - start);
				writer.write(replacement);
				start = index + 1;
			}
		}
		
		writer.write(input, start, length - start);

		return writer.toString();
	}

  private static char[] replace(char ch) {
	  
    if (ch > '\\') 
    {
      if (ch < '\u2028' || ch > '\u2029') 
      {
        return null;
      }
      else
      {
    	  return ch == '\u2028' ? UNICODE_2028 : UNICODE_2029;
      }
    }
    else if (ch == '\\') 
    {
      return BACKSLASH;
    }
    else if (ch > '"') 
    {
      return null;
    }
    else if (ch == '"') 
    {
      return QUOTE;
    }
    else if (ch > CONTROL_END) 
    {
      return null;
    }
    else if (ch == '\n') 
    {
      return LINE_FEED;
    }
    else if (ch == '\r') 
    {
      return CARRIAGE_RETURN;
    }
    else if (ch == '\t') 
    {
      return TAB;
    }
    else
    {
    	return new char[] {'\\', 'u', '0', '0', HEX_DIGITS[ch >> 4 & 0x000f], HEX_DIGITS[ch & 0x000f]};
    }
  }
}
