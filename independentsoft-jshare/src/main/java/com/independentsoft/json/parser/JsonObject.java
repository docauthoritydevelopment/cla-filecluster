package com.independentsoft.json.parser;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class JsonObject extends JsonValue {

  private List<Member> members = new ArrayList<Member>();

  public JsonObject() {    
  }

  public List<Member> getMembers() {
    return members;
  }
  
  public double getDoubleValue(String name)
  {
      JsonValue jsonValue = getValue(name);

      if (jsonValue != null && jsonValue instanceof JsonNumber)
      {
          JsonNumber jsonNumber = (JsonNumber)jsonValue;

          return jsonNumber.toDouble();
      }
      else
      {
          return Double.MIN_VALUE;
      }
  }
  
  public float getFloatValue(String name)
  {
      JsonValue jsonValue = getValue(name);

      if (jsonValue != null && jsonValue instanceof JsonNumber)
      {
          JsonNumber jsonNumber = (JsonNumber)jsonValue;

          return jsonNumber.toFloat();
      }
      else
      {
          return Float.MIN_VALUE;
      }
  }
  
  public long getLongValue(String name)
  {
      JsonValue jsonValue = getValue(name);

      if (jsonValue != null && jsonValue instanceof JsonNumber)
      {
          JsonNumber jsonNumber = (JsonNumber)jsonValue;

          return jsonNumber.toLong();
      }
      else
      {
          return Long.MIN_VALUE;
      }
  }
  
  public int getIntValue(String name)
  {
      JsonValue jsonValue = getValue(name);

      if (jsonValue != null && jsonValue instanceof JsonNumber)
      {
          JsonNumber jsonNumber = (JsonNumber)jsonValue;

          return jsonNumber.toInteger();
      }
      else
      {
          return Integer.MIN_VALUE;
      }
  }
 
  public String getStringValue(String name)
  {
      JsonValue jsonValue = getValue(name);

      if (jsonValue != null && jsonValue instanceof JsonString)
      {
          JsonString jsonString = (JsonString)jsonValue;

          return jsonString.getValue();
      }
      else
      {
          return null;
      }
  }
  
  public boolean getBooleanValue(String name)
  {
      JsonValue jsonValue = getValue(name);

      if (jsonValue != null && jsonValue instanceof JsonBoolean)
      {
          JsonBoolean jsonBoolean = (JsonBoolean)jsonValue;

          return jsonBoolean.getValue();
      }
      else
      {
          return false;
      }
  }
  
  public JsonObject getObjectValue(String name)
  {
      JsonValue jsonValue = getValue(name);

      if (jsonValue != null && jsonValue instanceof JsonObject)
      {
          JsonObject jsonObject = (JsonObject)jsonValue;

          return jsonObject;
      }

      return null;
  }
  
  public JsonValue getValue(String name)
  {
      for (int i = 0; i < members.size(); i++)
      {
          if(members.get(i).getName().equals(name))
          {
              return members.get(i).getValue();
          }
      }

      return null;
  }
 
  public List<String> getStringArrayValues(String name)
  {
      List<JsonValue> array = getArrayValues(name);
      List<String> values = new ArrayList<String>();

      for (int i = 0; i < array.size(); i++ )
      {
          if (array.get(i) != null && array.get(i) instanceof JsonString)
          {
              JsonString jsonString = (JsonString)array.get(i);

              values.add(jsonString.getValue());
          }
      }
          
      return values;
  }

  public List<JsonValue> getArrayValues(String name)
  {
      JsonValue jsonValue = getValue(name);

      if (jsonValue != null && jsonValue instanceof JsonArray)
      {
          JsonArray jsonArray = (JsonArray)jsonValue;

          return jsonArray.getValues();
      }
      else
      {
          return new ArrayList<JsonValue>();
      }
  }
  
  @Override
  public String toString() {

		String stringValue = new String();
		
		stringValue += "{";    
	      
	    for (int i=0; i < members.size(); i++)
		{
			stringValue += members.get(i).toString();
			
			if(i < members.size() - 1)
			{
				stringValue += ",";
			}
		}

		stringValue += "}";
    
		return stringValue;
  }
  
  @Override
  public int hashCode() {
    return members.hashCode();
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    JsonObject other = (JsonObject)obj;
    return members.equals(other.members);
  }

  

}
