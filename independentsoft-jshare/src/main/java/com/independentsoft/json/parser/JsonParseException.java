package com.independentsoft.json.parser;

public class JsonParseException extends RuntimeException {

  private final int offset;
  private final int line;
  private final int column;

  public JsonParseException(String message, int offset, int line, int column) {
    super(message + " at " + line + ":" + column);
    this.offset = offset;
    this.line = line;
    this.column = column;
  }

  public int getOffset() {
    return offset;
  }

  public int getLine() {
    return line;
  }

  public int getColumn() {
    return column;
  }

}
