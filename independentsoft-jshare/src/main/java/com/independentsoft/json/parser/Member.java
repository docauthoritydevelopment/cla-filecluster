package com.independentsoft.json.parser;

public class Member {

	private String name;
	private JsonValue value;
	
    public Member()
    {
    }

    public Member(String name, JsonValue value)
    {
        this.name = name;
        this.value = value;
    }
    
    @Override
    public String toString() 
    {
  		String stringValue =  "\"" + name + "\":";
     	stringValue += value.toString();      
  		return stringValue;
    }
    
    public String getName()
    {
    	return name;
    }
    
    public void setName(String name)
    {
    	this.name = name;
    }
    
    public JsonValue getValue()
    {
    	return value;
    }
    
    public void setValue(JsonValue value)
    {
    	this.value = value;
    }
}
