package com.independentsoft.json.parser;

public class JsonNumber extends JsonValue {

  private String value;

  public JsonNumber(int value) {	  
	  this.value = Integer.toString(value);
  }
  
  public JsonNumber(long value) {	  
	  this.value = Long.toString(value);
  }
  
  public JsonNumber(float value) {	  
	  this.value = Float.toString(value);
  }
  
  public JsonNumber(double value) {	  
	  this.value = Double.toString(value);
  }
  
  public JsonNumber(String value) {	  
	  this.value = value;
  }
  
  public int toInteger() {
	  return Integer.parseInt(value, 10);
  }
  
  public long toLong() {
	  return Long.parseLong(value, 10);
  }
  
  public float toFloat() {
	  return Float.parseFloat(value);
  }
  
  public double toDouble() {
	  return Double.parseDouble(value);
  }

  @Override
  public String toString() {
    return value;
  }

  @Override
  public int hashCode() {
    return value.hashCode();
  }
  
  @Override
  public boolean equals(Object object) {
    if (this == object) {
      return true;
    }
    if (object == null) {
      return false;
    }
    if (getClass() != object.getClass()) {
      return false;
    }
    JsonNumber other = (JsonNumber)object;
    return value.equals(other.value);
  }

}
