package com.cla.filecluster.util;

import org.junit.Test;

import static org.junit.Assert.*;

public class ActiveAssociationUtilsTest {

    @Test
    public void testGetActiveAssociationIdentifier() {
        assertEquals("g.4.43", ActiveAssociationUtils.getActiveAssociationIdentifier("g.4.43.MANUAL.f0"));
        assertEquals("dt.g.8", ActiveAssociationUtils.getActiveAssociationIdentifier("dt.g.8..f0"));
        assertEquals("g.4.41", ActiveAssociationUtils.getActiveAssociationIdentifier("g.4.41.MANUAL.f1;g.4.43.MANUAL.f0"));
        assertEquals("dt.g.8.24", ActiveAssociationUtils.getActiveAssociationIdentifier("dt.g.8.24..f1;dt.g.8..f0"));
        assertEquals("g.1.3", ActiveAssociationUtils.getActiveAssociationIdentifier("g.1.3.MANUAL.vdt.f0"));
        assertEquals("dept.5", ActiveAssociationUtils.getActiveAssociationIdentifier("dept.5.f0"));
        assertEquals("dept.2", ActiveAssociationUtils.getActiveAssociationIdentifier("dept.2.f1;dept.5.f0"));
        assertEquals("g.1.7", ActiveAssociationUtils.getActiveAssociationIdentifier("g.1.7.MANUAL.f1;g.1.3.MANUAL.vdt.f0"));
        assertEquals("g.23.121", ActiveAssociationUtils.getActiveAssociationIdentifier("g.23.121.MANUAL.fileGroup"));
        assertEquals("dt.g.8.9", ActiveAssociationUtils.getActiveAssociationIdentifier("dt.g.8.9..fileGroup;dt.g.8..f0"));
        assertEquals("g.1.3", ActiveAssociationUtils.getActiveAssociationIdentifier("g.1.3.MANUAL.vdt.fileGroup"));
        assertEquals("g.1.3", ActiveAssociationUtils.getActiveAssociationIdentifier("g.1.3.MANUAL.vdt.fileGroup;g.1.3.MANUAL.vdt.f0"));
        assertEquals("g.1.7", ActiveAssociationUtils.getActiveAssociationIdentifier("g.1.7.MANUAL.file;g.1.9.MANUAL.f2;g.1.7.MANUAL.f1;g.1.3.MANUAL.vdt.f0"));
        assertEquals("dt.g.44", ActiveAssociationUtils.getActiveAssociationIdentifier("dt.g.44..file;dt.g.6..f0"));
        assertEquals("g.2.17", ActiveAssociationUtils.getActiveAssociationIdentifier("g.2.17.MANUAL.file"));
        assertEquals("g.1.9", ActiveAssociationUtils.getActiveAssociationIdentifier("g.1.9.MANUAL.f2;g.1.7.MANUAL.f1;g.1.3.MANUAL.vdt.f0"));
    }
}