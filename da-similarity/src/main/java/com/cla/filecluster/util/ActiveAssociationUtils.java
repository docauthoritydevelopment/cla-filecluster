package com.cla.filecluster.util;

/**
 * Utility created to calculate active association field from department or scoped tags
 * set in this project so can be used by both server and solr plugin
 *
 * Created by: yael
 * Created on: 9/19/2019
 */
public class ActiveAssociationUtils {

    /**
     * calculate active association value
     * @param prioritizedAssociationIdentifier - prioritized value of association
     * @return active association identifier (without priority or tag action)
     */
    public static String getActiveAssociationIdentifier(String prioritizedAssociationIdentifier) {
        if (prioritizedAssociationIdentifier == null) {
            return null;
        }
        String[] vals = prioritizedAssociationIdentifier.split(";");
        String result = vals[0];
        if (result.endsWith(".file") || result.endsWith(".fileGroup") || result.matches("^.*[.]+f[0-9]+$")) {
            result = result.substring(0, result.lastIndexOf("."));
        }
        result = result.replace(".MANUAL", "");
        result = result.replace(".SUGGESTION", "");
        result = result.replace(".APPROVAL", "");
        result = result.replace(".OVERRULE", "");
        result = result.replace(".IGNORE", "");
        result = result.replace(".vdt", "");
        while (result.endsWith(".")) {
            result = result.substring(0, result.length()-1);
        }
        return result;
    }
}
