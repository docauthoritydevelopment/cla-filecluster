package com.cla.filecluster.util.pv;

import com.cla.common.domain.dto.file.ExcelAnalysisMetadata;
import com.cla.common.domain.dto.file.FileCollections;
import com.cla.common.domain.dto.histogram.ClaHistogramCollection;
import com.cla.common.domain.dto.histogram.ClaLongHistogramCollectionArray;
import com.cla.common.domain.dto.histogram.ClaSuperCollection;
import com.cla.common.domain.dto.mediaproc.AnalysisMetadata;
import com.cla.common.domain.dto.mediaproc.excel.ExcelSheetMetaData;
import com.cla.common.domain.dto.pv.PVType;
import com.cla.common.domain.dto.pv.PropertyVectorKey;
import com.cla.common.domain.dto.pv.SimilarityMapKey;
import com.cla.common.serialization.BackwardCompatabilityInputStream;
import com.cla.filecluster.domain.entity.pv.PvAnalysisData;
import com.cla.filecluster.domain.entity.pv.PvAnalysisDataFormat;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.factories.ReflectionSerializerFactory;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import com.esotericsoftware.kryo.serializers.CompatibleFieldSerializer;
import org.apache.commons.text.similarity.LevenshteinDistance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;
import java.util.zip.GZIPInputStream;

/**
 * Created by uri on 08/02/2017.
 */
public class PvAnalysisDataUtils {

    public static final int QUERY_RESPONSE_COUNT = 0;
    public static final int MAP_ENTRIES_COUNT = 1;
    private static Logger logger = LoggerFactory.getLogger(PvAnalysisDataUtils.class);

    /**
     * Kryo is not thread safe, so serialization in parallel must be done locally
     * When initializing a kryo instance it's best to register known classs
     * for optimized performance
     */
    static private final ThreadLocal<Kryo> kryos = ThreadLocal.withInitial(() -> {
        Kryo kryo = new Kryo();
        kryo.register(FileCollections.class);
        kryo.register(ClaSuperCollection.class);
        kryo.register(ClaHistogramCollection.class);
        kryo.register(AnalysisMetadata.class);
        kryo.register(ExcelAnalysisMetadata.class);
        kryo.register(ExcelSheetMetaData.class);
        kryo.register(ClaLongHistogramCollectionArray.class);
        kryo.register(LinkedHashMap.class);
        return kryo;
    });


    public static String buildQueryClauseForPv(final PropertyVectorKey propertyVectorKey, final ClaHistogramCollection<Long> claHistogramCollection) {
        final PVType pvt = PVType.valueOf(propertyVectorKey.getPvType());
        try {
            return claHistogramCollection.getJoinedKeysWithBoost(pvt.name() + ":");
        } catch (final Exception e) {
            logger.error("Unexpected error while generating query string for {}. {}", propertyVectorKey.toString(), e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }

    /**
     * analyze the metadata for the excel sheet
     * 1. extract the total number of cells and the number
     * 2. Compute the cell ratio relative to the total number for each sheet
     *
     * @param excelAnalysisMetadata excel analysis metadata
     * @return map of sheets and their cell ratios
     */
    public static Map<String, Float> getExcelSheetsDominance(final ExcelAnalysisMetadata excelAnalysisMetadata) {
        final Map<String, Float> sheetsDominanceRatio = new HashMap<>();
        if (excelAnalysisMetadata != null) {
            final Map<String, ExcelSheetMetaData> sheetsMd = excelAnalysisMetadata.getSheetsMetadata();
            final int totalCells = sheetsMd.values().stream()
                    .mapToInt(ExcelSheetMetaData::getNumOfCells).sum();
            if (totalCells <= 0) {
                logger.debug("Unexpected value for workbool totalCells={}", totalCells);
                return sheetsDominanceRatio;
            }

            // Calculate sheets dominance ratio per num of cells in each sheet.
            sheetsMd.forEach((key, value) -> sheetsDominanceRatio.put(key,
                    ((float) value.getNumOfCells()) / ((float) totalCells)));
        }
        return sheetsDominanceRatio;
    }

    public static List<String> sortSheetsBySize(Map<String, Float> sheetsDominanceRatio) {
        return sheetsDominanceRatio.entrySet().stream()
                .sorted((e1, e2) -> Float.compare(e2.getValue(), e1.getValue()))
                .map(Map.Entry::getKey).collect(Collectors.toList());
    }

    public static int[] createAuditTrailObject() {
        final int[] auditTrail = new int[2];
        auditTrail[QUERY_RESPONSE_COUNT] = 0;
        auditTrail[MAP_ENTRIES_COUNT] = 0;
        return auditTrail;
    }

    public static int[] convertSimilarityMapKeysToInternalIds(Collection<SimilarityMapKey> similarityMapKeys) {
        return similarityMapKeys.stream().mapToInt(SimilarityMapKey::getInternalId).toArray();
    }

    public static void mapPartToDoc(final Map<Long, Map<String, List<String>>> matchedSheetsByDoc, final SimilarityMapKey matchedPartKey, final String part) {
        final Long matchedDocLid = matchedPartKey.getDocIdFromKey();
        final String matchedpart = matchedPartKey.getPartFromKey();
        // matchedSheetsByDoc: destimationDocId -> ( matchedPart -> List<matching destination parts or the referencing destination docId> )
        Map<String, List<String>> docMap = matchedSheetsByDoc.computeIfAbsent(matchedDocLid, k -> new HashMap<>());
        List<String> matchedParts = docMap.computeIfAbsent(part, k -> new ArrayList<>());
        matchedParts.add(matchedpart);
//		if (logger.isTraceEnabled()) {
//			logger.trace("@@@ mapPartToDoc: adding part {} to matchedList of parts for {} (new size={}) matchedDoc={}", part, matchedpart, matchedParts.size(), matchedDocLid);
//		}
    }

    public static float workBookAggregatedSimilarity(long contentId, final Long matchedDocLid, final Map<String, List<String>> map,
                                                     final Map<String, Float> sheetsDominanceRatio, final Map<String, String> sheetMatches) {

        if (sheetsDominanceRatio == null) {
            return 0f;
        }

        /*
         * Proposed matching strategy: sort sheets by number of matches and then by #cells.
         * Create an empty pickedParts list.
         * Iterate over the sorted list:
         * 			skip matches which are in pickedParts list. If got no matches, log a debug message 'left out without matches ...'
         * 			if 1 match, take it; if has match with same name, take it. (future: pick match with lowest edit distance)
         * 			or take the first one.
         * 			Add picked match to pickedParts list
         * After max sheets are matched, accumulate their dominace rate
         */
        final Set<String> matchedSheets = new HashSet<>();
        final float result = (float) sheetsDominanceRatio.entrySet().stream()
                .filter(e -> map.get(e.getKey()) != null)    // remove unmapped sheets
                .sorted((e1, e2) -> compareSheetMd(e1, e2, map)).mapToDouble(entry -> {
                    final String currSheet = entry.getKey();
                    final List<String> matchCandidates = map.get(currSheet);
                    if (matchCandidates != null) {
                        final String bestMatchedSheet = matchCandidates.stream()
                                .filter(s -> !matchedSheets.contains(s))
                                .min(Comparator.comparingInt(s -> getEditDistance(currSheet, s)))
                                .orElse(null);
                        if (bestMatchedSheet != null) {
                            if (logger.isTraceEnabled()) {
                                logger.trace("AggregatedSimilarity: best match for {} is {} dominance {}", currSheet, bestMatchedSheet, sheetsDominanceRatio.get(currSheet));
                            }
                            matchedSheets.add(bestMatchedSheet);
                            sheetMatches.put(currSheet, bestMatchedSheet);
                            return Double.valueOf(entry.getValue());
                        }
                    }
                    return (double) 0;
                }).sum();

        if (logger.isTraceEnabled()) {
            logger.trace("AggregatedSimilarity: returned value {} for {} and {}",
                    result, contentId, matchedDocLid);
        }
        return result;
    }

    // Sort by maps list size ascending, and then by sheet size descending.
    private static int compareSheetMd(final Map.Entry<String, Float> e1, final Map.Entry<String, Float> e2, final Map<String, List<String>> map) {
        final int maps1 = map.get(e1.getKey()).size();
        final int maps2 = map.get(e2.getKey()).size();
        if (maps1 != maps2) {
            return (maps1 - maps2);
        }

        return Float.compare(e2.getValue(), e1.getValue());
    }

    private static int getEditDistance(final String s1, final String s2) {
        try {
            return
                    LevenshteinDistance.getDefaultInstance().apply(s1, s2);
        } catch (final Exception e) {
            return -1;
        }
    }

    private static Map<String, Integer> getSheetsOrdinals(final ExcelAnalysisMetadata excelAnalysisMetadata) {
        Map<String, ExcelSheetMetaData> sheetsMetadata = excelAnalysisMetadata.getSheetsMetadata();
        List<String> sheetsFullNames = excelAnalysisMetadata.getSheetsFullNames();
        Map<String, Integer> res = new HashMap<>(sheetsMetadata.size());
        Iterator<String> iter = sheetsFullNames.iterator();
        int i = 0;
        while (iter.hasNext()) {
            String e = iter.next();
            res.put(e, i++);
        }
        return res;
    }

    public static float limitProcessedSheets(ExcelAnalysisMetadata analysisMeadata, Map<String, Float> sheetsDominanceRatio,
                                             List<String> sheetsSortedBySizeDesc,
                                             int groupedExcelSheetsProcessingFirstThreshold, int groupedExcelSheetsProcessingLastThreshold) {
        float skippedSheetsDominance = 0;                // Track dominance that is ignored within similarity optimization
        final Map<String, Integer> sheetsOrdinals = PvAnalysisDataUtils.getSheetsOrdinals(analysisMeadata);
        if (sheetsOrdinals.size() > (groupedExcelSheetsProcessingFirstThreshold + groupedExcelSheetsProcessingLastThreshold)) {
            skippedSheetsDominance = (float) sheetsDominanceRatio.entrySet().stream()
                    .filter(e -> isSheetSkippedByOrdinalThresholds(e.getKey(), sheetsOrdinals, groupedExcelSheetsProcessingFirstThreshold, groupedExcelSheetsProcessingLastThreshold))
                    .mapToDouble(Map.Entry::getValue).sum();
            if (skippedSheetsDominance == 0f) {
                logger.warn("Skipped sheets total dominance sum is unexpected {}", skippedSheetsDominance);
            }
            final float fSkippedSheetsDominance = skippedSheetsDominance;
            sheetsOrdinals.forEach((sheetName, value) -> {
                if (isSheetSkippedByOrdinalThresholds(value, sheetsOrdinals.size(), groupedExcelSheetsProcessingFirstThreshold, groupedExcelSheetsProcessingLastThreshold)) {
                    logger.debug("Skip sheet {} ordinal {} not withing first {} or last {}", sheetName, value, groupedExcelSheetsProcessingFirstThreshold, groupedExcelSheetsProcessingLastThreshold);
                    sheetsDominanceRatio.remove(sheetName);
                    sheetsSortedBySizeDesc.remove(sheetName);
                } else {
                    float updDominance = sheetsDominanceRatio.get(sheetName) / (1f - fSkippedSheetsDominance);
                    sheetsDominanceRatio.put(sheetName, updDominance);
                    logger.debug("Update sheetDominance (compensate for skipped). sheet={} New dom={}", sheetName, updDominance);
                }
            });
        }
        return skippedSheetsDominance;
    }

    private static boolean isSheetSkippedByOrdinalThresholds(int sheetOrd, int numSheets,
                                                             int groupedExcelSheetsProcessingFirstThreshold, int groupedExcelSheetsProcessingLastThreshold) {
        boolean keep = (sheetOrd < groupedExcelSheetsProcessingFirstThreshold ||
                sheetOrd >= (numSheets - groupedExcelSheetsProcessingLastThreshold));
        return !keep;
    }

    private static boolean isSheetSkippedByOrdinalThresholds(final String key, final Map<String, Integer> sheetsOrdinals,
                                                             int groupedExcelSheetsProcessingFirstThreshold, int groupedExcelSheetsProcessingLastThreshold) {
        if (!sheetsOrdinals.containsKey(key)) {
            logger.error("Could not find sheet {} within sheetsOnridnals {}", key, sheetsOrdinals.keySet().toString());
            return true;
        }
        final int sheetOrd = sheetsOrdinals.get(key);
        final int numSheets = sheetsOrdinals.size();
        return isSheetSkippedByOrdinalThresholds(sheetOrd, numSheets, groupedExcelSheetsProcessingFirstThreshold, groupedExcelSheetsProcessingLastThreshold);
    }

    public static byte[] serializeToByteArray(Serializable o, Class<?> type) {
        try (ByteArrayOutputStream baos = new ByteArrayOutputStream();
             Output oos = new Output(baos)) {
            kryos.get().writeObjectOrNull(oos, o, o == null ? type : o.getClass());
            oos.flush();
            return baos.toByteArray();
        } catch (IOException e) {
            throw new RuntimeException("Failed to serialize PV Analysis data collections to Base64", e);
        }

    }

    /**
     * Serialize pvCollecoins using kryo fast & efficient mechanism
     *
     * @param pvCollections PV collections
     * @return serialized byte array
     */
    public static byte[] serializePvCollections(FileCollections pvCollections) {
        try (ByteArrayOutputStream baos = new ByteArrayOutputStream();
             Output oos = new Output(baos)) {
            // Using this API cause object cannot be null and class is known
            kryos.get().writeObject(oos, pvCollections);
            oos.flush();
            return baos.toByteArray();
        } catch (IOException e) {
            throw new RuntimeException("Failed to serialize PV Analysis data collections to Base64", e);
        }
    }

    /**
     * Serialize analysis metadata using kryo fast & efficient serialize mechanism
     *
     * @param analysisMeadata analysis meadata
     * @return serialized byte array
     */
    public static byte[] serializeAnalysisMetadata(AnalysisMetadata analysisMeadata) {
        try (ByteArrayOutputStream baos = new ByteArrayOutputStream();
             Output oos = new Output(baos)) {
            // Using this api because analysis metadata can be null and class is not known at compile time
            kryos.get().writeClassAndObject(oos, analysisMeadata);
            oos.flush();
            return baos.toByteArray();
        } catch (IOException e) {
            throw new RuntimeException("Failed to serialize PV Analysis data collections to Base64", e);
        }
    }

    /**
     * Converting a serialized document into PvAnalysisData
     *
     * @param analysisDataParams document map
     * @param compressed         whether is compresses
     * @param compatibilityMap   compatibility map for legacy serialization
     * @return PvAnalysisData
     */
    public static PvAnalysisData convertDocumentToPvAnalysisData(Map<String, Object> analysisDataParams, boolean compressed,
                                                                 Map<String, String> compatibilityMap) {
        // Extract data to build PvAnalysisData
        Long contentId = Long.valueOf((String) analysisDataParams.get("id"));
        byte[] pvCollectionsSrc = (byte[]) analysisDataParams.get("pvCollections");
        byte[] metadataSrc = (byte[]) analysisDataParams.get("metadata");
        int maturity = (Integer) analysisDataParams.getOrDefault("maturity", 0);

        // Convert according tp PvAnalysis format.
        Integer format = (Integer) analysisDataParams.get("format");
        if (format == null) { // Case no format then this object is legacy using the standard java serializer
            return lagacyConvertBinaryToPvCollectionsAnalysisData(contentId, compressed, pvCollectionsSrc, metadataSrc, maturity, compatibilityMap);
        } else {
            PvAnalysisDataFormat pvAnalysisDataFormat = PvAnalysisDataFormat.values()[format];
            switch (pvAnalysisDataFormat) {
                case PV_COLLECTIONS:
                case PV_ARRAYS:
                    return convertBinaryToPvCollectionsAnalysisData(contentId, pvCollectionsSrc, metadataSrc, maturity);
            }
            throw new RuntimeException(String.format("PvAnalysisData format %s is not supported yet", pvAnalysisDataFormat));
        }
    }

    private static PvAnalysisData lagacyConvertBinaryToPvCollectionsAnalysisData(Long contentId, boolean compressed, byte[] pvCollectionsSrc, byte[] metadataSrc, int maturity, Map<String, String> compatibilityMap) {
        FileCollections pvCollections = (FileCollections) legacyDeserializeFromByteArray(pvCollectionsSrc, compressed, compatibilityMap);
        AnalysisMetadata metaData = (AnalysisMetadata) legacyDeserializeFromByteArray(metadataSrc, compressed, compatibilityMap);
        PvAnalysisData pvAnalysisData = new PvAnalysisData(contentId, pvCollections, metaData);
        pvAnalysisData.setMaturityFile(maturity);
        if (logger.isTraceEnabled()) {
            logger.trace("convertBinaryToPvAnalysisData: compressed={}, pvCollectionsSrc({}), metadataSrc({}), maturity={}",
                    compressed ? "true" : "false", pvCollectionsSrc.length, metadataSrc.length, maturity);
        }
        return pvAnalysisData;
    }

    /**
     * Generic method for deserializing binary content
     * (Used for {@link FileCollections} or {@link AnalysisMetadata})
     *
     * @param data                 byte array data
     * @param compressAnalysisData whether the byte array is compressed
     * @return deserialized object
     */
    public static Object legacyDeserializeFromByteArray(byte[] data, boolean compressAnalysisData, Map<String, String> compatibilityMap) {
        if (compressAnalysisData) {
            return deserializeFromCompressedByteArray(data, compatibilityMap);
        } else {
            return deserializeFromUncompressedByteArray(data);
        }
    }

    /**
     * Read the object from Base64 string.
     */
    private static Object deserializeFromCompressedByteArray(byte[] data, Map<String, String> compatibilityMap) {
        try (
                InputStream compressorInputStream = new GZIPInputStream(new ByteArrayInputStream(data));
                ObjectInputStream ois = new BackwardCompatabilityInputStream(
                        compressorInputStream, compatibilityMap)
        ) {
            return ois.readObject();
        } catch (IOException | ClassNotFoundException e) {
            throw new RuntimeException("Failed to deserialize from compressed string", e);
        }
    }

    /**
     * Read the object from Base64 string.
     */
    private static Object deserializeFromUncompressedByteArray(byte[] data) {
        try (ObjectInputStream ois = new ObjectInputStream(
                new ByteArrayInputStream(data))) {
            return ois.readObject();
        } catch (IOException e) {
            throw new RuntimeException("IO Exception - Failed to deserialize object from byte array of size " + data.length, e);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("Class not found Exception - Failed to deserialize object from byte array of size " + data.length, e);
        }
    }

    /**
     * Deserializing binary into {@link FileCollections} using kryo
     */
    public static FileCollections deserializePvCollections(byte[] data) {
        try (Input input = new Input(data)) {
            return kryos.get().readObject(input, FileCollections.class);
        } catch (Exception e) {
            throw new RuntimeException("Exception - Failed to deserialize object from byte array of size " + data.length, e);
        }
    }

    /**
     * Deserializing binary into {@link AnalysisMetadata} using kryo
     */
    private static AnalysisMetadata deserializeAnalysisMetadata(byte[] data) {
        try (Input input = new Input(data)) {
            // Using thi API cause object can be null and class is written in the data
            return (AnalysisMetadata) kryos.get().readClassAndObject(input);
        } catch (Exception e) {
            throw new RuntimeException("Exception - Failed to deserialize object from byte array of size " + data.length, e);
        }
    }

    /**
     * Constructing PvAnalysisData binary data using kryo serialization framework
     *
     * @param contentId        content ID
     * @param pvCollectionsSrc byte array of PV collection
     * @param metadataSrc      byte array of metadata
     * @param maturity         maturity rank
     * @return PvAnalysisData
     */
    public static PvAnalysisData convertBinaryToPvCollectionsAnalysisData(Long contentId, byte[] pvCollectionsSrc,
                                                                          byte[] metadataSrc, int maturity) {
        FileCollections pvCollections = deserializePvCollections(pvCollectionsSrc);
        AnalysisMetadata metaData = deserializeAnalysisMetadata(metadataSrc);
        PvAnalysisData pvAnalysisData = new PvAnalysisData(contentId, pvCollections, metaData);
        pvAnalysisData.setMaturityFile(maturity);
        return pvAnalysisData;
    }

}
