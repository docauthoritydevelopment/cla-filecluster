package com.cla.filecluster.service.files.pv;

import com.cla.common.domain.dto.pv.PVType;
import com.cla.common.domain.dto.pv.SimilarityRates;
import com.cla.connector.domain.dto.file.FileType;

import java.util.HashMap;
import java.util.Map;

public class SimplifiedSimilarityMap {
	private final long documentId;
	private final FileType fileType;
	private final PVType pvType;
	private final String sheet;
	private Map<Integer, SimilarityRates> map = new HashMap<>();

	public SimplifiedSimilarityMap(long documentId, FileType fileType, PVType pvType,String sheet) {
		this.documentId = documentId;
		this.fileType = fileType;
		this.pvType = pvType;
		this.sheet = sheet;
	}
	
//	public SimilarityMapKey getKey(final Long docLid, final String part) {
//		return new SimilarityMapKey(docLid, part);
//	}
	
	private int counter = 0;

	public int incrementCounter() {
		return ++counter;
	}

	public int getCounter() {
		return counter;
	}

	public void resetCounter() {
		this.counter = 0;
	}

	public void setCounter(final int counter) {
		this.counter = counter;
	}

	public SimilarityRates getRates() {
		return new SimilarityRates();
	}	

	public Map<Integer, SimilarityRates> getSimilarityMap() {
		return map;
	}
	public SimilarityRates getRates(final Integer simKey) {
		return map.get(simKey);
	}
	public void setRates(final Integer simKey, final SimilarityRates r) {
		map.put(simKey, r);
	}
	public float getSimilarityRate(final Integer simKey) {
		final SimilarityRates r = map.get(simKey);
		return (r==null)?0f:r.getSimilarity();
	}
	public float getInclusionRate(final Integer simKey) {
		final SimilarityRates r = map.get(simKey);
		return (r==null)?0f:r.getInclusion();
	}
	public int size() {
		return map.size();
	}

	@Override
	public String toString() {
		return "SimplifiedSimilarityMap:{" + map + "}";
	}
	
	public void close() {
		if ((!isClosed()) && !map.isEmpty()) {	// Do not close the null map instance (nullSimilarityMap)
			map.clear();
			map = null;
		}
	}
	
	public boolean isClosed() {
		return (map == null);
	}

	public void merge(final SimplifiedSimilarityMap sim) {
		sim.getSimilarityMap().entrySet().forEach(e->{
			final SimilarityRates rates = getRates(e.getKey());
			if (rates==null) {
				// new key in sim - add it
				setRates(e.getKey(), e.getValue());
			}
			else {
				// key exists - sum values
				rates.setSimilarity(rates.getSimilarity()+e.getValue().getSimilarity());
				rates.setInclusion(rates.getInclusion()+e.getValue().getInclusion());
			}
		});
	}

	public void factorRates(final float factor) {
		getSimilarityMap().values().forEach(r->{
			r.setSimilarity(factor*r.getSimilarity());
			r.setInclusion(factor*r.getInclusion());
		});
	}

	/*public SimplifiedSimilarityMap filterMinRate(final float minSimRateThreshold) {
		final SimplifiedSimilarityMap ret = new SimplifiedSimilarityMap();
		getSimilarityMap().entrySet().stream().filter(e->(e.getValue().getSimilarity()>=minSimRateThreshold))
		.forEach(e->ret.setRates(e.getKey(), e.getValue()));
		return ret;
	}*/


	public long getDocumentId() {
		return documentId;
	}

	public FileType getFileType() {
		return fileType;
	}

	public PVType getPvType() {
		return pvType;
	}

	public String getSheet() {
		return sheet;
	}
}
