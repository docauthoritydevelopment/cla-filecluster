package com.cla.filecluster.service.files.pv;

import java.util.Collection;
import java.util.Map;

/**
 * Created by uri on 08/02/2017.
 */
public class ContentIdGroupsPool {

    private Map<Long, ContentGroupDetails> recordMap = null;

    public ContentIdGroupsPool(GrpHelperFetcher grpHelperFetcher, final Collection<Long> contentIdList) {
        final long startTime = System.currentTimeMillis();
        recordMap = grpHelperFetcher.getContentGroupByIds(contentIdList);
        final long time = System.currentTimeMillis() - startTime;
        int size = contentIdList.size();
        grpHelperFetcher.updateStatistics(time,size);
//        fileGroupsPoolManager.totalTime += time;
//        fileGroupsPoolManager.count++;
//        fileGroupsPoolManager.totalRecords += size;


//        if (fileGroupsPoolManager.count % 100 == 0) {
//            if (FileGroupsPoolManager.logger.isDebugEnabled()) {
//                FileGroupsPoolManager.logger.debug("ContentGroupsPoolManager read in {}ms. size={} count={} avgSize={} avgTime={}",
//                        time, size, fileGroupsPoolManager.getCount(), fileGroupsPoolManager.getAveragePoolSize(), fileGroupsPoolManager.getAverageReadTimeMS());
//            }
//        } else {
//            if (FileGroupsPoolManager.logger.isTraceEnabled()) {
//                FileGroupsPoolManager.logger.trace("FileGroupsPoolManager read in {}ms. size={} count={} avgSize={} avgTime={}",
//                        time, size, fileGroupsPoolManager.getCount(), fileGroupsPoolManager.getAveragePoolSize(), fileGroupsPoolManager.getAverageReadTimeMS());
//            }
//        }
    }

    public ContentGroupDetails getContentGroupId(final long contentId) {
        return recordMap.get(contentId);
    }

}
