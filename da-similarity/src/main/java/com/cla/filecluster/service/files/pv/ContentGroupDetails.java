package com.cla.filecluster.service.files.pv;

import com.cla.common.domain.dto.file.AnalyzeHint;

/**
 * Created by uri on 2/13/2017.
 */
public class ContentGroupDetails {
    String groupId;
    AnalyzeHint analyzeHint;

    public ContentGroupDetails(String groupId, AnalyzeHint analyzeHint) {
        this.groupId = groupId;
        this.analyzeHint = analyzeHint;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public AnalyzeHint getAnalyzeHint() {
        return analyzeHint;
    }

    public void setAnalyzeHint(AnalyzeHint analyzeHint) {
        this.analyzeHint = analyzeHint;
    }
}
