package com.cla.filecluster.service.files.pv;

import com.cla.filecluster.domain.entity.pv.PvAnalysisData;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by uri on 05/02/2017.
 */
public class PvAnalysisDataPool {

    private PvAnalysisDataFetcher pvAnalysisDataFetcher;
    private Map<Long, PvAnalysisData> recordMap = null;

    public PvAnalysisDataPool(final Collection<Long> contentIdList, PvAnalysisDataFetcher pvAnalysisDataFetcher) {
        this.pvAnalysisDataFetcher = pvAnalysisDataFetcher;
        if (contentIdList == null) {
            return;        // no pre-fetch
        }

        final long startTime = System.currentTimeMillis();
        final List<PvAnalysisData> pvAnalysisDataList = pvAnalysisDataFetcher.getPvAnalysisDataByContentIds(contentIdList);
        final long time = System.currentTimeMillis() - startTime;
        pvAnalysisDataFetcher.updateStatistics(time, contentIdList.size());
        recordMap = new HashMap<>(pvAnalysisDataList.size());
        pvAnalysisDataList.forEach(rec -> recordMap.put(rec.getContentId(), rec));

    }

    public PvAnalysisDataPool(List<PvAnalysisData> pvAnalysisDataList) {
        recordMap = new HashMap<>(pvAnalysisDataList.size());
        pvAnalysisDataList.forEach(rec -> recordMap.put(rec.getContentId(), rec));
    }

    public PvAnalysisData getRecord(final long contentId) {
        return (recordMap == null) ?
                pvAnalysisDataFetcher.getPvAnalysisDataByContentId(contentId) :
                recordMap.get(contentId);
    }

}
