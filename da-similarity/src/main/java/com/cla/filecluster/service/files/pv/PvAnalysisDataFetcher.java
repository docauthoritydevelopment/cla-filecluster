package com.cla.filecluster.service.files.pv;

import com.cla.filecluster.domain.entity.pv.PvAnalysisData;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Collection;
import java.util.List;

/**
 * Created by uri on 02/02/2017.
 */
public interface PvAnalysisDataFetcher {

    PvAnalysisData getPvAnalysisDataByContentId(Long contentId);

    Pair<PvAnalysisData,Integer> getPvAnalysisDataAndSizeByContentId(Long contentId);

    List<PvAnalysisData> getPvAnalysisDataByContentIds(Collection<Long> contentIdList);

    List<Pair<PvAnalysisData,Integer>> getPvAnalysisDataByContentIdsAndSize(Collection<Long> contentIdList);

    void updateStatistics(long time, int size);
}
