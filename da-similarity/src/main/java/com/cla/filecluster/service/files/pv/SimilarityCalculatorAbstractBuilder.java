package com.cla.filecluster.service.files.pv;

import com.cla.common.domain.dto.pv.ExcelSimilarityParameters;
import com.cla.common.domain.dto.pv.WordSimilarityParameters;

/**
 * Created by uri on 09/02/2017.
 */
public abstract class SimilarityCalculatorAbstractBuilder {

    protected boolean groupingForceMaturityLevelSimilarity;
    protected boolean maturityModelSingleMaturityLevel;
    protected String maturityContentPvNames;
    private PvAnalysisDataFetcher pvAnalysisDataFetcher;
    protected String maturityLowThreshodsByPV;
    protected String maturityMediumThreshodsByPV;
    protected String maturityPvGroups;

    protected String[] wordSimilarityContentHighThreshold;
    protected String[] wordSimilarityStyleLowThreshold;
    protected String[] wordSimilarityDocstartLowThreshold;
    protected int[] wordMinStyleItems;
    protected int[] wordMinLetterHeadItems;
    protected int[] wordMinStyleSequenceItems;
    protected String[] wordSimilarityContentLowThreshold;
    protected String[] wordSimilarityHeadingsLowThreshold;
    protected String[] wordSimilarityStyleHighThreshold;
    protected String[] wordSimilarityHeadingsHighThreshold;
    protected int[] wordMinHeadingItems;
    protected int[] wordMinPLabelItems;

    protected int[] excelMinHeadingCountThreshold;
    protected int[] excelMinLayoutCountThreshold;
    protected int[] excelMinFormulaThreshold;
    protected int[] excelMinStyleCountThreshold;
    protected int[] excelMinConcreteValueThreshold;
    protected String[] excelSimilarityHighContentThreshold;
    protected int[] excelMinValueThreshold;
    protected String[] excelSimilarityHighStyleThreshold;
    protected String[] excelSimilarityHeadingHighSimThreshold;
    protected String[] excelSimilarityHighLayoutThreshold;
    protected String[] excelWorkbookAggregatedSimilarityThreshold;


    public void setPvAnalysisDataFetcher(PvAnalysisDataFetcher pvAnalysisDataFetcher) {
        this.pvAnalysisDataFetcher = pvAnalysisDataFetcher;
    }

    public void setMaturityContentPvNames(String maturityContentPvNames) {
        this.maturityContentPvNames = maturityContentPvNames;
    }


    public void setMaturityModelSingleMaturityLevel(boolean maturityModelSingleMaturityLevel) {
        this.maturityModelSingleMaturityLevel = maturityModelSingleMaturityLevel;
    }

    public void setGroupingForceMaturityLevelSimilarity(boolean groupingForceMaturityLevelSimilarity) {
        this.groupingForceMaturityLevelSimilarity = groupingForceMaturityLevelSimilarity;
    }

    public SimilarityCalculator init() {
        return init(false);
    }
    public SimilarityCalculator init(final boolean isSolrPlugin) {
        WordSimilarityParameters wordSimParams = getWordSimParams(0);
        WordSimilarityParameters wordSimParamsLow = getWordSimParams(1);
        WordSimilarityParameters wordSimParamsMed = getWordSimParams(2);
        WordSimilarityParameters wordSimParamsHigh = getWordSimParams(3);

        ExcelSimilarityParameters excelSimParams = getExcelSimParams(0);
        ExcelSimilarityParameters excelSimParamsLow = getExcelSimParams(1);
        ExcelSimilarityParameters excelSimParamsMed = getExcelSimParams(2);
        ExcelSimilarityParameters excelSimParamsHigh = getExcelSimParams(3);

        WordSimilarityParameters[] wordSimParamsByMaturity = new WordSimilarityParameters[]{wordSimParams, wordSimParamsLow, wordSimParamsMed, wordSimParamsHigh};
        ExcelSimilarityParameters[] excelSimParamsByMaturity = new ExcelSimilarityParameters[]{excelSimParams, excelSimParamsLow, excelSimParamsMed, excelSimParamsHigh};
        SimilarityCalculator similarityCalculator = new SimilarityCalculator(wordSimParamsByMaturity,
                excelSimParamsByMaturity,
                groupingForceMaturityLevelSimilarity,
                maturityModelSingleMaturityLevel,
                maturityContentPvNames,
                maturityLowThreshodsByPV,
                maturityMediumThreshodsByPV,
                maturityPvGroups,
                pvAnalysisDataFetcher,
                isSolrPlugin);
        return similarityCalculator;
    }

    private WordSimilarityParameters getWordSimParams(int level) {
        return new WordSimilarityParameters(paramByLevel(level, wordSimilarityContentHighThreshold), paramByLevel(level, wordSimilarityContentLowThreshold), paramByLevel(level, wordSimilarityStyleHighThreshold),
                paramByLevel(level, wordSimilarityStyleLowThreshold), paramByLevel(level, wordSimilarityHeadingsLowThreshold), paramByLevel(level, wordSimilarityHeadingsHighThreshold),
                paramByLevel(level, wordSimilarityDocstartLowThreshold), paramByLevel(level, wordMinHeadingItems), paramByLevel(level, wordMinPLabelItems),
                paramByLevel(level, wordMinStyleItems), paramByLevel(level, wordMinStyleSequenceItems), paramByLevel(level, wordMinLetterHeadItems));
    }

    private ExcelSimilarityParameters getExcelSimParams(int level) {
        return new ExcelSimilarityParameters(paramByLevel(level, excelMinHeadingCountThreshold), paramByLevel(level, excelMinStyleCountThreshold),
                paramByLevel(level, excelMinLayoutCountThreshold), paramByLevel(level, excelMinValueThreshold), paramByLevel(level, excelMinConcreteValueThreshold),
                paramByLevel(level, excelMinFormulaThreshold), paramByLevel(level, excelSimilarityHighContentThreshold),
                paramByLevel(level, excelSimilarityHighStyleThreshold), paramByLevel(level, excelSimilarityHighLayoutThreshold),
                paramByLevel(level, excelSimilarityHeadingHighSimThreshold), paramByLevel(level, excelWorkbookAggregatedSimilarityThreshold));
    }

    private float paramByLevel(int level, String[] vals) {
        if (level >= vals.length) {
            level = 0;            // In case of missing value take #0 which is the original default
        }
        return Float.valueOf(vals[level]).floatValue();
    }

    private int paramByLevel(int level, int[] vals) {
        if (level >= vals.length) {
            level = vals.length - 1;
        }
        return vals[level];
    }

    public void setMaturityLowThreshodsByPV(String maturityLowThreshodsByPV) {
        this.maturityLowThreshodsByPV = maturityLowThreshodsByPV;
    }

    public void setMaturityMediumThreshodsByPV(String maturityMediumThreshodsByPV) {
        this.maturityMediumThreshodsByPV = maturityMediumThreshodsByPV;
    }


    public void setMaturityPvGroups(String maturityPvGroups) {
        this.maturityPvGroups = maturityPvGroups;
    }
}
