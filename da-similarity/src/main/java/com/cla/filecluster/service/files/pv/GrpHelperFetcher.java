package com.cla.filecluster.service.files.pv;

import java.util.Collection;
import java.util.Map;
import java.util.UUID;

/**
 * Created by uri on 08/02/2017.
 */
public interface GrpHelperFetcher {

    String getContentGroupId(long contentId);

    ContentGroupDetails getContentGroupDetails(long contentId);

    UUID getContentGroupIdUUID(long contentId);

    Map<Long, ContentGroupDetails> getContentGroupByIds(Collection<Long> contentIds);

    void updateStatistics(long time, int size);
}
