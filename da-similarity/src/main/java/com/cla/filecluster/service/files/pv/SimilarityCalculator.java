package com.cla.filecluster.service.files.pv;

import com.cla.common.domain.dto.pv.*;
import com.cla.filecluster.domain.dto.excel.ExcelSheetSimilarity;
import com.cla.common.domain.dto.histogram.ClaSuperCollection;
import com.cla.common.domain.dto.word.WordFilesSimilarity;
import com.cla.filecluster.domain.entity.pv.PvAnalysisData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toSet;

/**
 * Created by uri on 02/02/2017.
 */
public class SimilarityCalculator {


    public static final String CONTENT_IDS = "contentIds";

    private final WordSimilarityParameters[] wordSimParamsByMaturity;
    private final ExcelSimilarityParameters[] excelSimParamsByMaturity;
    private final boolean groupingForceMaturityLevelSimilarity;
    private final PvAnalysisDataFetcher pvAnalysisDataFetcher;
    private final Logger logger = LoggerFactory.getLogger(SimilarityCalculator.class);
    private Map<String, Integer> maturityPvGroupMap = new HashMap<>();
    private    Map<String, Integer[]> maturityThresholdsMap = new HashMap<>();

    private final MaturityHelper maturityHelper;

    public SimilarityCalculator(WordSimilarityParameters[] wordSimParamsByMaturity, ExcelSimilarityParameters[] excelSimParamsByMaturity,
                                boolean groupingForceMaturityLevelSimilarity, boolean maturityModelSingleMaturityLevel,
                                String maturityContentPvNames,
                                String maturityLowThreshodsByPV,
                                String maturityMediumThreshodsByPV,
                                String maturityPvGroups,
                                PvAnalysisDataFetcher pvAnalysisDataFetcher,
                                boolean isSolrPlugin) {

        this.wordSimParamsByMaturity = wordSimParamsByMaturity;
        this.excelSimParamsByMaturity = excelSimParamsByMaturity;
        this.groupingForceMaturityLevelSimilarity = groupingForceMaturityLevelSimilarity;
        this.pvAnalysisDataFetcher = pvAnalysisDataFetcher;
        logger.info("Init WordSim parameter set: {}", Arrays.toString(getWordSimParamsByMaturity()));
        logger.info("Init ExcelSim parameter set: {}", Arrays.toString(getExcelSimParamsByMaturity()));

        fillMaturityLevelThresholds(maturityLowThreshodsByPV, MaturityLevel.ML_Low);
        fillMaturityLevelThresholds(maturityMediumThreshodsByPV, MaturityLevel.ML_Med);

        MaturityHelper.maturityThreshodsToPvGroups(maturityThresholdsMap, maturityPvGroupMap, maturityPvGroups);

        maturityHelper = new MaturityHelper(maturityContentPvNames, maturityModelSingleMaturityLevel, isSolrPlugin);
        logger.info("Init maturityThresholdsMap parameter set: {}",
                maturityThresholdsMap==null?"null":maturityThresholdsMap.entrySet().stream().map(e->e.getKey()+":"+Arrays.asList(e.getValue()).toString())
                        .collect(Collectors.joining(",","{","}")));
        logger.info("Init maturityPvGroupMap parameter set: {}", maturityPvGroupMap.toString());

        maturityHelper.setMaturityPvGroupMap(maturityPvGroupMap);
        maturityHelper.setMaturityThresholdsMap(maturityThresholdsMap);
    }

    public static int maturityCombinedValueMaxMaturityValue(int combinedValue) {
        // Same code for single/multi ML model
        return Integer.max((combinedValue % 10), (combinedValue / 10));
    }

    public WordSimilarityParameters wordSimilarityParams(int combinedMaturityLevel, MaturityClass mc) {
        return wordSimParamsByMaturity[maturityHelper.simParamObjByCombinedMaturityLevelSelector(combinedMaturityLevel, wordSimParamsByMaturity, mc)];
    }

    private WordSimilarityParameters wordSimilarityParams(int combinedMaturityLevel, PVType pvt) {
        return wordSimParamsByMaturity[maturityHelper.simParamObjByCombinedMaturityLevelSelector(combinedMaturityLevel, wordSimParamsByMaturity, pvt)];
    }

    public WordSimilarityParameters[] getWordSimParamsByMaturity() {
        return wordSimParamsByMaturity;
    }

    public ExcelSimilarityParameters[] getExcelSimParamsByMaturity() {
        return excelSimParamsByMaturity;
    }

    public float getWordSimilarityStyleLowThreshold(PvAnalysisData pvAnalysisData) {
        int combinedMaturityLevel = maturityHelper.getPartMaturityLevel(pvAnalysisData.getDefaultPvCollection());
        WordSimilarityParameters styleWsp = wordSimilarityParams(combinedMaturityLevel, MaturityClass.MC_STYLE);
        return styleWsp.wordSimilarityStyleLowThreshold;
    }

    /*
* Check strong similarity algorithm using full analysis data of both files.
*/
    public int wordFullSimThresholdPassed(final PvAnalysisData doc1PvAnalysisData, final PvAnalysisData doc2PvAnalysisData,
                                          final StringBuilder debInfo, final int combinedMaturityLevel) {
//			final float contentHighThreshold,
//			final float contentLowThreshold,
//			final float styleHighThreshold,
//			final float styleLowThreshold,
//			final float headingsLowThreshold,
//			final float headingsHighThreshold,
//			final float docstartLowThreshold) {

        final WordFilesSimilarity wfs = new WordFilesSimilarity(
                doc1PvAnalysisData.getDefaultPvCollection().getCollections(),
                doc2PvAnalysisData.getDefaultPvCollection().getCollections());

        final float docstartNgramsSimRate = wfs.getSimilarityValue(PVType.PV_DocstartNgrams);
        final float bodyNgramsSimRate = wfs.getSimilarityValue(PVType.PV_BodyNgrams);
        final float subBodyNgramsSimRate = wfs.getSimilarityValue(PVType.PV_SubBodyNgrams);
        final float headingsSimRate = wfs.getSimilarityValue(PVType.PV_Headings);
        final float pLabelSimRate = wfs.getSimilarityValue(PVType.PV_ParagraphLabels);
        final float letterHeadLabelsSimRate = wfs.getSimilarityValue(PVType.PV_LetterHeadLabels);

        final boolean areBothWord = ((wfs.getCountA(PVType.PV_StyleSignatures) > 0) && (wfs.getCountB(PVType.PV_StyleSignatures) > 0));

        WordSimilarityParameters styleWsp = wordSimilarityParams(combinedMaturityLevel, MaturityClass.MC_STYLE);
        float wordSimilarityStyleLowThresholdVal = styleWsp.wordSimilarityStyleLowThreshold;

        if (debInfo != null) {
            debInfo.append(wfs.dumpSimilarityVector());
//			debInfo.append(String.format(" ds=%.2f bn=%.2f sbn=%.2f", docstartNgramsSimRate, bodyNgramsSimRate, subBodyNgramsSimRate));
        }


		/*
         * Strong content sim && weak style sim => sure match
		 */
        final float styleSignaturesSimRate = areBothWord ? wfs.getSimilarityValue(PVType.PV_StyleSignatures) : wfs.getSimilarityValue(PVType.PV_AbsoluteStyleSignatures);
        final float styleSequenceSimRate = areBothWord ? wfs.getSimilarityValue(PVType.PV_StyleSequence) : wfs.getSimilarityValue(PVType.PV_AbsoluteStyleSequence);

        WordSimilarityParameters contentWsp = wordSimilarityParams(combinedMaturityLevel, MaturityClass.MC_CONTENT);
        WordSimilarityParameters docStartWsp = wordSimilarityParams(combinedMaturityLevel, PVType.PV_DocstartNgrams);
        if ((bodyNgramsSimRate > contentWsp.wordSimilarityContentHighThreshold ||
                subBodyNgramsSimRate > contentWsp.wordSimilarityContentHighThreshold) &&
                (styleSignaturesSimRate > wordSimilarityStyleLowThresholdVal ||
                        styleSequenceSimRate > wordSimilarityStyleLowThresholdVal)) {

            if (docstartNgramsSimRate < docStartWsp.wordSimilarityDocstartLowThreshold) {
                final int headingsMaxSimCount = wfs.getMaxCount(PVType.PV_Headings);
                WordSimilarityParameters headingsWsp = wordSimilarityParams(combinedMaturityLevel, PVType.PV_Headings);
                if (headingsMaxSimCount > headingsWsp.wordMinHeadingItems &&
                        headingsSimRate < headingsWsp.wordSimilarityHeadingsHighThreshold) {
                    return 0;
                } else {
                    return 1;
                }
            }
            return 2;
        }

		/*
		 * Strong style sim
		 */
        final int styleSignatureMinItems = areBothWord ? wfs.getMinItems(PVType.PV_StyleSignatures) : wfs.getMinItems(PVType.PV_AbsoluteStyleSignatures);
        final int styleSquenceMinItems = areBothWord ? wfs.getMinItems(PVType.PV_StyleSequence) : wfs.getMinItems(PVType.PV_AbsoluteStyleSequence);

        if ((styleSignaturesSimRate > styleWsp.wordSimilarityStyleHighThreshold && styleSignatureMinItems > styleWsp.wordMinStyleItems) ||
                (styleSequenceSimRate > styleWsp.wordSimilarityStyleHighThreshold && styleSquenceMinItems > styleWsp.wordMinStyleSequenceItems)) {
            if ((docstartNgramsSimRate > contentWsp.wordSimilarityContentLowThreshold) ||
                    (bodyNgramsSimRate > contentWsp.wordSimilarityContentLowThreshold) ||
                    (subBodyNgramsSimRate > contentWsp.wordSimilarityContentLowThreshold)) {

                final int headingsMaxSimTotalCount = wfs.getMaxCount(PVType.PV_Headings);    // Use getUnprocessed() as TotalCount.
                if (docstartNgramsSimRate < docStartWsp.wordSimilarityDocstartLowThreshold) {
                    WordSimilarityParameters headingsWsp = wordSimilarityParams(combinedMaturityLevel, PVType.PV_Headings);
                    if (headingsMaxSimTotalCount > headingsWsp.wordMinHeadingItems && headingsSimRate < headingsWsp.wordSimilarityHeadingsHighThreshold) {
                        return 0;
                    }
                }
                final int headingsMinSimCount = wfs.getMinCount(PVType.PV_Headings);
                final int pLabelMaxSimCount = wfs.getMaxCount(PVType.PV_ParagraphLabels);
                final int letterHeadLabelsMaxSimCount = wfs.getMaxCount(PVType.PV_LetterHeadLabels);
                WordSimilarityParameters headingsWsp = wordSimilarityParams(combinedMaturityLevel, PVType.PV_Headings);
                if (headingsMinSimCount > headingsWsp.wordMinHeadingItems) {
                    if (headingsSimRate > headingsWsp.wordSimilarityHeadingsLowThreshold) {
                        // headings presence with weak sim, falses match.
                        return 3;
                    } else {
                        return 0;
                    }
                } else {
                    WordSimilarityParameters pLabelWsp = wordSimilarityParams(combinedMaturityLevel, PVType.PV_ParagraphLabels);
                    WordSimilarityParameters letterHeadWsp = wordSimilarityParams(combinedMaturityLevel, PVType.PV_LetterHeadLabels);
                    if (pLabelMaxSimCount > pLabelWsp.wordMinPLabelItems) {
                        if (pLabelSimRate > pLabelWsp.wordSimilarityHeadingsLowThreshold ||
                                wfs.getOrderedMatchCountsHistParagraphLabels() >= letterHeadWsp.wordMinLetterHeadItems) {
                            // No headings + paragraph-labels presence + weak sim, falses match
                            return 4;
                        } else {
                            return 0;
                        }
                    } else if (letterHeadLabelsMaxSimCount > letterHeadWsp.wordMinLetterHeadItems) {
                        if (letterHeadLabelsSimRate > letterHeadWsp.wordSimilarityHeadingsLowThreshold ||
                                wfs.getOrderedMatchCountsHistLetterHeadLabels() >= letterHeadWsp.wordMinLetterHeadItems) {
                            return 5;
                        } else {
                            // No headings + letter-head presence + weak sim, falses match
                            return 0;
                        }
                    }
                }
                return 6;
            }
        }
        return 0;
    }

    private ExcelSimilarityParameters excelSimilarityParams(int combinedMaturityLevel, MaturityClass mc) {
        return excelSimParamsByMaturity[maturityHelper.simParamObjByCombinedMaturityLevelSelector(combinedMaturityLevel, excelSimParamsByMaturity, mc)];
    }

    private ExcelSimilarityParameters excelSimilarityParams(int combinedMaturityLevel, PVType pvt) {
        return excelSimParamsByMaturity[maturityHelper.simParamObjByCombinedMaturityLevelSelector(combinedMaturityLevel, excelSimParamsByMaturity, pvt)];
    }

    public boolean checkSheetFullSimilarity(
            final Long doc1Lid, final String sheet1Name, final ClaSuperCollection<Long> claSuperCollection1,
            final Long doc2Lid, final String sheet2Name, final ClaSuperCollection<Long> claSuperCollection2) {

        int part1MaturityLevel = maturityHelper.getPartMaturityLevel(claSuperCollection1);
        if (groupingForceMaturityLevelSimilarity) {
            int part2MaturityLevel = maturityHelper.getPartMaturityLevel(claSuperCollection2);        // check ML match
            if (part1MaturityLevel != part2MaturityLevel) {
                if (logger.isTraceEnabled()) {
                    logger.trace("checkSheetFullSimilarity ({}-{},{}-{}): failure due to maturity level mismatch {}<>{}", doc1Lid, sheet1Name, doc2Lid, sheet2Name, part1MaturityLevel, part2MaturityLevel);
                }
                return false;
            }
        }
//		ExcelSimilarityParameters esp = excelSimParamsByMaturity[part1MaturityLevel];

        final ExcelSheetSimilarity ess = new ExcelSheetSimilarity(claSuperCollection1.getCollections(),
                claSuperCollection2.getCollections());

        final float rowHeadingsSimRate = ess.getSimilarityValue((PVType.PV_RowHeadings));
        final float styleSimRate = ess.getSimilarityValue((PVType.PV_ExcelStyles));
        final float valuesSimRate = ess.getSimilarityValue((PVType.PV_CellValues));
        final float formulasSimRate = ess.getSimilarityValue((PVType.PV_Formulas));
        final float concreteValuesSimRate = ess.getSimilarityValue((PVType.PV_ConcreteCellValues));
//		final float textNgramsSimRate = ess.getSimilarityValue((PVType.PV_TextNgrams));
//		final float titlesNgramsSimRate = ess.getSimilarityValue((PVType.PV_TitlesNgrams));
        final float sheetLayoutSimRate = ess.getSimilarityValue((PVType.PV_SheetLayoutSignatures));

        final int rowHeadingsMinSimCount = ess.getMinCount(PVType.PV_RowHeadings);
        final int styleMinSimCount = ess.getMinCount(PVType.PV_ExcelStyles);
        final int valuesMinSimCount = ess.getMinCount(PVType.PV_CellValues);
        final int concreteValuesMinSimCount = ess.getMinCount(PVType.PV_ConcreteCellValues);
        final int formulasMinSimCount = ess.getMinCount(PVType.PV_Formulas);
        final int sheetLayoutMinSimCount = ess.getMinCount(PVType.PV_SheetLayoutSignatures);

//		final float contentThreshold = esp.excelSimilarityHighContentThreshold;
//		final float styleThreshold = esp.excelSimilarityHighStyleThreshold;
//		final float layoutThreshold = esp.excelSimilarityHighLayoutThreshold;
//		final float headingHighSimThreshold = esp.excelSimilarityHeadingHighSimThreshold;
        ExcelSimilarityParameters contentEsp = excelSimilarityParams(part1MaturityLevel, MaturityClass.MC_CONTENT);
        ExcelSimilarityParameters styleEsp = excelSimilarityParams(part1MaturityLevel, MaturityClass.MC_STYLE);
        final float contentThreshold = contentEsp.excelSimilarityHighContentThreshold;
        final float styleThreshold = styleEsp.excelSimilarityHighStyleThreshold;
        final float layoutThreshold = excelSimilarityParams(part1MaturityLevel, PVType.PV_SheetLayoutSignatures).excelSimilarityHighLayoutThreshold;
        final float headingHighSimThreshold = excelSimilarityParams(part1MaturityLevel, PVType.PV_RowHeadings).excelSimilarityHeadingHighSimThreshold;
        final float valuesExcelMinValueThreshold = excelSimilarityParams(part1MaturityLevel, PVType.PV_CellValues).excelMinValueThreshold;
        final float excelMinConcreteValueThreshold = excelSimilarityParams(part1MaturityLevel, PVType.PV_ConcreteCellValues).excelMinConcreteValueThreshold;
        final float excelMinFormulaThreshold = excelSimilarityParams(part1MaturityLevel, PVType.PV_Formulas).excelMinFormulaThreshold;
        final int excelMinHeadingCountThreshold = excelSimilarityParams(part1MaturityLevel, PVType.PV_RowHeadings).excelMinHeadingCountThreshold;
        final int excelMinStyleCountThreshold = styleEsp.excelMinStyleCountThreshold;
        final int excelMinLayoutCountThreshold = excelSimilarityParams(part1MaturityLevel, PVType.PV_SheetLayoutSignatures).excelMinLayoutCountThreshold;

//		final int rowHeadingsMaxSimCount = ess.getMaxCount(PVType.PV_RowHeadings);
//		final int styleMaxSimCount = ess.getMaxCount(PVType.PV_ExcelStyles);
//		final int valuesMaxSimCount = ess.getMaxCount(PVType.PV_CellValues);
//		final int formulasMaxSimCount = ess.getMaxCount(PVType.PV_Formulas);
//		final int sheetLayoutMaxSimCount = ess.getMaxCount(PVType.PV_SheetLayoutSignatures);

//		final float formulaDominance = (valuesMaxSimCount==0)?0f:
//			((float)(ess.getCountA(PVType.PV_Formulas)+ess.getCountB(PVType.PV_Formulas))) /
//			((float)(ess.getCountA(PVType.PV_CellValues)+ess.getCountB(PVType.PV_CellValues)));

        int result = 0;
        int contentSimCode = 0;
        if (valuesSimRate >= contentThreshold && valuesMinSimCount > valuesExcelMinValueThreshold) {
            contentSimCode = 1;
        } else if (concreteValuesSimRate >= contentThreshold && concreteValuesMinSimCount > excelMinConcreteValueThreshold) {
            contentSimCode = 2;
        } else if (formulasSimRate >= contentThreshold && formulasMinSimCount > excelMinFormulaThreshold) {
            contentSimCode = 3;
        }

        int visualSimCode = 0;
        if (rowHeadingsMinSimCount > excelMinHeadingCountThreshold && rowHeadingsSimRate >= contentThreshold) {
            visualSimCode = 1;
        } else if (styleMinSimCount > excelMinStyleCountThreshold && styleSimRate >= styleThreshold) {
            visualSimCode = 2;
        } else if (sheetLayoutMinSimCount > excelMinLayoutCountThreshold && sheetLayoutSimRate >= layoutThreshold) {
            visualSimCode = 3;
        }
        // Special handling of strong row-headings + style - treat as content similarity was found
        if (rowHeadingsMinSimCount > excelMinHeadingCountThreshold && rowHeadingsSimRate >= headingHighSimThreshold) {
            if (styleMinSimCount == 0 || styleSimRate >= headingHighSimThreshold) {
                visualSimCode = (styleMinSimCount == 0) ? 4 : 5;
                if (contentSimCode == 0) {
                    contentSimCode = 4;
                }
            }
        }

        if (contentSimCode != 0 && visualSimCode != 0) {
            result = 10 * contentSimCode + visualSimCode;    // two digits code indicating match origin
        }
        if (result == 0 && logger.isDebugEnabled()) {
            logger.debug("Excel sheet fullSim disqualified {}/{} and {}/{}", doc1Lid, sheet1Name, doc2Lid, sheet2Name);
        }

        return (result != 0);
    }

    public int getWordSimCalc2(final PvAnalysisData doc1PvAnalysisData, final StringBuilder sim2DebInfo,
                               final Long doc2Lid, final PvAnalysisDataPool pvAnalysisDataPool) {

        ClaSuperCollection<Long> collection1 = doc1PvAnalysisData.getDefaultPvCollection();
        int doc1Ml = maturityHelper.getPartMaturityLevel(collection1);
        if (collection1 == null) {
            logger.error("Null default collections for matching file {}. ", doc2Lid);
            return 0;
        }

        final PvAnalysisData doc2PvAnalysisData = (pvAnalysisDataPool != null) ?
                pvAnalysisDataPool.getRecord(doc2Lid) :
                pvAnalysisDataFetcher.getPvAnalysisDataByContentId(doc2Lid);
        if (doc2PvAnalysisData==null) {
            logger.error("Could not get analysisData for doc2 id {} final match. skipping this file.", doc2Lid);
            return 0;
        }
        if (groupingForceMaturityLevelSimilarity) {
//            if (doc2PvAnalysisData != null && AnalyzeHint.EXCLUDED.equals(doc2PvAnalysisData.getAnalyzeHint())) {
//                if (logger.isTraceEnabled()) {
//                    logger.trace("checkSheetFullSimilarity skipped due to doc2 {} Excluded from Analysis", doc2Lid);
//                }
//                return 0;
//            }
            ClaSuperCollection<Long> collection2 = doc2PvAnalysisData.getDefaultPvCollection();
            if (collection2 == null) {
                logger.error("Null default collections for file {}. Failed to verify similarity", doc2Lid);
                return 0;
            }
            int doc2Ml = maturityHelper.getPartMaturityLevel(collection2);        // check ML match
            if (doc1Ml != doc2Ml) {
                if (logger.isTraceEnabled()) {
                    logger.trace("checkSheetFullSimilarity ({},{}): failure due to maturity level mismatch {}<>{}", "?", doc2Lid, doc1Ml, doc2Ml);
                }
                return 0;
            }
        }
//		WordSimilarityParameters wps = wordSimParamsByMaturity[doc1Ml];			// Should be based on Doc ML
        int simCalc2 = -1;
        if (doc2PvAnalysisData != null) {
            simCalc2 = wordFullSimThresholdPassed(doc1PvAnalysisData, doc2PvAnalysisData, sim2DebInfo, doc1Ml);

//                    wordSimilarityContentHighThreshold,
//                    wordSimilarityContentLowThreshold,
//                    wordSimilarityStyleHighThreshold,
//                    wordSimilarityStyleLowThreshold,
//                    wordSimilarityHeadingsLowThreshold,
//                    wordSimilarityHeadingsHighThreshold,
//                    wordSimilarityDocstartLowThreshold);
        }
        return simCalc2;
    }

    public int getWordSimCalc2(PvAnalysisData doc1PvAnalysisData, PvAnalysisData doc2PvAnalysisData) {
        ClaSuperCollection<Long> collection1 = doc1PvAnalysisData.getDefaultPvCollection();
        if (collection1 == null) {
            return 0;
        }
        int firstDocMaturityLevel = maturityHelper.getPartMaturityLevel(collection1);

        if (groupingForceMaturityLevelSimilarity) {
            ClaSuperCollection<Long> collection2 = doc2PvAnalysisData.getDefaultPvCollection();
            if (collection2 == null) {
                return 0;
            }
            int secondDocMaturityLevel = maturityHelper.getPartMaturityLevel(collection2);
            if (firstDocMaturityLevel != secondDocMaturityLevel) {
                return 0;
            }
        }
        return wordFullSimThresholdPassed(doc1PvAnalysisData, doc2PvAnalysisData, null, firstDocMaturityLevel);
    }


    public float floatVal(final Float f) {
        return (f == null) ? 0 : f.floatValue();
    }

    public Integer NotNull(final Integer val) {
        return (val == null) ? 0 : val;
    }

    public float workBookAggregatedSimilarityFullMatch(long contentId, final Long matchedDocLid,
                                                       final Map<String, Float> sheetsDominanceRatio, final Map<String, Float> matchedDocsheetsDominanceRatio,
                                                       final Map<String, String> sheetMatches,
                                                       final PvAnalysisData doc1PvAnalysisData, final PvAnalysisData doc2PvAnalysisData, float skippedSheetsDominance) {

        if (sheetsDominanceRatio == null || matchedDocsheetsDominanceRatio == null) {
            return 0f;
        }

        final Map<String, ClaSuperCollection<Long>> book1SheetsCollections = doc1PvAnalysisData.getPvCollections().getCollections();
        if (doc2PvAnalysisData.getPvCollections() == null) {
            logger.warn("Null doc2 PvCollections for file {}", matchedDocLid);
            return 0f;
        }
        final Map<String, ClaSuperCollection<Long>> book2SheetsCollections = doc2PvAnalysisData.getPvCollections().getCollections();

        float otherDominanceFactor = 1f;
        if (skippedSheetsDominance > 0f && otherDominanceFactor < 1f) {
            otherDominanceFactor = 1f/(1f - skippedSheetsDominance);
            logger.debug("Factor other docs in similarity aggregation by {} 1/(1-{})", otherDominanceFactor, skippedSheetsDominance);
        }
        final float fOtherDominanceFactor = otherDominanceFactor;
        final float result = (float) sheetMatches.entrySet().stream()
                .filter(e -> checkSheetFullSimilarity(contentId, e.getKey(), book1SheetsCollections.get(e.getKey()),
                        matchedDocLid, e.getValue(), book2SheetsCollections.get(e.getValue())))
//						excelSimilarityHighContentThreshold,
//						excelSimilarityHighStyleThreshold,
//						excelSimilarityHighLayoutThreshold,
//						excelSimilarityHeadingHighSimThreshold))
                .mapToDouble(e -> Double.valueOf(0.5*sheetsDominanceRatio.get(e.getKey())+
                        0.5*Float.min(1f, matchedDocsheetsDominanceRatio.get(e.getValue())*fOtherDominanceFactor))).sum();

        if (logger.isTraceEnabled()) {
            logger.debug("ValidateAggregatedSimilarity: returned value {} for {} and {}", result, contentId, matchedDocLid);
        }
        return result;
    }

    public int workbookMaturityLevel(PvAnalysisData doc2PvAnalysisData) {
        return maturityHelper.workbookMaturityLevel(doc2PvAnalysisData);
    }

    //TODO:this code appears also at MaturityLevelService
    private void fillMaturityLevelThresholds(String maturityThreshodsByPV, MaturityLevel level) {
        if(maturityThreshodsByPV == null || maturityThreshodsByPV.isEmpty()) {
            return;
        }
        Set<String> pvtSet = Stream.of(maturityThreshodsByPV.split(",")).collect(toSet());
        for (String pvtDef : pvtSet) {
            String[] items = pvtDef.split(":",2);
            String pvName = items[0];
            int val = Integer.valueOf(items[1]).intValue();
            Integer[] valArray =  maturityThresholdsMap.get(pvName);
            if (valArray == null) {
                valArray = new Integer[MaturityLevel.MAX_VALUE];
                maturityThresholdsMap.put(pvName, valArray);
                for (int i=0;i<valArray.length;++i) valArray[i] = 0;
            }
            valArray[level.toInt()] = val;
        }
//		logger.debug("Set fillMaturityLevelThresholds {} to {}", level, maturityThreshodsByPV);
    }

    public MaturityHelper getMaturityHelper() {
        return maturityHelper;
    }
}
