package com.cla.filecluster.service.files.pv;

import com.cla.common.domain.dto.file.FileCollections;
import com.cla.common.domain.dto.histogram.ClaHistogramCollection;
import com.cla.common.domain.dto.histogram.ClaSuperCollection;
import com.cla.common.domain.dto.pv.MaturityClass;
import com.cla.common.domain.dto.pv.MaturityLevel;
import com.cla.common.domain.dto.pv.PVType;
import com.cla.filecluster.domain.entity.pv.PvAnalysisData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toSet;

/**
 * Created by uri on 02/02/2017.
 */
public class MaturityHelper {
    private static Logger logger = LoggerFactory.getLogger(MaturityHelper.class);

    private final boolean maturityModelSingleMaturityLevel;

    private boolean isSolrPlugin;

    private Map<String, Integer> maturityPvGroupMap = new HashMap<>();

    private Map<String, Integer[]> maturityThresholdsMap  = new HashMap<>();

    private final Set<String> contentPvNamesSet ;

    public MaturityHelper(final String contentPvNames, final boolean maturityModelSingleMaturityLevel) {
        this(contentPvNames, maturityModelSingleMaturityLevel, false);
    }

    public MaturityHelper(final String contentPvNames, final boolean maturityModelSingleMaturityLevel, final boolean isSolrPlugin) {
        this.contentPvNamesSet = Stream.of(contentPvNames.split(",")).collect(toSet());
        this.maturityModelSingleMaturityLevel = maturityModelSingleMaturityLevel;
        this.isSolrPlugin = isSolrPlugin;
    }

    public MaturityClass pvMaturityClass(String pvName) {
        return contentPvNamesSet.contains(pvName)? MaturityClass.MC_CONTENT: MaturityClass.MC_STYLE;
    }

    public int getMaturityLevelSingleValue(ClaSuperCollection<Long> partPvCollection) {
        if(maturityPvGroupMap == null || maturityPvGroupMap.isEmpty())//add or is empty!!
        {
            logger.error("MaturityHelper maturityPvGroupMap is null");
            throw new RuntimeException("MaturityHelper maturityPvGroupMap is null");
        }
        Map<Integer,Integer> pvGroup2LevelMap = new HashMap<>();

        if(maturityThresholdsMap == null || maturityThresholdsMap.isEmpty()) {
            logger.error("MaturityHelper: maturityThresholdsMap is null{}", isSolrPlugin ? " (plugin)" : " (non-pluging)");
        }
        partPvCollection.getCollections().entrySet().stream().filter(e->isMaturityCriteria(e.getKey())).forEach(e->{
            int level = getPartMaturityLevel(e.getKey(), e.getValue());
            Integer pvtGroup = maturityPvGroupMap.get(e.getKey());
            Integer currGroupLevel = pvGroup2LevelMap.get(pvtGroup);
            // Get max level within group
            if (currGroupLevel==null || currGroupLevel < level) {
                pvGroup2LevelMap.put(pvtGroup, Integer.valueOf(level));
            }
        });
        return pvGroup2LevelMap.values().stream().mapToInt(v->v.intValue()).min().orElse(0);
    }

    public int simParamObjByCombinedMaturityLevelSelector(int combinedMaturityLevel, Object[] vec, MaturityClass mc) {
//        if (maturityModelSingleMaturityLevel) {
//            return (combinedMaturityLevel >= vec.length)  ? 0 : combinedMaturityLevel;
//        }
//        else {
            final int mlValue = maturityClassesCombinedValueToClassValue(combinedMaturityLevel, mc);
            return (mlValue >= vec.length)  ? 0 : mlValue;
//        }
    }

    public static int maturityClassesCombinedValueToClassValue(int value, MaturityClass mc) {
        return (mc== MaturityClass.MC_CONTENT) ? (value % 10) : (value / 10);
    }

    public int simParamObjByCombinedMaturityLevelSelector(int combinedMaturityLevel, Object[] vec, PVType pvt) {
        return simParamObjByCombinedMaturityLevelSelector(combinedMaturityLevel, vec, pvt.name());
    }

    public int simParamObjByCombinedMaturityLevelSelector(int combinedMaturityLevel, Object[] vec, String pvName) {
//        if (maturityModelSingleMaturityLevel) {
//            return (combinedMaturityLevel >= vec.length)  ? 0 : combinedMaturityLevel;
//        }
//        else {
            MaturityClass mc = pvMaturityClass(pvName);
            return maturityClassesCombinedValueToClassValue(combinedMaturityLevel, mc);
//        }
    }

    public boolean isMaturityCriteria(String key) {
        if(maturityThresholdsMap == null || maturityThresholdsMap.isEmpty()) {
            return false;
        }
        return maturityThresholdsMap.containsKey(key);
    }

    public int calcPartMaturityLevelMultiValue(ClaSuperCollection<Long> partPvCollection) {
        return calcPartMaturityLevelMultiValue(partPvCollection.getCollections());
    }

    public int calcPartMaturityLevelMultiValue(Map<String, ClaHistogramCollection<Long>> partCollections) {
        Map<Integer,Integer> contentPvGroup2LevelMap = new HashMap<>();
        Map<Integer,Integer> stylePvGroup2LevelMap = new HashMap<>();
        if(maturityThresholdsMap == null || maturityThresholdsMap.isEmpty()) {
            logger.error("MaturityHelper: maturityThresholdsMap is null");
            } else {
            if (logger.isTraceEnabled()) {
                logger.trace("MaturityHelper: maturityThresholdsMap size={}", maturityThresholdsMap.size());
            }
        }
        if (logger.isTraceEnabled()) {
            String dump = partCollections.entrySet().stream().filter(e->isMaturityCriteria(e.getKey())).map(e->{
            int level = getPartMaturityLevel(e.getKey(), e.getValue());
            Integer pvtGroup = maturityPvGroupMap.get(e.getKey());
            Map<Integer,Integer> pvGroup2LevelMap = (pvMaturityClass(e.getKey())== MaturityClass.MC_CONTENT)?contentPvGroup2LevelMap:stylePvGroup2LevelMap;
            Integer currGroupLevel = pvGroup2LevelMap.get(pvtGroup);
            // Get max level within group
                return e.getKey()+":"+((currGroupLevel==null)?"null":currGroupLevel)+"<"+level+")";
            }).collect(Collectors.joining(",","[","]"));
            logger.trace("calcPartMaturityLevelMultiValue dump: {}", dump);
        }
        partCollections.entrySet().stream().filter(e->isMaturityCriteria(e.getKey())).forEach(e->{
            int level = getPartMaturityLevel(e.getKey(), e.getValue());
            Integer pvtGroup = maturityPvGroupMap.get(e.getKey());
            Map<Integer,Integer> pvGroup2LevelMap = (pvMaturityClass(e.getKey())== MaturityClass.MC_CONTENT)?contentPvGroup2LevelMap:stylePvGroup2LevelMap;
            Integer currGroupLevel = pvGroup2LevelMap.get(pvtGroup);
            // Get max level within group
            if (currGroupLevel==null || currGroupLevel < level) {
                pvGroup2LevelMap.put(pvtGroup, Integer.valueOf(level));
            }
        });
        int contentMl = contentPvGroup2LevelMap.values().stream().mapToInt(v->v.intValue()).min().orElse(0);
        int styleMl = stylePvGroup2LevelMap.values().stream().mapToInt(v->v.intValue()).min().orElse(0);
        return maturityClassesToCombinedValue(contentMl, styleMl);
    }

    public int getPartMaturityLevel(ClaSuperCollection<Long> partPvCollection) {
         if (partPvCollection==null) {
            logger.trace("Got null partPvCollection");
            return 0;
        }
//        return maturityModelSingleMaturityLevel ? getMaturityLevelSingleValue(partPvCollection) : calcPartMaturityLevelMultiValue(partPvCollection);
        return calcPartMaturityLevelMultiValue(partPvCollection);
    }

    public int getPartMaturityLevel(Map<String, ClaHistogramCollection<Long>> partCollections) {
        if (partCollections==null) {
            logger.trace("Got null partCollections");
            return 0;
        }
//        return maturityModelSingleMaturityLevel ? getMaturityLevelSingleValue(partPvCollection) : calcPartMaturityLevelMultiValue(partPvCollection);
        return calcPartMaturityLevelMultiValue(partCollections);
    }

    public int getPartMaturityLevel(String pvName, ClaHistogramCollection<Long> claHistogramCollection) {
        Integer[] thresholds = maturityThresholdsMap.get(pvName);
        if (thresholds == null) {
            return MaturityLevel.ML_High.toInt();
        }
        int level = thresholds.length-1;
        int count = claHistogramCollection.getItemsCount();
        while (level>0 && count < thresholds[level]) {
            level--;
        }
        return level+1;
    }

    public static int maturityClassesToCombinedValue(int contentMl, int styleMl) {
        return contentMl + styleMl*10;
    }

    // Get the max maturity level for all sheets (parts)
    public int workbookMaturityLevelSingleValue(final PvAnalysisData docPvAnalysisData) {
		if (docPvAnalysisData==null) {
			return 0;
		}
		return docPvAnalysisData.getPvCollections().getCollections().values().stream()
    			.mapToInt(sc -> getMaturityLevelSingleValue(sc)).max().orElse(0);
	}

    // Get the max maturity level for all sheets (parts)
    public int workbookMaturityLevelMultiValue(final PvAnalysisData docPvAnalysisData) {
        if (docPvAnalysisData == null || docPvAnalysisData.getPvCollections() == null || docPvAnalysisData.getPvCollections().getCollections() == null) {
            logger.warn("Counld not get workbook PV collection values");
            return 0;
        }
        return calcFileMaturityLevelValueByCollection(docPvAnalysisData.getPvCollections());
    }

    public int calcFileMaturityLevelValueByCollection(FileCollections fileCollections)
    {
        Collection<ClaSuperCollection<Long>> collections = fileCollections.getCollections().values();
        int maxContentMl = 0;
        int maxStyleMl = 0;
        for (ClaSuperCollection<Long> col : collections) {
            int ml = calcPartMaturityLevelMultiValue(col);
            int contentMl = maturityClassesCombinedValueToClassValue(ml, MaturityClass.MC_CONTENT);
            if (contentMl>maxContentMl) {
                maxContentMl = contentMl;
            }
            int styleMl = maturityClassesCombinedValueToClassValue(ml, MaturityClass.MC_STYLE);
            if (styleMl>maxStyleMl) {
                maxStyleMl = styleMl;
            }
        }
        return maturityClassesToCombinedValue(maxContentMl, maxStyleMl);
    }

    // Get the max maturity level for all sheets (parts)
	public int workbookMaturityLevel(final PvAnalysisData docPvAnalysisData) {
//        return maturityModelSingleMaturityLevel ? workbookMaturityLevelSingleValue(docPvAnalysisData) : workbookMaturityLevelMultiValue(docPvAnalysisData);
        return workbookMaturityLevelMultiValue(docPvAnalysisData);
	}

    private void fillPvtGroups(String maturityPvtGroups, Integer startGroup) {
        if(maturityPvtGroups.isEmpty()) {
            return;
        }
        Set<String> pvtGroupsSet = Stream.of(maturityPvtGroups.split(",")).collect(toSet());
        for (String pvtGroup : pvtGroupsSet) {
            String[] items = pvtGroup.split("\\|");
            for (String g : items) {
                maturityPvGroupMap.put(g, startGroup);
            }
            startGroup++;
        }
    }

    public void setMaturityPvGroupMap(Map<String, Integer> maturityPvGroupMap) {
        this.maturityPvGroupMap = maturityPvGroupMap;
    }

    public void setMaturityThresholdsMap(Map<String, Integer[]> maturityThresholdsMap) {
        this.maturityThresholdsMap = maturityThresholdsMap;
    }

    public static void maturityThreshodsToPvGroups(Map<String, Integer[]> maturityThresholdsMap, Map<String, Integer> maturityPvGroupMap, String maturityPvGroups) {
        Integer pvtGroup = 0;
        // Set each pv as a separate group unless it is later found as a group member
        for (Map.Entry<String, Integer[]> e : maturityThresholdsMap.entrySet()) {
            maturityPvGroupMap.put(e.getKey(), pvtGroup);
            pvtGroup++;
        }
        // Parse groups and update their group member
        fillPvtGroups(maturityPvGroups, pvtGroup, maturityPvGroupMap);
        logger.info("MaturityLevelPvtGroups: {}", maturityPvGroupMap.toString());
    }

    public static void fillMaturityLevelThresholds(Map<String, Integer[]> maturityThresholdsMap, String maturityThreshodsByPV, MaturityLevel level) {
        if(maturityThreshodsByPV.isEmpty()) {
            return;
        }
        Set<String> pvtSet = Stream.of(maturityThreshodsByPV.split(",")).collect(toSet());
        for (String pvtDef : pvtSet) {
            String[] items = pvtDef.split(":",2);
            String pvName = items[0];
            int val = Integer.valueOf(items[1]).intValue();
            Integer[] valArray =  maturityThresholdsMap.get(pvName);
            if (valArray == null) {
                valArray = new Integer[MaturityLevel.MAX_VALUE];
                maturityThresholdsMap.put(pvName, valArray);
                for (int i=0;i<valArray.length;++i) valArray[i] = 0;
            }
            valArray[level.toInt()] = val;
        }
//		logger.debug("Set fillMaturityLevelThresholds {} to {}", level, maturityThreshodsByPV);
    }

    private static void fillPvtGroups(String maturityPvtGroups, Integer startGroup, Map<String, Integer> maturityPvGroupMap) {
        if(maturityPvtGroups==null || maturityPvtGroups.isEmpty()) {
            return;
        }
        Set<String> pvtGroupsSet = Stream.of(maturityPvtGroups.split(",")).collect(toSet());
        for (String pvtGroup : pvtGroupsSet) {
            String[] items = pvtGroup.split("\\|");
            for (String g : items) {
                maturityPvGroupMap.put(g, startGroup);
            }
            startGroup++;
        }
    }

    //    public int getPartMaturityLevel(ClaSuperCollection<Long> partPvCollection) {
//        if (partPvCollection==null) {
//            return 0;
//        }
//        return maturityModelSingleMaturityLevel ? getMaturityLevelSingleValue(partPvCollection) : calcPartMaturityLevelMultiValue(partPvCollection);
//    }

}
