package com.cla.filecluster.service.files.pv;

import com.cla.common.domain.dto.pv.PVType;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class SimplifiedPartSimilarityMaps implements Map<PVType, SimplifiedSimilarityMap> {
	private Map<PVType, SimplifiedSimilarityMap> similarityMap = new HashMap<>();
	private Map<PVType, Integer> pvItemsCounts = new HashMap<>();
	private Map<PVType, Integer> pvGlobalCounts = new HashMap<>();
	
	public SimplifiedPartSimilarityMaps() {
	}

	// Similarity stats counters
	private final int[] counters = new int[]{0,0,0};
	
	public int incrementCounter(final int inx) {
		if (inx<0 || inx>=counters.length)
			return -1;
		return ++(counters[inx]);
	}

	public int getCounter(final int inx) {
		if (inx<0 || inx>=counters.length)
			return -1;
		return counters[inx];
	}

	public void resetCounter(final int inx) {
		if (inx<0 || inx>=counters.length)
			return;
		counters[inx] = 0;
	}
	public void resetCounters() {
		for (int i=0;i<counters.length;++i)
			counters[i] = 0;
	}
	
	@Override
	public void clear() {
		similarityMap.clear();
		pvItemsCounts.clear();
		pvGlobalCounts.clear();
	}

	@Override
	public boolean containsKey(final Object key) {
		return (key==null)?null:similarityMap.containsKey(key);
	}

	@Override
	public boolean containsValue(final Object value) {
		return (value==null)?null:similarityMap.containsValue(value);
	}

	@Override
	public Set<Entry<PVType, SimplifiedSimilarityMap>> entrySet() {
		return similarityMap.entrySet();
	}

//	public Set<Entry<PVType, Integer>> pvGlobalCountsEntrySet() {
//		return pvGlobalCounts.entrySet();
//	}

	@Override
	public SimplifiedSimilarityMap get(final Object key) {
		return similarityMap.get(key);
	}
	
//	public Integer getPvGlobalCount(final PVType key) {
//		return pvGlobalCounts.get(key);
//	}

	@Override
	public boolean isEmpty() {
		return similarityMap.isEmpty();
	}

	@Override
	public Set<PVType> keySet() {
		return similarityMap.keySet();
	}

//	public Set<PVType> pvGlobalCountsKeySet() {
//		return pvGlobalCounts.keySet();
//	}

	@Override
	public SimplifiedSimilarityMap put(final PVType key, final SimplifiedSimilarityMap value) {
		return (key==null)?null:similarityMap.put(key, value);
	}

//	public Integer put(final PVType key, final Integer value) {
//		return (key==null)?null:pvGlobalCounts.put(key, value);
//	}

	public void put(final PVType key, final SimplifiedSimilarityMap sValue, final Integer itemsCountValue, final Integer globalCountValue) {
		if (key!=null) {
			similarityMap.put(key, sValue);
			pvItemsCounts.put(key, itemsCountValue);
			pvGlobalCounts.put(key, globalCountValue);
		}
	}

	@Override
	public void putAll(final Map<? extends PVType, ? extends SimplifiedSimilarityMap> m) {
		if (m!=null)
			similarityMap.putAll(m);
	}

//	public void putAllPvCounts(final Map<? extends PVType, ? extends Integer> m) {
//		if (m!=null)
//			pvGlobalCounts.putAll(m);
//	}

	@Override
	public SimplifiedSimilarityMap remove(final Object key) {
		return (key==null)?null:similarityMap.remove(key);
	}

	@Override
	public int size() {
		return similarityMap.size();
	}

	public int pvGlobalCountsSize() {
		return pvGlobalCounts.size();
	}

	@Override
	public Collection<SimplifiedSimilarityMap> values() {
		return similarityMap.values();
	}
	
	public Collection<Integer> pvItemsCountsValues() {
		return pvItemsCounts.values();
	}

	public Collection<Integer> pvGlobalCountsValues() {
		return pvGlobalCounts.values();
	}

	public Map<PVType, Integer> getPvItemsCounts() {
		return pvItemsCounts;
	}

	public Map<PVType, Integer> getPvGlobalCounts() {
		return pvGlobalCounts;
	}

	public void close() {
		if (!isClosed()) {
			similarityMap.values().stream().forEach(sm->sm.close());
			similarityMap.clear();
			similarityMap = null;
			pvItemsCounts.clear();
			pvGlobalCounts.clear();
			pvGlobalCounts = null;
		}
	}
	
	public boolean isClosed() {
		return (similarityMap == null);
	}

}
