package com.cla.filecluster.domain.dto.excel;


import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.*;


public class ExcelSimilarityResult{
	
	private String fileName;

	
	private final List<String> valuesSimilarityFiles = new LinkedList<>();
	@JsonProperty
	private final List<String> valuesSimilarityTitles = new LinkedList<>();
	
	private final List<String> valuesCountsSimilarityFiles = new LinkedList<>();
	@JsonProperty
	private final List<String> valuesCountsSimilarityTitles = new LinkedList<>();

	private final List<String> formulasCountsSimilarityFiles = new LinkedList<>();
	@JsonProperty
	private final List<String> formulasCountsSimilarityTitles = new LinkedList<>();

	private final List<String> formulaSimilarityFiles = new LinkedList<>();
	@JsonProperty
	private final List<String> formulaSimilarityTitles = new LinkedList<>();
	
	@JsonProperty("versionSimilarityRates")
	private final List<Float> valuesSimilarityRates = new LinkedList<>();
	
	@JsonProperty
	private final List<Float> valuesCountsSimilarityRates = new LinkedList<>();

	@JsonProperty
	private final List<Float> formulasCountsSimilarityRates = new LinkedList<>();

	@JsonProperty("categorySimilarityRates")
	private final List<Float> formulaSimilarityRates = new LinkedList<>();

	@JsonProperty
	private Integer fileId;

	@JsonProperty
	private Set<String> titles;
	

	public Set<String> getTitles() {
		return titles;
	}


	public ExcelSimilarityResult() {
	}

	
	public ExcelSimilarityResult(final String fileName) {
		final String[] split = fileName.split("/");
		this.fileName=split[0];
		if (split.length > 1)
			this.fileId = Integer.valueOf(split[1]);
		else
			this.fileId = 0;
	}
	
	public String getName(){
		return fileName+"/"+fileId;
	}
	
	public Integer getFileId() {
		return fileId;
	}


	public void setFileId(final Integer fileId) {
		this.fileId = fileId;
	}


	public void addSimilarValueFile(final String fileName){
		valuesSimilarityFiles.add(fileName);
	}
	
	public void addSimilarValueTitles(final Collection<String> titles){
		valuesSimilarityTitles.addAll(titles);
	}
	
	public void addSimilarValueCountFile(final String fileName){
		valuesCountsSimilarityFiles.add(fileName);
	}

	public void addSimilarValueCountTitles(final Collection<String> titles){
		valuesCountsSimilarityTitles.addAll(titles);
	}
	
	public void addSimilarFormulaCountFile(final String fileName){
		formulasCountsSimilarityFiles.add(fileName);
	}

	public void addSimilarFormulaCountTitles(final Collection<String> titles){
		formulasCountsSimilarityTitles.addAll(titles);
	}
	
	
	public void addSimilarFormulaFile(final String fileName){
		formulaSimilarityFiles.add(fileName);
	}

	public void addSimilarFormulaTitles(final Collection<String> titles){
		formulaSimilarityTitles.addAll(titles);
	}

	public void addSimilarValueRate(final float rate){
		valuesSimilarityRates.add(rate);
	}
	
	public void addSimilarValueCountRate(final float rate){
		valuesCountsSimilarityRates.add(rate);
	}

	public void addSimilarFormulaCountRate(final float rate){
		formulasCountsSimilarityRates.add(rate);
	}

	
	public void addSimilarFormulaRate(final float rate){
		formulaSimilarityRates.add(rate);
	}


	public List<String> getValuesSimilarityTitles() {
		return valuesSimilarityTitles;
	}


	public List<String> getValuesCountsSimilarityTitles() {
		return valuesCountsSimilarityTitles;
	}


	public List<String> getFormulasCountsSimilarityTitles() {
		return formulasCountsSimilarityTitles;
	}


	public List<String> getFormulaSimilarityTitles() {
		return formulaSimilarityTitles;
	}


	@JsonProperty
	public String getFileName() {
//		return fileName;
		return fileName.split("/")[0];
	}

	
	@JsonProperty("versionSimilarity")
	public List<String> getValuesSimilarityFiles() {
//		return valuesSimilarityFiles;
		final List<String> onlyNames = new ArrayList<>(valuesSimilarityFiles.size());
		for (final String valuesSimilarityFile : valuesSimilarityFiles) 
			onlyNames.add(valuesSimilarityFile.split("/")[0]);
		return onlyNames;
	}
	
	
	public List<Integer> getForulasSimilarityFilesIds() {
//		return valuesSimilarityFiles;
		final List<Integer> onlyIds = new ArrayList<>(formulaSimilarityFiles.size());
		for (final String valuesSimilarityFile : formulaSimilarityFiles) 
			onlyIds.add(Integer.valueOf(valuesSimilarityFile.split("/")[1]));
		return onlyIds;
	}
	
	public List<Integer> getValuesSimilarityFilesIds() {
//		return valuesSimilarityFiles;
		final List<Integer> onlyIds = new ArrayList<>(valuesSimilarityFiles.size());
		for (final String valuesSimilarityFile : valuesSimilarityFiles) 
			onlyIds.add(Integer.valueOf(valuesSimilarityFile.split("/")[1]));
		return onlyIds;
	}
	
	@JsonProperty
	public List<String> getValuesCountsSimilarityFiles() {
//		return valuesSimilarityFiles;
		final List<String> onlyNames = new ArrayList<>(valuesCountsSimilarityFiles.size());
		for (final String valuesCountSimilarityFile : valuesCountsSimilarityFiles) 
			onlyNames.add(valuesCountSimilarityFile.split("/")[0]);
		return onlyNames;
	}

	@JsonProperty
	public List<String> getFormulasCountsSimilarityFiles() {
//		return valuesSimilarityFiles;
		final List<String> onlyNames = new ArrayList<>(formulasCountsSimilarityFiles.size());
		for (final String formulasCountSimilarityFile : formulasCountsSimilarityFiles) 
			onlyNames.add(formulasCountSimilarityFile.split("/")[0]);
		return onlyNames;
	}


	@JsonProperty("categorySimilarity")
	public List<String> getFormulaSimilarityFiles() {
//		return formulaSimilarityFiles;
		final List<String> onlyNames = new ArrayList<>(formulaSimilarityFiles.size());
		for (final String formulaSimilarityFile : formulaSimilarityFiles) 
			onlyNames.add(formulaSimilarityFile.split("/")[0]);
		return onlyNames;
	}


	public void setTitles(final Set<String> titles) {
		this.titles=titles;		
	}
	
	
	

}
