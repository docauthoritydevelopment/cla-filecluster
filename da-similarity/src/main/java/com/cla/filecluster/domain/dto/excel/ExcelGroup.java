package com.cla.filecluster.domain.dto.excel;

import com.cla.common.domain.dto.pv.TitleToken;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

public class ExcelGroup {

    public enum GroupType {VERSION, CATEGORY}


    @JsonProperty
    private String id = UUID.randomUUID().toString();

    @JsonProperty
    private final Set<Integer> fileIds = new HashSet<>();

    @JsonProperty
    private Set<String> fileNames = new HashSet<>();


    @JsonProperty
    private int firstFileId;

    @JsonProperty
//	@Field("name")
    private String firstFileName;

    @JsonProperty
//	@Field("groupType")
    private GroupType type;

    @JsonProperty
//	@ElementCollection
//	@Field
    private Set<String> titles = new HashSet<>();

    //	@Transient
    @JsonProperty
    private List<TitleToken> excelTitleToken;


    public ExcelGroup() {
    }


    public void setId(final String id) {
        this.id = id;
    }


    public ExcelGroup(final int firstFileId, final String firstFileName, final GroupType type) {
        this.firstFileId = firstFileId;
        fileIds.add(firstFileId);
        this.firstFileName = firstFileName;
        this.type = type;
    }


    public int getFirstFileId() {
        return firstFileId;
    }

    public void setFirstFileId(final int firstFileId) {
        this.firstFileId = firstFileId;
    }

    public String getFirstFileName() {
        return firstFileName;
    }

    public void setFirstFileName(final String firstFileName) {
        this.firstFileName = firstFileName;
    }

    public GroupType getType() {
        return type;
    }

    public void setType(final GroupType type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public Set<Integer> getFileIds() {
        return fileIds;
    }


    public Set<String> getFileNames() {
        return fileNames;
    }


    public void setFileNames(final Set<String> fileNames) {
        this.fileNames = fileNames;
    }


    public Set<String> getTitles() {
        return titles;
    }


    public void setTitles(final Set<String> titles) {
        this.titles = titles;
    }


    public void setExcelTitleToken(final List<TitleToken> excelTitleToken) {
        this.excelTitleToken = excelTitleToken;
    }


}
