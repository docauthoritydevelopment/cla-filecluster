package com.cla.filecluster.domain.dto.excel;

import com.cla.common.domain.dto.mediaproc.excel.ExcelWorkbook;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.SerializationUtils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.GZIPInputStream;

public class ExcelFile{


    @JsonProperty
    private int id;

    @JsonProperty
    private String fullName;

    private ExcelFileData claFileData;


    public ExcelFile() {
    }

    public ExcelFile(final String fullName) {
        this.fullName = fullName;
    }


    public String getFullName() {
        return fullName;
    }


    public byte[] getAnalyzedData() {
        return (claFileData == null) ? null : claFileData.getAnalyzedData();
    }

    public void setAnalyzedData(final byte[] analyzedData) {
        claFileData = new ExcelFileData(analyzedData);
    }


    public ExcelFileData getClaFileData() {
        return claFileData;
    }

    public void setClaFileData(final ExcelFileData claFileData) {
        this.claFileData = claFileData;
    }


    public int getId() {
        return id;
    }

    public void setId(final int id) {
        this.id = id;
    }

    public void setFullName(final String fullName) {
        this.fullName = fullName;
    }

    public ExcelWorkbook getExcelWorkbook() {
        return (ExcelWorkbook) SerializationUtils.deserialize(getAnalyzedData());
    }


    public byte[] unCompressBytes(final byte[] str) throws IOException {
        if (str == null || str.length == 0) {
            return null;
        }
        // Conditional compression: look for GZIP magic number prefix.
        if (str[0] != 31 || str[1] != -117) return str;

        final GZIPInputStream gis = new GZIPInputStream(new ByteArrayInputStream(str));
        final ByteArrayOutputStream outputStream = new ByteArrayOutputStream(10000);
        final byte[] line = new byte[10000];
        int count;
        while ((count = gis.read(line)) > 0) {
            outputStream.write(line, 0, count);
        }
        return outputStream.toByteArray();
    }


}
