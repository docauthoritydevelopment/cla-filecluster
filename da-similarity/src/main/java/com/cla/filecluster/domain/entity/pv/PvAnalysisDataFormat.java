package com.cla.filecluster.domain.entity.pv;

public enum PvAnalysisDataFormat {

    PV_COLLECTIONS, PV_ARRAYS
}
