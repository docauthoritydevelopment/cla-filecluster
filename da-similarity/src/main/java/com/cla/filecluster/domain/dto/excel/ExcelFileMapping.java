package com.cla.filecluster.domain.dto.excel;

public class ExcelFileMapping {

//	@Id
	private Integer fileId;

//	@Column(columnDefinition = "char(36)")
	private String groupId;

	public ExcelFileMapping() {
	}

	public ExcelFileMapping(final Integer fileId, final String groupId) {
		this.fileId = fileId;
		this.groupId = groupId;
	}

	public Integer getFileId() {
		return fileId;
	}

	public String getGroupId() {
		return groupId;
	}

}
