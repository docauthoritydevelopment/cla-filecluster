package com.cla.filecluster.domain.dto.excel;

import com.cla.common.domain.dto.histogram.ClaHistogramCollection;
import com.cla.common.domain.dto.mediaproc.excel.ExcelSheet;
import com.cla.common.domain.dto.mediaproc.excel.ExcelWorkbook;
import com.cla.common.domain.dto.mediaproc.excel.ExcelWorkbookHelper;
import com.cla.common.domain.dto.mediaproc.excel.ExcelWorkbookMetaData;
import com.cla.common.domain.dto.pv.PVType;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class ExcelSimilarity{

	private Integer id;

	private static final float formulaMinDominanceRate = 0.01f;
	private static final float styleMinComparedCells = 8;
	private static final float minHeadingsMatchRatio = 0.8f;
	private static final float minHeadingsCountThreshod = 8;	// compared with sum of headingsA+B.
	private static final float sheetLayoutSimilarityThreshold = 0.9f;

	@JsonProperty
//	@Transient
	private final Map<String, ExcelSheetSimilarity> sheetsSimilarity = new LinkedHashMap<>();

//	@JsonProperty
//	private int numOfEqualCells;
//
//	@JsonProperty
//	private int numOfEqualConcreteValueCells;
//
//	@JsonProperty
//	private int numOfEqualCellFormulas;
//	
//	@JsonProperty
//	private int numOfAFormulaCells;
//	
//	@JsonProperty
//	private int numOfBFormulaCells;
//
//	@JsonProperty
//	private int numOfABlankCells;
//	
//	@JsonProperty
//	private int numOfBBlankCells;
//
//	@JsonProperty
//	private int numOfACells;
//
//	@JsonProperty
//	private int numOfBCells;
//	
//	@JsonProperty
//	private int countOfEquals;
//	
//	@JsonProperty
//	private int numOfASheets;
//
//	@JsonProperty
//	private int numOfBSheets;
//	
//	@JsonProperty
//	private int numOfHeadingsA;
//	
//	@JsonProperty
//	private int numOfHeadingsB;
//	
//	@JsonProperty
//	private int numOfMatchedHeadingSheets;
//	
//	@JsonProperty
//	private int numOfMatchedHeadingValues;
//
//	@JsonProperty
//	private float headingsSheetsSimilarityRate;
//
//	@JsonProperty
//	private int countOfEqualFormulas;
//	
//	@JsonProperty
//	private float formulaSimilarityRate;
//
//	@JsonProperty
//	private float formulasCountSimilarityRate;
//
//	@JsonProperty
//	private float valuesCountSimilarityRate;
//
//	@JsonProperty
//	private float concreteValuesSimilarityRate;
//
//	@JsonProperty
//	private float valuesSimilarityRate;
//	
//	@JsonProperty
//	private float styleSimilarityRate;
//	
//	@JsonProperty
//	private float layoutSimilarityRate;
	
	@JsonProperty
	private float comparedSheetsDominamce;
	
	// Maintain titles on this object to assist later collection when the grouping results is finalized.

	private String proposedTitlesA;

	private String proposedTitlesB;
	
	private static ObjectMapper mapper = new ObjectMapper();

	@JsonProperty
	ExcelWorkbookMetaData fileAProps;

	@JsonProperty
	ExcelWorkbookMetaData fileBProps;



	private boolean requireTotalsRecalc = true;

	private String sheetsSimilarityDebString = null;

	public ExcelSimilarity() {
		init();
	}

	public ExcelSimilarity(final ExcelWorkbook book1, final ExcelWorkbook book2) {
		this(book1, book2, true);
	}
	
	public ExcelSimilarity(final ExcelWorkbook book1, final ExcelWorkbook book2, final boolean fullHeadingMatch) {
		this();
		
		final Map<String, ExcelSheet> sheetsA = new LinkedHashMap<>(book1.getSheets());
		final Map<String, ExcelSheet> sheetsB = new LinkedHashMap<>(book2.getSheets());
		
		fileAProps = book1.getMetadata();
		fileBProps = book2.getMetadata();
		
		/*
		 * Look for sheets with matching names (exact match for now)
		 * Compare sheets with matching names and then remove them from the sheets lists to allow iteration on remaining sheets.
		 */
		final Map<String, String> book1SheetNamesMap = book1.getSheetFullNamesMap();
		final Map<String, String> book2SheetNamesMap = book2.getSheetFullNamesMap();
		final Iterator<String> book1SheetNamesIter = book1SheetNamesMap.keySet().iterator();
		while (book1SheetNamesIter.hasNext()) {
			final String fullNameA = book1SheetNamesIter.next();
			final String sheetNameB = book2SheetNamesMap.get(fullNameA);
			if (sheetNameB != null) {
				final String sheetNameA = book1SheetNamesMap.get(fullNameA);
				final ExcelSheet sheetA = sheetsA.get(sheetNameA);
				final ExcelSheet sheetB = sheetsB.get(sheetNameB);
				final ExcelSheetSimilarity sheetSimilarity = new ExcelSheetSimilarity(sheetA, sheetB);
				// If sheets seem strangers, then ignore the named matched sheets.
				// TODO: take the threshold out to config file
				if (sheetSimilarity.getCombinedCountSimilarityRate() > 0.5 || 
						sheetSimilarity.getStyleSimilarityRate() > 0.5 ||
						sheetNameA.equals(sheetNameB)) {
					addSheetsSimilarity(sheetNameA+","+sheetNameB, sheetSimilarity);
					// remove the matched sheets from the sheets-lists
					sheetsA.remove(sheetNameA);
					sheetsB.remove(sheetNameB);
				}
			}
		}	// Iterating book1SheetNamesIter
		
		// Perform sheet by sheet comparison
		// 	Naive approach: direct positional match - no attempt for skip or look for best match
		final Iterator<String> i1 = sheetsA.keySet().iterator();
		final Iterator<String> i2 = sheetsB.keySet().iterator();
		boolean b1 = i1.hasNext(); 
		boolean b2 = i2.hasNext(); 
		while (b1 && b2) {
			final String nameA = i1.next();
			final String nameB = i2.next();
			final ExcelSheet sheetA = sheetsA.get(nameA);
			final ExcelSheet sheetB = sheetsB.get(nameB);
			
			final ExcelSheetSimilarity sheetSimilarity = new ExcelSheetSimilarity(sheetA, sheetB);
			addSheetsSimilarity(nameA+","+nameB, sheetSimilarity);

			b1 = i1.hasNext(); 
			b2 = i2.hasNext(); 
		}
		finalizeGlobalMetrics();
		setProposedTitlesA(ExcelWorkbookHelper.getProposedTitles(book1));
		setProposedTitlesB(ExcelWorkbookHelper.getProposedTitles(book2));

	}

	private void init() {
	}


	
//	public ExcelSimilarity(final int numOfACells, final int numOfBCells,final int numOfAFormulaCells, final int numOfBFormulaCells,
//			final int numOfABlankCells, final int numOfBBlankCells, final int numOfASheets, final int numOfBSheets) {
//		this.numOfAFormulaCells = numOfAFormulaCells;
//		this.numOfBFormulaCells = numOfBFormulaCells;
//		this.numOfACells = numOfACells;
//		this.numOfBCells = numOfBCells;
//		this.numOfABlankCells = numOfABlankCells;
//		this.numOfBBlankCells = numOfBBlankCells;
//		this.numOfASheets = numOfASheets;
//		this.numOfBSheets = numOfBSheets;
//		requireTotalsRecalc = true;
//	}

//	public void increaseCountOfEquals(final int equals) {
//		countOfEquals = countOfEquals+equals;
//		requireTotalsRecalc = true;
//	}
//	
//	public void increaseCountOfEqualFormulas(final int equals) {
//		countOfEqualFormulas = countOfEqualFormulas+equals;
//		requireTotalsRecalc = true;
//	}
//	
//	public void increaseEquals() {
//		numOfEqualCells++;
//		requireTotalsRecalc = true;
//	}
//	
//	public void increaseConcreteValueEquals() {
//		numOfEqualConcreteValueCells++;
//		requireTotalsRecalc = true;
//	}
//
//	public void increaseEqualFormulas() {
//		numOfEqualCellFormulas++;
//		requireTotalsRecalc = true;
//	}
	
	public void addSheetsSimilarity(final String key, final ExcelSheetSimilarity sheetSimilarity) {
		this.sheetsSimilarity.put(key, sheetSimilarity);
		requireTotalsRecalc = true;
	}

	public void finalizeGlobalMetrics() {
		// update calculated value into private members
//		getValuesSimilarityRate();
//		getConcreteValuesSimilarityRate();
//		getValuesCountSimilarityRate();
//		getFormulasCountSimilarityRate();
//		getFormulaSimilarityRate();
//		getCombinedCountsSimilarityRate();
//		getStyleSimilarityRate();
		getSheetsSimilarityDebString();
//		getLayoutSheetsSimilarityRate();
		
	}
	
	
	@Override
	public String toString() {
		return "ExcelSimilarity [f1=" + fileAProps.toString() + " f2="+ fileBProps.toString() + 
				" simInfo=" + getSheetsSimilarityDebString(true) + "]";
	}

	private void setSheetsSimilarityDebString(final String sheetsSimilarityDebString) {
	}

	public String getSheetsSimilarityDebString() {
		return getSheetsSimilarityDebString(false);
	}

	public String getSheetsSimilarityDebString(final boolean withTitles) {
		final Map<PVType, StringBuffer> sbMap = new HashMap<>();
		Arrays.asList(ClaHistogramCollection.excelLongHistogramNames).forEach(name->sbMap.put(name, new StringBuffer()));

		boolean first = true;
		for (final Map.Entry<String, ExcelSheetSimilarity> sheetSimEntry : sheetsSimilarity.entrySet()) {
			final ExcelSheetSimilarity sheetSimilarity = sheetSimEntry.getValue();
			if (first) {
				first = false;
			}
			else {
				sbMap.values().forEach(sb -> sb.append("|"));
			}
			sbMap.keySet().forEach(n -> sbMap.get(n).append((int)(100*sheetSimilarity.getSimilarityValue(n))));
		}
		if (withTitles) {
			sheetsSimilarityDebString = Arrays.asList(ClaHistogramCollection.excelLongHistogramNames).stream()
					.map(n->(n.toString()+"="+sbMap.get(n).toString())).collect(Collectors.joining(","));
		}
		else {
			sheetsSimilarityDebString = Arrays.asList(ClaHistogramCollection.excelLongHistogramNames).stream()
					.map(n->sbMap.get(n).toString()).collect(Collectors.joining(","));
		}
		return sheetsSimilarityDebString;
	}

//	public float getValuesSimilarityRate() {
//		if (requireTotalsRecalc) {
//			float rate = 0.0f;
//			int count = 0;
//			
//			for (final Map.Entry<String, ExcelSheetSimilarity> sheetSimEntry : sheetsSimilarity.entrySet()) {
//				final ExcelSheetSimilarity sheetSimilarity = sheetSimEntry.getValue();
//				final int delta = sheetSimilarity.getNumOfACells() + sheetSimilarity.getNumOfBCells();
//				count += delta;
//				rate += sheetSimilarity.getValuesSimilarityRate() * delta;
//			}
//			valuesSimilarityRate = (count==0)?0.0f:rate/count;
//			comparedSheetsDominamce = ((numOfACells+numOfBCells)==0)?0.0f:((float)count)/(numOfACells+numOfBCells);
//		}
//		return valuesSimilarityRate;
//	}

//	public float getComparedSheetsDominamce() {
//		if (requireTotalsRecalc) {
//			getValuesSimilarityRate();
//		}
//		return comparedSheetsDominamce;
//	}
//	
//	public float getConcreteValuesSimilarityRate() {
//		if (requireTotalsRecalc) {
//			float rate = 0.0f;
//			int count = 0;
//			
//			for (final Map.Entry<String, ExcelSheetSimilarity> sheetSimEntry : sheetsSimilarity.entrySet()) {
//				final ExcelSheetSimilarity sheetSimilarity = sheetSimEntry.getValue();
//				final int delta = sheetSimilarity.getNumOfACells() + sheetSimilarity.getNumOfBCells()
//						- (sheetSimilarity.getNumOfAFormulaCells() + sheetSimilarity.getNumOfBFormulaCells());
//				count += delta;
//				rate += sheetSimilarity.getConcreteValuesSimilarityRate() * delta;
//			}
//			concreteValuesSimilarityRate = (count==0)?0.0f:rate/count;
//		}
//		return concreteValuesSimilarityRate;
//	}
//
//	public float getValuesCountSimilarityRate() {
//		if (requireTotalsRecalc) {
//			float rate = 0.0f;
//			int count = 0;
//			
//			for (final Map.Entry<String, ExcelSheetSimilarity> sheetSimEntry : sheetsSimilarity.entrySet()) {
//				final ExcelSheetSimilarity sheetSimilarity = sheetSimEntry.getValue();
//				final int delta = sheetSimilarity.getNumOfACells() + sheetSimilarity.getNumOfBCells();
//				count += delta;
//				rate += sheetSimilarity.getValuesCountSimilarityRate() * delta;
//			}
//			valuesCountSimilarityRate = (count==0)?0.0f:rate/count;
//		}
//		return valuesCountSimilarityRate;
//	}
//
//	public float getStyleSimilarityRate() {
//		if (requireTotalsRecalc) {
//			float rate = 0.0f;
//			int count = 0;
//			
//			for (final Map.Entry<String, ExcelSheetSimilarity> sheetSimEntry : sheetsSimilarity.entrySet()) {
//				final ExcelSheetSimilarity sheetSimilarity = sheetSimEntry.getValue();
//				if (sheetSimilarity.getNumOfComparedStyles() < styleMinComparedCells) // set minimum number of compared styles
//					continue;
//				//int delta = sheetSimilarity.getNumOfACells() + sheetSimilarity.getNumOfBCells();
//				final int delta = sheetSimilarity.getNumOfComparedStyles();
//				count += delta;
//				rate += sheetSimilarity.getStyleSimilarityRate() * delta;
//			}
//			styleSimilarityRate = (count==0)?0.0f:rate/count;
//		}
//		return styleSimilarityRate;
//	}
//
//	
//	public float getCombinedCountsSimilarityRate() {
//		if (requireTotalsRecalc) {
//			float rate = 0.0f;
//			int count = 0;
//			
//			for (final Map.Entry<String, ExcelSheetSimilarity> sheetSimEntry : sheetsSimilarity.entrySet()) {
//				final ExcelSheetSimilarity sheetSimilarity = sheetSimEntry.getValue();
//				final int delta = sheetSimilarity.getNumOfACells() + sheetSimilarity.getNumOfBCells();
//				count += delta;
//				rate += sheetSimilarity.getCombinedCountSimilarityRate() * delta;
//			}
//			combinedCountsSimilarityRate = (count==0)?0.0f:rate/count;
//		}
//		return combinedCountsSimilarityRate;
//	}
//
//	public float getFormulasCountSimilarityRate() {
//		if (requireTotalsRecalc) {
//			float rate = 0.0f;
//			int count = 0;
//			
//			for (final Map.Entry<String, ExcelSheetSimilarity> sheetSimEntry : sheetsSimilarity.entrySet()) {
//				final ExcelSheetSimilarity sheetSimilarity = sheetSimEntry.getValue();
//				if (sheetSimilarity.getFormulaDominanceRate() < formulaMinDominanceRate)
//					continue;
//				final int delta = sheetSimilarity.getNumOfAFormulaCells() + sheetSimilarity.getNumOfBFormulaCells();
//				count += delta;
//				rate += sheetSimilarity.getFormulasCountSimilarityRate() * delta;
//			}
//			formulasCountSimilarityRate = (count==0)?0.0f:rate/count;
//		}
//		return formulasCountSimilarityRate;
//	}
//
//	public float getFormulaSimilarityRate() {
//		if (requireTotalsRecalc) {
//			float rate = 0.0f;
//			int count = 0;
//			
//			for (final Map.Entry<String, ExcelSheetSimilarity> sheetSimEntry : sheetsSimilarity.entrySet()) {
//				final ExcelSheetSimilarity sheetSimilarity = sheetSimEntry.getValue();
//				if (sheetSimilarity.getFormulaDominanceRate() < formulaMinDominanceRate)
//					continue;
//				final int delta = sheetSimilarity.getNumOfAFormulaCells() + sheetSimilarity.getNumOfBFormulaCells();
//				count += delta;
//				rate += sheetSimilarity.getFormulaSimilarityRate() * delta;
//			}
//			formulaSimilarityRate = (count==0)?0.0f:rate/count;
//		}
//		return formulaSimilarityRate;
//	}
//
//	@JsonProperty
//	public float getFormulaDominanceRate() {
//		if(numOfACells+numOfBCells == 0)
//			return 0;
//		return ((float)(numOfAFormulaCells+numOfBFormulaCells))/((float)(numOfACells+numOfBCells));
//	}
//
//	@Access(AccessType.PROPERTY)
//	public float getHeadingsSheetsSimilarityRate() {
//		return headingsSheetsSimilarityRate;
//	}
//	
//	private void setHeadingsSheetsSimilarityRate(final float headingsSheetsSimilarityRate) {
//	}

	/*
	 * Getters for debug purposes
	 */
//	public int getNumOfEqualCells() {
//		return numOfEqualCells;
//	}
//
//	public int getNumOfEqualCellFormulas() {
//		return numOfEqualCellFormulas;
//	}
//
//	public int getNumOfAFormulaCells() {
//		return numOfAFormulaCells;
//	}
//
//	public int getNumOfBFormulaCells() {
//		return numOfBFormulaCells;
//	}
//
//	public int getNumOfACells() {
//		return numOfACells;
//	}
//
//	public int getNumOfBCells() {
//		return numOfBCells;
//	}
//
//	public int getCountOfEquals() {
//		return countOfEquals;
//	}
//
//	public int getCountOfEqualFormulas() {
//		return countOfEqualFormulas;
//	}
//	
//	public int getNumOfABlankCells() {
//		return numOfABlankCells;
//	}
//
//	public int getNumOfBBlankCells() {
//		return numOfBBlankCells;
//	}
//
//	public int getNumOfASheets() {
//		return numOfASheets;
//	}
//
//
//
//	public void setNumOfASheets(final int numOfASheets) {
//		this.numOfASheets = numOfASheets;
//	}
//
//	public int getNumOfBSheets() {
//		return numOfBSheets;
//	}
//
//
//
//	public void setNumOfBSheets(final int numOfBSheets) {
//		this.numOfBSheets = numOfBSheets;
//	}
//
//
//
//	public int getNumOfHeadingsA() {
//		return numOfHeadingsA;
//	}
//
//
//
//	public void setNumOfHeadingsA(final int numOfHeadingsA) {
//		this.numOfHeadingsA = numOfHeadingsA;
//	}
//
//
//
//	public int getNumOfHeadingsB() {
//		return numOfHeadingsB;
//	}
//
//
//
//	public void setNumOfHeadingsB(final int numOfHeadingsB) {
//		this.numOfHeadingsB = numOfHeadingsB;
//	}
//
//
//
//	public int getNumOfMatchedHeadingSheets() {
//		return numOfMatchedHeadingSheets;
//	}
//
//
//
//	public void setNumOfMatchedHeadingSheets(final int numOfMatchedHeadingSheets) {
//		this.numOfMatchedHeadingSheets = numOfMatchedHeadingSheets;
//	}
//
//
//
//	public int getNumOfMatchedHeadingValues() {
//		return numOfMatchedHeadingValues;
//	}
//
//
//
//	public void setNumOfMatchedHeadingValues(final int numOfMatchedHeadingValues) {
//		this.numOfMatchedHeadingValues = numOfMatchedHeadingValues;
//	}

	public ExcelWorkbookMetaData getFileAProps() {
		return fileAProps;
	}

	public ExcelWorkbookMetaData getFileBProps() {
		return fileBProps;
	}

	@SuppressWarnings("unchecked")
	public Map<String, String> getProposedTitlesA() {
		try {
			if(proposedTitlesA==null)
				return null;
			return mapper.readValue(proposedTitlesA,Map.class);
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
	}

	public void setProposedTitlesA(final Map<String, String> proposedTitlesA) {
		try {
			if(proposedTitlesA!=null)
				this.proposedTitlesA = mapper.writeValueAsString(proposedTitlesA);
		} catch (final JsonProcessingException e) {
			throw new RuntimeException(e);
		}
	}

	@SuppressWarnings("unchecked")
	public Map<String, String> getProposedTitlesB() {
		try {
			if(proposedTitlesB==null)
				return null;
			return mapper.readValue(proposedTitlesB,Map.class);
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
	}

	public void setProposedTitlesB(final Map<String, String> proposedTitlesB) {
		try {
			if(proposedTitlesB != null) {
				this.proposedTitlesB = mapper.writeValueAsString(proposedTitlesB);
			}
		} catch (final JsonProcessingException e) {
			throw new RuntimeException(e);
		}
	}

	public static String toCsvStringTitle() {
		return toCsvStringTitle(",");
	}
	
	public static String toCsvStringTitle(final String sep) {
		return 
				"names" + sep + 
				Arrays.stream(ClaHistogramCollection.excelLongHistogramNames)
					.map(PVType::toString)
					.collect(Collectors.joining(sep))+sep+"simDeb"+sep+"dominance";
	}
	
	// This code dumps a column per sheet
	public String toCsvString() {
		return toCsvString(",");
	}

	public String toCsvString(final String sep) {
		
		final String str = sheetsSimilarity.values().stream().map( (sheetSimilarity) -> {
				final StringBuffer res = new StringBuffer();
			
				res.append(sheetSimilarity.getSheetAProps().getSheetName());
				res.append('+');
				res.append(sheetSimilarity.getSheetBProps().getSheetName());
				res.append(',');
				res.append(Arrays.stream(ClaHistogramCollection.excelLongHistogramNames)
						.map(n->String.valueOf((int)(0.5f+100*sheetSimilarity.getSimilarityValue(n))))
						.collect(Collectors.joining(",")) );
				res.append(',');
				res.append(sheetSimilarity.getSimilarityDebug());
				res.append(',');
				res.append((int)(0.5f+100*getSheetSimilarityDominance(sheetSimilarity)));

				return res.toString();
		}).collect(Collectors.joining(sep));

		return str;
	}

	// This code dumps a column per PV
//	public String toCsvString() {
//		
//		final Map<String, StringBuffer> sbMap = new HashMap<>();
//		pvKeyList.forEach(name->sbMap.put(name, new StringBuffer()));
//		StringBuffer namesSb = new StringBuffer();
//
//		boolean first = true;
//		for (final Map.Entry<String, ExcelSheetSimilarity> sheetSimEntry : sheetsSimilarity.entrySet()) {
//			final ExcelSheetSimilarity sheetSimilarity = sheetSimEntry.getValue();
//			if (first) {
//				first = false;
//			}
//			else {
//				sbMap.keySet().forEach(n -> sbMap.get(n).append("|"));
//				namesSb.append("|");
//			}
//			sbMap.keySet().forEach(n -> sbMap.get(n).append((int)(100*sheetSimilarity.getSimilarityValue(n))));
//			namesSb.append(sheetSimilarity.getSheetAProps().getSheetName()+"+"+sheetSimilarity.getSheetBProps().getSheetName());
//		}
//
//		// Use pvKeyList to assure order of strings
//		final String result = pvKeyList.stream().map(n->sbMap.get(n).toString()).collect(Collectors.joining(",")); 
//		return namesSb.toString()+","+result;
//	}

//	public void setHeadingMatching(final int totalHeadingsCountA, final int totalHeadingsCountB, final int matchedSheetsCount,
//								   final int matchedHeadingsCount) {
//		this.numOfHeadingsA = totalHeadingsCountA;
//		this.numOfHeadingsB = totalHeadingsCountB;
//		this.numOfMatchedHeadingSheets = matchedSheetsCount;
//		this.numOfMatchedHeadingValues = matchedHeadingsCount;
//	}
//
//	@Access(AccessType.PROPERTY)
//	public float getLayoutSheetsSimilarityRate() {
//		if (requireTotalsRecalc) {
//			int count = 0;
//			int totals = 0;
//			
//			for (final Map.Entry<String, ExcelSheetSimilarity> sheetSimEntry : sheetsSimilarity.entrySet()) {
//				final ExcelSheetSimilarity sheetSimilarity = sheetSimEntry.getValue();
//				final float sheetRate = sheetSimilarity.getLayoutSimilarityRate();
//				final int delta = sheetSimilarity.getNumOfACells() + sheetSimilarity.getNumOfBCells();
//				totals += delta;
//				if (sheetRate > sheetLayoutSimilarityThreshold) {
//					count += delta;
//				}
//			}
//			layoutSimilarityRate = (count==0)?0.0f:((float)count)/(float)totals;
//		}
//		return layoutSimilarityRate;
//	}
//	
//	private void setLayoutSheetsSimilarityRate(final float layoutSheetsSimilarityRate) {
//	}

	private float getSheetSimilarityDominance(final ExcelSheetSimilarity sheetSimilarity) {
		final int totCells = fileAProps.getNumOfCells() + fileBProps.getNumOfCells();
		
		return (totCells == 0)?0.0f:
				((float)(sheetSimilarity.getSheetAProps().getNumOfCells()+sheetSimilarity.getSheetBProps().getNumOfCells())) 
						/ ((float)(totCells));
	}

	public float getPotentialSimilarityRate() {
		return (sheetsSimilarity.size()==0)?
				0.0f:
				sheetsSimilarity.values().stream()
						.map(ExcelSheetSimilarity::getPotentialSimilarityRate)
						.max(Float::compare).get();
	}

	public boolean passThreshold(final float contentThreshold,
			final float styleThreshold,
			final float layoutThreshold, 
			final float comparedSheetsDominamceThreshold) {

		int count = 0;
		final int totCells = fileAProps.getNumOfCells() + fileBProps.getNumOfCells();
		if (totCells == 0) {
//			logger.warn("Comparing files with no cells. {}, {}",
//					fileA.getFileName(), fileBProps.getFileName());
		
			comparedSheetsDominamce = 0.0f;
			return false;
		}
		
		for (final Map.Entry<String, ExcelSheetSimilarity> sheetSimEntry : sheetsSimilarity.entrySet()) {
			final ExcelSheetSimilarity sheetSimilarity = sheetSimEntry.getValue();
			
			if (sheetSimilarity.passThreshold(contentThreshold, styleThreshold, layoutThreshold)) {
				final int delta = sheetSimilarity.getSheetAProps().getNumOfCells() + 
						sheetSimilarity.getSheetBProps().getNumOfCells();
				count += delta;
			}
		}
		comparedSheetsDominamce = ((float)count)/totCells;
		
		return (comparedSheetsDominamce >= comparedSheetsDominamceThreshold);
	}

	@Deprecated
	public float getValuesSimilarityRate() {
		return 0;
	}

	@Deprecated
	public float getValuesCountSimilarityRate() {
		return 0;
	}

	@Deprecated
	public float getFormulasCountSimilarityRate() {
		return 0;
	}

	@Deprecated
	public float getFormulaSimilarityRate() {
		return 0;
	}
	
}
