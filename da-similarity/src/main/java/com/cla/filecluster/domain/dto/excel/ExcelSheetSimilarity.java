package com.cla.filecluster.domain.dto.excel;


import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import com.cla.common.domain.dto.mediaproc.excel.ExcelSheet;
import com.cla.common.domain.dto.mediaproc.excel.ExcelSheetMetaData;
import com.cla.common.domain.dto.histogram.ClaHistogramCollection;
import com.cla.common.domain.dto.pv.PVType;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ExcelSheetSimilarity{

	private static final int minHeadingCountThreshold = 4;
	private static final int minStyleCountThreshold = 3;
	private static final int minLayoutCountThreshold = 3;
	private static final int minNgramsCountThreshold = 10;

	@JsonProperty
	ExcelSheetMetaData sheetAProps;
	@JsonProperty
	ExcelSheetMetaData sheetBProps;
	
	@JsonProperty
	private int numOfHeadingsA;
	
	@JsonProperty
	private int numOfHeadingsB;
	
	private Integer id;
	
	private final Map<PVType, SimilarityVector> similarities = new LinkedHashMap<>();
	private int similarityDebug;

	public ExcelSheetSimilarity() {
		init();
	}

	public ExcelSheetSimilarity(final ExcelSheet sheetA, final ExcelSheet sheetB) {
		init();

		this.sheetAProps = sheetA.getMetaData();
		this.sheetBProps = sheetB.getMetaData();
		
		Arrays.asList(ClaHistogramCollection.excelLongHistogramNames)
			.forEach(simName -> setSimilarityVector(simName, sheetA.getSimilarity(sheetB, simName), 
										sheetA.getLongHistogram(simName).getGlobalCount(), 
										sheetB.getLongHistogram(simName).getGlobalCount(), 
										sheetA.getLongHistogram(simName).getItemsCount(), 
										sheetB.getLongHistogram(simName).getItemsCount() 
										/*,sheetA.getOrderedMatchCount(sheetB, simName) */));
	}
	
	public ExcelSheetSimilarity(
			final Map<String, ClaHistogramCollection<Long>> fileAHistograms,
			final Map<String, ClaHistogramCollection<Long>> fileBHistograms) {
		init();

		Arrays.asList(ClaHistogramCollection.excelLongHistogramNames)
		.forEach(simName -> {
			final ClaHistogramCollection<Long> sheetAHist = fileAHistograms.get(simName.name());
			final ClaHistogramCollection<Long> sheetBHist = fileBHistograms.get(simName.name());
			if (sheetAHist!=null && sheetBHist!=null) {
				setSimilarityVector(simName, sheetAHist.getNormalizedSimilarityRatio(sheetBHist),
						sheetAHist.getGlobalCount(), 
						sheetBHist.getGlobalCount(), 
						sheetAHist.getItemsCount(), 
						sheetBHist.getItemsCount()
						/*,sheetAHist.getOrderedSimilarityCount(sheetBHist)*/);
			}
			else {
				setSimilarityVector(simName, 0f, 0, 0, 0, 0 /*, 0*/);
			}
		});
	}

	private void init() {
		createSimilarityValues();
	}

	private void createSimilarityValues() {
		Arrays.asList(ClaHistogramCollection.excelLongHistogramNames).forEach(name -> similarities.put(name, new SimilarityVector()));
	}

	public Set<PVType> getSimilaritiesKeySet() {
		return similarities.keySet();
	}
	
	public float getSimilarityValue(final PVType name) {
		final SimilarityVector ret = similarities.get(name);
		if (ret == null)
			throw new RuntimeException("similarities name not found: " + name);
		return ret.rate;
	}

	public void setSimilarityValue(final PVType name, final float val) {
		final SimilarityVector ret = similarities.get(name);
		if (ret == null)
			throw new RuntimeException("similarities name not found: " + name);
		
		ret.rate = val;
	}

	public int getCountA(final PVType name) {
		final SimilarityVector ret = similarities.get(name);
		if (ret == null)
			throw new RuntimeException("similarities name not found: " + name);
		return ret.countA;
	}

	public void setCountA(final PVType name, final int val) {
		final SimilarityVector ret = similarities.get(name);
		if (ret == null)
			throw new RuntimeException("similarities name not found: " + name);
		
		ret.countA = val;
	}

	public int getCountB(final PVType name) {
		final SimilarityVector ret = similarities.get(name);
		if (ret == null)
			throw new RuntimeException("similarities name not found: " + name);
		return ret.countB;
	}

	public void setCountB(final PVType name, final int val) {
		final SimilarityVector ret = similarities.get(name);
		if (ret == null)
			throw new RuntimeException("similarities name not found: " + name);
		
		ret.countB = val;
	}

	public int getMinCount(final PVType name) {
		return Integer.min(getCountA(name), getCountB(name));
	}
		
	public int getMaxCount(final PVType name) {
		return Integer.max(getCountA(name), getCountB(name));
	}
		

	public int getItemsA(final PVType name) {
		final SimilarityVector ret = similarities.get(name);
		if (ret == null)
			throw new RuntimeException("similarities name not found: " + name);
		return ret.itemsA;
	}

	public void setItemsA(final PVType name, final int val) {
		final SimilarityVector ret = similarities.get(name);
		if (ret == null)
			throw new RuntimeException("similarities name not found: " + name);
		
		ret.itemsA = val;
	}

	public int getItemsB(final PVType name) {
		final SimilarityVector ret = similarities.get(name);
		if (ret == null)
			throw new RuntimeException("similarities name not found: " + name);
		return ret.itemsB;
	}

	public void setItemsB(final PVType name, final int val) {
		final SimilarityVector ret = similarities.get(name);
		if (ret == null)
			throw new RuntimeException("similarities name not found: " + name);
		
		ret.itemsB = val;
	}

	public int getMinItems(final PVType name) {
		return Integer.min(getItemsA(name), getItemsB(name));
	}
		
	public int getMaxItems(final PVType name) {
		return Integer.max(getItemsA(name), getItemsB(name));
	}
		
	private void setSimilarityVector(final PVType name, final float rate, 
			final int countA, final int countB, 
//			final int totalCountA, final int totalCountB 
			final int itemsA, final int itemsB 
			/*, final int orderedMatchCount */) {
		final SimilarityVector ret = similarities.get(name);
		if (ret == null)
			throw new RuntimeException("similarities name not found: " + name);
		
		ret.rate = rate;
		ret.countA = countA;
		ret.countB = countB;
//		ret.totalCountA = totalCountA;
//		ret.totalCountB = totalCountB;
		ret.itemsA = itemsA;
		ret.itemsB = itemsB;
//		ret.orderedMatchCount = orderedMatchCount;
	}

	public SimilarityVector getSimilarityVector(final PVType name) {
		final SimilarityVector ret = similarities.get(name);
		if (ret == null)
			throw new RuntimeException("similarities name not found: " + name);
		return ret;
	}
	
	public Integer getId() {
		return id;
	}

	public int getSimilarityDebug() {
		return similarityDebug;
	}

	public void setSimilarityDebug(final int similarityDebug) {
		this.similarityDebug = similarityDebug;
	}

	public ExcelSheetMetaData getSheetAProps() {
		return sheetAProps;
	}

	public ExcelSheetMetaData getSheetBProps() {
		return sheetBProps;
	}

	// ---

	

	/**
	 * Get a basic indication for a potential similarity
	 * 
	 * @return	A float indicating minimal similarity ratio [0-1]
	 */
	public float getPotentialSimilarityRate() {
		return similarities.size()==0?0.0f:similarities.values().stream().map(v->v.rate).max(Float::compare).get();
	}
	
	public static String getTitleCsvString() {
		return 
				Arrays.asList(ClaHistogramCollection.excelLongHistogramNames).stream().map(t->t.toString())
				.collect(Collectors.joining(","));
	}

	public boolean passThreshold(final float contentThreshold,
			final float styleThreshold,
			final float layoutThreshold) {

		final SimilarityVector rowHeadingsSimVec = getSimilarityVector(PVType.PV_RowHeadings);
		final SimilarityVector styleSimVec = getSimilarityVector(PVType.PV_ExcelStyles);
		final SimilarityVector valuesSimVec = getSimilarityVector(PVType.PV_CellValues);
		final SimilarityVector formulasSimVec = getSimilarityVector(PVType.PV_Formulas);
		final SimilarityVector concreteValuesSimVec = getSimilarityVector(PVType.PV_ConcreteCellValues);
		final SimilarityVector textNgramsSimVec = getSimilarityVector(PVType.PV_TextNgrams);
		final SimilarityVector titlesNgramsSimVec = getSimilarityVector(PVType.PV_TitlesNgrams);
		final SimilarityVector sheetLayoutSimVec = getSimilarityVector(PVType.PV_SheetLayoutSignatures);
		final float formulaDominance = getFormulaDominanceRate();
		
		final float combinedFormValueRate = formulasSimVec.rate*formulaDominance + valuesSimVec.rate*(1.0f-formulaDominance);
		
		boolean res = false;

		similarityDebug = 0;
		do {
			if (
					(	// Content oriented sim
						valuesSimVec.rate >= contentThreshold ||
						concreteValuesSimVec.rate >= contentThreshold ||
						formulasSimVec.rate >= contentThreshold
					) &&
					(	// visual/style oriented sim
							(Integer.min(rowHeadingsSimVec.countA, rowHeadingsSimVec.countB) > minHeadingCountThreshold &&
				 			 rowHeadingsSimVec.rate >= contentThreshold) ||
							(Integer.min(styleSimVec.countA, styleSimVec.countB) > minStyleCountThreshold &&
							styleSimVec.rate >= styleThreshold) ||
							sheetLayoutSimVec.rate >= layoutThreshold
					)
			) {
				res = true;
				similarityDebug = 1;
				break;
			}

		} while (false);

		return res;
	}

	public float getFormulaDominanceRate() {
		// Formulas are counted for non-analyzed cells as well
		final int denom = sheetAProps.getNumOfCells()+sheetBProps.getNumOfCells(); 
		if(denom == 0)
			return 0;
		return ((float)(sheetAProps.getNumOfFormulas()+sheetBProps.getNumOfFormulas()))/((float)(denom));
	}
	
	public float getCombinedCountSimilarityRate() {
		final float formulaDominance = getFormulaDominanceRate();
		final SimilarityVector valuesSimVec = getSimilarityVector(PVType.PV_CellValues);
		final SimilarityVector formulasSimVec = getSimilarityVector(PVType.PV_Formulas);

		return (formulasSimVec.rate*formulaDominance + valuesSimVec.rate*(1.0f-formulaDominance));
	}

	public float getStyleSimilarityRate() {
		final SimilarityVector styleSignaturesSimVec = getSimilarityVector(PVType.PV_ExcelStyles);
		return styleSignaturesSimVec.rate;
	}

	
	public String getCsvString() {
		return 
			Arrays.asList(ClaHistogramCollection.excelLongHistogramNames).stream()
				.map(name->String.valueOf(getSimilarityValue(name)))
				.collect(Collectors.joining(","));
	}
	
	private void setCsvString(final String csvString){
		
	}
	
	private class SimilarityVector {
		public float rate = 0.0f;
		public int countA = 0;
		public int countB = 0;
//		public int totalCountA = 0;
//		public int totalCountB = 0;
		public int itemsA = 0;
		public int itemsB = 0;
//		public int orderedMatchCount = 0;
		
		@Override
		public String toString() {
			return String.valueOf(((int)(rate*100f))/100f)+" "+itemsA+"/"+itemsB+" "+countA+"/"+countB;  
		}
	}

	public String getSimilarityVectorString(PVType pvName) {
		SimilarityVector sv = getSimilarityVector(pvName);
		return (sv==null)?"null":sv.toString();
	}

}
