package com.cla.filecluster.domain.dto.excel;

import com.fasterxml.jackson.annotation.JsonProperty;

//@Entity
//@Table(name = "excels_files_data")
public class ExcelFileData{

	
	private int id;

	
	private byte[] analyzedData;

	public ExcelFileData() {
	}

	public ExcelFileData(final byte[] analyzedData) {
		this.analyzedData = analyzedData;
	}

//	@Lob
	public byte[] getAnalyzedData() {
		return analyzedData;
	}

//	@Id
//	@GeneratedValue
	@JsonProperty
	public int getId() {
		return id;
	}

	public void setId(final int id) {
		this.id = id;
	}

	public void setAnalyzedData(final byte[] analyzedData) {
		this.analyzedData = analyzedData;
	}
	
	
	
	

}
