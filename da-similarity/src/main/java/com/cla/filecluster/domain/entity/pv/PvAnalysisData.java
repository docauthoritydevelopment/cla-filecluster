package com.cla.filecluster.domain.entity.pv;

import com.cla.common.domain.dto.file.FileCollections;
import com.cla.common.domain.dto.histogram.ClaSuperCollection;
import com.cla.common.domain.dto.mediaproc.AnalysisMetadata;
import com.cla.common.domain.dto.pv.PropertyVector;


public class PvAnalysisData {


    private Long contentId;

//    private AnalyzeHint analyzeHint = AnalyzeHint.NORMAL;

    private FileCollections pvCollections;

    //	@Column(columnDefinition = "BLOB")
    private AnalysisMetadata analysisMeadata;

    private int maturityFile;

//	@OneToOne
//	@JoinColumn(name="cla_file_id")
//	@NotNull
//	private ClaFile claFile;

    public PvAnalysisData(long contentId, final ClaSuperCollection<Long> longHistograms, final AnalysisMetadata analysisMeadata) {
//		this.claFile=claFile;
        this.contentId = contentId;
        this.pvCollections = new FileCollections(longHistograms);
        this.analysisMeadata = analysisMeadata;
    }

    public PvAnalysisData(long contentId, final FileCollections pvCollections, final AnalysisMetadata analysisMeadata) {
        this.contentId = contentId;
//		this.claFile=claFile;
        this.pvCollections = pvCollections;
        this.analysisMeadata = analysisMeadata;
    }


    public PvAnalysisData() {
    }

//	public ClaFile getClaFile() {
//		return claFile;
//	}

    public AnalysisMetadata getAnalysisMeadata() {
        return analysisMeadata;
    }

//	public void setAnalysisMeadata(final AnalysisMetadata analysisMeadata) {
//		this.analysisMeadata = analysisMeadata;
//	}

    public FileCollections getPvCollections() {
        return pvCollections;
    }

//	public void setPvCollections(final FileCollections pvCollections) {
//		this.pvCollections = pvCollections;
//	}

    public ClaSuperCollection<Long> getDefaultPvCollection() {
        if (pvCollections != null && pvCollections.size() == 1) {
            return pvCollections.getPartHistogram(PropertyVector.DEF_SYS_PART);
        }
        return null;
    }


//    public Long getId() {
//        return id;
//    }

    public Long getContentId() {
        return contentId;
    }

    public void setContentId(Long contentId) {
        this.contentId = contentId;
    }

//    public AnalyzeHint getAnalyzeHint() {
//        return analyzeHint;
//    }
//
//    public void setAnalyzeHint(AnalyzeHint analyzeHint) {
//        this.analyzeHint = analyzeHint;
//    }

    public int getMaturityFile() {
        return maturityFile;
    }

    public void setMaturityFile(int maturityFile) {
        this.maturityFile = maturityFile;
    }
}
