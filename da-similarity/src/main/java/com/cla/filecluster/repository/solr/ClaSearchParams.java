package com.cla.filecluster.repository.solr;

/**
 * Created by uri on 07-Jun-17.
 *
 * Sample search query for production
 * join.collection=Extraction&join.q.field=content&join.q.value=62CD275989E78EE5
 * Sample search query for tests
 * join.collection=TestExtraction&join.q.field=content&join.q.value=39F7B9B748D762DF
 *
 */
public interface ClaSearchParams {

    public static String JOIN_COLLECTION = "join.collection";

    /**
     * The value used in the query on the joined core (for example - hashed content)
     */
    public static String JOIN_QUERY_VALUE = "join.q.value";

    /**
     * The field used in the query on the joined core.
     */
    public static String JOIN_QUERY_FIELD = "join.q.field";

    /**
     * Join field on joined collection (join.collection)
     */
    public static String JOIN_FROM_FIELD = "join.from.field";

    /**
     * Join field on queried collection
     */
    public static String JOIN_TO_FIELD = "join.to.field";
}
