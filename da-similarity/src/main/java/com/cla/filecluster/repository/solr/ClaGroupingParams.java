package com.cla.filecluster.repository.solr;

/**
 * Created by uri on 23/06/2015.
 */
public interface ClaGroupingParams {

    /**
     * Group field query
     */
    public static final String GF = "gf";

    public String DEF_SYS_PART="_";	// default value (for word, etc.)

    /**
     * PV Type for a Group field query
     */
    public String PT = "pt";

    /**
     * Origin global count for each field type
     */
    public String OGC = "ogc";

    /**
     * Origin items count for each field type
     */
    public String OIC = "oic";

    /**
     * Original document
     */
    public String OD = "od";

    /**
     * Custom thresholds
     */
    public String TR = "tr";

    /**
     * Document maturity level
     */
    public String ML = "oml";

    /**
     * Document maturity level + force similarity within same ML (makes ML redundant if exists)
     */
    public String FML = "fml";

    /**
     * FileType
     */
    public String FT = "ft";

    /**
     * Document maturity level PV field name
     */
    public String ML_field = "maturity";

    /**
     * Document maturity level FILE PV field name
     */
    public String ML_FILE_field = "maturity_file";


    /**
     * Taken from Query parsing constants
     * Whether to split on whitespace prior to analysis
     */

    String SPLIT_ON_WHITESPACE = "sow";
}
