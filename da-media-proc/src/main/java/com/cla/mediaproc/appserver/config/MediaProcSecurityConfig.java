package com.cla.mediaproc.appserver.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 * spring security for rest interface of media processor
 * Created by yael on 11/13/2017.
 */
@Configuration
@Profile({"prod", "win", "dev", "default"})
public class MediaProcSecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(final HttpSecurity http) throws Exception {
        http.csrf().disable();
        authorizeRequests(http);
    }

    private void authorizeRequests(final HttpSecurity http) throws Exception {
        // Allow access internal localhot requests without auth
        http.authorizeRequests()
                .antMatchers(HttpMethod.GET, "/management/**").access(
                "hasIpAddress('0:0:0:0:0:0:0:1') or hasIpAddress('127.0.0.1/32')")
                .antMatchers(HttpMethod.POST, "/management/**").access(
                "hasIpAddress('0:0:0:0:0:0:0:1') or hasIpAddress('127.0.0.1/32')")
                .antMatchers("/actuator/**").access(
                "hasIpAddress('0:0:0:0:0:0:0:1') or hasIpAddress('127.0.0.1/32')");
    }
}

