package com.cla.mediaproc.appserver;

import com.cla.common.domain.dto.mediaproc.TextualResult;
import com.cla.common.domain.dto.messages.*;
import com.cla.connector.domain.dto.error.ProcessingErrorType;
import com.cla.connector.domain.dto.file.FileType;
import com.cla.connector.utils.Converters;
import com.cla.connector.utils.Pair;
import com.cla.connector.utils.TimeSource;
import com.cla.filecluster.service.IngestRequestHandler;
import com.cla.filecluster.service.IngestRequestHandlerProvider;
import com.cla.mediaproc.appserver.connectors.ResultMessagesHandler;
import com.cla.mediaproc.service.ingest.MediaEntryIngestRequestHandler;
import com.cla.mediaproc.service.ingest.SearchPatternService;
import com.cla.mediaproc.service.managers.MediaProcessorStateService;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.AccessControlException;
import java.util.List;
import java.util.Optional;

/**
 * Service used to handle ingestion requests from the appserver
 * Created by liora on 13/06/2017.
 */
@Service
public class IngestConsumer {

    private final static Logger logger = LoggerFactory.getLogger(IngestConsumer.class);

    @Autowired
    private ResultMessagesHandler appServerCommService;

    @Autowired
    private MediaEntryIngestRequestHandler mediaEntryIngestRequestHandler;

    @Autowired
    private MediaProcessorStateService mediaProcessorStateService;

    @Autowired
    private IngestRequestHandlerProvider ingestRequestHandlerProvider;

    @Autowired
    private SearchPatternService searchPatternService;

    private TimeSource timeSource = new TimeSource();


    void consumeIngestionRequest(IngestRequestMessage requestMessage) {
        if (validateIngestRequest(requestMessage)) {

            try {
                IngestTaskParameters payload = requestMessage.getPayload();

                logger.trace("***** Start Ingest JobId:{} FileID: {} according to: {} ", payload.getJobId(), payload.getFileId(), payload);

                IngestRequestHandler ingestRequestHandler =
                        ingestRequestHandlerProvider.getIngestRequestHandler(requestMessage);
                List<Pair<IngestTaskParameters, IngestMessagePayload>> ingestParamsAndResults =
                        ingestRequestHandler.handleIngestionRequest(requestMessage);

                logger.trace("***** After ingest handling JobId:{} FileId: {} ", payload.getJobId(), payload.getFileId());

                for (Pair<IngestTaskParameters, IngestMessagePayload> ingestParamsAndResult : ingestParamsAndResults) {
                    IngestTaskParameters ingestTaskParams = ingestParamsAndResult.getKey();
                    IngestMessagePayload fileIngestResult = ingestParamsAndResult.getValue();
                    handleFileIngestResult(requestMessage, ingestTaskParams, fileIngestResult);
                }

            } catch (OutOfMemoryError err) {
                sendMediaProcessorIngestErrorResult(
                        requestMessage, requestMessage.getPayload().getFileId(), ProcessingErrorType.ERR_SUSPECT_POISON, err);
                mediaProcessorStateService.incIngestTaskRequestFailure();
                logger.trace("Failed to handle ingest request message. OutOfMemoryError ({})", requestMessage, err);
            } catch (RuntimeException ex) {
                ex.printStackTrace();
                logger.error("Failed to handle ingest request message. ({})", requestMessage, ex);
                ProcessingErrorType errType = ex instanceof AccessControlException ?
                        ProcessingErrorType.ERR_TOO_MANY_REQUESTS : ProcessingErrorType.ERR_UNKNOWN;
                sendMediaProcessorIngestErrorResult(requestMessage, requestMessage.getPayload().getFileId(), errType, ex);
                mediaProcessorStateService.incIngestTaskRequestFailure();
            } catch (Throwable t) {
                logger.error("Failed to handle ingest request message. ({})", requestMessage, t);
                ProcessingErrorType errorType = Optional.ofNullable(t.getMessage()).orElse("")
                        .contains("403 Forbidden") ?
                            ProcessingErrorType.ERR_TOO_MANY_REQUESTS : ProcessingErrorType.ERR_UNKNOWN;
                sendMediaProcessorIngestErrorResult(requestMessage, null, errorType, t);
                mediaProcessorStateService.incIngestTaskRequestFailure();
            }
        } else {
            logger.trace("Failed to handle ingest request message. INVALID REQUEST ({})", requestMessage);
        }
    }

    private void handleFileIngestResult(
            IngestRequestMessage requestMessage, IngestTaskParameters payload, IngestMessagePayload ingestProcessingResult)
            throws JsonProcessingException {

        Long fileId = payload.getFileId();
        if (ingestProcessingResult == null) {
            logger.error("ingest result null for request {}", requestMessage);
            sendMediaProcessorIngestErrorResult(requestMessage, payload.getFileId(), ProcessingErrorType.ERR_UNKNOWN, null);
        } else if (ingestProcessingResult.hasError()) {

            sendFileIngestErrorResult(requestMessage, fileId, ingestProcessingResult);

            if (requestMessage.getRequestId() == null) {
                mediaProcessorStateService.incIngestTaskFinishedWithError();
            }
            logger.trace("***** Finished Ingest with Error JobId:{} FileId: {} ", payload.getJobId(), payload.getFileId());
        } else {
            dispatchIngestResult(requestMessage, payload, fileId, ingestProcessingResult);
            logger.trace("***** Finished Ingest Successfully JobId:{} FileId: {} ", payload.getJobId(), payload.getFileId());
        }
    }

    @SuppressWarnings("unchecked")
    private void dispatchIngestResult(
            IngestRequestMessage requestMessage, IngestTaskParameters payload, Long fileId,
            IngestMessagePayload ingestProcessingResult) throws JsonProcessingException {

        FileType fileType = payload.getFileType();
        IngestResultMessage response = createIngestResultMessage(requestMessage, fileId);
        response.setPayload(ingestProcessingResult);
        response.setFileProperties(ingestProcessingResult.getClaFileProperties());

        // Content metadata -------------------------------------------------------------------
        // if existing content was found - set this content ID and corresponding flag,
        // otherwise set pre-allocated content ID (the appserver will create new content with this ID
        //-------------------------------------------------------------------------------------
        Long contentId = ingestProcessingResult.getClaFileProperties().getContentMetadataId();
        if (contentId == null) {
            contentId = payload.getAllocatedContentId();
        } else {
            response.setDuplicateContentFound(true);
        }
        response.setContentMetadataId(contentId);
		response.setPayloadType(getPayloadType(fileType));
        // calc search patterns and remove open content from result
        String requestId = requestMessage.getRequestId();
        if (ingestProcessingResult instanceof TextualResult) {
            TextualResult res = (TextualResult) ingestProcessingResult;
            res.handleSearchPatterns(requestId, searchPatternService);
        }

        if (requestId != null) {
            response.setRequestId(requestId);
        }

        appServerCommService.sendIngestResult(response);

        if (requestId == null) {
            mediaProcessorStateService.incIngestTaskFinished();
            logger.trace("Finished ingest task {} response message: {}", requestMessage.getTaskId(), response);
        } else {
            logger.trace("Finished ingest request {} response message: {}", requestId, response);
        }
    }

	private boolean validateIngestRequest(IngestRequestMessage requestMessage) {
        IngestTaskParameters payload = requestMessage.getPayload();
        if (payload == null || payload.getMediaConnectionDetailsDto() == null) {
            logger.error("Ingest message request with no parameters, unable to continue handling.");
            sendMediaProcessorIngestErrorResult(requestMessage, null, ProcessingErrorType.ERR_UNKNOWN, null);
            return false;
        }

        Long fileId = payload.getFileId();
        if (mediaEntryIngestRequestHandler.getExitSequenceInitiated()) {
            logger.error("Out of memory exit sequence was initiated, skipping.");
            sendMediaProcessorIngestErrorResult(requestMessage, fileId, ProcessingErrorType.ERR_UNKNOWN, null);
            return false;
        }
        return true;
    }

    private MessagePayloadType getPayloadType(FileType fileType) {
        MessagePayloadType messagePayloadType;
        switch (fileType) {
            case EXCEL:
                messagePayloadType = MessagePayloadType.INGEST_EXCEL;
                break;
            case WORD:
                messagePayloadType = MessagePayloadType.INGEST_WORD;
                break;
            case SCANNED_PDF:
            case PDF:
                messagePayloadType = MessagePayloadType.INGEST_PDF;
                break;
  //          case PACKAGE:
  //              messagePayloadType = MessagePayloadType.INGEST_PACKAGE;
  //              break;
            case PST:
                throw new IllegalArgumentException("MessagePayloadType can't be determined by FileType.PST");
            case OTHER:
            case MAIL:
            default:
                messagePayloadType = MessagePayloadType.INGEST_OTHER;
        }
        return messagePayloadType;
    }

    private IngestResultMessage createIngestResultMessage(IngestRequestMessage message, Long fileId) {
        IngestResultMessage ingestResultMessage = IngestResultMessage.create();
        ingestResultMessage.setTaskId(message.getTaskId());
        ingestResultMessage.setJobId(message.getJobId());
        ingestResultMessage.setRunContext(message.getRunContext());
        ingestResultMessage.setClaFileId(fileId);
        return ingestResultMessage;
    }

    @SuppressWarnings("unchecked")
    private void sendMediaProcessorIngestErrorResult(IngestRequestMessage message, Long fileId, ProcessingErrorType errorType, Throwable e) {
        logger.error("Ingestion failed on request message {}. Notify AppServer", message, e);
        try {

            IngestResultMessage ingestResultMessage = createIngestResultMessage(message, fileId);
            ingestResultMessage.setPayloadType(MessagePayloadType.INGEST_ERROR);
            IngestError ingestError = IngestError.create();
            if (e != null) {
                String errorMessage = e.getClass().getName() + ":" + e.getMessage();
                if (e.getMessage() == null && e.getCause() != null) {
                    errorMessage = e.getCause().getClass().getName() + ":" + e.getCause().getMessage();
                }
                ingestResultMessage.setPayloadStr(errorMessage);

                ingestError.setTimeStamp(timeSource.currentTimeMillis())
                        .setExceptionType(e.getClass().getSimpleName())
                        .setExceptionText(Converters.getExceptionText(e))
                        .setErrorType(errorType);
                ingestResultMessage.addError(ingestError);
            }
            if (message.getRequestId() != null) {
                ingestResultMessage.setRequestId(message.getRequestId());
            }
            ingestResultMessage.addError(ingestError);
            appServerCommService.sendIngestResult(ingestResultMessage);

        } catch (Exception ex) {
            logger.error("Failed to notify AppServer that ingestion of failed", ex);
        }

    }

    @SuppressWarnings("unchecked")
    private void sendFileIngestErrorResult(IngestRequestMessage message, Long fileId, IngestMessagePayload ingestProcessingResult) {
        logger.debug("*****Ingestion failed JobId:{} FileId: {} ", message.getJobId(), fileId);

        try {
            IngestResultMessage ingestResultMessage = createIngestResultMessage(message, fileId);
            ingestResultMessage.setPayloadType(message.getPayloadType());
            ingestResultMessage.setPayload(ingestProcessingResult);
            ingestResultMessage.setFileProperties(ingestProcessingResult.getClaFileProperties());
            IngestError ingestError = IngestError.create();
            ingestError.setTimeStamp(timeSource.currentTimeMillis())
                    .setErrorType(ingestProcessingResult.calculateProcessErrorType());
            ingestError.setText(ingestProcessingResult.calculateProcessErrorType().getText());
            ingestError.setExceptionType(ingestProcessingResult.getExceptionText());
            if (message.getRequestId() != null) {
                ingestResultMessage.setRequestId(message.getRequestId());
            }
            ingestResultMessage.addError(ingestError);
            appServerCommService.sendIngestResult(ingestResultMessage);

        } catch (Exception e1) {
            logger.error("Failed to notify AppServer that ingestion of failed", e1);
        }

    }
}
