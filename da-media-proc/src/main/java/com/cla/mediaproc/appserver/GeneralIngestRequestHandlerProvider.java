package com.cla.mediaproc.appserver;

import com.cla.common.domain.dto.messages.*;
import com.cla.common.domain.pst.PstPath;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.media.MediaConnectionDetailsDto;
import com.cla.connector.domain.dto.media.MediaType;
import com.cla.connector.mediaconnector.AbstractMediaConnector;
import com.cla.filecluster.service.IngestRequestHandler;
import com.cla.filecluster.service.IngestRequestHandlerProvider;
import com.cla.filecluster.service.media.MediaConnectorFactory;
import com.cla.mediaproc.service.ingest.MediaEntryIngestRequestHandler;
import com.cla.mediaproc.service.ingest.OfflineFileIngestRequestHandler;
import com.cla.mediaproc.service.ingest.PstIngestRequestHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.io.FileNotFoundException;


@Component
@Profile("!file_simulator")
public class GeneralIngestRequestHandlerProvider implements IngestRequestHandlerProvider {
	private final static Logger logger = LoggerFactory.getLogger(GeneralIngestRequestHandlerProvider.class);

	@Autowired
	private MediaEntryIngestRequestHandler mediaEntryIngestRequestHandler;

	@Autowired
	private PstIngestRequestHandler pstIngestRequestHandler;

	@Autowired
	private OfflineFileIngestRequestHandler offlineFileIngestRequestHandler;

	@Autowired
	private MediaConnectorFactory mediaConnectorFactory;

	@Value("${ingester.log-offline-files:true}")
	private boolean logOfflineFiles;


	public IngestRequestHandler getIngestRequestHandler(IngestRequestMessage requestMessage) throws FileNotFoundException {
		// Order is important!!
		if (PstPath.isPstEntryPath(requestMessage.getPayload().getMediaEntityId())) {
			return pstIngestRequestHandler;
		} else if (isFileOffLine(requestMessage)) { // This should only be asked if the request is not a packaged file or a PST entry
			if (logOfflineFiles && logger.isDebugEnabled()) {
				logger.debug("File {} id={} has OFFLINE attribute set.", requestMessage.getPayload().getFilePath(), requestMessage.getPayload().getFileId());
			}
			return offlineFileIngestRequestHandler;
		} else {
			return mediaEntryIngestRequestHandler;
		}
	}

	private boolean isFileOffLine(IngestRequestMessage requestMessage) throws FileNotFoundException {
		IngestTaskParameters ingestTaskParameters = requestMessage.getPayload();
		MediaConnectionDetailsDto connectionDetailsDto = ingestTaskParameters.getMediaConnectionDetailsDto();
		if (connectionDetailsDto.getMediaType() == MediaType.FILE_SHARE) {
			AbstractMediaConnector mediaConnector = mediaConnectorFactory.getMediaConnector(ingestTaskParameters.getMediaConnectionDetailsDto());
			String fileSearchIdentifier = resolveFileSearchIdentifier(requestMessage.getPayload());
			//TODO: [Costa] the resulting file attributes should be cached to prevent unnecessary external api calls
			ClaFilePropertiesDto fileAttributes = mediaConnector.getFileAttributes(fileSearchIdentifier);
			return fileAttributes.isOffline();
		} else {
			return false;
		}
	}

	/**
	 * This method is used in order to bridge the gap between media connectors which use the file path as a unique identifier and those who use a custom id (i.e. box).
	 * When the only identifier used by media connectors will be the mediaEntityId, this method will be removed.
	 */
	public static String resolveFileSearchIdentifier(MediaConnectionDetailsProvider mediaConnectionDetailsProvider){
		MediaType mediaType = mediaConnectionDetailsProvider.getMediaConnectionDetailsDto().getMediaType();
		if (!mediaType.isPathAsMediaEntity()) {
			return mediaConnectionDetailsProvider.getMediaEntityId();
		}
		if(mediaConnectionDetailsProvider instanceof IngestTaskParameters){
			return ((IngestTaskParameters) mediaConnectionDetailsProvider).getFilePath();
		}
		if(mediaConnectionDetailsProvider instanceof FileContentTaskParameters){
			return ((FileContentTaskParameters) mediaConnectionDetailsProvider).getFileName();
		}
		throw new RuntimeException("Unhandled instance of media connector details provider: " + mediaConnectionDetailsProvider.getClass().getSimpleName());
	}
}
