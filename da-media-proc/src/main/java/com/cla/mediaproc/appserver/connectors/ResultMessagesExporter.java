package com.cla.mediaproc.appserver.connectors;

import com.cla.common.domain.dto.messages.ExcelIngestResult;
import com.cla.common.domain.dto.messages.IngestResultMessage;
import com.cla.common.domain.dto.messages.WordIngestResult;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Charsets;
import com.google.common.io.CharSink;
import com.google.common.io.Files;
import da.file.simulator.ingest.ExportedIngestResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

/*
* This class acts a wrapper for the AppServerCommService, intercepting the ingest results and exporting them to a text file
* in a specified folder. The normal flow of ingest then resumes.
* */
@Service
@ConditionalOnProperty(prefix = "ingest.result.messages",name = "export-enable", havingValue = "true")
public class ResultMessagesExporter  extends AppServerCommService {
	private static final Logger logger = LoggerFactory.getLogger(ResultMessagesExporter.class);
	static ObjectMapper mapper = new ObjectMapper();
	private String exportPath;

	public ResultMessagesExporter(@Value("${ingest.result.messages.export-dir}") String exportPath) {
		this.exportPath = exportPath;
		createFolderIfNotExist(exportPath);
	}

	@Override
	public void sendIngestResult(IngestResultMessage ingestResultMessage) throws JsonProcessingException {
		try {
			logger.debug("exporting IngestResultMessage for file " + ingestResultMessage.getFileProperties().getFileName() );
			ExportedIngestResult exportedIngestResult = resolveExportedIngestResult(ingestResultMessage);
			saveToFile(exportedIngestResult);
		} catch (Exception e) {
			logger.error("can not save exported result", e);
		}
		super.sendIngestResult(ingestResultMessage);
	}

	private ExportedIngestResult resolveExportedIngestResult(IngestResultMessage ingestResultMessage) {
		ExportedIngestResult result = new ExportedIngestResult();
		Map<String,Object> stats = new HashMap<>();
		switch(ingestResultMessage.getPayloadType()){
			case INGEST_EXCEL:
				ExcelIngestResult excelIngestResult = (ExcelIngestResult) ingestResultMessage.getPayload();
				stats.put("numberOfSheets", excelIngestResult.getSheetsMetadata().getSheetsMetadata().size());
				stats.put("numberOfCells", excelIngestResult.getSheetsMetadata().getSheetsMetadata()
						.values().stream().mapToInt(md->md.getNumOfCells()).sum());
				stats.put("grossPvSize",excelIngestResult.getFileCollections().getCollections()
						.values().stream().flatMap(c->c.getCollections().values().stream()).mapToInt(i->i.getGlobalCount()).sum());
				break;
			case INGEST_PDF:
			case INGEST_WORD:
				WordIngestResult wordIngestResult = (WordIngestResult) ingestResultMessage.getPayload();
				stats.put("grossPvSize",wordIngestResult.getLongHistograms().getCollections()
						.values().stream().flatMap(c->c.getCollection().keySet().stream()).count());
				break;

			default:
				return null;
		}
		result.setIngestResultMessage(ingestResultMessage);
		result.setStatistics(stats);
		return result;
	}

	private void saveToFile(ExportedIngestResult exportedIngestResult) throws IOException {
		if(exportedIngestResult == null ) {
			return;
		}
		IngestResultMessage ingestResultMessage = exportedIngestResult.getIngestResultMessage();
		String ingestResultJson = serialize(exportedIngestResult);
		String fileName = Paths.get(ingestResultMessage.getFileProperties().getFileName()).getFileName().toString();
		File file = new File(Paths.get( exportPath, fileName + ".json").toString());
		CharSink sink = Files.asCharSink(file, Charsets.UTF_8);
		sink.write(ingestResultJson);
	}

	private void createFolderIfNotExist(String path){
		File directory = new File(path);
		if (! directory.exists()){
			directory.mkdirs();
		}
	}

	private static String serialize(ExportedIngestResult exportedIngestResult){
		try {
			String valueAsString = mapper.writeValueAsString(exportedIngestResult);
			return valueAsString;
		} catch (JsonProcessingException e) {
			logger.error("can not serialize exported result ", e);
		}
		return null;
	}

}
