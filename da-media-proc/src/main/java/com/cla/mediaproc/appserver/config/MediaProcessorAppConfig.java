package com.cla.mediaproc.appserver.config;

import com.cla.common.media.files.pdf.ClaNativePdfExtractor;
import com.cla.common.media.files.pdf.ClaPdfExtractor;
import com.cla.common.media.files.word.ClaWord6Extractor;
import com.cla.common.media.files.word.ClaWordExtractor;
import com.cla.common.media.files.word.ClaWordXmlExtractor;
import com.cla.connector.domain.dto.file.FileType;
import com.cla.connector.utils.FileTypeUtils;
import com.cla.eventbus.EventBus;
import com.cla.eventbus.vertx.VertxEventBus;
import com.cla.filecluster.util.executors.BlockingExecutor;
import com.cla.filecluster.util.executors.ClientThreadBlockingExecutor;
import com.cla.filecluster.util.executors.ThreadPoolBlockingExecutor;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import org.apache.tika.Tika;
import org.apache.tika.config.TikaConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.retry.backoff.ExponentialBackOffPolicy;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Configuration
public class MediaProcessorAppConfig {
    private static final Logger logger = LoggerFactory.getLogger(MediaProcessorAppConfig.class);

    @Value("${utilBlockingExecutor.poolSize:10}")
    private int utilBlockingExecutorPoolSize;

    @Value("${utilBlockingExecutor.QueueSize:15}")
    private int utilBlockingExecutorQueueSize;


	@Value("${ingestBlockingExecutor.poolSize:10}")
	private int ingestBlockingExecutorPoolSize;
	
	@Value("${ingestBlockingExecutor.QueueSize:15}")
	private int ingestBlockingExecutorQueueSize;

    @Value("${ingestPackageBlockingExecutor.poolSize:10}")
    private int ingestPackageBlockingExecutorPoolSize;

    @Value("${ingestPackageBlockingExecutor.QueueSize:15}")
    private int ingestPackageBlockingExecutorQueueSize;

    @Value("${ngestPackageCloseBlockingExecutor.poolSize:10}")
    private int packageCloseBlockingExecutorPoolSize;

    @Value("${ngestPackageCloseBlockingExecutor.QueueSize:15}")
    private int packageCloseBlockingExecutorQueueSize;

    @Value("${startScanBlockingExecutor.poolSize:1}")
    private int startScanBlockingExecutorPoolSize;

    @Value("${startScanBlockingExecutor.QueueSize:0}")
    private int startScanBlockingExecutorQueueSize;


    @Value("${analyzeBlockingExecutor.poolSize:10}")
    private int analyzeBlockingExecutorPoolSize;

    @Value("${analyzeBlockingExecutor.queueSize:15}")
    private int analyzeBlockingExecutorQueueSize;

    @Value("${ingest.others.tikaConfig:}")
    private String tikeConfigFilename;

    @Value("${threadPool.idleTimeBeforeShutdown:1}")
    private long idleTimeBeforeShutdown;

    @Value("${maxStringLengthToExtract:50000}")
    private int maxStringLength;

    @Value("${pv.dbOperationsRetryCount:5}")
    private int dbOperationsRetryCount;

    @Value("${pv.dbOperationsSleepSec:2}")
    private int dbOperationsSleepSec;

    @Value("${useThreadPoolExecutors:true}")
    private boolean useThreadPoolExecutors;

    @Value("${alternative.name.override:false}")
    private boolean overrideWithAlternativeName;

    @Value("${app.displayString:DocAuthority MediaProcessor}")
    private String appDisplayString;

    @Value(("${file.support-additional-xls:true}"))
    private boolean supportAdditionalExcelFiles;

    @Value("${ingester.package.enabled:true}")
    private boolean ingestPackageEnabled;

    @Value("${ingester.package.maxsize:100000000}")
    private long ingesterPackageMaxSize;

    @Value("${event-bus.consumers.concurrency:15}")
    private int consumersConcurrency;

    @Value("${event-bus.sendTimeout:10000}")
    private long sendTimeout;

    @Autowired
    private ApplicationContext context;

    @PostConstruct
    private void init() {
        if (!useThreadPoolExecutors) {
            logger.warn("==============================================================================");
            logger.warn("| THE APPLICATION IS SINGLE THREADED. THIS SHOULD BE USED ONLY FOR DEBUG !!! |");
            logger.warn("==============================================================================");
        }
        logger.warn("{} Loading profiles : {}", appDisplayString, Arrays.asList(context.getEnvironment().getActiveProfiles()));

        if (supportAdditionalExcelFiles) {
            FileTypeUtils.addExtension("xlt", FileType.EXCEL);
            FileTypeUtils.addExtension("xlsm", FileType.EXCEL);
            FileTypeUtils.addExtension("xltx", FileType.EXCEL);
        }
    }

    @Bean
    public EventBus eventBus() {
        final ExecutorService consumersExecutor = Executors.newFixedThreadPool(
                consumersConcurrency,
                new ThreadFactoryBuilder().setNameFormat("eb-consumer-%d").build());
        return new VertxEventBus(sendTimeout, consumersExecutor);
    }

    @Bean
    public BlockingExecutor ingestBlockingExecutor() {
        if (useThreadPoolExecutors) {
            return new ThreadPoolBlockingExecutor("ingest", ingestBlockingExecutorPoolSize, ingestBlockingExecutorQueueSize, idleTimeBeforeShutdown, true);
        }
        return new ClientThreadBlockingExecutor();
    }

    @Bean
    public BlockingExecutor ingestPackageBlockingExecutor() {
        if (useThreadPoolExecutors) {
            return new ThreadPoolBlockingExecutor("ingestPackage", ingestPackageBlockingExecutorPoolSize, ingestPackageBlockingExecutorQueueSize, idleTimeBeforeShutdown, true);
        }
        return new ClientThreadBlockingExecutor();
    }

    @Bean
    public BlockingExecutor packageCloseBlockingExecutor() {
        if (useThreadPoolExecutors) {
            return new ThreadPoolBlockingExecutor("packageClose", packageCloseBlockingExecutorPoolSize, packageCloseBlockingExecutorQueueSize, idleTimeBeforeShutdown, true);
        }
        return new ClientThreadBlockingExecutor();
    }

    @Bean
    public BlockingExecutor startScanBlockingExecutor(){
    	if(useThreadPoolExecutors) {
			return new ThreadPoolBlockingExecutor("start_scan", startScanBlockingExecutorPoolSize, startScanBlockingExecutorQueueSize, idleTimeBeforeShutdown, true);
		}
    	return new ClientThreadBlockingExecutor();
    }



    @Bean
    public BlockingExecutor utilBlockingExecutor() {
        if (useThreadPoolExecutors) {
            return new ThreadPoolBlockingExecutor("util", utilBlockingExecutorPoolSize, utilBlockingExecutorQueueSize, idleTimeBeforeShutdown, true);
        }
        return new ClientThreadBlockingExecutor();
    }

//    @Bean
//    public BlockingExecutor extractionBlockingExecutor(){
//		if(useThreadPoolExecutors) {
//			return new ThreadPoolBlockingExecutor("extraction", utilBlockingExecutorPoolSize, utilBlockingExecutorQueueSize,idleTimeBeforeShutdown);
//		}
//    	return new ClientThreadBlockingExecutor();
//    }

    @Bean
    public TaskScheduler defaultScheduler() {
        ThreadPoolTaskScheduler taskScheduler = new ThreadPoolTaskScheduler();
        taskScheduler.setDaemon(true);
        taskScheduler.setThreadNamePrefix("da-scheduler");
        taskScheduler.setPoolSize(10);
        return taskScheduler;
    }

    @Bean
    public Tika tika() {
        TikaConfig tikaConfig = TikaConfig.getDefaultConfig();
        if (tikeConfigFilename != null && !tikeConfigFilename.isEmpty()) {
            try {
                TikaConfig daTikaConfig = new TikaConfig(tikeConfigFilename);
                tikaConfig = daTikaConfig;
                logger.info("TikaConfig overridden with elements in file {}", tikeConfigFilename);
            } catch (Exception e) {
                logger.warn("Exception while creating custom TikaConfig from file {}. {}", tikeConfigFilename, e.getMessage());
            }
        }
        final Tika tika = new Tika(tikaConfig);
        tika.setMaxStringLength(maxStringLength);
        return tika;
    }


    ////////////////////////////////////////////////////////////////
    @Bean
    @Scope("prototype")
    public ClaWord6Extractor claWord6Extractor() {
        return new ClaWord6Extractor();
    }

    @Bean
    @Scope("prototype")
    public ClaWordXmlExtractor claWordXmlExtractor() {
        return new ClaWordXmlExtractor();
    }

    @Bean
    public ClaWordExtractor claWordExtractor() {
        return new ClaWordExtractor() {
            @Override
            protected ClaWord6Extractor createClaWord6Extractor() {
                return claWord6Extractor();
            }

            @Override
            protected ClaWordXmlExtractor createClaWordXmlExtractor() {
                return claWordXmlExtractor();
            }

        };
    }

    @Bean
    @Scope("prototype")
    public ClaNativePdfExtractor claNativePdfExtractor() {
        return new ClaNativePdfExtractor();
    }

    @Bean
    public ClaPdfExtractor claPdfExtractor() {
        return new ClaPdfExtractor() {
            @Override
            protected ClaNativePdfExtractor createClaPdfExtractor() {
                return claNativePdfExtractor();
            }

        };
    }


}