package com.cla.mediaproc.appserver;

import com.cla.connector.progress.ProgressTracker;
import com.cla.connector.utils.TimeSource;

import java.util.function.Consumer;

/**
 * An implementation of {@link ProgressTracker} that takes a reporter callback to report of the amount of progress made.
 *
 * Created by Itai Marko.
 */
public class ScanProgressReporter implements ProgressTracker {

    private TimeSource timeSource = new TimeSource();
    private long lastReportTime;
    private final long rateInMillis;
    private final Consumer<Long> reporter;

    private long totalProgressMade = 0L;
    private long progressMadeSinceLastUpdate = 0L;


    ScanProgressReporter(long startTime, long rateInMillis, Consumer<Long> reporter) {
        this.lastReportTime = startTime;
        this.rateInMillis = rateInMillis;
        this.reporter = reporter;
    }

    @Override
    public void startStageTracking(int stageWeight) {
        // nothing to do here
    }

    @Override
    public void setCurStageEstimatedTotal(long estimateTotal) {
        // nothing to do here
    }

    @Override
    public boolean incrementProgress(long amount) {
        if (amount < 0) {
            throw new IllegalArgumentException("Negative progress amount: " + amount);
        }
        boolean progressReported = false;
        totalProgressMade = Math.addExact(totalProgressMade, amount);
        progressMadeSinceLastUpdate = Math.addExact(progressMadeSinceLastUpdate, amount);
        if (timeSource.millisSince(lastReportTime) >= rateInMillis) {
            reportProgress();
            progressReported = true;
        }
        return progressReported;
    }

    @Override
    public void endCurStage() {
        reportProgress();
    }

    @Override
    public void endTracking() {
        reportProgress();
    }

    private void reportProgress() {
        // total (cumulative) number of files in the current task, 
        // which were mapped but not sent to the AppServer
        // because they have not changed since the last map
        reporter.accept(totalProgressMade);
        progressMadeSinceLastUpdate = 0L;
        lastReportTime = timeSource.currentTimeMillis();
    }

    public long getTotalProgressMade() {
        return totalProgressMade;
    }
}
