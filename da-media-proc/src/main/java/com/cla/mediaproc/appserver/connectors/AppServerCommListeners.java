package com.cla.mediaproc.appserver.connectors;

import com.cla.common.domain.dto.messages.*;
import com.cla.common.domain.dto.messages.control.ControlRequestMessage;
import com.cla.common.utils.DaMessageConversionUtils;
import com.cla.connector.utils.TextUtils;
import com.cla.common.utils.thread.ExecutorUtil;
import com.cla.connector.utils.TimeSource;
import com.cla.filecluster.util.executors.BlockingExecutor;
import com.cla.mediaproc.appserver.AppServerProcessingService;
import com.cla.mediaproc.appserver.JobStateMonitor;
import com.cla.mediaproc.appserver.LabelingConsumer;
import com.cla.mediaproc.service.managers.MediaProcessorStateService;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.config.SimpleJmsListenerEndpoint;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.listener.DefaultMessageListenerContainer;
import org.springframework.stereotype.Component;
import org.springframework.util.ErrorHandler;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.jms.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;

/**
 * listeners configuration
 * Created by vladi on 2/1/2017.
 */
@Component
public class AppServerCommListeners implements ExceptionListener {

    private static final Logger logger = LoggerFactory.getLogger(AppServerCommListeners.class);

    @Autowired
    private AppServerProcessingService appServerProcessingService;

    @Autowired
    private JmsTemplate mpJmsTemplate;

    @Autowired
    private BlockingExecutor ingestBlockingExecutor;

    @Autowired
    private BlockingExecutor utilBlockingExecutor;

    @Autowired
    private JobStateMonitor jobStateMonitor;

    @Autowired
    private LabelingConsumer labelingConsumer;

    @Autowired
    private MediaProcessorStateService mediaProcessorStateService;

    @Autowired
    private Map<String, Object> consumerConfigs;

    @Autowired
    private ConnectionFactory connectionFactory;

    @Value("${control.requests.broadcast.topic}")
    private String controlRequestsBroadcastTopic;

    @Value("${realtime.requests.broadcast.topic:realtime.requests.broadcast}")
    private String realtimeRequestsBroadcastTopic;

    @Value("${scan.requests.topic}")
    private String scanRequestTopic;

    @Value("${ingest.requests.topic}")
    private String ingestRequestsTopic;

    @Value("${realtime.requests.topic}")
    private String realtimeRequestsTopic;

    @Value("${labeling.requests.topic}")
    private String labelingRequestsTopic;

    @Value("${comm.debug.dump-size:64}")
    private int dataLoggingSize;

    @Autowired
    private BlockingExecutor startScanBlockingExecutor;

    @Autowired
    private DefaultJmsListenerContainerFactory mdQueueListenerFactory;

    @Value("${startScanBlockingExecutor.QueueSize:0}")
    private int startScanBlockingExecutorQueueSize;

    @Value("${spring.activemq.broker-url}")
    private String messageBrokerUrl;

    @Value("${content-check-executor.pool-size:5}")
    private int contentCheckExecutorPoolSize;

    @Value("${content-check-executor.queue-size:50}")
    private int contentCheckExecutorQueueSize;

    @Value("${scan.request.concurrency:1}")
    private int scanConcurrentConsumers;

    @Value("${ingest.request.concurrency:1}")
    private int ingestConcurrentConsumers;

    @Value("${realtime.request.concurrency:1}")
    private int realtimeConcurrentConsumers;

    @Value("${labeling.request.concurrency:1}")
    private int labelingConcurrentConsumers;

    @Value("${labeling-executor.pool-size:5}")
    private int labelingExecutorPoolSize;

    @Value("${labeling-executor.queue-size:50}")
    private int labelingExecutorQueueSize;

    private Long dataCenterId;

    private List<DefaultMessageListenerContainer> listeners = new ArrayList<>();

    private ExecutorService contentCheckBlockingExecutor;

    private ExecutorService labelingBlockingExecutor;

    private static ObjectMapper mapper;

    private boolean active = false;

    private TimeSource timeSource = new TimeSource();

    @PostConstruct
    private void init() {
        mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        contentCheckBlockingExecutor = ExecutorUtil.newFixedThreadPoolWithQueueSize(contentCheckExecutorPoolSize, contentCheckExecutorQueueSize);
        labelingBlockingExecutor = ExecutorUtil.newFixedThreadPoolWithQueueSize(labelingExecutorPoolSize, labelingExecutorQueueSize);
    }

    @PreDestroy
    private void destroy(){
        clearListeners();
        contentCheckBlockingExecutor.shutdown();
        labelingBlockingExecutor.shutdown();
    }

    private void createListener(String topic, MessageListener messageListenerHandler, int concurrentConsumers) {
        //See http://activemq.apache.org/multiple-consumers-on-a-queue.html
        //If you want to consume concurrently from a queue, then you must use a different session for each consumer.

        try {
            SimpleJmsListenerEndpoint jmsListenerEndpoint = new SimpleJmsListenerEndpoint();
            jmsListenerEndpoint.setMessageListener(messageListenerHandler); //TODO Liora: verify onMessage is synced
            DefaultMessageListenerContainer listener = mdQueueListenerFactory.createListenerContainer(jmsListenerEndpoint);

            listener.setDestinationName(topic);
            listener.setConcurrentConsumers(concurrentConsumers);

            listener.afterPropertiesSet();
            listener.start();
            listener.setErrorHandler( new ErrorHandler() {
                @Override
                public void handleError(Throwable t) {
                    logger.error("!!!!!!!!!!!!!!!!!!!!! Error in listener {}", topic, t);
                }
            });
            listeners.add(listener);

        } catch (Exception e) {
            logger.error("Failed to create listeneter for {} ",topic);

        }
    }

    private void clearListeners() {
        logger.warn("Shutting down {} jms listeners.", listeners.size());

        listeners.forEach(l -> {
            try {
                l.shutdown();
            } catch (Exception e) {
                logger.error("Failed to clear listeners for dataCenterId {} topic: {} ", dataCenterId, l.getDestinationName());
            }
        });
        listeners.clear();

    }

    public synchronized void deactivateListeners(){
        this.active = false;
        clearListeners();
    }

    public synchronized void redirectListeningEndPoint(Long dataCenterId, boolean requirePrefix) {
        if (!this.active  || !this.dataCenterId.equals(dataCenterId) ) {
            clearListeners();
            logger.warn("Redirecting MP listening point to dataCenter id {}. (prev active={}, prev dcId={})", dataCenterId, this.active, this.dataCenterId);

            String prefix = requirePrefix ? dataCenterId + "_" : "";
            createListener(prefix + ingestRequestsTopic, new MessageListener() {
                @Override
                public void onMessage(Message message) {
                    try {

                        if (message instanceof TextMessage) {
                            TextMessage textMessage = (TextMessage) message;
                            String text = textMessage.getText();
                            String s = mapper.readValue(text, String.class);  //Due to double serialization
                            ingestListener(s);
                        }
                    } catch (Exception e) {
                        logger.error("Failed to receive message from queue  {} ", prefix + ingestRequestsTopic, e);
                    }

                }
            }, ingestConcurrentConsumers);

            createListener(prefix + scanRequestTopic, new MessageListener() {
                @Override
                public void onMessage(Message message) {
                    try {
                        TextMessage textMessage = (TextMessage) message;
                        String text = textMessage.getText();
                        String s = mapper.readValue(text, String.class);
                        scanListener(s);
                    } catch (Exception e) {
                        logger.error("Failed to receive message from queue  {} ", prefix + scanRequestTopic, e);
                    }
                }
            }, scanConcurrentConsumers);

            createListener(prefix + labelingRequestsTopic, new MessageListener() {
                @Override
                public void onMessage(Message message) {
                    try {
                        TextMessage textMessage = (TextMessage) message;
                        String text = textMessage.getText();
                        String s = mapper.readValue(text, String.class);
                        labelingListener(s);
                    } catch (Exception e) {
                        logger.error("Failed to receive message from queue  {} ", prefix + labelingRequestsTopic, e);
                    }
                }
            }, labelingConcurrentConsumers);

            createListener(prefix + realtimeRequestsTopic, new MessageListener() {
                @Override
                public void onMessage(Message message) {
                    try {
                        TextMessage textMessage = (TextMessage) message;
                        String text = textMessage.getText();
                        String s = mapper.readValue(text, String.class);
                        realtimeListener(s);
                    } catch (Exception e) {
                        logger.error("Failed to receive message from queue  {} ", prefix + realtimeRequestsTopic, e);
                    }
                }
            }, realtimeConcurrentConsumers);

            if (dataCenterId != null) {
                this.dataCenterId = dataCenterId;
            }
            if (this.dataCenterId == null) {
                logger.warn("No datacenter is set. Forcing active=false");
                this.active = true;
            } else {
                this.active = true;
            }
        }
    }

    public void scanListener(String data) {
        String obfuscatedData = null;
        try {
            if (logger.isInfoEnabled()) {
                obfuscatedData = TextUtils.obfuscatePassword(data);
                logger.info("Receiving start scan task {}", obfuscatedData);
            }
            if ("test".equals(data)) {
                logger.warn("TEST MESSAGE RECEIVED");
                return;
            }
            mediaProcessorStateService.resetScanStartStats();
            mediaProcessorStateService.incStartScanTaskRequested();
            long activeCount = mediaProcessorStateService.incActiveScanTasks();

            startScanBlockingExecutor.execute(() -> {
                ScanRequestMessage scanRequestMessage = DaMessageConversionUtils.Scan.deserializeRequestMessage(data);
                appServerProcessingService.consumeStartScanRequest(scanRequestMessage);
            });
            logger.info("Scan task of RF {} has started. {} active scans in progress", obfuscatedData, activeCount);
            if (startScanBlockingExecutorQueueSize == 0) {
                startScanBlockingExecutor.blockUntilProcessingFinished();
            }
        } catch (Exception e) {
            logger.error("Failed to handle control request message.", e);
        }
    }

    private void ingestListener(String data) {
        if (logger.isTraceEnabled()) {
            logger.trace("Receiving ingest task ({} bytes in request) ", TextUtils.obfuscatePassword(data));
        }
        mediaProcessorStateService.incIngestTaskRequested();
        ingestBlockingExecutor.execute(() -> {

            if (logger.isTraceEnabled()) {
                logger.trace("Receiving ingest task and start handling {}", TextUtils.obfuscatePassword(data));
            }

            IngestRequestMessage requestMessage = DaMessageConversionUtils.Ingest.deserializeRequestMessage(data);

            if (logger.isTraceEnabled()) {
                logger.trace("Receiving ingest task and deserialized {}", requestMessage);
            }

            long currTime = timeSource.currentTimeMillis();
            if (requestMessage.getExpirationTime() != null && requestMessage.getExpirationTime() > 0 &&
                    requestMessage.getExpirationTime() < currTime) {
                logger.info("Received expired ingest request ({} sec late) - ignore it {}",
                        timeSource.secondsSince(requestMessage.getExpirationTime()), requestMessage);
            } else {
                appServerProcessingService.consumeIngestRequest(requestMessage);
            }
        });
    }

    @JmsListener(destination = "${contentcheck.responses.broadcast.topic:contentcheck.responses.broadcast}",
            concurrency = "${contentcheck.concurrency:5}", containerFactory = "mdBroadcastListenerFactory")
    public void contentCheckBroadcastListener(String data) {
        logger.info("Receiving content check response ({} bytes in request) ", data);
        contentCheckBlockingExecutor.execute(() -> {
            ContentMetadataResponseMessage res = DaMessageConversionUtils.ContentCheck.deserializeResultMessage(data);
            if (res.getMediaProcessorId()== null ||
                    res.getMediaProcessorId().equals(appServerProcessingService.getMediaProcessorInstanceId())) {
                logger.debug("content check response will be handled {}", data);
                appServerProcessingService.consumeContentCheckResponse(res);
            }
        });
    }

    @JmsListener(destination = "${control.requests.broadcast.topic:control.requests.broadcast}",
            concurrency = "${control.requests.concurrency:1}", containerFactory = "mdBroadcastListenerFactory")
    public void controlBroadcastListener(String data) {
        try {
            if (logger.isTraceEnabled()) {
                logger.debug("Receive control request via broadcast. len={}: {}", data.length(), data);
            } else if (logger.isDebugEnabled()) {
                    logger.debug("Receive control request via broadcast len={} msg-start={}...", data.length(), data.substring(0, Integer.min(dataLoggingSize, data.length())));
            }
            ControlRequestMessage requestMessage = DaMessageConversionUtils.Control.deserializeRequestMessage(data);

            if (requestMessage.getMediaProcessorId()== null ||
                    requestMessage.getMediaProcessorId().equals(appServerProcessingService.getMediaProcessorInstanceId())) {
                logger.debug("Control request will be handled {}", data);
                appServerProcessingService.consumeControlRequest(requestMessage);
            }
        } catch (Exception e) {
            logger.error("Failed to handle control request message.", e);
        }
    }

    @JmsListener(destination = "${realtime.requests.broadcast.topic:realtime.requests.broadcast}",
            concurrency = "${realtime.requests.concurrency:1}", containerFactory = "mdBroadcastListenerFactory")
    public void realtimeBroadcastListener(String data) {
        realtimeListener(data);
    }

    public void labelingListener(String data) {
        try {
            labelingBlockingExecutor.execute(() -> {
                try {
                    LabelRequestMessage labelRequestMessage = DaMessageConversionUtils.Labeling.deserializeRequestMessage(data);
                    labelingConsumer.handleLabelingRequest(labelRequestMessage);
                } catch (Exception ex) {
                    logger.error("Failed to handle labeling request message.", ex);
                }
            });
        } catch (Exception e) {
            logger.error("Failed to handle labeling request message.", e);
        }
    }

    public void realtimeListener(String data) {
        try {
            utilBlockingExecutor.execute(() -> {
                try {
                    FileContentRequestMessage fileContentRequestMessage = DaMessageConversionUtils.FileContent.deserializeRequestMessage(data);
                    if(fileContentRequestMessage.getPayloadType() != MessagePayloadType.LIST_FOLDERS_REQUEST) {
						appServerProcessingService.consumeFileContentRequest(fileContentRequestMessage);
					}else{
                    	appServerProcessingService.consumeListFoldersRequest(fileContentRequestMessage.getPayload());
					}

                } catch (IOException ex) {
                    logger.error("Failed to handle file content request message.", ex);
                }
            });
        } catch (Exception e) {
            logger.error("Failed to handle control request message.", e);
        }
    }

    @Override
    public void onException(JMSException exception) {
        logger.error("JMSException", exception);
    }

}
