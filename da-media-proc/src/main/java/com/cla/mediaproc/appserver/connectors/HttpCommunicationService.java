package com.cla.mediaproc.appserver.connectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

/**
 * Created by liora on 14/06/2017.
 */
@Service
public class HttpCommunicationService {

    private final static Logger logger = LoggerFactory.getLogger(HttpCommunicationService.class);

    @Value("${appserver.lenient-mode.retries:3}")
    private int lenientModeRetries;

    @Value("${appserver.lenient-mode.sleep:10000}")
    private long lenientModeSleep;

    @Value("${appserver.authcookie.cache.expire:120000}")
    private long authCookieCacheExpiry;

    @Value("${appserver.authentication.username:mp_service}")
    private String mpAppserverAuthenticationUser;

    @Value("${appserver.authentication.password:password}")
    private String mpAppserverAuthenticationPassword;

    private String authCookieCache;
    private long authCookieLastUse;

    private final Object authCookieLock = new Object();

    public <T> T sendRestCallWithRetries(String url,String loginUrl, Class<T> responseType) {
        RuntimeException lastException = null;
        for (int i = 0; i < lenientModeRetries; i++) {
            try {
                return sendRestCall(url, loginUrl, responseType);
            } catch (RuntimeException e) {
                logger.warn("Failed to check for existing content in lenient mode. Sleep for {} and try again ({})", lenientModeSleep, e);
                try {
                    Thread.sleep(lenientModeSleep);
                } catch (InterruptedException e1) {
                    logger.error("Check for existing content stopped by user",e1);
                }
                lastException = e;
            }
        }
        if (lastException != null) {
            logger.warn("Failed to check for existing content: {}", lastException);
            throw lastException;
        } else {
            throw new RuntimeException("Failed to check for existing content");
        }
    }

    public <T> T sendRestCall(String url, String loginUrl, Class<T> responseType) {
        logger.info("Sending http call to url: ({})",url);
        try {
//            String authCookie = getAuthCookie(loginUrl);
//            RestTemplate restTemplate = new FormAuthenticatedRestTemplate(authCookie);
            RestTemplate restTemplate = new RestTemplate();
            return restTemplate.getForObject(url, responseType);
        } catch (ResourceAccessException e) {
            throw new RuntimeException("Resource Access exception while sending rest call to remote file processor at URL: " + url, e);
        } catch (HttpClientErrorException e) {
            logger.warn("Unexpected error while trying to call file processor:\n ..: {}", e.getResponseBodyAsString(), e);
            if (e.getStatusCode() != null && e.getStatusCode().is4xxClientError()) {
                logger.warn("Got error code {} from remote file processor. Recreating connection", e.getStatusCode());
                resetAuthCookie();
            }
            throw e;
        }
    }

    @Deprecated
    private String getAuthCookie(String loginUrl) {
        return getAuthCookie(mpAppserverAuthenticationUser, mpAppserverAuthenticationPassword, loginUrl);
    }

    @Deprecated
    /*
     * Shoudl be replaced with basic authentication access over the API request URL - no need for separate 'login' call.
     */
    private String getAuthCookie(final String username, final String password, String loginUrl) {
        synchronized (authCookieLock) {
            if (authCookieCache != null && System.currentTimeMillis() - authCookieLastUse < authCookieCacheExpiry) {
                authCookieLastUse = System.currentTimeMillis();
                return authCookieCache;
            }
            final HttpHeaders requestHeaders = new HttpHeaders();
            requestHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
            final HttpEntity<String> requestEntity = new HttpEntity<>("username=" + username + "&password=" + password, requestHeaders);
            final ResponseEntity<String> rs = new RestTemplate().exchange(loginUrl, HttpMethod.POST, requestEntity, String.class);
            if (!rs.getStatusCode().equals(HttpStatus.OK)) {
                throw new RuntimeException("Remote appserver - Login failed: " + rs.getStatusCode());
            }
            final String authCookie = rs.getHeaders().getFirst("Set-Cookie").split(" ")[0];
            authCookieCache = authCookie;
            authCookieLastUse = System.currentTimeMillis();
            return authCookie;
        }
    }

    private void resetAuthCookie() {
        synchronized (authCookieLock) {
            logger.debug("Reset Auth cookie");
            authCookieCache = null;
            authCookieLastUse = 0;
        }
    }
}
