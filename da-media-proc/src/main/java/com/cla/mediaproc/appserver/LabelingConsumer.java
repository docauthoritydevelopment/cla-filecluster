package com.cla.mediaproc.appserver;

import com.cla.common.domain.dto.messages.LabelRequestMessage;
import com.cla.common.domain.dto.messages.LabelTaskParameters;
import com.cla.common.domain.dto.messages.LabelingError;
import com.cla.common.domain.dto.messages.LabelingResponseMessage;
import com.cla.connector.domain.dto.error.ProcessingErrorType;
import com.cla.connector.mediaconnector.labeling.mip.MipMediaConnector;
import com.cla.filecluster.service.media.MediaConnectorFactory;
import com.cla.mediaproc.actions.labeling.LabelingAction;
import com.cla.mediaproc.actions.labeling.mip.MipLabelingAction;
import com.cla.mediaproc.appserver.connectors.ResultMessagesHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * Created by: yael
 * Created on: 7/10/2019
 */
@Service
public class LabelingConsumer {

    private final static Logger logger = LoggerFactory.getLogger(LabelingConsumer.class);

    @Autowired
    private ResultMessagesHandler appServerCommService;

    @Autowired
    private MediaConnectorFactory mediaConnectorFactory;

    @Value("${labeling.write.target-path:}")
    private String targetPathForLabelWrite;

    public void handleLabelingRequest(LabelRequestMessage labelRequestMessage) {
        try {
            LabelTaskParameters payload = labelRequestMessage.getPayload();
            LabelingAction action = createAction(payload);

            LabelingResponseMessage labelingResponseMessage;

            logger.debug("received label {} request for file {} vendor {} label {}", payload.getRequestType(),
                    payload.getFileId(), payload.getLabelingVendor(), payload.getNativeId());

            switch (payload.getRequestType()) {
                case READ_LABELS:
                    labelingResponseMessage = action.retrieveLabel(labelRequestMessage);
                    break;
                case WRITE_LABEL:
                    labelingResponseMessage = action.applyLabel(labelRequestMessage);
                    break;
                default:
                    throw new RuntimeException("Unknown label request type.");
            }

            appServerCommService.sendLabelingResponse(labelingResponseMessage);
        } catch (Exception e) {
            logger.error("failed handling labelRequestMessage {}", labelRequestMessage, e);
            handleLabelingError(labelRequestMessage, e.getMessage());
        }
    }

    private void handleLabelingError(LabelRequestMessage labelRequestMessage, String errorMsg) {
        LabelingResponseMessage response = new LabelingResponseMessage(labelRequestMessage.getPayload());
        LabelingError responseError = new LabelingError();
        responseError.setExceptionText(errorMsg);
        responseError.setErrorType(ProcessingErrorType.ERR_UNKNOWN);
        response.addError(responseError);
        appServerCommService.sendLabelingResponse(response);
    }

    @SuppressWarnings("SwitchStatementWithTooFewBranches")
    private LabelingAction createAction(LabelTaskParameters payload){
        MipMediaConnector mediaConnector = mediaConnectorFactory.getMediaConnector(payload.getLabelingConnection());
        switch (payload.getLabelingVendor()) {
            case MicrosoftInformationProtection:
                MipLabelingAction action = new MipLabelingAction(mediaConnector);
                action.setTargetPathForLabelWrite(targetPathForLabelWrite);
                return action;
            default:
                throw new RuntimeException("Unknown labeling vendor.");
        }
    }
}
