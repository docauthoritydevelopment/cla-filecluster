package com.cla.mediaproc.appserver.connectors;

import com.cla.common.domain.dto.messages.*;
import com.cla.common.domain.dto.messages.control.ControlResultMessage;
import com.fasterxml.jackson.core.JsonProcessingException;

public interface ResultMessagesHandler {
	long getLastResponseDate();

	void setLastResponseDate();

	void sendContentCheckRequest(ContentMetadataRequestMessage contentMetadataRequestMessage);

	void sendFileContentResponse(FileContentResultMessage fileContentResultMessage);

	void sendIngestResult(IngestResultMessage ingestResultMessage) throws JsonProcessingException;

	void sendControlResult(ControlResultMessage resultMessage);

	void sendHandShakeRequestMessage(ControlResultMessage message);

	void sendLabelingResponse(LabelingResponseMessage labelingResponseMessage);

	void sendBufferedScanResult(ScanResultMessage scanResultMessage, boolean forceFlush);

	ContentMetadataResponseMessage checkForExistingContent(ContentMetadataRequestMessage mdRequest, String mediaProcessorInstanceId);

	void sendScanResult(ScanResultMessage scanResultMessage);
}
