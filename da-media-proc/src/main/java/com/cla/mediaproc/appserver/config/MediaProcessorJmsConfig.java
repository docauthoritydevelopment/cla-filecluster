package com.cla.mediaproc.appserver.config;

import com.cla.common.jms.ErrorLoggingPolicy;
import com.cla.config.jms.JmsErrorHandler;
import com.cla.config.jms.JmsErrorHandlingTemplate;
import com.cla.config.jms.JmsErrorRetryPolicy;
import com.cla.filecluster.utils.EncUtils;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.pool.PooledConnectionFactory;
import org.apache.activemq.transport.DefaultTransportListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.boot.autoconfigure.jms.DefaultJmsListenerContainerFactoryConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.ResourceAllocationException;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.config.JmsListenerContainerFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.support.converter.MappingJackson2MessageConverter;
import org.springframework.jms.support.converter.MessageConverter;
import org.springframework.jms.support.converter.MessageType;
import org.springframework.util.backoff.BackOffExecution;

import javax.jms.*;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * Created by uri on 22-Jun-17.
 */
@Configuration
public class MediaProcessorJmsConfig {

    @Value("${spring.activemq.broker-url}")
    private String messageBrokerUrl;

    @Value("${da.activemq.maxAllowedMessageFrameSize:268435456}")
    private int maxAllowedMessageFrameSize;

    @Value("${scan.responses.retryPolicy:-1;1}")
    private String scanResponsesPolicyConfig;

    @Value("${scan.responses.topic:}")
    private String scanResponsesTopic;

    @Value("${ingest.responses.retryPolicy:1;1}")
    private String ingestResponsesPolicyConfig;

    @Value("${ingest.responses.topic}")
    private String ingestResponsesTopic;

    @Value("${control.responses.retryPolicy:5;1}")
    private String controlResponsesPolicyConfig;

    @Value("${control.responses.topic}")
    private String controlResponsesTopic;

    @Value("${realtime.responses.retryPolicy:5;1}")
    private String realtimeResponsesPolicyConfig;

    @Value("${realtime.responses.topic:5;1}")
    private String realtimeResponsesTopic;

    @Value("${da.activemq.template-ttl-min:1440}")
    private int templateTTlMin;

    @Value("${da.activemq.timeout.millis:2000}")
    private int timeout;

    @Value("${activemq.monitor-queue-user:admin}")
    private String activemqUser;

    @Value("${activemq.monitor-queue-pass:admin}")
    private String activemqPass;

    @Value("${use-enc-pass:false}")
    private boolean useEncPassword;

    private final static Logger logger = LoggerFactory.getLogger(MediaProcessorJmsConfig.class);

    /**
     * We provide the active mq connection factory inorder to track the transport errors and provide
     * stats about them
     *
     * @return active mq connection factory
     */
    @ConditionalOnExpression("!'${spring.activemq.broker-url}'.contains('vm://')")
    @Bean
    public ConnectionFactory connectionFactory() {
        logger.info("Creating active mediaproc mq config: {}", messageBrokerUrl);
        ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory();
        activeMQConnectionFactory.setBrokerURL(messageBrokerUrl);
        activeMQConnectionFactory.setCloseTimeout(timeout);
        activeMQConnectionFactory.setOptimizeAcknowledgeTimeOut(timeout);
        activeMQConnectionFactory.setSendTimeout(timeout);
        activeMQConnectionFactory.setUserName(activemqUser);
        activeMQConnectionFactory.setPassword(useEncPassword ? EncUtils.decrypt(activemqPass) : activemqPass);

        // Setting transportErrorLoggingPolicy listener to track active mq failover errors
        activeMQConnectionFactory.setTransportListener(new DefaultTransportListener() {

            ErrorLoggingPolicy transportErrorLoggingPolicy = new ErrorLoggingPolicy();

            @Override
            public void onException(IOException error) {
                logger.info("Transport exception occurred: type: {}, message: {}", error.getClass().getName(), error.getMessage());
                transportErrorLoggingPolicy.onException(logger, error);
            }

        });
        return new PooledConnectionFactory(activeMQConnectionFactory);
    }


    /**
     * DefaultJmsListenerContainerFactory for the listeners of sca, ingest messages from app server.
     * We disable here backoff policy, cause we use the one in active mq.
     * Also we configure to convert message via a custom {@link MediaProcessorJmsConfig#jacksonJmsMessageConverter()}
     *
     * @param connectionFactory JMS connection factory
     * @param configurer  DefaultJmsListenerContainerFactoryConfigurer
     * @return DefaultJmsListenerContainerFactory
     */
    @Bean
    public DefaultJmsListenerContainerFactory mdQueueListenerFactory(ConnectionFactory connectionFactory,
                                                                     DefaultJmsListenerContainerFactoryConfigurer configurer) {
        DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
        // This provides all boot's default to this factory, including the message converter
        //factory.setReceiveTimeout();
        // Disable backoff attempts (activemq will do it)
        factory.setBackOff(() -> () -> BackOffExecution.STOP);
        factory.setPubSubDomain(false);
        factory.setMessageConverter(jacksonJmsMessageConverter());

        configurer.configure(factory, connectionFactory);

        // You could still override some of Boot's default if necessary.
        logger.info("ActiveMQ broker URL: {}", messageBrokerUrl);
        return factory;
    }

    /**
     * DefaultJmsListenerContainerFactory for the listeners for control messages from app server).
     * We disable here backoff policy, cause we use the one in active mq.
     *
     * @param connectionFactory JMS message factory
     * @param configurer  DefaultJmsListenerContainerFactoryConfigurer
     * @return JMS listener container factory
     */
    @Bean
    public JmsListenerContainerFactory<?> mdBroadcastListenerFactory(ConnectionFactory connectionFactory,
                                                                     DefaultJmsListenerContainerFactoryConfigurer configurer) {
        DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
        // Disable backoff attempts (activemq will do it)
        factory.setBackOff(() -> () -> BackOffExecution.STOP);
        //  factory.setSubscriptionDurable(false);
        // This provides all boot's default to this factory, including the message converter
        configurer.configure(factory, connectionFactory);
        factory.setPubSubDomain(true);
        // You could still override some of Boot's default if necessary.
        return factory;
    }


    /**
     * Serialize message content to json using TextMessage
     *
     * @return Jackson JMS message converter
     */
    @Bean
    public MessageConverter jacksonJmsMessageConverter() {
        MappingJackson2MessageConverter converter = new MappingJackson2MessageConverter();
        converter.setTargetType(MessageType.TEXT);
        converter.setTypeIdPropertyName("_type");
        return converter;
    }

    /**
     * Provide the jms template to send messages over the active mq.
     * The jms template will throw an error in case message length is greater than what is configured for
     * da.activemq.maxAllowedMessageFrameSize
     *
     * @param connectionFactory JMS connection factory
     * @param messageConverter JMS message converter
     * @param jmsErrorHandler JMS error handler
     * @return jmsTemplate
     */
    @Bean
    public JmsTemplate mpJmsTemplate(ConnectionFactory connectionFactory, MessageConverter messageConverter,
                                     JmsErrorHandler jmsErrorHandler) {
        final JmsErrorHandlingTemplate jmsErrorHandlingTemplate =
                new JmsErrorHandlingTemplate(connectionFactory, jmsErrorHandler, maxAllowedMessageFrameSize);
        jmsErrorHandlingTemplate.setTimeToLive(TimeUnit.MINUTES.toMillis(templateTTlMin));
        jmsErrorHandlingTemplate.setExplicitQosEnabled(true);
        jmsErrorHandlingTemplate.setMessageConverter(messageConverter);
        return jmsErrorHandlingTemplate;
    }

    @Bean
    public JmsErrorHandler jmsErrorHandler() {
        return JmsErrorHandler.builder()
                // Scan responses error policy
                .withErrorPolicy(new JmsErrorRetryPolicy(scanResponsesPolicyConfig))
                .onDestination(scanResponsesTopic)
                .against(ResourceAllocationException.class)

                // Ingest responses error policy
                .withErrorPolicy(new JmsErrorRetryPolicy(ingestResponsesPolicyConfig))
                .onDestination(ingestResponsesTopic)
                .against(ResourceAllocationException.class)

                // Control responses error policy
                .withErrorPolicy(new JmsErrorRetryPolicy(controlResponsesPolicyConfig))
                .onDestination(controlResponsesTopic)
                .against(ResourceAllocationException.class)

                // Realtime responses error policy
                .withErrorPolicy(new JmsErrorRetryPolicy(realtimeResponsesPolicyConfig))
                .onDestination(realtimeResponsesTopic)
                .against(ResourceAllocationException.class)

                .build();

    }


}
