package com.cla.mediaproc.appserver;

import com.cla.common.domain.dto.crawler.JobStateByDate;
import com.cla.common.domain.dto.messages.control.ControlStateRequestPayload;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

/**
 * In-memory container for job states received from the appserver
 * Created by vladi on 6/7/2017.
 */
@Service
public class JobStateMonitor {
    private Cache<Long, JobStateByDate> jobStates = CacheBuilder.newBuilder()
            .maximumSize(1000)
            .expireAfterAccess(50, TimeUnit.MINUTES)
            .build();

    private Cache<Long, Boolean> jobLimitReached = CacheBuilder.newBuilder()
            .maximumSize(1000)
            .expireAfterAccess(50, TimeUnit.MINUTES)
            .build();

    public void updateJobStates(ControlStateRequestPayload msg){
        msg.getJobStates().forEach(cell -> {
            Long timestamp = cell.getTimstamp() != null && cell.getTimstamp() <= 0 ? null : cell.getTimstamp();
            jobStates.put(cell.getJobId(), JobStateByDate.of(cell.getJobId(), cell.getState(), timestamp));
        });
    }

    public boolean isJobActive(Long id, Long jobStartTime) {
        if (id == null){
            return true;
        }

        JobStateByDate jobState = jobStates.getIfPresent(id);

        // we assume that the job is active if it is not known
        if (jobState == null) {
            return true;
        }

        // if timestamp doesnt match, task belongs to earlier cycle - ignore it
        if (jobStartTime != null && jobState.getTimstamp() != null && !jobState.getTimstamp().equals(jobStartTime)) {
            return false;
        }

        return jobState.getState().isStarted();

    }

    public void updateJobLimitReached(Long jobId) {
        jobLimitReached.put(jobId, true);
    }

    public boolean isJobLimitReached(Long id) {
        if (id == null){
            return false;
        }

        Boolean limitReached = jobLimitReached.getIfPresent(id);

        return limitReached != null && limitReached;
    }
}
