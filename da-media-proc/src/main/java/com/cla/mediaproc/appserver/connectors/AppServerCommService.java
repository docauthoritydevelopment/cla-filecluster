package com.cla.mediaproc.appserver.connectors;

import com.cla.common.domain.dto.messages.*;
import com.cla.common.domain.dto.messages.control.ControlResultMessage;
import com.cla.common.utils.DaMessageConversionUtils;
import com.cla.connector.utils.TimeSource;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.jms.JmsException;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Communication service used for sending messages back to the app server
 * Created by liora on 11/06/2017.
 */
@Service
@ConditionalOnProperty(prefix = "ingest.result.messages",name = "export-enable", havingValue = "false" ,matchIfMissing = true)
public class AppServerCommService implements ResultMessagesHandler {

    private final static Logger logger = LoggerFactory.getLogger(AppServerCommService.class);

    @Autowired
    private JmsTemplate mpJmsTemplate;

    @Autowired
    private HttpCommunicationService httpCommunicationService;

    @Value("${appserver.host:localhost}")
    private String appServerHost;

    @Value("${appserver.port:9000}")
    private int appServerPort;

    @Value("${appserver.content-check.max-allowed-time-ms:15000}")
    private int maxAllowedContentCheckTimeMs;

    @Value("${scan.responses.topic:}")
    private String scanResponsesTopic;

    @Value("${ingest.responses.topic}")
    private String ingestResponsesTopic;

    @Value("${realtime.responses.topic}")
    private String realtimeResponsesTopic;

    @Value("${control.responses.topic:}")
    private String controlResponseTopic;

    @Value("${labeling.responses.topic}")
    private String labelingResponsesTopic;

    @Value("${contentcheck.requests.topic:contentcheck.requests}")
    private String contentCheckRequestTopic;

    @Value("${ingest.retry.sleep:10000}")
    private long retrySleep;

    @Value("${content.signature.check.url:/api/content/checkSignature}")
    private String contentSignitureCheckUrl;

    @Value("${content.signature.check.scheme:http}")
    private String contentSignitureCheckScheme;

    @Value("${scan.buffer:true}")
    private boolean scanBuffer;

    private final Object scanBufferLock = new Object();

    @Value("${scan.buffer.size:10}")
    private int scanBufferSize;

    private ArrayList<ScanResultMessage> buffer = Lists.newArrayList();
    private Set<Long> scanTasksInBuffer = new HashSet<>();

    private long pendingSendCounter;

    private long scanDonePendingTimeout;

    private TimeSource timeSource = new TimeSource();

    private AtomicLong lastResponseDate = new AtomicLong(timeSource.currentTimeMillis());

    @Override
	public long getLastResponseDate() {
        return lastResponseDate.get();
    }

    @Override
	public void setLastResponseDate() {
        lastResponseDate.set(timeSource.currentTimeMillis());
    }

    @Override
	public void sendContentCheckRequest(ContentMetadataRequestMessage contentMetadataRequestMessage) {
        try {
            String fileContentResultJson = DaMessageConversionUtils.ContentCheck.serializeRequestMessage(contentMetadataRequestMessage);
            mpJmsTemplate.convertAndSend(contentCheckRequestTopic, fileContentResultJson);
        } catch (Exception e) {
            logger.error("Failed to convert and send message.", e);
        }
    }

    @Override
	public void sendFileContentResponse(FileContentResultMessage fileContentResultMessage) {
        try {
            String fileContentResultJson = DaMessageConversionUtils.FileContent.serializeResultMessage(fileContentResultMessage);
            mpJmsTemplate.convertAndSend(realtimeResponsesTopic, fileContentResultJson);
        } catch (Exception e) {
            logger.error("Failed to convert and send message.", e);
        }
    }

    @Override
    public void sendLabelingResponse(LabelingResponseMessage labelingResponseMessage) {
        try {
            String labelingResultJson = DaMessageConversionUtils.Labeling.serializeResultMessage(labelingResponseMessage);
            mpJmsTemplate.convertAndSend(labelingResponsesTopic, labelingResultJson);
        } catch (Exception e) {
            logger.error("Failed to convert and send message.", e);
        }
    }


    @Override
	public void sendIngestResult(IngestResultMessage ingestResultMessage) throws JsonProcessingException {
        String ingestResultJson = DaMessageConversionUtils.Ingest.serializeResultMessage(ingestResultMessage);
        mpJmsTemplate.convertAndSend(ingestResponsesTopic, ingestResultJson);
        logger.debug("Ingest result message sent to AppServer for file id {} ", ingestResultMessage.getClaFileId());
    }

    @Override
	public void sendControlResult(ControlResultMessage resultMessage) {
        logger.trace("Send control result message");
        try {
            String data = DaMessageConversionUtils.Control.serializeResultMessage(resultMessage);
            mpJmsTemplate.convertAndSend(controlResponseTopic, data);
            logger.debug("Control message sent");
            setLastResponseDate();
        } catch (RuntimeException e) {
            logger.error("Failed to send control result message", e);
            throw e;
        } catch (JsonProcessingException e) {
            logger.error("Failed to convert to Json", e);
            throw new RuntimeException("Failed to convert to Json", e);
        }
    }

    @Override
	public void sendHandShakeRequestMessage(ControlResultMessage message) {
        try {
            logger.debug("About to Send handshake control message {} ", message);
            String messageStr = DaMessageConversionUtils.Control.serializeResultMessage(message);
            mpJmsTemplate.convertAndSend(controlResponseTopic, messageStr); //TDO Liora: send broadcast to appServer
            logger.trace("Sent handshake control message {} ", message);
            setLastResponseDate();
        } catch (Exception e) {
            logger.error("Failed to send hand shake control message.", e);
        }
    }

    @Override
	public void sendBufferedScanResult(ScanResultMessage scanResultMessage, boolean forceFlush) {
        if (scanBuffer) {
            synchronized (scanBufferLock) {
                buffer.add(scanResultMessage);
                scanTasksInBuffer.add(scanResultMessage.getTaskId());
            }
            if (buffer.size() >= scanBufferSize || forceFlush) {
                flushScanResultBuffer();
            }
        } else {
            sendScanResult(scanResultMessage);
        }
    }

    @Override
	public ContentMetadataResponseMessage checkForExistingContent(ContentMetadataRequestMessage mdRequest, String mediaProcessorInstanceId) {
        logger.info("Check for existing content {}", mdRequest);

        String url = UriComponentsBuilder.newInstance()
                .scheme(contentSignitureCheckScheme).host(appServerHost).port(appServerPort)
                .path(contentSignitureCheckUrl)
                .queryParam("signature", mdRequest.getSignature())
                .queryParam("fileSize", mdRequest.getFileSize())
                .queryParam("mpId", mediaProcessorInstanceId)
                .queryParam("rootFolderId", mdRequest.getRootFolderId())
                .build().encode().toUriString();

        String loginUrl = contentSignitureCheckScheme + "://" + appServerHost + ":" + appServerPort + "/login";
        try {
            long timeInfo = System.currentTimeMillis();
            ContentMetadataResponseMessage contentMetadataResponseMessage =
                    httpCommunicationService.sendRestCallWithRetries(url, loginUrl, ContentMetadataResponseMessage.class);
            timeInfo = System.currentTimeMillis() - timeInfo;
            if (timeInfo > maxAllowedContentCheckTimeMs) {
                logger.warn("Slow check for existing content via url: {}. Tool {}ms. Return with response: {}", url, timeInfo, contentMetadataResponseMessage);
            } else {
                logger.info("Check for existing content via url: {}. Tool {}ms. Return with response: {}", url, timeInfo, contentMetadataResponseMessage);
            }
            return contentMetadataResponseMessage;
        } catch (RuntimeException e) {
            logger.warn("Failed to check for existing content after retries for url {}", url);
        }
        return null;
    }

    @Override
	public void sendScanResult(ScanResultMessage scanResultMessage) {
        String data = DaMessageConversionUtils.Scan.serializeResultMessage(scanResultMessage);
        logger.debug("One SCAN result message sent to appServer {}", data);
        increasePendingCounter();
        try {
            mpJmsTemplate.convertAndSend(scanResponsesTopic, data);
        } catch (JmsException ex) {
            logger.error("Failed to send scan result message {}", ex, ex);
        }
        decreasePendingCounter();
    }

    private synchronized void increasePendingCounter() {
        pendingSendCounter++;
    }

    private synchronized void decreasePendingCounter() {
        pendingSendCounter--;
    }

    private void flushScanResultBuffer() {
        if (buffer == null || buffer.size() == 0) {
            return;
        }
        List<ScanResultMessage> currentBuffer;
        Set<Long> taskIds;
        synchronized (scanBufferLock) {
            currentBuffer = buffer;
            taskIds = scanTasksInBuffer;
            buffer = Lists.newArrayListWithCapacity(scanBufferSize);
            scanTasksInBuffer = new HashSet<>();
        }
        if (currentBuffer != null) {
            String data = DaMessageConversionUtils.Scan.serializeResultMessages(currentBuffer, taskIds);
            logger.info("{} buffered SCAN result messages sent to appServer", currentBuffer.size());
            mpJmsTemplate.convertAndSend(scanResponsesTopic, data);
        }
        long start = timeSource.currentTimeMillis();
        //Wait for all pending messages to flush  //TODO: liora, for what this is?
        while (pendingSendCounter > 0 && timeSource.millisSince(start) < scanDonePendingTimeout) {
            try {
                logger.info("{} scan messages are still in the queue", pendingSendCounter);
                Thread.sleep(100);
            } catch (InterruptedException e) {
                logger.error("Failed to sleep while waiting for pending scan counter");
            }
        }
        setLastResponseDate();
    }


}
