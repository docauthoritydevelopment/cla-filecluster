package com.cla.mediaproc.appserver;

import com.cla.common.connectors.ExistingContentConnector;
import com.cla.common.domain.dto.file.DirectoryExistStatus;
import com.cla.common.domain.dto.filetree.RootFolderDto;
import com.cla.common.domain.dto.messages.*;
import com.cla.common.domain.dto.messages.control.*;
import com.cla.common.utils.LogExportUtils;
import com.cla.connector.domain.dto.error.ProcessingErrorType;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.file.FileContentDto;
import com.cla.connector.domain.dto.file.ServerResourceDto;
import com.cla.connector.domain.dto.media.MediaConnectionDetailsDto;
import com.cla.connector.domain.dto.messages.ProcessingError;
import com.cla.connector.domain.dto.messages.ScanTaskParameters;
import com.cla.connector.mediaconnector.FileMediaConnector;
import com.cla.connector.mediaconnector.MediaConnector;
import com.cla.connector.utils.Pair;
import com.cla.connector.utils.TimeSource;
import com.cla.filecluster.service.media.MediaConnectorFactory;
import com.cla.filecluster.util.LocalFileUtils;
import com.cla.mediaproc.appserver.connectors.AppServerCommListeners;
import com.cla.mediaproc.appserver.connectors.ResultMessagesHandler;
import com.cla.mediaproc.service.filecontent.FileContentManagerService;
import com.cla.mediaproc.service.ingest.SearchPatternService;
import com.cla.mediaproc.service.managers.MediaProcessorStateService;
import com.google.common.base.Strings;
import com.sun.management.OperatingSystemMXBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.io.*;
import java.lang.management.ManagementFactory;
import java.net.InetAddress;
import java.nio.file.Path;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.zip.ZipOutputStream;

/**
 * Receives requests from appserver through the communication listeners
 * and relays them to the relevant services.
 * Created by liora on 11/06/2017.
 */
@Service
public class AppServerProcessingService implements ExistingContentConnector {
    private static Logger logger = LoggerFactory.getLogger(AppServerProcessingService.class);

    @Value("${mediaprocessor.id:1}")
    private String mediaProcessorInstanceId;

    @Value("${log.top-index-to-export:5}")
    private int exportFilesTopIndex;

    @Value("${log.max-days-to-export:2}")
    private int exportFilesTopDays;

    @Value("${log.files-to-export:}")
    private String[] exportFiles;

    @Value(("${content-check.timeout-min:3}"))
    private int contentCheckTimeoutMinutes;

    @Autowired
    private ResultMessagesHandler appServerCommService;

    @Autowired
    private JobStateMonitor jobStateMonitor;

    @Autowired
    private IngestConsumer ingestConsumer;

    @Autowired
    private ScanConsumer scanConsumer;

    @Autowired
    private FileContentManagerService fileContentManagerService;

    @Value("${content.check.type:JMS}")
    private ContentCheckerType contentCheckerType;

    @Autowired
    private MediaProcessorStateService mediaProcessorStateService;

    @Autowired
    private SearchPatternService searchPatternService;

    @Autowired
    private MediaConnectorFactory mediaConnectorFactory;

    private TimeSource timeSource = new TimeSource();
    private ConcurrentHashMap<String, CompletableFuture<ContentMetadataResponseMessage>> lockMapIngest = new ConcurrentHashMap<>();

    @Resource
    private AppServerCommListeners appServerCommListeners;

    private boolean initialized = false;
    private boolean active = true;
    private long statusMsgCount = 0;

    @PostConstruct
    private void init() {
        sendHandShakeRequest(true);   // so we get back job statuses
        mediaConnectorFactory.setKeyValuePairsConsumer(map -> {
            KeyValuePayloadResp keyValuePayloadResp = new KeyValuePayloadResp(map);
            appServerCommService.sendFileContentResponse(keyValuePayloadResp);
        });
    }

    public void consumeStartScanRequest(ScanRequestMessage scanRequestMessage) {
        long currentTime = timeSource.currentTimeMillis();

        try {
            logger.info("Starting scan task {} at {}, thread name {}.", scanRequestMessage, currentTime,
                    Thread.currentThread().getName());
            ScanTaskParameters scanParams = scanRequestMessage.getPayload();
            boolean paused = !jobStateMonitor.isJobActive(scanParams.getJobId(), scanParams.getJobStateIdentifier());
            sendScanStartedMsg(scanRequestMessage.getTaskId(), scanParams.getJobId(), scanParams.isFirstScan());
            if (paused) {
                logger.info("Scan job #{} is paused. Skipping scan task #{}", scanParams.getJobId(), scanParams.isFirstScan());
            } else {
                scanConsumer.startScanMediaFiles(scanRequestMessage);
                logger.info("Finishing scan task {} at {}", scanRequestMessage, currentTime);
            }
        } catch (RuntimeException e) {
            logger.error("Failed to handle scan task {}", scanRequestMessage, e);
            try {
            ScanTaskParameters scanParams = scanRequestMessage.getPayload();
                DirectoryExistStatus directoryExistStatus =
            fileContentManagerService.getDirectoryExistStatus(scanParams.getRootFolderId(),
                                scanParams.getPath(), true, scanParams.getConnectionDetails());
                scanConsumer.sendScanFailedResponse(scanRequestMessage, e.getMessage(),
                        DirectoryExistStatus.YES.equals(directoryExistStatus));
            }
            catch (RuntimeException ex) {
                scanConsumer.sendScanFailedResponse(scanRequestMessage,e.getMessage(),false);
            }
        }
    }

    public String getMediaProcessorInstanceId() {
        return mediaProcessorInstanceId;
    }

    public void consumeFileContentRequest(FileContentRequestMessage fileContentRequestMessage) {
        FileContentTaskParameters payload = fileContentRequestMessage.getPayload();
		FileContentMessagePayload fileContentMessagePayload = new FileContentMessagePayload();
		Long rootFolderId = null;
		FileContentType fileContentType = payload.getFileContentType();
		FileContentType resultType = fileContentType;
		try {
			MediaConnectionDetailsDto mediaConnectionDetailsDto = payload.getMediaConnectionDetailsDto();

			switch (fileContentType) {
				case IS_DIRECTORY_EXISTS:
					RootFolderDto rootFolderDto = payload.getRootFolderDto();
					rootFolderId = rootFolderDto.getId();
					DirectoryExistStatus directoryExistStatus = fileContentManagerService.isDirectoryExists(
							rootFolderDto, mediaConnectionDetailsDto, fileContentRequestMessage.getPayload().isForceAccessibilityCheck());
					fileContentMessagePayload.setDirectoryExistsStatus(directoryExistStatus);
					break;
				case GET_LOGS:
					if (fileContentRequestMessage.getPayload().getInstanceId() != null &&
							!fileContentRequestMessage.getPayload().getInstanceId().toString().equals(mediaProcessorInstanceId)) {
						return; // not current mp, ignore
					}
					collectLogs(fileContentMessagePayload);
					break;
				default:
					String fileSearchIdentifier = GeneralIngestRequestHandlerProvider.resolveFileSearchIdentifier(payload);
					Pair<FileContentDto, ? extends ClaFilePropertiesDto> fileData =
							fileContentManagerService.getFileContent(payload.getFileType(), fileContentType,
                                    payload.getMediaEntityId(), fileSearchIdentifier, mediaConnectionDetailsDto);
					FileContentDto fileContentDto = fileData.getKey();
					ClaFilePropertiesDto fileAttributesDto = fileData.getValue();
					fileContentMessagePayload.setModTimeMilli(fileAttributesDto.getModTimeMilli());

					try (InputStream is = fileContentDto.getInputStream()) {
						if (fileContentType == FileContentType.EXTRACT_HIGHLIGHT_FILE) {
							fileContentMessagePayload = fileContentManagerService.handleHighlight(payload.getFileName(),
                                    payload.getFileType(), payload.getTextTokens(), is, fileContentMessagePayload);
						} else {
							fileContentMessagePayload = fileContentManagerService.handleContentDownload(payload.getFileName(),
                                    fileContentDto, is, fileContentMessagePayload);
						}
					}
					break;
			}
        } catch(Exception e){
			logger.error("Failed to handle file content request message.", e);
			fileContentMessagePayload.setContent("Failed to handle file content request message "+e.getMessage());
			resultType = FileContentType.ERROR;
		}
        finally {
			FileContentResultMessage fileContentResultMessage = new FileContentResultMessage(fileContentMessagePayload);
			fileContentMessagePayload.setBaseName(payload.getBaseName());
			fileContentMessagePayload.setFileType(payload.getFileType());
			fileContentMessagePayload.setRequestId(payload.getRequestId());
			fileContentResultMessage.setClaFileId(payload.getFileId() == null ? 0 : payload.getFileId());
			fileContentResultMessage.setRootFolderId(rootFolderId);
			fileContentResultMessage.setRequestType(resultType);
			appServerCommService.sendFileContentResponse(fileContentResultMessage);
		}
    }

    public void consumeIngestRequest(IngestRequestMessage requestMessage) {
        boolean paused = !jobStateMonitor.isJobActive(requestMessage.getJobId(), requestMessage.getPayload().getJobStateIdentifier());
        if (paused) {
            logger.info("Discarding ingest task {} , Job {} is no longer active", requestMessage.getJobId(), requestMessage.getTaskId());
        } else {
            ingestConsumer.consumeIngestionRequest(requestMessage);
        }
    }

    public synchronized void consumeControlRequest(ControlRequestMessage requestMessage) {
        try {
            switch (requestMessage.getType()) {
                case JOBS_STATES:
                    ControlStateRequestPayload controlStatePayload = (ControlStateRequestPayload) requestMessage.getPayload();
                    if (controlStatePayload != null && controlStatePayload.getJobStates() != null) {
                        if (logger.isDebugEnabled()) {
                            logger.debug("JOBS_STATES update received: {}", controlStatePayload.getJobStates());
                        }
                        jobStateMonitor.updateJobStates(controlStatePayload);
                    }
                    break;

                case SEARCH_PATTERNS:
                    ControlPatternRequestPayload payload = (ControlPatternRequestPayload) requestMessage.getPayload();
                    if (payload != null) {
                        searchPatternService.setMaxSingleSearchPatternMatches(payload.getMaxSingleSearchPatternMatches());
                        searchPatternService.setSearchPatternStopSearchAfterMulti(payload.isSearchPatternStopSearchAfterMulti());
                        searchPatternService.setSearchPatternCountThresholdMulti(payload.getSearchPatternCountThresholdMulti());
                        if (payload.getSearchPatterns() != null && !payload.getSearchPatterns().isEmpty()) {
                            payload.getSearchPatterns().forEach(
                                    p -> searchPatternService.updatePattern(p)
                            );
                            logger.debug("SEARCH_PATTERNS update received {}", payload.getSearchPatterns());
                        } else {
                            searchPatternService.markGotPatterns();
                        }
                    }
                    break;

                case HANDSHAKE:
                    processHandshake(requestMessage);
                    break;

                case STATE_CHANGE:
                    processStateChange(requestMessage);
                    break;

                case LIMIT_REACHED:
                    jobStateMonitor.updateJobLimitReached(requestMessage.getJobId());
                    logger.info("received scan limit reached message for job {}", requestMessage.getJobId());
                    break;

                case REFRESH_CONNECTOR:
                    RefreshConnectionDetailsPayload refreshConnectionDetailsPayload = (RefreshConnectionDetailsPayload) requestMessage.getPayload();
                    MediaConnectionDetailsDto connectionDetails = refreshConnectionDetailsPayload.getMediaConnectionDetails();
                    mediaConnectorFactory.createMediaConnector(connectionDetails);
                    logger.info("Refreshed media-connection details for connection {} - {}", connectionDetails.getId(), connectionDetails.getName());
                    break;
                default:
                    logger.warn("Unknown ControlRequestType {}", requestMessage.getType());
            }
        } catch (Exception e) {
            logger.error("Failed to handle control request message. ({})", e);
        }
    }

    private void processHandshake(ControlRequestMessage requestMessage) {
        Long dataCenterId;
        initialized = false;
        ServerHandShakeRequestPayload payload = (ServerHandShakeRequestPayload) requestMessage.getPayload();
        dataCenterId = payload.getDataCenterId();
        active = payload.isActive();
        if (!active) {
            appServerCommListeners.deactivateListeners();
        } else {
            if (dataCenterId == null) {
                logger.warn("Handshake received with empty dataCenterId.");
            }
            appServerCommListeners.redirectListeningEndPoint(dataCenterId, payload.isDataCenterRequiresPrefix());
        }
        logger.warn(">>> Media processor {} initialized on dataCenterId {}  Active: {}", mediaProcessorInstanceId, dataCenterId, active);
        initialized = true;
        sendStatusReport(); //change the mp status from starting to OK
    }

    private void processStateChange(ControlRequestMessage requestMessage) {
//      initialized = false;
        ServerStateChangeRequestPayload payload = (ServerStateChangeRequestPayload) requestMessage.getPayload();
        Long dataCenterId = payload.getDataCenterId();
        active = payload.isActive();
        if (!active) {
            appServerCommListeners.deactivateListeners();
        } else {
            if (dataCenterId == null) {
                logger.warn("StateChange received with empty dataCenterId.");
            }
            appServerCommListeners.redirectListeningEndPoint(dataCenterId, payload.isDataCenterRequiresPrefix());
        }
        logger.warn(">>> Media processor {} stateChange on dataCenterId {}  Active: {}", mediaProcessorInstanceId, dataCenterId, active);
//                initialized = true;
        //        sendStatusReport();
        // A state change should be considered as a MP restart.
        sendHandShakeRequest(false);
    }

    private void sendScanStartedMsg(long taskId, long jobId, boolean firstScan) {
        ScanStartedPayload payload = new ScanStartedPayload(taskId, jobId, firstScan);
        ControlResultMessage scanStartedMsg =
                new ControlResultMessage(payload, MessagePayloadType.SCAN_STARTED, mediaProcessorInstanceId);
        scanStartedMsg.setSendTime(timeSource.currentTimeMillis());
        appServerCommService.sendControlResult(scanStartedMsg);
    }

    public void sendStatusReport() {
        ++statusMsgCount;
        logger.debug("Periodic status report / handshake handler. Init={}, count={}", (initialized ? "T" : "F"), statusMsgCount);
        if (initialized && active) {
            ControlStatusResponsePayload responsePayload = new ControlStatusResponsePayload();
            responsePayload.setIngestTaskRequestFailure(mediaProcessorStateService.getIngestTaskRequestFailure());
            responsePayload.setIngestTaskRequests(mediaProcessorStateService.getIngestTaskRequests());
            responsePayload.setIngestTaskRequestsFinished(mediaProcessorStateService.getIngestTaskRequestsFinished());
            responsePayload.setIngestTaskRequestsFinishedWithError(mediaProcessorStateService.getIngestTaskRequestsFinishedWithError());
            responsePayload.setScanTaskFailedResponsesFinished(mediaProcessorStateService.getScanTaskFailedResponsesFinished());
            responsePayload.setScanTaskResponsesFinished(mediaProcessorStateService.getScanTaskResponsesFinished());
            responsePayload.setStartScanTaskRequests(mediaProcessorStateService.getStartScanTaskRequests());
            responsePayload.setActiveScanTasks(mediaProcessorStateService.getActiveScanTasks());
            responsePayload.setStartScanTaskRequestsFinished(mediaProcessorStateService.getStartScanTaskRequestsFinished());
            responsePayload.setStartScanTaskRequestsPaused(mediaProcessorStateService.getStartScanTaskRequestsPaused());
            responsePayload.setFailedSignatureCheck(mediaProcessorStateService.getFailedSignatureCheck());

            responsePayload.setIngestRunningThroughput(mediaProcessorStateService.getIngestRunningThroughput());
            responsePayload.setAverageIngestThroughput(mediaProcessorStateService.getAverageIngestThroughput());
            responsePayload.setScanRunningThroughput(mediaProcessorStateService.getScanRunningThroughput());
            responsePayload.setAverageScanThroughput(mediaProcessorStateService.getAverageScanThroughput());
            responsePayload.setNeedSearchPatterns(searchPatternService.waitingForPatterns());
            responsePayload.setVersion(mediaProcessorStateService.getVersion());
            setOperationSystemUsage(responsePayload);

            ControlResultMessage resultMessage = new ControlResultMessage(responsePayload, MessagePayloadType.STATUS_REPORT, mediaProcessorInstanceId);
            resultMessage.setSendTime(timeSource.currentTimeMillis());
            appServerCommService.sendControlResult(resultMessage);
        } else if (!initialized) {
            sendHandShakeRequest(false);
        }
    }

    public void consumeContentCheckResponse(ContentMetadataResponseMessage res) {
        if (Strings.isNullOrEmpty(res.getRequestId())) {
            logger.warn("received response with no request id {}", res);
        } else {
            CompletableFuture<ContentMetadataResponseMessage> req = lockMapIngest.get(res.getRequestId());
            if (req == null) {
                logger.warn("received response but cant find request {}", res);
            } else {
                lockMapIngest.remove(res.getRequestId());
                req.complete(res);
            }
        }
    }

    private String addContentCheckToMapLock(CompletableFuture<ContentMetadataResponseMessage> req) {
        String uniqueID = UUID.randomUUID().toString();
        lockMapIngest.put(uniqueID, req);
        return uniqueID;
    }

    @Override
    public ContentMetadataResponseMessage checkForExistingContent(ContentMetadataRequestMessage mdRequest) {
        switch (contentCheckerType) {
            case REST:
                ContentMetadataResponseMessage res = appServerCommService.checkForExistingContent(mdRequest, getMediaProcessorInstanceId());
                if (res == null) {
                    mediaProcessorStateService.incFailedSignatureCheck();
                }
                return res;
            case DUMMY:
                logger.warn("USING DUMMY CONTENT METADATA CHECKER!");
                return new ContentMetadataResponseMessage(null, false);
            case JMS:
                CompletableFuture<ContentMetadataResponseMessage> completableFuture = new CompletableFuture<>();
                String requestId = addContentCheckToMapLock(completableFuture);
                mdRequest.setRequestId(requestId);
                mdRequest.setMediaProcessorId(mediaProcessorInstanceId);
                appServerCommService.sendContentCheckRequest(mdRequest);
                try {
                    ContentMetadataResponseMessage resp = completableFuture.get(contentCheckTimeoutMinutes, TimeUnit.MINUTES);
                    logger.debug("received resp for content check {}", resp);
                    return resp;
                } catch (Exception e) {
                    mediaProcessorStateService.incFailedSignatureCheck();
                    logger.error("failed getting content check response {}", mdRequest, e);
                    return null;
                }
            default:
                throw new RuntimeException("unsupported content checker type " + contentCheckerType);
        }
    }

    public void consumeListFoldersRequest(FileContentTaskParameters requestMessagePayload) {
        try {
            MediaConnector mediaConnector;
            List<ServerResourceDto> subFolders;
            if (requestMessagePayload.isTest()) {
                mediaConnector = mediaConnectorFactory.createMediaConnector(requestMessagePayload.getMediaConnectionDetailsDto());
                subFolders = mediaConnector.testConnection();
            } else {
                mediaConnector = mediaConnectorFactory.getMediaConnector(requestMessagePayload.getMediaConnectionDetailsDto());
                if (mediaConnector instanceof FileMediaConnector) {
                    subFolders = ((FileMediaConnector)mediaConnector).browseSubFolders(requestMessagePayload.getMediaEntityId());
                } else {
                    throw new RuntimeException("browse folders operation not supported");
                }
            }
            FoldersListResultMessage foldersListResultMessage = new FoldersListResultMessage(subFolders, requestMessagePayload.getRequestId());
            appServerCommService.sendFileContentResponse(foldersListResultMessage);
        } catch (Exception e) {
            logger.error("error consuming list folders request", e);
            FoldersListResultMessage foldersListResultMessage = new FoldersListResultMessage(Collections.emptyList(),
                    requestMessagePayload.getRequestId());
            IngestError error = new IngestError();
            error.setExceptionText(e.getMessage());
            error.setErrorType(ProcessingErrorType.ERR_UNKNOWN);
            error.setSeverity(ProcessingError.ErrorSeverity.CRITICAL);
            foldersListResultMessage.addError(error);
            appServerCommService.sendFileContentResponse(foldersListResultMessage);
        }
    }

    private void sendHandShakeRequest(boolean isFirstTimeSinceGoingUp) {
        try {
            ServerHandShakeResponsePayload responsePayload = new ServerHandShakeResponsePayload();
            setHostDetails(responsePayload);
            responsePayload.setNeedSearchPatterns(searchPatternService.waitingForPatterns());
            ControlResultMessage resultMessage = new ControlResultMessage(responsePayload, MessagePayloadType.HANDSHAKE_RESPONSE, mediaProcessorInstanceId);
            resultMessage.setSendTime(timeSource.currentTimeMillis());
            resultMessage.setFirstTimeSinceGoingUp(isFirstTimeSinceGoingUp);
            appServerCommService.sendHandShakeRequestMessage(resultMessage);
        } catch (RuntimeException e) {
            logger.error("Failed to handle handshake request.", e);
        }
    }


    private int operationSystemUsageCounter = 0;

    private void setOperationSystemUsage(ControlStatusResponsePayload responsePayload) {
        try {
            /*
             * For a Windows platform the account running the process/service should be part of on of the following groups in order to get CPU performance data.
             * Administrators
             * ‘Performance Monitor Users’ - a builtin AD group
             * 'Performance Log Users' - a builtin local group
             */
            OperatingSystemMXBean osBean = ManagementFactory.getPlatformMXBean(OperatingSystemMXBean.class);
            responsePayload.setFreePhysicalMemorySize(osBean.getFreePhysicalMemorySize());
            responsePayload.setTotalPhysicalMemorySize(osBean.getTotalPhysicalMemorySize());
            responsePayload.setCommittedVirtualMemorySize(osBean.getCommittedVirtualMemorySize());
            responsePayload.setFreeSwapSpaceSize(osBean.getFreeSwapSpaceSize());
            responsePayload.setTotalSwapSpaceSize(osBean.getTotalSwapSpaceSize());
            responsePayload.setProcessCpuTime(osBean.getProcessCpuTime());
            responsePayload.setProcessCpuLoad(osBean.getProcessCpuLoad());
            responsePayload.setSystemCpuLoad(osBean.getSystemCpuLoad());
            if (logger.isDebugEnabled() && (operationSystemUsageCounter++ % 100) == 0) {
                String sb = "Arch=" + osBean.getArch() +
                        ", Name=" + osBean.getName() +
                        ", SystemCpuLoad=" + responsePayload.getSystemCpuLoad() + (responsePayload.getSystemCpuLoad()<0d ? " (check Performance Monitor permissions)":"") +
                        ", ProcessCpuLoad=" + responsePayload.getProcessCpuLoad() +
                        ", SystemLoadAverage=" + osBean.getSystemLoadAverage() +
                        ", AvailableProcessors=" + osBean.getAvailableProcessors() +
                        ", ProcessCpuTime=" + responsePayload.getProcessCpuTime() +
                        ", FreePhysicalMemorySize=" + responsePayload.getFreePhysicalMemorySize() +
                        ", CommittedVirtualMemorySize=" + responsePayload.getCommittedVirtualMemorySize();
                logger.debug("osBead data: {}", sb);
            }
        } catch (Exception e) {
            logger.error("Failed to set MP OperationSystem details {}", e);
        }
    }

    private void setHostDetails(ServerHandShakeResponsePayload requestPayload) {
        String bestAddress;
        try {
            InetAddress localhost = InetAddress.getLocalHost();
            bestAddress = localhost.getCanonicalHostName();
            requestPayload.setHostName(bestAddress);

            // Just in case this host has multiple IP addresses....
            InetAddress[] allMyIps = InetAddress.getAllByName(localhost.getCanonicalHostName());
            if (allMyIps != null && allMyIps.length > 1) {
                //  logger.info(myIp.getCanonicalHostName());
                List<String> collect = Arrays.stream(allMyIps)
                        .map(InetAddress::getHostAddress)
                        .collect(Collectors.toList());
                requestPayload.setAddresses(collect);
            }
        } catch (Exception e) {
            logger.error("Failed to set MP host details {}", e);
        }
    }

    private void collectLogs(FileContentMessagePayload fileContentMessagePayload) throws IOException {
		String fileFullName = null;
		try {
			List<Path> files = LogExportUtils.getLogFilesToExport(exportFiles, exportFilesTopIndex, exportFilesTopDays);
			String time = OffsetDateTime.now(ZoneOffset.UTC).format(DateTimeFormatter.ofPattern("yyyy-MM-d‌​d-HH-mm"));
			String fileName = "mp-logs-" + mediaProcessorInstanceId + "-" + time + ".zip";
			fileFullName = "./logs/" + fileName;
			FileOutputStream fos = new FileOutputStream(fileFullName);
			BufferedOutputStream bos = new BufferedOutputStream(fos);
			try (ZipOutputStream zipOutputStream = new ZipOutputStream(bos)) {
				files.forEach(f -> {
					try {
						LocalFileUtils.addFileToZip(f, zipOutputStream);
					} catch (IOException e) {
						logger.error("Failed to add log file " + f + " to zip stream", e);
					}
				});
			}

			try (FileInputStream is = new FileInputStream(fileFullName)) {
				long fileSize = is.getChannel().size();
				FileContentDto fileContentDto = new FileContentDto(is, fileSize, fileName);
				fileContentManagerService.handleContentDownload(fileName, fileContentDto, is, fileContentMessagePayload);
			}
		} finally {
			if (!Strings.isNullOrEmpty(fileFullName)) {
				File f = new File(fileFullName);
				if (f.exists()) {
					f.delete();
				}
			}
		}
	}
}
