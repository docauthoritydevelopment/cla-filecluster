package com.cla.mediaproc.appserver;

import com.cla.common.domain.dto.crawler.FileEntity;
import com.cla.common.domain.dto.file.DirectoryExistStatus;
import com.cla.common.domain.dto.file.ItemDeletionPayload;
import com.cla.common.domain.dto.file.ScanProgressPayload;
import com.cla.common.domain.dto.messages.KeyValuePayloadResp;
import com.cla.common.domain.dto.messages.MessagePayloadType;
import com.cla.common.domain.dto.messages.ScanRequestMessage;
import com.cla.common.domain.dto.messages.ScanResultMessage;
import com.cla.common.exceptions.BreakStreamException;
import com.cla.common.media.files.pst.model.PstFileFacade;
import com.cla.common.scan.DeletedItemsProcessor;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.file.DiffType;
import com.cla.connector.domain.dto.file.FileType;
import com.cla.connector.domain.dto.media.MediaChangeLogDto;
import com.cla.connector.domain.dto.media.MediaType;
import com.cla.connector.domain.dto.messages.*;
import com.cla.connector.mediaconnector.AbstractMediaConnector;
import com.cla.connector.mediaconnector.FileMediaConnector;
import com.cla.connector.mediaconnector.fileshare.FileShareMediaUtils;
import com.cla.connector.mediaconnector.fileshare.preserveaccesstime.WindowsPreserveAccessTime;
import com.cla.connector.progress.DummyProgressTracker;
import com.cla.connector.progress.ProgressTracker;
import com.cla.connector.utils.FileTypeUtils;
import com.cla.connector.utils.MediaConnectorUtils;
import com.cla.connector.utils.Pair;
import com.cla.filecluster.service.media.MediaConnectorFactory;
import com.cla.kvdb.FileEntityStore;
import com.cla.kvdb.KeyValueDatabaseRepository;
import com.cla.kvdb.domain.FileEntityUtils;
import com.cla.kvdb.xodus.XodusStringComparator;
import com.cla.mediaproc.appserver.connectors.ResultMessagesHandler;
import com.cla.mediaproc.service.filecontent.FileContentManagerService;
import com.cla.mediaproc.service.managers.MediaProcessorStateService;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinWorkerThread;
import java.util.concurrent.TimeUnit;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.Optional.ofNullable;

/**
 * Scans media using media connector and send result to the app server using AppServerCommService
 */
@Service
public class ScanConsumer {

    private final static Logger logger = LoggerFactory.getLogger(ScanConsumer.class);

    @Autowired
    private MediaProcessorStateService mediaProcessorStateService;

    @Autowired
    private MediaConnectorFactory mediaConnectorFactory;

    @Autowired
    private ResultMessagesHandler appServerCommService;

    @Autowired
    private JobStateMonitor jobStateMonitor;

    @Autowired
    private FileContentManagerService fileContentManagerService;

    @Autowired
    private PstFileFacade pstFileFacade;

    @Value("${scan.dir-listing.enabled:true}")
    private boolean dirListingEnabled;

    @Value("${scan.file-share.expand-server-shares.active:false}")
    private Boolean expandServerSharesActive;

    @Value("${scan.file-share.expand-server-shares.dummy-basename:_}")
    private String expandServerSharesDummyBasename;

    @Value("${scan.file-share.preserve-accessed-time.temp-folder:./temp}")
    private String preserveAccessedTimeTempFolder;

    @Value("${scan.file-share.temp-folder.clean-on-startup:false}")
    private boolean cleanTempFolderOnStartup;

    @Value("${scan.dir-listing.page-limit:10000}")
    private int dirListingPageLimit;

    @Value("${scan.file-share.is-diff-active:true}")
    private Boolean isFileShareDiffActive;

    @Value("${scan-progress-report.rate.millis:60000}")
    private int scanProgressReportRate;

    @Value("${scan.concurrency.max-pool-size:0}")
    private int maxScanPoolSize;

    @Value("${scan.concurrency.parallelism:0}")
    private int parallelism;

    @Value("${map.file-share.map-errors-ignore:}")
    private String[] fileShareMapErrorIgnore;

    @Value("${scan.file-share.double-check-is-directory:true}")
    private boolean doubleCheckDirectory;

    @Value("${scan.file-share.force-is-directory-by-attr:true}")
    private boolean forceIsDirectoryByAttributes;

    private ForkJoinPool scanForkJoinPool;

    @Autowired
    private KeyValueDatabaseRepository kvdbRepo;

    private DeletedItemsProcessor deletedItemsProcessor;

    @SuppressWarnings("unused")
    @PostConstruct
    public void init() {
        deletedItemsProcessor = new DeletedItemsProcessor(kvdbRepo);

        FileShareMediaUtils.createTempFolderIfNeeded(preserveAccessedTimeTempFolder);

        if (cleanTempFolderOnStartup) {
            FileShareMediaUtils.cleanFolder(preserveAccessedTimeTempFolder);
        }

        if (fileShareMapErrorIgnore != null && fileShareMapErrorIgnore.length > 0) {
            WindowsPreserveAccessTime.setFileShareMapErrorIgnore(Lists.newArrayList(fileShareMapErrorIgnore));
        }

        WindowsPreserveAccessTime.setDoubleCheckDirectory(doubleCheckDirectory);
        WindowsPreserveAccessTime.setForceIsDirectoryByAttributes(forceIsDirectoryByAttributes);
        logger.info("Init: doubleCheckDirectory={}, forceIsDirectoryByAttributes={}", doubleCheckDirectory?"true":"false", forceIsDirectoryByAttributes?"true":"false");

        if (parallelism > 0) {
            LoggingForkJoinWorkerThreadFactory factory = new LoggingForkJoinWorkerThreadFactory();
            scanForkJoinPool = new ForkJoinPool(parallelism, factory, null, false) {
                @SuppressWarnings("StringBufferReplaceableByString")
                @Override
                public String toString() {
                    final StringBuilder sb = new StringBuilder("{");
                    sb.append("getParallelism=").append(getParallelism());
                    sb.append(", getPoolSize=").append(getPoolSize());
                    sb.append(", getActiveThreadCount=").append(getActiveThreadCount());
                    sb.append(", getStealCount=").append(getStealCount());
                    sb.append(", isQuiescent=").append(isQuiescent());
                    sb.append(", isTerminated=").append(isTerminated());
                    sb.append(", isTerminating=").append(isTerminating());
                    sb.append(", isShutdown=").append(isShutdown());
                    sb.append('}');
                    return sb.toString();
                }
            };
        }
    }


    void startScanMediaFiles(ScanRequestMessage scanRequestMessage) {
        long start = scanRequestMessage.getPayload().getStartScanTime();
        ScanTaskParameters scanParams = scanRequestMessage.getPayload();
        Long taskId = scanRequestMessage.getTaskId();
        validateRequestMessage(scanParams, taskId);
        Long jobId = scanRequestMessage.getJobId();
        long rootFolderId = scanParams.getRootFolderId();
        String storeName = getKvdbRepoName(rootFolderId);
        boolean isScanDeltaMode = isDeltaScanMode(scanRequestMessage);
        scanParams.setFullScan(!isScanDeltaMode);
        Predicate<Pair<Long, Long>> scanActivePredicate = createJobStatePredicate();


        logger.info("Start Scan JobId:{} taskId: {}  according to: {} ", jobId, taskId, scanParams);

        long beforeAction = System.currentTimeMillis();
        AbstractMediaConnector mediaConnector =
                mediaConnectorFactory.getMediaConnector(scanParams.getConnectionDetails());
        if (System.currentTimeMillis() - beforeAction > 2000) {
            logger.debug("getting connection details took {} for run {}", (System.currentTimeMillis() - beforeAction), scanParams.getRunId());
        }

        try {
            Consumer<DirListingPayload> dirListingConsumer = null;
            if (dirListingEnabled) {
                dirListingConsumer = (dirListing -> consumeDirListing(dirListing, scanRequestMessage, isScanDeltaMode));
            }

            Consumer<String> createScanTaskConsumer = null;
            if (scanParams.isFstLevelOnly()) {
                createScanTaskConsumer = (path -> consumeCreateScanTask(path, scanRequestMessage));
            }

            beforeAction = System.currentTimeMillis();
            DirectoryExistStatus directoryExistStatus =
                    fileContentManagerService.getDirectoryExistStatus(scanParams.getRootFolderId(), scanParams.getPath(), true, scanParams.getConnectionDetails());
            if (System.currentTimeMillis() - beforeAction > 2000) {
                logger.debug("testing directory exists took {} for run {}", (System.currentTimeMillis() - beforeAction), scanParams.getRunId());
            }

            boolean limitReached = jobStateMonitor.isJobLimitReached(scanParams.getJobId());
            long progressUnreportedFiles = 0L;
            if (!limitReached) {
                progressUnreportedFiles = executeScan(isScanDeltaMode, mediaConnector, scanRequestMessage, start, dirListingConsumer, createScanTaskConsumer, scanActivePredicate, storeName);
            } else {
                logger.info("limit reached for job {} end scan", scanParams.getJobId());
            }

            // if job wasn't paused during the files streaming - finalize task
            if (jobStateMonitor.isJobActive(jobId, scanParams.getJobStateIdentifier())) {
                finalizeJob(scanRequestMessage, scanParams, taskId, jobId, rootFolderId, isScanDeltaMode, directoryExistStatus, progressUnreportedFiles);
            } else {
                logger.info("Job {} is no longer active, discarding the scan task {}. Current number of active scan tasks: {}",
                        jobId, scanRequestMessage.getTaskId(), mediaProcessorStateService.getActiveScanTasks());
            }
            mediaProcessorStateService.decActiveScanTasks();

        } catch (BreakStreamException e) {
            mediaProcessorStateService.incStartScanTaskRequestsPaused();
            long active = mediaProcessorStateService.decActiveScanTasks();
            logger.info("Job {} is no longer active, discarding the scan task {}. {} active", jobId, scanRequestMessage.getTaskId(), active);
        }
    }

    private void finalizeJob(ScanRequestMessage scanRequestMessage, ScanTaskParameters scanParams, Long taskId, Long jobId,
                             long rootFolderId, boolean isScanDeltaMode, DirectoryExistStatus directoryExistStatus, long progressUnreportedFiles) {
        logger.info("SCAN DONE JobID: {} taskId: {} of root folder ID: {} path: {} done.",
                jobId, taskId, rootFolderId, scanParams.getPath());

        ScanResultMessage scanDoneResultMessage = ScanResultMessage.scanDoneMessage(
                scanParams.getRunId(), rootFolderId, scanParams.isFirstScan(), taskId, jobId, isScanDeltaMode);

        final ScanTaskParameters payload = scanRequestMessage.getPayload();
        scanDoneResultMessage.setPath(payload.getPath());
        scanDoneResultMessage.setRootFolderAccessible(DirectoryExistStatus.YES.equals(directoryExistStatus));

        if (progressUnreportedFiles > 0) {
            scanDoneResultMessage.setProgressUnreportedFiles(progressUnreportedFiles);
        }

        MediaType mediaType = payload.getConnectionDetails().getMediaType();
        if (!mediaType.hasPath()) {
            scanDoneResultMessage.setSyncState(payload.getSyncState());
            scanDoneResultMessage.setMailboxUPN(payload.getMailboxUPN());
        }

        mediaProcessorStateService.setScanEndTime();
        appServerCommService.sendBufferedScanResult(scanDoneResultMessage, true);
        mediaProcessorStateService.incScanResponseTaskFinished();
        mediaProcessorStateService.incStartScanTaskFinished();

        if (mediaType == MediaType.BOX && payload.isFirstScan()) {
            //saving the position counters a hour before the current scan,
            // in order to include all the events which will be added during the scan when we delta scan
            Long rfId = payload.getRootFolderId();
            KeyValuePayloadResp keyValuePayloadResp = new KeyValuePayloadResp(new HashMap<String, String>() {{
                put("box-delta-position.lastScanDate.RF" + rfId,
                        String.valueOf(payload.getStartScanTime() - TimeUnit.HOURS.toMillis(1)));
            }});
            appServerCommService.sendFileContentResponse(keyValuePayloadResp);
        }
    }

    private long executeScan(boolean isScanDeltaMode,
                             FileMediaConnector mediaConnector,
                             ScanRequestMessage scanRequestMessage,
                             long startTime,
                             Consumer<DirListingPayload> dirListingConsumer,
                             Consumer<String> createScanTaskConsumer,
                             Predicate<Pair<Long, Long>> scanActivePredicate,
                             String storeName) {

        ScanTaskParameters scanParams = scanRequestMessage.getPayload();
        ScanProgressReporter filePropsProgressTracker = getScanProgressTracker(scanRequestMessage, startTime, isScanDeltaMode);

        if (scanParams.isGetSharePermissions()) {
            logger.debug("getting share permission for {} run {}", scanParams.getPath(), scanParams.getRunId());
            try {
                List<String> sharePermissions = mediaConnector.getSharePermissions(scanParams.getPath());
                consumeSharePermissions(scanRequestMessage, sharePermissions, null);
                logger.debug("done getting share permission for {} run {}", scanParams.getPath(), scanParams.getRunId());
            } catch (Exception e) {
                logger.error("error getting share permission for {}", scanParams.getPath(), e);
                consumeSharePermissions(scanRequestMessage, null, e.getMessage());
            }
        }

        // The actual file crawling
        boolean scanChanges = isScanDeltaMode && mediaConnector.getMediaType().isSupportsNativeChangeLog();
        if (scanChanges) {
            scanChanges(
                    mediaConnector, scanRequestMessage, startTime, scanParams,
                    createScanTaskConsumer, filePropsProgressTracker);
        } else {
            executeFullScan(
                    isScanDeltaMode, mediaConnector, scanRequestMessage, startTime, dirListingConsumer,
                    createScanTaskConsumer, scanActivePredicate, scanParams, filePropsProgressTracker);

            // if needed search kvdb for deleted files and mark as deleted
            if (isScanDeltaMode) {  // for delta mode also send msg to fc for deleted files
                processDeletedFiles(scanRequestMessage, startTime, storeName, scanParams,
                        (key, deletedItem) -> handleDeletions(key, scanRequestMessage, true));
            } else if (scanRequestMessage.isForceScanFromScratch() && mediaConnector.getMediaType().hasPath()) {
                processDeletedFiles(scanRequestMessage, startTime, storeName, scanParams,
                        (key, deletedItem) -> logger.trace("mark as deleted in mp kvdb {} {}", key, deletedItem));
            }
        }
        return filePropsProgressTracker.getTotalProgressMade();
    }

    private void processDeletedFiles(ScanRequestMessage scanRequestMessage, long startTime,
                                     String storeName, ScanTaskParameters scanParams,
                                     BiConsumer<String, FileEntity> deletedItemHandler) {
        Map<String, Boolean> paths = new HashMap<>();
        paths.put(scanParams.getPath(), scanParams.isFstLevelOnly());
        deletedItemsProcessor.streamDeletedItems(scanRequestMessage.getRunContext(), startTime, storeName,
                new DummyProgressTracker(), paths, deletedItemHandler);
    }

    private void scanChanges(
            FileMediaConnector mediaConnector, ScanRequestMessage requestMessage, long startTime,
            ScanTaskParameters scanParams, Consumer<String> createScanTaskConsumer,
            ProgressTracker filePropsTracker) {

        MediaType mediaType = requestMessage.getPayload().getConnectionDetails().getMediaType();
        Predicate<? super String> fileTypesPredicate = MediaConnectorUtils.createFileTypePredicate(scanParams);

        boolean isFileShare = mediaType == MediaType.FILE_SHARE;
        final Consumer<MediaChangeLogDto> changeConsumer = chg -> {
            String mediaItemId = chg.getMediaItemId();
            if (DiffType.DELETED == chg.getEventType()) {
                handleDeletions(mediaItemId, requestMessage, false);
            } else if (isChangeRecordRelevant(requestMessage, chg, fileTypesPredicate)) {
                consumeFileProperties(chg, requestMessage, true, startTime, filePropsTracker, isFileShare);
            }

        };

        if (parallelism > 0) {
            logger.debug("Scanning changes in path {} in CONCURRENT mode.", scanParams.getPath());
            mediaConnector.concurrentStreamMediaChangeLog(
                    scanForkJoinPool, scanParams, scanParams.getPath(), scanParams.getRunId(),
                    scanParams.getLastScannedOnTimestamp(), changeConsumer, createScanTaskConsumer, fileTypesPredicate);
        } else {
            logger.debug("Scanning changes in path {} in SINGLE-THREADED mode.", scanParams.getPath());
            mediaConnector.streamMediaChangeLog(
                    scanParams, scanParams.getPath(), scanParams.getRunId(), scanParams.getLastScannedOnTimestamp(),
                    changeConsumer, createScanTaskConsumer, fileTypesPredicate);
        }
    }

    private boolean isChangeRecordRelevant(ScanRequestMessage scanRequestMessage, MediaChangeLogDto change,
                                           Predicate<? super String> fileTypesPredicate) {
        MediaType mediaType = scanRequestMessage.getPayload().getConnectionDetails().getMediaType();
        boolean belongsToRootFolder = !mediaType.hasPath() ||  // no need to do the kvdb check below for exchange as we're not using it
                isItemBelongToRootFolder(change.getMediaItemId(), change.getFileName(), scanRequestMessage);
        return belongsToRootFolder && (
                change.getType() == FileType.MAIL ||
                        change.isFolder() ||
                        fileTypesPredicate.test(change.getFileName()));
    }

    private void executeFullScan(boolean isScanDeltaMode,
                                 FileMediaConnector mediaConnector,
                                 ScanRequestMessage scanRequestMessage,
                                 long startTime,
                                 Consumer<DirListingPayload> dirListingConsumer,
                                 Consumer<String> createScanTaskConsumer,
                                 Predicate<Pair<Long, Long>> scanActivePredicate,
                                 ScanTaskParameters scanParams,
                                 ProgressTracker filePropsProgressTracker) {

        Predicate<? super String> fileTypesPredicate = MediaConnectorUtils.createFileTypePredicate(scanParams);
        boolean isFileShare = scanRequestMessage.getPayload().getConnectionDetails().getMediaType() == MediaType.FILE_SHARE;
        Consumer<ClaFilePropertiesDto> filePropsConsumer = fileProps -> mapMediaItem(
                fileProps, fileTypesPredicate, scanRequestMessage, isScanDeltaMode, startTime, filePropsProgressTracker, isFileShare);
        // if pool size is > 0 scan with concurrency
        if (parallelism > 0) {
            // if (logger.isDebugEnabled()) { // no String concat, therefor this cond is redundant.
            logger.debug("Scanning path {} in CONCURRENT mode (x{}, maxScanPoolSize={}, scanForkJoinPool={}).",
                    scanParams.getPath(), parallelism, maxScanPoolSize, scanForkJoinPool.toString());
            //}
            mediaConnector.concurrentStreamMediaItems(scanForkJoinPool, scanParams, filePropsConsumer,
                    dirListingConsumer, createScanTaskConsumer, scanActivePredicate, filePropsProgressTracker);
        } else {// if pool size == 0 scan in one thread
            logger.debug("Scanning path {} in SINGLE-THREADED mode.", scanParams.getPath());
            mediaConnector.streamMediaItems(scanParams, filePropsConsumer, dirListingConsumer, createScanTaskConsumer,
                    scanActivePredicate, filePropsProgressTracker);
        }
    }

    private void mapMediaItem(ClaFilePropertiesDto props, Predicate<? super String> fileTypesPredicate,
                              ScanRequestMessage scanRequestMessage, boolean isScanDeltaMode, long startTime,
                              ProgressTracker filePropsProgressTracker, boolean isFileShare) {

        Long runId = scanRequestMessage.getRunContext();
        String pathName = props.getFileName();
        if (props.isFolder() || fileTypesPredicate.test(pathName)) {
            Consumer<ClaFilePropertiesDto> legacyConsumer = fileProps -> consumeFileProperties(
                    fileProps, scanRequestMessage, isScanDeltaMode, startTime, filePropsProgressTracker, isFileShare);

            FileType fileType = FileTypeUtils.getFileType(pathName);
            switch (fileType) {
                case PST:
                    legacyConsumer.accept(props); // Send a message for the pst file itself
                    pstFileFacade.mapPstFile(props, fileTypesPredicate, legacyConsumer, runId);
                    break;
                default:
                    // consume file properties
                    legacyConsumer.accept(props);
            }
        } else {
            logger.debug("File type of the current path ({}) is ignored, skipping.", pathName);
            filePropsProgressTracker.incrementProgress(0); // we call this so we'll eventually send scan progress if needed to server
        }
    }

    private Predicate<Pair<Long, Long>> createJobStatePredicate() {
        return data -> jobStateMonitor.isJobActive(data.getKey(), data.getValue()) && !jobStateMonitor.isJobLimitReached(data.getKey());
    }

    private void sendScanProgressMsg(Long progress, ScanRequestMessage scanRequestMessage, boolean isDeltaScan) {
        ScanProgressPayload payload = new ScanProgressPayload(progress);
        consumePayload(payload, scanRequestMessage, isDeltaScan, false);
    }

    private void handleDeletions(String filename, ScanRequestMessage scanRequestMessage, boolean isDeltaScan) {
        ItemDeletionPayload deletionPayload = new ItemDeletionPayload(filename);
        consumePayload(deletionPayload, scanRequestMessage, isDeltaScan, true);
    }

    private boolean isItemBelongToRootFolder(String mediaItemId, String filename, ScanRequestMessage scanRequestMessage) {
        ScanTaskParameters scanParams = scanRequestMessage.getPayload();
        long rootFolderId = scanParams.getRootFolderId();
        String rootFolderPath = scanParams.getPath() + (scanParams.getPath().endsWith("/") ? "" : "/");
        String storeName = getKvdbRepoName(rootFolderId);

        return kvdbRepo.getFileEntityDao().executeInTransaction(scanRequestMessage.getRunContext(), storeName, store ->
                ofNullable(store.get(mediaItemId))
                        .isPresent()
                        || ofNullable(filename)
                        .orElse("")
                        .startsWith(rootFolderPath)
        );
    }

    private void consumeFileProperties(ClaFilePropertiesDto fileProps, ScanRequestMessage scanRequestMessage,
                                       boolean isScanDeltaMode, long time, ProgressTracker progressTracker,
                                       boolean isFileShare) {

        ScanTaskParameters scanParams = scanRequestMessage.getPayload();

        if (!jobStateMonitor.isJobActive(scanParams.getJobId(), scanParams.getJobStateIdentifier())) {
            throw new BreakStreamException("Job " + scanParams.getJobId() + " became inactive, aborting.");
        } else if (jobStateMonitor.isJobLimitReached(scanParams.getJobId())) {
            logger.debug("Job " + scanParams.getJobId() + " limit reached, aborting.");
            return;
        }

        long rootFolderId = scanParams.getRootFolderId();
        updateFileEntityStore(rootFolderId, scanRequestMessage.getRunContext(), scanRequestMessage.getPayload().getConnectionDetails().getMediaType().isPathAsMediaEntity(),
                fileProps, time, isScanDeltaMode,
                affected -> {
                    // Case we have error from OD/SP than we would like to consume them as scan errors as well.
                    boolean possibleChangeErrors = scanParams.getConnectionDetails()
                            .getMediaType().isSupportsNativeChangeLog() && !fileProps.getErrors().isEmpty();
                    if (possibleChangeErrors || affected || !isScanDeltaMode) {
                        consumePayload(fileProps, scanRequestMessage, isScanDeltaMode, true);
                    } else if (fileProps.isFile()) {
                        progressTracker.incrementProgress(1L);
                    }
                }, isFileShare);
    }

    private void updateFileEntityStore(long rootFolderId, Long runId, boolean isPathAsMediaEntity, ClaFilePropertiesDto fileProps, long time, boolean isScanDeltaMode,
                                       Consumer<Boolean> followupAction, boolean isFileShare) {

        String storeName = getKvdbRepoName(rootFolderId);
        Boolean outerAffected = kvdbRepo.getFileEntityDao().executeInTransaction(runId, storeName, store ->
                updateFileEntityStore(fileProps, time, store, isScanDeltaMode, isPathAsMediaEntity, isFileShare));

        // Execute the followup action outside the 'executeInTransaction()' because that handler may be activated multiple times.
        if (followupAction != null) {
            followupAction.accept(outerAffected);
        }
    }

    private ScanProgressReporter getScanProgressTracker(ScanRequestMessage scanRequestMessage, long startTime, boolean isDeltaScan) {
        Consumer<Long> reporter = progress -> {
            sendScanProgressMsg(progress, scanRequestMessage, isDeltaScan);
            if (progress > 0) {
                // if there was some progress - update last scan response time
                // in order to avoid system restart when no messages were sent for a long time
                mediaProcessorStateService.markLastScanResponseProduced();
            }
        };
        return new ScanProgressReporter(startTime, scanProgressReportRate, reporter);
    }

    private String getKvdbRepoName(long rootFolderId) {
        return kvdbRepo.getRepoName(rootFolderId, "media-proc");
    }

    private boolean updateFileEntityStore(ClaFilePropertiesDto fileProps, long time, FileEntityStore store, boolean isScanDeltaMode, boolean isPathAsMediaEntity, boolean isFileShare) {

        String kvdbKey = ofNullable(fileProps.getMediaItemId()).orElse(fileProps.getFileName());
        FileEntity oldFileEntity = store.get(kvdbKey);
        DiffType diffType = FileEntityUtils.detectChanges(!isScanDeltaMode, oldFileEntity, fileProps, isFileShare);

        logger.trace("diff type detected {} for key {}", diffType, kvdbKey);

        boolean diffDetected = true;

        switch (diffType) {
            case SIMILAR:
                diffDetected = false;
                oldFileEntity.setDeleted(false);
                break;
            case DELETED:
                oldFileEntity.setDeleted(true);
                break;
            default:
                oldFileEntity = null;
                String pathForKvdb = (isPathAsMediaEntity ? null : fileProps.getFileName());
                FileEntity fileEntity = FileEntityUtils.createFileEntity(null, null, time, fileProps, pathForKvdb);
                store.put(kvdbKey, fileEntity);
                logger.trace("**** Media proc side: Added new file entity (name: {}, file id:{}, folder id: {})", fileProps.getFileName(), fileEntity.getFileId(), fileEntity.getFolderId());
        }

        // if was not a new file - update last scanned time in KVDB
        if (oldFileEntity != null) {
            oldFileEntity.setLastScanned(time);
            store.put(kvdbKey, oldFileEntity);
        }

        return diffDetected;
    }

    /**
     * Get a dir listing of a folder from the media connector, sort and send.
     * If exceeds page size, split to pages.
     *
     * @param dirListing         dir listing payload with all the files/sub-folders, unlimited size
     * @param scanRequestMessage original request message
     */
    private void consumeDirListing(DirListingPayload dirListing, ScanRequestMessage scanRequestMessage, boolean isDeltaScan) {
        Stream<String> sortedListing = dirListing.getDirectoryList().stream().sorted(new XodusStringComparator());

        final DirListingPayload[] newDirListing = {DirListingPayload.create()
                .setFolderPath(dirListing.getFolderPath())};

        if (dirListing.size() > dirListingPageLimit) {
            final int[] page = {1};
            final int[] counter = {0};

            newDirListing[0].setPage(page[0]).setLastPage(false);

            sortedListing.forEach(path -> {
                newDirListing[0].addPath(path);
                counter[0]++;
                // if this is the last path - mark as last page and send
                if (counter[0] >= dirListing.size()) {
                    newDirListing[0].setLastPage(true);
                    consumePayload(newDirListing[0], scanRequestMessage, isDeltaScan, false);
                }
                // if the page exceeded the limit, send it and start a new one
                if (newDirListing[0].size() >= dirListingPageLimit) {
                    consumePayload(newDirListing[0], scanRequestMessage, isDeltaScan, false);
                    // last path of the previous listing is always the first of the next one
                    // in order to be able to detect gaps between sorted elements
                    newDirListing[0] = DirListingPayload.create()
                            .setFolderPath(dirListing.getFolderPath())
                            .setPage(++page[0])
                            .setLastPage(false)
                            .addPath(path);
                }
            });
        } else {
            consumePayload(newDirListing[0].setLastPage(true).setDirectoryList(sortedListing.collect(Collectors.toList())),
                    scanRequestMessage, isDeltaScan, false);
        }

    }

    private void consumeSharePermissions(ScanRequestMessage scanRequestMessage, List<String> sharePermissions, String errorText) {
        ScanResultMessage msg = new ScanResultMessage();
        msg.setPath(scanRequestMessage.getPayload().getPath());
        msg.setRunContext(scanRequestMessage.getRunContext());
        msg.setTaskId(scanRequestMessage.getTaskId());
        msg.setJobId(scanRequestMessage.getJobId());
        msg.setRootFolderId(scanRequestMessage.getPayload().getRootFolderId());
        ScanSharePermissions perm = ScanSharePermissions.create()
                .setRootFolderId(scanRequestMessage.getPayload().getRootFolderId())
                .setSharePermissions(sharePermissions)
                .setErrorText(errorText)
                ;
        msg.setPayload(perm);
        msg.setPayloadType(MessagePayloadType.SCAN_SHARE_PERMISSIONS);
        appServerCommService.sendScanResult(msg);
    }

    private void consumeCreateScanTask(String path, ScanRequestMessage scanRequestMessage) {
        ScanResultMessage msg = new ScanResultMessage();
        msg.setPath(path);
        msg.setTaskId(scanRequestMessage.getTaskId());
        msg.setJobId(scanRequestMessage.getJobId());
        msg.setRunContext(scanRequestMessage.getRunContext());
        msg.setPayload(scanRequestMessage.getPayload());
        msg.setPayloadType(MessagePayloadType.SCAN_CREATE_TASK);
        msg.setFirstScan(scanRequestMessage.getPayload().isFirstScan());
        appServerCommService.sendScanResult(msg);
    }

    private void consumePayload(PhaseMessagePayload payload,
                                ScanRequestMessage scanRequestMessage, boolean isDeltaScan, boolean buffer) {
        Long jobId = scanRequestMessage.getJobId();
        ScanTaskParameters scanParams = scanRequestMessage.getPayload();

        if (jobStateMonitor.isJobActive(jobId, scanParams.getJobStateIdentifier())) {

            ScanResultMessage scanResultMessage = new ScanResultMessage(payload, scanParams.getRootFolderId(),
                    scanParams.isFirstScan(), scanRequestMessage.getRunContext(), scanRequestMessage.getTaskId(), jobId);
            scanResultMessage.setDeltaScan(isDeltaScan);
            if (buffer) {
                appServerCommService.sendBufferedScanResult(scanResultMessage, false);
            } else {
                appServerCommService.sendScanResult(scanResultMessage);
            }

            mediaProcessorStateService.incScanResponseTaskFinished();
        } else {
            logger.debug("Job {} was stopped. ", jobId);
        }
    }

    private void validateRequestMessage(ScanTaskParameters scanParameters, Long taskId) {
        if (scanParameters == null) {
            throw new RuntimeException("Unable to handle start scan request message - null scanParams. taskId: " + taskId);
        }
        ScanTypeSpecification scanTypeSpecification = scanParameters.getScanTypeSpecification();
        List<FileType> typesToScan = scanTypeSpecification.getTypesToScan();
        if (typesToScan == null) {
            throw new RuntimeException("Illegal startScanMediaFiles request - null typesToScan. taskId: " + taskId);
        }
        if (scanParameters.getConnectionDetails() == null) {
            throw new RuntimeException("Illegal startScanMediaFiles request - null connectionDetails. taskId: " + taskId);
        }


    }

    /**
     * A central place to determine the scan mode.
     * Scan mode can be either delta scan (sending messages only for files that have changed and then detecting deletions)
     * or from-scratch scan (sending messages for all scanned files).
     * <p>
     * The scan mode depends on the MediaType, on a property, on a flag in the scan request and on
     * whether or not a cache of a previous scan exists.
     *
     * @return true if in delta mode false if in from-scratch mode
     */
    private boolean isDeltaScanMode(ScanRequestMessage scanRequestMessage) {

        MediaType type = ofNullable(scanRequestMessage.getPayload().getConnectionDetails().getMediaType())
                .orElse(MediaType.FILE_SHARE);

        if (type == MediaType.EXCHANGE_365) {
            // Don't perform the checks below for Exchange as it always should perform a (native) delta scan
            return true;
        }

        if (scanRequestMessage.getPayload().isFirstScan()) {
            return false;
        }

        // Either MP requested by app-server to full rescan, or if connector supports change-log,
        // it should run with method requested by app-server (since some connectors do not rely on mp's kvdb for delta scans.)
        if (scanRequestMessage.isForceScanFromScratch() || type.isSupportsNativeChangeLog()) {
            logger.debug("Determined scan: {} (type={})", (scanRequestMessage.isForceScanFromScratch() ? "Full" : "Delta"), type);
            return !scanRequestMessage.isForceScanFromScratch();
        }
        if (!isFileShareDiffActive) {
            logger.debug("File-delta-scan is disabled by property");
            return false;
        }

        ScanTaskParameters scanParams = scanRequestMessage.getPayload();
        Long rootFolderId = scanParams.getRootFolderId();
        String storeName = getKvdbRepoName(rootFolderId);

        boolean storeExists = false;
        List<Boolean> folderExistsList = new ArrayList<>();
        if (kvdbRepo != null && kvdbRepo.getFileEntityDao() != null) {
            storeExists = kvdbRepo.getFileEntityDao().storeExists(storeName);
            if (storeExists) {
                // check that specific path is already known
                // this is for multi task map 
                kvdbRepo.getFileEntityDao().executeInReadonlyTransaction(storeName, fileEntityStore -> {
                    FileEntity fileEntity = fileEntityStore.get(scanParams.getPath());
                    folderExistsList.add(fileEntity != null && fileEntity.getLastScanned() != null);
                    return true;
                });
            }
        }
        boolean folderExists = !folderExistsList.isEmpty() && folderExistsList.get(0);
        return scanParams.getLastScannedOnTimestamp() != 0 && storeExists && folderExists;
    }

    void sendScanFailedResponse(ScanRequestMessage scanRequestMessage, String cause, Boolean rootFolderAccessible) {
        try {
            ScanTaskParameters scanTaskParameters = scanRequestMessage.getPayload();
            logger.info("SCAN DONE FAILED of {} failed. Send SCAN_DONE message with FAILED status", scanTaskParameters.getPath());
            ScanResultMessage scanResultMessage = ScanResultMessage.scanFailedMessage(scanTaskParameters.getRunId(),
                    scanTaskParameters.getRootFolderId(), scanTaskParameters.isFirstScan(),
                    scanRequestMessage.getTaskId(), scanRequestMessage.getJobId());
            scanResultMessage.setPath(scanTaskParameters.getPath());
            scanResultMessage.setErrorMessage(cause);
            if (rootFolderAccessible != null) {
                scanResultMessage.setRootFolderAccessible(rootFolderAccessible);
            }
            appServerCommService.sendBufferedScanResult(scanResultMessage, true);
            mediaProcessorStateService.incScanFailedResponseTaskFinished();
        } catch (Exception e) {
            logger.error("Failed to send SCAN FAILED RESPONSE", e);
        }

    }

    /**
     * ForkJoin thread factory that produces thread with specific prefix.
     * The produced thread also logs its creation and termination.
     */
    final class LoggingForkJoinWorkerThreadFactory implements ForkJoinPool.ForkJoinWorkerThreadFactory {
        @Override
        public final ForkJoinWorkerThread newThread(ForkJoinPool pool) {
            if (maxScanPoolSize == 0 || pool.getPoolSize() < maxScanPoolSize) {
                final ForkJoinWorkerThread worker = new LogginForkJoinWorkerThread(pool);
                worker.setName("scan-fork-" + worker.getPoolIndex());
                return worker;
            } else {
                if (logger.isTraceEnabled()) {
                    logger.trace("Blocked attempt to create a new thread, pool size: {}, max pool size: {}",
                            pool.getPoolSize(), maxScanPoolSize);
                }
                return null;
            }
        }
    }

    /**
     * Thread that logs its creation and termination
     */
    final class LogginForkJoinWorkerThread extends ForkJoinWorkerThread {
        LogginForkJoinWorkerThread(ForkJoinPool pool) {
            super(pool);
        }

        @Override
        protected void onTermination(Throwable exception) {
            if (logger.isTraceEnabled()) {
                logger.trace("Terminating thread {} with state {}, exception: {}",
                        this.getName(), this.getState(), exception);
            }
            super.onTermination(exception);
        }

        @Override
        protected void onStart() {
            super.onStart();
            if (logger.isTraceEnabled()) {
                logger.trace("Started thread {} with state {}", this.getName(), this.getState());
            }
        }
    }
}
