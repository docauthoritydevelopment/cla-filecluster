package com.cla.mediaproc.rest;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.joran.JoranConfigurator;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.Appender;
import ch.qos.logback.core.joran.spi.JoranException;
import com.cla.common.domain.dto.filetree.RsaKey;
import com.cla.filecluster.util.LocalFileUtils;
import com.cla.mediaproc.appserver.AppServerProcessingService;
import com.cla.mediaproc.appserver.connectors.ResultMessagesHandler;
import com.cla.mediaproc.service.managers.EncryptionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Response;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.PrivateKey;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.zip.ZipOutputStream;

/**
 * Controller for the Media Processor administration
 * Created by vladi on 1/31/2017.
 */
@RestController
@RequestMapping(value = "/management")
public class ManagementController {

    private static Logger logger = LoggerFactory.getLogger(ManagementController.class);

    @Autowired
    private AppServerProcessingService appServerProcessingService;

    @Autowired
    private ResultMessagesHandler appServerCommService;

    @Autowired
    private EncryptionManager encryptionManager;

    @Value("${logging.config}")
    private String logbackConfigPath;

    @RequestMapping(value = "/logs/fetch", method = RequestMethod.GET)
    public void fetchLogs(HttpServletResponse response) {
        logger.info("Fetch log files from FileCluster");
        flushLogs();
        try {
            zipFilesToResponse(response, "DA_logs", "./logs", ".*\\.log(\\.[1-5])?");
            logger.debug("log files fetched");
        } catch (IOException ex) {
            logger.info("Error writing log files to output stream.", ex);
            throw new RuntimeException("IOError writing log files to output stream");
        }
    }

    @RequestMapping(value = "/logs/reloadConfig", method = RequestMethod.GET)
    public String reloadLogsConfig() {
        LoggerContext context = (LoggerContext) LoggerFactory.getILoggerFactory();
        context.reset();
        JoranConfigurator configurator = new JoranConfigurator();
        configurator.setContext(context);
        try {
            configurator.doConfigure(logbackConfigPath);
        } catch (JoranException e) {
            logger.info("Error reloading logs config", e);
            throw new RuntimeException("Error reloading log files config");
        }
        logger.info("Logs config reloaded according to {}", logbackConfigPath);
        return "Logs config reloaded!";
    }

    @RequestMapping(value = "/healthCheck", method = RequestMethod.GET)
    public Object healthCheck() {
        return Response.ok().header("LAST_ACTIVE_DATE", appServerCommService.getLastResponseDate()).build();
    }

    @RequestMapping(value = "/loadKey", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public void addPrivateKey(@RequestBody RsaKey keys, @RequestParam(value = "password") String keyStorePassword) {
        try {
            encryptionManager.savePrivateKey(keys, keyStorePassword);
        } catch (Exception e) {
            logger.info("Error setting new private key for data center.", e);
            throw new RuntimeException("Error setting new private key for data center "+e.getMessage());
        }
    }

    @RequestMapping(value = "/readKey", method = RequestMethod.GET)
    public void retrievePrivateKey() {
        try {
            PrivateKey key = encryptionManager.retrievePrivateKey();
            if (key != null && key.getAlgorithm() != null) {
                logger.info("read private key for data center");
            } else {
                throw new RuntimeException("Error reading private key for data center - empty");
            }
        } catch (Exception e) {
            logger.info("Error reading private key for data center.", e);
            throw new RuntimeException("Error reading private key for data center "+e.getMessage());
        }
    }

    @RequestMapping(value = "/logs/flush", method = RequestMethod.GET)
    public void flushLogs() {
        if (!flushLogback()) {
            for (int i = 0; i < 65; i++) {
                logger.debug("*************************************************************************************************************");
            }
        }
    }

    private boolean flushLogback() {
        boolean res = false;
        if (logger instanceof ch.qos.logback.classic.Logger) {
            res = true;
            ch.qos.logback.classic.Logger lbLogger = (ch.qos.logback.classic.Logger) logger;
            Map<String, Integer> ftMap = new HashMap<>();
            Iterator<Appender<ILoggingEvent>> appendersIterator = lbLogger.iteratorForAppenders();
            while (appendersIterator.hasNext()) {
                Appender<ILoggingEvent> appender = appendersIterator.next();
                if (appender instanceof ch.qos.logback.core.AsyncAppenderBase) {
                    ch.qos.logback.core.AsyncAppenderBase<ILoggingEvent> lbApp = (ch.qos.logback.core.AsyncAppenderBase<ILoggingEvent>) appender;
                    ftMap.put(lbApp.getName(), lbApp.getMaxFlushTime());
                    lbApp.setMaxFlushTime(0);
                } else {
                    logger.info("Found appender {} of class {}", appender.getName(), appender.getClass());
                }
            }
            lbLogger.info("*****************************************************************************************");
            lbLogger.info("****************************** Flushing logger ************************ ({})",
                    ftMap.entrySet().stream()
                            .map(e -> e.getKey() + ":" + e.getValue())
                            .collect(Collectors.joining(",", "{", "}")));
            lbLogger.info("*****************************************************************************************");
            appendersIterator = lbLogger.iteratorForAppenders();
            while (appendersIterator.hasNext()) {
                Appender<ILoggingEvent> appender = appendersIterator.next();
                if (appender instanceof ch.qos.logback.core.AsyncAppenderBase) {
                    ch.qos.logback.core.AsyncAppenderBase<ILoggingEvent> lbApp = (ch.qos.logback.core.AsyncAppenderBase<ILoggingEvent>) appender;
                    Integer mft = ftMap.get(lbApp.getName());
                    if (mft != null) {
                        logger.debug("Restore appender {} to {}", lbApp.getName(), mft);
                        lbApp.setMaxFlushTime(mft);
                    } else {
                        logger.debug("Could not Restore appender {} MaxFlushTime", lbApp.getName());
                    }
                }
            }
        }
        return res;
    }

    private void zipFilesToResponse(HttpServletResponse response, String zipFilePrefix, String path, String fileSpecRegex) throws IOException {
        // set headers for the response
        response.setContentType("application/zip");
        final Date curDate = new Date();
        final SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd_hhmmss");
        final String suffix = format.format(curDate);
        final String headerKey = "Content-Disposition";
        final String headerValue = String.format("attachment; filename=\"%s.%s.zip\"", zipFilePrefix, suffix);
        response.setHeader(headerKey, headerValue);

        final Pattern logFilePattern = Pattern.compile(fileSpecRegex);

        ZipOutputStream zipOutputStream = new ZipOutputStream(new BufferedOutputStream(response.getOutputStream()));
        try (Stream<Path> logFileList = Files.list(Paths.get(path))) {
            logger.debug("zip each log file in <{}> into {}", fileSpecRegex, headerValue);
            logFileList.filter(p -> logFilePattern.matcher(p.getFileName().toString()).matches())
                    .forEach(f -> {
                        try {
                            LocalFileUtils.addFileToZip(f, zipOutputStream);
                        } catch (IOException e) {
                            logger.error("Failed to add log file " + f + " to zip stream", e);
                        }
                    });
        }
        logger.debug("flush buffers");
        response.flushBuffer();
        logger.debug("close zip stream");
        zipOutputStream.close();
    }

    @RequestMapping(value = "/status/send", method = RequestMethod.GET)
    public void sendStatusReport() {
        appServerProcessingService.sendStatusReport();
    }

}
