package com.cla.mediaproc.actions.labeling;

import com.cla.common.domain.dto.messages.LabelRequestMessage;
import com.cla.common.domain.dto.messages.LabelingResponseMessage;
import com.cla.connector.mediaconnector.MediaConnector;

public abstract class LabelingAction<T extends MediaConnector> {

    protected T mediaConnector;
    protected String targetPathForLabelWrite;

    public LabelingAction(T mediaConnector) {
        this.mediaConnector = mediaConnector;
    }

    public abstract LabelingResponseMessage retrieveLabel(LabelRequestMessage labelRequestMessage);

    public abstract LabelingResponseMessage applyLabel(LabelRequestMessage labelRequestMessage);

    public String getTargetPathForLabelWrite() {
        return targetPathForLabelWrite;
    }

    public void setTargetPathForLabelWrite(String targetPathForLabelWrite) {
        this.targetPathForLabelWrite = targetPathForLabelWrite;
    }
}
