package com.cla.mediaproc.actions.labeling.mip;

import com.cla.common.domain.dto.messages.LabelRequestMessage;
import com.cla.common.domain.dto.messages.LabelTaskParameters;
import com.cla.common.domain.dto.messages.LabelingError;
import com.cla.common.domain.dto.messages.LabelingResponseMessage;
import com.cla.connector.domain.dto.error.ProcessingErrorType;
import com.cla.connector.mediaconnector.labeling.LabelResult;
import com.cla.connector.mediaconnector.labeling.RawLabelingError;
import com.cla.connector.mediaconnector.labeling.SetFileLabelOp;
import com.cla.connector.mediaconnector.labeling.mip.MipLabelResult;
import com.cla.connector.mediaconnector.labeling.mip.MipLabelResultTyped;
import com.cla.connector.mediaconnector.labeling.mip.MipMediaConnector;
import com.cla.mediaproc.actions.labeling.LabelingAction;
import com.google.common.base.Strings;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class MipLabelingAction extends LabelingAction<MipMediaConnector> {

    private final static Logger logger = LoggerFactory.getLogger(MipLabelingAction.class);

    public MipLabelingAction(MipMediaConnector mediaConnector) {
        super(mediaConnector);
    }

    @Override
    public LabelingResponseMessage retrieveLabel(LabelRequestMessage labelRequestMessage) {
        LabelTaskParameters payload = labelRequestMessage.getPayload();
        LabelingResponseMessage labelingResponseMessage = new LabelingResponseMessage(payload);
        String filePath = payload.getMediaEntityId();

        logger.trace("Retrieving MIP label for path: {} file ID: {}", filePath, payload.getFileId());
        MipLabelResult[] fileLabels = mediaConnector.getFileLabel(filePath);
        if (fileLabels != null) {
            List<LabelResult> results = new ArrayList<>();
            for (MipLabelResult label : fileLabels) {
                results.add(new MipLabelResultTyped(label));
            }
            labelingResponseMessage.setExternalMetadata(results);
        }
        return labelingResponseMessage;
    }

    @Override
    public LabelingResponseMessage applyLabel(LabelRequestMessage labelRequestMessage){
        LabelTaskParameters payload = labelRequestMessage.getPayload();
        LabelingResponseMessage response = new LabelingResponseMessage(payload);
        String filePath = payload.getMediaEntityId();

        logger.trace("Setting MIP label for path: {} file ID: {}", filePath, payload.getFileId());

        SetFileLabelOp op = new SetFileLabelOp();
        op.setFilePath(filePath);
        op.setLabelName(payload.getNativeId());

        if (!Strings.isNullOrEmpty(targetPathForLabelWrite)) {
            op.setNewFilePath(FilenameUtils.concat(targetPathForLabelWrite, FilenameUtils.getName(filePath)));
        }

        // todo: add info to the response when the outcome is positive as well
        RawLabelingError[] err = mediaConnector.setFileLabel(op);
        if (err != null && err.length > 0) {
            LabelingError responseError = new LabelingError();
            responseError.setExceptionText(err[0].getErrorMessage());
            // todo: convert type properly
            responseError.setErrorType(ProcessingErrorType.ERR_UNKNOWN);
            response.addError(responseError);
            logger.info("got error {} writing label {} to file {}", responseError.getExceptionText(), op.getLabelName(), payload.getFileId());
        }
        return response;
    }

}
