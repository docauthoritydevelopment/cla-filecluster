package com.cla.mediaproc.service.ingest;

import com.cla.common.domain.dto.messages.IngestRequestMessage;
import com.cla.common.domain.dto.messages.IngestTaskParameters;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.file.FileContentDto;
import com.cla.connector.domain.dto.media.MediaConnectionDetailsDto;
import com.cla.connector.utils.Pair;
import com.cla.mediaproc.appserver.GeneralIngestRequestHandlerProvider;
import com.cla.mediaproc.service.filecontent.FileContentManagerService;
import org.apache.commons.lang3.tuple.Triple;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Provide data for ingestion for non package file - extractFileData is via fileContentManagerService
 */
@Service
public class MediaEntryIngestRequestHandler extends ContentIngestHandler {

    private static final Logger logger = LoggerFactory.getLogger(MediaEntryIngestRequestHandler.class);

    @Autowired
    private FileContentManagerService fileContentManagerService;

    @Override
    protected Pair<FileContentDto, ? extends ClaFilePropertiesDto> extractFileData(IngestRequestMessage ingestRequestMessage) throws IOException {
        IngestTaskParameters ingestTaskParameters = ingestRequestMessage.getPayload();
        String fileSearchIdentifier = GeneralIngestRequestHandlerProvider.resolveFileSearchIdentifier(ingestTaskParameters);
        return fileContentManagerService.getFileContent(ingestTaskParameters.getFileType(),
                fileSearchIdentifier, ingestTaskParameters.getMediaConnectionDetailsDto(), false);
    }

    @Override
    protected List<Triple<IngestTaskParameters, FileContentDto, ? extends ClaFilePropertiesDto>> extractContainerFileData(
            IngestRequestMessage ingestRequestMessage) throws IOException {

        IngestTaskParameters containerFilePayload = ingestRequestMessage.getPayload();
        List<IngestTaskParameters> containedFilesPayloads = ingestRequestMessage.getContainedFilesPayloads();
        MediaConnectionDetailsDto connectionDetails = containerFilePayload.getMediaConnectionDetailsDto();
        String containerFileSearchIdentifier =
                GeneralIngestRequestHandlerProvider.resolveFileSearchIdentifier(containerFilePayload);

        Map<String, IngestTaskParameters> fileIdToPayloadMap = mapPayloadsByFileId(containedFilesPayloads);
        fileIdToPayloadMap.put(containerFileSearchIdentifier, containerFilePayload);
        Set<String> filesIds = Collections.unmodifiableSet(fileIdToPayloadMap.keySet());
        Map<String, Pair<FileContentDto, ClaFilePropertiesDto>> containerFileContent =
                fileContentManagerService.getContainerFileContent(
                        containerFileSearchIdentifier, filesIds, connectionDetails);
        // TODO Itai: go over results and report deleted/ingestionError

        return containerFileContent
                .entrySet()
                .stream()
                .map(midToFileDataEntry -> {
                    String fileMid = midToFileDataEntry.getKey();
                    IngestTaskParameters fileIngestPayload = fileIdToPayloadMap.get(fileMid);
                    return Triple.of(
                            fileIngestPayload,
                            midToFileDataEntry.getValue().getKey(),
                            midToFileDataEntry.getValue().getValue());
                })
                .collect(Collectors.toList());
    }

    private Map<String, IngestTaskParameters> mapPayloadsByFileId(List<IngestTaskParameters> ingestPayloads) {
        Map<String, IngestTaskParameters> result = new HashMap<>(ingestPayloads.size());
        ingestPayloads.forEach(ingestPayload -> {
            String fileSearchIdentifier = GeneralIngestRequestHandlerProvider.resolveFileSearchIdentifier(ingestPayload);
            if (result.containsKey(fileSearchIdentifier)) {
                IngestTaskParameters alreadyExistingPayload = result.get(fileSearchIdentifier);
                logger.warn("Got ingest request for a container file with duplicate contained file id: {}. Skipping the second one. First payload: {}, second payload: {}",
                        fileSearchIdentifier, alreadyExistingPayload, ingestPayload);
            } else {
                result.put(fileSearchIdentifier, ingestPayload);
            }
        });
        return result;
    }

}
