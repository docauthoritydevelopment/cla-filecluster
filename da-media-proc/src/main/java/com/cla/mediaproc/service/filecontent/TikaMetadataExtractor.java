package com.cla.mediaproc.service.filecontent;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.AutoDetectParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;


public class TikaMetadataExtractor implements MetadataExtractor {

	private static final Logger logger = LoggerFactory.getLogger(TikaMetadataExtractor.class);
	private final ContentHandler DUMMY_BODY_PARSE_HANDLER = new DefaultHandler();
	private static final String TIKA_PARSER_PROPERTY = "X-Parsed-By";

	@Override
	public Map<String, String> extractLocal(InputStream inputStream) {
		try {
			Metadata metadata = extractMeta(inputStream);
			return Arrays.stream(metadata.names())
					.filter(name -> !TIKA_PARSER_PROPERTY.equals(name))
					.distinct()
					.collect(Collectors.toMap(this::stripTikaMarker, metadata::get, (v1, v2) -> v1));
		} catch (Exception e) {
			logger.error("Failed extracting metadata", e);
		}
		return ImmutableMap.of();
	}

	@Override
	public Map<String, String> extractExternal() {
		return Maps.newHashMap();
	}

	private Metadata extractMeta(InputStream inputStream) throws IOException, TikaException, SAXException {
		Metadata metadata = new Metadata();
		AutoDetectParser parser = new AutoDetectParser();//MAYBE MAKE A CONST FIELD? BUT IS IT THREAD SAFE?
		parser.parse(inputStream, DUMMY_BODY_PARSE_HANDLER, metadata);
		return metadata;
	}

	private String stripTikaMarker(String property){
		return Strings.isNullOrEmpty(property) ? property :
				property.substring(property.indexOf(':') + 1);
	}
}
