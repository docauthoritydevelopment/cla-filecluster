package com.cla.mediaproc.service.managers;

import com.cla.connector.utils.TimeSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedResource;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicLong;

@Component
@ManagedResource(objectName = "cla:name=mediaProcessorStateService")
public class MediaProcessorStateService {

    private static final Logger logger = LoggerFactory.getLogger(MediaProcessorStateService.class);

    private static final String VERSION_PROPERTIES_FILE = "." + File.separator + "config" + File.separator + "version.properties";

    private long duplicateCounter = 0;
    private long scanStartTime = 0;
    private Long scanEndTime;

    private long timeSpentFindMatchesInPotentialCandidatesWord = 0L;

    private double averageScanThroughput = 0f;
    private float scanRunningThroughput = 0f;

    private double ingestRunningThroughput =0;
    private double averageIngestThroughput =0;

    private AtomicLong startScanTaskRequests = new AtomicLong(0);
    private AtomicLong activeScanTasks = new AtomicLong(0);
    private AtomicLong startScanTaskRequestsFinished = new AtomicLong(0);
    private AtomicLong startScanTaskRequestsPaused = new AtomicLong(0);
    private AtomicLong scanTaskResponsesFinished  = new AtomicLong(0);
    private AtomicLong scanTaskFailedResponsesFinished  = new AtomicLong(0);

    private AtomicLong lastScanResponseProduced = new AtomicLong(0L);

    private AtomicLong ingestTaskRequests = new AtomicLong(0);
    private AtomicLong ingestTaskRequestsFinished  = new AtomicLong(0);
    private AtomicLong ingestTaskRequestsFinishedWithError  = new AtomicLong(0);
    private AtomicLong ingestTaskRequestFailure  = new AtomicLong(0);

    private AtomicLong failedSignatureCheck = new AtomicLong(0);

    private long prevIngestTaskRequestsFinishedCounter  ;
    private long prevIngestTaskRequestsFinishedCounterTime ;
    private long prevIngestPerTimeCount;

    private final Object ingestRateLock = new Object();
    private final Object scanRateLock = new Object();

    private TimeSource timeSource = new TimeSource();

    private String version;

    @ManagedAttribute
    public long getStartScanTaskRequests() {
        return startScanTaskRequests.get();
    }

    @ManagedAttribute
    public long getActiveScanTasks() {
        return activeScanTasks.get();
    }

    @ManagedAttribute
    public long getStartScanTaskRequestsFinished() {
        return startScanTaskRequestsFinished.get();
    }

    @ManagedAttribute
    public long getStartScanTaskRequestsPaused() {
        return startScanTaskRequestsPaused.get();
    }

    @ManagedAttribute
    public long getScanTaskResponsesFinished() {
        return scanTaskResponsesFinished.get();
    }

    @ManagedAttribute
    public long getScanTaskFailedResponsesFinished() {
        return scanTaskFailedResponsesFinished.get();
    }

    @ManagedAttribute
    public long getIngestTaskRequests() {
        return ingestTaskRequests.get();
    }

    @ManagedAttribute
    public long getIngestTaskRequestsFinished() {
        return ingestTaskRequestsFinished.get();
    }

    @ManagedAttribute
    public long getIngestTaskRequestsFinishedWithError() {
        return ingestTaskRequestsFinishedWithError.get();
    }

    @ManagedAttribute
    public long getIngestTaskRequestFailure() {
        return ingestTaskRequestFailure.get();
    }

    @ManagedAttribute
    public long getFailedSignatureCheck() {
        return failedSignatureCheck.get();
    }

    public void incFailedSignatureCheck() {
        this.failedSignatureCheck.incrementAndGet();
    }

    public void incStartScanTaskRequested() {
        startScanTaskRequests.incrementAndGet();
    }

    public long incActiveScanTasks() {
        return  activeScanTasks.incrementAndGet();
    }

    public long decActiveScanTasks() {
        return  activeScanTasks.decrementAndGet();
    }

    public void incStartScanTaskFinished() {
        startScanTaskRequestsFinished.incrementAndGet();
    }

    public void incStartScanTaskRequestsPaused() {
         startScanTaskRequestsPaused.incrementAndGet();
    }

    public void incScanResponseTaskFinished() {
        scanTaskResponsesFinished.incrementAndGet();
        lastScanResponseProduced.set(timeSource.currentTimeMillis());
    }

    public void incScanFailedResponseTaskFinished() {
        scanTaskFailedResponsesFinished.incrementAndGet();
        lastScanResponseProduced.set(timeSource.currentTimeMillis());
    }

    public void incIngestTaskRequested() {
        ingestTaskRequests.incrementAndGet();
    }

    public void incIngestTaskFinished() {
        ingestTaskRequestsFinished.incrementAndGet();
    }
    public void incIngestTaskFinishedWithError() {
        ingestTaskRequestsFinishedWithError.incrementAndGet();
    }

    public void incIngestTaskRequestFailure() {
        ingestTaskRequestFailure.incrementAndGet();
    }

    @ManagedAttribute
    public long getTimeSpentFindMatchesInPotentialCandidatesWord() {
        return timeSpentFindMatchesInPotentialCandidatesWord;
    }

    @ManagedAttribute
    public float getScanRunningThroughput() {
        return scanRunningThroughput;
    }

    @ManagedAttribute
    public double getIngestRunningThroughput() {
        return ingestRunningThroughput;
    }

    @ManagedAttribute
    public long getDuplicateCounter() {
        return duplicateCounter;
    }

    public void setDuplicateCounter(long duplicateCounter) {
        this.duplicateCounter = duplicateCounter;
    }


    public void incrementDuplicateCounter() {
        ++duplicateCounter;
    }

    public void updateAvarageScanThroughput(boolean forceCalc) { //TODO Liora: calc running throuput
        synchronized (scanRateLock) {
            if (isScanning() || forceCalc) {
                double seconds = timeSource.secondsSince(getScanStartTime());
                if (seconds > 0d) {
                    this.averageScanThroughput = (scanTaskResponsesFinished.get() + scanTaskFailedResponsesFinished.get()) / seconds;
                } else {
                    logger.debug("updateAvarageScanThroughput got 0 seconds");
                }
            }
        }
    }

    public void updateIngestThrouputStats() {
        synchronized (ingestRateLock) {
            long currentIngest = ingestTaskRequests.get();
            long ingestsPerTime = currentIngest - prevIngestTaskRequestsFinishedCounter;

//            if (ingestsPerTime > 0) { //Only if there are new incoming ingests
            long currentTime = timeSource.currentTimeMillis();
            double seconds = timeSource.secondsSince(prevIngestTaskRequestsFinishedCounterTime);
            //     this.averageIngestThroughput =  this.ingestRunningThroughput * prevIngestPerTimeCount; // TODO Liora: calc this
            this.ingestRunningThroughput = (seconds > 0) ? (ingestsPerTime / seconds) : 0d;
            logger.debug("Ingest rate {} for the last {} sec. finished ingests sent {}", averageIngestThroughput, seconds, ingestsPerTime);
//            }
            prevIngestPerTimeCount = ingestsPerTime;
            prevIngestTaskRequestsFinishedCounter = currentIngest;
            prevIngestTaskRequestsFinishedCounterTime = currentTime;
        }
    }

    private boolean isScanning()
    {
        return scanEndTime == null;
    }

    public void resetScanStartStats() {
        scanTaskResponsesFinished.set(0);
        scanTaskFailedResponsesFinished.set(0);
        scanStartTime = timeSource.currentTimeMillis();
        scanEndTime = null;
    }

    public void setScanEndTime() {
        scanEndTime = timeSource.currentTimeMillis();
        updateAvarageScanThroughput(true);
    }


    @ManagedAttribute
    public long getScanStartTime() {
        return scanStartTime;
    }

    @ManagedAttribute
    public Long getScanEndTime() {
         return scanEndTime;
     }

    @ManagedAttribute
    public double getAverageScanThroughput() {
        return averageScanThroughput;
    }

    @ManagedAttribute
    public double getAverageIngestThroughput() {
        return averageIngestThroughput;
    }

    public void markLastScanResponseProduced() {
        this.lastScanResponseProduced.set(timeSource.currentTimeMillis());
    }

    public long getLastScanResponseProduced() {
        return lastScanResponseProduced.get();
    }

    @ManagedAttribute
    public String getVersion() {
        if (version == null) {
            version = readVersionFromFile(VERSION_PROPERTIES_FILE);
        }
        return version;
    }

    private String readVersionFromFile(String fileName) {
        String readString = null;
        try {
            FileReader fr = new FileReader(fileName);
            char[] cbuf = new char[100];
            int read = fr.read(cbuf);
            readString = new String(cbuf, 0, read);
        } catch (IOException e) {
            logger.error("Failed to open " + VERSION_PROPERTIES_FILE + " file err {}", e.getMessage());
        }
        return readString;
    }


}
