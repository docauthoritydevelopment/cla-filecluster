package com.cla.mediaproc.service.filecontent;

import java.io.InputStream;
import java.util.Map;

public interface MetadataExtractor {
	Map<String,String> extractLocal(InputStream inputStream);
	Map<String,String> extractExternal();
}
