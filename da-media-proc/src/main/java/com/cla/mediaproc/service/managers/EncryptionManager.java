package com.cla.mediaproc.service.managers;

import com.cla.common.domain.dto.filetree.RsaKey;
import com.cla.common.utils.EncryptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;

/**
 * Handle usage of private key (set, get, read stores in memory for fast retrieve)
 * Created by yael on 11/13/2017.
 */
@Service
public class EncryptionManager {

    private Logger logger = LoggerFactory.getLogger(EncryptionManager.class);

    private static final String PASS_FILE_NAME = "data-center-data.txt";
    private static final String STORE_FILE_NAME = "ppks.txt";
    private static final String KEY_ENTRY = "DataCenterKey";
    private static final String ENCODING = "UTF-8";
    private static final String KEY_STORE_TYPE = "PKCS12";

    private PrivateKey privateKey = null;

    public synchronized PrivateKey getPrivateKey() throws Exception {
        if (privateKey == null) {
            privateKey = retrievePrivateKey();
        }
        return privateKey;
    }

    // get private key from key store
    public PrivateKey retrievePrivateKey() throws Exception {
        String keyStorePassword = new String(Files.readAllBytes(Paths.get(PASS_FILE_NAME))).trim();
        KeyStore ks = KeyStore.getInstance(KEY_STORE_TYPE);
        try (final InputStream is = new FileInputStream(STORE_FILE_NAME)) {
            ks.load(is, keyStorePassword.toCharArray());
        }
        return (PrivateKey)ks.getKey(KEY_ENTRY, keyStorePassword.toCharArray());
    }

    // store private key in key store with password given
    public synchronized void savePrivateKey(RsaKey keys, String keyStorePassword) throws Exception {
        logger.info("====== saving new private key START ======");
        KeyStore ks = KeyStore.getInstance(KEY_STORE_TYPE);
        ks.load(null, null);
        X509Certificate cert = EncryptionUtils.getCertificateFromString(keys.getCertificate());
        Certificate[] certChain = new Certificate[1];
        certChain[0] = cert;
        PrivateKey key = EncryptionUtils.getPrivateKeyFromString(keys.getPrivateKey());
        ks.setKeyEntry(KEY_ENTRY, key, keyStorePassword.toCharArray(), certChain);
        try (final OutputStream out = new FileOutputStream(STORE_FILE_NAME)) {
            ks.store(out, keyStorePassword.toCharArray());
        }
        PrintWriter writer = new PrintWriter(PASS_FILE_NAME, ENCODING);
        writer.println(keyStorePassword);
        writer.close();
        privateKey = key;
        logger.info("====== saving new private key END ======");
    }
}
