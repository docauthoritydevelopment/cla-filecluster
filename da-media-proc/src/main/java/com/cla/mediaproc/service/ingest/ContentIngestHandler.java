package com.cla.mediaproc.service.ingest;

import com.cla.common.connectors.ExistingContentConnector;
import com.cla.common.domain.dto.FileMetadata;
import com.cla.common.domain.dto.mediaproc.OtherFile;
import com.cla.common.domain.dto.mediaproc.OtherFileMetadata;
import com.cla.common.domain.dto.mediaproc.excel.ExcelWorkbookMetaData;
import com.cla.common.domain.dto.messages.*;
import com.cla.common.domain.dto.word.WordfileMetaData;
import com.cla.common.media.files.excel.ExcelIngesterService;
import com.cla.common.media.files.other.OtherFileIngesterService;
import com.cla.common.media.files.pdf.PdfIngesterService;
import com.cla.common.media.files.word.FileMetadataUtils;
import com.cla.common.media.files.word.WordIngesterService;
import com.cla.common.media.ingest.FileIngestService;
import com.cla.common.utils.FileSizeUnits;
import com.cla.connector.domain.dto.error.ProcessingErrorType;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.file.FileContentDto;
import com.cla.connector.domain.dto.file.FileType;
import com.cla.connector.utils.Pair;
import com.cla.filecluster.domain.exceptions.StoppedByUserException;
import com.cla.filecluster.service.IngestRequestHandler;
import com.google.common.base.Strings;
import org.apache.commons.lang3.tuple.Triple;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.security.AccessControlException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

/**
 * Manage ingestion task processing on the media processor (worker) side
 * Created by vladi on 3/14/2017.
 */
@Component
public abstract class ContentIngestHandler implements IngestRequestHandler {

    private Logger logger = LoggerFactory.getLogger(ContentIngestHandler.class);

    //<editor-fold services>

    @Autowired
    private WordIngesterService wordIngesterService;

    @Autowired
    private ExcelIngesterService excelIngesterService;

    @Autowired
    private OtherFileIngesterService otherFileIngesterService;

    @Autowired
    private PdfIngesterService pdfIngesterService;

    @Autowired
    @Qualifier("appServerProcessingService")
    private ExistingContentConnector existingContentConnector;

    //</editor-fold>

    //<editor-fold members>

    @Value("${ingester.memory.outofmemory.exit:false}")
    private boolean exitOnOutOfMemory;

    @Value("${ingester.memory.limit.percent:0}")
    private int ingestMemoryPercentLimit;

    @Value("${ingester.memory.limit.sleep-seconds:2}")
    private int ingestMemoryLimitSleepSeconds;

    @Value("${ingester.memory.limit.sleep-cycles-max-count:600}")
    private int ingestMemoryLimitSleepCount;

    private AtomicInteger sleepingThreadsCount = new AtomicInteger(0);

    private boolean exitSequenceInitiated = false;

    //</editor-fold>

    //<editor-fold public methods>

    public boolean getExitSequenceInitiated() {
        return exitSequenceInitiated;
    }

    @Override
    public List<Pair<IngestTaskParameters, IngestMessagePayload>> handleIngestionRequest(
            IngestRequestMessage ingestRequestMessage) throws IOException {

        IngestMessagePayload result;
        IngestTaskParameters mainFileTaskParameters = ingestRequestMessage.getPayload();
        List<Triple<IngestTaskParameters, FileContentDto, ? extends ClaFilePropertiesDto>> fileDataList;
        Long taskId = ingestRequestMessage.getTaskId();
        try {
            logger.trace("extractFileData task {}", taskId);
            if (ingestRequestMessage.getContainedFilesPayloads().isEmpty()) {
                // Regular (non-container) file ingestion
                Pair<FileContentDto, ? extends ClaFilePropertiesDto> fileData = extractFileData(ingestRequestMessage);
                fileDataList = Collections.singletonList(Triple.of(
                        mainFileTaskParameters,
                        fileData.getKey(),
                        fileData.getValue()
                ));
            } else {
                // A container file ingestion - the request has a list of IngestTaskParameters for each contained file
                fileDataList = extractContainerFileData(ingestRequestMessage);
            }


        } catch (AccessControlException e) {
            logger.info("Failed to get file data - probably throttle control.", e);
            result = buildIngestResultWithError(mainFileTaskParameters, ProcessingErrorType.ERR_TOO_MANY_REQUESTS, e);
            return Collections.singletonList(Pair.of(mainFileTaskParameters, result));
        } catch (FileNotFoundException ex) {
            logger.info("Failed to open file {}" + mainFileTaskParameters.getFilePath(), ex);
            result = buildIngestResultWithError(mainFileTaskParameters,
                    wordIngesterService.testNetworkError(ex.getMessage()) ? ProcessingErrorType.ERR_NETWORK : ProcessingErrorType.ERR_FILE_NOT_FOUND, ex
            );
            return Collections.singletonList(Pair.of(mainFileTaskParameters, result));
        }

        List<Pair<IngestTaskParameters, IngestMessagePayload>> results = new ArrayList<>(fileDataList.size());

        for (Triple<IngestTaskParameters, FileContentDto, ? extends ClaFilePropertiesDto> fileData : fileDataList) {
            IngestTaskParameters ingestTaskParams = fileData.getLeft();
            result = ingestFileData(taskId, ingestTaskParams, Pair.of(fileData.getMiddle(), fileData.getRight()));
            results.add(Pair.of(ingestTaskParams, result));
        }
        return results;
    }

    private IngestMessagePayload ingestFileData(
            Long taskId,
            IngestTaskParameters ingestTaskParameters,
            Pair<FileContentDto, ? extends ClaFilePropertiesDto> fileData)
            throws IOException {

        FileContentDto fileContentDto = fileData.getKey();
        ClaFilePropertiesDto fileAttributesDto = fileData.getValue();
        String filePath = ingestTaskParameters.getFilePath();

        logger.trace("get from input stream task {}", taskId);

        if (fileAttributesDto != null && !fileAttributesDto.getFileName().equals(ingestTaskParameters.getFilePath())) {
            if (!Strings.isNullOrEmpty(ingestTaskParameters.getFilePath())) {
                fileAttributesDto.setFileName(ingestTaskParameters.getFilePath());
            }
        }

        //noinspection SpellCheckingInspection
        try (InputStream is = fileContentDto.getInputStream()) {
            logMemoryConsumption(ingestTaskParameters);
            sleepOnHighMemoryLoad();
            return ingestContent(fileAttributesDto, is, ingestTaskParameters);
        } catch (OutOfMemoryError oome) {
            logger.error("OUT OF MEMORY ERROR - Failed to ingest file id:{}, path:{}, type:{}.",
                    ingestTaskParameters.getFileId(), filePath, oome);

            if (exitOnOutOfMemory) {
                handleOutOfMemoryExit();
                delayedHalt();
            }
            throw oome;
        } finally {
            // Space padding below is intentional to look nice in the logs just below the log from logMemoryConsumption()
            logger.debug("Finished document ingestion,                                               path={}", filePath);
        }
    }

    @NotNull
    private IngestMessagePayload buildIngestResultWithError(IngestTaskParameters ingestTaskParameters, ProcessingErrorType errorType, Exception e) {
        WordIngestResult result = new WordIngestResult();
        result.setErrorType(errorType);
        result.setExceptionText(e == null ? null : e.getMessage());
        ClaFilePropertiesDto claFileProperties = new ClaFilePropertiesDto(ingestTaskParameters.getFilePath(), 0L);
        claFileProperties.setType(ingestTaskParameters.getFileType());
        result.setClaFileProperties(claFileProperties);
        return result;
    }

    protected abstract Pair<FileContentDto, ? extends ClaFilePropertiesDto> extractFileData(
            IngestRequestMessage ingestRequestMessage) throws IOException;

    protected List<Triple<IngestTaskParameters, FileContentDto, ? extends ClaFilePropertiesDto>> extractContainerFileData(
            IngestRequestMessage ingestRequestMessage) throws IOException {
        // Default impl: just call the non-container method and wrap in a List
        Pair<FileContentDto, ? extends ClaFilePropertiesDto> fileData = extractFileData(ingestRequestMessage);
        Triple<IngestTaskParameters, FileContentDto, ? extends ClaFilePropertiesDto> result =
                Triple.of(ingestRequestMessage.getPayload(), fileData.getKey(), fileData.getValue());
        return Collections.singletonList(result);
    }

    //</editor-fold>

    //<editor-fold private methods>

    private IngestMessagePayload ingestContent(
            ClaFilePropertiesDto claFilePropertiesDto, InputStream inputStream, IngestTaskParameters ingestTaskParameters) {

        IngestMessagePayload result = null;
        if (!exitSequenceInitiated && ingestTaskParameters != null) {
            FileType fileType = ingestTaskParameters.getFileType();

            boolean ingestContents = ingestTaskParameters.getIngestContents();
            if (ingestContents) {
                result = getIngestHandler(ingestTaskParameters).ingestFile(ingestTaskParameters, claFilePropertiesDto,
                        inputStream, existingContentConnector, ingestTaskParameters.getRootFolderId());
            } else {
                result = extractMetadataOnly(fileType, claFilePropertiesDto);
            }
        }
        return result;
    }

    private FileIngestService getIngestHandler(IngestTaskParameters ingestTaskParameters) {
        FileType fileType = ingestTaskParameters.getFileType();
        if (fileType == null) {
            return otherFileIngesterService;
        }
        switch (fileType) {
            case WORD:
                return wordIngesterService;
            case EXCEL:
                return excelIngesterService;
            case PDF:
            case SCANNED_PDF:
                return pdfIngesterService;
            default:
                return otherFileIngesterService;
        }
    }

    /**
     * Create ingestion result with metadata only, no content
     *
     * @param fileType             file type
     * @param claFilePropertiesDto file props
     * @return ingest message payload
     */
    private IngestMessagePayload extractMetadataOnly(FileType fileType, ClaFilePropertiesDto claFilePropertiesDto) {
        IngestMessagePayload result = null;
        Class<? extends FileMetadata> metadataType;

        switch (fileType) {
            case WORD:
            case PDF:
            case SCANNED_PDF:
                result = new WordIngestResult();
                metadataType = WordfileMetaData.class;
                break;
            case EXCEL:
                result = new ExcelIngestResult();
                metadataType = ExcelWorkbookMetaData.class;
                break;
            default:
                result = new OtherFile();
                metadataType = OtherFileMetadata.class;
        }

        if (metadataType != null) {
            FileMetadata fileMetadata = FileMetadataUtils.extractMetadata(metadataType, null, claFilePropertiesDto);
            //noinspection unchecked
            result.setMetadata(fileMetadata);
            result.setMetadataOnly(true);
            result.setClaFileProperties(claFilePropertiesDto);
            result.setFileName(claFilePropertiesDto.getFileName());
        }

        return result;
    }

    private void logMemoryConsumption(IngestTaskParameters params) {
        if (logger.isDebugEnabled()) {
            FileType fileType = params.getFileType();
            String filePath = params.getFilePath();
            Runtime runtime = Runtime.getRuntime();
            long tm = FileSizeUnits.BYTE.toMegabytes(runtime.totalMemory());
            long fm = FileSizeUnits.BYTE.toMegabytes(runtime.freeMemory());
            long mm = FileSizeUnits.BYTE.toMegabytes(runtime.maxMemory());
            logger.debug("Starting document ingestion, type={}, mem(T;U;F;M),{},{},{},{},MB, path={}", fileType,
                    tm, tm - fm, fm, mm, filePath);
        }
    }

    private void sleepOnHighMemoryLoad() {

        if (ingestMemoryPercentLimit > 0) {
            boolean sleep;
            boolean wasAsleep = false;
            int count = 0;
            do {
                sleep = false;
                count++;

                Runtime runtime = Runtime.getRuntime();
                long tm = FileSizeUnits.BYTE.toMegabytes(runtime.totalMemory());
                long fm = FileSizeUnits.BYTE.toMegabytes(runtime.freeMemory());
                long mm = FileSizeUnits.BYTE.toMegabytes(runtime.maxMemory());
                long um = tm - fm;

                // check used mem as percentage of the max-memory available.
                if ((100 * um) > ingestMemoryPercentLimit * mm) {

                    if (!wasAsleep) {
                        wasAsleep = true;
                        logger.debug("Sleeping due to high memory utilization: {} out of {} Mb ({}% > {}%). {} threads are asleep.",
                                um, mm, (100 * um / mm), ingestMemoryPercentLimit, sleepingThreadsCount.incrementAndGet());
                    }

                    sleep = true;

                    try {
                        Thread.sleep(TimeUnit.SECONDS.toMillis(ingestMemoryLimitSleepSeconds));
                    } catch (final InterruptedException e) {
                        throw new StoppedByUserException(e);
                    }
                    Runtime.getRuntime().gc(); //TODO 2.0 - remove this
                }
            } while (sleep && count < ingestMemoryLimitSleepCount);

            if (wasAsleep) {
                logger.debug("Out of sleep after {} seconds. {} are still sleeping", ingestMemoryLimitSleepSeconds*count, sleepingThreadsCount.decrementAndGet());
            }
        }
    }

    /**
     * Fail-safe halt.
     * If the system doesn't exit in 5 minutes - halt forcefully.
     */
    private void delayedHalt() {
        logger.error("System will go down in up to 5 minutes");
        long ONE_MINUTE = TimeUnit.MINUTES.toMillis(1);
        Executors.newSingleThreadExecutor().execute(() -> {
            IntStream.range(0, 5).forEach(
                    min -> {
                        logger.info("Waiting {} minutes before killing the process.", 5 - min);
                        try {
                            Thread.sleep(ONE_MINUTE);
                        } catch (InterruptedException e) {
                            logger.warn("Force exit task interrupted");
                        }
                    });

            logger.info("Killing the process");
            Runtime.getRuntime().halt(100);
        });
    }

    /**
     * Handle out of memory error.
     * Try to shutdown the threads and exit.
     */
    private void handleOutOfMemoryExit() {
        exitSequenceInitiated = true;
//        Executors.newSingleThreadExecutor().execute(() -> defaultKafkaListenerEndpointRegistry.stop());
        logger.error("Out of memory error was encountered, the process will exit.");
        System.exit(100);
    }

    //</editor-fold>
}
