package com.cla.mediaproc.service.filecontent;

import com.cla.common.domain.dto.file.DirectoryExistStatus;
import com.cla.common.domain.dto.file.FileContentHolder;
import com.cla.common.domain.dto.filetree.RootFolderDto;
import com.cla.common.domain.dto.mediaproc.OtherFile;
import com.cla.common.domain.dto.mediaproc.excel.ExcelSheet;
import com.cla.common.domain.dto.mediaproc.excel.ExcelWorkbook;
import com.cla.common.domain.dto.messages.FileContentMessagePayload;
import com.cla.common.domain.dto.messages.FileContentType;
import com.cla.common.domain.dto.messages.WordIngestResult;
import com.cla.common.domain.pst.PstPath;
import com.cla.common.media.files.excel.ExcelFilesService;
import com.cla.common.media.files.other.OtherFileIngesterService;
import com.cla.common.media.files.pdf.PdfIngesterService;
import com.cla.common.media.files.pst.model.PstFileFacade;
import com.cla.common.media.files.word.WordIngesterService;
import com.cla.common.utils.tokens.DictionaryHashedTokensConsumerImpl;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.file.FileContentDto;
import com.cla.connector.domain.dto.file.FileType;
import com.cla.connector.domain.dto.media.MediaConnectionDetailsDto;
import com.cla.connector.domain.dto.media.MediaType;
import com.cla.connector.mediaconnector.FileMediaConnector;
import com.cla.connector.utils.Pair;
import com.cla.filecluster.service.media.MediaConnectorFactory;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.io.FilenameUtils;
import org.codehaus.plexus.util.IOUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.joining;


/**
 * Manage file content task processing on the media processor (worker) side
 * Created by liron on 4/12/2017.
 */
@Service
public class FileContentManagerService {

    private final static Logger logger = LoggerFactory.getLogger(FileContentManagerService.class);

    @Autowired
    private MediaConnectorFactory mediaConnectorFactory;

    @Autowired
    private ExcelFilesService excelFilesService;

    @Autowired
    private WordIngesterService wordIngesterService;

    @Autowired
    private PdfIngesterService pdfIngesterService;

    @Autowired
    private OtherFileIngesterService otherFileIngesterService;

    @Autowired
    private PstFileFacade pstFileFacade;

    @Autowired
    private MetadataService metadataService;

    @Autowired
    private RootFolderAccessibilityTracker rootFolderAcessibilityTracker;

    @Value("${root-folders.accessible.check.timeout:30000}")
    private long rootFolderAccessibleTimeout;

    @Value("${root-folders.accessible.active:true}")
    private boolean isDirectoryExistActive;

    @Value("${pst-cache.enable-for-fileshare:false}")
    private boolean isPstCacheEnabledForFileShare;

    @Value("${root-folders.accessible.pool-size:5}")
    private int rfAccessibleThreadPoolSize;

    @Value("${ingest.read-file-metadata:true}")
    private boolean shouldReadFileMetadata;

    public Pair<FileContentDto, ? extends ClaFilePropertiesDto> getFileContent(FileType fileType,
                                                                    FileContentType fileContentType,
                                                                    String mediaEntityId,
                                                                    String fileFullName,
                                                                    MediaConnectionDetailsDto mediaConnectionDetailsDto) throws IOException {
        Objects.requireNonNull(fileContentType);
        if (PstPath.isPstEntryPath(mediaEntityId)) {
            switch (fileContentType) {
                case EXTRACT_HIGHLIGHT_FILE:
                    return getPstEntryData(mediaEntityId, mediaConnectionDetailsDto, false);
                case EXTRACT_DOWNLOAD_FILE:
                    return getPstEntryData(mediaEntityId, mediaConnectionDetailsDto, true);
                default:
                    throw new IllegalArgumentException("Unexpected FileContentType: " + fileContentType + " when getting the content of " + fileFullName);
            }
        } else {
            return getFileContent(fileType, fileFullName, mediaConnectionDetailsDto, true);
        }
    }

    public Map<String, Pair<FileContentDto, ClaFilePropertiesDto>> getContainerFileContent(
            String containerFileIdentifier, Set<String> expectedContainedFilesIdentifiers,
            MediaConnectionDetailsDto mediaConnectionDetailsDto)
            throws IOException {

        Objects.requireNonNull(mediaConnectionDetailsDto);
        FileMediaConnector mediaConnector = mediaConnectorFactory.getMediaConnector(mediaConnectionDetailsDto);
        return mediaConnector.getContainerFileDataList(containerFileIdentifier, expectedContainedFilesIdentifiers);
    }

    public Pair<FileContentDto, ClaFilePropertiesDto> getFileContent(
            FileType fileType, String fileIdentifier,
            MediaConnectionDetailsDto mediaConnectionDetailsDto,
            boolean forUserDownload) throws IOException {

        if (mediaConnectionDetailsDto != null) {
            FileMediaConnector mediaConnector = mediaConnectorFactory.getMediaConnector(mediaConnectionDetailsDto);
            Pair<FileContentDto, ClaFilePropertiesDto> itemDtoAndContent = mediaConnector.getItemDtoAndContent(fileIdentifier, forUserDownload);
            itemDtoAndContent.getValue().setType(fileType);

            if (shouldReadFileMetadata) {
                Map<String, String> metadata = metadataService.extractLocal(itemDtoAndContent.getValue(),
                        itemDtoAndContent.getKey().getInputStream());
                itemDtoAndContent.getValue().setMetadata(metadata);
            }

			return itemDtoAndContent;
        }
        else {
            throw new RuntimeException("Missing FileMediaConnector details");
        }
    }

    public Pair<FileContentDto, ? extends ClaFilePropertiesDto> getPstEntryData(
            String pstEntryPathStr, MediaConnectionDetailsDto mediaConnectionDetailsDto, boolean extractMimeMessage)
            throws IOException {

        if (mediaConnectionDetailsDto == null) {
            throw new IllegalArgumentException("mediaConnectionDetailsDto can't be null");
        }

        FileMediaConnector mediaConnector = mediaConnectorFactory.getMediaConnector(mediaConnectionDetailsDto);
        PstPath pstEntryPath = PstPath.fromString(pstEntryPathStr);
        Path pstFilePath = pstEntryPath.getPstFilePath();
        ClaFilePropertiesDto pstFileProps = mediaConnector.getFileAttributes(pstFilePath.toString());
        boolean usePstCache =
                mediaConnectionDetailsDto.getMediaType() == MediaType.FILE_SHARE
                        ? isPstCacheEnabledForFileShare
                        : mediaConnectionDetailsDto.isPstCacheEnabled();
        return pstFileFacade.getPstEntryData(pstFileProps, pstEntryPath, extractMimeMessage, usePstCache);
    }

    public FileContentMessagePayload handleHighlight(String fileFullName, FileType fileType, Set<String> textTokens,
                                                     InputStream is, FileContentMessagePayload messagePayload) {

        FileContentMessagePayload result;
        try {
            switch(fileType){
                case EXCEL:
                    result = handleExcelHighlight(fileFullName, textTokens, is, messagePayload);
                    break;
                case PDF:
                    result = handlePDFHighlight(fileFullName, is, messagePayload);
                    break;
                case WORD:
                    result = handleWordHighlight(fileFullName, is, messagePayload);
                    break;
                default:
                    result = handleOtherHighlight(fileFullName, is, messagePayload);
            }
        } catch (Exception e) {
            logger.error("Failed to handle file content task.", e);
            result = messagePayload.copy();
        }
        return result;
    }

    private FileContentMessagePayload handleExcelHighlight(String fileName, Set<String> textTokens, InputStream is,
                                                           FileContentMessagePayload fileContentMessagePayload) throws Exception{
        FileContentMessagePayload result = fileContentMessagePayload.copy();

        excelFilesService.setPatternSearchActive(true);

        if (textTokens == null || textTokens.isEmpty()) {
            textTokens = new HashSet<>();
        }
        DictionaryHashedTokensConsumerImpl hashedTokensConsumer = new DictionaryHashedTokensConsumerImpl(textTokens, true);
        final ExcelWorkbook excelWorkbook = excelFilesService.ingestWorkbook(fileName, hashedTokensConsumer, is, true);
        String content = excelWorkbook.getSheets().values().stream()
                .map(ExcelSheet::getOrigContentTokens).collect(joining("\n\n"));
        Map<String, List<String>> locations = getExcelLocationTokenLocations(hashedTokensConsumer);

        result.setContent(content);
        result.setLocations(locations);

        return result;
    }

    private FileContentMessagePayload handlePDFHighlight(String fileName, InputStream is,
                                                         FileContentMessagePayload messagePayload){

        FileContentMessagePayload result = messagePayload.copy();
        ClaFilePropertiesDto claFileProperties = new ClaFilePropertiesDto(fileName, 0L);
        WordIngestResult res = pdfIngesterService.extract(is, fileName, false, claFileProperties);
        result.setContent(res.getOriginalContent());
        result.setFileType(FileType.PDF);

        return result;
    }

    private FileContentMessagePayload handleWordHighlight(String fileName, InputStream is,
                                                          FileContentMessagePayload messagePayload) {
        FileContentMessagePayload result = messagePayload.copy();
        ClaFilePropertiesDto claFileProperties = new ClaFilePropertiesDto(fileName, 0L);
        WordIngestResult res = wordIngesterService.extract(is, fileName, false, claFileProperties);
        result.setContent(res.getOriginalContent());
        result.setFileType(FileType.WORD);
        return result;
    }

    private FileContentMessagePayload handleOtherHighlight(String fileName, InputStream is,
                                                           FileContentMessagePayload messagePayload) {
        FileContentMessagePayload result = messagePayload.copy();
        OtherFile otherFile = otherFileIngesterService.extract(is, fileName, null, null);
        result.setContent(otherFile.getExtractedContent());
        result.setFileType(otherFile.getType());
        return result;
    }

    public FileContentMessagePayload handleContentDownload(String fileName, FileContentDto fileContentDto, InputStream is,
                                                           FileContentMessagePayload messagePayload) throws IOException {

        byte[] contentBytes = fileContentDto.isContentAlreadyLoaded() ? fileContentDto.getContent() : IOUtil.toByteArray(is);
        FileContentHolder fileContentHolder = new FileContentHolder().setBytes(contentBytes);

        FileContentMessagePayload result = messagePayload.copy();
        result.setFileName(fileName);
        result.setFileLength(fileContentDto.getFileLength());
        result.setFileContentHolder(fileContentHolder);
        result.setBaseName(FilenameUtils.getName(fileContentDto.getFileName()));
        return result;
    }

    private Map<String, List<String>> getExcelLocationTokenLocations(DictionaryHashedTokensConsumerImpl hashedTokensConsumer) {
        try {
            Map<String, List<String>> locations = hashedTokensConsumer.getTokensLocations();
            return orderBySheetAggregateTokens(locations);
        } catch (final Exception e) {
            logger.error("Failed to get excel locations.", e);
        }
        return null;
    }

    private Map<String, List<String>> orderBySheetAggregateTokens(final Map<String, List<String>> locations) {
        if (locations == null) {
            return null;
        }

        final String separator = ":";

        final Map<String, Map<String, List<String>>> tmpResult = new HashMap<>();
        locations.forEach((key, value) -> value.forEach(l -> {
            final String[] split = l.split(separator);
            if (split.length == 2) {
                Map<String, List<String>> sheetTokensMap = tmpResult.computeIfAbsent(split[0], k -> Maps.newHashMap());
                List<String> tokenLocations = sheetTokensMap.computeIfAbsent(key, k -> Lists.newArrayList());
                tokenLocations.add(split[1]);
            }
        }));
        logger.trace("Locations ordered by sheet: {}", tmpResult.toString());

        final Map<String, List<String>> result = Maps.newHashMap();
        tmpResult
                .forEach((key, value) -> result.put(
                        key,
                        value.entrySet().stream()
                                .map(te -> te.getKey() + separator + " " + String.join(", ", te.getValue()))
                                .collect(Collectors.toList())));

        logger.trace("Aggregated locations: {}", result.toString());

        return (result.isEmpty() ? locations : result);
    }

    public DirectoryExistStatus isDirectoryExists(RootFolderDto rootFolderDto, MediaConnectionDetailsDto mediaConnectionDetailsDto, boolean forceCheck) {
        return getDirectoryExistStatus(rootFolderDto.getId(), rootFolderDto.getRealPath(), forceCheck, mediaConnectionDetailsDto);
    }

    public DirectoryExistStatus getDirectoryExistStatus(Long rootFolderId, String path, Boolean forceCheck, MediaConnectionDetailsDto mediaConnectionDetailsDto) {
        if (!isDirectoryExistActive) {
            return DirectoryExistStatus.YES;
        }

        return rootFolderAcessibilityTracker.track(rootFolderId, path, forceCheck, mediaConnectionDetailsDto);
    }
}
