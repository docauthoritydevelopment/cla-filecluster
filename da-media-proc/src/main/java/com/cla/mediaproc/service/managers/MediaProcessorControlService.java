package com.cla.mediaproc.service.managers;

import com.cla.connector.utils.TimeSource;
import com.cla.mediaproc.appserver.AppServerProcessingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

/**
 * Periodically sends heartbeat message from MP to the appserver
 * Created by uri on 21-May-17.
 */
@Service
public class MediaProcessorControlService {

    private final static Logger logger = LoggerFactory.getLogger(MediaProcessorControlService.class);

    @Autowired
    private AppServerProcessingService appServerProcessingService;

    @Autowired
    private MediaProcessorStateService mediaProcessorStateService;

    @Value("${scan.responses.idle-threshold-sec:1200}")
    private long scanResponseIdleThresholdSec ;

    private TimeSource timeSource = new TimeSource();

    @PostConstruct
    private void init() {
        logger.debug("MediaProcessor control service init");
    }

    @Scheduled(initialDelayString = "${status.reports.initial.ms:30000}", fixedRateString = "${status.reports.cycle.ms:300000}")
    @ConditionalOnProperty(value = "is.toolExecutionMode", havingValue = "false", matchIfMissing = true)
    public void periodicStatusReportScheduler() {
        try {
            mediaProcessorStateService.updateIngestThrouputStats();
            mediaProcessorStateService.updateAvarageScanThroughput(false);
            appServerProcessingService.sendStatusReport();

            long lastScanResponseProduced = mediaProcessorStateService.getLastScanResponseProduced();
            long activeScanTasks = mediaProcessorStateService.getActiveScanTasks();
            if (scanResponseIdleThresholdSec > 0 &&
                    timeSource.secondsSince(lastScanResponseProduced) > scanResponseIdleThresholdSec
                    && activeScanTasks > 0){
                logger.error("WATCHDOG_RESTART: No responses sent to server for {} seconds, " +
                        "even though there are {} active tasks.", scanResponseIdleThresholdSec, activeScanTasks);
            }

        } catch (Exception e) {
            logger.error("Failed to send status report to AppServer ({})",e);
        }
    }



}
