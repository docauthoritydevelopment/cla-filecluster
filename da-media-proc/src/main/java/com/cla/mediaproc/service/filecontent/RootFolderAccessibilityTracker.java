package com.cla.mediaproc.service.filecontent;

import com.cla.common.domain.dto.file.DirectoryExistStatus;
import com.cla.connector.domain.dto.media.MediaConnectionDetailsDto;
import com.cla.connector.mediaconnector.FileMediaConnector;
import com.cla.filecluster.service.media.MediaConnectorFactory;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import reactor.core.Disposable;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.time.Duration;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class RootFolderAccessibilityTracker {

    private static final Logger logger = LoggerFactory.getLogger(RootFolderAccessibilityTracker.class);


    private MediaConnectorFactory mediaConnectorFactory;

    private final long rfAccessibleCheckTimeoutInMillis;
    private final long rfAccessibleCheckInterval;
    private final int rfAccessCheckConcurrency;

    private final Flux<RootFolderAccessCheckDto> rootFolderAccessibilityStream;

    private Disposable rootFoldersAccessibilitySubscriber;

    private ConcurrentHashMap<Long, RootFolderAccessCheckDto> rootFolderAccessibilityMap = new ConcurrentHashMap<>();

    public RootFolderAccessibilityTracker(MediaConnectorFactory mediaConnectorFactory,
                                          @Value("${root-folders.accessible.mp.timeout-in-millis:10000}")
                                                  long rfAccessibleCheckTimeoutInMillis,
                                          @Value("${root-folders.accessible.mp.interval-in-millis:300000}")
                                                  long rfAccessibleCheckInterval,
                                          @Value("${root-folders.accessible.mp.concurrency:5}")
                                                  int rfAccessCheckConcurrency) {
        this.mediaConnectorFactory = mediaConnectorFactory;
        this.rfAccessibleCheckTimeoutInMillis = rfAccessibleCheckTimeoutInMillis;
        this.rfAccessibleCheckInterval = rfAccessibleCheckInterval;
        this.rfAccessCheckConcurrency = rfAccessCheckConcurrency;
        this.rootFolderAccessibilityStream = buildRootFolderAccessibilityCheckStream();
    }

    @PostConstruct
    private void init() {
        rootFoldersAccessibilitySubscriber = rootFolderAccessibilityStream.subscribe();
    }

    private Flux<RootFolderAccessCheckDto> buildRootFolderAccessibilityCheckStream() {
        return Flux.interval(Duration.ofMillis(rfAccessibleCheckInterval))
                .doOnNext(tick -> logStartStatusCheck())
                .flatMap(tick -> Flux
                        .fromIterable(rootFolderAccessibilityMap.values())
                        .parallel(rfAccessCheckConcurrency)
                        .doOnNext(rootFolderAccessCheckDto ->
                                checkAccessibility(rootFolderAccessCheckDto)
                                        .doOnNext(rootFolderAccessCheckDto::setStatus))
                        .sequential());
    }

    private void logStartStatusCheck() {
        logger.debug("Performing directory exists status check for {} root folders",
                rootFolderAccessibilityMap.size());
    }

    public DirectoryExistStatus track(long rootFolderId, String path, boolean forceCheck, MediaConnectionDetailsDto mediaConnectionDetailsDto) {
        logger.trace("Tracking on media {}, root folder {}, path {}",
                mediaConnectionDetailsDto.getMediaType(), rootFolderId, path);
        boolean existed = rootFolderAccessibilityMap.containsKey(rootFolderId);
        if (!existed || forceCheck) { //new rf not yet in cache. Otherwise it is periodically checked by scheduler
            RootFolderAccessCheckDto accessCheckDto = prepareAccessCheckDto(rootFolderId, path, mediaConnectionDetailsDto);
            return checkAccessibility(accessCheckDto)
                    .doOnNext(accessCheckDto::setStatus)
                    .block();
        } else {
            return prepareAccessCheckDto(rootFolderId, path, mediaConnectionDetailsDto).getStatus();
        }
    }

    private Mono<DirectoryExistStatus> checkAccessibility(RootFolderAccessCheckDto rootFolderAccessCheckDto) {
        return Mono.fromSupplier(() -> updateRootFolderAccessbilityStatus(rootFolderAccessCheckDto))
                .timeout(Duration.ofMillis(rfAccessibleCheckTimeoutInMillis), Mono.just(DirectoryExistStatus.NO))
                .doOnError(e -> logger.error("Failed to update mpRootFolderAccessibilityStatusMap", e))
                .onErrorReturn(DirectoryExistStatus.UNKNOWN);
    }

    private RootFolderAccessCheckDto prepareAccessCheckDto(long rootFolderId, String path, MediaConnectionDetailsDto mediaConnectionDetailsDto) {
        RootFolderAccessCheckDto accessCheckDto = rootFolderAccessibilityMap.get(rootFolderId);
        if (accessCheckDto == null) {
            accessCheckDto = RootFolderAccessCheckDto.builder()
                    .isCheckActive(false)
                    .rootFolderId(rootFolderId)
                    .rootFolderPath(path)
                    .mediaConnectionDetailsDto(mediaConnectionDetailsDto)
                    .build();
            rootFolderAccessibilityMap.put(rootFolderId, accessCheckDto);
        } else if (path != null && !path.equals(accessCheckDto.rootFolderPath)) {
            accessCheckDto.rootFolderPath = path;
        }
        return accessCheckDto;
    }

    private DirectoryExistStatus updateRootFolderAccessbilityStatus(RootFolderAccessCheckDto rootFolderAccessCheckDto) {
        MediaConnectionDetailsDto mediaConnectionDetailsDto = rootFolderAccessCheckDto.getMediaConnectionDetailsDto();
        FileMediaConnector mediaConnector = mediaConnectorFactory.getMediaConnector(mediaConnectionDetailsDto);
        //TODO : for Exchange - pass a sample mailbox here (DAMAIN-6774)
        boolean directoryExists = mediaConnector.isDirectoryExists(rootFolderAccessCheckDto.rootFolderPath, true);
        logger.trace("On media {}, directory exists status is {} for root folder {}, path: {}",
                mediaConnectionDetailsDto.getMediaType(), directoryExists, rootFolderAccessCheckDto.rootFolderId,
                rootFolderAccessCheckDto.rootFolderPath);
        DirectoryExistStatus directoryExistStatus = directoryExists ? DirectoryExistStatus.YES : DirectoryExistStatus.NO;
        return directoryExistStatus;
    }

    @Data
    @NoArgsConstructor
    @Builder
    @AllArgsConstructor
    public static class RootFolderAccessCheckDto {
        volatile long rootFolderId;
        volatile String rootFolderPath;
        volatile boolean isCheckActive;
        private volatile int statusOrdinal;
        private MediaConnectionDetailsDto mediaConnectionDetailsDto;

        void setStatus(DirectoryExistStatus status) {
            if (status == null) {
                statusOrdinal = DirectoryExistStatus.UNKNOWN.ordinal();
            } else {
                statusOrdinal = status.ordinal();
            }
        }

        public DirectoryExistStatus getStatus() {
            return DirectoryExistStatus.values()[statusOrdinal];
        }
    }

    @PreDestroy
    private void destroy() {
        rootFoldersAccessibilitySubscriber.dispose();
    }

    public Flux<RootFolderAccessCheckDto> getRootFolderAccessibilityStream() {
        return rootFolderAccessibilityStream;
    }
}
