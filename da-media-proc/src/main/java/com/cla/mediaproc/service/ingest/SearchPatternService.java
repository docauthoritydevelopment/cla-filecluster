package com.cla.mediaproc.service.ingest;

import com.cla.common.domain.dto.extraction.SearchPatternCountDto;
import com.cla.common.domain.dto.filesearchpatterns.TextSearchPatternImpl;
import com.cla.common.domain.dto.mediaproc.SearchPatternServiceInterface;
import com.cla.common.utils.PatternSearcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

/**
 * service allowing searching of patterns in texts and counting their appearances
 *
 * Created by: yael
 * Created on: 8/5/2018
 */
@Service
public class SearchPatternService implements SearchPatternServiceInterface {

    private static final Logger logger = LoggerFactory.getLogger(SearchPatternService.class);

    @Value("${no-database:false}")
    private boolean noDatabase;

    @Value("${ingester.patternSearchActive:true}")
    private boolean patternSearchActive;

    @Value("${ingester.pattern-search.ignored-values-list:}")
    private String[] ignoreValuesList;

    @Value("${ingester.pattern-search.count-distinct-matches:true}")
    private boolean countDistinctMatches;

    private PatternSearcher patternSearcher;
    private Map<Integer, TextSearchPatternImpl> patterns = new HashMap<>();
    private AtomicBoolean gotPatterns = new AtomicBoolean(false);


    @PostConstruct
    void init() {
        patternSearcher = new PatternSearcher(patternSearchActive, countDistinctMatches, ignoreValuesList);
    }

    @Override
    public Set<SearchPatternCountDto> getSearchPatternsCounting(String fileText){
        Collection<TextSearchPatternImpl> allFileSearchPatternImpls = getAllFileSearchPatternImpls();
        return patternSearcher.getSearchPatternsCounting(fileText, false, allFileSearchPatternImpls);
    }

    public Set<SearchPatternCountDto> getSearchPatternsCounting(String fileText, boolean withMatchingTerms){
        Collection<TextSearchPatternImpl> allFileSearchPatternImpls = getAllFileSearchPatternImpls();
        return patternSearcher.getSearchPatternsCounting(fileText, withMatchingTerms, allFileSearchPatternImpls);
    }

    @Override
    public Collection<TextSearchPatternImpl> getAllFileSearchPatternImpls() {
        waitForPatterns();
        return patterns.values().stream().filter(p -> p.isActive() && p.isSubCategoryActive()).collect(Collectors.toList());
    }

    private void waitForPatterns() {
        while (!gotPatterns.get()) {
            try {
                logger.warn("Did not get search patterns from server, waiting with ingest until it arrives...");
                Thread.sleep(10000);
            } catch (Exception e) {
            }
        }
    }

    public synchronized void markGotPatterns() {
        if (!gotPatterns.get()) {
            gotPatterns.set(true);
        }
    }

    public synchronized void updatePattern(TextSearchPatternImpl pattern) {
        patterns.put(pattern.getId(), pattern);
        if (!gotPatterns.get()) {
            gotPatterns.set(true);
        }
    }

    public boolean waitingForPatterns() {
        return !gotPatterns.get();
    }

    public void setMaxSingleSearchPatternMatches(Integer maxSingleSearchPatternMatches) {
        patternSearcher.setMaxSingleSearchPatternMatches(maxSingleSearchPatternMatches);
    }

    public void setSearchPatternStopSearchAfterMulti(boolean searchPatternStopSearchAfterMulti) {
        patternSearcher.setSearchPatternStopSearchAfterMulti(searchPatternStopSearchAfterMulti);
    }

    public void setSearchPatternCountThresholdMulti(Integer searchPatternCountThresholdMulti) {
        patternSearcher.setSearchPatternCountThresholdMulti(searchPatternCountThresholdMulti);
    }
}
