package com.cla.mediaproc.service.filecontent;

import com.cla.connector.domain.dto.file.FileType;
import com.cla.connector.domain.dto.media.MediaType;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class MetadataExtractorFactory {

    public Optional<MetadataExtractor> getExtractor(MediaType mediaType, FileType fileType){
        if (fileType == FileType.PDF || fileType == FileType.SCANNED_PDF) {
            // TODO: should create a custom extractor for PDF until we resolve pdf-box versioning issue
            //  between ClaPdfExtractor and Tika. See DAMAIN-9066
            return Optional.empty();
        }
        switch (mediaType){
            case FILE_SHARE:
                    return Optional.of(new TikaMetadataExtractor());
            case SHARE_POINT:
            case ONE_DRIVE:
            default:
                return Optional.empty();
        }
    }

}
