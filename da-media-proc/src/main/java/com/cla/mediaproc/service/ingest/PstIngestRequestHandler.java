package com.cla.mediaproc.service.ingest;

import com.cla.common.domain.dto.messages.IngestRequestMessage;
import com.cla.common.domain.dto.messages.IngestTaskParameters;
import com.cla.common.domain.pst.PstPath;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.file.FileContentDto;
import com.cla.connector.domain.dto.media.MediaConnectionDetailsDto;
import com.cla.connector.utils.Pair;
import com.cla.mediaproc.service.filecontent.FileContentManagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;

/**
 * Created By Itai Marko
 */
@Service
public class PstIngestRequestHandler extends ContentIngestHandler {

    @Autowired
    private FileContentManagerService fileContentManagerService;

    @Override
    protected Pair<FileContentDto, ? extends ClaFilePropertiesDto> extractFileData(IngestRequestMessage ingestRequestMessage)
            throws IOException {

        IngestTaskParameters ingestTaskParameters = ingestRequestMessage.getPayload();
        String filePath = ingestTaskParameters.getMediaEntityId();
        if (!PstPath.isPstEntryPath(filePath)) {
            throw new IllegalStateException(
                    "Invalid non-pst file path: " + filePath +
                    " FileType: " + ingestTaskParameters.getFileType() +
                    " ingestRequestMessage: " + ingestRequestMessage.toString());
        }
        MediaConnectionDetailsDto mediaConnectionDetailsDto = ingestTaskParameters.getMediaConnectionDetailsDto();
        return fileContentManagerService.getPstEntryData(filePath, mediaConnectionDetailsDto, false);
    }

}
