package com.cla.mediaproc.service.ingest;

import com.cla.common.domain.dto.mediaproc.OtherFile;
import com.cla.common.domain.dto.messages.*;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.file.FileType;
import com.cla.connector.domain.dto.error.ProcessingErrorType;
import com.cla.connector.mediaconnector.FileMediaConnector;
import com.cla.connector.utils.Pair;
import com.cla.filecluster.service.IngestRequestHandler;
import com.cla.filecluster.service.media.MediaConnectorFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

@Service
public class OfflineFileIngestRequestHandler implements IngestRequestHandler {

    @Autowired
    private MediaConnectorFactory mediaConnectorFactory;

    @Autowired
    private MediaEntryIngestRequestHandler mediaEntryIngestRequestHandler;

    @Value("${ingester.first-scan-skip-offline-files:false}")
    private boolean firstScanSkipOfflineFiles;

    @Value("${ingester.re-scan-skip-offline-files:false}")
    private boolean rescanSkipOfflineFiles;

    private final static Logger logger = LoggerFactory.getLogger(OfflineFileIngestRequestHandler.class);

    @Override
    public List<Pair<IngestTaskParameters, IngestMessagePayload>> handleIngestionRequest(
            IngestRequestMessage ingestRequestMessage) throws IOException {

        IngestTaskParameters ingestTaskParameters = ingestRequestMessage.getPayload();
        boolean skipOfflineFiles = (ingestTaskParameters.isFirstScan() && firstScanSkipOfflineFiles) ||
                (!ingestTaskParameters.isFirstScan() && rescanSkipOfflineFiles);
        if (skipOfflineFiles) {
            logger.trace("skipOfflineFiles task {}",ingestRequestMessage.getTaskId());
            FileType fileType = ingestTaskParameters.getFileType();
            IngestMessagePayload ingestPayload = createIngestPayload(fileType);
            FileMediaConnector mediaConnector = mediaConnectorFactory.getMediaConnector(ingestTaskParameters.getMediaConnectionDetailsDto());
            //TODO: [Costa] file attributes should be cached to prevent unnecessary external api calls, it is already fetched in the GeneralIngestRequestHandlerProvider
            ClaFilePropertiesDto claFilePropertiesDto = mediaConnector.getFileAttributes(ingestTaskParameters.getFilePath());
            ingestPayload.setClaFileProperties(claFilePropertiesDto);
            ingestPayload.setErrorType(ProcessingErrorType.ERR_OFFLINE_SKIPPED);
            return Collections.singletonList(Pair.of(ingestTaskParameters, ingestPayload));
        } else {
            logger.trace("don't skipOfflineFiles task {}", ingestRequestMessage.getTaskId());
            return mediaEntryIngestRequestHandler.handleIngestionRequest(ingestRequestMessage);
        }
    }

    private IngestErrorInfo createSkippedIngestionError(IngestTaskParameters ingestTaskParameters) {
        IngestErrorInfo info = new IngestErrorInfo();
        info.setText(String.format("Skipped due to being offline, file %s", ingestTaskParameters.getFilePath()));
        info.setType(ProcessingErrorType.ERR_OFFLINE_SKIPPED);
        return info;
    }

    private IngestMessagePayload createIngestPayload(FileType fileType) {
        if (fileType == null) {
            return new OtherFile();
        }
        switch (fileType) {
            case WORD:
                return new WordIngestResult();
            case EXCEL:
                return new ExcelIngestResult();
            case PDF:
            case SCANNED_PDF:
                return new WordIngestResult();
            case OTHER:
            default:
                return new OtherFile();
        }
    }
}