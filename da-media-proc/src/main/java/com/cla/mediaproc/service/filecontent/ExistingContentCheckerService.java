//package com.cla.mediaproc.service.filecontent;
//
//import com.cla.common.connectors.ExistingContentConnector;
//import com.cla.common.domain.dto.messages.ContentCheckerType;
//import com.cla.common.domain.dto.messages.ContentMetadataRequestMessage;
//import com.cla.common.domain.dto.messages.ContentMetadataResponseMessage;
//import com.cla.mediaproc.RemoteAppServerService;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.stereotype.Service;
//
///**
// * Created by uri on 16-Mar-17.
// */
//@Service
//@Qualifier("existingContentCheckerService")
//public class ExistingContentCheckerService implements ExistingContentConnector {
//
//    @Value("${filecontent.check.type:REST}")
//    private ContentCheckerType contentCheckerType;
//
//    @Autowired
//    RemoteAppServerService remoteAppServerService;
//
//    private static final Logger logger = LoggerFactory.getLogger(ExistingContentCheckerService.class);
//
//    @Override
//    public ContentMetadataResponseMessage checkForExistingContent(ContentMetadataRequestMessage mdRequest) {
//        switch (contentCheckerType) {
//
//            case REST:
//                return remoteAppServerService.checkForExistingContent(mdRequest);
//            case DUMMY:
//                logger.warn("USING DUMMY CONTENT METADATA CHECKER!");
//                return new ContentMetadataResponseMessage(null,false);
//                default:
//                    throw new RuntimeException("unsupported filecontent checker type "+contentCheckerType);
//        }
//
//    }
//}
