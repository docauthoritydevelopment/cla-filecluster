package com.cla.mediaproc.service.filecontent;

import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.media.MediaType;
import com.cla.connector.mediaconnector.FileMediaConnector;
import com.cla.connector.mediaconnector.fileshare.preserveaccesstime.PreserveAccessedTimeFileInputStream;
import com.cla.connector.utils.FileNamingUtils;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.channels.FileChannel;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;

import static java.util.Map.Entry;
import static java.util.stream.Collectors.toMap;

@Service
public class MetadataService {

	private Logger logger = LoggerFactory.getLogger(MetadataService.class);

	@Autowired
	private MetadataExtractorFactory metadataExtractorFactory;

	private Config config;

	@PostConstruct
	private void initConfig(){
		//some config init logic by external json or whatever?
		config = new Config();
		config.setIncludedFileTypes(Sets.newHashSet("xlsx", "docx", "pdf"));
	}

	/**
	*	Extracts locally available metadata from given file.  No network calls are needed.
	* */
	Map<String, String> extractLocal(ClaFilePropertiesDto fileProperties, InputStream inputStream) {

		//validate the file type against the config.
		String extension = Optional.ofNullable(FileNamingUtils.getFilenameExtension(fileProperties.getFileName()))
				.orElse("").toLowerCase();
		if (!config.getIncludedFileTypes().contains(extension)) {
			return ImmutableMap.of();
		}

		//extract the MD
		Optional<MetadataExtractor> metadataExtractor = metadataExtractorFactory.getExtractor(MediaType.FILE_SHARE, fileProperties.getType());
		if (metadataExtractor.isPresent()) {
			try {
				logger.debug("try extract of metadata for file {}", fileProperties.getFileName());
				Map<String, String> metadataMap = metadataExtractor.get().extractLocal(inputStream);
				//filter/manipulate MD as preconfigured
				return metadataMap.entrySet().stream()
						.filter(entry -> config.getKeysPredicate(extension).test(entry.getKey()))
						.collect(toMap(Entry::getKey, Entry::getValue));
			} catch (Throwable e) {
				logger.error("fail extract of metadata for file {}", fileProperties.getFileName(), e);
			} finally {
				resetFilePosition(inputStream);
			}
		}

		return ImmutableMap.of();
	}

	private void resetFilePosition(InputStream inputStream) {
		try {
			if (inputStream.markSupported()) {
				inputStream.reset();
			} else if (inputStream instanceof FileInputStream) {
				FileChannel fc = ((FileInputStream)inputStream).getChannel();
				fc.position(0);
			} else {
				logger.warn("problem reset of input stream, ingest might fail {}", inputStream.getClass());
			}
		} catch (Exception e) {
			logger.error("input stream reset err {}", inputStream.getClass(), e);
		}
	}

	/**
	 *	Extracts metadata from remote source. Such source can be BOX metadata, MIP, ect.
	 * */
	public Map<String, String> extractExternal(FileMediaConnector mediaConnector, ClaFilePropertiesDto fileProperties, InputStream inputStream) {
		//tbd
		return null;
	}

	public static class Config{
		private Set<String> includedFileTypes = Sets.newHashSet();
		private Map<String, Set<String>> includedMetadataKeys = Maps.newHashMap();

		public Set<String> getIncludedFileTypes() {
			return includedFileTypes;
		}

		public Config setIncludedFileTypes(Set<String> includedFileTypes) {
			this.includedFileTypes = includedFileTypes;
			return this;
		}

		public Map<String, Set<String>> getIncludedMetadataKeys() {
			return includedMetadataKeys;
		}

		public Predicate<String> getKeysPredicate(String extension){
			return s -> {
				if (includedMetadataKeys == null || includedMetadataKeys.isEmpty()){
					return true;
				}
				return includedMetadataKeys.get(extension).contains(s);
			};
		}

		public Config setIncludedMetadataKeys(Map<String, Set<String>> includedMetadataKeys) {
			this.includedMetadataKeys = includedMetadataKeys;
			return this;
		}
	}
}
