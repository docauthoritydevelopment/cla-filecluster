package com.cla.mediaproc.sharepoint;

import com.cla.common.domain.dto.messages.ScanRequestMessage;
import com.cla.connector.domain.dto.messages.ScanTaskParameters;
import com.cla.mediaproc.appserver.AppServerProcessingService;
import com.cla.mediaproc.base.BaseTests;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by uri on 20-Jun-17.
 */
public class SharePointMediaConnectorTests extends BaseTests {

    private final static Logger logger = LoggerFactory.getLogger(SharePointMediaConnectorTests.class);

    private static final String SCAN_PATH = "";

    @Autowired
    private AppServerProcessingService appServerProcessingService;

    /**
     * 20 09:25:23,172  INFO [lis_1] ScanConsumer:67 - ***** Start Scan JobId:1 taskId: 1
     * according to:
     * ScanTaskParameters{runId=1
     * , path='https://docauthority.sharepoint.com/tests/basic'
     * , rootFolderId=1
     * , firstScan=true
     * , rootFolderScanLimit=200
     * , nonEqExcludedRules=[]
     * , skipPaths=null
     * , skipDirnames=null
     * , typesToScan=[WORD, EXCEL, PDF]
     * , extensionsToScan=[xlsx, pdf, doc, xls, docx]
     * , extensionsToIgnore=[]
     * , currentScanCount=0
     * , connectionDetails=MediaConnectionDetailsDto
     *   {id=1
     *   , name='https:ro-admin2@docauthority.onmicrosoft.com@docauthority.sharepoint.com/tests'}}
     */
    @Test
    @Ignore
    public void testScanSharePoint365RootFolder() {
        logger.info("testScanSharePoint365RootFolder");

        ScanTaskParameters scanTaskParameters = ScanTaskParameters.create()
                .setPath(SCAN_PATH);
        ScanRequestMessage scanRequestMessage = new ScanRequestMessage(scanTaskParameters);
        appServerProcessingService.consumeStartScanRequest(scanRequestMessage);
    }
}
