package com.cla.mediaproc.appserver;

import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.file.FileType;
import com.cla.connector.domain.dto.error.ProcessingErrorType;
import com.cla.connector.domain.dto.media.MediaConnectionDetailsDto;
import com.cla.common.domain.dto.mediaproc.excel.ExcelParseParams;
import com.cla.common.domain.dto.mediaproc.excel.ExcelWorkbook;
import com.cla.common.domain.dto.messages.*;
import com.cla.connector.mediaconnector.AbstractMediaConnector;
import com.cla.connector.utils.Pair;
import com.cla.filecluster.service.IngestRequestHandlerProvider;
import com.cla.filecluster.service.media.MediaConnectorFactory;
import com.cla.mediaproc.appserver.connectors.AppServerCommService;
import com.cla.mediaproc.service.ingest.MediaEntryIngestRequestHandler;
import com.cla.mediaproc.service.managers.MediaProcessorStateService;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.Silent.class)
public class IngestConsumerTest {

    @Mock
    private AppServerCommService appServerCommService;

    @Mock
    private MediaEntryIngestRequestHandler ingestManagerService;

    @Mock
    private MediaProcessorStateService mediaProcessorStateService;

    @Mock
    private MediaConnectorFactory mediaConnectorFactory;

    @Mock
    private AbstractMediaConnector mediaConnector;

    @Mock
    private MediaEntryIngestRequestHandler mediaEntryIngestRequestHandler;

    @Mock
    private IngestRequestHandlerProvider ingestRequestHandlerProvider;

    @InjectMocks
    private IngestConsumer ingestConsumer;

    @After
    public void resetMocks() {
        Mockito.reset(ingestManagerService);
        Mockito.reset(appServerCommService);
        Mockito.reset(mediaProcessorStateService);
    }

    @Test
    public void testHandleIngestionRequest() throws Exception {

        IngestTaskParameters parameters = IngestTaskParameters.create()
                .setIngestContents(false)
                .setJobId(5)
                .setMediaConnectionDetailsDto(new MediaConnectionDetailsDto());

        parameters.setRootFolderId(1L)
                .setFilePath("")
                .setFileType(FileType.EXCEL)
                .setFileId(1L);

        IngestRequestMessage requestMessage = new IngestRequestMessage(parameters);
        ExcelParseParams excelParseParams = new ExcelParseParams(true, 3, null, true, false);
        IngestMessagePayload ingestProcessingResult = new ExcelWorkbook(excelParseParams);
        ingestProcessingResult.setClaFileProperties(ClaFilePropertiesDto.create());

        when(ingestManagerService.handleIngestionRequest(requestMessage)).thenReturn(
                Collections.singletonList(Pair.of(parameters ,ingestProcessingResult)));

        when(mediaConnectorFactory.getMediaConnector(any())).thenReturn(mediaConnector);

        when(mediaConnector.getFileAttributes(anyString())).thenReturn(ingestProcessingResult.getClaFileProperties());

        when(ingestRequestHandlerProvider.getIngestRequestHandler(any())).thenReturn(mediaEntryIngestRequestHandler);

        when(mediaEntryIngestRequestHandler.handleIngestionRequest(any())).thenReturn(
                Collections.singletonList(Pair.of(parameters ,ingestProcessingResult)));

        ArgumentCaptor<IngestResultMessage> resultCaptor = ArgumentCaptor.forClass(IngestResultMessage.class);

        ingestConsumer.consumeIngestionRequest(requestMessage);

        verify(appServerCommService, times(1)).sendIngestResult(resultCaptor.capture());
        verify(mediaProcessorStateService, times(1)).incIngestTaskFinished();

        IngestResultMessage val = resultCaptor.getValue();
        assertEquals(val.getPayloadType(), MessagePayloadType.INGEST_EXCEL);
    }

    @Test
    public void testHandleIngestionInvalidRequest() throws Exception {
        IngestRequestMessage requestMessage = new IngestRequestMessage();
        ArgumentCaptor<IngestResultMessage> resultCaptor = ArgumentCaptor.forClass(IngestResultMessage.class);
        ingestConsumer.consumeIngestionRequest(requestMessage);
        verify(appServerCommService, times(1)).sendIngestResult(resultCaptor.capture());
        verify(mediaProcessorStateService, never()).incIngestTaskFinished();
        verify(ingestManagerService, never()).handleIngestionRequest(any());
        IngestResultMessage val = resultCaptor.getValue();
        assertEquals(val.getPayloadType(), MessagePayloadType.INGEST_ERROR);
    }

    @Test
    public void testHandleIngestionFailure() throws Exception {

        IngestTaskParameters parameters = IngestTaskParameters.create()
                .setIngestContents(false)
                .setJobId(5)
                .setMediaConnectionDetailsDto(new MediaConnectionDetailsDto());

        parameters.setRootFolderId(1L)
                .setFilePath("")
                .setFileType(FileType.EXCEL)
                .setFileId(1L);

        IngestRequestMessage requestMessage = new IngestRequestMessage(parameters);

        ExcelIngestResult ingestProcessingResult = new ExcelIngestResult();
        ingestProcessingResult.setClaFileProperties(ClaFilePropertiesDto.create());
        ingestProcessingResult.setErrorType(ProcessingErrorType.ERR_FILE_NOT_FOUND);

        when(ingestManagerService.handleIngestionRequest(requestMessage)).thenReturn(
                Collections.singletonList(Pair.of(parameters ,ingestProcessingResult)));
        when(mediaConnectorFactory.getMediaConnector(any())).thenReturn(mediaConnector);
        when(mediaConnector.getFileAttributes(anyString())).thenReturn(ingestProcessingResult.getClaFileProperties());
        when(ingestRequestHandlerProvider.getIngestRequestHandler(any())).thenReturn(mediaEntryIngestRequestHandler);
        when(mediaEntryIngestRequestHandler.handleIngestionRequest(any())).thenReturn(
                Collections.singletonList(Pair.of(parameters ,ingestProcessingResult)));

        ArgumentCaptor<IngestResultMessage> resultCaptor = ArgumentCaptor.forClass(IngestResultMessage.class);
        ingestConsumer.consumeIngestionRequest(requestMessage);
        verify(appServerCommService, times(1)).sendIngestResult(resultCaptor.capture());
        verify(mediaProcessorStateService, times(1)).incIngestTaskFinishedWithError();
        IngestResultMessage val = resultCaptor.getValue();
        assertNull(val.getPayloadType());
    }
}
