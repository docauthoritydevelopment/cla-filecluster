package com.cla.mediaproc.appserver;

import com.cla.common.domain.dto.crawler.JobStateByDate;
import com.cla.common.domain.dto.jobmanager.JobState;
import com.cla.common.domain.dto.messages.control.ControlStateRequestPayload;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class JobStateMonitorTest {

    @Test
    public void testJobActive() {
        JobStateMonitor mon = new JobStateMonitor();

        // no data
        Assert.assertEquals(true, mon.isJobActive(1L, null));
        Assert.assertEquals(true, mon.isJobActive(1L, 5L));

        ControlStateRequestPayload msg = new ControlStateRequestPayload();
        List<JobStateByDate> states = new ArrayList<>();
        states.add(JobStateByDate.of(1L, JobState.IN_PROGRESS, 5L));
        msg.setJobStates(states);
        mon.updateJobStates(msg);

        // one data no timestamp
        Assert.assertEquals(true, mon.isJobActive(1L, null));

        // one data matching
        Assert.assertEquals(true, mon.isJobActive(1L, 5L));

        // one data not matching
        Assert.assertEquals(false, mon.isJobActive(1L, 4L));


        states = new ArrayList<>();
        states.add(JobStateByDate.of(1L, JobState.PAUSED, 7L));
        msg.setJobStates(states);
        mon.updateJobStates(msg);

        // newly paused
        Assert.assertEquals(false, mon.isJobActive(1L, null));
        Assert.assertEquals(false, mon.isJobActive(1L, System.currentTimeMillis()));
    }
}