package com.cla.mediaproc.appserver;

import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.file.FileContentDto;
import com.cla.connector.domain.dto.file.ServerResourceDto;
import com.cla.connector.domain.dto.media.MediaType;
import com.cla.connector.domain.dto.media.MediaConnectionDetailsDto;
import com.cla.common.domain.dto.messages.*;
import com.cla.connector.domain.dto.messages.DirListingPayload;
import com.cla.connector.domain.dto.messages.ScanTaskParameters;
import com.cla.connector.mediaconnector.AbstractMediaConnector;
import com.cla.connector.progress.ProgressTracker;
import com.cla.connector.utils.Pair;
import com.cla.filecluster.service.media.MediaConnectorFactory;
import com.cla.kvdb.xodus.XodusStringComparator;
import com.cla.mediaproc.appserver.connectors.AppServerCommService;
import com.google.common.collect.Lists;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.File;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.ForkJoinPool;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Mockito.*;

/**
 * Testing scan consumer
 * Created by vladi on 8/9/2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class ScanConsumerTest {

    private static final String RESOURCE = "./src/test/resources/com.cla.mediaproc.appserver/scan/dir-listing.txt";

    @Mock
    private MediaConnectorFactory mediaConnectorFactory;

    @Mock
    private AppServerCommService appServerCommService;

    @Mock
    private JobStateMonitor jobStateMonitor;

    @InjectMocks
    private ScanConsumer scanConsumer = new ScanConsumer();

    private List<String> dirListing;

    @Before
    public void init(){
        try (Scanner s = new Scanner(new File(RESOURCE))) {
            s.useDelimiter("\r\n");
            dirListing = Lists.newLinkedList();
            while (s.hasNext()) {
                dirListing.add(s.next());
            }
        }
        catch(Exception e){
            Assert.fail(e.toString());
        }

        ReflectionTestUtils.setField(scanConsumer, "dirListingPageLimit", 100);
    }

    /**
     * Test the ability to send directory listings and split them to pages
     */
    @Test @Ignore
    public void consumeDirListingTest(){

        //-------------------------- set up -------------------------------------------------------------------
        // make the factory return mock media connector (see below)
        when(mediaConnectorFactory.getMediaConnector(any())).thenReturn(mediaConnectorSim());
        // make job state monitor always return true on job activity test
        when(jobStateMonitor.isJobActive(any(), any())).thenReturn(true);
        ArgumentCaptor<ScanResultMessage> resultCaptor = ArgumentCaptor.forClass(ScanResultMessage.class);

        //-------------------------- actual scan call ---------------------------------------------------------
        scanConsumer.startScanMediaFiles(getScanRequestMessage());

        //-------------------------- verifications ------------------------------------------------------------
        verify(appServerCommService, atLeastOnce()).sendScanResult(resultCaptor.capture());
        verify(appServerCommService, atLeastOnce()).sendBufferedScanResult(resultCaptor.capture(), anyBoolean());

        List<ScanResultMessage> allValues = resultCaptor.getAllValues();
        Assert.assertEquals(4, allValues.size());

        // check that payload types are correct
        Assert.assertEquals(MessagePayloadType.SCAN_DIR_LISTING, allValues.get(0).getPayloadType());
        Assert.assertEquals(MessagePayloadType.SCAN_DIR_LISTING, allValues.get(1).getPayloadType());
        Assert.assertEquals(MessagePayloadType.SCAN_DIR_LISTING, allValues.get(2).getPayloadType());
        Assert.assertEquals(MessagePayloadType.SCAN_DONE, allValues.get(3).getPayloadType());

        // check for correct paging
        Assert.assertEquals(100, payload(allValues, 0).size());
        Assert.assertEquals(100, payload(allValues, 1).size());
        Assert.assertEquals(76, payload(allValues, 2).size());

        // check for correct page numbering
        Assert.assertEquals(1, payload(allValues, 0).getPage());
        Assert.assertEquals(2, payload(allValues, 1).getPage());
        Assert.assertEquals(3, payload(allValues, 2).getPage());

        // check for correct last page flag
        Assert.assertFalse(payload(allValues, 0).isLastPage());
        Assert.assertFalse(payload(allValues, 1).isLastPage());
        Assert.assertTrue(payload(allValues, 2).isLastPage());
    }

    @NotNull
    private ScanRequestMessage getScanRequestMessage() {
        ScanRequestMessage request = new ScanRequestMessage();

        ScanTaskParameters scanParams = ScanTaskParameters.create();
        scanParams.setRootFolderId(1L);
        scanParams.setRunId(1L);
        scanParams.setJobId(1L);
        scanParams.setConnectionDetails(new MediaConnectionDetailsDto());

        request.setPayload(scanParams);
        request.setTaskId(1L);
        request.setJobId(1L);
        request.setRunContext(1L);
        return request;
    }

    /**
     * Extract payload from captured parameters list
     * @param allValues captured parameters list
     * @param index     index in the list
     * @return          payload
     */
    private DirListingPayload payload(List<ScanResultMessage> allValues, int index){
        return (DirListingPayload)allValues.get(index).getPayload();
    }

    /**
     * Create mock directory listing
     * @return original un-paged payload
     */
    private DirListingPayload createDirListing(){
        DirListingPayload dirListingPayload = new DirListingPayload();
        dirListingPayload.setFolderPath("D:\\Data");
        List<String> sortedList = dirListing.stream()
                .sorted(new XodusStringComparator())
                .collect(Collectors.toList());
        dirListingPayload.setDirectoryList(sortedList);
        return dirListingPayload;
    }

    /**
     * create mock media connector
     * @return media connector
     */
    private AbstractMediaConnector mediaConnectorSim(){
        return new AbstractMediaConnector() {
            @Override
            public MediaType getMediaType() {
                return null;
            }

            @Override
            public ClaFilePropertiesDto getFileAttributes(String fileId){
                return null;
            }

            @Override
            public ClaFilePropertiesDto getMediaItemAttributes(String mediaItemId){
                return null;
            }

            @Override
            public InputStream getInputStream(ClaFilePropertiesDto props){
                return null;
            }

            @Override
            public void streamMediaItems(ScanTaskParameters scanParams, Consumer<ClaFilePropertiesDto> filePropertiesConsumer,
                                         Consumer<DirListingPayload> directoryListingConsumer,
                                         Consumer<String> createScanTaskConsumer, Predicate<Pair<Long, Long>> scanActivePredicate,
                                         ProgressTracker filePropsProgressTracker) {
                if (directoryListingConsumer != null) {
                    directoryListingConsumer.accept(createDirListing());
                }
            }

            @Override
            public void concurrentStreamMediaItems(ForkJoinPool forkJoinPool, ScanTaskParameters scanParams,
                                                   Consumer<ClaFilePropertiesDto> filePropertiesConsumer,
                                                   Consumer<DirListingPayload> directoryListingConsumer,
                                                   Consumer<String> createScanTaskConsumer,
                                                   Predicate<Pair<Long, Long>> scanActivePredicate,
                                                   ProgressTracker filePropsProgressTracker) {
                if (directoryListingConsumer != null) {
                    directoryListingConsumer.accept(createDirListing());
                }
            }

            @Override
            public boolean isDirectoryExists(String path, boolean ignoreAccessErrors) {
                return true;
            }

            @Override
            public void fetchAcls(ClaFilePropertiesDto claFileProp) {
            }

            @Override
            public List<ServerResourceDto> testConnection() {
            	return Arrays.asList(new ServerResourceDto());
            }

            @Override
            public String getIdentifier() {
                return null;
            }

			@Override
            public FileContentDto getFileContent(String filename, boolean forUserDownload){
                return null;
            }

			@Override
			public List<ServerResourceDto> browseSubFolders(String folderMediaEntityId) {
				return null;
			}
		};
    }
}
