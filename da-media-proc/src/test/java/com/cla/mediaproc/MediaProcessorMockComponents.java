package com.cla.mediaproc;

import com.cla.common.domain.dto.messages.IngestMessagePayload;
import com.cla.common.domain.dto.messages.IngestRequestMessage;
import com.cla.common.domain.dto.messages.IngestTaskParameters;
import com.cla.common.media.files.excel.ExcelFilesService;
import com.cla.common.media.files.excel.ExcelIngesterService;
import com.cla.common.media.files.other.OtherFileIngesterService;
import com.cla.common.media.files.pdf.PdfIngesterService;
import com.cla.common.media.files.word.WordIngesterService;
import com.cla.connector.domain.dto.file.FileType;
import com.cla.connector.utils.Pair;
import com.cla.filecluster.service.IngestRequestHandlerProvider;
import com.cla.filecluster.service.media.MediaConnectorFactory;
import com.cla.filecluster.util.executors.ThreadPoolBlockingExecutor;
import com.cla.mediaproc.appserver.*;
import com.cla.mediaproc.appserver.connectors.AppServerCommService;
import com.cla.mediaproc.service.filecontent.FileContentManagerService;
import com.cla.mediaproc.service.ingest.MediaEntryIngestRequestHandler;
import com.cla.mediaproc.service.managers.MediaProcessorStateService;
import org.mockito.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MediaProcessorMockComponents {

	public static MediaProcessorMockComponents newMock() {
        MediaProcessorMockComponents mediaProcessorMockComponents = new MediaProcessorMockComponents();
        MockitoAnnotations.initMocks(mediaProcessorMockComponents);
        return mediaProcessorMockComponents;
    }

    private MediaProcessorMockComponents() {
    }

    @Spy
    @InjectMocks
    private IngestConsumer ingestConsumer = new IngestConsumer();

    @Mock
    private JobStateMonitor jobStateMonitor;

    @Spy
    @InjectMocks
    private AppServerProcessingService appServerProcessingService = Mockito.mock(AppServerProcessingService.class);

    @Mock
    private MediaConnectorFactory mediaConnectorFactory;

    @Mock
    private ScanConsumer scanConsumer;

    @Spy
    private ThreadPoolBlockingExecutor blockingExecutor = new ThreadPoolBlockingExecutor("ingest", 10, 15, 1, true);

	@Mock
    private WordIngesterService wordIngesterService;

    @Mock
    private ExcelIngesterService excelIngesterService;

    @Mock
    private PdfIngesterService pdfIngesterService;

    @Mock
    private AppServerCommService appServerCommService;

    @Mock
    private OtherFileIngesterService otherFileIngesterService;

    @Mock
    private ExcelFilesService excelFilesService;

    @Mock
    private MediaProcessorStateService mediaProcessorStateService;

    @Spy
    @InjectMocks
    private IngestRequestHandlerProvider ingestRequestHandlerProvider = new GeneralIngestRequestHandlerProvider();

    @Spy
    @InjectMocks
    private FileContentManagerService fileContentManagerService = new FileContentManagerService();

    @Spy
    @InjectMocks
    private MediaEntryIngestRequestHandlerTracker mediaEntryIngestRequestHandler = new MediaEntryIngestRequestHandlerTracker();

    public IngestConsumer getIngestConsumer() {
        return ingestConsumer;
    }

    public JobStateMonitor getJobStateMonitor() {
        return jobStateMonitor;
    }

    public AppServerProcessingService getAppServerProcessingService() {
        return appServerProcessingService;
    }

    public MediaConnectorFactory getMediaConnectorFactory() {
        return mediaConnectorFactory;
    }

    public ScanConsumer getScanConsumer() {
        return scanConsumer;
    }

    public ThreadPoolBlockingExecutor getBlockingExecutor() {
        return blockingExecutor;
    }

    public WordIngesterService getWordIngesterService() {
        return wordIngesterService;
    }

    public ExcelIngesterService getExcelIngesterService() {
        return excelIngesterService;
    }

    public PdfIngesterService getPdfIngesterService() {
        return pdfIngesterService;
    }

    public AppServerCommService getAppServerCommService() {
        return appServerCommService;
    }

    public OtherFileIngesterService getOtherFileIngesterService() {
        return otherFileIngesterService;
    }

    public ExcelFilesService getExcelFilesService() {
        return excelFilesService;
    }

    public MediaProcessorStateService getMediaProcessorStateService() {
        return mediaProcessorStateService;
    }

    public FileContentManagerService getFileContentManagerService() {
        return fileContentManagerService;
    }

    public MediaEntryIngestRequestHandlerTracker getMediaEntryIngestRequestHandler() {
        return mediaEntryIngestRequestHandler;
    }

    public IngestRequestHandlerProvider getIngestRequestHandlerProvider() {
        return ingestRequestHandlerProvider;
    }

    public class MediaEntryIngestRequestHandlerTracker extends MediaEntryIngestRequestHandler {

        private final Logger logger = LoggerFactory.getLogger(MediaEntryIngestRequestHandlerTracker.class);

        Map<FileType, Long> fileTypeCount = new HashMap<>();

        private Map<IngestRequestMessage, IngestMessagePayload> results = new HashMap<>();

        @Override
        public synchronized List<Pair<IngestTaskParameters, IngestMessagePayload>> handleIngestionRequest(
                IngestRequestMessage ingestRequestMessage) throws IOException {

            List<Pair<IngestTaskParameters, IngestMessagePayload>> ingestParamsAndResults =
                    super.handleIngestionRequest(ingestRequestMessage);

            for (Pair<IngestTaskParameters, IngestMessagePayload> ingestParamsAndResult : ingestParamsAndResults) {
                IngestMessagePayload ingestMessagePayload = ingestParamsAndResult.getValue();
                results.put(ingestRequestMessage, ingestMessagePayload);
                FileType fileType = ingestMessagePayload.getClaFileProperties().getType();
                Long count = fileTypeCount.get(fileType);
                if (count == null){
                    fileTypeCount.put(fileType,1L);
                }else{
                    fileTypeCount.put(fileType,count + 1);
                }
                logger.info("---------------------------> File type count {}",fileTypeCount);
            }
            return ingestParamsAndResults;
        }

        public Map<IngestRequestMessage, IngestMessagePayload>
        getResults() {
            return results;
        }
    }

}
