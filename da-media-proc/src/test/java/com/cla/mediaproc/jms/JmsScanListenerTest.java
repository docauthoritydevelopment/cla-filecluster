//package com.cla.mediaproc.jms;
//
//import com.cla.connector.domain.dto.file.FileType;
//import com.cla.common.domain.dto.filetree.MediaType;
//import com.cla.connector.domain.dto.media.MediaConnectionDetailsDto;
//import com.cla.common.domain.dto.messages.ScanRequestMessage;
//import com.cla.connector.domain.dto.messages.ScanTaskParameters;
//import com.cla.mediaproc.base.BaseTestService;
//import com.cla.mediaproc.base.BaseTests;
//import com.cla.mediaproc.service.managers.MediaProcessorStateService;
//import com.cla.mediaproc.util.DaMessageConversionUtils;
//import com.google.common.collect.Lists;
//import org.junit.Test;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.jms.core.JmsTemplate;
//
//import javax.jms.Message;
//import javax.jms.TextMessage;
//import java.util.List;
//
///**
// * Created by uri on 22-Jun-17.
// */
//public class JmsScanListenerTest extends BaseTests {
//
//    private static final Logger logger = LoggerFactory.getLogger(JmsScanListenerTest.class);
//
//    @Autowired
//    JmsTemplate jmsTemplate;
//
//    @Autowired
//    private BaseTestService baseTestService;
//
//    @Autowired
//    MediaProcessorStateService mediaProcessorStateService;
//
//    @Test(timeout = 30000)
//    public void testSendScanTestRequest() {
//        logger.info("testSendScanRequest");
//        jmsTemplate.convertAndSend("scan.requests", "test");
//        logger.info("Message sent");
//
//    }
//
//    @Test(timeout = 30000)
//    public void testScanFromRequest() throws Exception {
//        logger.info("testScanFromRequest");
////        final BlockingQueue<ConsumerRecord<Integer, String>> records = new LinkedBlockingQueue<>();
////        KafkaMessageListenerContainer<Integer, String> kafkaMessageListenerContainer
////                = baseTestService.createKafkaListenerContainer(records, SCAN_RESULT_TOPIC);
////        baseTestService.waitForContainerToBeReady(kafkaMessageListenerContainer);
//
//        logger.debug("Send message in JMS)");
//        int scanTaskRequestsBefore = mediaProcessorStateService.getScanTaskRequests();
//        int scanTaskRequestsFinishedBefore = mediaProcessorStateService.getScanTaskRequestsFinished();
//
//        sendScanRequest();
//        logger.debug("Wait for scan requests to arrive");
//        waitForScanRequestToArrive(scanTaskRequestsBefore);
//        waitForScanToFinish(scanTaskRequestsFinishedBefore);
//
//        Message receive = jmsTemplate.receive(SCAN_RESULT_TOPIC);
//        logger.debug("Got scan result message {}", receive);
//        String text = ((TextMessage) receive).getText();
//        logger.debug("Got text result: {}", text);
////        verifyScanResultArrived(receive);
//    }
//
////    private void verifyScanResultArrived(BlockingQueue<ConsumerRecord<Integer, String>> records) throws InterruptedException {
////        logger.info("Verify scan result messages arrived");
////        ConsumerRecord<Integer, String> received = records.poll(5, TimeUnit.SECONDS);
////        logger.debug("Got record: {} {}", received.key(), received.value());
////    }
//
//    private void waitForScanToFinish(int scanTaskRequestsFinished) throws InterruptedException {
//        logger.info("Wait for scan task to finish");
//        while (mediaProcessorStateService.getScanTaskRequestsFinished() == scanTaskRequestsFinished) {
//            Thread.sleep(20);
//        }
//        logger.info("Scan finished");
//    }
//
//    private void waitForScanRequestToArrive(int scanTaskRequests) throws InterruptedException {
//        long start = System.currentTimeMillis();
//        while (mediaProcessorStateService.getScanTaskRequests() == scanTaskRequests) {
//            Thread.sleep(20);
//        }
//        long duration = System.currentTimeMillis() - start;
//        logger.info("It took {}ms to get the scan request ({})", duration, mediaProcessorStateService.getScanTaskRequests());
//    }
//
//    private void sendScanRequest() {
//        ScanRequestMessage scanRequestMessage = createScanRequestMessage();
//        String message = DaMessageConversionUtils.Scan.serializeRequestMessage(scanRequestMessage);
//        jmsTemplate.convertAndSend(SCAN_REQUEST_TOPIC, message);
//    }
//
//    private ScanRequestMessage createScanRequestMessage() {
//        ScanTaskParameters scanTaskParameters = ScanTaskParameters.create().setPath(baseTestService.getDefaultPath());
//        List<FileType> typesToScan = Lists.newArrayList(FileType.values());
//        scanTaskParameters.setFirstScan(true).setRunId(1L).setPackageEntryTypesToScan(typesToScan);
//        scanTaskParameters.setRootFolderId(1L);
//        MediaConnectionDetailsDto mediaConnectionDetailsDto = new MediaConnectionDetailsDto();
//        mediaConnectionDetailsDto.setMediaType(MediaType.FILE_SHARE);
//        scanTaskParameters.setConnectionDetails(mediaConnectionDetailsDto);
//        return new ScanRequestMessage(scanTaskParameters);
//    }
//
//}
