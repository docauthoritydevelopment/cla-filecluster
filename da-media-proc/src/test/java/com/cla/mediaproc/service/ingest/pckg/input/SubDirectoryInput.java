package com.cla.mediaproc.service.ingest.pckg.input;

public class SubDirectoryInput extends AbstractFilesContainerInput{

    private final boolean packaged;

    public SubDirectoryInput(boolean packaged) {
        this.packaged = packaged;
    }

    public boolean isPackaged() {
        return packaged;
    }
}
