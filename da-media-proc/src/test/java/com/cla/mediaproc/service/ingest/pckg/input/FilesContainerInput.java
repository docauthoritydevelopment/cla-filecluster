package com.cla.mediaproc.service.ingest.pckg.input;

public interface FilesContainerInput {

    int getExcelFileCount();

    int getPdfFileCount();

    public int getWordFileCount();

    String getName();

}
