package com.cla.mediaproc.service.ingest.pckg.input;

public class SubDirectoryInputBuilder extends AbstractFilesContainerTestInputBuilder<SubDirectoryInput> {


    SubDirectoryInputBuilder(SubDirectoryInput subDirectoryInput) {
        super(subDirectoryInput);
    }


    public static SubDirectoryInputBuilder newPackagedBuilder() {
        return new SubDirectoryInputBuilder(new SubDirectoryInput(true));
    }

    public static SubDirectoryInputBuilder newBuilder() {
        return new SubDirectoryInputBuilder(new SubDirectoryInput(false));
    }

    @Override
    public SubDirectoryInput build() {
        return filesContainerInput;
    }
}
