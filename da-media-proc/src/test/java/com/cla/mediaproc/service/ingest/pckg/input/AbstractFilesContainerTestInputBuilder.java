package com.cla.mediaproc.service.ingest.pckg.input;

public abstract class AbstractFilesContainerTestInputBuilder<T extends AbstractFilesContainerInput> {

    T filesContainerInput;

    AbstractFilesContainerTestInputBuilder(T filesContainerInput) {
        this.filesContainerInput = filesContainerInput;
    }

    public AbstractFilesContainerTestInputBuilder<T> withWordFileCount(int wordFileCount) {
        filesContainerInput.wordFileCount = wordFileCount;
        return this;
    }

    public AbstractFilesContainerTestInputBuilder<T> withExcelFileCount(int excelFileCount) {
        filesContainerInput.excelFileCount = excelFileCount;
        return this;
    }

    public AbstractFilesContainerTestInputBuilder<T> withPdfFileCount(int pdfFileCount) {
        filesContainerInput.pdfFileCount = pdfFileCount;
        return this;
    }

    public AbstractFilesContainerTestInputBuilder<T> withSubDirectory(SubDirectoryInput subDirectoryInput) {
        filesContainerInput.subDirectories.add(subDirectoryInput);
        return this;
    }

    public abstract T build();

}


