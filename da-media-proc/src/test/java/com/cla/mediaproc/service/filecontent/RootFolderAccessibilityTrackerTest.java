package com.cla.mediaproc.service.filecontent;

import com.cla.common.domain.dto.file.DirectoryExistStatus;
import com.cla.connector.domain.dto.media.MediaConnectionDetailsDto;
import com.cla.connector.domain.dto.media.MediaType;
import com.cla.connector.mediaconnector.AbstractMediaConnector;
import com.cla.filecluster.service.media.MediaConnectorFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import reactor.test.StepVerifier;

import java.util.function.Function;

public class RootFolderAccessibilityTrackerTest {

    private MediaConnectorFactory mediaConnectorFactory;

    @Before
    public void testSetup() {
        mediaConnectorFactory = Mockito.mock(MediaConnectorFactory.class);
    }

    @Test
    public void testRootFolderAccessibility() {
        // Prepare mock media connector
        MediaConnectionDetailsDto connectionDetails = mediaConnectionDetails(MediaType.FILE_SHARE, 1L);
        configureMediaConnectionFactory(connectionDetails,path -> path.equals("a") ? true : false);

        // Add root folders to tracker
        RootFolderAccessibilityTracker accessibilityTracker = new RootFolderAccessibilityTracker(
                mediaConnectorFactory,
                1000,
                1000,
                5);
        accessibilityTracker.track(1, "a", false, connectionDetails);
        accessibilityTracker.track(2, "b", false, connectionDetails);

        // verify correct status returns from accessibility stream
        StepVerifier.create(accessibilityTracker.getRootFolderAccessibilityStream())
                .assertNext(rootFolderAccessCheckDto ->
                        Assert.assertEquals(rootFolderAccessCheckDto.getStatus(), DirectoryExistStatus.YES))
                .assertNext(rootFolderAccessCheckDto ->
                        Assert.assertEquals(rootFolderAccessCheckDto.getStatus(), DirectoryExistStatus.NO))
                .thenCancel()
                .verify();
    }


    @Test
    public void testRootFolderAccessibilityTimeout() {
        long timeoutInMillisToTest = 1000;
        // Prepare media connectors which simulate waiting longer than the defined timeoutInMillisToTest
        MediaConnectionDetailsDto connectionDetails = mediaConnectionDetails(MediaType.FILE_SHARE, 1L);
        configureMediaConnectionFactory(
                connectionDetails,
                path -> {
                    if (path.equals("a")) {
                        try {
                            Thread.sleep(timeoutInMillisToTest + 1000);
                        } catch (InterruptedException e) {
                            throw new RuntimeException(e);
                        }
                        return true;
                    } else {
                        return true;
                    }
                });

        // Add root folders to tracker
        RootFolderAccessibilityTracker accessibilityTracker = new RootFolderAccessibilityTracker(
                mediaConnectorFactory,
                timeoutInMillisToTest,
                1000,
                5);
        accessibilityTracker.track(1, "a", false, connectionDetails);
        accessibilityTracker.track(2, "b", false, connectionDetails);

        // verify correct status returns from accessibility stream
        StepVerifier.create(accessibilityTracker.getRootFolderAccessibilityStream())
                .assertNext(rootFolderAccessCheckDto ->
                        // This is the fallback from timeout!!
                        Assert.assertEquals(DirectoryExistStatus.NO, rootFolderAccessCheckDto.getStatus()))
                .assertNext(rootFolderAccessCheckDto ->
                        Assert.assertEquals(DirectoryExistStatus.YES, rootFolderAccessCheckDto.getStatus()))
                .thenCancel()
                .verify();
    }

    @Test
    public void testRootFolderAccessibilityError() {
        // Prepare media connectors which simulate waiting longer than the defined timeoutInMillisToTest
        MediaConnectionDetailsDto connectionDetails = mediaConnectionDetails(MediaType.FILE_SHARE, 1L);
        configureMediaConnectionFactory(
                connectionDetails,
                path -> {
                    if (path.equals("a")) {
                        throw new IllegalStateException("Invalid path");
                    } else {
                        return false;
                    }
                });

        // Add root folders to tracker
        RootFolderAccessibilityTracker accessibilityTracker = new RootFolderAccessibilityTracker(
                mediaConnectorFactory,
                1000,
                1000,
                5);
        accessibilityTracker.track(1, "a", false, connectionDetails);
        accessibilityTracker.track(2, "b", false, connectionDetails);

        // verify correct status returns from accessibility stream
        StepVerifier.create(accessibilityTracker.getRootFolderAccessibilityStream())
                .assertNext(rootFolderAccessCheckDto ->
                        // This is the fallback from error!!
                        Assert.assertEquals(DirectoryExistStatus.UNKNOWN, rootFolderAccessCheckDto.getStatus()))
                .assertNext(rootFolderAccessCheckDto ->
                        Assert.assertEquals(DirectoryExistStatus.NO, rootFolderAccessCheckDto.getStatus()))
                .thenCancel()
                .verify();
    }

    @Test
    public void testRootFolderAccessibilityMultiple() {
        // Prepare mocked media connectors that returns true/false/true/false...
        MediaConnectionDetailsDto fileShareConnectionDetails1 = mediaConnectionDetails(MediaType.FILE_SHARE, 1L);
        MediaConnectionDetailsDto fileShareConnectionDetails2 = mediaConnectionDetails(MediaType.FILE_SHARE, 2L);
        MediaConnectionDetailsDto sharePointConnectionDetails1 = mediaConnectionDetails(MediaType.SHARE_POINT, 3L);
        MediaConnectionDetailsDto sharePointConnectionDetails2 = mediaConnectionDetails(MediaType.SHARE_POINT, 4L);
        MediaConnectionDetailsDto boxConnectionDetails1 = mediaConnectionDetails(MediaType.BOX, 5L);
        MediaConnectionDetailsDto boxConnectionDetails2 = mediaConnectionDetails(MediaType.BOX, 6L);

        configureMediaConnectionFactory(fileShareConnectionDetails1, path -> true);
        configureMediaConnectionFactory(fileShareConnectionDetails2, path -> false);
        configureMediaConnectionFactory(sharePointConnectionDetails1, path -> true);
        configureMediaConnectionFactory(sharePointConnectionDetails2, path -> false);
        configureMediaConnectionFactory(boxConnectionDetails1, path -> true);
        configureMediaConnectionFactory(boxConnectionDetails2, path -> false);

        // // Add root folders to tracker per media connection
        RootFolderAccessibilityTracker accessibilityTracker = new RootFolderAccessibilityTracker(
                mediaConnectorFactory,
                1000,
                1000,
                5);
        accessibilityTracker.track(1, "a", false, fileShareConnectionDetails1);
        accessibilityTracker.track(2, "b", false, fileShareConnectionDetails2);
        accessibilityTracker.track(3, "c", false, sharePointConnectionDetails1);
        accessibilityTracker.track(4, "d", false, sharePointConnectionDetails2);
        accessibilityTracker.track(5, "e", false, boxConnectionDetails1);
        accessibilityTracker.track(6, "f", false, boxConnectionDetails2);

        // verify multiple media connection results
        StepVerifier.create(accessibilityTracker.getRootFolderAccessibilityStream())
                .assertNext(rootFolderAccessCheckDto ->
                        Assert.assertEquals(DirectoryExistStatus.YES, rootFolderAccessCheckDto.getStatus()))
                .assertNext(rootFolderAccessCheckDto ->
                        Assert.assertEquals(DirectoryExistStatus.NO, rootFolderAccessCheckDto.getStatus()))
                .assertNext(rootFolderAccessCheckDto ->
                        Assert.assertEquals(DirectoryExistStatus.YES, rootFolderAccessCheckDto.getStatus()))
                .assertNext(rootFolderAccessCheckDto ->
                        Assert.assertEquals(DirectoryExistStatus.NO, rootFolderAccessCheckDto.getStatus()))
                .assertNext(rootFolderAccessCheckDto ->
                        Assert.assertEquals(DirectoryExistStatus.YES, rootFolderAccessCheckDto.getStatus()))
                .assertNext(rootFolderAccessCheckDto ->
                        Assert.assertEquals(DirectoryExistStatus.NO, rootFolderAccessCheckDto.getStatus()))
                .thenCancel()
                .verify();
    }

    private void configureMediaConnectionFactory(MediaConnectionDetailsDto connectionDetails,
                                                 Function<String, Boolean> existsFunction) {
        AbstractMediaConnector mockConnector = Mockito.mock(AbstractMediaConnector.class);
        Mockito
                .when(mediaConnectorFactory.getMediaConnector(connectionDetails))
                .thenAnswer(invocation -> mockConnector);
        Mockito
                .when(mockConnector.isDirectoryExists(Mockito.anyString(), Mockito.anyBoolean()))
                .thenAnswer(existsInvocation -> existsFunction.apply(existsInvocation.getArgument(0)));
    }

    private MediaConnectionDetailsDto mediaConnectionDetails(MediaType mediaType, long id) {
        MediaConnectionDetailsDto mediaConnectionDetailsDto = new MediaConnectionDetailsDto();
        mediaConnectionDetailsDto.setMediaType(mediaType);
        mediaConnectionDetailsDto.setId(id);
        return mediaConnectionDetailsDto;
    }
}
