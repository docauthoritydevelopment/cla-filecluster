package com.cla.mediaproc.service.ingest.pckg.input;

import java.util.LinkedList;
import java.util.List;

public class AbstractFilesContainerInput implements FilesContainerInput {

    String name;

    int excelFileCount;
    int wordFileCount;
    int pdfFileCount;

    List<SubDirectoryInput> subDirectories = new LinkedList<>();

    public AbstractFilesContainerInput() {
    }

    @Override
    public int getExcelFileCount() {
        return excelFileCount;
    }

    @Override
    public int getPdfFileCount() {
        return pdfFileCount;
    }

    @Override
    public int getWordFileCount() {
        return wordFileCount;
    }

    public List<SubDirectoryInput> getSubDirectories() {
        return subDirectories;
    }

    @Override
    public String getName() {
        return name;
    }
}
