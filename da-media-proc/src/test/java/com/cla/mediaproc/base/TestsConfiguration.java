package com.cla.mediaproc.base;

import com.cla.mediaproc.MediaProcessor;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * Created by uri on 07-Mar-17.
 */
@Configuration
@Import(MediaProcessor.class)
public class TestsConfiguration {
}
