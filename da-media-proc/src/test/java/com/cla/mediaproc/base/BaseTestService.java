package com.cla.mediaproc.base;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.nio.file.Paths;

/**
 *
 * Created by uri on 08-Mar-17.
 */
@Service
public class BaseTestService {

    private static final Logger logger = LoggerFactory.getLogger(BaseTestService.class);

    private ObjectMapper mapper;

    private static final String SRC_TEST_RESOURCES_FILES_ANALYZE_TEST = "./src/test/resources/files/analyzeTest";

    @PostConstruct
    public void init() {
        mapper = new ObjectMapper();
        mapper.configure(JsonParser.Feature.ALLOW_SINGLE_QUOTES,true);
    }

//    public<T> KafkaTemplate<Integer, T> createKafkaProducerTemplate(String topic) {
//        logger.debug("Create Kafka template (test)");
//        KafkaEmbedded embeddedKafka = BaseTests.embeddedKafka;
//        Map<String, Object> senderProps = KafkaTestUtils.senderProps(embeddedKafka.getBrokersAsString());
//        ProducerFactory<Integer, T> pf =
//                new DefaultKafkaProducerFactory<Integer, T>(senderProps);
//        KafkaTemplate<Integer, T> template = new KafkaTemplate<Integer,T>(pf);
//        template.setDefaultTopic(topic);
//        return template;
//    }

//    public KafkaMessageListenerContainer<Integer, String> createKafkaMessageListenerContainer(String topic) {
//        KafkaEmbedded embeddedKafka = BaseTests.embeddedKafka;
//        Map<String, Object> consumerProps = KafkaTestUtils.consumerProps("testT", "false", embeddedKafka);
//        DefaultKafkaConsumerFactory<Integer, String> cf =
//                new DefaultKafkaConsumerFactory<Integer, String>(consumerProps);
//        ContainerProperties containerProperties = new ContainerProperties(topic);
//        return new KafkaMessageListenerContainer<>(cf, containerProperties);
//    }

    public ObjectMapper getMapper() {
        return mapper;
    }

    public String getDefaultPath() {
        return Paths.get(SRC_TEST_RESOURCES_FILES_ANALYZE_TEST).toAbsolutePath().normalize().toString();
    }

//    public KafkaMessageListenerContainer<Integer, String> createKafkaListenerContainer(BlockingQueue<ConsumerRecord<Integer, String>> records, String topic) {
//        logger.debug("Create Kafka Listener container");
//        KafkaMessageListenerContainer<Integer, String> kafkaMessageListenerContainer = createKafkaMessageListenerContainer(topic);
//
//        logger.debug("Create message listener handler for the container");
//        kafkaMessageListenerContainer.setupMessageListener((MessageListener<Integer, String>) record -> {
//            logger.debug("OnMessage:" + record);
//            records.add(record);
//        });
//        kafkaMessageListenerContainer.setBeanName("templateTests");
//        kafkaMessageListenerContainer.start();
//        return kafkaMessageListenerContainer;
//    }

//    public void waitForContainerToBeReady(KafkaMessageListenerContainer<Integer, String> kafkaMessageListenerContainer) throws Exception {
//        logger.debug("Wait until Container is ready");
//        long l = System.currentTimeMillis();
//        ContainerTestUtils.waitForAssignment(kafkaMessageListenerContainer, BaseTests.embeddedKafka.getPartitionsPerTopic());
//        logger.debug("Container is ready after {}ms",System.currentTimeMillis() - l);
//    }
}
