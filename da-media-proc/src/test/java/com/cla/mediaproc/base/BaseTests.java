package com.cla.mediaproc.base;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Created by uri on 07-Mar-17.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestsConfiguration.class)
@ActiveProfiles("test")
public abstract class BaseTests {

    public static final String SCAN_REQUEST_TOPIC = "testmp.scan.requests";
    public static final String SCAN_RESULT_TOPIC = "testmp.scan.results";

//    @ClassRule
//    public static KafkaEmbedded embeddedKafka = new KafkaEmbedded(1,true,TEST_TOPIC,
//            SCAN_REQUEST_TOPIC,
//            SCAN_RESULT_TOPIC,
//            MEDIA_PROC_NOTIFICATIONS_TOPIC);

//    @BeforeClass
//    public static void setupSpec() {
//        System.setProperty("kafka.server-address",  embeddedKafka.getBrokersAsString());
//    }


}
