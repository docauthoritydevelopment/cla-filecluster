package com.cla.mediaproc;

import com.google.common.collect.Lists;
import org.mockito.ArgumentMatcher;
import org.mockito.Mockito;

import java.util.Objects;

public class MockitoEnhancements {


    public static <T> T argThat(T value) {
        return Mockito.argThat(new ArgumentMatcher<T>() {
            @Override
            public boolean matches(Object argument) {
                T t;
                try {
                    t = (T) argument;
                } catch (ClassCastException cce) {
                    return false;
                }
                return Objects.equals(t, argument);
            }
        });
    }
}
