﻿param (
    [string]$AutomaticDeploymentEnv =  "C:\jenkins-ws\da-remote-install\automatedDeployment\AD_perBuild\distributedLab_$buildNumber",
    [string]$destination = $AutomaticDeployment+'\DAInstallation',
    [string]$pushToS3 = $AutomaticDeploymentEnv + '\pushToS3.bat',
    [string]$deployRevision = $AutomaticDeploymentEnv + '\deployRevision.bat',
    
    [string]$revisionFileDestination = $AutomaticDeploymentEnv + '\deployRevision.bat',
    [string]$createApplication = $AutomaticDeploymentEnv + '\CreateApplication.bat',
    [string]$createGroupDeployment = $AutomaticDeploymentEnv + '\CreateGroupDeploy.bat',
    [string]$deleteGroupDeployment = $AutomaticDeploymentEnv + '\DeleteDeploymentGroup.bat',
    [string]$deleteApplication = $AutomaticDeploymentEnv + '\DeleteApplication.bat',

    [string]$launch_MySQL_Instance = $AutomaticDeploymentEnv + '\launch_MySQL_Instance_Testing.bat',

    
    [string]$checkInstanceStatus = $AutomaticDeploymentEnv + '\verifyInstanceStatus.bat',

    [string]$applyInstanceNameTo_MySQL = $AutomaticDeploymentEnv + '\applyInstanceNameTo_MySQL.bat',
    [string]$applyInstanceNameTo_MP = $AutomaticDeploymentEnv + '\applyInstanceNameTo_MP.bat',
    [string]$applyInstanceNameTo_FileCluster = $AutomaticDeploymentEnv + '\applyInstanceNameTo_FileCluster.bat',

    
    [string]$deploymentStateFile = $AutomaticDeploymentEnv + '\deploymentState.bat',
    #[string]$exportToJSON = '--output json >C:\jenkins-ws\da-remote-install\ADOutputFiles\deploymentState.json',
    #[string]$exportDeployIdToText = '--output=text >C:\jenkins-ws\da-remote-install\ADOutputFiles\deployId.txt',
    #[string]$hostName = $outputFilesPath + '\HostName.txt',
    [string]$runAutomationSuit = $AutomaticDeploymentEnv + '\runAutomationSuit.bat',
    [string]$terminateInstance = $AutomaticDeploymentEnv + 'terminateInstanceByID.bat',
    [string]$Setup_Name,
    [string]$NumberOfInstances,
    # AIO, DoubleInstances, TripleInstances
    [string]$End_Of_Run_Mode,
    [string]$Branch_As_Source,
    [string]$NumberOfMPInstances,
    [string]$daEnronValidate,
    [string]$AWS_Account,
    [string]$buildNumber,
    [string]$job_name,
    [string]$baseDA_Release,
    [string]$Branch_To_Upgrade 
    
)
 
    
    aws configure set AWS_ACCESS_KEY_ID AKIAJES7XW26B4A4CD3A
    aws configure set AWS_SECRET_ACCESS_KEY 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
    aws configure set default.region us-west-2



# Copy automatedDeployment Lab per build

Write-Host " ================ Create a new AutomaticDeployment Lab per $buildNumber number ============================"
$pathToautomatedDeploymentHome = 'C:\jenkins-ws\da-remote-install\automatedDeployment\upgradePackage'
$newAutomatedDeplymentPerRun = "C:\jenkins-ws\da-remote-install\automatedDeployment\AD_perBuild\distributedLab_$buildNumber"

Copy-Item -Path $pathToautomatedDeploymentHome -Destination $newAutomatedDeplymentPerRun -recurse -Force 



# Create a folder to put all the running stuff

[string]$outputFilesPath = 'C:\jenkins-ws\da-remote-install\ADOutputFiles\output_'+$buildNumber
[string]$exportToJSON = "--output json >C:\jenkins-ws\da-remote-install\ADOutputFiles\output_$buildNumber\deploymentState.json"
[string]$exportDeployIdToText = "--output=text >C:\jenkins-ws\da-remote-install\ADOutputFiles\output_$buildNumber\deployId.txt"
[string]$hostName = $outputFilesPath + '\HostName.txt'
[string]$path2deploymentId = $outputFilesPath + '\deployId.txt'
[string]$path2eTag = $outputFilesPath + '\output.txt'

$AutomaticDeploymentEnv =  $newAutomatedDeplymentPerRun
$runningFolderName = $buildNumber
$runningFolder = New-Item -ItemType directory -Path $outputFilesPath\RunFolder_$job_name$runningFolderName
$automationFolder = New-Item -ItemType directory -Path $runningFolder\Automation_$buildNumber

$date = Get-Date -format M-d
$ErrorActionPreference="SilentlyContinue"
Stop-Transcript | out-null
$ErrorActionPreference = "Continue"
Start-Transcript -path $runningFolder\ADRunLog_$buildNumber  -append




if ($baseDA_Release -eq "Release16_P3")
{

#Start FileCluster instance

$path_to_launch_FC = "C:\jenkins-ws\da-remote-install\automatedDeployment\AD_perBuild\distributedLab_$buildNumber\launch_FC_Instance.ps1"
[string]$iam_Instance = ' --iam-instance-profile Name="EC2_CodeDeploy"'
[string]$last_part_script= ' --instance-type m4.xlarge --key-name CodeDeployKey --security-groups DefaultGS --output json'
[string]$output=' > C:\jenkins-ws\da-remote-install\ADOutputFiles\output_'+$buildNumber+'\startedFC_InstanceId.json'
[string]$firstPart= 'aws ec2 run-instances --image-id   ami-faff6382 --count 1'
$run_FC_Instnace=   $firstPart +$Number_of_FC_Servers+  $iam_Instance  + $last_part_script +  $output | Out-File $path_to_launch_FC
#Write-Host "$run_FC_Instnace"
write-host "======== Start $Number_of_FC_Servers of FileCluster Instance for deployment and Testing  ========"
       &C:\jenkins-ws\da-remote-install\automatedDeployment\AD_perBuild\distributedLab_$buildNumber\launch_FC_Instance.ps1

}

if ($baseDA_Release -eq "Andromeda")
{

#Start FileCluster instance

$path_to_launch_FC = "C:\jenkins-ws\da-remote-install\automatedDeployment\AD_perBuild\distributedLab_$buildNumber\launch_FC_Instance.ps1"
[string]$iam_Instance = ' --iam-instance-profile Name="EC2_CodeDeploy"'
[string]$last_part_script= ' --instance-type m4.xlarge --key-name CodeDeployKey --security-groups DefaultGS --output json'
[string]$output=' > C:\jenkins-ws\da-remote-install\ADOutputFiles\output_'+$buildNumber+'\startedFC_InstanceId.json'
[string]$firstPart= 'aws ec2 run-instances --image-id   ami-b2f26eca --count 1'
$run_FC_Instnace=   $firstPart +$Number_of_FC_Servers+  $iam_Instance  + $last_part_script +  $output | Out-File $path_to_launch_FC
#Write-Host "$run_FC_Instnace"
write-host "======== Start $Number_of_FC_Servers of FileCluster Instance for deployment and Testing  ========"
       &C:\jenkins-ws\da-remote-install\automatedDeployment\AD_perBuild\distributedLab_$buildNumber\launch_FC_Instance.ps1

}

if ($baseDA_Release -eq "Dev_Andro_patch_01")
{

#Start FileCluster instance

$path_to_launch_FC = "C:\jenkins-ws\da-remote-install\automatedDeployment\AD_perBuild\distributedLab_$buildNumber\launch_FC_Instance.ps1"
[string]$iam_Instance = ' --iam-instance-profile Name="EC2_CodeDeploy"'
[string]$last_part_script= ' --instance-type m4.xlarge --key-name CodeDeployKey --security-groups DefaultGS --output json'
[string]$output=' > C:\jenkins-ws\da-remote-install\ADOutputFiles\output_'+$buildNumber+'\startedFC_InstanceId.json'
[string]$firstPart= 'aws ec2 run-instances --image-id   ami-5afc6522 --count 1'
$run_FC_Instnace=   $firstPart +$Number_of_FC_Servers+  $iam_Instance  + $last_part_script +  $output | Out-File $path_to_launch_FC
#Write-Host "$run_FC_Instnace"
write-host "======== Start $Number_of_FC_Servers of FileCluster Instance for deployment and Testing  ========"
       &C:\jenkins-ws\da-remote-install\automatedDeployment\AD_perBuild\distributedLab_$buildNumber\launch_FC_Instance.ps1

}

$json_FC = (Get-Content $outputFilesPath\startedFC_InstanceId.json -Raw) | ConvertFrom-Json
$instanceId_FC= $json_FC.Instances.InstanceId
$InstanceName_FC= $json_FC.Instances.PrivateIpAddress | Out-File $outputFilesPath'\HostName_FC.txt'
$instancePrivateIP_FC= $json_FC.Instances.PrivateIpAddress
$privateDnsName = $json_FC.Instances.PrivateDnsName
$privateIP_FC = Get-Content $outputFilesPath'\HostName_FC.txt'


$responseFile_AIO=$AutomaticDeploymentEnv+'\responseFiles\response_AIO.varfile'



  $relevantResponse = $responseFile_AIO





#Get the public IP of the FC Instance
$publicInstanceIP_script_FC = aws ec2 describe-instances --instance-ids $instanceId_FC --output json > $outputFilesPath\publicInstanceIP_FC.json
$json_FC = (Get-Content $outputFilesPath\publicInstanceIP_FC.json -Raw) | ConvertFrom-Json
$publicInstanceIP_FC = $json_FC.Reservations.Instances.PublicIpAddress
$clientPort = ':9000'

write-host "======== Running Instance of FC   =================

                                 InstanceID: $instanceId_FC
                                 InstanceIP: $publicInstanceIP_FC
                                 InstancePrivateIP: $instancePrivateIP_FC
                                 Address For Browsing: https://$publicInstanceIP_FC$clientPort

                                  "



#Apply name to FileCluster instance
Write-Host "SetInstanceName --------------------------------------"
    [string]$ValueFC='Value='+ $Setup_Name +'_codeDeploy_Automation'
    [string]$InstanceKeyName =' --tags Key=Name,'+ $ValueFC
    $contentForApplyName= "aws ec2 create-tags --resources $instanceId_FC $InstanceKeyName"
    Set-Content -Value $contentForApplyName -Path $applyInstanceNameTo_FileCluster
    #write-host "======= Apply a name to the new created $instanceId_FC ========="
    &$applyInstanceNameTo_FileCluster
    
$path_To_Source = 'C:\Program Files (x86)\Jenkins\userContent\'+$Branch_To_Upgrade
$files = Get-ChildItem -Path $path_To_Source -Filter *.exe | sort LastWriteTime -Descending #Date Modified

$counter = 0
$daFiles = @()

write-host "======== Searching $path_To_Source for files... ========"

foreach($file in $files)
{
    $fileName = $file.Name

    if($fileName.StartsWith('DocAuthority_') -And -not $fileName.StartsWith('patch'))
    {
        write-host $file.Name " " $file.LastWriteTime
        $counter++
        $daFiles+=$file
    }

}

$newDAFile = $daFiles[0]

if($counter -gt 0)
{


	write-host "======================"
    write-host "Found $counter file matching the criteria."

    write-host "======================  Copy $path_To_Source\$newDAFile TO $newAutomatedDeplymentPerRun ========================"
    
    Copy-Item  $path_To_Source\$newDAFile -Destination $newAutomatedDeplymentPerRun  -Force

    write-host "Done Copying!" -BackgroundColor Green
    Rename-Item -NewName DocAuthority_windows.exe -Path $newAutomatedDeplymentPerRun\$newDAFile -Force

    # Create Group in CodeDeploy
    

    $pathToCreateGroup=$AutomaticDeploymentEnv+'\CreateGroupDeploy.bat'
    $valueToScript = '_codeDeploy*'
    $createGroupDeployment = "aws deploy create-deployment-group --application-name DAInstallation --deployment-config-name CodeDeployDefault.OneAtATime --deployment-group-name DA_Installation_$buildNumber --ec2-tag-filters Key=Name,Value=$Setup_Name$valueToScript,Type=KEY_AND_VALUE --service-role-arn arn:aws:iam::968670743267:role/EC2_CodeDeploy"
    
    Set-Content -Value $createGroupDeployment -Path $pathToCreateGroup
    Write-Host "========Create Code Deploy GroupDeployment" -BackgroundColor Blue
    &$pathToCreateGroup

      
    write-host "========Found Installation file is going to push to S3"  -BackgroundColor Blue
    write-host "========Running: $pushToS3 ========"
    $pushToS3 = "aws deploy push --application-name DAInstallation  --ignore-hidden-files --s3-location s3://testingcodedeploybucket/TestInstallation.zip --source C:\jenkins-ws\da-remote-install\automatedDeployment\AD_perBuild\distributedLab_$buildNumber > C:\jenkins-ws\da-remote-install\ADOutputFiles\output_$buildNumber\output.txt"
    $pathToS3file = $AutomaticDeploymentEnv+ '\pushToS3.bat'
    Set-Content -Value $pushToS3 -Path $pathToS3file 
    &$pathToS3file
        write-host "======== DA Installation is going to be deployed in the CodeDeploy machine"
    
    
    


}
else
{
    write-host "No file matching the creteria were found." -BackgroundColor Red
}

write-host
write-host " =========== Creating Revision File: ============"


$input = Get-Content -Path C:\jenkins-ws\da-remote-install\ADOutputFiles\output_$buildNumber\output.txt

foreach($line in $input)
{
    $line = $line.Split(",").Split(" ")
    foreach($splt in $line)
    {
        if($splt.Contains("eTag"))
        {
            $eTag = $splt.Remove(0,5)
            Write-Host $etag
        }
    }

}

$deployRevisionScript= "aws deploy create-deployment --application-name DAInstallation --s3-location bucket=testingcodedeploybucket,key=TestInstallation.zip,bundleType=zip,eTag=$eTag --deployment-group-name DA_Installation_$buildNumber --deployment-config-name CodeDeployDefault.AllAtOnce --description DAInstallation_Deploy --ignore-application-stop-failures $exportDeployIdToText"


#Run revision deployment
Set-Content -Value $deployRevisionScript -Path $revisionFileDestination
write-host "========Running: $revisionFileDestination ========"
    &$revisionFileDestination


#Get Deployment Status
$deploymentId = Get-Content -Path C:\jenkins-ws\da-remote-install\ADOutputFiles\output_$buildNumber\deployId.txt
$deploymentState= "aws deploy get-deployment --deployment-id $deploymentId $exportToJSON"
Set-Content -Value $deploymentState -Path $deploymentStateFile
Write-Host "========== Get Deployment Status ======"
&$deploymentStateFile

while($generalState -eq "InProgress" -or $genetalState -eq "Pending" -or "Created")
{
   $time = Get-Date -format u
   Write-Host "The deployment status is InProgress or Pending $time"
   $convertToJSON= aws deploy get-deployment --deployment-id $deploymentId --output json >C:\jenkins-ws\da-remote-install\ADOutputFiles\output_$buildNumber\deploymentState.json
   $json = (Get-Content C:\jenkins-ws\da-remote-install\ADOutputFiles\output_$buildNumber\deploymentState.json -Raw) | ConvertFrom-Json
   $generalState = $json.deploymentInfo.status
   write-host $generalState
   sleep -Seconds 30

   if ($generalState -eq "Succeeded")
   { write-host "===================================== Deployment is Succeeded =============================" -BackgroundColor Green
   break}

   if ($generalState -eq "Failed")
   {write-host "======================================= Deployment is Failed ================================" -BackgroundColor Red

    write-host "Stopping Instance and Automation won't run" -BackgroundColor Gray
     $stopInstanceId = "aws ec2 terminate-instances --instance-ids $instanceId"
     Set-Content -Value $stopInstanceId -Path $terminateInstance
     &$terminateInstance
    #sleep -Seconds 45
    exit}
        }
        
#Delete DeploymentGroup

sleep -Seconds 30
aws deploy delete-deployment-group --application-name DAInstallation --deployment-group-name DA_Installation_$buildNumber
Write-Host " ============ Delete deplyment group: DA_Installation_$buildNumber ========================="




#Run the End Of Run mode - Stop / Terminate / KeepOnRunning
if ($End_Of_Run_Mode -eq "KeepOnRunning")
{
Write-Host "=========================  The Setup will keep on running and any termination activity should be taken in the future  =================================="
exit
}

if ($End_Of_Run_Mode -eq "stop" -or "terminate")
{

aws ec2 describe-instances --query 'Reservations[].Instances[].Tags[?Key==`Name`].Value[]'--output json > 'C:\jenkins-ws\da-remote-install\ADOutputFiles\output_'$buildNumber\Start_Stop_Terminate_Logs\activity.json

$input = Get-Content -Path 'C:\jenkins-ws\da-remote-install\ADOutputFiles\output_'$buildNumber\Start_Stop_Terminate_Logs\activity.json
foreach($line in $input)
{
    $line = $line.Split().Split(" ")
    foreach($splt in $line)
    {
        if($splt.Contains($Setup_Name))
        {
            $splt = $splt.Replace('"','').Replace(",","")
            $splt | Out-File "c:\$splt.txt" -Append
            Write-Host $splt
            $instanceID= aws ec2 describe-tags --filters "Name=value,Values=$splt" --output json >'C:\jenkins-ws\da-remote-install\ADOutputFiles\output_'$buildNumber\Start_Stop_Terminate_Logs\activity.json
            $json_instanceID = (Get-Content 'C:\jenkins-ws\da-remote-install\ADOutputFiles\output_'$buildNumber\Start_Stop_Terminate_Logs\activity.json -Raw) | ConvertFrom-Json
            $instanceID= $json_instanceID.Tags.ResourceId

            aws ec2 $End_Of_Run_Mode-instances --instance-ids $instanceID
        }

    }
}
Write-Host "=========================  The Setup will be in $End_Of_Run_Mode mode!!  In case it's in STOPPED mode, there is an option to RUN it from 'Start_Stop_Terminate AWS instance' - Jenkins Job  =================================="

}

# Remove AutoDeployment Package

Write-Host " ============================== Remove Deployment Package ======================"
 Remove-Item -path $AutomaticDeploymentEnv  -Recurse -Force

Write-Host " ============================== Copy Logs ========================================================="
# ZIP the folder with the output logs
$source = $runningFolder+'\ADRunLog_'+$buildNumber
$destination = "C:\Program Files (x86)\Jenkins\userContent\AutomationResults\DeploymentLogs\Logs_$buildNumber"


Add-Type -assembly "system.io.compression.filesystem"
ZIP Test Result Package
[io.compression.zipfile]::CreateFromDirectory($Source, $destination)

Stop-Transcript
