#.\manageActionSchedule.ps1 -req create -name f44rttss -filter savedFilterName -property TAG -value fdgdg -cron "0 0 0 * * */1" -path "D:\temp" -tempFolder "D:\temp" -username aa
#.\manageActionSchedule.ps1 -req upd-active -name f44rttss -active $False -username aa
#.\manageActionSchedule.ps1 -req upd-cron -name f44rttss -username aa -cron "0 2 0 * * */1"
#.\manageActionSchedule.ps1 -req upd-path -name f44rttss -username aa -path "D:\temp2"
#.\manageActionSchedule.ps1 -req upd-filter -name f44rttss -username aa -filter savedFilterName

param (
[string]$req = 'list',
[string]$test,
[string]$name = '',
[bool]$active = $True,
[string]$filter = '',
[string]$property = '',
[string]$value = '',
[string]$cron = '',
[string]$path = '',
[string]$tempFolder = '',
[string]$action = 'SET_PROPERTY_VALUE',
[Parameter(Mandatory=$true)][string]$username,
[string]$password = $( Read-Host "Input password, please" )
)

if (($req -Match 'create') -or $req.StartsWith('upd-')) {
 if (!$name) {
  Write-Host "schedule name is mandatory for this req"
  exit
 }
 if (($req -Match 'upd-filter' -or $req -Match 'create') -and !$filter) {
  Write-Host "filter is mandatory for this req"
  exit
 }
 if (($req -Match 'upd-cron' -or $req -Match 'create') -and !$cron) {
  Write-Host "cron is mandatory for this req"
  exit
 }
}

if ($req -Match 'create') {
 if (!$action) {
  Write-Host "action is mandatory for this req"
  exit
 }
 if ('SET_PROPERTY_VALUE' -ne $action) {
  Write-Host "the only action currently supported is SET_PROPERTY_VALUE"
  exit
 }
 if (!$property) {
  Write-Host "property is mandatory for this req"
  exit
 }
 if (!$value) {
  Write-Host "value is mandatory for this req"
  exit
 }
 if (!$tempFolder) {
  Write-Host "tempFolder is mandatory for this req"
  exit
 }
}

$headers2 = @{
    "Content-Type" = "application/x-www-form-urlencoded"
}

$mainPage = Invoke-WebRequest -Uri "http://localhost:8080/login" -Method Post -SessionVariable 'Session' -Headers $headers2 -Body "username=$username&password=$password"
$cookie = $mainPage.Headers["Set-Cookie"]
$cookie = ($cookie.Split(",") -match "^\S+=\S+;" -replace ";.*") -join '; '
Write-Host $cookie
$Session

$headers = @{
 "Content-Type" = "application/json;charset=UTF-8"
 Accept = "application/json, text/plain, */*"
"Cookie" = $cookie
}

$url = ""
$body = ""

if ($req -Match 'create') {
 $body = @{
  "property" = $property
  "value" = $value
  "tempFolder" = $tempFolder
   "filter" = $filter
   "cron" = $cron
   "path" = $path
 } | ConvertTo-Json
 $url = "http://localhost:8080/api/shell/actionschedule/schedule?name=$name&action=$action"
} elseif ($req -Match 'upd-active') {
 $url = "http://localhost:8080/api/shell/actionschedule/active?name=$name&state=$active"
} elseif ($req -Match 'upd-filter') {
 $body = $filter
 $url = "http://localhost:8080/api/shell/actionschedule/filter?name=$name"
} elseif ($req -Match 'upd-cron') {
 $url = "http://localhost:8080/api/shell/actionschedule/cron?name=$name"
 $body = $cron
} elseif ($req -Match 'upd-path') {
 $url = "http://localhost:8080/api/shell/actionschedule/path?name=$name"
 $path = $path.replace('\','\\')
 $body = $path
} elseif ($req -Match 'delete') {
 $url = "http://localhost:8080/api/shell/actionschedule/delete?name=$name"
} elseif ($req -Match 'list') {
 $req = 'list'
 $url = "http://localhost:8080/api/action/schedule/list"
} else {
 Write-Host "req options: list, create, upd-active, upd-filter, upd-cron, upd-path, delete"
 exit
}

$res = ""
try {
 if (!$body) {
  Write-Host "get " $url " with headers " (ConvertTo-Json($headers))
  $res = Invoke-WebRequest -Uri $url -Method GET -Headers $headers  -WebSession $Session
 } else {
  Write-Host "post " $url " with body " $body " with headers " (ConvertTo-Json($headers))
  $res = Invoke-WebRequest -Uri $url -Method POST -Headers $headers -Body $body  -WebSession $Session
} } catch {
 $result = $_.Exception.Response.GetResponseStream()
 $reader = New-Object System.IO.StreamReader($result)
 $reader.BaseStream.Position = 0
 $reader.DiscardBufferedData()
 $responseBody = $reader.ReadToEnd();
 Write-Host $result $responseBody
}

Write-Host "result:"

if ($res.statuscode -eq 200) {
 $show = ""
 if ($req -Match 'list') {
  $list = ConvertFrom-Json $res.content
  $show = $list.content
 } else {
  $show = $res.content
 }
 Write-Host $show
} else {
 Write-Host $res
}



