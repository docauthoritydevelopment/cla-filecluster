param (
    [string]$AutomaticDeploymentEnv =  "C:\jenkins-ws\da-remote-install\automatedDeployment\AD_perBuild\distributedLab_$buildNumber",
    [string]$destination = $AutomaticDeployment+'\DAInstallation',
    [string]$pushToS3 = $AutomaticDeploymentEnv + '\pushToS3.bat',
    [string]$deployRevision = $AutomaticDeploymentEnv + '\deployRevision.bat',
    
    [string]$revisionFileDestination = $AutomaticDeploymentEnv + '\deployRevision.bat',
    [string]$createApplication = $AutomaticDeploymentEnv + '\CreateApplication.bat',
    [string]$createGroupDeployment = $AutomaticDeploymentEnv + '\CreateGroupDeploy.bat',
    [string]$deleteGroupDeployment = $AutomaticDeploymentEnv + '\DeleteDeploymentGroup.bat',
    [string]$deleteApplication = $AutomaticDeploymentEnv + '\DeleteApplication.bat',     
    [string]$checkInstanceStatus = $AutomaticDeploymentEnv + '\verifyInstanceStatus.bat',
    [string]$applyInstanceNameTo_FileCluster = $AutomaticDeploymentEnv + '\applyInstanceNameTo_FileCluster.ps1',
    [string]$deploymentStateFile = $AutomaticDeploymentEnv + '\deploymentState.bat',
    [string]$exportToJSON = '--output json >C:\jenkins-ws\da-remote-install\ADOutputFiles\deploymentState.json',
    [string]$exportDeployIdToText = '--output=text >C:\jenkins-ws\da-remote-install\ADOutputFiles\deployId.txt',
    [string]$hostName = $outputFilesPath + '\HostName.txt',
    [string]$runAutomationSuit = $AutomaticDeploymentEnv + '\runAutomationSuit.bat',
    [string]$terminateInstance = $AutomaticDeploymentEnv + 'terminateInstanceByID.bat',
    [string]$Setup_Name,
    
    # AIO, DoubleInstances, TripleInstances
    [string]$End_Of_Run_Mode,
    [string]$Branch_As_Source,
    [string]$daEnronValidate,
    [string]$AWS_Account,
    [string]$buildNumber,
    [string]$job_name,
    [String]$automationType,
    [String]$setupName,
    [String]$LogLevel 
    
)
 
    
    aws configure set AWS_ACCESS_KEY_ID AKIAJES7XW26B4A4CD3A
    aws configure set AWS_SECRET_ACCESS_KEY 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
    aws configure set default.region us-west-2



# Copy automatedDeployment Lab per build

Write-Host " ================ Create a new AutomaticDeployment Lab per $buildNumber number ============================"
$pathToautomatedDeploymentHome = 'C:\jenkins-ws\workspace\Automation_BigData_Run\automatedDeployment\bigDataRun'
$newAutomatedDeplymentPerRun = "C:\jenkins-ws\da-remote-install\automatedDeployment\AD_perBuild\distributedLab_$buildNumber"

Copy-Item -Path $pathToautomatedDeploymentHome -Destination $newAutomatedDeplymentPerRun -recurse -Force 



# Create a folder to put all the running stuff

[string]$outputFilesPath = 'C:\jenkins-ws\da-remote-install\ADOutputFiles\output_'+$buildNumber
[string]$exportToJSON = "--output json >C:\jenkins-ws\da-remote-install\ADOutputFiles\output_$buildNumber\deploymentState.json"
[string]$exportDeployIdToText = "--output=text >C:\jenkins-ws\da-remote-install\ADOutputFiles\output_$buildNumber\deployId.txt"
[string]$hostName = $outputFilesPath + '\HostName.txt'
[string]$path2deploymentId = $outputFilesPath + '\deployId.txt'
[string]$path2eTag = $outputFilesPath + '\output.txt'

$AutomaticDeploymentEnv =  $newAutomatedDeplymentPerRun
$runningFolderName = $buildNumber
$runningFolder = New-Item -ItemType directory -Path $outputFilesPath\RunFolder_$job_name$runningFolderName
$automationFolder = New-Item -ItemType directory -Path $runningFolder\Automation_$buildNumber

$date = Get-Date -format M-d
$ErrorActionPreference="SilentlyContinue"
Stop-Transcript | out-null
$ErrorActionPreference = "Continue"
Start-Transcript -path $runningFolder\ADRunLog_$buildNumber  -append

${USER_DATA} = '<powershell>

$password = ""doc4docsAD"" | ConvertTo-SecureString -asPlainText -Force
$username = ""testing.da.com\administrator""
$credential = New-Object System.Management.Automation.PSCredential($username,$password)
$domainname = ""testing.da.com""

$instanceID = invoke-restmethod -uri http://169.254.169.254/latest/meta-data/instance-id

Rename-Computer -NewName $instanceID -DomainCredential $credential -Verbose -Force
sleep 5
add-computer -DomainName $domainname -Credential $credential -Options JoinWithNewName,AccountCreate -Passthru -Verbose -Force -Restart

</powershell>'

#Start BigData instance

$path_to_launch_FC = "C:\jenkins-ws\da-remote-install\automatedDeployment\AD_perBuild\distributedLab_$buildNumber\launch_FC_Instance.ps1"
[string]$iam_Instance = ' --iam-instance-profile Name="EC2_CodeDeploy"'
[string]$last_part_script= ' --instance-type m4.xlarge --key-name CodeDeployKey --security-groups DefaultGS --output json'
[string]$output=' > C:\jenkins-ws\da-remote-install\ADOutputFiles\output_'+$buildNumber+'\startedFC_InstanceId.json'


if ($Branch_As_Source -eq "Bellatrix")
{
[string]$firstPart= "aws ec2 run-instances --image-id  ami-02cdf56c7b3a48507 --count 1 --user-data '$USER_DATA'"
}

if ($Branch_As_Source -eq "Master" -or $Branch_As_Source -eq "Castor")
{
[string]$firstPart= "aws ec2 run-instances --image-id  ami-0b013d55d56ded193 --count 1 --user-data '$USER_DATA'"

}

if ($Branch_As_Source -eq "Master" -or $Branch_As_Source -eq "Castor_clean")
{
[string]$firstPart= "aws ec2 run-instances --image-id  ami-036dbf79dc0e0c0fd --count 1 --user-data '$USER_DATA'"

}


$run_FC_Instnace=   $firstPart +  $iam_Instance  + $last_part_script +  $output | Out-File $path_to_launch_FC
#Write-Host "$run_FC_Instnace"
write-host "======== Start BigData Machine  ========"
       &C:\jenkins-ws\da-remote-install\automatedDeployment\AD_perBuild\distributedLab_$buildNumber\launch_FC_Instance.ps1

$json_FC = (Get-Content $outputFilesPath\startedFC_InstanceId.json -Raw) | ConvertFrom-Json
$instanceId_FC= $json_FC.Instances.InstanceId
$InstanceName_FC= $json_FC.Instances.PrivateIpAddress | Out-File $outputFilesPath'\HostName_FC.txt'
$instancePrivateIP_FC= $json_FC.Instances.PrivateIpAddress
$privateDnsName = $json_FC.Instances.PrivateDnsName
$privateIP_FC = Get-Content $outputFilesPath'\HostName_FC.txt'

#Get the public IP of the FC Instance
$publicInstanceIP_script_FC = aws ec2 describe-instances --instance-ids $instanceId_FC --output json > $outputFilesPath\publicInstanceIP_FC.json
$json_FC = (Get-Content $outputFilesPath\publicInstanceIP_FC.json -Raw) | ConvertFrom-Json
$publicInstanceIP_FC = $json_FC.Reservations.Instances.PublicIpAddress
$clientPort = ':9000'

write-host "======== Running Instance of FC   =================
                                 InstanceID: $instanceId_FC
                                 InstanceIP: $publicInstanceIP_FC
                                 InstancePrivateIP: $instancePrivateIP_FC
                                 Address For Browsing: https://$publicInstanceIP_FC$clientPort
                                  "


#Save Instance ID for next build termination:
Write-Host "Saving Instance ID in WS for next build termination"
$instanceId_FC | Out-File -filepath C:\jenkins-ws\workspace\Automation_BigData_Run\InstaceID.txt


#Apply name to FileCluster instance
Write-Host "============= $setupName ========================="
Write-Host "SetInstanceName --------------------------------------"
    [string]$ValueFC="Value= $setupName_$buildNumber"+"_codeDeploy_AutomationBigData"
    [string]$InstanceKeyName =" --tags 'Key=Name, $ValueFC'"
    $contentForApplyName= "aws ec2 create-tags --resources $instanceId_FC $InstanceKeyName"
    Set-Content -Value $contentForApplyName -Path $applyInstanceNameTo_FileCluster
    write-host "======= Apply a name to the new created $instanceId_FC ========="
    &C:\jenkins-ws\da-remote-install\automatedDeployment\AD_perBuild\distributedLab_$buildNumber\applyInstanceNameTo_FileCluster.ps1
    
$path_To_Source = 'C:\Program Files (x86)\Jenkins\userContent\'+$Branch_As_Source
$files = Get-ChildItem -Path $path_To_Source -Filter *.exe | sort LastWriteTime -Descending #Date Modified

$counter = 0
$daFiles = @()

write-host "======== Searching $path_To_Source for files... ========"

foreach($file in $files)
{
    $fileName = $file.Name

    if($fileName.StartsWith('DocAuthority_') -And -not $fileName.StartsWith('patch'))
    {
        write-host $file.Name " " $file.LastWriteTime
        $counter++
        $daFiles+=$file
    }

}

$newDAFile = $daFiles[0]

if($counter -gt 0)
{


	write-host "======================"
    write-host "Found $counter file matching the criteria."

    write-host "======================  Copy $path_To_Source\$newDAFile TO $newAutomatedDeplymentPerRun ========================"
    
    Copy-Item  $path_To_Source\$newDAFile -Destination $newAutomatedDeplymentPerRun  -Force

    write-host "Done Copying!" -BackgroundColor Green
    Rename-Item -NewName DocAuthority_windows.exe -Path $newAutomatedDeplymentPerRun\$newDAFile -Force

    # Create Group in CodeDeploy
    

    $pathToCreateGroup=$AutomaticDeploymentEnv+'\CreateGroupDeploy.bat'
    $valueToScript = 'AIO_codeDeploy_AutomationBigData'
    #$createGroupDeployment = "aws deploy create-deployment-group --application-name DAInstallation --deployment-config-name CodeDeployDefault.OneAtATime --deployment-group-name DA_Installation_$buildNumber --ec2-tag-filters Key=Name,Value=$Setup_Name$valueToScript,Type=KEY_AND_VALUE --service-role-arn arn:aws:iam::968670743267:role/EC2_CodeDeploy"
    $createGroupDeployment = "aws deploy create-deployment-group --application-name DAInstallation --deployment-config-name CodeDeployDefault.OneAtATime --deployment-group-name DA_Installation_$buildNumber --ec2-tag-filters Key=Name,Value=$valueToScript,Type=KEY_AND_VALUE --service-role-arn arn:aws:iam::968670743267:role/EC2_CodeDeploy"
    Set-Content -Value $createGroupDeployment -Path $pathToCreateGroup
    Write-Host "========Create Code Deploy GroupDeployment" -BackgroundColor Blue
    &$pathToCreateGroup

      
    write-host "========Found Installation file is going to push to S3"  -BackgroundColor Blue
    write-host "========Running: $pushToS3 ========"
    $pushToS3 = "aws deploy push --application-name DAInstallation  --ignore-hidden-files --s3-location s3://testingcodedeploybucket/CI.zip --source C:\jenkins-ws\da-remote-install\automatedDeployment\AD_perBuild\distributedLab_$buildNumber > C:\jenkins-ws\da-remote-install\ADOutputFiles\output_$buildNumber\output.txt"
    $pathToS3file = $AutomaticDeploymentEnv+ '\pushToS3.bat'
    Set-Content -Value $pushToS3 -Path $pathToS3file 
    &$pathToS3file
        write-host "======== DA Installation is going to be deployed in the CodeDeploy machine"
   }
else
{
    write-host "No file matching the creteria were found." -BackgroundColor Red
}

write-host
write-host " =========== Creating Revision File: ============"


$input = Get-Content -Path C:\jenkins-ws\da-remote-install\ADOutputFiles\output_$buildNumber\output.txt

foreach($line in $input)
{
    $line = $line.Split(",").Split(" ")
    foreach($splt in $line)
    {
        if($splt.Contains("eTag"))
        {
            $eTag = $splt.Remove(0,5)
            Write-Host $etag
        }
    }

}

$deployRevisionScript= "aws deploy create-deployment --application-name DAInstallation --s3-location bucket=testingcodedeploybucket,key=TestInstallation.zip,bundleType=zip,eTag=$eTag --deployment-group-name DA_Installation_$buildNumber --deployment-config-name CodeDeployDefault.AllAtOnce --description DAInstallation_Deploy --ignore-application-stop-failures $exportDeployIdToText"


#Run revision deployment
Set-Content -Value $deployRevisionScript -Path $revisionFileDestination
write-host "========Running: $revisionFileDestination ========"
    &$revisionFileDestination

    
sleep -Seconds 480
aws deploy delete-deployment-group --application-name DAInstallation --deployment-group-name DA_Installation_$buildNumber
Write-Host " ============ Delete deplyment group: DA_Installation_$buildNumber ========================="

#Set-ExecutionPolicy RemoteSigned
Add-Type -Path 'C:\Program Files (x86)\MySQL\MySQL Connector Net 6.9.12\Assemblies\v4.5\MySql.Data.dll'
$Connection = [MySql.Data.MySqlClient.MySqlConnection]@{ConnectionString="server=$instancePrivateIP_FC;uid=root;pwd=root;database=docauthority"}
$Connection.Open()
$MYSQLCommand = New-Object MySql.Data.MySqlClient.MySqlCommand
$MYSQLDataAdapter = New-Object MySql.Data.MySqlClient.MySqlDataAdapter
$MYSQLDataSet = New-Object System.Data.DataSet
$MYSQLCommand.Connection=$Connection
$MYSQLCommand.CommandText="delete from docauthority.mon_components where component_type =  'SOLR_NODE'"
$MYSQLDataAdapter.SelectCommand=$MYSQLCommand
$NumberOfDataSets=$MYSQLDataAdapter.Fill($MYSQLDataSet, "data")
foreach($DataSet in $MYSQLDataSet.tables[0])
{
write-host "The sql removed the line"
}
$Connection.Close()

$password = "doc4docsAD" | ConvertTo-SecureString -asPlainText -Force
$username = "testing.da.com\administrator"
$credential = New-Object System.Management.Automation.PSCredential($username,$password)
$domainname = "testing.da.com"

Invoke-Command -ComputerName $instancePrivateIP_FC -Credential $credential -ScriptBlock {Set-Service -Name "DocAuthority-filecluster" -Status Running -PassThru}


#Start the Filecluster service
Start-Service -InputObject $(Get-Service -Computer $instancePrivateIP_FC -Name DocAuthority-filecluster)

#RunSetupAutomation
# Download Automation WS from S3 
aws configure set AWS_ACCESS_KEY_ID AKIAIOWORECYZA5IC75Q 
    aws configure set AWS_SECRET_ACCESS_KEY 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
    aws configure set default.region us-west-2
if ($Branch_As_Source -eq "Master")
{
Write-Host "==== Download Automation WS from S3 for Master====="
aws s3 cp s3://da-builds/DA-Automation/DA_Automation_Master/DA-Automation_Master.zip C:\jenkins-ws\DA-Automation_$buildNumber.zip
#Unzip the downloaded package
Write-Host " ==================== Unzip Automation WS package ========================="
$zipfile= "C:\jenkins-ws\DA-Automation_$buildNumber.zip"
$outpath= "C:\jenkins-ws\DA-Automation_$buildNumber" 
Add-Type -AssemblyName System.IO.Compression.FileSystem
function Unzip
{
    param([string]$zipfile, [string]$outpath)
    [System.IO.Compression.ZipFile]::ExtractToDirectory($zipfile, $outpath)
}
Unzip "$zipfile" "$outpath"
}
if ($Branch_As_Source -eq "Castor" -or $Branch_As_Source -eq "Castor_clean")
{
Write-Host "==== Download Automation WS from S3 for Castor====="
aws s3 cp s3://da-builds/DA-Automation/DA_Automation_Castor/DA-Automation_Castor.zip C:\jenkins-ws\DA-Automation_$buildNumber.zip
#Unzip the downloaded package
Write-Host " ==================== Unzip Automation WS package ========================="
$zipfile= "C:\jenkins-ws\DA-Automation_$buildNumber.zip"
$outpath= "C:\jenkins-ws\DA-Automation_$buildNumber" 
Add-Type -AssemblyName System.IO.Compression.FileSystem
function Unzip
{
    param([string]$zipfile, [string]$outpath)
    [System.IO.Compression.ZipFile]::ExtractToDirectory($zipfile, $outpath)
}
Unzip "$zipfile" "$outpath"
}
if ($Branch_As_Source -eq "Bellatrix")
{
Write-Host "==== Download Automation WS from S3 for Master or Bellatrix ====="
aws s3 cp s3://da-builds/DA-Automation/DA_Automation_Bellatrix_bg/DA-Automation_bellatrix_bg.zip C:\jenkins-ws\DA-Automation_$buildNumber.zip
#Unzip the downloaded package
Write-Host " ==================== Unzip Automation WS package ========================="
$zipfile= "C:\jenkins-ws\DA-Automation_$buildNumber.zip"
$outpath= "C:\jenkins-ws\DA-Automation_$buildNumber" 
Add-Type -AssemblyName System.IO.Compression.FileSystem
function Unzip
{
    param([string]$zipfile, [string]$outpath)
    [System.IO.Compression.ZipFile]::ExtractToDirectory($zipfile, $outpath)
}
Unzip "$zipfile" "$outpath"
}
#Run Testing configuration for Remote file Serenity
[string]$outputFilesPath = "$runningFolder\ADRunLog_$buildNumber\Automation_$buildNumber"
[string]$port="9000"
[string]$pathToHost = "C:\jenkins-ws\da-remote-install\ADOutputFiles\output_$buildNumber"
$firstRow = $instancePrivateIP_FC
write-host "====== prepare the testing remote config file ======"
Write-host "======SUT.Properties file =========================="
$path_SUT_File = "C:\jenkins-ws\DA-Automation_$buildNumber\sut_orig.properties"
$word40 = "da.url="
$replacement40 = "da.url=https://$instancePrivateIP_FC$port"
$text40 = get-content $path_SUT_File
$newText40 = $text40 -replace $word40,$replacement40
$newText40 > $path_SUT_File
Write-host "$replacement40"
$word1 = "da.version="
$replacement1 = "da.version=2.0"
$text1 = get-content $path_SUT_File
$newText1 = $text1 -replace $word1,$replacement1
$newText1 > $path_SUT_File
Write-host "$replacement1"
$word31 = "da.enron.isValidate=" 
$replacement31 = "da.enron.isValidate=false"
$text31 = get-content $path_SUT_File 
$newText31 = $text31 -replace $word31,$replacement31
$newText31 > $path_SUT_File
$word41 = "da.log.level="
$replacement41 = "da.log.level=$LogLevel"
$text41 = get-content $path_SUT_File 
$newText41 = $text41 -replace $word41,$replacement41
$newText41 > $path_SUT_File
$word45 = "da.web.protocol="
$replacement45 = "da.web.protocol=https"
$text45 = get-content $path_SUT_File 
$newText45 = $text45 -replace $word45,$replacement45
$newText45 > $path_SUT_File
$word40 = "da.web.host="
$replacement40 = "da.web.host=$firstRow"
$text40 = get-content $path_SUT_File 
$newText40 = $text40 -replace $word40,$replacement40
$newText40 > $path_SUT_File
$word46 = "da.web.port="
$replacement46 = "da.web.port=$port"
$text46 = get-content $path_SUT_File 
$newText46 = $text46 -replace $word46,$replacement46
$newText46 > $path_SUT_File
$UserName = "aa"
$word20 = "da.web.username="
$replacement20 = "da.web.username=$UserName"
$text20 = get-content $path_SUT_File 
$newText20 = $text20 -replace $word20,$replacement20
$newText20 > $path_SUT_File
$Password = "123"
$word30 = "da.web.password="
$replacement30 = "da.web.password=$Password"
$text30 = get-content $path_SUT_File 
$newText30 = $text30 -replace $word30,$replacement30
$newText30 > $path_SUT_File
$portSQL = "3306"
$word203 = "da.mysql.host="
$replacement203 = "da.mysql.host=$firstRow"
$text203 = get-content $path_SUT_File
$newText203 = $text203 -replace $word203,$replacement203
$newText203 > $path_SUT_File
Write-Host "$replacement203"
$word201 = "da.mysql.port="
$replacement201 = "da.mysql.port=$portSQL"
$text201 = get-content $path_SUT_File
$newText201 = $text201 -replace $word201,$replacement201
$newText201 > $path_SUT_File
Write-Host "$replacement201"
$word202 = "da.mysql.sid="
$replacement202 = "da.mysql.sid=docauthority"
$text202 = get-content $path_SUT_File
$newText202 = $text202 -replace $word202,$replacement202
$newText202 > $path_SUT_File
Write-Host "$replacement202"
$word203 = "da.mysql.username="
$replacement203 = "da.mysql.username=root"
$text203 = get-content $path_SUT_File
$newText203 = $text203 -replace $word203,$replacement203
$newText203 > $path_SUT_File
Write-Host "$replacement203"
$word204= "da.mysql.password="
$replacement204 = "da.mysql.password=root"
$text204 = get-content $path_SUT_File
$newText204 = $text204 -replace $word204,$replacement204
$newText204 > $path_SUT_File
Write-Host "$replacement203"
$word41 = "da.filecluster.log.path="
$replacement41 = "da.filecluster.log.path=\\\\$firstRow\\c$\\Installation\\DocAuthority\\filecluster\\logs"
$text41 = get-content $path_SUT_File 
$newText41 = $text41 -replace $word41,$replacement41
$newText41 > $path_SUT_File
$word42 = "test.browser.type="
$replacement42 = "test.browser.type=CHROME"
$text42 = get-content $path_SUT_File 
$newText42 = $text42 -replace $word42,$replacement42
$newText42 > $path_SUT_File
$word50 = "test.log.path="
$replacement50 = "test.log.path=C:\\jenkins-ws\\AutomationLogs"
$text50 = get-content $path_SUT_File 
$newText50 = $text50 -replace $word50,$replacement50
$newText50 > $path_SUT_File
$word60 = "test.data.path="
$replacement60 = "test.data.path=\\\\172.31.41.248\\AutomationData"
$text60 = get-content $path_SUT_File 
$newText60 = $text60 -replace $word60,$replacement60
$newText60 > $path_SUT_File
$word61 = "test.log.level="
$replacement61 = "test.log.level=INFO"
$text61 = get-content $path_SUT_File 
$newText61 = $text61 -replace $word61,$replacement61
$newText61 > $path_SUT_File

if ($automationType -eq "da-test-bigdata-ui" -or $automationType -eq "da-test-scan-ui")
{
Get-Content C:\jenkins-ws\DA-Automation_$buildNumber\sut_orig.properties | Set-Content -Encoding utf8 C:\jenkins-ws\DA-Automation_$buildNumber\sut.properties
$automationPropertiesFileSanity = "C:\jenkins-ws\da-remote-install\automatedDeployment\AD_perBuild\distributedLab_$buildNumber\runAutomationSuit_regression_da-auto-setup.bat"
$updateGradleProperties = "pushd C:\jenkins-ws\DA-Automation_$buildNumber
                           gradlew test aggregate -i -p C:\jenkins-ws\DA-Automation_$buildNumber\$automationType
                           popd"
Set-Content -Value $updateGradleProperties -Path $automationPropertiesFileSanity
$runtAutomation= "C:\jenkins-ws\da-remote-install\automatedDeployment\AD_perBuild\distributedLab_$buildNumber\runAutomationSuit_regression_da-auto-setup.bat"
&$runtAutomation
}


if ($automationType -eq "setup" -or $automationType -eq "sanity")
{
Get-Content C:\jenkins-ws\DA-Automation_$buildNumber\sut_orig.properties | Set-Content -Encoding utf8 C:\jenkins-ws\DA-Automation_$buildNumber\sut.properties
$automationPropertiesFileSanity = "C:\jenkins-ws\da-remote-install\automatedDeployment\AD_perBuild\distributedLab_$buildNumber\runAutomationSuit_regression_da-auto-setup.bat"
$updateGradleProperties = "pushd C:\jenkins-ws\DA-Automation_$buildNumber
                           gradlew test aggregate -i -p C:\jenkins-ws\DA-Automation_$buildNumber\da-auto-$automationType
                           popd"
Set-Content -Value $updateGradleProperties -Path $automationPropertiesFileSanity
$runtAutomation= "C:\jenkins-ws\da-remote-install\automatedDeployment\AD_perBuild\distributedLab_$buildNumber\runAutomationSuit_regression_da-auto-setup.bat"
&$runtAutomation
}

Write-Host "============= ZIP Test Result Package ==============="
$number = Get-Random -Minimum 0 -Maximum 10
$date = get-date -f yyyy-MM-dd
$source = "C:\jenkins-ws\DA-Automation_$buildNumber\da-auto-setup\target\site\serenity"
$destination = "C:\Program Files (x86)\Jenkins\userContent\AutomationResults\Sanity\$buildNumber-da-auto-setup-AutomationReport_$date.zip"
Add-Type -assembly "system.io.compression.filesystem"
[io.compression.zipfile]::CreateFromDirectory($Source, $destination) 
#aws s3 cp $destination s3://da-code-deploy-bucket/SanityAutomationResults/
Write-Host "                                                                                                                          "
Write-Host "                                                                                                                          "
Write-Host "===================== Regression Automation Report name is: $buildNumber-da-auto-setup-SanityAutomationReport_$date.zip ========================="
Write-Host "                                                                                                                          "
Write-Host "                                                                                                                          "
Write-Host "===================== Please download it from  http://52.10.244.29:18080/userContent/AutomationResults/Sanity/$buildNumber-$i-SanityAutomationReport_$date.zip ============================="
