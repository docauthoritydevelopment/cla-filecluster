﻿$arrToRunAutomation = $automationType -split ','
$arrToRunAutomation # Print output

foreach ($i in $arrToRunAutomation)
{

$automationPropertiesFileSanity = "C:\jenkins-ws\da-remote-install\automatedDeployment\AD_perBuild\distributedLab_$buildNumber\runAutomationSuit.bat"
$updateGradleProperties = "pushd C:\jenkins-ws\da-remote-install\
                           gradlew test aggregate -i -p C:\jenkins-ws\DA-Automation_$buildNumber\$arrToRunAutomation
                           popd"
Set-Content -Value $updateGradleProperties -Path $automationPropertiesFileSanity
#Run Automation

Write-Host "========== Run Automation Suite ============="
Write-Host "=================== $arrToRunAutomation Automation is running================="
$runtAutomation= "C:\jenkins-ws\da-remote-install\automatedDeployment\AD_perBuild\distributedLab_$buildNumber\runAutomationSuit.bat"
&$runtAutomation

}