#!/bin/sh

# Launch new instance from a custom Windows AMI and runs a Powershell to Rename the instance and add to domain.
# Instance naming convention : [VERSION]_[USER]
# Instance taging : Creator , purpose , Schedual(optional)
# Instance schedual tasks : self terminate , self stop 

# Get from user in Jenkins
INSTANCE_TYPE=$1
INSTANCE_USER=$2
INSTANCE_PURPOSE=$3
Terminate_In_Days=$4
Stop_In_Hours=$5
INSTANCE_CREATOR="$6"
INSTANCE_SCHEDULE=$7

INSTANCE_NAME=$INSTANCE_TYPE"_"$INSTANCE_USER

# Defualt vars
INSTANCE_NUM="1"
INSTANCE_SIZE="m4.xlarge"
INSTANCE_SECURITY_GROUP="DefaultGS"
INSTANCE_KEY="Updated_Testing_Key"

USER_DATA=file:///var/lib/jenkins/workspace/Launch_Instance/EC2_scripts/user_data_scripts/set_name_add_domain.txt

# Instance Type
# Clean
if [ "$INSTANCE_TYPE" = "Clean" ]
then
    AMI_ID="ami-04b5222bb806777d0"
    echo Launching a clean instance...
elif [ "$INSTANCE_TYPE" = "Big_Data_BT" ]
then
    AMI_ID="ami-020548663961b6974"
    echo Launching a Big_Data_BT instance...
else
    echo Not A valid option
    exit 1
fi


# Run Instance

INSTANCE_ID=$(aws ec2 run-instances --image-id $AMI_ID \
                                    --count $INSTANCE_NUM \
                                    --instance-type $INSTANCE_SIZE \
                                    --key-name $INSTANCE_KEY \
                                    --security-groups $INSTANCE_SECURITY_GROUP \
                                    --user-data $USER_DATA \
                                    --tag-specifications ResourceType=instance,Tags=[{Key=Name,Value=$INSTANCE_NAME}] --output text | awk '/INSTANCE/{print $7}')

# Wait for the instace to be ready
aws ec2 wait instance-status-ok --instance-ids $INSTANCE_ID

# Get Publice IP
INSTANCE_PUBLIC_IP=$(aws ec2 describe-instances --instance-ids $INSTANCE_ID --query "Reservations[*].Instances[*].PublicIpAddress" --output text)
# Get Private IP
INSTANCE_PRIVATE_IP=$(aws ec2 describe-instances --instance-ids $INSTANCE_ID --query "Reservations[*].Instances[*].PrivateIpAddress" --output text)


# Prints details
echo "==================================================================="
echo "=== Instane Name:\t\t$INSTANCE_NAME"
echo "=== Instane ID:\t\t\t$INSTANCE_ID"
echo "=== Instane Public IP:\t\t$INSTANCE_PUBLIC_IP"
echo "=== Instane Private IP:\t\t$INSTANCE_PRIVATE_IP"
echo "=== Instane Owner:\t\t$INSTANCE_USER"
echo "=== Instane Creator:\t\t$INSTANCE_CREATOR"

# Schedule Stop instance
if [ "$Stop_In_Hours" != "0" ]; then
    printf  "=== STOP at:\t\t\t"
    echo "aws ec2 stop-instances --instance-ids $INSTANCE_ID" | at now + $Stop_In_Hours hours 2>/dev/null; at -l | sort -n | tail -n1 | awk '{print $5" " $4" "$3" "$6" "$2}'
fi
# Schedule Terminate instance
if [ "$Terminate_In_Days" != "0" ]; then
    printf  "=== TERMINATE at:\t\t"
    echo "aws ec2 terminate-instances --instance-ids $INSTANCE_ID" | at now + $Terminate_In_Days days 2>/dev/null; at -l | sort -n |tail -n1 | awk '{print $5" " $4" "$3" "$6" "$2}'
fi
echo "==================================================================="

# Add Schedule?

# Create downloadable RDP file
echo "screen mode id:i:2
use multimon:i:0
desktopwidth:i:1920
desktopheight:i:1080
session bpp:i:32
winposstr:s:0,3,0,0,800,600
compression:i:1
keyboardhook:i:2
audiocapturemode:i:0
videoplaybackmode:i:1
connection type:i:7
networkautodetect:i:1
bandwidthautodetect:i:1
displayconnectionbar:i:1
username:s:administrator@testing.da.com
enableworkspacereconnect:i:0
disable wallpaper:i:0
allow font smoothing:i:0
allow desktop composition:i:0
disable full window drag:i:1
disable menu anims:i:1
disable themes:i:0
disable cursor setting:i:0
bitmapcachepersistenable:i:1
full address:s:$INSTANCE_PUBLIC_IP
audiomode:i:0
redirectprinters:i:1
redirectcomports:i:0
redirectsmartcards:i:1
redirectclipboard:i:1
redirectposdevices:i:0
autoreconnection enabled:i:1
authentication level:i:2
prompt for credentials:i:0
negotiate security layer:i:1
remoteapplicationmode:i:0
alternate shell:s:
shell working directory:s:
gatewayhostname:s:
gatewayusagemethod:i:4
gatewaycredentialssource:i:4
gatewayprofileusagemethod:i:0
promptcredentialonce:i:0
gatewaybrokeringtype:i:0
use redirection server name:i:0
rdgiskdcproxy:i:0
kdcproxyname:s:
" >> $INSTANCE_NAME.rdp
echo -e '\e]8;;http://52.43.114.204:8080/view/DevOps/job/Launch_Instance/ws/$INSTANCE_NAME.rdp\e\\Download RDP File\e]8;;\e\\'
echo "Download RDP file: http://52.43.114.204:8080/view/DevOps/job/Launch_Instance/ws/$INSTANCE_NAME.rdp"
