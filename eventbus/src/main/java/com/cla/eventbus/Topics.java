package com.cla.eventbus;

public final class Topics {

    public static final String PROCESSES_STATE_MACHINES = "processes.sm.topic";
    public static final String PROCESS_ERRORS = "processes.error";
    public static final String KVDB_ERRORS = "kvdb.errors";
    public static final String DIAGNOSTICS = "diagnostics.topic";
    public static final String MAP = "map.topic";
    public static final String INGEST = "ingest.topic";
    public static final String RUN = "run.topic";
    public static final String FILE_GROUP_ATTACH = "file-group.attach";
}
