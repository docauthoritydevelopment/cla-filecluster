package com.cla.eventbus.events.handler;

import com.cla.eventbus.events.Event;

public interface EventHandler {

    void handle(Event event);
}
