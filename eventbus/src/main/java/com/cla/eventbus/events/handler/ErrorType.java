package com.cla.eventbus.events.handler;

public enum ErrorType {
    NO_PROCESS_KEY("No process key"),
    NO_PROCESS("No process"),
    PROCESS_STATE_MACHINE_ALREADY_STARTED("Process already started"),
    NO_MATCHING_PROCESS_STATE_MACHINE("No process initiated"),
    START_PROCESS_ERROR("Start process failed"),
    NO_EXISTING_PROCESS("No existing process"),
    RESTORE_PROCESS_ERROR("Failed to restore process"),
    IO_FAILURE("Failed to execute an IO operation")
    ;

    private final String text;

    ErrorType(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }
}
