package com.cla.eventbus.events;

public class Event {

    public EventType eventType;
    private Context context = new Context();

    public Event() {
    }

    public Context getContext() {
        return context;
    }

    public EventType getEventType() {
        return eventType;
    }

    public static Builder builder() {
        Event event = new Event();
        return new Builder(event);
    }

    @Override
    public String toString() {
        return "Event{" +
                "eventType=" + eventType +
                ", context=" + context +
                '}';
    }

    public static final class Builder {

        private final Event event;

        public Builder(Event event) {
            this.event = event;
        }


        public Builder withEntry(String key, Object value) {
            this.event.getContext().addEntry(key, value);
            return this;
        }

        public Builder withEventType(EventType eventType) {
            this.event.eventType = eventType;
            return this;
        }


        public Event build() {
            return this.event;
        }

    }
}