package com.cla.eventbus.events.handler;

import com.cla.eventbus.EventBus;
import com.cla.eventbus.events.Event;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class AbstractEventHandler implements EventHandler {

    public static String ERROR_MSG_TEMPLATE = "Cannot fulfill %s, reason: %s";

    protected final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    protected EventBus eventBus;

    @Override
    public void handle(Event event) {
        ValidationResult validationResult = validateEvent(event);
        if (!validationResult.isOk()) {
            broadcastError(event, validationResult.getErrorType());
        }
        logger.trace("Handling {}", event);
        handleEvent(event);
    }

    protected abstract ValidationResult validateEvent(Event event);

    protected abstract void handleEvent(Event event);

    protected void broadcastError(Event invalidEvent, ErrorType errorType) {
        final String errorMessage = String.format(ERROR_MSG_TEMPLATE, invalidEvent, errorType.getText());
        logger.error(errorMessage);
        ErrorUtils.broadcastError(eventBus, errorType, errorMessage);
    }
}
