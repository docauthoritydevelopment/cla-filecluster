package com.cla.eventbus.events;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

public class Context {

    private Map<String, Object> map = new HashMap<>();

    public Context() {
    }

    public void addEntry(String key, Object value) {
        this.map.put(key, value);
    }

    public <T> T get(String key, Class<T> type) {
        final Object obj = map.get(key);
        return obj == null ? null : type.cast(obj);
    }

    public boolean containsKey(String key) {
        return map.containsKey(key);
    }

    public <T> ContextEntry<T> onEntry(String key, Class<T> type) {
        return ContextEntry.of(get(key, type));
    }

    public void onEachEntry(BiConsumer<String, Object> entryConsumer) {
        map.forEach(entryConsumer);
    }

    public <T> T getOrDefault(String key, T defaultValue) {
        boolean containsKey = map.containsKey(key);
        if (!containsKey && defaultValue == null) {
            return null;
        }
        return (T) map.getOrDefault(key, defaultValue);
    }

    public static final class ContextEntry<T> {

        private final T value;

        public ContextEntry(T value) {
            this.value = value;
        }

        public static <T> ContextEntry<T> of(T t) {
            return new ContextEntry<>(t);
        }

        /**
         * On given entry if found then run entryConsumer ont that entry, otherwise run the empty handler
         *
         * @param entryConsumer
         * @param noEntryHandler
         */
        public void perform(Consumer<T> entryConsumer, Runnable noEntryHandler) {
            if (value != null) {
                entryConsumer.accept(value);
            } else {
                noEntryHandler.run();
            }
        }
    }

    @Override
    public String toString() {
        return map.toString();

    }
}
