package com.cla.eventbus.events.handler;

public class ValidationResult {

    private final ErrorType errorType;

    public static ValidationResult Ok() {
        return new ValidationResult(null);
    }

    public static ValidationResult error(ErrorType errorType) {
        return new ValidationResult(errorType);
    }

    private ValidationResult(ErrorType errorType) {
        this.errorType = errorType;
    }

    public boolean isOk() {
        return errorType == null;
    }

    public ErrorType getErrorType() {
        return errorType;
    }
}