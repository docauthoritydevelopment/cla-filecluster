package com.cla.eventbus.events;

public final class EventKeys {

    public static final String PROCESS_KEY = "process-key";
    public static final String PROCESS = "process-definition";
    public static final String ERROR_TYPE = "error-type";
    public static final String ERROR_MESSAGE = "error-message";
    public static final String EXCEPTION = "exception";
    public static final String RUN_ID = "run-id";
    public static final String OPERATION_MESSAGE = "operation-message";
    public static final String FILE_MAP_FAIL = "map-failure";
    public static final String JOB_ID = "job-id";
    public static final String TASK_ID = "task-id";
    public static final String ITEM_COUNT = "item-count";
    public static final String MEANINGFUL_ITEM_COUNT = "meaningful-item-count";
    public static final String NEW_FILE = "new-file";
    public static final String FILE_METADATA_UPDATE = "file-metadata-update";
    public static final String FILE_CONTENT_UPDATE = "file-content-update";
    public static final String FILE_DIFF_PAYLOAD = "file-diff-payload";
    public static final String NEW_FILE_PAYLOAD = "new-file-payload";
    public static final String ROOT_FOLDER_ID= "root-folder-id";
    public static final String FILE_MEDIA_TYPE = "file-media-type";
    public static final String NEW_GROUPED_FILES = "new-grouped-files";
    public static final String ATTACHED_GROUPED_CONTENT_ID = "group-attach-content-id";
    public static final String ATTACHED_GROUPED_FILE_ID = "group-attach-file-id";
    public static final String IS_A_FILE = "is-a-file";
}
