package com.cla.eventbus.events.handler;

import com.cla.eventbus.EventBus;
import com.cla.eventbus.Topics;
import com.cla.eventbus.events.Event;
import com.cla.eventbus.events.EventKeys;
import com.cla.eventbus.events.EventType;

public final class ErrorUtils {

    public static void broadcastError(EventBus eventBus, ErrorType errorType, String errorMessage) {
        eventBus.broadcast(Topics.PROCESS_ERRORS, Event.builder()
                .withEventType(EventType.SCAN_ERROR)
                .withEntry(EventKeys.ERROR_TYPE, errorType)
                .withEntry(EventKeys.ERROR_MESSAGE, errorMessage)
                .build());
    }
}
