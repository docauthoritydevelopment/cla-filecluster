package com.cla.eventbus.vertx;

import com.cla.eventbus.events.Event;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.eventbus.MessageCodec;
import io.vertx.core.json.Json;

public class EventMessageCodec implements MessageCodec<Event, Event> {

    @Override
    public void encodeToWire(Buffer buffer, Event event) {
        final String encode = Json.encode(event);
        buffer.appendInt(encode.length());
        buffer.appendBuffer(Buffer.buffer(encode));
    }

    @Override
    public Event decodeFromWire(int pos, Buffer buffer) {
        int length = buffer.getInt(pos);
        pos += 4;
        return Json.decodeValue(buffer.slice(pos, pos + length), Event.class);
    }

    @Override
    public Event transform(Event event) {
        Event.Builder eventBuilder = Event.builder().withEventType(event.getEventType());
        event.getContext().onEachEntry(eventBuilder::withEntry);
        return eventBuilder.build();
    }

    @Override
    public String name() {
        return EventMessageCodec.class.getSimpleName();
    }

    @Override
    public byte systemCodecID() {
        return -1;
    }
}
