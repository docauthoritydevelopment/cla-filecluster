package com.cla.eventbus.vertx;

import com.cla.eventbus.EventBus;
import com.cla.eventbus.events.Event;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.eventbus.MessageConsumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.function.Consumer;

public class VertxEventBus implements EventBus {

    private final io.vertx.core.eventbus.EventBus eventBus;
    private final DeliveryOptions deliveryOptions;
    private final Executor consumersExecutor;
    private Map<ConsumerDelegationKey, ConsumerDelegation> consumerRegistry = new HashMap<>();

    private static final Logger logger = LoggerFactory.getLogger(VertxEventBus.class);

    public VertxEventBus(long sendTimeout, Executor consumersExecutor) {
        // Disable .vertx folder creation in production - set by installer launcher settings -Dvertx.disableFileCPResolving=true
        this.eventBus = Vertx.vertx().eventBus();
        this.consumersExecutor = consumersExecutor;
        eventBus.registerDefaultCodec(Event.class, new EventMessageCodec());
        this.deliveryOptions = new DeliveryOptions().setSendTimeout(sendTimeout);
    }

    @Override
    public synchronized void subscribe(String topic, Consumer<Event> consumer) {
        final ConsumerDelegationKey delegationKey = ConsumerDelegationKey.of(topic, consumer);
        ConsumerDelegation consumerDelegation = consumerRegistry.get(delegationKey);
        if (consumerDelegation == null) {
            final MessageConsumer<Event> messageConsumer = eventBus.consumer(topic);
            consumerDelegation = ConsumerDelegation.of(messageConsumer, consumer);
            consumerRegistry.put(delegationKey, consumerDelegation);
        }
        consumerDelegation.bind(consumersExecutor);
        logger.info("Subscribed {} to topic {}", consumer.getClass().getSimpleName(), topic);
    }

    @Override
    public synchronized void unSubscribe(String topic, Consumer<Event> consumer) {
        final ConsumerDelegationKey delegationKey = ConsumerDelegationKey.of(topic, consumer);
        final ConsumerDelegation consumerDelegation = consumerRegistry.get(delegationKey);
        if (consumerDelegation != null) {
            consumerDelegation.unbind();
            consumerRegistry.remove(delegationKey);
        }
    }


    @Override
    public void broadcast(String topic, Event event) {
        eventBus.publish(topic, event, deliveryOptions);
    }

    private static class ConsumerDelegation {

        private final MessageConsumer<Event> messageConsumer;
        private final Consumer<Event> consumer;

        public ConsumerDelegation(MessageConsumer messageConsumer, Consumer<Event> consumer) {
            this.messageConsumer = messageConsumer;
            this.consumer = consumer;
        }

        public static ConsumerDelegation of(MessageConsumer messageConsumer, Consumer<Event> consumer) {
            return new ConsumerDelegation(messageConsumer, consumer);
        }

        public void bind(Executor consumersExecutor) {
            this.messageConsumer.handler(event ->
                    consumersExecutor.execute(() -> consumer.accept(event.body())));
        }

        public void unbind() {
            messageConsumer.unregister();
        }
    }

    private static final class ConsumerDelegationKey {

        private final String topic;
        private final Consumer<? extends Event> consumer;

        public static <E extends Event> ConsumerDelegationKey of(String topic, Consumer<E> consumer) {
            return new ConsumerDelegationKey(topic, consumer);
        }

        public <E extends Event> ConsumerDelegationKey(String topic, Consumer<E> consumer) {
            this.topic = topic;
            this.consumer = consumer;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            ConsumerDelegationKey that = (ConsumerDelegationKey) o;

            if (topic != null ? !topic.equals(that.topic) : that.topic != null) return false;
            return consumer != null ? consumer.equals(that.consumer) : that.consumer == null;
        }

        @Override
        public int hashCode() {
            int result = topic != null ? topic.hashCode() : 0;
            result = 31 * result + (consumer != null ? consumer.hashCode() : 0);
            return result;
        }
    }


}
