package com.cla.eventbus;

import com.cla.eventbus.events.Event;

import java.util.function.Consumer;

/**
 * Pub/Sub mechanism:
 * <ul>
 * <li>Allow subscribers to register on topic</li>
 * <li>Expose api to broadcast events</li>
 * </ul>
 *
 * @author nadav
 */
public interface EventBus {

    void subscribe(String topic, Consumer<Event> consumer);

    void broadcast(String topic, Event event);

    void unSubscribe(String topic, Consumer<Event> consumer);

}