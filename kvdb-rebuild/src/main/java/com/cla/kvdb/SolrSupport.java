package com.cla.kvdb;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.impl.CloudSolrClient;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;

/**
 * Created by: yael
 * Created on: 11/22/2018
 */
public class SolrSupport {

    private static final Logger logger = LoggerFactory.getLogger(SolrSupport.class);

    private static int timeoutInMillis = 600000;

    public static SolrClient createSolrClientForCore(String coreName, String solrServerType, String solrServerURL) {
        SolrClient solrClient;
        String finalUrl = solrServerURL;
        boolean specificCore = !Strings.isNullOrEmpty(coreName);

        if (specificCore) {
            finalUrl += "/" + coreName + "/";
        }

        if (isSolrCloud(solrServerType, solrServerURL)) {
            String[] split = solrServerURL.split("/", 2);
            String zkUrl = split[0];
            String zkRoot = (split.length == 1) ? null : ("/" + split[1]);
            logger.info("Create Cloud Solr Client {} URL={} zkRoot={}, timeout: {} ms",
                    (coreName != null) ? coreName : "-Generic-", zkUrl, zkRoot, timeoutInMillis);
            solrClient = new CloudSolrClient.Builder(Lists.newArrayList(zkUrl), Optional.of(zkRoot))
                    .withSocketTimeout(timeoutInMillis)
                    .build();

            if (coreName != null) {
                ((CloudSolrClient) solrClient).setDefaultCollection(coreName);
            }
        } else {
            logger.info("Create Solr Client  {}", coreName);
            HttpSolrClient.Builder builder = new HttpSolrClient.Builder()
                    .withSocketTimeout(timeoutInMillis)
                    .allowCompression(true);
            solrClient = builder.withBaseSolrUrl(finalUrl).build();
        }
        return solrClient;
    }

    private static boolean isSolrCloud(String solrServerType, String solrServerURL) {
        return solrServerType.equalsIgnoreCase("cloud")
                || !solrServerURL.contains("http");
    }
}
