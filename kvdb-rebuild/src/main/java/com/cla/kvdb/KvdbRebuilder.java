package com.cla.kvdb;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import com.cla.common.constants.CatFileFieldType;
import com.cla.common.constants.CatFoldersFieldType;
import com.cla.common.domain.dto.crawler.FileEntity;
import com.cla.connector.utils.Pair;
import com.cla.eventbus.EventBus;
import com.cla.eventbus.events.Event;
import com.cla.filecluster.utils.EncUtils;
import com.google.common.base.Strings;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.slf4j.LoggerFactory;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.function.Consumer;

/**
 * Created by: yael
 * Created on: 11/18/2018
 */
@ShellComponent()
public class KvdbRebuilder {

    private String url;
    private String userName;
    private String password;
    private int amountPerCycle = 10000;
    private KeyValueDatabaseRepository kvdbRepo;
    private String defaultPropertiesFilePath = "./config";
    private final String configPathPropertyName = "configPath";
    private final String amountPerCyclePropertyName = "pageSize";

    private final String defaultKvdbStorePrefix = "appserver";

    private String solrServerURL;
    private String solrServerType = "http";

    private SolrClient fileSolrClient;
    private SolrClient folderSolrClient;

    /**
     * usage example: shell:>rebuild-kvdb "D:/temp"
     * @param output - path to place the new kvdb
     * @param storePrefix - store prefix (optional)
     */
    @ShellMethod("Rebuild KVDB")
    public void rebuildKvdb(String output, String storePrefix) {
        Connection connection = null;
        long globalFoldersCounter = 0;
        long globalFilesCounter = 0;
        String kvdbStorePrefix = (storePrefix == null || storePrefix.trim().isEmpty()) ? defaultKvdbStorePrefix : storePrefix.trim();

        try {
            silentThirdPartyLoggers();
        } catch (Exception e) {}

        try {
            if (validateFolderKvdbRepo(output)) {
                System.err.println(String.format("Folder %s already contains a kvdb repository. Exiting...", output));
                return;
            }
            String amountPerCycleString = System.getProperty(amountPerCyclePropertyName);
            if (amountPerCycleString != null) {
                try {
                    amountPerCycle = Integer.parseInt(amountPerCycleString);
                } catch (NumberFormatException e) {}
                System.out.println(String.format("Setting page size to %d", amountPerCycle));
            }
            String configPath = System.getProperty(configPathPropertyName);
            if (configPath == null) {
                configPath = defaultPropertiesFilePath;
            }
            getDbConnectionDetails(configPath);
            createKvdbRepo(output);
            createSolrClientsPerCore();
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection(url, userName, password);

            if (areThereActiveMapRuns(connection)) {
                System.err.println("there are existing map jobs for active runs, cannot activate tool. please stop runs");
                return;
            }

            Map<Long, String> rootFolderIds = getRootFolders(connection);

            for (Long rfId : rootFolderIds.keySet()) {
                System.out.println("start handling root folder " + rfId);

                String rfPath = rootFolderIds.get(rfId);

                long lastScanTime = getLastScanTime(connection, rfId);
                long rfFoldersCounter = 0;
                long rfFilesCounter = 0;

                Long totalFolders = getFoldersCount(rfId);
                Long totalFiles = getFilesCount(rfId);

                String cursorMark;
                String nextCursorMark = "*";
                Map<String, FileEntity> values;
                long ts = System.currentTimeMillis();
                do {
                    cursorMark = nextCursorMark;
                    Pair<String, Map<String, FileEntity>> res = getFoldersForRootFolder(rfId, cursorMark, amountPerCycle, lastScanTime, rfPath);
                    values = res.getValue();
                    nextCursorMark = res.getKey();
                    writeToKvdb(rfId, values, kvdbStorePrefix);
                    rfFoldersCounter += values.size();
                    globalFoldersCounter += values.size();
                    long newTs = System.currentTimeMillis();
                    if (!values.isEmpty()) {
                        System.out.println(String.format("Set %d folders for root folder %d in %dms. (%.1f%%)",
                                values.size(), rfId, newTs - ts, Math.min(100f,100f*rfFoldersCounter/totalFolders)));
                    } else {
                        nextCursorMark = cursorMark; // exit loop
                    }
                    ts = newTs;
                } while (!nextCursorMark.equals(cursorMark));

                nextCursorMark = "*";
                do {
                    cursorMark = nextCursorMark;
                    Pair<String, Map<String, FileEntity>> res = getFilesForRootFolder(rfId, cursorMark, amountPerCycle, rfPath, lastScanTime);
                    values = res.getValue();
                    nextCursorMark = res.getKey();
                    writeToKvdb(rfId, values, kvdbStorePrefix);
                    rfFilesCounter += values.size();
                    globalFilesCounter += values.size();
                    long newTs = System.currentTimeMillis();
                    if (!values.isEmpty()) {
                        System.out.println(String.format("Set %d files for root folder %d in %dms. (%.1f%%)",
                                values.size(), rfId, newTs - ts, Math.min(100f,100f*rfFilesCounter/totalFiles)));
                    } else {
                        nextCursorMark = cursorMark; // exit loop
                    }
                    ts = newTs;
                } while (!nextCursorMark.equals(cursorMark));
                System.out.println(String.format("Created %d folders and %d files for root folder %d%n",
                        rfFoldersCounter, rfFilesCounter, rfId));
            }
            System.out.println(String.format("%nDone restoring %d folders and %d files in %d root-folders.%n",
                    globalFoldersCounter, globalFilesCounter, rootFolderIds.size()));
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {if (connection != null) connection.close();} catch (Exception e) {}
            try {kvdbRepo.destroy();} catch (Exception e) {}
            try {fileSolrClient.close();} catch (Exception e) {}
            try {folderSolrClient.close();} catch (Exception e) {}
        }
    }

    private boolean validateFolderKvdbRepo(String output) {
        Path location = Paths.get(output);
        if (Files.isDirectory(location)) {
            String[] files = location.toFile().list();
            if (files != null) {
                for (String file : files) {
                    if (file.endsWith(".xd") || file.endsWith(".lck")) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private void createKvdbRepo(String output) {
        kvdbRepo = new KeyValueDatabaseRepository();
        kvdbRepo.setAutoDelete(false);
        kvdbRepo.setKvdbActive(true);
        kvdbRepo.setKvdbDataPath(output);

        EventBus eventBus = new EventBus() {
            @Override
            public void subscribe(String topic, Consumer<Event> consumer) {}

            @Override
            public void broadcast(String topic, Event event) {}

            @Override
            public void unSubscribe(String topic, Consumer<Event> consumer) {}
        };
        kvdbRepo.setEventBus(eventBus);

        kvdbRepo.init();
    }

    private Map<Long, String> getRootFolders(Connection connection) throws Exception {
        Statement stmt = null;
        ResultSet rs = null;
        try {
            stmt = connection.createStatement();
            String sql;
            sql = "select id, real_path from root_folder";
            rs = stmt.executeQuery(sql);
            Map<Long, String> rootFolderIds = new HashMap<>();
            while (rs.next()) {
                rootFolderIds.put(rs.getLong(1), rs.getString(2));
            }
            return rootFolderIds;
        } finally {
            try {if (rs != null) rs.close();} catch (Exception e) {}
            try {if (stmt != null) stmt.close();} catch (Exception e) {}
        }
    }

    private boolean areThereActiveMapRuns(Connection connection) throws Exception {
        Statement stmt = null;
        ResultSet rs = null;
        try {
            stmt = connection.createStatement();
            String sql;
            sql = "select count(*) from jm_jobs j, crawl_runs c " +
                    "where c.id = j.run_context " +
                    "and c.run_status in ('RUNNING', 'PAUSED') " +
                    "and j.`type` in ('SCAN', 'SCAN_FINALIZE') " +
                    "and j.state <> 'DONE'";
            rs = stmt.executeQuery(sql);
            if (rs.next() && rs.getInt(1) > 0) {
                return true;
            }
            return false;
        } finally {
            try {if (rs != null) rs.close();} catch (Exception e) {}
            try {if (stmt != null) stmt.close();} catch (Exception e) {}
        }
    }

    private Pair<String, Map<String, FileEntity>> getFilesForRootFolder(long rfId, String cursorMark, int amount, String rfPath, long lastScanTime) throws Exception {
        Map<String, FileEntity> files = new HashMap<>();
        SolrQuery query = new SolrQuery();
        query.setQuery("*:*");
        query.addFilterQuery("rootFolderId:" + rfId);
        query.setSort(SolrQuery.SortClause.asc(CatFileFieldType.ID.getSolrName()));
        query.setRows(amount);
        if (cursorMark == null) {
            cursorMark = "*";
        }
        query.set("cursorMark", cursorMark);
        query.setFields("fileId", "aclSignature", "deleted", "mediaEntityId, size, modificationDate, lastMetadataChangeDate");
        QueryResponse queryResponse = fileSolrClient.query(query);
        String nextCursorMark = queryResponse.getNextCursorMark();
        queryResponse.getResults().forEach(res -> {
            try {
                String media_entity_id = (String)res.getFieldValue("mediaEntityId");
                if (media_entity_id  == null) {
                    System.err.println(String.format("WARN: file with null mediaEntityId: %s", res));
                } else {
                    FileEntity fileEntity = new FileEntity();
                    Long fileLastScanned = (Long)res.getFieldValue("lastMetadataChangeDate");
                    if (fileLastScanned == null) {
                        fileLastScanned = lastScanTime;
                    }
                    fileEntity.setLastScanned(fileLastScanned);
                    fileEntity.setFile(true);
                    fileEntity.setFolder(false);
                    fileEntity.setInaccessible(false);
                    fileEntity.setFileId((Long)res.getFieldValue("fileId"));
                    fileEntity.setDeleted((Boolean)res.getFieldValue("deleted"));
                    fileEntity.setAclSignature((String)res.getFieldValue("aclSignature"));
                    fileEntity.setLastModified(((java.util.Date)res.getFieldValue("modificationDate")).getTime());
                    fileEntity.setSize((Long)res.getFieldValue("size"));
                    FileEntity exists = files.put(rfPath+media_entity_id, fileEntity);
                    if (exists != null) {
                        System.err.println(String.format("WARN: duplicate entry for %s: %s", rfPath+media_entity_id, res));
                    }
                }
            } catch (Exception e) {
                System.err.println(String.format("ERROR: could not process record %s", res));
            }
        });
        return Pair.of(nextCursorMark, files);
    }

    private void createSolrClientsPerCore() {
        fileSolrClient = SolrSupport.createSolrClientForCore("CatFiles", solrServerType, solrServerURL);
        folderSolrClient = SolrSupport.createSolrClientForCore("CatFolders", solrServerType, solrServerURL);
    }

    private long getLastScanTime(Connection connection, Long rootFolderId) throws Exception {
        Statement stmt = null;
        ResultSet rs = null;
        try {
            stmt = connection.createStatement();
            String sql;
            sql = "select UNIX_TIMESTAMP (c.stop_time)*1000 from crawl_runs c, root_folder r where r.id = "+rootFolderId+" and r.last_run_id = c.id";
            rs = stmt.executeQuery(sql);
            if (rs.next()) {
                return rs.getLong(1);
            } else {
                return System.currentTimeMillis();
            }
        } finally {
            try {if (rs != null) rs.close();} catch (Exception e) {}
            try {if (stmt != null) stmt.close();} catch (Exception e) {}
        }
    }

    private long getFilesCount(long rfId) throws Exception {
        return getCountByRootFolder(rfId, fileSolrClient);
    }

    private long getFoldersCount(long rfId) throws Exception {
        return getCountByRootFolder(rfId, folderSolrClient);
    }

    private long getCountByRootFolder(long rfId, SolrClient solrClient)  throws Exception {
        SolrQuery query = new SolrQuery();
        query.setQuery("*:*");
        query.addFilterQuery("rootFolderId:" + rfId);
        query.setRows(0);
        QueryResponse queryResponse = solrClient.query(query);
        return queryResponse.getResults().getNumFound();
    }

    private Pair<String, Map<String, FileEntity>> getFoldersForRootFolder(long rfId, String cursorMark, int amount, long lastScanTime, String rfPath) throws Exception {
        Map<String, FileEntity> files = new HashMap<>();
        SolrQuery query = new SolrQuery();
        query.setQuery("*:*");
        query.addFilterQuery("rootFolderId:" + rfId);
        query.setSort(SolrQuery.SortClause.asc(CatFoldersFieldType.ID.getSolrName()));
        query.setRows(amount);
        if (cursorMark == null) {
            cursorMark = "*";
        }
        query.set("cursorMark", cursorMark);
        query.setFields("id", "aclSignature", "deleted", "mediaEntityId");
        QueryResponse queryResponse = folderSolrClient.query(query);
        String nextCursorMark = queryResponse.getNextCursorMark();
        queryResponse.getResults().forEach(res -> {
            try {
                Object media_entity_id = res.getFieldValue("mediaEntityId");
                if (media_entity_id  == null) {
//                    System.err.println(String.format("WARN: folder with null media_entity_id: %s", res));
                } else {
                    FileEntity fileEntity = new FileEntity();
                    fileEntity.setFile(false);
                    fileEntity.setFolder(true);
                    fileEntity.setInaccessible(false);

                    fileEntity.setDeleted((Boolean)res.getFieldValue("deleted"));
                    fileEntity.setLastScanned(lastScanTime);
                    fileEntity.setFolderId((Long)res.getFieldValue("id"));
                    fileEntity.setAclSignature((String)res.getFieldValue("aclSignature"));
                    FileEntity exists = files.put(rfPath+media_entity_id, fileEntity);
                    if (exists != null) {
                        System.err.println(String.format("WARN: duplicate entry for %s: %s", rfPath+media_entity_id, res.toString()));
                    }
                }
            } catch (Exception e) {
                System.err.println(String.format("ERROR: could not process record %s", res.toString()));
            }
        });
        return Pair.of(nextCursorMark, files);
    }

    private void getDbConnectionDetails(String path) {
        Properties properties = new Properties();
        System.out.println("Reading properties file at " + path);
        File propertiesFile = new File(path,"application.properties");
        if (!propertiesFile.exists()) {
            System.out.println("Failed to find application.properties file ("+propertiesFile.getAbsolutePath()+")");
            return;
        }
        try {
            properties.load(new FileInputStream(propertiesFile));
        } catch (IOException e) {
            System.out.println("Failed to load properties file at "+path+" (IO Exception)"+e.getMessage());
            return;
        }

        String amountPerCycleStr = properties.getProperty("record-amount-per-cycle");
        if (!Strings.isNullOrEmpty(amountPerCycleStr)) {
            amountPerCycle = Integer.parseInt(amountPerCycleStr);
        }
        url = properties.getProperty("bonecp.url");
        userName = properties.getProperty("bonecp.username");
        password = properties.getProperty("bonecp.password");
        String useEncPasswordStr = properties.getProperty("use-enc-pass");
        boolean useEncPass = false;
        if (!Strings.isNullOrEmpty(useEncPasswordStr)) {
            useEncPass = Boolean.parseBoolean(useEncPasswordStr);
        }
        password = useEncPass ? EncUtils.decrypt(password) : password;

        solrServerType = properties.getProperty("spring.data.solr.type");
        solrServerURL = properties.getProperty("spring.data.solr.host");

//        String otherPathOption = properties.getProperty("filecluster-properties-path");
 //       if (!Strings.isNullOrEmpty(otherPathOption)) {
//            getDbConnectionDetails(otherPathOption);
 //       }
    }

    private String getStoreName(Long rootFolderId, String storePrefix) {
        return kvdbRepo.getRepoName(rootFolderId, storePrefix);
    }

    private void writeToKvdb(Long rootFolderId, Map<String, FileEntity> files, String storePrefix) {
        String storeName = getStoreName(rootFolderId, storePrefix);
        kvdbRepo.getFileEntityDao().executeInTransaction(null, storeName, fileEntityStore -> {
            for (Map.Entry<String, FileEntity> entry : files.entrySet()) {
                FileEntity older = fileEntityStore.get(entry.getKey());
                if (older != null) {
                    fileEntityStore.put(entry.getKey(), merge(older, entry.getValue()));
                } else {
                    fileEntityStore.put(entry.getKey(), entry.getValue());
                }
            }
            return true;
        });
    }

    private FileEntity merge(FileEntity older, FileEntity newer) {
        FileEntity result;
        FileEntity other;
        if (older.isFile()) {
            result = older;
            other = newer;
        } else {
            result = newer;
            other = older;
        }

        if (!result.isFolder()) {
            result.setFolder(other.isFolder());
            result.setFolderId(other.getFolderId());
        }

        return result;
    }

    private static void silentThirdPartyLoggers() {
        Logger logger;
        logger = (Logger) LoggerFactory.getLogger("jetbrains.exodus");
        logger.setLevel(Level.ERROR);
        logger = (Logger) LoggerFactory.getLogger("com.cla");
        logger.setLevel(Level.WARN);
        logger = (Logger) LoggerFactory.getLogger("org");
        logger.setLevel(Level.WARN);
    }
}