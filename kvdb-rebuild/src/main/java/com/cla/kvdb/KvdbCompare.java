package com.cla.kvdb;

import com.cla.common.domain.dto.crawler.FileEntity;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;

import java.util.List;

/**
 * Created by: yael
 * Created on: 11/18/2018
 */
@ShellComponent()
public class KvdbCompare {

    /**
     * shell:>kvdb-compare "D:/temp" "D:/temp2"
     * @param source1 - path to first kvdb
     * @param source2 - path to second kvdb
     */
    @ShellMethod("Compare KVDB")
    public void kvdbCompare(String source1, String source2) {
        KeyValueDatabaseRepository kvdbRepo1 = null;
        KeyValueDatabaseRepository kvdbRepo2 = null;

        try {
            boolean isDifferent = false;

            kvdbRepo1 = createKvdbRepo(source1);
            kvdbRepo2 = createKvdbRepo(source2);

            List<String> storeNames1 = kvdbRepo1.getFileEntityDao().getAllStoreNames();
            List<String> storeNames2 = kvdbRepo2.getFileEntityDao().getAllStoreNames();

            if (storeNames1.size() > storeNames2.size()) {
                System.out.println("there are missing stores in store 2");
                isDifferent = true;
            } else if (storeNames1.size() < storeNames2.size()) {
                System.out.println("there are missing stores in store 1");
                isDifferent = true;
            } else {
                for (String name : storeNames1) {
                    if (!storeNames2.contains(name)) {
                        System.out.println("there are missing stores in store 2 " + name);
                        isDifferent = true;
                    }
                }
            }

            if (!isDifferent) {
                for (String name : storeNames1) {
                    KeyValueDatabaseRepository finalKvdbRepo2 = kvdbRepo2;
                    kvdbRepo1.getFileEntityDao().executeInTransaction(null, name, store1 -> {
                        finalKvdbRepo2.getFileEntityDao().executeInTransaction(null, name, store2 -> {
                            store1.forEach((key, fileEntity) -> {
                                FileEntity fileEntity2 = store2.get(key);
                                compareEntities(key, fileEntity, fileEntity2);
                            });
                            return true;
                        });
                        return true;
                    });
                }
            }
        } finally {
            try {if (kvdbRepo1 != null) kvdbRepo1.destroy();} catch (Exception e) {}
            try {if (kvdbRepo2 != null) kvdbRepo2.destroy();} catch (Exception e) {}
            System.out.println("DONE");
        }
    }

    private void compareEntities(String key, FileEntity e1, FileEntity e2) {
        if (e1 == null && e2 != null) {
            System.out.println("entity only in store 2 "+key+" " +e2);
        } else if (e1 != null && e2 == null) {
            System.out.println("entity only in store 1 "+key+" "+e1);
        } else if (e1.isFile() && !e1.getFileId().equals(e2.getFileId())) {
            System.out.println("entity diff file id "+e1+" " +e2);
        } else if (e1.isFolder() && !e1.getFolderId().equals(e2.getFolderId())) {
            System.out.println("entity diff folder id "+e1+" " +e2);
        } else if (!e1.getAclSignature().equals(e2.getAclSignature())) {
            System.out.println("entity diff acl "+e1+" " +e2);
        } else if (e1.getSize() != null && !e1.getSize().equals(e2.getSize())) {
            System.out.println("entity diff size "+e1+" " +e2);
        } else if (e1.isDeleted() != e2.isDeleted()) {
            System.out.println("entity diff deleted "+e1+" " +e2);
        } else if (e1.getContentMdId() != null && !e1.getContentMdId().equals(e2.getContentMdId())) {
            System.out.println("entity diff deleted "+e1+" " +e2);
        } else if (e1.getLastScanned() == null || e1.getLastScanned() <= 0) {
            System.out.println("entity store 1 bad last scan date "+e1);
        } else if (e2.getLastScanned() == null || e2.getLastScanned() <= 0) {
            System.out.println("entity store 2 bad last scan date "+e1);
        }
    }

    private KeyValueDatabaseRepository createKvdbRepo(String path) {
        KeyValueDatabaseRepository kvdbRepo = new KeyValueDatabaseRepository();
        kvdbRepo.setAutoDelete(false);
        kvdbRepo.setKvdbActive(true);
        kvdbRepo.setKvdbDataPath(path);
        kvdbRepo.init();
        return kvdbRepo;
    }
}