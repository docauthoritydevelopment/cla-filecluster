package com.cla.kvdb;

/**
 * Created by: yael
 * Created on: 11/18/2018
 */

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KvdbRebuildApplication {
    public static void main(String[] args) {
        if (args != null && (args.length == 2 || args.length == 3) && args[0].equals("rebuild-kvdb")) {
            KvdbRebuilder builder = new KvdbRebuilder();
            builder.rebuildKvdb(args[1], (args.length == 3)?args[2]:null);
        } else {
            SpringApplication.run(KvdbRebuildApplication.class, args);
        }
    }
}