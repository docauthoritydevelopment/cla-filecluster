package com.cla.searchpattern.custom;

import com.cla.common.domain.dto.filesearchpatterns.TextSearchPatternImpl;

import java.util.function.Predicate;

public class TextSearchPatternSkeleton extends TextSearchPatternImpl {

    public static final String SKELETON_PATERN = "\\b\\d{5,9}\\b";

    public TextSearchPatternSkeleton() {
    }

    public TextSearchPatternSkeleton(final Integer id, final String name) {
        super(id, name, SKELETON_PATERN);
    }

    @Override
    public Predicate<String> getPredicate() {
        return text -> {
            // implement your extra validation logic here
            return false;
        };
    }


}
