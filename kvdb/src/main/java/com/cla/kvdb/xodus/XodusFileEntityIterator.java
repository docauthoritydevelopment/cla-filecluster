package com.cla.kvdb.xodus;

import com.cla.connector.utils.Pair;
import com.cla.kvdb.FileEntityIterator;
import com.cla.common.domain.dto.crawler.FileEntity;
import jetbrains.exodus.bindings.StringBinding;
import jetbrains.exodus.env.Cursor;

/**
 * Xodus-specific implementation of FileEntityIterator
 * Created by vladi on 8/13/2017.
 */
public class XodusFileEntityIterator implements FileEntityIterator{

    private Cursor cursor;

    public static XodusFileEntityIterator create(Cursor cursor){
        XodusFileEntityIterator iterator = new XodusFileEntityIterator();
        iterator.setCursor(cursor);
        return iterator;
    }

    @Override
    public boolean goToKey(String key) {
        return cursor.getSearchKey(StringBinding.stringToEntry(key)) != null;
    }

    @Override
    public Pair<String, FileEntity> current() {
        return getCurrentPair();
    }

    @Override
    public Pair<String, FileEntity> next() {
        if (cursor.getNext()){
            return getCurrentPair();
        }
        return null;
    }

    @Override
    public Pair<String, FileEntity> prev() {
        if (cursor.getPrev()){
            return getCurrentPair();
        }
        return null;
    }

    @Override
    public boolean deleteCurrent() {
        return cursor.deleteCurrent();
    }

    public void setCursor(Cursor cursor) {
        this.cursor = cursor;
    }

    private Pair<String, FileEntity> getCurrentPair(){
        return Pair.of(StringBinding.entryToString(cursor.getKey()),
                FileEntityXodusSerializer.deserialize(cursor.getValue()));
    }
}
