package com.cla.kvdb.xodus;

import com.cla.common.exceptions.BreakStreamException;
import com.cla.common.utils.ErrorHandlingUtils;
import com.cla.eventbus.EventBus;
import com.cla.eventbus.Topics;
import com.cla.eventbus.events.Event;
import com.cla.eventbus.events.EventKeys;
import com.cla.eventbus.events.EventType;
import com.cla.eventbus.events.handler.ErrorType;
import com.cla.kvdb.*;
import jetbrains.exodus.ConfigurationStrategy;
import jetbrains.exodus.ExodusException;
import jetbrains.exodus.env.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;

/**
 * Xodus-specific implementation of key-value DB dao
 */
public class XodusFileEntityDao extends KeyValueFileEntityDao {

    private final static Logger logger = LoggerFactory.getLogger(XodusFileEntityDao.class);

    private ContextualEnvironment env;

    public XodusFileEntityDao(KeyValueDaoConfig config, EventBus eventBus) {
        super(config, eventBus);
    }

    @Override
    public void init() {
        EnvironmentConfig xodusConfig = new EnvironmentConfig(ConfigurationStrategy.IGNORE);
        xodusConfig.setLogDurableWrite(config.isDurableTransactions());
        env = Environments.newContextualInstance(config.getBasePath(), xodusConfig);
    }

    public void close() throws IOException {
        env.close();
    }

    @Override
    public List<String> getAllStoreNames() {
        Transaction tx = env.beginReadonlyTransaction();
        try {
            return env.getAllStoreNames();
        } finally {
            tx.abort();
        }
    }

    public boolean storeExists(String storeName) {
        List<String> storeNames = getAllStoreNames();
        return storeNames.contains(storeName);
    }

    public <T> T executeInTransaction(Long runId, String storeName, Executable<T> executable) {
        try {
            ResultWrapper<T> resultWrapper = new ResultWrapper<>();
            env.executeInTransaction(txn -> {
                try {
                    ContextualStore store = env.openStore(storeName, StoreConfig.WITHOUT_DUPLICATES_WITH_PREFIXING, txn);
                    resultWrapper.wrap(executable.execute(XodusFileEntityStore.create(store)));
                } catch (BreakStreamException bse) {
                    logger.info("Xodus transaction was aborted following job interruption request (probably paused by user).");
                    throw bse;
                }
            });
            return resultWrapper.unwrap();
        } catch (ExodusException e) {
            handlePossibleIoRelatedException(runId, storeName, e);
            throw e;
        }
    }

    public <T> T executeInReadonlyTransaction(String storeName, Executable<T> executable) {
        ResultWrapper<T> resultWrapper = new ResultWrapper<>();
        env.executeInReadonlyTransaction(txn -> {
            try{
                ContextualStore store = env.openStore(storeName, StoreConfig.USE_EXISTING, txn);
                resultWrapper.wrap(executable.execute(XodusFileEntityStore.create(store)));
            } catch(BreakStreamException bse){
                logger.info("Xodus transaction was aborted following job interruption request (probably paused by user).");
                throw bse;
            }
        });
        return resultWrapper.unwrap();
    }


    public void iterateInTransaction(Long runId, String storeName, boolean readOnly, KvdbIterable iterable) {
        try {
            Transaction tx = readOnly ? env.beginReadonlyTransaction() : env.beginTransaction();
            ContextualStore store = env.openStore(storeName, StoreConfig.WITHOUT_DUPLICATES_WITH_PREFIXING, tx, !readOnly);
            if (store == null) {
                logger.warn("Failed to open store " + storeName);
                return;
            }

            try (Cursor cursor = store.openCursor()) {
                FileEntityIterator iterator = XodusFileEntityIterator.create(cursor);
                iterable.iterate(iterator);
                tx.commit();
            } catch (Exception e) {
                logger.error("Xodus transaction aborted.", e);
                tx.abort();
            }
        } catch (ExodusException e) {
            handlePossibleIoRelatedException(runId, storeName, e);
            throw e;
        }
    }

    @Override
    public XodusFileEntityStore openTransaction(String storeName, boolean readOnly) {
        Transaction tx = readOnly ? env.beginReadonlyTransaction() : env.beginTransaction();
        ContextualStore store = env.openStore(storeName, StoreConfig.WITHOUT_DUPLICATES_WITH_PREFIXING, tx, !readOnly);
        return XodusFileEntityStore.create(store);
    }

    @Override
    public void deleteStore(String storeName) {
        env.executeInTransaction( tx -> {
            env.removeStore(storeName, tx);
        });
    }

    @Override
    public void createStore(String storeName) {
        env.executeInTransaction( tx -> {
            env.openStore(storeName, StoreConfig.WITHOUT_DUPLICATES_WITH_PREFIXING, tx, true);
        });
    }

    @Override
    public void commit() {
        safeGetCurrentTransaction().commit();
    }

    @Override
    public void flush() {
        safeGetCurrentTransaction().flush();
    }

    @Override
    public void revert() {
        safeGetCurrentTransaction().revert();
    }

    @Override
    public void abort() {
        safeGetCurrentTransaction().abort();
    }

    @Override
    public void dropDb() {
        env.clear();
    }

    private Transaction safeGetCurrentTransaction() {
        Transaction currentTransaction = env.getCurrentTransaction();
        if (currentTransaction == null) {
            throw new RuntimeException("Current transaction was null.");
        }
        return currentTransaction;
    }

    private static class ResultWrapper<T> {
        private T result;

        private void wrap(T result) {
            this.result = result;
        }

        private T unwrap() {
            return result;
        }


    }

    private void handlePossibleIoRelatedException(Long runId, String storeName, ExodusException e) {
        Throwable cause = e.getCause();
        if (ErrorHandlingUtils.isIORelatedException(cause)) {
            String errorMsg = "Failed to execute Xodus transaction on store: " + storeName;
            logger.error(errorMsg, e);
            eventBus.broadcast(Topics.KVDB_ERRORS,
                    Event.builder()
                            .withEventType(EventType.SCAN_ERROR)
                            .withEntry(EventKeys.ERROR_TYPE, ErrorType.IO_FAILURE)
                            .withEntry(EventKeys.ERROR_MESSAGE, errorMsg)
                            .withEntry(EventKeys.RUN_ID, runId)
                            .withEntry(EventKeys.EXCEPTION, e)
                            .build());
        }
    }
}
