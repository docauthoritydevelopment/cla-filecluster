package com.cla.kvdb.xodus;

import jetbrains.exodus.bindings.StringBinding;

import java.util.Comparator;

/**
 *
 * Comparator used to sort strings in the order consistent with Xodus key sorting
 */
public class XodusStringComparator implements Comparator<String>{

    @Override
    public int compare(String str1, String str2) {
        return StringBinding.stringToEntry(str1).compareTo(StringBinding.stringToEntry(str2));
    }
}
