package com.cla.kvdb.xodus;

import com.cla.common.domain.dto.crawler.FileEntity;
import com.google.common.base.Charsets;
import com.google.common.base.Strings;
import jetbrains.exodus.ArrayByteIterable;
import jetbrains.exodus.ByteIterable;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;

/**
 * File entity serializer for storing in Xodus
 */
public class FileEntityXodusSerializer {

    private static final int LENGTH_WITHOUT_SIGPATH = 60; // 4 bytes, 2 int * 4, 6 long * 8 (+ sig + path)
    private static final Charset CHARSET = Charsets.UTF_8;

    public static FileEntity deserialize(ByteIterable bi){
        if (bi == null){
            return null;
        }
        byte[] bytes = bi.getBytesUnsafe();
        ByteBuffer buffer = ByteBuffer.allocate(bytes.length);
        buffer.put(bi.getBytesUnsafe());
        buffer.rewind();

        FileEntity entity = new FileEntity();
        entity.setFolder(buffer.get() == 1);
        entity.setLastModified(safeLongBox(buffer.getLong()));
        entity.setSize(safeLongBox(buffer.getLong()));
        entity.setFileId(safeLongBox(buffer.getLong()));
        entity.setContentMdId(safeLongBox(buffer.getLong()));
        entity.setLastScanned(safeLongBox(buffer.getLong()));
        entity.setDeleted(buffer.get() == 1);
        int signatureLength = buffer.getInt();
        byte[] sigBytes = new byte[signatureLength];
        buffer.get(sigBytes);
        entity.setAclSignature(new String(sigBytes, CHARSET));
        if (buffer.hasRemaining()) {
            entity.setInaccessible(buffer.get() == 1);
        }
        if (buffer.hasRemaining()) {
            entity.setFile(buffer.get() == 1);
        }
        if (buffer.hasRemaining()) {
            entity.setFolderId(safeLongBox(buffer.getLong()));
        }
        if (buffer.hasRemaining()) {
            int pathLength = buffer.getInt();
            byte[] pathBytes = new byte[pathLength];
            buffer.get(pathBytes);
            entity.setPath(new String(pathBytes, CHARSET));
        }

        return entity;
    }

    public static ByteIterable serialize(FileEntity entity){
        if (entity == null){
            throw new RuntimeException("Received null file entity");
        }
        String aclSignature = entity.getAclSignature();

        int sigLength;
        byte[] sigBytes = null;
        if (Strings.isNullOrEmpty(aclSignature)){
            sigLength = 0;
        }
        else{
            sigBytes = aclSignature.getBytes(CHARSET);
            sigLength = sigBytes.length;
        }

        int pathLength;
        byte[] pathBytes = null;
        if (Strings.isNullOrEmpty(entity.getPath())){
            pathLength = 0;
        }
        else{
            pathBytes = entity.getPath().getBytes(CHARSET);
            pathLength = pathBytes.length;
        }

        ByteBuffer buffer = ByteBuffer.allocate(LENGTH_WITHOUT_SIGPATH + sigLength + pathLength);
        buffer.put(entity.isFolder() ? (byte)1 : (byte)0);
        buffer.putLong(safeLongUnbox(entity.getLastModified()));
        buffer.putLong(safeLongUnbox(entity.getSize()));
        buffer.putLong(safeLongUnbox(entity.getFileId()));
        buffer.putLong(safeLongUnbox(entity.getContentMdId()));
        buffer.putLong(safeLongUnbox(entity.getLastScanned()));
        buffer.put(entity.isDeleted() ? (byte)1 : (byte)0);
        buffer.putInt(sigLength);
        if (sigBytes != null){
            buffer.put(sigBytes);
        }
        buffer.put(entity.isInaccessible() ? (byte)1 : (byte)0);
        buffer.put(entity.isFile() ? (byte)1 : (byte)0);
        buffer.putLong(safeLongUnbox(entity.getFolderId()));

        buffer.putInt(pathLength);
        if (pathBytes != null){
            buffer.put(pathBytes);
        }

        buffer.rewind();
        return new ArrayByteIterable(buffer.array());
    }

    private static long safeLongUnbox(Long l){
        return l == null ? -1L : l;
    }

    private static Long safeLongBox(long l) {
        return l == -1L ? null : l;
    }
}
