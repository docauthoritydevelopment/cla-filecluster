package com.cla.kvdb.xodus;

import com.cla.connector.utils.FileNamingUtils;
import com.cla.kvdb.FileEntityStore;
import com.cla.common.domain.dto.crawler.FileEntity;
import jetbrains.exodus.ByteIterable;
import jetbrains.exodus.bindings.StringBinding;
import jetbrains.exodus.env.ContextualStore;
import jetbrains.exodus.env.Cursor;
import jetbrains.exodus.env.Store;

import java.util.function.BiConsumer;

/**
 * Xodus-specific executable
 */
public class XodusFileEntityStore implements FileEntityStore{
    protected ContextualStore store;

    public static XodusFileEntityStore create(ContextualStore store){
        XodusFileEntityStore result = new XodusFileEntityStore();
        result.setStore(store);
        return result;
    }

    public boolean put(String key, FileEntity value){
        key = FileNamingUtils.convertPathToUniversalString(key);
		boolean put = store.put(StringBinding.stringToEntry(key), FileEntityXodusSerializer.serialize(value));
		return put;
	}

    public FileEntity get(String key){
        key = FileNamingUtils.convertPathToUniversalString(key);
        ByteIterable bi = store.get(StringBinding.stringToEntry(key));
        return FileEntityXodusSerializer.deserialize(bi);
    }

    public long count(){
        return store.count();
    }

    public void delete(String key){
        store.delete(StringBinding.stringToEntry(key));
    }

    public Store getStore() {
        return store;
    }

    public void setStore(ContextualStore store) {
        this.store = store;
    }

    public void forEach(BiConsumer<String, FileEntity> action){
        try (Cursor cursor = store.openCursor()) {
            while (cursor.getNext()) {
                action.accept(StringBinding.entryToString(cursor.getKey()),
                        FileEntityXodusSerializer.deserialize(cursor.getValue()));
            }
        }
    }

}
