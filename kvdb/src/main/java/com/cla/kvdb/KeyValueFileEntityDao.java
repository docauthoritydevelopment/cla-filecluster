package com.cla.kvdb;

import com.cla.eventbus.EventBus;
import com.cla.kvdb.xodus.XodusFileEntityDao;

import java.io.Closeable;
import java.util.List;
import java.util.Objects;

/**
 *
 */
public abstract class KeyValueFileEntityDao implements Closeable {

    protected KeyValueDaoConfig config;
    protected EventBus eventBus;

    protected KeyValueFileEntityDao(KeyValueDaoConfig config, EventBus eventBus) {
        this.config = config;
        this.eventBus = Objects.requireNonNull(eventBus);
    }

    public static KeyValueFileEntityDao getInstance(KeyValueDaoConfig config, EventBus eventBus) {
        KeyValueFileEntityDao dao;

        switch (config.getImplementation()) {
            case XODUS:
            default:
                dao = new XodusFileEntityDao(config, eventBus);
        }
        dao.init();
        return dao;
    }

    public abstract void init();


    public abstract <T> T executeInTransaction(Long RunId, String storeName, Executable<T> executable);

    public abstract <T> T executeInReadonlyTransaction(String storeName, Executable<T> executable);

    public abstract List<String> getAllStoreNames();

    public abstract boolean storeExists(String storeName);

    public abstract void createStore(String storeName);

    public abstract void iterateInTransaction(Long runId, String storeName, boolean readOnly, KvdbIterable iterable);

    public abstract FileEntityStore openTransaction(String storeName, boolean readOnly);

    public abstract void commit();

    public abstract void flush();

    public abstract void revert();

    public abstract void abort();

    public abstract void dropDb();

    public void deleteStore(String storeName) {};

}
