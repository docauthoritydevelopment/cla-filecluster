package com.cla.kvdb;

/**
 * Configuration settings for key-value DB
 */
public class KeyValueDaoConfig {

    public enum Implementation{
        XODUS
    }

    private Implementation implementation = Implementation.XODUS;
    private String basePath;
    private boolean duplicateEntries = false;
    private boolean durableTransactions = false;

    private KeyValueDaoConfig(){}

    public static KeyValueDaoConfig create(){
        return new KeyValueDaoConfig();
    }

    public String getBasePath() {
        return basePath;
    }

    public KeyValueDaoConfig setBasePath(String basePath) {
        this.basePath = basePath;
        return this;
    }

    public boolean isDuplicateEntries() {
        return duplicateEntries;
    }

    public KeyValueDaoConfig setDuplicateEntries(boolean duplicateEntries) {
        this.duplicateEntries = duplicateEntries;
        return this;
    }

    public boolean isDurableTransactions() {
        return durableTransactions;
    }

    public void setDurableTransactions(boolean durableTransactions) {
        this.durableTransactions = durableTransactions;
    }

    public Implementation getImplementation() {
        return implementation;
    }

    public void setImplementation(Implementation implementation) {
        this.implementation = implementation;
    }
}
