package com.cla.kvdb;

/**
 *
 * Created by vladi on 8/13/2017.
 */
public interface KvdbIterable {

    void iterate(FileEntityIterator iterate);

}
