package com.cla.kvdb.domain;

import com.cla.common.domain.dto.crawler.FileEntity;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.file.DiffType;
import com.cla.connector.domain.dto.media.MediaChangeLogDto;
import com.cla.kvdb.FileEntityStore;

import java.util.Objects;

/**
 * Created by Itai Marko.
 */
public class FileEntityUtils {

    /**
     * Detect changes between existing file entity and re-scan result message
     * @param oldFileEntity     existing file entity
     * @param payload           re-scan result message payload
     * @return                  difference type: NEW, SIMILAR, ACL_UPDATED, CONTENT_UPDATED
     *                          (DELETED will be detected separately)
     */
    public static DiffType detectChanges(boolean isFullScan, FileEntity oldFileEntity, ClaFilePropertiesDto payload, boolean isFileShare){
        if (payload.isChangeLogSupported()) {
            DiffType detectDiff = ((MediaChangeLogDto) payload).getEventType();
            if (detectDiff != null) {
                return detectDiff;
            }
        }

        if (oldFileEntity == null) {
            return DiffType.NEW;
        }

        if (oldFileEntity.isDeleted()) {
            return DiffType.UNDELETED;
        }

        if (payload.isFolder()){
            if (Objects.equals(payload.getAclSignature(), oldFileEntity.getAclSignature())){
                return DiffType.SIMILAR;
            } else{
                return DiffType.ACL_UPDATED;
            }
        } else {
            if (isFullScan) {
                return !isFileShare ? DiffType.RENAMED : DiffType.CONTENT_UPDATED;
            } else if (!Objects.equals(payload.getFileSize(), oldFileEntity.getSize()) ||
                    !Objects.equals(payload.getModTimeMilli(), oldFileEntity.getLastModified())) {
                return DiffType.CONTENT_UPDATED;
            } else if (equalAclSignatures(payload.getAclSignature(), oldFileEntity.getAclSignature())){
                return DiffType.SIMILAR;
            } else{
                return DiffType.ACL_UPDATED;
            }
        }
    }

    private static boolean equalAclSignatures(String aclSig1, String aclSig2) {
        if (aclSig1 == null || aclSig1.isEmpty()) {
            // consider null and empty as equal
            return aclSig2 == null || aclSig2.isEmpty();
        } else {
            return Objects.equals(aclSig1, aclSig2);
        }
    }

    public static boolean isContentUpdated(FileEntity oldFileEntity, ClaFilePropertiesDto payload) {
        return oldFileEntity != null && !oldFileEntity.isDeleted() && payload.isFile() &&
                (!Objects.equals(payload.getFileSize(), oldFileEntity.getSize()) ||
                        !Objects.equals(payload.getModTimeMilli(), oldFileEntity.getLastModified()));
    }

    public static boolean isAclUpdated(FileEntity oldFileEntity, ClaFilePropertiesDto payload) {
        return oldFileEntity != null && !oldFileEntity.isDeleted() &&
                !Objects.equals(payload.getAclSignature(), oldFileEntity.getAclSignature());
    }

    /**
     * Create entity to put into a {@link FileEntityStore}
     *
     * @param fileId    file/folder ID
     * @param time      timestamp of the scan
     * @param props     properties
     * @return          KVDB file entity
     */
    public static FileEntity createFileEntity(Long fileId, Long folderId, long time, ClaFilePropertiesDto props, String path){
        FileEntity fileEntity = new FileEntity();
        fileEntity.setLastScanned(time);
        fileEntity.setFile(props.isFile());
        fileEntity.setFolder(props.isFolder());
        fileEntity.setInaccessible(props.isInaccessible());
        fileEntity.setFileId(fileId);
        fileEntity.setFolderId(folderId);
        fileEntity.setAclSignature(props.getAclSignature());
        if (props.isFile()){
            fileEntity.setLastModified(props.getModTimeMilli());
            fileEntity.setContentMdId(props.getContentMetadataId());
            fileEntity.setSize(props.getFileSize());
        }
        fileEntity.setPath(path);
        return fileEntity;
    }
}
