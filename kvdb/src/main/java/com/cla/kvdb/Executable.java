package com.cla.kvdb;

/**
 * Executable to perform inside KVDB transaction.
 * Override the execute method and use the provided store to perform operations
 */
public interface Executable<T> {

     T execute(FileEntityStore store);

}
