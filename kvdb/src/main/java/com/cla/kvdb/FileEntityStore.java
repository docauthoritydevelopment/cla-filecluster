package com.cla.kvdb;

import com.cla.common.domain.dto.crawler.FileEntity;

import java.util.function.BiConsumer;

/**
 * FileEntityStore for KVDB operations in transaction
 */
public interface FileEntityStore{

    boolean put(String key, FileEntity value);

    FileEntity get(String key);

    long count();

    void delete(String key);

    void forEach(BiConsumer<String, FileEntity> action);
}
