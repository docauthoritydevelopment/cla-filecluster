package com.cla.kvdb;

import com.cla.common.domain.dto.crawler.FileEntity;
import com.cla.connector.utils.Pair;

/**
 * Iterator for KVDB operations in transaction
 */
public interface FileEntityIterator {

    boolean goToKey(String key);

    Pair<String, FileEntity> current();

    Pair<String, FileEntity> next();

    Pair<String, FileEntity> prev();

    boolean deleteCurrent();

}
