package com.cla.kvdb;

import com.cla.eventbus.EventBus;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 *
 */
@Service
public class KeyValueDatabaseRepository {

    private final static Logger logger = LoggerFactory.getLogger(KeyValueDatabaseRepository.class);

    @Value("${spring.jpa.hibernate.ddl-auto:}")
    private String hibernateDdlAuto;

    @Value("${kvdb.autoDelete:true}")
    private boolean autoDelete;

    @Value("${kvdb.data-dir:}")
    private String kvdbDataPath;

    @Value("${kvdb.active:true}")
    private Boolean isKvdbActive;

    @Autowired
    private EventBus eventBus;

    private KeyValueFileEntityDao fileEntityDao;

    @PostConstruct
    public void init(){
        if (!isKvdbActive || kvdbDataPath==null || kvdbDataPath.trim().isEmpty()) {
            logger.warn("kvdb inactive. Not initialized ({})", kvdbDataPath);
            return;
        }
        if (autoDelete && hibernateDdlAuto.contains("create") && Files.isDirectory(Paths.get(kvdbDataPath))) {
            logger.info("Delete kvdb folder content as hibernateDdlAuto is on create, which causes DB tables recreate: {}", kvdbDataPath);
            try {
                FileUtils.cleanDirectory(new File(kvdbDataPath));
            } catch (IOException e) {
                logger.error("Could not clean kvdb directory at: {}", kvdbDataPath, e);
            }
        }
        if (!Files.isDirectory(Paths.get(kvdbDataPath))) {
            try {
                logger.info("Creating missing kvdbDataPath: {} ...", kvdbDataPath);
                Files.createDirectories(Paths.get(kvdbDataPath));
            } catch (IOException e) {
                logger.error("Could not create missing kvdbDataPath: {}. Error: {}", kvdbDataPath, e);
                throw new RuntimeException(e);
            }
        }
        KeyValueDaoConfig keyValueDaoConfig = KeyValueDaoConfig.create().setBasePath(kvdbDataPath);
        fileEntityDao = KeyValueFileEntityDao.getInstance(keyValueDaoConfig, eventBus);
    }

    @PreDestroy
    public void destroy(){
        try {
            if (fileEntityDao != null) {
                fileEntityDao.close();
            }
        } catch (Exception e) {
           logger.error("Failed to close KVDB dao", e);
        }
    }

    public KeyValueFileEntityDao getFileEntityDao() {
        return fileEntityDao;
    }

    public String getRepoName(Long rootFolderId, String storePrefix){
        return storePrefix + "_scan_" + rootFolderId;
    }

    public void setAutoDelete(boolean autoDelete) {
        this.autoDelete = autoDelete;
    }

    public void setKvdbDataPath(String kvdbDataPath) {
        this.kvdbDataPath = kvdbDataPath;
    }

    public void setKvdbActive(Boolean kvdbActive) {
        isKvdbActive = kvdbActive;
    }

    public void setEventBus(EventBus eventBus) {
        this.eventBus = eventBus;
    }
}
