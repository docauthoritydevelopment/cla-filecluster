package com.cla.kvdb;

import com.cla.common.domain.dto.crawler.FileEntity;
import com.cla.eventbus.EventBus;
import com.cla.eventbus.events.Event;
import org.apache.commons.io.FileUtils;
import org.junit.*;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

import static org.junit.Assert.*;

/**
 * Test Key-Value DB
 */
public class KvdbTest {

    private EventBus eventBus;

    private KeyValueFileEntityDao dao;

    @Before
    public void init() {
//        String tempDir = System.getProperty("java.io.tmpdir") + "kvdbtest";
        String tempDir = "./temp/kvdbtest/test";
        try {
            File f = new File(tempDir);
            if (f.isDirectory()) {
                FileUtils.cleanDirectory(f);
            }
        } catch (IOException e) {
            throw new RuntimeException("Can not clean directory " + tempDir, e);
        }
        KeyValueDaoConfig config = KeyValueDaoConfig.create()
                .setBasePath(tempDir)
                .setDuplicateEntries(false);

        // Just a dummy impl. Better add gradle dep to mockito
        eventBus = new EventBus() {
            @Override
            public void subscribe(String topic, Consumer<Event> consumer) {}

            @Override
            public void broadcast(String topic, Event event) {}

            @Override
            public void unSubscribe(String topic, Consumer<Event> consumer) {}
        };

        dao = KeyValueFileEntityDao.getInstance(config, eventBus);
    }

    @After
    public void tearDown() throws IOException {
        dao.dropDb();
        dao.close();
    }

    @Test
    public void basicTest() {
        long time = 1234567890;
        String path = "c:\\this\\is\\my\\long\\path";
        String sig = "12345678";

        dao.executeInTransaction(null, "MyTable", store -> {
            FileEntity fe = new FileEntity();
            fe.setFile(false);
            fe.setFolder(true);
            fe.setAclSignature(sig);
            fe.setSize(12000L);
            fe.setFileId(666L);
            fe.setContentMdId(12L);
            fe.setLastScanned(1502695999734L);
            fe.setLastModified(time);
            fe.setDeleted(true);
            store.put(path, fe);

            FileEntity retFe = store.get(path);
            assertNotNull(retFe);
            return true;
        });

        FileEntity file = dao.executeInTransaction(null, "MyTable", store -> store.get(path));

        assertNotNull(file);
        assertEquals(time, file.getLastModified().longValue());
        assertEquals(sig, file.getAclSignature());
        assertEquals(12000L, file.getSize().longValue());
        assertEquals(666L, file.getFileId().longValue());
        assertEquals(12L, file.getContentMdId().longValue());
        assertEquals(1502695999734L, file.getLastScanned().longValue());
        assertTrue(file.isFolder());
        assertTrue(file.isDeleted());
    }


    @Test
    public void testMultiThreadWrite() throws InterruptedException {
        ExecutorService executorService = Executors.newFixedThreadPool(15);
        long time = 1234567890;
        String sig = "12345678";
        int size = 120;
        StringBuilder pathBuilder = new StringBuilder();
        for (int i = 0; i < 120; i++) {
            String path = pathBuilder.append(i).append("/").toString();
            executorService.execute(() ->
                    dao.executeInTransaction(null, "MyTable", store -> {
                        FileEntity fe = new FileEntity();
                        fe.setFile(false);
                        fe.setFolder(true);
                        fe.setAclSignature(sig);
                        fe.setSize(12000L);
                        fe.setFileId(666L);
                        fe.setContentMdId(12L);
                        fe.setLastScanned(1502695999734L);
                        fe.setLastModified(time);
                        fe.setDeleted(true);
                        store.put(path, fe);

                        FileEntity retFe = store.get(path);
                        assertNotNull(retFe);
                        return true;
                    }));

        }

        executorService.shutdown();
        boolean terminated = executorService.awaitTermination(1, TimeUnit.MINUTES);
        if (!terminated) {
            executorService.shutdownNow();
            fail("Threads did not finish job on exodus in a timely manner (more than 1 minute)");
        }
        long storeCount = dao.executeInTransaction(null, "MyTable", FileEntityStore::count);
        assertEquals(size, storeCount);
    }
}
