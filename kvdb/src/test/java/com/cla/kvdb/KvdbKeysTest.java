package com.cla.kvdb;

import com.cla.eventbus.EventBus;
import com.cla.eventbus.events.Event;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.function.Consumer;

/**
 * Created by: yael
 * Created on: 12/12/2018
 */
@Ignore
public class KvdbKeysTest {

    private EventBus eventBus;

    private KeyValueFileEntityDao dao;

    @Before
    public void init() {
        // Just a dummy impl. Better add gradle dep to mockito
        eventBus = new EventBus() {
            @Override
            public void subscribe(String topic, Consumer<Event> consumer) {}

            @Override
            public void broadcast(String topic, Event event) {}

            @Override
            public void unSubscribe(String topic, Consumer<Event> consumer) {}
        };
    }

    @After
    public void tearDown() throws IOException {
        dao.close();
    }

    @Test
    public void listAllKeys() {
        KeyValueDaoConfig config = KeyValueDaoConfig.create()
                .setBasePath("D:\\dev\\cla-filecluster\\da-media-proc\\data\\kvdb")
                .setDuplicateEntries(false);

        dao = KeyValueFileEntityDao.getInstance(config, eventBus);

        List<String> allStoreNames = dao.getAllStoreNames();
        System.out.println("Existing stores:");
        System.out.println("---------------------------------------------");
        allStoreNames.forEach(storeName -> System.out.println("Store: " + storeName));
        System.out.println("---------------------------------------------");

        allStoreNames.forEach(storeName -> {
            System.out.println("---------------" + storeName + "------------------");
            dao.executeInReadonlyTransaction(storeName, store -> {
                store.forEach((str, fileEntity) -> System.out.println(str + " -> " + fileEntity.toString()));
                return true;
            });
            System.out.println("--------------------------------------------------");
        });
    }
}
