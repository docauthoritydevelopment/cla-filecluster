cla-filecluster
===============

How to start the server on a dev machine:

1. Copy 3rd-party dir from here:
  Google Drive\share\engineering\environment\3rd-party
  To your cla-filecluster workspace (into a 3rd-party folder)
  Share link: https://drive.google.com/drive/folders/0B2Ob_F7djXxjZktaZ0taNWVQWmc?usp=sharing
2. Unzip the following files:
    - apache-activemq-5.14.5-bin.zip (should have 'apache-activemq-5.14.5' under <proj-dir>\3rd-party)
    - zookeeper-3.4.9.zip  (should have 'zookeeper-3.4.9' under <proj-dir>\3rd-party)
    - solr-7.7.1.zip (should have 'solr-7.7.1' under <proj-dir> ! different than above)
    ** Note that the ZIP files contain redundant top-level folders that should not exist in the dev environment

  You should now have a 3rd-party/activemq-5.14.5 , 3rd-party/zookeeper-3.4.9 folder
  and a 3rd-party/kafka folder (not for long) 
   

3. Run the PrepareStartSolr task (once per reboot of the machine)
    'gradlew PrepareStartSolr' or at the InteliJ Gradle project view click 'cla-filecluster/other/PrepareStartSolr'

4. start activeMq
   Run the batch script dev-scripts\activemq_console.bat
   Keep it running in the background
   IntelliJ lets us right-click batch files and execute them (when you have the batch plugin).
   
5. If Resetting the environment, or running for the first time: reset the schema
   dev-scripts\reset-schema-and-baseline.bat do-reset

6. Start the media processor
  dev-scripts\run_da_media-proc.bat
  Keep it running in the background

7. Start the main service (easy to use bootRun task for that)

   When the following line appears, the server is up and running:  
   24 15:42:51,512  WARN [main] Application:46 - Active profiles : [solr, webapp, win]

8. If you need UI:
   dev-scripts\doc-authority-ui-start.bat

    * For initial UI environment build see ???

Solr Cloud testing (embedded ZK)
--------------------------------

To test using solr cloud on a dev machine:

1. Run the gradle task PreateStartSolrCloud
    'gradlew PrepareStartSolrCloud' or at the InteliJ Gradle project view click 'cla-filecluster/other/PrepareStartSolrCloud'

2. Reload Solr cloud configuration:
    'dev-scripts\solr_cloud_config auto-config-embedded-zk 2'

3. Change application-test.properties, application.properties to use the solr cloud address.
    spring.data.solr.host=localhost:8983
    spring.data.solr.type=cloud

   
   
Solr Cloud testing (external ZK)
--------------------------------

To test using solr cloud on a dev machine:

1. Run the gradle task PreateStartSolrCloud
    'gradlew PrepareStartSolrCloud' or at the InteliJ Gradle project view click 'cla-filecluster/other/PrepareStartSolrCloud'

2. Stop solr:
  dev-scripts\solr_stop.bat

3. Start the zookeeper
  dev-scripts\zookeeper-console.bat
  Keep it running in the background

4. Start solr cloud with external ZK:
  solr_cloud_start_zk.bat

5. Reload Solr cloud configuration:
    'dev-scripts\solr_cloud_config auto-config-zk 2'

6. Change application-test.properties, application.properties to use the solr cloud address.
    spring.data.solr.host=localhost:2181
    spring.data.solr.type=cloud




