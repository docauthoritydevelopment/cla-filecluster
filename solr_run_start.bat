@ECHO OFF
set DIRNAME=%~dp0
set CLA_HOME=%DIRNAME%\solr-7.7.2
set SOLR_JAVA_MEM=-Xms2g -Xmx2g

if "%SOLR_PORT%" == "" SET SOLR_PORT=8983

if "%SOLR_HOME%" == "" set "SOLR_HOME=%DIRNAME%\solr"

if not "%CUSTOM_SOLR_DATA_DIR%" == "" goto dataDirDef

if not exist "D:\Data\SolrData7" goto noData1
echo "CUSTOM SOLR DATA DIR FOUND at D:\Data\SolrData7"
set CUSTOM_SOLR_DATA_DIR=D:\Data\SolrData7
goto dataDirDef

:noData1
if not exist "%CLA_HOME%\data" goto noData2
echo "CUSTOM SOLR DATA DIR FOUND NEAR SOLR"
set CUSTOM_SOLR_DATA_DIR=%CLA_HOME%\data
goto dataDirDef

:noData2
if not exist "D:\Data\SolrData2" goto noData3
ECHO "CUSTOM SOLR DATA DIR FOUND at D:\Data\SolrData2"
set CUSTOM_SOLR_DATA_DIR=D:\Data\SolrData2
goto dataDirDef

:noData3
set CUSTOM_SOLR_DATA_DIR=%SOLR_HOME%\data
goto dataDirDef

:dataDirDef
REM echo %CUSTOM_SOLR_DATA_DIR%

if NOT '%1' == '-clean' goto doStart

ECHO    ...%SOLR_HOME%\lib
RMDIR /S /Q %SOLR_HOME%\lib

if '%2' == 'all' (
  ECHO.
  ECHO Cleaning all Solr collections at %CUSTOM_SOLR_DATA_DIR%
  FOR /F "tokens=* USEBACKQ" %%F IN (`dir /b %CUSTOM_SOLR_DATA_DIR%\*`) DO (
    IF exist "%CUSTOM_SOLR_DATA_DIR%\%%F\index" (
        ECHO    ...%CUSTOM_SOLR_DATA_DIR%\%%F
        RMDIR /S /Q %CUSTOM_SOLR_DATA_DIR%\%%F
    ) ELSE ECHO No index found at: "%CUSTOM_SOLR_DATA_DIR%\%%F"
  )
) ELSE if '%2' == 'test' (
  ECHO.
  ECHO Cleaning test Solr collections at %CUSTOM_SOLR_DATA_DIR%
  FOR /F "tokens=* USEBACKQ" %%F IN (`dir /b %CUSTOM_SOLR_DATA_DIR%\test*`) DO (
    IF exist "%CUSTOM_SOLR_DATA_DIR%\%%F\index" (
        ECHO    ...%CUSTOM_SOLR_DATA_DIR%\%%F
        RMDIR /S /Q %CUSTOM_SOLR_DATA_DIR%\%%F
    ) ELSE ECHO No index found at: "%CUSTOM_SOLR_DATA_DIR%\%%F"
  )
) ELSE goto usage
SHIFT
SHIFT
ECHO Cleaning done
ECHO.
GOTO finally

:doStart
REM ###########################
REM Section added to allow Java 11 support in parallel to other branches using Java 8.
REM Itay R. May 2019, Solr7 migration
set JAVA11_HOME=%ProgramW6432%\Java\jdk-11.0.3
IF NOT EXIST "%JAVA11_HOME%" (
   echo Could not find Java 11 home at "%JAVA11_HOME%"
   exit /b 1
)
Echo Using Java 11!
set SOLR_JAVA_HOME=%JAVA11_HOME%
REM ## End of section added for Java 11 support
REM ###########################
rem set LOG4J_CONFIG=%SOLR_HOME%\conf\log4j.properties
ECHO.
ECHO Starting solr with data-dir at %CUSTOM_SOLR_DATA_DIR%
ECHO %SOLR_HOME%
call "%CLA_HOME%\bin\solr.cmd" start -p %SOLR_PORT% -Denable.runtime.lib=true -Dsolr.data.dir="%CUSTOM_SOLR_DATA_DIR%" -a "-solr.log.dir=%SOLR_LOGS_DIR%" -a "-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=1%SOLR_PORT%" %1 %2 %3
goto finally

:usage
ECHO Usage:
ECHO        %0 [-clean all^|test]
GOTO finally

:finally
