package com.cla.damanage.service.managers;

import com.cla.common.domain.dto.messages.control.DriveDto;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.util.ReflectionTestUtils;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.Silent.class)
@SpringBootTest(classes = DriveSizeProbe.class)
@ActiveProfiles("test")
public class DriveSizeProbeTest {

	@Mock
	private ManagementAgentControlService agentControlService;

	@InjectMocks
	DriveSizeProbe driveSizeProbe;

	final String[] foldersStr = "..,..,invalid:\\path\\to\\nowhere".split(",");

	@Before
	public void initMocks(){
		MockitoAnnotations.initMocks(this);
		Mockito.doNothing().when(agentControlService).reportDrivesStatus(Mockito.any());
		ReflectionTestUtils.setField(driveSizeProbe, "foldersStr", foldersStr);
		driveSizeProbe.init();
	}


	@Test
	public void measureFoldersSize() {
		Map<String, Integer> measureFoldersSize = driveSizeProbe.measureFoldersSize();
		assertTrue(measureFoldersSize.size()>0);
	}

	@Test
	public void getDriveCapacityMetrics() {
		List<DriveDto.DriveCapacityMetrics> driveCapacityMetrics = driveSizeProbe.getDriveCapacityMetrics();
		assertTrue(driveCapacityMetrics.size()>0);
	}

	@Test
	public void gatherDriveAndFolderMetrics() {
		DriveDto driveDto = driveSizeProbe.gatherDriveAndFolderMetrics();
		assertTrue(driveDto.getDriveCapacities().size()>0);
		assertTrue(driveDto.getFolderSizes().size()>0);

		String hostAddress = "0.0.0.0";
		try {
			hostAddress = InetAddress.getLocalHost().getHostAddress();
		} catch (UnknownHostException e) {
		}
		assertEquals(driveDto.getMachineId(), hostAddress);
	}

}