package com.cla.damanage.base;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

/**
 * Created by uri on 07-Mar-17.
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@TestPropertySource(properties = "server.port=19088")
public abstract class BaseIntegrationTests extends BaseTests{
}
