package com.cla.damanage.base;

import com.cla.damanage.DaManageAgent;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * Created by uri on 07-Mar-17.
 */
@Configuration
@Import(DaManageAgent.class)
public class TestsConfiguration {
}
