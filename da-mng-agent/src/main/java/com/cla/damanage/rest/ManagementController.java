package com.cla.damanage.rest;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.joran.JoranConfigurator;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.Appender;
import ch.qos.logback.core.joran.spi.JoranException;
import com.cla.common.utils.LogExportUtils;
import com.cla.damanage.service.managers.ManagementAgentControlService;
import com.cla.filecluster.util.LocalFileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.zip.ZipOutputStream;

/**
 * Controller for the Media Processor administration
 * Created by vladi on 1/31/2017.
 */
@RestController
@RequestMapping(value = "/management")
public class ManagementController {

    private Logger logger = LoggerFactory.getLogger(ManagementController.class);

    @Value("${logging.config:./config/logback-spring.xml}")
    private String logbackConfigPath;

    @Value("${log.solr.files-to-export:}")
    private String[] solrExportFiles;

    @Value("${log.mp.files-to-export:}")
    private String[] mpExportFiles;

    @Value("${log.fc.files-to-export:}")
    private String[] fcExportFiles;

    @Value("${log.top-index-to-export:1}")
    private int exportFilesTopIndex;

    @Value("${log.max-days-to-export:2}")
    private int exportFilesTopDays;

    @Autowired
    private ManagementAgentControlService managementAgentControlService;

    @RequestMapping(value = "/logs/fetch", method = RequestMethod.GET)
    public void fetchLogs(HttpServletResponse response) {
        logger.info("Fetch log files from FileCluster");
        flushLogs();
        try {
            zipFilesToResponse(response, "DA_logs", "./logs", ".*\\.log(\\.[1-5])?");
            logger.debug("log files fetched");
        } catch (IOException ex) {
            logger.info("Error writing log files to output stream.", ex);
            throw new RuntimeException("IOError writing log files to output stream");
        }
    }

    @RequestMapping(value = "/logs/fetch/active/components", method = RequestMethod.GET)
    public void fetchLogsActiveComponents(HttpServletResponse response) {
        List<Path> files = new ArrayList<>();
        List<String> componentNames = new ArrayList<>();

        try {
            if (managementAgentControlService.isFcCheckEnabled()) {
                files.addAll(LogExportUtils.getLogFilesToExport(fcExportFiles, exportFilesTopIndex, exportFilesTopDays));
                componentNames.add("filecluster_logs");
            }
            if (managementAgentControlService.isMpCheckEnabled()) {
                files.addAll(LogExportUtils.getLogFilesToExport(mpExportFiles, exportFilesTopIndex, exportFilesTopDays));
                componentNames.add("media-processor_logs");
            }
            if (managementAgentControlService.isSolrCheckEnabled()) {
                files.addAll(LogExportUtils.getLogFilesToExport(solrExportFiles, exportFilesTopIndex, exportFilesTopDays));
                componentNames.add("solr_logs");
            }

            String zipName = componentNames.size() == 1 ? componentNames.get(0) : "components_logs";

            zipFilesToResponse(response, zipName, files);

        } catch (Exception e) {
            logger.info("Error writing log files to output stream.", e);
            throw new RuntimeException("IOError writing log files to output stream");
        }
    }

    @RequestMapping(value = "/logs/reloadConfig", method = RequestMethod.GET)
    public String reloadLogsConfig(HttpServletResponse response) {
        LoggerContext context = (LoggerContext) LoggerFactory.getILoggerFactory();
        context.reset();
        JoranConfigurator configurator = new JoranConfigurator();
        configurator.setContext(context);
        try {
            configurator.doConfigure(logbackConfigPath);
        } catch (JoranException e) {
            logger.info("Error reloading logs config", e);
            throw new RuntimeException("Error reloading log files config");
        }
        logger.info("Logs config reloaded according to {}", logbackConfigPath);
        return "Logs config reloaded!";
    }

    @RequestMapping(value = "/logs/flush", method = RequestMethod.GET)
    public void flushLogs() {
        if (!flushLogback()) {
            for (int i = 0; i < 65; i++) {
                logger.debug("*************************************************************************************************************");
            }
        }
    }

    private boolean flushLogback() {
        boolean res = false;
        if (logger instanceof ch.qos.logback.classic.Logger) {
            res = true;
            ch.qos.logback.classic.Logger lbLogger = (ch.qos.logback.classic.Logger) logger;
            Map<String, Integer> ftMap = new HashMap<>();
            Iterator<Appender<ILoggingEvent>> appendersIterator = lbLogger.iteratorForAppenders();
            while (appendersIterator.hasNext()) {
                Appender<ILoggingEvent> appender = appendersIterator.next();
                if (appender instanceof ch.qos.logback.core.AsyncAppenderBase) {
                    ch.qos.logback.core.AsyncAppenderBase<ILoggingEvent> lbApp = (ch.qos.logback.core.AsyncAppenderBase<ILoggingEvent>) appender;
                    ftMap.put(lbApp.getName(), lbApp.getMaxFlushTime());
                    lbApp.setMaxFlushTime(0);
                } else {
                    logger.info("Found appender {} of class {}", appender.getName(), appender.getClass());
                }
            }
            lbLogger.info("*****************************************************************************************");
            lbLogger.info("****************************** Flushing logger ************************ ({})",
                    ftMap.entrySet().stream()
                            .map(e -> e.getKey() + ":" + e.getValue())
                            .collect(Collectors.joining(",", "{", "}")));
            lbLogger.info("*****************************************************************************************");
            appendersIterator = lbLogger.iteratorForAppenders();
            while (appendersIterator.hasNext()) {
                Appender<ILoggingEvent> appender = appendersIterator.next();
                if (appender instanceof ch.qos.logback.core.AsyncAppenderBase) {
                    ch.qos.logback.core.AsyncAppenderBase<ILoggingEvent> lbApp = (ch.qos.logback.core.AsyncAppenderBase<ILoggingEvent>) appender;
                    Integer mft = ftMap.get(lbApp.getName());
                    if (mft != null) {
                        logger.debug("Restore appender {} to {}", lbApp.getName(), mft);
                        lbApp.setMaxFlushTime(mft);
                    } else {
                        logger.debug("Could not Restore appender {} MaxFlushTime", lbApp.getName());
                    }
                }
            }
        }
        return res;
    }

    private void zipFilesToResponse(HttpServletResponse response, String zipFilePrefix, String path, String fileSpecRegex) throws IOException {
        final Pattern logFilePattern = Pattern.compile(fileSpecRegex);
        List<Path> files = new ArrayList<>();
        try (Stream<Path> logFileList = Files.list(Paths.get(path))) {
            logFileList.filter(p -> logFilePattern.matcher(p.getFileName().toString()).matches())
                    .forEach(f -> {
                        files.add(f);
                    });
        }
        zipFilesToResponse(response, zipFilePrefix, files);
    }

    private void zipFilesToResponse(HttpServletResponse response, String zipFilePrefix, List<Path> files) throws IOException {
        // set headers for the response
        response.setContentType("application/zip");
        final Date curDate = new Date();
        final SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd_hhmmss");
        final String suffix = format.format(curDate);
        final String headerKey = "Content-Disposition";
        final String headerValue = String.format("attachment; filename=\"%s.%s.zip\"", zipFilePrefix, suffix);
        response.setHeader(headerKey, headerValue);

        ZipOutputStream zipOutputStream = new ZipOutputStream(new BufferedOutputStream(response.getOutputStream()));
        files.forEach(f -> {
                        try {
                            LocalFileUtils.addFileToZip(f, zipOutputStream);
                        } catch (IOException e) {
                            logger.error("Failed to add log file " + f + " to zip stream", e);
                        }
                    });

        logger.debug("flush buffers");
        response.flushBuffer();
        logger.debug("close zip stream");
        zipOutputStream.close();
    }
}
