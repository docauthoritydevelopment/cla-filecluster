package com.cla.damanage.appserver;

import com.cla.common.domain.dto.jobmanager.JobState;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * In-memory container for job states received from the appserver
 * Created by vladi on 6/7/2017.
 */
@Service
public class JobStateMonitor {

    private Cache<Long,JobState> jobStates = CacheBuilder.newBuilder()
            .maximumSize(100)
            .expireAfterWrite(5, TimeUnit.MINUTES)
            .build();

    public void updateJobStates(Map<Long,JobState> states){
        jobStates.putAll(states);
    }

    public boolean isJobActive(Long id) {
        if (id == null){
            return true;
        }

        JobState jobState = jobStates.getIfPresent(id);
        // we assume that the job is active if it is not known
        return jobState == null || jobState.isActive();
    }

}
