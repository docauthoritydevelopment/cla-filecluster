package com.cla.damanage.appserver.config;

import com.cla.filecluster.util.executors.BlockingExecutor;
import com.cla.filecluster.util.executors.ClientThreadBlockingExecutor;
import com.cla.filecluster.util.executors.ThreadPoolBlockingExecutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

import javax.annotation.PostConstruct;
import java.util.Arrays;

@Configuration
public class DaManageAppConfig {
	private static final Logger logger = LoggerFactory.getLogger(DaManageAppConfig.class);
	
	@Value("${utilBlockingExecutor.poolSize:10}")
	private int utilBlockingExecutorPoolSize;
	
	@Value("${utilBlockingExecutor.QueueSize:15}")
	private int utilBlockingExecutorQueueSize;


	@Value("${ingestBlockingExecutor.poolSize:10}")
	private int ingestBlockingExecutorPoolSize;
	
	@Value("${ingestBlockingExecutor.QueueSize:15}")
	private int ingestBlockingExecutorQueueSize;

	@Value("${analyzeBlockingExecutor.poolSize:10}")
	private int analyzeBlockingExecutorPoolSize;
	
	@Value("${analyzeBlockingExecutor.queueSize:15}")
	private int analyzeBlockingExecutorQueueSize;

	@Value("${ingest.others.tikaConfig:}")
	private String tikeConfigFilename;

	@Value("${threadPool.idleTimeBeforeShutdown:1}")
	private long idleTimeBeforeShutdown;

	@Value("${maxStringLengthToExtract:50000}")
	private int maxStringLength;
	
	@Value("${pv.dbOperationsRetryCount:5}")
	private int dbOperationsRetryCount;
	
	@Value("${pv.dbOperationsSleepSec:2}")
	private int dbOperationsSleepSec;
	
	@Value("${useThreadPoolExecutors:true}")
	private boolean useThreadPoolExecutors;

    @Value("${alternative.name.override:false}")
    private boolean overrideWithAlternativeName;

	@Value("${app.displayString:DocAuthority Management Agent}")
	private String appDisplayString;


    @Autowired
	private ApplicationContext context;

    @PostConstruct
	private void init(){
		if(!useThreadPoolExecutors) {
			logger.warn("==============================================================================");
			logger.warn("| THE APPLICATION IS SINGLE THREADED. THIS SHOULD BE USED ONLY FOR DEBUG !!! |");
			logger.warn("==============================================================================");
		}
		logger.warn("{} Loading profiles : {}",appDisplayString, Arrays.asList(context.getEnvironment().getActiveProfiles()));

	}

	@Bean
    public BlockingExecutor ingestBlockingExecutor(){
    	if(useThreadPoolExecutors) {
			return new ThreadPoolBlockingExecutor("ingest", ingestBlockingExecutorPoolSize, ingestBlockingExecutorQueueSize, idleTimeBeforeShutdown, true);
		}
    	return new ClientThreadBlockingExecutor();
    }
    
//    @Bean
//    public BlockingExecutor analyzeBlockingExecutor(){
//    	if(useThreadPoolExecutors) {
//			logger.info("Create Analyze Blocking Executor");
//            return new ThreadPoolBlockingExecutor("analyze", analyzeBlockingExecutorPoolSize, analyzeBlockingExecutorQueueSize,idleTimeBeforeShutdown);
//		}
//    	return new ClientThreadBlockingExecutor();
//    }
    
    @Bean
    public BlockingExecutor utilBlockingExecutor(){
    	if(useThreadPoolExecutors) {
			return new ThreadPoolBlockingExecutor("util", utilBlockingExecutorPoolSize, utilBlockingExecutorQueueSize,idleTimeBeforeShutdown,true);
		}
    	return new ClientThreadBlockingExecutor();
    }
    
//    @Bean
//    public BlockingExecutor extractionBlockingExecutor(){
//		if(useThreadPoolExecutors) {
//			return new ThreadPoolBlockingExecutor("extraction", utilBlockingExecutorPoolSize, utilBlockingExecutorQueueSize,idleTimeBeforeShutdown);
//		}
//    	return new ClientThreadBlockingExecutor();
//    }

    @Bean
    public TaskScheduler defaultScheduler() {
        ThreadPoolTaskScheduler taskScheduler = new ThreadPoolTaskScheduler();
        taskScheduler.setDaemon(true);
        taskScheduler.setThreadNamePrefix("da-scheduler");
        taskScheduler.setPoolSize(10);
        return taskScheduler;
    }

////////////////////////////////////////////////////////////////

}