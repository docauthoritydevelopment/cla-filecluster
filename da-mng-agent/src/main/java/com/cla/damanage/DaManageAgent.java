package com.cla.damanage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.solr.SolrAutoConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.util.Arrays;

/**
 * Main application of DA Management Agent process
 * Created by vladi on 1/31/2017.
 */
@Configuration
@ComponentScan(basePackages = {"com.cla"})
@EnableAutoConfiguration(exclude = {SolrAutoConfiguration.class})
//@EnableKafka
//@EnableJms
@EnableScheduling
public class DaManageAgent {

    private static Logger logger = LoggerFactory.getLogger(DaManageAgent.class);

    public static void main(final String[] args) {
        @SuppressWarnings("unused") ApplicationContext context = SpringApplication.run(DaManageAgent.class, args);
        System.out.println("DaManagementAgent started.");
        logger.info("***************************************************************************");
        logger.info("Active profiles : {}", Arrays.asList(context.getEnvironment().getActiveProfiles()));
    }

}
