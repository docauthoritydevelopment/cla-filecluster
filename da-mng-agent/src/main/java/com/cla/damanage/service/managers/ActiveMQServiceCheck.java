package com.cla.damanage.service.managers;

import com.cla.damanage.service.managers.domain.QueueNameResponse;
import com.cla.damanage.service.managers.domain.QueueStatusResponse;
import com.cla.filecluster.utils.EncUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * service to connect to active mq and check its state
 *
 * Created by: yael
 * Created on: 7/23/2018
 */
@Service
public class ActiveMQServiceCheck {

    private final static Logger logger = LoggerFactory.getLogger(ActiveMQServiceCheck.class);

    @Value("${activemq.monitor-queue-status-url:read/org.apache.activemq:type=Broker,brokerName=localhost,destinationType=Queue,destinationName=}")
    private String activemqStatusUrl;

    @Value("${activemq.monitor-queue-url-prefix:http://localhost:8161/api/jolokia/}")
    private String activemqUrlPrefix;

    @Value("${activemq.monitor-queue-names-url:search/*:destinationType=Queue,*}")
    private String activemqQueueUrl;

    @Value("${activemq.monitor-queue-user:admin}")
    private String activemqUser;

    @Value("${activemq.monitor-queue-pass:admin}")
    private String activemqPass;

    @Value("${use-enc-pass:false}")
    private boolean useEncPassword;

    @Value("${activemq.monitor-queue-attributes:/QueueSize}")
    private String activemqAttributes;

    @Value("${activemq.monitor-queue-names-ignore:ActiveMQ.DLQ}")
    private String[] activemqQueueNamesToIgnore;

    @Value("${activemq.max-queue-size:10000}")
    public int maxQueueSize;

    @Autowired
    private ManagementAgentStateService managementAgentStateService;

    @Autowired
    private ManagementAgentControlService managementAgentControlService;

    private ObjectMapper mapper;

    private AtomicInteger fatalCount = new AtomicInteger();
    private boolean amqMarkedForRestart = false;

    @PostConstruct
    public void init() {
        mapper = new ObjectMapper();
        mapper.configure(com.fasterxml.jackson.core.JsonParser.Feature.ALLOW_SINGLE_QUOTES, true);
    }

    private List<String> getQueueNames() {
        List<String> queueNamesToIgnore = Arrays.asList(activemqQueueNamesToIgnore);
        List<String> queueNames = new ArrayList<>();
        try {
            URL url = new URL(activemqUrlPrefix+activemqQueueUrl);
            URLConnection uc = url.openConnection();
            String userpass = activemqUser + ":" + (useEncPassword ? EncUtils.decrypt(activemqPass) : activemqPass);
            String basicAuth = "Basic " + new String(Base64.getEncoder().encode(userpass.getBytes()));
            uc.setRequestProperty("Authorization", basicAuth);
            BufferedReader in = new BufferedReader(new InputStreamReader(
                    uc.getInputStream()));
            String inputLine;
            StringBuilder result = new StringBuilder();
            while ((inputLine = in.readLine()) != null) {
                result.append(inputLine);
            }
            in.close();
            QueueNameResponse resp = mapper.readValue(result.toString(), QueueNameResponse.class);
            if (resp != null && resp.getValue() != null && resp.getValue().length > 0) {
                for (String value : resp.getValue()) {
                    int start = value.indexOf("destinationName=") + "destinationName=".length();
                    int end = value.indexOf(",",start);
                    if (start > 0) {
                        String name = value.substring(start,end);
                        if (!queueNamesToIgnore.contains(name)) {
                            queueNames.add(name);
                        }
                    }
                }
            }
        } catch (Exception e) {
            logger.error("failed getting active MQ queue names", e);
        }
        return queueNames;
    }

    public boolean shouldRestartQueueTooBig() {
        try {
            List<String> queueNames = getQueueNames();
            for (String name : queueNames) {
                URL url = new URL(activemqUrlPrefix + activemqStatusUrl + name + activemqAttributes);
                URLConnection uc = url.openConnection();
                String userpass = activemqUser + ":" + activemqPass;
                String basicAuth = "Basic " + new String(Base64.getEncoder().encode(userpass.getBytes()));
                uc.setRequestProperty("Authorization", basicAuth);
                BufferedReader in = new BufferedReader(new InputStreamReader(
                        uc.getInputStream()));
                String inputLine;
                StringBuilder result = new StringBuilder();
                while ((inputLine = in.readLine()) != null) {
                    result.append(inputLine);
                }
                in.close();
                try {
                    QueueStatusResponse resp = mapper.readValue(result.toString(), QueueStatusResponse.class);
                    Integer queueSize = (Integer)resp.getValue();
                    if (queueSize > maxQueueSize) {
                        return true;
                    }
                } catch (Exception e) {
                    logger.error("failed parsing active MQ queue sizes for queue {} result was {}", name, result.toString(), e);
                }
            }
        } catch (Exception e) {
            logger.error("failed getting active MQ queue sizes", e);
            return true;
        }
        return false;
    }

    public boolean markAmqForRestart() {
        boolean last = amqMarkedForRestart;
        amqMarkedForRestart = true;
        return last;
    }

    public boolean clearAmqForRestart() {
        boolean last = amqMarkedForRestart;
        amqMarkedForRestart = false;
        return last;
    }

    public boolean isAmqMarkedForRestart() {
        return amqMarkedForRestart;
    }

    public void incFatalCount() {
        fatalCount.incrementAndGet();
    }

}
