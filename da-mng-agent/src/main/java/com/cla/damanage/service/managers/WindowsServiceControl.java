package com.cla.damanage.service.managers;

import com.google.common.base.Strings;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.function.Supplier;

public class WindowsServiceControl {

    private static final Logger logger = LoggerFactory.getLogger(WindowsServiceControl.class);
    public static final String FAILED_INDICATION = "FAILED";

    private final ServiceControlConfig scConfig;

    public WindowsServiceControl(ServiceControlConfig scConfig) {
        this.scConfig = scConfig;
    }

    public boolean isServiceStarted() {
        return statusCheck(scConfig.queryMediaProcServiceStartedScript);
    }

    private boolean statusCheck(String[] statusQueryCmd) {
        String line = null;
        Process process = null;
        try {
            process = Runtime.getRuntime().exec(statusQueryCmd);
            BufferedReader br = new BufferedReader(new InputStreamReader(process.getInputStream()));
            line = br.readLine();
            boolean foundStatus = !Strings.isNullOrEmpty(line) && Integer.valueOf(line) == 1;
            logger.trace("***** Checking service {} for status {}, found: {}", scConfig.serviceName, statusQueryCmd[8], foundStatus);
            return foundStatus;
        } catch (NumberFormatException e) {
            logger.error(String.format("Suppose to receive a 0/1 status, but got %s, treating it as if service is down", line), e);
            return false;
        } catch (IOException e) {
            logger.error(String.format("Failed to check service %s status, reason: ", scConfig.serviceName, e.getMessage()));
            throw new RuntimeException(e);
        } finally {
            if (process != null) {
                process.destroy();
            }
        }
    }

    public boolean stopService() {
        return executeServiceLifeCycleCmd(scConfig.stopMediaProcServiceScript, this::isServiceStopped, scConfig.stopTimeout, "stop");
    }

    private boolean isServiceStopped() {
        return statusCheck(scConfig.queryMediaProcServiceStoppedScript);
    }

    private boolean executeServiceLifeCycleCmd(String[] serviceActionScript, Supplier<Boolean> serviceStatusCheckAction, long serviceActionTimeout, String serviceAction) {
        Process process = null;
        try {
            logger.debug("Performing action \"{}\" for service {}", serviceAction, scConfig.serviceName);
            process = Runtime.getRuntime().exec(serviceActionScript);
            BufferedReader br = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line = br.readLine();
            if (line.contains(FAILED_INDICATION)) {
                StringBuilder reason = new StringBuilder(line.substring(line.indexOf(FAILED_INDICATION)));
                line = br.readLine();
                while (line != null) {
                    reason.append(line).append('\n');
                    line = br.readLine();
                }
                logger.error("Failed to execute action \"{}\" for service {}, reason:\n{}", serviceAction, scConfig.serviceName, reason.toString());
                return false;
            }
        } catch (IOException e) {
            logger.error(String.format("Failed to execute action \"%s\" for service %s, reason:\n%s", serviceAction, scConfig.serviceName, e.getMessage()));
            throw new RuntimeException(e);
        } finally {
            if (process != null) {
                process.destroy();
            }
        }

        int seconds = 0;
        while (seconds < serviceActionTimeout) {
            if (serviceStatusCheckAction.get()) {
                logger.debug("Action \"{}\" succeeded for service {}, after {} seconds", serviceAction, scConfig.serviceName, seconds);
                return true;
            }
            seconds++;
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
            }
        }

        logger.error(String.format("Failed to execute action \"%s\" for service %s after %d seconds", serviceAction, scConfig.serviceName, serviceActionTimeout));
        return false;
    }

    public boolean startService() {
        return executeServiceLifeCycleCmd(scConfig.startMediaProcServiceScript, this::isServiceStarted, scConfig.startTimeout, "start");
    }

    public boolean killService() {
        boolean processKilled = false;
        Process proc = null;
        try {
            proc = Runtime.getRuntime().exec(scConfig.killMediaProcessorScript);
            String reason = String.join("\n", IOUtils.readLines(proc.getInputStream()).toArray(new String[0]));
            System.out.println(reason);
            proc.waitFor();
            processKilled = proc.exitValue() == 0;
            if (!processKilled) {

                logger.error(String.format("Failed to kill process %s, reason: %s", scConfig.serviceName, reason));
            } else {
                logger.debug("Killed process {} successfully", scConfig.serviceName);
            }
        } catch (IOException e) {
            logger.error(String.format("Failed to run exec to kill process %s, reason: %s", scConfig.serviceName, e.getMessage()));
        } catch (InterruptedException e) {
            logger.error(String.format("Interrupted will waiting for kill exec on service %s to finish , reason: %s", scConfig.serviceName, e.getMessage()));
        } finally {
            if (proc != null) {
                proc.destroy();
            }
        }
        return processKilled;
    }

    public static class ServiceControlConfig {

        private final String serviceName;
        private final String serviceProcess;
        private final long startTimeout;
        private final long stopTimeout;

        //to check whether service is running or not
        private String[] queryMediaProcServiceStartedScript;
        //to check whether service is running or not
        private String[] queryMediaProcServiceStoppedScript;
        //to start service
        private String[] startMediaProcServiceScript;
        //to stop service
        private String[] stopMediaProcServiceScript;
        //to kill process
        private String[] killMediaProcessorScript;

        public ServiceControlConfig(String serviceName, long startTimeout, long stopTimeout) {
            this.serviceName = serviceName;
            this.serviceProcess = serviceName + ".exe";
            this.startTimeout = startTimeout;
            this.stopTimeout = stopTimeout;
            queryMediaProcServiceStartedScript = new String[]{"cmd.exe", "/c", "sc", "query", serviceName, "|", "find", "/C", "\"RUNNING\""};
            queryMediaProcServiceStoppedScript = new String[]{"cmd.exe", "/c", "sc", "query", serviceName, "|", "find", "/C", "\"STOPPED\""};//to check whether service is running or not
            startMediaProcServiceScript = new String[]{"cmd.exe", "/c", "sc", "start", serviceName};//to start service
            stopMediaProcServiceScript = new String[]{"cmd.exe", "/c", "sc", "stop", serviceName};//to stop service
            killMediaProcessorScript = new String[]{"cmd.exe", "/c", "taskkill", "/F", "/IM", serviceProcess};//to kill process
        }

        public String getServiceName() {
            return serviceName;
        }

        public String getServiceProcess() {
            return serviceProcess;
        }

        public long getStartTimeout() {
            return startTimeout;
        }

        public long getStopTimeout() {
            return stopTimeout;
        }
    }
}