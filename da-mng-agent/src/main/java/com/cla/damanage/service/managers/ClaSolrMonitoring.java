package com.cla.damanage.service.managers;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteWatchdog;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.Socket;
import java.net.URI;
import java.net.UnknownHostException;

/**
 * Created by itay on 05/09/2017.
 */
@Service
public class ClaSolrMonitoring {
    private static final Logger logger = LoggerFactory.getLogger(ClaSolrMonitoring.class);

//    @Value("${damanage.solr.restart.script:..\\DocAuthority-solr-e.exe /stop & timeout /t 5 & ..\\DocAuthority-solr-e.exe /start}")
    @Value("${damanage.solr.restart.script:taskkill /F /IM DocAuthority-solr-e.exe}")   // Process will be restarted by the service wrapper
    private String solrRestartScript;

    @Value("${genericScriptActivation:cmd /c}")
    private String genericScriptActivation;


    public boolean isSolrReachable(String connectionAddress) {
        //This is a standard Solr server
        URI uri = URI.create(connectionAddress);
        String host = uri.getHost();
        int port = uri.getPort();
        try (Socket s = new Socket(host, Integer.valueOf(port))) {
        } catch (UnknownHostException e) {
            logger.info("Solr host {}:{} is unreachable",host,port);
            return false;
        } catch (IOException e) {
            logger.info("Error while trying to connect to Solr {}:{}",host,port);
            return false;
        }
        return true;
    }

    public static boolean isSolrReachable(String host, int port) {
        try (Socket s = new Socket(host, port)) {
        } catch (UnknownHostException e) {
            logger.info("Zookeeper host {}:{} is unreachable",host,port);
            return false;
        } catch (IOException e) {
            logger.info("Error while trying to connect to Zookeeper {}:{}",host,port);
            return false;
        }
        return true;
    }

    public static boolean isActiveMQReachable(String connectionAddress) {
        //Zookeeper server
        String[] split = connectionAddress.split("/");
        String[] split1 = split[0].split(":");
        String host = split1[0];
        String port = split1[1];
        try (Socket s = new Socket(host, Integer.valueOf(port))) {
        } catch (UnknownHostException e) {
            logger.info("ActiveMQ host {}:{} is unreachable",host,port);
            return false;
        } catch (IOException e) {
            logger.info("Error while trying to connect to ActiveMQ {}:{}",host,port);
            return false;
        }
        return true;
    }

    public void solrRestart() {
        String commandLine = String.format("%s %s", genericScriptActivation, solrRestartScript);
        try {
            CommandLine cmdLine = CommandLine.parse(commandLine);
            DefaultExecutor executor = new DefaultExecutor();
            executor.setExitValue(0);
            ExecuteWatchdog watchdog = new ExecuteWatchdog(60000);
            executor.setWatchdog(watchdog);
            logger.info("Restarting Solr service using: {}", solrRestartScript);
            int stat = executor.execute(cmdLine);
            logger.info("Solr service restarted ({})", stat);
        } catch (IOException e) {
            logger.error("Error activating script '{}'", commandLine, e);
        } catch (RuntimeException e) {
            logger.error("Error activating script '{}'", commandLine, e);
        }
    }

}
