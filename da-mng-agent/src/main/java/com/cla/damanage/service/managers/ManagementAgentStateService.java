package com.cla.damanage.service.managers;

import com.cla.connector.utils.TimeSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedResource;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicLong;

@Component
@ManagedResource(objectName = "cla:name=managementAgentStateService")
public class ManagementAgentStateService {

    private static final Logger logger = LoggerFactory.getLogger(ManagementAgentStateService.class);

    private static final String VERSION_PROPERTIES_FILE = "." + File.separator + "config" + File.separator + "version.properties";

    private AtomicLong solrRestartCount = new AtomicLong(0);
    private AtomicLong amqRestartCount = new AtomicLong(0);
    private long prevSolrRestartCount ;

    private AtomicLong periodicChecksCount = new AtomicLong(0);
    private long prevPeriodicChecksCount ;

    private TimeSource timeSource = new TimeSource();

    private String version;

    public void incrementSolrRestartCount() {
        solrRestartCount.incrementAndGet();
    }

    public void incrementAmqRestartCount() {
        amqRestartCount.incrementAndGet();
    }

    public void incrementPeriodicChecksCount() {
        periodicChecksCount.incrementAndGet();
    }

    @ManagedAttribute
    public String getVersion() {
        if (version == null) {
            String fileName = VERSION_PROPERTIES_FILE;
            version = readVersionFromFile(fileName);
        }
        return version;
    }

    private String readVersionFromFile(String fileName) {
        String readString = null;
        try {
            FileReader fr = new FileReader(fileName);
            char[] cbuf = new char[100];
            int read = fr.read(cbuf);
            readString = new String(cbuf, 0, read);
        } catch (FileNotFoundException e) {
            logger.error("File not found " + VERSION_PROPERTIES_FILE + " file");
        } catch (IOException e) {
            logger.error("Failed to open " + VERSION_PROPERTIES_FILE + " file", e);
        }
        return readString;
    }

}
