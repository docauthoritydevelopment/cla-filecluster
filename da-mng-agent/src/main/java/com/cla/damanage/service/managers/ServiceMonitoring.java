package com.cla.damanage.service.managers;

import com.cla.connector.utils.TimeSource;
import com.google.common.base.Strings;
import com.google.gson.*;
import org.apache.catalina.connector.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.TimeUnit;


/**
 * Utility class to perform the following service control operations:
 * restart, stop , start, health-check & kill a service
 */
public class ServiceMonitoring {
    private static final Logger logger = LoggerFactory.getLogger(ServiceMonitoring.class);
    private final WindowsServiceControl.ServiceControlConfig config;

    private String healthCheckUrl;

    private long healthCheckTimeout;

    private WindowsServiceControl appServerSc;

    private TimeSource timeSource = new TimeSource();

    public ServiceMonitoring(WindowsServiceControl.ServiceControlConfig config, String healthCheckUrl, long healthCheckTimeouts) {
        this.config = config;
        appServerSc = new WindowsServiceControl(config);
        this.healthCheckUrl = healthCheckUrl;
        this.healthCheckTimeout = healthCheckTimeouts;
    }

    public boolean isServiceReachable(long minTimeSinceLastServiceRestartSec) {
        IOException lastExp = null;
        int seconds = 0;
        logger.debug("Performing {} health check, url: {} ...", config.getServiceName(), healthCheckUrl);
        HttpURLConnection con = null;
        while (seconds < healthCheckTimeout) {
            try {
                lastExp = null;
                con = (HttpURLConnection) new URL(healthCheckUrl).openConnection();
                con.setReadTimeout(Long.valueOf(TimeUnit.SECONDS.toMillis(healthCheckTimeout)).intValue());
                con.setRequestMethod("GET");

                boolean statusIsOK = con.getResponseCode() == Response.SC_OK;
                logger.debug("{} health check returned status: {}", config.getServiceName(), con.getResponseCode());
                if (statusIsOK && minTimeSinceLastServiceRestartSec > 0) {
                    BufferedReader in = new BufferedReader(new InputStreamReader(
                            con.getInputStream()));
                    String inputLine;
                    StringBuilder response = new StringBuilder();
                    while ((inputLine = in.readLine()) != null) {
                        response.append(inputLine);
                    }
                    in.close();
                    String headerValue = getHeaderIfExists(response.toString(), "LAST_ACTIVE_DATE");
                    if (!Strings.isNullOrEmpty(headerValue)) {
                        long lastActiveDate = Long.parseLong(headerValue);
                        if (lastActiveDate < timeSource.millisSince(TimeUnit.SECONDS.toMillis(minTimeSinceLastServiceRestartSec))) {
                            statusIsOK = false;
                            logger.debug("{} health check last active returned {} - too much time passed - status not ok",
                                    config.getServiceName(), lastActiveDate);
                        }
                    }
                }

                return statusIsOK;
            }
            catch (IOException e) {
                logger.debug("Connection not established yet: {} seconds, message: {}", seconds, e.getMessage());
                lastExp = e;
                seconds++;
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e1) {
                    // do nothing
                }
            } finally {
                if (con != null) {
                    con.disconnect();
                }
            }
        }
        if (lastExp != null) {
            logger.error("Failed to acquire connection to check {} health status", config.getServiceName(), lastExp);
        }
        return false;
    }

    private String getHeaderIfExists(String response, String headerName) {
        if (response == null || response.isEmpty()) return null;

        String headerValue = null;

        try {
            JsonElement json = new JsonParser().parse(response);
            if (json != null) {
                JsonObject obj = json.getAsJsonObject();
                if (obj != null) {
                    JsonObject headers = obj.getAsJsonObject("headers");
                    if (headers != null) {
                        JsonElement header = headers.get(headerName);
                        if (header != null) {
                            headerValue = header.getAsString();
                        }
                    }
                }
            }
        } catch (Exception e) {
            // response is not in json format
        }
        return headerValue;
    }

    public boolean restartService() {
        try {
            if (appServerSc.isServiceStarted()) {
                return appServerSc.stopService() && appServerSc.startService();
            }
            return appServerSc.startService();
        } catch (Throwable t) {
            logger.error(String.format("Failed restart service %S", config.getServiceName()), t);
            return false;
        }
    }

    public boolean forceRestart() {
        return appServerSc.killService() && appServerSc.startService();
    }


}
