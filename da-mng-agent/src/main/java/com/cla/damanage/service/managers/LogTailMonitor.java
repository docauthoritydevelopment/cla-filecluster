package com.cla.damanage.service.managers;

import org.apache.commons.io.input.Tailer;
import org.apache.commons.io.input.TailerListener;
import org.apache.commons.io.input.TailerListenerAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedResource;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.google.common.base.Strings.*;

/**
 * Created by liora on 10/07/2017.
 */
@Component
@ManagedResource(objectName = "cla:name=logTailMonitor")
public class LogTailMonitor {

    private static final Logger logger = LoggerFactory.getLogger(LogTailMonitor.class);

    private static final String VERSION_PROPERTIES_FILE = "." + File.separator + "config" + File.separator + "version.properties";

    @Autowired
    private ManagementAgentControlService managementAgentControlService;

    @Autowired
    private SolrServiceCheck solrServiceCheck;

    @Autowired
    private ActiveMQServiceCheck activeMQServiceCheck;

    @Value("${tailer.error.regex:}")
    private String[] tailerErrorRegex;

    @Value("${tailer.restart-indication.regex:}")
    private String[] tailerRestartIndicationRegex;

    @Value("${tailer.fatal.regex:}")
    private String[] tailerFatalRegex;

	@Value("${tailer.filecluster.ignore.regex:}#{T(java.util.Collections).emptyList()}")
	private List<String> appserverTailerIgnoreRegex;

    @Value("${tailer.files:}")
    private String[] tailerFiles;

    @Value("${tailer.recreate-after-exception:true}")
    private Boolean recreateTailerAfterException;

    @Value("${damanage.solr.logfile.name:solr.log}")
    private String solrLogFileName;

    @Value("${damanage.amq.logfile.name:activemq.log}")
    private String amqLogFileName;

    @Value("${damanage.mediaproc.logfile.pattern:mediaProcessor}")
    private String mediaProcessorLogFilePattern;

    @Value("${damanage.appserver.logfile.pattern:filecluster}")
    private String appServerLogFilePattern;

    private String mpLogFileName;

    @Value("${tailer.monitor.delay.ms:30000}")
    private long tailerMonitorDelayMs;

    private long tailerTotalMatchCount = 0;
    private long tailerMatchCount = 0;            // since last reset
    private long tailerRestartMatchCount = 0;
    private long tailerFatalMatchCount = 0;

    private boolean tailerActive = false;

    private static List<Pattern> tailerRestartIndicationSearchPatterns = new ArrayList<>();
    private static List<Pattern> tailerErrorSearchPatterns = new ArrayList<>();
    private static List<Pattern> tailerFatalSearchPatterns = new ArrayList<>();
    private static List<Pattern> appserverIgnoredPatterns = new ArrayList<>();


    @PostConstruct
    private void init() {
        // Create Tailers
        // Tailers will handle lines only after set tailerActive = true;
        Arrays.asList(tailerErrorRegex).forEach(LogTailMonitor::addTailerErrorSearchPattern);
        Arrays.asList(tailerRestartIndicationRegex).forEach(LogTailMonitor::addTailerRestartIndicationSearchPattern);
        Arrays.asList(tailerFatalRegex).forEach(LogTailMonitor::addTailerFatalSearchPattern);
        appserverIgnoredPatterns = appserverTailerIgnoreRegex.stream().map(regex -> Pattern.compile(regex)).collect(Collectors.toList());

        tailers = Stream.of(tailerFiles).map(f -> createTailer(f, tailerMonitorDelayMs)).collect(Collectors.toList());
        tailers.forEach(ClaTailer::start);
    }

    private class ClaTailerListener extends TailerListenerAdapter {
        private ClaTailer tailer;
        private int fileNotFound = 0;

        ClaTailerListener(ClaTailer t) {
            super();
            this.tailer = t;
        }

        @Override
        public void fileNotFound() {
            if (fileNotFound == 0) {
                logger.info("Tailer file not found: {}", tailer.file.getAbsolutePath());
            }
            fileNotFound++;
        }

        @Override
        public void handle(Exception ex) {
            logger.error("Tailer exception: {}", ex.getMessage());
            if (recreateTailerAfterException) {
                try {
                    tailer.stop();
                    tailers.remove(this);
                    ClaTailer newTailer = createTailer(tailer.file.getAbsolutePath(), tailer.delay);
                    tailers.add(newTailer);
                    newTailer.start();
                } catch (Exception e) {
                    logger.warn("Could not create new tailer for {}.", tailer.file.getAbsolutePath(), e);
                }
            }
        }

        @Override
        public void handle(String line) {
             if (!tailerActive) {
                if (logger.isTraceEnabled()) {
                    logger.trace("Logger handle event while tailer is inactive");
                }
                return;
            }
			String tailerAbsolutePath = tailer.file.getAbsolutePath();
            boolean isSolrLog = !isNullOrEmpty(solrLogFileName) && tailerAbsolutePath.contains(solrLogFileName);
            boolean isAmqLog = !isNullOrEmpty(amqLogFileName) && tailerAbsolutePath.contains(amqLogFileName);
			boolean isMediaProcessorLog = !isNullOrEmpty(mediaProcessorLogFilePattern) && tailerAbsolutePath.toLowerCase().contains(mediaProcessorLogFilePattern.toLowerCase());
			boolean isAppserver = !isNullOrEmpty(appServerLogFilePattern) && tailerAbsolutePath.toLowerCase().contains(appServerLogFilePattern.toLowerCase());
			Predicate<String> isIgnoredPattern = testedLine -> appserverIgnoredPatterns.stream().anyMatch(ignorePattern -> ignorePattern.matcher(line).find());

			for (Pattern p : tailerFatalSearchPatterns) {
                if (p.matcher(line).find()) {
                    if (isSolrLog) {
                        logger.warn("Found fatal match in Solr log. Marking for immediate restart");
                        solrServiceCheck.incSolrFatalCount();
                        solrServiceCheck.markSolrForRestart();
                    } else if (isAmqLog) {
                        logger.warn("Found fatal match in ActiveMQ log. Marking for immediate restart");
                        activeMQServiceCheck.incFatalCount();
                        activeMQServiceCheck.markAmqForRestart();
                    } else if (isMediaProcessorLog) {
                        logger.warn("Found fatal match in Media processor log. Marking for immediate restart");
                        managementAgentControlService.incMediaProcessorFatalCount();
                        managementAgentControlService.markMediaProcessorForRestart();
                    } else if (isAppserver && !isIgnoredPattern.test(line)) {
                        logger.warn("Found fatal match in Appserver processor log. Marking for immediate restart");
                        managementAgentControlService.markAppServerForRestart();
                        managementAgentControlService.incAppServerFatalCount();
                        managementAgentControlService.updateAppServerLastFatalLine(line);
                    }
					logger.debug("Tailer found fatal match for pattern {} line=<{}>", p.pattern(), line);
                    return;
                }
            }
            for (Pattern p : tailerErrorSearchPatterns) {
                if (p.matcher(line).find()) {
                    if (isSolrLog) {
                        logger.warn("Found error match in Solr log. Counted");
                        solrServiceCheck.incSolrErrorCount();
                    } else if (isMediaProcessorLog) {
                        logger.warn("Found error match in Media processor log. Counted");
                        managementAgentControlService.incMediaProcErrorCount();
                    } else if (isAppserver && !isIgnoredPattern.test(line)) {
                        logger.warn("Found fatal match in Appserver processor log. Counted");
                        managementAgentControlService.incAppServerErrorCount();
                        managementAgentControlService.updateAppServerLastErrorLine(line);
                    }
                    logger.debug("Tailer found error match for pattern {} line=<{}>", p.pattern(), line);
                    return;
                }
            }

            for (Pattern p : tailerRestartIndicationSearchPatterns) {
                if (p.matcher(line).find()) {
                    if (isSolrLog) {
                        logger.warn("Solr server restart detected in log");
                        solrServiceCheck.incSolrRestartCount();
                    } else if (isMediaProcessorLog) {
                        logger.warn("MediaProcessor server restart detected in log");
                        managementAgentControlService.incMediaProcRestartCount();
                    } else if (isAppserver && !isIgnoredPattern.test(line)) {
                        logger.warn("Filecluster server restart detected in log");
                        managementAgentControlService.incAppServerRestartCount();
                    }
                    logger.debug("Tailer found restart-indication match for pattern {} line=<{}>", p.pattern(), line);
                    return;
                }
            }
        }

        @Override
        public void fileRotated() {
            logger.debug("File rotation was detected for {}", tailer.file.getAbsolutePath());
        }
    }

    @ManagedAttribute
    public long getTailerTotalMatchCount() {
        return tailerTotalMatchCount;
    }

    @ManagedAttribute
    public long getTailerRestartMatchCount() {
        return tailerRestartMatchCount;
    }

    @ManagedAttribute
    public long getTailerMatchCount() {
        return tailerMatchCount;
    }

    @ManagedAttribute
    public long getTailerFatalMatchCount() {
        return tailerFatalMatchCount;
    }

    public long resetTailerMatchCount() {
        long c = tailerMatchCount;
        tailerMatchCount = 0;
        tailerRestartMatchCount = 0;
        return c;
    }

    class ClaTailer {
        private TailerListener listener;
        private Tailer tailer;
        private Thread thread;
        private boolean isRunning = false;
        private File file;
        private long delay;

        public void start() {
            if (isRunning) {
                logger.info("ClaTailer already running. Ingored");
                return;
            }
            isRunning = true;
            thread.start();
            logger.info("ClaTailer has started. file={}", file.getAbsolutePath());
        }

        public void stop() {
            if (!isRunning) {
                logger.info("ClaTailer not running. Ingored");
                return;
            }
            isRunning = false;
            tailer.stop();
            logger.info("ClaTailer has stopped. file={}", file.getAbsolutePath());
        }
    }


    ClaTailer createTailer(String fileName, long delay) {
        logger.info("Creating tailer for file {} with delay {} ms", fileName, delay);

        ClaTailer t = new ClaTailer();

        t.file = new File(fileName);
        t.listener = new ClaTailerListener(t);
        // Should have reOpen=true to allow components' loggers to be able to rename log files based on the rotation strategy
        t.tailer = new Tailer(t.file, t.listener, delay, true, true);        // Listens from the end of the file
        t.thread = new Thread(t.tailer);
        t.thread.setDaemon(true); // optional
        t.thread.setName("ClaTailer-" + t.file.getName());
        t.delay = delay;
        return t;
    }

    // Update running rate every 15 seconds
//    private LogTailMonitor.IntervalTimerExecutor intervalTimerExecutor = new LogTailMonitor.IntervalTimerExecutor(15);

    private String version;
    private List<ClaTailer> tailers;

    public List<ClaTailer> getTailers() {
        return tailers;
    }

//    private class IntervalTimerExecutor {
//        Timer timer;
//        long seconds;
//
//        public IntervalTimerExecutor(int seconds) {
//            timer = new Timer("OpState-calc", true);
//            this.seconds = seconds;
//            long millis = TimeUnit.SECONDS.toMillis(seconds);
//            timer.scheduleAtFixedRate(new LogTailMonitor.IntervalTask(), millis, millis);
//        }
//    }

    private static void addTailerErrorSearchPattern(String regex) {
        tailerErrorSearchPatterns.add(Pattern.compile(regex));
    }

    private static void addTailerRestartIndicationSearchPattern(String regex) {
        tailerRestartIndicationSearchPatterns.add(Pattern.compile(regex));
    }

    private static void addTailerFatalSearchPattern(String regex) {
        tailerFatalSearchPatterns.add(Pattern.compile(regex));
    }

    public boolean isTailerActive() {
        return tailerActive;
    }

    public void setTailerActive(boolean tailerActive) {
        this.tailerActive = tailerActive;
        logger.info("Setting tailerActive={}", tailerActive ? "true" : "false");
    }

//    @Override
//    protected void finalize() throws Throwable {
//        intervalTimerExecutor.timer.cancel();
//        super.finalize();
//    }

//    class IntervalTask extends TimerTask {
//        public void run() {
////    		logger.debug("timer interval");
//   //         processCurrentPhaseRate();
//        }
//    }

}
