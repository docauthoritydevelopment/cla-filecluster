package com.cla.damanage.service.managers;

import com.cla.common.domain.dto.messages.control.DriveDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Periodically checks components livelihood
 * Created by uri on 21-May-17.
 */
@Service
public class ManagementAgentControlService {
    private final static Logger logger = LoggerFactory.getLogger(ManagementAgentControlService.class);
    private static final String MEDIA_PROCESSOR_SERVICE_NAME = "DocAuthority-mediaprocessor";
    private static final String APP_SERVER_SERVICE_NAME = "DocAuthority-filecluster";
    private static final String AMQ_SERVICE_NAME = "DocAuthority ActiveMQ";

    @Autowired
    private LogTailMonitor logTailMonitor;

    @Autowired
    private ManagementAgentStateService managementAgentStateService;

    @Autowired
    private ActiveMQServiceCheck activeMQServiceCheck;

    @Autowired
    private SolrServiceCheck solrServiceCheck;

    private ServiceMonitoring appServerMonitoring;

    private ServiceMonitoring mediaProcMonitoring;

    private ServiceMonitoring amqMonitoring;

    @Value("${damanage.mediaproc.num-allowed-failures:3}")
    private long allowedMediaProcFailures;

    @Value("${damanage.appserver.num-allowed-failures:3}")
    private long allowedAppServerFailures;

    @Value("${damanage.amq.num-allowed-failures:3}")
    private long allowedAmqFailures;

    @Value("${damanage.mediaproc.checkReachability:false}")
    private boolean checkMediaProcReachable;

    @Value("${damanage.mediaproc.connection.string:http://localhost:8089}")
    private String mediaProcApiUrl;

    @Value("${damanage.mediaproc.restart.mintime.sec:900}")
    private long minTimeSinceLastMediaProcessorRestartSec;

    @Value("${damanage.mediaproc.service-start.timeout:50}")
    private long mediaProcStartTimeout;

    @Value("${damanage.mediaproc.service-stop.timeout:30}")
    private long mediaProcStopTimeout;

    @Value("${damanage.mediaproc.healthcheck-timeout:20}")
    private long mediaProcHealthCheckTimeout;

    @Value("${damanage.amq.restart.mintime.sec:900}")
    private long minTimeSinceLastAmqRestartSec;

    @Value("${damanage.amq.service-start.timeout:50}")
    private long amqStartTimeout;

    @Value("${damanage.amq.service-stop.timeout:30}")
    private long amqStopTimeout;

    @Value("${damanage.amq.healthcheck-timeout:20}")
    private long amqHealthCheckTimeout;

    @Value("${damanage.amq.connection.string:http://localhost:8161}")
    private String amqApiUrl;

    @Value("${damanage.amq.checkReachability:true}")
    private boolean checkAmqReachable;

    @Value("${damanage.appserver.checkReachability:false}")
    private boolean checkAppserverReachable;

    @Value("${damanage.appserver.connection.host:localhost}")
    private String appServerApiHost;

    @Value("${damanage.appserver.connection.port:8080}")
    private String appServerApiPort;

    @Value("${damanage.appserver.connection.protocol:http}")
    private String appServerApiProtocol;

    private String appServerApiUrl;

    @Value("${damanage.appserver.restart.mintime.sec:900}")
    private long minTimeSinceLastAppServerRestartSec;

    @Value("${damanage.appserver.service-start.timeout:50}")
    private long appServerStartTimeout;

    @Value("${damanage.appserver.service-stop.timeout:30}")
    private long appServerStopTimeout;

    @Value("${damanage.appserver.healthcheck-timeout:60}")
    private long appServerHealthCheckTimeout;

	@Value("${damanage.health.drive.resource:/health/drive}")
	private String driveStatusResource;

    @Value("${damanage.solr.status-check.enabled:true}")
    private boolean solrCheckEnabled;

    @Value("${damanage.mediaproc.status-check.enabled:true}")
    private boolean mpCheckEnabled;

    @Value("${damanage.amq.status-check.enabled:true}")
    private boolean amqCheckEnabled;

    @Value("${damanage.appserver.status-check.enabled:true}")
    private boolean fcCheckEnabled;

	private RestTemplate restTemplate = new RestTemplate();

	private long lastMpRestartTime = 0L;
	private boolean mpMarkedForRestart = false;

	private long lastAppServerRestartTime = 0;
	private boolean appServerMarkedForRestart = false;

    private long lastAmqRestartTime = 0;
    private boolean amqMarkedForRestart = false;

    private boolean  firstSchedule = false;

	private AtomicInteger mediaProcFatalCount = new AtomicInteger();
	private AtomicInteger mediaProcErrorCount = new AtomicInteger();
	private AtomicInteger mediaProcRestartCount = new AtomicInteger();

	private AtomicInteger appServerRestartCount = new AtomicInteger();
	private AtomicInteger appServerFatalCount = new AtomicInteger();
	private AtomicInteger appServerErrorCount = new AtomicInteger();

	@PostConstruct
    private void init() {
        appServerApiUrl = String.format("%s://%s:%s/api", appServerApiProtocol, appServerApiHost, appServerApiPort);
        logger.debug("ManagementAgent control service init. Reporting to appserver witch connection: {}", appServerApiUrl);
        mediaProcMonitoring = new ServiceMonitoring(new WindowsServiceControl
                .ServiceControlConfig(MEDIA_PROCESSOR_SERVICE_NAME, mediaProcStartTimeout, mediaProcStopTimeout),
                mediaProcApiUrl + "/management/healthCheck", mediaProcHealthCheckTimeout);
        appServerMonitoring = new ServiceMonitoring(new WindowsServiceControl
                .ServiceControlConfig(APP_SERVER_SERVICE_NAME, appServerStartTimeout, appServerStopTimeout),
                appServerApiUrl + "/healthCheck", appServerHealthCheckTimeout);
        amqMonitoring = new ServiceMonitoring(new WindowsServiceControl
                .ServiceControlConfig(AMQ_SERVICE_NAME, amqStartTimeout, amqStopTimeout), amqApiUrl, amqHealthCheckTimeout);
    }

    @Scheduled(initialDelayString = "${damanage.solr.status.checks.initial.ms:180000}", fixedRateString = "${status.checks.cycle.ms:180000}")
    @ConditionalOnProperty(value = "damanage.solr.status-check.enabled", havingValue = "true", matchIfMissing = true)
    public void periodicStatusReportScheduler() {
        if (!firstSchedule) {
            firstSchedule = true;
            logTailMonitor.setTailerActive(true);
        }
	    if (!solrCheckEnabled) {
            return;
        }
        try {
            managementAgentStateService.incrementPeriodicChecksCount();
            solrServiceCheck.doSolrCheckup();
		} catch (Exception e) {
            logger.error("Failed to do solr checkup ({})", e);
        }
    }

	public void reportDrivesStatus(DriveDto driveDto) {
		restTemplate.postForEntity(appServerApiUrl + driveStatusResource, driveDto, Void.class);
	}

    @Scheduled(initialDelayString = "${damanage.mediaproc.status.checks.initial.ms:240000}", fixedRateString = "${status.checks.cycle.ms:180000}")
    @ConditionalOnProperty(value = "damanage.mediaproc.status-check.enabled", havingValue = "true", matchIfMissing = true)
    public void periodicMediaProcStatusReportScheduler() {
	    if (!mpCheckEnabled) return;
        try {
            managementAgentStateService.incrementPeriodicChecksCount();
            doMediaProcCheckup();
        } catch (Exception e) {
            logger.error("Failed to send status report for media processor ({})", e);
        }
    }

    private void doMediaProcCheckup() {
        ServiceUtils.doServerCheckup(MEDIA_PROCESSOR_SERVICE_NAME,
                checkMediaProcReachable,
                mediaProcMonitoring,
                mpMarkedForRestart,
                time -> lastMpRestartTime = time,
                this::resetMediaProcessorMarkForRestart,
                lastMpRestartTime,
                minTimeSinceLastMediaProcessorRestartSec,
                allowedMediaProcFailures);
    }

    @Scheduled(initialDelayString = "${damanage.amq.status.checks.initial.ms:300000}", fixedRateString = "${status.checks.cycle.ms:180000}")
    @ConditionalOnProperty(value = "damanage.amq.status-check.enabled", havingValue = "true", matchIfMissing = true)
    public void periodicAmqStatusReportScheduler() {
	    if (!amqCheckEnabled) return;
        try {
            managementAgentStateService.incrementPeriodicChecksCount();
            doAmqCheckup();
        } catch (Exception e) {
            logger.error("Failed to send status report for AMQ ({})", e);
        }
    }

    private void doAmqCheckup() {

	    if (activeMQServiceCheck.isAmqMarkedForRestart()) {
            activeMQServiceCheck.clearAmqForRestart();
            logger.debug("ActiveMQ marked for restart (log tail monitor)");
            doAmqServiceRestart();
            return;
        }

        ServiceUtils.doServerCheckup(AMQ_SERVICE_NAME,
                checkAmqReachable,
                amqMonitoring,
                amqMarkedForRestart,
                time -> lastAmqRestartTime = time,
                this::resetAmqForRestart,
                lastAmqRestartTime,
                minTimeSinceLastAmqRestartSec,
                allowedAmqFailures);

        if (activeMQServiceCheck.shouldRestartQueueTooBig()) {
            doAmqServiceRestart();
        }
    }

    protected void doAmqServiceRestart() {
        managementAgentStateService.incrementAmqRestartCount();
        ServiceUtils.restartService(AMQ_SERVICE_NAME, amqMonitoring, this::resetAmqForRestart,
                time -> lastAmqRestartTime = time, lastAmqRestartTime,
                minTimeSinceLastAmqRestartSec, allowedAmqFailures);
    }

    @Scheduled(initialDelayString = "${damanage.appserver.status.checks.initial.ms:300000}", fixedRateString = "${status.checks.cycle.ms:180000}")
	@ConditionalOnProperty(value = "damanage.appserver.status-check.enabled", havingValue = "true", matchIfMissing = true)
	public void periodicAppServerStatusReportScheduler() {
	    if (!fcCheckEnabled) return;
		try {
			managementAgentStateService.incrementPeriodicChecksCount();
			doAppServerCheckup();
		} catch (Exception e) {
			logger.error("Failed to send status report for app server ({})", e);
		}
	}

    private void doAppServerCheckup() {
        ServiceUtils.doServerCheckup(APP_SERVER_SERVICE_NAME,
                checkAppserverReachable,
                appServerMonitoring,
                appServerMarkedForRestart,
                time -> lastAppServerRestartTime = time,
                this::resetAppServerForRestart,
                lastAppServerRestartTime,
                minTimeSinceLastAppServerRestartSec,
                allowedAppServerFailures);
    }

    public boolean markMediaProcessorForRestart() {
        boolean last = mpMarkedForRestart;
        mpMarkedForRestart = true;
        resetMediaProcCounters();
        return last;
    }

    private boolean resetMediaProcessorMarkForRestart() {
        boolean last = mpMarkedForRestart;
        mpMarkedForRestart = false;
        return last;

    }

    private void resetMediaProcCounters() {
        mediaProcErrorCount.set(0);
        mediaProcFatalCount.set(0);
    }

    public boolean markAppServerForRestart() {
        boolean last = appServerMarkedForRestart;
        appServerMarkedForRestart = true;
        resetAppServerCounters();
        return last;
    }

    private void resetAppServerCounters() {
        appServerFatalCount.set(0);
        appServerErrorCount.set(0);
    }

    private boolean resetAppServerForRestart() {
        boolean last = appServerMarkedForRestart;
        appServerMarkedForRestart = false;
        return last;
    }

    private boolean resetAmqForRestart() {
        boolean last = amqMarkedForRestart;
        amqMarkedForRestart = false;
        return last;
    }

    void incMediaProcessorFatalCount() {
        mediaProcFatalCount.incrementAndGet();
    }

    void incMediaProcErrorCount() {
        mediaProcErrorCount.incrementAndGet();
    }

    void incMediaProcRestartCount() {
        mediaProcRestartCount.incrementAndGet();
    }

    void incAppServerFatalCount() {
        appServerFatalCount.incrementAndGet();
        ResponseEntity<Void> resp = restTemplate.postForEntity(appServerApiUrl + "/run/logmon/incFatal", null, Void.class);
        if (resp.getStatusCode() != HttpStatus.OK) {
            logger.warn("Failed to update app server fatal count");
        }
    }

    void incAppServerErrorCount() {
        appServerErrorCount.incrementAndGet();
        ResponseEntity<Void> resp = restTemplate.postForEntity(appServerApiUrl + "/run/logmon/incError", null, Void.class);
        if (resp.getStatusCode() != HttpStatus.OK) {
            logger.warn("Failed to update app server error count");
        }
    }

    public void incAppServerRestartCount() {
        appServerRestartCount.incrementAndGet();
    }

    public void updateAppServerLastFatalLine(String line) {
        ResponseEntity<Void> resp = restTemplate.postForEntity(appServerApiUrl + "/run/logmon/updateLastFatal", line, Void.class);
        if (resp.getStatusCode() != HttpStatus.OK) {
            logger.warn("Failed to update app server error count");
        }
    }

    public void updateAppServerLastErrorLine(String line) {
        ResponseEntity<Void> resp = restTemplate.postForEntity(appServerApiUrl + "/run/logmon/updateLastError", line, Void.class);
        if (resp.getStatusCode() != HttpStatus.OK) {
            logger.warn("Failed to update app server error count");
        }
    }

    public boolean isSolrCheckEnabled() {
        return solrCheckEnabled;
    }

    public boolean isMpCheckEnabled() {
        return mpCheckEnabled;
    }

    public boolean isAmqCheckEnabled() {
        return amqCheckEnabled;
    }

    public boolean isFcCheckEnabled() {
        return fcCheckEnabled;
    }
}
