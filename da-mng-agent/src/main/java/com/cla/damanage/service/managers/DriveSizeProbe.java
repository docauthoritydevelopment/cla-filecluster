package com.cla.damanage.service.managers;

import com.cla.common.domain.dto.messages.control.DriveDto;
import com.cla.common.utils.FileSizeUnits;
import com.google.common.collect.Sets;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.file.Path;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@ConditionalOnProperty(value = "damanage.drive.status-check.enabled", havingValue = "true", matchIfMissing = false)
@Component
public class DriveSizeProbe {
	private static final Logger logger = LoggerFactory.getLogger(DriveSizeProbe.class);
	private Set<File> folders;

	@Value("${folders.size.check-recursive-size:false}")
	private boolean checkFolderSizeRecursively;

	@Value("${folders.size.duplicate-drive-check:false}")
	private boolean checkDuplicateDrives;

	@Value("${folders.size.check}")
	private String[] foldersStr;

	@Autowired
	private ManagementAgentControlService agentControlService;

	@PostConstruct
	public void init() {
		this.folders = validateCanonicalPathCorrectness(Stream.of(foldersStr).map(File::new).collect(Collectors.toSet()));
		logger.info("Folders for sizeProbe set to: {}, checkFolderSizeRecursively={}",
				Stream.of(foldersStr).collect(Collectors.joining(",")), checkFolderSizeRecursively);
	}

	@Scheduled(initialDelayString = "${damanage.drive.status.checks.initial.ms:180000}", fixedRateString = "${drive.checks.cycle.ms:600000}")
	public void doDriveCheckup() {
		try {
			DriveDto driveDto = gatherDriveAndFolderMetrics();
			agentControlService.reportDrivesStatus(driveDto);
		} catch (Exception e) {
			logger.error("Failed to send status and drive report to AppServer ({})", e);
		}
	}

	//region ...getters & setters
	public Set<File> getFolders() {
		return folders;
	}

	public DriveSizeProbe setFolders(Set<File> folders) {
		this.folders = folders;
		return this;
	}
	//endregion

	/**
	 * Calculates the size in full MB of each <b>existing</b> folder
	 * @return pairs of folder path and it`s size
	 */
	public Map<String,Integer> measureFoldersSize(){
		Map<String, Integer> folderSizes = folders.stream()
												.filter(File::exists)
												.collect(Collectors.toMap(File::getPath,
																			folder -> (int)(FileSizeUnits.BYTE.toMegabytes(getFolderSizeRecursively(folder)))));
		if (logger.isTraceEnabled()) {
			logger.trace("Folder sizes ({}): {}", folderSizes.entrySet().size(),
					folderSizes.entrySet().stream().map(e -> (e.getKey() + " size " + e.getValue()))
                            .collect(Collectors.joining(", ")));
		}
		return folderSizes;
	}

	private long getFolderSizeRecursively(File folder) {
		if (!checkFolderSizeRecursively || !folder.exists()) {
			return 0;
		}
		try {
			return FileUtils.sizeOfDirectory(folder);
		} catch (Exception e) {
			logger.info("Error while checking sizeOfDirectory({}) {}", folder, e.getMessage());
			return -1;
		}
	}


	public DriveDto gatherDriveAndFolderMetrics(){
		String hostAddress = "0.0.0.0";
		logger.trace("Do drive checkup for folders {}", folders);
		try {
			hostAddress = InetAddress.getLocalHost().getHostAddress();
		} catch (UnknownHostException e) {
			logger.warn("Could not retrieve a local host IP. Error:" + e.getMessage());
		}
		return new DriveDto().setFolderSizes(measureFoldersSize())
				.setDriveCapacities(getDriveCapacityMetrics())
				.setMachineId(hostAddress);
	}


	/**
	 * Gathers the metrics total/free space of each <b>existing</b> drive (root of all given folders)
	 * @return
	 */
	public List<DriveDto.DriveCapacityMetrics> getDriveCapacityMetrics(){
		Collection<Path> drives = checkDuplicateDrives ?
				folders.stream().filter(File::exists)
						.map(folder -> folder.toPath().getRoot()).collect(Collectors.toList()) :
				folders.stream().filter(File::exists)
				.map(folder -> folder.toPath().getRoot()).collect(Collectors.toSet());

		List<DriveDto.DriveCapacityMetrics> driveCapacityMetrics = drives.stream()
				.map(Path::toFile)
				.map(drive -> new DriveDto.DriveCapacityMetrics(drive.getPath(),
						(int) FileSizeUnits.BYTE.toMegabytes(drive.getTotalSpace()),
						(int) FileSizeUnits.BYTE.toMegabytes(drive.getFreeSpace())))
				.collect(Collectors.toList());

		if (logger.isDebugEnabled()) {
			logger.debug("Measured drive capacity metrics: {} (checkDuplicateDrives={})",
					driveCapacityMetrics.stream().map(d->d.toString()).collect(Collectors.joining(",")), checkDuplicateDrives);
		}
		return driveCapacityMetrics;
	}

	private static Set<File> validateCanonicalPathCorrectness(Set<File> folders){
		Set<File> correctFolders = Sets.newHashSet();
		folders.forEach(folder -> {
			try {
				correctFolders.add(folder.getCanonicalFile());
			} catch (IOException e) {
				logger.warn("Can`t monitor folder {}. The path is invalid.", folder.getPath());
			}
		});
		return correctFolders;
	}
}
