package com.cla.damanage.service.managers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by: yael
 * Created on: 7/23/2018
 */
@Service
public class SolrServiceCheck {

    private final static Logger logger = LoggerFactory.getLogger(SolrServiceCheck.class);

    @Autowired
    private ClaSolrMonitoring claSolrMonitoring;

    @Autowired
    private ManagementAgentStateService managementAgentStateService;

    @Value("${damanage.solr.connection.string:}")
    private String solrConnectionString;

    @Value("${damanage.solr.restart.mintime.sec:900}")
    private long minTimeSinceLastSolrRestartSec;

    @Value("${damanage.solr.num-allowed-failures:3}")
    private long allowedSolrFailures;

    @Value("${damanage.solr.sleep-after-failure.ms:3000}")
    private long solrSleepAfterFailure;

    private AtomicInteger solrFatalCount = new AtomicInteger();
    private AtomicInteger solrErrorCount = new AtomicInteger();
    private AtomicInteger solrRestartCount = new AtomicInteger();

    private long lastSolrRestartTime = 0L;
    private boolean solrMarkedForRestart = false;
    private boolean solrCheckUpActive = false;

    @PostConstruct
    void init() {
        if (solrConnectionString == null || solrConnectionString.trim().isEmpty()) {
            logger.warn("solrConnectionString empty - no solr checkup");
        }
    }

    public boolean isSolrMarkedForRestart() {
        return solrMarkedForRestart;
    }

    private void solrRestart() {
        logger.warn("Restarting Solr");
        claSolrMonitoring.solrRestart();
    }

    public boolean markSolrForRestart() {
        boolean last = solrMarkedForRestart;
        solrMarkedForRestart = true;
        resetSolrCounters();
        return last;
    }

    private void resetSolrCounters() {
        solrFatalCount.set(0);
        solrErrorCount.set(0);
    }

    public boolean clearSolrForRestart() {
        boolean last = solrMarkedForRestart;
        solrMarkedForRestart = false;
        return last;
    }

    void incSolrFatalCount() {
        solrFatalCount.incrementAndGet();
    }

    void incSolrErrorCount() {
        solrErrorCount.incrementAndGet();
    }

    void incSolrRestartCount() {
        solrRestartCount.incrementAndGet();
    }

    public void doSolrCheckup() {
        if (isSolrMarkedForRestart()) {
            clearSolrForRestart();
            logger.debug("Solr marked for restart (external to periodic checkup)");
            if (solrConnectionString == null || solrConnectionString.trim().isEmpty()) {
                logger.warn("Unexpected: Solr marked for restart while connection string is not set !!!");
            } else {
                processSolrRestart();
            }
            return;
        }
        if (solrConnectionString == null || solrConnectionString.trim().isEmpty()) {
            return;
        }
        if (solrCheckUpActive) {
            logger.warn("Solr checkup active for too long. Skipping");
            return;
        }
        solrCheckUpActive = true;
        try {
            logger.trace("Solr checkup...");
            boolean solrAlive = false;
            int checksCount = 0;
            do {
                checksCount++;
                solrAlive = claSolrMonitoring.isSolrReachable(solrConnectionString);
                if (!solrAlive) {
                    Thread.sleep(solrSleepAfterFailure);
                }
            } while (!solrAlive && checksCount < allowedSolrFailures);
            if (!solrAlive) {
                processSolrRestart();
            } else {
                logger.debug("Solr checkup ok ({} failures)", checksCount - 1);
            }
        } catch (Exception e) {
            logger.warn("Exception during Solr checkup. {}", e.getMessage(), e);
        } finally {
            solrCheckUpActive = false;
        }
    }

    private void processSolrRestart() {
        long time = System.currentTimeMillis();
        logger.warn("Solr checkup failure. Last restart {} sec ago.", lastSolrRestartTime == 0 ? 0 : ((time - lastSolrRestartTime) / 1000));
        if ((time - lastSolrRestartTime) > minTimeSinceLastSolrRestartSec * 1000) {
            lastSolrRestartTime = time;
            managementAgentStateService.incrementSolrRestartCount();
            solrRestart();
        }
    }
}
