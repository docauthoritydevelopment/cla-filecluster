package com.cla.damanage.service.managers.domain;

/**
 * Created by: yael
 * Created on: 7/23/2018
 */
public class QueueStatusResponse {
    private Object request;
    private Object value;
    private long timestamp;
    private int status;

    public QueueStatusResponse() {
    }

    public Object getRequest() {
        return request;
    }

    public void setRequest(Object request) {
        this.request = request;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
