package com.cla.damanage.service.managers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.function.Consumer;
import java.util.function.Supplier;

/**
 * Created by: yael
 * Created on: 7/23/2018
 */
public class ServiceUtils {

    private final static Logger logger = LoggerFactory.getLogger(ServiceUtils.class);

    public static void doServerCheckup(String serviceName, boolean checkAppserverReachable, ServiceMonitoring serviceMonitoring, boolean serviceMarkedForRestart, Consumer<Long> timeSetter, Supplier<Boolean> resetMarkForRestart, long lastServiceRestartTime, long minTimeSinceLastServiceRestartSec, long allowedServiceFailures) {
        int numOfFailures = 0;
        if (serviceMarkedForRestart) {
            numOfFailures = restartService(serviceName, serviceMonitoring, resetMarkForRestart, timeSetter, lastServiceRestartTime, minTimeSinceLastServiceRestartSec, allowedServiceFailures);
        } else if (checkAppserverReachable) {
            logger.debug("Performing health check on {}", serviceName);
            boolean serviceReachable = serviceMonitoring.isServiceReachable(minTimeSinceLastServiceRestartSec);
            if (!serviceReachable) {
                numOfFailures = restartService(serviceName, serviceMonitoring, resetMarkForRestart, timeSetter, lastServiceRestartTime, minTimeSinceLastServiceRestartSec, allowedServiceFailures);
            } else {
                logger.debug("{} health check passed", serviceName);
            }
        }

        logger.debug("{} checkup ({} failures)", serviceName, numOfFailures);
    }

    public static int restartService(String serviceName, ServiceMonitoring serviceMonitoring, Supplier<Boolean> resetMarkForRestart, Consumer<Long> timeSetter, long lastServiceRestartTime, long minTimeSinceLastServiceRestartSec, long allowedServiceFailures) {
        int numOfFailures = 0;
        long time = System.currentTimeMillis();
        if ((time - lastServiceRestartTime) > minTimeSinceLastServiceRestartSec * 1000) {
            logger.warn("Restarting service {} ...", serviceName);
            resetMarkForRestart.get();
            timeSetter.accept(time);

            boolean serviceReachable = false;
            while (!serviceReachable && numOfFailures < allowedServiceFailures) {
                logger.debug("{}: try #{}", serviceName, numOfFailures + 1);
                serviceReachable = serviceMonitoring.restartService() && serviceMonitoring.isServiceReachable(minTimeSinceLastServiceRestartSec);
                if (!serviceReachable) {
                    numOfFailures++;
                }
            }
            if (numOfFailures == allowedServiceFailures) { //Set for later on try to restart again if failed
                logger.warn("{} is still not responding, forcing restart", serviceName);
                boolean success = serviceMonitoring.forceRestart() && serviceMonitoring.isServiceReachable(minTimeSinceLastServiceRestartSec);
                if (!success) {
                    logger.error("{} not functioning, failed to start even after force restart", serviceName);
                }
            }
        } else {
            logger.trace("**** Skipping restart, last one was {} sec ago.", lastServiceRestartTime == 0 ? 0 : ((time - lastServiceRestartTime) / 1000));
        }

        return numOfFailures;
    }
}
