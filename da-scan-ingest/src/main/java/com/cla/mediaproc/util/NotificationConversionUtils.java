package com.cla.mediaproc.util;

import com.cla.common.domain.dto.messages.notifications.MediaProcessorNotification;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Created by uri on 14-Mar-17.
 */
public class NotificationConversionUtils {
    private final static ObjectMapper mapper;

    static {
        mapper = new ObjectMapper();
        mapper.configure(JsonParser.Feature.ALLOW_SINGLE_QUOTES,true);
    }

    public static String serializeMediaProcessorNotification(MediaProcessorNotification data) {
        try {
            return mapper.writeValueAsString(data);
        } catch (JsonProcessingException e) {
            throw new RuntimeException("Failed to convert media processor notification to string",e);
        }
    }
}
