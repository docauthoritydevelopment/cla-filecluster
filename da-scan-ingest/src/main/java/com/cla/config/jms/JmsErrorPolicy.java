package com.cla.config.jms;

import org.springframework.jms.JmsException;
import org.springframework.jms.core.JmsTemplate;

/**
 * Interface for implementing different kind of policies in case of an error sending messages via jms
 */
public interface JmsErrorPolicy {

    void onError(JmsTemplate jmsTemplate, String destinationName, Object message, JmsException e);
}
