package com.cla.config.jms;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.JmsException;
import org.springframework.jms.core.JmsTemplate;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Policy for retrying to send messages by num of max retries and sleep interval between retries
 */
public class JmsErrorRetryPolicy implements JmsErrorPolicy {

    private static final Logger logger = LoggerFactory.getLogger(JmsErrorRetryPolicy.class);

    private int maxRetries;

    private long intervalInSec;

    private AtomicInteger policyUsageCounter = new AtomicInteger(0);


    public JmsErrorRetryPolicy(String policyConfig) {
        final String[] retriesAndInterval = policyConfig.split(";");
        this.maxRetries = Integer.valueOf(retriesAndInterval[0].trim());
        this.intervalInSec = Long.valueOf(retriesAndInterval[1].trim());
    }

    public JmsErrorRetryPolicy(int maxRetries, long intervalInSec) {
        this.maxRetries = maxRetries;
        this.intervalInSec = intervalInSec;
    }


    @Override
    public void onError(JmsTemplate jmsTemplate, String destinationName, Object message, JmsException e) {
        if (policyUsageCounter.getAndIncrement() == 0) {
            logger.debug("++++++++++++++++++++++++ Retry policy on {} started for {} ({} retries)", e.getClass().getSimpleName(), destinationName, maxRetries == -1 ? "infinite" : maxRetries);
        }
        int retryCount = 0;
        while (retryCount < maxRetries || maxRetries == -1 /*means forever*/) {
            try {
                logger.trace("Trying to releasing message {} to {} after {} retries", Objects.hash(message), destinationName, retryCount);
                jmsTemplate.convertAndSend(destinationName, message);
                logger.trace("Succeeded releasing message {} to {} after {} retries", Objects.hash(message), destinationName, retryCount);
                break;
            } catch (JmsException je) {

            }
            try {
                Thread.sleep(intervalInSec);
            } catch (InterruptedException e1) {
            }
        }
        if (retryCount == maxRetries) {
            logger.trace("Failed to send message {} to {} after {} retries dropping it", Objects.hash(message), destinationName, retryCount + 1);
        }
        if (policyUsageCounter.decrementAndGet() == 0) {
            logger.debug("++++++++++++++++++++++++ Retry policy done on {} for {}", e.getClass().getSimpleName(), destinationName);
        }
    }
}
