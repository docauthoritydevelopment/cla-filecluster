package com.cla.config.jms;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.JmsException;
import org.springframework.jms.core.JmsTemplate;

import java.util.*;


/**
 * Error handler hold error policy which is configured per queue
 * currently only jmsErrorRetryPolicy is configured, we'll probably later change this for other queues which
 * do not need to retry and send messages
 */
public class JmsErrorHandler {

    private static final Logger logger = LoggerFactory.getLogger(JmsErrorHandler.class);

    private static final JmsErrorPolicy DEFAULT_ERROR_HANDLING_POLICY = new DefaultJmsErrorPolicy();

    private Map<ErrorPolicyKey, JmsErrorPolicy> jmsErrorPolicyMap = new HashMap<>();

    private Map<ErrorPolicyKey, JmsErrorPolicy> jmsPrefixErrorPolicyMap = new HashMap<>();

    private JmsErrorHandler() {

    }

    public void handle(JmsTemplate jmsTemplate, String destinationName, Object message, JmsException e) {
        logger.trace("Handing caught exception {} for message {} that was sent to {}", e.getClass().getSimpleName(), Objects.hash(message), destinationName);
        JmsErrorPolicy jmsErrorPolicy = jmsErrorPolicyMap.get(new ErrorPolicyKey(destinationName, e.getClass()));
        if (jmsErrorPolicy == null) {
            jmsErrorPolicy = jmsPrefixErrorPolicyMap.entrySet().stream()
                    .filter(entry -> destinationName.startsWith(entry.getKey().destinationName))
                    .findFirst()
                    .map(entry -> entry.getValue())
                    .orElse(DEFAULT_ERROR_HANDLING_POLICY);
        }
        jmsErrorPolicy.onError(jmsTemplate, destinationName, message, e);
    }

    public static JmsErrorHandlerBuilder builder() {
        return new JmsErrorHandlerBuilder();
    }


    public void addDestinationPrefixPolicy(ErrorPolicyKey errorPolicyKey, JmsErrorPolicy jmsErrorPolicy) {
        jmsPrefixErrorPolicyMap.put(errorPolicyKey, jmsErrorPolicy);
    }

    public void addPolicy(ErrorPolicyKey errorPolicyKey, JmsErrorPolicy jmsErrorPolicy) {
        jmsErrorPolicyMap.put(errorPolicyKey, jmsErrorPolicy);
    }

    private static class ErrorPolicyKey {

        private final String destinationName;
        private final Class<? extends JmsException> errorType;

        public ErrorPolicyKey(String destinationName, Class<? extends JmsException> errorType) {
            this.destinationName = destinationName;
            this.errorType = errorType;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            ErrorPolicyKey that = (ErrorPolicyKey) o;

            if (destinationName != null ? !destinationName.equals(that.destinationName) : that.destinationName != null)
                return false;
            return errorType != null ? errorType.equals(that.errorType) : that.errorType == null;
        }

        @Override
        public int hashCode() {
            int result = destinationName != null ? destinationName.hashCode() : 0;
            result = 31 * result + (errorType != null ? errorType.hashCode() : 0);
            return result;
        }

        @Override
        public String toString() {
            return "ErrorPolicyKey{" +
                    "destinationName='" + destinationName + '\'' +
                    ", errorType=" + errorType +
                    '}';
        }
    }

    public static class JmsErrorHandlerBuilder {

        private List<PolicyHolder> policyHolders = new ArrayList<>();

        private JmsErrorHandlerBuilder() {

        }

        public PolicyHolder withErrorPolicy(JmsErrorRetryPolicy jmsErrorRetryPolicy) {
            final PolicyHolder policyHolder = new PolicyHolder(jmsErrorRetryPolicy, this);
            policyHolders.add(policyHolder);
            return policyHolder;
        }

        public JmsErrorHandler build() {
            JmsErrorHandler jmsErrorHandler = new JmsErrorHandler();

            for (PolicyHolder policyHolder : policyHolders) {
                final DestinationHolder destinationHolder = policyHolder.destinationHolder;
                if (destinationHolder.prefix) {
                    jmsErrorHandler.addDestinationPrefixPolicy(
                            new JmsErrorHandler.ErrorPolicyKey(destinationHolder.destination, destinationHolder.jmsException),
                            policyHolder.jmsErrorPolicy);
                } else {
                    jmsErrorHandler.addPolicy(
                            new JmsErrorHandler.ErrorPolicyKey(destinationHolder.destination, destinationHolder.jmsException),
                            policyHolder.jmsErrorPolicy);
                }
            }

            return jmsErrorHandler;
        }

        public static final class PolicyHolder {

            private final JmsErrorPolicy jmsErrorPolicy;
            private final JmsErrorHandlerBuilder jmsErrorHandlerBuilder;
            private DestinationHolder destinationHolder;

            private PolicyHolder(JmsErrorPolicy jmsErrorPolicy, JmsErrorHandlerBuilder jmsErrorHandlerBuilder) {
                this.jmsErrorPolicy = jmsErrorPolicy;
                this.jmsErrorHandlerBuilder = jmsErrorHandlerBuilder;
            }


            public DestinationHolder onDestination(String destination) {
                this.destinationHolder = new DestinationHolder(destination, false, jmsErrorHandlerBuilder);
                return destinationHolder;
            }

            public DestinationHolder onAllDestinations() {
                this.destinationHolder = new DestinationHolder("", true, jmsErrorHandlerBuilder);
                return destinationHolder;
            }

            public DestinationHolder onDestinationPrefix(String prefix) {
                this.destinationHolder = new DestinationHolder(prefix, true, jmsErrorHandlerBuilder);
                return destinationHolder;
            }


        }

        public static final class DestinationHolder {

            private final String destination;
            private final JmsErrorHandlerBuilder jmsErrorHandlerBuilder;
            private final boolean prefix;
            private Class<? extends JmsException> jmsException;

            private DestinationHolder(String destination, boolean prefix, JmsErrorHandlerBuilder jmsErrorHandlerBuilder) {
                this.destination = destination;
                this.prefix = prefix;
                this.jmsErrorHandlerBuilder = jmsErrorHandlerBuilder;
            }

            public JmsErrorHandlerBuilder against(Class<? extends JmsException> jmsException) {
                this.jmsException = jmsException;
                return jmsErrorHandlerBuilder;
            }
        }


    }

}
