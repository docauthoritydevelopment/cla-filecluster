package com.cla.config.jms;

import java.util.List;

public interface JmsBatchSupport {

    void sendInBatch(String destinationName, List<? extends Object> Messages);
}
