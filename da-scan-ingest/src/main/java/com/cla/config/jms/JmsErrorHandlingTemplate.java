package com.cla.config.jms;

import org.jetbrains.annotations.Nullable;
import org.springframework.jms.JmsException;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.jms.core.ProducerCallback;
import org.springframework.jms.support.JmsUtils;
import org.springframework.jms.support.converter.MessageConverter;

import javax.jms.*;
import java.util.List;

/**
 * Handles size constraint errors and apply error policies in case send message exceptions .
 */
public class JmsErrorHandlingTemplate extends JmsTemplate implements JmsBatchSupport {

    private final JmsErrorHandler jmsErrorHandler;
    private final long maxAllowedMessageFrameSize;
    private final JmsTemplate alternateTemplate;

    public JmsErrorHandlingTemplate(ConnectionFactory connectionFactory, JmsErrorHandler jmsErrorHandler, long maxAllowedMessageFrameSize) {
        super(connectionFactory);
        this.maxAllowedMessageFrameSize = maxAllowedMessageFrameSize;
        this.jmsErrorHandler = jmsErrorHandler;

        this.alternateTemplate = new JmsTemplate(connectionFactory) {
            @Override
            protected void doSend(MessageProducer producer, Message message) throws JMSException {
                assertMessageSize(message);
                super.doSend(producer, message);
            }
        };
        alternateTemplate.setMessageConverter(this.getMessageConverter());
        alternateTemplate.setExplicitQosEnabled(this.isExplicitQosEnabled());
        alternateTemplate.setTimeToLive(this.getTimeToLive());
    }

    private void assertMessageSize(Message message) throws JMSException {
        long messageByteSize = 0;
        if (message instanceof TextMessage) {
            messageByteSize = ((TextMessage) message).getText().getBytes().length;
        } else if (message instanceof BytesMessage) {
            messageByteSize = ((BytesMessage) message).getBodyLength();
        }
        if (messageByteSize > maxAllowedMessageFrameSize) {
            throw new RuntimeException(String.format("Size (%d bytes) of message to be sent is greater than max allowd (%d bytes)", messageByteSize, maxAllowedMessageFrameSize));
        }
    }

    @Override
    protected void doSend(MessageProducer producer, Message message) throws JMSException {
        assertMessageSize(message);
        super.doSend(producer, message);
    }


    @Override
    public void setTimeToLive(long timeToLive) {
        super.setTimeToLive(timeToLive);
        if (alternateTemplate != null) {
            alternateTemplate.setTimeToLive(timeToLive);
        }
    }

    @Override
    public void setExplicitQosEnabled(boolean explicitQosEnabled) {
        super.setExplicitQosEnabled(explicitQosEnabled);
        if (alternateTemplate != null) {
            alternateTemplate.setExplicitQosEnabled(explicitQosEnabled);
        }
    }

    @Override
    public void setMessageConverter(MessageConverter messageConverter) {
        super.setMessageConverter(messageConverter);
        if (alternateTemplate != null) {
            alternateTemplate.setMessageConverter(messageConverter);
        }
    }

    public void setPubSubDomain(boolean pubSubDomain) {
        super.setPubSubDomain(pubSubDomain);
        if (alternateTemplate != null) {
            alternateTemplate.setPubSubDomain(pubSubDomain);
        }
    }

    @Override
    public void setDeliveryMode(int deliveryMode) {
        super.setDeliveryMode(deliveryMode);
        if (alternateTemplate != null) {
            alternateTemplate.setDeliveryMode(deliveryMode);
        }
    }

    @Override
    public void sendInBatch(String destinationName, List<? extends Object> Messages) {
        execute(destinationName, (session, producer) -> batchExecution(Messages, session, producer));
    }

    private Object batchExecution(List<? extends Object> Messages, Session session, MessageProducer producer) throws JMSException {
        try {
            for (Object message : Messages) {
                final Message jmsMessage = getMessageConverter().toMessage(message, session);
                doSend(producer, jmsMessage);
            }

            if (session.getTransacted() && isSessionLocallyTransacted(session)) {
                // Transacted session created by this template -> commit.
                JmsUtils.commitIfNecessary(session);
            }
        } catch (JmsException e) {
            logger.trace("Ignoring exceptions on batch, error: " + e.getMessage());
        } finally {
            JmsUtils.closeMessageProducer(producer);
        }
        return null;
    }


}
