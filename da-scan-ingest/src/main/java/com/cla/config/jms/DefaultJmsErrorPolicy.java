package com.cla.config.jms;

import org.springframework.jms.JmsException;
import org.springframework.jms.core.JmsTemplate;

/**
 * Default error policy which just propagate the error up
 */
public class DefaultJmsErrorPolicy implements JmsErrorPolicy {


    @Override
    public void onError(JmsTemplate jmsTemplate, String destinationName, Object message, JmsException e) {
        throw e;
    }
}
