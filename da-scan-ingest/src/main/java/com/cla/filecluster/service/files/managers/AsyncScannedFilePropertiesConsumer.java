package com.cla.filecluster.service.files.managers;

import com.cla.common.domain.dto.messages.ScanResultMessage;
import com.cla.connector.domain.dto.messages.ScanTaskParameters;
import com.cla.filecluster.mediaproc.connectors.ScanResultMessageConsumer;

import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Predicate;

/**
 * Consumer of scan results produced by Connection Wrapper
 * Created by vladi on 2/7/2017.
 */
public class AsyncScannedFilePropertiesConsumer implements ScanResultMessageConsumer {

    private Long runId = null;
    private Long rootFolderId = null;
    private boolean firstScan = true;
    private Predicate<? super String> fileTypePredicate;
    private ScanResultMessageConsumer persistenceConsumer;
    private Executor executor;

    private AtomicInteger scanCounter = new AtomicInteger(0);



    @Override
    public void accept(ScanResultMessage result) {
        scanCounter.incrementAndGet();
        executor.execute(() -> handleScanResultMessage(result));
//        ClaFilePropertiesDto fileProps = result.getPayload();
//        if (firstScan) {
//            executor.execute(() -> persistenceConsumer.addScannedItem(rootFolderId, runId, fileTypePredicate, fileProps));
//        } else {
//            executor.execute(() -> persistenceConsumer.addCrawlTempItem(rootFolderId, runId, fileTypePredicate, fileProps));
//        }
    }

    private void handleScanResultMessage(ScanResultMessage result) {
        switch (result.getPayloadType()) {
            case SCAN:
                persistenceConsumer.accept(result);
                break;
            case SCAN_DONE:
//                Long scanJobId = jobManagerService.getJobIdCached(runContext, JobType.SCAN);
//                if (scanJobId != null) {
//                    jobManagerService.updateJobState(scanJobId, JobState.DONE);
//                }
        }
    }

    public int getScanCount() {
        return scanCounter.get();
    }

    public AsyncScannedFilePropertiesConsumer setRunId(Long runId) {
        this.runId = runId;
        return this;
    }

    public AsyncScannedFilePropertiesConsumer setRootFolderId(Long rootFolderId) {
        this.rootFolderId = rootFolderId;
        return this;
    }

    public AsyncScannedFilePropertiesConsumer setFileTypePredicate(Predicate<? super String> fileTypePredicate) {
        this.fileTypePredicate = fileTypePredicate;
        return this;
    }

    public AsyncScannedFilePropertiesConsumer setExecutor(Executor executor) {
        this.executor = executor;
        return this;
    }

    public AsyncScannedFilePropertiesConsumer setPersistenceConsumer(ScanResultMessageConsumer consumer) {
        this.persistenceConsumer = consumer;
        return this;
    }

    public AsyncScannedFilePropertiesConsumer setFirstScan(boolean firstScan) {
        this.firstScan = firstScan;
        return this;
    }

    public boolean isFirstScan() {
        return firstScan;
    }

    public AsyncScannedFilePropertiesConsumer validate() {
        if (runId != null &&
                rootFolderId != null &&
                fileTypePredicate != null &&
                executor != null &&
                persistenceConsumer != null) {
            return this;
        } else {
            throw new RuntimeException("Consumer parameters are incomplete.");
        }
    }

    public AsyncScannedFilePropertiesConsumer setRunParameters(ScanTaskParameters scanTaskParameters) {
        setRunId(scanTaskParameters.getRunId());
        setFirstScan(scanTaskParameters.isFirstScan());
        setRootFolderId(scanTaskParameters.getRootFolderId());
        return this;
    }
}
