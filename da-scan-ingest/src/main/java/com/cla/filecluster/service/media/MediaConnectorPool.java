package com.cla.filecluster.service.media;

import com.cla.connector.domain.dto.media.MediaType;
import com.cla.connector.domain.dto.media.MediaConnectionDetailsDto;
import com.cla.connector.mediaconnector.FileMediaConnector;
import org.apache.commons.pool2.BasePooledObjectFactory;
import org.apache.commons.pool2.ObjectPool;
import org.apache.commons.pool2.PooledObject;
import org.apache.commons.pool2.impl.DefaultPooledObject;
import org.apache.commons.pool2.impl.GenericObjectPool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Media connectors pool
 * Created by uri on 26/09/2016.
 */
public class MediaConnectorPool implements ObjectPool<FileMediaConnector> {

    private static Logger logger = LoggerFactory.getLogger(MediaConnectorPool.class);

    private MediaConnectorFactory mediaConnectorFactory;
    private MediaConnectionDetailsDto connectionDetails;

    private ObjectPool<FileMediaConnector> objectPool;

    public MediaConnectorPool(MediaConnectorFactory mediaConnectorFactory, MediaConnectionDetailsDto connectionDetails) {
        this.mediaConnectorFactory = mediaConnectorFactory;
        this.connectionDetails = connectionDetails;
        MediaConnectorPoolFactory mediaConnectorPoolFactory = new MediaConnectorPoolFactory();
        objectPool = new GenericObjectPool<>(mediaConnectorPoolFactory);
    }

    public MediaType getMediaType() {
        try {
            FileMediaConnector mediaConnector = objectPool.borrowObject();
            return mediaConnector.getMediaType();
        } catch (Exception e) {
            logger.error("Failed to get MediaType. Media connector pool exhausted", e);
            throw new RuntimeException("Failed to get MediaType. Media connector pool exhausted", e);
        }
    }

    @Override
    public FileMediaConnector borrowObject() throws Exception {
        FileMediaConnector mediaConnector = objectPool.borrowObject();
        logger.debug("Borrow object {}", mediaConnector.getIdentifier());
        return mediaConnector;
    }

    @Override
    public void returnObject(FileMediaConnector obj) {
        try {
            objectPool.returnObject(obj);
        } catch (Exception e) {
            logger.error("Failed to return mediaConnectorWrapper to the pool", e);
        }
    }

    @Override
    public void invalidateObject(FileMediaConnector obj) {
        if (obj == null) {
            return;
        }
        logger.info("Invalidate media connector: {}", obj.getIdentifier());
        try {
            objectPool.invalidateObject(obj);
        } catch (Exception e) {
            logger.error("Failed to invalidate media connector", e);
        }
    }

    @Override
    public void addObject() throws Exception{
        objectPool.addObject();
    }

    @Override
    public int getNumIdle() {
        return objectPool.getNumIdle();
    }

    @Override
    public int getNumActive() {
        return objectPool.getNumActive();
    }

    @Override
    public void clear() throws Exception {
        objectPool.clear();
    }

    @Override
    public void close() {
        objectPool.close();
    }

    private class MediaConnectorPoolFactory extends BasePooledObjectFactory<FileMediaConnector> {

        @Override
        public FileMediaConnector create() throws Exception {
            return mediaConnectorFactory.createMediaConnector(connectionDetails);
        }

        @Override
        public PooledObject<FileMediaConnector> wrap(FileMediaConnector obj) {
            return new DefaultPooledObject<>(obj);
        }

    }
}
