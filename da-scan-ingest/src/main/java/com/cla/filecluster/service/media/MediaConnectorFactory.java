package com.cla.filecluster.service.media;


import com.cla.connector.domain.dto.media.MediaConnectionDetailsDto;
import com.cla.connector.mediaconnector.AbstractMediaConnector;
import com.cla.connector.mediaconnector.MediaConnector;

import java.util.Map;
import java.util.function.Consumer;

public interface MediaConnectorFactory {
	<T extends MediaConnector, S extends MediaConnectionDetailsDto> T getMediaConnector(S connectionDetails);

	MediaConnectorPool createMediaConnectionPool(MediaConnectionDetailsDto connectionDetails);

	<T extends MediaConnector, S extends MediaConnectionDetailsDto> T createMediaConnector(S connectionDetails);

	void setKeyValuePairsConsumer(Consumer<Map<String,String>> mapConsumer);
}
