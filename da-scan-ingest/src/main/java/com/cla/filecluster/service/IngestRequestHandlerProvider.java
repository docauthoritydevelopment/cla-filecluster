package com.cla.filecluster.service;

import com.cla.common.domain.dto.messages.IngestRequestMessage;

import java.io.FileNotFoundException;

public interface IngestRequestHandlerProvider {
	IngestRequestHandler getIngestRequestHandler(IngestRequestMessage requestMessage) throws FileNotFoundException;
}
