package com.cla.filecluster.service;

import com.cla.common.domain.dto.messages.IngestMessagePayload;
import com.cla.common.domain.dto.messages.IngestRequestMessage;
import com.cla.common.domain.dto.messages.IngestTaskParameters;
import com.cla.connector.utils.Pair;

import java.io.IOException;
import java.util.List;

public interface IngestRequestHandler {

    List<Pair<IngestTaskParameters, IngestMessagePayload>> handleIngestionRequest(
            IngestRequestMessage ingestRequestMessage) throws IOException;

}
