package com.cla.filecluster.service.media;

import com.cla.common.utils.EncryptionUtils;
import com.cla.connector.domain.dto.media.*;
import com.cla.connector.mediaconnector.MediaConnector;
import com.cla.connector.mediaconnector.ThrottlingStartledTurtle;
import com.cla.connector.mediaconnector.box.BoxMediaConnector;
import com.cla.connector.mediaconnector.exchange.Exchange365MediaConnector;
import com.cla.connector.mediaconnector.exchange.Exchange365Params;
import com.cla.connector.mediaconnector.exchange.auth.MSGraphAuthConfig;
import com.cla.connector.mediaconnector.exchange.throttling.MsGraphThrottlingParams;
import com.cla.connector.mediaconnector.fileshare.FileShareMediaConnector;
import com.cla.connector.mediaconnector.labeling.mip.MipMediaConnector;
import com.cla.connector.mediaconnector.microsoft.MSAppInfo;
import com.cla.connector.mediaconnector.microsoft.MSConnectionConfig;
import com.cla.connector.mediaconnector.microsoft.onedrive.OneDriveMediaConnector;
import com.cla.connector.mediaconnector.microsoft.sharepoint.SharePointMediaConnector;
import com.google.common.base.Strings;
import com.google.common.collect.Maps;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.net.HttpURLConnection;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;


/**
 * Factory for creating media connector instances
 * Created by uri on 08/08/2016.
 */
@SuppressWarnings("unchecked")
@Service
@Profile("!file_simulator")
public class MediaConnectorFactoryGeneral implements MediaConnectorFactory {

    private static final Logger logger = LoggerFactory.getLogger(MediaConnectorFactoryGeneral.class);

    private static final ThrottlingStartledTurtle THROTTLER = new ThrottlingStartledTurtle();

    /**
     * Maximum number of attempts to retry sending a call to Exchange 365 MS-Graph API.
     * Zero or negative values indicates to retry forever.
     */
    @Value("${exchange.365.throttling.max-retries:1000}")
    private int exchange365ThrottlingMaxRetries;

    @Value("${sharepoint.connection.retries:5}")
    private int sharepointMaxRetries;

    @Value("${sharepoint.page.size:1000}")
    private int sharepointPageSize;

    @Value("${sharepoint.max.file.size:20000000}")
    private long sharepointMaxFileSize;

    @Value("${sharepoint.httpclient.connect-timeout:5000}")
    private int sharepointConnectionTimeout;

    @Value("${sharepoint.httpclient.connection-request-timeout:5000}")
    private int sharepointConnectionRequestTimeout;

    @Value("${sharepoint.httpclient.socket-timeout:5000}")
    private int sharepointSocketTimeout;

    @Value("${sharepoint.service.request-timeout:20000}")
    private int sharepointServiceCallsTimeout;

    @Value("#{'${sharepoint.filter.chars:&#x14;,&#x2;,&#xB;}'.split(',')}")
    private String[] sharePointCharsToFilter;

    @Value("#{'${sharepoint.filter.folders:}'.split(',')}")
    private List<String> sharepointFilterFolders;

    @Value("${sharepoint.site.crawl.max.depth:-1}")
    private int sharepointSiteCrawlMaxDepth;

    @Value("${sharepoint.timeout.retry.interval:500}")
    private int sharepointTimeoutRetryInterval;

    @Value("${sharepoint.timeout.retry.backoff-multiplier:2}")
    private double sharepointBackOffMultiplier;

    @Value("${sharepoint.api.call.barrier:1000}")
    private int sharepointApiCallBarrier;

    @Value("${sharepoint.api.call.barrier.time.to.sleep:60000}")
    private int sharepointApiCallBarrierTTS;

    @Value("${sharepoint.api.max.connection.host:100}")
    private int sharepointMaxConnectionsPerHost;

    @Value("${sharepoint.connection.pool.close.stale.connection.ttl:60000}")
    private int sharepointConnectionPoolCloseStaleConnectionTTL;

    @Value("${debug.sharepoint.scan.iteration.times:-1}")
    private int sharepointScanIterationTimes;

    @Value("${debug.sharepoint.scan.folder.fail:#{null}}")
    private String sharepointFolderToFail;

    @Value("${sharepoint.anti-throttling.app-name:#{null}}")
    private String sharepointAppName;

    @Value("${sharepoint.anti-throttling.company-name:#{null}}")
    private String sharepointCompanyName;

    @Value("${sharepoint.anti-throttling.app-version:#{null}}")
    private String sharepointAppVersion;

    @Value("${sharepoint.special-char-support.enabled:false}")
    private boolean sharepointSupportSpecialCharacters;

    @Value("${onedrive.httpclient.connect-timeout:5000}")
    private int onedriveConnectionTimeout;

    @Value("${onedrive.httpclient.connection-request-timeout:5000}")
    private int onedriveConnectionRequestTimeout;

    @Value("${onedrive.timeout.retry.interval:500}")
    private int onedriveTimeoutRetryInterval;

    @Value("${onedrive.timeout.retry.backoff-multiplier:2}")
    private double onedriveBackOffMultiplier;

    @Value("${onedrive.httpclient.socket-timeout:5000}")
    private int onedriveSocketTimeout;

    @Value("${onedrive.service.request-timeout:20000}")
    private int onedriveServiceCallsTimeout;

    @Value("#{'${onedrive.filter.chars:&#x14;,&#x2;,&#xB;}'.split(',')}")
    private String[] oneDriveCharsToFilter;

    @Value("${onedrive.connection.retries:5}")
    private int onedriveMaxRetries;

    @Value("${onedrive.page.size:1000}")
    private int onedrivePageSize;

    @Value("${onedrive.max.file.size:20000000}")
    private long onedriveMaxFileSize;

    @Value("${onedrive.connection.pool.close.stale.connection.ttl:60000}")
    private int onedriveConnectionPoolCloseStaleConnectionTTL;

    @Value("${onedrive.api.max.connection.host:100}")
    private int onedriveMaxConnectionsPerHost;

    @Value("${onedrive.api.call.barrier:1000}")
    private int onedriveApiCallBarrier;

    @Value("${onedrive.api.call.barrier.time.to.sleep:60000}")
    private int onedriveApiCallBarrierTTS;

    @Value("#{'${onedrive.filter.folders:}'.split(',')}")
    private List<String> onedriveFilterFolders;

    @Value("${onedrive.anti-throttling.app-name:#{null}}")
    private String onedriveAppName;

    @Value("${onedrive.anti-throttling.company-name:#{null}}")
    private String onedriveCompanyName;

    @Value("${onedrive.anti-throttling.app-version:#{null}}")
    private String onedriveAppVersion;

    @Value("${onedrive.special-char-support.enabled:false}")
    private boolean onedriveSupportSpecialCharacters;

    @Value("${scan.file-share.preserve-accessed-time.active:false}")
    private Boolean preserveAccessedTimeActive;

    @Value("${scan.file-share.impersonate-before-action.active:true}")
    private Boolean impersonateBeforeAction;

    @Value("d0c#Pocs${media.encryption.suffix:328753985628935629386}")
    private String encryptionKey;

    @Value("${media.encryption.algorithm:AES/ECB/PKCS5Padding}")
    private String encryptionAlgorithm;

    @Value("${box.throttling.retries:5}")
    private int boxThrottlingRetries;

    @Value("${box.parallel.scan:3}")
    private int boxParallelScan;

    @Value("${exchange.oauth.prefetch-token-before-expiry:60000}")
    private int exchangePrefetchTokenBeforeExpiry;

    @Value("${exchange.oauth.connection.read-timeout:5000}")
    private int exchangeReadTimeout;

    @Value("${exchange.connection.connect-timeout:5000}")
    private int exchangeConnectTimeout;

    @Value("${exchange.oauth.max-attempts:3}")
    private int exchangeMaxAttempts;

    @Value("${exchange.oauth.retry-sleep-interval:1000}")
    private int exchangeRetrySleepInterval;

    @Value("${mip.login.oauth-token-address:https://login.microsoftonline.com/common/oauth2/token}")
    private String mipLoginUrl;

    @Value("${share-permission-script.path:.\\sharePermission.ps1}")
    private String pathToSharePermissionScript;

    /**
     * This eventually is set to the MS-Graph's underlying {@link HttpURLConnection#setConnectTimeout(int)}.
     * Zero means no timeout
     * <p/>
     * Note 30,000 millis is the default of MS-Graph java sdk defined in:
     *
     */
    @Value(("${exchange.ms-graph.connection.connect.timeout.millis:30000}"))
    private int exchangeConnectionConnectTimeout;

    /**
     * This eventually is set to the MS-Graph's underlying {@link HttpURLConnection#setReadTimeout(int)}.
     * Zero means no timeout
     * <p/>
     * Note 30,000 millis is the default of MS-Graph java sdk defined in:
     *
     */
    @Value(("${exchange.ms-graph.connection.read.timeout.millis:30000}"))
    private int exchangeConnectionReadTimeout;


    private Consumer<Map<String, String>> mapConsumer;

    private final Map<Long, MediaConnector> mediaConnectors = Maps.newHashMap();

    public <T extends MediaConnector, S extends MediaConnectionDetailsDto> T getMediaConnector(S connectionDetails) {
        return getOrCreateMediaConnector(connectionDetails, false);
    }

    private <T extends MediaConnector, S extends MediaConnectionDetailsDto> T getOrCreateMediaConnector
            (S connectionDetails, @SuppressWarnings("SameParameterValue") boolean forceCreate) {


        if (forceCreate) {
            logger.debug("Forcing creation of {}", connectionDetails);
            return createMediaConnector(connectionDetails);
        }
        synchronized (mediaConnectors) {
            T wrapper = (T) mediaConnectors.get(connectionDetails.getId());
            // if not found in cache, or the connection details were re-read from the DB - create
            if (wrapper == null || connectionDetails.getTimestamp() > wrapper.getConnectionDetailsRefreshed()) {
                wrapper = createMediaConnector(connectionDetails);
                if (wrapper != null && (connectionDetails.getId() != null || connectionDetails.getMediaType() == MediaType.FILE_SHARE)) {
                    wrapper.setConnectionDetailsRefreshed(connectionDetails.getTimestamp());
                    mediaConnectors.put(connectionDetails.getId(), wrapper);
                }
            }
            return wrapper;
        }
    }

    @Override
    public void setKeyValuePairsConsumer(Consumer<Map<String, String>> mapConsumer) {
        this.mapConsumer = mapConsumer;
    }

    public MediaConnectorPool createMediaConnectionPool(MediaConnectionDetailsDto connectionDetails) {
        return new MediaConnectorPool(this, connectionDetails);
    }

    public <T extends MediaConnector, S extends MediaConnectionDetailsDto> T createMediaConnector(S connectionDetails) {
        if (connectionDetails == null) {
            throw new RuntimeException("Unable to create media connector. Connection details are missing.");
        }

        if (connectionDetails.getMediaType().equals(MediaType.SHARE_POINT) || connectionDetails.getMediaType().equals(MediaType.ONE_DRIVE)) {
            SharePointConnectionParametersDto param = ((MicrosoftConnectionDetailsDtoBase) connectionDetails).getSharePointConnectionParametersDto();
            param.setPassword(decrypt(param.getPassword()));
        }

        logger.debug("Creating connector {}", connectionDetails);
        MediaConnector connector;
        String decryptedPass;

        switch (connectionDetails.getMediaType()) {
            case SHARE_POINT:
                SharePointMediaConnector.SharePointMediaConnectorBuilder spBuilder = SharePointMediaConnector.builder()
                        .withSharePointConnectionParametersDto(((SharePointConnectionDetailsDto) connectionDetails).getSharePointConnectionParametersDto())
                        .withMaxRetries(sharepointMaxRetries)
                        .withPageSize(sharepointPageSize)
                        .withCharactersToFilter(sharePointCharsToFilter)
                        .withMaxFileSize(sharepointMaxFileSize)
                        .withFoldersToFilter(sharepointFilterFolders)
                        .withSiteCrawlMaxDepth(sharepointSiteCrawlMaxDepth)
                        .withIsSpecialCharactersSupported(sharepointSupportSpecialCharacters)
                        .withScanIterationTimes(sharepointScanIterationTimes)
                        .withFolderToFail(sharepointFolderToFail);

                MSConnectionConfig spConnConfig = MSConnectionConfig.builder()
                        .serviceClientTimeoutRetryInterval(sharepointTimeoutRetryInterval)
                        .serviceClientTimeout(sharepointServiceCallsTimeout)
                        .connectionTimeout(sharepointConnectionTimeout)
                        .socketTimeout(sharepointSocketTimeout)
                        .backoffMultiplier(sharepointBackOffMultiplier)
                        .connectionRequestTimeout(sharepointConnectionRequestTimeout)
                        .maxConnectionsPerHost(sharepointMaxConnectionsPerHost)
                        .apiCallBarrier(sharepointApiCallBarrier)
                        .apiCallBarrierTTS(sharepointApiCallBarrierTTS)
                        .connectionPoolCloseStaleConnectionTTL(sharepointConnectionPoolCloseStaleConnectionTTL)
                        .build();

                spBuilder.withConnectionConfig(spConnConfig);

                if (sharepointAppName != null || sharepointCompanyName != null || sharepointAppVersion != null) {
                    spBuilder = spBuilder.withAppInfo(new MSAppInfo(sharepointCompanyName, sharepointAppName, sharepointAppVersion));
                }
                connector = spBuilder.build();

                break;
            case ONE_DRIVE:
                OneDriveMediaConnector.OneDriveMediaConnectorBuilder odBuilder = OneDriveMediaConnector.builder()
                        .withSharePointConnectionParametersDto(((OneDriveConnectionDetailsDto) connectionDetails).getSharePointConnectionParametersDto())
                        .withDoInit(true)
                        .withMaxRetries(onedriveMaxRetries)
                        .withPageSize(onedrivePageSize)
                        .withCharactersToFilter(oneDriveCharsToFilter)
                        .withMaxFileSize(onedriveMaxFileSize)
                        .withFoldersToFilter(onedriveFilterFolders)
                        .withIsSpecialCharactersSupported(onedriveSupportSpecialCharacters);

                MSConnectionConfig odConnectionConfig = MSConnectionConfig.builder()
                        .serviceClientTimeoutRetryInterval(onedriveTimeoutRetryInterval)
                        .serviceClientTimeout(onedriveServiceCallsTimeout)
                        .connectionTimeout(onedriveConnectionTimeout)
                        .socketTimeout(onedriveSocketTimeout)
                        .backoffMultiplier(onedriveBackOffMultiplier)
                        .connectionRequestTimeout(onedriveConnectionRequestTimeout)
                        .maxConnectionsPerHost(onedriveMaxConnectionsPerHost)
                        .apiCallBarrier(onedriveApiCallBarrier)
                        .apiCallBarrierTTS(onedriveApiCallBarrierTTS)
                        .connectionPoolCloseStaleConnectionTTL(onedriveConnectionPoolCloseStaleConnectionTTL)
                        .build();

                odBuilder.withConnectionConfig(odConnectionConfig);

                if (onedriveAppName != null || onedriveCompanyName != null || onedriveAppVersion != null) {
                    odBuilder = odBuilder.withAppInfo(new MSAppInfo(onedriveCompanyName, onedriveAppName, onedriveAppVersion));
                }
                connector = odBuilder.build();

                break;
            case BOX:
                BoxConnectionDetailsDto boxConnectionDetailsDto = (BoxConnectionDetailsDto) connectionDetails;
                BoxMediaConnector boxMediaConnector = new BoxMediaConnector(decrypt(boxConnectionDetailsDto.getJwt()), boxThrottlingRetries, boxParallelScan);

                boxMediaConnector.setLastEventStreamPositionConsumer(mapConsumer);
                //endregion
                connector = boxMediaConnector;
                break;
            case EXCHANGE_365:
                Exchange365ConnectionDetailsDto connectionDetailsDto =
                        (Exchange365ConnectionDetailsDto) connectionDetails;

                Exchange365ConnectionParametersDto exchangeConnectionParametersDto =
                        connectionDetailsDto.getExchangeConnectionParametersDto();

                exchangeConnectionParametersDto.setConnectionConnectTimeout(exchangeConnectionConnectTimeout);
                exchangeConnectionParametersDto.setConnectionReadTimeout(exchangeConnectionReadTimeout);

                decryptedPass = decrypt(exchangeConnectionParametersDto.getPassword());
                exchangeConnectionParametersDto.setPassword(decryptedPass);

                MSGraphAuthConfig authConfig = MSGraphAuthConfig.builder()
                        .connectTimeout(exchangeConnectTimeout)
                        .readTimeout(exchangeReadTimeout)
                        .retrySleepInterval(exchangeRetrySleepInterval)
                        .maxAttempts(exchangeMaxAttempts)
                        .prefetchTokenBeforeExpiry(exchangePrefetchTokenBeforeExpiry)
                        .build();


                MsGraphThrottlingParams throttlingParams =
                        new MsGraphThrottlingParams(THROTTLER, exchange365ThrottlingMaxRetries);
                Exchange365Params exchange365Params =
                        new Exchange365Params(exchangeConnectionParametersDto, authConfig, throttlingParams);
                connector = new Exchange365MediaConnector(exchange365Params);
                break;
            case MIP:
                MIPConnectionDetailsDto mipConnectionDetailsDto = (MIPConnectionDetailsDto)connectionDetails;

                MIPConnectionParametersDto mipParams =
                        mipConnectionDetailsDto.getMIPConnectionParametersDto();

                decryptedPass = decrypt(mipParams.getPassword());
                mipParams.setPassword(decryptedPass);
                connector = new MipMediaConnector(mipLoginUrl, mipConnectionDetailsDto);
                ((MipMediaConnector)connector).init();
                break;
            default:
                connector = new FileShareMediaConnector(preserveAccessedTimeActive, impersonateBeforeAction, pathToSharePermissionScript);
        }

        if (connectionDetails.getId() != null) {
            synchronized (mediaConnectors) {
                mediaConnectors.put(connectionDetails.getId(), connector);
            }
        }

        return (T) connector;
    }

    private String decrypt(String encryptedVal) {
        if (Strings.isNullOrEmpty(encryptedVal)) {
            return encryptedVal;
        }
        String decryptedValue = encryptedVal;
        encryptedVal = encryptedVal.substring(2);
        try {
            decryptedValue = EncryptionUtils.decryptSymmetric(encryptedVal, encryptionKey, encryptionAlgorithm);
        } catch (Exception e) {
            logger.error("Failed to decrypt string", e);
        }
        return decryptedValue;
    }
}