package com.cla.filecluster.mediaproc.connectors;

import com.cla.common.domain.dto.messages.ScanResultMessage;

/**
 * Interface for scan result message consumers
 * Created by uri on 12-Mar-17.
 */
public interface ScanResultMessageConsumer {

    void accept(ScanResultMessage scanResultMessage);

}
