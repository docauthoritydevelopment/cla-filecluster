package com.cla.filecluster.mediaproc.connectors;

import com.cla.common.domain.dto.messages.ScanRequestMessage;

/**
 * Interface for scan result message consumers
 * Created by uri on 12-Mar-17.
 */
public interface ScanRequestMessageConsumer {

    public void accept(ScanRequestMessage request);

}
