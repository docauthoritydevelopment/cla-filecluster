package com.cla.common.utils.tokens;

import com.google.common.base.Charsets;
import com.google.common.collect.Lists;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.core.LowerCaseFilter;
import org.apache.lucene.analysis.core.StopFilter;
import org.apache.lucene.analysis.core.WhitespaceTokenizer;
import org.apache.lucene.analysis.en.EnglishAnalyzer;
import org.apache.lucene.analysis.miscellaneous.WordDelimiterFilter;
import org.apache.lucene.analysis.standard.StandardTokenizer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.joining;

@Component
public class NGramTokenizerHashed implements DocAuthorityTokenizer {
    private static final String SEPARATOR = " ";

    private static final char[] hexCvtArr = {
            '0', '1', '2', '3', '4', '5', '6', '7'
            , '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'
            , 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N'
            , 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V'
    };

    private Map<Long, String> debugHashMapper = null;

    @Value("${tokemMatcher.isIngestCleartext:false}")
    private boolean isIngestCleartext;

    @Value("${tokemMatcher.reuseTextTokenizer:true}")
    private boolean reuseTextTokenizer;

    private static final boolean regexSearchAndReplace = true;

    private String removeValuesBellowLength;
    //All punctuation ->   "\\p{P}\\p{S}";
    @Value("${tokenizer.ignoreChars:[\\-]}")
    // No need to remove ' . it will damage 's stemming and it will be removed by the tokenizer.
    private String ignoreChars;
    @Value("${tokenizer.separatorChars:}")
    // no need for , replacement as it is properly handled by the tokenizer.
    private String separatorChars;

    @Value("#{'${removalStrs:999999999}'.split(',')}")
    private List<String> removalStrs;

    @Value("${ingestion.token.minValueLength:3}")
    private int minValueLength;

    /*
     * TODO: Create tokenStreamFactory class to allow generation of multiple tokenStreams per need:
     * 		- Ngrams per language
     * 		- TokenMatcher doc ingestion
     * 		- ToeknMatcher Entity property processing
     */

    /*
     * Entity extraction tokenizer
     */
    private final static Pattern zeroLeadingNumber = Pattern.compile("\\b(0+?)([1-9][0-9\\.\\,/]*)\\b");
    private final static Pattern commaPattern = Pattern.compile("([^0-9\\s])[\\.;\\,]");
    private final static Pattern commaZeroSearchPattern = Pattern.compile("(\\b(0+?)([1-9][0-9\\.\\,/]*)\\b)|(([^0-9s])[\\.;\\,])");

    public class NgramTokenizerObject implements TokenizerObject {

        private final Tokenizer tokenizer;
        private final TokenStream tokenStream;

        public NgramTokenizerObject(final String input) {
            tokenizer = getFillTokensTokenizer(input);
            tokenStream = getFillTokensTokenStream(this.tokenizer);
            tokenStream.addAttribute(CharTermAttribute.class);
        }

        public NgramTokenizerObject(final Tokenizer tokenizer, final TokenStream tokenStream) {
            super();
            this.tokenizer = tokenizer;
            this.tokenStream = tokenStream;
        }

        public Tokenizer getTokenizer() {
            return tokenizer;
        }

        public TokenStream getTokenStream() {
            return tokenStream;
        }

        public DocAuthorityTokenStream setInput(final String input) throws IOException {
            tokenizer.setReader(getFillTokensStringReader(input));
            return new NgramTokenStream(tokenStream);
        }

    }

    private static String fillTokensStringFilter(final String input) {
        /*
         * Pre-processing of the input string prior to tokenization:
         * zero leading numbers are stemmed prior to tokenization: "000222,33" -> "000222,33 222,33"
         * Replace a non-number comma/period/semicolon with a space
         */
        if (regexSearchAndReplace && !commaZeroSearchPattern.matcher(input).find()) {
            return input;
        }
        return zeroLeadingNumber.matcher(commaPattern.matcher(input).replaceAll("$1 ")).replaceAll("$1$2 $2");
    }

    private static StringReader getFillTokensStringReader(final String input) {
        return new StringReader(fillTokensStringFilter(input));
    }

    private static Tokenizer getFillTokensTokenizer(final String input) {
        StringReader fillTokensStringReader = getFillTokensStringReader(input);
        WhitespaceTokenizer whitespaceTokenizer = new WhitespaceTokenizer();
        try {
            whitespaceTokenizer.setReader(fillTokensStringReader);
        } catch (Exception e) {
            throw new RuntimeException("Failed to set whiteSpaceTokenizer reader",e);
        }
        return whitespaceTokenizer;
    }

    private static TokenStream getFillTokensTokenStream(final Tokenizer t) {
        TokenStream tokenStream;
        tokenStream = new WordDelimiterFilter(t,
                WordDelimiterFilter.GENERATE_WORD_PARTS | WordDelimiterFilter.CATENATE_NUMBERS |/*WordDelimiterFilter.CATENATE_WORDS|*/
                        WordDelimiterFilter.PRESERVE_ORIGINAL | WordDelimiterFilter.STEM_ENGLISH_POSSESSIVE,
                null);
        tokenStream = new LowerCaseFilter(tokenStream);
//			tokenStream = new StopFilter(tokenStream, EnglishAnalyzer.getDefaultStopSet());
        return tokenStream;
    }

    private static DocAuthorityTokenStream getNewTokenStream(final String input) {
        final Tokenizer t = getFillTokensTokenizer(input);
        final TokenStream ts = getFillTokensTokenStream(t);
        ts.addAttribute(CharTermAttribute.class);
        return new NgramTokenStream(ts);
    }


    // TODO: replace all String.format("%X",i) -> getStringToken(i)
    private static StringBuilder addStringToken(final StringBuilder sb, final long i) {
        return addStringToken(sb, i, null);
    }

    private static StringBuilder addStringToken(final StringBuilder sb, long i, final Character space) {
        if (space != null) {
            sb.append(space);
        }
        if (i == 0L) {
            sb.append('0');
        }
        while (i != 0L) {
            sb.append(hexCvtArr[(int) (i & 0x1f)]);
            i >>= 5;
        }
        return sb;
    }

    public static String getStringToken(final long i) {
        final StringBuilder sb = new StringBuilder(16);            // using 32 long alphabet (5 bits) results max token of 13 chars
        addStringToken(sb, i);
//		return sb.reverse().toString();	// We can skip the 'reverse' since we're not interested in the real HEX value
        return sb.toString();    // We can skip the 'reverse' since we're not interested in the real HEX value
    }

    @PostConstruct
    private void init() {
        removeValuesBellowLength = String.valueOf(minValueLength - 1);
    }

    /*
     * N-Gram tokenizer for the doc text
     */
    public List<Long> getHashlist(final String input, final int len) {
        if (input.length() == 0) {
            return null;
        }

        try (
                StandardTokenizer standardTokenizer = new StandardTokenizer();
                TokenStream tokenStream = new StopFilter(
                        new LowerCaseFilter(
                                standardTokenizer),
                        EnglishAnalyzer.getDefaultStopSet());
        ) {
            standardTokenizer.setReader(new StringReader(input));
            final List<Long> result = Lists.newArrayList();
            final CharTermAttribute charTermAttribute = tokenStream.addAttribute(CharTermAttribute.class);
            final LinkedList<String> fifo = new LinkedList<>();
            tokenStream.reset();
            while (tokenStream.incrementToken()) {
                final String token = charTermAttribute.toString();
                fifo.add(token);
                if (fifo.size() >= len) {
                    final String str = fifo.stream().collect(Collectors.joining(SEPARATOR));
                    result.add(MD5Long(str));
                    fifo.remove();
                }
            }
            // Get residue in case of not enough tokens
            if (result.size() == 0 && fifo.size() > 0) {
                result.add(MD5Long(fifo.stream().collect(Collectors.joining(SEPARATOR))));
            }
            return result;
        } catch (final Exception e) {
            throw new RuntimeException(e);
        }
    }

//	public List<String> getNgrams(final String input, final int len) {
//		if (input.length() == 0) {
//			return null;
//		}
//
//		try (
//			TokenStream tokenStream =  new StopFilter(
//					new LowerCaseFilter(
//						new StandardTokenizer(new StringReader(input))),
//					EnglishAnalyzer.getDefaultStopSet())
//		){
//			final List<String> result = Lists.newArrayList();
//			final CharTermAttribute charTermAttribute = tokenStream.addAttribute(CharTermAttribute.class);
//			final LinkedList<String> fifo = new LinkedList<>();
//			tokenStream.reset();
//			while (tokenStream.incrementToken()) {
//				final String token = charTermAttribute.toString();
//				fifo.add(token);
//				if (fifo.size() >= len) {
//					result.add(fifo.stream().collect(Collectors.joining(SEPARATOR)));
//					fifo.remove();
//				}
//			}
//			// Get residue in case of not enough tokens
//			if (result.size() == 0 && fifo.size() > 0) {
//				result.add(fifo.stream().collect(Collectors.joining(SEPARATOR)));
//			}
//			return result;
//		}
//		catch(final Exception e){
//			throw new RuntimeException(e);
//		}
//	}

    /*
     * Token matcher list of hashed tokens ready for Solr indexing
     */
    public String getHash(final String text, final HashedTokensConsumer hashedTokensConsumer) {
        return getHash(text, hashedTokensConsumer, null);
    }

    public String getHash(final String text, final HashedTokensConsumer hashedTokensConsumer, final String locationDesc) {
        return getHashReuseTokenizer(text, hashedTokensConsumer, locationDesc, null);
    }

    public String getHashReuseTokenizer(final String text, final HashedTokensConsumer hashedTokensConsumer, final TokenizerObject ngramTokenizerObject) {
        return getHashReuseTokenizer(text, hashedTokensConsumer, null, ngramTokenizerObject);
    }

    @Override
    public String getHashReuseTokenizer(final String text, final HashedTokensConsumer hashedTokensConsumer, final String locationDesc,
                                        final TokenizerObject tokenizerObject) {
        final String filteredText = filter(text);
        if (filteredText == null || filteredText.isEmpty()) {
            return "";
        }
        final List<?> tokens = fillTokens(filteredText, hashedTokensConsumer, locationDesc, tokenizerObject);
        if (tokens == null || tokens.isEmpty()) {
            return "";
        }
        String hashedStr;
        if (isIngestCleartext) {
            // list of string tokens
            hashedStr = tokens.stream().map(Object::toString).collect(joining(" "));
        } else {
            // list of Long hash codes
            hashedStr = tokens.stream().map(l -> String.format("%X", l)).collect(joining(" "));
        }
        return hashedStr;
    }

    public String filter(final String input) {
        if (input == null || input.isEmpty()) {
            return null;
        }
        String output = input;
        if (separatorChars != null && !separatorChars.isEmpty()) {
            output = output.replaceAll(separatorChars, " ");        // Separators replaced with space)
        }
        if (ignoreChars != null && !ignoreChars.isEmpty()) {
            output = output.replaceAll(ignoreChars, "");        // Ignored chars to be removed
        }
        for (final String s : removalStrs) {
            output = output.replaceAll(s, " ");
        }
//		if(minValueLength>1)
//			output = output.replaceAll("\\b\\w{1,"+removeValuesBellowLength+"}\\b\\s?", "");
        return (output.isEmpty()) ? null : output;
    }

    public long MD5Long(final String text) {
        try {
            final long hash = MD5Long(text.getBytes("UTF-8"/* "iso-8859-1" */));
            if (debugHashMapper != null) {
                debugHashMapper.put(hash, text);
            }
            return hash;
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }

    }


    public static long MD5Long(final byte[] bytes) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        final byte[] md5hash = MD5(bytes);
        // Trim digest into the size of a long (8 bytes). MD5 uniformity should
        // guaranty uniformity within long value.
        final ByteBuffer buffer = ByteBuffer.allocate(8);
        buffer.put(md5hash, 0, 8);
        buffer.flip();// need flip
        return buffer.getLong();
    }

    private static byte[] MD5(final byte[] bytes) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        final MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(bytes);
        return md.digest();
    }

    public static byte[] MD5(final String text) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        return MD5(text.getBytes(Charsets.UTF_8));
    }

//	public Set<Object> getTokenMatcherList(final String input) {
//		if (input.length() == 0) return null;
//		final Set<Object> tokens = new HashSet<>();
//		fillTokens(input, tokens);
//		return tokens;
//	}


    /*
     * Free text tokeniztion and hashing for earch and entity extraction
     */
    private List<Object> fillTokens(final String input, final HashedTokensConsumer hashedTokensConsumer, final String locationDesc) {
        return fillTokens(input, hashedTokensConsumer, locationDesc,
                null);
    }

    private List<Object> fillTokens(final String input, final HashedTokensConsumer hashedTokensConsumer, final String locationDesc,
                                    final TokenizerObject tokenizerObject) {
        if (input == null || input.isEmpty()) {
            return null;
        }

        DocAuthorityTokenStream tokenStream = null;
        try {
            /*
             * Tokenizer requirements:
             * 	normalize, remove 's (Jon's -> Jon), remove '.' (I.B.M -> IBM), lowercase, include originals.
             */
            final List<Object> tokens = Lists.newArrayList();
            final List<Object> deb = Lists.newArrayList();
            tokenStream = ((!reuseTextTokenizer) || tokenizerObject == null) ?
                    getNewTokenStream(input) : tokenizerObject.setInput(input);
//			final CharTermAttribute charTermAttribute = tokenStream.addAttribute(CharTermAttribute.class);
            final CharTermAttribute charTermAttribute = (CharTermAttribute) tokenStream.getCharTermAttribute();

            tokenStream.reset();
            while (tokenStream.incrementToken()) {
                String token = charTermAttribute.toString();
                token = trimToken(token);
                if (token.length() >= minValueLength) {
                    final Long tHash = MD5Long(token);
                    tokens.add(tHash);
                    deb.add(token);
                    if (hashedTokensConsumer != null) {
                        hashedTokensConsumer.accept(token, tHash, locationDesc);
                    }
                    if (debugHashMapper != null) {
                        debugHashMapper.put(tHash, token);
                    }
                    final String hStem = hebrewSimpleStem(token);
                    if (hStem != null) {
                        final Long sHash = MD5Long(hStem);
                        tokens.add(sHash);
                        deb.add(hStem);
                        if (hashedTokensConsumer != null) {
                            hashedTokensConsumer.accept(hStem, sHash, locationDesc);
                        }
                        if (debugHashMapper != null) {
                            debugHashMapper.put(sHash, hStem);
                        }
                    }
                }
            }
            if (isIngestCleartext) {
                return deb;
            }
            return tokens;
        } catch (final Exception e) {
            throw new RuntimeException(e);
        } finally {
            try {
                if (tokenStream != null) {
                    tokenStream.close();
                }
            } catch (final IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

//	private List<Object> getTokens(final String input) {
////		if (input.length() == 0) return null;
////		final List<Object> tokens = new ArrayList<>();
////		fillTokens(input, tokens);
//		return fillTokens(input);
//	}


    private final static String hebPrefixLetterStemming = "\ud791\ud79c\ud7a9\ud795\ud79b\ud97e";    // UTF8 BET LAMED SHIN VAV CHAF MEM

    // private final static String hebPrefixLetterStemming = "׳‘׳�׳©׳•׳›׳�";
    public static String hebrewSimpleStem(final String token) {
        final char c = token.charAt(0);
        return (hebPrefixLetterStemming.indexOf(c) >= 0 && token.length() > 2) ? token.substring(1) : null;
    }

    public static String trimToken(final String token) {
        int first = -1;
        int last = 0;
        final int l = token.length();
        for (int i = 0; i < l; ++i) {
            final char c = token.charAt(i);
            if (Character.isAlphabetic(c) || Character.isDigit(c)) {
                if (first < 0) {
                    first = i;
                }
                last = i;
            }
        }
        return (first >= 0) ? token.substring(first, last + 1) : "";
    }

    public Map<Long, String> getDebugHashMapper() {
        return debugHashMapper;
    }

    public void setDebugHashMapper(final Map<Long, String> debugHashMapper) {
        this.debugHashMapper = debugHashMapper;
    }

    public TokenizerObject getTokenizerObject() {
        return getTokenizerObject("");
    }

    public TokenizerObject getTokenizerObject(final String input) {
        return new NgramTokenizerObject(input);
    }

}
