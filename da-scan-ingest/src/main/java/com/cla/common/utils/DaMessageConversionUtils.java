package com.cla.common.utils;

import com.cla.common.domain.dto.messages.*;
import com.cla.common.domain.dto.messages.control.ControlMessagePayload;
import com.cla.common.domain.dto.messages.control.ControlRequestMessage;
import com.cla.common.domain.dto.messages.control.ControlResultMessage;
import com.cla.connector.domain.dto.messages.PhaseMessagePayload;
import com.cla.connector.domain.dto.messages.ScanSharePermissions;
import com.cla.connector.domain.dto.messages.ScanTaskParameters;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Strings;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Utility methods for converting remote processing messages from strings to objects and back
 * Created by uri on 08-Mar-17.
 */
public class DaMessageConversionUtils {

    private final static ObjectMapper mapper;

    static {
        mapper = new ObjectMapper();
        mapper.configure(JsonParser.Feature.ALLOW_SINGLE_QUOTES, true);
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);     // Allow null fields to be sent (and ignored during serialization)
    }

    /**
     * Utility methods for converting scan remote processing messages
     */
    public static class Scan {

        public static String serializeTaskParameters(ScanTaskParameters scanTaskParameters) {
            return serialize(scanTaskParameters);
        }

        public static ScanTaskParameters deserializeTaskParameters(String scanTaskParameters) {
            if (StringUtils.isEmpty(scanTaskParameters)) {
                return null;
            }
            try {
                return mapper.readValue(scanTaskParameters, ScanTaskParameters.class);
            } catch (IOException e) {
                throw new RuntimeException("Failed to convert scan task parameters from string [" + scanTaskParameters + "]", e);
            }
        }

        public static String serializeSharePermissions(ScanSharePermissions scanSharePermissions) {
            return serialize(scanSharePermissions);
        }

        public static ScanSharePermissions deserializeSharePermissions(String scanSharePermissions) {
            if (StringUtils.isEmpty(scanSharePermissions)) {
                return null;
            }
            try {
                return mapper.readValue(scanSharePermissions, ScanSharePermissions.class);
            } catch (IOException e) {
                throw new RuntimeException("Failed to convert scan share permissions from string [" + scanSharePermissions + "]", e);
            }
        }

        public static ScanRequestMessage deserializeRequestMessage(String data) {
            if (Strings.isNullOrEmpty(data)) {
                throw new RuntimeException("Empty message.");
            }
            try {
                return mapper.readValue(data, ScanRequestMessage.class);
            } catch (IOException e) {
                throw new RuntimeException("Failed to convert scan request message from string [" + data + "]", e);
            }
        }

        public static String serializeRequestMessage(ScanRequestMessage scanRequestMessage) {
            return serialize(scanRequestMessage);
        }


        public static String serializeResultMessage(ScanResultMessage scanResultMessage){
            prepareMessageForSerialization(scanResultMessage);
            return serialize(scanResultMessage);
        }

        public static ScanResultMessage deserializeResultMessage(String data) {
            if (StringUtils.isEmpty(data)) {
                return null;
            }

            try {
                ScanResultMessage message = mapper.readValue(data, ScanResultMessage.class);
                finalizeResultMessageDeserialization(message);
                return message;
            } catch (IOException e) {
                throw new RuntimeException("Failed to convert scan result message from string [" + data + "]", e);
            }
        }

        public static List<ScanResultMessage> deserializeResultMessages(List<String> list) {
            return list.stream().map(Scan::deserializeResultMessage).collect(Collectors.toList());
        }

        public static String serializeResultMessages(List<ScanResultMessage> buffer, Set<Long> taskIds) {
            ScanResultMessage scanResultMessage = new ScanResultMessage();
            scanResultMessage.setTaskIds(taskIds);
            buffer.forEach(Scan::prepareMessageForSerialization);
            scanResultMessage.setContainedMessages(buffer);
            return serialize(scanResultMessage);
        }

        private static void prepareMessageForSerialization(ProcessingPhaseMessage message) {
            // replace payload with payload string to allow differentiated deserialization
            if (message.hasPayload() ) {
                try {
                    String payloadStr = mapper.writeValueAsString(message.getPayload());
                    message.setPayloadStr(payloadStr);
                    message.setPayload(null);
                } catch (JsonProcessingException e) {
                    throw new RuntimeException("Failed to serialize scan result payload from string.", e);
                }
            }
        }

        private static void finalizeResultMessageDeserialization(ScanResultMessage message) {
            List<ScanResultMessage> containedMessages = message.getContainedMessages();
            if (containedMessages != null && containedMessages.size() > 0){
                containedMessages.forEach(Scan::finalizeResultMessageDeserialization);
            }

            // deserialize payload string and set the payload object
            finalizeMessageDeserialization(message);
        }

        private static void finalizeMessageDeserialization(ProcessingPhaseMessage message) {
            // deserialize payload string and set the payload object
            if (!Strings.isNullOrEmpty(message.getPayloadStr())) {
                try {
                    PhaseMessagePayload payload = (PhaseMessagePayload) mapper.readValue(message.getPayloadStr(),
                            message.getPayloadType().getRealType());
                    message.setPayload(payload);
                    message.setPayloadStr(null);
                } catch (Exception e) {
                    throw new RuntimeException("Failed to serialize scan result payload from string.", e);
                }
            }
        }
    }

    /**
     * Utility methods for converting ingest remote processing messages
     */
    @SuppressWarnings("unchecked")
    public static class Ingest{

        public static IngestResultMessage deserializeResultMessage(String data) throws IOException, ClassNotFoundException {
            IngestResultMessage message = mapper.readValue(data, IngestResultMessage.class);
            if (message.hasPayload()) {
                IngestMessagePayload payload = (IngestMessagePayload) mapper.readValue(message.getPayloadStr(),
                        message.getPayloadType().getRealType());
                message.setPayload(payload);
            }
            return message;
        }

        public static String serializeResultMessage(IngestResultMessage response) throws JsonProcessingException {
            PhaseMessagePayload payload = response.getPayload();
            if (response.getPayloadType() == null && payload != null) {
                response.setPayloadType(MessagePayloadType.typeOf(payload));
            }
            String payloadStr = mapper.writeValueAsString(payload);
            response.setPayloadStr(payloadStr);
            response.setPayload(null);
            return mapper.writeValueAsString(response);
        }

        public static String serializeRequestMessage(IngestRequestMessage request) throws JsonProcessingException {
            return mapper.writeValueAsString(request);
        }

        public static IngestRequestMessage deserializeRequestMessage(String data){
            if (Strings.isNullOrEmpty(data)) {
                throw new RuntimeException("Empty message.");
            }
            try {
                return mapper.readValue(data, IngestRequestMessage.class);
            } catch (IOException e) {
                throw new RuntimeException("Failed to convert ingest request message from string [" + data + "]", e);
            }
        }

    }

    /**
     * Utility methods for converting file content remote processing messages
     */
    @SuppressWarnings("unchecked")
    public static class  FileContent {

        public static FileContentResultMessage deserializeResultMessage(String data) throws IOException, ClassNotFoundException {
            return mapper.readValue(data, FileContentResultMessage.class);
        }

        //I am sorry to put it here, but... well... damn...
		public static FoldersListResultMessage deserializeFoldersListResultMessage(String data) throws IOException{
			return mapper.readValue(data, FoldersListResultMessage.class);
		}

		public static KeyValuePayloadResp deserializeKeyValueResultMessage(String data) throws IOException{
			return mapper.readValue(data, KeyValuePayloadResp.class);
		}

		public static String serializeResultMessage(FileContentResultMessage response) throws JsonProcessingException {
            PhaseMessagePayload payload = response.getPayload();
            if (response.getPayloadType() == null && payload != null) {
                response.setPayloadType(MessagePayloadType.typeOf(payload));
            }
            String payloadStr = mapper.writeValueAsString(payload);
            response.setPayloadStr(payloadStr);
            return mapper.writeValueAsString(response);
        }

        public static String serializeRequestMessage(FileContentRequestMessage request) throws JsonProcessingException {
            return mapper.writeValueAsString(request);
        }

        public static FileContentRequestMessage deserializeRequestMessage(String request) throws IOException {
            return mapper.readValue(request, FileContentRequestMessage.class);
        }
    }


    /**
     * Utility methods for converting file content remote processing messages
     */
    @SuppressWarnings("unchecked")
    public static class  Control {

        public static ControlResultMessage deserializeResultMessage(String data) throws IOException, ClassNotFoundException {
            ControlResultMessage message = mapper.readValue(data, ControlResultMessage.class);
            if (message.hasPayload()) {
                ControlMessagePayload payload = (ControlMessagePayload) mapper.readValue(message.getPayloadStr(),
                        message.getPayloadType().getRealType());
                message.setPayload(payload);
            }
            return message;
        }

        public static String serializeResultMessage(ControlResultMessage response) throws JsonProcessingException {
            PhaseMessagePayload payload = response.getPayload();
            if (response.getPayloadType() == null && payload != null) {
                response.setPayloadType(MessagePayloadType.typeOf(payload));
            }
            String payloadStr = mapper.writeValueAsString(payload);
            response.setPayloadStr(payloadStr);
            response.setPayload(null);
            return mapper.writeValueAsString(response);
        }

        public static String serializeRequestMessage(ControlRequestMessage request) throws JsonProcessingException {
            PhaseMessagePayload payload = request.getPayload();
            if (request.getPayloadType() == null && payload != null) {
                request.setPayloadType(MessagePayloadType.typeOf(payload));
            }
            String payloadStr = payload == null ? null : mapper.writeValueAsString(payload);
            request.setPayloadStr(payloadStr);
            request.setPayload(null);
            return mapper.writeValueAsString(request);
        }


        public static ControlRequestMessage deserializeRequestMessage(String data) throws IOException {
            ControlRequestMessage message = mapper.readValue(data, ControlRequestMessage.class);
            if (message.hasPayload()) {
                ControlMessagePayload payload = (ControlMessagePayload) mapper.readValue(message.getPayloadStr(),
                        message.getPayloadType().getRealType());
                message.setPayload(payload);
            }
            return message;
        }


    }

    public static class ContentCheck {

        public static ContentMetadataResponseMessage deserializeResultMessage(String data) {
            try {
                ContentMetadataResponseMessage message = mapper.readValue(data, ContentMetadataResponseMessage.class);
                return message;
            } catch (IOException e) {
                throw new RuntimeException("Failed to convert content check response message from string [" + data + "]", e);
            }
        }

        public static String serializeResultMessage(ContentMetadataResponseMessage response) throws JsonProcessingException {
            return mapper.writeValueAsString(response);
        }

        public static String serializeRequestMessage(ContentMetadataRequestMessage request) throws JsonProcessingException {
            return mapper.writeValueAsString(request);
        }

        public static ContentMetadataRequestMessage deserializeRequestMessage(String data){
            if (Strings.isNullOrEmpty(data)) {
                throw new RuntimeException("Empty message.");
            }
            try {
                return mapper.readValue(data, ContentMetadataRequestMessage.class);
            } catch (IOException e) {
                throw new RuntimeException("Failed to convert ingest request message from string [" + data + "]", e);
            }
        }

    }

    public static class Labeling {

        public static LabelingResponseMessage deserializeResultMessage(String data) {
            try {
                LabelingResponseMessage message = mapper.readValue(data, LabelingResponseMessage.class);
                return message;
            } catch (IOException e) {
                throw new RuntimeException("Failed to convert labeling response message from string [" + data + "]", e);
            }
        }

        public static String serializeResultMessage(LabelingResponseMessage response) throws JsonProcessingException {
            return mapper.writeValueAsString(response);
        }


        public static String serializeRequestMessage(LabelRequestMessage request) throws JsonProcessingException {
            return mapper.writeValueAsString(request);
        }

        public static LabelRequestMessage deserializeRequestMessage(String data){
            if (Strings.isNullOrEmpty(data)) {
                throw new RuntimeException("Empty message.");
            }
            try {
                return mapper.readValue(data, LabelRequestMessage.class);
            } catch (IOException e) {
                throw new RuntimeException("Failed to convert labeling request message from string [" + data + "]", e);
            }
        }
    }

    public static <T> String serialize(T object){
        try {
            return mapper.writeValueAsString(object);
        }
        catch (JsonProcessingException e) {
            throw new RuntimeException("Failed to serialize " + object.getClass().getSimpleName(), e);
        }
    }
}
