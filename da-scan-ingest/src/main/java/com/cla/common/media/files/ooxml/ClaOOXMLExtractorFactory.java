package com.cla.common.media.files.ooxml;

import com.cla.common.media.files.word.ClaXWPFDocument;
import org.apache.commons.io.input.CloseShieldInputStream;
import org.apache.poi.POIXMLDocument;
import org.apache.poi.POIXMLTextExtractor;
import org.apache.poi.extractor.ExtractorFactory;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.openxml4j.opc.PackageAccess;
import org.apache.poi.xslf.extractor.XSLFPowerPointExtractor;
import org.apache.poi.xslf.usermodel.XMLSlideShow;
import org.apache.poi.xssf.extractor.XSSFEventBasedExcelExtractor;
import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.tika.exception.TikaException;
import org.apache.tika.io.TikaInputStream;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.mime.MediaType;
import org.apache.tika.parser.EmptyParser;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.microsoft.ooxml.*;
import org.apache.tika.parser.pkg.ZipContainerDetector;
import org.apache.xmlbeans.XmlException;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;

/**
 * Copied from original factory in order to enable parsing process customization
 *
 * @see OOXMLExtractorFactory
 * Created by vladi on 4/2/2017.
 */
public class ClaOOXMLExtractorFactory {

    public static void parse(
            InputStream stream, ContentHandler baseHandler,
            Metadata metadata, ParseContext context, int sizeThreshold, int totalObjectsLimit)
            throws IOException, SAXException, TikaException {
        Locale locale = context.get(Locale.class, Locale.getDefault());
        ExtractorFactory.setThreadPrefersEventExtractors(true);

        //-------------------------------------------------------
        // CLA customization: set the limits
        //-------------------------------------------------------
        ClaXWPFDocument.setSizeThresholds(sizeThreshold, totalObjectsLimit);

        try {
            OOXMLExtractor extractor;
            OPCPackage pkg;

            // Locate or Open the OPCPackage for the file
            TikaInputStream tis = TikaInputStream.cast(stream);
            if (tis != null && tis.getOpenContainer() instanceof OPCPackage) {
                pkg = (OPCPackage) tis.getOpenContainer();
            } else if (tis != null && tis.hasFile()) {
                pkg = OPCPackage.open(tis.getFile().getPath(), PackageAccess.READ);
                tis.setOpenContainer(pkg);
            } else {
                InputStream shield = new CloseShieldInputStream(stream);
                pkg = OPCPackage.open(shield);
            }

            // Get the type, and ensure it's one we handle
            MediaType type = ZipContainerDetector.detectOfficeOpenXML(pkg);
            if (type == null || ClaOOXMLParser.getUnSupportedTypes().contains(type)) {
                // Not a supported type, delegate to Empty Parser
                EmptyParser.INSTANCE.parse(stream, baseHandler, metadata, context);
                return;
            }
            metadata.set(Metadata.CONTENT_TYPE, type.toString());

            //-------------------------------------------------------
            // CLA customization: ClaExtractorFactory.createCustomExtractor ***
            //-------------------------------------------------------
            POIXMLTextExtractor poiExtractor = ClaExtractorFactory.createCustomExtractor(pkg);

            POIXMLDocument document = poiExtractor.getDocument();
            if (poiExtractor instanceof XSSFEventBasedExcelExtractor) {
                //-------------------------------------------------------
                // CLA customization: ClaXSSFExcelExtractorDecorator ***
                //-------------------------------------------------------
                extractor = new ClaXSSFExcelExtractorDecorator(
                        context, (XSSFEventBasedExcelExtractor) poiExtractor, locale, sizeThreshold, totalObjectsLimit);
            } else if (document == null) {
                throw new TikaException(
                        "Expecting UserModel based POI OOXML extractor with a document, but none found. " +
                                "The extractor returned was a " + poiExtractor
                );
            } else if (document instanceof XMLSlideShow) {
                extractor = new XSLFPowerPointExtractorDecorator(
                        context, (XSLFPowerPointExtractor) poiExtractor);
            } else if (document instanceof XWPFDocument) {
                extractor = new XWPFWordExtractorDecorator(
                        context, (XWPFWordExtractor) poiExtractor);
            } else {
                extractor = new POIXMLTextExtractorDecorator(context, poiExtractor);
            }

            // Get the bulk of the metadata first, so that it's accessible during
            //  parsing if desired by the client (see TIKA-1109)
            extractor.getMetadataExtractor().extract(metadata);

            // Extract the text, along with any in-document metadata
            extractor.getXHTML(baseHandler, metadata, context);
        }
        catch (IllegalArgumentException e) {
            if (e.getMessage() != null &&
                    e.getMessage().startsWith("No supported documents found")) {
                throw new TikaException(
                        "TIKA-418: RuntimeException while getting content"
                                + " for thmx and xps file types", e);
            } else {
                throw new TikaException("Error creating OOXML extractor", e);
            }
        }
        catch (OpenXML4JException | XmlException e) {
            throw new TikaException("Error creating OOXML extractor", e);
        }
    }

}
