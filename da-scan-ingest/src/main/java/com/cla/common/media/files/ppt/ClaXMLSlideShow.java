package com.cla.common.media.files.ppt;

import com.cla.common.media.files.xml.CurtailedPackagePart;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.PackagePart;
import org.apache.poi.xslf.usermodel.XMLSlideShow;

/**
 * Power Point XMLSlideShow wrapper for purposes of limiting number of XML objects in file
 *
 * Created by vladi on 4/3/2017.
 */
@SuppressWarnings("unused")
public class ClaXMLSlideShow extends XMLSlideShow {

    @Override
    protected PackagePart getCorePart() {
        try {
            return new CurtailedPackagePart(super.getCorePart());
        }
        catch (InvalidFormatException e) {
            throw new RuntimeException("Failed to create curtailed part.", e);
        }
    }

}
