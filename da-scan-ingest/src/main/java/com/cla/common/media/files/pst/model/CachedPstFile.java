package com.cla.common.media.files.pst.model;

import com.cla.common.domain.pst.PstPath;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.pff.PSTAttachment;
import com.pff.PSTException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

/**
 * An extension of {@link PstFile} to be used in a cache.
 * <p/>
 * This class counts the 'handles' of the underlying file that are currently held. As long as there are handles held,
 * the underlying file should not be released. A cached instance should be checked periodically whether it can be
 * released and if so, released (by calling {@link #release()}.
 * <p/>
 * One handle is counted for the file itself when calling {@link #open()}. This handle will be released when
 * {@link #close()} is called (typically at the end of a try-with-resources block). A closed file may be reopened by
 * calling open() again. This allows the cache to hold a closed file and reopen it when requested. A newly created
 * CachedPstFile is considered closed. This doesn't mean it is inoperable. It only means that it might be released if
 * no other handles for it are held (and that will make it inoperable).
 * <p/>
 * Other handles are counted for every InputStream returned by {@link #registeredInputStream(InputStream)}.
 * Each such handle will be released when the corresponding InputStream is closed.
 * <p/>
 * Additionally this class maintain the last update time of the handles count to allow the file to be released only
 * when deemed stale.
 * <p/>
 * @implNote The correctness of this class' implementation depends on that only the same one single thread will modify
 * the volatile handleCount and lastHandlesUpdateTime fields. These fields are modified only by the
 * {@link #incrementHandlesCount()} and {@link #decrementHandlesCount()} private methods. These methods are called only
 * from {@link #open()} and the InputStream returned from {@link #registeredInputStream(InputStream)}. This means that
 * The thread that opens a CachedPstFile must be the only one that handles its registered InputStream and the only one
 * that closes it.
 * <p/>
 * Created By Itai Marko
 */
class CachedPstFile extends PstFile {

    private static final Logger logger = LoggerFactory.getLogger(CachedPstFile.class);

    private boolean opened = false;     // indicates the handle for this file is held
    private boolean released = false;   // indicates the underlying file is released
    private volatile int handleCount = 0;
    private volatile long lastHandlesUpdateTime = Long.MAX_VALUE;
    private final String cacheKeyName;  // holds the name of the cache key that may not be available

    CachedPstFile(ClaFilePropertiesDto pstFileProps, String cacheKeyName) throws PSTException, IOException {
        super(pstFileProps);
        this.cacheKeyName = cacheKeyName;
    }

    /**
     * Counts one handle for holding this file.
     * This prevents a file to be released before any {@link PstEntryInputStream} is opened.
     * <p/>
     * This method can be used to reopen the file after it was closed.
     */
    void open() {
        if (!opened) {
            incrementHandlesCount();
            opened = true;
        }
    }

    /**
     * This doesn't really close the underlying PSTFile. It just decrements the handle counted for this file .
     * The actual closing of the underlying PSTFile happens in the {@link #release} method.
     */
    @Override
    public void close() {
        if (opened) {
            decrementHandlesCount();
            opened = false;
        }
    }

    /**
     * Actually releases the underlying PSTFile
     */
    public void release() {
        if (!released) {
            if (handleCount == 0) {
                released = true;
                super.close(); // release the underlying file
            } else {
                String stackTrace = Arrays.toString(Thread.currentThread().getStackTrace());
                logger.error("Attempted to release a PstFile with handleCount of {}. StackTrace: {}",
                        handleCount, stackTrace);
            }
        }
    }

    int getHandleCount() {
        return handleCount;
    }

    long getLastHandlesUpdateTime() {
        return lastHandlesUpdateTime;
    }

    private void incrementHandlesCount() {
        // Cancel inspection as the impl of PstFileFacade only modifies handleCount from a single thread and
        // reading a soon-to-be-old value will only cause the cached file to be released in th next cycle.
        //noinspection NonAtomicOperationOnVolatileField
        handleCount++;
        lastHandlesUpdateTime = System.currentTimeMillis();
    }

    private void decrementHandlesCount() {
        if (handleCount == 0) {
            throw new IllegalStateException("Attempted to close PstFile with no handles. Filename: " + getPstFileProps().getFileName());
        }
        // Cancel inspection as the impl of PstFileFacade only modifies handleCount from a single thread and
        // reading a soon-to-be-old value will only cause the cached file to be released in th next cycle.
        //noinspection NonAtomicOperationOnVolatileField
        handleCount--;
        lastHandlesUpdateTime = System.currentTimeMillis();
    }

    /**
     * Wraps and returns the given InputStream with an InputStream that reports its opening and closing to this PstFile.
     * <p/>
     * This prevents this file from closing the underlying {@link java.io.RandomAccessFile} before the returned
     * InputStream was closed. Any {@link PstEntry} which returns an InputStream that's mapped to the underlying
     * RandomAccessFile should wrap it using this method. Otherwise the file may be already closed when the InputStream
     * is attempted to be read.
     */
    @Override
    InputStream registeredInputStream(InputStream in) {
        return new PstEntryInputStream(in);
    }

    @Override
    PstAttachment createChildPstAttachment(PSTAttachment libpstAttachment, PstPath pstAttachmentPath, PstEmailMessage emailMsg) {
        // Override the parent's pre-loading of the attachment since this class maintains a handle for each InputStream
        return new PstAttachment(libpstAttachment, this, pstAttachmentPath, emailMsg);
    }

    String getCacheKeyName() {
        return cacheKeyName;
    }

    @Override
    public String toString() {
        return "CachedPstFile{" +
                "cacheKeyName=" + cacheKeyName +
                ", opened=" + opened +
                ", released=" + released +
                ", handleCount=" + handleCount +
                ", lastHandlesUpdateTime=" + lastHandlesUpdateTime +
                '}' +
                ", parent: " + super.toString();
    }

    /**
     * A decorator of InputStream that adds the functionality of counting opening an closing of the underlying
     * InputStream as open handles of this PstFile.
     */
    private class PstEntryInputStream extends FilterInputStream  {

        private PstEntryInputStream(InputStream in) {
            super(in);
            incrementHandlesCount();
        }

        @Override
        public void close() throws IOException {
            decrementHandlesCount();
            in.close();
        }
    }
}
