package com.cla.common.media.files.pst.model;

import com.pff.PSTRecipient;

import javax.mail.Message;
import java.util.HashMap;
import java.util.Map;

/**
 * Created By Itai Marko
 */
public class PstRecipient extends PstAddress {

    private static final Map<Integer, Message.RecipientType> recipientTypesMap;

    static {
        recipientTypesMap = new HashMap<>(3);
        recipientTypesMap.put(PSTRecipient.MAPI_TO, Message.RecipientType.TO);
        recipientTypesMap.put(PSTRecipient.MAPI_CC, Message.RecipientType.CC);
        recipientTypesMap.put(PSTRecipient.MAPI_BCC, Message.RecipientType.BCC);
    }

    private final Message.RecipientType recipientType;

    public PstRecipient(String name, String emailAddress, int pstRecipientType) {
        super(name, emailAddress);
        this.recipientType = recipientTypesMap.get(pstRecipientType);
        if (recipientType == null) {
            throw new IllegalStateException("Unexpected recipient type: " + pstRecipientType + " of recipient: " + emailAddress);
        }
    }

    public Message.RecipientType getRecipientType() {
        return recipientType;
    }
}
