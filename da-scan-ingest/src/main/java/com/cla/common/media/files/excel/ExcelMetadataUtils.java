package com.cla.common.media.files.excel;

import com.cla.common.domain.dto.ClaDocProperties;
import com.cla.common.domain.dto.mediaproc.excel.ExcelSheet;
import com.cla.common.domain.dto.mediaproc.excel.ExcelSheetMetaData;
import com.cla.common.domain.dto.mediaproc.excel.ExcelWorkbook;
import com.cla.common.domain.dto.mediaproc.excel.ExcelWorkbookMetaData;
import org.apache.poi.POIXMLProperties;
import org.apache.poi.hpsf.DocumentSummaryInformation;
import org.apache.poi.hpsf.PropertySet;
import org.apache.poi.hpsf.SummaryInformation;
import org.apache.poi.hpsf.UnexpectedPropertySetTypeException;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.DirectoryEntry;
import org.apache.poi.poifs.filesystem.DocumentEntry;
import org.apache.poi.poifs.filesystem.DocumentInputStream;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openxmlformats.schemas.officeDocument.x2006.extendedProperties.CTProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Map;
import java.util.function.ToIntFunction;

import static java.util.stream.Collectors.toList;

/**
 *
 * Created by uri on 19/10/2015.
 */
public class ExcelMetadataUtils {

    private static Logger logger = LoggerFactory.getLogger(ExcelMetadataUtils.class);


    public static ClaDocProperties extractProperties(final Workbook wb, final POIFSFileSystem poifs, final String path) {
        final ClaDocProperties res = new ClaDocProperties();

        if (wb != null && wb instanceof XSSFWorkbook) {
            final XSSFWorkbook doc = (XSSFWorkbook) wb;
            final POIXMLProperties dp = doc.getProperties();
            final POIXMLProperties.CoreProperties dcop = dp.getCoreProperties();
            final POIXMLProperties.ExtendedProperties dep = dp.getExtendedProperties();
            final CTProperties depup = dep.getUnderlyingProperties();

            res.setCreator(spaceIfNull(dcop.getCreator()));
            res.setSubject(spaceIfNull(dcop.getSubject()));
            res.setTitle(spaceIfNull(dcop.getTitle()));
            res.setKeywords(spaceIfNull(dcop.getKeywords()));
            res.setAppNameVer(spaceIfNull(depup.getApplication()) + "/" + spaceIfNull(depup.getAppVersion()));
            res.setTemplate(spaceIfNull(depup.getTemplate()));
            res.setCreated(dcop.getCreated());
            res.setModified(dcop.getModified());
            res.setCompany(spaceIfNull(depup.getCompany()));
            res.setWords(depup.getWords());
            res.setPages(depup.getPages());
            res.setChars(depup.getCharactersWithSpaces());
            res.setParagraphs(depup.getParagraphs());
            res.setLines(depup.getLines());
            final int sec = depup.getDocSecurity();
            res.setSecAppCode(sec);
            res.setSecPasswordProtected((sec & 1) != 0);
            res.setSecReadonlyRecommended((sec & 2) != 0);
            res.setSecReadonlyEnforced((sec & 4) != 0);
            res.setSecLockedForAnnotations((sec & 8) != 0);
        }
        else
        if (wb != null && wb instanceof HSSFWorkbook) {
            //final HSSFWorkbook doc = (HSSFWorkbook) wb;
            logger.info("Unhandled extractProperties case for file {}", path);
        }

        // Applicable for HSSFWorkbook (XLS files)
        if (poifs != null) {
            extractMetadataFromPoiFS(poifs, res,path);
        }

        if (logger.isTraceEnabled()) {
            logger.trace("Extracted props: " + res.toString());
        }

        return res;
    }

    public static void summarizeWorkbookMetadata(ExcelWorkbook workbook) {
        Map<String, ExcelSheet> sheets = workbook.getSheets();
        Map<String, String> sheetFullNames = workbook.getSheetFullNamesMap();
        ExcelWorkbookMetaData metaData = workbook.getMetadata();

        // metaData.setFileName(fileName);
        metaData.setCellsSkipped(sumSheetsStat(ExcelSheetMetaData::getCellsSkipped, sheets));
        metaData.setNumOfBlanks(sumSheetsStat(ExcelSheetMetaData::getNumOfBlanks, sheets));
        metaData.setNumOfCells(sumSheetsStat(ExcelSheetMetaData::getNumOfCells, sheets));
        metaData.setNumOfErrors(sumSheetsStat(ExcelSheetMetaData::getNumOfErrors, sheets));
        metaData.setNumOfFormulas(sumSheetsStat(ExcelSheetMetaData::getNumOfFormulas, sheets));
        metaData.setNumOfSheets(sheets.size());
        metaData.setSheetNames(sheetFullNames.keySet().stream()
                .sorted()
                .map(sheetFullNames::get)
                .collect(toList()));
    }


    private static void extractMetadataFromPoiFS(POIFSFileSystem poifs, ClaDocProperties res, String path) {
        SummaryInformation si = null;
        DocumentSummaryInformation dsi = null;
        DocumentEntry siEntry = null;

        final DirectoryEntry dir = poifs.getRoot();
        try {
            siEntry = (DocumentEntry)dir.getEntry(SummaryInformation.DEFAULT_STREAM_NAME);
        } catch (final FileNotFoundException e) {
            logger.warn("Can not get si.DEFAULT_STREAM entry from {}. Exception: {}",path, e.getMessage());
        }
        DocumentInputStream dis = null;
        if (siEntry!=null) {
            try {
                dis = new DocumentInputStream(siEntry);
            } catch (final IOException e) {
                logger.warn("Can not create DocumentInputStream from {}. Exception: {}",path, e.getMessage());
            }
        }
        PropertySet ps = null;
        if (dis!=null) {
            try {
                ps = new PropertySet(dis);
                dis.close();
                siEntry = null;
            } catch (final Exception e) {
                logger.warn("Can not create PropertySet on {}. Exception: {}",path, e.getMessage());
            }
        }

        if (ps!=null) {
            try {
                si = new SummaryInformation(ps);
            } catch (final UnexpectedPropertySetTypeException e) {
                logger.warn("Can not create SummaryInformation on {}. Exception: {}",path, e.getMessage());
            }
        }

        try {
            siEntry = (DocumentEntry)dir.getEntry(DocumentSummaryInformation.DEFAULT_STREAM_NAME);
        } catch (final FileNotFoundException e) {
            logger.warn("Can not get dsi.DEFAULT_STREAM entry from {}. Exception: {}",path, e.getMessage());
        }
        dis = null;
        if (siEntry!=null) {
            try {
                dis = new DocumentInputStream(siEntry);
            } catch (final IOException e) {
                logger.warn("Can not create DocumentInputStream from {}. Exception: {}",path, e.getMessage());
            }
        }
        ps = null;
        if (dis!=null) {
            try {
                ps = new PropertySet(dis);
                dis.close();
            } catch (final Exception e) {
                logger.warn("Can not create PropertySet from {}. Exception: {}",path, e.getMessage());
            }
        }
        if (ps!=null) {
            try {
                dsi = new DocumentSummaryInformation(ps);
            } catch (final UnexpectedPropertySetTypeException e) {
                logger.warn("Can not create SummaryInformation from {}. Exception: {}",path, e.getMessage());
            }
        }
        if (si != null) {
            res.setCreator(si.getAuthor());
            //res.setCompany(company);
            res.setSubject(si.getSubject());
            res.setTitle(si.getTitle());
            res.setKeywords(si.getKeywords());
            res.setAppNameVer(si.getApplicationName()+"/"+si.getRevNumber());
            res.setTemplate(si.getTemplate());
            if (si.getCreateDateTime()!=null) {
                res.setCreated(si.getCreateDateTime());
            }
            if (si.getLastSaveDateTime()!=null) {
                res.setModified(si.getLastSaveDateTime());
            }
            res.setWords(si.getWordCount());
            res.setChars(si.getCharCount());
            res.setPages(si.getPageCount());
            final int sec = si.getSecurity();
            res.setSecAppCode(sec);
            res.setSecPasswordProtected((sec & 1)!=0);
            res.setSecReadonlyRecommended((sec & 2)!=0);
            res.setSecReadonlyEnforced((sec & 4)!=0);
            res.setSecLockedForAnnotations((sec & 8)!=0);
        }
        if (dsi != null) {
            res.setCompany(dsi.getCompany());
            res.setLines(dsi.getLineCount());
            res.setParagraphs(dsi.getParCount());
        }
    }


    private static int sumSheetsStat(ToIntFunction<ExcelSheetMetaData> func, Map<String, ExcelSheet> sheets){
        return sheets.values().stream()
                .map(ExcelSheet::getMetaData)
                .mapToInt(func)
                .sum();
    }

    private static String spaceIfNull(final String str) {
        return (str == null || str.equals("null")) ? "" : str;
    }
}
