package com.cla.common.media.files.xml;

import com.beust.jcommander.internal.Maps;
import com.cla.common.utils.thread.ClaPropertySet;
import com.cla.common.utils.thread.ClaThreadPropertiesSet;
import com.google.common.base.Charsets;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.BufferOverflowException;
import java.util.Arrays;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 *
 * Created by vladi on 2/16/2017.
 */
public class XMLAwareCurtailingPipe {
    private static final Logger logger = LoggerFactory.getLogger(XMLAwareCurtailingPipe.class);

    private enum XMLMachineState {
        COPY_CHAR,
        TAG_START_FOUND,
    }

    private static final Pattern EXTRACT_PATTERN = Pattern.compile("ENCODING[ \\t]*=[ \\t]*[\"']([ A-Z0-9\\-]*)[\"']");

    private final static String TEXT_NAME = "w:t";
    // Bit map action codes
    private static final int NAME_ACTION_STATS = 0x01;
    private static final int NAME_ACTION_CHECK_ENCODING = 0x02;

    private int totalObjectsLimit = 50000;         // The LIMIT - 0 = no limit

    private static Map<String, Integer> specialNames;

    static {
        specialNames = Maps.newHashMap();
        specialNames.put("xml", NAME_ACTION_CHECK_ENCODING);
        specialNames.put("w:t", NAME_ACTION_STATS);
        specialNames.put("w:tbl", NAME_ACTION_STATS);
        specialNames.put("w:p", NAME_ACTION_STATS);
    }

    private boolean countText = false;
    private int textCounter = 0;
    private int textSkippedCounter = 0;
    private int skippedObjectEndsCounter = 0;
    private int nonTagSkippedCharCounter = 0;
    private int nonTagCharCounter = 0;

    private int allGlobalCounter = 0;
    private int skippedObjectsCounter = 0;

    private final Map<String, Integer> globalStats = Maps.newHashMap();

    private int level = 0;
    private boolean skipBytes = false;
    private int skipLevel = 0;
    private XMLMachineState xmlMachineState = XMLMachineState.COPY_CHAR;

    private static final int bufSize = 10*1024;
    private static final int maxBufSize = 100*1000*1024;
    private static final int bufSizeIncreaseFactor = 10;
    private char[] buf = new char[bufSize];
    private int accumulatedCount = 0;

    private StringBuffer nameSb;

    private InputStream originalInputStream;        // original stream to read from
    private PipedOutputStream outStream;            // internal stream to write into
    private PipedInputStream inputStream = null;
    private Thread myThread = null;

    private ClaPropertySet claPropertySet;
    private String path;

    private boolean objectNameFound = false;
    private boolean isObjectEnd = false;
    private boolean isNoBodyObject = false;
    private boolean nameEnd = false;

    //----------------------------------------------------------------------------------------------------------------

    public XMLAwareCurtailingPipe(InputStream originalInputStream) {
        this.originalInputStream = originalInputStream;
    }

    public InputStream getCurtailedInputStream() throws IOException {
        if (inputStream != null) {
            return inputStream;
        }
        inputStream = new PipedInputStream();
        outStream = new PipedOutputStream(inputStream);

        claPropertySet = ClaThreadPropertiesSet.getThreadLocalPropertiesSet();
        path = (claPropertySet.getString(ClaThreadPropertiesSet.KEY_PATH) == null) ?
                "-" : claPropertySet.getString("path");

        long curTime = System.currentTimeMillis();
        myThread = new Thread(this::streamData);
        myThread.setName("xmlPipe-" + Thread.currentThread().getName() + "-" + (curTime % 10000L));
        myThread.start();

        return inputStream;
    }

    public void shutDown() {
        if (myThread != null) {
            try {
                inputStream.close();
            }
            catch (IOException e) {}
            try {
                outStream.close();
            }
            catch (IOException e) {}
            myThread.interrupt();
        }
    }

    // Keep for reference test
    private void streamDataCopy(){
        try (
            Reader reader = new InputStreamReader(originalInputStream, Charsets.UTF_8);
            Writer writer = new OutputStreamWriter(outStream, Charsets.UTF_8);
        ) {
            int charInt;
            while ((charInt = reader.read()) != -1) {
                writer.write((char)charInt);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (Exception e) {
            logger.error("Exception: state={} buf[{}]={}", xmlMachineState.toString(),
                    accumulatedCount, (accumulatedCount <= 0) ? "" : String.valueOf(buf, 0, Integer.min(accumulatedCount, buf.length)));
            throw new RuntimeException(e);
        }
        finally {
            try {
                outStream.close();
            } catch (IOException e) {}
            try {
                originalInputStream.close();
            } catch (IOException e) {}
        }
    }

    private void resetObjectFlags() {
        objectNameFound = false;
        isObjectEnd = false;
        isNoBodyObject = false;
        nameEnd = false;
    }

    /**
     * read from originalInputStream and write to out
     * whoever called the getCurtailedInputStream() will read the resulting data
     */
    private void streamData() {
        logger.debug("Start XML filter streaming. Object limit={}", totalObjectsLimit);
        try (
            Reader reader = new InputStreamReader(originalInputStream, Charsets.UTF_8);
            Writer writer = new OutputStreamWriter(outStream, Charsets.UTF_8)
        ) {
            int charInt = -1;
            int prevChar = -1;
            while ((charInt = reader.read()) != -1) {
                switch (xmlMachineState) {
                    case TAG_START_FOUND:
                        // Looking for new item's type
                        addCharToBuf((char) charInt);
                        switch ((char) charInt) {
                            // It's a closing item
                            case '/':
                                if (objectNameFound) {
                                    nameEnd = true;
                                }
                                isObjectEnd = (prevChar == '<');
                                break;
                            case '?':
                                isNoBodyObject = true;
                                if (objectNameFound) {
                                    nameEnd = true;
                                }
                                break;
                            case '>':
                                isNoBodyObject = isNoBodyObject || (prevChar == '/');

                                if (isObjectEnd && !isNoBodyObject) {
                                    processTagObjectEnd(writer);
                                    endObject();
                                } else if (objectNameFound) {
                                    startObject(nameSb.toString());
                                    processTagObjectStart(writer);
                                    if (isNoBodyObject) {
                                        processTagObjectEnd(writer);
                                        endObject();
                                    }
                                } else {
                                    logger.warn("Unknown state");
                                }
                                xmlMachineState = XMLMachineState.COPY_CHAR;
                                break;
                            case ' ':
                            case '\n':
                            case '\t':
                                if (objectNameFound) {
                                    nameEnd = true;
                                }
                                // white space, just move on
                                break;
                            default:
                                // looking for name token
                                if (!objectNameFound) {
                                    objectNameFound = true;
                                    nameSb = new StringBuffer();
                                }
                                if (!nameEnd) {
                                    nameSb.append((char) charInt);
                                }
                        }
                        break;

                    case COPY_CHAR:
                        while (charInt != (int) '<' && charInt >= 0) {
                            processChar(writer, charInt);
                            charInt = reader.read();
                        }
                        addCharToBuf((char) charInt);
                        resetObjectFlags();
                        xmlMachineState = XMLMachineState.TAG_START_FOUND;
                        break;
                }
                prevChar = charInt;
            }
        } catch (IOException|BufferOverflowException e) {
            throw new RuntimeException(e);
        } catch (Exception e) {
            logger.error("Exception: state={} buf[{}]={}", xmlMachineState.toString(),
                    accumulatedCount, (accumulatedCount <= 0) ? "" : String.valueOf(buf, 0, Integer.min(accumulatedCount, buf.length)), e);
            throw new RuntimeException(e);
        } finally {
            try {
                outStream.close();
            } catch (IOException e) {
            }
            try {
                originalInputStream.close();
            } catch (IOException e) {}
            processStats();
        }
    }

    private void addCharToBuf(char ch) throws BufferOverflowException {
        // Check if buf size exceeded
        if (accumulatedCount == buf.length) {
            if (buf.length >= maxBufSize) {
                logger.warn("XML buffer size reached max allowed allocation ({}). State={}. Aborting processing.", xmlMachineState);
                buf = null;
                throw new BufferOverflowException();
            }
            // Allocate new buffer with bigger size
            int newSize = bufSizeIncreaseFactor * buf.length;
            logger.info("XML buffer size reached allocated size {}. Re-alocating size of {}. State={} buf start={}...",
                    buf.length, newSize, xmlMachineState, String.valueOf(buf, 0, Integer.min(accumulatedCount, 100)));
            buf = Arrays.copyOf(buf, newSize);
        }
        buf[accumulatedCount++] = ch;
    }

    private void processStats() {
        logger.debug("Global stats: allObj={} textCount={} skippedText={} freeText={} skippedObj={} skippedObjEnds={} skippedFreeText={}",
                allGlobalCounter, textCounter, textSkippedCounter, nonTagCharCounter, skippedObjectsCounter, skippedObjectEndsCounter, nonTagSkippedCharCounter);
        logger.debug("Stats per type: {}",
                globalStats.entrySet().stream()
                        .map( e -> "(" + e.getKey() + "=" + e.getValue() + ")")
                        .collect(Collectors.joining(",")));
    }

    private void startObject(String name) {
        level++;
        countText = TEXT_NAME.equals(name);
        nameHandler(level, name);
//        logger.debug("Start object {} at level {}", name, level);
    }

    private void endObject() {
//        logger.debug("End object at level {}", level);
        countText = false;
        level--;
    }

    private void nameHandler(int level, String name) {
        ++allGlobalCounter;
        nameAction(name);
    }

    private int nameAction(String name) {
        Integer nameAction = specialNames.get(name);
        if (nameAction == null) {
            name = "Others";
            nameAction = NAME_ACTION_STATS;
        }
        else {
            if ((nameAction & NAME_ACTION_CHECK_ENCODING) != 0) {
                if (accumulatedCount > 0 && accumulatedCount < buf.length) {
                    String tag = String.valueOf(buf, 0, accumulatedCount);
                    String encoding = getEncodingValue(tag);
                    if (encoding == null) {
                        logger.debug("Missing encoding in XML tag '{}'. Path={}", tag, path);
                    } else {
                        if (!"UTF-8".equals(encoding) && !"UTF8".equals(encoding)) {
                            logger.warn("XML filter: unsupported encoding {}. Path={}. Tag={}", encoding, path, tag);
                        } else {
                            logger.trace("XML encoding={}", encoding);
                        }
                    }
                }
                else {
                    logger.warn("Unexpected accumulatedCount value {}. Path={}", accumulatedCount, path);
                    throw new RuntimeException("Unexpected accumulatedCount value " + accumulatedCount);
                }
            }
            // More action check to be added here.
        }
        int count = 0;
        if ((nameAction & NAME_ACTION_STATS) != 0) {
            count = countName(globalStats, name);
        }

        return count;
    }


    private String getEncodingValue(String tag) {
        try {
            Matcher m = EXTRACT_PATTERN.matcher(tag.toUpperCase());
            if (!m.find() || m.groupCount() < 1) {
                return null;
            }
            return m.group(1).trim();
        }
        catch (Exception e) {
            logger.warn("Exception during encoding extraction. tag={}. e={}", tag, e);
        }
        return null;
    }

    private int countName(Map<String, Integer> stats, String name) {
        Integer count = stats.get(name);
        if (count == null) {
            count = 0;
        }
        stats.put(name, ++count);
        return count;
    }

    private int getNameCount(Map<String, Integer> stats, String name) {
        Integer count = stats.get(name);
        return (count == null) ? 0 : count;
    }

    private void processTagObjectEnd(Writer writer) throws IOException {
        if (!skipBytes || level <= skipLevel) {
            processBuf(writer);
        }
        else {
            logSkippedTag();
            accumulatedCount = 0;
            skippedObjectEndsCounter++;
        }
        if (skipBytes && level == skipLevel) {
            skipLevel--;
            logger.trace("Reducing skip level to {}", skipLevel);
        }
    }

    private void processTagObjectStart(Writer writer) throws IOException {
        if (skipBytes) {
            logSkippedTag();
            accumulatedCount = 0;
            skippedObjectsCounter++;
        }
        else if (totalObjectsLimit > 0 && allGlobalCounter > totalObjectsLimit) {
            skipBytes = true;
            skipLevel = level-1;
            logger.warn("Trimming XML level={} {} objects found. Path ={}", level, allGlobalCounter, path);
            claPropertySet.setBoolean(ClaThreadPropertiesSet.KEY_TRIMMED, true);
            logSkippedTag();
            accumulatedCount = 0;
            skippedObjectsCounter++;
        }
        else {
            processBuf(writer);
        }
    }

    private void logSkippedTag() {
        if (accumulatedCount>0) {
//            logger.debug("Skip: {}", String.valueOf(buf, 0, accumulatedCount));      // ToDO: remove for performance
        }
    }


    private void processBuf(Writer writer) throws IOException {
        if (0 == accumulatedCount) {
            return;
        }
        int s = accumulatedCount;
        accumulatedCount = 0;
        writer.write(buf, 0, s);
//        logger.debug("Output tag: {}", String.valueOf(buf, 0, s));      // ToDO: remove for performance
    }

    private void processChar(Writer writer, int charInt) throws IOException {
        nonTagCharCounter++;
        if (countText) {
            textCounter++;
        }
        if (skipBytes) {
            nonTagSkippedCharCounter++;
            textSkippedCounter++;
            return;
        }
        writer.write(charInt);
    }

    public void setTotalObjectsLimit(int totalObjectsLimit) {
        this.totalObjectsLimit = totalObjectsLimit;
    }

    public int getTotalObjectsLimit() {
        return totalObjectsLimit;
    }

}
