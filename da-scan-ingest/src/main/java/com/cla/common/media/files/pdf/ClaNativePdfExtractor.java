package com.cla.common.media.files.pdf;

import com.cla.common.allocation.IngestionResourceManager;
import com.cla.common.allocation.PdfMemoryAllocationStrategy;
import com.cla.common.domain.dto.ClaDocProperties;
import com.cla.common.domain.dto.mediaproc.word.WordFile;
import com.cla.common.domain.dto.word.WordfileMetaData;
import com.cla.common.media.files.Extractor;
import com.cla.common.media.files.word.FileMetadataUtils;
import com.cla.common.utils.FileSizeUnits;
import com.cla.common.utils.signature.SigningStringBuilder;
import com.cla.common.utils.tokens.HashedTokensConsumer;
import com.cla.common.utils.tokens.NGramTokenizerHashed;
import com.cla.connector.domain.dto.error.ProcessingErrorType;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.google.common.base.Strings;
import org.apache.pdfbox.exceptions.CryptographyException;
import org.apache.pdfbox.io.RandomAccessFile;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentCatalog;
import org.apache.pdfbox.pdmodel.PDDocumentInformation;
import org.apache.pdfbox.pdmodel.common.PDMetadata;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import javax.annotation.PostConstruct;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Collectors;

// Loaded by AppConfig
//@Component
public class ClaNativePdfExtractor extends Extractor<WordFile> {
    private static final Logger logger = LoggerFactory.getLogger(ClaNativePdfExtractor.class);

    @Autowired
    private NGramTokenizerHashed nGramTokenizerHashed;

    @Autowired
    private IngestionResourceManager ingestionResourceManager;

    @Autowired
    private PdfMemoryAllocationStrategy allocationStrategy;

    private final ClaPDFParserConfig config = new ClaPDFParserConfig();

    @Value("${wordFetchHyperlinks:false}")
    private boolean fetchHyperlinks;

    @Value("${wordBodyTextNgramSize:5}")
    private short bodyTextNgramSize;

    @Value("${wordHeadingsTextNgramSize:3}")
    private short headingsTextNgramSize;

    @Value("${wordMaxHeadingLength:60}")
    private int maxHeadingLength;

    @Value("${wordMinHeadingLength:5}")
    private int minHeadingLength;

    @Value("${paragraphLabelMaxTokens:4}")
    private int paragraphLabelMaxTokens;

    @Value("${tokenMatcherActive}")
    private boolean isTokenMatcherActive;

    @Value("${ingester.pdfMaxPagesToProcess:0}")
    private int pdfMaxPagesToProcess;    // 0 = no limit

    @Value("${ingester.pdf.convert-word-seperator-to-space:true}")
    private boolean convertWordSeparatorToSpace;

    @Value("${ingester.pdf.debug-text-extraction:false}")
    private boolean debugPdfTextExtraction;

    @Value("${ingester.pdf-acquire-image-memory:false}")
    private boolean pdfAcquireImageMemory;

    private int logNumerator = 1;

    private HashedTokensConsumer hashedTokensConsumer;

    @Value("${ingester.pdfMaxFileSize:52428800}")    // 50MB default max PDF size
    private int pdfMaxFileSize;

    @Value("${ingester.pdfIgnoreMaxFileSize:false}")
    private boolean pdfIgnoreMaxFileSize;

    @Value("${ingester.pdf.processSpaceCorruption:false}")
    private boolean processSpaceCorruption;

    @Value("${ingester.pdfbox.baseParser.pushBackSize:131072}")
    private String pdfBoxPushbackSize;

    @Value("${ingester.pdfbox.warnPdfProcessingIssues:false}")
    private boolean warnPdfProcessingIssues;

    @Value("${ingester.pdfbox.useScratchFile:true}")
    private boolean useScratchFile;             // whether to use scratch file for big files processing

    @Value("${ingester.pdfbox.scratchFileThresholdMb:2}")
    private int scratchFileThresholdMb;      // processing of PDF files larger than this threshold will use scratch files

    @Value("${ingester.pdfbox.scratchFileTempDir:./tempDir}")
    private String scratchFileTempDir;      // empty path implies system TEMP DIR

    @Value("${ingester.pdfbox.scratchFileNoCleanup:false}")
    private boolean scratchFileNoCleanup;      // keep temp files for debug

    private Path tempDir = null;

    @PostConstruct
    public void init() {
        if (!Strings.isNullOrEmpty(scratchFileTempDir)) {
            try {
                tempDir = Files.createDirectories(Paths.get(scratchFileTempDir));
            }
            catch (IOException e) {
                logger.warn("Could not create scratchFile directory {}", scratchFileTempDir);
            }
        }
        System.setProperty("org.apache.pdfbox.baseParser.pushBackSize", pdfBoxPushbackSize);

        logger.info("pdfAcquireImageMemory={}", pdfAcquireImageMemory?"active":"off");
        logger.info("pdfMaxPagesToProcess={}, convertWordSeparatorToSpace={}, debugPdfTextExtraction={}",
                pdfMaxPagesToProcess, (convertWordSeparatorToSpace?"avtive":"off"), (debugPdfTextExtraction?"avtive":"off"));
    }

    public void setHashedTokensConsumer(final HashedTokensConsumer hashedTokensConsumer) {
        this.hashedTokensConsumer = hashedTokensConsumer;
    }

    @SuppressWarnings("static-access")
    public WordFile extract(InputStream is, final String path, final boolean extractMetadata, ClaFilePropertiesDto claFileProperties) {
        final WordFile wordFile = new WordFile(path);
        boolean resourcesAquired = false;
        int resourcesAquiredMB = 0;

        Path scratchFile = null;

        PDDocument pdfDocument = null;
        try {
            if (logNumerator == 1) {
                logger.info("ClaPdfExtractor configuration parameters: {}, {}, {}, {}",
                        "bodyTextNgramSize=" + bodyTextNgramSize,
                        "headingsTextNgramSize=" + headingsTextNgramSize,
                        "maxHeadingLength=" + maxHeadingLength,
                        "minHeadingLength=" + minHeadingLength);
            }
            logger.debug("Start extracting file {} ...", path);

            // Convert inputStream to ByteArrayInputStream
            if (!(is instanceof ByteArrayInputStream)) {
                final int size = is.available();
                if (size > pdfMaxFileSize && !pdfIgnoreMaxFileSize) {
                    wordFile.setErrorType(ProcessingErrorType.ERR_LIMIT_EXCEEDED);
                    logger.warn("PDF file size limit exceeded ({} > {})", size, pdfMaxFileSize);
                    if (extractMetadata) {
                        WordfileMetaData claMetadata = FileMetadataUtils.extractMetadata(WordfileMetaData.class,
                                null, claFileProperties);
                        wordFile.setMetaData(claMetadata);
                    }
                    return wordFile;
                }
                if (size > 0 && size <= pdfMaxFileSize) {
                    final byte[] buffer = new byte[size];
                    int len = 0;
                    int offset = 0;
                    while (offset < size && (len = is.read(buffer, offset, size - offset)) > 0) {
                        offset += len;
                    }
                    if (offset != size) {
                        wordFile.setErrorType(ProcessingErrorType.ERR_UNKNOWN);
                        logger.error("Error during IS conversion. size={}, offset={}, lastLen={}", size, offset, len);
                        if (extractMetadata) {
                            final WordfileMetaData claMetadata = FileMetadataUtils.extractMetadata(WordfileMetaData.class,
                                    null, claFileProperties);
                            wordFile.setMetaData(claMetadata);
                        }
                        return wordFile;
                    }
                    is.close();
                    is = new ByteArrayInputStream(buffer);
                } else {
                    logger.debug("Using non-buffered IS");
                }
            }    // Convert inputStream to ByteArrayInputStream

            // Acquire resource token - Limit number of concurrently opened workbooks to avoid heap overflow when many large files are encountered
            int resources = allocationStrategy.getOverheadMemoryAllocationMB();
            if (resources > 0) {
                resourcesAquired = ingestionResourceManager.acquire(resources);
                if (resourcesAquired) {
                    resourcesAquiredMB = resources;
                    if (logger.isDebugEnabled()) {
                        logger.debug("resources acquired {}. {} available", resources, ingestionResourceManager.getAvailable());
                    }
                }
            }

            if (useScratchFile && claFileProperties.getFileSize() > FileSizeUnits.MB.toBytes(scratchFileThresholdMb)) {
                String suffix = "-" + claFileProperties.getFileSize() + ".tmp";
                scratchFile = (tempDir == null) ?
                        Files.createTempFile("cla-pdf-scratch-", suffix) :
                        Files.createTempFile(tempDir, "cla-pdf-scratch-", suffix);
                pdfDocument = PDDocument.loadNonSeq(is, new RandomAccessFile(scratchFile.toFile(), "rw"));
                logger.debug("Processing PDF file {} using scratchFile {}", path, scratchFile.getFileName());
            }
            else {
                pdfDocument = PDDocument.load(is);
            }

            if (pdfDocument.isEncrypted()) {
                logger.debug("PDF file is encrypted");
                // We are expecting non-encrypted documents here, but it is common
                // for users to pass in a document that is encrypted with an empty
                // password (such a document appears to not be encrypted by
                // someone viewing the document, thus the confusion).  We will
                // attempt to decrypt with the empty password to handle this case.
                //
                try {
                    pdfDocument.decrypt("");
                } catch (final CryptographyException e) {
                    logger.warn("Error decrypting document {}. Error is {}", path, e, e);
                    wordFile.setErrorType(ProcessingErrorType.ERR_ENCRYPTED);
                    wordFile.setExceptionText(e.getMessage());
                    return wordFile;
                }
            }

            // Acquire resource token - Limit number of concurrently opened workbooks to avoid heap overflow when many large files are encountered
            if (allocationStrategy.pageMemAllocationActive()) {
                resources = allocationStrategy.calculateRequiredMemory(pdfDocument.getNumberOfPages());
                if (ingestionResourceManager.acquire(resources)) {
                    resourcesAquired = true;
                    resourcesAquiredMB += resources;
                    if (logger.isDebugEnabled()) {
                        logger.debug("resources acquired {} (additional {}) for {} pages. {} available",
                                resourcesAquiredMB, resources, pdfDocument.getNumberOfPages(),
                                ingestionResourceManager.getAvailable());
                    }
                }
            }

            nGramTokenizerHashed.setDebugHashMapper(WordFile.getDebugHashMapper());        // For debug/troubleshooting

            if (extractMetadata) {
                final ClaDocProperties fileProps = extractProperties(pdfDocument);
                final WordfileMetaData claMetadata = FileMetadataUtils.extractMetadata(WordfileMetaData.class,
                        fileProps, claFileProperties);
                wordFile.setMetaData(claMetadata);
            }

            final SigningStringBuilder sbOpenText = SigningStringBuilder.create(userContentSignatureEnabled);
            final StringBuilder sbHashedText = new StringBuilder();

            final ClaPdfProcessor pdfProcessor = new ClaPdfProcessor(wordFile, config, sbOpenText, sbHashedText, nGramTokenizerHashed, hashedTokensConsumer);
            pdfProcessor.setTokenMatcherActive(isTokenMatcherActive);
            pdfProcessor.setPatternSearchActive(patternSearchActive);
            pdfProcessor.setPdfMaxPagesToProcess(pdfMaxPagesToProcess);
            pdfProcessor.setConvertWordSeparatorToSpace(convertWordSeparatorToSpace);
            pdfProcessor.setDebugTextExtraction(debugPdfTextExtraction);
            pdfProcessor.setProcessSpaceCorruption(processSpaceCorruption);
            pdfProcessor.setWarnPdfProcessingIssues(warnPdfProcessingIssues);
            pdfProcessor.setAcquireImageMemory(pdfAcquireImageMemory);
            pdfProcessor.setIngestionResourceManager(ingestionResourceManager);

            if (WordFile.getNGramTokenizer() == null) {
                logger.warn("NGramTokenizer not set. Using parameter.");
                WordFile.setNGramTokenizer(nGramTokenizerHashed);
            }

            pdfProcessor.process(pdfDocument);

			/*
             * Show memory footprint.
			 * Requires integration with https://github.com/DimitrisAndreou/memory-measurer.
			 * Need to add the following to the VM parameters list:
			 * 			 -javaagent:D:\Work\Ext-projects\memory-measurer\dist\object-explorer.jar
			 * /
			if (logger.isDebugEnabled()) {
				final long m1 = MemoryMeasurer.measureBytes(pdfDocument);
				final long m2 = MemoryMeasurer.measureBytes(pdfProcessor);
				final long m3 = MemoryMeasurer.measureBytes(wordFile);
				final int p = pdfDocument.getNumberOfPages();
				logger.debug("pdfDocument memory({})={}/{}/{}, {} pages, mpp={}M", path, 
						m1, 
						m2, 
						m3, 
						p,
						((m1+m2+m3)/p/1024/102)/10f);
			}
			/ *****/

            wordFile.setOrigBody(sbOpenText.toString());
            wordFile.setBody(sbHashedText.toString());
            claFileProperties.setUserContentSignature(sbOpenText.getSignatureAsBase64());
            wordFile.finalizeIngestion(true);        // extract only absolute signature styles when processing the PVs.

            if (logger.isDebugEnabled()) {
                logger.debug("Finished extracting file {} pv-hist sizes={}", path,
                        (wordFile.getLongHistograms().getCollections().entrySet().stream()
                                .map(e -> (e.getKey().concat(":") + e.getValue().getCollection().size())))
                                .collect(Collectors.joining(",")));
            }
            return wordFile;

        } catch (final Exception e) {
            logger.warn("Failed to read file {}. Error is {}. StackTrace on debug", path, e.getMessage());        // Output.log should not get 'Exceptions stack trace'
            logger.debug("Failed to read file {}. Error is {}", path, e, e);
            wordFile.setErrorTypeIfEmpty(ProcessingErrorType.ERR_UNKNOWN);
            wordFile.setExceptionText(e.getMessage());
            return wordFile;
        } finally {
            if (pdfDocument != null) {
                try {
                    pdfDocument.close();
                    is.close();
                } catch (final IOException e) {
                    // ignore
                }
            }
            if (resourcesAquired) {
                ingestionResourceManager.release(resourcesAquiredMB);
                if (logger.isDebugEnabled()) {
                    logger.debug("resources released {}. {} available", resourcesAquiredMB, ingestionResourceManager.getAvailable());
                }
            }

            if (scratchFile != null && !scratchFileNoCleanup){
                try {
                    Files.delete(scratchFile);
                } catch (IOException e) {
                    // ignore
                }
            }

        }
    }

    private ClaDocProperties extractProperties(final PDDocument pdfDocument) {
        final ClaDocProperties res = new ClaDocProperties();

        final PDDocumentInformation info = pdfDocument.getDocumentInformation();
        final PDDocumentCatalog cat = pdfDocument.getDocumentCatalog();
        final PDMetadata metadata = cat.getMetadata();

        res.setCreator(spaceIfNull(info.getCreator()));
        res.setSubject(spaceIfNull(info.getSubject()));
        res.setTitle(spaceIfNull(info.getTitle()));
        res.setKeywords(spaceIfNull(info.getKeywords()));
        res.setAppNameVer(spaceIfNull(info.getProducer()));
        res.setTemplate("");
        try {
            if (info.getCreationDate() != null) {
                res.setCreated(info.getCreationDate().getTime());
            }
        } catch (final IOException e) {
            logger.debug("Can not read creationDate: " + e.getMessage());
        }
        try {
            if (info.getModificationDate() != null) {
                res.setModified(info.getModificationDate().getTime());
            }
        } catch (final IOException e) {
            logger.debug("Can not read modificationDate: " + e.getMessage());
        }
        res.setCompany("");
        res.setWords(-1);
        res.setPages(pdfDocument.getNumberOfPages());
        res.setChars(-1);
        res.setParagraphs(-1);
        res.setLines(-1);
        final int sec = pdfDocument.isEncrypted() ? 1 : 0;
        res.setSecAppCode(sec);
        res.setSecPasswordProtected((sec & 1) != 0);
        //res.setSecReadonlyRecommended((sec & 2)!=0);
        //res.setSecReadonlyEnforced((sec & 4)!=0);
        //res.setSecLockedForAnnotations((sec & 8)!=0);
        //System.out.println( "Author=" + info.getAuthor() );

        if (metadata != null) {
            try {
                if (logger.isTraceEnabled()) {
                    final String str = metadata.getInputStreamAsString();
                    logger.trace("Metadata=" + str.substring(0, Integer.min(200, str.length())));    // limit dump length
                }
            } catch (final IOException e) {
                // ignore
            }
        }
        if (logger.isTraceEnabled()) {
            logger.trace("Extracted props: " + res.toString());
        }

        return res;
    }

    private static String spaceIfNull(final String str) {
        return (str != null) ? str : "";
    }

    public int getLogNumerator() {
        return logNumerator;
    }

    public void setLogNumerator(final int logNumerator) {
        this.logNumerator = logNumerator;
    }

}
