package com.cla.common.media.files.pst.visitor;

import com.cla.connector.domain.dto.file.FileContentDto;
import com.cla.connector.domain.dto.messages.ScanErrorDto;
import com.cla.common.domain.dto.file.PstEntryPropertiesDto;
import com.cla.common.media.files.pst.model.PstAttachment;
import com.cla.common.media.files.pst.model.PstEmailMessage;
import com.cla.common.media.files.pst.model.PstFolder;
import com.cla.connector.utils.Pair;

import java.io.InputStream;

/**
 * Created By Itai Marko
 */
public class PstEntryDataExtractingVisitor implements PstEntryVisitor<Pair<FileContentDto, PstEntryPropertiesDto>> {

    @Override
    public Pair<FileContentDto, PstEntryPropertiesDto> visitFolder(PstFolder pstFolder) {
        throw new UnsupportedOperationException("No data to extract from folder");
    }

    @Override
    public Pair<FileContentDto, PstEntryPropertiesDto> visitFolderFailed(PstFolder pstFolder, ScanErrorDto scanError) {
        throw new UnsupportedOperationException("No data to extract from folder");
    }

    @Override
    public Pair<FileContentDto, PstEntryPropertiesDto> visitEmailMessage(PstEmailMessage pstEmailMessage) {

        String pstEmailPath = pstEmailMessage.getPstPath().toString();
        FileContentDto emailContentDto = new FileContentDto(pstEmailMessage.getContentBytes(), pstEmailPath);

        PstEntryPropertiesDto payload = PstDtoConversionUtils.dtoForPstEmailMessage(pstEmailMessage);
        return Pair.of(emailContentDto, payload);
    }

    @Override
    public Pair<FileContentDto, PstEntryPropertiesDto> visitAttachment(PstAttachment pstAttachment) {
        int pstAttachmentFileSize = pstAttachment.getFileSize();
        InputStream pstAttachmentInputStream = pstAttachment.getAttachmentInputStream();
        String pstAttachmentPath = pstAttachment.getPstPath().toString();
        FileContentDto attachmentContentDto =
                new FileContentDto(pstAttachmentInputStream, (long)pstAttachmentFileSize, pstAttachmentPath);

        PstEntryPropertiesDto payload = PstDtoConversionUtils.dtoForPstAttachment(pstAttachment);
        return Pair.of(attachmentContentDto, payload);
    }
}
