package com.cla.common.media.files.word;

import com.cla.common.domain.dto.mediaproc.word.WordFile;
import com.cla.common.media.files.Extractor;
import com.cla.common.utils.tokens.HashedTokensConsumer;
import com.cla.common.utils.tokens.NGramTokenizerHashed;
import com.cla.connector.domain.dto.error.ProcessingErrorType;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.InputStream;


public abstract class ClaWordExtractor extends Extractor<WordFile> {
	private static final Logger logger = LoggerFactory.getLogger(ClaWordExtractor.class);

	private int logNumerator = 1;	// used for debug logging

	@Autowired
	private NGramTokenizerHashed nGramTokenizerHashed;

	@PostConstruct
	private void init(){
		WordFile.setNGramTokenizer(nGramTokenizerHashed);
	}

	public WordFile extract(final InputStream inputStream, final String path,
                            final HashedTokensConsumer hashedTokensConsumer,
                            final boolean extractMetadata,
                            ClaFilePropertiesDto claFileProperties) {

		if (WordFile.getNGramTokenizer() == null) {
			logger.warn("NGramTokenizer not set. Using default.");
			WordFile.setNGramTokenizer(new NGramTokenizerHashed());
		}
		WordFile result;
		if (path.toLowerCase().endsWith("docx")) {
			final ClaWordXmlExtractor claWordXmlExtractor = createClaWordXmlExtractor();
			if (patternSearchActiveSet) {
				claWordXmlExtractor.setPatternSearchActive(patternSearchActive);
			}
			claWordXmlExtractor.setHashedTokensConsumer(hashedTokensConsumer);
			claWordXmlExtractor.setLogNumerator(logNumerator);

			result = claWordXmlExtractor.extract(inputStream, path, extractMetadata, claFileProperties);
			logNumerator = claWordXmlExtractor.getLogNumerator();
		}
		else if (path.toLowerCase().endsWith("doc")) {
			if (inputStream.markSupported()) {
				inputStream.mark(10000);
			}
			final ClaWord6Extractor claWord6Extractor = createClaWord6Extractor();
			if (patternSearchActiveSet) {
				claWord6Extractor.setPatternSearchActive(patternSearchActive);
			}
			claWord6Extractor.setLogNumerator(logNumerator);
			claWord6Extractor.setHashedTokensConsumer(hashedTokensConsumer);
			result = claWord6Extractor.extract(inputStream, path, extractMetadata, claFileProperties);
			logNumerator = claWord6Extractor.getLogNumerator();
			if (ProcessingErrorType.ERR_OFFICE_XML_ERROR == result.getErrorType()) {
                logger.debug("Word file {} seems like DOCX with doc extention. ", path);
				if (inputStream.markSupported()) {
					try {
						inputStream.reset();
					} catch (IOException e3) {
						logger.info("Exception during doc ingestion {} {}. Could not recover - IS marking failed", path, e3);
					}
				}
				final ClaWordXmlExtractor claWordXmlExtractor = createClaWordXmlExtractor();
				if (patternSearchActiveSet) {
					claWordXmlExtractor.setPatternSearchActive(patternSearchActive);
				}
				claWordXmlExtractor.setHashedTokensConsumer(hashedTokensConsumer);
				claWordXmlExtractor.setLogNumerator(logNumerator);

				result = claWordXmlExtractor.extract(inputStream, path, extractMetadata, claFileProperties);
				logNumerator = claWordXmlExtractor.getLogNumerator();
			}
		}
//			else if (false && /* TODO: */path.toLowerCase().endsWith("pdf")) {
//				final ClaNativePdfExtractor claNativePdfExtractor = createClaPdfExtractor();
//				if (patternSearchActiveSet) {
//					claNativePdfExtractor.setPatternSearchActive(patternSearchActive);
//				}
//				claNativePdfExtractor.setHashedTokensConsumer(hashedTokensConsumer);
//				claNativePdfExtractor.setLogNumerator(logNumerator);
//	
//				result = claNativePdfExtractor.extract(is,path,extractMetadata);
//				logNumerator = claNativePdfExtractor.getLogNumerator();
//
//				
//				claNativePdfExtractor.setLogNumerator(logNumerator);
//				result = claNativePdfExtractor.extract(is,path,extractMetadata);
//				logNumerator = claNativePdfExtractor.getLogNumerator();
//			}
		else {
			logger.error("Unexpected doc suffix: {}", path);
			result = null;
		}
		if(result!=null) {
			result.setHashedTokensConsumer(hashedTokensConsumer);
		}
		return result;
	}
	
	public WordFile extract(final InputStream is, final String path, final boolean extractMetadata, ClaFilePropertiesDto claFileProperties){
		return extract(is, path,null, extractMetadata, claFileProperties);
	}

	protected abstract ClaWord6Extractor createClaWord6Extractor();
	protected abstract ClaWordXmlExtractor createClaWordXmlExtractor();

}
