package com.cla.common.media.files.word;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import com.cla.common.media.files.Extractor;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.error.ProcessingErrorType;
import com.cla.connector.utils.TextUtils;
import com.cla.common.utils.signature.SigningStringBuilder;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.hpsf.DocumentSummaryInformation;
import org.apache.poi.hpsf.PropertySet;
import org.apache.poi.hpsf.SummaryInformation;
import org.apache.poi.hpsf.UnexpectedPropertySetTypeException;
import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.hwpf.OldWordFileFormatException;
import org.apache.poi.hwpf.model.DocumentProperties;
import org.apache.poi.hwpf.model.FieldsDocumentPart;
import org.apache.poi.hwpf.model.PicturesTable;
import org.apache.poi.hwpf.model.StyleDescription;
import org.apache.poi.hwpf.usermodel.BorderCode;
import org.apache.poi.hwpf.usermodel.CharacterRun;
import org.apache.poi.hwpf.usermodel.Field;
import org.apache.poi.hwpf.usermodel.HeaderStories;
import org.apache.poi.hwpf.usermodel.Paragraph;
import org.apache.poi.hwpf.usermodel.Picture;
import org.apache.poi.hwpf.usermodel.Range;
import org.apache.poi.hwpf.usermodel.Table;
import org.apache.poi.hwpf.usermodel.TableCell;
import org.apache.poi.hwpf.usermodel.TableRow;
import org.apache.poi.poifs.filesystem.DirectoryEntry;
import org.apache.poi.poifs.filesystem.DocumentEntry;
import org.apache.poi.poifs.filesystem.DocumentInputStream;
import org.apache.poi.poifs.filesystem.OfficeXmlFileException;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.cla.common.domain.dto.ClaDocProperties;
import com.cla.common.domain.dto.histogram.ClaHistogramCollection;
import com.cla.common.domain.dto.histogram.ClaStyle;
import com.cla.common.domain.dto.pv.PVType;
import com.cla.common.domain.dto.mediaproc.word.WordFile;
import com.cla.common.domain.dto.word.WordfileMetaData;
import com.cla.common.utils.tokens.HashedTokensConsumer;
import com.cla.common.utils.tokens.NGramTokenizerHashed;


public class ClaWord6Extractor extends Extractor<WordFile> {
	private static final Logger logger = LoggerFactory.getLogger(ClaWord6Extractor.class);

	private static final ClaStyle.Justification[] justifications = {
		ClaStyle.Justification.LEFT_JUST
		,ClaStyle.Justification.CENTER_JUST
		,ClaStyle.Justification.RIGHT_JUST
		,ClaStyle.Justification.JUSTIFIED_JUST
		};

	@Autowired
	private NGramTokenizerHashed nGramTokenizerHashed;
	
	private HashedTokensConsumer hashedTokensConsumer;

	// private final Pattern alphaNumericPattern = Pattern.compile("[a-zA-Z0-9]");
	static private final Pattern pLabelIndiactionPattern = Pattern.compile("[\t:]\\s");
//	static private final Pattern pLabelPattern = Pattern.compile("[\\w&&[\\D]].*");	// Note that pattern may be locale specific (start of word)
	static private final Pattern pLabelPattern = Pattern.compile("[\\p{IsAlphabetic}&&[\\D]].*");	// Note that pattern may be locale specific (start of word)
	static private final Pattern pLabelSplitPattern = Pattern.compile("\\s+");
	private final static String[] paragraphLabelDelimiters = {"\t", ":", "   "};
//	static private final Pattern injectedFieldCodePattern = Pattern.compile("(F\\d+\\s\\{.*\\})|_+\\s?");	// Note that pattern may be locale specific (start of word)
	
	@Value("${wordBodyTextNgramSize:5}")
	private short bodyTextNgramSize;
	
	@Value("${tokenMatcherActive}")
	private boolean isTokenMatcherActive;

	@Value("${wordHeadingsTextNgramSize:3}")
	private short headingsTextNgramSize;

	@Value("${wordMaxHeadingLength:85}")
	private int maxHeadingLength;

	@Value("${wordMinHeadingLength:5}")
	private int minHeadingLength;

	@Value("${paragraphLabelMaxTokens:4}")
	private int paragraphLabelMaxTokens;
	
	@Value("${word.titleExtraction.ignoreRightJustified:true}")
	private boolean titleExtractionIgnoreRightJustified;
	
	@Value("${word.titleExtraction.MaxCount:10}")		// TODO: should be 5 or 3
	private int titleExtractionMaxCount;
	
	@Value("${word.titleExtraction.addStyleToken:false}")
	private boolean debTitleExtractionAddStyleToken;

	@Value("${word.getFontSizeStats:false}")
	private boolean isGetFontSizeStats;
	
	@Value("${ingester.word.docStartWordsLimit:1000}")
	private int wordDocStartWordsLimit;
	
	@Value("${ingester.word.docStartParagraphsLimit:30}")
	private int wordDocStartParagraphsLimit;
	
	@Value("${ingester.word.ingestionWordsLimit:0}")
	private int wordIngestionWordsLimit;
	
	@Value("${ingester.word.ingestionParagraphsLimit:0}")
	private int wordIngestionParagraphsLimit;

	@Value("${ingester.word.clean-unprintable-chars:true}")
	private boolean shouldCleanUnprintableChars;

	private int bodyParagraphsCounter = 0;
	private int bodyWordsCounter = 0;
	
	private int logNumerator = 1;
	private int paragraphId = 1;
	private boolean prevParagraphWasTitle = false;

	@Value("${ingester.word.tableRowForTitleExtraction:4}")
	private int tableRowForTitleExtraction;

	private String tableHeadTitle = "";
	private int tableHeadTitleMaxRate = 0;
	private boolean tableHeadTitleFreeze = false;
	private int tableHeadTitleLastRow = (-1);
	private int tableHeadTitleLastCol = (-1);

	public HashedTokensConsumer getHashedTokensConsumer() {
		return hashedTokensConsumer;
	}

	public void setHashedTokensConsumer(final HashedTokensConsumer hashedTokensConsumer) {
		this.hashedTokensConsumer = hashedTokensConsumer;
	}

	public WordFile extract(final InputStream is, final String path, final boolean extractMetadata, ClaFilePropertiesDto claFileProperties) {

		POIFSFileSystem fs;
		final WordFile wordFile = new WordFile(path);
		try {
			if (logNumerator==1) {
				logger.trace("ClaWord6Extractor configuration parameters: {}, {}, {}, {}",
						"bodyTextNgramSize="+bodyTextNgramSize,
						"headingsTextNgramSize="+headingsTextNgramSize,
						"maxHeadingLength="+maxHeadingLength,
						"minHeadingLength="+minHeadingLength);
			}
			logger.trace("Start extracting file {} ...",path);
			fs = new POIFSFileSystem(is);
			final HWPFDocument doc = new HWPFDocument(fs);

			// Grab the list of pictures. As far as we can tell,
			//  the pictures should be in order, and may be directly
			//  placed or referenced from an anchor
			final PicturesTable pictureTable = doc.getPicturesTable();
			final PicturesSource pictures = new PicturesSource(doc);

			nGramTokenizerHashed.setDebugHashMapper(WordFile.getDebugHashMapper());		// For debug/troubleshooting
			resetDocPosition();

			if(extractMetadata){
				final ClaDocProperties fileProps = extractProperties(doc, fs);
				final WordfileMetaData claMetadata = FileMetadataUtils.extractMetadata(WordfileMetaData.class,
						fileProps, claFileProperties);
				wordFile.setMetaData(claMetadata);
			}

			// Start processing document user content (i.e. content excluding metadata)
			MessageDigest userContentSigDigest = DigestUtils.getSha1Digest();

			processBody(doc, wordFile, pictures, pictureTable, userContentSigDigest);
			flushTableHeadTitle(wordFile);
			
			processHeadersNFooters(doc, wordFile, pictures, pictureTable, userContentSigDigest);
			processFloatingPictures(pictures, wordFile);
			
			wordFile.finalizeIngestion();
			claFileProperties.setUserContentSignature(Base64.encodeBase64String(userContentSigDigest.digest()));
			
			if (logger.isDebugEnabled()) {
				logger.debug("Finished extracting file {} pv-hist sizes={}", path,
						wordFile.getLongHistograms().getCollections().entrySet().stream()
						.map(e -> (e.getKey() + ":" + e.getValue().getCollection().size()))
						.collect(Collectors.joining(",")));
			}
			return wordFile;
			
		} catch (final OldWordFileFormatException e) {
			logger.info("Cannot ingest file - old format {}. Error is {}. exception={}",path,e.getMessage(), e.getClass().toString());
			setErrorData(wordFile, ProcessingErrorType.ERR_OLD_FORMAT, e);
	    	return wordFile;
		} catch (final IOException e) {
			logger.info("Cannot ingest file {}. Error is {}. exception={}",path,e.getMessage(), e.getClass().toString());
			setErrorData(wordFile, ProcessingErrorType.ERR_IO, e);
	    	return wordFile;
		} catch (final EncryptedDocumentException e) {
			logger.info("Cannot ingest file {}. Error is {}. exception={}",path,e.getMessage(), e.getClass().toString());
			setErrorData(wordFile, ProcessingErrorType.ERR_ENCRYPTED, e);
	    	return wordFile;
		} catch (final OfficeXmlFileException e) {
			logger.debug("Cannot ingest file {}. Error is {}. exception={}",path,e.getMessage(), e.getClass().toString());
			setErrorData(wordFile, ProcessingErrorType.ERR_OFFICE_XML_ERROR, e);
	    	return wordFile;
		} catch (final Exception e) {
			logger.info("Failed to read file {}. Error is {}.",path,e.getMessage(), e);
			setErrorData(wordFile, ProcessingErrorType.ERR_UNKNOWN, e);
	    	return wordFile;
		}
	}

	private void setErrorData(WordFile wordFile, ProcessingErrorType type, Exception e) {
		wordFile.setErrorType(type);
		wordFile.setExceptionText(e == null ? null : e.getMessage());
	}

	public int getLogNumerator() {
		return logNumerator;
	}

	public void setLogNumerator(final int logNumerator) {
		this.logNumerator = logNumerator;
	}

	private void processFloatingPictures(final PicturesSource pictures, final WordFile wordFile) {
		// Handle any pictures that we haven't output yet
		for(Picture p = pictures.nextUnclaimed(); p != null; ) {
			handlePictureCharacterRun(null, p, pictures, wordFile);
			p = pictures.nextUnclaimed();
		}
	}

	private void processHeadersNFooters(final HWPFDocument wdDoc, final WordFile wordFile, 
			final PicturesSource pictures, final PicturesTable pictureTable, MessageDigest userContentSigDigest) {

		final HeaderStories headerFooter = new HeaderStories(wdDoc);

		// Process headers
		//------------------------------------------------------------------
		final SigningStringBuilder sbOpenTextHeader = SigningStringBuilder.create(userContentSigDigest, userContentSignatureEnabled);
		final StringBuilder sbHashedTextHeader = new StringBuilder();

		// Do any headers, if present
		final Range[] headers = new Range[] { headerFooter.getFirstHeaderSubrange(),
				headerFooter.getEvenHeaderSubrange(), headerFooter.getOddHeaderSubrange() };
		handleHeaderFooter(headers, wdDoc, pictures, pictureTable, wordFile, sbOpenTextHeader, sbHashedTextHeader);

		wordFile.setHeader(sbHashedTextHeader.toString());
		wordFile.setOrigHeader(sbOpenTextHeader.toString());

		// Process footers
		//------------------------------------------------------------------
		final SigningStringBuilder sbOpenTextFooter = SigningStringBuilder.create(userContentSigDigest, userContentSignatureEnabled);
		final StringBuilder sbHashedTextFooter = new StringBuilder();

		// Do any footers, if present
		final Range[] footers = new Range[] { headerFooter.getFirstFooterSubrange(),
				headerFooter.getEvenFooterSubrange(), headerFooter.getOddFooterSubrange() };
		handleHeaderFooter(footers, wdDoc, pictures, pictureTable, wordFile, sbOpenTextFooter, sbHashedTextFooter);

		wordFile.setOrigFooter(sbOpenTextFooter.toString());
		wordFile.setFooter(sbHashedTextFooter.toString());
	}

	private void handleHeaderFooter(final Range[] headers, final HWPFDocument wdDoc,
			final PicturesSource pictures, final PicturesTable pictureTable,
			final WordFile wordFile, final Appendable sbOpenText,final Appendable sbHashedText) {

		for (final Range range : headers) {
			if (range == null) {
				continue;
			}
			
			IntStream.range(0, range.numParagraphs())
				.mapToObj(range::getParagraph)
				.forEach(p -> processHeaderParagraph(p, range, wdDoc, null, pictures, pictureTable, wordFile, sbOpenText, sbHashedText));

			paragraphId++;
		}
	}

	private void processHeaderParagraph(final Paragraph p, final Range range, final HWPFDocument wdDoc,
			final FieldsDocumentPart docPart, final PicturesSource pictures, final PicturesTable pictureTable, 
			final WordFile wordFile, final Appendable sbOpenText, final Appendable sbHashedText) {
		
		final int stInx = p.getStyleIndex();
		final StyleDescription sd = (stInx>wdDoc.getStyleSheet().numStyles())?null:
			wdDoc.getStyleSheet().getStyleDescription(stInx);
		
		final int numRuns = p.numCharacterRuns();
		final ClaStyle claStyle = extractStyleNText(p, 0, range, wdDoc, docPart, pictures, pictureTable, wordFile);
		
		boolean gotTitle = false;

		CharacterRun firstRun = p.getCharacterRun(0);
		if (firstRun.isSymbol() && numRuns > 1) {
			firstRun = p.getCharacterRun(1);
		}
		final ClaStyle style = getClaStyle((sd==null)?"":sd.getName(), p, firstRun);

		//final String pText = getTextNoFieldValues(p);
		final String pText = claStyle.getText();
		final int pTextLenth = pText.length();
		wordFile.histogramAddLongItems(PVType.PV_HeaderNgrams, nGramTokenizerHashed.getHashlist(pText, headingsTextNgramSize));
		// Process tokens for entity-extraction mechanism
		if (isTokenMatcherActive && !pText.isEmpty()) {
			append( sbOpenText, sbHashedText, pText);
		}
		if (pTextLenth >= minHeadingLength) {	// process style stats
			final String styleSig = style.getSimpleSignature();
			wordFile.addHeaderStyleSignature(styleSig);
		}
		
		final boolean isHeading = (pTextLenth < maxHeadingLength) && 
				(pTextLenth >= minHeadingLength) && ((style.getHeadingLevel() > 0) || style.isHeading());
		if (isHeading) {
			final String pTitle = removeFieldTransformedText(pText.trim());				// Remove field literals injected to the text
			final int pTitleLength = pTitle.length();
			if (((!titleExtractionIgnoreRightJustified) || style.getJustification() != ClaStyle.Justification.RIGHT_JUST) &&
					(pTitleLength < maxHeadingLength) && (pTitleLength >= minHeadingLength) &&
					(wordFile.getProposedTitlesCount() < titleExtractionMaxCount))
			{
				gotTitle = true;
				wordFile.addProposedTitles((p.isInTable()?"H5":"H6"), style.getStyleHeadingPotentialRate(),
						pTitle,
						prevParagraphWasTitle);
			}
		}
		prevParagraphWasTitle = gotTitle;
	}

	private void append(final Appendable sbOpenText,final Appendable sbHashedText, final String pText) {
		try {
			if (patternSearchActive) {
				sbOpenText.append(pText).append("\n");
			}
			sbHashedText.append(nGramTokenizerHashed.getHash(pText, hashedTokensConsumer)).append(" ");
		}
		catch(IOException ioe){
			logger.error("Failed to append text {}.", pText);
		}
	}

	private void processBody(final HWPFDocument wdDoc, final WordFile wordFile, 
			final PicturesSource pictures, final PicturesTable pictureTable, MessageDigest userContentSigDigest) {

		final SigningStringBuilder sbOpenText = SigningStringBuilder.create(userContentSigDigest, userContentSignatureEnabled);
		final StringBuilder sbHashedText = new StringBuilder();

		final Range r = wdDoc.getRange();
		for(int i=0; i<r.numParagraphs(); i++) {
			final Paragraph p = r.getParagraph(i);
			i += handleBodyParagraph(p, r, 0, 0, 0, wdDoc, FieldsDocumentPart.MAIN, pictures, pictureTable, wordFile, sbOpenText, sbHashedText);
		}

		wordFile.setOrigBody(sbOpenText.toString());
		wordFile.setBody(sbHashedText.toString());
	}

	static private ClaStyle getClaStyle(final String name, final Paragraph p, final CharacterRun run ) {
		final int color = (run.getColor()!=0)?run.getIco24():0;
		final ClaStyle style = new ClaStyle(name, run.getFontName(), run.getFontSize()/2f, (color!=-1)?color:0, 
				run.isBold(), run.getUnderlineCode()!=0, run.text().length());
		style.setBorder(styleBorderHash(p.getTopBorder()),
				styleBorderHash(p.getRightBorder()),
				styleBorderHash(p.getBottomBorder()),
				styleBorderHash(p.getLeftBorder()));
		style.setListItem(p.isInList() || run.isSymbol());
		style.setTableLevel(p.isInTable()?1:0);
		int level = p.getLvl()+1;
		if (level > 9) {
			level = 0;
		}
		style.setHeadingLevel(level);
		
		final int ico24 = run.getIco24();
		final int col = run.getColor();

		// Get paragraph properties
		style.setFirstLineIndent((short) Integer.signum(p.getFirstLineIndent()/20));
		style.setIndentFromLeft((short) (p.getIndentFromLeft()/1440));
		style.setIndentFromRight((short) (p.getIndentFromRight()/1440));
		if (p.getJustification() >=0 && p.getJustification() < justifications.length) {
			style.setJustification(justifications[p.getJustification()]);
		}
		else {
			style.setJustification(justifications[0]);
		}
		style.setSpacingBefore((short) (p.getSpacingBefore()/20));
		style.setSpacingAfter((short) (p.getSpacingAfter()/20));
		style.setTableLevel(p.getTableLevel());
		style.setText(run.text());

		return style;
	}

	static private int styleBorderHash(final BorderCode border) {
		if (border.isEmpty() || border.getLineWidth() == 0) {
			return 0;
		}
		return border.toInt();
	}

	private int handleBodyParagraph(final Paragraph p, final Range r, final int parentTableLevel, final int row, final int col,
			final HWPFDocument document, final FieldsDocumentPart docPart, 
			final PicturesSource pictures, final PicturesTable pictureTable, 
			final WordFile wordFile, final Appendable sbOpenText, final Appendable sbHashedText) {
		// Note - a poi bug means we can't currently properly recurse
		//  into nested tables, so currently we don't
		if(p.isInTable() && p.getTableLevel() > parentTableLevel && parentTableLevel == 0) {
			final Table t = r.getTable(p);
			for(int rn=0; rn<t.numRows(); rn++) {
				final TableRow tableRow = t.getRow(rn);
				for(int cn=0; cn<tableRow.numCells(); cn++) {
					final TableCell cell = tableRow.getCell(cn);
					for(int pn=0; pn<cell.numParagraphs(); pn++) {
						final Paragraph cellP = cell.getParagraph(pn);
						handleBodyParagraph(cellP, cell, p.getTableLevel(), rn, cn, document, docPart, pictures, pictureTable, wordFile, sbOpenText, sbHashedText);
					}
				}
				if (parentTableLevel == 0) {
					paragraphId++;
				}
			}
			return (t.numParagraphs()-1);
		}

		final ClaStyle claStyle = extractStyleNText(p, parentTableLevel, r, document, docPart, pictures, pictureTable, wordFile);

		processBodyParagraphText(document, p, row, col, claStyle.getText(), claStyle, wordFile, sbOpenText, sbHashedText);
		
		paragraphId++;

		return 0;
	}

	private ClaStyle extractStyleNText(final Paragraph p, final int parentTableLevel, final Range r, final HWPFDocument document, 
			final FieldsDocumentPart docPart, final PicturesSource pictures, 
			final PicturesTable pictureTable, final WordFile wordFile) {

		String styleName = null;
		if (document.getStyleSheet().numStyles()>p.getStyleIndex()) {
			final StyleDescription style =
					document.getStyleSheet().getStyleDescription(p.getStyleIndex());
			if (style != null) {
				styleName = style.getName();
			}
		}
		
		final List <String> textParts = new LinkedList<>();
		ClaStyle claStyle = null;
		int crMaxTextLength = 0;
		for(int j=0; j<p.numCharacterRuns(); j++) {
			final CharacterRun cr = p.getCharacterRun(j);

			// FIELD_BEGIN_MARK
			if (cr.text().getBytes()[0] == 0x13) {
				final Field field = document.getFields().getFieldByStartOffset(docPart, cr.getStartOffset());
				// 58 is an embedded document
				// 56 is a document link
				if (field != null && (field.getType() == 58 || field.getType() == 56)) {
					// Embedded Object: add a <div
					// class="embedded" id="_X"/> so consumer can see where
					// in the main text each embedded document
					// occurred:
					// String id = "_" + field.getMarkSeparatorCharacterRun(r).getPicOffset();
					// TODO:
					logger.info("Unhandled embedded object field found. Paragraph starts with {}", field.getType());
				}
			}

			final String crText = cr.text();
			if (crMaxTextLength < crText.length()) {
				claStyle = getClaStyle(styleName, p, cr);
				crMaxTextLength = crText.length();
			}
			if(crText.equals("\u0013")) {
				j += handleSpecialCharacterRuns(p, j, claStyle.getHeadingLevel() > 0, pictures, textParts, wordFile, true);
			} else if(crText.startsWith("\u0008")) {
				// Floating Picture(s)
				for(int pn=0; pn<cr.text().length(); pn++) {
					// Assume they're in the order from the unclaimed list...
					final Picture picture = pictures.nextUnclaimed();
					handlePictureCharacterRun(cr, picture, pictures, wordFile);
				}
			} else if(pictures != null &&
					pictureTable != null &&
					pictureTable.hasPicture(cr)) {
				// Inline Picture
				final Picture picture = pictures.getFor(cr);
				handlePictureCharacterRun(cr, picture, pictures, wordFile);
			} else {
				handleCharacterRun(cr, textParts);
			}
		}

		// Process collected text and style
		final String text = textParts.stream().collect(Collectors.joining()); 
		claStyle.setText(text);
		claStyle.setTableLevel(parentTableLevel);

		return claStyle;
	}

	private void processBodyParagraphText(final HWPFDocument wdDoc, final Paragraph p, final int rowNum, final int colNum, 
			final String pText, final ClaStyle style, final WordFile wordFile, 
			final Appendable sbOpenText, final Appendable sbHashedText) {
		
		boolean gotTitle = false;

		final int pTextLenth = pText.length();
		if (pTextLenth > 2 * maxHeadingLength || p.isInTable()) {
			wordFile.setBodyLongTextProcessed(true);
		}
		boolean isHeading = /*(!p.isInTable()) && */(pTextLenth < maxHeadingLength) && 
				(pTextLenth >= minHeadingLength) && ((style.getHeadingLevel() > 0) || style.isHeading());

		final List<Long> nGrams = nGramTokenizerHashed.getHashlist(pText, bodyTextNgramSize);
		
		// Process paragraphs at 1st table level
		if (p.isInTable() && style.getTableLevel()==1 && rowNum==0) {
			if (colNum==0) {
				clearTableHeadTitle(wordFile);
			}
			if (colNum<4) {
				addTableHeadCellTitle((isHeading && !p.isInList()), 
						pText, style.getStyleHeadingPotentialRate(), rowNum, colNum);
			}
		}
		if (isHeading && (!p.isInTable()) &&
				nGrams!=null && nGrams.size()>0) {		// make sure there is at least 1 meaningful text token
			final String pTextT = pText.trim();
			final int pTextTLength = pTextT.length();
			if ((pTextTLength < maxHeadingLength) && (pTextTLength >= minHeadingLength)) {
				wordFile.histogramAddItem(PVType.PV_Headings, pTextT);
			}			
			if (isWithinDocPreamble()) {
				final String pTitle =  removeFieldTransformedText(pTextT);				// Remove field literals injected to the text
				final int pTitleLength = pTitle.length();
				if (((!titleExtractionIgnoreRightJustified) || style.getJustification() != ClaStyle.Justification.RIGHT_JUST) &&
						(wordFile.getProposedTitlesCount() < titleExtractionMaxCount) &&
						(pTitleLength < maxHeadingLength) && (pTitleLength >= minHeadingLength) &&
						(wordFile.getProposedTitlesCount() == 0 || !wordFile.isBodyLongTextProcessed()))	// once body long-text is seen process title only if none exist
				{
					gotTitle = true;
					// Set priority based on first character - letter=highest
					final String priority = Character.isAlphabetic(pTextT.codePointAt(0))?"P4":"P5";

					wordFile.addProposedTitles(priority, style.getStyleHeadingPotentialRate(),
							pTitle,
							prevParagraphWasTitle);
				}
			}
		}

		updateDocPosition(wdDoc, p, (nGrams==null || nGrams.size()==0)?0:nGrams.size()+bodyTextNgramSize/2);	// Estimation for number of words in this paragraph
		
		if (nGrams != null && isWithinIngestionPart()) {
			wordFile.histogramAddLongItems(PVType.PV_BodyNgrams, nGrams);

			wordFile.histogramAddLongItems(PVType.PV_SubBodyNgrams, nGrams);
			final ClaHistogramCollection<Long> sbNgHist = wordFile.getLongHistogram(PVType.PV_SubBodyNgrams);
			// Mark all but every fifth item in the SubBodyNgrams to filter them out during PV_Entries save.
			final Iterator<Long> ngIter = nGrams.iterator();
			for (int i=0;ngIter.hasNext();++i) {
				final Long ng = ngIter.next();
				if ((i % bodyTextNgramSize) != 0) {
					sbNgHist.markItem(ng);	// Marked items will not be matched
				}
			}
		}
		
		// Process tokens for entity-extraction mechanism
		if (isTokenMatcherActive && !pText.isEmpty()) {
			append(sbOpenText, sbHashedText, pText);
		}
		
		if (isWithinIngestionPart()) {
			if (pTextLenth >= minHeadingLength) {	// process style stats
				final int leftMar = p.getSection(0).getMarginLeft();
				final int rightMar = p.getSection(0).getMarginRight();
				//			final String styleSig = style.getSimpleSignature();
				//			wordFile.addStyleSignature(styleSig, absoluteStyleSig, !style.isInTable());
				final String absoluteStyleSig = style.getAbsoluteSignatureNoFont(leftMar/1440f,rightMar/1440f);
				wordFile.addDelayedStyleSignature(style.getFontName(), absoluteStyleSig, pTextLenth, !style.isInTable());
				logger.trace("AbsStyleDeb: f({}) {} {} text={}",style.getFontName(), absoluteStyleSig, (!style.isInTable())?1:0, pText);
				//			logger.debug("StyleDeb6: {} Text={}", style.getSimpleSignature(), pText.substring(0, Integer.min(50, pText.length())));
			}

			final String pTextT = pText.trim();
			String pLabel = getParagraphLabel(pTextT); // based on text analysis
			if (pLabel == null)
			{
				pLabel = getParagraphLabel(p); // based on run style analysis
			}
			if (pLabel != null) {
				wordFile.histogramAddItem(PVType.PV_ParagraphLabels, pLabel);
				logger.trace("pLabel: {} out of {}",pLabel, pText);
			}

			// Extraction logic to be performed only on the first 1-2 pages
			if (isWithinDocPreamble()) {
				if (pLabel != null) {
					wordFile.histogramAddItem(PVType.PV_LetterHeadLabels, pLabel);

					// A section with letter-head is a low candidate for doc title (prefer the one with largest text within range)
					final String pTitle = removeFieldTransformedText(pTextT);				// Remove field literals injected to the text
					final int pTitleLength = pTitle.length();
					if ((pTitleLength < maxHeadingLength) && (pTitleLength >= minHeadingLength) && 
							((style.isInTable() && style.getTableLevel()==1 && rowNum < tableRowForTitleExtraction) || !style.isInTable())) {
						gotTitle = true;
						//					wordFile.addProposedTitles("P8", 100-pTitleLength, pTitle, prevParagraphWasTitle);
						wordFile.addProposedTitles("L8", style.getStyleHeadingPotentialRate(), pTitle, prevParagraphWasTitle);
					}
				}

				lookForDocTitle(wdDoc, p, pText, style, wordFile);

				wordFile.histogramAddLongItems(PVType.PV_DocstartNgrams, nGrams);
			}

			// Collect font-size statistics
			if (isGetFontSizeStats) {
				for (int i=0;i<p.numCharacterRuns();++i) {
					final CharacterRun cr = p.getCharacterRun(i);
					final int size = cr.getFontSize();
					final int lenght = cr.text().length();
					wordFile.processFontStats(cr.getFontName(), size, lenght);
				}
			}
		}
		prevParagraphWasTitle = gotTitle;
	}


	private void addTableHeadCellTitle(boolean isHeading, String text, int rate, int row, int col) {
		text = removeFieldTransformedText(text);
		if (text.length() < 3) {
			return;
		}
		if (logger.isTraceEnabled()) {
			logger.trace("addTableHeadCellTitle: added <{}> ({}/{}) at {},{} to <{}>", text, rate, (isHeading?"hdn":"notH"), row, col, tableHeadTitle);
		}
		if ((!isHeading) && (!tableHeadTitle.isEmpty()) &&
				(tableHeadTitleLastRow > row || tableHeadTitleLastCol > col) && !text.isEmpty()) {
			// If not all are 'headings', then get only the first 'heading' paragraph.
			tableHeadTitleFreeze = true;
			logger.trace("addTableHeadCellTitle: not heading. Stop adding cells");
			return;
		}
		if (isHeading && !tableHeadTitleFreeze) {
			logger.trace("addTableHeadCellTitle: added");
			tableHeadTitle += (tableHeadTitle.isEmpty()?"":", ") + text;
			if (rate > tableHeadTitleMaxRate) {
				tableHeadTitleMaxRate = rate;
			}
			tableHeadTitleLastRow = row;
			tableHeadTitleLastCol = col;
		}
	}

	private void clearTableHeadTitle(WordFile wordFile) {
		flushTableHeadTitle(wordFile);
		tableHeadTitle = "";
		tableHeadTitleMaxRate = 0;
		tableHeadTitleFreeze = false;
		tableHeadTitleLastRow = (-1);
		tableHeadTitleLastCol = (-1);
	}

	private void flushTableHeadTitle(WordFile wordFile) {
		if (tableHeadTitle!=null && !tableHeadTitle.isEmpty()) {
			logger.debug("flushTableHeadTitle: {} ({})", tableHeadTitle, tableHeadTitleMaxRate);
			wordFile.addProposedTitles("T6", tableHeadTitleMaxRate, tableHeadTitle, false);
		}
	}

	/**
	 * @return true if the extraction is within the body first 1-2 pages (estimation)
	 */
	private boolean isWithinDocPreamble() {
		return ((bodyParagraphsCounter < wordDocStartParagraphsLimit) && 
				(bodyWordsCounter < wordDocStartWordsLimit));
	}

	private boolean isWithinIngestionPart() {
		// If paragraphs/words ingestion limit set then return false when the limit is passed
		return ((wordIngestionParagraphsLimit <= 0 || bodyParagraphsCounter < wordIngestionParagraphsLimit) && 
				(wordIngestionWordsLimit <= 0 || bodyWordsCounter < wordIngestionWordsLimit));
	}

	private void resetDocPosition() {
		bodyParagraphsCounter = 0;
		bodyWordsCounter = 0;
	}

	private void updateDocPosition(final HWPFDocument wdDoc, final Paragraph p, final int paragraphWordCount) {
		if (paragraphWordCount > 0) {
			bodyParagraphsCounter++;
		}
		// rough estimation to number or words processed.
		bodyWordsCounter += paragraphWordCount;
	}

	/**
	 * Look for the best candidate for doc title
	 */
	private void lookForDocTitle(final HWPFDocument wdDoc, final Paragraph p, 
			final String pText, final ClaStyle style, final WordFile wordFile) {
		// TODO Heuristics to look for the best candidate for doc title
		
	}
	
	/**
	 * Can be \13..text..\15 or \13..control..\14..text..\15 .
	 * Nesting is allowed
	 * @param wordFile  word file object
	 */
	private int handleSpecialCharacterRuns(final Paragraph p, final int index, final boolean skipStyling, 
			final PicturesSource pictures, final List<String> textParts, final WordFile wordFile,
			final boolean addHyperlinks) {

		List<CharacterRun> controls = new ArrayList<>();
		List<CharacterRun> texts = new ArrayList<>();
		boolean has14 = false;

		// Split it into before and after the 14
		int i;
		for(i=index+1; i<p.numCharacterRuns(); i++) {
			final CharacterRun cr = p.getCharacterRun(i);
			if(cr.text().equals("\u0013")) {
				// Nested, oh joy...
				final int increment = handleSpecialCharacterRuns(p, i+1, skipStyling, pictures, textParts, wordFile, addHyperlinks);
				i += increment;
			} else if(cr.text().equals("\u0014")) {
				has14 = true;
			} else if(cr.text().equals("\u0015")) {
				if(!has14) {
					texts = controls;
					controls = new ArrayList<CharacterRun>();
				}
				break;
			} else {
				if(has14) {
					texts.add(cr);
				} else {
					controls.add(cr);
				}
			}
		}

		// Do we need to do something special with this?
		if(controls.size() > 0) {
			String text = controls.get(0).text();
			for(int j=1; j<controls.size(); j++) {
				text += controls.get(j).text();
			}

			final int q1 = text.indexOf('"');
			if(addHyperlinks && (text.startsWith("HYPERLINK") || text.startsWith(" HYPERLINK"))
					&& q1 > -1) {
				/*
				int q2 = text.lastIndexOf('"');
				String url = text.substring(
						q1 + 1,
						(q2>q1)?q2:text.length());	// 2nd quote may be missing
				// Special handling to HYPERLINK-fields.
				 */
				for(final CharacterRun cr : texts) {
					handleCharacterRun(cr, textParts);
				}
			} else {
				// Just process any pictures that may be embedded
				if (pictures != null) {
					for(final CharacterRun cr : texts) {
						if(pictures.hasPicture(cr)) {
							final Picture picture = pictures.getFor(cr);
							handlePictureCharacterRun(cr, picture, pictures, wordFile);
						//} else {
							// handleCharacterRun(cr, textParts); // field value
						}
					}
				}
				// Process the fields code as text parts
				controls.forEach(cr -> handleFieldcodeRun(cr, textParts));
			}
		} else {
			// We only had text
			// Output as-is
			for(final CharacterRun cr : texts) {
				handleCharacterRun(cr, textParts);
			}
		}

		// Tell them how many to skip over
		return i-index;
	}	// handleSpecialCharacterRuns()

	private void handlePictureCharacterRun(final CharacterRun cr, 
			final Picture picture, final PicturesSource pictures, final WordFile wordFile) {
		if(!isRendered(cr) || picture == null || wordFile == null) {
			// Oh dear, we've run out...
			// Probably caused by multiple \u0008 images referencing
			//  the same real image
			return;
		}

		// Which one is it?
		//String extension = picture.suggestFileExtension();
		//int pictureNumber = pictures.pictureNumber(picture);

		// Make up a name for the picture
		// There isn't one in the file, but we need to be able to reference
		//  the picture from the img tag and the embedded resource
		// String filename = "image"+pictureNumber+(extension.length()>0 ? "."+extension : "");

		// Have we already output this one?
		// (Only expose each individual image once) 
		//if(! pictures.hasOutput(picture)) {
		//	final byte[] content = picture.getContent();
		//	pictures.recordOutput(picture);
		//}
		try {
			if (logger.isDebugEnabled()) {
				try {
					logger.debug("Picture: num={} name={} desc={} mimeType={} type={} ext={} size={}x{} scaleRatio={}x{} WxH={}x{}", 
							pictures.pictureNumber(picture), picture.suggestFullFileName(), picture.getDescription(), picture.getMimeType(),
							picture.suggestPictureType().name(), picture.suggestFileExtension(), picture.getDxaGoal(), picture.getDyaGoal(), 
							picture.getHorizontalScalingFactor(), picture.getVerticalScalingFactor(),
							picture.getWidth(), picture.getHeight());
				} catch (Exception e) {}	// Ignore debug exceptions
			}
			final String text = "P"+ NGramTokenizerHashed.MD5Long(picture.getContent());
			wordFile.histogramAddItem(PVType.PV_Pictures, text);
		} catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
			logger.error("Failed handlePictureCharacterRun in file {}. Error is {}",wordFile.getFileName(),e);
//			e.printStackTrace();
		}
	}

	private void handleCharacterRun(final CharacterRun cr, final List<String> textParts) {
		// Skip trailing newlines
		if(!isRendered(cr) || cr.text().equals("\r")) {
			return;
		}

		// remove unprintable chars
		String cleanText = shouldCleanUnprintableChars ? TextUtils.removeUnprintableCharacters(cr.text()) : cr.text();
		textParts.add(cleanText);
	}

	private void handleFieldcodeRun(final CharacterRun cr, final List<String> textParts) {
		// Skip trailing newlines
		if(!isRendered(cr)) {
			return;
		}

		// Convert entire field string into a single unique token (for later nGram processing).
		final String crText = cr.text().trim();
		if (crText.length() == 0) {
			return;
		}
//		try {
			final String text = getFieldTransformedText(crText);
			textParts.add(text);
//		} catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
//			e.printStackTrace();
//		}
	}
	
	static private final Pattern fieldTransformationPattern = Pattern.compile("F_[A-F0-9]{1,16}[^\\}]*\\}");
	private String removeFieldTransformedText(final String text) {
		Matcher fieldMatcher = fieldTransformationPattern.matcher(text);
		if (fieldMatcher.find()) {
			if (logger.isTraceEnabled()) {
				logger.trace("removeFieldTransformedText: removing fieldTrans from <{}>", text);
			}
			return fieldMatcher.replaceAll("");
		}
		else {
			return text;
		}
	}
	// Replace the field code string with a single identifiable token. Allow the entire pattern to be removed using removeFieldTransformedText().
	private String getFieldTransformedText(final String fieldText) {
//		return "F"+Math.abs(nGramTokenizerHashed.MD5Long(fieldText)) /* for debug*/ +" {"+ fieldText +"}";
		return String.format("F_%X {%s}", nGramTokenizerHashed.MD5Long(fieldText), /* for debug*/ fieldText);
	}	

	private String getParagraphLabel(final String pText) {
		
		String[] split = pLabelIndiactionPattern.split(pText+" ", 2);
		if (split.length == 1) {
			return null;
		}
		
		final Matcher matcher = pLabelPattern.matcher(split[0].trim());
		if (!matcher.find()) {
			return null;
		}
		final String res = matcher.group().trim();
		if (res.length() == 0) {
			return null;
		}
		split = pLabelSplitPattern.split(res, paragraphLabelMaxTokens);
		if (split.length == paragraphLabelMaxTokens) {
			return null;
		}
		
		return res;
	}
	
	private String getParagraphLabel(final Paragraph p) {
		final int runs = Integer.min(3, p.numCharacterRuns())-1;
		for (int j=0;j<runs;++j) {
			final CharacterRun run1 = p.getCharacterRun(j);
			if (p.numCharacterRuns() == 1 && (!run1.isBold()) && (run1.getUnderlineCode() == 0) && (!p.isInTable())) {
				return null;
			}
			String text = run1.text();
			if(text.equals("\u0013")) {
				final List <String> textParts = new LinkedList<>();
				j += handleSpecialCharacterRuns(p, j, false, null, textParts, null, false);
				text = textParts.stream().collect(Collectors.joining());
			}
			for (int i=j+1;i<p.numCharacterRuns();++i) {
				final CharacterRun run2 = p.getCharacterRun(i);
				if (run1.getFontSize() > run2.getFontSize() || 
						(run1.isBold() && !run2.isBold()) ||
						(run1.getUnderlineCode() > run2.getUnderlineCode())) {
					final String ret = getParagraphLabel(text + paragraphLabelDelimiters[1]);
					if (ret != null) {
						return ret;
					}
				}
				else {
					String text2 = run2.text(); 
					if(text2.equals("\u0013")) {
						final List <String> textParts = new LinkedList<>();
						i += handleSpecialCharacterRuns(p, i, false, null, textParts, null, false);
						text2 = textParts.stream().collect(Collectors.joining());
					}
					text = text + text2;
				}
			}
		}
		return null;
	}

	/**
	 * Source: Tika
	 * Provides access to the pictures both by offset, iteration
	 *  over the un-claimed, and peeking forward
	 */
	static private class PicturesSource {
		private final PicturesTable picturesTable; 
		private final Set<Picture> output = new HashSet<>();
		private final Map<Integer,Picture> lookup;
		private final List<Picture> nonU1based;
		private final List<Picture> all;
		private int pn = 0;

		private PicturesSource(final HWPFDocument doc) {
			picturesTable = doc.getPicturesTable();
			all = picturesTable.getAllPictures();

			// Build the Offset-Picture lookup map
			lookup = new HashMap<>();
			for(final Picture p : all) {
				lookup.put(p.getStartOffset(), p);
			}

			// Work out which Pictures aren't referenced by
			//  a \u0001 in the main text
			// These are \u0008 escher floating ones, ones 
			//  found outside the normal text, and who
			//  knows what else...
			nonU1based = new ArrayList<Picture>();
			nonU1based.addAll(all);
			final Range r = doc.getRange();
			for(int i=0; i<r.numCharacterRuns(); i++) {
				final CharacterRun cr = r.getCharacterRun(i);
				if(picturesTable.hasPicture(cr)) {
					final Picture p = getFor(cr);
					final int at = nonU1based.indexOf(p);
					nonU1based.set(at, null);
				}
			}
		}

		private boolean hasPicture(final CharacterRun cr) {
			return picturesTable.hasPicture(cr);
		}

		private void recordOutput(final Picture picture) {
			output.add(picture);
		}

		private boolean hasOutput(final Picture picture) {
			return output.contains(picture);
		}

		private int pictureNumber(final Picture picture) {
			return all.indexOf(picture) + 1;
		}

		private Picture getFor(final CharacterRun cr) {
			return lookup.get(cr.getPicOffset());
		}

		/**
		 * Return the next unclaimed one, used towards
		 *  the end
		 */
		private Picture nextUnclaimed() {
			Picture p;
			while(pn < nonU1based.size()) {
				p = nonU1based.get(pn);
				pn++;
				if(p != null) {
					return p;
				}
			}
			return null;
		}
	}

	/**
	 * Determines if character run should be included in the extraction.
	 * 
	 * @param cr character run.
	 * @return true if character run should be included in extraction.
	 */
	static private boolean isRendered(final CharacterRun cr) {
		return cr == null || !cr.isMarkedDeleted();
	}
	
	private ClaDocProperties extractProperties(final HWPFDocument doc, final POIFSFileSystem poifs) {
		final ClaDocProperties res = new ClaDocProperties();

		if (doc != null) {
			final DocumentProperties dp = doc.getDocProperties();
			//final SummaryInformation ddsi = doc.getSummaryInformation();
			//final String propStr = Arrays.asList(ddsi.getProperties()).stream().map(p->p.toString()).collect(Collectors.joining("|"));
	
			//res.setCreated(new Date(1000*(long)dp.getDttmCreated()));
			//res.setModified(new Date(1000*(long)dp.getDttmRevised()));
			res.setWords(dp.getCWords());
			res.setPages(dp.getCPg());
			res.setChars(dp.getCChWS());
			res.setParagraphs(dp.getCParas());
			res.setLines(dp.getCLines());
		}
		
		SummaryInformation si = null;
		DocumentSummaryInformation dsi = null;
		DocumentEntry siEntry = null;
		if (poifs != null) {
			final DirectoryEntry dir = poifs.getRoot();
			try {
				siEntry = (DocumentEntry)dir.getEntry(SummaryInformation.DEFAULT_STREAM_NAME);
			} catch (final FileNotFoundException e) {
				logger.info("Can not get si.DEFAULT_STREAM entry ", e.getMessage());
			}
		}
		DocumentInputStream dis = null;
		if (siEntry!=null) {
			try {
				dis = new DocumentInputStream(siEntry);
			} catch (final IOException e) {
				logger.info("Can not create DocumentInputStream", e.getMessage());
			}
		}
		PropertySet ps = null;
		if (dis!=null) {
			try {
				ps = new PropertySet(dis);
				dis.close();
				dis = null;
				siEntry = null;
			} catch (final Exception e) {
				logger.info("Can not create PropertySet", e.getMessage());
			}
		}
		if (ps!=null) {
			try {
				si = new SummaryInformation(ps);
			} catch (final UnexpectedPropertySetTypeException e) {
				logger.info("Can not create SummaryInformation", e.getMessage());
			}
		}
		if (poifs != null) {
			final DirectoryEntry dir = poifs.getRoot();
			try {
				siEntry = (DocumentEntry)dir.getEntry(DocumentSummaryInformation.DEFAULT_STREAM_NAME);
			} catch (final FileNotFoundException e) {
				logger.info("Can not get dsi.DEFAULT_STREAM entry ", e.getMessage());
			}
		}
		dis = null;
		if (siEntry!=null) {
			try {
				dis = new DocumentInputStream(siEntry);
			} catch (final IOException e) {
				logger.info("Can not create DocumentInputStream", e.getMessage());
			}
		}
		ps = null;
		if (dis!=null) {
			try {
				ps = new PropertySet(dis);
				dis.close();
				dis = null;
			} catch (final Exception e) {
				logger.info("Can not create PropertySet", e.getMessage());
			}
		}
		if (ps!=null) {
			try {
				dsi = new DocumentSummaryInformation(ps);
			} catch (final UnexpectedPropertySetTypeException e) {
				logger.info("Can not create SummaryInformation", e.getMessage());
			}
		}		
		if (si != null) {
			res.setCreator(si.getAuthor());
			res.setSubject(si.getSubject());
			res.setTitle(si.getTitle());
			res.setKeywords(si.getKeywords());
			res.setAppNameVer(si.getApplicationName()+"/"+si.getRevNumber());
			res.setTemplate(si.getTemplate());
			if (si.getCreateDateTime()!=null) {
				res.setCreated(si.getCreateDateTime());
			}
			if (si.getLastSaveDateTime()!=null) {
				res.setModified(si.getLastSaveDateTime());
			}
			res.setWords(si.getWordCount());
			res.setChars(si.getCharCount());
			res.setPages(si.getPageCount());
			final int sec = si.getSecurity();
			res.setSecPasswordProtected((sec & 1)!=0);
			res.setSecReadonlyRecommended((sec & 2)!=0);
			res.setSecReadonlyEnforced((sec & 4)!=0);
			res.setSecLockedForAnnotations((sec & 8)!=0);
		}
		if (dsi != null) {
			res.setCompany(dsi.getCompany());
			res.setLines(dsi.getLineCount());
			res.setParagraphs(dsi.getParCount());
		}

		logger.debug("Extracted props: " + res.toString());

		return res;
	}	// extractProperties()


}
