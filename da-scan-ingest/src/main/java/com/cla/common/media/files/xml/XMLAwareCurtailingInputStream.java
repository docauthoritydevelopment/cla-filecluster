package com.cla.common.media.files.xml;

import com.google.common.base.Charsets;

import java.io.*;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 *
 * Created by Itay on 2/18/2017. --> TODO: integration not finished. XML parser is not happy with stream termination.!!!
 */
@Deprecated
public class XMLAwareCurtailingInputStream extends InputStream {
    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(XMLAwareCurtailingInputStream.class);

    private enum XMLMachineState {
        XML_MACHINE_STATE_copy_char,
        XML_MACHINE_STATE_read_tag,
        XML_MACHINE_STATE_process_object_start,
        XML_MACHINE_STATE_process_object_end,
        XML_MACHINE_STATE_dump_and_closed,
        XML_MACHINE_STATE_closed,
    };

    public final static String allItemsConfigToken = "ALL";
    public final static String textName = "w:t";

    public int totalObjectsLimit = 50000;         // The LIMIT - 0 = no limit

    private final static List<String> namesStatsList = Arrays.asList("w:t", "w:tbl", "w:p");

    private boolean countText = false;
    private int textCounter = 0;
    private int textSkippedCounter = 0;
    private int skippedObjectEndsCounter = 0;

    private int allGlobalCounter = 0;
    private int skippedObjectsCounter = 0;
    private int allFirstLevelCounter = 0;

    private final Map<String, Integer> globalStats = new HashMap<>();
//    private final Map<String, Integer> fisrtLevelStats = new HashMap<>();

    private int level = 0;
    private boolean skipBytes = false;
    private int skipLevel = 0;
    private XMLMachineState xmlMachineState = XMLMachineState.XML_MACHINE_STATE_copy_char;

    private static final int bufSize = 4069;
    private char[] buf = new char[bufSize];
    private int accumulatedCount = 0;
    private int sentOutCount = 0;

    private StringBuffer nameSb;

    private final InputStream originalInputStream;    // original stream to read from
    private final Reader reader;

    XMLAwareCurtailingInputStream(InputStream originalInputStream) {
        this.originalInputStream = originalInputStream;
        this.reader = new InputStreamReader(originalInputStream, Charsets.UTF_8);
        logger.debug("Start XML filter streaming. Object limit={}", totalObjectsLimit);
    }

    InputStream getCurtailedInputStream() throws IOException {
        return this;
    }

    public void shutDown() {
        try {
            close();
        } catch (IOException e) {}
    }

    StringBuffer deb = new StringBuffer();

    @Override
    public void close() throws IOException {
        xmlMachineState = XMLMachineState.XML_MACHINE_STATE_closed;
        closeReader();
    }

    private byte[] utf8Bytes = null;
    private int utfBytesSent = 0;

    @Override
    public int read() throws IOException {
        if (utf8Bytes == null || utf8Bytes.length <= utfBytesSent) {
            int ch = getSstreamDataNextChar();
            if (ch == -1) {
                return ch;
            }
//            if (ch != -1) {
//                deb.append((char) ch);
//                if (ch == (char) '>') {
//                    logger.debug("Stream output: {}", deb.toString());
//                    deb = new StringBuffer();
//                }
//            }
            utf8Bytes = String.valueOf((char)ch).getBytes(Charsets.UTF_8);
            utfBytesSent = 0;
        }
        return (int)utf8Bytes[utfBytesSent++];
    }

    // Keep for reference test
    private int getSstreamDataNextCharCopy(){
        try {
            charInt = reader.read();
            return charInt;
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (Exception e) {
            logger.error("Exception: state={} buf[{}]={}", xmlMachineState.toString(),
                    accumulatedCount, (accumulatedCount==0)?"":String.valueOf(buf, 0, accumulatedCount));
            throw new RuntimeException(e);
        }
        finally {
        }
    }

    private boolean objectNameFound = false;
    private boolean isObjectEnd = false;
    private boolean isNoBodyObject = false;
    private boolean nameEnd = false;

    private int charInt = -1;
    private int prevChar = -1;

    private void resetObjectFlags() {
        accumulatedCount = 0;
        sentOutCount = 0;
        objectNameFound = false;
        isObjectEnd = false;
        isNoBodyObject = false;
        nameEnd = false;
    }

    /**
     * read from originalInputStream, maintain XML state machine and return next char of filtered stream
     * whoever called the getCurtailedInputStream() will read the resulting data
     */
    private int getSstreamDataNextChar() {
        try {
            while (true) {
                switch (xmlMachineState) {
                    case XML_MACHINE_STATE_read_tag:
                        readChar();
                        // Looking for new item's type
                        buf[accumulatedCount++] = (char) charInt;
                        switch ((char) charInt) {
                            case (char)-1:
                                accumulatedCount--;
                                xmlMachineState = XMLMachineState.XML_MACHINE_STATE_dump_and_closed;
                                break;
                            // It's a closing item
                            case '/':
                                if (objectNameFound) {
                                    nameEnd = true;
                                }
                                isObjectEnd = (prevChar == '<');
                                break;
                            case '?':
                                isNoBodyObject = true;
                                if (objectNameFound) {
                                    nameEnd = true;
                                }
                                break;
                            case '>':
                                isNoBodyObject = isNoBodyObject || (prevChar == '/');

                                if (isObjectEnd && !isNoBodyObject) {
                                    xmlMachineState = XMLMachineState.XML_MACHINE_STATE_process_object_end;
                                    break;
                                } else if (objectNameFound) {
                                    startObject(nameSb.toString());
                                    processTagObjectStart();
                                    xmlMachineState = XMLMachineState.XML_MACHINE_STATE_process_object_start;
                                    break;
                                } else {
                                    logger.warn("Unknown state");
                                }
                                xmlMachineState = XMLMachineState.XML_MACHINE_STATE_copy_char;
                                break;
                            case ' ':
                            case '\n':
                            case '\t':
                                if (objectNameFound) {
                                    nameEnd = true;
                                }
                                // white space, just move on
                                break;
                            default:
                                // looking for name token
                                if (!objectNameFound) {
                                    objectNameFound = true;
                                    nameSb = new StringBuffer();
                                }
                                if (!nameEnd) {
                                    nameSb.append((char) charInt);
                                }
                                break;
                        }
                        break;

                    case XML_MACHINE_STATE_copy_char:
                        readChar();
                        switch (charInt) {
                            case -1:
                                xmlMachineState = XMLMachineState.XML_MACHINE_STATE_closed;
                                processStats();
                                return charInt;
                            case (int)'<':
                                resetObjectFlags();
                                buf[accumulatedCount++] = (char) charInt;
                                xmlMachineState = XMLMachineState.XML_MACHINE_STATE_read_tag;
                                break;
                            default:
                                if (processChar(charInt)) {
                                    return charInt;
                                }
                                break;
                        }
                        break;

                    case XML_MACHINE_STATE_process_object_start:
                        if (sentOutCount < accumulatedCount) {
                            return buf[sentOutCount++];
                        }
                        if (isNoBodyObject) {
                            xmlMachineState = XMLMachineState.XML_MACHINE_STATE_process_object_end;
                        }
                        else {
                            xmlMachineState = XMLMachineState.XML_MACHINE_STATE_copy_char;
                        }
                        break;

                    case XML_MACHINE_STATE_process_object_end:
                        if (!skipBytes || level <= skipLevel) {
                            if (sentOutCount < accumulatedCount) {
                                return buf[sentOutCount++];
                            }
                        }
                        else {
                            logSkippedTag();
                            accumulatedCount = 0;
                            skippedObjectEndsCounter++;
                        }
                        if (skipBytes && level == skipLevel) {
                            skipLevel--;
                            logger.debug("Reducing skip level to {}", skipLevel);
                        }
                        endObject();
                        xmlMachineState = XMLMachineState.XML_MACHINE_STATE_copy_char;
                        break;

                    case XML_MACHINE_STATE_dump_and_closed:
                        if (sentOutCount >= accumulatedCount) {
                            xmlMachineState = XMLMachineState.XML_MACHINE_STATE_closed;
                            processStats();
                            return -1;
                        }
                        return buf[sentOutCount++];

                    case XML_MACHINE_STATE_closed:
                        closeReader();
                        return (char)-1;
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (Exception e) {
            logger.error("Exception: state={} buf[{}]={}", xmlMachineState.toString(),
                    accumulatedCount, (accumulatedCount == 0) ? "" : String.valueOf(buf, 0, accumulatedCount));
            throw new RuntimeException(e);
        }
        finally {
        }
    }

    private void closeReader() {
        try {
            reader.close();
        } catch (IOException e) {}
        try {
            originalInputStream.close();
        } catch (IOException e) {}
    }

    private void readChar() throws IOException {
        prevChar = charInt;
        charInt = reader.read();
    }

    private void processStats() {
        logger.debug("Global stats: allObj={} textCount={} skippedText={} skippedObj={} skippedObjEnds={}",
                allGlobalCounter, textCounter, textSkippedCounter, skippedObjectsCounter, skippedObjectEndsCounter);
        logger.debug("Stats per type: {}", globalStats.entrySet().stream().map(e->"("+e.getKey()+"="+e.getValue()+")").collect(Collectors.joining(",")));
//        logger.debug("Final first-level per type: {}", fisrtLevelStats.entrySet().stream().map(e->"("+e.getKey()+"="+e.getValue()+")").collect(Collectors.joining(",")));
    }

    private void startObject(String name) {
        level++;
        countText = textName.equals(name);
        countName(level, name);
//        logger.debug("Start object {} at level {}", name, level);
    }

    private void endObject() {
//        logger.debug("End object at level {}", level);
        countText = false;
        level--;
    }

    private void countName(int level, String name) {
        ++allGlobalCounter;
        countName(globalStats, name);
    }

    private int countName(Map<String, Integer> stats, String name) {
        if (!namesStatsList.contains(name)) {
            name = "Others";
        }
        Integer count = stats.get(name);
        if (count == null) {
            count = Integer.valueOf(0);
        }
        stats.put(name, ++count);
        return count;
    }

    private int getNameCount(Map<String, Integer> stats, String name) {
        Integer count = stats.get(name);
        return (count == null) ? 0 : count;
    }

//    private void processTagObjectEnd(Writer writer) throws IOException {
//        if (!skipBytes || level <= skipLevel) {
//            processBuf(writer);
//        }
//        else {
//            logSkippedTag();
//            accumulatedCount = 0;
//            skippedObjectEndsCounter++;
//        }
//        if (skipBytes && level == skipLevel) {
//            skipLevel--;
//            logger.debug("Reducing skip level to {}", skipLevel);
//        }
//    }

    private void processTagObjectStart() throws IOException {
        if (skipBytes) {
            logSkippedTag();
            accumulatedCount = 0;
            skippedObjectsCounter++;
        }
        else if (totalObjectsLimit > 0 && allGlobalCounter > totalObjectsLimit) {
            skipBytes = true;
            skipLevel = level-1;
            logger.debug("Trimming XML level={}", level);
            logSkippedTag();
            accumulatedCount = 0;
            skippedObjectsCounter++;
        }
    }

    private void logSkippedTag() {
        if (accumulatedCount>0) {
//            logger.debug("Skip: {}", String.valueOf(buf, 0, accumulatedCount));      // ToDO: remove for performance
        }
    }


    private void processBuf(Writer writer) throws IOException {
        if (0 == accumulatedCount) {
            return;
        }
        int s = accumulatedCount;
        accumulatedCount = 0;
        writer.write(buf, 0, s);
//        logger.debug("Output tag: {}", String.valueOf(buf, 0, s));      // ToDO: remove for performance
    }

    private boolean processChar(int charInt) throws IOException {
        if (countText) {
            textCounter++;
        }
        if (skipBytes) {
            textSkippedCounter++;
            return false;
        }
        return true;
    }

    public void setTotalObjectsLimit(int totalObjectsLimit) {
        this.totalObjectsLimit = totalObjectsLimit;
    }

    public int getTotalObjectsLimit() {
        return totalObjectsLimit;
    }

}
