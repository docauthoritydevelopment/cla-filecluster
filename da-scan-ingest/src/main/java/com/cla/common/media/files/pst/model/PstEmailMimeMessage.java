package com.cla.common.media.files.pst.model;

import com.cla.common.domain.pst.PstPath;
import com.cla.connector.utils.MailRelatedUtils;
import com.pff.PSTMessage;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.Header;
import javax.mail.MessagingException;
import javax.mail.internet.*;
import javax.mail.util.ByteArrayDataSource;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.List;

/**
 * Created By Itai Marko
 */
public class PstEmailMimeMessage extends PstEmailMessage {

    private final byte[] bytes;

    PstEmailMimeMessage(PSTMessage libpstEmailMsg, PstFile pstFile, PstPath pstPath) {
        super(libpstEmailMsg, pstFile, pstPath);
        MimeMessage mimeMessage = getMimeMessage();
        this.bytes = MailRelatedUtils.getMimeMessageBytes(mimeMessage);
    }

    @Override
    public byte[] getContentBytes() {
        return bytes;
    }

    @Override
    public long getContentSize() {
        return bytes.length;
    }

    private MimeMessage getMimeMessage() { // TODO Itai: [DAMAIN-3075] Better use Spring email integration when this code moves th MP
        MimeMessage mimeMessage = MailRelatedUtils.getMimeMessage();
        try {
            setHeadersToMimeMessage(mimeMessage);
            setContentToMimeMessage(mimeMessage);
        } catch (MessagingException | IOException e) {
            throw new RuntimeException("Error occurred while extracting MimeMessage from PstEmailMessage: " + getPstPath().toString(), e);
        }

        return mimeMessage;
    }

    private void setHeadersToMimeMessage(MimeMessage mimeMessage) throws MessagingException, IOException {
        String transportMessageHeaders = libpstEmailMsg.getTransportMessageHeaders().trim();
        if (!transportMessageHeaders.isEmpty()) {
            InternetHeaders headers = new InternetHeaders(new ByteArrayInputStream(transportMessageHeaders.getBytes()));
            headers.removeHeader("Content-Type");
            //noinspection unchecked
            Enumeration<Header> headersEnumeration = headers.getAllHeaders();
            while (headersEnumeration.hasMoreElements()) {
                Header header = headersEnumeration.nextElement();
                mimeMessage.addHeader(header.getName(), header.getValue());
            }
        } else {
            mimeMessage.setSubject(libpstEmailMsg.getSubject().trim());
            mimeMessage.setSentDate(getSentDate());
            InternetAddress fromMailbox = new InternetAddress();
            PstAddress sender = getSender();
            String senderEmailAddress = sender.getEmailAddress();
            fromMailbox.setAddress(senderEmailAddress);
            String senderName = sender.getName();
            if (!senderName.isEmpty()) {
                fromMailbox.setPersonal(senderName); // TODO Itai: [DAMAIN-3075] detect encoding using Tika
            } else {
                fromMailbox.setPersonal(senderEmailAddress);
            }
            mimeMessage.setFrom(fromMailbox);

            List<PstRecipient> recipients = getRecipients();
            for (PstRecipient recipient : recipients) {
                InternetAddress recipientAddress = new InternetAddress(recipient.getEmailAddress(), recipient.getName());
                mimeMessage.setRecipient(recipient.getRecipientType(), recipientAddress);
            }
        }
    }

    private void setContentToMimeMessage(MimeMessage mimeMessage) throws MessagingException, IOException {
        MimeMultipart rootMultipart = new MimeMultipart();
        MimeBodyPart contentBodyPart = new MimeBodyPart();
        MimeMultipart contentMultipart = new MimeMultipart();
        contentBodyPart.setContent(contentMultipart);

        String msgBody = libpstEmailMsg.getBody().trim();
        if (!msgBody.isEmpty()) {
            MimeBodyPart textBodyPart = new MimeBodyPart();
            textBodyPart.setText(msgBody);
            contentMultipart.addBodyPart(textBodyPart);
        }

        String msgHtml = libpstEmailMsg.getBodyHTML().trim();
        if (!msgHtml.isEmpty()) {
            MimeBodyPart htmlBodyPart = new MimeBodyPart();
            htmlBodyPart.setDataHandler(new DataHandler(new ByteArrayDataSource(msgHtml, "text/html"))); // TODO Itai: [DAMAIN-3075] detect mime type using Tika
            contentMultipart.addBodyPart(htmlBodyPart);
        }

        if (msgBody.isEmpty() && msgHtml.isEmpty()) {
            MimeBodyPart textBodyPart = new MimeBodyPart();
            textBodyPart.setText("<<Empty Email Body>>");
            textBodyPart.addHeaderLine("Content-Type: text/plain; charset=\"utf-8\"");
            textBodyPart.addHeaderLine("Content-Transfer-Encoding: quoted-printable");
            contentMultipart.addBodyPart(textBodyPart);
        }
        rootMultipart.addBodyPart(contentBodyPart);

        int attachmentsNum = libpstEmailMsg.getNumberOfAttachments();
        for (int i = 0; i < attachmentsNum; i++) {
            MimeBodyPart attachmentBodyPart = new MimeBodyPart();
            PstAttachment pstAttachment = createChildPstAttachment(i);
            String mimeTag = pstAttachment.getMimeTag();
            if (mimeTag.isEmpty()) {
                mimeTag = "application/octet-stream"; // TODO Itai: [DAMAIN-3075] move to const
            }
            DataSource source = new ByteArrayDataSource(pstAttachment.getAttachmentInputStream(), mimeTag);
            attachmentBodyPart.setDataHandler(new DataHandler(source));
            attachmentBodyPart.setContentID(pstAttachment.getContentId());
            attachmentBodyPart.setFileName(pstAttachment.getFilename());
            rootMultipart.addBodyPart(attachmentBodyPart);
        }

        mimeMessage.setContent(rootMultipart);
    }
}
