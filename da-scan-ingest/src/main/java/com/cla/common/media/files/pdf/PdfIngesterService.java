package com.cla.common.media.files.pdf;

import com.cla.common.domain.dto.mediaproc.word.WordFile;
import com.cla.common.domain.dto.messages.IngestTaskParameters;
import com.cla.common.domain.dto.messages.WordIngestResult;
import com.cla.common.media.files.word.WordIngesterService;
import com.cla.common.media.ingest.FileIngestService;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.file.FileType;
import com.cla.connector.domain.dto.error.ProcessingErrorType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.InputStream;

@Component
public class PdfIngesterService extends FileIngestService<WordIngestResult> {

    @Value("${ingester.pdf.convert-array-histograms:true}")
    private boolean useArrayHistograms;

    @Autowired
    private ClaPdfExtractor pdfExtractor;

    @Override
    protected FileType getBaseFileType() {
        return FileType.PDF;
    }

    @Override
    public WordIngestResult extractViaRequestMessage(
            IngestTaskParameters ingestTaskParameters, InputStream is, String path, boolean extractMetadata,
            ClaFilePropertiesDto claFileProperties) {

        return extract(is, path, extractMetadata, claFileProperties);
    }

    @Override
    public WordIngestResult extract(InputStream is, String path, boolean extractMetadata, ClaFilePropertiesDto claFileProperties) {
        WordFile extract = pdfExtractor.extract(is, path, extractMetadata, claFileProperties);
        return WordIngesterService.convertIngestionResult(extract, claFileProperties, useArrayHistograms);
    }

    @Override
    protected WordIngestResult createIngestPayload(ProcessingErrorType processingErrorType, ClaFilePropertiesDto claFilePropertiesDto, final String text, final Exception e) {
        WordIngestResult result = new WordIngestResult();
        result.setErrorType(processingErrorType);
        result.setClaFileProperties(claFilePropertiesDto);
        result.setExceptionText(e == null ? text : e.getMessage());
        return result;
    }

}
