package com.cla.common.media.files.pst.visitor;

import com.cla.connector.domain.dto.messages.ScanErrorDto;
import com.cla.common.media.files.pst.model.PstAttachment;
import com.cla.common.media.files.pst.model.PstEmailMessage;
import com.cla.common.media.files.pst.model.PstFolder;

/**
 * Created By Itai Marko
 */
public interface PstEntryVisitor <T> {

    T visitFolder(PstFolder pstFolder);

    T visitFolderFailed(PstFolder pstFolder, ScanErrorDto scanError);

    T visitEmailMessage(PstEmailMessage pstEmailMessage);

    T visitAttachment(PstAttachment pstAttachment);
}
