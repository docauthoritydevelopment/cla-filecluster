package com.cla.common.media.files.ooxml;

import com.cla.common.media.files.ppt.ClaXSLFSlideShow;
import org.apache.poi.POIXMLTextExtractor;
import org.apache.poi.extractor.ExtractorFactory;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.openxml4j.opc.PackagePart;
import org.apache.poi.openxml4j.opc.PackageRelationshipCollection;
import org.apache.poi.xslf.extractor.XSLFPowerPointExtractor;
import org.apache.poi.xslf.usermodel.XSLFRelation;
import org.apache.poi.xssf.extractor.XSSFEventBasedExcelExtractor;
import org.apache.poi.xssf.extractor.XSSFExcelExtractor;
import org.apache.poi.xssf.usermodel.XSSFRelation;
import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.apache.poi.xwpf.usermodel.XWPFRelation;
import org.apache.xmlbeans.XmlException;

import java.io.IOException;

/**
 * The method createCustomExtractor was copied from ExtractorFactory.createExtractor in order to enable
 * usage of customized extractors
 *
 * Created by vladi on 4/2/2017.
 */
class ClaExtractorFactory extends ExtractorFactory{

    static POIXMLTextExtractor createCustomExtractor(OPCPackage pkg) throws IOException, OpenXML4JException, XmlException {
        try {
            // Check for the normal Office core document
            PackageRelationshipCollection core;
            core = pkg.getRelationshipsByType(CORE_DOCUMENT_REL);

            // If nothing was found, try some of the other OOXML-based core types
            if (core.size() == 0) {
                // Could it be an OOXML-Strict one?
                core = pkg.getRelationshipsByType(STRICT_DOCUMENT_REL);
            }
            if (core.size() == 0) {
                // Could it be a visio one?
                core = pkg.getRelationshipsByType(VISIO_DOCUMENT_REL);
                if (core.size() == 1)
                    // --------------- CLA customization for Visio ------- *
                    return new ClaXDGFVisioExtractor(pkg);
                    //--------------------------------------------------- *
            }

            // Should just be a single core document, complain if not
            if (core.size() != 1) {
                throw new IllegalArgumentException("Invalid OOXML Package received - expected 1 core document, found " + core.size());
            }

            // Grab the core document part, and try to identify from that
            final PackagePart corePart = pkg.getPart(core.getRelationship(0));
            final String contentType = corePart.getContentType();

            // Excel
            // No special CLA customization, since it is done in the ClaXSSFExcelExtractorDecorator
            for (XSSFRelation rel : XSSFExcelExtractor.SUPPORTED_TYPES) {
                if ( rel.getContentType().equals( contentType ) ) {
                    if (getPreferEventExtractor()) {
                        return new XSSFEventBasedExcelExtractor(pkg);
                    }
                    return new XSSFExcelExtractor(pkg);
                }
            }

            // Word
            for (XWPFRelation rel : XWPFWordExtractor.SUPPORTED_TYPES) {
                if ( rel.getContentType().equals( contentType ) ) {
                    // --------------- CLA customization for Word ------- *
                    return new ClaXWPFWordExtractor(pkg);
                    //--------------------------------------------------- *
                }
            }

            // Power Point
            for (XSLFRelation rel : XSLFPowerPointExtractor.SUPPORTED_TYPES) {
                if ( rel.getContentType().equals( contentType ) ) {
                    // --------------- CLA customization for Power Point ------- *
                    return new ClaXSLFPowerPointExtractor(pkg);
                    //--------------------------------------------------- *
                }
            }

            // Power Point
            // special handling for SlideShow-Theme-files,
            if (XSLFRelation.THEME_MANAGER.getContentType().equals(contentType)) {
                // --------------- CLA customization for Power Point ------- *
                return new ClaXSLFPowerPointExtractor(new ClaXSLFSlideShow(pkg));
                //--------------------------------------------------- *
            }

            throw new IllegalArgumentException("No supported documents found in the OOXML package (found " + contentType + ")");

        } catch (IOException | OpenXML4JException | XmlException | RuntimeException e) {
            // ensure that we close the package again if there is an error opening it, however
            // we need to revert the package to not re-write the file via close(), which is very likely not wanted for a TextExtractor!
            pkg.revert();
            throw e;
        }

    }

}
