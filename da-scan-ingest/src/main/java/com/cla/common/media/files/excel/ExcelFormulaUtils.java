package com.cla.common.media.files.excel;

import org.apache.poi.ss.formula.*;
import org.apache.poi.ss.formula.ptg.*;
import org.apache.poi.ss.usermodel.Cell;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * Created by uri on 19/10/2015.
 */
public class ExcelFormulaUtils {
    private static Logger logger = LoggerFactory.getLogger(ExcelFormulaUtils.class);

    public static String[] getOperands(final Stack<String> stack, final int nOperands) {
        final String[] operands = new String[nOperands];

        for (int j = nOperands - 1; j >= 0; j--) { // reverse iteration because args were pushed in-order
            if (stack.isEmpty()) {
                final String msg = "Too few arguments supplied to operation. Expected (" + nOperands
                        + ") operands but got (" + (nOperands - j - 1) + ")";
                throw new IllegalStateException(msg);
            }
            operands[j] = stack.pop();
        }
        return operands;
    }

    public static String formulaIngestion(Object evaluationWorkbookWrapper, int sheetIndex, String formula, Object stringRepresenter, int columnIndex, int rowIndex) {
        Ptg[] ptgs;
            /* parse formula */
        try {
            ptgs = FormulaParser.parse(formula, (FormulaParsingWorkbook) evaluationWorkbookWrapper, FormulaType.CELL, sheetIndex);
        } catch (final Exception e) {
            if (e instanceof FormulaParseException) {
                final String f2 = formula.replaceAll("#REF!", "");
                try {
                    ptgs = FormulaParser.parse(f2, (FormulaParsingWorkbook) evaluationWorkbookWrapper, FormulaType.CELL, sheetIndex);
                } catch (final Exception e2) {
                    logger.trace("Unexpected exception at sheet# {} cell={} formula={}. Exception1: {} Exception2: {}", sheetIndex, stringRepresenter.toString(), formula, e, e2);
//                	return null;
                    return formula;    // better have a unique formula than a null string
                }
            } else {
                logger.trace("Unexpected exception at sheet# {} cell={} formula={}. Exception: {}", sheetIndex, stringRepresenter.toString(), formula, e);
//            	return null;
                return formula;    // better have a unique formula than a null string
            }
        }

        final FormulaRefMapper refMapper = new FormulaRefMapper();


        if (ptgs == null || ptgs.length == 0) {
            throw new IllegalArgumentException("ptgs must not be null");
        }

        // 1st pass - collect cell references from formula
        for (final Ptg ptg : ptgs) {
            if ((ptg instanceof RefPtg) ||
                    (ptg instanceof AreaPtg)) {
                String key = null;
                try {
                    key = ptg.toFormulaString();
                    refMapper.addPtg(key, ptg);
                } catch (final Exception e) {
                    logger.debug("Unexpected exception at sheet# {} cell={} key={} formula={}. Exception: {}", sheetIndex, stringRepresenter.toString(), key, formula, e);
                }
                continue;
            }
            if ((ptg instanceof Ref3DPtg) ||
                    (ptg instanceof Area3DPtg)) {
                final WorkbookDependentFormula optg = (WorkbookDependentFormula) ptg;
                String key = null;
                try {
                    key = optg.toFormulaString((FormulaRenderingWorkbook) evaluationWorkbookWrapper);
                    refMapper.addPtg(key, ptg);
                } catch (final Exception e) {
                    logger.debug("Unexpected exception at sheet# {} cell={} key={} formula={}. Exception: {}", sheetIndex, stringRepresenter.toString(), key, formula, e);
                }
                continue;
            }
            if ((ptg instanceof Ref3DPxg) ||
                    (ptg instanceof Area3DPxg)) {
                final Pxg3D optg = (Pxg3D) ptg;
                String key = null;
                try {
                    key = optg.toFormulaString();
                    refMapper.addPtg(key, ptg);
                } catch (final Exception e) {
                    logger.debug("Unexpected exception at sheet# {} cell={} key={} formula={}. Exception: {}", sheetIndex, stringRepresenter.toString(), key, formula, e);
                }
                continue;
            }
        }

        // 2nd pass - Re-build formula string
        final Stack<String> stack = new Stack<String>();
        for (final Ptg ptg : ptgs) {
            // @@@ - MemNoMemPtg: not supported by POI; not so common though
            if (ptg instanceof MemAreaPtg || ptg instanceof MemFuncPtg || ptg instanceof MemErrPtg) {
                // marks the start of a list of area expressions which will be naturally combined
                // by their trailing operators (e.g. UnionPtg)
                // 	put comment and throw exception in toFormulaString() of these classes
                continue;
            }
            if (ptg instanceof ParenthesisPtg) {
                final String contents = stack.pop();
                stack.push("(" + contents + ")");
                continue;
            }
            if (ptg instanceof AttrPtg) {
                final AttrPtg attrPtg = ((AttrPtg) ptg);
                if (attrPtg.isOptimizedIf() || attrPtg.isOptimizedChoose() || attrPtg.isSkip()) {
                    continue;
                }
                if (attrPtg.isSpace()) {
                    // POI currently doesn't render spaces in formulas
                    continue;
                    // but if it ever did, care must be taken:
                    // tAttrSpace comes *before* the operand it applies to, which may be consistent
                    // with how the formula text appears but is against the RPN ordering assumed here
                }
                if (attrPtg.isSemiVolatile()) {
                    // similar to tAttrSpace - RPN is violated
                    continue;
                }
                if (attrPtg.isSum()) {
                    final String[] operands = getOperands(stack, attrPtg.getNumberOfOperands());
                    try {
                        stack.push(attrPtg.toFormulaString(operands));
                    } catch (final Exception e) {
                        logger.debug("Unexpected exception at sheet# {} cell={} formula={}. Exception: {}", sheetIndex, stringRepresenter.toString(), formula, e);
                    }
                    continue;
                }
                logger.debug("Unexpected tAttr {} at sheet# {} cell={} formula={}", attrPtg.toString(), sheetIndex, stringRepresenter.toString(), formula);
                // throw new RuntimeException("Unexpected tAttr: " + attrPtg.toString());
            }
            if (ptg instanceof WorkbookDependentFormula) {
                // Handle reference strings - replace formula ref with a symbolic parameter index
                final WorkbookDependentFormula optg = (WorkbookDependentFormula) ptg;
                String key = null;
                try {
                    key = optg.toFormulaString((FormulaRenderingWorkbook) evaluationWorkbookWrapper);
                    final String newName = refMapper.rename(key, columnIndex, rowIndex);
                    stack.push(newName);
                } catch (final Exception e) {
                    logger.debug("Unexpected exception at sheet# {} cell={} key={} formula={}. Exception: {}", sheetIndex, stringRepresenter.toString(), key, formula, e);
                }
                continue;
            }
            if (ptg instanceof Pxg3D) {
                // Handle reference strings - replace formula ref with a symbolic parameter index
                final Pxg3D optg = (Pxg3D) ptg;
                String key = null;
                try {
                    key = optg.toFormulaString();
                    final String newName = refMapper.rename(key, columnIndex, rowIndex);
                    stack.push(newName);
                } catch (final Exception e) {
                    logger.debug("Unexpected exception at sheet# {} cell={} key={} formula={}. Exception: {}", sheetIndex, stringRepresenter.toString(), key, formula, e);
                }
                continue;
            }
            if ((ptg instanceof RefPtgBase) ||    //base class for cell reference
                    (ptg instanceof AreaPtgBase))    //base class for cell range reference
            {
                // Handle reference strings - replace formula ref with a symbolic parameter index
                String key = null;
                try {
                    key = ptg.toFormulaString();
                    final String newName = refMapper.rename(key, columnIndex, rowIndex);
                    stack.push(newName);
                } catch (final Exception e) {
                    logger.debug("Unexpected exception at sheet# {} cell={} key={} formula={}. Exception: {}", sheetIndex, stringRepresenter.toString(), key, formula, e);
                }
                continue;
            }
            if (!(ptg instanceof OperationPtg)) {
                try {
                    stack.push(ptg.toFormulaString());
                } catch (final Exception e) {
                    logger.debug("Unexpected exception at sheet# {} cell={} formula={}. Exception: {}", sheetIndex, stringRepresenter.toString(), formula, e);
                }
                continue;
            }

            final OperationPtg o = (OperationPtg) ptg;
            final String[] operands = getOperands(stack, o.getNumberOfOperands());
            try {
                stack.push(o.toFormulaString(operands));
            } catch (final Exception e) {
                logger.debug("Unexpected exception at sheet# {} cell={} formula={}. Exception: {}", sheetIndex, stringRepresenter.toString(), formula, e);
            }
        }
        if (stack.isEmpty()) {
            // inspection of the code above reveals that every stack.pop() is followed by a
            // stack.push(). So this is either an internal error or impossible.
            logger.debug("formulaIngestion: stack underflow at sheet# {} cell={} formula={}", sheetIndex, stringRepresenter.toString(), formula);
//            throw new IllegalStateException("Stack underflow");
        }
        final String result = stack.pop();
        if (!stack.isEmpty()) {
            // Might be caused by some tokens like AttrPtg and Mem*Ptg, which really shouldn't
            // put anything on the stack
            logger.debug("formulaIngestion: too much stuff left on the stack at sheet# {} cell={} formula={}", sheetIndex, stringRepresenter.toString(), formula);
//            throw new IllegalStateException("too much stuff left on the stack");
        }


//        refMapper.delete();
//        refMapper = null;
//        logger.info("f({},{},{}): '{}' -> '{}'", sheetIndex, rowIndex, columnIndex, formula, result);

        return result;
    }

    /*
         * Taken from POI: org.apache.poi.ss.formula: FormulaRenderer.toFormulaString()
         *
         * Replace cell references in formula with relative index references (relative to cell beseCol,baseRow)
         * Expect to get a formula string which is more tolerant to occasional row/column insertion/deletion.
         */
    public static String formulaIngestion(final Cell cell, final Object evaluationWorkbookWrapper, final int sheetIndex) {

        if (cell.getCellType() != Cell.CELL_TYPE_FORMULA) {
            return null;
        }

        final String formula = cell.getCellFormula();
        final Object stringRepresenter = cell;
        int columnIndex = cell.getColumnIndex();
        int rowIndex = cell.getRowIndex();

        return formulaIngestion(evaluationWorkbookWrapper, sheetIndex, formula, stringRepresenter, columnIndex, rowIndex);
    }    // END formulaIngestion()

    public static class CellRefData {
        public enum RefType {
            REF_CELL,
            REF_AREA,
            REF_EXTERN_CELL,
            REF_EXTERN_AREA
        }

        int col1, row1, col2, row2;
        int sheetRef;
        RefType type;

        public CellRefData(final Ptg p) {
            if (p instanceof RefPtg) {
                final RefPtg cp = (RefPtg) p;
                col1 = cp.getColumn();
                row1 = cp.getRow();
                col2 = row2 = -1;
                sheetRef = -1;
                type = RefType.REF_CELL;
            } else if (p instanceof AreaPtg) {
                final AreaPtg cp = (AreaPtg) p;
                col1 = cp.getFirstColumn();
                row1 = cp.getFirstRow();
                col2 = cp.getLastColumn();
                row2 = cp.getLastRow();
                sheetRef = -1;
                type = RefType.REF_AREA;
            } else if (p instanceof Ref3DPtg) {
                final Ref3DPtg cp = (Ref3DPtg) p;
                col1 = cp.getColumn();
                row1 = cp.getRow();
                col2 = row2 = -1;
                sheetRef = cp.getExternSheetIndex();
                type = RefType.REF_EXTERN_CELL;
            } else if (p instanceof Ref3DPxg) {
                final Ref3DPxg cp = (Ref3DPxg) p;
                col1 = cp.getColumn();
                row1 = cp.getRow();
                col2 = row2 = -1;
                sheetRef = cp.getExternalWorkbookNumber();
                type = RefType.REF_EXTERN_CELL;
            } else if (p instanceof Area3DPtg) {
                final Area3DPtg cp = (Area3DPtg) p;
                col1 = cp.getFirstColumn();
                row1 = cp.getFirstRow();
                col2 = cp.getLastColumn();
                row2 = cp.getLastRow();
                sheetRef = cp.getExternSheetIndex();
                type = RefType.REF_EXTERN_AREA;
            } else if (p instanceof Area3DPxg) {
                final Area3DPxg cp = (Area3DPxg) p;
                col1 = cp.getFirstColumn();
                row1 = cp.getFirstRow();
                col2 = cp.getLastColumn();
                row2 = cp.getLastRow();
                sheetRef = cp.getExternalWorkbookNumber();
                type = RefType.REF_EXTERN_AREA;
            }
        }

        @Override
        public String toString() {
            String s = null;
            switch (type) {
                case REF_CELL:
                    s = new String("<C" + col1 + ",R" + row1 + ">");
                    break;
                case REF_AREA:
                    s = new String("<C" + col1 + ",R" + row1 + ":" + col2 + ",R" + row2 + ">");
                    break;
                case REF_EXTERN_CELL:
                    s = new String("<S" + sheetRef + ",C" + col1 + ",R" + row1 + ">");
                    break;
                case REF_EXTERN_AREA:
                    s = new String("<S" + sheetRef + ",C" + col1 + ",R" + row1 + ":" + col2 + ",R" + row2 + ">");
                    break;
            }
            return s;
        }
    }    // internal class CellRefData

    public static class RefIdexHelper {

        private final Map<Integer, Set<Integer>> inxMap;

        public RefIdexHelper() {
            inxMap = new LinkedHashMap<>();
        }

        public void add(final int sheetIndex, final int coord) {
            Set<Integer> s = inxMap.get(sheetIndex);
            if (s == null) {
                s = new HashSet<>();
                inxMap.put(sheetIndex, s);
            }
            s.add(coord);
        }

        public int getOdrinal(final int sheetIndex, final int coord, int reference) {
            if (coord == reference) {
                return 0;
            }
            final Set<Integer> s = inxMap.get(sheetIndex);
            if (s == null) {
                return 0;
            }
            int count = 0;
            if (sheetIndex != -1) {
                reference = 0;        // for external sheet ignore the reference coordinate
            }
            if (reference < coord) {
                for (final Integer i : s) {
                    if (i > reference && i <= coord) {
                        ++count;
                    }
                }
            } else {
                for (final Integer i : s) {
                    if (i < reference && i >= coord) {
                        --count;        // negative value to indicate lower reference
                    }
                }
            }
            return count;
        }

        // T OD O: check nessecity and implementation of the memory clean-up
//    	public void delete() {
//    		final Set<Integer> keys = new HashSet<>(inxMap.keySet());
//    		for (final Integer key : keys) {
//    			inxMap.remove(key);
//    		}
//    	}
    }    // internal class RefIdexHelper

    public static class FormulaRefMapper {
        private final Map<String, CellRefData> cellRefMap;
        private final RefIdexHelper colIndexHelper;
        private final RefIdexHelper rowIndexHelper;

        public FormulaRefMapper() {
            cellRefMap = new LinkedHashMap<>();
            colIndexHelper = new RefIdexHelper();
            rowIndexHelper = new RefIdexHelper();
        }

        public void addPtg(final String key, final Ptg p) {
            CellRefData refData = cellRefMap.get(key);
            if (refData == null) {
                refData = new CellRefData(p);
                colIndexHelper.add(refData.sheetRef, refData.col1);
                rowIndexHelper.add(refData.sheetRef, refData.row1);
                if (refData.type == CellRefData.RefType.REF_AREA ||
                        refData.type == CellRefData.RefType.REF_EXTERN_AREA) {
                    colIndexHelper.add(refData.sheetRef, refData.col2);
                    rowIndexHelper.add(refData.sheetRef, refData.row2);
                }
                cellRefMap.put(key, refData);
            }
        }

        public String rename(final String key, final int cellCol, final int cellRow) {
            final CellRefData refData = cellRefMap.get(key);
            if (refData == null) {
                return key;        // ref data not found: return original formula string
            }
            String newName = new String("<");
            if (refData.sheetRef >= 0) {
                newName = newName.concat("" + refData.sheetRef + "!");
            }

            int newCol = colIndexHelper.getOdrinal(refData.sheetRef, refData.col1, cellCol);
            int newRow = rowIndexHelper.getOdrinal(refData.sheetRef, refData.row1, cellRow);
            newName = newName.concat("" + newCol + "|" + newRow);
            if (refData.type == CellRefData.RefType.REF_AREA ||
                    refData.type == CellRefData.RefType.REF_EXTERN_AREA) {
                newCol = colIndexHelper.getOdrinal(refData.sheetRef, refData.col2, cellCol);
                newRow = rowIndexHelper.getOdrinal(refData.sheetRef, refData.row2, cellRow);
                newName = newName.concat(":" + newCol + "|" + newRow);
            }
            newName = newName.concat(">");
            return newName;
        }

        // T OD O: check necessity and implementation of the memory clean-up
//		public void delete() {
//			cellRefMap = null;
//			colIndexHelper.delete();
//			colIndexHelper = null;
//			rowIndexHelper.delete();
//			rowIndexHelper = null;
//		}
    }    // internal class FormulaRefMapper
}
