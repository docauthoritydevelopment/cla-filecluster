package com.cla.common.media.files;

import com.cla.common.domain.dto.messages.ExtractionResult;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import org.springframework.beans.factory.annotation.Value;

import java.io.InputStream;

/**
 * Common super-class for all extractors
 *
 * TODO: this is mostly a stub. Should move here more common functionality from the extending classes
 * Created by vladi on 1/1/2017.
 */
public abstract class Extractor<T extends ExtractionResult> {

    @Value("${extraction.userContentSignatureEnabled:true}")
    protected boolean userContentSignatureEnabled;

    @Value("${ingester.patternSearchActive:true}")
    protected boolean patternSearchActive;

    @Value("${ingester.excel.use-formatted-values:false}")
    protected boolean useFormattedPatterns;

    protected boolean patternSearchActiveSet = false;

    public void setPatternSearchActive(final boolean patternSearchActive) {
        this.patternSearchActive = patternSearchActive;
        this.patternSearchActiveSet = true;
    }

    public boolean isPatternSearchActive() {
        return patternSearchActive;
    }

    public boolean isUseFormattedPatterns() {
        return useFormattedPatterns;
    }

    public void setUseFormattedPatterns(boolean useFormattedPatterns) {
        this.useFormattedPatterns = useFormattedPatterns;
    }

    public abstract T extract(final InputStream is, final String path,
                              final boolean extractMetadata, ClaFilePropertiesDto claFileProperties);

}
