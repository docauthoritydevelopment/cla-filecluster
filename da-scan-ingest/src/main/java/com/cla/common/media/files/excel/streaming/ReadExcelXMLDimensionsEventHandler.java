package com.cla.common.media.files.excel.streaming;

import com.cla.common.domain.dto.excel.ExcelSheetDimensions;
import com.google.common.collect.Maps;
import org.apache.poi.ss.util.CellReference;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.Map;
import java.util.regex.Pattern;

/**
 *
 * Created by uri on 20/10/2015.
 */
public class ReadExcelXMLDimensionsEventHandler extends DefaultHandler {

    public static final String POSITION_REGEX = "(?<=\\D)(?=\\d)|(?<=\\d)(?=\\D)";

    public static final Pattern positionRegexPattern = Pattern.compile(POSITION_REGEX);

    private final Map<String, ExcelSheetDimensions> excelSheetDimensionsMap = Maps.newHashMap();

    private ExcelSheetDimensions currentExcelSheetDimensions;

    private int lastEncounteredRow = -1;

    private StringBuilder lastContents;

    private String lastReferencePosition;
    private String lastType;

    static Pattern refPattern = Pattern.compile("(?<=\\D)(?=\\d)|(?<=\\d)(?=\\D)");

    public void initSheetDimensions(String sheetName) {
        ExcelSheetDimensions value = new ExcelSheetDimensions();
        excelSheetDimensionsMap.put(sheetName,value);
        currentExcelSheetDimensions = value;
    }

    public void startElement(String uri, String localName, String name,
                             Attributes attributes) throws SAXException {

        switch (name) {
            case "c": // c => cell
                handleCellStart(attributes);
                break;
            case "worksheet":
                break;
            case "dimension":
                break;
            case "sheetViews":
                break;
            case "sheetView":
                //Attributes: rightToLeft, tabSelected, workbookViewId,
                break;
            case "sheetFormatPr":
                //Attributes: defaultRowHeight, x14ac
                break;
            case "sheetData":
                //Attributes: defaultRowHeight, x14ac, workbookViewId,
                break;
            case "pageMargins":
                break;
        }
        // Clear contents cache
        lastContents = new StringBuilder();

    }

    private void handleCellStart(Attributes attributes) {
        lastType = attributes.getValue("t");
        lastReferencePosition = attributes.getValue("r");
    }

    public void characters(char[] ch, int start, int length)
            throws SAXException {
        lastContents.append(ch,start,length);
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (!"c".equals(qName)) {
            return; //We only care about cell elements for now
        }
        String cellType = lastType;
        if("e".equals(cellType)) {
            //ERROR cell type
            return;
        }
        if ("n".equals(cellType) || cellType == null) {
            //Numeric cell may be a blank cell
            if (lastContents == null || lastContents.length() == 0) {
                //BLANK cell
                return;
            }
        }
        String ref = lastReferencePosition;
        if (ref == null) {
            throw new RuntimeException("Can't extract row data from cell "+uri+" "+localName+" "+qName);
        }
        String[] coord = positionRegexPattern.split(ref);
        Integer columnIndex = CellReference.convertColStringToIndex(coord[0]);
        int rowIndex = Integer.parseInt(coord[1]) - 1;

        currentExcelSheetDimensions.updateColumn(columnIndex.shortValue());
        currentExcelSheetDimensions.updateRow(rowIndex);
        if (rowIndex != lastEncounteredRow) {
            currentExcelSheetDimensions.addNonBlankRow();
            lastEncounteredRow = rowIndex;
        }
        currentExcelSheetDimensions.addNonBlankCell(rowIndex);

    }

    public Map<String, ExcelSheetDimensions> getExcelSheetDimensionsMap() {
        return excelSheetDimensionsMap;
    }
}
