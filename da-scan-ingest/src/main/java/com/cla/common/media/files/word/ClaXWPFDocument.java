package com.cla.common.media.files.word;

import com.cla.common.media.files.xml.CurtailedPackagePart;
import org.apache.poi.POIXMLDocumentPart;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.openxml4j.opc.PackagePart;
import org.apache.poi.util.PackageHelper;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;

/**
 *
 * Created by vladi on 2/16/2017.
 */
public class ClaXWPFDocument extends XWPFDocument {
    private static final Logger logger = LoggerFactory.getLogger(ClaXWPFDocument.class);

    // size threshold in bytes
    private static int sizeThreshold;

    private static int totalObjectsLimit;

    public ClaXWPFDocument(OPCPackage pkg) throws IOException {
        super(pkg);
        load(XWPFFactory.getInstance());
    }

    ClaXWPFDocument(InputStream is) throws IOException {
        super(PackageHelper.open(is));
        load(XWPFFactory.getInstance());
    }

    public static void setSizeThresholds(int xmlFilteringSizeThreshold, int xmlFilteringMaxObjects) {
        sizeThreshold = xmlFilteringSizeThreshold;
        totalObjectsLimit = xmlFilteringMaxObjects;
    }

    @Override
    protected void onDocumentRead() throws IOException {
        try {
            Field packagePartField = POIXMLDocumentPart.class.getDeclaredField("packagePart");
            packagePartField.setAccessible(true);

            PackagePart packagePart = getPackagePart();
            if (!(packagePart instanceof CurtailedPackagePart)) {
                int dataSize = packagePart.getInputStream().available();

                // if data size is bigger than the threshold - replace the original package part with the wrapper
                if (dataSize > sizeThreshold) {
                    logger.debug("XML data size ({}) is over the threshold ({}). Using XML aware pipe filter", dataSize, sizeThreshold);
                    CurtailedPackagePart part = new CurtailedPackagePart(packagePart);
                    part.setTotalObjectsLimit(totalObjectsLimit);
                    packagePartField.set(this, part);
                }
            }

            super.onDocumentRead();
        } catch (NoSuchFieldException | IllegalAccessException | InvalidFormatException e) {
            throw new RuntimeException(e);
        }
    }

}
