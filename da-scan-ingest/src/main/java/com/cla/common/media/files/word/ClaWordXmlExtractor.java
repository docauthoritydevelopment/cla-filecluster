package com.cla.common.media.files.word;

import com.cla.common.domain.dto.ClaDocProperties;
import com.cla.common.domain.dto.histogram.ClaHistogramCollection;
import com.cla.common.domain.dto.histogram.ClaStyle;
import com.cla.common.domain.dto.pv.PVType;
import com.cla.common.domain.dto.mediaproc.word.WordFile;
import com.cla.common.domain.dto.word.WordfileMetaData;
import com.cla.common.media.files.Extractor;
import com.cla.common.utils.tokens.HashedTokensConsumer;
import com.cla.common.utils.tokens.NGramTokenizerHashed;
import com.cla.common.utils.signature.SigningStringBuilder;
import com.cla.common.utils.thread.ClaPropertySet;
import com.cla.common.utils.thread.ClaThreadPropertiesSet;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.error.ProcessingErrorType;
import org.apache.poi.POIXMLProperties;
import org.apache.poi.POIXMLProperties.CoreProperties;
import org.apache.poi.POIXMLProperties.ExtendedProperties;
import org.apache.poi.xwpf.model.XWPFCommentsDecorator;
import org.apache.poi.xwpf.model.XWPFHeaderFooterPolicy;
import org.apache.poi.xwpf.usermodel.*;
import org.apache.xmlbeans.XmlCursor;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlObject;
import org.openxmlformats.schemas.officeDocument.x2006.extendedProperties.CTProperties;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.*;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STFldCharType.Enum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.namespace.QName;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class ClaWordXmlExtractor extends Extractor<WordFile> {
	private static final Logger logger = LoggerFactory.getLogger(ClaWordXmlExtractor.class);

	@Autowired
	private NGramTokenizerHashed nGramTokenizerHashed;
	
	private HashedTokensConsumer hashedTokensConsumer;

	// private final Pattern alphaNumericPattern = Pattern.compile("[a-zA-Z0-9]");
	static private final Pattern pLabelIndiactionPattern = Pattern.compile("[\t:]\\s");
//	static private final Pattern pLabelPattern = Pattern.compile("[\\w&&[\\D]].*");	// Note that pattern may be locale specific (start of word)
	static private final Pattern pLabelPattern = Pattern.compile("[\\p{IsAlphabetic}&&[\\D]].*");	// Note that pattern may be locale specific (start of word)
	static private final Pattern pLabelSplitPattern = Pattern.compile("\\s+");
//	static private final Pattern injectedFieldCodePattern = Pattern.compile("(F\\d+\\s\\{.*\\})|_+\\s?");	// Note that pattern may be locale specific (start of word)
	private final static String[] paragraphLabelDelimiters = {"\t", ":", "   "};

	private static final ClaStyle.Justification[] justifications = {
		ClaStyle.Justification.LEFT_JUST
		,ClaStyle.Justification.CENTER_JUST
		,ClaStyle.Justification.RIGHT_JUST
		,ClaStyle.Justification.JUSTIFIED_JUST
		};

	private enum WordPartType {
		WP_none,
		WP_body,
		WP_headerFooter
	}
	
	private enum Runtype {
		RT_Text,
		RP_FieldStart,
		RP_FieldDef,
		RP_FieldSeperator,
		RP_FieldEnd,
		RT_Other
	}
	
	@Value("${wordFetchHyperlinks:false}")
	private boolean fetchHyperlinks;
	
	@Value("${wordBodyTextNgramSize:5}")
	private short bodyTextNgramSize;

	@Value("${wordHeadingsTextNgramSize:3}")
	private short headingsTextNgramSize;

	@Value("${wordMaxHeadingLength:85}")
	private int maxHeadingLength;

	@Value("${wordMinHeadingLength:5}")
	private int minHeadingLength;

	@Value("${paragraphLabelMaxTokens:4}")
	private int paragraphLabelMaxTokens;

	@Value("${word.titleExtraction.ignoreRightJustified:true}")
	private boolean titleExtractionIgnoreRightJustified;
	
	@Value("${word.titleExtraction.MaxCount:10}")		// TODO: should be 5 or 3
	private int titleExtractionMaxCount;

	@Value("${word.titleExtraction.addStyleToken:false}")
	private boolean debTitleExtractionAddStyleToken;

	@Value("${word.getFontSizeStats:false}")
	private boolean isGetFontSizeStats;

	@Value("${ingester.word.tableRowForTitleExtraction:4}")
	private int tableRowForTitleExtraction;

	@Value("${ingester.word.docStartWordsLimit:1000}")
	private int wordDocStartWordsLimit;
	
	@Value("${ingester.word.docStartParagraphsLimit:30}")
	private int wordDocStartParagraphsLimit;
	
	@Value("${ingester.word.ingestionWordsLimit:0}")
	private int wordIngestionWordsLimit;

	@Value("${ingester.word.xmlFilteringSizeThreshold:1000000}")
	private int xmlFilteringSizeThreshold;

	@Value("${ingester.word.xmlFilteringMaxObjects:900000}")
	private int xmlFilteringMaxObjects;

	@Value("${ingester.word.ingestionParagraphsLimit:0}")
	private int wordIngestionParagraphsLimit;

	private int bodyParagraphsCounter = 0;
	private int bodyWordsCounter = 0;

	private int logNumerator = 1;
	private XWPFStyles styles = null;
	private CTRPr docDefaultRPr = null;
	
	private String tableHeadTitle = "";
	private int tableHeadTitleMaxRate = 0;
	private boolean tableHeadTitleFreeze = false;
	private int tableHeadTitleLastRow = (-1);
	private int tableHeadTitleLastCol = (-1);

	@Value("${tokenMatcherActive}")
	private boolean isTokenMatcherActive;

	private boolean prevParagraphWasTitle = false;
	
	//private final CTPPr docDefaultPPr = null;		// Build workaround: public jar for poi-ooxml-schemas-3.11-beta1 is missing CTPPrDefault class)
	
	private int exsParagraphId = 1;

	public HashedTokensConsumer getHashedTokensConsumer() {
		return hashedTokensConsumer;
	}

	public void setHashedTokensConsumer(final HashedTokensConsumer hashedTokensConsumer) {
		this.hashedTokensConsumer = hashedTokensConsumer;
	}

	public static void setSizeThresholds(int sizeThreshold, int xmlFilteringMaxObjects) {
		ClaXWPFDocument.setSizeThresholds(sizeThreshold, xmlFilteringMaxObjects);
	}

	public WordFile extract(final InputStream is, final String path, final boolean extractMetadata, ClaFilePropertiesDto claFileProperties){
		final WordFile wordFile = new WordFile(path);

		ClaPropertySet claPropertySet = ClaThreadPropertiesSet.getThreadLocalPropertiesSet();
		claPropertySet.setBoolean(ClaThreadPropertiesSet.KEY_TRIMMED, false);
		claPropertySet.setString(ClaThreadPropertiesSet.KEY_PATH, path);

		try {
			setSizeThresholds(xmlFilteringSizeThreshold, xmlFilteringMaxObjects);
			if (logNumerator==1) {
				logger.info("ClaWordXmlExtractor configuration parameters: {}, {}, {}, {}",
						"bodyTextNgramSize="+bodyTextNgramSize,
						"headingsTextNgramSize="+headingsTextNgramSize,
						"maxHeadingLength="+maxHeadingLength,
						"minHeadingLength="+minHeadingLength);
			}
			logger.debug("Start extracting file {} ...",path);
			final XWPFDocument doc = new ClaXWPFDocument(is);
			final XWPFHeaderFooterPolicy hfPolicy = doc.getHeaderFooterPolicy();
			styles = doc.getStyles();
		    if (styles == null) {
		    	logger.warn("Doc styles list is null");
		    	wordFile.setErrorType(ProcessingErrorType.ERR_CORRUPTED);
		    	return wordFile;
		    }
			CTDocDefaults dd = null;
			try {
				dd = doc.getStyle().isSetDocDefaults()?doc.getStyle().getDocDefaults():null;
			} catch (final Exception e) {
		    	logger.warn("Cannot get DocDefaults");
			}
			if (dd!=null) {
				docDefaultRPr = (dd.isSetRPrDefault() && dd.getRPrDefault().isSetRPr())?dd.getRPrDefault().getRPr():null;
			}
			//docDefaultPPr = (dd.isSetPPrDefault() && dd.getPPrDefault().isSetPPr())?dd.getPPrDefault().getPPr():null;
			
			nGramTokenizerHashed.setDebugHashMapper(WordFile.getDebugHashMapper());		// For debug/troubleshooting

			if(extractMetadata){
				final ClaDocProperties fileProps = extractProperties(doc);
				final WordfileMetaData claMetadata = FileMetadataUtils.extractMetadata(WordfileMetaData.class,
						fileProps,claFileProperties);
				wordFile.setMetaData(claMetadata);
			}			
			
			resetDocPosition();

			// Extract the document user content, i.e. all the content that is actually visible to the user
			//----------------------------------------------------------------------------------------------
			final SigningStringBuilder sbOpenText = SigningStringBuilder.create(userContentSignatureEnabled);
			final StringBuilder sbHashedText = new StringBuilder();
			
			// Start out with all headers
			extractHeaders(doc, hfPolicy, wordFile, sbOpenText, sbHashedText);

			// Process all body elements
			for (final IBodyElement e : doc.getBodyElements()){
				appendBodyElementText(doc, e, wordFile, WordPartType.WP_body, sbOpenText, sbHashedText);
			}

			flushTableHeadTitle(wordFile);

			// Finish up with all the footers
			extractFooters(doc, hfPolicy, wordFile, sbOpenText, sbHashedText);
			
			wordFile.setOrigBody(sbOpenText.toString());
			wordFile.setBody(sbHashedText.toString());

			claFileProperties.setUserContentSignature(sbOpenText.getSignatureAsBase64());

			wordFile.finalizeIngestion();

			if (logger.isDebugEnabled()) {
				logger.debug("Finished extracting file {} pv-hist sizes={}", path,
						wordFile.getLongHistograms().getCollections().entrySet().stream()
						.map(e -> (e.getKey() + ":" + e.getValue().getCollection().size()))
						.collect(Collectors.joining(",")));
			}
			Boolean trimmed = claPropertySet.getBoolean(ClaThreadPropertiesSet.KEY_TRIMMED);
			if (trimmed) {
				logger.warn("File {} was trimmed.", path);
				logger.debug("ThreadPropertiesSet={}", claPropertySet);
			}

			return wordFile;

		} catch (final Exception e) {
			logger.info("Failed to read file {}. Error is {}.", path, e.getMessage(), e);
//			e.printStackTrace();
	    	wordFile.setErrorType(ProcessingErrorType.ERR_UNKNOWN);
			wordFile.setExceptionText(e.getMessage());
	    	return wordFile;
		} 
		//		catch (final Exception e) {
		//			throw new RuntimeException(e);
		//		}
	}

//	private WordfileMetaData extractMetadata(final String path, final ClaDocProperties fileProps) {
//		final WordfileMetaData claMetadata = new WordfileMetaData(path, fileProps);
//		try {
//			final ClaFileProperties cfp = new ClaFileProperties(path);
//			claMetadata.setOwner(cfp.getOwnerName());
//			claMetadata.setFsFileSize(cfp.getFileSize());
//			claMetadata.setFsLastModified(new Date(cfp.getModTimeMilli()));
//			claMetadata.setAclReadAllowed(cfp.getAclReadAllowed());
//			claMetadata.setAclWriteAllowed(cfp.getAclWriteAllowed());
//			claMetadata.setAclReadDenied(cfp.getAclReadDenied());
//			claMetadata.setAclWriteDenied(cfp.getAclWriteDenied());
//		} catch (final Exception e) {
//		}
//		logger.debug("MetaData = {}", claMetadata.toCsvString());
//		return claMetadata;
//	}

	private ClaDocProperties extractProperties(final XWPFDocument doc) {
		final ClaDocProperties res = new ClaDocProperties();
		
		final POIXMLProperties dp = doc.getProperties();
		final CoreProperties dcop = dp.getCoreProperties();	
		final ExtendedProperties dep = dp.getExtendedProperties();
		final CTProperties depup = dep.getUnderlyingProperties();
		/*
		final String copStr = "CoreProps: cat="+dcop.getCategory()+",content="+dcop.getContentStatus()+",contType="+dcop.getContentType()+
				",Creator="+dcop.getCreator()+",Desc="+dcop.getDescription()+",Ident="+dcop.getIdentifier()+",Keyw="+dcop.getKeywords()+
				",rev="+dcop.getRevision()+",Sub="+dcop.getSubject()+",Title="+dcop.getTitle()+
				",created="+((dcop.getCreated()!=null)?dcop.getCreated().toString():"")+
				",printed="+((dcop.getLastPrinted()!=null)?dcop.getLastPrinted().toString():"")+
				",modified="+((dcop.getModified()!=null)?dcop.getModified().toString():"");
		logger.debug(copStr);
		final org.openxmlformats.schemas.officeDocument.x2006.customProperties.CTProperties dcup = dp.getCustomProperties().getUnderlyingProperties();
		final String dcupStr = "CustProps: "+dcup.getPropertyList().stream().map(p->p.getName()).collect(Collectors.joining("|"));
		logger.debug(dcupStr);
		final String depStr = "ExtProps: App="+depup.getApplication()+",appVer="+depup.getAppVersion()+",Chars="+depup.getCharacters()+",charsSpc="+depup.getCharactersWithSpaces()+
				",company="+depup.getCompany()+",HLBase="+depup.getHyperlinkBase()+
				",Lines="+depup.getLines()+",Mgr="+depup.getManager()+",MMClips="+depup.getMMClips()+",Notes="+depup.getNotes()+",pages="+depup.getPages()+",Parag="+depup.getParagraphs()+
				",Template="+depup.getTemplate()+",totTime="+depup.getTotalTime()+",words="+depup.getWords();
		logger.debug(depStr);
		*/

		res.setCreator(spaceIfNull(dcop.getCreator()));
		res.setSubject(spaceIfNull(dcop.getSubject()));
		res.setTitle(spaceIfNull(dcop.getTitle()));
		res.setKeywords(spaceIfNull(dcop.getKeywords()));
		res.setAppNameVer(spaceIfNull(depup.getApplication())+"/"+spaceIfNull(depup.getAppVersion()));
		res.setTemplate(spaceIfNull(depup.getTemplate()));
		res.setCreated(dcop.getCreated());
		res.setModified(dcop.getModified());
		res.setCompany(spaceIfNull(depup.getCompany()));
		res.setWords(depup.getWords());
		res.setPages(depup.getPages());
		res.setChars(depup.getCharactersWithSpaces());
		res.setParagraphs(depup.getParagraphs());
		res.setLines(depup.getLines());
		final int sec = depup.getDocSecurity();
		res.setSecAppCode(sec);
		res.setSecPasswordProtected((sec & 1)!=0);
		res.setSecReadonlyRecommended((sec & 2)!=0);
		res.setSecReadonlyEnforced((sec & 4)!=0);
		res.setSecLockedForAnnotations((sec & 8)!=0);

		logger.debug("Extracted props: " + res.toString());

		return res;
	}

	private static String spaceIfNull(final String str) {
		return (str!=null)?str:"";
	}

	public int getLogNumerator() {
		return logNumerator;
	}

	public void setLogNumerator(final int logNumerator) {
		this.logNumerator = logNumerator;
	}

	private void appendBodyElementText(final XWPFDocument document, final IBodyElement e, final WordFile wordFile, 
			final WordPartType docPart, final Appendable sbOpenText, final Appendable sbHashedText ){
		appendBodyElementText(document, e, wordFile, docPart, 0, 0, 0, null, sbOpenText, sbHashedText);
	}

	private void appendBodyElementText(final XWPFDocument document, final IBodyElement element, final WordFile wordFile,
			final WordPartType docPart, final int tableLevel, final int rowNum, final int cellNum,
			final XWPFStyle parentStyle, final Appendable sbOpenText,final Appendable sbHashedText){

		if (element instanceof XWPFParagraph) {
			//processParagraphText(document, (XWPFParagraph)e, wordFile, docPart, table_lvl);
			extractParagraph(document, (XWPFParagraph) element, docPart, wordFile, tableLevel, rowNum, cellNum, parentStyle, sbOpenText, sbHashedText);
			if (docPart == WordPartType.WP_body && tableLevel == 0) {
				exsParagraphId++;
			}
		}
		else if (element instanceof XWPFTable) {
			appendTableText(document, (XWPFTable)element, wordFile, docPart, tableLevel, sbOpenText, sbHashedText);
			if (docPart == WordPartType.WP_body && tableLevel == 0) {
				exsParagraphId++;
			}
		}
		else if (element instanceof XWPFSDT) {
			processSDTRun(document, (XWPFSDT)element, docPart, wordFile, sbHashedText);
		}
	}

	private void appendTableText(final XWPFDocument document, final XWPFTable table, final WordFile wordFile,
								 final WordPartType docPart, final int tableLevel,
								 final Appendable sbOrigText,final Appendable sbText) {
		//this works recursively to pull embedded tables from tables
		final XWPFStyle ts = (table.getStyleID() != null) ? styles.getStyle(table.getStyleID()) : null;
		
		for (int rn = 0; rn< table.getNumberOfRows(); ++rn) {
			final XWPFTableRow row = table.getRow(rn);
//		for (final XWPFTableRow row : table.getRows()){
			final List<XWPFTableCell> cells = row.getTableCells();
			for (int cn = 0; cn < cells.size(); ++cn) {
				final XWPFTableCell cell = cells.get(cn);
				final List<IBodyElement> cbe = cell.getBodyElements();
				for (final IBodyElement be : cbe) {
//				cbe.forEach(e -> appendBodyElementText(document, e, wordFile, docPart, table_lvl+1, rn, cn, ts, sbOrigText,sbText)); // TODO: test
					appendBodyElementText(document, be, wordFile, docPart, tableLevel + 1, rn, cn, ts, sbOrigText,sbText);
				}
				//text.append(cell.getTextRecursively());	// TODO:
				//if (cn < cells.size()-1){
					//text.append("\t");
				//}
			}
			// Consider all tables columns as a single paragraph ID for entity extraction purposes
			if (docPart == WordPartType.WP_body && tableLevel <= 1) {
				exsParagraphId++;
			}
			//text.append('\n');
		}
	}

	private void extractFooters(final XWPFDocument document,
								final XWPFHeaderFooterPolicy hfPolicy, final WordFile wordFile,
								final Appendable sbOpenText, final Appendable sbHashedText) {

		if (hfPolicy == null) {
			return;
		}

		final XWPFFooter[] headers = new XWPFFooter[] {
				hfPolicy.getFirstPageFooter(),
				hfPolicy.getEvenPageFooter(),
				hfPolicy.getDefaultFooter()
		};

		Arrays.stream(headers)
			.filter(Objects::nonNull)
			.forEach(f -> handleHeaderFooterItems(document, f.getBodyElements(), wordFile, sbOpenText, sbHashedText));
	}

	private void extractHeaders(final XWPFDocument document,
								final XWPFHeaderFooterPolicy hfPolicy, final WordFile wordFile,
								final Appendable sbOrigText, final Appendable sbText) {
		if (hfPolicy == null) {
			return;
		}

		final XWPFHeader[] headers = new XWPFHeader[] {
				hfPolicy.getFirstPageHeader(),
				hfPolicy.getEvenPageHeader(),
				hfPolicy.getDefaultHeader()
		}; 
		Arrays.stream(headers)
			.filter(Objects::nonNull)
			.forEach(h -> handleHeaderFooterItems(document, h.getBodyElements(), wordFile, sbOrigText, sbText));
	}

	private void handleHeaderFooterItems(final XWPFDocument document, final List<IBodyElement> be, final WordFile wordFile,
										 final Appendable sbOrigText,final Appendable sbText) {
		if (be != null) {
			for (final IBodyElement e : be){
				appendBodyElementText(document, e, wordFile, WordPartType.WP_headerFooter, sbOrigText,sbText);
				exsParagraphId++;
			}
		}
	}

	private ClaStyle getClaStyle(final XWPFParagraph p, final XWPFRun run, 
			final int tableLevel, final XWPFStyle parentStyle) {
		XWPFStyle style = null;
		XWPFStyle defStyle;
		XWPFStyle linkedStyle = null;
		XWPFStyle baseStyle = null;
		XWPFStyle baseLinkedStyle = null;
		final String sid = p.getStyleID();
		String lSid;
		String bSid;
		String blSid;
		if (sid != null) { 
			style = styles.getStyle(sid);
		}
		else {
			defStyle = styles.getStyle("Normal");
			if (defStyle != null) {
				style = defStyle;
			}
		}
		if (style != null) {
			bSid = style.getBasisStyleID();
			if (bSid != null) { 
				baseStyle = styles.getStyle(bSid);
			}
			lSid = style.getLinkStyleID();
			if (lSid != null) { 
				linkedStyle = styles.getStyle(bSid);
				if (linkedStyle != null) {
					blSid = linkedStyle.getBasisStyleID();
					if (blSid != null) { 
						baseLinkedStyle = styles.getStyle(blSid);
					}
				}
			}
		}
		try {
			final String name = (style!=null)?style.getName():"";

			final CTPPr paraPPr = p.getCTP().getPPr();
			final CTPPr psPPr = (parentStyle!=null)?parentStyle.getCTStyle().getPPr():null;
			final CTPPr sPPr = (style!=null)?style.getCTStyle().getPPr():null;
			final CTPPr bsPPr = (baseStyle!=null)?baseStyle.getCTStyle().getPPr():null;
			final CTPPr lsPPr = (linkedStyle!=null)?linkedStyle.getCTStyle().getPPr():null;
			final CTPPr blsPPr = (baseLinkedStyle!=null)?baseLinkedStyle.getCTStyle().getPPr():null;

			final CTRPr runRPr = (run.getCTR()!=null)?run.getCTR().getRPr():null;
			final CTRPr psRPr = (parentStyle!=null && parentStyle.getCTStyle()!=null)?parentStyle.getCTStyle().getRPr():null;
			final CTRPr sRPr = (style!=null && style.getCTStyle()!=null)?style.getCTStyle().getRPr():null;
			final CTRPr bsRPr = (baseStyle!=null && baseStyle.getCTStyle()!=null)?baseStyle.getCTStyle().getRPr():null;
			final CTRPr lsRPr = (linkedStyle!=null && linkedStyle.getCTStyle()!=null)?linkedStyle.getCTStyle().getRPr():null;
			final CTRPr blsRPr = (baseLinkedStyle!=null && linkedStyle.getCTStyle()!=null)?baseLinkedStyle.getCTStyle().getRPr():null;

			// debug: visibility variables

			// Get the CTSectPr object that contains the information about the document section and strip (some of) the information from it.
			@SuppressWarnings("unused") final CTSectPr pSectPr = (paraPPr != null)?paraPPr.getSectPr():null;
			@SuppressWarnings("unused") final CTSectPr dSectPr = p.getDocument().getDocument().getBody().getSectPr();
			@SuppressWarnings("unused") final BodyElementType paragEt = p.getElementType();
			@SuppressWarnings("unused") final BodyType partType = p.getPartType();
			@SuppressWarnings("unused") final Class<? extends IRunBody> parentEt = run.getParent().getClass();
			@SuppressWarnings("unused") final String type = (style!=null)?style.getType().toString() + "(" + style.getType().intValue() + ")":"";	// Verify
			@SuppressWarnings("unused") final CTFonts rFonts = (runRPr!=null && runRPr.isSetRFonts())?runRPr.getRFonts():null;
			@SuppressWarnings("unused") final CTFonts sFonts = (sRPr!=null && sRPr.isSetRFonts())?sRPr.getRFonts():null;
			@SuppressWarnings("unused") final CTFonts bsFonts = (bsRPr!=null && bsRPr.isSetRFonts())?bsRPr.getRFonts():null;
			@SuppressWarnings("unused") final CTFonts lsFonts = (lsRPr!=null && lsRPr.isSetRFonts())?lsRPr.getRFonts():null;
			@SuppressWarnings("unused") final CTFonts psFonts = (psRPr!=null && psRPr.isSetRFonts())?psRPr.getRFonts():null;
			@SuppressWarnings("unused") final CTFonts dFonts = (docDefaultRPr!=null && docDefaultRPr.isSetRFonts())?docDefaultRPr.getRFonts():null;
			@SuppressWarnings("unused") final String rColor = (runRPr!=null && runRPr.isSetColor())?Integer.toHexString(rgbToInt(runRPr.getColor().getVal())):null;
			@SuppressWarnings("unused") final String sColor = (sRPr!=null && sRPr.isSetColor())?Integer.toHexString(rgbToInt(sRPr.getColor().getVal())):null;
			@SuppressWarnings("unused") final String bsColor = (bsRPr!=null && bsRPr.isSetColor())?Integer.toHexString(rgbToInt(bsRPr.getColor().getVal())):null;
			@SuppressWarnings("unused") final String lsColor = (lsRPr!=null && lsRPr.isSetColor())?Integer.toHexString(rgbToInt(lsRPr.getColor().getVal())):null;
			@SuppressWarnings("unused") final String psColor = (psRPr!=null && psRPr.isSetColor())?Integer.toHexString(rgbToInt(psRPr.getColor().getVal())):null;
			@SuppressWarnings("unused") final String dColor = (docDefaultRPr!=null && docDefaultRPr.isSetColor())?docDefaultRPr.getColor().getVal().toString():null;

			final List<CTPPr> ctPPrVector = Arrays.asList(paraPPr, psPPr, sPPr, bsPPr, lsPPr, blsPPr/*, docDefaultPPr*/);	// paragraph related style hierarchy
			final List<CTRPr> ctRPrVector = Arrays.asList(runRPr, psRPr, sRPr, bsRPr, lsRPr, blsRPr, docDefaultRPr);	// run related style hierarchy

			final int color = ctRPrVector.stream()
					.filter(c->c!=null && c.isSetColor())
					.mapToInt(c->rgbToInt(c.getColor().getVal()))
					.findFirst().orElse(
							ctPPrVector.stream()
							.filter(c->c!=null && c.isSetRPr() && c.getRPr().isSetColor())
							.mapToInt(c->rgbToInt(c.getRPr().getColor().getVal()))
							.findFirst().orElse(0));									// set default as '0'.

			// debug
			@SuppressWarnings("unused") final CTFonts fonts = ctRPrVector.stream()
					.filter(c->c!=null && c.isSetRFonts())
					.map(CTRPr::getRFonts).findFirst().orElse(null);

			final String asciiFont = ctRPrVector.stream()
					.filter(c->c!=null && c.isSetRFonts() && c.getRFonts().isSetAscii())
					.map(c->c.getRFonts().getAscii()).findFirst().orElse(null);
			final String hFont = ctRPrVector.stream()
					.filter(c->c!=null && c.isSetRFonts() && c.getRFonts().isSetHAnsi())
					.map(c->c.getRFonts().getHAnsi()).findFirst().orElse(null);
			final String eaFont = ctRPrVector.stream()
					.filter(c->c!=null && c.isSetRFonts() && c.getRFonts().isSetEastAsia())
					.map(c->c.getRFonts().getEastAsia()).findFirst().orElse(null);
			final String csFont = ctRPrVector.stream()
					.filter(c->c!=null && c.isSetRFonts() && c.getRFonts().isSetCs())
					.map(c->c.getRFonts().getCs()).findFirst().orElse(null);

			String fontFamily = (asciiFont!=null)?asciiFont:csFont;	// TODO: implement formal OpenXML logic ... http://msdn.microsoft.com/en-us/library/ff533743.aspx
			if (runRPr==null && sRPr==null && bsRPr==null && lsRPr==null && eaFont!=null && asciiFont!=null && asciiFont.equals(hFont)) {
				fontFamily = eaFont;
			}

			@SuppressWarnings("unused") final String rStyle = (runRPr!=null && runRPr.isSetRStyle())?runRPr.getRStyle().getVal():"";

			final int fontSize = ctRPrVector.stream()
					.filter(c->c!=null && c.isSetSz())
					.mapToInt(c->c.getSz().getVal().intValue())
					.findFirst().orElse(
							ctPPrVector.stream()
							.filter(c->c!=null && c.isSetRPr() && c.getRPr().isSetSz())
							.mapToInt(c->c.getRPr().getSz().getVal().intValue())
							.findFirst().orElse(20));					// hard-coded default font-size. TODO: verify

//			@SuppressWarnings("unused") final int fontSizeCs = ctRPrVector.stream()
//					.filter(c->c!=null && c.isSetSzCs())
//					.mapToInt(c->c.getSzCs().getVal().intValue())
//					.findFirst().orElse(
//							ctPPrVector.stream()
//							.filter(c->c!=null && c.isSetRPr() && c.getRPr().isSetSzCs())
//							.mapToInt(c->c.getRPr().getSzCs().getVal().intValue())
//							.findFirst().orElse(20));					// hard-coded default font-size. TODO: verify
//
//			@SuppressWarnings("unused") final CTLanguage lang = ctRPrVector.stream()
//					.filter(c->c!=null && c.isSetLang())
//					.map(c->c.getLang()).findFirst().orElse(
//							ctPPrVector.stream()
//							.filter(c->c!=null && c.isSetRPr() && c.getRPr().isSetLang())
//							.map(c->c.getRPr().getLang())
//							.findFirst().orElse(null));
//
//			@SuppressWarnings("unused") final Object bidi = ctRPrVector.stream()
//					.filter(c->c!=null && c.isSetLang() && c.getLang().isSetBidi())
//					.map(c->c.getLang().getBidi()).findFirst().orElse(null);
//
			final String pt = p.getText();

			final boolean isBold = (ctPPrVector.stream().anyMatch(c->c!=null && c.isSetRPr() && c.getRPr().isSetB()) ||
					ctRPrVector.stream().anyMatch(c->c!=null && c.isSetB()));

			final boolean isCs = (ctPPrVector.stream().anyMatch(c->c!=null && c.isSetRPr() && c.getRPr().isSetCs()) ||
					ctRPrVector.stream().anyMatch(c->c!=null && c.isSetCs()));
			if (isCs) {
				logger.debug("Found CS text: {} af={} cs={}", pt, asciiFont, csFont);
			}

			final ClaStyle claStyle = new ClaStyle(name, fontFamily, fontSize/2f, color, 
					isBold, run.getUnderline() != UnderlinePatterns.NONE, run.toString().length());
			claStyle.setBorder(styleBorderHash(p.getBorderTop()),
					styleBorderHash(p.getBorderRight()),
					styleBorderHash(p.getBorderBottom()),
					styleBorderHash(p.getBorderLeft()));
			
			int alignment = ctPPrVector.stream()
					.filter(c->c!=null && c.isSetJc())
					.mapToInt(c->c.getJc().getVal().intValue())
					.findFirst().orElse(1) - 1;		// -1 to match Word6 values
			final int o_alignment = (paraPPr!=null && paraPPr.isSetJc())?paraPPr.getJc().getVal().intValue():
				((sPPr!=null && sPPr.isSetJc())?sPPr.getJc().getVal().intValue():1) - 1;	// -1 to match Word6 values
			
			final boolean isRtl = (ctPPrVector.stream().anyMatch(c->c!=null && c.isSetRPr() && c.getRPr().isSetRtl()) ||
					ctRPrVector.stream().anyMatch(c->c!=null && c.isSetRtl()));
			final boolean o_isRtl = ((paraPPr!=null && paraPPr.isSetRPr() && paraPPr.getRPr().isSetRtl()) ||
					ctRPrVector.stream().anyMatch(c->c!=null && c.isSetRtl()));
			if (isRtl) {	// paragraph is RTL - update justification. TODO: more? borders, left/right indent swap
				if (alignment == 0) {
					alignment = 2;
				} else if (alignment == 2) {
					alignment = 0;
				}
			}
			if (alignment >=0 && alignment < justifications.length) {
				claStyle.setJustification(justifications[alignment]);
			}
			else {
				claStyle.setJustification(justifications[0]);	// Make values same as HWPF (Word6)
			}
			
			// TODO: @@@ Change all the paragraph-level stuff to use the PPr  ...
			// Get paragraph properties
			final CTInd indantation = ctPPrVector.stream()
					.filter(c->c!=null && c.isSetInd())
					.map(c->c.getInd()).findFirst().orElse(null);
			final int ind = ctPPrVector.stream()
					.filter(c->c!=null && c.isSetInd() && c.getInd().isSetFirstLine())
					.mapToInt(c->c.getInd().getFirstLine().intValue()).findFirst().orElse(
							- ctPPrVector.stream()		// negative value indicate hanging first line
							.filter(c->c!=null && c.isSetInd() && c.getInd().isSetHanging())
							.mapToInt(c->c.getInd().getHanging().intValue()).findFirst().orElse(0));
			final BigInteger numId = ctPPrVector.stream()
					.filter(c->c!=null && c.isSetNumPr() && c.getNumPr().isSetNumId())
					.map(c->c.getNumPr().getNumId().getVal()).findFirst().orElse(null);
			final BigInteger numIlvl = ctPPrVector.stream()
					.filter(c->c!=null && c.isSetNumPr() && c.getNumPr().isSetIlvl())
					.map(c->c.getNumPr().getIlvl().getVal()).findFirst().orElse(null);
			final CTLvl numLevel = getNumLevel(p.getDocument(), numId, numIlvl);
			final String numFormat = (numLevel!=null && numLevel.isSetNumFmt())?numLevel.getNumFmt().getVal().toString():null;
			
			final CTInd numIndent = (numLevel!=null && numLevel.isSetPPr() && numLevel.getPPr().isSetInd())?numLevel.getPPr().getInd():null;
			/*
			if (pnf != null || pni != null || pnl != null){
				logger.debug("Interesting style: text={} format={} lvl={} nid={} li={} ri={}", paragraph.getText(),
						(pnf!=null)?pnf:"", (pnl!=null)?pnl.intValue():-1, (pni!=null)?pni.intValue():-1, 
								(nCti!=null && nCti.isSetLeft())?nCti.getLeft():-1,
								(nCti!=null && nCti.isSetRight())?nCti.getRight():-1);
			}
			*/
			final int indLeft = ctPPrVector.stream()
					.filter(c->c!=null && c.isSetInd() && c.getInd().isSetLeft())
					.mapToInt(c->c.getInd().getLeft().intValue()).findFirst().orElse(0); 
			final int indRight = ctPPrVector.stream()
					.filter(c->c!=null && c.isSetInd() && c.getInd().isSetRight())
					.mapToInt(c->c.getInd().getRight().intValue()).findFirst().orElse(0);
			final int spacingBefore = ctPPrVector.stream()
					.filter(c->c!=null && c.isSetSpacing() && c.getSpacing().isSetBefore())
					.mapToInt(c->c.getSpacing().getBefore().intValue()).findFirst().orElse(0); 
			final int spacingAfter = ctPPrVector.stream()
					.filter(c->c!=null && c.isSetSpacing() && c.getSpacing().isSetAfter())
					.mapToInt(c->c.getSpacing().getAfter().intValue()).findFirst().orElse(0); 
			claStyle.setListItem(numLevel!=null);
			claStyle.setFirstLineIndent((short) Integer.signum(ind/20));
			claStyle.setIndentFromLeft(indLeft/1440f);
			claStyle.setIndentFromRight(indRight/1440f);
			claStyle.setSpacingBefore((short) (spacingBefore/20));
			claStyle.setSpacingAfter((short) (spacingAfter/20));

			claStyle.setTableLevel(tableLevel);

			int level = (numIlvl!=null)?numIlvl.intValue()+1:0;	// TODO: verify
			if(level == 0 && 
					(name.startsWith("heading") || name.startsWith("Heading"))) {
				// "Heading 3" or "Heading2" or "heading 4"
				int num = 1;
				try {
					num = Integer.parseInt(name.substring(name.length()-1));
				} catch(final NumberFormatException e) {}
				level =  Math.min(num, 6);
			}
			claStyle.setHeadingLevel(level);
			return claStyle;
		}
		catch (final Exception e) {
			logger.warn("Error while extracting style", e);
		}

		return null;
	}

	static private int styleBorderHash(final Borders border) {
		if (border == Borders.NIL || border == Borders.NONE) {
			return 0;
		}
		return border.getValue();
	}

	private ClaStyle extractStyle(final XWPFParagraph paragraph, final XWPFDocument document, 
			final WordPartType docPart, final WordFile wordFile, final int tableLevel, final XWPFStyle parentStyle) {

		ClaStyle claStyle = null;
		int crMaxTextLength = 0;
		final List<XWPFRun> runs = paragraph.getRuns();
		if (runs.size()==0) {
			return null;
		}

		for (final XWPFRun run : runs) {
			final String runText = getRunString(run, false);

			if (crMaxTextLength < runText.length()) {
				claStyle = getClaStyle(paragraph, run, tableLevel, parentStyle);
				crMaxTextLength = runText.length();
			}
		}

		if (claStyle==null) {
			return null;
		}

		return claStyle;
	}


	private void processParagraph(final XWPFDocument wdDoc, final XWPFParagraph p, final String pText,
								  final ClaStyle style, final WordPartType docPart, final WordFile wordFile,
								  final int tableLevel, final int rowNum, final int colNum,
								  final Appendable sbOpenText, final Appendable sbHashedText) {

		boolean gotTitle = false;
		final int pTextLenth = pText.length();

		final boolean isHeading = (pTextLenth < maxHeadingLength) &&
				(style != null) &&
				(pTextLenth >= minHeadingLength) &&
				((style.getHeadingLevel() > 0) || style.isHeading());

		if (docPart == WordPartType.WP_body) {
			if (pTextLenth > 2 * maxHeadingLength || tableLevel > 0) {
				wordFile.setBodyLongTextProcessed(true);
			}
			final List<Long> bodyNGrams = nGramTokenizerHashed.getHashlist(pText, bodyTextNgramSize);
			
			// Process paragraphs at 1st table level
			if (tableLevel == 0 && rowNum==0) {
				if (colNum==0) {
					clearTableHeadTitle(wordFile);
				}
				if (colNum < 4) {
					addTableHeadCellTitle((isHeading && !style.isListItem()), 
							pText, style.getStyleHeadingPotentialRate(), rowNum, colNum);
				}
			}
			if (isHeading && bodyNGrams != null && bodyNGrams.size() > 0) {// make sure there is at least 1 meaningful text token

				final String pTextT = pText.trim();
				final int pTextTLength = pTextT.length();
				if ((pTextTLength < maxHeadingLength) && (pTextTLength >= minHeadingLength)) {
					wordFile.histogramAddItem(PVType.PV_Headings, pTextT);
				}
				if (isWithinDocPreamble()) {
					final String pTitle = removeFieldTransformedText(pTextT);				// Remove field literals injected to the text
					final int pTitleLength = pTitle.length();
					if (((!titleExtractionIgnoreRightJustified) || style.getJustification() != ClaStyle.Justification.RIGHT_JUST) &&
							(pTitleLength < maxHeadingLength) && (pTitleLength >= minHeadingLength) &&
							(wordFile.getProposedTitlesCount() < titleExtractionMaxCount) &&
							(wordFile.getProposedTitlesCount() == 0 || !wordFile.isBodyLongTextProcessed()))	// once body long-text is seen process title only if none exist
					{
						gotTitle = true;
						// Set priority based on first character - letter=highest
						final String priority = Character.isAlphabetic(pTitle.codePointAt(0))?"P4":"P5";

						wordFile.addProposedTitles(priority, style.getStyleHeadingPotentialRate(),
								pTitle,
								prevParagraphWasTitle);
					}
				}
			}

			updateDocPosition(wdDoc, p, (bodyNGrams==null || bodyNGrams.size()==0)?0:bodyNGrams.size()+bodyTextNgramSize/2);	// Estimation for number of words in this paragraph
			
			if (isWithinIngestionPart()) {
				if (bodyNGrams != null) {
					wordFile.histogramAddLongItems(PVType.PV_BodyNgrams, bodyNGrams);

					wordFile.histogramAddLongItems(PVType.PV_SubBodyNgrams, bodyNGrams);
					final ClaHistogramCollection<Long> sbNgHist = wordFile.getLongHistogram(PVType.PV_SubBodyNgrams);
					// Mark all but every fifth item in the SubBodyNgrams to filter them out during PV_Entries save.
					final Iterator<Long> ngIter = bodyNGrams.iterator();
					for (int i=0;ngIter.hasNext();++i) {
						final Long ng = ngIter.next();
						if ((i % bodyTextNgramSize) != 0) {
							sbNgHist.markItem(ng);	// Marked items will not be matched
						}
					}
				}

				if (pTextLenth >= minHeadingLength ) {	// process style stats
					final int leftMar = getLeftMargine(p);
					final int rightMar = getRightMargine(p);
					//				final String styleSig = style.getSimpleSignature();
					//				wordFile.addStyleSignature(styleSig, absoluteStyleSig, !style.isInTable());
					final String absoluteStyleSig = style.getAbsoluteSignatureNoFont(leftMar/1440f,rightMar/1440f);
					wordFile.addDelayedStyleSignature(style.getFontName(), absoluteStyleSig, pTextLenth, !style.isInTable());
					logger.debug("AbsStyleDeb: f({}) {} {} text={}",style.getFontName(), absoluteStyleSig, (!style.isInTable())?1:0, pText);
					//				logger.debug("StyleDebX: {} Text={}", style.getSimpleSignature(), pText.substring(0, Integer.min(50, pText.length())));
				}

				final String pTextT = pText.trim();
				String pLabel = getParagraphLabel(pTextT); // based on text analysis
				if (pLabel == null)
				{
					pLabel = getParagraphLabel(p, tableLevel); // based on run style analysis
				}
				if (pLabel != null) {
					wordFile.histogramAddItem(PVType.PV_ParagraphLabels, pLabel);
					logger.trace("pLabel: {} out of {}",pLabel, pText);
				}
				// Extraction logic to be performed only on the first 1-2 pages
				if (isWithinDocPreamble()) {
					if (pLabel != null) {
						wordFile.histogramAddItem(PVType.PV_LetterHeadLabels, pLabel);

						// A section with letter-head is a low candidate for doc title (prefer the one with largest text within range)
						final String pTitle = removeFieldTransformedText(pTextT);				// Remove field literals injected to the text
						final int pTitleLength = pTitle.length();
						if ((pTitleLength < maxHeadingLength) && (pTitleLength >= minHeadingLength) && 
								((style.isInTable() && style.getTableLevel()==1 && rowNum < tableRowForTitleExtraction) || !style.isInTable())) {
							gotTitle = true;
							//						wordFile.addProposedTitles("P8", 100-pTitleLength, pTitle, prevParagraphWasTitle);
							wordFile.addProposedTitles("L8", style.getStyleHeadingPotentialRate(), pTitle, prevParagraphWasTitle);
						}
					}

					wordFile.histogramAddLongItems(PVType.PV_DocstartNgrams, bodyNGrams);
				}
			}
		}
		else if (docPart == WordPartType.WP_headerFooter) {
			final List<Long> nGrams = nGramTokenizerHashed.getHashlist(pText, headingsTextNgramSize);
			wordFile.histogramAddLongItems(PVType.PV_HeaderNgrams, nGrams);
			
			if (pTextLenth >= minHeadingLength) {	// process style stats
				final String styleSig = style.getSimpleSignature();
				wordFile.addHeaderStyleSignature(styleSig);
			}
			
			if (isHeading) {
				final String pTitle = removeFieldTransformedText(pText.trim());				// Remove field literals injected to the text
				final int pTitleLength = pTitle.length();
				if (((!titleExtractionIgnoreRightJustified) || style.getJustification() != ClaStyle.Justification.RIGHT_JUST) &&
						(pTitleLength < maxHeadingLength) && (pTitleLength >= minHeadingLength) &&
						(wordFile.getProposedTitlesCount() < titleExtractionMaxCount))
				{
					gotTitle = true;
					wordFile.addProposedTitles((tableLevel > 0) ? "H5" : "H6", style.getStyleHeadingPotentialRate(),
							pTitle,
							prevParagraphWasTitle);
				}
			}
		}
		// Process tokens for entity-extraction mechanism
		if (isTokenMatcherActive && !pText.isEmpty()) {
			append(sbOpenText, sbHashedText, pText);
		}

		if (docPart != WordPartType.WP_body || isWithinIngestionPart()) {
			// Collect font-size statistics
			if (isGetFontSizeStats) {
				p.getRuns().forEach(cr -> {
					final int size = cr.getFontSize();
					final int lenght = cr.toString().length();
					wordFile.processFontStats(cr.getFontFamily(), size, lenght);
				});
			}
		}
		prevParagraphWasTitle = gotTitle;
	}
	
	private void addTableHeadCellTitle(boolean isHeading, String text, int rate, int row, int col) {
		text = removeFieldTransformedText(text);
		if (text.length() < 3) {
			return;
		}
		if (logger.isTraceEnabled()) {
			logger.trace("addTableHeadCellTitle: added <{}> ({}/{}) at {},{} to <{}>", text, rate, (isHeading?"hdn":"notH"), row, col, tableHeadTitle);
		}
		if ((!isHeading) && (!tableHeadTitle.isEmpty()) &&
				(tableHeadTitleLastRow > row || tableHeadTitleLastCol > col) && !text.isEmpty()) {
			// If not all are 'headings', then get only the first 'heading' paragraph.
			tableHeadTitleFreeze = true;
			logger.trace("addTableHeadCellTitle: not heading. Stop adding cells");
			return;
		}
		if (isHeading && !tableHeadTitleFreeze) {
			logger.trace("addTableHeadCellTitle: added");
			tableHeadTitle += (tableHeadTitle.isEmpty()?"":", ") + text;
			if (rate > tableHeadTitleMaxRate) {
				tableHeadTitleMaxRate = rate;
			}
			tableHeadTitleLastRow = row;
			tableHeadTitleLastCol = col;
		}
	}

	private void clearTableHeadTitle(WordFile wordFile) {
		flushTableHeadTitle(wordFile);
		tableHeadTitle = "";
		tableHeadTitleMaxRate = 0;
		tableHeadTitleFreeze = false;
		tableHeadTitleLastRow = (-1);
		tableHeadTitleLastCol = (-1);
	}

	private void flushTableHeadTitle(WordFile wordFile) {
		if (tableHeadTitle!=null && !tableHeadTitle.isEmpty()) {
			logger.debug("flushTableHeadTitle: {} ({})", tableHeadTitle, tableHeadTitleMaxRate);
			wordFile.addProposedTitles("T6", tableHeadTitleMaxRate, tableHeadTitle, false);
		}
	}

	/*
	 * Get paragraph / section margines - overcome an issue where sectPr.getPgMar() is not found
	 */
	private CTSectPr lastPSection = null;
	private Node docMarNode = null;
	private Node lastMarNode = null;
	private int leftMargin = -1;
	private int rightMargin = -1;
	
	private int getLeftMargine(final XWPFParagraph p) {
		calcMargines(p);
		return leftMargin;
	}
	
	private int getRightMargine(final XWPFParagraph p) {
		calcMargines(p);
		return rightMargin;
	}
	
	private void calcMargines(final XWPFParagraph p) {
		final CTSectPr pSection = (p.getCTP()!=null && p.getCTP().getPPr()!=null && p.getCTP().getPPr().getSectPr()!=null)?
				p.getCTP().getPPr().getSectPr():null;
		if (lastPSection!=null && lastPSection.equals(pSection)) {
			return;
		}
		lastPSection = pSection;
		Node marNode = getPgMarNode(pSection);
		if (marNode==null) {
			if (docMarNode == null) {
				docMarNode = getPgMarNode(p.getDocument().getDocument().getBody().getSectPr());
			}
			marNode = docMarNode;
		}
		if (lastMarNode!=null && lastMarNode.equals(marNode)) {
			return;
		}
		lastMarNode = marNode;
		if (marNode != null) {
			final NamedNodeMap attrib = marNode.getAttributes();
			for (int j=0;j<attrib.getLength();++j) {
				if (attrib.item(j).getNodeName().equals("w:left")) {
					leftMargin = Integer.valueOf(attrib.item(j).getNodeValue());
				} else if (attrib.item(j).getNodeName().equals("w:right")) {
					rightMargin = Integer.valueOf(attrib.item(j).getNodeValue());
				}
			}
		}

	}
	
	private Node getPgMarNode(final CTSectPr sectPr) {
		if (sectPr!=null && sectPr.getDomNode()!=null && sectPr.getDomNode().getChildNodes()!=null) {
			final NodeList childNodes = sectPr.getDomNode().getChildNodes();
			for (int i=0;i<childNodes.getLength();++i) {
				if (childNodes.item(i).getNodeName().equals("w:pgMar")) {
					return childNodes.item(i);
				}
			}
		}
		return null;
	}

	private void append(final Appendable sbOrigText, final Appendable sbText, final String pText) {
		try {
			if (patternSearchActive) {
				sbOrigText.append(pText).append("\n");
			}
			sbText.append(nGramTokenizerHashed.getHash(pText, hashedTokensConsumer)).append(" ");
		}
		catch(IOException ioe){
			logger.error("Failed to append text {}.", pText);
		}
	}

	/**
	 * @return true if the extraction is within the body first 1-2 pages (estimation)
	 */
	private boolean isWithinDocPreamble() {
		return ((bodyParagraphsCounter < wordDocStartParagraphsLimit) && 
				(bodyWordsCounter < wordDocStartWordsLimit));
	}

	private boolean isWithinIngestionPart() {
		// If paragraphs/words ingestion limit set then return false when the limit is passed
		return ((wordIngestionParagraphsLimit <= 0 || bodyParagraphsCounter < wordIngestionParagraphsLimit) && 
				(wordIngestionWordsLimit <= 0 || bodyWordsCounter < wordIngestionWordsLimit));
	}

	private void resetDocPosition() {
		bodyParagraphsCounter = 0;
		bodyWordsCounter = 0;
	}

	private void updateDocPosition(final XWPFDocument wdDoc, final XWPFParagraph p, final int paragraphWordCount) {
		if (paragraphWordCount > 0) {
			bodyParagraphsCounter++;
		}
		// rough estimation to number or words processed.
		bodyWordsCounter += paragraphWordCount;
	}

	private void handlePictureCharacterRun(final XWPFParagraph p, final XWPFRun run, final WordFile wordFile) {
		try {
			// If we have any pictures, output them
			for(final XWPFPicture picture : run.getEmbeddedPictures()) {
				if(p.getDocument() != null) {
					final XWPFPictureData data = picture.getPictureData();
					if(data != null) {
						if (logger.isDebugEnabled()) {
							logger.debug("Picture: filename={} desc={} type={} ext={} shapeProp={}", 
									data.getFileName(), picture.getDescription(),
									data.getPictureType(), data.suggestFileExtension(), 
									picture.getCTPicture().getSpPr().toString());
						}
						final String text = "P"+ NGramTokenizerHashed.MD5Long(data.getData());
						wordFile.histogramAddItem(PVType.PV_Pictures, text);
					}
				}
			}
		} catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
			logger.error("Failed handlePictureCharacterRun in file {}. Error is {}",wordFile.getFileName(),e);
//			e.printStackTrace();
		}
	}

	/**
	 * @param run			region of text with a common set of properties
	 * @param textParts		text parts list
	 * @param index
	 * @param iruns
	 * @return number of runs to skip (in case of multiple runs of a field.
	 */
	private int handleCharacterRun(final XWPFRun run, final List<String> textParts, final int index, final List<IRunElement> iruns) {

		// TODO: check if start of field code runs-sequence and act accordingly
		if (getRunType(run) == Runtype.RP_FieldStart) {
			int i = index + 1;
			for (;i<iruns.size();++i) {
				if (!(iruns.get(i) instanceof XWPFRun)) {
					continue;
				}
				final XWPFRun r = (XWPFRun)iruns.get(i);
				final Runtype ft = getRunType(r);
				if (ft == Runtype.RP_FieldDef) {
					final String fld = getRunString(r).trim();
//					try {
						final String text = getFieldTransformedText(fld);
						textParts.add(text);
//					} catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
//						e.printStackTrace();
//					}
				}
				if (ft == Runtype.RP_FieldEnd) {
					break;
				}
			}
			return i-index;
		}
		
		// Skip trailing newlines
		final String runText = getRunString(run);
		if(runText.trim().length() > 0) {
			textParts.add(runText);
		}
		return 0;
	}

	static private final Pattern fieldTransformationPattern = Pattern.compile("F_[A-F0-9]{1,16}[^\\}]*\\}");
	private String removeFieldTransformedText(final String text) {
		Matcher fieldMatcher = fieldTransformationPattern.matcher(text);
		if (fieldMatcher.find()) {
			if (logger.isTraceEnabled()) {
				logger.trace("removeFieldTransformedText: removing fieldTrans from <{}>", text);
			}
			return fieldMatcher.replaceAll("");
		}
		else {
			return text;
		}
	}
	// Replace the field code string with a single identifiable token. Allow the entire pattern to be removed using removeFieldTransformedText().
	private String getFieldTransformedText(final String fieldText) {
//		return "F"+Math.abs(nGramTokenizerHashed.MD5Long(fieldText)) /* for debug*/ +" {"+ fieldText +"}";
		return String.format("F_%X {%s}", nGramTokenizerHashed.MD5Long(fieldText), /* for debug*/ fieldText);
	}	

	private void handleFieldcodeRun(final XWPFRun run, final List<String> textParts) {
		// Skip trailing newlines
		final String runText = getRunString(run);
		if(runText.equals("\r")) {
			return;
		}

		// Convert entire field string into a single unique token (for later nGram processing).
//		try {
			final String text = getFieldTransformedText(runText);
			textParts.add(text);
//		} catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
//			e.printStackTrace();
//		}
	}

	private String getParagraphLabel(final String pText) {
		String[] split = pLabelIndiactionPattern.split(pText.trim()+" ", 2);
		if (split.length == 1) {
			return null;
		}

		final Matcher matcher = pLabelPattern.matcher(split[0].trim());
		if (!matcher.find()) {
			return null;
		}
		final String res = matcher.group().trim();
		if (res.length() == 0) {
			return null;
		}
		split = pLabelSplitPattern.split(res, paragraphLabelMaxTokens);
		if (split.length == paragraphLabelMaxTokens) {
			return null;
		}

		return res;
	}

	 private String getParagraphLabel(final XWPFParagraph p, final int tableLevel) {

		final List<XWPFRun> runs = p.getRuns();
		final XWPFRun run1 = runs.get(0);
		if (runs.size() == 1 && (!run1.isBold()) && (run1.getUnderline() == UnderlinePatterns.NONE) && (tableLevel == 0)) {
			return null;
		}
		String text = getRunString(run1);
		for (int i=1;i<runs.size();++i) {
			final XWPFRun run2 = runs.get(i);
			if (run1.getFontSize() > run2.getFontSize() || 
					(run1.isBold() && !run2.isBold()) ||
					(run1.getUnderline() != UnderlinePatterns.NONE && run2.getUnderline() == UnderlinePatterns.NONE)) {
				break;
			}
			else {
				text = text + getRunString(run2);
			}
		}
		return getParagraphLabel(text + paragraphLabelDelimiters[1]); 
	}

    private void processSDTRun(final XWPFDocument document, final XWPFSDT sdt, final WordPartType docPart,
							   final WordFile wordFile, final Appendable sbHashedText){
    	// TODO:
         logger.debug("SDT item: text={} tag={} title={}", sdt.getContent().getText(), sdt.getTag(), sdt.getTitle());
    }

    private void processSDTRun(final XWPFDocument document, final XWPFSDT sdt, final List <String> textParts){
         logger.debug("SDT item: text={} tag={} title={}", sdt.getContent().getText(), sdt.getTag(), sdt.getTitle());
         textParts.add(sdt.getContent().getText());
    }

	private void extractParagraph(final XWPFDocument document, final XWPFParagraph paragraph, 
			final WordPartType docPart, final WordFile wordFile, final int tableLevel, final int rowNum, final int cellNum, 
			final XWPFStyle parentStyle,final Appendable sbOpenText, final Appendable sbHashedText ) {
		
       // If this paragraph is actually a whole new section, then
       //  it could have its own headers and footers
       // Check and handle if so
       XWPFHeaderFooterPolicy headerFooterPolicy = null;
       if (paragraph.getCTP().getPPr() != null) {
    	   final CTSectPr ctSectPr = paragraph.getCTP().getPPr().getSectPr();
    	   if(ctSectPr != null) {
    		   try {
    			   headerFooterPolicy =
    					   new XWPFHeaderFooterPolicy(document, ctSectPr);
    		   } catch (final Exception e) {
    				logger.error("Failed extractParagraph in file {}. Error is {}",wordFile.getFileName(),e);
//    			   e.printStackTrace();
    		   }
    		   extractHeaders(document, headerFooterPolicy, wordFile, sbOpenText, sbHashedText);
    	   }
       }
       
       // Is this a paragraph, or a heading?
       @SuppressWarnings("unused") String styleClass = null;
       boolean isHeading = false;
       if(paragraph.getStyleID() != null) {
          final XWPFStyle style = styles.getStyle(paragraph.getStyleID());

          if (style != null && style.getName() != null) {
             final TagAndStyle tas = buildParagraphTagAndStyle(style.getName(), paragraph.getPartType() == BodyType.TABLECELL);
             styleClass = tas.getStyleClass();
             isHeading = tas.isHeading();
          }
       }
       
       // Output placeholder for any embedded docs:
       for(final XWPFRun run : paragraph.getRuns()) {
          final XmlCursor c = run.getCTR().newCursor();
          c.selectPath("./*");
          while (c.toNextSelection()) {
             final XmlObject o = c.getObject();
             if (o instanceof CTObject) {
                final XmlCursor c2 = o.newCursor();
                c2.selectPath("./*");
                while (c2.toNextSelection()) {
                   final XmlObject o2 = c2.getObject();

                   final XmlObject embedAtt = o2.selectAttribute(new QName("Type"));
                   if (embedAtt != null && embedAtt.getDomNode().getNodeValue().equals("Embed")) {
                      // Type is "Embed"
                      final XmlObject relIDAtt = o2.selectAttribute(new QName("http://schemas.openxmlformats.org/officeDocument/2006/relationships", "id"));
                      if (relIDAtt != null) {
                         final String relID = relIDAtt.getDomNode().getNodeValue();
                         logger.debug("Embed: relId={}", relID);
                      }
                   }
                }
                c2.dispose();
             }
          }
          c.dispose();
       }
       
       // Attach bookmarks for the paragraph
       // for (final CTBookmark bookmark : paragraph.getCTP().getBookmarkStartList()) {
       //     logger.debug("Bookmark: name={}", bookmark.getName());
       // }
       
       final List <String> textParts = new LinkedList<>();
       
       // Do the iruns
       final List<IRunElement> iruns = paragraph.getIRuns();
       int j;
       for (j=0;j<iruns.size();++j) {
    	  final IRunElement run = iruns.get(j);
          if (run instanceof XWPFSDT){
        	  processSDTRun(document, (XWPFSDT)run, textParts);
          } else {
        	  j += handleCharacterRun((XWPFRun) run, textParts, j, iruns);
        	  handlePictureCharacterRun(paragraph, (XWPFRun) run, wordFile);
          }
       }
       final String runsText = textParts.stream().collect(Collectors.joining());

       final ClaStyle claStyle = extractStyle(paragraph, document, docPart, wordFile, tableLevel, parentStyle);
       if (claStyle != null && runsText.length() > 0) {
    	   claStyle.setText(runsText);
    	   claStyle.setHeadingLevel(isHeading ? 1 : 0);
           processParagraph(document, paragraph, runsText, claStyle, docPart, wordFile, tableLevel, rowNum, cellNum, sbOpenText, sbHashedText);
       }
       else {
    	   if (runsText.length() > 0) {
    		   logger.warn("Extracted a null style. Text={} #runs={}", runsText, paragraph.getRuns().size());
    	   }
       }
       
       // Now do any comments for the paragraph
       final XWPFCommentsDecorator comments = new XWPFCommentsDecorator(paragraph, null);
       final String commentText = comments.getCommentText();
       if(commentText != null && commentText.length() > 0) {
           logger.debug("Comment: text={}", commentText);
       }

       final String footnameText = paragraph.getFootnoteText();
       if(footnameText != null && footnameText.length() > 0) {
           logger.debug("Footnote: text={}", footnameText);
       }

       // Also extract any paragraphs embedded in text boxes:
       for (final XmlObject embeddedParagraph : paragraph.getCTP().selectPath("declare namespace w='http://schemas.openxmlformats.org/wordprocessingml/2006/main' declare namespace wps='http://schemas.microsoft.com/office/word/2010/wordprocessingShape' .//*/wps:txbx/w:txbxContent/w:p")) {
    	   try {
    		   extractParagraph(document,
    				   new XWPFParagraph(CTP.Factory.parse(embeddedParagraph.xmlText()), paragraph.getBody()), 
    				   docPart, wordFile, tableLevel, rowNum, cellNum, null, sbOpenText, sbHashedText);
    	   } catch (final XmlException e) {
    		   logger.warn("CTP Factory parse error: {}", e.getMessage());
    	   }
       }

       if (headerFooterPolicy != null) {
    	   extractFooters(document, headerFooterPolicy, wordFile, sbOpenText, sbHashedText);
       }
    }

    private static final Map<String,TagAndStyle> fixedParagraphStyles = new HashMap<String,TagAndStyle>();
    private static final TagAndStyle defaultParagraphStyle = new TagAndStyle("p", null);
    static {
        fixedParagraphStyles.put("Default", defaultParagraphStyle);
        fixedParagraphStyles.put("Normal", defaultParagraphStyle);
        fixedParagraphStyles.put("heading", new TagAndStyle("h1", null));
        fixedParagraphStyles.put("Heading", new TagAndStyle("h1", null));
        fixedParagraphStyles.put("Title", new TagAndStyle("h1", "title"));
        fixedParagraphStyles.put("Subtitle", new TagAndStyle("h2", "subtitle"));
        fixedParagraphStyles.put("HTML Preformatted", new TagAndStyle("pre", null));
    }
    
    /**
     * Given a style name, return what tag should be used, and
     *  what style should be applied to it. 
     */
    public static TagAndStyle buildParagraphTagAndStyle(final String styleName, final boolean isTable) {
       final TagAndStyle tagAndStyle = fixedParagraphStyles.get(styleName);
       if (tagAndStyle != null) {
           return tagAndStyle;
       }

       if (styleName.equals("Table Contents") && isTable) {
           return defaultParagraphStyle;
       }

       String tag = "p";
       String styleClass = null;

       if(styleName.startsWith("heading") || styleName.startsWith("Heading")) {
           // "Heading 3" or "Heading2" or "heading 4"
           int num = 1;
           try {
               num = Integer.parseInt(
                                      styleName.substring(styleName.length()-1)
                                      );
           } catch(final NumberFormatException e) {}
           // Turn it into a H1 - H6 (H7+ isn't valid!)
           tag = "h" + Math.min(num, 6);
       } else {
           styleClass = styleName.replace(' ', '_');
           styleClass = styleClass.substring(0,1).toLowerCase() +
               styleClass.substring(1);
       }

       return new TagAndStyle(tag,styleClass);
    }
    
    public static class TagAndStyle {
       private final String tag;
       private final String styleClass;
       public TagAndStyle(final String tag, final String styleClass) {
         this.tag = tag;
         this.styleClass = styleClass;
       }
       public String getTag() {
         return tag;
       }
       public String getStyleClass() {
         return styleClass;
       }
       public boolean isHeading() {
          return tag.length()==2 && tag.startsWith("h");
       }
    }

    /**
     * Returns the string version of the text, with tabs and
     *  carriage returns in place of their xml equivalents.
     */
    private static String getRunString(final XWPFRun r) {
    	return getRunString(r, true);
    }

    private static String getRunString(final XWPFRun r, final boolean addHyperlinks) {
        final StringBuilder text = new StringBuilder();
        final CTR run = r.getCTR();
        if (run == null) {
			return "";
		}

		if (r instanceof XWPFHyperlinkRun) {
			final XWPFHyperlinkRun linkRun = (XWPFHyperlinkRun)r;
			final XWPFHyperlink link = linkRun.getHyperlink(r.getDocument());
			if (addHyperlinks) {
				if(link != null && link.getURL() != null) {
					text.append(link.getURL());
				}
				else if(linkRun.getAnchor() != null && linkRun.getAnchor().length() > 0) {
					text.append("#").append(linkRun.getAnchor());
				}
			}
			else {
				text.append("HYPERLINK ");
			}
		}
		else {
	        
	        // Grab the text and tabs of the text run
	        // Do so in a way that preserves the ordering
	        final XmlCursor c = run.newCursor();
	        c.selectPath("./*");
	        while (c.toNextSelection()) {
	            final XmlObject o = c.getObject();
	            if (o instanceof CTText) {
	                //final String tagName = o.getDomNode().getNodeName();
	                // Field Codes (w:instrText, defined in spec sec. 17.16.23)
	                //  come up as instances of CTText, but we don't want them
	                //  in the normal text output
	                //if (!"w:instrText".equals(tagName)) {
	                text.append(((CTText) o).getStringValue());
	                //}
	            }
	
	            if (o instanceof CTPTab) {
	                text.append("\t");
	            }
	            if (o instanceof CTBr) {
	                text.append("\n");
	            }
	            if (o instanceof CTEmpty) {
	                // Some inline text elements get returned not as
	                //  themselves, but as CTEmpty, owing to some odd
	                //  definitions around line 5642 of the XSDs
	                // This bit works around it, and replicates the above
	                //  rules for that case
	                final String tagName = o.getDomNode().getNodeName();
	                if ("w:tab".equals(tagName)) {
	                    text.append("\t");
	                }
	                if ("w:br".equals(tagName)) {
	                    text.append("\n");
	                }
	                if ("w:cr".equals(tagName)) {
	                    text.append("\n");
	                }
	            }
	            if (o instanceof CTFtnEdnRef) {
	                final CTFtnEdnRef ftn = (CTFtnEdnRef)o;
	                final String footnoteRef = ftn.getDomNode().getLocalName().equals("footnoteReference") ?
	                    "[footnoteRef:" + ftn.getId().intValue() + "]" : "[endnoteRef:" + ftn.getId().intValue() + "]";
	                text.append(footnoteRef);
	            }            
	            if (o instanceof CTPicture) {
	            	final CTPicture p = (CTPicture)o;
	            	logger.trace("Picture found: {}", p.toString());
	            }
	            if (o instanceof CTFldChar) {
	            	final CTFldChar f = (CTFldChar)o;
	            	final Enum fct = f.getFldCharType();
	            	logger.trace("CTFldChar found: {} type={}", f.toString(), fct.toString());
	            }
	        }
	
	        c.dispose();
		}
        // Any picture text?
        final String pictureText = r.getPictureText();
        if(pictureText != null && pictureText.length() > 0) {
        	logger.trace("Picture text: {}", pictureText);
            //text.append("\n").append(pictureText);
        }

        return text.toString();
    }

    private static Runtype getRunType(final XWPFRun r) {

    	final CTR run = r.getCTR();
    	if (run == null) {
			return Runtype.RT_Other;
		}

    	final XmlCursor c = run.newCursor();
    	try {
    		// Grab the text and tabs of the text run
    		// Do so in a way that preserves the ordering
    		c.selectPath("./*");
    		while (c.toNextSelection()) {
    			final XmlObject o = c.getObject();
    			if (o instanceof CTText) {
    				final String tagName = o.getDomNode().getNodeName();
    				// Field Codes (w:instrText, defined in spec sec. 17.16.23)
    				//  come up as instances of CTText, but we don't want them
    				//  in the normal text output
    				if ("w:instrText".equals(tagName)) {
    					return Runtype.RP_FieldDef;
    				}
    				else {
    					return Runtype.RT_Text;
    				}
    			}
    			if (o instanceof CTFldChar) {
    				final CTFldChar f = (CTFldChar)o;
    				final STFldCharType.Enum fct = f.getFldCharType();
    				switch (fct.intValue()) {
    				case STFldCharType.INT_BEGIN:
    					return Runtype.RP_FieldStart;
    				case STFldCharType.INT_SEPARATE:
    					return Runtype.RP_FieldSeperator;
    				case STFldCharType.INT_END:
    					return Runtype.RP_FieldEnd;
    				}
    				logger.trace("CTFldChar found: {} type={}", f.toString(), fct.toString());
    			}
    		}
    	}
    	finally {
    		if (c != null) {
				c.dispose();
			}
    	}
    	return Runtype.RT_Other;
    }
    
    private static int rgbToInt(final Object o) {
    	if (o instanceof byte[]) {
    		final byte[] b = (byte[])o;
	    	int ret = 0;
	    	for (int i=b.length-1;i>=0;--i) {
	    		ret = (b[i] & 0xff) | (ret << 8);
	    	}
	    	return ret;
    	}
    	else if (o instanceof String) {
			final String s = (String)o;
			if (s.toLowerCase().equals("auto")) {
				return 0;
			}
			logger.warn("Unexpected color name: {}", s);
		}
		else {
			logger.warn("Unexpected color object: class={} sty={}", o.getClass().toString(), o.toString());
		}
    	return 0;
    }

    private CTInd getNumIndent(final XWPFParagraph p) {
        final BigInteger numID = p.getNumID();
        final XWPFNumbering numbering = p.getDocument().getNumbering();
        if(numID != null && numbering != null) {
            final XWPFNum num = numbering.getNum(numID);
            if(num != null) {
                final BigInteger ilvl = p.getNumIlvl();
                final BigInteger abstractNumId = num.getCTNum().getAbstractNumId().getVal();
                final CTAbstractNum anum = numbering.getAbstractNum(abstractNumId).getAbstractNum();
                CTLvl level = null;
                for(int i = 0; i < anum.sizeOfLvlArray(); i++) {
                    final CTLvl lvl = anum.getLvlArray(i);
                    if(lvl.getIlvl().equals(ilvl)) {
                        level = lvl;
                        break;
                    }
                }
                if(level != null && level.getPPr() != null) {
					return level.getPPr().getInd();
				}
            }
        }
        return null;
    }

    /**
     * Returns numbering format for this paragraph, eg bullet or
     *  lowerLetter.
     * Returns null if this paragraph does not have numeric style.
     */
    public CTLvl getNumLevel(final XWPFDocument doc, final BigInteger numId, final BigInteger ilvl) {
        final XWPFNumbering numbering = doc.getNumbering();
        if(numId != null && numbering != null) {
            final XWPFNum num = numbering.getNum(numId);
            if(num != null) {
                final BigInteger abstractNumId = num.getCTNum().getAbstractNumId().getVal();
                final CTAbstractNum anum = numbering.getAbstractNum(abstractNumId).getAbstractNum();
                CTLvl level = null;
                for(int i = 0; i < anum.sizeOfLvlArray(); i++) {
                    final CTLvl lvl = anum.getLvlArray(i);
                    if(lvl.getIlvl().equals(ilvl)) {
                        level = lvl;
                        break;
                    }
                }
                if(level != null) {
					return level;
				}
            }
        }
        return null;
    }

}
