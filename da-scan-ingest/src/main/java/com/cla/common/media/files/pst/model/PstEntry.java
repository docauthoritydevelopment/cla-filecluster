package com.cla.common.media.files.pst.model;


import com.cla.common.domain.pst.PstPath;
import com.cla.common.media.files.pst.visitor.PstEntryVisitor;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;

/**
 * Created By Itai Marko
 */
public interface PstEntry {

    long getPstEntryId();

    PstPath getPstPath();

    ClaFilePropertiesDto getPstFileProps();

    <T> T accept(PstEntryVisitor<T> visitor);

    <T> void acceptAndDescend(PstEntryVisitor<T> visitor);
}
