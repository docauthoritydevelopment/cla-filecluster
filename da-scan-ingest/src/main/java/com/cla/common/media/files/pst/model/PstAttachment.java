package com.cla.common.media.files.pst.model;

import com.cla.common.domain.pst.PstPath;
import com.cla.common.media.files.pst.visitor.PstEntryVisitor;
import com.pff.PSTAttachment;
import com.pff.PSTException;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created By Itai Marko
 */
public class PstAttachment extends PstEntryBase {

    private static final int DEFAULT_BUFFER_SIZE = 8176; // According to java-libpst docs, this is the best buffer size

    private final PstEmailMessage containingEmailMsg;
    private final int bufferSize;
    private final PSTAttachment libpstAttachment;
    private final String filename;

    PstAttachment(PSTAttachment attachment, PstFile pstFile, PstPath pstPath, PstEmailMessage containingEmailMsg) {
        super(pstFile, pstPath);
        this.libpstAttachment = attachment;
        String attachmentFilename = libpstAttachment.getLongFilename().trim();
        if (attachmentFilename.isEmpty()) {
            attachmentFilename = libpstAttachment.getFilename().trim();
        }
        this.filename = attachmentFilename;
        this.bufferSize = DEFAULT_BUFFER_SIZE;
        this.containingEmailMsg = containingEmailMsg;
    }

    @Override
    public <T> T accept(PstEntryVisitor<T> visitor) {
        return visitor.visitAttachment(this);
    }

    @Override
    <T> void descend(PstEntryVisitor<T> visitor) {
        throw new UnsupportedOperationException("Unexpected descend call to PstAttachment: " + getPstPath().toString());
    }

    @Override
    public long getPstEntryId() {
        PstPath pstPath = getPstPath();
        return pstPath.getEntryId().orElseThrow(() ->
                new IllegalStateException("PstAttachment built with non unique PstPath: " + pstPath.toString()));
    }

    public int getFileSize() {
        try {
            return libpstAttachment.getFilesize();
        } catch (PSTException | IOException e) {
            throw new RuntimeException("Error occurred while trying to get the attachment file size of " + getPstPath().toString(), e);
        }
    }

    public InputStream getAttachmentInputStream() {
        try {
            InputStream actualInputStream = libpstAttachment.getFileInputStream();
            BufferedInputStream bufferedInputStream = new BufferedInputStream(actualInputStream, bufferSize);
            return getPstFile().registeredInputStream(bufferedInputStream);
        } catch (IOException | PSTException e) {
            throw new RuntimeException("Error occurred while trying to get the attachment file InputStream of " + getPstPath().toString(), e);
        }
    }

    public String getFilename() {
        return filename;
    }

    String getMimeTag() {
        return libpstAttachment.getMimeTag().trim();
    }

    String getContentId() {
        return libpstAttachment.getContentId().trim();
    }

    public PstEmailMessage getContainingEmailMsg() {
        return containingEmailMsg;
    }

}
