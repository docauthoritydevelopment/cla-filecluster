package com.cla.common.media.files.xml;


import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.apache.poi.openxml4j.opc.PackagePart;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 *
 * Created by vladi on 2/16/2017.
 *
 * POI OOXML package part that uses XML aware pre-processor to limit size of underlying XML before it is loaded to XMLBeans as a DOM
 */
public class CurtailedPackagePart extends PackagePart {

    private PackagePart originalPart;
    private int sizeThreshold = 0;
    private int totalObjectsLimit;

    public CurtailedPackagePart(PackagePart originalPart, int sizeThreshold) throws InvalidFormatException {
        this(originalPart, sizeThreshold, 0);
    }

    public CurtailedPackagePart(PackagePart originalPart, int sizeThreshold, int totalObjectsLimit) throws InvalidFormatException {
        this(originalPart);
        this.sizeThreshold = sizeThreshold;
        this.totalObjectsLimit = totalObjectsLimit;
    }


    public CurtailedPackagePart(PackagePart originalPart) throws InvalidFormatException {
        super(originalPart.getPackage(), originalPart.getPartName(), originalPart.getContentType());
        this.originalPart = originalPart;
    }

//    private XMLAwareCurtailingInputStream pipe = null;
    private XMLAwareCurtailingPipe pipe = null;

    @Override
    protected InputStream getInputStreamImpl() throws IOException {
        InputStream originalInput = originalPart.getInputStream();

        InputStream inStream = originalInput;
        if (!(originalPart instanceof CurtailedPackagePart)) {
            int dataSize = 1;
            if (sizeThreshold > 0) {
                dataSize = originalInput.available();
            }
            if (dataSize > sizeThreshold) {
                //        pipe = new XMLAwareCurtailingInputStream(originalPart.getInputStream());
                pipe = new XMLAwareCurtailingPipe(originalPart.getInputStream());
                pipe.setTotalObjectsLimit(totalObjectsLimit);
                inStream = pipe.getCurtailedInputStream();
            }
        }
        return inStream;
    }

    @Override
    protected OutputStream getOutputStreamImpl() {
        return originalPart.getOutputStream();
    }

    @Override
    public boolean save(OutputStream zos) throws OpenXML4JException {
        return originalPart.save(zos);
    }

    @Override
    public boolean load(InputStream ios) throws InvalidFormatException {
        return originalPart.load((ios));
    }

    @Override
    public void close() {
        originalPart.close();
        if (pipe != null) {
            pipe.shutDown();
        }
    }

    @Override
    public void flush() {
        originalPart.flush();
    }

    public void setTotalObjectsLimit(int totalObjectsLimit) {
        this.totalObjectsLimit = totalObjectsLimit;
    }
}
