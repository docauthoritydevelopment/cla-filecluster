package com.cla.common.media.files.excel.streaming;

import com.cla.common.domain.dto.excel.ExcelSheetDimensions;
import com.cla.common.domain.dto.mediaproc.excel.ExcelCell;
import com.cla.common.domain.dto.mediaproc.excel.ExcelWorkbook;
import com.cla.common.domain.dto.mediaproc.excel.ExcellCellStyle;
import com.cla.common.media.files.excel.ExcelFileUtils;
import com.cla.common.media.files.excel.ExcelFormulaUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import org.apache.poi.hssf.eventusermodel.HSSFListener;
import org.apache.poi.hssf.model.HSSFFormulaParser;
import org.apache.poi.hssf.model.InternalWorkbook;
import org.apache.poi.hssf.record.*;
import org.apache.poi.hssf.record.aggregates.FormulaRecordAggregate;
import org.apache.poi.hssf.record.aggregates.PageSettingsBlock;
import org.apache.poi.hssf.record.aggregates.SharedValueManager;
import org.apache.poi.hssf.record.common.UnicodeString;
import org.apache.poi.hssf.record.pivottable.ViewDefinitionRecord;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 *
 * Created by uri on 18/10/2015.
 */
public class ReadExcelContentEventListener implements HSSFListener {

    private Logger logger = LoggerFactory.getLogger(ReadExcelContentEventListener.class);

    private final Map<String, String> sheetFullNames = new LinkedHashMap<>();

    private final Map<Integer, String> sheetIndexToName= new HashMap<>();

    private int numberOfCells = 0;

    private int sheetIndex = -1; //We increase the sheet index on each sheet encountered.
    private String sheetIndexAsString = "";
    private int sheetNameIndex = -1; //We encounter the sheet names first, and then the sheets themselves
//    private BoundSheetRecord currentSheet;
    private ExcelWorkbook excelWorkbook;

    private Map<Integer, ExcelSheetDimensions> excelSheetDimensionsMap;

    private ExcelSheetDimensions currentSheetDimensions;

    private ExcelFileUtils.ExcelIssuesCounters excelIssuesCounters = new ExcelFileUtils.ExcelIssuesCounters();
    private int currentRowsCount = -1;
    private int currentCellsInRow = 0;

    private int lastRow = -1;

    private int lastRowInRowBlock = -1;
    private int lastColumnInRowBlock = -1;
    private List<Record> currentRowBlock = new ArrayList<>();

    private InternalWorkbook internalWorkbook;
    private HSSFWorkbook workbook;
    private final boolean isTokenMatcherActive;

    private HSSFEvaluationWorkbook evaluationWorkbookWrapper;

    private SharedValueManager sharedValueManager;


    private List<Record> shFrmRecords = Lists.newArrayList();
    private List<CellReference> firstCellRefs = Lists.newArrayList();
    private List<Record> arrayRecords = Lists.newArrayList();
    private List<Record> tableRecords = Lists.newArrayList();
    private Record lastRecord = null;

    private boolean firstException = true;
    private int currentRowArrayPosition = 0;
    private int expectedEOFCounter = 0;

//    Long totalHandleCellRecordNanos = 0L;
    private Long totalAddExcelCellNanos = 0L;
    private long totalAddExcelCellContentNanos = 0L;

    public ReadExcelContentEventListener(ExcelWorkbook excelWorkbook,
                                         Map<Integer, ExcelSheetDimensions> excelSheetDimensionsMap,
                                         InternalWorkbook internalWorkbook,
                                         HSSFWorkbook workbook ,
                                         boolean isTokenMatcherActive) {

        this.excelWorkbook = excelWorkbook;
        this.excelSheetDimensionsMap = excelSheetDimensionsMap;
        this.internalWorkbook = internalWorkbook;
        this.workbook  = workbook ;
        this.isTokenMatcherActive = isTokenMatcherActive;
        evaluationWorkbookWrapper =
                HSSFEvaluationWorkbook.create((HSSFWorkbook) workbook);
        sharedValueManager = SharedValueManager.createEmpty();


    }

    public Map<String, String> getSheetFullNames() {
        return sheetFullNames;
    }

    public int getNumberOfSheets() {
        return sheetFullNames.keySet().size();
    }

    public int getNumberOfCells() {
        return numberOfCells;
    }

    @Override
    public void processRecord(Record record) {
        switch (record.getSid()) {
            // the BOFRecord can represent either the beginning of a sheet or the workbook
            case BOFRecord.sid:
                BOFRecord bof = (BOFRecord) record;
                handleBofRecord(bof);
                break;
            case BoundSheetRecord.sid:
                handleSheetRecord((BoundSheetRecord) record);
                break;
            case RowRecord.sid:
                break;
            case SSTRecord.sid:
                break;
            case NumberRecord.sid:
            case LabelSSTRecord.sid:
            case FormulaRecord.sid:
            case BoolErrRecord.sid:
            case StringRecord.sid:
                addCellRecordToRowBlock(record);
                break;
            case RKRecord.sid:
                logger.warn("Got a RKRecord cell - unhandled cell type");
                break;
            case SharedFormulaRecord.sid:
                handleSharedFormulaRecord((SharedFormulaRecord) record);
                break;
            case ArrayRecord.sid:
                arrayRecords.add(record);
                break;
            case TableRecord.sid:
                tableRecords.add(record);
                break;
            case EOFRecord.sid:
                handleEndOfFileRecord((EOFRecord)record);
                break;
            default:
//                logger.debug("Got a record that is: " + record.getClass().getName());
                break;
        }
        lastRecord= record;
        if (currentRowBlock.size() > 0 && isEndOfRowBlock(record.getSid())) {
            handleEndOfRowBlock();
        }
    }

    private void addCellRecordToRowBlock(Record record) {
        if (expectedEOFCounter > 1) {
            return; // We are in an internal chart - not parsed for now.
        }
        if (record instanceof CellRecord) {
            CellRecord cellRecord = (CellRecord) record;
//            logger.debug("Add Cell Record at {} {} to rowBlock",cellRecord.getRow(),cellRecord.getColumn());
            int row = cellRecord.getRow();
            int column = cellRecord.getColumn();
            if (lastRowInRowBlock > row || (lastRowInRowBlock == row && lastColumnInRowBlock > column)) {
                logger.debug("We encountered a row {} smaller then what we encountered already {} or a repeat of a row",row,lastRowInRowBlock);
                currentRowBlock = new ArrayList<>();
            }
            lastRowInRowBlock = row;
            lastColumnInRowBlock = column;
        }
        currentRowBlock.add(record);
    }

    private void handleEndOfRowBlock() {
        logger.trace("End of row block encountered");
        SharedFormulaRecord[] sharedFormulaRecords = new SharedFormulaRecord[shFrmRecords.size()];
        CellReference[] firstCells = new CellReference[firstCellRefs.size()];
        ArrayRecord[] arrayRecs = new ArrayRecord[arrayRecords.size()];
        TableRecord[] tableRecs = new TableRecord[tableRecords.size()];
        shFrmRecords.toArray(sharedFormulaRecords);
        firstCellRefs.toArray(firstCells);
        arrayRecords.toArray(arrayRecs);
        tableRecords.toArray(tableRecs);

        sharedValueManager = SharedValueManager.create(sharedFormulaRecords, firstCells, arrayRecs, tableRecs);

        //Reset for the next row block
        shFrmRecords = Lists.newArrayList();
        firstCellRefs = Lists.newArrayList();
        arrayRecords = Lists.newArrayList();
        tableRecords = Lists.newArrayList();
        //TODO - find a way to prevent this.
        if (currentRowBlock.size() > 15000) {
            logger.debug("Process all ({}) collected cells in row block", currentRowBlock.size());
        }
        currentRowArrayPosition = 0;
        String sheetName = sheetIndexToName.get(sheetIndex);
        for (Record record: currentRowBlock) {
            if (record instanceof CellRecord) {
                handleCellRecord((CellRecord)record,sheetName);
            }
            currentRowArrayPosition++;
        }
        if (currentRowBlock.size() > 15000) {
            logger.debug("Processed all ({}) collected cells in row block", currentRowBlock.size());
        }
        currentRowBlock = new ArrayList<>();
        lastRowInRowBlock = -1;
        lastColumnInRowBlock = -1;
    }

    private void handleSharedFormulaRecord(SharedFormulaRecord record) {
//        logger.debug("Handle shared formula record " + record.toString());
        shFrmRecords.add(record);
        FormulaRecord fr = (FormulaRecord) lastRecord;
        firstCellRefs.add(new CellReference(fr.getRow(), fr.getColumn()));

    }


    private void handleCellRecord(CellRecord record,String sheetName) {
//        Long start = System.nanoTime();
//        logger.debug("Cell found with class " + record.getClass()
//                + " at row " + record.getRow() + " and column " + record.getColumn());

        int nonBlankRows = currentSheetDimensions.getNonBlankRows();
        Integer nonBlankCellsCount = currentSheetDimensions.getNonBlankCellCount().get(record.getRow());
        if (nonBlankCellsCount == null) {
            nonBlankCellsCount = 0;
        }

        ExcelCell cellValue = null;

        //TODO - move row calculation here...
        boolean ingestCellContentOnly = ExcelFileUtils.shouldIngestCellContentOnly(currentRowsCount, nonBlankRows);

        try {
            cellValue = parseExcelCell(record,ingestCellContentOnly);
        } catch (Exception e) {
            excelIssuesCounters.addIngestionError();
            logger.trace(e.getMessage() + ". Skipping cell: [" + record.getColumn() + "," + record.getRow() + "] sheet:" + sheetName + " file:" + excelWorkbook.getFileName());
            if (firstException) {
                logger.debug(e.getMessage() + ". Skipping cell: [" + record.getColumn() + "," + record.getRow() + "] sheet:" + sheetName + " file:" + excelWorkbook.getFileName());
                logger.debug(e.getMessage(),e);
                firstException = false;
            }
//            Long duration = System.nanoTime() - start;
//            totalHandleCellRecordNanos += duration;
            return;
        }
        if (cellValue == null) {
            excelIssuesCounters.addIngestionError();
//            Long duration = System.nanoTime() - start;
//            totalHandleCellRecordNanos += duration;
            return;
        }

        if (record.getRow() > lastRow) {
            currentCellsInRow = 0;
            lastRow = record.getRow();
            if (nonBlankCellsCount > 0) {
                currentRowsCount++;
            }
        }
        String cellRefStr = calculateCellReferenceString(sheetName,record.getRow(),record);

//        String sheetIndexAsString = String.valueOf(sheetIndex);

        if (ingestCellContentOnly) {
            long start = System.nanoTime();
            excelWorkbook.addExcelCellContentOnly(sheetIndexAsString,sheetName,cellRefStr,cellValue);
            totalAddExcelCellContentNanos += System.nanoTime() - start;
        }
        else {
            boolean detailedAnalysis = ExcelFileUtils.isDetailedAnalysis(excelIssuesCounters, excelWorkbook, nonBlankRows, currentRowsCount, nonBlankCellsCount, currentCellsInRow);
            long start = System.nanoTime();
            excelWorkbook.addExcelCell(sheetIndexAsString,
                    record.getColumn(), record.getRow(),
                    cellValue,
                    sheetName,
                    detailedAnalysis,
                    cellRefStr,
                    isTokenMatcherActive);
            totalAddExcelCellNanos += System.nanoTime() - start;
        }
        if (cellValue.getType() != Cell.CELL_TYPE_BLANK &&
                cellValue.getType() != Cell.CELL_TYPE_ERROR) {
            numberOfCells++; // increment non-empty cells counter
            currentCellsInRow++;
        }
//        Long duration = System.nanoTime() - start;
//        totalHandleCellRecordNanos += duration;

//        if (cellValue.getType() != Cell.CELL_TYPE_BLANK &&
//                cellValue.getType() != Cell.CELL_TYPE_ERROR) {
//        }

    }


//
//    private void handleFormulaCell(FormulaRecord record) {
//        FormulaRecord formulaRecord = record;
//        logger.debug("Got a formula record" + formulaRecord.getColumn());
//        for (Ptg ptg : formulaRecord.getParsedExpression()) {
////                        logger.trace("PtgClass: "+ptg.getPtgClass()+" formula string:"+ptg.toFormulaString());
//        }
//    }
//
//    private void handleLabelCell(LabelSSTRecord record) {
//        LabelSSTRecord lrec = record;
//        logger.debug("String cell found with value "
//                + sstrec.getString(lrec.getSSTIndex()));
//    }

    private ExcelCell parseExcelCell(CellRecord cellRecord, boolean ingestCellContentOnly) {
        ExcelCell cellValue = getCellValue(cellRecord, sheetIndex,ingestCellContentOnly);
        if (cellValue.isIngestionError()) {
            excelIssuesCounters.addIngestionError();
        }
        if (ingestCellContentOnly) {
            //Skip cell style
            return cellValue;
        }
        try {
            final ExcellCellStyle ecs = getCellStyle(cellRecord);
            cellValue.setCellStyle(ecs);
        } catch (final Exception ex) {
            excelIssuesCounters.addIngestionError();
            //logger.debug(ex.getMessage() + " cell: [" + cell.getColumnIndex() + "," + cell.getRowIndex() + "] sheet:" + cell.getSheet().getSheetName() + " file:" + nameOrMime);
        }
        return cellValue;
    }

    private ExcellCellStyle getCellStyle(CellRecord cellRecord) {
        HSSFCellStyle style = workbook.getCellStyleAt(cellRecord.getXFIndex());
        final Font font = workbook.getFontAt(style.getFontIndex());

        final ExcellCellStyle ecs = new ExcellCellStyle(font.getFontHeightInPoints(),
                style.getIndex()==0,font.getBoldweight()>Font.BOLDWEIGHT_NORMAL,
                font.getUnderline() > 0,
                1, 0,
                font.getColor(), font.getColor()==Font.COLOR_NORMAL,
                style.getDataFormatString(),
                ExcelFileUtils.getPatternData(style),
                ExcelFileUtils.getBorderData(style),
                (byte)style.getAlignment());

        if (cellRecord.getSid() == LabelSSTRecord.sid) {
            UnicodeString unicodeString = getUnicodeString((LabelSSTRecord) cellRecord);
            //ecs.setNumOfRuns(rtf.numFormattingRuns());
            ecs.setSizeOfFirstRun(unicodeString.getFormatRunCount() > 1 ? getIndexOfFormattingRun(unicodeString, 1) : unicodeString.getCharCount());
        }

        return ecs;
    }

    private UnicodeString getUnicodeString(LabelSSTRecord cellRecord) {
        LabelSSTRecord labelSSTRecord = cellRecord;
        UnicodeString unicodeString =  internalWorkbook.getSSTString(labelSSTRecord.getSSTIndex());
        return unicodeString;
    }

    /**
     * The index within the string to which the specified formatting run applies.
     * @param index     the index of the formatting run
     * @return  the index within the string.
     */
    public static int getIndexOfFormattingRun(UnicodeString _string, int index)
    {
        UnicodeString.FormatRun r = _string.getFormatRun(index);
        return r.getCharacterPos();
    }


    private ExcelCell getCellValue(CellRecord cellRecord, int sheetIndex, boolean ingestCellContentOnly) {
        ExcelCell excelCell = new ExcelCell();
        int cellValType = ExcelFileUtils.determineType(cellRecord);
        StringRecord cachedStringRecord = null;

        if (cellValType == Cell.CELL_TYPE_FORMULA){
            if (currentRowBlock.size() > currentRowArrayPosition + 2 && currentRowBlock.get(currentRowArrayPosition+1) instanceof StringRecord) {
//                logger.debug("String record found for formula");
                cachedStringRecord = ((StringRecord) currentRowBlock.get(currentRowArrayPosition+1));
            }
            FormulaRecord formulaRecord = (FormulaRecord) cellRecord;
            FormulaRecordAggregate formulaRecordAggregate = new FormulaRecordAggregate(formulaRecord,cachedStringRecord,sharedValueManager);
            cellValType = formulaRecordAggregate.getFormulaRecord().getCachedResultType();
            if (!ingestCellContentOnly) {
                //Parse formula
                String cellFormula = HSSFFormulaParser.toFormulaString(workbook, formulaRecordAggregate.getFormulaTokens());
                excelCell.setFormula(cellFormula);
                if (evaluationWorkbookWrapper!=null) {
                    try {
                        excelCell.setFormulaIngestion(formulaIngestion(formulaRecordAggregate, evaluationWorkbookWrapper, sheetIndex));
                    }catch(final Exception ex){
                        String sheetName = sheetIndexToName.get(sheetIndex);
                        logger.warn(ex.getMessage() + " cell: " + cellRecord.toString() + " sheet: " + sheetName);
                        excelCell.setFormulaIngestion(cellFormula);
                        excelCell.setIngestionError(true);
                    }
                }
                else {
                    excelCell.setFormulaIngestion(cellFormula);
                }
            }
        }
        excelCell.setType(cellValType);
        switch (cellValType) {
            case Cell.CELL_TYPE_BLANK:
//			excelCell.setBlank(true);	 -- implicitly set by type.
                break;
            case Cell.CELL_TYPE_BOOLEAN:
                boolean booleanValue = ((BoolErrRecord) cellRecord).getBooleanValue();
                excelCell.setBooleanValue(booleanValue);
                break;
            case Cell.CELL_TYPE_ERROR:
//			excelCell.setError(true);	-- implicitly set by type.
                break;
            case Cell.CELL_TYPE_NUMERIC:

                double value = ExcelFileUtils.getNumericCellValue(cellValType,cellRecord);
                excelCell.setNumericValue(value);
                ExtendedFormatRecord rec = workbook.getInternalWorkbook().getExFormatAt(cellRecord.getXFIndex());
                String format = getCellFormatForNumericValue(rec);

                if (!Strings.isNullOrEmpty(format)) {
                    if (DateUtil.isADateFormat(rec.getFormatIndex(), format)) {
                        excelCell.setDate(true);
                    }
                    DataFormatter df = new DataFormatter();
                    String res = df.formatRawCellContents(value, rec.getFormatIndex(), format);
                    excelCell.setStringValue(res);
                }

                break;
            case Cell.CELL_TYPE_STRING:
                getStringCellValue(cellRecord, excelCell, cachedStringRecord);
                break;
            default:
                throw new RuntimeException("Not supported type : " + cellValType);
        }
        return excelCell;
    }

    private String getCellFormatForNumericValue(ExtendedFormatRecord rec) {
        String format = null;
        if (rec != null) {
            format = BuiltinFormats.getBuiltinFormat(rec.getFormatIndex());
            if (Strings.isNullOrEmpty(format)) {
                List<FormatRecord> recs = workbook.getInternalWorkbook().getFormats();
                if (recs != null) {
                    for (FormatRecord r : recs) {
                        if (r.getIndexCode() == rec.getFormatIndex()) {
                            format = r.getFormatString();
                            break;
                        }
                    }
                }
            }
        }
        return format;
    }

    private void getStringCellValue(CellRecord cellRecord, ExcelCell excelCell, StringRecord cachedStringRecord) {
        if (cellRecord.getSid() == FormulaRecord.sid) {
            String strVal = cachedStringRecord != null? cachedStringRecord.getString() : "";
            HSSFRichTextString hssfRichTextString = new HSSFRichTextString(strVal == null ? "" : strVal);
            excelCell.setStringValue(hssfRichTextString.getString());
        }
        else {
            UnicodeString unicodeString = getUnicodeString((LabelSSTRecord) cellRecord);
            excelCell.setStringValue(unicodeString.getString());
        }
    }

    private String formulaIngestion(FormulaRecordAggregate formulaRecordAggregate, HSSFEvaluationWorkbook evaluationWorkbookWrapper, int sheetIndex) {
        String cellFormula = HSSFFormulaParser.toFormulaString(workbook, formulaRecordAggregate.getFormulaTokens());
        return ExcelFormulaUtils.formulaIngestion(evaluationWorkbookWrapper, sheetIndex, cellFormula, formulaRecordAggregate, formulaRecordAggregate.getColumn(), formulaRecordAggregate.getRow());
    }

    private void handleSheetRecord(BoundSheetRecord bsr) {
        sheetNameIndex++;
        logger.trace("New sheet {} named: {}", sheetNameIndex, bsr.getSheetname());
        sheetFullNames.put(bsr.getSheetname(), String.valueOf(sheetNameIndex));
        sheetIndexToName.put(sheetNameIndex,bsr.getSheetname());
        workbook.createSheet(bsr.getSheetname());
    }

    /**
     * A sheet should be bounded by an EOF record
     * @param record
     */
    private void handleEndOfFileRecord(EOFRecord record) {
        if (expectedEOFCounter >0) {
            expectedEOFCounter--;
        }
    }

    private void handleBofRecord(BOFRecord bof) {
        if (bof.getType() == bof.TYPE_WORKBOOK) {
            logger.trace("Encountered workbook");
            // assigned to the class level member
        } else if (bof.getType() == bof.TYPE_WORKSHEET) {
            expectedEOFCounter = 1;
            currentRowsCount = -1;
            lastRow = -1;
            sheetIndex++;
            sheetIndexAsString = String.valueOf(sheetIndex);
            logger.trace("Encountered sheet reference on index {}",sheetIndex);
            currentSheetDimensions = excelSheetDimensionsMap.get(sheetIndex);
            if (currentSheetDimensions == null) {
                throw new RuntimeException("Got a sheet without dimensions on index "+sheetIndex);
            }
        }
        else if (bof.getType() == BOFRecord.TYPE_CHART) {
            //Not typical sheets, but sheets non the less
            if (expectedEOFCounter >0) {
                logger.debug("Found internal chart - this is not a sheet");
            }
            else {
                currentRowsCount = -1;
                lastRow = -1;
                sheetIndex++;
                sheetIndexAsString = String.valueOf(sheetIndex);
                logger.debug("Encountered chart sheet reference on index {}",sheetIndex);
                currentSheetDimensions = excelSheetDimensionsMap.get(sheetIndex);
                if (currentSheetDimensions == null) {
                    throw new RuntimeException("Got a chart/macro sheet without dimensions on index "+sheetIndex);
                }
            }
            expectedEOFCounter++;
        }
        else if (bof.getType() == BOFRecord.TYPE_EXCEL_4_MACRO) {
            //Not typical sheets, but sheets non the less
//            currentRowsCount = -1;
//            lastRow = -1;
//            sheetIndex++;
//            logger.debug("Encountered macro sheet reference on index {}",sheetIndex);
//            currentSheetDimensions = excelSheetDimensionsMap.get(sheetIndex);
//            if (currentSheetDimensions == null) {
//                throw new RuntimeException("Got a chart/macro sheet without dimensions on index "+sheetIndex);
//            }
        }
    }

    private String calculateCellReferenceString(String sheetName, int rowNumber, CellRecord cell) {
//        long start = System.nanoTime();
        String cellRefStr = null;
        try {
            final CellReference celRef = new CellReference(cell.getRow(),cell.getColumn(),false,false);
            cellRefStr = sheetName + ":" + celRef.formatAsString();
        } catch (final Exception e) {}
        if (cellRefStr == null) {
            cellRefStr = sheetName+":"+cell.getColumn()+"/"+rowNumber;
        }
        return cellRefStr;
    }

    public ExcelFileUtils.ExcelIssuesCounters getExcelIssuesCounters() {
        return excelIssuesCounters;
    }

    //WriteAccessRecord - username
    //WindowOneRecord - first visible tab, tab width ratio
    //CountryRecord
    //NameRecord - many
    //FontRecord - many font records
    //FormatRecord - many. formatting string
    //StyleRecord - many with names

    //TableStylesRecord
    //ExtendedFormatRecord
    //PasswordRecord
    //InterfaceHdrRecord - useless?
    //MMSRecord - useless?
    //InterfaceEndRecord - useless?
    //CodePageRecord - useless?
    //DSFRecord - useless?
    //TabIdRecord - useless?
    //FnGroupCountRecord - useless?
    //WindowProtectRecord
    //ProtectRecord
    //ProtectionRev4Record
    //PasswordRev4Record
    //BackupRecord
    //HideObjRecord
    //DateWindowRecord
    //PrecisionRecord
    //RefreshAllRecord
    //BookBoolRecord
    //UseSelFSRecord
    //ExtSSTRecord - with subRecords...
    //EOFRecord
    //IndexRecord
    //CalcModeRecord
    //CalcCountRecord

    /*
 * Taken from POI: org.apache.poi.ss.formula: FormulaRenderer.toFormulaString()
 *
 * Replace cell references in formula with relative index references (relative to cell beseCol,baseRow)
 * Expect to get a formula string which is more tolerant to occasional row/column insertion/deletion.
 */

    /**
     * @return <code>true</code> if the specified record ID terminates a sequence of Row block records
     * It is assumed that at least one row or cell value record has been found prior to the current
     * record
     */
    public static boolean isEndOfRowBlock(int sid) {
        switch(sid) {
            case ViewDefinitionRecord.sid:
                // should have been prefixed with DrawingRecord (0x00EC), but bug 46280 seems to allow this
            case DrawingRecord.sid:
            case DrawingSelectionRecord.sid:
            case ObjRecord.sid:
            case TextObjectRecord.sid:
            case ColumnInfoRecord.sid: // See Bugzilla 53984
            case GutsRecord.sid:   // see Bugzilla 50426
            case WindowOneRecord.sid:
                // should really be part of workbook stream, but some apps seem to put this before WINDOW2
            case WindowTwoRecord.sid:
                return true;

            case DVALRecord.sid:
                return true;
        }
        return PageSettingsBlock.isComponentRecord(sid);
    }

//    public Long getTotalHandelCellRecordMilli() {
//        return totalHandleCellRecordNanos / 1000000;
//    }

    public Long getTotalAddExcelCellMillis() {
        return totalAddExcelCellNanos / 1000000;
    }

    public Long getTotalAddExcelCellContentMillis() {
        return totalAddExcelCellContentNanos / 1000000;
    }
}
