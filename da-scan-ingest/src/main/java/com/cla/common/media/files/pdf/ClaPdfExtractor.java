package com.cla.common.media.files.pdf;

import com.cla.common.domain.dto.mediaproc.word.WordFile;
import com.cla.common.media.files.Extractor;
import com.cla.common.utils.tokens.HashedTokensConsumer;
import com.cla.common.utils.tokens.NGramTokenizerHashed;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.io.InputStream;


public abstract class ClaPdfExtractor extends Extractor<WordFile> {

	private static final Logger logger = LoggerFactory.getLogger(ClaPdfExtractor.class);
	private int logNumerator = 1;	// used for debug logging

	
	@Autowired
	private NGramTokenizerHashed nGramTokenizerHashed;
	
	@PostConstruct
	private void init(){
		WordFile.setNGramTokenizer(nGramTokenizerHashed);
	}
	
	public WordFile extract(final InputStream is, final String path, final HashedTokensConsumer hashedTokensConsumer, final boolean extractMetadata, ClaFilePropertiesDto claFileProperties) {
		if (WordFile.getNGramTokenizer() == null) {
			logger.warn("NGramTokenizer not set. Using default.");
			WordFile.setNGramTokenizer(new NGramTokenizerHashed());
		}
		WordFile wordFile;

		if (path.toLowerCase().endsWith("pdf")) {
			final ClaNativePdfExtractor claNativePdfExtractor = createClaPdfExtractor();
			if (patternSearchActiveSet) {
				claNativePdfExtractor.setPatternSearchActive(patternSearchActive);
			}
			claNativePdfExtractor.setHashedTokensConsumer(hashedTokensConsumer);
			claNativePdfExtractor.setLogNumerator(logNumerator);

			wordFile = claNativePdfExtractor.extract(is,path,extractMetadata, claFileProperties);
			logNumerator = claNativePdfExtractor.getLogNumerator();
		}
		else {
			logger.error("Unexpected doc suffix: {}", path);
			wordFile = null;
		}
		if(wordFile!=null) {
			wordFile.setHashedTokensConsumer(hashedTokensConsumer);
		}
		return wordFile;
	}
	
	public WordFile extract(final InputStream is, final String path, final boolean extractMetadata, ClaFilePropertiesDto claFileProperties){
		return extract(is, path,null,extractMetadata, claFileProperties);
	}

	public int getLogNumerator() {
		return logNumerator;
	}

	public void setLogNumerator(final int logNumerator) {
		this.logNumerator = logNumerator;
	}

	protected abstract ClaNativePdfExtractor createClaPdfExtractor();

}
