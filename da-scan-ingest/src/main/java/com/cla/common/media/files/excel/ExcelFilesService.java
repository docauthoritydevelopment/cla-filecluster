package com.cla.common.media.files.excel;

import com.cla.common.domain.dto.mediaproc.excel.ExcelWorkbook;
import com.cla.common.utils.tokens.DictionaryHashedTokensConsumerImpl;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;

import java.io.FileNotFoundException;
import java.io.InputStream;

public interface ExcelFilesService {



    ExcelWorkbook ingestWorkbook(String p,
                                 final InputStream is,
                                 final boolean extractMetadata,
                                 final ClaFilePropertiesDto claFileProperties);

	int getLogNumerator();

	void setLogNumerator(int logNumerator);


	boolean isPatternSearchActive();
	void setPatternSearchActive(final boolean patternSearchActive);

    ExcelWorkbook ingestWorkbook(String fileName, DictionaryHashedTokensConsumerImpl hashedTokensConsumer,
								 InputStream is, boolean extractMetadata) throws FileNotFoundException;
}
