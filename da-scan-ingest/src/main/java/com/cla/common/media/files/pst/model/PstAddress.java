package com.cla.common.media.files.pst.model;

/**
 * Created By Itai Marko
 */
public class PstAddress {

    private final String name;
    private final String emailAddress;


    PstAddress(String name, String emailAddress) {
        this.name = name;
        this.emailAddress = emailAddress;
    }

    public String getName() {
        return name;
    }

    public String getEmailAddress() {
        return emailAddress;
    }
}
