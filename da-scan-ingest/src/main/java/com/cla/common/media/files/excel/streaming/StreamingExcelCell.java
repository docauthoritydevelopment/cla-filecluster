package com.cla.common.media.files.excel.streaming;

import com.cla.common.exceptions.NotSupportedException;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.ss.SpreadsheetVersion;
import org.apache.poi.ss.formula.*;
import org.apache.poi.ss.formula.ptg.Ptg;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellAddress;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFEvaluationWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by uri on 20/10/2015.
 */
public class StreamingExcelCell implements Cell {

    private int columnIndex;
    private int rowIndex;

    private Object contents;
    private Object rawContents;
    private String numericFormat;
    private Short numericFormatIndex;
    private String type;
    private Row row;
    private Workbook workbook;

    private RichTextString richTextString;

    private static final String FALSE_AS_STRING = "0";
    private static final String TRUE_AS_STRING  = "1";


    CellStyle style;

    String formulaString;
    private String formulaType;
    private SharedFormulaHolder sharedFormulaHolder;

    StreamingExcelSheet sheet;

    public StreamingExcelCell(int columnIndex, int rowIndex, Workbook workbook,StreamingExcelSheet sheet) {
        this.columnIndex = columnIndex;
        this.rowIndex = rowIndex;
        this.workbook = workbook;
        this.sheet = sheet;
    }

    public Object getContents() {
        return contents;
    }

    public void setContents(Object contents) {
        this.contents = contents;
    }

    public Object getRawContents() {
        return rawContents;
    }

    public void setRawContents(Object rawContents) {
        this.rawContents = rawContents;
    }

    public String getNumericFormat() {
        return numericFormat;
    }

    public void setNumericFormat(String numericFormat) {
        this.numericFormat = numericFormat;
    }

    public Short getNumericFormatIndex() {
        return numericFormatIndex;
    }

    public void setNumericFormatIndex(Short numericFormatIndex) {
        this.numericFormatIndex = numericFormatIndex;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setRow(Row row) {
        this.row = row;
    }

  /* Supported */

    /**
     * Returns column index of this cell
     *
     * @return zero-based column index of a column in a sheet.
     */
    @Override
    public int getColumnIndex() {
        return columnIndex;
    }

    /**
     * Returns row index of a row in the sheet that contains this cell
     *
     * @return zero-based row index of a row in the sheet that contains this cell
     */
    @Override
    public int getRowIndex() {
        return rowIndex;
    }

    /**
     * Returns the Row this cell belongs to. Note that keeping references to cell
     * rows around after the iterator window has passed <b>will</b> preserve them.
     *
     * @return the Row that owns this cell
     */
    @Override
    public Row getRow() {
        return row;
    }

    /**
     * Return the cell type. Note that only the numeric, string, and blank types are
     * currently supported.
     *
     * @return the cell type
     * @throws UnsupportedOperationException Thrown if the type is not one supported by the streamer.
     *                                       It may be possible to still read the value as a supported type
     *                                       via {@code getStringCellValue()}, {@code getNumericCellValue},
     *                                       or {@code getDateCellValue()}
     * @see Cell#CELL_TYPE_BLANK
     * @see Cell#CELL_TYPE_NUMERIC
     * @see Cell#CELL_TYPE_STRING
     */
    @Override
    public int getCellType() {
        if (formulaString != null) {
            return CELL_TYPE_FORMULA;
        }
        return getBaseCellType(true);
    }

    @Override
    public CellType getCellTypeEnum() {
        if (formulaString != null) {
            return CellType.FORMULA;
        }
        return getBaseCellTypeEnum(true);
    }

    private CellType getBaseCellTypeEnum(boolean blankCells) {
        if(contents == null || type == null) {
            return CellType.BLANK;
        } else if("n".equals(type)) {
            if (blankCells && contents instanceof String && contents.toString().length() == 0) {
                return CellType.BLANK;
            }
            return CellType.NUMERIC;
        } else if("s".equals(type) || "inlineStr".equals(type)) {
            return CellType.STRING;
        } else if("str".equals(type)) {
            return CellType.STRING;
//            return CELL_TYPE_FORMULA;
        } else if("b".equals(type)) {
            return CellType.BOOLEAN;
        } else if("e".equals(type)) {
            return CellType.ERROR;
        } else {
            throw new UnsupportedOperationException("Unsupported cell type '" + type + "'");
        }
    }


    private int getBaseCellType(boolean blankCells) {
        if(contents == null || type == null) {
            return CELL_TYPE_BLANK;
        } else if("n".equals(type)) {
            if (blankCells && contents instanceof String && contents.toString().length() == 0) {
                return CELL_TYPE_BLANK;
            }
            return CELL_TYPE_NUMERIC;
        } else if("s".equals(type) || "inlineStr".equals(type)) {
            return CELL_TYPE_STRING;
        } else if("str".equals(type)) {
            return CELL_TYPE_STRING;
//            return CELL_TYPE_FORMULA;
        } else if("b".equals(type)) {
            return CELL_TYPE_BOOLEAN;
        } else if("e".equals(type)) {
            return CELL_TYPE_ERROR;
        } else {
            throw new UnsupportedOperationException("Unsupported cell type '" + type + "'");
        }
    }

    /**
     * Get the value of the cell as a string. For numeric cells we throw an exception.
     * For blank cells we return an empty string.
     *
     * @return the value of the cell as a string
     */
    @Override
    public String getStringCellValue() {
        return (String) contents;
    }

    /**
     * Get the value of the cell as a number. For strings we throw an exception. For
     * blank cells we return a 0.
     *
     * @return the value of the cell as a number
     * @throws NumberFormatException if the cell value isn't a parsable <code>double</code>.
     */
    @Override
    public double getNumericCellValue() {
        return Double.parseDouble((String) rawContents);
    }

    /**
     * Get the value of the cell as a date. For strings we throw an exception. For
     * blank cells we return a null.
     *
     * @return the value of the cell as a date
     * @throws IllegalStateException if the cell type returned by {@link #getCellType()} is CELL_TYPE_STRING
     * @throws NumberFormatException if the cell value isn't a parsable <code>double</code>.
     */
    @Override
    public Date getDateCellValue() {
        return HSSFDateUtil.getJavaDate(getNumericCellValue());
    }

  /* Not supported */

    /**
     * Not supported
     */
    @Override
    public void setCellType(int cellType) {
        throw new NotSupportedException();
    }

    @Override
    public void setCellType(CellType cellType) {
    }

    /**
     * Not supported
     */
    @Override
    public Sheet getSheet() {
        return sheet;
    }

    /**
     * Not supported
     */
    @Override
    public int getCachedFormulaResultType() {
        return getBaseCellType(false);
    }

    @Override
    public CellType getCachedFormulaResultTypeEnum() {
        return getBaseCellTypeEnum(false);
    }

    /**
     * Not supported
     */
    @Override
    public void setCellValue(double value) {
        throw new NotSupportedException();
    }

    /**
     * Not supported
     */
    @Override
    public void setCellValue(Date value) {
        throw new NotSupportedException();
    }

    /**
     * Not supported
     */
    @Override
    public void setCellValue(Calendar value) {
        throw new NotSupportedException();
    }

    /**
     * Not supported
     */
    @Override
    public void setCellValue(RichTextString value) {
        throw new NotSupportedException();
    }

    /**
     * Not supported
     */
    @Override
    public void setCellValue(String value) {
        throw new NotSupportedException();
    }

    /**
     * Not supported
     */
    @Override
    public void setCellFormula(String formula) throws FormulaParseException {
        this.formulaString = formula;
    }

    /**
     * Not supported
     */
    @Override
    public String getCellFormula() {
        if (isPartOfArrayFormulaGroup() && formulaString == null) {
            return formulaString;
        }

//        CTCellFormula f = _cell.getF();
//        if (isPartOfArrayFormulaGroup() && f == null) {
//            XSSFCell cell = getSheet().getFirstCellInArrayFormula(this);
//            return cell.getCellFormula();
//        }
        if (sharedFormulaHolder != null) {
            return convertSharedFormula(sharedFormulaHolder);
        }
        return formulaString;
    }

    /**
     * Creates a non shared formula from the shared formula counterpart
     *
     * @param sharedFormulaHolder
     * @return non shared formula created for the given shared formula and this cell
     */
    private String convertSharedFormula(SharedFormulaHolder sharedFormulaHolder){

        String sharedFormula = sharedFormulaHolder.getFormulaString(); // We save the shared formula string as formulaString
        //Range of cells which the shared formula applies to

        CellRangeAddress ref = CellRangeAddress.valueOf(sharedFormulaHolder.getRef());

        int sheetIndex = sharedFormulaHolder.getSheetIndex();
        XSSFEvaluationWorkbook fpb = XSSFEvaluationWorkbook.create((XSSFWorkbook) workbook);
        SharedFormula sf = new SharedFormula(SpreadsheetVersion.EXCEL2007);

        Ptg[] ptgs = FormulaParser.parse(sharedFormula, fpb, FormulaType.CELL, sheetIndex);
        Ptg[] fmla = sf.convertSharedFormulas(ptgs,
                getRowIndex() - ref.getFirstRow(), getColumnIndex() - ref.getFirstColumn());
        return FormulaRenderer.toFormulaString(fpb, fmla);
    }

    /**
     * Not supported
     */
    @Override
    public RichTextString getRichStringCellValue() {
        return richTextString;
//        int cellType = getCellType();
//        XSSFRichTextString rt;
//        String subType = getType();
//        switch (cellType) {
//            case CELL_TYPE_BLANK:
//                rt = new XSSFRichTextString("");
//                break;
//            case CELL_TYPE_STRING:
//                if (subType == "inlineStr") {
//                    if(_cell.isSetIs()) {
//                        //string is expressed directly in the cell definition instead of implementing the shared string table.
//                        rt = new XSSFRichTextString(_cell.getIs());
//                    } else if (_cell.isSetV()) {
//                        //cached result of a formula
//                        rt = new XSSFRichTextString(_cell.getV());
//                    } else {
//                        rt = new XSSFRichTextString("");
//                    }
//                } else if (_cell.getT() == STCellType.STR) {
//                    //cached formula value
//                    rt = new XSSFRichTextString(_cell.isSetV() ? _cell.getV() : "");
//                } else {
//                    if (_cell.isSetV()) {
//                        int idx = Integer.parseInt(_cell.getV());
//                        rt = new XSSFRichTextString(_sharedStringSource.getEntryAt(idx));
//                    }
//                    else {
//                        rt = new XSSFRichTextString("");
//                    }
//                }
//                break;
//            case CELL_TYPE_FORMULA:
//                checkFormulaCachedValueType(CELL_TYPE_STRING, getBaseCellType(false));
//                rt = new XSSFRichTextString(_cell.isSetV() ? _cell.getV() : "");
//                break;
//            default:
//                throw typeMismatch(CELL_TYPE_STRING, cellType, false);
//        }
//        rt.setStylesTableReference(_stylesSource);
//        return rt;
//
//        throw new NotSupportedException();
    }

    /**
     * Not supported
     */
    @Override
    public void setCellValue(boolean value) {
        throw new NotSupportedException();
    }

    /**
     * Not supported
     */
    @Override
    public void setCellErrorValue(byte value) {
        throw new NotSupportedException();
    }

    /**
     * Not supported
     */
    @Override
    public boolean getBooleanCellValue() {
        int cellType = getCellType();
        switch(cellType) {
            case CELL_TYPE_BLANK:
                return false;
            case CELL_TYPE_BOOLEAN:
            case CELL_TYPE_FORMULA:
                return StringUtils.isNotEmpty((String) contents) && TRUE_AS_STRING.equals(contents);
            default:
                return false;
        }
    }

    /**
     * Not supported
     */
    @Override
    public byte getErrorCellValue() {
        throw new NotSupportedException();
    }

    @Override
    public void setCellStyle(CellStyle style) {
        this.style = style;
    }

    /**
     * Not supported
     */
    @Override
    public CellStyle getCellStyle() {
        return style;
    }

    /**
     * Not supported
     */
    @Override
    public void setAsActiveCell() {
        throw new NotSupportedException();
    }

    @Override
    public CellAddress getAddress() {
        throw new NotSupportedException();
    }

    /**
     * Not supported
     */
    @Override
    public void setCellComment(Comment comment) {
        throw new NotSupportedException();
    }

    /**
     * Not supported
     */
    @Override
    public Comment getCellComment() {
        throw new NotSupportedException();
    }

    /**
     * Not supported
     */
    @Override
    public void removeCellComment() {
        throw new NotSupportedException();
    }

    /**
     * Not supported
     */
    @Override
    public Hyperlink getHyperlink() {
        throw new NotSupportedException();
    }

    /**
     * Not supported
     */
    @Override
    public void setHyperlink(Hyperlink link) {
        throw new NotSupportedException();
    }

    /**
     * Not supported
     */
    @Override
    public CellRangeAddress getArrayFormulaRange() {
        throw new NotSupportedException();
    }

    /**
     * Not supported
     */
    @Override
    public boolean isPartOfArrayFormulaGroup() {
        return sheet.isCellInArrayFormulaContext(this);
    }

    @Override
    public void removeHyperlink() {
        throw new NotSupportedException();
    }

    public void setRichTextString(RichTextString richTextString) {
        this.richTextString = richTextString;
    }

    public void setFormulaType(String formulaType) {
        this.formulaType = formulaType;
    }

    public String getFormulaType() {
        return formulaType;
    }

    public SharedFormulaHolder getSharedFormulaHolder() {
        return sharedFormulaHolder;
    }

    public void setSharedFormulaHolder(SharedFormulaHolder sharedFormulaHolder) {
        this.sharedFormulaHolder = sharedFormulaHolder;
    }

    @Override
    public String toString() {
        return "StreamingExcelCell{" +
                "columnIndex=" + columnIndex +
                ", rowIndex=" + rowIndex +
                ", contents=" + contents +
                ", type='" + type + '\'' +
                ", formulaType='" + formulaType + '\'' +
                '}';
    }
}
