package com.cla.common.media.files.excel;

import com.cla.common.domain.dto.FileMediaType;
import com.cla.common.domain.dto.mediaproc.excel.*;
import com.cla.common.media.files.excel.streaming.StreamingExcelCell;
import com.cla.common.media.files.word.FileMetadataUtils;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import org.apache.commons.io.output.NullOutputStream;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.record.*;
import org.apache.poi.hssf.record.aggregates.FormulaRecordAggregate;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFDataFormatter;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.apache.poi.openxml4j.opc.*;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.util.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.poi.ss.usermodel.DateUtil;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;
import java.net.URLDecoder;
import java.util.*;
import java.util.regex.Pattern;

/**
 *
 * Created by uri on 18/10/2015.
 */
public class ExcelFileUtils {

    private static final int INGEST_CONTENT_ONLY_FLOOR = 2500;
    private static final int INGEST_CONTENT_ONLY_CEILING = 250;
    private static Logger logger = LoggerFactory.getLogger(ExcelFileUtils.class);

    // Define sheet margin to be analyzed in details
    private static final int cellsAnalysisArea_top = 40;
    private static final int cellsAnalysisArea_bottom = 20;
    private static final int cellsAnalysisArea_left = 40;
    private static final int cellsAnalysisArea_right = 20;
    private static final int cellsAnalysisFullLineDropRange_top = 150;
    private static final int cellsAnalysisFullLineDropRange_bottom = 100;
    private static final int totalCellsLimit_threshold1 = 30000;
    private static final int totalCellsLimit_threshold2 = 60000;
    // Max cells for detailed analysis for the above numbers = (40+20)*(40+20) = 3600

    /*
     * Code taken from project Apache-Tika: NameDetector.detector()
	 */
    private static final Map<Pattern, FileMediaType> patterns;

    private static final Pattern utf8ValidPattern = Pattern.compile(".+%[0-9,a-f,A-F][0-9,a-f,A-F].*");

    static {
        final Map<Pattern, FileMediaType> p = new HashMap<>();
        p.put(Pattern.compile(".*\\.xlsx", Pattern.CASE_INSENSITIVE), FileMediaType.EXCELXML);
        p.put(Pattern.compile(".*\\.xltx", Pattern.CASE_INSENSITIVE), FileMediaType.EXCELXML);
        p.put(Pattern.compile(".*\\.xls", Pattern.CASE_INSENSITIVE), FileMediaType.EXCEL);
        p.put(Pattern.compile(".*\\.xlt", Pattern.CASE_INSENSITIVE), FileMediaType.EXCEL);
        p.put(Pattern.compile(".*\\.xlsm", Pattern.CASE_INSENSITIVE), FileMediaType.EXCELXML);
        patterns = Collections.unmodifiableMap(p);
    }

    public static FileMediaType mediaNameDetector(final String nameOrMime) {

        // Look for a resource name in the input metadata
        String name = nameOrMime;
        if (name != null) {
            // If the name is a URL, skip the trailing query and fragment parts
            final int question = name.indexOf('?');
            if (question != -1) {
                name = name.substring(0, question);
            }
            /*
            int hash = name.indexOf('#');
            if (hash != -1) {
                name = name.substring(0, hash);
            }
			*/
            // If the name is a URL or a path, skip all but the last component
            final int slash = name.lastIndexOf('/');
            if (slash != -1) {
                name = name.substring(slash + 1);
            }
            final int backslash = name.lastIndexOf('\\');
            if (backslash != -1) {
                name = name.substring(backslash + 1);
            }

            // Decode any potential URL encoding
            final int percent = name.indexOf('%');
            if (percent != -1) {
                try {
                    if (utf8ValidPattern.matcher(name).matches()) {
                        name = URLDecoder.decode(name, "UTF-8");
                    }
                    //} catch (UnsupportedEncodingException e) {
                } catch (final Exception e) {
                    logger.warn("Bad decoding of URL <{}>. {}", name, e.getMessage());
                    //throw new IllegalStateException("UTF-8 not supported", e);
                }
            }

            // Skip any leading or trailing whitespace
            name = name.trim();
            if (name.length() > 0) {
                // Match the name against the registered patterns
                for (final Pattern pattern : patterns.keySet()) {
                    if (pattern.matcher(name).matches()) {
                        return patterns.get(pattern);
                    }
                }
            }
        }

        return FileMediaType.UNKNOWN;
    }

    public static boolean isDetailedAnalysis(ExcelIssuesCounters excelIssuesCounters, ExcelWorkbook excelWorkbook, int nonBlankRowsCount, int rowsCount, int nonBlankCellsCount, int cellsCount) {
        boolean detailedAnalysis = false;
        // don't add cells data if global cells count reached peak
        if (excelWorkbook.getNumOfCells() < totalCellsLimit_threshold2) {
            // reduce peripheral margin when above first global-cells-limit
            final int factor = (excelWorkbook.getNumOfCells() < totalCellsLimit_threshold1) ? 1 : 2;
            // flag if cell is within the peripheral margins set for detailed analysis
            detailedAnalysis = !(rowsCount * factor >= cellsAnalysisArea_top &&
                    rowsCount * factor < (nonBlankRowsCount - cellsAnalysisArea_bottom) &&
                    cellsCount * factor >= cellsAnalysisArea_left &&
                    cellsCount * factor < (nonBlankCellsCount - cellsAnalysisArea_right));
            if (!detailedAnalysis) {
                excelIssuesCounters.addExtraSkippedCell(factor,rowsCount);
            }
            if (detailedAnalysis &&
                    rowsCount > cellsAnalysisFullLineDropRange_top &&
                    rowsCount < (nonBlankRowsCount - cellsAnalysisFullLineDropRange_bottom)) {
                detailedAnalysis = false;
                excelIssuesCounters.addExtraSkippedCell(factor,rowsCount);
            }
        }
        return detailedAnalysis;
    }

    public static short getPatternData(final CellStyle style) {
        if (style.getFillPattern() == CellStyle.NO_FILL) {
            return 0;        // zero should indicate 'no pattern'.
        }
        final short prime = 31;
        short result = style.getFillPattern();
        result = (short) (prime * result + style.getFillBackgroundColor());
        result = (short) (prime * result + style.getFillForegroundColor());
        return result;
    }

    public static short getBorderData(final CellStyle style) {
        final short bt = style.getBorderTop();
        final short br = style.getBorderRight();
        final short bb = style.getBorderBottom();
        final short bl = style.getBorderLeft();
        if (bt == CellStyle.BORDER_NONE &&
                br == CellStyle.BORDER_NONE &&
                bb == CellStyle.BORDER_NONE &&
                bl == CellStyle.BORDER_NONE) {
            return 0;        // zero should indicate 'no borders'.
        }
        final short prime = 37;
        short result = 1;
        result = (short) (prime * result + bt);
        result = (short) (prime * result + br);
        result = (short) (prime * result + bb);
        result = (short) (prime * result + bl);
        result = (short) (prime * result + ((bt == CellStyle.BORDER_NONE) ? 0 : style.getTopBorderColor()));
        result = (short) (prime * result + ((br == CellStyle.BORDER_NONE) ? 0 : style.getRightBorderColor()));
        result = (short) (prime * result + ((bb == CellStyle.BORDER_NONE) ? 0 : style.getBottomBorderColor()));
        result = (short) (prime * result + ((bl == CellStyle.BORDER_NONE) ? 0 : style.getLeftBorderColor()));
        return result;
    }

    public static ExcelCell getCellValue(final Cell cell, final Object evaluationWorkbookWrapper, final int sheetIndex) {
        final ExcelCell excelCell = new ExcelCell();

        int cellValType = cell.getCellType();
        if (cellValType == Cell.CELL_TYPE_FORMULA) {
            cellValType = cell.getCachedFormulaResultType();
            excelCell.setFormula(cell.getCellFormula());
            if (evaluationWorkbookWrapper != null) {
                try {
                    excelCell.setFormulaIngestion(ExcelFormulaUtils.formulaIngestion(cell, evaluationWorkbookWrapper, sheetIndex));
                } catch (final Exception ex) {
                    logger.warn(ex.getMessage() + " cell: " + cell.toString() + " sheet: " + cell.getSheet().getSheetName());
                    excelCell.setFormulaIngestion(cell.getCellFormula());
                    excelCell.setIngestionError(true);
                }
            } else {
                excelCell.setFormulaIngestion(cell.getCellFormula());
            }
        }
        excelCell.setType(cellValType);
        switch (cellValType) {
            case Cell.CELL_TYPE_BLANK:
//			excelCell.setBlank(true);	 -- implicitly set by type.
                break;
            case Cell.CELL_TYPE_BOOLEAN:
                excelCell.setBooleanValue(cell.getBooleanCellValue());
                break;
            case Cell.CELL_TYPE_ERROR:
//			excelCell.setError(true);	-- implicitly set by type.
                break;
            case Cell.CELL_TYPE_NUMERIC:
                excelCell.setNumericValue(cell.getNumericCellValue());
                if (DateUtil.isCellDateFormatted(cell)) {
                    excelCell.setDate(true);
                }
                if (cell instanceof StreamingExcelCell) { // no support for others
                    excelCell.setStringValue(cell.getStringCellValue());
                }
                break;
            case Cell.CELL_TYPE_STRING:
                excelCell.setStringValue(cell.getStringCellValue());
                break;
            default:
                throw new RuntimeException("Not supported type : " + cell.getCellType());
        }
        return excelCell;
    }

    public static ExcellCellStyle getCellStyle(final Cell cell, final Workbook workbook) {

        final CellStyle style = cell.getCellStyle();
        final Font font = workbook.getFontAt(style.getFontIndex());

        final ExcellCellStyle ecs = new ExcellCellStyle(font.getFontHeightInPoints(),
                style.getIndex() == 0, font.getBoldweight() > Font.BOLDWEIGHT_NORMAL,
                font.getUnderline() > 0,
                1, 0,
                font.getColor(), font.getColor() == Font.COLOR_NORMAL,
                style.getDataFormatString(),
                getPatternData(style),
                getBorderData(style),
                (byte) style.getAlignment());

        if (cell.getCellType() == Cell.CELL_TYPE_STRING) {
            final RichTextString rtf = cell.getRichStringCellValue();
            //ecs.setNumOfRuns(rtf.numFormattingRuns());
            ecs.setSizeOfFirstRun(rtf.numFormattingRuns() > 1 ? rtf.getIndexOfFormattingRun(1) : rtf.length());
        }

        return ecs;
    }

    public static ExcelCell createExcelCell(ExcelIssuesCounters excelIssuesCounters, Workbook workbook, Object evaluationWorkbookWrapper, int sheetIndex, Cell cell) {
    /*
     * There may be a better way to pass the evaluationWorkbook and seetIndex all the way down to the formulaIngestion function
     */
        ExcelCell cellValue;
        try {
            cellValue = getCellValue(cell, evaluationWorkbookWrapper, sheetIndex);
        } catch (final Exception ex) {
            excelIssuesCounters.addIngestionError();
            logger.debug(ex.getMessage() + " cell: [" + cell.getColumnIndex() + "," + cell.getRowIndex() + "] sheet:" + cell.getSheet().getSheetName());
            return null;
        }
        if (cellValue.isIngestionError()) {
            excelIssuesCounters.addIngestionError();
        }
        try {
            final ExcellCellStyle ecs = getCellStyle(cell, workbook);
            cellValue.setCellStyle(ecs);
        } catch (final Exception ex) {
            excelIssuesCounters.addIngestionError();
            //logger.debug(ex.getMessage() + " cell: [" + cell.getColumnIndex() + "," + cell.getRowIndex() + "] sheet:" + cell.getSheet().getSheetName() + " file:" + nameOrMime);
        }
        return cellValue;
    }

    public static boolean checkMaxExcelFileSizeLimit(long maxExcelFileSize, String path, ExcelWorkbook excelWorkbook, ClaFilePropertiesDto cfp) {
        if (maxExcelFileSize > 0 && cfp != null && cfp.getFileSize() > maxExcelFileSize) {
            // Don't ingest files with size above threshold - potential heap overflow.
            // TODO: handle this properly: use streamlined POI API; handle sheet by sheet; process in a dedicated queue/thread; etc.
            logger.warn("File {} skipped ingestion because file size {} > limit {}", path.toString(), cfp.getFileSize(), maxExcelFileSize);

            final ExcelWorkbookMetaData claMetadata = FileMetadataUtils.extractMetadata(ExcelWorkbookMetaData.class,
                    null, cfp);
            claMetadata.setNumOfSheets(-1);
            excelWorkbook.setMetadata(claMetadata);

            if (logger.isTraceEnabled()) {
                logger.trace("MetaData = {}", claMetadata.toCsvString());
            }
            return true;
        }
        return false;
    }

    public static boolean shouldIngestCellContentOnly(int currentNonBlankRowsCount, int nonBlankRows) {
        return currentNonBlankRowsCount > INGEST_CONTENT_ONLY_FLOOR &&
                currentNonBlankRowsCount < nonBlankRows - INGEST_CONTENT_ONLY_CEILING;
    }

    public static class ExcelIssuesCounters {
        int extraSkippedCells = 0;
        int ingestionErrors = 0;
        int blankCells = 0;
        int errorCells = 0;

        private int factor;
        public Set<Integer> rows = new HashSet<>();
        public void addIngestionError() {
            ingestionErrors++;
        }

        public void addExtraSkippedCell(int factor, int rowsCount) {
            this.factor = factor;
            extraSkippedCells++;
            rows.add(rowsCount);
        }

        public int getExtraSkippedCells() {
            return extraSkippedCells;
        }

        public void printAdditionalInformation() {
            String rowsString = StringUtils.join(rows, ",");
            logger.debug("Mid-range factor {}, number of rows {}. Rows: {}", factor, rows.size(),rowsString);

        }

        public int getBlankCells() {
            return blankCells;
        }

        public int getIngestionErrors() {
            return ingestionErrors;
        }

        public void addBlankCell() {
            blankCells++;
        }

        public void addErrorCell() {
            errorCells++;
        }

        public int getErrorCells() {
            return errorCells;
        }
    }

    /**
     * Get the value of the cell as a number.
     * For strings we throw an exception.
     * For blank cells we return a 0.
     * See {@link HSSFDataFormatter} for turning this
     * number into a string similar to that which
     * Excel would render this number as.
     */
    public static double getNumericCellValue(int cellType, CellRecord record) {
        if (record.getSid() == FormulaRecord.sid) {
            FormulaRecord fr = ((FormulaRecord) record);
            return fr.getValue();
        }
        switch (cellType) {
            case Cell.CELL_TYPE_BLANK:
                return 0.0;
            case Cell.CELL_TYPE_NUMERIC:
                return ((NumberRecord) record).getValue();
            default:
                throw new RuntimeException("Type mismatch on cell value " + cellType + " :" + record.toString());
        }
    }

    /**
     * used internally -- given a cell value record, figure out its type
     */
    public static int determineType(CellValueRecordInterface cval) {
        if (cval instanceof FormulaRecordAggregate) {
            return HSSFCell.CELL_TYPE_FORMULA;
        }
        if (cval instanceof FormulaRecord) {
            return HSSFCell.CELL_TYPE_FORMULA;
        }
        // all others are plain BIFF records
        Record record = (Record) cval;
        switch (record.getSid()) {

            case NumberRecord.sid:
                return HSSFCell.CELL_TYPE_NUMERIC;
            case BlankRecord.sid:
                return HSSFCell.CELL_TYPE_BLANK;
            case LabelSSTRecord.sid:
                return HSSFCell.CELL_TYPE_STRING;
            case BoolErrRecord.sid:
                BoolErrRecord boolErrRecord = (BoolErrRecord) record;

                return boolErrRecord.isBoolean()
                        ? HSSFCell.CELL_TYPE_BOOLEAN
                        : HSSFCell.CELL_TYPE_ERROR;
        }
        throw new RuntimeException("Bad cell value rec (" + cval.getClass().getName() + ")");
    }

    /**
     * Clone the specified package.
     *
     * @param pkg the package to clone
     * @return the cloned package
     */
    public static OPCPackage cloneNoSheets(OPCPackage pkg) throws OpenXML4JException, IOException {

        OPCPackage dest = OPCPackage.create(new NullOutputStream());
        PackageRelationshipCollection rels = pkg.getRelationships();
        for (PackageRelationship rel : rels) {
            PackagePart part = pkg.getPart(rel);
            PackagePart part_tgt;
            if (rel.getRelationshipType().equals(PackageRelationshipTypes.CORE_PROPERTIES)) {
                copyProperties(pkg.getPackageProperties(), dest.getPackageProperties());
                continue;
            }
            dest.addRelationship(part.getPartName(), rel.getTargetMode(), rel.getRelationshipType());
            part_tgt = dest.createPart(part.getPartName(), part.getContentType());

            OutputStream out = part_tgt.getOutputStream();
            IOUtils.copy(part.getInputStream(), out);
            out.close();

            if (part.hasRelationships()) {
                copy(pkg, part, dest, part_tgt);
            }
        }
        return dest;
    }

    /**
     * Clone the specified package.
     *
     * @param pkg the package to clone
     * @return the cloned package
     */
    public static OPCPackage cloneCurtailedParts(OPCPackage pkg) throws OpenXML4JException, IOException {

        OPCPackage dest = OPCPackage.create(new NullOutputStream());
        PackageRelationshipCollection rels = pkg.getRelationships();
        for (PackageRelationship rel : rels) {
            PackagePart part = pkg.getPart(rel);
            PackagePart part_tgt;
            if (rel.getRelationshipType().equals(PackageRelationshipTypes.CORE_PROPERTIES)) {
                copyProperties(pkg.getPackageProperties(), dest.getPackageProperties());
                continue;
            }
            dest.addRelationship(part.getPartName(), rel.getTargetMode(), rel.getRelationshipType());
            part_tgt = dest.createPart(part.getPartName(), part.getContentType());

            OutputStream out = part_tgt.getOutputStream();
            IOUtils.copy(part.getInputStream(), out);
            out.close();

            if (part.hasRelationships()) {
                copy(pkg, part, dest, part_tgt);
            }
        }
        return dest;
    }

    /**
     * Copy core package properties
     *
     * @param src source properties
     * @param tgt target properties
     */
    private static void copyProperties(PackageProperties src, PackageProperties tgt) {
        tgt.setCategoryProperty(src.getCategoryProperty().getValue());
        tgt.setContentStatusProperty(src.getContentStatusProperty().getValue());
        tgt.setContentTypeProperty(src.getContentTypeProperty().getValue());
        tgt.setCreatorProperty(src.getCreatorProperty().getValue());
        tgt.setDescriptionProperty(src.getDescriptionProperty().getValue());
        tgt.setIdentifierProperty(src.getIdentifierProperty().getValue());
        tgt.setKeywordsProperty(src.getKeywordsProperty().getValue());
        tgt.setLanguageProperty(src.getLanguageProperty().getValue());
        tgt.setRevisionProperty(src.getRevisionProperty().getValue());
        tgt.setSubjectProperty(src.getSubjectProperty().getValue());
        tgt.setTitleProperty(src.getTitleProperty().getValue());
        tgt.setVersionProperty(src.getVersionProperty().getValue());
    }

    /**
     * Recursively copy package parts to the destination package
     */
    private static void copy(OPCPackage pkg, PackagePart part, OPCPackage tgt, PackagePart part_tgt) throws OpenXML4JException, IOException {
        PackageRelationshipCollection rels = part.getRelationships();
        if (rels != null) for (PackageRelationship rel : rels) {
            PackagePart p;
            if (rel.getTargetMode() == TargetMode.EXTERNAL) {
                part_tgt.addExternalRelationship(rel.getTargetURI().toString(), rel.getRelationshipType(), rel.getId());
                //external relations don't have associated package parts
                continue;
            }
            URI uri = rel.getTargetURI();
            ///xl/worksheets/sheet1.xml
            if (uri.getPath() != null && uri.getPath().matches("/xl/worksheets/.*\\.xml")) {
                logger.trace("Not copying sheet: " + uri.getPath());
                continue;
            }
            if (uri.getRawFragment() != null) {
                part_tgt.addRelationship(uri, rel.getTargetMode(), rel.getRelationshipType(), rel.getId());
                continue;
            }
            PackagePartName relName = PackagingURIHelper.createPartName(rel.getTargetURI());
            p = pkg.getPart(relName);
            part_tgt.addRelationship(p.getPartName(), rel.getTargetMode(), rel.getRelationshipType(), rel.getId());


            PackagePart dest;
            if (!tgt.containPart(p.getPartName())) {
                dest = tgt.createPart(p.getPartName(), p.getContentType());
                OutputStream out = dest.getOutputStream();
                IOUtils.copy(p.getInputStream(), out);
                out.close();
                copy(pkg, p, tgt, dest);
            }
        }
    }

    public static String getCellStringValue(StreamingExcelCell cell) {
        final String prefix ="";

        int cellValType = cell.getCellType();
        if (cellValType == Cell.CELL_TYPE_FORMULA) {
            cellValType = cell.getCachedFormulaResultType();
        }
        String stringValue = "";
        switch (cellValType) {
            case Cell.CELL_TYPE_STRING:
            case Cell.CELL_TYPE_BOOLEAN:
                stringValue = cell.getStringCellValue();
                break;
            case Cell.CELL_TYPE_NUMERIC:
                stringValue = String.valueOf(cell.getNumericCellValue());
                break;
        }
        return ExcelCell.doToString(cellValType,stringValue,prefix);
    }
}
