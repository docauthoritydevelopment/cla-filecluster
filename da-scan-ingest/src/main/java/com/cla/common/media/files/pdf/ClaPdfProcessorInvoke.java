package com.cla.common.media.files.pdf;

import com.cla.common.allocation.IngestionResourceManager;
import com.cla.common.utils.FileSizeUnits;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSInteger;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSStream;
import org.apache.pdfbox.pdfviewer.PageDrawer;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDResources;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.graphics.PDGraphicsState;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDXObject;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDXObjectForm;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDXObjectImage;
import org.apache.pdfbox.util.Matrix;
import org.apache.pdfbox.util.PDFOperator;
import org.apache.pdfbox.util.operator.pagedrawer.Invoke;

import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.GeneralPath;
import java.awt.geom.Point2D;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * Created by itay on 26/041/2017.
 */

public class ClaPdfProcessorInvoke extends Invoke {
    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(ClaPdfProcessorInvoke.class);

    ClaPdfProcessorInvoke(IngestionResourceManager ingestionResourceManager, boolean acquireImageMemory) {
        super();
        this.ingestionResourceManager = ingestionResourceManager;
        this.acquireImageMemory = acquireImageMemory;
    }

    private boolean acquireImageMemory = false;

    private IngestionResourceManager ingestionResourceManager;

    public void process(PDFOperator operator, List<COSBase> arguments) throws IOException
    {
        PageDrawer drawer = (PageDrawer)context;
        PDPage page = drawer.getPage();
        COSName objectName = (COSName)arguments.get( 0 );
        Map<String, PDXObject> xobjects = drawer.getResources().getXObjects();
        PDXObject xobject = (PDXObject)xobjects.get( objectName.getName() );
        if ( xobject == null )
        {
            logger.info("WARN: Can't find the XObject for '{}'", objectName.getName());
        }
        else if( xobject instanceof PDXObjectImage)
        {
            PDXObjectImage image = (PDXObjectImage)xobject;
            int resourcesAquiredMB = 0;
            try
            {
                if (acquireImageMemory) {
                    if (ingestionResourceManager==null) {
                        logger.error("Null ingestionResourceManager in PDF processor");
                    }
                    else {
                        int width = image.getWidth();
                        int height = image.getHeight();
                        int resources = image.getPDStream().getLength();  // Estimate memory required for image
                        if (width > 0 && height > 0) {
                            resources = Integer.max(resources / 4, width * height);
                        }
                        resources = (int) FileSizeUnits.BYTE.toMegabytes(resources * 4);  // Convert from pixels to memory MB
                        if (resources > 10) {   // No point to allocate for small images
                            boolean resourcesAquired = ingestionResourceManager.acquire(resources);
                            if (resourcesAquired) {
                                resourcesAquiredMB = resources;
                                if (logger.isDebugEnabled()) {
                                    logger.debug("Image resources acquired {}. {} available", resources, ingestionResourceManager.getAvailable());
                                }
                            } else {
                                if (logger.isDebugEnabled()) {
                                    logger.debug("Image resources NOT acquired {}. {} available", resources, ingestionResourceManager.getAvailable());
                                }
                            }
                        } else {
                            logger.trace("Skipped image resources acquision. Length={} WxH {}x{}", image.getPDStream().getLength(), width, height);
                        }
                    }
                }
                if (image.getImageMask()) {
                    // set the current non stroking colorstate, so that it can
                    // be used to create a stencil masked image
                    image.setStencilColor(drawer.getGraphicsState().getNonStrokingColor());
                }
                // ** Avoid reading image bytes into memory
//                BufferedImage awtImage = image.getRGBImage();
//                if (awtImage == null) {
//                    logger.info("WARN: getRGBImage returned NULL");
//                    return;//TODO PKOCH
//                }
//                int imageWidth = awtImage.getWidth();
//                int imageHeight = awtImage.getHeight();
                int imageWidth = image.getWidth();
                int imageHeight = image.getHeight();
                if (imageWidth <=0 || imageHeight <=0) {
                    logger.info("WARN: Could not find image size from PDXObjectImage. Length={}", image.getPDStream().getLength());
                }
                double pageHeight = drawer.getPageSize().getHeight();

                if (imageWidth > 500 && imageHeight > 500) {
                    logger.debug("image: WxH {}x{}", imageWidth, imageHeight);
                }
                else {
                    logger.trace("image: WxH {}x{}", imageWidth, imageHeight);
                }

                Matrix ctm = drawer.getGraphicsState().getCurrentTransformationMatrix();
                float yScaling = ctm.getYScale();
                float angle = (float) Math.acos(ctm.getValue(0, 0) / ctm.getXScale());
                if (ctm.getValue(0, 1) < 0 && ctm.getValue(1, 0) > 0) {
                    angle = (-1) * angle;
                }
                ctm.setValue(2, 1, (float) (pageHeight - ctm.getYPosition() - Math.cos(angle) * yScaling));
                ctm.setValue(2, 0, (float) (ctm.getXPosition() - Math.sin(angle) * yScaling));
                // because of the moved 0,0-reference, we have to shear in the opposite direction
                ctm.setValue(0, 1, (-1) * ctm.getValue(0, 1));
                ctm.setValue(1, 0, (-1) * ctm.getValue(1, 0));
                AffineTransform ctmAT = ctm.createAffineTransform();
                ctmAT.scale(1f / imageWidth, 1f / imageHeight);

//                    drawer.drawImage(awtImage, ctmAT);
            }
            catch( Exception e )
            {
                logger.info("WARN: Exception during PDF invoke. {}", e.getMessage());
            }
            finally {
                if (resourcesAquiredMB > 0) {
                    ingestionResourceManager.release(resourcesAquiredMB);
                    if (logger.isDebugEnabled()) {
                        logger.debug("Image resources released {}. {} available", resourcesAquiredMB, ingestionResourceManager.getAvailable());
                    }
                }
            }
        }
        else if(xobject instanceof PDXObjectForm)
        {
            // save the graphics state
            context.getGraphicsStack().push( (PDGraphicsState)context.getGraphicsState().clone() );

            PDXObjectForm form = (PDXObjectForm)xobject;
            COSStream formContentstream = form.getCOSStream();
            // find some optional resources, instead of using the current resources
            PDResources pdResources = form.getResources();
            // if there is an optional form matrix, we have to map the form space to the user space
            Matrix matrix = form.getMatrix();
            if (matrix != null)
            {
                Matrix xobjectCTM = matrix.multiply( context.getGraphicsState().getCurrentTransformationMatrix());
                context.getGraphicsState().setCurrentTransformationMatrix(xobjectCTM);
            }
            if (form.getBBox() != null)
            {
                PDGraphicsState graphicsState = context.getGraphicsState();
                PDRectangle bBox = form.getBBox();

                float x1 = bBox.getLowerLeftX();
                float y1 = bBox.getLowerLeftY();
                float x2 = bBox.getUpperRightX();
                float y2 = bBox.getUpperRightY();

                Point2D p0 = drawer.transformedPoint(x1, y1);
                Point2D p1 = drawer.transformedPoint(x2, y1);
                Point2D p2 = drawer.transformedPoint(x2, y2);
                Point2D p3 = drawer.transformedPoint(x1, y2);

                GeneralPath bboxPath = new GeneralPath();
                bboxPath.moveTo((float) p0.getX(), (float) p0.getY());
                bboxPath.lineTo((float) p1.getX(), (float) p1.getY());
                bboxPath.lineTo((float) p2.getX(), (float) p2.getY());
                bboxPath.lineTo((float) p3.getX(), (float) p3.getY());
                bboxPath.closePath();

                Area resultClippingArea = new Area(graphicsState.getCurrentClippingPath());
                Area newArea = new Area(bboxPath);
                resultClippingArea.intersect(newArea);

                graphicsState.setCurrentClippingPath(resultClippingArea);
            }
            getContext().processSubStream(page, pdResources, formContentstream);

            // restore the graphics state
            context.setGraphicsState( (PDGraphicsState)context.getGraphicsStack().pop() );
        }
    }

    public static int getCosIntValue(PDXObjectImage image, String key) {
        try {
            COSBase cObj = image.getCOSStream().getItem(key);
            if (cObj != null && cObj instanceof COSInteger) {
                return ((COSInteger)cObj).intValue();
            }
        }
        catch (Exception e) {
        }
        return -1;
    }

}
