package com.cla.common.media.files.word;

import com.cla.common.domain.dto.histogram.ClaHistogramCollection;
import com.cla.common.domain.dto.mediaproc.word.WordFile;
import com.cla.common.domain.dto.word.WordFilesSimilarity;

public class WordComparator {

	public static WordFilesSimilarity compareFiles(WordFile fileA, WordFile fileB) {
		return new WordFilesSimilarity(fileA, fileB);
	}

	private static float analyzeHeading(ClaHistogramCollection<String> headings1,
			ClaHistogramCollection<String> headings2) {
		return headings1.getSimilarityRatio(headings2);
	}

	private static float analyzeNGrams(ClaHistogramCollection<Long> ngrams1,
			ClaHistogramCollection<Long> ngrams2) {
		return ngrams1.getSimilarityRatio(ngrams2);
	}

}
