package com.cla.common.media.files.ooxml;

import com.cla.common.media.files.excel.ClaXSSFReader;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.openxml4j.opc.PackagePart;
import org.apache.poi.xssf.eventusermodel.ReadOnlySharedStringsTable;
import org.apache.poi.xssf.eventusermodel.XSSFReader;
import org.apache.poi.xssf.extractor.XSSFEventBasedExcelExtractor;
import org.apache.poi.xssf.model.CommentsTable;
import org.apache.poi.xssf.model.StylesTable;
import org.apache.poi.xssf.usermodel.XSSFShape;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.microsoft.ooxml.XSSFExcelExtractorDecorator;
import org.apache.tika.sax.XHTMLContentHandler;
import org.apache.xmlbeans.XmlException;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Locale;

/**
 *
 * Created by vladi on 4/3/2017.
 */
public class ClaXSSFExcelExtractorDecorator extends XSSFExcelExtractorDecorator {

    private  XSSFEventBasedExcelExtractor innerExtractor;
    private int sizeThreshold;
    private int totalObjectsLimit;

    private static Constructor<SheetTextAsHTML> sheetTextConstructor;
    private static Field headersField;
    private static Field footersField;
    private static Field sheetPartsField;
    private static Method extractHeaderFooterMethod;
    private static Method addDrawingHyperLinksMethod;
    private static Method processShapesMethod;
    private static Method extractHyperLinksMethod;

    static{
        try {
            sheetTextConstructor = SheetTextAsHTML.class.getDeclaredConstructor(XHTMLContentHandler.class);
            headersField = SheetTextAsHTML.class.getDeclaredField("headers");
            footersField = SheetTextAsHTML.class.getDeclaredField("footers");
            sheetPartsField = XSSFExcelExtractorDecorator.class.getDeclaredField("sheetParts");

            extractHeaderFooterMethod = XSSFExcelExtractorDecorator.class.getDeclaredMethod("extractHeaderFooter",
                    String.class, XHTMLContentHandler.class);
            addDrawingHyperLinksMethod = XSSFExcelExtractorDecorator.class.getDeclaredMethod("addDrawingHyperLinks",
                    PackagePart.class);
            processShapesMethod = XSSFExcelExtractorDecorator.class.getDeclaredMethod("processShapes",
                    List.class, XHTMLContentHandler.class);
            extractHyperLinksMethod = XSSFExcelExtractorDecorator.class.getDeclaredMethod("extractHyperLinks",
                    PackagePart.class, XHTMLContentHandler.class);

            sheetTextConstructor.setAccessible(true);
            headersField.setAccessible(true);
            footersField.setAccessible(true);
            sheetPartsField.setAccessible(true);
            extractHeaderFooterMethod.setAccessible(true);
            addDrawingHyperLinksMethod.setAccessible(true);
            processShapesMethod.setAccessible(true);
            extractHyperLinksMethod.setAccessible(true);
        }
        catch (NoSuchMethodException | NoSuchFieldException e) {
            throw new RuntimeException("Failed to apply reflection on SheetTextAsHTML.", e);
        }
    }

    ClaXSSFExcelExtractorDecorator(ParseContext context, XSSFEventBasedExcelExtractor extractor, Locale locale,
                                   int sizeThreshold, int totalObjectsLimit) {
        super(context, extractor, locale);
        this.innerExtractor = extractor;
        this.sizeThreshold = sizeThreshold;
        this.totalObjectsLimit = totalObjectsLimit;
    }

    @Override
    protected void buildXHTML(XHTMLContentHandler xhtml) throws SAXException,
            XmlException, IOException {
        OPCPackage container = innerExtractor.getPackage();

        ReadOnlySharedStringsTable strings;
        ClaXSSFReader.SheetIterator iter;
        XSSFReader xssfReader;
        StylesTable styles;
        try {
            //-------------------------------------------------------
            // CLA customization: ClaXSSFReader ***
            //-------------------------------------------------------
            xssfReader = new ClaXSSFReader(container, sizeThreshold, totalObjectsLimit);
            styles = xssfReader.getStylesTable();
            iter = (ClaXSSFReader.SheetIterator) xssfReader.getSheetsData();
            strings = new ReadOnlySharedStringsTable(container);
        } catch ( OpenXML4JException e) {
            throw new XmlException(e);
        }

        while (iter.hasNext()) {
            InputStream stream = iter.next();
            PackagePart sheetPart = iter.getSheetPart();
            innerAddDrawingHyperLinks(sheetPart);
            getSheetParts().add(sheetPart);

            SheetTextAsHTMLAdapter sheetExtractor = new SheetTextAsHTMLAdapter(xhtml);
            CommentsTable comments = iter.getSheetComments();

            // Start, and output the sheet name
            xhtml.startElement("div");
            xhtml.element("h1", iter.getSheetName());

            // Extract the main sheet contents
            xhtml.startElement("table");
            xhtml.startElement("tbody");

            processSheet(sheetExtractor.getInnerInstance(), comments, styles, strings, stream);

            xhtml.endElement("tbody");
            xhtml.endElement("table");

            // Output any headers and footers
            // (Need to process the sheet to get them, so we can't
            //  do the headers before the contents)
            for (String header : sheetExtractor.getHeaders()) {
                innerExtractHeaderFooter(header, xhtml);
            }
            for (String footer : sheetExtractor.getFooters()) {
                innerExtractHeaderFooter(footer, xhtml);
            }
            innerProcessShapes(iter.getShapes(), xhtml);

            //for now dump sheet hyperlinks at bottom of page
            //consider a double-pass of the inputstream to reunite hyperlinks with cells/textboxes
            //step 1: extract hyperlink info from bottom of page
            //step 2: process as we do now, but with cached hyperlink relationship info
            innerExtractHyperLinks(sheetPart, xhtml);
            // All done with this sheet
            xhtml.endElement("div");
        }
    }

    //--------------- classes and methods used to access different parts of the original class with reflection

    private void innerAddDrawingHyperLinks(PackagePart sheetPart){
        invokeMethod(addDrawingHyperLinksMethod, sheetPart);
    }

    private void innerProcessShapes(List<XSSFShape> shapes, XHTMLContentHandler xhtml){
        invokeMethod(processShapesMethod, shapes, xhtml);
    }

    private void innerExtractHeaderFooter(String part, XHTMLContentHandler xhtml){
        invokeMethod(extractHeaderFooterMethod, part, xhtml);
    }

    private void innerExtractHyperLinks(PackagePart sheetPart, XHTMLContentHandler xhtml){
        invokeMethod(extractHyperLinksMethod, sheetPart, xhtml);
    }

    private void invokeMethod(Method method, Object... args){
        try {
            method.invoke(this, args);
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new RuntimeException("Failed to invoke method.", e);
        }
    }

    @SuppressWarnings("unchecked")
    private List<PackagePart> getSheetParts(){
        try {
            return (List<PackagePart>)sheetPartsField.get(this);
        } catch (IllegalAccessException e) {
            throw new RuntimeException("Failed to access SheetTextAsHTML field sheetParts.", e);
        }
    }

    public static class SheetTextAsHTMLAdapter{
        private SheetTextAsHTML inner;

        SheetTextAsHTMLAdapter(XHTMLContentHandler handler) {
            try {
                inner = sheetTextConstructor.newInstance(handler);
            }
            catch (IllegalAccessException | InvocationTargetException | InstantiationException  e) {
                throw new RuntimeException("Failed to instantiate SheetTextAsHTML.", e);
            }
        }

        SheetTextAsHTML getInnerInstance(){
            return inner;
        }

        @SuppressWarnings("unchecked")
        List<String> getHeaders(){
            try {
                return (List<String>)headersField.get(inner);
            }
            catch (IllegalAccessException e) {
                throw new RuntimeException("Failed to access SheetTextAsHTML headers.", e);
            }
        }

        @SuppressWarnings("unchecked")
        List<String> getFooters(){
            try {
                return (List<String>)footersField.get(inner);
            }
            catch (IllegalAccessException e) {
                throw new RuntimeException("Failed to access SheetTextAsHTML footers.", e);
            }
        }

    }


}
