package com.cla.common.media.files.word;

import com.cla.common.domain.dto.histogram.ClaLongSuperCollectionArray;
import com.cla.common.domain.dto.mediaproc.word.WordFile;
import com.cla.common.domain.dto.messages.IngestTaskParameters;
import com.cla.common.domain.dto.messages.WordIngestResult;
import com.cla.common.media.ingest.FileIngestService;
import com.cla.connector.domain.dto.error.ProcessingErrorType;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.file.FileType;
import com.google.common.base.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.InputStream;

@Component
public class WordIngesterService extends FileIngestService<WordIngestResult> {

    @Value("${ingester.word.convert-array-histograms:true}")
    private boolean useArrayHistograms;

    @Autowired
    private ClaWordExtractor wordExtractor;

    @Override
    public WordIngestResult extractViaRequestMessage(
            IngestTaskParameters ingestTaskParameters, InputStream is, String path, boolean extractMetadata,
            ClaFilePropertiesDto claFileProperties) {

        return extract(is, path, extractMetadata, claFileProperties);
    }

    @Override
    protected FileType getBaseFileType() {
        return FileType.WORD;
    }

    @Override
    public WordIngestResult extract(InputStream is, String path, boolean extractMetadata, ClaFilePropertiesDto claFileProperties) {
        WordFile wordFile = wordExtractor.extract(is, path, extractMetadata, claFileProperties);
        return convertIngestionResult(wordFile, claFileProperties, useArrayHistograms);
    }

    @Override
    protected WordIngestResult createIngestPayload(ProcessingErrorType processingErrorType, ClaFilePropertiesDto claFilePropertiesDto, final String text, final Exception e) {
        WordIngestResult result = new WordIngestResult();
        result.setErrorType(processingErrorType);
        result.setClaFileProperties(claFilePropertiesDto);
        result.setExceptionText(e == null ? text : e.getMessage());
        return result;
    }

    /**
     * Convert WordFile object produced by the ingester to WordIngestResult
     *
     * @param wordFile object produced by the ingester
     * @return json serializable result
     */
    public static WordIngestResult convertIngestionResult(final WordFile wordFile, final ClaFilePropertiesDto claFileProperties,
                                                          final boolean useArrayHistograms) {

        WordIngestResult result = new WordIngestResult();
        result.setCsvValue(wordFile.getCsvString());
        result.setMetadata(wordFile.getMetaData());
        result.setErrorType(wordFile.getErrorType());
        result.setExceptionText(wordFile.getExceptionText());
        result.setCombinedProposedTitles(wordFile.getProposedTitlesCombined());
        result.setFileName(wordFile.getFileName());
        result.setLongHistograms(useArrayHistograms ?
                ClaLongSuperCollectionArray.create(wordFile.getLongHistograms()) :
                wordFile.getLongHistograms());
        result.setOriginalContent(wordFile.getOrigContent());
        String content = "";
        if (!Strings.isNullOrEmpty(wordFile.getHeader())) {
            content += wordFile.getHeader() + "\n";
        }
        content += wordFile.getBody();
        if (!Strings.isNullOrEmpty(wordFile.getFooter())) {
            content += "\n" + wordFile.getFooter();
        }
        result.setContent(content);
        result.setScanned(wordFile.isScannedDoc());
        result.setClaFileProperties(claFileProperties);
        return result;
    }
}
