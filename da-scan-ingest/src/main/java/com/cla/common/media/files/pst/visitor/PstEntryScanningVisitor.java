package com.cla.common.media.files.pst.visitor;

import com.cla.connector.domain.dto.messages.ScanErrorDto;
import com.cla.common.domain.dto.file.PstEntryPropertiesDto;
import com.cla.common.media.files.pst.model.PstAttachment;
import com.cla.common.media.files.pst.model.PstEmailMessage;
import com.cla.common.media.files.pst.model.PstFolder;

import java.util.function.Consumer;
import java.util.function.Predicate;

/**
 * Created By Itai Marko
 */
public class PstEntryScanningVisitor implements PstEntryVisitor<Void> {

    private final Consumer<? super PstEntryPropertiesDto> mediaItemPropsConsumer;
    private final Predicate<? super String> fileTypesPredicate;

    public PstEntryScanningVisitor(Consumer<? super PstEntryPropertiesDto> mediaItemPropsConsumer,
                                   Predicate<? super String> fileTypesPredicate) {

        this.mediaItemPropsConsumer = mediaItemPropsConsumer;
        this.fileTypesPredicate = fileTypesPredicate;
    }

    @Override
    public Void visitFolder(PstFolder pstFolder) {
        PstEntryPropertiesDto payload = PstDtoConversionUtils.dtoForPstFolder(pstFolder);
        mediaItemPropsConsumer.accept(payload);
        return null;
    }

    @Override
    public Void visitFolderFailed(PstFolder pstFolder, ScanErrorDto scanError) {
        PstEntryPropertiesDto payload = PstDtoConversionUtils.dtoForPstFolder(pstFolder);
        payload.addError(scanError);
        mediaItemPropsConsumer.accept(payload);
        return null;
    }

    @Override
    public Void visitEmailMessage(PstEmailMessage pstEmailMessage) {
        if (!fileTypesPredicate.test(pstEmailMessage.getPstPath().toString())) {
            return null;
        }
        PstEntryPropertiesDto payload = PstDtoConversionUtils.dtoForPstEmailMessage(pstEmailMessage);
        mediaItemPropsConsumer.accept(payload);
        return null;
    }

    @Override
    public Void visitAttachment(PstAttachment pstAttachment) {
        String pstPathStr = pstAttachment.getPstPath().toString();
        if (pstPathStr.endsWith(".zip") || !fileTypesPredicate.test(pstPathStr)) {
            return null; // TODO Itai: [DAMAIN-3075] [DAMAIN-3700] This is just for now. Need to support zip attachments
        }
        PstEntryPropertiesDto payload = PstDtoConversionUtils.dtoForPstAttachment(pstAttachment);
        mediaItemPropsConsumer.accept(payload);
        return null;
    }
}
