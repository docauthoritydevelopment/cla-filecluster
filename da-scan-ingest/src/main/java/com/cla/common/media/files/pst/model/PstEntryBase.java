package com.cla.common.media.files.pst.model;


import com.cla.common.domain.pst.PstPath;
import com.cla.common.media.files.pst.visitor.PstEntryVisitor;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;

/**
 * Created By Itai Marko
 */
public abstract class PstEntryBase implements PstEntry {

    private final PstFile pstFile;
    private final PstPath pstPath;

    PstEntryBase(PstFile pstFile, PstPath pstPath) {
        this.pstFile = pstFile;
        this.pstPath = pstPath;
    }

    @Override
    public PstPath getPstPath() {
        return pstPath;
    }

    @Override
    public ClaFilePropertiesDto getPstFileProps() {
        return pstFile.getPstFileProps();
    }

    @Override
    public <T> void acceptAndDescend(PstEntryVisitor<T> visitor) {
        accept(visitor);
        descend(visitor);
    }

    abstract <T> void descend(PstEntryVisitor<T> visitor);

    PstFile getPstFile() {
        return pstFile;
    }
}
