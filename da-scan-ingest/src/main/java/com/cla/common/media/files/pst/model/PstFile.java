package com.cla.common.media.files.pst.model;

import com.cla.common.domain.pst.PstPath;
import com.cla.common.media.files.pst.visitor.PstEntryVisitor;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.media.mail.MailItemType;
import com.pff.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.Objects;

/**
 * Created By Itai Marko
 */
class PstFile implements AutoCloseable {

    private static final Logger logger = LoggerFactory.getLogger(PstFile.class);

    private boolean closed = false;
    private final PSTFile libpstFile;
    private final ClaFilePropertiesDto pstFileProps;

    PstFile(ClaFilePropertiesDto pstFileProps) throws PSTException, IOException {
        this.pstFileProps = Objects.requireNonNull(pstFileProps);
        Path pstFilePath = pstFileProps.getFilePath();
        this.libpstFile = new PSTFile(pstFilePath.toFile());
    }

    PstEntry retrieveEntry(PstPath entryPath, boolean mimeMode) throws PSTException, IOException {
        validatePstFileOpened();
        long pstEntryId = entryPath.getEntryId().orElseThrow(() ->
                new IllegalArgumentException("PST entry retrieval attempt with non unique PstPath: " + entryPath));
        switch (entryPath.getEntryType()) {
            case ATTACHMENT:
                PstPath parentEmailPath = entryPath.getParent();
                logger.trace("Retrieving parent PST entry of {}. Parent path is: {}", entryPath, parentEmailPath);
                PstEntry pstParentEntry = retrieveEntry(parentEmailPath, mimeMode);
                MailItemType parentEntryType = parentEmailPath.getEntryType();
                if (parentEntryType != MailItemType.EMAIL) {
                    throw new RuntimeException("Unexpected MailItemType: " + parentEntryType + " of PST parent entry: " + parentEmailPath.toString() + " of attachment entry: " + entryPath);
                }
                PstEmailMessage pstParentEmail = (PstEmailMessage) pstParentEntry;
                return pstParentEmail.createChildPstAttachment(Math.toIntExact(pstEntryId));
            case EMAIL:
            case FOLDER:
                PSTObject pstObject = PSTObject.detectAndLoadPSTObject(libpstFile, pstEntryId);
                return pstEntryForLibpstObject(pstObject, entryPath, mimeMode);
            default:
                throw new UnsupportedOperationException("Retrieving entry of type " +entryPath.getEntryType() + " is not yet supported");
        }
    }

    private PstEntry pstEntryForLibpstObject(PSTObject pstObject, PstPath entryPath, boolean mimeMode) {

        if (pstObject instanceof PSTMessage) {
            if (mimeMode) {
                return new PstEmailMimeMessage((PSTMessage) pstObject, this, entryPath);
            } else {
                return new PstEmailMessage((PSTMessage) pstObject, this, entryPath);
            }
        }
        if (pstObject instanceof PSTFolder) {
            return new PstFolder((PSTFolder) pstObject, this, entryPath);
        }

        logger.warn("PstObject type {} not yet supported", pstObject.getClass());
        return DummyPstEntry.INSTANCE;
    }

    PstEntry createChildPstEntry(PSTObject pstChild, PstPath parentEntryPath) {
        validatePstFileOpened();
        if (pstChild instanceof PSTMessage) {
            return createChildPstEmailMessage((PSTMessage) pstChild, parentEntryPath);
        }
        if (pstChild instanceof PSTFolder) {
            return createChildPstFolder((PSTFolder) pstChild, parentEntryPath);
        }

        logger.warn("PstObject type {} not yet supported", pstChild.getClass());
        return DummyPstEntry.INSTANCE;
    }

    PstFolder createChildPstFolder(PSTFolder libpstFolder, PstPath parentEntryPath) {
        PstPath pstFolderPath = parentEntryPath.resolveFolder(libpstFolder.getDisplayName());
        return new PstFolder(libpstFolder, this, pstFolderPath);
    }

    private PstEmailMessage createChildPstEmailMessage(PSTMessage libpstEmailMessage, PstPath parentEntryPath) {
        String emailMsgName = libpstEmailMessage.getSubject().trim();
        long emailMsgId = libpstEmailMessage.getDescriptorNodeId();
        PstPath pstEmailPath = parentEntryPath.resolveUniqueEntry(emailMsgName, emailMsgId, MailItemType.EMAIL, null);
        return new PstEmailMessage(libpstEmailMessage, this, pstEmailPath);
    }

    PstAttachment createChildPstAttachment(PSTAttachment libpstAttachment, PstPath pstAttachmentPath, PstEmailMessage emailMsg) {
        // preload the attachment to memory to avoid using its InputStream after this file was closed
        return new PreLoadedPstAttachment(libpstAttachment, this, pstAttachmentPath, emailMsg);
    }

    ClaFilePropertiesDto getPstFileProps() {
        return pstFileProps;
    }

    String getName() throws PSTException, IOException {
        validatePstFileOpened();
        return libpstFile.getMessageStore().getDisplayName();
    }

    String getFileName() {
        return pstFileProps.getFileName();
    }

    /**
     * Don't get confused, this is just the folder at the root of the pst file.
     * It is irrelevant to our notion of root folder.
     */
    PstFolder getPstRootFolder() throws PSTException, IOException {
        validatePstFileOpened();
        PSTFolder libpstRootFolder = libpstFile.getRootFolder();
        PstPath pstRootFolderPath = new PstPath(pstFileProps.getFilePath());
        return new PstFolder(libpstRootFolder, this, pstRootFolderPath);
    }

    // Should be used in the beginning of any non private method that depend on the file to be open
    private void validatePstFileOpened() {
        if (closed) {
            throw new IllegalStateException("PstFile is closed. Filename: " + pstFileProps.getFileName());
        }
    }

    /**
     * An empty impl (just returns the given InputStream) to allow for an overriding class to handle InputStreams that
     * are mapped to the underlying {@link java.io.RandomAccessFile}.
     */
    InputStream registeredInputStream(InputStream in) {
        return in;
    }

    @Override
    public void close() {
        if (!closed) {
            closed = true;
            try {
                libpstFile.close();
            } catch (IOException e) {
                logger.error("Failed to close underlying PSTFile:", e);
            }
        }
    }

    @Override
    public String toString() {
        return "PstFile{" +
                "closed=" + closed +
                ", libpstFile=" + libpstFile +
                ", pstFileProps=" + pstFileProps +
                '}';
    }

    /**
     * Used as a place holder for entries that are not yet supported
     */
    private final static class DummyPstEntry implements PstEntry {

        private static final DummyPstEntry INSTANCE = new DummyPstEntry();

        @Override
        public long getPstEntryId() {
            throw new UnsupportedOperationException();
        }

        @Override
        public PstPath getPstPath() {
            throw new UnsupportedOperationException();
        }

        @Override
        public ClaFilePropertiesDto getPstFileProps() {
            throw new UnsupportedOperationException();
        }

        @Override
        public <T> T accept(PstEntryVisitor<T> visitor) {
            return null;
        }

        @Override
        public <T> void acceptAndDescend(PstEntryVisitor<T> visitor) {

        }
    }
}
