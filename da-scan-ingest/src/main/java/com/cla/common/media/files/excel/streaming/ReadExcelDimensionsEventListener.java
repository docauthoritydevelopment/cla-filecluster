package com.cla.common.media.files.excel.streaming;

import com.cla.common.domain.dto.excel.ExcelSheetDimensions;
import com.cla.common.media.files.excel.ExcelFileUtils;
import org.apache.poi.hssf.eventusermodel.HSSFListener;
import org.apache.poi.hssf.record.*;
import org.apache.poi.ss.usermodel.Cell;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by uri on 18/10/2015.
 */
public class ReadExcelDimensionsEventListener implements HSSFListener {

    Logger logger = LoggerFactory.getLogger(ReadExcelDimensionsEventListener.class);

    private final Map<Integer, ExcelSheetDimensions> excelSheetDimensionsMap = new HashMap<>();

    private int numberOfCells = 0;

    private int sheetIndex = -1; //We increase the sheet index first.

    private ExcelFileUtils.ExcelIssuesCounters excelIssuesCounters = new ExcelFileUtils.ExcelIssuesCounters();
    private ExcelSheetDimensions currentExcelSheetDimensions;

    private int lastEncounteredRow;

    private Map<Short, Integer> recordTypeHistogram = new HashMap<>();

    int expectedEOFCounter = 0;

    private List<Record> recordList;
    public ReadExcelDimensionsEventListener() {
        recordList = new LinkedList<>();
    }

    public int getNumberOfCells() {
        return numberOfCells;
    }

    @Override
    public void processRecord(Record record) {
        switch (record.getSid()) {
            // the BOFRecord can represent either the beginning of a sheet or the workbook
            case BOFRecord.sid:
                BOFRecord bof = (BOFRecord) record;
                handleBofRecord(bof);
                break;
            case BoundSheetRecord.sid:
                handleSheetRecord((BoundSheetRecord) record);
                break;
            case RowRecord.sid:
                break;
            case NumberRecord.sid:
                handleCellRecord((NumberRecord) record);
                break;
            case LabelSSTRecord.sid:
                handleCellRecord((LabelSSTRecord) record);
                break;
            case FormulaRecord.sid:
                handleCellRecord((FormulaRecord) record);
                break;
            case RKRecord.sid:
                handleCellRecord((RKRecord) record);
                break;
            case BoolErrRecord.sid:
                handleCellRecord((BoolErrRecord) record);
                break;
            case EOFRecord.sid:
                handleEndOfFileRecord((EOFRecord) record);
            default:
//                logger.debug("Got a record by type: {}",record.getClass().getName());
                break;
        }
        //Decide if to add to record list
        if (record instanceof CellRecord) {
            return;
        }
        if (record.getSid() == StringRecord.sid) {
            return;
        }
//        if (record.getSid() == CRNRecord.sid) {
//            return;
//        }
        recordList.add(record);
        int count = recordTypeHistogram.getOrDefault(record.getSid(),0) + 1;
        recordTypeHistogram.put(record.getSid(),count);
    }



    public List<Record> getRecordList() {
        return recordList;
    }

    private void handleCellRecord(CellRecord record) {
        if (currentExcelSheetDimensions == null) {
            logger.debug("Found a CellRecord before any sheet records were found "+record.toString());
            return;
        }
        int cellValType = ExcelFileUtils.determineType(record);
        if (cellValType == Cell.CELL_TYPE_BLANK || cellValType == Cell.CELL_TYPE_ERROR) {
            return;
        }

        currentExcelSheetDimensions.updateColumn(record.getColumn());
        currentExcelSheetDimensions.updateRow(record.getRow());

        if (record.getRow() != lastEncounteredRow) {
            currentExcelSheetDimensions.addNonBlankRow();
            lastEncounteredRow = record.getRow();
        }
        currentExcelSheetDimensions.addNonBlankCell(record.getRow());
    }

    private void handleBofRecord(BOFRecord bof) {
        if (bof.getType() == bof.TYPE_WORKSHEET) {
            sheetIndex++;
            logger.trace("Encountered sheet");
            currentExcelSheetDimensions = new ExcelSheetDimensions();
            excelSheetDimensionsMap.put(sheetIndex, currentExcelSheetDimensions);
            lastEncounteredRow = -1;
            expectedEOFCounter = 1;
//            logger.debug("Encountered sheet index {}",sheetIndex);
        }
        else if (bof.getType() == BOFRecord.TYPE_CHART ||
            bof.getType() == BOFRecord.TYPE_EXCEL_4_MACRO) {
            if (expectedEOFCounter >0) {
                logger.debug("Found internal chart - this is not a sheet");
            }
            else {
                //Not typical sheets, but sheets non the less
                sheetIndex++;
                logger.debug("Encountered chart/macro sheet");
                currentExcelSheetDimensions = new ExcelSheetDimensions();
                excelSheetDimensionsMap.put(sheetIndex, currentExcelSheetDimensions);
                lastEncounteredRow = -1;
            }
            expectedEOFCounter++;
        }
    }

    /**
     * A sheet should be bounded by an EOF record
     * @param record
     */
    private void handleEndOfFileRecord(EOFRecord record) {
        if (expectedEOFCounter >0) {
            expectedEOFCounter--;
        }
    }

    private void handleSheetRecord(BoundSheetRecord bsr) {
        logger.trace("New sheet {} named: {}", sheetIndex, bsr.getSheetname());
    }

    public Map<Integer, ExcelSheetDimensions> getExcelSheetDimensionsMap() {
        return excelSheetDimensionsMap;
    }
}
