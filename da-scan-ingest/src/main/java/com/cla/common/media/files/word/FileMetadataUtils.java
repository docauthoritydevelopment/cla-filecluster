package com.cla.common.media.files.word;

import com.cla.common.domain.dto.ClaDocProperties;
import com.cla.common.domain.dto.FileMetadata;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Constructor;
import java.util.Date;


public class FileMetadataUtils {
	private static final Logger logger = LoggerFactory.getLogger(FileMetadataUtils.class);

	public static <T extends FileMetadata> T extractMetadata(Class<T> mdClass, ClaDocProperties fileProps, ClaFilePropertiesDto cfp) {

		T claMetadata = null;
		try {
			Constructor<T> constructor = mdClass.getConstructor(ClaDocProperties.class);
			claMetadata = constructor.newInstance(fileProps);
			if (cfp != null) {
				claMetadata.setFsFileSize(cfp.getFileSize());
				claMetadata.setOwner(cfp.getOwnerName());
				claMetadata.setFsLastModified(new Date(cfp.getModTimeMilli()));
				claMetadata.setAclReadAllowed(cfp.getAclReadAllowed());
				claMetadata.setAclWriteAllowed(cfp.getAclWriteAllowed());
				claMetadata.setAclReadDenied(cfp.getAclReadDenied());
				claMetadata.setAclWriteDenied(cfp.getAclWriteDenied());
			}
		}
		catch (Exception e) {
			logger.error("Failed to create file metadata.", e);
		}
		return claMetadata;
	}


}
