package com.cla.common.media.files.excel;

import com.cla.common.allocation.IngestionResourceManager;
import com.cla.common.domain.dto.ClaDocProperties;
import com.cla.common.domain.dto.FileMediaType;
import com.cla.common.domain.dto.mediaproc.excel.ExcelCell;
import com.cla.common.domain.dto.mediaproc.excel.ExcelParseParams;
import com.cla.common.domain.dto.mediaproc.excel.ExcelWorkbook;
import com.cla.common.domain.dto.mediaproc.excel.ExcelWorkbookMetaData;
import com.cla.common.media.files.word.FileMetadataUtils;
import com.cla.common.utils.tokens.DictionaryHashedTokensConsumerImpl;
import com.cla.common.utils.tokens.HashedTokensConsumer;
import com.cla.common.utils.tokens.NGramTokenizerHashed;
import com.cla.connector.domain.dto.error.ProcessingErrorType;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.hssf.OldExcelFormatException;
import org.apache.poi.hssf.usermodel.HSSFEvaluationWorkbook;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.util.RecordFormatException;
import org.apache.poi.xssf.usermodel.XSSFEvaluationWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.InputStream;
import java.util.Set;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toSet;

@Component
public class ExcelFilesServiceImpl implements ExcelFilesService {

    private static final Logger logger = LoggerFactory.getLogger(ExcelFilesServiceImpl.class);

    private int logNumerator = 1;

    @Value("${tokenMatcherActive}")
    private boolean isTokenMatcherActive;

    @Value("${ingester.patternSearchActive}")
    private boolean patternSearchActive;

    @Value("${ingester.excel.use-formatted-values:false}")
    protected boolean useFormattedPatterns;

    @Value("${ingester.maxExcelFileSize:0}")
    private long maxExcelFileSize;

    @Value("${ingester.excel.useTableHeadingsAsTitles:true}")
    private boolean useTableHeadingsAsTitles;

    private Set<String> ignoredSheetNamePrefixesAsTitlesSet = null;

    @Value("${ingester.excel.ignoredSheetNamePrefixesAsTitles:sheet,\u05D2\u05DC\u05D9\u05D5\u05DF}")
    private String[] ignoredSheetNamePrefixesAsTitles;

    @Autowired
    private NGramTokenizerHashed nGramTokenizerHashed;

    @Autowired
    private IngestionResourceManager ingestionResourceManager;

    private boolean resourcesAcquired = false;

    @Value("${ingester.excelWorkbooksMemoryAllocationMB:900}")
    private int excelWorkbooksMemoryAllocationMB;

    @Value("${ingester.excel.singleTokenNumberedTitleRateReduction:3}")
    private int singleTokenNumberedTitleRateReduction;

    @Value("${tokenMatcher.excel.CellMinStringLength:3}")
    private int tokenMatcherExcelCellMinStringLength;

    @Value("${tokenMatcher.excel.CellMinNumberLength:4}")
    private int tokenMatcherExcelCellMinNumberLength;

    @Value("${tokenMatcher.excel.IgnoreComplexNumbers:true}")
    private boolean tokenMatcherExcelIgnoreComplexNumbers;

    @Override
    public boolean isPatternSearchActive() {
        return patternSearchActive;
    }

    @Override
    public void setPatternSearchActive(final boolean patternSearchActive) {
        this.patternSearchActive = patternSearchActive;
    }

    @Override
    public ExcelWorkbook ingestWorkbook(final String p, final InputStream is, final boolean extractMetadata,final ClaFilePropertiesDto claFileProperties) {
        return ingestWorkbook(is, p, null, extractMetadata, claFileProperties);
    }

    @PostConstruct
    public void init() {
        if (ignoredSheetNamePrefixesAsTitlesSet == null && ignoredSheetNamePrefixesAsTitles.length > 0) {
            ignoredSheetNamePrefixesAsTitlesSet = Stream.of(ignoredSheetNamePrefixesAsTitles).map(String::toLowerCase).collect(toSet());
        }
        ExcelParseParams.setNGramTokenizer(nGramTokenizerHashed);
        ExcelParseParams.setTokenMatchedParameters(tokenMatcherExcelCellMinStringLength,
                tokenMatcherExcelCellMinNumberLength,
                tokenMatcherExcelIgnoreComplexNumbers);
    }

    @Override
    public ExcelWorkbook ingestWorkbook(String fileName, DictionaryHashedTokensConsumerImpl hashedTokensConsumer, InputStream is, boolean extractMetadata){
        return ingestWorkbook(is, fileName, hashedTokensConsumer, extractMetadata, null);
    }

    @SuppressWarnings("static-access")
    private ExcelWorkbook ingestWorkbook(final InputStream is,
                                         final String path,
                                         final HashedTokensConsumer hashedTokensConsumer,
                                         final boolean extractMetadata,
                                         final ClaFilePropertiesDto claFileProperties) {
        ExcelFileUtils.ExcelIssuesCounters excelIssuesCounters = new ExcelFileUtils.ExcelIssuesCounters();
        logger.trace("Starting to ingest {}", path);
        ExcelParseParams excelParseParams =
                new ExcelParseParams(useTableHeadingsAsTitles,
                        singleTokenNumberedTitleRateReduction,
                        ignoredSheetNamePrefixesAsTitlesSet,
                        patternSearchActive, useFormattedPatterns);
        final ExcelWorkbook excelWorkbook = new ExcelWorkbook(excelParseParams);
        Workbook workbook = null;
        try {

//			Map<String, ExcelCell> cellValues = new LinkedHashMap<>();
//			Map<String, Integer> valuesCounts = new LinkedHashMap<>();

            excelWorkbook.setFileName(path);
            excelWorkbook.setHashedTokensConsumer(hashedTokensConsumer);
            POIFSFileSystem fs = null;

            if (ExcelFileUtils.checkMaxExcelFileSizeLimit(maxExcelFileSize,path, excelWorkbook, claFileProperties)) {
                return excelWorkbook;
            }

            final FileMediaType fileMediaType = ExcelFileUtils.mediaNameDetector(path);

            // Acquire resource token - Limit number of concurrently opened workbooks to avoid heap overflow when many large files are encountered
            if (excelWorkbooksMemoryAllocationMB > 0) {
                resourcesAcquired = ingestionResourceManager.acquire(excelWorkbooksMemoryAllocationMB);
                if (logger.isTraceEnabled()) {
                    logger.trace("resource acquired {}. {} available", excelWorkbooksMemoryAllocationMB, ingestionResourceManager.getAvailable());
                }
            }

            // Used for the formula parsing and re-rendering as a relative formula
            Object evaluationWorkbookWrapper;
            if (fileMediaType == FileMediaType.EXCELXML) {
                workbook = new XSSFWorkbook(is);
                evaluationWorkbookWrapper =
                        XSSFEvaluationWorkbook.create((XSSFWorkbook) workbook);
            } else if (fileMediaType == FileMediaType.EXCEL) {
                try {
                    fs = new POIFSFileSystem(is);
                    workbook = new HSSFWorkbook(fs);
                    evaluationWorkbookWrapper =
                            HSSFEvaluationWorkbook.create((HSSFWorkbook) workbook);
                } catch (final OldExcelFormatException e) {
                    // Excel 5.0/7.0 (BIFF5) format
                    logger.warn("Old file-format (Excel 5.0/7.0) file: {}. Error: {}", path, e.getMessage());
                    setErrorData(excelWorkbook, ProcessingErrorType.ERR_OLD_FORMAT, e);
                    return excelWorkbook;
                } catch (final RecordFormatException e) {
                    logger.warn("Can not open file {}.", path, e);
                    setErrorData(excelWorkbook, ProcessingErrorType.ERR_CORRUPTED, e);
                    return excelWorkbook;
                } catch (final EncryptedDocumentException e) {
                    logger.warn("File seems to be encrypted. Can not open {}. Exception: {}", path, e);
                    setErrorData(excelWorkbook, ProcessingErrorType.ERR_ENCRYPTED, e);
                    return excelWorkbook;
                } catch (final IOException e) {
                    logger.warn("Can not open file {}. IOException {}. {}", path, e.getMessage(), e);
                    setErrorData(excelWorkbook, ProcessingErrorType.ERR_IO, e);
                    return excelWorkbook;
                } catch (final Exception e) {
                    if (e.getMessage().contains("bytes remaining still to be read")) {
                        logger.warn("Can not open file {}. Exception: {}", path, e);
                        setErrorData(excelWorkbook, ProcessingErrorType.ERR_CORRUPTED, e);
                    } else {
                        logger.warn("Can not open file {}. Unexpected exception: {}", path, e);
                        if (excelWorkbook.calculateProcessErrorType() == ProcessingErrorType.ERR_NONE) {
                            setErrorData(excelWorkbook, ProcessingErrorType.ERR_UNKNOWN, e);
                        }
                    }
                    return excelWorkbook;
                }
            } else {
                logger.warn("Bad nameOrMime value: {}", path);
                setErrorData(excelWorkbook, ProcessingErrorType.ERR_WRONG_EXT, null);
                return excelWorkbook;
            }

            if (extractMetadata) {
                extractMetaData(path, excelWorkbook, workbook, fs, claFileProperties);
            }


            for (int i = 0; i < workbook.getNumberOfSheets(); i++) {
                final Sheet sheet = workbook.getSheetAt(i);
                final int sheetIndex = workbook.getSheetIndex(sheet); // sheet index
                excelWorkbook.processSheetName(sheet.getSheetName());
                final int nonBlankRowsCount = getNonBlankRowsCount(sheet);
                logger.debug("Sheet index {} has {} nonBlankRows",sheetIndex,nonBlankRowsCount);
                int rowsCount = 0;
                for (final Row row : sheet) {
                    final int nonBlankCellsCount = getNonBlankCellsCount(row);
                    int cellsCount = 0;
                    for (final Cell cell : row) {
                        ExcelCell cellValue = ExcelFileUtils.createExcelCell(excelIssuesCounters, workbook, evaluationWorkbookWrapper, sheetIndex, cell);
                        if (cellValue == null) {
                            continue;
                        }

                        boolean detailedAnalysis = ExcelFileUtils.isDetailedAnalysis(excelIssuesCounters, excelWorkbook, nonBlankRowsCount, rowsCount, nonBlankCellsCount, cellsCount);
                        /*if (!detailedAnalysis){
                            logger.debug("skip center of sheet {} {} {} {} {}", sheetIndex, cellsCount, rowsCount, nonBlankCellsCount, nonBlankRowsCount);
						}*/
                        String cellRefStr = calculateCellReferenceString(sheet.getSheetName(), row.getRowNum(), cell);

                        excelWorkbook.addExcelCell("" + i /*sheet.getSheetName()*/,
                                cell.getColumnIndex(), row.getRowNum(),
                                cellValue,
                                sheet.getSheetName(),
                                detailedAnalysis,
                                cellRefStr,
                                isTokenMatcherActive);

                        if (cell.getCellTypeEnum() == CellType.BLANK) {
                            excelIssuesCounters.addBlankCell();
                        }
                        else
                        if (cell.getCellTypeEnum() == CellType.ERROR) {
                            excelIssuesCounters.addErrorCell();
                        }
                        else{
                            cellsCount++;    // increment non-empty cells counter
                        }
                    }    // for cellIterator

                    if (nonBlankCellsCount > 0) {
                        rowsCount++;    // increment counter for non empty rows
                    }
                }    // for rowIterator
            }    // crawl on sheets
            excelWorkbook.processStyle();

            // create metadata statistics
            ExcelMetadataUtils.summarizeWorkbookMetadata(excelWorkbook);

            if (excelIssuesCounters.getIngestionErrors() > 0) {
                logger.debug("Ingestion errors ({}) for {}", excelIssuesCounters.getIngestionErrors(), path);
            }
            if (excelIssuesCounters.getExtraSkippedCells() > 0) {
                logger.debug("Mid-range extra skipped cells {} for {}", excelIssuesCounters.getExtraSkippedCells(), path);
                logger.debug("Number of blank cells: {}. Number of error cells: {}",excelIssuesCounters.getBlankCells(),excelIssuesCounters.getErrorCells());
//                excelIssuesCounters.printAdditionalInformation();
            }
            logger.trace("Finished ingesting {}", path);

//			if (logger.isDebugEnabled()) {
//				if (logNumerator==1) {
//					EFDLogger.trace("{},{},{},{}",
//							"seq", "timeStamp", 
//							"fileName", 
//							ExcelWorkbook.toCsvStringTitle(","));
//				}
//				EFDLogger.trace("{},{},\"{}\",{}",
//						logNumerator++, System.currentTimeMillis(), 
//						path, 
//						excelWorkbook.toCsvString("\n,,,"));
//			}

			/*
			 * Show memory footprint. 
			 * Requires integration with https://github.com/DimitrisAndreou/memory-measurer.
			 * Need to add the following to the VM parameters list:
			 * 			 -javaagent:D:\Work\Ext-projects\memory-measurer\dist\object-explorer.jar
			 * /
			logger.debug("Workbook memory({})={}, {}", path, MemoryMeasurer.measureBytes(workbook), MemoryMeasurer.measureBytes(excelWorkbook));
			/ *****/
            return excelWorkbook;
        }
        catch (final IOException e) {
            logger.warn("Ingestion failure for file {}. Exception: {}", path, e.getMessage(), e);
            setErrorData(excelWorkbook, ProcessingErrorType.ERR_IO, e);
        }
        catch (final Exception e) {
            logger.warn("Failed to read file {}. Error is {}.", path, e.getMessage(), e);
            setErrorData(excelWorkbook, ProcessingErrorType.ERR_UNKNOWN, e);
        }
        finally {
            if (workbook != null) {
                try {
                    workbook.close();
                } catch (final IOException e) {
                    // ignore exceptions at this point
                }
            }
            if (resourcesAcquired) {
                ingestionResourceManager.release(excelWorkbooksMemoryAllocationMB);
                logger.debug("workbookSemaphore released. {} available", ingestionResourceManager.getAvailable());
            }
        }
        return excelWorkbook;
    }

    private void setErrorData(ExcelWorkbook excelWorkbook, ProcessingErrorType type, Exception e) {
        excelWorkbook.setErrorType(type);
        excelWorkbook.setExceptionText(e == null ? null : e.getMessage());
    }

    private String calculateCellReferenceString(String sheetName, int rowNumber, Cell cell) {
        String cellRefStr = null;
        try {
            final CellReference celRef = new CellReference(cell);
            cellRefStr = sheetName + ":" + celRef.formatAsString();
        }
        catch (final Exception e) {
            // ignore exceptions at this point
        }

        if (cellRefStr == null) {
            cellRefStr = sheetName + ":" + cell.getColumnIndex() + "/" + rowNumber;
        }
        return cellRefStr;
    }

    private void extractMetaData(String path, ExcelWorkbook excelWorkbook, Workbook workbook, POIFSFileSystem fs, ClaFilePropertiesDto cfp) {
        final ClaDocProperties fileProps = ExcelMetadataUtils.extractProperties(workbook, fs, path);
        final ExcelWorkbookMetaData claMetadata = FileMetadataUtils.extractMetadata(ExcelWorkbookMetaData.class, fileProps, cfp);
        excelWorkbook.setMetadata(claMetadata);

        if (logger.isTraceEnabled()) {
            logger.trace("MetaData = {}", claMetadata.toCsvString());
        }
    }


    /*
     * Return the number of non blank rows.
     */
    private int getNonBlankRowsCount(final Sheet sheet) {
        int nonEmptyRowsCount = 0;
        for (final Row row : sheet) {
            for (final Cell cell : row) {
                if (cellTypeNotEmpty(cell.getCellTypeEnum())) {
                    nonEmptyRowsCount++;
                    break;
                }
            }
        }
        return nonEmptyRowsCount;
    }

    /*
     * Return the number of non blank cells in a row.
     */
    private int getNonBlankCellsCount(final Row row) {
        int rowCellsCount = 0;
        for (Cell cell : row) {
            if (cellTypeNotEmpty(cell.getCellTypeEnum())){
                rowCellsCount++;
            }
        }
        return rowCellsCount;
    }

    private boolean cellTypeNotEmpty(CellType cellType){
        return cellType != CellType.BLANK &&
                cellType != CellType.ERROR;
    }


    //	@Override
//	public Map<String, ExcelSimilarity> findFolderSimilarities(final String folderName) {
//		try{
//			final Map<String, ExcelSimilarity> similarities = new LinkedHashMap<>();
//			final List<String> fileNames = getExcelFileNamesInFolder(folderName);
//			for (final String first : fileNames) {
//				final ExcelWorkbook excelWorkbookA = ingestWorkbook(new FileInputStream(first), first);
//				if (excelWorkbookA == null) {
//					logger.warn("Null ingestion for file {}",first);
//					continue;
//				}
//				for (final String second : fileNames) {
//					if(first.equals(second))
//						continue;
//					// All files are in the same folder so use basename as a key.
//					final String base1 = Paths.get(first).getFileName().toString();
//					final String base2 = Paths.get(second).getFileName().toString();
//					if(similarities.containsKey(base1+","+base2) || similarities.containsKey(base2+","+base1))
//						continue;
//					final ExcelWorkbook excelWorkbookB = ingestWorkbook(new FileInputStream(second), second);
//					if (excelWorkbookB == null) {
//						logger.warn("Null ingestion for file {}",second);
//						continue;
//					}
//					final ExcelSimilarity similarity = findSimilarity(excelWorkbookA, excelWorkbookB);
//					similarities.put(base1+","+base2, similarity);
//				}
//			}
//			return similarities;
//		}catch(final Exception e){
////			e.printStackTrace();
//			logger.warn("Failed to read file {}. Error is {}",folderName,e);
//			throw new RuntimeException(e);
//		}
//	}

//	@Override
//	public ExcelSimilarityResult[] findExcelSimilaritiesInFSFolder(final String folderName, final Float similarityThreshold) {
//		try{
//			logger.debug("Ingest files and find similarities ...");
//			final Map<String, ExcelSimilarity> similarities = findFolderSimilaritiesInMemory(folderName);			
//			logger.debug("Apply similarity... " + similarities.size() + " matches");
//			final ExcelSimilarityResultsMap excelSimilarityResultsMap = new ExcelSimilarityResultsMap(similarities);
//			logger.debug("Anaylze similarity... " + excelSimilarityResultsMap.getExcelSimilarityResults().size() + " items");
//			final ExcelSimilarityResult[] results = excelSimilarityResultsMap.createSimilarityResults(similarityThreshold);
//			logger.debug("Finished similarity...");
//			return results;
//		}catch(final Exception e){
//			logger.error("Unexpected error: {}", e);
//			throw new RuntimeException(e);
//		}
//	}
//

//	@Override
//	public Map<String, ExcelSimilarity> findFolderSimilaritiesInMemory(final String folderName) {
//		String processedFile = null;
//		try{
//			final Map<String, ExcelWorkbook > books = new LinkedHashMap<>();
//			final Map<String, ExcelSimilarity> similarities = new LinkedHashMap<>();
//			final List<String> fileNames = getExcelFileNamesInFolder(folderName);
//			// ingest all files.
//			for (final String file : fileNames) {
//				processedFile = "Ingest " + file;
//				try {
//					final ExcelWorkbook ew = ingestWorkbook(new FileInputStream(file), file);
//					if (ew == null) {
//						logger.warn("Null ingestion for file " + file);
//						continue;
//					}
//					//logger.debug("Good ingestion for file: " + file);
//					final String basename = Paths.get(file).getFileName().toString();
//					// All files are in the same folder so use basename as a key.
//					books.put(basename, ew);
//				}catch(final Exception e){
//					logger.warn("Exception - " + e.getMessage() + ": " + processedFile);
//				}
//			}
//			// find similarity
//			for (final Map.Entry<String, ExcelWorkbook> first : books.entrySet()) {
//				final ExcelWorkbook excelWorkbookA = first.getValue();
//				processedFile = "Similarity with " + first.getKey();
//				for (final Map.Entry<String, ExcelWorkbook> second : books.entrySet()) {
//					if(first.getKey().equals(second.getKey()))
//						continue;
//					if(similarities.containsKey(first.getKey()+","+second.getKey()) || similarities.containsKey(second.getKey()+","+first.getKey()))
//						continue;
//					processedFile = "Similarity with " + first.getKey() + " and " + second.getKey();
//					final ExcelWorkbook excelWorkbookB = second.getValue();
//					final ExcelSimilarity similarity = findSimilarity(excelWorkbookA, excelWorkbookB);
//					similarities.put(first.getKey()+","+second.getKey(), similarity);
//				}
//			}
//			return similarities;
//		}catch(final Exception e){
//			logger.error("Exception - " + e.getMessage() + ": " + processedFile);
//			throw new RuntimeException(e);
//		}
//	}

//    @Override
//    public List<String> getExcelFileNamesInFolder(final String folderName) {
//        final List<String> results = new ArrayList<String>();
//        final File[] files = new File(folderName).listFiles();
//        for (final File file : files) {
//            if (file.isFile() &&
//                    (file.getName().endsWith(".xlsx") || file.getName().endsWith(".xls")) &&
//                    !file.getName().startsWith("~")) {
//                results.add(file.getAbsolutePath());
//            }
//        }
//        return results;
//    }


    @Override
    public int getLogNumerator() {
        return logNumerator;
    }

    @Override
    public void setLogNumerator(final int logNumerator) {
        this.logNumerator = logNumerator;
    }

}
