package com.cla.common.media.files.excel.streaming;

/**
 * Created by uri on 02/11/2015.
 */
public class SharedFormulaHolder {

    public String formulaString;
    public String si;
    public String ref;
    private int sheetIndex;

    public String getFormulaString() {
        return formulaString;
    }

    public void setFormulaString(String formulaString) {
        this.formulaString = formulaString;
    }

    public String getSi() {
        return si;
    }

    public void setSi(String si) {
        this.si = si;
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public void setSheetIndex(int sheetIndex) {
        this.sheetIndex = sheetIndex;
    }

    public int getSheetIndex() {
        return sheetIndex;
    }

    @Override
    public String toString() {
        return "SharedFormulaHolder{" +
                "formulaString='" + formulaString + '\'' +
                ", si='" + si + '\'' +
                ", ref='" + ref + '\'' +
                ", sheetIndex=" + sheetIndex +
                '}';
    }
}
