package com.cla.common.media.files.pdf;

import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.pdfviewer.PageDrawer;
import org.apache.pdfbox.pdmodel.graphics.color.*;
import org.apache.pdfbox.util.PDFOperator;
import org.apache.pdfbox.util.TextPosition;
import org.apache.pdfbox.util.operator.pagedrawer.*;

import java.awt.*;
import java.awt.color.ColorSpace;
import java.awt.color.ICC_ColorSpace;
import java.awt.geom.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

public class ClaPdfPageDrawer extends PageDrawer {
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(ClaPdfPageDrawer.class);

	private static final double minRectangleDimension = 4.5;	// equal to 7.5 font points
	
	ClaPdfProcessor textProcessor;

	@Override
	protected void processOperator( final PDFOperator operator, final List<COSBase> arguments ) throws IOException
    {
        final String operation = operator.getOperation();
        if (logger.isTraceEnabled()) {
			logger.trace("G.OP> {}: {}", operation,(arguments==null)?"null":arguments.toString());
		}
        try {
			super.processOperator(operator, arguments);
		} catch (final Exception e) {
			if (logger.isTraceEnabled()) {
				logger.warn("Error while processOperator({},{}). {}", operation,(arguments==null)?"null":arguments.toString(), e, e);
			} else {
				logger.warn("Error while processOperator({},{}). {}", operation,(arguments==null)?"null":arguments.toString(), e);
			}
		}
    }
	
	ClaPdfPageDrawer(final ClaPdfProcessor textProcessor) throws IOException {
		super();
		resetEngine();
		this.textProcessor = textProcessor;

        registerOperatorProcessor("b", new CloseFillNonZeroAndStrokePath());
        registerOperatorProcessor("B", new FillNonZeroAndStrokePath());
        registerOperatorProcessor("b*", new CloseFillEvenOddAndStrokePath());
        registerOperatorProcessor("B*", new FillEvenOddAndStrokePath());
        registerOperatorProcessor("BI", new ClaPdfProcessorBI());
        registerOperatorProcessor("c", new CurveTo());
        registerOperatorProcessor("d", new SetLineDashPattern());
        registerOperatorProcessor("Do", new ClaPdfProcessorInvoke(textProcessor.getIngestionResourceManager(), textProcessor.isAcquireImageMemory()));
        registerOperatorProcessor("f", new FillNonZeroRule());
        registerOperatorProcessor("F", new FillNonZeroRule());
        registerOperatorProcessor("f*", new FillEvenOddRule());
        registerOperatorProcessor("h", new ClosePath());
        registerOperatorProcessor("j", new SetLineJoinStyle());
        registerOperatorProcessor("J", new SetLineCapStyle());
        registerOperatorProcessor("l", new LineTo());
        registerOperatorProcessor("m", new MoveTo());
        registerOperatorProcessor("M", new SetLineMiterLimit());
        registerOperatorProcessor("n", new EndPath());
        registerOperatorProcessor("re", new AppendRectangleToPath());
        registerOperatorProcessor("S", new StrokePath());
        registerOperatorProcessor("sh", new SHFill());
        registerOperatorProcessor("v", new CurveToReplicateInitialPoint());
        registerOperatorProcessor("w", new SetLineWidth());
        registerOperatorProcessor("W", new ClipNonZeroRule());
        registerOperatorProcessor("W*", new ClipEvenOddRule());
        registerOperatorProcessor("y", new CurveToReplicateFinalPoint());
		
	}
	
    @Override
	public void strokePath() throws IOException
    {
    	final GeneralPath path = getLinePath();
    	logger.trace("strokePath: {}", dumpPath(path));
    	Color col = null;
        try {
			col = claGetJavaColor(getGraphicsState().getStrokingColor());
		} catch (final Exception e) {
			logger.debug("Error getting color. {}", e);
		}
        if ( col == null )
        {
            col = (Color) getGraphicsState().getStrokingColor().getPaint(pageSize.height);
        }
        if ( col == null )
        {
            logger.trace("ColorSpace {} doesn't provide a stroking color, using white instead!", 
            		getGraphicsState().getStrokingColor().getColorSpace().getName());
            col = Color.WHITE;
        }

    	final Rectangle2D r = path2Rect(path);
    	if (r != null) {
    		textProcessor.addRectangle((Rectangle2D)r, col, false);
    	}
    	else {
    		final Color color = col;
    		final List<Line2D> l = path2Lines(path);
    		l.stream().forEach(li->{
    			if (li.getX1() == li.getX2()) {
    				textProcessor.addLineVertical(li.getX1(), Double.min(li.getY1(), li.getY2()), Double.max(li.getY1(), li.getY2()), color);
    			}
    			else if (li.getY1() == li.getY2()) {
    				textProcessor.addLineHorizontal(Double.min(li.getX1(), li.getX2()), Double.max(li.getX1(), li.getX2()), li.getY1(), color);
    			} 
    		});
    	}
    	getLinePath().reset();
//    	super.strokePath();
    }
    
	@Override
	public void fillPath(final int windingRule) throws IOException
    {
    	final GeneralPath path = getLinePath();
    	logger.trace("fillPath({}): {}", windingRule, dumpPath(path));
    	path.setWindingRule(windingRule);
    	Color col = null;
    	try {
			col = claGetJavaColor(getGraphicsState().getNonStrokingColor());
		} catch (final Exception e) {
			logger.debug("Error getting color. {}", e);
		}
        if ( col == null )
        {
        	col = (Color) getGraphicsState().getNonStrokingColor().getPaint(pageSize.height);
        }
        if ( col == null )
        {
            logger.trace("ColorSpace {} doesn't provide a non-stroking color, using white instead!",
            		getGraphicsState().getNonStrokingColor().getColorSpace().getName());
            col = Color.WHITE;
        }
    	final Rectangle2D r = path2Rect(path);
    	if (r != null) {
    		if (r.getWidth()>minRectangleDimension && r.getHeight()>minRectangleDimension &&
    				!((r.getWidth() <3*minRectangleDimension && r.getWidth() <4*r.getHeight()) ||
    				  (r.getHeight()<3*minRectangleDimension && r.getHeight()<4*r.getWidth() ))) {
    			textProcessor.addRectangle((Rectangle2D)r, col, true);
    		}
    		else if (r.getWidth()>minRectangleDimension && r.getWidth()>r.getHeight()) {
				textProcessor.addLineHorizontal(r.getMinX(), r.getMaxX(), r.getMinY()+r.getHeight()/2d, col);
    		}
    		else if (r.getHeight()>minRectangleDimension && r.getHeight()>r.getWidth()) {
				textProcessor.addLineVertical(r.getMinX()+r.getWidth()/2d, r.getMinY(), r.getMaxY(), col);
    		}
    	}
    	else {
    		final Color color = col;
    		final List<Line2D> l = path2Lines(path);
    		l.stream().forEach(li->{
    			if (li.getX1() == li.getX2()) {
    				textProcessor.addLineVertical(li.getX1(), Double.min(li.getY1(), li.getY2()), Double.max(li.getY1(), li.getY2()), color);
    			}
    			else if (li.getY1() == li.getY2()) {
    				textProcessor.addLineHorizontal(Double.min(li.getX1(), li.getX2()), Double.max(li.getX1(), li.getX2()), li.getY1(), color);
    			} 
    		});
    	}
    	getLinePath().reset();

//    	super.fillPath(windingRule);
    }
    
    @Override
	public void shFill(final COSName shadingName) throws IOException
    {
    	logger.trace("shFill({})", shadingName);
    	// TODO:
    }
    
    @Override
	public void drawImage(final Image awtImage, final AffineTransform at)
    {
    	logger.trace("drawImage({},{})", awtImage.toString(), at.toString());
    	// TODO:
    }
    
    @Override
	protected void processTextPosition( final TextPosition text )
    {
    	// Do nothing
    }

    private List<Line2D> path2Lines(final GeneralPath path) {
    	final List<Line2D> l = new ArrayList<>();
    	final int lastType = -1;
    	double prevX = 0d;
    	double prevY = 0d;
    	double firstX = -1d;
    	double firstY = -1d;
    	for (final PathIterator pi = path.getPathIterator(null); !pi.isDone(); pi.next()) {
    		final double[] coords = new double[6];
    		// The type will be SEG_LINETO, SEG_MOVETO, or SEG_CLOSE
    		// Because the Area is composed of straight lines
    		final int type = pi.currentSegment(coords);
    		if (type == PathIterator.SEG_MOVETO || type == PathIterator.SEG_LINETO) {
    			if (firstX < 0d) {
    				firstX = coords[0];
    				firstY = coords[1];
    			}
    			if (type == PathIterator.SEG_LINETO) {
    				l.add(new Line2D.Double(prevX, prevY, coords[0], coords[1]));
    			}
				prevX = coords[0];
				prevY = coords[1];
    		}
    		else if (type == PathIterator.SEG_CLOSE) {
				l.add(new Line2D.Double(prevX, prevY, firstX, firstY));
    		}
    	}
		return l;
	}

	private Rectangle2D path2Rect(final GeneralPath path) {
    	final List<Point2D> pl = new ArrayList<>();
    	int firstType = -1;
    	int lastType = -1;
    	for (final PathIterator pi = path.getPathIterator(null); !pi.isDone(); pi.next()) {
    		final double[] coords = new double[6];
    		// The type will be SEG_LINETO, SEG_MOVETO, or SEG_CLOSE
    		// Because the Area is composed of straight lines
    		final int type = pi.currentSegment(coords);
    		if (firstType == -1) {
    			firstType = type;
    			pl.add(new Point2D.Double(coords[0],coords[1]));
    		}
    		else if (type == PathIterator.SEG_LINETO) {
    			pl.add(new Point2D.Double(coords[0],coords[1]));
    		}
    		lastType = type;
    	}
    	if (lastType == PathIterator.SEG_CLOSE && pl.size()==4) {	// closed shape with 4 points
    		if ((pl.get(0).getX()==pl.get(1).getX() || pl.get(0).getY()==pl.get(1).getY()) &&
    				(pl.get(1).getX()==pl.get(2).getX() || pl.get(1).getY()==pl.get(2).getY()) &&
    				(pl.get(2).getX()==pl.get(3).getX() || pl.get(2).getY()==pl.get(3).getY()) &&
    				(pl.get(3).getX()==pl.get(0).getX() || pl.get(3).getY()==pl.get(0).getY())) {
    			return new Rectangle2D.Double(Double.min(Double.min(pl.get(0).getX(),pl.get(1).getX()),pl.get(2).getX()),
    					Double.min(Double.min(pl.get(0).getY(),pl.get(1).getY()),pl.get(2).getY()),
    					Double.max(Math.abs(pl.get(0).getX()-pl.get(1).getX()),Math.abs(pl.get(1).getX()-pl.get(2).getX())),
    					Double.max(Math.abs(pl.get(0).getY()-pl.get(1).getY()),Math.abs(pl.get(1).getY()-pl.get(2).getY())));
    		}
    		logger.trace("Path is not a rectangle: {}", dumpPath(path));
    	}

		return null;
	}

	private String dumpPath(final GeneralPath path) {
    	final StringBuffer sb = new StringBuffer(); 
    	for (final PathIterator pi = path.getPathIterator(null); !pi.isDone(); pi.next()) {
    		final double[] coords = new double[6];
    		// The type will be SEG_LINETO, SEG_MOVETO, or SEG_CLOSE
    		// Because the Area is composed of straight lines
    		final int type = pi.currentSegment(coords);
    		sb.append(String.format("t:%d %.3f,%.3f; ", type, coords[0],coords[1]));
    	}
    	return sb.toString();
    }

    public static Color claGetJavaColor(final PDColorState pdColor) {
    	final PDColorSpace colorSpace = pdColor.getColorSpace();
    	final float[] components = pdColor.getColorSpaceValue();
    	
    	boolean componentsLT1 = true;
    	for (int i=0;i<components.length;++i) {
			if (components[i]<0f || components[i]>1f) {
    			componentsLT1 = false;
    			break;
    		}
		}
    	
        try
        {
            final String csName = colorSpace.getName();
            if (PDDeviceRGB.NAME.equals(csName) && components.length == 3)
            {
            	if (!componentsLT1) {
            		logger.debug("Bad color components found {} colorSpaceName={}", Arrays.asList(components).toString(), colorSpace.getName());
            		return Color.BLACK;
            	}
                return new Color(components[0], components[1], components[2]);
            }
            else if (PDLab.NAME.equals(csName))
            {
                // transform the color values from Lab- to RGB-space
                final float[] csComponents = colorSpace.getJavaColorSpace().toRGB(components);
                return new Color(csComponents[0], csComponents[1], csComponents[2]);
            }
            else
            {
                if (components.length == 1)
                {
                    if (PDSeparation.NAME.equals(csName))
                    {
                        // Use that component as a single-integer RGB value
                        return new Color((int) components[0]);
                    }
                    if (PDDeviceGray.NAME.equals(csName))
                    {
                    	if (!componentsLT1) {
                    		logger.debug("Bad color components found {} colorSpaceName={}", Arrays.asList(components).toString(), colorSpace.getName());
                    		return Color.BLACK;
                    	}
                        return new Color(components[0], components[0], components[0]);
                    }
                }
                final Color override = Color.BLACK;
                ColorSpace cs;
                if (colorSpace instanceof PDCalGray) {
                    if (components.length == 1) {
						return new Color((int) components[0]);
					}
                	if (!componentsLT1) {
                		logger.debug("Bad color components found {} colorSpaceName={}", Arrays.asList(components).toString(), colorSpace.getName());
                		return Color.BLACK;
                	}
                    final float avgComponent = (float)IntStream.range(0, components.length).mapToDouble(i->Double.valueOf(components[i])).sum()/3;
                    return new Color(avgComponent, avgComponent, avgComponent);
                } else if (colorSpace instanceof PDPattern) {
                    logger.debug("Error getting colorSpace for PDPattern. components={}", components);
                    return override;
                }
				try {
					cs = colorSpace.getJavaColorSpace();
				} catch (final IOException e) {
                    logger.debug("Error getting colorSpace for {}. {}", colorSpace.getClass().toString(), e);
                    return override;
				}
                if (cs instanceof ICC_ColorSpace && override != null)
                {
                    logger.trace("Using an ICC override color to avoid a potential JVM crash (see PDFBOX-511)");
                    return override;
                } else {
                    return new Color(cs, components, 1f);
                }
            }
        }
        // Catch IOExceptions from PDColorSpace.getJavaColorSpace(), but
        catch (final Exception e)
        {
            Color cGuess;
            String sMsg = "Unable to create the color instance ";
            if (logger.isTraceEnabled()) {
				sMsg += Arrays.toString(components) + " in color space " + colorSpace + "; {}; guessing color ... ";
			}
            try
            {
                switch (components.length)
                {
                case 1:// Use that component as a single-integer RGB value
                    cGuess = new Color((int) components[0]);
                    if (logger.isTraceEnabled()) {
						sMsg += "\nInterpretating as single-integer RGB";
					}
                    break;
                case 3: // RGB
                    cGuess = new Color(components[0], components[1], components[2]);
                    if (logger.isTraceEnabled()) {
						sMsg += "\nInterpretating as RGB";
					}
                    break;
                case 4: // CMYK
                    // do a rough conversion to RGB as I'm not getting the CMYK to work.
                    // http://www.codeproject.com/KB/applications/xcmyk.aspx
                    float r,
                    g,
                    b,
                    k;
                    k = components[3];

                    r = components[0] * (1f - k) /*+ k*/;
                    g = components[1] * (1f - k) /*+ k*/;
                    b = components[2] * (1f - k) /*+ k*/;

                    r = (1f - r);
                    g = (1f - g);
                    b = (1f - b);

                    cGuess = componentsLT1?(new Color(r, g, b)):Color.BLACK;
                    if (logger.isTraceEnabled()) {
						sMsg += "\nInterpretating as CMYK";
					}
                    break;
                default:
                	if (logger.isTraceEnabled()) {
						sMsg += "\nUnable to guess using " + components.length + " components; using black instead";
					}
                    cGuess = Color.BLACK;
                }
            }
            catch (final Exception e2)
            {
                if (logger.isTraceEnabled()) {
					sMsg += "\nColor interpolation failed; using black instead\n";
				}
                if (logger.isTraceEnabled()) {
					sMsg += e2.toString();
				}
                cGuess = Color.BLACK;
            }
            logger.trace(sMsg, e);
            return cGuess;
        }
    }

}
