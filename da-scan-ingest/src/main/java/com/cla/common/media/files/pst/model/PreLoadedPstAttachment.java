package com.cla.common.media.files.pst.model;

import com.cla.common.domain.pst.PstPath;
import com.pff.PSTAttachment;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Objects;

/**
 * Created By Itai Marko
 */
class PreLoadedPstAttachment extends PstAttachment {

    private static final Logger logger = LoggerFactory.getLogger(PreLoadedPstAttachment.class);

    private final byte[] attachmentBytes;

    PreLoadedPstAttachment(PSTAttachment attachment, PstFile pstFile, PstPath pstPath, PstEmailMessage containingEmailMsg) {
        super(attachment, pstFile, pstPath, containingEmailMsg);
        this.attachmentBytes = readAttachmentBytes();
    }

    @Override
    public int getFileSize() {
        int parentFileSize = super.getFileSize();
        int contentLength = attachmentBytes.length;
        if (parentFileSize != contentLength) {
            String stackTrace = Arrays.toString(Thread.currentThread().getStackTrace());
            logger.warn("Overriding parents's FileSize. Parent's is: {}, using: {} StackTrace: {}",
                    parentFileSize, contentLength, stackTrace);
        }
        return contentLength;
    }

    @Override
    public InputStream getAttachmentInputStream() {
        Objects.requireNonNull(attachmentBytes);
        return new ByteArrayInputStream(attachmentBytes);
    }

    private byte[] readAttachmentBytes() {
        try (InputStream attachmentInputStream = super.getAttachmentInputStream()) {
            return IOUtils.toByteArray(attachmentInputStream);
        } catch (IOException e) {
            throw new RuntimeException("Failed to read email attachment into a byte array for PstAttachment " + getPstPath().toString(), e);
        }
    }
}
