package com.cla.common.media.files.visio;

import com.cla.common.media.files.xml.CurtailedPackagePart;
import com.microsoft.schemas.office.visio.x2012.main.VisioDocumentDocument1;
import com.microsoft.schemas.office.visio.x2012.main.VisioDocumentType;
import org.apache.poi.POIXMLDocumentPart;
import org.apache.poi.POIXMLException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.openxml4j.opc.PackageRelationshipTypes;
import org.apache.poi.xdgf.usermodel.XDGFDocument;
import org.apache.poi.xdgf.usermodel.XDGFFactory;
import org.apache.poi.xdgf.usermodel.XmlVisioDocument;
import org.apache.xmlbeans.XmlException;

import java.io.IOException;
import java.lang.reflect.Field;

/**
 * Custom XmlVisioDocument for the purpose of limiting the number of XML objects
 * Created by vladi on 4/3/2017.
 */
public class ClaXmlVisioDocument extends XmlVisioDocument {

    private static Field coreDocumentRelField;
    static{
        try {
            coreDocumentRelField = POIXMLDocumentPart.class.getDeclaredField("coreDocumentRel");
            coreDocumentRelField.setAccessible(true);
        }
        catch (NoSuchFieldException e) {
            throw new RuntimeException("Failed to apply reflection on POIXMLDocumentPart.", e);
        }
    }

    public ClaXmlVisioDocument(OPCPackage pkg) throws IOException {
        super(pkg);
        try {
            coreDocumentRelField.set(this, PackageRelationshipTypes.VISIO_CORE_DOCUMENT);
        }
        catch (IllegalAccessException e) {
            throw new RuntimeException("Failed to set PackageRelationshipTypes.", e);
        }

        VisioDocumentType document;

        try {
            CurtailedPackagePart curtailedPackagePart = new CurtailedPackagePart(getPackagePart());
            document = VisioDocumentDocument1.Factory.parse(curtailedPackagePart.getInputStream()).getVisioDocument();
        }
        catch (IOException | XmlException e) {
            throw new POIXMLException(e);
        }
        catch (InvalidFormatException e) {
            throw new RuntimeException("Failed to create curtailed part.", e);
        }

        _document = new XDGFDocument(document);

        //build a tree of POIXMLDocumentParts, this document being the root
        load(new XDGFFactory(_document));
    }




}
