package com.cla.common.media.files.pst.model;

import com.cla.common.domain.pst.PstPath;
import com.cla.common.media.files.pst.visitor.PstEntryVisitor;
import com.cla.common.utils.EmailUtils;
import com.cla.connector.domain.dto.media.mail.MailItemType;
import com.pff.PSTAttachment;
import com.pff.PSTException;
import com.pff.PSTMessage;
import com.pff.PSTRecipient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created By Itai Marko
 */
public class PstEmailMessage extends PstEntryBase {

    private static final Logger logger = LoggerFactory.getLogger(PstEmailMessage.class);

    final PSTMessage libpstEmailMsg;
    private final byte[] contentBytes;

    PstEmailMessage(PSTMessage libpstEmailMsg, PstFile pstFile, PstPath pstPath) {
        super(pstFile, pstPath);
        this.libpstEmailMsg = libpstEmailMsg;
        // TODO Itai: [DAMAIN-3075] we will need to add on top of this if we include fields like 'subject', 'to' and 'from' in what we consider the content of the email 'file'
        String content = libpstEmailMsg.getBody();
        // TODO Itai: [DAMAIN-3075] The following assumes UTF8 encoding. Detect encoding using Tika (need spring to use Tika)
        this.contentBytes = content.getBytes(StandardCharsets.UTF_8);
    }

    @Override
    public long getPstEntryId() {
        return libpstEmailMsg.getDescriptorNodeId();
    }

    @Override
    public <T> T accept(PstEntryVisitor<T> visitor) {
        return visitor.visitEmailMessage(this);
    }

    @Override
    <T> void descend(PstEntryVisitor<T> visitor) {
        int attachmentsCount = libpstEmailMsg.getNumberOfAttachments();
        for (int i = 0; i < attachmentsCount; i++) {
            PstEntry pstAttachment = createChildPstAttachment(i);
            pstAttachment.accept(visitor);
        }
    }

    PstAttachment createChildPstAttachment(int attachmentIndex) {
        try {
            PSTAttachment libpstAttachment = libpstEmailMsg.getAttachment(attachmentIndex);
            String attachmentFilename = libpstAttachment.getLongFilename().trim();
            if (attachmentFilename.isEmpty()) {
                attachmentFilename = libpstAttachment.getFilename().trim();
            }
            PstPath pstAttachmentPath =
                    getPstPath().resolveUniqueEntry(attachmentFilename, attachmentIndex, MailItemType.ATTACHMENT, libpstEmailMsg.getDescriptorNodeId());
            return getPstFile().createChildPstAttachment(libpstAttachment, pstAttachmentPath, this);
        } catch (PSTException | IOException e) {
            throw new RuntimeException("Error occurred while creating attachment #" + attachmentIndex + " of pst email message: " + getPstPath().toString(), e);
        }
    }

    public byte[] getContentBytes() {
        return contentBytes;
    }

    public long getContentSize() {
        return contentBytes.length;
    }

    public boolean hasAttachments() {
        return libpstEmailMsg.getNumberOfAttachments() > 0;
    }

    public Integer getNumOfAttachments() {
        return libpstEmailMsg.getNumberOfAttachments() > 0 ?
                libpstEmailMsg.getNumberOfAttachments() : null;
    }

    /**
     * @return an immutable pair of sender name (may be empty), an sender email address
     */
    public PstAddress getSender() {
        String senderName = libpstEmailMsg.getSenderName().trim();
        String senderEmailAddress = libpstEmailMsg.getSenderEmailAddress().trim();
        if (logger.isTraceEnabled() && EmailUtils.extractDomainFromEmailAddress(senderEmailAddress, false, false) == null) {
            logger.trace("Creating PstAddress with no domain part for sender name: {} with address: {} of PstEmailMessage: {}", senderName, senderEmailAddress, getPstPath());
        }
        return new PstAddress(senderName, senderEmailAddress);
    }

    public String getSubject() {
        return libpstEmailMsg.getSubject();
    }

    public List<PstRecipient> getRecipients() {
        try {
            int recipientsNum = libpstEmailMsg.getNumberOfRecipients();
            List<PstRecipient> recipients = new ArrayList<>(recipientsNum);
            for (int i = 0; i < recipientsNum; i++) {
                PSTRecipient recipient = libpstEmailMsg.getRecipient(i);
                String recipientName = recipient.getDisplayName().trim();
                String recipientEmailAddress = recipient.getEmailAddress().trim();
                int recipientType = recipient.getRecipientType();
                if (logger.isTraceEnabled() && EmailUtils.extractDomainFromEmailAddress(recipientEmailAddress, false, false) == null) {
                    logger.trace("Creating PstAddress with no domain part for recipient name: {} with address: {} of PstEmailMessage: {}", recipientName, recipientEmailAddress, getPstPath());
                }
                recipients.add(new PstRecipient(recipientName, recipientEmailAddress, recipientType));
            }
            return recipients;
        } catch (PSTException | IOException e) {
            throw new RuntimeException("Failed to get the recipients of pst email message: " + getPstPath().toString(), e);
        }
    }

    public Date getSentDate() {
        return libpstEmailMsg.getClientSubmitTime();
    }
}
