package com.cla.common.media.files.ooxml;

import com.cla.common.media.files.word.ClaXWPFDocument;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.apache.xmlbeans.XmlException;

import java.io.IOException;

/**
 * Customization to enable usage of ClaXWPFDocument
 * Created by vladi on 4/2/2017.
 */
public class ClaXWPFWordExtractor extends XWPFWordExtractor {

    public ClaXWPFWordExtractor(OPCPackage container) throws XmlException, OpenXML4JException, IOException {
        super(new ClaXWPFDocument(container));
    }

}
