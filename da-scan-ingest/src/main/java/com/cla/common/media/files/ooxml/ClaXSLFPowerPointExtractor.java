package com.cla.common.media.files.ooxml;

import com.cla.common.media.files.ppt.ClaXSLFSlideShow;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.xslf.extractor.XSLFPowerPointExtractor;
import org.apache.xmlbeans.XmlException;

import java.io.IOException;

/**
 * Custom Power Point extractor for limiting XML objectsnumber
 * Created by vladi on 4/3/2017.
 */
public class ClaXSLFPowerPointExtractor extends XSLFPowerPointExtractor {

    ClaXSLFPowerPointExtractor(OPCPackage container) throws XmlException, OpenXML4JException, IOException {
        super(new ClaXSLFSlideShow(container));
    }

    ClaXSLFPowerPointExtractor(ClaXSLFSlideShow slideshow) throws XmlException, IOException {
        super(slideshow);
    }
}
