package com.cla.common.media.files.excel.streaming;

import com.cla.common.exceptions.NotSupportedException;
import org.apache.poi.hssf.util.PaneInformation;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellAddress;
import org.apache.poi.ss.util.CellRangeAddress;

import java.util.*;

/**
 * This class is used by the streaming excel reader to hold a "sheet" without really holding it.
 * Created by uri on 02/11/2015.
 */
public class StreamingExcelSheet implements Sheet {

    private String sheetName;
    private Workbook workbook;

    private List<CellRangeAddress> arrayFormulas  = new ArrayList<>();

    public StreamingExcelSheet(String sheetName, Workbook workbook) {
        this.sheetName = sheetName;
        this.workbook = workbook;
    }

    @Override
    public Row createRow(int rownum) {
        throw new NotSupportedException();
    }

    @Override
    public void removeRow(Row row) {
        throw new NotSupportedException();
    }

    @Override
    public Row getRow(int rownum) {
        throw new NotSupportedException();
    }

    @Override
    public int getPhysicalNumberOfRows() {
        throw new NotSupportedException();
    }

    @Override
    public int getFirstRowNum() {
        throw new NotSupportedException();
    }

    @Override
    public int getLastRowNum() {
        throw new NotSupportedException();
    }

    @Override
    public void setColumnHidden(int columnIndex, boolean hidden) {
        throw new NotSupportedException();
    }

    @Override
    public boolean isColumnHidden(int columnIndex) {
        throw new NotSupportedException();
    }

    @Override
    public void setRightToLeft(boolean value) {
        throw new NotSupportedException();
    }

    @Override
    public boolean isRightToLeft() {
        throw new NotSupportedException();
    }

    @Override
    public void setColumnWidth(int columnIndex, int width) {
        throw new NotSupportedException();
    }

    @Override
    public int getColumnWidth(int columnIndex) {
        throw new NotSupportedException();
    }

    @Override
    public float getColumnWidthInPixels(int columnIndex) {
        throw new NotSupportedException();
    }

    @Override
    public void setDefaultColumnWidth(int width) {
        throw new NotSupportedException();
    }

    @Override
    public int getDefaultColumnWidth() {
        throw new NotSupportedException();
    }

    @Override
    public short getDefaultRowHeight() {
        throw new NotSupportedException();
    }

    @Override
    public float getDefaultRowHeightInPoints() {
        throw new NotSupportedException();
    }

    @Override
    public void setDefaultRowHeight(short height) {
        throw new NotSupportedException();

    }

    @Override
    public void setDefaultRowHeightInPoints(float height) {
        throw new NotSupportedException();

    }

    @Override
    public CellStyle getColumnStyle(int column) {
        throw new NotSupportedException();
    }

    @Override
    public int addMergedRegion(CellRangeAddress region) {
        throw new NotSupportedException();
    }

    @Override
    public int addMergedRegionUnsafe(CellRangeAddress region) {
        throw new NotSupportedException();
    }

    @Override
    public void validateMergedRegions() {
        throw new NotSupportedException();
    }

    @Override
    public void setVerticallyCenter(boolean value) {
        throw new NotSupportedException();
    }

    @Override
    public void setHorizontallyCenter(boolean value) {
        throw new NotSupportedException();

    }

    @Override
    public boolean getHorizontallyCenter() {
        throw new NotSupportedException();
    }

    @Override
    public boolean getVerticallyCenter() {
        throw new NotSupportedException();
    }

    @Override
    public void removeMergedRegion(int index) {
        throw new NotSupportedException();

    }

    @Override
    public void removeMergedRegions(Collection<Integer> indices) {
        throw new NotSupportedException();
    }

    @Override
    public int getNumMergedRegions() {
        throw new NotSupportedException();
    }

    @Override
    public CellRangeAddress getMergedRegion(int index) {
        throw new NotSupportedException();
    }

    @Override
    public List<CellRangeAddress> getMergedRegions()  {
        throw new NotSupportedException();
    }

    @Override
    public Iterator<Row> rowIterator() {
        throw new NotSupportedException();
    }

    @Override
    public void setForceFormulaRecalculation(boolean value) {
        throw new NotSupportedException();

    }

    @Override
    public boolean getForceFormulaRecalculation() {
        throw new NotSupportedException();
    }

    @Override
    public void setAutobreaks(boolean value) {
        throw new NotSupportedException();

    }

    @Override
    public void setDisplayGuts(boolean value) {
        throw new NotSupportedException();

    }

    @Override
    public void setDisplayZeros(boolean value) {
        throw new NotSupportedException();

    }

    @Override
    public boolean isDisplayZeros() {
        throw new NotSupportedException();
    }

    @Override
    public void setFitToPage(boolean value) {
        throw new NotSupportedException();

    }

    @Override
    public void setRowSumsBelow(boolean value) {
        throw new NotSupportedException();

    }

    @Override
    public void setRowSumsRight(boolean value) {
        throw new NotSupportedException();

    }

    @Override
    public boolean getAutobreaks() {
        throw new NotSupportedException();
    }

    @Override
    public boolean getDisplayGuts() {
        throw new NotSupportedException();
    }

    @Override
    public boolean getFitToPage() {
        throw new NotSupportedException();
    }

    @Override
    public boolean getRowSumsBelow() {
        throw new NotSupportedException();
    }

    @Override
    public boolean getRowSumsRight() {
        throw new NotSupportedException();
    }

    @Override
    public boolean isPrintGridlines() {
        throw new NotSupportedException();
    }

    @Override
    public void setPrintGridlines(boolean show) {
        throw new NotSupportedException();

    }

    @Override
    public boolean isPrintRowAndColumnHeadings() {
        throw new NotSupportedException();
    }

    @Override
    public void setPrintRowAndColumnHeadings(boolean show) {
        throw new NotSupportedException();
    }

    @Override
    public PrintSetup getPrintSetup() {
        throw new NotSupportedException();
    }

    @Override
    public Header getHeader() {
        throw new NotSupportedException();
    }

    @Override
    public Footer getFooter() {
        throw new NotSupportedException();
    }

    @Override
    public void setSelected(boolean value) {
        throw new NotSupportedException();

    }

    @Override
    public double getMargin(short margin) {
        throw new NotSupportedException();
    }

    @Override
    public void setMargin(short margin, double size) {
        throw new NotSupportedException();

    }

    @Override
    public boolean getProtect() {
        throw new NotSupportedException();
    }

    @Override
    public void protectSheet(String password) {
        throw new NotSupportedException();

    }

    @Override
    public boolean getScenarioProtect() {
        throw new NotSupportedException();
    }

    @Override
    public void setZoom(int numerator, int denominator) {
        throw new NotSupportedException();

    }

    @Override
    public void setZoom(int scale) {
        throw new NotSupportedException();
    }

    @Override
    public short getTopRow() {
        throw new NotSupportedException();
    }

    @Override
    public short getLeftCol() {
        throw new NotSupportedException();
    }

    @Override
    public void showInPane(int toprow, int leftcol) {
        throw new NotSupportedException();

    }

    @Override
    public void shiftRows(int startRow, int endRow, int n) {
        throw new NotSupportedException();

    }

    @Override
    public void shiftRows(int startRow, int endRow, int n, boolean copyRowHeight, boolean resetOriginalRowHeight) {
        throw new NotSupportedException();

    }

    @Override
    public void createFreezePane(int colSplit, int rowSplit, int leftmostColumn, int topRow) {
        throw new NotSupportedException();

    }

    @Override
    public void createFreezePane(int colSplit, int rowSplit) {

        throw new NotSupportedException();
    }

    @Override
    public void createSplitPane(int xSplitPos, int ySplitPos, int leftmostColumn, int topRow, int activePane) {
        throw new NotSupportedException();

    }

    @Override
    public PaneInformation getPaneInformation() {
        throw new NotSupportedException();
    }

    @Override
    public void setDisplayGridlines(boolean show) {
        throw new NotSupportedException();

    }

    @Override
    public boolean isDisplayGridlines() {
        throw new NotSupportedException();
    }

    @Override
    public void setDisplayFormulas(boolean show) {
        throw new NotSupportedException();

    }

    @Override
    public boolean isDisplayFormulas() {
        throw new NotSupportedException();
    }

    @Override
    public void setDisplayRowColHeadings(boolean show) {
        throw new NotSupportedException();

    }

    @Override
    public boolean isDisplayRowColHeadings() {
        throw new NotSupportedException();
    }

    @Override
    public void setRowBreak(int row) {
        throw new NotSupportedException();

    }

    @Override
    public boolean isRowBroken(int row) {
        throw new NotSupportedException();
    }

    @Override
    public void removeRowBreak(int row) {
        throw new NotSupportedException();

    }

    @Override
    public int[] getRowBreaks() {
        throw new NotSupportedException();
    }

    @Override
    public int[] getColumnBreaks() {
        throw new NotSupportedException();
    }

    @Override
    public void setColumnBreak(int column) {
        throw new NotSupportedException();

    }

    @Override
    public boolean isColumnBroken(int column) {
        throw new NotSupportedException();
    }

    @Override
    public void removeColumnBreak(int column) {
        throw new NotSupportedException();

    }

    @Override
    public void setColumnGroupCollapsed(int columnNumber, boolean collapsed) {
        throw new NotSupportedException();

    }

    @Override
    public void groupColumn(int fromColumn, int toColumn) {

        throw new NotSupportedException();
    }

    @Override
    public void ungroupColumn(int fromColumn, int toColumn) {

        throw new NotSupportedException();
    }

    @Override
    public void groupRow(int fromRow, int toRow) {

        throw new NotSupportedException();
    }

    @Override
    public void ungroupRow(int fromRow, int toRow) {
        throw new NotSupportedException();

    }

    @Override
    public void setRowGroupCollapsed(int row, boolean collapse) {
        throw new NotSupportedException();

    }

    @Override
    public void setDefaultColumnStyle(int column, CellStyle style) {
        throw new NotSupportedException();

    }

    @Override
    public void autoSizeColumn(int column) {
        throw new NotSupportedException();

    }

    @Override
    public void autoSizeColumn(int column, boolean useMergedCells) {
        throw new NotSupportedException();

    }

    @Override
    public Comment getCellComment(int row, int column) {
        throw new NotSupportedException();
    }

    @Override
    public Comment getCellComment(CellAddress ref) {
        throw new NotSupportedException();
    }

    @Override
    public Map<CellAddress, ? extends Comment> getCellComments() {
        throw new NotSupportedException();
    }

    @Override
    public Drawing getDrawingPatriarch() {
        throw new NotSupportedException();
    }

    @Override
    public Drawing createDrawingPatriarch() {
        throw new NotSupportedException();
    }

    @Override
    public Workbook getWorkbook() {
        return workbook;
    }

    @Override
    public String getSheetName() {
        return sheetName;
    }

    @Override
    public boolean isSelected() {
        return false;
    }

    @Override
    public CellRange<? extends Cell> setArrayFormula(String formula, CellRangeAddress range) {
        throw new NotSupportedException();
    }

    @Override
    public CellRange<? extends Cell> removeArrayFormula(Cell cell) {
        throw new NotSupportedException();
    }

    @Override
    public DataValidationHelper getDataValidationHelper() {
        throw new NotSupportedException();
    }

    @Override
    public List<? extends DataValidation> getDataValidations() {
        throw new NotSupportedException();
    }

    @Override
    public void addValidationData(DataValidation dataValidation) {
        throw new NotSupportedException();

    }

    @Override
    public AutoFilter setAutoFilter(CellRangeAddress range) {
        throw new NotSupportedException();
    }

    @Override
    public SheetConditionalFormatting getSheetConditionalFormatting() {
        throw new NotSupportedException();
    }

    @Override
    public CellRangeAddress getRepeatingRows() {
        throw new NotSupportedException();
    }

    @Override
    public CellRangeAddress getRepeatingColumns() {
        throw new NotSupportedException();
    }

    @Override
    public void setRepeatingRows(CellRangeAddress rowRangeRef) {

        throw new NotSupportedException();
    }

    @Override
    public void setRepeatingColumns(CellRangeAddress columnRangeRef) {

        throw new NotSupportedException();
    }

    @Override
    public Iterator<Row> iterator() {
        throw new NotSupportedException();
    }

    public boolean isCellInArrayFormulaContext(StreamingExcelCell cell) {
        for (CellRangeAddress range : arrayFormulas) {
            if (range.isInRange(cell.getRowIndex(), cell.getColumnIndex())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int getColumnOutlineLevel(int columnIndex) {
        throw new NotSupportedException();
    }

    @Override
    public Hyperlink getHyperlink(int row, int column) {
        throw new NotSupportedException();
    }

    @Override
    public Hyperlink getHyperlink(CellAddress addr) {
        throw new NotSupportedException();
    }

    @Override
    public List<? extends Hyperlink> getHyperlinkList() {
        throw new NotSupportedException();
    }

    @Override
    public CellAddress getActiveCell() {
        throw new NotSupportedException();
    }

    @Override
    public void setActiveCell(CellAddress address) {
        throw new NotSupportedException();
    }

    public void addArrayFormula(CellRangeAddress cellRangeAddress) {
        arrayFormulas.add(cellRangeAddress);
    }
}
