package com.cla.common.media.ingest;

import com.cla.common.connectors.ExistingContentConnector;
import com.cla.common.domain.dto.histogram.ClaStyle;
import com.cla.common.domain.dto.mediaproc.word.WordFile;
import com.cla.common.domain.dto.messages.*;
import com.cla.common.utils.signature.ContentAndSignature;
import com.cla.common.utils.signature.ContentSignHelper;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.file.FileType;
import com.cla.connector.domain.dto.error.ProcessingErrorType;
import com.cla.common.domain.pst.PstPath;
import com.google.common.base.Strings;
import org.apache.tika.Tika;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

@Component
public abstract class FileIngestService<T extends IngestMessagePayload> {
    protected final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private Tika tika;

    @Value("${ingester.signature.maxsize:25000000}")
    private int ingesterSignatureMaxSize;

    @Value("${ingester.file.maxsize:100000000}")
    private long ingesterFileMaxSize;

    // # If set to false, should set FileCluster component.media-processor.verify-stats=false
    @Value("${ingester.check-content-duplicate:false}")
    private boolean checkContentAlreadyExists;

    @Value("${ingester.word.color-translation.gamma:}")
    private Float colorTranslationGamma;

    @Value("${ingester.word.color-translation.bits:}")
    private Integer colorTranslationBits;

    @Value("${ingester.word.font-size-translation:}")
    private String fontSizeLookupStr;

    @Value("${ingester.network-error:}")
    private String[] networkErrorMessages;

    private List<String> networkErrorMsg;

    protected ContentSignHelper signHelper;

    public FileIngestService() {
    }

    public FileIngestService(long ingesterFileMaxSize) {
        this.ingesterFileMaxSize = ingesterFileMaxSize;
    }

    @PostConstruct
    public void init() {
        signHelper = new ContentSignHelper(ingesterSignatureMaxSize);
        networkErrorMsg = networkErrorMessages == null ? new ArrayList<>() : Arrays.asList(networkErrorMessages);

        if (colorTranslationBits != null && colorTranslationGamma != null) {
            ClaStyle.setColorLookupParams(colorTranslationGamma, colorTranslationBits);
            logger.info("Setting ClaStyle colorTranslation({},{})", colorTranslationGamma, colorTranslationBits);
        }
        if (fontSizeLookupStr != null && !fontSizeLookupStr.isEmpty()) {
            ClaStyle.setFontSizeLookup(fontSizeLookupStr);
            logger.info("Setting ClaStyle fontSizeLookupStr({})", fontSizeLookupStr);
        }
    }

    public boolean testNetworkError(String err) {
        if (!networkErrorMsg.isEmpty()) {
            for (String msg : networkErrorMsg) {
                if (err.contains(msg)) {
                    return true;
                }
            }
        }
        return false;
    }

    public T ingestFile(
            IngestTaskParameters ingestTaskParameters, ClaFilePropertiesDto claFilePropertiesDto, InputStream inputStream,
            ExistingContentConnector existingContentConnector, long rootFolderId) {

        try {
            claFilePropertiesDto.setType(getBaseFileType());
            T errorIngestPayload = validateFile(claFilePropertiesDto);
            if (errorIngestPayload != null) {
                return errorIngestPayload;
            } else {
                return signContentAndIngest(
                        ingestTaskParameters, claFilePropertiesDto, inputStream, existingContentConnector, rootFolderId);
            }
        } catch (final FileNotFoundException e) {
            logger.info("File [{}] not found", claFilePropertiesDto.getFileName());
            return createIngestPayload(
                    testNetworkError(e.getMessage()) ? ProcessingErrorType.ERR_NETWORK : ProcessingErrorType.ERR_FILE_NOT_FOUND,
                    claFilePropertiesDto, e.getMessage(), e);
        } catch (IOException | RuntimeException e) {
            logger.warn("Failed to ingest file {}. Error is {}.", claFilePropertiesDto.getFileName(), e.getMessage(), e);
            return createIngestPayload(ProcessingErrorType.ERR_IO, claFilePropertiesDto, e.getMessage(), e);
        }
    }

    protected T signContentAndIngest(
            IngestTaskParameters ingestTaskParameters, ClaFilePropertiesDto claFilePropertiesDto, InputStream inputStream,
            ExistingContentConnector existingContentConnector, long rootFolderId) throws IOException {

        ContentAndSignature contAndSign = signHelper.readAndSign(inputStream, claFilePropertiesDto.getFileSize().intValue(),
                claFilePropertiesDto.getFileName());

        if (checkContentAlreadyExists) {
            // get signature metadata from connector
            ContentMetadataResponseMessage checkForExistingContentResponse =
                    checkForExistingContent(existingContentConnector, claFilePropertiesDto.getFileSize(), contAndSign.getSignature(), rootFolderId);

            // if content with similar signature already exists in the DB
            // create result with content metadata entity ID
            if (checkForExistingContentResponse == null) {
                logger.debug("Content request failed for file {}", claFilePropertiesDto.getFileName());
            } else if (checkForExistingContentResponse.isForceReIngest()) {
                logger.debug("Content {} already exists in the database but force re-ingest", checkForExistingContentResponse.getContentMetadataId());
                claFilePropertiesDto.setContentMetadataId(checkForExistingContentResponse.getContentMetadataId());
            } else if (checkForExistingContentResponse.metadataExists()) {
                logger.debug("Content {} already exists in the database. Skip full ingestion", checkForExistingContentResponse.getContentMetadataId());
                claFilePropertiesDto.setContentMetadataId(checkForExistingContentResponse.getContentMetadataId());
                return createIngestPayload(ProcessingErrorType.ERR_NONE, claFilePropertiesDto, "", null);
            }
        }
        signHelper.readRestOfFile(contAndSign, inputStream, claFilePropertiesDto.getFileSize().intValue());
        return ingestFile(ingestTaskParameters, contAndSign.getContent(), true, claFilePropertiesDto);
    }



    private T validateFile(ClaFilePropertiesDto claFilePropertiesDto) {
        if (fileExceedsSizeLimit(claFilePropertiesDto)) {
            logger.debug("Skipping ingestion for file {}, reason: file size is greater than max size ({} > {}).", claFilePropertiesDto.getFileName(), claFilePropertiesDto.getFileSize(), ingesterFileMaxSize);
            return createIngestPayload(ProcessingErrorType.ERR_LIMIT_EXCEEDED, claFilePropertiesDto, "Size limit is " + ingesterFileMaxSize, null);
        } else if (claFilePropertiesDto.getFileSize() == 0) {
            logger.debug("Skipping ingestion for file {}, reason: empty file", claFilePropertiesDto.getFileName(), claFilePropertiesDto.getFileSize(), ingesterFileMaxSize);
            return createIngestPayload(ProcessingErrorType.ERR_NONE, claFilePropertiesDto, "File size is 0", null);
        }
        return null;
    }

    private ContentMetadataResponseMessage checkForExistingContent(ExistingContentConnector existingContentConnector, Long fileSize, String contentSignature, long rootFolderId) {
        ContentMetadataResponseMessage response = null;
        if (!Strings.isNullOrEmpty(contentSignature) && fileSize != null) {
            ContentMetadataRequestMessage contentMetadataRequestMessage = ContentMetadataRequestMessage.create()
                    .setFileSize(fileSize.intValue())
                    .setSignature(contentSignature)
                    .setRootFolderId(rootFolderId);
            response = existingContentConnector.checkForExistingContent(contentMetadataRequestMessage);
        }
        return response;
    }

    public T ingestFile(
            IngestTaskParameters ingestTaskParameters, final byte[] content, final boolean extractMetadata,
            final ClaFilePropertiesDto claFilePropertiesDto) {

        String path = claFilePropertiesDto.getFileName();
        try (ByteArrayInputStream inputStream = new ByteArrayInputStream(content)) {
            String mime = PstPath.isPstEntryPath(claFilePropertiesDto.getMediaItemId()) ? tika.detect(content) : tika.detect(path);
            claFilePropertiesDto.setMime(mime);
            if (claFilePropertiesDto.getContentSignature() == null) {
                claFilePropertiesDto.setContentSignature(signHelper.sign(content));
            }
            Map<Long, String> reverseDictionary = new HashMap<>();
            if (ingestTaskParameters.isGetReverseDictionary()) {
                WordFile.setDebugHashMapper(reverseDictionary);
            }
            T result = extractViaRequestMessage(
                    ingestTaskParameters, inputStream, path, extractMetadata, claFilePropertiesDto);
            if (ingestTaskParameters.isGetReverseDictionary()) {
                WordFile.setDebugHashMapper(null);
                removeNGramFromReverseDictionary(reverseDictionary);
                result.setReverseDictionary(reverseDictionary);
            }
            return result;
        } catch (IOException e) {
            logger.warn("Failed to close input stream of file {}. Error is {}.", path, e.getMessage(), e);
            return null;
        }
    }

    private void removeNGramFromReverseDictionary(Map<Long, String> reverseDictionary) {
        Set<Long> toRemove = new HashSet<>();
        for (Map.Entry<Long, String> entry : reverseDictionary.entrySet()) {
            if (entry.getValue().contains(" ")) {
                toRemove.add(entry.getKey());
            }
        }

        for (Long key : toRemove)
            reverseDictionary.remove(key);
    }

    private boolean fileExceedsSizeLimit(ClaFilePropertiesDto props) {
        Long fileSize = props.getFileSize();
        return fileSize != null && fileSize > ingesterFileMaxSize;
    }

    protected abstract T createIngestPayload(ProcessingErrorType processingErrorType, ClaFilePropertiesDto claFilePropertiesDto, final String text, final Exception e);


    protected abstract FileType getBaseFileType();

    public abstract T extract(InputStream is, String path, boolean extractMetadata, ClaFilePropertiesDto claFileProperties);

    protected abstract T extractViaRequestMessage(
            IngestTaskParameters ingestTaskParameters, InputStream is, String path, boolean extractMetadata,
            ClaFilePropertiesDto claFileProperties);


}
