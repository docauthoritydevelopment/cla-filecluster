package com.cla.common.media.files.excel;

import com.beust.jcommander.internal.Lists;
import com.cla.common.domain.dto.file.ExcelAnalysisMetadata;
import com.cla.common.domain.dto.file.FileCollections;
import com.cla.common.domain.dto.histogram.ClaLongSuperCollectionArray;
import com.cla.common.domain.dto.mediaproc.excel.ExcelSheet;
import com.cla.common.domain.dto.mediaproc.excel.ExcelWorkbook;
import com.cla.common.domain.dto.mediaproc.excel.ExcelWorkbookHelper;
import com.cla.common.domain.dto.messages.ExcelIngestResult;
import com.cla.common.domain.dto.messages.ExcelSheetIngestResult;
import com.cla.common.domain.dto.messages.IngestTaskParameters;
import com.cla.common.media.files.excel.streaming.ExcelStreamingFilesExtractor;
import com.cla.common.media.ingest.FileIngestService;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.file.FileType;
import com.cla.connector.domain.dto.error.ProcessingErrorType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.InputStream;
import java.util.List;

@Component
public class ExcelIngesterService extends FileIngestService<ExcelIngestResult> {

    @Value("${ingester.excel.streaming:true}")
    private boolean streamingExcelIngester;

    @Value("${ingester.excel.convert-array-histograms:true}")
    private boolean useArrayHistograms;

    @Autowired
    private ExcelFilesService excelFilesService;

    @Autowired
    private ExcelStreamingFilesExtractor excelStreamingFilesService;

    @PostConstruct
    public void init() {
        super.init();
    }

    @Override
    protected FileType getBaseFileType() {
        return FileType.EXCEL;
    }

    @Override
    public ExcelIngestResult extractViaRequestMessage(
            IngestTaskParameters ingestTaskParameters, InputStream is, String path, boolean extractMetadata,
            ClaFilePropertiesDto claFileProperties) {

        return extract(is, path, extractMetadata, claFileProperties);
    }

    @Override
    public ExcelIngestResult extract(InputStream is, String path, boolean extractMetadata, ClaFilePropertiesDto claFileProperties) {
        ExcelWorkbook workbook;
        if (streamingExcelIngester) {
            workbook = excelStreamingFilesService.ingestWorkbook(path, is, claFileProperties);
        } else {
            workbook = excelFilesService.ingestWorkbook(path, is, extractMetadata, claFileProperties);
        }
        return convertIngestionResult(workbook, claFileProperties);
    }

    @Override
    protected ExcelIngestResult createIngestPayload(ProcessingErrorType processingErrorType, ClaFilePropertiesDto claFilePropertiesDto, final String text, final Exception e) {
        ExcelIngestResult result = new ExcelIngestResult();
        result.setErrorType(processingErrorType);
        result.setClaFileProperties(claFilePropertiesDto);
        result.setExceptionText(e == null ? text : e.getMessage());
        return result;
    }

    /**
     * Convert ExcelWorkbook object produced by the ingester to ExcelIngestResult
     *
     * @param workbook object produced by the ingester
     * @return json serializable result
     */
    private ExcelIngestResult convertIngestionResult(final ExcelWorkbook workbook, final ClaFilePropertiesDto claFileProperties) {

        ExcelIngestResult result = new ExcelIngestResult();
        result.setCsvValue(workbook.toCsvString("\n,,,"));
        result.setMetadata(workbook.getMetadata());
        result.setErrorType(workbook.getErrorType());
        result.setExceptionText(workbook.getExceptionText());
        result.setCombinedProposedTitles(ExcelWorkbookHelper.getProposedTitlesCombined(workbook));
        result.setFileName(workbook.getFileName());

        FileCollections fileCollections = new FileCollections();
        ExcelAnalysisMetadata sheetsMetadata = new ExcelAnalysisMetadata();

        List<ExcelSheetIngestResult> sheetIngestResults = Lists.newLinkedList();
        workbook.getSheets().values().forEach(sheet -> {
            final String sheetFullName = sheet.getFullSheetName();
            sheetIngestResults.add(convertSheetResult(sheet));
            fileCollections.addPartHistogram(sheetFullName,
                    useArrayHistograms ?
                            ClaLongSuperCollectionArray.create(sheet.getLongHistograms()) :
                            sheet.getLongHistograms());
            sheetsMetadata.addSheetMetadata(sheetFullName, sheet.getMetaData());
        });

        result.setSheets(sheetIngestResults);
        result.setFileCollections(fileCollections);
        result.setSheetsMetadata(sheetsMetadata);
        result.setClaFileProperties(claFileProperties);

        return result;
    }

    private ExcelSheetIngestResult convertSheetResult(ExcelSheet sheet) {
        ExcelSheetIngestResult sheetResult = new ExcelSheetIngestResult();
        sheetResult.setFullSheetName(sheet.getFullSheetName());
        sheetResult.setContentTokens(sheet.getContentTokens());
        sheetResult.setOrigContentTokens(sheet.getOrigContentTokens());

        return sheetResult;
    }

}
