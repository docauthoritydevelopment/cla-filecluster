package com.cla.common.media.files.pdf;

import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.util.PDFOperator;
import org.apache.pdfbox.util.operator.pagedrawer.BeginInlineImage;

import java.io.IOException;
import java.util.List;

/**
 * Created by itay on 08/01/2017.
 */

public class ClaPdfProcessorBI extends BeginInlineImage {
    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(ClaPdfProcessorBI.class);

    private static final int maxExceptions = 20;
    private int exceptionCounter = 0;

    @Override
    public void process(PDFOperator operator, List<COSBase> arguments)  throws IOException {
        try {
            if (exceptionCounter <= maxExceptions) {
                super.process(operator, arguments);
            }
        } catch (final ArrayIndexOutOfBoundsException e) {
            exceptionCounter++;
            logger.trace("Exception while processing #{}. {}", exceptionCounter, e, e);
            if (exceptionCounter == maxExceptions) {
                logger.warn("Too many exceptions. Stop image processing.");
            }
        } catch (final Exception e) {
            logger.debug("Exception while processing. {}", e, e);
        }
    }

}
