package com.cla.common.media.files.ooxml;

import com.cla.common.media.files.visio.ClaXmlVisioDocument;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.xdgf.extractor.XDGFVisioExtractor;

import java.io.IOException;

/**
 * Custom Visio extractor for limiting the number of XML objects
 * Created by vladi on 4/3/2017.
 */
public class ClaXDGFVisioExtractor extends XDGFVisioExtractor {

    @SuppressWarnings("unused")
    public ClaXDGFVisioExtractor(ClaXmlVisioDocument document) {
        super(document);
    }

    ClaXDGFVisioExtractor(OPCPackage openPackage) throws IOException {
        super(new ClaXmlVisioDocument(openPackage));
    }

}
