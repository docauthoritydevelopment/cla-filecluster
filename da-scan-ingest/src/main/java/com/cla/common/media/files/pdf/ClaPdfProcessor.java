package com.cla.common.media.files.pdf;

import com.cla.common.allocation.IngestionResourceManager;
import com.cla.common.domain.dto.mediaproc.word.FontStats;
import com.cla.common.domain.dto.mediaproc.word.WordFile;
import com.cla.common.domain.dto.pv.PVType;
import com.cla.common.utils.FileSizeUnits;
import com.cla.common.utils.tokens.HashedTokensConsumer;
import com.cla.common.utils.tokens.NGramTokenizerHashed;
import org.apache.commons.io.IOExceptionWithCause;
import org.apache.fontbox.util.BoundingBox;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSStream;
import org.apache.pdfbox.pdfviewer.PageDrawer;
import org.apache.pdfbox.pdmodel.*;
import org.apache.pdfbox.pdmodel.common.COSObjectable;
import org.apache.pdfbox.pdmodel.common.PDNameTreeNode;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.common.filespecification.PDComplexFileSpecification;
import org.apache.pdfbox.pdmodel.common.filespecification.PDEmbeddedFile;
import org.apache.pdfbox.pdmodel.graphics.PDGraphicsState;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDXObject;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDXObjectForm;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDXObjectImage;
import org.apache.pdfbox.pdmodel.interactive.digitalsignature.PDSignature;
import org.apache.pdfbox.pdmodel.interactive.documentnavigation.outline.PDDocumentOutline;
import org.apache.pdfbox.pdmodel.interactive.documentnavigation.outline.PDOutlineItem;
import org.apache.pdfbox.pdmodel.interactive.documentnavigation.outline.PDOutlineNode;
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;
import org.apache.pdfbox.pdmodel.interactive.form.PDField;
import org.apache.pdfbox.pdmodel.interactive.form.PDSignatureField;
import org.apache.pdfbox.pdmodel.text.PDTextState;
import org.apache.pdfbox.util.Matrix;
import org.apache.pdfbox.util.PDFOperator;
import org.apache.pdfbox.util.PDFTextStripper;
import org.apache.pdfbox.util.TextPosition;
import org.apache.pdfbox.util.operator.*;

import java.awt.*;
import java.awt.color.ColorSpace;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.Writer;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

class ClaPdfProcessor extends PDFTextStripper {
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(ClaPdfProcessor.class);
    
    /**
     * format used for signature dates
     */
    private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
 
    /**
     * Maximum recursive depth during AcroForm processing.
     * Prevents theoretical AcroForm recursion bomb. 
     */
    private final static int MAX_ACROFORM_RECURSIONS = 10;
    private final static int titleStyleMinRepeatCount = 3;
    private final static double defaultPageWidth = 1000;
    private final static double defaultPageHeight = 1000;
	private static short bodyTextNgramSize = 5;
    
    private final NGramTokenizerHashed nGramTokenizerHashed;

    private final ClaPageGraphics pageGraphics = new ClaPageGraphics();

    // TODO: remove once PDFBOX-1130 is fixed:
    // TODO: remove once PDFBOX-2160 is fixed
    private boolean inParagraph = false;
    
    private ClaPdfPage currClaPage = null;
    private final List<ClaPdfPage> docPages = new ArrayList<>();
	private final List<ClaPdfPage.PParagraph> docTitles = new ArrayList<>();
	private final List<ClaPdfPage.PParagraph> docHeadings = new ArrayList<>();

	private final List<String> imageSignatures = new ArrayList<>();
	
	private final FontStats fontSizeStats = new FontStats();

	private boolean tokenMatcherActive = false;
	private boolean patternSearchActive = false;

	private boolean docStart = false;
    private boolean docEnd = false;
    
    private int pdfMaxPagesToProcess = 0;
	private boolean firstWarnPrinted;
	private boolean warnPdfProcessingIssues;

	private boolean debugTextExtraction = false;


	public boolean isDebugTextExtraction() {
		return debugTextExtraction;
	}

	public void setDebugTextExtraction(boolean debugTextExtraction) {
		this.debugTextExtraction = debugTextExtraction;
	}

	private boolean acquireImageMemory = false;
	private IngestionResourceManager ingestionResourceManager;

	public boolean isAcquireImageMemory() {
		return acquireImageMemory;
	}

	public void setAcquireImageMemory(boolean acquireImageMemory) {
		this.acquireImageMemory = acquireImageMemory;
	}

	public IngestionResourceManager getIngestionResourceManager() {
		return ingestionResourceManager;
	}

	public void setIngestionResourceManager(IngestionResourceManager ingestionResourceManager) {
		this.ingestionResourceManager = ingestionResourceManager;
	}

	public int getPdfMaxPagesToProcess() {
		return pdfMaxPagesToProcess;
	}

	public void setPdfMaxPagesToProcess(final int pdfMaxPagesToProcess) {
		this.pdfMaxPagesToProcess = pdfMaxPagesToProcess;
	}

	public void setWarnPdfProcessingIssues(boolean warnPdfProcessingIssues) {
		this.warnPdfProcessingIssues = warnPdfProcessingIssues;
	}

	public boolean isWarnPdfProcessingIssues() {
		return warnPdfProcessingIssues;
	}

	private final DocPositionTracker docPositionTracker = new DocPositionTracker();
    
    public class DocPositionTracker {
    	private int bodyParagraphsCounter;
		private int bodyWordsCounter;

		/**
    	 * @return true if the extraction is within the body first 1-2 pages (estimation)
    	 */
    	public boolean isWithinDocPreamble() {
    		// TODO Auto-generated method stub
    		
    		return bodyParagraphsCounter < 30 && bodyWordsCounter < 1000;
    	}

    	public void resetDocPosition() {
    		bodyParagraphsCounter = 0;
    		bodyWordsCounter = 0;
    	}

    	public void updateDocPosition(final int paragraphWordCount) {
    		if (paragraphWordCount > 0) {
    			bodyParagraphsCounter++;
    		}
    		// rough estimation to number or words processed.
    		bodyWordsCounter += paragraphWordCount;
    	}
		public int getBodyParagraphsCounter() {
			return bodyParagraphsCounter;
		}
		public int getBodyWordsCounter() {
			return bodyWordsCounter;
		}
    }		// class DocPositionTracker

    public void process(final PDDocument document) throws IOException {
            // Extract text using a dummy Writer as we override the
            // key methods to output to the given content handler.
    	
//            final ClaPdfProcessor pdfProcessor = new ClaPdfProcessor(wordFile, config, sbOrigText, sbText, nGramTokenizerHashed, hashedTokensConsumer);
    	
            initOperatorsConfiguration();
            
            /*
             * PDF document processing phases:
             * - Process pages (see below)
             * - Extract PVs (can extract after all pages are processed or after 4 pages are processed to get constant memory consumption)
             * 
             * Page processing:
             * - Extract graphic objects to get all lines and rectangles.
             * - Extract images and objects (later)
             * - Extract page runs
             * - Process runs:
             * 		+ split run that cross table boundaries (compare with vertical lines)
             * 		+ identify and split underlined runs: look for horizontal line that is near text bottom and 
             * 		  start/end within the text boundaries. Then identify text that matches the line start/end.
             * 		* Limitation: handle rotated text ...
             * - Header/footer processing:
             * 		+ In a dedicated collection, collect all runs within specific range from the top/bottom (e.g. 3" = 144.0)
             * 		+ Once 4 pages are processed (or at the end), identify runs with identical position and text. 
             * 		  These matches should set the top/bottom virtual margin of the page. All paragraphs/runs above/below the margins are set as header/body/footer.
             * 		+ Header/footer should be de-duplicated based on content. First version can take process 'as is' header/footer for pages 1-3.
             * 		* Limitations: identify 'page' field (later...)
             * - Join paragraphs:
             * 		+ Table cells
             * 		+ Similar style + indentation + line spacing
             * 		+ Body paragraphs that crosses page boundaries (after header/footer processing)
             * - Extract paragraph style (style of largest run, similar to Word)
             * - Get style signature:
             * 		+ Font, Left/right margins
             * 		* Challenges: indentation, justification, identify page left/right margins.
             * - Identify paragraph labels and letter-heads (similar to Word).
             *  
             */
            this.writeText(document, new Writer() {
                @Override public void write(final char[] cbuf, final int off, final int len) {}
                @Override public void flush() {}
                @Override public void close() {}
            });
    }

	private void initOperatorsConfiguration() {
		registerOperatorProcessor("rg", new SetNonStrokingRGBColor());
		registerOperatorProcessor("RG", new SetStrokingRGBColor());
		registerOperatorProcessor("G", new SetStrokingGrayColor());
		registerOperatorProcessor("g", new SetNonStrokingGrayColor());
		registerOperatorProcessor("CS", new SetStrokingColorSpace());
		registerOperatorProcessor("cs", new SetNonStrokingColorSpace());
		registerOperatorProcessor("K", new SetStrokingCMYKColor());
		registerOperatorProcessor("k", new SetNonStrokingCMYKColor());
		registerOperatorProcessor("SC", new SetStrokingColor());
		registerOperatorProcessor("sc", new SetNonStrokingColor());
		registerOperatorProcessor("SCN", new SetStrokingColor());
		registerOperatorProcessor("scn", new SetNonStrokingColor());
	}

    private final WordFile wordFile;
    private final ClaPDFParserConfig config;

	private int paragraphCounter;
	private int pageCounter;
	
	private final PageDrawer pageDrawer;		// Used to extract page graphics - mainly lines

	private final Appendable sbOrigText;
	private final Appendable sbText;

	private final HashedTokensConsumer hashedTokensConsumer;
    
    ClaPdfProcessor(final WordFile wordFile, final ClaPDFParserConfig config,
					final Appendable sbOpenText, final Appendable sbHashedText,
					final NGramTokenizerHashed nGramTokenizerHashed,
					final HashedTokensConsumer hashedTokensConsumer) throws IOException {
        this.config = config;
        this.wordFile = wordFile;
        this.sbOrigText = sbOpenText;
        this.sbText = sbHashedText;
        this.nGramTokenizerHashed = nGramTokenizerHashed;
        this.hashedTokensConsumer = hashedTokensConsumer;
        
        pageDrawer = new ClaPdfPageDrawer(this);
        
        setForceParsing(true);
        setSortByPosition(config.getSortByPosition());
        if (config.getEnableAutoSpace()) {
            setWordSeparator(" ");
        } else {
            setWordSeparator("");
        }
        // TODO: maybe expose these setting as well:
        //setAverageCharTolerance(1.0f);
        //setSpacingTolerance(1.0f);
        setSuppressDuplicateOverlappingText(config.getSuppressDuplicateOverlappingText());
    }

    void extractBookmarkText(final WordFile wordFile, 
    		final Appendable sbOpenText,
    		final Appendable sbHashedText,
    		final NGramTokenizerHashed nGramTokenizerHashed, 
    		final HashedTokensConsumer hashedTokensConsumer) {
    	
        final PDDocumentOutline outline = document.getDocumentCatalog().getDocumentOutline();
        if (outline != null) {
            extractBookmarkText(outline,
            		wordFile, 
        			sbOpenText,
        			sbHashedText,
        			nGramTokenizerHashed, 
        			hashedTokensConsumer);
        }
    }
    
    private static final String INVOKE_OPERATOR = "Do";
    /*
     * Handle special Operators: Images, fields, etc.
     */
    @Override
	protected void processOperator( final PDFOperator operator, final List<COSBase> arguments ) throws IOException
    {
        final String operation = operator.getOperation();
        if (logger.isTraceEnabled()) {
			logger.trace("OP> {}: {}", operation,(arguments==null)?"null":arguments.toString());
		}
        if( INVOKE_OPERATOR.equals(operation) ) {
            final COSName objectName = (COSName)arguments.get( 0 );
            final Map<String, PDXObject> xobjects = getResources().getXObjects();
            final PDXObject xobject = xobjects.get( objectName.getName() );
            if( xobject instanceof PDXObjectImage ) {
                handleImage(objectName, (PDXObjectImage)xobject);
            }
            else if(xobject instanceof PDXObjectForm) {
                // save the graphics state
                getGraphicsStack().push( (PDGraphicsState)getGraphicsState().clone() );
                final PDPage page = getCurrentPage();
                
                final PDXObjectForm form = (PDXObjectForm)xobject;
				if (warnPdfProcessingIssues || !firstWarnPrinted) {
					logger.warn("Unhandled form object found - check out.");
					firstWarnPrinted = true;
				}
				else {
					logger.info("Unhandled form object found - check out.");
				}
            	
                final COSStream invoke = (COSStream)form.getCOSObject();
                PDResources pdResources = form.getResources();
                if(pdResources == null) {
                    pdResources = page.findResources();
                }
                // if there is an optional form matrix, we have to
                // map the form space to the user space
                final Matrix matrix = form.getMatrix();
                if (matrix != null) {
                    final Matrix xobjectCTM = matrix.multiply( getGraphicsState().getCurrentTransformationMatrix());
                    getGraphicsState().setCurrentTransformationMatrix(xobjectCTM);
                }
                processSubStream( page, pdResources, invoke );
                
                // restore the graphics state
                setGraphicsState( getGraphicsStack().pop() );
            }
        }
        else {
            super.processOperator( operator, arguments );
        }
    }

	private void handleImage(final COSName objectName, final PDXObjectImage image ) {
		int resourcesAquiredMB = 0;
		if (isAcquireImageMemory()) {
			if (ingestionResourceManager==null) {
				logger.error("Null ingestionResourceManager in PDF processor");
			}
			else {
				int width = image.getWidth();
				int height = image.getHeight();
				int resources = image.getPDStream().getLength();  // Estimate memory required for image
				if (width > 0 && height > 0) {
					resources = Integer.max(resources / 4, width * height);
				}
				resources = (int) FileSizeUnits.BYTE.toMegabytes(resources * 4);  // Convert from pixels to memory MB
				if (resources > 10) {   // No point to allocate for small images
					boolean resourcesAquired = ingestionResourceManager.acquire(resources);
					if (resourcesAquired) {
						resourcesAquiredMB = resources;
						if (logger.isDebugEnabled()) {
							logger.debug("Image resources acquired {}. {} available", resources, ingestionResourceManager.getAvailable());
						} else {
							if (logger.isDebugEnabled()) {
								logger.debug("Image resources NOT acquired {}. {} available", resources, ingestionResourceManager.getAvailable());
							}
						}
					}
				} else {
					logger.trace("Skipped image resources acquision. Length={} WxH {}x{}", image.getPDStream().getLength(), width, height);
				}
			}
		}

		try {
			final PDPage page = getCurrentPage();
			final int imageWidth = image.getWidth();
			final int imageHeight = image.getHeight();
			final double pageHeight = getPageMediaBox(page).getHeight();

			final Matrix ctmNew = getGraphicsState().getCurrentTransformationMatrix();
			final float yScaling = ctmNew.getYScale();
			float angle = (float) Math.acos(ctmNew.getValue(0, 0) / ctmNew.getXScale());
			if (ctmNew.getValue(0, 1) < 0 && ctmNew.getValue(1, 0) > 0) {
				angle = (-1) * angle;
			}
			ctmNew.setValue(2, 1, (float) (pageHeight - ctmNew.getYPosition() - Math.cos(angle) * yScaling));
			ctmNew.setValue(2, 0, (float) (ctmNew.getXPosition() - Math.sin(angle) * yScaling));
			// because of the moved 0,0-reference, we have to shear in the opposite direction
			ctmNew.setValue(0, 1, (-1) * ctmNew.getValue(0, 1));
			ctmNew.setValue(1, 0, (-1) * ctmNew.getValue(1, 0));
			final AffineTransform ctmAT = ctmNew.createAffineTransform();
			ctmAT.scale(1f / imageWidth, 1f / imageHeight);

			final float imageXScale = ctmNew.getXScale();
			final float imageYScale = ctmNew.getYScale();

			if (isAcquireImageMemory()) {
				// Getting image signature requires reading image into memory. Resources needs to be preserved.
				// TODO: Should add 'image style' signatures regardless to image bytes signature
				final String imageSignature = getImageSignature(image, objectName.getName(),
						ctmNew.getXPosition(),
						ctmNew.getYPosition(),
						imageWidth,
						imageHeight,
						imageXScale,
						imageYScale);
				if (imageSignature != null) {
					imageSignatures.add(imageSignature);
				}
			}
			String imageFile = null;
			String imageParams = null;
			if (image.getMetadata() != null) {
				try {
					imageFile = image.getMetadata().getFile().getFile();
				} catch (final Exception e) {
				}
				try {
					imageParams = image.getMetadata().getDecodeParms().toString();
				} catch (final Exception e) {
				}
			}
			currClaPage.addImage(objectName.getName(),
					ctmNew.getXPosition(), ctmNew.getYPosition(),
					imageWidth, imageHeight,
					imageXScale, imageYScale,
					imageFile, imageParams);

			if (logger.isTraceEnabled()) {
				logger.trace("PdfItem: image '{}'. position={},{} size={}x{} scale={}x{} file={} params={}",
						objectName.getName(),
						ctmNew.getXPosition(),
						ctmNew.getYPosition(),
						imageWidth,
						imageHeight,
						imageXScale,
						imageYScale,
						imageFile, imageParams);

			/*
			try {
				// retrieve image
				final BufferedImage bi = image.getRGBImage();
				String name = objectName.getName();
				if (name.isEmpty()) {
					name = "SavedPdfImage_"+System.currentTimeMillis();
				}
				name = "logs/" + name + ".png";
				final File outputfile = new File(name);
				ImageIO.write(bi, "png", outputfile);
				logger.debug("Saved image to file {}", outputfile.getAbsolutePath().toString());
			} catch (final IOException e) {
				logger.debug("Couldn't save image. {}", e, e);
			}
			*/
			}
			image.clear();        // reduce memory footprint
		}
        catch (final Exception e) {
		}
        finally {
			if (resourcesAquiredMB > 0) {
				ingestionResourceManager.release(resourcesAquiredMB);
				if (logger.isDebugEnabled()) {
					logger.debug("Image resources released {}. {} available", resourcesAquiredMB, ingestionResourceManager.getAvailable());
				}
			}
		}
	}


    private String getImageSignature(final PDXObjectImage image, final String name,
			final float xPosition, final float yPosition, final int imageWidth, final int imageHeight,
			final float imageXScale, final float imageYScale) {

    	try {
    		final String sig = "P"+ NGramTokenizerHashed.MD5Long(image.getPDStream().getByteArray());
    		return sig;
    	} catch (IOException | NoSuchAlgorithmException e) {
			if (warnPdfProcessingIssues || !firstWarnPrinted) {
				logger.warn("Failed handlePictureCharacterRun in file {}. Error is {}", wordFile.getFileName(), e);
				firstWarnPrinted = true;
			}
			else {
				logger.info("Failed handlePictureCharacterRun in file {}. Error is {}", wordFile.getFileName(), e);
			}
		}
    	return null;
    }

	private PDRectangle getPageMediaBox(final PDPage page) {
		return (page!=null && page.getMediaBox()!=null)?page.getMediaBox():new PDRectangle(new BoundingBox(0f,0f,(float)defaultPageWidth,(float)defaultPageHeight));
	}

	public boolean isTokenMatcherActive() {
		return tokenMatcherActive;
	}
	public void setTokenMatcherActive(final boolean tokenMatcherActive) {
		this.tokenMatcherActive = tokenMatcherActive;
	}

	public boolean isPatternSearchActive() {
		return patternSearchActive;
	}
	public void setPatternSearchActive(final boolean patternSearchActive) {
		this.patternSearchActive = patternSearchActive;
	}

	void extractBookmarkText(final PDOutlineNode bookmark, 
			final WordFile wordFile, 
			final Appendable sbOpenText,
			final Appendable sbHashedText,
			final NGramTokenizerHashed nGramTokenizerHashed, 
			final HashedTokensConsumer hashedTokensConsumer) {
		
    	logger.trace("Para-end");
        PDOutlineItem current = bookmark.getFirstChild();
        if (current != null) {
            ////handler.startElement("ul");
            while (current != null) {
                //handler.startElement("li");
                //handler.characters(current.getTitle());
                //handler.endElement("li");
                // Recurse:
                extractBookmarkText(current,
                		wordFile, 
            			sbOpenText,
            			sbHashedText,
            			nGramTokenizerHashed, 
            			hashedTokensConsumer);
                current = current.getNextSibling();
            }
        }
    }

    @Override
    protected void startDocument(final PDDocument pdf) throws IOException {
    	docStart = true;
    	logger.trace("Doc-start");
    }

    @Override
    protected void endDocument(final PDDocument pdf) throws IOException {
    	docEnd = true;
    	logger.trace("Doc-end");

    	docPages.forEach(p->p.processHeaderFooter(docPages));
    	
    	// Process potential title styles stats to allow final decision of title paragraphs during extraction
    	final int commonFontSize = fontSizeStats.getCommonFontSize();
    	final Map<ClaPdfPage.PStyle, Long> titlesCounts = docTitles.stream().collect(Collectors.groupingBy(ClaPdfPage.PParagraph::getParagraphStyle,Collectors.counting()));
    	final Map<ClaPdfPage.PStyle, Long> titlesCountsFiltered = titlesCounts.entrySet().stream().filter(e->(e.getValue()>=titleStyleMinRepeatCount && e.getKey().getFontScore() > commonFontSize))
    			.collect(Collectors.toMap(e->e.getKey(),e->e.getValue()));
    	final List<Integer> titlesFontSizeScores = titlesCountsFiltered.keySet().stream().map(ClaPdfPage.PStyle::getFontScore)
    			.collect(Collectors.toSet()).stream().sorted().collect(Collectors.toList());
    	final Map<Integer,Integer> titlesFontScoreToLevel = IntStream.range(0, titlesFontSizeScores.size()).mapToObj(i->Integer.valueOf(i))
    			.collect(Collectors.toMap(i->titlesFontSizeScores.get(i), i->(titlesFontSizeScores.size()- i))); 

    	if (logger.isDebugEnabled()) {
    		logger.debug("PDF Sorted fonts: {}", 
					((Stream<String>)(fontSizeStats.getFontNameIndexMap().entrySet().stream()
    				.sorted((e1,e2)->(e1.getValue()-e2.getValue()))
    				.map(e->(e.getKey().concat(":")+e.getValue())))).collect(Collectors.joining(",")));
    	}
    	
    	processImagesSignatures(wordFile);
    	
    	docPositionTracker.resetDocPosition();
    	// Must be last page processing since pages' resources are cleared after being processed to reduce memory footprint
    	docPages.forEach(p -> p.extract(wordFile,
    			docPositionTracker, 
    			sbOrigText,
    			sbText,
    			titlesFontScoreToLevel, 
    			commonFontSize, 
    			nGramTokenizerHashed, 
    			hashedTokensConsumer));
    	
        try {
            // Extract text for any bookmarks:
            extractBookmarkText(wordFile, 
        			sbOrigText,
        			sbText,
        			nGramTokenizerHashed, 
        			hashedTokensConsumer);
            
            extractEmbeddedDocuments(pdf,
            		wordFile, 
        			sbOrigText,
        			sbText,
        			titlesFontScoreToLevel, 
        			commonFontSize, 
        			nGramTokenizerHashed, 
        			hashedTokensConsumer);
            
            //extract acroform data at end of doc
            if (config.getExtractAcroFormContent()){
                extractAcroForm(pdf,
                		wordFile, 
            			sbOrigText,
            			sbText,
            			nGramTokenizerHashed, 
            			hashedTokensConsumer);
             }
            //handler.endDocument();
        } catch (final Exception e){
           throw new IOExceptionWithCause("Unable to end a document", e);
        }
    }

    private void processImagesSignatures(final WordFile wf) {
		wf.histogramAddStrItems(PVType.PV_Pictures, imageSignatures);
		
		final long scanedPages = docPages.stream().filter(page->page.isScannedPage()).count();
		if (scanedPages>0) {
			logger.debug("Scanned pages: {} out of {} in {}",
					scanedPages, docPages.size(), wf.getFileName());
		}
		if (scanedPages == docPages.size()) {
			wf.setScannedDoc(true);
		}
	}
    
    @Override
	protected void processPage( final PDPage page, final COSStream content ) throws IOException
    {
    	if (pdfMaxPagesToProcess > 0 && getCurrentPageNo() > pdfMaxPagesToProcess) {
    		if (getCurrentPageNo() == (pdfMaxPagesToProcess+1)) {
    			logger.info("PDF reached page limit {}. Extra pages ignored", pdfMaxPagesToProcess);
    		}
            page.clear();		// should be last access to the original doc page. Free resources to reduce memory footprint
			return;
    	}
    	super.processPage(page, content);
    }

	@Override
    protected void startPage(final PDPage page) throws IOException {
    	incrementPageCounter();
    	final PDRectangle mb = getPageMediaBox(page);
    	if (logger.isTraceEnabled()) {
			logger.trace("Page-start #{}/{} rect=({},{}) ({},{}) ({}x{})", 
					pageCounter,
					getCurrentPageNo(),
					mb.getLowerLeftX(),
					mb.getLowerLeftY(),
					mb.getUpperRightX(),
					mb.getUpperRightY(),
					mb.getWidth(),
					mb.getHeight());
		}
        
        processPageGraphics(page);

        if (currClaPage != null) {
        	logger.error("Wrong page handling");
        }
        currClaPage = new ClaPdfPage(getCurrentPageNo(), mb.getWidth(), mb.getHeight(), pageGraphics, 
        		docTitles, docHeadings, fontSizeStats, tokenMatcherActive, patternSearchActive,
				processSpaceCorruption, convertWordSeparatorToSpace,
				debugTextExtraction);

		// @@@ writeParagraphStart();
    }

    @SuppressWarnings("SuspiciousNameCombination")
	private void processPageGraphics(final PDPage page) {
    	pageGraphics.reset();
    	
        final PDRectangle cropBox = page.findCropBox();
        final Dimension drawDimension = cropBox.createDimension();
        final int rotation = page.findRotation();
        Dimension pageDimension;
        if (rotation == 90 || rotation == 270) {
            pageDimension = new Dimension(drawDimension.height, drawDimension.width);
        }
        else {
            pageDimension = drawDimension;
        }
        final BufferedImage img = new BufferedImage(100, 200, BufferedImage.TYPE_INT_RGB);
        final Graphics2D g = img.createGraphics();
        try {
			logger.trace("Processing page graphics ...");
			pageDrawer.drawPage( g, page, drawDimension );
			logger.trace("Processing graphics done");
			finalizeGraphicExtraction();
		} catch (Exception e) {
			logger.warn("Error during page graphics processing: {}",e, e);
		}
	}
    
	private void finalizeGraphicExtraction() {
		// TODO Auto-generated method stub
		
	}

	@Override
    protected void endPage(final PDPage page) throws IOException {
    	final PDRectangle mb = getPageMediaBox(page);
    	if (logger.isTraceEnabled()) {
			logger.trace("Page-end #{}/{} rect=({},{}) ({},{}) ({}x{})", 
					pageCounter,
					getCurrentPageNo(),
					mb.getLowerLeftX(),
					mb.getLowerLeftY(),
					mb.getUpperRightX(),
					mb.getUpperRightY(),
					mb.getWidth(),
					mb.getHeight());
		}

        try {
        	writeParagraphEnd();
        	
        	/*
        	 * Annotation processing - not for now
        	 * 
            // TODO: remove once PDFBOX-1143 is fixed:
            if (config.getExtractAnnotationText()) {
                for(final Object o : page.getAnnotations()) {
                    if( o instanceof PDAnnotationLink ) {
                        final PDAnnotationLink annotationlink = (PDAnnotationLink) o;
                        if (annotationlink.getAction()  != null) {
                            final PDAction action = annotationlink.getAction();
                            if( action instanceof PDActionURI ) {
                                final PDActionURI uri = (PDActionURI) action;
                                final String link = uri.getURI();
                                if (link != null) {
                                    //handler.startElement("div", "class", "annotation");
                                    //handler.startElement("a", "href", link);
                                    //handler.endElement("a");
                                    //handler.endElement("div");
                                }
                             }
                        }
                    }

                    if (o instanceof PDAnnotationMarkup) {
                        final PDAnnotationMarkup annot = (PDAnnotationMarkup) o;
                        final String title = annot.getTitlePopup();
                        final String subject = annot.getSubject();
                        final String contents = annot.getContents();
                        // TODO: maybe also annot.getRichContents()?
                        if (title != null || subject != null || contents != null) {
                            //handler.startElement("div", "class", "annotation");

                            if (title != null) {
                                //handler.startElement("div", "class", "annotationTitle");
                                //handler.characters(title);
                                //handler.endElement("div");
                            }

                            if (subject != null) {
                                //handler.startElement("div", "class", "annotationSubject");
                                //handler.characters(subject);
                                //handler.endElement("div");
                            }

                            if (contents != null) {
                                //handler.startElement("div", "class", "annotationContents");
                                //handler.characters(contents);
                                //handler.endElement("div");
                            }

                            //handler.endElement("div");
                        }
                    }
                }
            }
            //handler.endElement("div");
        */
        } catch (final Exception e) {
            throw new IOExceptionWithCause("Unable to end a page", e);
        }

        currClaPage.processPage();
        addPage(currClaPage);
        currClaPage = null;
        page.clear();		// should be last access to the original doc page. Free resources to reduce memory footprint
    }

    private void addPage(final ClaPdfPage claPage) {
    	docPages.add(claPage);	
	}

	@Override
    protected void writeParagraphStart() throws IOException {
        // TODO: remove once PDFBOX-1130 is fixed
        // TODO: remove once PDFBOX-2160 is fixed
        if (inParagraph) {
        	logger.trace("...");
            // Close last paragraph
            writeParagraphEnd();
        }
    	logger.trace("Para-start#{}", paragraphCounter);
//    	assert !inParagraph;
        inParagraph = true;
        try {
            //handler.startElement("p");
        } catch (final Exception e) {
            throw new IOExceptionWithCause("Unable to start a paragraph", e);
        }
        
    	currClaPage.startParagraph();
    }

    @Override
    protected void writeParagraphEnd() throws IOException {
        // TODO: remove once PDFBOX-1130 is fixed
    	logger.trace("Para-end #{}", paragraphCounter);
        if (!inParagraph) {
        	logger.trace("writeParagraphEnd...");
            writeParagraphStart();
        }
//        assert inParagraph;
        inParagraph = false;
        try {
        	currClaPage.processParagraph();
            //handler.endElement("p");
        } catch (final Exception e) {
            throw new IOExceptionWithCause("Unable to end a paragraph", e);
        }
    }

    double debLastMinX = 0d;
    double debLastMinY = 0d;
    double debLastMaxX = 0d;
    double debLastMaxY = 0d;

	private boolean processSpaceCorruption = false;
	private boolean convertWordSeparatorToSpace = true;

	@Override
    protected void writeString(final String text, final List<TextPosition> textPositions) throws IOException
    {
    	/*
    	 * Requirements:
    	 * - identify table cells (by sorting paragraphs)
    	 * - Underlined text (difficult) - implement PageDrawer handler for path/stroke operators and look for matching horizontal lines.
    	 * - Header/footer (difficult)
    	 * - extract paragraph margins, first-line-indentation and justification type.
    	 * 
    	 * Suggested model:
    	 *  - Page -> PParagraph -> Line -> Run
    	 *  - Sort paragraphs to join cross page paragraphs (no table support) - 
    	 *  	process last paragraph of a page after first page paragraph is processed.
    	 *  - Sort paragraphs to detect tables (paragraphs that are vertically overlapping).
    	 */
        writeString(text);
        
        // Ignore empty strings
        if (text.trim().isEmpty()) {
        	logger.trace("Ignore blank string of size {}", textPositions.size());
        	return;
        }

        if (logger.isTraceEnabled()) {
        	try {
	        	final double minX = textPositions.stream().mapToDouble(TextPosition::getX).min().orElse(0.0);
	        	final double maxX = textPositions.stream().mapToDouble(tp->tp.getX()+tp.getWidth()).max().orElse(0.0);
	        	final double minY = textPositions.stream().mapToDouble(tp->tp.getY()-tp.getHeight()).min().orElse(0.0);
	        	final double maxY = textPositions.stream().mapToDouble(TextPosition::getY).max().orElse(0.0);
	
	        	final String fonts = textPositions.stream().map(tp->tp.getFont().getBaseFont()).collect(Collectors.toSet())
	        			.stream().collect(Collectors.joining("|"));
	        	final String fontSzs = textPositions.stream().map(TextPosition::getFontSize).collect(Collectors.toSet())
	        			.stream().map(String::valueOf).collect(Collectors.joining("|"));
	        	final String fontSzPts = textPositions.stream().map(TextPosition::getFontSizeInPt).collect(Collectors.toSet())
	        			.stream().map(String::valueOf).collect(Collectors.joining("|"));
	        	final String xScales = textPositions.stream().map(TextPosition::getXScale).collect(Collectors.toSet())
	        			.stream().map(String::valueOf).collect(Collectors.joining("|"));
	        	final String yScales = textPositions.stream().map(TextPosition::getYScale).collect(Collectors.toSet())
	        			.stream().map(String::valueOf).collect(Collectors.joining("|"));
	        	final String colors = textPositions.stream().map(tp->currClaPage.getColorForTextPosition(tp, false))	// don't clear color map in the process
	        			.filter(Objects::nonNull).map(Color::toString)
	        			.collect(Collectors.toSet())
	        			.stream().collect(Collectors.joining("|"));
	        	final String fontWeights = textPositions.stream().filter(tp -> tp.getFont().getFontDescriptor() != null)
	        			.map(tp->tp.getFont().getFontDescriptor().getFontWeight())
	        			.collect(Collectors.toSet())
	        			.stream().map(String::valueOf).collect(Collectors.joining("|"));
	        	final String fontItalicAngles = textPositions.stream().filter(tp -> tp.getFont().getFontDescriptor() != null)
	        			.map(tp->tp.getFont().getFontDescriptor().getItalicAngle())
	        			.collect(Collectors.toCollection(TreeSet::new)).stream().map(String::valueOf).collect(Collectors.joining("|"));

	        	logger.trace(String.format("tpSize=%d/%d fSz=%s/%s fw=%s fIa=%s avgX=%.3f rect=%.3f,%.3f (%.3fx%.3f) dStart(%.3f,%.3f) dCont(%.3f,%.3f) scales=%s,%s colorSet=%s fonts=%s string<%s> ", 
	        			text.length(), textPositions.size(),
	        			fontSzs, fontSzPts,
	        			fontWeights,
	        			fontItalicAngles,
	        			(maxX-minX)/text.length(),
	        			minX, minY, maxX-minX, maxY-minY,
	        			minX-debLastMinX, minY-debLastMinY,
	        			minX-debLastMaxX, minY-debLastMaxY,
	        			//        		minX-debLastMaxX, debLastMinY-maxY,
	        			xScales, yScales,
	        			colors,
	        			fonts,
	        			text));

	        	debLastMinX = minX;	// @@@ Deb
	        	debLastMinY = minY;	// @@@ Deb
	        	debLastMaxX = maxX;	// @@@ Deb
	        	debLastMaxY = maxY;	// @@@ Deb
        	} catch (final Exception e) {}
        }

        // Process Runs
    	currClaPage.writeText(textPositions);
    }
    
    @Override
    protected void writeString(final String text) throws IOException {
    	logger.trace("writeString<{}>", text);
//    	try {
//            //handler.characters(text);
//        } catch (final Exception e) {
//            throw new IOExceptionWithCause(
//                    "Unable to write a string: " + text, e);
//        }
    }

    @Override
    protected void writeCharacters(final TextPosition text) throws IOException {
    	logger.trace("writeChars<{}>", text.getCharacter());
//        try {
//            //handler.characters(text.getCharacter());
//        } catch (final Exception e) {
//            throw new IOExceptionWithCause(
//                    "Unable to write a character: " + text.getCharacter(), e);
//        }
    }

    @Override
    protected void writeWordSeparator() throws IOException {
    	logger.trace("WS");
    	
    	currClaPage.addWordSeparator();
    	
//        try {
//            //handler.characters(getWordSeparator());
//        } catch (final Exception e) {
//            throw new IOExceptionWithCause(
//                    "Unable to write a space character", e);
//        }
    }

    @Override
    protected void writeLineSeparator() throws IOException {
    	logger.trace("LineSeparator");
    	
    	currClaPage.addLineSeparator();

//        try {
//            //handler.newline();
//        } catch (final Exception e) {
//            throw new IOExceptionWithCause(
//                    "Unable to write a newline character", e);
//        }
    }
    
    @Override
	protected void writeParagraphSeparator()throws IOException
    {
    	logger.trace("ParagraphSeparator");
    	
    	// Ignore default action (paragraph end/start) to overcome bugs in pdf-box paragraph detection logic.
//    	super.writeParagraphSeparator();
    }
    
    
    private void extractEmbeddedDocuments(final PDDocument document, 
    		final WordFile wordFile, 
    		final Appendable sbOpenText,
    		final Appendable sbHashedText,
    		final Map<Integer, Integer> titlesFontScoreToLevel, 
    		final int commonFontSize, 
    		final NGramTokenizerHashed nGramTokenizerHashed, 
    		final HashedTokensConsumer hashedTokensConsumer) throws IOException {
    	
        final PDDocumentCatalog catalog = document.getDocumentCatalog();
        final PDDocumentNameDictionary names = catalog.getNames();
        if (names == null){
            return;
        }
        final PDEmbeddedFilesNameTreeNode embeddedFiles = names.getEmbeddedFiles();

        if (embeddedFiles == null) {
            return;
        }

        //EmbeddedDocumentExtractor embeddedExtractor = context.get(EmbeddedDocumentExtractor.class);
        //if (embeddedExtractor == null) {
        //    embeddedExtractor = new ParsingEmbeddedDocumentExtractor(context);
        //}

        final Map<String, COSObjectable> embeddedFileNames = embeddedFiles.getNames();
        //For now, try to get the embeddedFileNames out of embeddedFiles or its kids.
        //This code follows: pdfbox/examples/pdmodel/ExtractEmbeddedFiles.java
        //If there is a need we could add a fully recursive search to find a non-null
        //Map<String, COSObjectable> that contains the doc info.
        if (embeddedFileNames != null){
            processEmbeddedDocNames(embeddedFileNames,
            		wordFile, 
        			sbOpenText,
        			sbHashedText,
        			nGramTokenizerHashed, 
        			hashedTokensConsumer);
        } else {
            final List<PDNameTreeNode> kids = embeddedFiles.getKids();
            if (kids == null){
                return;
            }
            for (final PDNameTreeNode n : kids){
                final Map<String, COSObjectable> childNames = n.getNames();
                if (childNames != null){
                    processEmbeddedDocNames(childNames,
                    		wordFile, 
                			sbOpenText,
                			sbHashedText,
                			nGramTokenizerHashed, 
                			hashedTokensConsumer);
                }
            }
        }
    }

    private void processEmbeddedDocNames(final Map<String, COSObjectable> embeddedFileNames, 
    		final WordFile wordFile, 
    		final Appendable sbOpenText,
    		final Appendable sbHashedText,
    		final NGramTokenizerHashed nGramTokenizerHashed, 
    		final HashedTokensConsumer hashedTokensConsumer) throws IOException {
    	
        if (embeddedFileNames == null){
            return;
        }
        for (final Map.Entry<String,COSObjectable> ent : embeddedFileNames.entrySet()) {
            final PDComplexFileSpecification spec = (PDComplexFileSpecification) ent.getValue();
            final PDEmbeddedFile file = spec.getEmbeddedFile();
            
            if (file!=null && file.getFile()!=null) {
				logger.debug("PdfItem: Embedded doc: {}/{} size {}/{}", file.getFile().getFile(), file.getSubtype(), file.getSize(), file.getLength());
			}

            //final Metadata metadata = new Metadata();
            // TODO: other metadata?
            //metadata.set(Metadata.RESOURCE_NAME_KEY, ent.getKey());
            //metadata.set(Metadata.CONTENT_TYPE, file.getSubtype());
            //metadata.set(Metadata.CONTENT_LENGTH, Long.toString(file.getContent().size()));

            // TODO:
            /*
            if (embeddedExtractor.shouldParseEmbedded(metadata)) {
                final TikaInputStream stream = TikaInputStream.get(file.createInputStream());
                try {
                    embeddedExtractor.parseEmbedded(
                            stream,
                            new EmbeddedContentHandler(handler),
                            metadata, false);
                } finally {
                    stream.close();
                }
            }
            */
        }
    }
    
    private void extractAcroForm(final PDDocument pdf, 
    		final WordFile wordFile, 
    		final Appendable sbOpenText,
    		final Appendable sbHashedText,
    		final NGramTokenizerHashed nGramTokenizerHashed, 
    		final HashedTokensConsumer hashedTokensConsumer) throws IOException {
        //Thank you, Ben Litchfield, for org.apache.pdfbox.examples.fdf.PrintFields
        //this code derives from Ben's code
        final PDDocumentCatalog catalog = pdf.getDocumentCatalog();

        if (catalog == null) {
			return;
		}

        final PDAcroForm form = catalog.getAcroForm();
        if (form == null) {
			return;
		}
        @SuppressWarnings("rawtypes")
		final List fields = form.getFields();

        if (fields == null) {
			return;
		}
        logger.debug("PdfItem: AcroForm {} fields", fields.size());
        @SuppressWarnings("rawtypes")
        final ListIterator itr  = fields.listIterator();

        if (itr == null) {
        	return;
        }

        //handler.startElement("div", "class", "acroform");
        //handler.startElement("ol");
        while (itr.hasNext()) {
        	final Object obj = itr.next();
        	if (obj != null) {
        		if (obj instanceof PDField){
        			processAcroField((PDField)obj, 0,
        					wordFile, 
        					sbOpenText,
        					sbHashedText,
        					nGramTokenizerHashed, 
        					hashedTokensConsumer);
        		} else {
        			logger.debug("AcroForm: found non-field item. type={}, val={}", obj.getClass().toString(), obj.toString());
        		}
        	}
        }
        //handler.endElement("ol");
        //handler.endElement("div");
    }

    private void processAcroField(final PDField field, 
    		final int recurseDepth, 
    		final WordFile wordFile, 
    		final Appendable sbOpenText,
    		final Appendable sbHashedText,
    		final NGramTokenizerHashed nGramTokenizerHashed, 
    		final HashedTokensConsumer hashedTokensConsumer) throws IOException { 

    	if (recurseDepth >= MAX_ACROFORM_RECURSIONS){
			logger.debug("AcroForm: Recursion level exceeded {}", recurseDepth);
    		return;
    	}

    	addFieldString(field,
    			wordFile, 
    			sbOpenText,
    			sbHashedText,
    			nGramTokenizerHashed, 
    			hashedTokensConsumer);

    	@SuppressWarnings("rawtypes")
    	final List kids = field.getKids();
    	if(kids != null){
    		@SuppressWarnings("rawtypes")
    		final Iterator kidsIter = kids.iterator();
    		if (kidsIter == null){
    			return;
    		}
    		final int r = recurseDepth+1;
    		while(kidsIter.hasNext()){
    			final Object pdfObj = kidsIter.next();
    			if(pdfObj != null && pdfObj instanceof PDField){
    				final PDField kid = (PDField)pdfObj;
    				//recurse
    				processAcroField(kid, r,
    						wordFile, 
    	        			sbOpenText,
    	        			sbHashedText,
    	        			nGramTokenizerHashed, 
    	        			hashedTokensConsumer);
    			}
    		}
    	}
    }
    
    private void addFieldString(final PDField field, 
    		final WordFile wordFile, 
    		final Appendable sbOpenText,
    		final Appendable sbHashedText,
    		final NGramTokenizerHashed nGramTokenizerHashed, 
    		final HashedTokensConsumer hashedTokensConsumer) {
    	
    	// TODO: add fields to the text (open + body text PV). Add value as well as <field-name>:<value>
    	// TODO: get fields into the letterHead PV with special prefix (e.g. A:<fld-name>)
    	
        if (field instanceof PDSignatureField){
            handleSignature((PDSignatureField)field,
            		wordFile, 
        			sbOpenText,
        			sbHashedText,
        			nGramTokenizerHashed, 
        			hashedTokensConsumer);
            return;
        }
        // If getType() or getPartialName() are null, this item may be considered as a ghost.

        final String fName = (field.getPartialName() != null)?field.getPartialName():"";
        final String fType = (field.getFieldType() != null)?field.getFieldType():"";
        String fValue;
		try {
			fValue = (field.getValue() != null)?field.getValue():"";
		} catch (final IOException e) {
			fValue = "";
		}
		appendBodyText(fName + " " + fValue, 
				wordFile, sbOpenText, sbHashedText, nGramTokenizerHashed, hashedTokensConsumer);

		if (!fName.isEmpty()) {
			wordFile.histogramAddItem(PVType.PV_LetterHeadLabels, "A:"+fType+":"+fName);
		}
//		wordFile.addProposedTitles("P8", 100-pTitleLength, pTitle, prevParagraphWasTitle!=null);

		
        if (logger.isDebugEnabled()) {
        	final StringBuilder sb = new StringBuilder();
        	if (field.getPartialName() != null){
        		sb.append("partialName:").append(field.getPartialName());
        	}
        	if (field.getAlternateFieldName() != null){
        		sb.append(", altName:").append(field.getAlternateFieldName());
        	}
        	if (field.getFieldType() != null){
        		sb.append(", type:").append(field.getFieldType());
        	}
        	try {
        		final List<COSObjectable> kids = field.getKids();
        		if (kids != null){
        			sb.append(", #kids:").append(kids.size());
        		}
        	} catch (final IOException e) {}
        	try {
        		final String fqn = field.getFullyQualifiedName(); 
        		if (fqn != null){
        			sb.append(", QualifiedName:").append(fqn);
        		}
        	} catch (final IOException e) {}
        	try {
        		final String value = field.getValue();
        		if (value != null){
        			sb.append(", value:").append(value);
        		}
        	} catch (final IOException e) {
        		sb.append(", value:").append("<exception>");
        	}
        	logger.debug("PdfItem: Form field {}", sb.toString());
        }
    }
    
	private void appendBodyText(final String pText, 
			final WordFile wordFile, 
    		final Appendable sbOpenText,
    		final Appendable sbHashedText,
    		final NGramTokenizerHashed nGramTokenizerHashed, 
    		final HashedTokensConsumer hashedTokensConsumer) {
		
		final List<Long> bodyNGrams = nGramTokenizerHashed.getHashlist(pText, bodyTextNgramSize);
		if (bodyNGrams != null) {
			wordFile.histogramAddLongItems(PVType.PV_BodyNgrams, bodyNGrams);
			wordFile.histogramAddLongItems(PVType.PV_SubBodyNgrams, bodyNGrams);
		}
		try {
			if (patternSearchActive) {
				sbOpenText.append(pText).append("\n");
			}
			sbHashedText.append(nGramTokenizerHashed.getHash(pText,hashedTokensConsumer)).append(" ");
		}
		catch (IOException e) {
			logger.error("Failed to append content {}.", pText);
		}
	}

    private void handleSignature(final PDSignatureField sigField, 
    		final WordFile wordFile, 
    		final Appendable sbOpenText,
    		final Appendable sbHashedText,
    		final NGramTokenizerHashed nGramTokenizerHashed, 
    		final HashedTokensConsumer hashedTokensConsumer) {
       
        final PDSignature sig = sigField.getSignature();
        if (sig == null){
            return;
        }
        final Map<String, String> vals= new TreeMap<>();
        vals.put("name", sig.getName());
        vals.put("contactInfo", sig.getContactInfo());
        vals.put("location", sig.getLocation());
        vals.put("reason", sig.getReason());

        final Calendar cal = sig.getSignDate();
        if (cal != null){
            dateFormat.setTimeZone(cal.getTimeZone());
            vals.put("date", dateFormat.format(cal.getTime()));
        }
        //see if there is any data
        int nonNull = 0;
        for (final String val : vals.keySet()){
            if (val != null && ! val.equals("")){
                nonNull++;
            }
        }
        //if there is, process it
        if (nonNull > 0){
            // handler.startElement("li", parentAttributes);

            // attrs.addAttribute("", "type", "type", "CDATA", "signaturedata");
        	logger.debug("PdfItem: AcroForm signature {} ({})", vals.toString(), sigField.toString());
//                	vals.values().stream()
//                	.filter(v->v != null && !"".equals(v))
//                	.collect(Collectors.joining(", ")));
        	
            //handler.startElement("ol", attrs);
        	/*
            for (final Map.Entry<String, String> e : vals.entrySet()){
                if (e.getValue() == null || e.getValue().equals("")){
                    continue;
                }
                //attrs = new AttributesImpl();
                //attrs.addAttribute("", "signdata", "signdata", "CDATA", e.getKey());
                //handler.startElement("li", attrs);
                //handler.characters(e.getValue());
                //handler.endElement("li");
            }
            //handler.endElement("ol");
            //handler.endElement("li");
             */
        }
    }

	public int getParagraphCounter() {
		return paragraphCounter;
	}

	public void setParagraphCounter(final int paragraphCounter) {
		this.paragraphCounter = paragraphCounter;
	}

	public int getPageCounter() {
		return pageCounter;
	}

	public void setPageCounter(final int pageCounter) {
		this.pageCounter = pageCounter;
	}

	public void incrementPageCounter() {
		this.pageCounter++;
	}
	
	@Override
	protected void processTextPosition(final TextPosition text) {

		if (text.getCharacter().trim().length() > 0) {
			Color col = Color.black;
			try {
				final ColorSpace cs = this.getGraphicsState().getStrokingColor().getColorSpace().getJavaColorSpace();
				final int rm = this.getGraphicsState().getTextState().getRenderingMode();
				switch(rm) {
				case PDTextState.RENDERING_MODE_FILL_TEXT:
				case PDTextState.RENDERING_MODE_FILL_TEXT_AND_ADD_TO_PATH_FOR_CLIPPING:
					col = ClaPdfPageDrawer.claGetJavaColor(this.getGraphicsState().getNonStrokingColor());
					break;
				case PDTextState.RENDERING_MODE_STROKE_TEXT:
				case PDTextState.RENDERING_MODE_FILL_THEN_STROKE_TEXT:
				case PDTextState.RENDERING_MODE_STROKE_TEXT_AND_ADD_TO_PATH_FOR_CLIPPING:
				case PDTextState.RENDERING_MODE_FILL_THEN_STROKE_TEXT_AND_ADD_TO_PATH_FOR_CLIPPING:
					col = ClaPdfPageDrawer.claGetJavaColor(this.getGraphicsState().getStrokingColor());
					break;
				case PDTextState.RENDERING_MODE_NEITHER_FILL_NOR_STROKE_TEXT:
				case PDTextState.RENDERING_MODE_ADD_TEXT_TO_PATH_FOR_CLIPPING:
					//basic support for text rendering mode "invisible"
					col = new Color(cs, 
								    new float[]{Color.black.getRed(), Color.black.getGreen(),Color.black.getBlue()},
								    0.0f);
					break;
				default:
//					col = new Color(cs, 
//								    new float[]{Color.black.getRed(), Color.black.getGreen(),Color.black.getBlue()},
//								    1.0f);
					if (warnPdfProcessingIssues || !firstWarnPrinted) {
						logger.warn("Unexpected text rendering. text=<{}> at page {}. Val={}", text.getCharacter(), pageCounter, rm);
						firstWarnPrinted = true;
					}
					else {
						logger.info("Unexpected text rendering. text=<{}> at page {}. Val={}", text.getCharacter(), pageCounter, rm);
					}
				}
			} catch (final Exception e) {
//				col = new Color(cs, 
//					    new float[]{Color.black.getRed(), Color.black.getGreen(),Color.black.getBlue()},
//					    1.0f);
//				logger.warn("Error while extracting text <{}> at page {}: {}", text.getCharacter(), pageCounter, e.getMessage());
			}
			currClaPage.setColorForTextPosition(text, col);
		}
		super.processTextPosition(text);
	}

	public PageDrawer getDrawer() {
		return pageDrawer;
	}

	public void addRectangle(final Rectangle2D r, final Color color, final boolean filled) {
		if (logger.isTraceEnabled()) {
			logger.trace("Rectangle {} in color {} {}", shapeToString(r), shapeToString(color), (filled?"filled":"empty"));
		}
		pageGraphics.addRectangle(r, color, filled);
	}
	public void addLineHorizontal(final double x1, final double x2, final double y, final Color color) {
		if (logger.isTraceEnabled()) {
			logger.trace("H-Line {} in color {}", String.format("%.3f-%.3f,%.3f", x1, x2, y), shapeToString(color));
		}
		pageGraphics.addHorizontalLine(x1, x2, y, color);
    }
    public void addLineVertical(final double x, final double y1, final double y2, final Color color) {
    	if (logger.isTraceEnabled()) {
			logger.trace("V-Line {} in color {}", String.format("%.3f,%.3f-%.3f",x, y1, y2), shapeToString(color));
		} 
		pageGraphics.addVertivalLine(x, y1, y2, color);
    }

    private String shapeToString(final Rectangle2D r) {
    	return String.format("(%.3f,%.3f)-(%.3f,%.3f) [%.3fx%.3f]", r.getMinX(), r.getMinY(), r.getMaxX(), r.getMaxY(), r.getWidth(), r.getHeight());
    }
    private String shapeToString(final Color c) {
    	return String.format("[rgb-%d,%d,%d]", c.getRed(),c.getGreen(),c.getBlue());
    }

	public void setProcessSpaceCorruption(boolean processSpaceCorruption) {
		this.processSpaceCorruption  = processSpaceCorruption;
	}

	public boolean isConvertWordSeparatorToSpace() {
		return convertWordSeparatorToSpace;
	}

	public void setConvertWordSeparatorToSpace(boolean convertWordSeparatorToSpace) {
		this.convertWordSeparatorToSpace = convertWordSeparatorToSpace;
	}
}
