package com.cla.common.media.files.pdf;

import com.cla.common.domain.dto.histogram.ClaHistogramCollection;
import com.cla.common.domain.dto.histogram.ClaStyle;
import com.cla.common.domain.dto.histogram.ClaStyle.Justification;
import com.cla.common.domain.dto.mediaproc.word.FontStats;
import com.cla.common.domain.dto.mediaproc.word.WordFile;
import com.cla.common.domain.dto.pv.PVType;
import com.cla.common.utils.tokens.HashedTokensConsumer;
import com.cla.common.utils.tokens.NGramTokenizerHashed;
import com.cla.common.utils.tokens.TokenizerObject;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDFontDescriptor;
import org.apache.pdfbox.util.TextPosition;

import java.awt.*;
import java.io.IOException;
import java.util.*;
import java.util.List;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class ClaPdfPage {
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(ClaPdfPage.class);

	private static final Pattern injectedFieldCodePattern = Pattern.compile("(F\\d+\\s\\{.*\\})|_+\\s?");	// Note that pattern may be locale specific (start of word)
	private static final Pattern pLabelIndiactionPattern = Pattern.compile("[\t:]\\s");
	private static final Pattern pLabelPattern = Pattern.compile("[\\p{IsAlphabetic}&&[\\D]].*");	// Note that pattern may be locale specific (start of word)
	private static final Pattern pLabelSplitPattern = Pattern.compile("\\s+");
	private static final String[] paragraphLabelDelimiters = {"\t", ":", "   "};

	private static final Pattern firstRunListItemPattern = Pattern.compile("^\\s*[^\\s\\w]{1,2}\\s");
	private static final Pattern firstTokenListItemPattern = Pattern.compile("^\\s*((\\d{1,3})|\\w)([.,]((\\d{1,3})|\\w))*[.,]?\\s");
	private static final Pattern meaningfulTokenPattern = Pattern.compile("\\b([^\\d.,!@#$%^&*()\\s_+\\]/\\\\{\\}\\`]){3,}\\b");
	private static final Pattern tabCharPattern = Pattern.compile("\\t");

	private static final Pattern spacedCorruptedTextRplacementPattern = Pattern.compile("([^a-zA-Z\\s])[ \\t](?=[^ \\t][ \\t])");
	private static final Pattern spacedCorruptedTextSearchPattern = Pattern.compile("(([^a-zA-Z\\s])[ \\t](?=[^ \\t][ \\t])){3,}");

	private static final double vMarginThresholdRatio = 0.25;
	private static final int minTitleLength = 4;
	private static final int maxTitleLength = 85;
	private static final int minTitleFontScore = 10;
	private static final double minJustifiedLeftTolerance = 20;
	private static final double minJustifiedRightTolerance = 20;
	private static final double minCenteredMarginesTolerance = 36;
	private static final double minCenteredJustifiedMargines = 20;
	private static final double maxTablePageCoverage = 0.95;
	private static final double paragraphSplitSpacingDiffThreshold = 5;
	private static final int paragraphLabelMaxTokens = 4;
	private static final int titleExtractionMaxCount = 10;
	private static final short bodyTextNgramSize = 5;
	private static final short headingsTextNgramSize = 3;
	//	@Value("${word.titleExtraction.ignoreRightJustified:true}")
	private static final boolean titleExtractionIgnoreRightJustified = false;
	private static final boolean ignoreHeaderFooterTitles = true;
	private static final long minWhiteSpaceFactor = 2;
	private static final long maxWhiteSpaceFactor = 3;
	private static final long maxTabSpaceFactor = 30;
	@SuppressWarnings("unused")
	private static final double misalignedLinesGap = 10;
	
	private static final double minFontScoreSpitDiff = 2;
	private static final double minHeaderFooterWordRunRatio = 0.2;

	private static final int pageWindow = 4;	// number of pages to use for top/bottom margin processing
	
	private LinkedList<TextPosition> tpBuffer = new LinkedList<>();;
	private PParagraph prevParagraph = null;
	private PParagraph curParagraph = null;

	private final List<PWord> pageTopWords = new ArrayList<>();
	private final List<PWord> pageBottomWords = new ArrayList<>();
	private List<PParagraph> titles;
	private List<PParagraph> headings;
	private final FontStats fontSizeStats;
	private List<PImage> images = new ArrayList<>();

	private List<PParagraph> pageParagraphs = new ArrayList<>();
	
	private ClaPageGraphics pageGraphics;

	public double pageHeight;
	public double pageWidth;
	
	private double leftMargin = 0;
	private double rightMargin = 0;
	private double topMargin = 0;
	private double bottomMargin = 0;
	
	private boolean gotTitle = false;
	private int titlesSequenceCount = 0;
	private PParagraph prevParagraphWasTitle = null;

	private boolean tokenMatcherActive = false;
	private boolean patternSearchActive = false;
	private boolean processSpaceCorruption = false;
	private boolean convertWordSeparatorToSpace = true;
	private boolean debugTextExtraction = false;

	private TokenizerObject ngramTokenizerObject = null;

	public class PStyle {
		private String font;
		private float fontSize;
		private Color color; 
		private boolean bold;

		private boolean italic;
		private int underline;
		private Justification justification; 
		private int headingLevel;	// 0 = not heading
		private boolean isListItem;	// Bullet or numbered
		private int tableLevel;		// 0 = not in table	
		private int border;	// 0 = no border
		private double firstLineIndent;
		private double indentFromLeft;
		private double indentFromRight;
		private boolean isTitle = true;

		public PStyle(final String font, final float fontSize, final Color color, final boolean bold, final boolean italic, final int underline) {
			this.font = font;
			this.fontSize = fontSize;
			this.color = (color==null)?Color.BLACK:color;	// In case of null color, use default color
			this.bold = bold;
			this.italic = italic;
			this.underline = underline;
		}
		public PStyle(final PStyle other) {
			this.font = other.font;
			this.fontSize = other.fontSize;
			this.color = other.color; 
			this.bold = other.bold;
			this.italic = other.italic;
			this.underline = other.underline;
			this.justification = other.justification; 
			this.headingLevel = other.headingLevel;
			this.isListItem = other.isListItem;
			this.tableLevel = other.tableLevel;
			this.border = other.border;
			this.firstLineIndent = other.firstLineIndent;
			this.indentFromLeft = other.indentFromLeft;
			this.indentFromRight = other.indentFromRight;
		}

		public Justification getJustification() {
			return justification;
		}

		public void setJustification(final Justification justification) {
			this.justification = justification;
		}

		public int getHeadingLevel() {
			return headingLevel;
		}

		public void setHeadingLevel(final int headingLevel) {
			this.headingLevel = headingLevel;
		}

		public boolean isHeading() {
			return headingLevel>0;
		}

		public boolean isListItem() {
			return isListItem;
		}

		public void setListItem(final boolean isListItem) {
			this.isListItem = isListItem;
		}

		public int getTableLevel() {
			return tableLevel;
		}

		public void setTableLevel(final int tableLevel) {
			this.tableLevel = tableLevel;
		}

		public int getBorder() {
			return border;
		}

		public void setBorder(final int border) {
			this.border = border;
		}

		public double getFirstLineIndent() {
			return firstLineIndent;
		}

		public void setFirstLineIndent(final double firstLineIndent) {
			this.firstLineIndent = firstLineIndent;
		}

		public double getIndentFromLeft() {
			return indentFromLeft;
		}

		public void setIndentFromLeft(final double indentFromLeft) {
			this.indentFromLeft = indentFromLeft;
		}

		public double getIndentFromRight() {
			return indentFromRight;
		}

		public void setIndentFromRight(final double indentFromRight) {
			this.indentFromRight = indentFromRight;
		}

		public int getUnderline() {
			return underline;
		}
		public int setUnderline(final int underline) {
			return this.underline = underline;
		}
		public String getFont() {
			return font;
		}
		public void setFont(final String font) {
			this.font = font;
		}
		public float getFontSize() {
			return fontSize;
		}
		public void setFontSize(final float fontSize) {
			this.fontSize = fontSize;
		}
		public Color getColor() {
			return color;
		}
		public void setColor(final Color color) {
			this.color = color;
		}
		public boolean isBold() {
			return bold;
		}
		public void setBold(final boolean bold) {
			this.bold = bold;
		}
		public boolean isItalic() {
			return italic;
		}
		public void setItalic(final boolean italic) {
			this.italic = italic;
		}

		public boolean isTitle() {
			return isTitle;
		}
		public void setIsTitle(final boolean isTitle) {
			this.isTitle  = isTitle;
		}
		
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + (bold ? 1231 : 1237);
			result = prime * result + border;
			result = prime * result + ((color == null) ? 0 : color.hashCode());
			long temp;
			temp = Double.doubleToLongBits(firstLineIndent);
			result = prime * result + (int) (temp ^ (temp >>> 32));
			result = prime * result + ((font == null) ? 0 : font.hashCode());
			result = prime * result + Float.floatToIntBits(fontSize);
			result = prime * result + headingLevel;
			temp = Double.doubleToLongBits(indentFromLeft);
			result = prime * result + (int) (temp ^ (temp >>> 32));
			temp = Double.doubleToLongBits(indentFromRight);
			result = prime * result + (int) (temp ^ (temp >>> 32));
			result = prime * result + (isListItem ? 1231 : 1237);
			result = prime * result + (italic ? 1231 : 1237);
			result = prime * result + (isTitle ? 1231 : 1237);
			result = prime * result
					+ ((justification == null) ? 0 : justification.hashCode());
			result = prime * result + tableLevel;
			result = prime * result + underline;
			return result;
		}

		@Override
		public boolean equals(final Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (getClass() != obj.getClass()) {
				return false;
			}
			final PStyle other = (PStyle) obj;
			if (bold != other.bold) {
				return false;
			}
			if (border != other.border) {
				return false;
			}
			if (color == null) {
				if (other.color != null) {
					return false;
				}
			} else if (!color.equals(other.color)) {
				return false;
			}
			if (Double.doubleToLongBits(firstLineIndent) != Double
					.doubleToLongBits(other.firstLineIndent)) {
				return false;
			}
			if (font == null) {
				if (other.font != null) {
					return false;
				}
			} else if (!font.equals(other.font)) {
				return false;
			}
			if (Float.floatToIntBits(fontSize) != Float
					.floatToIntBits(other.fontSize)) {
				return false;
			}
			if (headingLevel != other.headingLevel) {
				return false;
			}
			if (Double.doubleToLongBits(indentFromLeft) != Double
					.doubleToLongBits(other.indentFromLeft)) {
				return false;
			}
			if (Double.doubleToLongBits(indentFromRight) != Double
					.doubleToLongBits(other.indentFromRight)) {
				return false;
			}
			if (isListItem != other.isListItem) {
				return false;
			}
			if (italic != other.italic) {
				return false;
			}
			if (isTitle != other.isTitle) {
				return false;
			}
			if (justification != other.justification) {
				return false;
			}
			if (tableLevel != other.tableLevel) {
				return false;
			}
			if (underline != other.underline) {
				return false;
			}
			return true;
		}
		
		@Override
		public String toString() {
			return String.format("PStyle:{font=%s(%s), fSz=%.1f%s%s%s%s, color=[%d,%d,%d] Just=[%d,%d,%d,%s]%s%s}",
								 font, getSignatureFontName(), fontSize, (bold?"B":""), (italic?"I":""), (isTitle?"T":""), (underline==0)?"":("U"+underline), 
								 (color==null)?-1:color.getRed(),(color==null)?-1:color.getGreen(),(color==null)?-1:color.getBlue(),
								 (int)indentFromLeft,(int)indentFromRight,(int)firstLineIndent,(justification==null)?"null":justification.name(),
								 (isListItem?" list":""),((tableLevel>0)?" table":""));
		}

		private String getSignatureFontName() {
			final int fontIndex = fontSizeStats.getFontNameOrderedIndex(font);
			final String res = (fontIndex<0)?font:String.format("F%03d",fontIndex);
			return res;
		}
		
		public ClaStyle getClaStyle()
		{
//			final String fontSigName = getSignatureFontName();
//			final ClaStyle cStyle = new ClaStyle("", fontSigName, 2*(int)(fontSize/2), color.getRGB(), bold, (underline!=0), 0 /*@@@*/);
			final ClaStyle cStyle = new ClaStyle("", font, fontSize, color.getRGB(), bold, (underline!=0), 0 /*@@@*/);

			cStyle.setFirstLineIndent((short) (2*(int)(firstLineIndent/4)));
			cStyle.setHeadingLevel(headingLevel);
			cStyle.setIndentFromLeft((short) (indentFromLeft/144));
			cStyle.setIndentFromRight((short) (indentFromRight/144));
			cStyle.setJustification(justification);
			cStyle.setListItem(false);
			cStyle.setSpacingBefore((short) 0);
			cStyle.setSpacingAfter((short) 0);
			cStyle.setTableLevel(tableLevel);
			
			return cStyle;
		}
		
		public String getSimpleSignature()
		{
			return this.getClaStyle().getSimpleSignature();
		}
		public String getAbsoluteSignature()
		{
			return this.getClaStyle().getAbsoluteSignatureNoFont(0,0);
		}
		
		public int getFontScore() {
			int fontScore = Math.round(fontSize);		//Convert to points?
			if (underline > 0 || bold) {
				fontScore += 2;
			}
			return fontScore;
		}
		
		public boolean isInTable() {
			return (tableLevel>0);
		}
		
	}		// PStyle

	public static class PRectangle {
		private double minX;
		private double maxX;
		private double minY;
		private double maxY;
		private double leftTolerance;
		private double bottomTolerance;

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			long temp;
			temp = Double.doubleToLongBits(bottomTolerance);
			result = prime * result + (int) (temp ^ (temp >>> 32));
			temp = Double.doubleToLongBits(leftTolerance);
			result = prime * result + (int) (temp ^ (temp >>> 32));
			temp = Double.doubleToLongBits(maxX);
			result = prime * result + (int) (temp ^ (temp >>> 32));
			temp = Double.doubleToLongBits(maxY);
			result = prime * result + (int) (temp ^ (temp >>> 32));
			temp = Double.doubleToLongBits(minX);
			result = prime * result + (int) (temp ^ (temp >>> 32));
			temp = Double.doubleToLongBits(minY);
			result = prime * result + (int) (temp ^ (temp >>> 32));
			return result;
		}
		@Override
		public boolean equals(final Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (getClass() != obj.getClass()) {
				return false;
			}
			final PRectangle other = (PRectangle) obj;
			if (Double.doubleToLongBits(bottomTolerance) != Double
					.doubleToLongBits(other.bottomTolerance)) {
				return false;
			}
			if (Double.doubleToLongBits(leftTolerance) != Double
					.doubleToLongBits(other.leftTolerance)) {
				return false;
			}
			if (Double.doubleToLongBits(maxX) != Double
					.doubleToLongBits(other.maxX)) {
				return false;
			}
			if (Double.doubleToLongBits(maxY) != Double
					.doubleToLongBits(other.maxY)) {
				return false;
			}
			if (Double.doubleToLongBits(minX) != Double
					.doubleToLongBits(other.minX)) {
				return false;
			}
			if (Double.doubleToLongBits(minY) != Double
					.doubleToLongBits(other.minY)) {
				return false;
			}
			return true;
		}
		public PRectangle(final List<TextPosition> runTps) {
			this.minX = (double) runTps.stream().mapToDouble(tp->tp.getX()).min().orElse(0.0);
			this.maxX = (double) runTps.stream().mapToDouble(tp->tp.getX()+tp.getWidth()).max().orElse(0.0);
			this.minY = (double) runTps.stream().mapToDouble(tp->tp.getY()-tp.getHeight()).min().orElse(0.0);
			this.maxY = (double) runTps.stream().mapToDouble(tp->tp.getY()).max().orElse(0.0);
			this.leftTolerance = (double) runTps.stream().mapToDouble(tp->tp.getX()).max().orElse(0.0) - this.minX;
			this.bottomTolerance = this.maxY - (double) runTps.stream().mapToDouble(tp->tp.getY()).min().orElse(0.0);
		}
		public PRectangle(final double x1, final double y1, final double x2, final double y2) {
			this.minX = Double.min(x1,x2);
			this.maxX = Double.max(x1,x2);
			this.minY = Double.min(y1,y2);
			this.maxY = Double.max(y1,y2);
			this.leftTolerance = 0;
			this.bottomTolerance = 0;
		}
		public PRectangle() {
			this(0,0,0,0);
		}
		
		public PRectangle(final PRectangle pRect) {
			set(pRect);
		}
		@Override
		public String toString() {
			return String.format("PRect:{(%.2f,%.2f)(%.2f,%.2f) LT=%.2f BT=%.2f}",minX,minY,maxX,maxY,leftTolerance,leftTolerance);
		}
		
		public void set(final PRectangle pRect) {
			this.minX = pRect.minX;
			this.maxX = pRect.maxX;
			this.minY = pRect.minY;
			this.maxY = pRect.maxY;
			this.leftTolerance = pRect.leftTolerance;
			this.bottomTolerance = pRect.bottomTolerance;
		}


		public double getMinX() {
			return minX;
		}

		public double getMaxX() {
			return maxX;
		}

		public double getMinY() {
			return minY;
		}

		public double getMaxY() {
			return maxY;
		}

		public double getWidth() {
			return maxX-minX;
		}

		public double getHeight() {
			return maxY-minY;
		}

		public double getLeftTolerance() {
			return leftTolerance;
		}

		public double getBottomTolerance() {
			return bottomTolerance;
		}
		private boolean isEmpty() {
			return (minX==0 && maxX==0 && minY==0 && maxY==0);
		}
		public PRectangle merge(final PRectangle rect) {
			if (isEmpty()) {
				set(rect);
			}
			minX = Double.min(this.minX,rect.minX);
			minY = Double.min(this.minY,rect.minY);
			maxX = Double.max(this.maxX,rect.maxX);
			maxY = Double.max(this.maxY,rect.maxY);

			return this;
		}

	}		// PRectangle

	/*
	 * Used to detect header/footer by cross-page word (text/position) commonality
	 */
	public class PWord {
		private final String text;
		private final double xPos;
		private final PStyle style;
		private final double yPos;		
		private final double height;	// not part of equals() nor hashcode()
		
		public PWord(final String text, final double xPos, final double yPos, final double height, final String font, final float fontSize, final Color color, 
				final boolean bold, final boolean italic, final int underline) {
			this.style = new PStyle(font, fontSize, color, bold, italic, underline);
			this.xPos = xPos;
			this.yPos = yPos;
			this.height = height;
	        this.text = text;
		}
		public String getText() {
			return text;
		}
		public double getxPos() {
			return xPos;
		}
		public double getyPos() {
			return yPos;
		}
		public double getHeight() {
			return height;
		}
		public PStyle getStyle() {
			return style;
		}
		@Override
		public String toString() {
			return "PWord:{text=" + text + ", xPos=" + xPos + ", yPos=" + yPos /* + ", style="+ style*/ + "}";
		}
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((style == null) ? 0 : style.hashCode());
			result = prime * result + ((text == null) ? 0 : text.hashCode());
			long temp;
			temp = Double.doubleToLongBits(xPos);
			result = prime * result + (int) (temp ^ (temp >>> 32));
			temp = Double.doubleToLongBits(yPos);
			result = prime * result + (int) (temp ^ (temp >>> 32));
			return result;
		}
		@Override
		public boolean equals(final Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (getClass() != obj.getClass()) {
				return false;
			}
			final PWord other = (PWord) obj;
			if (style == null) {
				if (other.style != null) {
					return false;
				}
			} else if (!style.equals(other.style)) {
				return false;
			}
			if (text == null) {
				if (other.text != null) {
					return false;
				}
			} else if (!text.equals(other.text)) {
				return false;
			}
			if (Double.doubleToLongBits(xPos) != Double
					.doubleToLongBits(other.xPos)) {
				return false;
			}
			if (Double.doubleToLongBits(yPos) != Double
					.doubleToLongBits(other.yPos)) {
				return false;
			}
			return true;
		}
	}
	
	public class PRun {
		private String text;
		private PStyle style;
		private PRectangle pRect;
		private int trimmedLength = -1;

		public PRun(final List<TextPosition> runTps, final String font, final float fontSize, final Color color, 
				final boolean bold, final boolean italic, final int underline) {
			
			this.style = new PStyle(font, fontSize, color, bold, italic, underline);
			this.pRect = new PRectangle(runTps);

	        this.text = runTps.stream().map(tp->tp.getCharacter()).collect(Collectors.joining());
	        this.trimmedLength = this.text.trim().length();
		}

		public String getText() {
			return text;
		}

		public void setStyle(final PStyle style) {
			this.style = style;
		}

		public PStyle getStyle() {
			return style;
		}

		public PRectangle getPRect() {
			return pRect;
		}

		public void setPRect(final PRectangle pRect) {
			this.pRect = pRect;
		}

		public int getTrimmedLength() {
			return trimmedLength;
		}

		public boolean equalStyle(final PRun other) {
			return style != null && this.style.equals(other);
		}

		@Override
		public String toString() {
			return "PdfRun:{text=<"+text+"> "+(style!=null?style.toString():"")+" "+(pRect!=null?pRect.toString():"")+"}";
		}

		public void addWordSeparator() {
			text = text + " ";
		}
		public void addTab() {
			text = text + "\t";
		}

	}		// Run
	
	public class PLine {
		private List<PRun> runsSet = new LinkedList<>();
		private String text = null;
		private PRectangle pRect = new PRectangle();
		private double spacing = 0;
		private Boolean isListedItem = null;
		
		public PLine(final List<TextPosition> tps) {
			processLineTps(tps);
		}
		
		public PLine(final PLine pLine) {
			if (runsSet==null) {
				throw new RuntimeException("Something is wrong! runSet is null");
			}
			this.runsSet.addAll(pLine.runsSet);
			this.text = null;
			this.pRect = new PRectangle(pLine.pRect);
		}

		public String getText() {
			if (text==null) {
				if (runsSet==null) {
					throw new RuntimeException("Something is wrong! runSet is null");
				}
				text = runsSet.stream().map(r->r.getText()).collect(Collectors.joining());
			}
			return text;
		}
		
		public PRectangle getpRect() {
			return pRect;
		}

		public void setpRect(final PRectangle pRect) {
			this.pRect = pRect;
		}
		
		private void processLineTps(final List<TextPosition> textPositions) {
			final PRectangle pRect = new PRectangle(textPositions);
			final List<List<TextPosition>> runsTpList = processCrossingVerticalLines(textPositions, pRect);
			for (final Iterator<List<TextPosition>> iter = runsTpList.iterator();iter.hasNext();) {
				final List<TextPosition> runItemTps = iter.next();
				
				extractLineRuns(runItemTps);

				if (iter.hasNext()) {
					// for all items but last one
					addParagraphLine(this);
					startParagraph();
				}
			}
		}
		
		// Is this line seems like a bulleted or numbered list item
		public boolean isListedItem() {
			if (isListedItem == null) {
				boolean res = false;
				if (runsSet == null || runsSet.isEmpty()) {
					res = false;
				}
				else {
					final PRun firstRun = runsSet.get(0);
					if (firstRun.getStyle().getFont().toLowerCase().contains("symbol")) {
						res = true;
					}
					else if (firstRunListItemPattern.matcher(firstRun.getText()).find()) {
						res = true;
					}
					else {
						final String[] tokens = getText().split("\\s+", 2);
						if (firstTokenListItemPattern.matcher(tokens[0]).find()) {
							res = true;
						}
					}
				}
				isListedItem  = new Boolean(res);
			}
			return isListedItem.booleanValue();
		}


		private static final boolean performSpacingAction = true;	// used for debug
		private void extractLineRuns(final List<TextPosition> textPositions) {
			// Add space/tab/special-marker characters, based on horizontal space.
	    	// Process Runs
	    	final List<TextPosition> runTps = new LinkedList<>();
	    	final Iterator<TextPosition> tpIter = textPositions.iterator();

//	    	if (logger.isTraceEnabled()) {
//	    		final String spacedHLetters = IntStream.range(1, textPositions.size())
//	    				.filter(i->(textPositions.get(i).getX()-textPositions.get(i-1).getX()-textPositions.get(i-1).getWidth())>textPositions.get(i-1).getWidthOfSpace())
//	    				.mapToObj(i->(String.format("%d:%s(%.1f)", i, textPositions.get(i).getCharacter(),(textPositions.get(i).getX()-textPositions.get(i-1).getX()-textPositions.get(i-1).getWidth()))))
//	    				.collect(Collectors.joining(","));
//	    		final String spacedVLetters = IntStream.range(1, textPositions.size())
//	    				.filter(i->(textPositions.get(i).getY()-textPositions.get(i-1).getY())>2)
//	    				.mapToObj(i->(String.format("%d:%s(%.1f)", i, textPositions.get(i).getCharacter(),(textPositions.get(i).getY()-textPositions.get(i-1).getY()))))
//	    				.collect(Collectors.joining(","));
//	    		logger.trace("line spacing exceptions: {} {}", spacedHLetters, spacedVLetters);
//	    	}
	    	
	    	TextPosition tp = tpIter.next();
	    	runTps.add(tp);
//	    	String font = tp.getFont().getBaseFont();
	    	String font = strippedFontName(tp.getFont());
	    	float fontSize = tp.getFontSizeInPt();
	    	Color color = getColorForTextPosition(tp, true);	// remove entry
	    	//float fontWeight = tp.getFont().getFontDescriptor().getFontWeight();
	    	//float fontItalicAngle = tp.getFont().getFontDescriptor().getItalicAngle();
	    	boolean bold = isBold(tp.getFont());
	    	boolean italic = isItalic(tp.getFont());
	    	double hLoc;
	    	double vLoc;
	    	int tpCounter = 0;
	    	
	    	while (tpIter.hasNext()) {
	    		Color color2;
	    		boolean bold2;
	    		boolean italic2;
	    		do {
	    			tpCounter++;
	    			boolean splitRun = false;
	    			boolean addSpace = false;
	    			boolean addTab = false;
	    			boolean flushLine = false;
	    			boolean flushParagraph = false;
	    			boolean styleChanged = false;
		        	vLoc = tp.getY();
		        	hLoc = tp.getX()+tp.getWidth()+tp.getWidthOfSpace();
					tp = tpIter.next();	// Move to next tp
					color2 = getColorForTextPosition(tp, true);	// remove entry
					bold2 = isBold(tp.getFont());
					italic2 = isItalic(tp.getFont());
					final double vSpace = Math.abs(tp.getY()-vLoc);		// Compared to prev. tp
					final double hSpace = Math.abs(tp.getX()-hLoc);		// Compared to prev. tp
					final long spaceFactor = (long) (hSpace / tp.getWidthOfSpace());
	        		
		        	if (vSpace>tp.getFontSize()/2 && tp.getDir()==0) {
		        		if (logger.isTraceEnabled()) {
		        			final int tpCounterSnapShot = tpCounter;
		        			final String debString = IntStream.range(0,textPositions.size())
									.mapToObj(i->((i==tpCounterSnapShot)?"^":"")+textPositions.get(i).getCharacter())
									.collect(Collectors.joining()); 
//		        			if (debString.contains("Daily Average Electricity Demand")) {
//								logger.debug("Got it!");
//							}
							logger.debug("V-space found space={} at '{}'({}) within '{}'", vSpace, tp.getCharacter(), tpCounter, debString);
						}
		        		splitRun = true;
		        		flushLine = true;
		        	} else if (spaceFactor >= minWhiteSpaceFactor && tp.getDir()==0) {
		        		if (logger.isTraceEnabled()) {
		        			final int tpCounterSnapShot = tpCounter;
		        			final String debString = IntStream.range(0,textPositions.size())
									.mapToObj(i->((i==tpCounterSnapShot)?"^":"")+textPositions.get(i).getCharacter())
									.collect(Collectors.joining()); 
//		        			if (debString.contains("JOB GROUP: MANAGING DIRECTOR")) {
//								logger.debug("Got it!");
//							}
							logger.debug("H-space found factor={} at '{}'({}) within '{}'", spaceFactor, tp.getCharacter(), tpCounter, debString);
						}
		        		if (spaceFactor <= maxWhiteSpaceFactor) {
		        			addSpace = true;
			        		splitRun = true;
		        		} else if (spaceFactor >= maxTabSpaceFactor) {
			        		splitRun = true;
							flushParagraph = true;
						} else {
							addTab = true;
			        		splitRun = true;
						}
		        	} else if (font.equals(strippedFontName(tp.getFont())) &&
		    				fontSize == tp.getFontSizeInPt() &&
		    				bold == bold2 &&
		    				italic == italic2 &&
		    				(color != null && color.equals(color2))) {
		    	    	runTps.add(tp);
		    		} else {
		        		splitRun = true;
		        		styleChanged = true;
		    		}
		        	if (splitRun && (styleChanged || performSpacingAction)) {
		        		processRuns(runTps, font, fontSize, color, bold, italic);
		        		if (performSpacingAction && addSpace) {
							addWordSeparator();
						}
		        		if (performSpacingAction && addTab) {
							addTab();
						}
		        		if (performSpacingAction && flushLine) {
							addWordSeparator();
						}
		        		if (performSpacingAction && flushParagraph) {
							addParagraphLine(this);
		        			startParagraph();
						}
		        		runTps.clear();
		                runTps.add(tp);
//		            	font = tp.getFont().getBaseFont();
		            	font = strippedFontName(tp.getFont());
		            	fontSize = tp.getFontSizeInPt();
		            	color = color2;	
		            	bold = bold2;
		            	italic = italic2;
		    			break;
		    		}
	    		} while (tpIter.hasNext());
	    	}
			processRuns(runTps, font, fontSize, color, bold, italic);
		}
		
		private void processRuns(final List<TextPosition> runTps, final String font,
				final float fontSize, final Color color, final boolean bold, final boolean italic) {
			final PRectangle pRect = new PRectangle(runTps);
			breakWordsForHeaderFooterDetection(runTps, font, fontSize, color, bold, italic, 0);

//			final String debString = runTps.stream().map(tp->tp.getCharacter()).collect(Collectors.joining()); 
//			if (debString.contains("underlined")) {
//				logger.debug("Got it!");
//			}
			
			final List<PRun> runs = processUnderlines(runTps, pRect, font, fontSize, color, bold, italic);
			addRuns(runs);
		}

		private void addRuns(final List<PRun> runs) {
//			if (logger.isTraceEnabled()) {
//				logger.trace("New runs (#{}): {}", runs.size(), runs.toString());
//			}
			if (runsSet==null) {
				throw new RuntimeException("Something is wrong! runSet is null");
			}
			runsSet.addAll(runs);
			runs.stream().map(r->r.getPRect()).forEach(rect->{ pRect.merge(rect); });
			text = null;
		}

		private void breakWordsForHeaderFooterDetection(final List<TextPosition> runTps, 
				final String font, final float fontSize, final Color color, final boolean bold, final boolean italic, final int underline) {
			// check top/bottom margins threshold
			List<PWord> wordsCollection = null;
			double y = runTps.get(0).getY();
			if (y < (vMarginThresholdRatio*pageHeight)) {
				wordsCollection = pageTopWords;
			} else if ((pageHeight-y) < (vMarginThresholdRatio*pageHeight)) {
				wordsCollection = pageBottomWords;
			} else {
				return;
			}
			boolean isPrevCharSpace = true;
			StringBuffer sb = new StringBuffer();
			double xPos = 0;
			double height = 0;
			for (final Iterator<TextPosition> iter = runTps.iterator();iter.hasNext();) {
				final TextPosition tp = iter.next();
				final String text = tp.getCharacter().trim();
				if (text.isEmpty()) {
					if (!isPrevCharSpace && sb.length() > runTps.size()*minHeaderFooterWordRunRatio) {
						// add word
						final String word = sb.toString();
						if (word.trim().length()>=3) {
							wordsCollection.add(new PWord(word, xPos, y, height, font, fontSize, color, bold, italic, underline));
							sb = null;
						}
					}
					isPrevCharSpace = true;
				} 
				else {
					if (isPrevCharSpace) {
						// start new word
						isPrevCharSpace = false;
						if (sb==null) {
							sb = new StringBuffer();
						}
						xPos = tp.getX();
						y = tp.getY();
						height = tp.getHeight();
					}
					sb.append(text);
				}
			}
			if (!isPrevCharSpace) {
				// add word
				final String word = sb.toString();
				if (word.trim().length()>=3) {
					wordsCollection.add(new PWord(word, xPos, y, height, font, fontSize, color, bold, italic, underline));
				}
			}
		}

		/*
		 * Identify h-lines that are underlines and split runs accordingly.
		 * Known limitation: support only non-rotated text (direction==0) 
		 */
		private List<PRun> processUnderlines(final List<TextPosition> runTps, final PRectangle pRect,
				final String font, final float fontSize, final Color color, final boolean bold, final boolean italic) {
			final List<ClaPageGraphics.PageGraphicsItem> underlines = pageGraphics.getPotentialUnderlines(pRect);
			final List<PRun> result = new ArrayList<>();
			if (underlines==null || underlines.isEmpty() || runTps.get(0).getDir()!=0) {	// Known limitation - support only horizontal text
				// no UL - just convert tps to runs (non UL)
				result.add(new PRun(runTps, font, fontSize, color, bold, italic, 0));
				return result;
			}
			else {
				final double runLeft = runTps.get(0).getX();
				final double runRight = runTps.get(runTps.size()-1).getX()+1;
				final double ulLeft = underlines.get(0).getHLineLeft();
				final double ulRight = underlines.get(underlines.size()-1).getHLineRight();
				if (runLeft>ulRight || ulLeft>runRight) {	// no overlap between curretn run (tps) and any ul line.
					result.add(new PRun(runTps, font, fontSize, color, bold, italic, 0));
				}
				else {		// some overlap possible - perform full analysis
					List<TextPosition> nTp = new ArrayList<>();
					int prevUlCount = -1;
					for (final Iterator<TextPosition> tpIter=runTps.iterator(); tpIter.hasNext();) {
						final TextPosition tp = tpIter.next();
						final int ulCount = underlinesCount(tp, underlines);
						if (ulCount != prevUlCount && !nTp.isEmpty()) {
							result.add(new PRun(nTp, font, fontSize, color, bold, italic, prevUlCount));
							nTp = new ArrayList<>();
						}
						nTp.add(tp);
						prevUlCount = ulCount;
					}
					result.add(new PRun(nTp, font, fontSize, color, bold, italic, prevUlCount));
				}
			}
			return result;
		}

		private int underlinesCount(final TextPosition tp, final List<ClaPageGraphics.PageGraphicsItem> underlines) {
			final double midX = tp.getX()+tp.getWidth()/2;
			return (int) underlines.stream().filter(ul->(ClaPageGraphics.inRange(midX,ul.getHLineLeft(),ul.getHLineRight(),0))).count();
		}

		private List<List<TextPosition>> processCrossingVerticalLines(final List<TextPosition> runTps, final PRectangle pRect) {
			final List<ClaPageGraphics.PageGraphicsItem> crossingVLines = pageGraphics.getCrossingVerticalLines(pRect);

			final List<List<TextPosition>> result = new ArrayList<>();
			if (crossingVLines==null || crossingVLines.isEmpty() || runTps.get(0).getDir()!=0) {	// Known limitation - support only horizontal text
				result.add(runTps);
				return result;
			}
			List<TextPosition> nTp = new ArrayList<>();
			final Iterator<ClaPageGraphics.PageGraphicsItem> lineIter = crossingVLines.iterator();
			ClaPageGraphics.PageGraphicsItem vLine = lineIter.next();
			boolean crossedLast = false;
			for (final Iterator<TextPosition> tpIter=runTps.iterator(); tpIter.hasNext();) {
				final TextPosition tp = tpIter.next();
				if ((!crossedLast) && tp.getX()>vLine.getVLineX()) {
					if (!nTp.isEmpty()) {
						result.add(nTp);
						nTp = new ArrayList<>();
					}
					while (lineIter.hasNext() && tp.getX()>vLine.getVLineX()) {
						vLine = lineIter.next();	// skip adjacent lines
					}
					if (lineIter.hasNext()) {
						vLine = lineIter.next();
					} else {
						crossedLast = true;
					}
				}
				nTp.add(tp);
			}
			result.add(nTp);
			return result;
		}

		public boolean isEmpty() {
			return runsSet.isEmpty();
		}

		public double getSpacing() {
			return spacing;
		}
		public void setSpacing(final double spacing) {
			this.spacing = spacing;
		}

		public void clear() {
			runsSet.clear();
			text = "";
			pRect = new PRectangle();
		}

		@Override
		public String toString() {
			return "PLine:{#Runss=" + (runsSet==null?-1:runsSet.size()) + ", pRect=" + pRect + ", spacing=" + spacing + ", text=" + getText() + "}";
		}

		public void addWordSeparator() {
			if (runsSet!=null && !runsSet.isEmpty()) {
				runsSet.get(runsSet.size()-1).addWordSeparator();
				text = null;
			}
		}

		public void addTab() {
			if (runsSet!=null && !runsSet.isEmpty()) {
				runsSet.get(runsSet.size()-1).addTab();
				text = null;
			}
		}

		public boolean sameCell(final PLine otherLine) {
			final PRectangle boundingCell1 = pageGraphics.getBoundingCell(getpRect());
			final PRectangle boundingCell2 = pageGraphics.getBoundingCell(otherLine.getpRect());

			return boundingCell1.equals(boundingCell2);
		}
		
		// 
		public double getDominantFontScore() {
//			final PRun biggestRun = runsSet.stream().sorted((r1,r2)->Integer.compare(r2.getTrimmedLength(),r1.getTrimmedLength())).findFirst().orElse(null);
//			if (biggestRun == null) {
//				return -1;
//			}
//			if (((float)biggestRun.getTrimmedLength()) < 0.7*getText().length()) {
//				return -1;
//			}
			double avgFontScore = runsSet.stream().mapToDouble(r->(r.getTrimmedLength() * r.getStyle().getFontScore())).sum();
			avgFontScore /= getText().length();
			return avgFontScore;
		}
		
		public void finalClear() {
			this.getText();
			this.isListedItem();
			runsSet = null;
		}

	}		// PLine 

	public class PParagraph {
		private List<PLine> lines = new LinkedList<>();
		private String text;
		private PRectangle pRect;
		private PStyle style = null;
		private String paragraphLabelText = null;
		
		public List<PLine> getLines() {
			return lines;
		}
		public void setLines(final List<PLine> lines) {
			this.lines = lines;
			this.text = null;	// clear text cache 
			this.pRect = null;
		}
		public void append(final PLine line) {
			if (!lines.isEmpty()) {
				if (line.isListedItem()) {
					flushParagraphLines();
					logger.trace("Flushed paragraph before a listed item '{}'", line.getText());
				} else {
					final PLine lastLine = lines.get(lines.size()-1);
					if (!lastLine.sameCell(line)) {
						//				if (/* (line.getpRect().getMinX() - lastLine.getpRect().getMaxX()) > misalignedLinesGap || */
						//						(lastLine.getpRect().getMinX() - line.getpRect().getMaxX()) > misalignedLinesGap) {
						// Adjucent lines are misaligned - split paragraph
						flushParagraphLines();
						logger.trace("Split misaligned paragraph at <{}><{}>", lastLine.getText(), line.getText());
					} else if (lastLine.getpRect().getMaxY() > line.getpRect().getMinY()) {
						flushParagraphLines();
						logger.trace("Split un-ordered paragraph at <{}><{}>", lastLine.getText(), line.getText());
					}
				}
			}
			this.lines.add(line);
			this.text = null;	// clear text cache 
		}
		private void flushParagraphLines() {
			final PParagraph newPara = new PParagraph();
			newPara.append(this);
			pageParagraphs.add(newPara);
			prevParagraph = newPara;
			lines.clear();
			this.text = null;	// clear text cache
			this.pRect = null;
		}
		public void append(final List<PLine> lines) {
			this.lines.addAll(lines);
			this.text = null;	// clear text cache
			this.pRect = null;
		}
		public void append(final PParagraph p2) {
			if (p2==null) {
				return;
			}
			this.append(p2.getLines());
			this.text = null;	// clear text cache 
		}
		
		public String getText() {
			if (text == null) {
				this.text = lines.stream().map(l->l.getText()).collect(Collectors.joining(" "));
			}
			return text;
		}

		public String getParagraphLabelText() {
			if (paragraphLabelText  == null) {
				final int tableLevel = getParagraphStyle().tableLevel; 

				final List<PRun> runs = this.getLines().stream().flatMap(l->l.runsSet.stream()).collect(Collectors.toList());
				final PRun run1 = runs.get(0);
				if (runs.size() == 1 && (!run1.getStyle().isBold()) && (run1.getStyle().underline == 0) && (tableLevel == 0)) {
					paragraphLabelText = "";
				}
				else {
					String text = run1.getText();
					for (int i=1;i<runs.size();++i) {
						final PRun run2 = runs.get(i);
						if (run1.getStyle().getFontSize() > run2.getStyle().getFontSize() || 
								(run1.getStyle().isBold() && !run2.getStyle().isBold()) ||
								(run1.getStyle().underline > 0 && run2.getStyle().underline == 0)) {
							break;
						}
						else {
							text = text + run2.getText();
						}
					}
					paragraphLabelText = text;
				}
			}
			return paragraphLabelText;
		}
		
		public PRectangle getpRect() {
			if (pRect == null) {
				pRect = new PRectangle();
				lines.stream().map(l->l.getpRect()).forEach(r->{
					pRect.merge(r);
				});
			}

			return pRect;
		}

		public boolean isEmpty() {
			return lines.isEmpty();
		}
		
		public boolean isTitle() {
//			final int countLines = (int) lines.stream().filter(l->!l.getText().trim().isEmpty()).count();
//			if (countLines > 1) {
//				return false;
//			}
			if (pRect.getHeight() > 2*style.getFontSize()) {
				return false;
			}
			final PLine line = lines.stream().filter(l->!l.getText().trim().isEmpty()).findFirst().orElse(null);
			if (line==null) {
				return false;
			}
			final int len = line.getText().trim().length();
			if (len < minTitleLength || len > maxTitleLength) {
				return false;
			}
			getParagraphStyle();	// set the private style field
			return (style.getFontScore() >= minTitleFontScore);
		}
		
		public void processIndentation() {
			final boolean hasGoodToken = meaningfulTokenPattern.matcher(getText()).find();		// make sure there is at least 1 meaningful text token
			getParagraphStyle(hasGoodToken);	// set the private style field; calc stats only if text has a meaningful token
			if (style==null) {
				return;
			}

			// Check minX for all lines but the first one to get left just.
			// Check maxX for all lines but the last one to get right just. Check maxX tolerance to detect justification type.
			// Collect potential titles into global collection
			
			// Known-limitation: for tables, the margines should be based on bounding-cell and not the (0,pageWidth)

			PRectangle pFirstLine = null;
			PRectangle pLastLine = null;
			double minStart = pageWidth;
			double maxStart = 0;
			double minEnd = pageWidth;
			double maxEnd = 0;
			int linesCount = 0;
			
			for (final Iterator<PLine> iter = lines.iterator();iter.hasNext();) {
				final PLine l = iter.next();
				if (l.getText().trim().isEmpty()) {
					continue;
				}
				linesCount++;
				final PRectangle r = l.getpRect();
				if (pFirstLine == null) {
					pFirstLine = r;
				} else {
					// Get start stats for all lines but first
					minStart = Double.min(minStart, r.getMinX());
					maxStart = Double.max(maxStart, r.getMinX());
				}
				if (pLastLine != null) {
					// Get end stats for all lines but last
					minEnd = Double.min(minEnd, pLastLine.getMaxX());
					maxEnd = Double.max(maxEnd, pLastLine.getMaxX());
				}
				pLastLine = r;
			}
			style.setJustification(ClaStyle.Justification.LEFT_JUST);
			style.setFirstLineIndent(0);
			if (linesCount==0) {
				return;
			}
			final PRectangle boundingCell = pageGraphics.getBoundingCell(pFirstLine);
			final double leftReference = boundingCell.getMinX();
			final double rightReference = Double.min(pageWidth, boundingCell.getMaxX());
			style.setTableLevel((boundingCell.getWidth()<pageWidth*maxTablePageCoverage && boundingCell.getHeight()<pageHeight*maxTablePageCoverage)?1:0);
			
			if (linesCount > 2) {
				style.setJustification(ClaStyle.Justification.CENTER_JUST);
				// check start tolerance including last line, excluding first
				final double leftMargine = minStart - leftReference;
				final double rightMargine = rightReference - maxEnd;
				style.setIndentFromLeft(leftMargine);
				style.setIndentFromRight(rightMargine);
				final double startTolerance = maxStart - minStart;
				if (startTolerance < minJustifiedLeftTolerance) {
					style.setIndentFromLeft(Double.min(minStart,pLastLine.getMinX()));
					style.setJustification(ClaStyle.Justification.LEFT_JUST);
					final double firstLineIndent = pFirstLine.getMinX() - minStart;
					style.setFirstLineIndent(firstLineIndent/20);
				}
				// check end tolerance including first line, excluding last
				final double endTolerance = maxEnd - minEnd;
				if (endTolerance < minJustifiedRightTolerance) {
					if (style.getJustification()==ClaStyle.Justification.LEFT_JUST) {
						style.setJustification(ClaStyle.Justification.JUSTIFIED_JUST);
					} else {
						style.setJustification(ClaStyle.Justification.RIGHT_JUST);
					}
				}
				if (style.getJustification()==ClaStyle.Justification.CENTER_JUST) {
					style.setFirstLineIndent(0);
					style.setIndentFromLeft(0);
					style.setIndentFromRight(0);
				}
			} else if (linesCount==1) {
				final double leftMargine = pFirstLine.getMinX() - leftReference;
				final double rightMargine = rightReference - pFirstLine.getMaxX();
				style.setFirstLineIndent(0);
				style.setIndentFromLeft(leftMargine);
				style.setIndentFromRight(rightMargine);
				if (Math.abs(rightMargine-leftMargine) < minCenteredMarginesTolerance && rightMargine > minCenteredJustifiedMargines) {
					style.setJustification(ClaStyle.Justification.CENTER_JUST);
					style.setFirstLineIndent(0);
					style.setIndentFromLeft(0);
					style.setIndentFromRight(0);
				} else {	// no right-just for a single line text
					style.setJustification(ClaStyle.Justification.LEFT_JUST);
				}
			} else {	// 2 lines paragraph - set common reasonable values
				// check start tolerance including both lines
				final double firstLineIndent = pFirstLine.getMinX() - minStart;
				style.setFirstLineIndent(firstLineIndent);
				final double startTolerance = Double.max(maxStart,pFirstLine.getMinX()) - Double.min(minStart,pFirstLine.getMinX());
				final double endTolerance = Double.max(maxEnd,pFirstLine.getMaxX()) - Double.min(minEnd,pFirstLine.getMaxX());
				final double leftMargine = Double.min(minStart,pFirstLine.getMinX()) - leftReference;
				final double rightMargine = rightReference - Double.max(maxEnd,pFirstLine.getMaxX());
				style.setIndentFromLeft(leftMargine);
				style.setIndentFromRight(rightMargine);
				if (Math.abs(rightMargine-leftMargine) < minCenteredMarginesTolerance && rightMargine > minCenteredJustifiedMargines) {
					style.setJustification(ClaStyle.Justification.CENTER_JUST);
					style.setFirstLineIndent(0);
					style.setIndentFromLeft(0);
					style.setIndentFromRight(0);
				} else if (startTolerance < minJustifiedLeftTolerance) {
					style.setJustification(ClaStyle.Justification.LEFT_JUST);
				} else if (endTolerance < minJustifiedRightTolerance) {
					style.setJustification(ClaStyle.Justification.RIGHT_JUST);
				}
				if (style.getJustification()==ClaStyle.Justification.LEFT_JUST) {
					style.setIndentFromRight(0);
				} else if (style.getJustification()==ClaStyle.Justification.RIGHT_JUST) {
					style.setFirstLineIndent(0);
					style.setIndentFromLeft(0);
				} else {
					style.setFirstLineIndent(0);
					style.setIndentFromLeft(0);
					style.setIndentFromRight(0);
				}
			}

			// Collect titles for later processing of heading levels
			if (isTitle()) {
				titles.add(this);
				headings.add(this);
			}
		}

		public boolean isBody() {
			return (pRect.getMaxY() < bottomMargin && pRect.getMinY() > topMargin);
		}
	    // Executed at endDocument() handler
		public PStyle getParagraphStyle() {
			return getParagraphStyle(false);
		}
		public PStyle getParagraphStyle(final boolean calcStats) {
			if (style == null) {
				final Map<PStyle,Integer> sm = new HashMap<>();
				lines.forEach(l->{
					l.runsSet.forEach(r->{
						final PStyle s = r.style;
						if (s!=null) {
							Integer count = sm.get(s);
							if (count == null) {
								count = 0;
							}
							final int len = r.getTrimmedLength();
							sm.put(s,count+len);
							if (calcStats)
							 {
								fontSizeStats.addRun(s.getFont(), Math.round(s.getFontSize()), len);	// Collect stats about font size/score
							}
						}
					});
				});
				final Entry<PStyle, Integer> se = sm.entrySet().stream().max((e1,e2)->e1.getValue().compareTo(e2.getValue())).orElse(null);
				if (se==null) {
					logger.error("No paragraph style extracted: #lines={} #runs={} <{}>", lines.size(), 
							lines.stream().mapToInt(l->l.runsSet.size()).sum(), getText());
					return null;
				}
//				if (logger.isTraceEnabled()) {
//					logger.trace("Style dominance of {}% - {}/{}", Math.round(100f*se.getValue()/getText().length()),se.getValue(),getText().length());
//				}
				style = new PStyle(se.getKey());
			}
			return style;
		}

		// Analyze title-level, justification
		public PStyle finalizeParagraphStyle() {
			final Map<PStyle,Integer> sm = new HashMap<>();
			lines.forEach(l->{
				l.runsSet.forEach(r->{
					final PStyle s = r.style;
					if (s!=null) {
						Integer count = sm.get(s);
						if (count == null) {
							count = 0;
						}
						sm.put(s,count+r.getTrimmedLength());
					}
				});
			});
			final Entry<PStyle, Integer> se = sm.entrySet().stream().max((e1,e2)->e1.getValue().compareTo(e2.getValue())).orElse(null);
			if (se==null) {
				logger.error("No paragraph style extracted: #lines={} #runs={} <{}>", lines.size(), 
						lines.stream().mapToInt(l->l.runsSet.size()).sum(), getText());
				return null;
			}
			if (logger.isTraceEnabled()) {
				logger.trace("Style dominance of {}% - {}/{}", Math.round(100f*se.getValue()/getText().length()),se.getValue(),getText().length());
			}
			return se.getKey();
		}

		// split paragraphs based on font score
		public List<PParagraph> splitParagraphsFontScore() {
			final List<PParagraph> res = new ArrayList<>();
			if (lines.size()<=1) {
				res.add(this);
				return res; 
			}
			List<PLine> lList = new ArrayList<>();
			double prevFontScore = -1;
			for (final Iterator<PLine> iter = lines.iterator();iter.hasNext();) {
				final PLine l = iter.next();
				if (l.getText().trim().isEmpty()) {
					continue;
				}
				final double fontScore = l.getDominantFontScore();
				if (prevFontScore > 0 && fontScore > 0 && Math.abs(fontScore - prevFontScore) >= minFontScoreSpitDiff) {
					final PParagraph newPara = new PParagraph();
					newPara.append(lList);
					newPara.style = null;
					newPara.getParagraphStyle();
					res.add(newPara);
					lList = new ArrayList<>();
				}
				lList.add(l);
				prevFontScore = fontScore;
			}
			if ((!lList.isEmpty()) && !res.isEmpty()) {
				final PParagraph newPara = new PParagraph();
				newPara.append(lList);
				newPara.style = null;
				newPara.getParagraphStyle();
				res.add(newPara);
			} else {
				res.add(this);
			}
			return res;
		}

		
		// split paragraphs based on line-start and line-spacing
		public List<PParagraph> splitParagraphsVericalSpacing() {
			final List<PParagraph> res = new ArrayList<>();
			if (lines.size()<=3) {
				res.add(this);
				return res; 
			}
			PLine prevLine = null;
			double avgSpacing = 0;
			double maxSpacing = 0;
			double minSpacing = 0;
			// calc line spacing stats
			for (final Iterator<PLine> iter = lines.iterator();iter.hasNext();) {
				final PLine l = iter.next();
				if (l.getText().trim().isEmpty()) {
					continue;
				}
				if (prevLine != null) {
					final double spacing = l.getpRect().getMaxY() - prevLine.getpRect().getMaxY();
					l.setSpacing(spacing);
					if (minSpacing>spacing) {
						minSpacing = spacing;
					}
					if (maxSpacing<spacing) {
						maxSpacing = spacing;
					}
					avgSpacing += spacing;
				}
				prevLine = l;
			}
			avgSpacing = avgSpacing / (lines.size()-1);
			// Look for splits;
			List<PLine> lList = new ArrayList<>();
			prevLine = null;
			for (final Iterator<PLine> iter = lines.iterator();iter.hasNext();) {
				final PLine l = iter.next();
				if (l.getText().trim().isEmpty()) {
					continue;
				}
				if (lList.size()>1) {
					final double diffSpacing = l.getSpacing() - prevLine.getSpacing();
					if (diffSpacing > paragraphSplitSpacingDiffThreshold) {
						final PParagraph newPara = new PParagraph();
						newPara.append(lList);
						newPara.style = null;
						newPara.getParagraphStyle();
						res.add(newPara);
						lList = new ArrayList<>();
					}
				} else if (lList.size()==1) {	// First line should be treated differently.
					final double diffSpacing = l.getSpacing() - avgSpacing;
					if (diffSpacing > paragraphSplitSpacingDiffThreshold/2) {
						final PParagraph newPara = new PParagraph();
						newPara.append(lList);
						newPara.style = null;
						newPara.getParagraphStyle();
						res.add(newPara);
						lList = new ArrayList<>();
					}
				}
				lList.add(l);
				prevLine = l;
			}
			if ((!lList.isEmpty()) && !res.isEmpty()) {
				final PParagraph newPara = new PParagraph();
				newPara.append(lList);
				newPara.style = null;
				newPara.getParagraphStyle();
				res.add(newPara);
			} else {
				res.add(this);
			}
			return res;
		}

		@Override
		public String toString() {
			return "PParagraph:{#lines=" + lines.size() + ", pRect=" + pRect + ", style=" + style + ", text=" + getText() + "}";
		}
		
	    // Executed at endDocument() handler
		public void updateHeadings(final Map<Integer, Integer> titlesFontScoreToLevel, final int commonFontSize) {
			final PStyle s = getParagraphStyle();
			final int paraFontScore = s.getFontScore();
			s.setListItem(lines.get(0).isListedItem());
			if (isTitle() && paraFontScore > commonFontSize) {
				final Integer h = titlesFontScoreToLevel.get(Integer.valueOf(paraFontScore));
				if (h==null || s.getJustification() == Justification.CENTER_JUST) {
//					logger.trace("Title fontScore not found. para={}, titleFontScoreMap={}",this.toString(), titlesFontScoreToLevel);
					s.setIsTitle(!s.isListItem());
				} else {
					s.setHeadingLevel(h);
				}
			}
		}
		
		public void addWordSeparator() {
			if (!lines.isEmpty()) {
				lines.get(lines.size()-1).addWordSeparator();
			}
		}
		
		public PLine getFirstLine() {
			return lines.isEmpty()?null:lines.get(0);
		}
		
		public void clear() {
			this.getParagraphLabelText();
			// calculate aggregated text and 
			lines.forEach(l->l.finalClear());
		}

	}		// PParagraph
	
	public class PImage {
		private final String name;
		private final double xPos;
		private final double yPos;
		private final int width;
		private final int height;
		private final double xScale;
		private final double yScale;
		private final String fileName;
		private final String params;
		
		public PImage(final String name, final double xPos, final double yPos, final int width,
				final int height, final double xScale, final double yScale, final String fileName,
				final String params) {
			this.name = name;
			this.xPos = xPos;
			this.yPos = yPos;
			this.width = width;
			this.height = height;
			this.xScale = xScale;
			this.yScale = yScale;
			this.fileName = fileName;
			this.params = params;
		}
		public String getName() {
			return name;
		}
		public double getxPos() {
			return xPos;
		}
		public double getyPos() {
			return yPos;
		}
		public int getWidth() {
			return width;
		}
		public int getHeight() {
			return height;
		}
		public double getxScale() {
			return xScale;
		}
		public double getyScale() {
			return yScale;
		}
		public String getFileName() {
			return fileName;
		}
		public String getParams() {
			return params;
		}
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result
					+ ((fileName == null) ? 0 : fileName.hashCode());
			result = prime * result + height;
			result = prime * result + ((name == null) ? 0 : name.hashCode());
			result = prime * result + width;
			return result;
		}
		@Override
		public boolean equals(final Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (getClass() != obj.getClass()) {
				return false;
			}
			final PImage other = (PImage) obj;
			if (fileName == null) {
				if (other.fileName != null) {
					return false;
				}
			} else if (!fileName.equals(other.fileName)) {
				return false;
			}
			if (height != other.height) {
				return false;
			}
			if (name == null) {
				if (other.name != null) {
					return false;
				}
			} else if (!name.equals(other.name)) {
				return false;
			}
			if (width != other.width) {
				return false;
			}
			return true;
		}
		@Override
		public String toString() {
			return "PImage:{name=" + name + ", xPos=" + xPos + ", yPos=" + yPos + ", width=" + width + ", height=" + height + ", xScale="
					+ xScale + ", yScale=" + yScale + ", fileName=" + fileName + ", params=" + params + "}";
		}
		
		public double getDisplaySize() {
			return xScale * yScale;
		}
	}		// PImage

	private final Map<TextPosition, Color> tpColorMap = new HashMap<>();
	private static final int maxTPMapEntries = 5000;

	private static final double minScannedImageRatio = 0.7;
	private static final int maxScannedImagePageText = 20;
	private static final int maxScannedImagesInPage = 5;

	private Color lastInsertedColor = null;
	private Color lastQueriedColor = null;

	private final int pageNumber;

	public void setColorForTextPosition(final TextPosition text, final Color col) {
		if (lastInsertedColor != null && lastInsertedColor.equals(col)) {
			return; 	// inserts and queries are sequential. Don't insert sequences of identical values.
		}
		lastInsertedColor = col;
		if (tpColorMap.size() >= maxTPMapEntries) {
			logger.warn("tpColorMap size exceeded limit of {}. text={}. Clearing history.", 
					maxTPMapEntries, text.getCharacter());
			tpColorMap.clear();
		}
		tpColorMap.put(text, col);
	}
	
	public Color getColorForTextPosition(final TextPosition text, final boolean remove) {
		final Color col = remove ? tpColorMap.remove(text) : tpColorMap.get(text);
		if (col == null) {
			return lastQueriedColor; 	// inserts and queries are sequential. Return last not-null value.
		}
		lastQueriedColor = col;
		return col;
	}
	
    public ClaPdfPage(final int pageNumber, final float width, final float height,
					  final ClaPageGraphics pageGraphics,
					  final List<PParagraph> titles, final List<PParagraph> headings, final FontStats fontSizeStats,
					  final boolean tokenMatcherActive, final boolean patternSearchActive,
					  final boolean processSpaceCorruption, final boolean convertWordSeparatorToSpace,
					  final boolean debugTextExtraction) {
    	this.pageWidth = width;
    	this.pageHeight = height;
    	this.pageNumber = pageNumber;
    	this.pageGraphics = pageGraphics;
    	this.titles = titles;
    	this.headings = headings;
    	this.fontSizeStats = fontSizeStats;
    	this.tokenMatcherActive = tokenMatcherActive;
    	this.patternSearchActive = patternSearchActive;
    	this.processSpaceCorruption  = processSpaceCorruption;
		this.convertWordSeparatorToSpace = convertWordSeparatorToSpace;
		this.debugTextExtraction = debugTextExtraction;

		this.startPage();
	}

	public static boolean isBold(final PDFont font) {
    	final PDFontDescriptor pd = font.getFontDescriptor();
    	final String fn = font.getBaseFont();
    	// font weight of 700 and above indicates boldness
    	return ((pd != null && (pd.isForceBold() || pd.getFontWeight()>699)) || (fn!=null && fn.toLowerCase().contains("bold")));
    }

    public static boolean isItalic(final PDFont font) {
    	final PDFontDescriptor pd = font.getFontDescriptor();
    	final String fn = font.getBaseFont();
    	return ((pd != null && (pd.isItalic() || pd.getItalicAngle()!=0.0)) || (fn!=null && fn.toLowerCase().contains("italic")));
    }

    // Cache stripped names
    private final Map<String,String> strippedFontNamesMap = new HashMap<>();

	public String strippedFontName(final PDFont font) {
//    	final PDFontDescriptor pd = font.getFontDescriptor();
    	final String fn = font.getBaseFont();
    	if (fn == null) {
    		final String gn = "F-"+font.getSubType()+"-"+font.hashCode();
    		logger.trace("Null font name. Set name to {}",gn);
    		return gn;
    	}
    	String sn = strippedFontNamesMap.get(fn);
    	if (sn == null) {
    		sn = fn.toLowerCase().replaceAll("[^a-z]?bold", "");
    		strippedFontNamesMap.put(fn, sn);
    	}
    	return sn;
    }

	public void addParagraphLine(final PLine pLine) {
    	if (pLine.isEmpty()) {
			return;
		}
    	
    	if (curParagraph == null) {
    		logger.error("curParagraph is null");
    		curParagraph = new PParagraph();
    	}
    	curParagraph.append(new PLine(pLine));
    	pLine.clear();
    }
    
    public void addWordSeparator() {
    	if (curParagraph == null) {
    		logger.error("curParagraph is null");
    		curParagraph = new PParagraph();
    	}
    	curParagraph.addWordSeparator();
		if (convertWordSeparatorToSpace) {
			addWordSeparatorToTpBuff();
		}
    }
    
    public void addLineSeparator() {
    	flushLine();
    }
    
    public void startParagraph() {
		flushLine();
    	if (curParagraph == null) {
    		curParagraph = new PParagraph();
    	} else if (!curParagraph.isEmpty()) {
    		pageParagraphs.add(curParagraph);
    		prevParagraph = curParagraph;
    		curParagraph = new PParagraph();
    	}
    }
    
    public void processParagraph() {
		flushLine();
    	if (curParagraph != null &&  !curParagraph.isEmpty()) {
    		// split paragraphs based on line-start and line-spacing
    		final List<PParagraph> pList1 = curParagraph.splitParagraphsVericalSpacing();
    		final List<PParagraph> pList2 = pList1.stream().flatMap(p->p.splitParagraphsFontScore().stream()).collect(Collectors.toList());

    		pList2.forEach(p->pageParagraphs.add(p));
    		prevParagraph = curParagraph;
    		curParagraph = new PParagraph();
    	}
    }

    // Executed at endDocument() handler
	public void processHeaderFooter(final List<ClaPdfPage> pages) {

		if (logger.isTraceEnabled()) {
			final List<PWord> topWords = pageTopWords.stream()
					.filter(w->(pages.stream().filter(p->((p!=this)
							&& p.pageNumber >= pageNumber-pageWindow
							&& p.pageNumber <= pageNumber+pageWindow)).anyMatch(p->containWord(w, p.pageTopWords))))
					.collect(Collectors.toList());
			final List<PWord> bottomWords =  pageBottomWords.stream()
					.filter(w->(pages.stream().filter(p->((p!=this)
							&& p.pageNumber >= pageNumber-pageWindow
							&& p.pageNumber <= pageNumber+pageWindow)).anyMatch(p->containWord(w, p.pageBottomWords))))
					.collect(Collectors.toList());
	    	logger.trace("Page #{} topWords={} bottomWords={}", pageNumber, topWords, bottomWords);
		}

    	topMargin = pageTopWords.stream()
    			.filter(w->
    				(pages.stream().filter(p->((p!=this)
						&& p.pageNumber >= pageNumber-pageWindow
						&& p.pageNumber <= pageNumber+pageWindow)).anyMatch(p->containWord(w, p.pageTopWords)))
				)
    			.mapToDouble(w->(w.getyPos()+w.getHeight()-1)).max().orElse(0);
    	
    	bottomMargin = pageBottomWords.stream()
    			.filter(w->
    				(pages.stream().filter(p->((p!=this)
						&& p.pageNumber >= pageNumber-pageWindow
						&& p.pageNumber <= pageNumber+pageWindow)).anyMatch(p->containWord(w, p.pageBottomWords)))
				)
    			.mapToDouble(w->(w.getyPos()-1)).min().orElse(pageHeight);
    	
    	if (logger.isTraceEnabled()) {
			logger.trace("PDF Page #{} margins: top={} bot={}/{} left={} right={}/{}", pageNumber, Math.floor(10*topMargin)/10f, Math.floor(10*bottomMargin)/10f, pageHeight, Math.floor(10*leftMargin)/10f, Math.floor(10*rightMargin)/10f, pageWidth);
		}
    }

	private boolean containWord(final PWord w, final List<PWord> wordList) {
		return wordList.contains(w);
	}

	public void startPage() {

		logger.trace("PDF page start #{}. graphics: #hlines={} #vlines={} #rects={}",
				pageNumber,
				pageGraphics.getHLinesCount(),pageGraphics.getVLinesCount(),pageGraphics.getRectanglesCount());
    }

    public PParagraph getPrevParagraph() {
		return prevParagraph;
	}

	public double getLeftMargin() {
		return leftMargin;
	}

	public double getRightMargin() {
		return rightMargin;
	}

	public double getTopMargin() {
		return topMargin;
	}

	public double getBottomMargin() {
		return bottomMargin;
	}

	public void processPage() {
		// Calculate left/right margins
		leftMargin = pageParagraphs.stream().mapToDouble(p->p.getpRect().getMinX()).min().orElse(0);
		rightMargin = pageParagraphs.stream().mapToDouble(p->p.getpRect().getMaxX()).min().orElse(pageWidth);
		pageParagraphs.forEach(PParagraph::processIndentation);
		pageParagraphs.forEach(PParagraph::clear);
		logger.trace("process page #{} margins: left={} right={}/{}", pageNumber, leftMargin, rightMargin, pageWidth);
    }

	public void writeText(final List<TextPosition> textPositions) {
    	if (curParagraph == null) {
    		logger.error("curParagraph is null");
    		curParagraph = new PParagraph();
    	}
    	addText(textPositions);
		
//		final PLine pLine = new PLine(textPositions);
//		addParagraphLine(pLine);
	}

    // Executed at endDocument() handler
	public void extract(final WordFile wordFile, 
			final ClaPdfProcessor.DocPositionTracker docPositionTracker,
			final Appendable sbOpenText,
			final Appendable sbHashedText,
			final Map<Integer, Integer> titlesFontScoreToLevel, 
			final int commonFontSize, 
			final NGramTokenizerHashed nGramTokenizerHashed, 
			final HashedTokensConsumer hashedTokensConsumer) {
		
		for (final Iterator<PParagraph> pIter = pageParagraphs.iterator();pIter.hasNext();) {
			final PParagraph para = pIter.next();
			para.updateHeadings(titlesFontScoreToLevel, commonFontSize);
			final PStyle s = para.getParagraphStyle();
			
			if (para.isBody()) {
				// Extract body paragraph:
				// Titles / heading identification !!!
				// Get letter-heads/paragraph labels
				// Add n-Grams
				extractParagraphLabels(para, docPositionTracker, wordFile);
				extractBodyText(para, docPositionTracker, wordFile, nGramTokenizerHashed);
				logger.trace("Extract paragraph: body {} <{}>", s, para.getText());
			} else {
				// Process as header/footer
				extractHeaderFooterText(para, wordFile, nGramTokenizerHashed);
				// Known limitation: text is processed for each page - consider de-duping (can be done via histogram count limit)
				logger.trace("Extract paragraph: header/footer {} <{}>", s, para.getText());
			}
			if (prevParagraphWasTitle == null) {
				titlesSequenceCount = 0;
			}
			if (gotTitle) {
				titlesSequenceCount++;
			}
			prevParagraphWasTitle = gotTitle?para:null;
			// Process tokens for entity-extraction mechanism
			if (tokenMatcherActive) {
				final String pText = para.getText().trim();
				if (!pText.isEmpty()) {
					append(sbOpenText, sbHashedText, pText, nGramTokenizerHashed, hashedTokensConsumer);
				}
			}
		}	// Loop on paragraphs
		
		extractDocPictures(nGramTokenizerHashed);
		
		this.clear();		// free memory
	}
	
	public void clear() {
		titles = null;
		headings = null;
		images = null;
		pageParagraphs = null;
		pageGraphics = null;	// all pages use a single instance. Should not use the local reference after this point.
	}

	private void append(final Appendable sbOpenText, final Appendable sbHashedText, final String pText,
						final NGramTokenizerHashed nGramTokenizerHashed, final HashedTokensConsumer hashedTokensConsumer) {

		try {
			if (patternSearchActive) {
				sbOpenText.append(pText).append("\n");
			}
			if (ngramTokenizerObject == null) {
				ngramTokenizerObject = nGramTokenizerHashed.getTokenizerObject();
			}
			//		sbText.append(nGramTokenizerHashed.getHash(pText,hashedTokensConsumer)).append(" ");
			sbHashedText.append(nGramTokenizerHashed.getHashReuseTokenizer(pText, hashedTokensConsumer, ngramTokenizerObject)).append(" ");
		}
		catch(IOException ioe){
			logger.error("Failed to append content {}.", pText);
		}
	}

	private void extractDocPictures(final NGramTokenizerHashed nGramTokenizerHashed) {
		// TODO Auto-generated method stub
	}

	private void extractHeaderFooterText(final PParagraph para, final WordFile wordFile, final NGramTokenizerHashed nGramTokenizerHashed) {
		String pText = para.getText().trim();
		PStyle style = para.getParagraphStyle();

		if (processSpaceCorruption) {
			Matcher sctSearchMatcher = spacedCorruptedTextSearchPattern.matcher(pText);
//			int countSpaceCorruptions = 0;
//			while (countSpaceCorruptions<3 && sctSearchMatcher.find()) {
//				countSpaceCorruptions++;
//			}
//			if (countSpaceCorruptions >= 3) {
			if (sctSearchMatcher.find()) {
				Matcher sctRepMatcher = spacedCorruptedTextRplacementPattern.matcher(pText);
				String newPText = sctRepMatcher.replaceAll("$1");
				if (newPText != null && !newPText.isEmpty()) {
					pText = newPText;
				}
			}
		}

		final List<Long> nGrams = nGramTokenizerHashed.getHashlist(pText, headingsTextNgramSize);
		wordFile.histogramAddLongItems(PVType.PV_HeaderNgrams, nGrams);
		
		if (pText.length() >= minTitleLength) {	// process style stats
			final String styleSig = style.getSimpleSignature();
			logger.trace("Para header styleSignature: {}", styleSig);
			wordFile.addHeaderStyleSignature(styleSig);
			if (debugTextExtraction && logger.isDebugEnabled()) {
				logger.debug("PdfTextExt: header - {}{} ({}): {}",
						styleSig, ((style.isHeading() && !ignoreHeaderFooterTitles)?" (title)":""), pText.length(), pText);
			}
		}
		
		if (style.isHeading() && !ignoreHeaderFooterTitles) {
//			final String pTitle = injectedFieldCodePattern.matcher(pText.trim()).replaceAll("");				// Remove field literals injected to the text
			final String pTitle = pText.trim();				// No field ingested string for now
			final int pTitleLength = pTitle.length();
			if (((!titleExtractionIgnoreRightJustified) || style.getJustification() != ClaStyle.Justification.RIGHT_JUST) &&
					(pTitleLength < maxTitleLength) && (pTitleLength >= minTitleLength) &&
					(wordFile.getProposedTitlesCount() < titleExtractionMaxCount))
			{
				int mCount=0;  for (final Matcher m=tabCharPattern.matcher(pTitle);m.find();++mCount) {}
				if (mCount<2) {
					gotTitle = true;
					wordFile.addProposedTitles("P6", style.getClaStyle().getStyleHeadingPotentialRate(),
							pTitle,
							prevParagraphWasTitle!=null && titlesSequenceCount<3 && para.getFirstLine().sameCell(prevParagraph.getFirstLine()));
				} else {
					logger.debug("Ignore title found {} tabs. {}", mCount, pTitle);
				}
			}
		}
	}

	PParagraph prevExtractedPara = null;
	
    // Executed at endDocument() handler processing
	private void extractBodyText(final PParagraph para, 
			final ClaPdfProcessor.DocPositionTracker docPositionTracker,
			final WordFile wordFile, 
			final NGramTokenizerHashed nGramTokenizerHashed) {
		
		String pText = para.getText();
		int pTextLenth = pText.length();
		final PStyle style = para.getParagraphStyle();
		PRectangle boundingCell = null;

		if (processSpaceCorruption) {
			Matcher sctSearchMatcher = spacedCorruptedTextSearchPattern.matcher(pText);
//			int countSpaceCorruptions = 0;
//			while (countSpaceCorruptions<3 && sctSearchMatcher.find()) {
//				countSpaceCorruptions++;
//			}
//			if (countSpaceCorruptions >= 3) {
			if (sctSearchMatcher.find()) {
				Matcher sctRepMatcher = spacedCorruptedTextRplacementPattern.matcher(pText);
				String newPText = sctRepMatcher.replaceAll("$1");
				if (newPText != null && !newPText.isEmpty()) {
					pText = newPText;
					pTextLenth = pText.length();
				}
			}
		}
		
		if (style.isInTable()) {
			boundingCell = pageGraphics.getBoundingCell(para.getpRect()); 
		}
		
		final boolean isHeading = ((!style.isInTable()) || boundingCell.getWidth()>pageWidth*0.6) 
				&& (pTextLenth < maxTitleLength) && style != null && 
				(pTextLenth >= minTitleLength) && (style.isTitle() || style.isHeading());

		if (pTextLenth > 2 * maxTitleLength || style.isInTable()) {
			wordFile.setBodyLongTextProcessed(true);
		}
		
		final List<Long> bodyNGrams = nGramTokenizerHashed.getHashlist(pText, bodyTextNgramSize);
		final boolean hasGoodToken = meaningfulTokenPattern.matcher(pText).find();

		if (isHeading && 
				bodyNGrams!=null && bodyNGrams.size()>0 && 
				hasGoodToken) {		// make sure there is at least 1 meaningful text token
			final String pTextT = pText.trim();
			final int pTextTLength = pTextT.length();
			if ((pTextTLength < maxTitleLength) && (pTextTLength >= minTitleLength)) {
				wordFile.histogramAddItem(PVType.PV_Headings, pTextT);
			}
			if (docPositionTracker.isWithinDocPreamble()) {
				final String pTitle = injectedFieldCodePattern.matcher(pTextT).replaceAll("");				// Remove field literals injected to the text
				final int pTitleLength = pTitle.length();
				if (((!titleExtractionIgnoreRightJustified) || style.getJustification() != ClaStyle.Justification.RIGHT_JUST) &&
						(pTitleLength < maxTitleLength) && (pTitleLength >= minTitleLength) &&
						(wordFile.getProposedTitlesCount() < titleExtractionMaxCount) &&
						(wordFile.getProposedTitlesCount() == 0 || !wordFile.isBodyLongTextProcessed() || style.isTitle()))	// once body long-text is seen process title only if none exist
				{
					int mCount=0;  for (final Matcher m=tabCharPattern.matcher(pTitle);m.find();++mCount) {}
					if (mCount<2) {
						gotTitle = true;
						// Set priority based on first character - letter=highest
						final String priority = Character.isAlphabetic(pTitle.codePointAt(0))?"P4":"P5";

						wordFile.addProposedTitles(priority, style.getClaStyle().getStyleHeadingPotentialRate(),
								pTitle,
								prevParagraphWasTitle!=null && titlesSequenceCount<3 && para.getFirstLine().sameCell(prevParagraphWasTitle.getFirstLine()));
					} else {
						logger.debug("Ignore title found {} tabs. {}", mCount, pTitle);
					}
				}
			}
		}

		if (bodyNGrams != null) {
			wordFile.histogramAddLongItems(PVType.PV_BodyNgrams, bodyNGrams);

			if (hasGoodToken || pTextLenth >= 30)
			{
				docPositionTracker.updateDocPosition((bodyNGrams==null || bodyNGrams.size()==0)?0:bodyNGrams.size()+bodyTextNgramSize/2);	// Estimation for number of words in this paragraph
			}

			wordFile.histogramAddLongItems(PVType.PV_SubBodyNgrams, bodyNGrams);
			final ClaHistogramCollection<Long> sbNgHist = wordFile.getLongHistogram(PVType.PV_SubBodyNgrams);
			// Mark all but every fifth item in the SubBodyNgrams to filter them out during PV_Entries save.
			final Iterator<Long> ngIter = bodyNGrams.iterator();
			for (int i=0;ngIter.hasNext();++i) {
				final Long ng = ngIter.next();
				if ((i % bodyTextNgramSize) != 0) {
					sbNgHist.markItem(ng);	// Marked items will not be matched
				}
			}
		}

		String absoluteStyleSig = null;
		if (pTextLenth >= minTitleLength && hasGoodToken) {		// make sure there is at least 1 meaningful text token
			// process style stats
//			logger.trace("Para body styleSignature: {}", styleSig);
//			final String styleSig = style.getSimpleSignature();
//			wordFile.addStyleSignature(null, absoluteStyleSig, !style.isInTable());
			absoluteStyleSig = style.getAbsoluteSignature();
			// Get style pair sequence only if paragraphs are not in table and paragraphs are processed down the page
			final boolean styleInPair = (!style.isInTable()) && 
					(prevExtractedPara==null || prevExtractedPara.getpRect().getMaxY() <= para.getpRect().getMinY());
			wordFile.addDelayedStyleSignature(style.getFont(), absoluteStyleSig, pTextLenth, styleInPair);
			logger.trace("AbsStyleDeb: f({}) {} {} text={}",style.getFont(), absoluteStyleSig, (style.isInTable())?1:0, pText);
			//				logger.debug("StyleDebX: {} Text={}", style.getSimpleSignature(), pText.substring(0, Integer.min(50, pText.length())));
		}
		if (debugTextExtraction && logger.isDebugEnabled()) {
			logger.debug("PdfTextExt: body - {}{} ({}): {}",
					((absoluteStyleSig==null)?"-":absoluteStyleSig), (gotTitle?" (title)":""), pText.length(), pText);
		}

		final String pTextT = pText.trim();
		String pLabel = getParagraphLabel(pTextT); // based on text analysis
		if (pLabel == null)
		{
			pLabel = getParagraphLabel(para, style.getTableLevel()); // based on run style analysis
		}
		if (pLabel != null) {
			wordFile.histogramAddItem(PVType.PV_ParagraphLabels, pLabel);
			if (debugTextExtraction && logger.isDebugEnabled()) {
				logger.debug("PdfTextExt: pLable ({}): {}", pLabel.length(), pLabel);
			}
			logger.trace("pLabel: {} out of {}",pLabel, pText);
		}
		// Extraction logic to be performed only on the first 1-2 pages
		if (docPositionTracker.isWithinDocPreamble()) {
			if (pLabel != null) {
				wordFile.histogramAddItem(PVType.PV_LetterHeadLabels, pLabel);

				// A section with letter-head is a low candidate for doc title (prefer the one with larget text within range)
				//					final String pTitle = injectedFieldCodePattern.matcher(pTextT).replaceAll("");				// Remove field literals injected to the text
				final String pTitle = pTextT;				// No injected field text for now
				final int pTitleLength = pTitle.length();
				if ((pTitleLength < maxTitleLength) && (pTitleLength >= minTitleLength)) {
					gotTitle = true;
					wordFile.addProposedTitles("P8", style.getClaStyle().getStyleHeadingPotentialRate(), pTitle, prevParagraphWasTitle!=null);
				}
			}

			wordFile.histogramAddLongItems(PVType.PV_DocstartNgrams, bodyNGrams);
		}
		prevExtractedPara = para;
	}		// extractBodyText

    // Executed at endDocument() handler processing
	private void extractParagraphLabels(final PParagraph p, final ClaPdfProcessor.DocPositionTracker docPositionTracker, final WordFile wordFile) {
		final String pTextT = p.getText().trim();
		String pLabel = getParagraphLabel(pTextT); // based on text analysis
		if (pLabel == null)
		{
			pLabel = getParagraphLabel(p, p.getParagraphStyle().tableLevel); // based on run style analysis
		}
		if (pLabel != null) {
			wordFile.histogramAddItem(PVType.PV_ParagraphLabels, pLabel);
		}
		// Extraction logic to be performed only on the first 1-2 pages
		if (docPositionTracker.isWithinDocPreamble()) {
			if (pLabel != null) {
				wordFile.histogramAddItem(PVType.PV_LetterHeadLabels, pLabel);
			}
		}
	}

	private String getParagraphLabel(final String pText) {
		String[] split = pLabelIndiactionPattern.split(pText.trim()+" ", 2);
		if (split.length == 1) {
			return null;
		}

		final Matcher matcher = pLabelPattern.matcher(split[0].trim());
		if (!matcher.find()) {
			return null;
		}
		final String res = matcher.group().trim();
		if (res.length() == 0) {
			return null;
		}
		split = pLabelSplitPattern.split(res, paragraphLabelMaxTokens);
		if (split.length == paragraphLabelMaxTokens) {
			return null;
		}
		return res;
	}

	private String getParagraphLabel(final PParagraph p, final int tableLevel) {

		/*
		final List<PRun> runs = p.getLines().stream().flatMap(l->l.runsSet.stream()).collect(Collectors.toList());
		final PRun run1 = runs.get(0);
		if (runs.size() == 1 && (!run1.getStyle().isBold()) && (run1.getStyle().underline == 0) && (tableLevel == 0)) {
			return null;
		}
		String text = run1.getText();
		for (int i=1;i<runs.size();++i) {
			final PRun run2 = runs.get(i);
			if (run1.getStyle().getFontSize() > run2.getStyle().getFontSize() || 
					(run1.getStyle().isBold() && !run2.getStyle().isBold()) ||
					(run1.getStyle().underline > 0 && run2.getStyle().underline == 0)) {
				break;
			}
			else {
				text = text + run2.getText();
			}
		}
		*/
		final String text = p.getParagraphLabelText();
		return text.isEmpty()?null:getParagraphLabel(text + paragraphLabelDelimiters[1]); 
	}

	public void addText(final List<TextPosition> textPositions) {
		tpBuffer.addAll(textPositions);
	}

	private int dbgCounter = 0;
	public void addWordSeparatorToTpBuff() {
		if (tpBuffer==null || tpBuffer.size()==0) {
			if (logger.isDebugEnabled() && (dbgCounter++)%20 == 0) {
				logger.debug("Can not add WP to a null or empty tpBuffer (#{})", dbgCounter);
			}
			return;
		}
		TextPosition lastTp = tpBuffer.getLast();
//		Matrix positionMatrix = lastTp.getTextPos();
		TextPosition wsTp = new TextPosition(
				0, 			//	int pageRotation,
				(float)pageWidth,	// 	float pageWidthValue,
				(float)pageHeight,	//	float pageHeightValue,
				lastTp.getTextPos(),	//	Matrix textPositionSt,
				lastTp.getX()+lastTp.getWidth()+lastTp.getWidthOfSpace(), //	float endXValue,
				lastTp.getY()+lastTp.getHeightDir(),	// float endYValue,
				lastTp.getHeight(),		//	float maxFontH,
				lastTp.getIndividualWidths()[0],	//	float individualWidth,
				lastTp.getWidth(),	//lastTp.getWidthOfSpace(),	//	float spaceWidth,
				" ",	//	String string,
				lastTp.getCodePoints(),		//	int[] codePoints,
				lastTp.getFont(),		//	PDFont currentFont,
				lastTp.getFontSize(),	//	float fontSizeValue,
				(int)lastTp.getFontSizeInPt()	//	int fontSizeInPt
		);

		tpBuffer.add(wsTp);
	}

	public void flushLine() {
		if (tpBuffer.isEmpty()) {
			return;
		}
		final List<TextPosition> tpList = tpBuffer;
		tpBuffer = new LinkedList<>();
		final PLine pLine = new PLine(tpList);
		if (!pLine.isEmpty()) {
	    	if (curParagraph == null) {
	    		logger.error("curParagraph is null");
	    		curParagraph = new PParagraph();
	    	}
	    	curParagraph.append(pLine);
		}
	}

	public void addImage(final String name, final double xPos, final double yPos, final int width,
			final int height, final double xScale, final double yScale, final String fileName,
			final String params) {
		images.add(new PImage(name, xPos, yPos, width, height, xScale, yScale, fileName, params));
	}
	
	public boolean isScannedPage() {
		if (images.size() == 0 || images.size() > maxScannedImagesInPage) {
			return false;
		}
		final PImage largestImage = images.stream().sorted((i1,i2)->Double.compare(i2.getDisplaySize(),i2.getDisplaySize())).findFirst().orElse(null);
		if (largestImage == null ||
				(largestImage.xScale < pageWidth*minScannedImageRatio && largestImage.yScale < pageHeight*minScannedImageRatio)) {
			return false;
		}
		final int textSize = pageParagraphs.stream().mapToInt(p->p.getText().length()).sum();
		if (textSize > maxScannedImagePageText) {
			return false;
		}
		return true;
	}


}
