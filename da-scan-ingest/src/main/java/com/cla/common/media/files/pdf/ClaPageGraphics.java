package com.cla.common.media.files.pdf;

import java.awt.Color;
import java.awt.geom.Rectangle2D;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class ClaPageGraphics {
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(ClaPageGraphics.class);
	
	private static final double underlineBottomTolleranceUp = 2;
	private static final double underlineBottomTolleranceDown = 2.5;
	private static final double tableMinHSpace = 0.5;
	private static final double tableMinVSpace = 0.5;

	private static final Comparator<PageGraphicsItem> hLinesYComparator = ((l1,l2)->(Double.compare(l1.getHLineY(),l2.getHLineY())));
	private static final Comparator<PageGraphicsItem> vLinesXComparator = ((l1,l2)->(Double.compare(l1.getVLineX(),l2.getVLineX())));

	private static final double maxPageHeight = 72*100;
	private static final double maxPageWidth = 72*100;

	private static final double cornerMargin = 10;

	public enum PageGraphicsType {
		GR_NONE, GR_H_LINE, GR_V_LINE, GR_RECTANGLE
	}
	public static class PageGraphicsItem {
		PageGraphicsType type;
		public double v1;
		public double v2;
		public double v3;
		public Rectangle2D r;
		public Color color;
		public boolean filled;
		public PageGraphicsItem(final PageGraphicsType type, final double v1, final double v2, final double v3, final Color color) {
			this.type = type;
			this.v1 = v1;
			this.v2 = v2;
			this.v3 = v3;
			this.color = color;
		}
		public PageGraphicsItem(final Rectangle2D r, final Color color, final boolean filled) {
			type = PageGraphicsType.GR_RECTANGLE;
			this.r = r;
			this.color = color;
			this.filled = filled;
		}
		public double getVLineX() {
			return v1;
		}
		public double getVLineTop() {
			return v2;
		}
		public double getVLineBottom() {
			return v3;
		}
		public double getHLineLeft() {
			return v1;
		}
		public double getHLineRight() {
			return v2;
		}
		public double getHLineY() {
			return v3;
		}

		@Override
 		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((color == null) ? 0 : color.hashCode());
			result = prime * result + (filled ? 1231 : 1237);
			result = prime * result + ((r == null) ? 0 : r.hashCode());
			result = prime * result + ((type == null) ? 0 : type.hashCode());
			long temp;
			temp = Double.doubleToLongBits(v1);
			result = prime * result + (int) (temp ^ (temp >>> 32));
			temp = Double.doubleToLongBits(v2);
			result = prime * result + (int) (temp ^ (temp >>> 32));
			temp = Double.doubleToLongBits(v3);
			result = prime * result + (int) (temp ^ (temp >>> 32));
			return result;
		}
		
		@Override
		public String toString() {
			switch (type) {
			case GR_H_LINE:
				return String.format("H-Line: %.2f-%.2f,%.2f col=%s", getHLineLeft(), getHLineRight(), getHLineY(), color.toString());
			case GR_V_LINE:
				return String.format("V-Line: %.2f,%.2f-%.2f col=%s", getVLineX(), getVLineTop(), getVLineBottom(), color.toString());
			case GR_RECTANGLE:
				return String.format("RECT: (%.2f,%.2f),(%.2f,%.2f) [(%.2fx%.2f)] col=%s", 
						r.getMinX(), r.getMinY(),
						r.getMaxX(), r.getMaxY(),
						r.getWidth(), r.getHeight(),
						color.toString());
			default:
				break;
			}
			return String.format("GraphicShape: wrong type value %d", type.ordinal());
		}
		
		@Override
		public boolean equals(final Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (getClass() != obj.getClass()) {
				return false;
			}
			final PageGraphicsItem other = (PageGraphicsItem) obj;
			if (color == null) {
				if (other.color != null) {
					return false;
				}
			} else if (!color.equals(other.color)) {
				return false;
			}
			if (filled != other.filled) {
				return false;
			}
			if (type != other.type) {
				return false;
			}
			if (r == null) {
				if (other.r != null) {
					return false;
				}
			} else if (type == PageGraphicsType.GR_RECTANGLE) {
				if (!r.equals(other.r)) {
					return false;
				}
			}
			else {
				if (Double.doubleToLongBits(v1) != Double.doubleToLongBits(other.v1)) {
					return false;
				}
				if (Double.doubleToLongBits(v2) != Double.doubleToLongBits(other.v2)) {
					return false;
				}
				if (Double.doubleToLongBits(v3) != Double.doubleToLongBits(other.v3)) {
					return false;
				}
			}
			return true;
		}
		public Color getColor() {
			return color;
		}
	}

	public List<PageGraphicsItem> hLines = new LinkedList<>();
	public List<PageGraphicsItem> vLines = new LinkedList<>();
	public List<PageGraphicsItem> rectangles = new LinkedList<>();

	private boolean sorted = false;

	public void reset() {
		rectangles.clear();
		hLines.clear();
		vLines.clear();
		sorted = false;
	}

	public void sort() {
		hLines.sort(hLinesYComparator);
		vLines.sort(vLinesXComparator);
		sorted  = true;
	}

	public void addRectangle(final Rectangle2D r, final Color color, final boolean filled) {
		rectangles.add(new PageGraphicsItem(r, color, filled));
	}

	public void addHorizontalLine(final double x1, final double x2, final double y, final Color color) {
		hLines.add(new PageGraphicsItem(PageGraphicsType.GR_H_LINE, x1, x2, y, color));
	}

	public void addVertivalLine(final double x, final double y1, final double y2, final Color color) {
		vLines.add(new PageGraphicsItem(PageGraphicsType.GR_V_LINE, x, y1, y2, color));
	}
	
	public List<PageGraphicsItem> getCrossingVerticalLines(final ClaPdfPage.PRun run) {
		return getCrossingVerticalLines(run.getPRect());
	}
	public List<PageGraphicsItem> getCrossingVerticalLines(final ClaPdfPage.PRectangle runRect) {
		final double runLeft = runRect.getMinX();
		final double runRight = runRect.getMaxX();
		final double runBottom = runRect.getMaxY();
		if (!sorted) {
			sort();
		}
		final List<PageGraphicsItem> result = vLines.stream()
				.filter(l->(l.getVLineTop()<runBottom &&
							l.getVLineBottom()>runBottom &&
							l.getVLineX()>runLeft &&
							l.getVLineX()<runRight))
							.sorted((l1,l2)->Double.compare(l1.getVLineX(),l2.getVLineX()))
							.collect(Collectors.toList());
		
		return result;
	}

	public List<PageGraphicsItem> getPotentialUnderlines(final ClaPdfPage.PRun run) {
		return getPotentialUnderlines(run.getPRect());
	}
	public List<PageGraphicsItem> getPotentialUnderlines(final ClaPdfPage.PRectangle runRect) {
		final double runLeft = runRect.getMinX()-1;
		final double runRight = runRect.getMaxX();
		final double runBottom = runRect.getMaxY();
		if (!sorted) {
			sort();
		}

		final List<PageGraphicsItem> result = hLines.stream()
				.filter(l->(l.getHLineLeft()>runLeft &&
							l.getHLineRight()<runRight &&
							inRange(l.getHLineY()-runBottom, -underlineBottomTolleranceUp, underlineBottomTolleranceDown, 0)))
							.sorted((l1,l2)->Double.compare(l1.getHLineLeft(),l2.getHLineLeft()))
							.collect(Collectors.toList());
		
		if (logger.isDebugEnabled() && result.size()>1) {
			// Look for partial overlap
			PageGraphicsItem l2 = result.get(0); 
			for (int i=1;i<result.size();i++) {
				final PageGraphicsItem l1 = l2;
				l2 = result.get(i);
				if ((l2.getHLineLeft() != l1.getHLineLeft() ||
						l2.getHLineRight() != l1.getHLineRight()) &&
						l2.getHLineLeft() < l1.getHLineRight()) {
					logger.debug("Unexpected underline overlap: {}, {}", l1.toString(),l2.toString());
				}
			}
		}
		
		return result;
	}

	public ClaPdfPage.PRectangle getBoundingCell(final ClaPdfPage.PRun run) {
		return getBoundingCell(run.getPRect());
	}
	public ClaPdfPage.PRectangle getBoundingCell(final ClaPdfPage.PRectangle runRect) {
		final double runLeft = runRect.getMinX();
		final double runRight = runRect.getMaxX();
		final double runBottom = runRect.getMaxY();
		final double runTop = runRect.getMinY();
		if (!sorted) {
			sort();
		}

//		final double[] matchingHLinesY = hLines.stream()
//				.filter(l->(l.getHLineLeft()<runLeft-tableMinHSpace && l.getHLineRight()>runRight+tableMinHSpace))
//				.mapToDouble(l->l.getHLineY()).toArray();
		PageGraphicsItem top = null;
		PageGraphicsItem bottom = null;
		for (final Iterator<PageGraphicsItem> iter = hLines.iterator();iter.hasNext();) {
			final PageGraphicsItem l = iter.next();
			if (l.getHLineLeft()<runLeft-tableMinHSpace && l.getHLineRight()>runRight+tableMinHSpace) {
				if (l.getHLineY() < runTop) {
					top = l;
				}
				if (l.getHLineY() > runBottom) {
					bottom = l;
					break;
				}
			}
			else if (l.getHLineY() > runBottom) {
				break;
			}
		}
		
//		final double[] matchingVLinesX = vLines.stream()
//				.filter(l->(l.getVLineTop()<runTop-tableMinVSpace && l.getVLineBottom()>runBottom+tableMinHSpace))
//				.mapToDouble(l->l.getVLineX()).toArray();
		PageGraphicsItem left = null;
		PageGraphicsItem right = null;
		for (final Iterator<PageGraphicsItem> iter = vLines.iterator();iter.hasNext();) {
			final PageGraphicsItem l = iter.next();
			if (l.getVLineTop()<runTop-tableMinVSpace && l.getVLineBottom()>runBottom+tableMinHSpace) {
				if (l.getVLineX() < runLeft) {
					left = l;
				}
				if (l.getVLineX() > runRight) {
					right = l;
					break;
				}
			}
		}
		// In case of 3 sided rectangle, create the missing side
		if (top!=null && bottom!=null && left!=null && right==null) {
			if (top.getHLineRight()==bottom.getHLineRight()) {
				right = new PageGraphicsItem(PageGraphicsType.GR_V_LINE, top.getHLineRight(), top.getHLineY(), bottom.getHLineY(), top.getColor());
			} 
		}
		else if (top!=null && bottom!=null && left==null && right!=null) {
			if (top.getHLineLeft()==bottom.getHLineLeft()) {
				left = new PageGraphicsItem(PageGraphicsType.GR_V_LINE, top.getHLineLeft(), top.getHLineY(), bottom.getHLineY(), top.getColor());
			} 
		}
		else if (top!=null && bottom==null && left!=null && right!=null) {
			if (left.getVLineBottom()==right.getVLineBottom()) {
				bottom = new PageGraphicsItem(PageGraphicsType.GR_H_LINE, left.getVLineX(), right.getVLineX(), left.getVLineBottom(), left.getColor());
			} 
		}
		else if (top==null && bottom!=null && left!=null && right!=null) {
			if (left.getVLineTop()==right.getVLineTop()) {
				top = new PageGraphicsItem(PageGraphicsType.GR_H_LINE, left.getVLineX(), right.getVLineX(), left.getVLineTop(), left.getColor());
			} 
		}
		
		// Verify that lines are connecting or crossing
		final boolean topLeftMatch = cornerMatch(top,left); 
		final boolean topRightMatch = cornerMatch(top, right); 
		final boolean bottomLeftMatch = cornerMatch(bottom,left); 
		final boolean bottomRightMatch = cornerMatch(bottom, right); 

//		final double leftValue = ((topLeftMatch && bottomLeftMatch) || (left!=null && (top==null || bottom==null)))?left.getVLineX():0;
//		final double topValue = ((topLeftMatch && topRightMatch) || (top!=null && (left==null || right==null)))?top.getHLineY():0;
//		final double rightValue = ((topRightMatch && bottomRightMatch) || (right!=null && (top==null || bottom==null)))?right.getVLineX():maxPageWidth;
//		final double bottomValue = ((bottomLeftMatch && bottomRightMatch) || (bottom!=null && (left==null || right==null)))?bottom.getHLineY():maxPageHeight;
		
		final double leftValue = (topLeftMatch && bottomLeftMatch)?left.getVLineX():0;
		final double topValue = (topLeftMatch && topRightMatch)?top.getHLineY():0;
		final double rightValue = (topRightMatch && bottomRightMatch)?right.getVLineX():maxPageWidth;
		final double bottomValue = (bottomLeftMatch && bottomRightMatch)?bottom.getHLineY():maxPageHeight;

		final ClaPdfPage.PRectangle result = new ClaPdfPage.PRectangle(leftValue,topValue,rightValue,bottomValue);
		return result;
	}

	public static boolean cornerMatch(final PageGraphicsItem hLine, final PageGraphicsItem vLine) {
		if (hLine == null || vLine == null) {
			return false;
		}
		return 
				inRange(hLine.getHLineY(), vLine.getVLineTop(), vLine.getVLineBottom(), cornerMargin) && 
				inRange(vLine.getVLineX(), hLine.getHLineLeft(), hLine.getHLineRight(), cornerMargin);
	}

	public static boolean inRange(final double v, final double lowRange, final double highRange, final double margin) {
		return ((v+margin) > lowRange && (v-margin) < highRange);
	}

	public int getVLinesCount() {
		return vLines.size();
	}
	public int getHLinesCount() {
		return hLines.size();
	}
	public int getRectanglesCount() {
		return rectangles.size();
	}
}
