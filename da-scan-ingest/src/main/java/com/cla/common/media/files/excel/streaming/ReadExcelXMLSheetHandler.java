package com.cla.common.media.files.excel.streaming;

import com.cla.common.domain.dto.excel.ExcelSheetDimensions;
import com.cla.common.domain.dto.mediaproc.excel.ExcelCell;
import com.cla.common.domain.dto.mediaproc.excel.ExcelWorkbook;
import com.cla.common.media.files.excel.ExcelFileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.BuiltinFormats;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.RichTextString;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.model.SharedStringsTable;
import org.apache.poi.xssf.model.StylesTable;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFEvaluationWorkbook;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * Created by uri on 19/10/2015.
 */
public class ReadExcelXMLSheetHandler extends DefaultHandler {
    private SharedStringsTable sst;
    private String lastContents;
    private boolean nextIsString;

    private boolean warnOnFormulas = true; //We only print up to 1 warning on formulas per file

    private StreamingExcelCell currentCell;

    private int numberOfCells = 0;
    Logger logger = LoggerFactory.getLogger(ReadExcelXMLSheetHandler.class);

    private StylesTable stylesTable;

    private final DataFormatter dataFormatter = new DataFormatter();

    private ExcelWorkbook excelWorkbook;

    private int currentSheetIndex;
    private String currentSheetIndexAsString;
    private String currentSheetName;

    private ExcelFileUtils.ExcelIssuesCounters excelIssuesCounters;

    private ExcelSheetDimensions currentSheetDimensions;

    private int currentRowsCount = -1;
    private int currentCellsInRow = 0;

    private int lastRow = -1;

    private boolean isTokenMatcherActive;
    private XSSFWorkbook workbook;

    private Object evaluationWorkbookWrapper;

    private Map<Integer, SharedFormulaHolder> sharedFormulas = new HashMap<>();;

    SharedFormulaHolder currentSharedFormula;
    private StreamingExcelSheet currentSheet;

    static Pattern refPattern = Pattern.compile("(?<=\\D)(?=\\d)|(?<=\\d)(?=\\D)");

    String currentCellStyleString = null;

    boolean ingestCellContentOnly = false;
    private Integer currentRowNonBlankCellsCount;

    public ReadExcelXMLSheetHandler(SharedStringsTable sst, StylesTable stylesTable, ExcelWorkbook excelWorkbook, boolean isTokenMatcherActive, XSSFWorkbook workbook) {
        this.sst = sst;
        this.stylesTable = stylesTable;
        this.excelWorkbook = excelWorkbook;
        this.isTokenMatcherActive = isTokenMatcherActive;
        this.workbook = workbook;
        excelIssuesCounters = new ExcelFileUtils.ExcelIssuesCounters();
        evaluationWorkbookWrapper =
                XSSFEvaluationWorkbook.create((XSSFWorkbook) workbook);

    }


    public void startElement(String uri, String localName, String name,
                             Attributes attributes) throws SAXException {

        switch (name) {
            case "c": // c => cell
                handleCellStart(attributes);
                break;
            case "f":
                handleFormula(attributes);
            case "worksheet":
                break;
            case "dimension":
                break;
            case "sheetViews":
                break;
            case "sheetView":
                //Attributes: rightToLeft, tabSelected, workbookViewId,
                break;
            case "sheetFormatPr":
                //Attributes: defaultRowHeight, x14ac
                break;
            case "sheetData":
                //Attributes: defaultRowHeight, x14ac, workbookViewId,
                break;
            case "pageMargins":
                break;
            default:
//                logger.debug(name);
                break;
        }
        // Clear contents cache
        lastContents = "";
    }


    public void endElement(String uri, String localName, String name)
            throws SAXException {
        // Process the last contents as required.
        // Do now, as characters() may be called more than once
//        if(nextIsString) {
//            int idx = Integer.parseInt(lastContents);
//            lastContents = new XSSFRichTextString(sst.getEntryAt(idx)).toString();
//            nextIsString = false;
//        }

        switch (name) {
            case "v": // v => contents of a cell
                handleCellValue();
//                logger.debug(lastContents);
                break;
            case "row":
                break;
            case "c":
                ingestCurrentCell();
                break;
            case "f":
                handleFormulaValue();
                break;
        }
    }

    private void handleCellStart(Attributes attributes) {
        currentSharedFormula  = null;
        ingestCellContentOnly = false;
        String ref = attributes.getValue("r");
        currentCellStyleString = attributes.getValue("s");
        String[] coord = refPattern.split(ref);
//        String[] coord = ref.split("(?<=\\D)(?=\\d)|(?<=\\d)(?=\\D)");


        int columnIndex = CellReference.convertColStringToIndex(coord[0]);
        int rowIndex = Integer.parseInt(coord[1]) - 1;
        currentCell = new StreamingExcelCell(columnIndex, rowIndex,workbook,currentSheet);

        int nonBlankRows = currentSheetDimensions.getNonBlankRows();
        currentRowNonBlankCellsCount = currentSheetDimensions.getNonBlankCellCount().get(currentCell.getRowIndex());

        if (currentRowNonBlankCellsCount == null) {
            currentRowNonBlankCellsCount = 0;
        }
        if (currentCell.getRowIndex() > lastRow) {
            currentCellsInRow = 0;
            lastRow = currentCell.getRowIndex();
            if (currentRowNonBlankCellsCount > 0) {
                currentRowsCount++;
            }
        }

        //Check if ingest current cell only
        ingestCellContentOnly = ExcelFileUtils.shouldIngestCellContentOnly(currentRowsCount, nonBlankRows);

        String cellType = attributes.getValue("t");
        if(cellType != null) {
            currentCell.setType(cellType);
        }
        else {
            //The default is "n"
            currentCell.setType("n");
        }

        if (!ingestCellContentOnly) {
            XSSFCellStyle style = extractStyle(currentCellStyleString);
            currentCell.setCellStyle(style);
            setFormatString(style, currentCell);
        }
    }

    /**
     * Read the numeric format string out of the styles table for this cell. Stores
     * the result in the Cell.
     * @param cell
     */
    void setFormatString(XSSFCellStyle style, StreamingExcelCell cell) {
        if(style != null) {
            cell.setNumericFormatIndex(style.getDataFormat());
            String formatString = style.getDataFormatString();

            if(formatString != null) {
                cell.setNumericFormat(formatString);
            } else {
                cell.setNumericFormat(BuiltinFormats.getBuiltinFormat(cell.getNumericFormatIndex()));
            }
        } else {
            cell.setNumericFormatIndex(null);
            cell.setNumericFormat(null);
        }
    }

    private XSSFCellStyle extractStyle(String cellStyleString) {
        XSSFCellStyle style = null;

        if(cellStyleString != null) {
            style = stylesTable.getStyleAt(Integer.parseInt(cellStyleString));
        } else if(stylesTable.getNumCellStyles() > 0) {
            style = stylesTable.getStyleAt(0);
        }
        return style;
    }

    private void handleFormula(Attributes attributes) {
        if (ingestCellContentOnly) {
            return;
        }
        String t = attributes.getValue("t");
        String si = attributes.getValue("si");
        String ref = attributes.getValue("ref");
        currentCell.setFormulaType(t);
        if ("shared".equals(t)) {
            currentSharedFormula = new SharedFormulaHolder();
            currentSharedFormula.setSi(si);
            currentSharedFormula.setRef(ref);
            currentSharedFormula.setSheetIndex(currentSheetIndex);
        }
        else if ("array".equals(t) && ref != null){
                currentSheet.addArrayFormula(CellRangeAddress.valueOf(ref));
        }
//        logger.debug("handle formula cell: {}:{}:{}", t, si,ref);
    }

    private void handleFormulaValue() {
        if ("shared".equals(currentCell.getFormulaType()) && currentSharedFormula != null && currentSharedFormula.getSi() != null) {
            Integer sharedIndex = Integer.valueOf(currentSharedFormula.getSi());
            if (StringUtils.isNotEmpty(lastContents)) {
//                logger.debug("Add shared formula:" + lastContents);
                currentSharedFormula.setFormulaString(lastContents);
                sharedFormulas.put(sharedIndex, currentSharedFormula);

                currentCell.setSharedFormulaHolder(currentSharedFormula);
                currentCell.setCellFormula(lastContents);
            }
            else {
                SharedFormulaHolder sharedFormulaHolder = sharedFormulas.get(sharedIndex);
                if (sharedFormulaHolder == null) {
                    if (warnOnFormulas) {
                        logger.warn("Couldn't find shared formula for excel cell {} (current shared formula:{})",currentCell,currentSharedFormula);
                        warnOnFormulas = false;
                    }
                    else {
                        logger.debug("Couldn't find shared formula for excel cell {} (current shared formula:{})",currentCell,currentSharedFormula);
                    }
                }
                else {
//                logger.debug("Used shared formula:" + sharedFormulaHolder);
                    currentCell.setSharedFormulaHolder(sharedFormulaHolder);
                    currentCell.setCellFormula(sharedFormulaHolder.getFormulaString());
                }
            }
        }
        else if ("array".equals(currentCell.getFormulaType())) {
            currentCell.setCellFormula(lastContents);
//            currentCell.setArrayFormulas(arrayFormulas);;
        }
        else {
            currentCell.setCellFormula(lastContents);
        }
    }

    private void handleCellValue() {
        currentCell.setRawContents(unformattedContents());
        currentCell.setContents(formattedContents());
        currentCell.setRichTextString(formattedRichTextString());
    }

    private void ingestCurrentCell() {
        String sheetName = currentSheetIndexAsString;
        String cellRefStr = calculateCellReferenceString(lastRow);

        ExcelCell cellValue = ExcelFileUtils.createExcelCell(excelIssuesCounters, workbook, evaluationWorkbookWrapper, currentSheetIndex, currentCell);
        if (cellValue == null) {
            logger.debug("Failed to ingest cellValue");
            return;
        }
        if (ingestCellContentOnly) {
//            String cellStringValue = ExcelFileUtils.getCellStringValue(currentCell);
            excelWorkbook.addExcelCellContentOnly(sheetName,currentSheetName,cellRefStr,cellValue);
        }
        else {
            //Ingest more then just contents
            int nonBlankRows = currentSheetDimensions.getNonBlankRows();
            boolean detailedAnalysis = ExcelFileUtils.isDetailedAnalysis(excelIssuesCounters, excelWorkbook, nonBlankRows, currentRowsCount, currentRowNonBlankCellsCount, currentCellsInRow);
            excelWorkbook.addExcelCell(sheetName,
                    currentCell.getColumnIndex(), currentCell.getRowIndex(),
                    cellValue,
                    currentSheetName,
                    detailedAnalysis,
                    cellRefStr,
                    isTokenMatcherActive);
        }

        if (currentCell.getCellType() != Cell.CELL_TYPE_BLANK &&
                currentCell.getCellType() != Cell.CELL_TYPE_ERROR) {
            numberOfCells++;    // increment non-empty cells counter
            currentCellsInRow++;
        }
        if (currentCell.getCellType() == Cell.CELL_TYPE_BLANK) {
            excelIssuesCounters.addBlankCell();
        }
        if (currentCell.getCellType() == Cell.CELL_TYPE_ERROR) {
            excelIssuesCounters.addErrorCell();
        }

    }


    private String calculateCellReferenceString(int rowNumber) {
            String cellRefStr = null;
            try {
                final CellReference celRef = new CellReference(currentCell.getRowIndex(),currentCell.getColumnIndex(),false,false);
                cellRefStr = currentSheetName + ":" + celRef.formatAsString();
            } catch (final Exception e) {}
            if (cellRefStr == null) {
                cellRefStr = currentSheetName+":"+currentCell.getColumnIndex()+"/"+rowNumber;
            }
            return cellRefStr;
    }

    public void characters(char[] ch, int start, int length)
            throws SAXException {
        lastContents += new String(ch, start, length);
    }

    public int getNumberOfCells() {
        return numberOfCells;
    }

    /**
     * Returns the contents of the cell, with no formatting applied
     * @return
     */
    String unformattedContents(){
        switch(currentCell.getType()) {
            case "s":           //string stored in shared table
                int idx = Integer.parseInt(lastContents);
                return new XSSFRichTextString(sst.getEntryAt(idx)).toString();
            case "inlineStr":   //inline string (not in sst)
                return new XSSFRichTextString(lastContents).toString();
            default:
                return lastContents;
        }
    }

    /**
     * Tries to format the contents of the last contents appropriately based on
     * the type of cell and the discovered numeric format.
     *
     * @return
     */
    String formattedContents() {
        switch(currentCell.getType()) {
            case "s":           //string stored in shared table
                int idx = Integer.parseInt(lastContents);
                return new XSSFRichTextString(sst.getEntryAt(idx)).toString();
            case "inlineStr":   //inline string (not in sst)
                return new XSSFRichTextString(lastContents).toString();
            case "str":         //forumla type
                return new XSSFRichTextString(lastContents).toString();
//                return '"' + lastContents + '"';
            case "e":           //error type
                return "ERROR:  " + lastContents;
            case "n":           //numeric type
                if(currentCell.getNumericFormat() != null && lastContents.length() > 0) {
                    return dataFormatter.formatRawCellContents(
                            Double.parseDouble(lastContents),
                            currentCell.getNumericFormatIndex(),
                            currentCell.getNumericFormat());
                } else {
                    return lastContents;
                }
            default:
                return lastContents;
        }
    }


    RichTextString formattedRichTextString() {
        switch(currentCell.getType()) {
            case "s":           //string stored in shared table
                int idx = Integer.parseInt(lastContents);
                return new XSSFRichTextString(sst.getEntryAt(idx));
            case "n":           //numeric type
                return null;
            default:
                return new XSSFRichTextString(lastContents);
        }
    }

    public void setCurrentSheetDetails(int currentSheetIndex, String currentSheetName, ExcelSheetDimensions currentSheetDimensions) {
        this.currentSheetIndex = currentSheetIndex;
        this.currentSheetIndexAsString = String.valueOf(currentSheetIndex);
        this.currentSheetName = currentSheetName;
        this.currentSheetDimensions = currentSheetDimensions;
        currentSheet = new StreamingExcelSheet(currentSheetName,workbook);
    }

    public ExcelFileUtils.ExcelIssuesCounters getExcelIssuesCounters() {
        return excelIssuesCounters;
    }
}
