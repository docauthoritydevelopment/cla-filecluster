package com.cla.common.media.files.excel;

import com.cla.common.exceptions.NotSupportedException;
import org.apache.poi.ss.SpreadsheetVersion;
import org.apache.poi.ss.formula.EvaluationName;
import org.apache.poi.ss.formula.FormulaParsingWorkbook;
import org.apache.poi.ss.formula.SheetIdentifier;
import org.apache.poi.ss.formula.ptg.Area3DPxg;
import org.apache.poi.ss.formula.ptg.NamePtg;
import org.apache.poi.ss.formula.ptg.NameXPxg;
import org.apache.poi.ss.formula.ptg.Ptg;
import org.apache.poi.ss.formula.ptg.Ref3DPxg;
import org.apache.poi.ss.formula.ptg.StringPtg;
import org.apache.poi.ss.usermodel.Name;
import org.apache.poi.ss.usermodel.Table;
import org.apache.poi.ss.util.AreaReference;
import org.apache.poi.ss.util.CellReference;
import org.springframework.stereotype.Component;


@Component
public class ExcelExtractorUtils {
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(ExcelExtractorUtils.class);


	public class ClaFormulaParsingWorkbook implements FormulaParsingWorkbook {
		
		private final String fileName;

		public ClaFormulaParsingWorkbook(final String fileName) {
			this.fileName = fileName;
		}

		public String getFileName() {
			return fileName;
		}

		@Override
		public EvaluationName getName(final String name, final int sheetIndex) {
			logger.trace("{}:getName({},{})", fileName, name, sheetIndex);
			return new ClaName(name, sheetIndex);
		}

        @Override
        public Name createName() {
			throw new NotSupportedException();
        }

        @Override
        public Table getTable(String name) {
			throw new NotSupportedException();
        }

        @Override
		public Ptg getNameXPtg(final String name, final SheetIdentifier sheet) {
			logger.trace("{}:getNameXPtg({},{})", fileName, name, (sheet == null)?"null":sheet.getSheetIdentifier().getName());

			if (sheet == null) {
				return new NameXPxg(null, name);
			}
			if (sheet._sheetIdentifier == null) {
				if (sheet._bookName != null) {
					return new NameXPxg(sheet._bookName+"!", name);
				} else {
					return new NameXPxg(null, name);
				}
			}

			// Use the sheetname and process
			final String sheetName = sheet._sheetIdentifier.getName();

			if (sheet._bookName != null) {
				return new NameXPxg(sheet._bookName+"!"+sheetName, name);
			} else {
				return new NameXPxg(sheetName, name);
			}
		}

		@Override
		public Ptg get3DReferencePtg(final CellReference cell, final SheetIdentifier sheet) {
			logger.trace("{}:get3DReferencePtg({},{})", fileName, cell.formatAsString(), sheet.getSheetIdentifier().getName());
			return new Ref3DPxg(sheet, cell);
		}

		@Override
		public Ptg get3DReferencePtg(final AreaReference area, final SheetIdentifier sheet) {
			logger.trace("{}:get3DReferencePtg({},{})", fileName, area.formatAsString(), sheet.getSheetIdentifier().getName());
			return new Area3DPxg(sheet,area);
		}

		@Override
		public int getExternalSheetIndex(final String sheetName) {
			logger.trace("{}:getExternalSheetIndex({}), fileName", sheetName);
			return 0;
		}

		@Override
		public int getExternalSheetIndex(final String workbookName, final String sheetName) {
			logger.trace("{}:getExternalSheetIndex({},{})", fileName, workbookName, sheetName);
			return 0;
		}

		@Override
		public SpreadsheetVersion getSpreadsheetVersion() {
			logger.trace("{}:getSpreadsheetVersion()", fileName);
			return SpreadsheetVersion.EXCEL2007;
		}
	}

    private class ClaName implements EvaluationName {
        private final String _name;
        private final int _index;

        public ClaName(final String name, final int index) {
            _name = name;
            _index = index;
        }



        @Override
		public Ptg[] getNameDefinition() {
			logger.trace("getNameDefinition({},{})", _name, _index);
        	final Ptg[] pa = new Ptg[1];
        	pa[0] = new StringPtg(_name);
            return pa;
        }
        @Override
		public String getNameText() {
			logger.trace("getNameText({},{})", _name, _index);
            return _name;
        }
        @Override
		public boolean hasFormula() {
			logger.trace("hasFormula({},{})", _name, _index);
            return false;
        }
        @Override
		public boolean isFunctionName() {
			logger.trace("isFunctionName({},{})", _name, _index);
            return false;
        }
        @Override
		public boolean isRange() {
			logger.trace("isRange({},{})", _name, _index);
            return false;
        }
        @Override
		public NamePtg createPtg() {
			logger.trace("createPtg({},{})", _name, _index);
            return new NamePtg(_index);
        }
    }
    
    public FormulaParsingWorkbook getFormulaParsingWorkbook(final String fileName) {
    	return new ClaFormulaParsingWorkbook(fileName);
    }

	
}
