package com.cla.common.media.files.other;

import com.google.common.collect.Lists;
import org.apache.commons.io.IOUtils;
import org.apache.tika.Tika;
import org.apache.tika.metadata.Metadata;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by: yael
 * Created on: 2/21/2019
 */
@Component
public class ContentExtractorService {

    @Autowired
    private Tika tika;

    @Value("${ingester.other.mime-exception:}")
    private String[] mimeException;

    @Value("${ingester.other.extension-exception:}")
    private String[] extensionException;

    private List<String> mimeTikaException;
    private List<String> extensionTikaException;

    @PostConstruct
    public void init() {
        mimeTikaException = (mimeException != null && mimeException.length > 0) ? Lists.newArrayList(mimeException) : new ArrayList<>();
        extensionTikaException = (extensionException != null && extensionException.length > 0) ? Lists.newArrayList(extensionException) : new ArrayList<>();
    }

    public String detectMime(InputStream inputStream) throws Exception {
        return tika.detect(inputStream);
    }

    public String getText(InputStream inputStream, String mime, String fileExtension, String filePath) throws Exception {
        String text;

        if (mimeTikaException.contains(mime) && extensionTikaException.contains(fileExtension.toLowerCase())) {
            text = IOUtils.toString(inputStream, StandardCharsets.UTF_8);
        } else {
            Metadata tmd = new Metadata();
            tmd.set(Metadata.RESOURCE_NAME_KEY, filePath);
            text = tika.parseToString(inputStream, tmd);
        }

        return text;
    }
}
