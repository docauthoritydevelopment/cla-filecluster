package com.cla.common.media.files.excel.streaming;

import com.cla.common.allocation.ExcelMemoryAllocationStrategy;
import com.cla.common.allocation.IngestionResourceManager;
import com.cla.common.domain.dto.ClaDocProperties;
import com.cla.common.domain.dto.FileMediaType;
import com.cla.common.domain.dto.excel.ExcelSheetDimensions;
import com.cla.common.domain.dto.mediaproc.excel.ExcelParseParams;
import com.cla.common.domain.dto.mediaproc.excel.ExcelWorkbook;
import com.cla.common.domain.dto.mediaproc.excel.ExcelWorkbookMetaData;
import com.cla.common.media.files.Extractor;
import com.cla.common.media.files.excel.ClaXSSFReader;
import com.cla.common.media.files.excel.ExcelFileUtils;
import com.cla.common.media.files.excel.ExcelMetadataUtils;
import com.cla.common.media.files.word.FileMetadataUtils;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.error.ProcessingErrorType;
import com.cla.connector.utils.TimeSource;
import com.cla.common.utils.thread.ClaPropertySet;
import com.cla.common.utils.thread.ClaThreadPropertiesSet;
import com.cla.common.utils.tokens.HashedTokensConsumer;
import com.google.common.collect.Maps;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.poi.POIXMLException;
import org.apache.poi.hssf.eventusermodel.HSSFEventFactory;
import org.apache.poi.hssf.eventusermodel.HSSFRequest;
import org.apache.poi.hssf.model.InternalWorkbook;
import org.apache.poi.hssf.record.Record;
import org.apache.poi.hssf.record.RecordFactory;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.poifs.crypt.Decryptor;
import org.apache.poi.poifs.crypt.EncryptionInfo;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.eventusermodel.XSSFReader;
import org.apache.poi.xssf.model.SharedStringsTable;
import org.apache.poi.xssf.model.StylesTable;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.xml.sax.ContentHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import javax.annotation.PostConstruct;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.security.MessageDigest;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toSet;

/**
 * Streaming Excel files ingestion
 *
 * Created by uri on 18/10/2015.
 */
@Service
public class ExcelStreamingFilesExtractor extends Extractor<ExcelWorkbook> {
    private static final Logger logger = LoggerFactory.getLogger(ExcelStreamingFilesExtractor.class);

    @Autowired
    private IngestionResourceManager ingestionResourceManager;

    @Autowired
    private ExcelMemoryAllocationStrategy allocationStrategy;

    @Value("${tokenMatcherActive}")
    private boolean isTokenMatcherActive;

    @Value("${ingester.maxExcelFileSize:0}")
    private long maxExcelFileSize;

    @Value("${ingester.excel.maxRecordsWarnCount:400000}")
    private int excelMaxRecordsCount;

    @Value("${ingester.excel.recordFilteringThreshold:5000}")
    private int excelRecordFilteringThreshold;

    @Value("${ingester.excel.filterRecordsFromWorkbookBeforeCreation:true}")
    private boolean filterRecordsFromWorkBookBeforeCreation;

    @Value("${ingester.excel.bufferedInputStreamSize:100000}")
    private int bufferedInputStreamSize;

    @Value("${extraction.userContentSignatureEnabled:true}")
    private boolean userContentSignatureEnabled;

    @Value("${ingester.excel.useTableHeadingsAsTitles:true}")
    private boolean useTableHeadingsAsTitles;

    @Value("${ingester.excel.xmlFilteringSizeThreshold:1000000}")
    private int xmlFilteringSizeThreshold;

    @Value("${ingester.excel.xmlFilteringMaxObjects:1000000}")
    private int xmlFilteringMaxObjects;

    @Value("${ingester.excel.singleTokenNumberedTitleRateReduction:3}")
    private int singleTokenNumberedTitleRateReduction;

    @Value("${ingester.excel.ignoredSheetNamePrefixesAsTitles:sheet,\u05D2\u05DC\u05D9\u05D5\u05DF}")
    private String[] ignoredSheetNamePrefixesAsTitles;

    private Set<String> ignoredSheetNamePrefixesAsTitlesSet = null;
    private TimeSource timeSource = new TimeSource();

    @PostConstruct
    public void init() {
        if (ignoredSheetNamePrefixesAsTitlesSet == null && ignoredSheetNamePrefixesAsTitles.length > 0) {
            ignoredSheetNamePrefixesAsTitlesSet = Stream.of(ignoredSheetNamePrefixesAsTitles)
                    .map(String::toLowerCase)
                    .collect(toSet());
        }
    }

    @Override
    //TODO: consider using this method instead of ingestWorkbook
    public ExcelWorkbook extract(InputStream is, String path, boolean extractMetadata, ClaFilePropertiesDto claFileProperties) {
        return ingestWorkbook(path, is, claFileProperties);
    }

    public ExcelWorkbook ingestWorkbook(String fileName, InputStream is, ClaFilePropertiesDto claFileProperties) {
        return ingestWorkbook(is, fileName, null, claFileProperties);
    }

    public ExcelWorkbook ingestWorkbook(final InputStream is, final String path,
                                        final HashedTokensConsumer hashedTokensConsumer, ClaFilePropertiesDto claFilePropertiesDto) {

        int ingestionErrors = 0;
        logger.debug("start ingesting {} ...", path);
        ExcelParseParams excelParseParams = new ExcelParseParams(useTableHeadingsAsTitles,
                singleTokenNumberedTitleRateReduction,
                ignoredSheetNamePrefixesAsTitlesSet,
                patternSearchActive, useFormattedPatterns);
        final ExcelWorkbook excelWorkbook = new ExcelWorkbook(excelParseParams);
        excelWorkbook.setUserSignatureDigest(DigestUtils.getSha1Digest());
        excelWorkbook.setUserContentSignatureEnabled(userContentSignatureEnabled);

        try {
            excelWorkbook.setFileName(path);
            excelWorkbook.setHashedTokensConsumer(hashedTokensConsumer);
            if (claFilePropertiesDto == null) {
                logger.warn("ClaFilePropertiesDto is null");
//                ClaFileProperties claFileProperties = new ClaFileProperties(path);
//                claFilePropertiesDto = ModelDtoConversionUtils.convertClaFileProperties(claFileProperties);
            }
            if (ExcelFileUtils.checkMaxExcelFileSizeLimit(maxExcelFileSize,path, excelWorkbook, claFilePropertiesDto)) {
                return excelWorkbook;
            }

            //Extract metadata by reading the excel file without reading the sheets
            long start = System.currentTimeMillis();
            readExcelFile(is, path, excelWorkbook, claFilePropertiesDto);
            long duration = System.currentTimeMillis() - start;
            long size = 0;
            if (claFilePropertiesDto != null) {
                size = claFilePropertiesDto.getFileSize();
            }
            if (duration > 1000) {
                logger.debug("Finished ingesting {} of size {} in {} ms (phases breakdown:{})", path,size,duration,excelWorkbook.getPhasesDetails());
            } else {
                logger.debug("Finished ingesting {} of size {} in {} ms", path,size,duration);
            }
        }
        catch(Exception ex){
            logger.info("Failed to read file {}. Error is {}.", path, ex.getMessage(), ex);
            excelWorkbook.setErrorType(ProcessingErrorType.ERR_UNKNOWN);
            excelWorkbook.setExceptionText(ex.getMessage());
        }

        return excelWorkbook;
    }


    private void readExcelFile(InputStream fin, final String path, ExcelWorkbook excelWorkbook, ClaFilePropertiesDto claFileProperties) {
        int excelWorkbooksMemoryAllocationMB = 0;
        boolean resourcesAquired = false;

        ClaPropertySet claPropertySet = ClaThreadPropertiesSet.getThreadLocalPropertiesSet();
        claPropertySet.setBoolean(ClaThreadPropertiesSet.KEY_TRIMMED, false);
        claPropertySet.setString(ClaThreadPropertiesSet.KEY_PATH, path);

        if (fin instanceof FileInputStream) {
            logger.warn("FileInputStream given to excel file parser. Shouldn't happen anymore");
            //We need to wrap it in a buffered input stream
            int bufSize = Integer.min(bufferedInputStreamSize, claFileProperties.getFileSize().intValue()+10);
            fin = new BufferedInputStream(fin, bufSize);
            if (bufSize == bufferedInputStreamSize) {
                logger.info("File size {} exceeds bufferedInputStreamSize for {}", claFileProperties.getFileSize(), excelWorkbook.getFileName());
            }
        }
        final FileMediaType fileMediaType = ExcelFileUtils.mediaNameDetector(path);


        Workbook workbook = null;
        try {
            logger.trace("Read excel file {}", path);

            ExcelFileUtils.ExcelIssuesCounters excelIssuesCounters;
            POIFSFileSystem poifs = null;
            Map<String, String> sheetFullNames;
            int numberOfCells;

            if (fileMediaType == FileMediaType.EXCELXML) {
                OPCPackage opcPackage = open(path,fin);
                OPCPackage opcPackageNoSheets;

                try {
                    sheetFullNames = Maps.newHashMap();
                    opcPackageNoSheets = ExcelFileUtils.cloneNoSheets(opcPackage);
                    try {
                        workbook = new XSSFWorkbook(opcPackageNoSheets);
                    }
                    catch (NullPointerException e) {
                        logger.info("Can not open file {}. Unsupported format (probably strict open Excel XML)",path,e);
                        excelWorkbook.setErrorType(ProcessingErrorType.STRICT_OPEN_XML);
                        return;
                    }

                    excelWorkbooksMemoryAllocationMB = allocationStrategy.calculateRequiredMemory(claFileProperties.getFileSize(),
                            fileMediaType, path, opcPackage, excelWorkbook);

                    if (excelWorkbooksMemoryAllocationMB > 0) {
                        resourcesAquired = ingestionResourceManager.acquire(excelWorkbooksMemoryAllocationMB);
                        if (logger.isTraceEnabled()) {
                            logger.trace("resource acquired {}. {} available", excelWorkbooksMemoryAllocationMB, ingestionResourceManager.getAvailable());
                        }
                    }

                    long start = timeSource.currentTimeMillis();
                    ReadExcelXMLSheetHandler readExcelXMLSheetHandler = readSheetsFromExcelXML(sheetFullNames, opcPackage, excelWorkbook, workbook);
                    long duration = timeSource.millisSince(start);

                    excelWorkbook.addPhaseDetail("readExcelContents", duration);

                    MessageDigest userSignatureDigest = excelWorkbook.getUserSignatureDigest();
                    String userContentSignature = userSignatureDigest != null ?
                            Base64.getEncoder().encodeToString(userSignatureDigest.digest()) : null;
                    claFileProperties.setUserContentSignature(userContentSignature);

                    numberOfCells = readExcelXMLSheetHandler.getNumberOfCells();
                    excelIssuesCounters = readExcelXMLSheetHandler.getExcelIssuesCounters();
                }
                catch (OpenXML4JException e) {
                    logger.info("Can not open file {}. Exception: {}", path, e);
                    excelWorkbook.setErrorType(ProcessingErrorType.ERR_UNKNOWN);
                    excelWorkbook.setExceptionText(e.getMessage());
                    return;
                }
                catch (SAXException e) {
                    logger.info("Failed to read XML from excel file {}.", path, e);
                    excelWorkbook.setErrorType(ProcessingErrorType.ERR_PARSING_FAILURE);
                    excelWorkbook.setExceptionText(e.getMessage());
                    return;
                }
            }
            else {  // (fileMediaType == FileMediaType.EXCEL)
                excelWorkbooksMemoryAllocationMB = allocationStrategy.calculateBaseRequiredMemory(
                        claFileProperties.getFileSize(), fileMediaType);

                if (excelWorkbooksMemoryAllocationMB > 0) {
                    resourcesAquired = ingestionResourceManager.acquire(excelWorkbooksMemoryAllocationMB);
                    if (logger.isTraceEnabled()) {
                        logger.trace("resource acquired {}. {} available", excelWorkbooksMemoryAllocationMB, ingestionResourceManager.getAvailable());
                    }
                }

                poifs = getPoifsFileSystem(fin);
                ReadExcelDimensionsEventListener readExcelDimensionsEventListener = readExcelDimensions(poifs);
                //Create an internal workbook that doesn't contain cells
                long start = System.currentTimeMillis();
                InternalWorkbook internalWorkbook = createInternalWorkbook(path,readExcelDimensionsEventListener.getRecordList());
                //Create a workbook that doesn't contain cells
                workbook = HSSFWorkbook.create(internalWorkbook);
                long duration = System.currentTimeMillis() - start;
                excelWorkbook.addPhaseDetail("CreateInternalWorkbook",duration);
                for (Map.Entry<Integer, ExcelSheetDimensions> integerExcelSheetDimensionsEntry : readExcelDimensionsEventListener.getExcelSheetDimensionsMap().entrySet()) {
                    Integer key = integerExcelSheetDimensionsEntry.getKey();
                    int nonBlankRows = integerExcelSheetDimensionsEntry.getValue().getNonBlankRows();
                    if (nonBlankRows > 1000) {
                        logger.debug("Sheet {} dimensions: non-blank rows: {} > 1000",key, nonBlankRows);
                    }
                }

                // get the Workbook (excel part) stream in a InputStream
                start = System.currentTimeMillis();
                ReadExcelContentEventListener readExcelContentEventListener = readExcelContents(excelWorkbook,
                        readExcelDimensionsEventListener.getExcelSheetDimensionsMap(),
                        internalWorkbook,
                        (HSSFWorkbook)workbook ,
                        poifs );
                duration = System.currentTimeMillis() - start;
                excelWorkbook.addPhaseDetail("readExcelContents",duration);
                excelIssuesCounters = readExcelContentEventListener.getExcelIssuesCounters();
                sheetFullNames = readExcelContentEventListener.getSheetFullNames();
                numberOfCells = readExcelContentEventListener.getNumberOfCells();
//                excelWorkbook.addPhaseDetail("handleCellRecords",readExcelContentEventListener.getTotalHandelCellRecordMilli());
                excelWorkbook.addPhaseDetail("totalAddExcelCells",readExcelContentEventListener.getTotalAddExcelCellMillis());
                excelWorkbook.addPhaseDetail("totalAddExcelCellsContentOnly",readExcelContentEventListener.getTotalAddExcelCellContentMillis());
            }

            excelWorkbook.processStyle();

            parseExcelMetaData(workbook, poifs, path, excelWorkbook, claFileProperties);

            // create metadata statistics
            ExcelMetadataUtils.summarizeWorkbookMetadata(excelWorkbook);

            handleIssuesEncountered(path, excelIssuesCounters);
            updateMissingMetadata(excelWorkbook, sheetFullNames, numberOfCells);

        } catch (IOException e) {
            logger.info("Can not open file {}. IOException {}. {}", path, e.getMessage(), e);
            excelWorkbook.setErrorType(ProcessingErrorType.ERR_IO);
            excelWorkbook.setExceptionText(e.getMessage());
        }
        finally {
            Boolean trimmed = claPropertySet.getBoolean(ClaThreadPropertiesSet.KEY_TRIMMED);
            if (trimmed) {
                logger.warn("File {} was trimmed.", path);
                logger.debug("ThreadPropertiesSet={}", claPropertySet);
            }
            if (workbook != null) {
                try {
                    workbook.close();
                } catch (final IOException e) {
                    // ignore exceptions at this point
                }
            }
            if (resourcesAquired && excelWorkbooksMemoryAllocationMB > 0) {
                ingestionResourceManager.release(excelWorkbooksMemoryAllocationMB);
                logger.debug("workbookSemaphore released {}. {} available", excelWorkbooksMemoryAllocationMB, ingestionResourceManager.getAvailable());
                resourcesAquired = false;
            }
        }
    }




    private ReadExcelXMLSheetHandler readSheetsFromExcelXML(Map<String, String> sheetFullNames,
                                                            OPCPackage opcPackage,
                                                            ExcelWorkbook excelWorkbook,
                                                            Workbook workbook) throws IOException, OpenXML4JException, SAXException {
        ClaXSSFReader xssfReader = new ClaXSSFReader(opcPackage, xmlFilteringSizeThreshold, xmlFilteringMaxObjects);
        SharedStringsTable sharedStringsTable = xssfReader.getSharedStringsTable();
        StylesTable sharedStylesTable = xssfReader.getStylesTable();
        logger.debug("Excel XML metrics: path={} sharedStringsCount={} stylesTableCount={}",
                excelWorkbook.getFileName(),
                (sharedStringsTable==null?0:sizeNotNull(sharedStringsTable.getItems())),
                getXmlStylesTableItemsCount(sharedStylesTable));

        ReadExcelXMLSheetHandler readExcelXMLSheetHandler = new ReadExcelXMLSheetHandler(sharedStringsTable, sharedStylesTable, excelWorkbook, isTokenMatcherActive, (XSSFWorkbook) workbook);
        XMLReader parser = fetchSheetParser(readExcelXMLSheetHandler);

        ReadExcelXMLDimensionsEventHandler readExcelXMLDimensionsEventHandler = readSheetsDimensions(xssfReader);

        ClaXSSFReader.SheetIterator sheets = xssfReader.getSheetsData();
        int sheetIndex = -1;
        while(sheets.hasNext()) {
            sheetIndex++;
            readSheetContentXML(sheetFullNames, readExcelXMLSheetHandler, parser, readExcelXMLDimensionsEventHandler, sheets, sheetIndex);
        }
        return readExcelXMLSheetHandler;
    }

    static private int getXmlStylesTableItemsCount(StylesTable sharedStylesTable) {
        return (sharedStylesTable == null)? 0 :
                (
                        sharedStylesTable.getNumCellStyles()
                            + sizeNotNull(sharedStylesTable.getNumberFormats())
                            + sizeNotNull(sharedStylesTable.getBorders())
                            + sizeNotNull(sharedStylesTable.getFills())
                            + sizeNotNull(sharedStylesTable.getFonts())
                            + sizeNotNull(sharedStylesTable.getRelations())
                );
    }

    static private int sizeNotNull(Collection col) {
        return (col==null)?0:col.size();
    }
    static private int sizeNotNull(Map map) {
        return (map==null)?0:map.size();
    }

    private void readSheetContentXML(Map<String, String> sheetFullNames, ReadExcelXMLSheetHandler readExcelXMLSheetHandler, XMLReader parser, ReadExcelXMLDimensionsEventHandler readExcelXMLDimensionsEventHandler, ClaXSSFReader.SheetIterator sheets, int sheetIndex) throws IOException, SAXException {
        InputStream sheet = sheets.next();
        if (sheet == null) return;
        String sheetName = sheets.getSheetName();
        if (logger.isTraceEnabled()) {
            logger.trace("Processing new sheet: {} {}",sheetIndex,sheetName);
        }
        InputSource sheetSource = new InputSource(sheet);

        ExcelSheetDimensions currentSheetDimensions = readExcelXMLDimensionsEventHandler.getExcelSheetDimensionsMap().get(sheetName);
        logger.debug("Processing Sheet {} {}. Sheet dimensions: ",sheetIndex,sheetName,currentSheetDimensions);
        readExcelXMLSheetHandler.setCurrentSheetDetails(sheetIndex,sheetName,currentSheetDimensions);

        int startNumberOfCells = readExcelXMLSheetHandler.getNumberOfCells();
        parser.parse(sheetSource);
        int cellsInSheet = readExcelXMLSheetHandler.getNumberOfCells() - startNumberOfCells;
        if (cellsInSheet > 0) {
            sheetFullNames.put(sheetName, String.valueOf(sheetIndex));
        }
        else {
            logger.debug("Empty sheet found {}",sheetName);
        }
        sheet.close();
    }

    private ReadExcelXMLDimensionsEventHandler readSheetsDimensions(XSSFReader xssfReader) throws SAXException, IOException, InvalidFormatException {
        //Read dimensions
        ReadExcelXMLDimensionsEventHandler readExcelXMLDimensionsEventHandler = new ReadExcelXMLDimensionsEventHandler();
        XMLReader dimensionsParser = fetchSheetParser(readExcelXMLDimensionsEventHandler);
        ClaXSSFReader.SheetIterator sheets = ((ClaXSSFReader)xssfReader).getSheetsData();
        while(sheets.hasNext()) {
            try (InputStream sheet = sheets.next()){
                if (sheet == null) continue;
                String sheetName = sheets.getSheetName();
                logger.trace("Processing sheet : {} dimensions", sheetName);
                InputSource sheetSource = new InputSource(sheet);
                readExcelXMLDimensionsEventHandler.initSheetDimensions(sheetName);
                dimensionsParser.parse(sheetSource);
            }
        }
        return readExcelXMLDimensionsEventHandler;
    }

    public XMLReader fetchSheetParser(ContentHandler handler) throws SAXException {
        XMLReader parser =
                XMLReaderFactory.createXMLReader(
//                        "org.apache.xerces.parsers.SAXParser"
                );
        parser.setContentHandler(handler);
        return parser;
    }
    private POIFSFileSystem getPoifsFileSystem(InputStream fin) throws IOException {
        POIFSFileSystem poifs;// create a new org.apache.poi.poifs.filesystem.Filesystem
        poifs = new POIFSFileSystem(fin);
        // once all the events are processed close our file input stream
        fin.close();
        return poifs;
    }

    private void handleIssuesEncountered(String path, ExcelFileUtils.ExcelIssuesCounters excelIssuesCounters) {
        if (excelIssuesCounters.getIngestionErrors() > 0) {
            logger.debug("Ingestion errors ({}) for {}", excelIssuesCounters.getIngestionErrors(), path);
        }
        if (excelIssuesCounters.getExtraSkippedCells() > 0) {
            logger.debug("Mid-range extra skipped cells {} for {}",excelIssuesCounters.getExtraSkippedCells(), path);
            logger.trace("Number of blank cells: {}. Number of error cells: {}",excelIssuesCounters.getBlankCells(),excelIssuesCounters.getErrorCells());
//            excelIssuesCounters.printAdditionalInformation();
        }
        logger.trace("Finished ingesting {}", path);
    }

    private InternalWorkbook createInternalWorkbook(String path, List<Record> recordList) {
        if (recordList.size() > excelMaxRecordsCount) {
            logger.warn("We got more then {} records ({}) on {}", excelMaxRecordsCount, recordList.size(),path);
            // Create a filtered list that may be usable as a internal workbook
            if (filterRecordsFromWorkBookBeforeCreation) {
                try {
                    List<Record> filteredList = filterRecordListFromCommonItems(recordList);
                    return InternalWorkbook.createWorkbook(filteredList);
                }
                catch (RuntimeException e) {
                    logger.warn("Failed to filter common SID's",e);
                }
            }
        }
        return InternalWorkbook.createWorkbook(recordList);
    }

    private List<Record> filterRecordListFromCommonItems(List<Record> recordList) {
        Set<Short> filteredSids = new HashSet<>();
        //Let's break them down by type and find out the worst ones.
        Map<Short, Long> collect = recordList.stream().collect(Collectors.groupingBy(f -> f.getSid(), Collectors.counting()));
        for (Map.Entry<Short, Long> entry : collect.entrySet()) {
            if (entry.getValue() > excelRecordFilteringThreshold) {
                Class rc = RecordFactory.getRecordClass(entry.getKey());
                logger.warn("We got {} records of type {} {}. Filtering out this type",
                        entry.getValue(), entry.getKey(), (rc!=null)?rc.getCanonicalName():"<null>");
                filteredSids.add(entry.getKey());
            }
        }
        List<Record> filteredList = recordList.stream().filter(f -> !filteredSids.contains(f.getSid())).collect(Collectors.toList());
        logger.warn("Parsing a filtered list of {} records for internal workbook",filteredList.size());
        return filteredList;
    }

    private ReadExcelDimensionsEventListener readExcelDimensions(POIFSFileSystem poifs) throws IOException {
        ReadExcelDimensionsEventListener readExcelDimensionsEventListener;// get the Workbook (excel part) stream in a InputStream

        try(InputStream din = poifs.createDocumentInputStream("Workbook")) {
            // construct out HSSFRequest object
            HSSFRequest req = new HSSFRequest();
            // lazy listen for ALL records with the listener shown above
            readExcelDimensionsEventListener = new ReadExcelDimensionsEventListener();
            req.addListenerForAllRecords(readExcelDimensionsEventListener);
            // create our event factory
            HSSFEventFactory factory = new HSSFEventFactory();
            // process our events based on the document input stream
            factory.processEvents(req, din);
            // and our document input stream (don't want to leak these!)
        }
        logger.trace("Finished Reading excel file dimensions");
        return readExcelDimensionsEventListener;
    }

    private ReadExcelContentEventListener readExcelContents(ExcelWorkbook excelWorkbook, Map<Integer, ExcelSheetDimensions> excelSheetDimensionsMap,
                                                            InternalWorkbook internalWorkbook, HSSFWorkbook workbook, POIFSFileSystem poifs ) throws IOException {
        InputStream din;
        ReadExcelContentEventListener readExcelContentEventListener;
        logger.trace("Read excel file content");
        din = poifs.createDocumentInputStream("Workbook");
        try {
            // construct out HSSFRequest object
            HSSFRequest req = new HSSFRequest();
            // lazy listen for ALL records with the listener shown above
            readExcelContentEventListener = new ReadExcelContentEventListener(excelWorkbook,excelSheetDimensionsMap, internalWorkbook, workbook , isTokenMatcherActive);
            req.addListenerForAllRecords(readExcelContentEventListener);
            // create our event factory
            HSSFEventFactory factory = new HSSFEventFactory();
            // process our events based on the document input stream
            factory.processEvents(req, din);
            // and our document input stream (don't want to leak these!)
        } finally {
            din.close();
        }
        logger.trace("Finished Reading excel file content");
        return readExcelContentEventListener;
    }

    private void updateMissingMetadata(ExcelWorkbook excelWorkbook, Map<String, String> sheetFullNames, int numberOfCells) {
//        logger.debug("Update missing metadata");
//        ExcelWorkbookMetaData metaData = excelWorkbook.getMetadata();
//        metaData.setNumOfSheets(sheetFullNames.keySet().size());
//        Map<String, String> sheetFullNames = readExcelContentEventListener.getSheetFullNames();
//        metaData.setSheetNames(sheetFullNames.keySet().stream().sorted().map(k -> sheetFullNames.get(k)).collect(toList()));
//        metaData.setNumOfCells(numberOfCells);
    }

    private void parseExcelMetaData(Workbook wb, POIFSFileSystem poifs, String path, ExcelWorkbook excelWorkbook, ClaFilePropertiesDto claFileProperties) throws IOException {
        final ClaDocProperties claDocProperties = ExcelMetadataUtils.extractProperties(wb,poifs,path);
//        ExcelMetadataUtils.extractMetadataFromPoiFS(poifs, claDocProperties);

        final ExcelWorkbookMetaData claMetadata = FileMetadataUtils.extractMetadata(ExcelWorkbookMetaData.class,
                claDocProperties, claFileProperties);
        excelWorkbook.setMetadata(claMetadata);

        if (logger.isTraceEnabled()) {
            logger.trace("MetaData = {}", claMetadata.toCsvString());
        }
    }

    public static OPCPackage open(String path, InputStream is) throws IOException {
        try {
            is.mark(10000000);
            byte[] baseBuf = new byte[2];
            int read = is.read(baseBuf);
            is.reset();
            if (read ==2 && baseBuf[0] == 'P' && baseBuf[1] == 'K') {
                //It's a zip file. A valid XLSX file
                return OPCPackage.open(is);
            }
            else {
                logger.warn("Invalid header on XLSX file found {}. Try opening as a protected document",path);
                try {
                    POIFSFileSystem fs = new POIFSFileSystem(is);
                    EncryptionInfo info = new EncryptionInfo(fs);
                    Decryptor d = Decryptor.getInstance(info);
                    d.verifyPassword(Decryptor.DEFAULT_PASSWORD);
                    InputStream dataStream = d.getDataStream(fs);
                    return OPCPackage.open(dataStream);
                } catch (GeneralSecurityException e) {
                    logger.error("Got General Security exception trying to open XML as a protected file",e);
                    logger.error("Invalid format Excel XML",e);
                    throw new POIXMLException(e);
                }
            }
        } catch (InvalidFormatException e){
//            if (e.getMessage() != null && e.getMessage().contains("Package should contain a filecontent type part")) {
//                //Maybe it's password protected
//                try {
//                    logger.info("Try opening file as a protected document");
////                    is.reset();
//                    is = new FileInputStream(path); // the stream was closed :-(
//                    POIFSFileSystem fs = new POIFSFileSystem(is);
//                    EncryptionInfo info = new EncryptionInfo(fs);
//                    Decryptor d = Decryptor.getInstance(info);
//                    d.verifyPassword(Decryptor.DEFAULT_PASSWORD);
//                    InputStream dataStream = d.getDataStream(fs);
//                    return OPCPackage.open(dataStream);
//                } catch (GeneralSecurityException e1) {
//                    logger.error("Got General Security exception trying to open XML as a protected file",e1);
//                    logger.error("Invalid format Excel XML",e);
//                } catch (InvalidFormatException e1) {
//                    logger.error("InvalidFormatException trying to open XML as a protected file",e1);
//                    logger.error("Invalid format Protected Excel XML: "+e.getMessage(),e);
//                    throw new POIXMLException(e);
//                }
//            }
            logger.error("Invalid format Excel XML: "+e.getMessage(),e);
            throw new POIXMLException(e);
        }
    }

}