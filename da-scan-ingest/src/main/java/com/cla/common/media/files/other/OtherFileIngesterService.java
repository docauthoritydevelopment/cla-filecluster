package com.cla.common.media.files.other;

import com.cla.common.domain.dto.mediaproc.OtherFile;
import com.cla.common.domain.dto.messages.IngestTaskParameters;
import com.cla.common.media.ingest.FileIngestService;
import com.cla.common.utils.tokens.DictionaryHashedTokensConsumerImpl;
import com.cla.common.utils.tokens.NGramTokenizerHashed;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.file.FileType;
import com.cla.connector.domain.dto.error.ProcessingErrorType;
import com.cla.connector.utils.FileNamingUtils;
import org.apache.tika.io.TikaInputStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.MimeType;

import javax.annotation.PostConstruct;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Set;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toSet;

@Component
public class OtherFileIngesterService extends FileIngestService<OtherFile> {

    @Value("${ingester.other.noExtractionExtensions:}")
    private String[] noExtractionExtensionsString;

    private Set<String> noExtractionExtensionsSet = null;

    @Value("${tokenMatcherActive}")
    private boolean isTokenMatcherActive;

    @Value("${ingester.patternSearchActive}")
    private boolean patternSearchActive;

    @Value("${ingester.other.maxFileSizeForParse:30000000}")    // 30M
    private int maxFileSizeForTikaParse;

    @Value("${scanner.tike.pdfboxPushBack.propertyName:org.apache.pdfbox.baseParser.pushBackSize}")
    private String pdfboxPushBackSizePropertyName;

    @Value("${scanner.tike.pdfboxPushBack.size:1048576}")        // 1M	override BaseParser default=64K
    private String pdfboxPushBackSizePropertyValue;

    @Autowired
    private NGramTokenizerHashed nGramTokenizerHashed;

    @Autowired
    private ContentExtractorService contentExtractorService;

    @PostConstruct
    public void init() {
        super.init();
        if (noExtractionExtensionsString != null && noExtractionExtensionsString.length > 0) {
            noExtractionExtensionsSet = Stream.of(noExtractionExtensionsString).map(String::toLowerCase).collect(toSet());
            logger.info("Skipping text extraction for file extensions: {}", noExtractionExtensionsSet.toString());
        }
    }


    @Override
    public OtherFile ingestFile(IngestTaskParameters ingestTaskParameters, final byte[] content, final boolean extractMetadata,
                                final ClaFilePropertiesDto claFilePropertiesDto) {
        try (ByteArrayInputStream inputStream = new ByteArrayInputStream(content)) {
            if (claFilePropertiesDto.getContentSignature() == null) {
                claFilePropertiesDto.setContentSignature(signHelper.sign(content));
            }
            OtherFile otherFile = extractViaRequestMessage(
                    ingestTaskParameters, inputStream, claFilePropertiesDto.getFileName(), extractMetadata, claFilePropertiesDto);
            updateClaFileProperties(otherFile, claFilePropertiesDto);
            return otherFile;
        } catch (IOException e) {
            logger.warn("Failed to close input stream of file {}. Error is {}.", claFilePropertiesDto.getFileName(), e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public OtherFile extractViaRequestMessage(
            IngestTaskParameters ingestTaskParameters, InputStream is, String path, boolean extractMetadata,
            ClaFilePropertiesDto claFileProperties) {

        if (claFileProperties.getType() == null) {
            claFileProperties.setType(ingestTaskParameters.getFileType());
        }
        return extract(is, path, extractMetadata, claFileProperties);
    }

    @Override
    protected OtherFile createIngestPayload(ProcessingErrorType processingErrorType,
                                            ClaFilePropertiesDto claFilePropertiesDto,
                                            final String text, final Exception e) {
        OtherFile result = new OtherFile();
        result.setErrorType(processingErrorType);
        result.setClaFileProperties(claFilePropertiesDto);
        result.setExceptionText(e == null ? text : e.getMessage());
        return result;
    }

    //TODO: move to handler.
    private void updateClaFileProperties(OtherFile otherFile, ClaFilePropertiesDto claFileProperties) {
        if (otherFile != null) {
            claFileProperties.setMime(otherFile.getMime());
            if (otherFile.getType() != null) {
                claFileProperties.setType(otherFile.getType());
            }
        }
    }

    @Override
    protected FileType getBaseFileType() {
        return null;
    }

    @Override
    public OtherFile extract(InputStream is, String path, boolean extractMetadata, ClaFilePropertiesDto claFileProperties) {
        return extract(is, path, null, claFileProperties);
    }

    public OtherFile extract(final InputStream is, final String path, final DictionaryHashedTokensConsumerImpl hashedTokensConsumer, final ClaFilePropertiesDto cfp) {
        if (System.getProperty(pdfboxPushBackSizePropertyName) == null && pdfboxPushBackSizePropertyValue != null) {
            // Override pdfBox BaseParser default
            System.setProperty(pdfboxPushBackSizePropertyName, pdfboxPushBackSizePropertyValue);
        }
        try (InputStream tis = TikaInputStream.get(is)) {
            String text = null;
            OtherFile result = new OtherFile();
            result.setMime(contentExtractorService.detectMime(tis));
            if (cfp != null) {
                result.setType(cfp.getType());
            }
            if ((result.getType() == null || result.getType().equals(FileType.OTHER)) &&
                MimeType.valueOf("text/*").includes(MimeType.valueOf(result.getMime()))) {
                result.setType(FileType.TEXT);
            }
            // Limit parsed file size to overcome Tika heap overflow with large or bogus files.
            // TODO: solve this with proper Tika configuration @@@
            if ((patternSearchActive || isTokenMatcherActive) && (cfp == null || cfp.getFileSize() <= maxFileSizeForTikaParse)) {
                String claFileExtension = FileNamingUtils.getFilenameExtension(path);
                if (noExtractionExtensionsSet == null || !noExtractionExtensionsSet.contains(claFileExtension.toLowerCase())) {
                    text = contentExtractorService.getText(tis, result.getMime(), claFileExtension, path);
                } else {
                    text = "";
                    logger.debug("Skipping text extraction for {}", path);
                }
            }
            if (text != null) {
                String hash = nGramTokenizerHashed.getHash(text, hashedTokensConsumer);
                result.setHashedContent(hash);
                result.setExtractedContent(text);
                result.setTextLength(text.length());
            }
            result.setClaFileProperties(cfp);
            return result;
        } catch (final NoClassDefFoundError e) {
            logger.warn("Ingestion failure for file {}. NoClassDefFound : {}", path, e.getMessage());
            return null;
        } catch (final Exception e) {
            // TODO: Other files ingestion error should not result err state.
            logger.warn("Ingestion failure for file {}. Exception: {}", path, e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }

}
