package com.cla.common.media.files.pst.model;

import com.cla.common.domain.pst.PstPath;
import com.cla.common.media.files.pst.visitor.PstEntryVisitor;
import com.pff.PSTException;
import com.pff.PSTFolder;
import com.pff.PSTObject;

import java.io.IOException;
import java.util.Vector;

/**
 * Created By Itai Marko
 */
public class PstFolder extends PstEntryBase {


    private final PSTFolder libpstFolder;

    PstFolder(PSTFolder libpstFolder, PstFile pstFile, PstPath pstPath) {
        super(pstFile, pstPath);
        this.libpstFolder = libpstFolder;
    }

    @Override
    public long getPstEntryId() {
        return libpstFolder.getDescriptorNodeId();
    }

    @Override
    public <T> T accept(PstEntryVisitor<T> visitor) {
        return visitor.visitFolder(this);
    }

    @Override
    <T> void descend(PstEntryVisitor<T> visitor) {
        // Descend into the folder's content (emails appointments etc.)
        if (hasContent()) {
            try {
                PstEntry child = nextChild();
                while (child != null) {
                    child.acceptAndDescend(visitor);
                    child = nextChild();
                }
            } catch (PSTException | IOException e) {
                throw new RuntimeException("Failed to iterate over the children of PstFolder: " + getPstPath().toString(), e);
            }
        }
        // Descend into subfolders
        if (libpstFolder.hasSubfolders()) {
            try {
                Vector<PSTFolder> childFolders = libpstFolder.getSubFolders();
                for (PSTFolder childLibpstFolder : childFolders) {
                    PstFolder subFolder = getPstFile().createChildPstFolder(childLibpstFolder, getPstPath());
                    subFolder.acceptAndDescend(visitor);
                }
            } catch (PSTException | IOException e) {
                throw new RuntimeException("Error Occurred while fetching subfolders of " + getPstPath().toString(), e);
            }
        }
    }

    private boolean hasContent() {
        return libpstFolder.getContentCount() > 0;
    }

    private PstEntry nextChild() throws PSTException, IOException {
        PSTObject libpstChild = libpstFolder.getNextChild();
        if (libpstChild == null) {
            return null;
        }
        return getPstFile().createChildPstEntry(libpstChild, getPstPath());
    }
}
