package com.cla.common.media.files.pst.model;

import com.cla.common.domain.dto.file.PstEntryPropertiesDto;
import com.cla.common.domain.pst.PstPath;
import com.cla.common.media.files.pst.visitor.PstEntryDataExtractingVisitor;
import com.cla.common.media.files.pst.visitor.PstEntryScanningVisitor;
import com.cla.common.media.files.pst.visitor.PstEntryVisitor;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.file.FileContentDto;
import com.cla.connector.domain.dto.messages.ScanErrorDto;
import com.cla.connector.utils.MediaConnectorUtils;
import com.cla.connector.utils.Pair;
import com.google.common.cache.*;
import com.pff.PSTException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.nio.file.Path;
import java.time.Duration;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.Predicate;

/**
 * Provides the API to scan and ingest PST files.
 * <p/>
 * This is the only class that should be used to handle PST files, as it manages PstFiles caching and encapsulates the
 * inter-dependencies between the classes in the pst package. Failing to do so may result in furious customers ;)
 * <p/>
 *
 *
 * Created By Itai Marko
 */
@Service
public class PstFileFacade {

    private static final Logger logger = LoggerFactory.getLogger(PstFileFacade.class);

    // Turns off the scheduled housekeeping methods. This will be turned on on the first usage of the cache
    private boolean isCacheEnabled = false;

    @Value("${pst-cache.stale-file-releasing.time-to-stale.millis:60000}")
    private int pstFilesStaleTimeMillis;

    @Value("${pst-cache.stale-file-releasing.time-to-very-stale.millis:36000000}")
    private int pstFilesVeryStaleTimeMillis;

    // TODO Itai: [DAMAIN-3075] [DAMAIN-3702] If we ever write to PST files we'll need to change the cache impl to lock PST files writes for all threads
    private LoadingCache<Thread, PstCacheEntry> pstFileCache = CacheBuilder.newBuilder()
            .weakKeys()
            .removalListener(new PstCacheRemovalListener())
            .build(new PstCacheLoader());

    public void mapPstFile(ClaFilePropertiesDto pstFileProps,
                           Predicate<? super String> fileTypesPredicate,
                           Consumer<ClaFilePropertiesDto> pstEntryPropsConsumer,
                           Long runId) {

        PstEntryScanningVisitor visitor = new PstEntryScanningVisitor(pstEntryPropsConsumer, fileTypesPredicate);
        // Get the pst file and traverse it with a scanning visitor:
        // Note: There's no fork-join and no multi-task in scanning of PST files, so there's no need for caching here
        try (PstFile pstFile = getPstFile(pstFileProps, false)) {
            logger.info("Traversing PST file: {}", pstFile.getName());
            PstFolder fileRootFolder = pstFile.getPstRootFolder();
            fileRootFolder.acceptAndDescend(visitor);
        } catch (PSTException | IOException e) {
            logger.warn("Failed to traverse PST file {}. Run: #{}", pstFileProps.getFilePath(), runId, e);
            PstErroneousRootFolder errPstRootFolder = PstErroneousRootFolder.create(pstFileProps, runId, e);
            errPstRootFolder.accept(visitor);
        }
    }

    public Pair<FileContentDto, ? extends ClaFilePropertiesDto> getPstEntryData(
            ClaFilePropertiesDto pstFileProps, PstPath pstEntryPath, boolean mimeMode, boolean useCache) throws IOException {

        // TODO Itai: [DAMAIN-3075] mimeMode should be removed from PstFile api and just used to create a different visitor
        PstEntryVisitor<Pair<FileContentDto, PstEntryPropertiesDto>> visitor = new PstEntryDataExtractingVisitor();
        // Get the pst file, retrieve the specified entry and visit with a data extracting visitor:
        try (PstFile pstFile = getPstFile(pstFileProps, useCache)) {
            PstEntry pstEntry = pstFile.retrieveEntry(pstEntryPath, mimeMode);
            return pstEntry.accept(visitor);
        } catch (PSTException e) {
            throw new RuntimeException("Failed to visit PST entry " + pstEntryPath + " from " + pstFileProps.getFileName(), e);
        }
    }

    private PstFile getPstFile(ClaFilePropertiesDto pstFileProps, boolean useCache) throws IOException, PSTException {
        if (useCache) {
            isCacheEnabled = true; // turn on the scheduled housekeeping methods on the first usage of the cache
            return getCachedPstFile(pstFileProps);
        } else {
            return new PstFile(pstFileProps);
        }
    }

    private PstFile getCachedPstFile(ClaFilePropertiesDto pstFileProps) throws PSTException, IOException {
        Objects.requireNonNull(pstFileProps);
        String pstFileName = Objects.requireNonNull(pstFileProps.getFileName());

        Thread curThread = Thread.currentThread();
        traceThread("Fetching CachedPstFile, cached for thread {}", curThread);

        PstCacheEntry threadPstFilesMap = pstFileCache.getUnchecked(curThread);
        //noinspection SynchronizationOnLocalVariableOrMethodParameter
        synchronized (threadPstFilesMap) {
            CachedPstFile cachedPstFile = threadPstFilesMap.get(pstFileName);
            if (cachedPstFile == null) {
                String curThreadName = curThread.getName();
                logger.info("Caching new PstFile         for thread {} file: {} ", curThreadName, pstFileName); // Space padding is intentional to look nice in the log file
                cachedPstFile = new CachedPstFile(pstFileProps, curThreadName);
                threadPstFilesMap.put(pstFileName, cachedPstFile);
            }
            cachedPstFile.open();
            return cachedPstFile;
        }
    }

    @Scheduled(initialDelayString = "${pst-cache.stale-file-releasing.initial-delay.millis:240000}",
            fixedRateString = "${pst-cache.stale-file-releasing.time-to-stale.millis:60000}")
    private void releaseStalePstFiles() {
        if (isCacheEnabled) {
            List<CachedPstFile> pstFilesToRelease = new ArrayList<>();

            pstFileCache.asMap().forEach((thread, threadPstFilesMap) -> {
                traceThread("Looking for stale PST file to be released from thread {}", thread);
                //noinspection SynchronizationOnLocalVariableOrMethodParameter
                synchronized (threadPstFilesMap) {
                    Iterator<CachedPstFile> iterator = threadPstFilesMap.values().iterator();
                    while (iterator.hasNext()) {
                        CachedPstFile cachedPstFile = iterator.next();
                        if (isStalePstFile(cachedPstFile)) {
                            logger.info("Removing stale PstFile from cache. Cached for thread: {}. Filename: {}",
                                    cachedPstFile.getCacheKeyName(), cachedPstFile.getFileName());
                            iterator.remove();
                            pstFilesToRelease.add(cachedPstFile);
                        }
                    }
                }
            });

            pstFilesToRelease.forEach(cachedPstFile -> {
                logger.info("Releasing stale cached PstFile.    Cached for thread: {}. Filename: {}",
                        cachedPstFile.getCacheKeyName(), cachedPstFile.getFileName());
                cachedPstFile.release();
            });
        }
    }

    @Scheduled(initialDelayString = "${pst-cache.cleanup.initial-delay.millis:300000}",
            fixedRateString = "${pst-cache.cleanup.interval.millis:180000}")
    private void cleanUpCache() {
        if (isCacheEnabled) {
            // Make sure to clean up after garbage-collected Threads even when there are no cache reads and writes
            pstFileCache.cleanUp();
        }
    }

    private boolean isStalePstFile(CachedPstFile cachedPstFile) {
        long millisSinceLastHandlesUpdate = System.currentTimeMillis() - cachedPstFile.getLastHandlesUpdateTime();
        if (millisSinceLastHandlesUpdate >= pstFilesStaleTimeMillis) {
            int handleCount = cachedPstFile.getHandleCount();
            if (handleCount == 0) {
                return true;
            } else {
                Duration noHandleChangeDuration = Duration.ofMillis(millisSinceLastHandlesUpdate);
                String threadName = cachedPstFile.getCacheKeyName();
                logger.warn("PstFile handleCount remained {} for {}. Cached for thread: {}. Filename: {}. {}",
                        handleCount, noHandleChangeDuration, threadName, cachedPstFile.getFileName(), cachedPstFile);
                if (millisSinceLastHandlesUpdate >= pstFilesVeryStaleTimeMillis) {
                    logger.warn("PstFile deemed very stale. Releasing anyway. File: {}", cachedPstFile);
                    return true;
                }
            }
        }
        return false;
    }

    private void traceThread(String format, Thread thread) {
        if (logger.isTraceEnabled()) {
            logger.trace(format, thread.getName());
        }
    }

    /**
     * Used to hold the cached value for each thread.
     */
    private static class PstCacheEntry extends HashMap<String, CachedPstFile> { // extending just to avoid delegation
        private final String keyName; // holds the name of the cache key that may not be available

        private PstCacheEntry(String keyName) {
            this.keyName = keyName;
        }
    }

    /**
     * Used to release cached PstFiles when their thread is garbage-collected (which invalidates the their cache entry).
     */
    private static class PstCacheRemovalListener implements RemovalListener<Thread, PstCacheEntry> {
        @Override
        public void onRemoval(@Nonnull RemovalNotification<Thread, PstCacheEntry> notification) {
            PstCacheEntry threadPstFilesMap = notification.getValue();
            logger.info("Thread {} was removed from the PST file cache with cause: {}.",
                    threadPstFilesMap.keyName, notification.getCause());
            int numOfCachedPstFileForThread = threadPstFilesMap.size();
            if (numOfCachedPstFileForThread > 0) {
                // no need to sync on threadPstFilesMap since its thread was garbage-collected
                threadPstFilesMap.forEach((cachedPstFilePath, cachedPstFile) -> {
                    logger.info("Releasing PstFile cached for thread {}. file path: {}",
                            cachedPstFile.getCacheKeyName(), cachedPstFilePath);
                    int handleCount = cachedPstFile.getHandleCount();
                    if (handleCount > 0) {
                        logger.warn("Releasing PstFile with {} open handles, cached for a thread which was removed. ", handleCount);
                    }
                    cachedPstFile.release();
                });
            } else {
                logger.debug("Thread {} had no remaining cached PstFiles");
            }
        }
    }


    private static class PstCacheLoader extends CacheLoader<Thread, PstCacheEntry> {
        @Override
        public PstCacheEntry load(@Nonnull Thread key) {
            String threadName = key.getName();
            logger.info("Loading new PST cache entry for thread {}", threadName);
            return new PstCacheEntry(threadName);
        }
    }

    /**
     * Used to let the visitor send a message when a exception prevents creating a real root PstFolder.
     */
    private static class PstErroneousRootFolder extends PstFolder {

        private final ClaFilePropertiesDto pstFileProps;
        private final ScanErrorDto scanError;

        private static PstErroneousRootFolder create(ClaFilePropertiesDto pstFileProps, Long runId, Exception e) {
            Path pstFilePath = pstFileProps.getFilePath();
            PstPath pstRootPath = new PstPath(pstFilePath);
            ScanErrorDto scanError =
                    MediaConnectorUtils.createScanError("Failed traverse PST file.", e, pstRootPath.toString(), runId);
            return new PstErroneousRootFolder(pstRootPath, pstFileProps, scanError);
        }

        private PstErroneousRootFolder(PstPath pstRootPath, ClaFilePropertiesDto pstFileProps, ScanErrorDto scanError) {
            super(null, null, pstRootPath);
            this.pstFileProps = pstFileProps;
            this.scanError = scanError;
        }

        @Override
        public long getPstEntryId() {
            return 0;
        }

        @Override
        public ClaFilePropertiesDto getPstFileProps() {
            return pstFileProps;
        }

        @Override
        public <T> T accept(PstEntryVisitor<T> visitor) {
            return visitor.visitFolderFailed(this, scanError);
        }

        @Override
        void descend(PstEntryVisitor visitor) {
            // do nothing
        }
    }
}
