package com.cla.common.media.files.ppt;

import com.cla.common.media.files.xml.CurtailedPackagePart;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.openxml4j.opc.PackagePart;
import org.apache.poi.xslf.usermodel.XSLFSlideShow;
import org.apache.xmlbeans.XmlException;

import java.io.IOException;

/**
 * Power Point XSLFSlideShow wrapper for purposes of limiting number of XML objects in file
 * Created by vladi on 4/3/2017.
 */
public class ClaXSLFSlideShow extends XSLFSlideShow {

    public ClaXSLFSlideShow(OPCPackage container) throws OpenXML4JException, IOException, XmlException {
        super(container);
    }

    @Override
    protected PackagePart getCorePart() {
        try {
            return new CurtailedPackagePart(super.getCorePart());
        }
        catch (InvalidFormatException e) {
            throw new RuntimeException("Failed to create curtailed part.", e);
        }
    }
}
