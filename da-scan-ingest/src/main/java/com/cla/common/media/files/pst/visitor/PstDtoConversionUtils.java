package com.cla.common.media.files.pst.visitor;

import com.cla.common.domain.dto.file.PstEntryPropertiesDto;
import com.cla.common.media.files.pst.model.*;
import com.cla.connector.domain.dto.file.FileType;
import com.cla.connector.domain.dto.file.ItemType;
import com.cla.connector.domain.dto.media.EmailAddress;
import com.cla.connector.mediaconnector.mail.MailRelated;
import com.cla.connector.utils.FileTypeUtils;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created By Itai Marko
 */
final class PstDtoConversionUtils {

    static PstEntryPropertiesDto dtoForPstFolder(PstFolder pstFolder) {
        PstEntryPropertiesDto payload = createPayloadFromPstEntry(pstFolder);
        payload.setFolder(true);
        payload.setFile(false);
        payload.setFileSize(0L); // note that for a regular folder the size is 4096
        return payload;
    }

    static PstEntryPropertiesDto dtoForPstEmailMessage(PstEmailMessage pstEmailMessage) {
        PstEntryPropertiesDto payload = createPayloadFromPstEntry(pstEmailMessage);
        payload.setFolder(false);
        payload.setFile(true);
        payload.setFileSize(pstEmailMessage.getContentSize());
        payload.setType(FileType.MAIL);
        populateEmailData(pstEmailMessage, payload);
        payload.setItemType(ItemType.MESSAGE);
        payload.setNumOfAttachments(pstEmailMessage.getNumOfAttachments());
        payload.setEmailSubject(MailRelated.getSubject(pstEmailMessage.getSubject()));
        return payload;
    }

    private static void populateEmailData(PstEmailMessage emailMessage, PstEntryPropertiesDto payload) {
        PstAddress pstSender = emailMessage.getSender();
        payload.setSender(new EmailAddress(pstSender.getName(), pstSender.getEmailAddress()));
        List<PstRecipient> pstRecipients = emailMessage.getRecipients();
        List<EmailAddress> recipientsDto = pstRecipients
                .stream()
                .map(pstRecipient ->
                        new EmailAddress(pstRecipient.getName(), pstRecipient.getEmailAddress()))
                .collect(Collectors.toList());
        payload.setRecipients(recipientsDto);
        payload.setSentDate(emailMessage.getSentDate());
    }

    static PstEntryPropertiesDto dtoForPstAttachment(PstAttachment pstAttachment) {
        PstEntryPropertiesDto payload = createPayloadFromPstEntry(pstAttachment);
        payload.setFolder(false);
        payload.setFile(true);
        payload.setFileSize((long) pstAttachment.getFileSize());
        String attachmentEntryPath = pstAttachment.getPstPath().getEntryPath();
        FileType attachmentFileType = FileTypeUtils.getFileType(attachmentEntryPath);
        payload.setType(attachmentFileType);
        populateEmailData(pstAttachment.getContainingEmailMsg(), payload);
        payload.setEmailPath(pstAttachment.getContainingEmailMsg().getPstPath().getNonUniquePath());
        payload.setItemType(ItemType.ATTACHMENT);
        payload.setEmailSubject(pstAttachment.getFilename());
        return payload;
    }

    private static PstEntryPropertiesDto createPayloadFromPstEntry(PstEntry pstEntry) {
        PstEntryPropertiesDto payload = new PstEntryPropertiesDto();
        payload.setPstFileProps(pstEntry.getPstFileProps());
        payload.setPstEntryId(pstEntry.getPstEntryId());
        payload.setPstPath(pstEntry.getPstPath());
        payload.setFileName(pstEntry.getPstPath().getNonUniquePath());
        payload.setMediaItemId(pstEntry.getPstPath().toString());
        return payload;
    }

    private PstDtoConversionUtils() {
        // avoid instantiation
    }
}
