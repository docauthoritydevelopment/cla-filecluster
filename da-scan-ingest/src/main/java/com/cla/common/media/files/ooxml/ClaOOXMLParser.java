package com.cla.common.media.files.ooxml;

import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.mime.MediaType;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.microsoft.ooxml.OOXMLParser;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.InputStream;
import java.util.Set;

/**
 * Custom OOXML parser, used as an entry point for parsing process customization
 * Created by vladi on 4/2/2017.
 */
public class ClaOOXMLParser extends OOXMLParser{

    private int sizeThreshold;      // file size threshold, larger files will be limited by totalObjectsLimit
    private int totalObjectsLimit;  // limit on total number of XML objects, when reached, the parsing stops

    public static Set<MediaType> getSupportedTypes(){
        return SUPPORTED_TYPES;
    }

    public static Set<MediaType> getUnSupportedTypes(){
        return UNSUPPORTED_OOXML_TYPES;
    }

    public void parse(
            InputStream stream, ContentHandler handler,
            Metadata metadata, ParseContext context)
            throws IOException, SAXException, TikaException {
        // Have the OOXML file processed
        ClaOOXMLExtractorFactory.parse(stream, handler, metadata, context, sizeThreshold, totalObjectsLimit);
    }

    public void setSizeThreshold(int sizeThreshold) {
        this.sizeThreshold = sizeThreshold;
    }

    public void setTotalObjectsLimit(int totalObjectsLimit) {
        this.totalObjectsLimit = totalObjectsLimit;
    }
}
