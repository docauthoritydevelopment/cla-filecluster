package com.cla.common.allocation;

import com.cla.common.domain.dto.FileMediaType;
import com.cla.common.domain.dto.mediaproc.excel.ExcelWorkbook;
import com.cla.common.utils.FileSizeUnits;
import com.cla.connector.domain.dto.error.ProcessingErrorType;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.openxml4j.opc.PackagePart;
import org.apache.poi.xssf.usermodel.XSSFRelation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * Strategy that helps to determine how much memory should be allocated during PDF document ingestion
 * Created by vladi on 4/20/2017.
 */
@Component
public class ExcelMemoryAllocationStrategy {

    private final static Logger logger = LoggerFactory.getLogger(ExcelMemoryAllocationStrategy.class);


    // ------------- which files are considered small, medium and large --------
    @Value("${ingester.excel.small.file.size:1000000}")
    private int smallFileSizeLimit;

    @Value("${ingester.excel.medium.file.size:10000000}")
    private int mediumFileSizeLimit;

    // ------------- old xsl: basic memory allocations per file size --------------------
    @Value("${ingester.excel.small.file.memory.allocation:50}")
    private int smallFileMemAllocationDoc;

    @Value("${ingester.excel.medium.file.memory.allocation:200}")
    private int mediumFileMemAllocationDoc;

    @Value("${ingester.excel.large.file.memory.allocation:900}")
    private int largeFileMemoryAllocationDoc;

    // ------------- XML xslx: basic memory allocations per file size --------------------
    @Value("${ingester.excel.xml.small.file.memory.allocation:200}")
    private int smallFileMemAllocationXml;

    @Value("${ingester.excel.xml.medium.file.memory.allocation:300}")
    private int mediumFileMemAllocationXml;

    @Value("${ingester.excel.xml.large.file.memory.allocation:500}")
    private int largeFileMemAllocationXml;

    // ------------- allocation factors per various document features --------------------
    @Value("${ingester.excel.xml.memoryAllocation.sharedStrings.factor:5}")
    private int sharedStringsMemoryAllocationFactor;

    @Value("${ingester.excel.xml.memoryAllocation.stylesTable.factor:5}")
    private int stylesTableMemoryAllocationFactor;

    @Value("${ingester.excel.xml.memoryAllocation.sheets.factor:5}")
    private int sheetsNumberMemoryAllocationFactor;

    // ------------- allocation settings ------------------------------------------------------
    @Value("${ingester.excel.xml.memoryAllocation.extraForSharedPools:true}")
    private boolean excelXmlAllocateMemoryForSharedPools;

    @Value("${ingester.excel.xml.memoryAllocation.minSharedPoolThreshold:500000}")
    private int minSharedPoolMemoryThreshold;

    @Value("${ingester.excel.xml.memoryAllocation.maxSharedPoolMemoryThreshold:100000000}")
    private int maxSharedPoolMemoryThreshold;

    //-----------------------------------------------------------------------------------------

    /**
     * Calculate required memory for Excel workbook processing
     * @param fileSize          file size
     * @param fileMediaType     type - old or new (XML) style excel
     * @param path              file path
     * @param opcPackage        OPC package object
     * @param excelWorkbook     workbook object
     * @return  required allocation in MB
     */
    public int calculateRequiredMemory(long fileSize, FileMediaType fileMediaType, String path,
                                       OPCPackage opcPackage, ExcelWorkbook excelWorkbook){

        int result = calculateBaseRequiredMemory(fileSize, fileMediaType);

        // additional memory allowance for sheets
        long numOfSheets = getSheetsNumber(opcPackage);
        result += (Math.min(numOfSheets, 50) * sheetsNumberMemoryAllocationFactor);

        // additional memory allowance for shared objects
        if (excelXmlAllocateMemoryForSharedPools) {
            long sharedStringsSize = getXmlSharedStringsPartSize(opcPackage);
            long stylesTableSize = getXmlStylesTablePartSize(opcPackage);

            long sharedObjectsAllocBytes = sharedStringsSize * sharedStringsMemoryAllocationFactor +
                    stylesTableSize * stylesTableMemoryAllocationFactor;

            if (sharedObjectsAllocBytes > minSharedPoolMemoryThreshold) {
                if (sharedObjectsAllocBytes > maxSharedPoolMemoryThreshold) {
                    logger.warn("Shared memory allocation {} exceeds limit for file {}. Strings size: {}, styles size: {}",
                            sharedObjectsAllocBytes, path, sharedStringsSize, stylesTableSize);
                    excelWorkbook.setErrorType(ProcessingErrorType.ERR_MEMORY_LIMIT_EXCEEDED);
                }
                result += FileSizeUnits.BYTE.toMegabytes(sharedObjectsAllocBytes);
            }
        }
        return result;
    }

    /**
     * Calculate basic memory allocation based solely on file size
     * @param fileSize          file size
     * @param fileMediaType     type - old or new (XML) style excel
     * @return  required allocation in MB
     */
    public int calculateBaseRequiredMemory(long fileSize, FileMediaType fileMediaType) {
        int result;
        switch (fileMediaType){
            case EXCEL:
                if (fileSize < smallFileSizeLimit) {
                    result = smallFileMemAllocationDoc;
                }
                else
                if (fileSize < mediumFileSizeLimit) {
                    result = mediumFileMemAllocationDoc;
                }
                else {
                    result = largeFileMemoryAllocationDoc;
                }
                break;
            case EXCELXML:
                if (fileSize < smallFileSizeLimit) {
                    result = smallFileMemAllocationXml;
                }
                else
                if (fileSize < mediumFileSizeLimit) {
                    result = mediumFileMemAllocationXml;
                }
                else {
                    result = largeFileMemAllocationXml;
                }
                break;
            default:
                throw new RuntimeException("Unsupported file type " + fileMediaType);

        }
        return result;
    }

    private long getSheetsNumber(OPCPackage pkg) {
        return pkg.getPartsByContentType(XSSFRelation.WORKSHEET.getContentType()).size();
    }

    private long getXmlSharedStringsPartSize(OPCPackage pkg) {
        return getFirstPartSize(pkg.getPartsByContentType(XSSFRelation.SHARED_STRINGS.getContentType()));
    }

    private long getXmlStylesTablePartSize(OPCPackage pkg) {
        return getFirstPartSize(pkg.getPartsByContentType(XSSFRelation.STYLES.getContentType()));
    }

    private long getFirstPartSize(ArrayList<PackagePart> parts) {
        if (parts.size() == 0) {
            return 0;
        }
        try {
            InputStream is = parts.get(0).getInputStream();
            if (is instanceof ByteArrayInputStream) {
                ByteArrayInputStream bais = (ByteArrayInputStream)is;
                return bais.available();
            }
        } catch (IOException e) {
            // do nothing
        }
        return parts.get(0).getSize();
    }


}
