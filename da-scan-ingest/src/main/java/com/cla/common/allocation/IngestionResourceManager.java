package com.cla.common.allocation;

import java.util.concurrent.Semaphore;

import javax.annotation.PostConstruct;

import com.cla.common.utils.FileSizeUnits;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class IngestionResourceManager {
	private static final Logger logger = LoggerFactory.getLogger(IngestionResourceManager.class);

	@Value("${ingester.fakeMinResourcesAvailableMB:0}")
	private int fakeMinResourcesAvailableMB;	// mainly for debug

	@Value("${ingester.spareResourcesMB:400}")
	private int spareResourcesMB;

	@Value("${useThreadPoolExecutors:true}")
	private boolean useThreadPoolExecutors;
	
	private Semaphore resourceSemaphore = null;
	private int maxAvailable = 0;

	@PostConstruct
	private void init() {
		if (!useThreadPoolExecutors) {
			logger.info("ThreadPool is off. ignore resource management.");
			return;
		}

		if (resourceSemaphore == null) {
			int maxMemMB = (int) FileSizeUnits.BYTE.toMegabytes(Runtime.getRuntime().maxMemory());
			if (fakeMinResourcesAvailableMB > maxMemMB) {
				logger.info("Force resource pool to {} instead of {}", fakeMinResourcesAvailableMB, maxMemMB);
				maxMemMB = fakeMinResourcesAvailableMB;
			}
			setResourceLimit(maxMemMB - spareResourcesMB);
		}
	}
	
	private void setResourceLimit(final int limit) {
		if (limit <= 0) {
			resourceSemaphore = null;
		}
		else {
			maxAvailable = limit;
			resourceSemaphore = new Semaphore(limit);
			logger.info("resourceSemaphore initialized to {}. {} available", limit, resourceSemaphore.availablePermits());
		}
	}
	
	public int getAvailable() {
		return (resourceSemaphore == null) ?
				-1 : resourceSemaphore.availablePermits();
	}
	
	public boolean acquire(final int permits) {
		if (resourceSemaphore != null) {
			int finalPermits = permits;
			if (permits > maxAvailable) {
				logger.warn("Asking more resources than totally available ({}>{}). " +
						"Allocating 90% of maximum available resources.", permits, maxAvailable);
				finalPermits = (maxAvailable / 100 * 90);
			}
			try {
				resourceSemaphore.acquire(finalPermits);
				if (logger.isTraceEnabled()) {
					logger.trace("resourceSemaphore acquired {}. {} available", permits, resourceSemaphore.availablePermits());
				}
				return true;
			} catch (final Exception e) {
				logger.warn("Exception during resources acquisition.", e);
			}
		}
		return false;
	}

	public void release(final int permits) {
		if (resourceSemaphore != null) {
			resourceSemaphore.release(permits);
			if (logger.isTraceEnabled()) {
				logger.trace("resourceSemaphore released {}. {} available", permits, resourceSemaphore.availablePermits());
			}
		}
	}

}
