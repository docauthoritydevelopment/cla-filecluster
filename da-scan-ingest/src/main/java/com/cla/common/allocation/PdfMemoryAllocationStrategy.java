package com.cla.common.allocation;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Strategy that helps to determine how much memory should be allocated during PDF document ingestion
 * Created by vladi on 4/20/2017.
 */
@Component
public class PdfMemoryAllocationStrategy {

    @Value("${ingester.pdfDocOverheadMemoryAllocationMB:300}")
    private int overheadMemoryAllocationMB;

    @Value("${ingester.pdfDocMaxMemoryAllocationMB:600}")
    private int maxMemoryAllocationMB;

    @Value("${ingester.pdfPageMemoryAllocationMB:1}")
    private int pageMemoryAllocationMB;

    public int getOverheadMemoryAllocationMB() {
        return overheadMemoryAllocationMB;
    }

    public boolean pageMemAllocationActive(){
        return (maxMemoryAllocationMB > 0 && pageMemoryAllocationMB > 0);
    }

    public int calculateRequiredMemory(int pages){
        return Math.min(maxMemoryAllocationMB, pages * pageMemoryAllocationMB);
    }
}
