package com.cla.common.scan;

import com.cla.common.domain.dto.crawler.FileEntity;
import com.cla.common.exceptions.BreakStreamException;
import com.cla.connector.progress.ProgressTracker;
import com.cla.connector.utils.FileNamingUtils;
import com.cla.kvdb.KeyValueDatabaseRepository;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

/**
 * Created by Itai Marko.
 */
public class DeletedItemsProcessor {

    private static final Logger logger = LoggerFactory.getLogger(DeletedItemsProcessor.class);

    private final KeyValueDatabaseRepository kvdbRepo;

    public DeletedItemsProcessor(KeyValueDatabaseRepository kvdbRepo) {
        Objects.requireNonNull(kvdbRepo);
        this.kvdbRepo = kvdbRepo;
    }

    public int processDeletedItems(long runId, long startTime, String storeName, ProgressTracker progressTracker,
                                   Consumer<List<Long>> deletedFilesHandler,
                                   Consumer<List<Long>> deletedFoldersHandler,
                                   int deletedItemsBatchSize,
                                   Map<String, Boolean> paths,
                                   BiConsumer<String, FileEntity> deletedEntityHandler) {

        if (deletedItemsBatchSize <= 0) {
            throw new IllegalArgumentException("deletedItemsBatchSize must be positive. Got: " + deletedItemsBatchSize);
        }
        final int[] deletedItemsCounter = {0};
        final int[] deletedItemsBatchCounter = {0};
        final List<Long> deletedFileIds = Lists.newArrayList();
        final List<Long> deletedFolderIds = Lists.newArrayList();

        streamDeletedItems(runId, startTime, storeName, progressTracker, paths, (key, fileEntity) -> {
            deletedItemsCounter[0]++;
            deletedItemsBatchCounter[0]++;

            logger.trace("Detected deletion of {} [{}]", fileEntity.isFolder() ? "folder" : "file", fileEntity);

            if (fileEntity.isFolder()) {
                if (fileEntity.getFolderId() != null) {
                    deletedFolderIds.add(fileEntity.getFolderId());
                } else {
                    logger.warn("folder kvdb entity without folder id {}", fileEntity);
                }
            }
            if (fileEntity.isFile()) {
                deletedFileIds.add(fileEntity.getFileId());
                deletedEntityHandler.accept(key,fileEntity);
            }

            if (deletedItemsBatchCounter[0] >= deletedItemsBatchSize) {
                deletedFilesHandler.accept(deletedFileIds);
                deletedFoldersHandler.accept(deletedFolderIds);
                deletedItemsBatchCounter[0] = 0;
                deletedFileIds.clear();
                deletedFolderIds.clear();
            }

        });

        // handle last batch
        deletedFilesHandler.accept(deletedFileIds);
        deletedFoldersHandler.accept(deletedFolderIds);

        return deletedItemsCounter[0];
    }

    public static boolean isValidFileEntity(FileEntity fileEntity) {
        if (fileEntity.isFolder() && fileEntity.getFolderId() == null) {
            return false;
        }
        return (!fileEntity.isFile() || fileEntity.getFileId() != null);
    }

    public void streamDeletedItems(long runId, long startTime, String storeName, ProgressTracker progressTracker,
                                   Map<String, Boolean> paths,
                                   BiConsumer<String, FileEntity> deletedItemHandler) {


        Long totalItemsBeforeDeletion = kvdbRepo.getFileEntityDao().executeInTransaction(runId, storeName, store -> {
            long itemsNum = store.count();
            if (itemsNum == 0L) {
                if (progressTracker != null) progressTracker.endCurStage();
                return itemsNum;
            }
            if (progressTracker != null) progressTracker.setCurStageEstimatedTotal(itemsNum);
            try {
                final String[] curInaccessibleFolder = {null};

                // cng paths to convenient form for compare
                Map<String, Boolean> pathsToCompare = new HashMap<>();
                if (paths != null && paths.size() > 0) {
                    paths.forEach((k,v) -> {
                        String pathToCompare = k.toLowerCase().startsWith("http") ? FileNamingUtils.convertPathToUniversalString(k) : FileNamingUtils.normalizeFileSharePath(k);
                        pathsToCompare.put(pathToCompare, v);
                    });
                }

                store.forEach((key, fileEntity) -> {
                    try {
                        if (fileEntity == null) {
                            logger.warn("Null KVDB entity for key: {}, Skipping entity", key);
                            store.delete(key);
                            return;
                        } else if (fileEntity.getLastScanned() == null) {
                            logger.warn("KVDB entity for key: {} without scan date, Skipping entity", key);
                            fileEntity.setLastScanned(startTime);
                            store.put(key, fileEntity);
                            return;
                        }

                        updateInaccessibleFolderRef(curInaccessibleFolder, key, fileEntity);
                        if (!fileEntity.isDeleted() && fileEntity.getLastScanned() < startTime) { // item not new

                            boolean relevant = true;

                            // for share point key is path for others its an id and we need to get the path for this
                            String pathOfKey = Strings.isNullOrEmpty(fileEntity.getPath()) ? key : fileEntity.getPath();

                            if (pathsToCompare.size() > 0) { // received specific paths to check

                                // chk if current entity is in paths given
                                String pathToCompare = null;
                                for (String p : pathsToCompare.keySet()) {
                                    if (pathOfKey.startsWith(p)) {
                                        if (pathToCompare == null || p.startsWith(pathToCompare)) {
                                            pathToCompare = p;
                                        }
                                    }
                                }

                                relevant = pathToCompare != null;

                                // if original task was for fst level only make sure
                                // path given is a direct parent
                                if (relevant && pathsToCompare.get(pathToCompare)) {
                                    relevant = FileNamingUtils.getParentDir(pathOfKey).equals(pathToCompare);
                                }
                            }

                            if (relevant) {
                                if (curInaccessibleFolder[0] == null) {
                                    // None of the ancestors of this FileEntity has become inaccessible. Consider it deleted
                                    fileEntity.setDeleted(true);
                                    deletedItemHandler.accept(key, fileEntity);
                                    store.put(key, fileEntity);
                                    if (fileEntity.isFolder() && pathsToCompare.size() > 0) {
                                        pathsToCompare.put(pathOfKey, false);
                                    }
                                } else if (key.startsWith(curInaccessibleFolder[0])) {
                                    // An ancestor of this FileEntity has become inaccessible. Don't consider it deleted
                                    fileEntity.setInaccessible(true);
                                    store.put(key, fileEntity);
                                }
                            }
                        }

                        if (progressTracker != null) progressTracker.incrementProgress(1L);
                    } catch (Exception e) {
                        logger.error("Error during deleted files streaming, while processing key: {} data: {} - Skipping", key, fileEntity, e);
                    }
                });
            } catch (BreakStreamException bse) {
                logger.info("Job pause was requested, skipping.");
            }
            return itemsNum;
        });
        logger.info("Number of files/folders before deletion: {}.", totalItemsBeforeDeletion);
    }

    /**
     * Maintains the curInaccessibleFolder so that it will refer to the topmost inaccessible folder reached so far in
     * dfs-order-traversal. curInaccessibleFolder will refer to the key of such folder as long as traversal is
     * within that inaccessible folder, otherwise it will be null.
     *
     * Note that this assumes that traversal is done in dfs order.
     *
     * @param curInaccessibleFolder the current reference holder to the topmost inaccessible folder
     * @param key                   the key of the current FileEntity
     * @param fileEntity            the current FileEntity
     */
    private void updateInaccessibleFolderRef(String[] curInaccessibleFolder, String key, FileEntity fileEntity) {

        if (curInaccessibleFolder[0] != null && !key.startsWith(curInaccessibleFolder[0])) {
            // The previously found inaccessible folder is not an ancestor of this FileEntity.
            curInaccessibleFolder[0] = null;
        }

        if (curInaccessibleFolder[0] == null && fileEntity.isInaccessible()) {
            // Found an inaccessible folder without an inaccessible parent folder
            curInaccessibleFolder[0] = key;
        }
    }

}
