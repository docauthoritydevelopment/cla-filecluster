package com.cla.common.domain.dto.messages;

import com.beust.jcommander.internal.Lists;
import com.cla.common.domain.dto.file.ExcelAnalysisMetadata;
import com.cla.common.domain.dto.file.FileCollections;
import com.cla.common.domain.dto.histogram.ClaSuperCollection;
import com.cla.common.domain.dto.mediaproc.excel.ExcelSheetMetaData;
import com.cla.common.domain.dto.mediaproc.excel.ExcelWorkbookMetaData;
import com.cla.connector.domain.dto.error.ProcessingErrorType;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.util.List;
import java.util.stream.IntStream;

import static org.junit.Assert.assertSame;

/**
 * Serialize / deserialize excel ingest result test
 * Created by vladi on 3/7/2017.
 */
public class ExcelIngestResultTest extends AbstractIngestResultTest{

    @Test
    public void serializationTest(){
        ExcelIngestResult original = new ExcelIngestResult();
        original.setMetadata(createWorkbookMetadata());
        original.setCombinedProposedTitles(createTitles());
        original.setCsvValue("1,2,3,4,5");
        original.setErrorType(ProcessingErrorType.ERR_UNKNOWN);

        List<ExcelSheetIngestResult> sheets = Lists.newLinkedList();
        IntStream.range(0, 10).forEach(i -> sheets.add(createSheet("name_" + i)));
        original.setSheets(sheets);

        FileCollections fileCollections = new FileCollections();
        ExcelAnalysisMetadata sheetsMetadata = new ExcelAnalysisMetadata();

        sheets.forEach(sheet -> {
            final String sheetFullName = sheet.getFullSheetName();
            fileCollections.addPartHistogram(sheetFullName, createLongHistograms());
            sheetsMetadata.addSheetMetadata(sheetFullName, createSheetMetaData("sheet_" + sheetFullName, random.nextInt(50)));
        });

        original.setFileCollections(fileCollections);
        original.setSheetsMetadata(sheetsMetadata);

        try {
            String valueAsString = mapper.writeValueAsString(original);
            System.out.println(valueAsString);
            ExcelIngestResult deserialized = mapper.readValue(valueAsString, ExcelIngestResult.class);
            assertEquals(original, deserialized);
        }
        catch (IOException e) {
            Assert.fail(e.toString());
        }
    }

    // ----------------------------- assert equals ---------------------------------------------------------
    private void assertEquals(ExcelIngestResult orig, ExcelIngestResult copy){
        Assert.assertEquals(orig.getCsvValue(), copy.getCsvValue());
        Assert.assertEquals(orig.getCombinedProposedTitles(), copy.getCombinedProposedTitles());

        assertEquals(orig.getMetadata(), copy.getMetadata());
        assertSame(orig.getErrorType(), copy.getErrorType());
        assertEquals(orig.getFileCollections(), copy.getFileCollections());
        assertEquals(orig.getSheetsMetadata(), copy.getSheetsMetadata());

        Assert.assertEquals(orig.getSheets().size(), copy.getSheets().size());
        IntStream.range(0, orig.getSheets().size()).forEach(index ->
                assertEquals(orig.getSheets().get(index), copy.getSheets().get(index)));
    }

    private void assertEquals(ExcelWorkbookMetaData orig, ExcelWorkbookMetaData copy){
        Assert.assertEquals(orig.getNumOfSheets(), copy.getNumOfSheets());
        Assert.assertEquals(orig.getAuthor(), copy.getAuthor());
    }

    private void assertEquals(ExcelSheetIngestResult orig, ExcelSheetIngestResult copy){
        Assert.assertEquals(orig.getFullSheetName(), copy.getFullSheetName());
        Assert.assertEquals(orig.getContentTokens(), copy.getContentTokens());
        Assert.assertEquals(orig.getOrigContentTokens(), copy.getOrigContentTokens());
    }

    private void assertEquals(FileCollections orig, FileCollections copy){
        orig.getCollections().keySet().forEach(key -> {
            ClaSuperCollection<Long> origCol = orig.getPartHistogram(key);
            ClaSuperCollection<Long> copyCol = copy.getPartHistogram(key);
            assertEquals(origCol, copyCol);
        });
    }

    private void assertEquals(ExcelAnalysisMetadata orig, ExcelAnalysisMetadata copy){
        Assert.assertEquals(orig.getSheetsFullNames(), copy.getSheetsFullNames());
        orig.getSheetsMetadata().keySet().forEach(key ->
                assertEquals(orig.getSheetMetadata(key), copy.getSheetMetadata(key)));
    }


    private void assertEquals(ExcelSheetMetaData orig, ExcelSheetMetaData copy){
        Assert.assertEquals(orig.toString(), copy.toString());
    }

    // ---------------------------- create mock data ----------------------------------------
    private ExcelSheetIngestResult createSheet(String name){
        ExcelSheetIngestResult sheet = new ExcelSheetIngestResult();
        sheet.setFullSheetName(name);
        sheet.setContentTokens("tokens_" + name);
        sheet.setOrigContentTokens("original_tokens_" + name);
        return sheet;
    }

    private ExcelSheetMetaData createSheetMetaData(String name, int cellsNum){
        ExcelSheetMetaData metadata = new ExcelSheetMetaData();
        metadata.setFullSheetName(name);
        metadata.setNumOfCells(cellsNum);
        return metadata;
    }

    private ExcelWorkbookMetaData createWorkbookMetadata(){
        ExcelWorkbookMetaData metadata = new ExcelWorkbookMetaData();
        metadata.setAuthor("Famous J. Author");
        metadata.setNumOfSheets(10);
        return metadata;
    }


}
