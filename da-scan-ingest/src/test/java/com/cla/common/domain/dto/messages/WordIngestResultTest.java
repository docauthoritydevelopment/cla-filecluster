package com.cla.common.domain.dto.messages;

import com.cla.common.domain.dto.word.WordfileMetaData;
import com.cla.connector.domain.dto.error.ProcessingErrorType;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertSame;

/**
 * Serialize / deserialize word ingest result test
 * Created by vladi on 3/8/2017.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class WordIngestResultTest extends AbstractIngestResultTest{

    private static final String LOREM_IPSUM = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. " +
            "Sed at efficitur lacus, nec cursus mauris. Sed sagittis augue sit amet vestibulum feugiat. " +
            "Vestibulum aliquam ultricies ipsum nec sagittis. Curabitur nisl mauris, tincidunt vel ipsum id, " +
            "rhoncus eleifend tortor. Maecenas mi tellus, aliquam id felis id, pharetra aliquam nisl. " +
            "Ut at diam leo. Ut non faucibus dui. Suspendisse posuere cursus suscipit. In sit amet justo ex. " +
            "Vivamus elementum faucibus eros ac lacinia. Curabitur feugiat, " +
            "lectus a porta porta, erat enim tincidunt turpis, auctor rutrum metus sem sed risus. " +
            "Nullam nunc mi, mollis quis finibus ut, dignissim eget orci. " +
            "Praesent nibh nulla, imperdiet in sapien eget, tincidunt scelerisque sem.";

    @Test
    public void serializationTest(){
        WordIngestResult original = new WordIngestResult();
        original.setMetadata(createWordMetadata());
        original.setCombinedProposedTitles(createTitles());
        original.setCsvValue("1,2,3,4,5");
        original.setErrorType(ProcessingErrorType.ERR_UNKNOWN);
        original.setLongHistograms(createLongHistograms());
        original.setScanned(false);
        original.setContent(LOREM_IPSUM);
        original.setOriginalContent(LOREM_IPSUM);
        original.setFileName("LoremIpsum.docx");

        try {
            String valueAsString = mapper.writeValueAsString(original);
            System.out.println(valueAsString);
            WordIngestResult deserialized = mapper.readValue(valueAsString, WordIngestResult.class);
            assertEquals(original, deserialized);
        }
        catch (IOException e) {
            Assert.fail(e.toString());
        }
    }

    // ----------------------------- assert equals ---------------------------------------------------------

    private void assertEquals(WordIngestResult orig, WordIngestResult copy){
        Assert.assertEquals(orig.getCsvValue(), copy.getCsvValue());
        Assert.assertEquals(orig.getCombinedProposedTitles(), copy.getCombinedProposedTitles());
        assertEquals(orig.getMetadata(), copy.getMetadata());
        assertSame(orig.getErrorType(), copy.getErrorType());
        assertEquals(orig.getLongHistograms(), copy.getLongHistograms());
        Assert.assertEquals(orig.getContent(), copy.getContent());
        Assert.assertEquals(orig.getOriginalContent(), copy.getOriginalContent());
        Assert.assertEquals(orig.isScanned(), copy.isScanned());
        Assert.assertEquals(orig.getFileName(), copy.getFileName());
    }

    private void assertEquals(WordfileMetaData orig, WordfileMetaData copy){
        Assert.assertEquals(orig.getAuthor(), copy.getAuthor());
        Assert.assertEquals(orig.getTitle(), copy.getTitle());
    }

    // --------------------------- create mock data --------------------------------
    private WordfileMetaData createWordMetadata(){
        WordfileMetaData result = new WordfileMetaData();
        result.setTitle("De Finibus Bonorum et Malorum");
        result.setAuthor("Cicero");
        return result;
    }

}
