package com.cla.common.domain.dto.messages;

import com.beust.jcommander.internal.Lists;
import com.cla.common.domain.dto.histogram.ClaHistogramCollection;
import com.cla.common.domain.dto.histogram.ClaSuperCollection;
import com.cla.connector.domain.dto.error.ProcessingErrorType;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;

import java.io.IOException;
import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;
import java.util.stream.LongStream;

/**
 * Common methods for testing ingest results serialization
 * Created by vladi on 3/8/2017.
 */
public abstract class AbstractIngestResultTest {

    protected ObjectMapper mapper = new ObjectMapper();
    protected Random random = new Random();

    // ----------------------------- assert equals ---------------------------------------------------------

    protected void assertEquals(ClaSuperCollection<Long> orig, ClaSuperCollection<Long> copy){
        orig.getCollections().keySet().forEach(key -> {
            ClaHistogramCollection<Long> origCol = orig.getCollection(key);
            ClaHistogramCollection<Long> copyCol = copy.getCollection(key);
            Assert.assertEquals(0, origCol.diff(copyCol).size());
        });
    }

    protected void assertEquals(IngestErrorInfo orig, IngestErrorInfo copy){
        Assert.assertEquals(orig.getExceptionText(), copy.getExceptionText());
        Assert.assertEquals(orig.getExceptionType(), copy.getExceptionType());
        Assert.assertEquals(orig.getText(), copy.getText());
        Assert.assertEquals(orig.getType(), copy.getType());
    }

    // ---------------------------- create mock data ----------------------------------------

    protected ClaSuperCollection<Long> createLongHistograms(){
        ClaSuperCollection<Long> longHistograms = new ClaSuperCollection<>();
        IntStream.range(0, ClaHistogramCollection.excelLongHistogramNames.length)
                .forEach(i -> longHistograms.getCollections()
                        .put(ClaHistogramCollection.excelLongHistogramNames[i].name(),
                                createRandomizedLongHistogram(i + 10)));


        return longHistograms;
    }

    private ClaHistogramCollection<Long> createRandomizedLongHistogram(int length){
        ClaHistogramCollection<Long> result = new ClaHistogramCollection<>();
        LongStream.generate(() -> random.nextLong()).limit(length).forEach(result::addItem);
        return result;
    }


    protected IngestErrorInfo createErrorInfo(){
        IngestErrorInfo info = new IngestErrorInfo();
        info.setExceptionText("ex-text");
        info.setExceptionType(IOException.class.getSimpleName());
        info.setText("user-friendly-text");
        info.setType(ProcessingErrorType.ERR_UNKNOWN);
        return info;
    }

    protected List<String> createTitles(){
        List<String> titles = Lists.newArrayList();
        titles.add("title1");
        titles.add("title2");
        titles.add("title3");
        return titles;
    }

}
