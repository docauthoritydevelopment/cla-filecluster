package com.cla.common.utils.tokens;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yael on 11/16/2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class NGramTokenizerHashedTest {

    @InjectMocks
    private NGramTokenizerHashed nGramTokenizerHashed;

    @Before
    public void init() {
        ReflectionTestUtils.setField(nGramTokenizerHashed, "reuseTextTokenizer", true);
        ReflectionTestUtils.setField(nGramTokenizerHashed, "minValueLength", 3);
        List<String> removalStrs = new ArrayList<>();
        removalStrs.add("999999999");
        ReflectionTestUtils.setField(nGramTokenizerHashed, "removalStrs", removalStrs);
    }

    @Test
    public void testCommaHash() {
        Assert.assertEquals(nGramTokenizerHashed.getHash("gomez,ANNUNCIO",null),nGramTokenizerHashed.getHash("GOMEZ ANNUNCIO",null));
    }
}
