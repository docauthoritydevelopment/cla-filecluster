package com.cla.common.media.files.pdf;

import com.cla.common.allocation.IngestionResourceManager;
import com.cla.common.allocation.PdfMemoryAllocationStrategy;
import com.cla.common.domain.dto.mediaproc.word.WordFile;
import com.cla.common.domain.dto.word.WordFilesSimilarity;
import com.cla.common.media.files.word.ClaWord6Extractor;
import com.cla.common.media.files.word.ClaWordExtractor;
import com.cla.common.media.files.word.ClaWordXmlExtractor;
import com.cla.common.media.files.word.WordComparator;
import com.cla.connector.mediaconnector.fileshare.FileShareMediaUtils;
import com.cla.common.utils.tokens.NGramTokenizerHashed;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Optional;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ClaNativePdfExtractorTest {

    @Mock
    private NGramTokenizerHashed nGramTokenizerHashed;

    @Mock
    private IngestionResourceManager ingestionResourceManager;

    @Mock
    private PdfMemoryAllocationStrategy allocationStrategy;

    @InjectMocks
    private ClaNativePdfExtractor claNativePdfExtractor;

    @InjectMocks
    private ClaWord6Extractor claWord6Extractor;

    private ClaWordXmlExtractor claWordXmlExtractor = new ClaWordXmlExtractor();

    private ClaWordExtractor claWordExtractor = new ClaWordExtractor(){
        @Override
        protected ClaWord6Extractor createClaWord6Extractor() {
            return claWord6Extractor;
        }

        @Override
        protected ClaWordXmlExtractor createClaWordXmlExtractor() {
            return claWordXmlExtractor;
        }

    };

    @Before
    public void init() {
        ReflectionTestUtils.setField(claNativePdfExtractor, "pdfMaxFileSize", 52428800);
        ReflectionTestUtils.setField(claNativePdfExtractor, "useScratchFile", true);
        ReflectionTestUtils.setField(claNativePdfExtractor, "scratchFileThresholdMb", 2);
        ReflectionTestUtils.setField(claNativePdfExtractor, "scratchFileTempDir", "./tempDir");
        ReflectionTestUtils.setField(claNativePdfExtractor, "pdfBoxPushbackSize", "131072");

        ReflectionTestUtils.setField(claWordXmlExtractor, "nGramTokenizerHashed", nGramTokenizerHashed);
        ReflectionTestUtils.setField(claWordExtractor, "patternSearchActive", true);
        ReflectionTestUtils.setField(claWordXmlExtractor, "xmlFilteringSizeThreshold", 1000000);
        ReflectionTestUtils.setField(claWordXmlExtractor, "xmlFilteringMaxObjects", 900000);
        ReflectionTestUtils.setField(claWordXmlExtractor, "userContentSignatureEnabled", true);

        when(allocationStrategy.getOverheadMemoryAllocationMB()).thenReturn(300);
        when(ingestionResourceManager.acquire(300)).thenReturn(true);
        when(allocationStrategy.pageMemAllocationActive()).thenReturn(true);
        when(allocationStrategy.calculateRequiredMemory(4)).thenReturn(4);
    }

    @Test
    public void pdfExtractor() throws FileNotFoundException {
        final String path = "src/test/resources/files/pdf/01032 Distribution.pdf";

        Optional<ClaFilePropertiesDto> props = FileShareMediaUtils.getMediaProperties(path, true, true);
        final WordFile wordFile = claNativePdfExtractor.extract(new FileInputStream(path), path,true, props.get());
        assertTrue(wordFile.getLongHistograms().getCollections().size() > 10);
        assertTrue(wordFile.getMetaData().getFsFileSize() > 10);
        assertTrue(wordFile.getProposedTitles().containsKey("P8=02=001"));
        assertTrue(wordFile.getLongHistograms().getCollections().size() == 15);
        assertTrue(wordFile.getLongHistograms().getCollections().get("PV_AbsoluteStyleSignatures").getGlobalCount() == 13);
    }


    @Test
    public void pdfExtract2Files1() throws FileNotFoundException{
        final String path1 = "src/test/resources/files/pdf/Tables BELDEN TIMOTHY N - cute.pdf";
        final String path2 = "src/test/resources/files/pdf/Tables BELDEN TIMOTHY N.pdf";

        Optional<ClaFilePropertiesDto> propsOp1 = FileShareMediaUtils.getMediaProperties(path1, true, true);

        final WordFile wf1 = claNativePdfExtractor.extract(new FileInputStream(path1), path1, true, propsOp1.get());
        assertTrue(wf1.getLongHistograms().getCollections().size() > 10);
        assertTrue(wf1.getMetaData().getFsFileSize() > 10);

        Optional<ClaFilePropertiesDto> props = FileShareMediaUtils.getMediaProperties(path2, true, true);
        final WordFile wf2 = claNativePdfExtractor.extract(new FileInputStream(path2), path2, true, props.get());
        assertTrue(wf2.getLongHistograms().getCollections().size() > 10);
        assertTrue(wf2.getMetaData().getFsFileSize() > 10);

        wf1.setId(1);
        wf2.setId(2);

        final WordFilesSimilarity wfs = WordComparator.compareFiles(wf1, wf2);
        assertTrue(wfs.getPotentialSimilarityRate()>0.9);
    }

    @Test
    public void pdfExtract2Files2() throws FileNotFoundException{
        final String path1 = "src/test/resources/files/pdf/wr569_CAB_spc_Feb2002.pdf";
        final String path2 = "src/test/resources/files/pdf/wr569_VGR_main_spc_May2002.pdf";

        Optional<ClaFilePropertiesDto> props = FileShareMediaUtils.getMediaProperties(path1, true, true);
        final WordFile wf1 = claNativePdfExtractor.extract(new FileInputStream(path1), path1, true, props.get());

        Optional<ClaFilePropertiesDto> props2 = FileShareMediaUtils.getMediaProperties(path2, true, true);
        final WordFile wf2 = claNativePdfExtractor.extract(new FileInputStream(path2), path2, true, props2.get());

        wf1.setId(1);
        wf2.setId(2);

        final WordFilesSimilarity wfs = WordComparator.compareFiles(wf1, wf2);
        assertTrue(wfs.getPotentialSimilarityRate()>0.9);
    }

    @Test
    public void pdfFormExtractor() throws FileNotFoundException{
        final String path = "src/test/resources/files/pdf/ILDC 2002 Release_form.pdf";

        final WordFile wordFile = extract(new FileInputStream(path), path,true);

        assertTrue(wordFile.getProposedTitles().containsKey("P8=02=001"));
        assertTrue(wordFile.getLongHistograms().getCollections().size() == 15);
        assertTrue(wordFile.getLongHistograms().getCollections().get("PV_AbsoluteStyleSignatures").getGlobalCount() == 3);
    }

    @Test
    public void pdfLetterExtractor() throws FileNotFoundException{
        final String path = "./src/test/resources/files/pdf/01032 Distribution.pdf";

        final WordFile wordFile = extract(new FileInputStream(path), path,true);

        assertTrue(wordFile.getProposedTitles().containsKey("P8=02=001"));
        assertTrue(wordFile.getLongHistograms().getCollections().size() == 15);
        assertTrue(wordFile.getLongHistograms().getCollections().get("PV_AbsoluteStyleSignatures").getGlobalCount() == 13);
    }

    @Test
    public void pdfExtractCmpWord() throws FileNotFoundException{
        final String path1 = "./src/test/resources/files/pdf/Tables BELDEN TIMOTHY N.pdf";
        final String path2 = "./src/test/resources/files/pdf/Tables BELDEN TIMOTHY N.doc";

        final WordFile wf1 = extract(new FileInputStream(path1), path1, true);

        Optional<ClaFilePropertiesDto> props = FileShareMediaUtils.getMediaProperties(path2, true, true);
        final WordFile wf2 = claWordExtractor.extract(new FileInputStream(path2), path2,true, props.get());

        wf1.setId(1);
        wf2.setId(2);

        final WordFilesSimilarity wfs = WordComparator.compareFiles(wf1, wf2);
        assertTrue(wfs.getPotentialSimilarityRate()>0.75);
    }

    public WordFile extract(InputStream is, final String path, final boolean extractMetadata) throws FileNotFoundException {
        Optional<ClaFilePropertiesDto> mediaProperties = FileShareMediaUtils.getMediaProperties(path, true, true);
        return claNativePdfExtractor.extract(is, path, extractMetadata, mediaProperties.orElse(null));
    }
}