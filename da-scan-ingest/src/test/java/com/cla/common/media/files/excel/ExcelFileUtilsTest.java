package com.cla.common.media.files.excel;

import com.cla.common.domain.dto.FileMediaType;
import com.cla.common.domain.dto.mediaproc.excel.ExcelParseParams;
import com.cla.common.domain.dto.mediaproc.excel.ExcelWorkbook;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class ExcelFileUtilsTest {

    @Test
    public void testMediaNameDetector() {
        String path = "src/test/resources/files/excel/special/macro enabled sheet.xlsm";
        FileMediaType fileMediaType = ExcelFileUtils.mediaNameDetector(path);
        assertTrue(fileMediaType == FileMediaType.EXCELXML);

        path = "src/test/resources/files/excel/gradual/A.xlsx";
        fileMediaType = ExcelFileUtils.mediaNameDetector(path);
        assertTrue(fileMediaType == FileMediaType.EXCELXML);

        path = "src/test/resources/files/excel/gradual/A.xls";
        fileMediaType = ExcelFileUtils.mediaNameDetector(path);
        assertTrue(fileMediaType == FileMediaType.EXCEL);

        path = "src/test/resources/files/excel/gradual/A.doc";
        fileMediaType = ExcelFileUtils.mediaNameDetector(path);
        assertTrue(fileMediaType == FileMediaType.UNKNOWN);
    }

    @Test
    public void testCheckMaxExcelFileSizeLimit() {
        long maxExcelFileSize = 10;
        String path = "src/test/resources/files/excel/gradual/A.doc";

        ExcelParseParams excelParseParams = new ExcelParseParams(true, 3, null, true, false);

        ExcelWorkbook excelWorkbook = new ExcelWorkbook(excelParseParams);
        ClaFilePropertiesDto cfp = ClaFilePropertiesDto.create();
        cfp.setFileSize(500L);
        boolean res = ExcelFileUtils.checkMaxExcelFileSizeLimit(maxExcelFileSize, path, excelWorkbook, cfp);
        assertTrue(res);
        assertTrue(excelWorkbook.getMetadata().getFsFileSize() == 500L);

        cfp.setFileSize(5L);
        res = ExcelFileUtils.checkMaxExcelFileSizeLimit(maxExcelFileSize, path, excelWorkbook, cfp);
        assertTrue(!res);
        assertTrue(excelWorkbook.getMetadata().getFsFileSize() == 500L);
    }
}