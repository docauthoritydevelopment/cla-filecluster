package com.cla.common.media.files.excel;

import com.cla.common.allocation.ExcelMemoryAllocationStrategy;
import com.cla.common.allocation.IngestionResourceManager;
import com.cla.common.domain.dto.mediaproc.excel.ExcelWorkbook;
import com.cla.common.utils.tokens.NGramTokenizerHashed;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.mediaconnector.fileshare.FileShareMediaUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;

import static org.junit.Assert.assertTrue;

/**
 * ExcelFilesService test
 * Created by yael on 11/15/2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class ExcelFilesServiceTest {

    @Mock
    private NGramTokenizerHashed nGramTokenizerHashed;

    @Mock
    private IngestionResourceManager ingestionResourceManager;

    @Mock
    private ExcelMemoryAllocationStrategy allocationStrategy;

    @InjectMocks
    private ExcelFilesServiceImpl excelFilesService;

    @Before
    public void init() {

        ReflectionTestUtils.setField(excelFilesService, "isTokenMatcherActive", true);
        ReflectionTestUtils.setField(excelFilesService, "patternSearchActive", true);
        ReflectionTestUtils.setField(excelFilesService, "maxExcelFileSize", 0);
        ReflectionTestUtils.setField(excelFilesService, "useTableHeadingsAsTitles", true);
        ReflectionTestUtils.setField(excelFilesService, "excelWorkbooksMemoryAllocationMB", 900);
        ReflectionTestUtils.setField(excelFilesService, "singleTokenNumberedTitleRateReduction", 3);

        String[] ignoredSheetNamePrefixesAsTitles = {"sheet","\u05D2\u05DC\u05D9\u05D5\u05DF"};
        ReflectionTestUtils.setField(excelFilesService, "ignoredSheetNamePrefixesAsTitles", ignoredSheetNamePrefixesAsTitles);

        excelFilesService.init();
    }

    private static final String enterprisePath = "src/test/resources/files/excel/EnterpriseFiles/";

    public ExcelWorkbook ingestWorkbook(String path, InputStream is, boolean extractMetadata) throws FileNotFoundException {
        Optional<ClaFilePropertiesDto> mediaProperties = FileShareMediaUtils.getMediaProperties(path, true, true);
        return excelFilesService.ingestWorkbook(path, is, extractMetadata, mediaProperties.orElse(null));
    }

    @Test
    public void testSampleWB() throws IOException {
        final String path = enterprisePath + "103820OPMHoursSurveyJan01.xls";
        final ExcelWorkbook ingestWorkbook = ingestWorkbook(path,new FileInputStream(path),true);
        assertTrue(ingestWorkbook.getSheets().size() == 1);
        assertTrue(ingestWorkbook.getSheets().get("0").getMetaData().getNumOfCells() == 153);
    }
}

