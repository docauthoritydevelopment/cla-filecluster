package com.cla.common.media.files.excel.streaming;

import com.cla.common.allocation.ExcelMemoryAllocationStrategy;
import com.cla.common.allocation.IngestionResourceManager;
import com.cla.common.domain.dto.mediaproc.excel.ExcelParseParams;
import com.cla.common.domain.dto.mediaproc.excel.ExcelWorkbook;
import com.cla.connector.mediaconnector.fileshare.FileShareMediaUtils;
import com.cla.common.utils.tokens.NGramTokenizerHashed;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.FileInputStream;
import java.util.Optional;

import static org.junit.Assert.assertTrue;

/**
 * ExcelStreamingFilesExtractor test
 * Created by yael on 11/14/2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class ExcelStreamingFilesExtractorTest {

    @Mock
    private NGramTokenizerHashed nGramTokenizerHashed;

    @Mock
    private IngestionResourceManager ingestionResourceManager;

    @Mock
    private ExcelMemoryAllocationStrategy allocationStrategy;

    @InjectMocks
    private ExcelStreamingFilesExtractor excelStreamingFilesExtractor;


    @Before
    public void init() {
        ReflectionTestUtils.setField(excelStreamingFilesExtractor, "useTableHeadingsAsTitles", true);
        ReflectionTestUtils.setField(excelStreamingFilesExtractor, "singleTokenNumberedTitleRateReduction", 3);
        ReflectionTestUtils.setField(excelStreamingFilesExtractor, "patternSearchActive", true);
        ReflectionTestUtils.setField(excelStreamingFilesExtractor, "userContentSignatureEnabled", true);
        ReflectionTestUtils.setField(excelStreamingFilesExtractor, "maxExcelFileSize", 0);
        ReflectionTestUtils.setField(excelStreamingFilesExtractor, "bufferedInputStreamSize", 100000);
        ReflectionTestUtils.setField(excelStreamingFilesExtractor, "xmlFilteringSizeThreshold", 1000000);
        ReflectionTestUtils.setField(excelStreamingFilesExtractor, "xmlFilteringMaxObjects", 1000000);

        ReflectionTestUtils.setField(excelStreamingFilesExtractor, "excelMaxRecordsCount", 400000);
        ReflectionTestUtils.setField(excelStreamingFilesExtractor, "excelRecordFilteringThreshold", 5000);
        ReflectionTestUtils.setField(excelStreamingFilesExtractor, "filterRecordsFromWorkBookBeforeCreation", true);

        ReflectionTestUtils.setField(excelStreamingFilesExtractor, "isTokenMatcherActive", true);

        String[] ignoredSheetNamePrefixesAsTitles = {"sheet","\u05D2\u05DC\u05D9\u05D5\u05DF"};
        ReflectionTestUtils.setField(excelStreamingFilesExtractor, "ignoredSheetNamePrefixesAsTitles", ignoredSheetNamePrefixesAsTitles);

        ExcelParseParams.setNGramTokenizer(nGramTokenizerHashed);
        excelStreamingFilesExtractor.init();
    }

    private static final String specialPath = "src/test/resources/files/excel/special/";
    private static final String gradualPath = "src/test/resources/files/excel/gradual/";

    @Test
    public void testProtectedExcelXMLFile() throws Exception {
        final String path1 = gradualPath + "A.xlsx";
        Optional<ClaFilePropertiesDto> props1 = FileShareMediaUtils.getMediaProperties(path1, true, true);
        final ExcelWorkbook streamIngestedWorkbook = excelStreamingFilesExtractor.ingestWorkbook(
                new FileInputStream(path1),path1,null, props1.get());
        assertTrue(streamIngestedWorkbook.getSheets().size() == 1);
        assertTrue(streamIngestedWorkbook.getSheets().get("0").getMetaData().getNumOfCells() == 181);
    }

    @Test
    public void testMacroEnabledExcel() throws Exception {
        final String path1 = specialPath+ "macro enabled sheet.xlsm";
        Optional<ClaFilePropertiesDto> props1 = FileShareMediaUtils.getMediaProperties(path1, true, true);
        final ExcelWorkbook streamIngestedWorkbook = excelStreamingFilesExtractor.ingestWorkbook(
                new FileInputStream(path1),path1,null, props1.get());
        assertTrue(streamIngestedWorkbook.getSheets().size() == 1);
        assertTrue(streamIngestedWorkbook.getSheets().get("0").getMetaData().getNumOfCells() == 1);
    }
}
