package com.cla.common.media.files.word;

import com.cla.common.domain.dto.mediaproc.word.WordFile;
import com.cla.common.domain.dto.pv.PVType;
import com.cla.common.domain.dto.word.WordFilesSimilarity;
import com.cla.connector.mediaconnector.fileshare.FileShareMediaUtils;
import com.cla.connector.mediaconnector.fileshare.FileShareMediaConnector;
import com.cla.common.utils.tokens.NGramTokenizerHashed;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class ClaWordExtractorTest {

    @Mock
    private NGramTokenizerHashed nGramTokenizerHashed;

    @InjectMocks
    private ClaWord6Extractor claWord6Extractor;

    private ClaWordXmlExtractor claWordXmlExtractor = new ClaWordXmlExtractor();

    private ClaWordExtractor claWordExtractor = new ClaWordExtractor(){
            @Override
            protected ClaWord6Extractor createClaWord6Extractor() {
                return claWord6Extractor;
            }

            @Override
            protected ClaWordXmlExtractor createClaWordXmlExtractor() {
                return claWordXmlExtractor;
            }

        };

    @Before
    public void init() {
        ReflectionTestUtils.setField(claWordXmlExtractor, "nGramTokenizerHashed", nGramTokenizerHashed);
        ReflectionTestUtils.setField(claWordExtractor, "patternSearchActive", true);
        ReflectionTestUtils.setField(claWordXmlExtractor, "xmlFilteringSizeThreshold", 1000000);
        ReflectionTestUtils.setField(claWordXmlExtractor, "xmlFilteringMaxObjects", 900000);
        ReflectionTestUtils.setField(claWordXmlExtractor, "userContentSignatureEnabled", true);
    }

    @Test
    public void testExtractor() throws FileNotFoundException {
        final String path = "src/test/resources/files/wordXml/one/01032 Distribution.docx";

        final Map<Long,String> debugMapper = new HashMap<>();

        WordFile.setDebugHashMapper(debugMapper);
        Optional<ClaFilePropertiesDto> props1 = FileShareMediaUtils.getMediaProperties(path, true, true);
        final WordFile wordFile = claWordExtractor.extract(new FileInputStream(path), path,true, props1.get());

        wordFile.setId(0);
        assertTrue(wordFile.getLongHistograms().getCollections().size() > 10);
        assertTrue(wordFile.getMetaData().getFsFileSize() > 10);
        assertTrue(wordFile.getLongHistograms().getCollections().size() == 15);
        assertTrue(wordFile.getLongHistograms().getCollections().get("PV_AbsoluteStyleSignatures").getGlobalCount() == 60);

        final WordFilesSimilarity wordFilesSimilarity = WordComparator.compareFiles(wordFile, wordFile);
        assertTrue(WordFilesSimilarity.getTitleCsvString().length() > 10);
        assertTrue(wordFilesSimilarity.getCsvString().length() > 10);
        assertTrue(wordFilesSimilarity.getSimilarityValue(PVType.PV_StyleSequence) == 1);
    }

    @Test
    public void testHebDocExtractor() throws FileNotFoundException{
        final String path = "src/test/resources/files/wordXml/תשע שאלות שצריך לשאול את חברת ההשמה לפני ראיון עבודה.docx";

        final Map<Long,String> debugMapper = new HashMap<>();

        WordFile.setDebugHashMapper(debugMapper);
        claWordExtractor.setPatternSearchActive(true);
        Optional<ClaFilePropertiesDto> props1 = FileShareMediaUtils.getMediaProperties(path, true, true);
        final WordFile wordFile = claWordExtractor.extract(
                new FileInputStream(path), path,true, props1.get());
        wordFile.setId(0);
        assertTrue(wordFile.getLongHistograms().getCollections().size() > 10);
        assertTrue(wordFile.getMetaData().getFsFileSize() > 10);
        assertTrue(wordFile.getLongHistograms().getCollections().size() == 15);
        assertTrue(wordFile.getLongHistograms().getCollections().get("PV_AbsoluteStyleSignatures").getGlobalCount() == 56);

        final WordFilesSimilarity wordFilesSimilarity = WordComparator.compareFiles(wordFile, wordFile);
        assertTrue(WordFilesSimilarity.getTitleCsvString().length() > 10);
        assertTrue(wordFilesSimilarity.getCsvString().length() > 10);
        assertTrue(wordFilesSimilarity.getSimilarityValue(PVType.PV_StyleSequence) == 1);
    }

    @Test
    public void testWordXmlDiff() throws FileNotFoundException{
        final String path6 = "src/test/resources/files/word/";
        final String pathXml = "src/test/resources/files/wordXml/";
        final String fileName = "CPUC Aug 23 Alternative Decision.doc";

        String name = path6 + fileName;
        Optional<ClaFilePropertiesDto> props = FileShareMediaUtils.getMediaProperties(name, true, true);
        final WordFile word6File = claWordExtractor.extract(
                new FileInputStream(name), name,true, props.get());
        String name1 = pathXml + fileName + "x";
        Optional<ClaFilePropertiesDto> props1 = FileShareMediaUtils.getMediaProperties(name1, true, true);
        final WordFile wordXmlFile = claWordExtractor.extract(
                new FileInputStream(name1), name1,true, props1.get());
        word6File.setId(0);
        wordXmlFile.setId(1);

        final WordFile diff = word6File.diff(wordXmlFile);
        final WordFilesSimilarity wordFilesSimilarity = WordComparator.compareFiles(word6File, wordXmlFile);

        assertTrue(WordFilesSimilarity.getTitleCsvString().length() > 10);
        assertTrue(wordFilesSimilarity.getCsvString().length() > 10);
        assertTrue(diff.getCsvString().length() > 10);
    }

    @Test
    public void testFileAttributes() throws IOException {
        final Path path = FileSystems.getDefault().getPath("src/test/resources/files/word/one/01032 Distribution.doc");
        final ClaFilePropertiesDto cfp = FileShareMediaUtils.getMediaProperties(path, true, true);
        assertTrue(cfp.getFileSize() > 5000);
        assertTrue(cfp.getAccessTimeMilli() > 1000);
        assertTrue(cfp.getAclSignature().length() > 10);
    }

    @Test
    public void testSimilarity1() throws FileNotFoundException {
        final String file1 = "src/test/resources/files/word/X20615.DOC";
        final String file2 = "src/test/resources/files/word/X20615_5.DOC";
        Optional<ClaFilePropertiesDto> props1 = FileShareMediaUtils.getMediaProperties(file1, true, true);
        Optional<ClaFilePropertiesDto> props2 = FileShareMediaUtils.getMediaProperties(file2, true, true);

        final WordFile wf1 = claWordExtractor.extract(new FileInputStream(file1), file1,true, props1.get());
        final WordFile wf2 = claWordExtractor.extract(new FileInputStream(file2),file2,true, props2.get());
        wf1.setId(1);
        wf2.setId(2);

        final WordFilesSimilarity wfs = WordComparator.compareFiles(wf1, wf2);
        assertTrue(wfs.getCsvString().length() > 10);
        float similarityValueToTest = wfs.getSimilarityValue(PVType.PV_StyleSequence);
        assertTrue("similarity is "+similarityValueToTest, similarityValueToTest > 0.79 && similarityValueToTest <= 1);
    }

    @Test
    public void testIngestWordFromSharePoint() throws IOException {
        String pathString = "src/test/resources/files/docx/GMI.docx";
        Path path = Paths.get(pathString);
        byte[] bytes = Files.readAllBytes(path);
        ByteArrayInputStream inputStream = new ByteArrayInputStream(bytes);
        FileShareMediaConnector fileShareMediaConnector = new FileShareMediaConnector(true, true, null);
        ClaFilePropertiesDto claFileProperties = fileShareMediaConnector.getFileAttributes(pathString);
        final WordFile wf = claWordExtractor.extract(inputStream,pathString,true,claFileProperties);
        assertTrue(wf.getMetaData().getFsFileSize() > 10);
        assertTrue(wf.getLongHistograms().getCollections().size() == 15);
        assertTrue(wf.getLongHistograms().getCollections().get("PV_AbsoluteStyleSignatures").getGlobalCount() == 96);
    }
}