package com.cla.common.media.files.excel;

import com.cla.common.allocation.ExcelMemoryAllocationStrategy;
import com.cla.common.allocation.IngestionResourceManager;
import com.cla.common.domain.dto.histogram.ClaHistogramCollection;
import com.cla.common.domain.dto.mediaproc.excel.ExcelSheet;
import com.cla.common.domain.dto.mediaproc.excel.ExcelWorkbook;
import com.cla.common.domain.dto.mediaproc.excel.ExcelWorkbookMetaData;
import com.cla.common.media.files.excel.streaming.ExcelStreamingFilesExtractor;
import com.cla.connector.mediaconnector.fileshare.FileShareMediaUtils;
import com.cla.common.utils.tokens.NGramTokenizerHashed;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.google.common.collect.MapDifference;
import com.google.common.collect.Maps;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.hamcrest.Matchers;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

/**
 * Compare Ingest in ExcelFilesService and ExcelStreamingFilesExtractor
 * Created by yael on 11/15/2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class ExcelIngestComparisonTest {

    @Mock
    private NGramTokenizerHashed nGramTokenizerHashed;

    @Mock
    private IngestionResourceManager ingestionResourceManager;

    @Mock
    private ExcelMemoryAllocationStrategy allocationStrategy;

    @InjectMocks
    private ExcelFilesServiceImpl excelFilesService;

    @InjectMocks
    private ExcelStreamingFilesExtractor excelStreamingFilesExtractor;

    @Before
    public void init() {

        ReflectionTestUtils.setField(excelFilesService, "isTokenMatcherActive", true);
        ReflectionTestUtils.setField(excelFilesService, "patternSearchActive", true);
        ReflectionTestUtils.setField(excelFilesService, "maxExcelFileSize", 0);
        ReflectionTestUtils.setField(excelFilesService, "useTableHeadingsAsTitles", true);
        ReflectionTestUtils.setField(excelFilesService, "excelWorkbooksMemoryAllocationMB", 900);
        ReflectionTestUtils.setField(excelFilesService, "singleTokenNumberedTitleRateReduction", 3);

        ReflectionTestUtils.setField(excelStreamingFilesExtractor, "useTableHeadingsAsTitles", true);
        ReflectionTestUtils.setField(excelStreamingFilesExtractor, "singleTokenNumberedTitleRateReduction", 3);
        ReflectionTestUtils.setField(excelStreamingFilesExtractor, "patternSearchActive", true);
        ReflectionTestUtils.setField(excelStreamingFilesExtractor, "userContentSignatureEnabled", true);
        ReflectionTestUtils.setField(excelStreamingFilesExtractor, "maxExcelFileSize", 0);
        ReflectionTestUtils.setField(excelStreamingFilesExtractor, "bufferedInputStreamSize", 100000);
        ReflectionTestUtils.setField(excelStreamingFilesExtractor, "xmlFilteringSizeThreshold", 1000000);
        ReflectionTestUtils.setField(excelStreamingFilesExtractor, "xmlFilteringMaxObjects", 1000000);

        ReflectionTestUtils.setField(excelStreamingFilesExtractor, "excelMaxRecordsCount", 400000);
        ReflectionTestUtils.setField(excelStreamingFilesExtractor, "excelRecordFilteringThreshold", 5000);
        ReflectionTestUtils.setField(excelStreamingFilesExtractor, "filterRecordsFromWorkBookBeforeCreation", true);

        ReflectionTestUtils.setField(excelStreamingFilesExtractor, "isTokenMatcherActive", true);

        String[] ignoredSheetNamePrefixesAsTitles = {"sheet","\u05D2\u05DC\u05D9\u05D5\u05DF"};
        ReflectionTestUtils.setField(excelStreamingFilesExtractor, "ignoredSheetNamePrefixesAsTitles", ignoredSheetNamePrefixesAsTitles);

        ReflectionTestUtils.setField(excelFilesService, "ignoredSheetNamePrefixesAsTitles", ignoredSheetNamePrefixesAsTitles);

        excelFilesService.init();
        excelStreamingFilesExtractor.init();
    }

    private static final String specialPath = "src/test/resources/files/excel/special/";
    private static final String gradualPath = "src/test/resources/files/excel/gradual/";
    private static final String enterprisePath = "src/test/resources/files/excel/EnterpriseFiles/";

    @Test
    public void testEmptyFilesComparison() throws Exception {
        final String path1 = gradualPath + "empty.xls";
        compareXmlFile(path1);
    }

    @Test
    public void testChartSheet() throws Exception {
        final String path1 = specialPath+ "chart_sheets.xls";
        compareXmlFile(path1);
    }

    @Test
    public void testChartSheetXML() throws Exception {
        final String path1 = specialPath+ "chart_sheets.xlsx";
        compareXmlFile(path1);
    }

    @Test
    public void testEmptyFilesComparisonXML() throws Exception {
        final String path1 = gradualPath + "empty.xlsx";
        compareXmlFile(path1);
    }

    @Test
    public void testOneCellFilesComparison() throws Exception {
        final String path1 = gradualPath + "one_cell.xls";
        compareXmlFile(path1);
    }

    @Test
    public void testOneCellFilesComparisonXML() throws Exception {
        final String path1 = gradualPath + "one_cell.xlsx";
        compareXmlFile(path1);
    }

    @Test
    public void testStringOpenExcel2013() throws Exception {
        final String path1 = gradualPath + "strict.xlsx";
        compareXmlFile(path1);
    }

    @Test
    public void testNumberCellsFilesComparison() throws Exception {
        final String path1 = gradualPath + "number_cells.xls";
        compareXmlFile(path1);
    }

    @Test
    public void testNumberCellsFilesComparisonXML() throws Exception {
        final String path1 = gradualPath + "number_cells.xlsx";
        compareXmlFile(path1);
    }

    @Test
    public void testFormulaCellFilesComparison() throws Exception {
        final String path1 = gradualPath + "formula_cell.xls";
        compareXmlFile(path1);
    }


    @Test
    public void testFormulaCellFilesComparisonXML() throws Exception {
        final String path1 = gradualPath + "formula_cell.xlsx";
        compareXmlFile(path1);
    }

    @Test
    public void testIllegalFormulaCellsXML() throws Exception {
        final String path1 = gradualPath + "cgas_3.xlsx";
        compareXmlFile(path1);
    }

    @Test
    public void testSharedFormulaSupportXML() throws Exception {
        final String path1 = gradualPath + "shared_formula.xlsx";
        compareXmlFile(path1);
    }

    @Test
    public void testManyRowsSheet1XML() throws Exception {
        final String path1 = gradualPath + "many_rows.xlsx";
        compareXmlFile(path1);
    }

    @Test
    public void testSheetWithStyleXML() throws Exception {
        final String path1 = gradualPath + "style_sheet.xlsx";
        compareXmlFile(path1);
    }

    @Test
    public void testComplexSheet2() throws Exception {
        final String path1 = gradualPath + "cgas_2.xls";
        compareXmlFile(path1);
    }

    @Test
    public void testSharedFormulaSupport() throws Exception {
        final String path1 = enterprisePath + "(CNGP) Consolidated Natural Gas Pipeline Inventory 2002.xls";
        compareXmlFile(path1);
    }

    @Test
    public void testMultipleSheetsComplexFileComparison1() throws Exception {
        final String path1 = enterprisePath + "1207LTXGD.xls";
        compareXmlFile(path1);
    }

    @Test
    public void testMultipleSheetsComplexFileComparison2() throws Exception {
        final String path1 = enterprisePath + "American Public Energy Agency.xls";
        compareXmlFile(path1);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    private ExcelWorkbook ingestWorkbook(String path, InputStream is, boolean extractMetadata) throws FileNotFoundException {
        Optional<ClaFilePropertiesDto> mediaProperties = FileShareMediaUtils.getMediaProperties(path, true, true);
        return excelFilesService.ingestWorkbook(path, is, extractMetadata, mediaProperties.orElse(null));
    }

    private void compareXmlFile(String path1) throws FileNotFoundException {
        final ExcelWorkbook legacyIngestedWorkbook = ingestWorkbook(path1,new FileInputStream(path1),true);

        Optional<ClaFilePropertiesDto> props1 = FileShareMediaUtils.getMediaProperties(path1, true, true);
        final ExcelWorkbook streamIngestedWorkbook = excelStreamingFilesExtractor.ingestWorkbook(
                new FileInputStream(path1),path1,null, props1.get());

        checkEquality(legacyIngestedWorkbook, streamIngestedWorkbook);
    }

    private void checkEquality(ExcelWorkbook ingestedWorkbook1, ExcelWorkbook ingestedWorkbook2) {
        checkMetadataEquality(ingestedWorkbook1, ingestedWorkbook2);
        checkSheetsEquality(ingestedWorkbook1, ingestedWorkbook2);
    }

    private void checkSheetsEquality(ExcelWorkbook ingestedWorkbook1, ExcelWorkbook ingestedWorkbook2) {
        final Map<String, String> book1SheetNamesMap = ingestedWorkbook1.getSheetFullNamesMap();
        final Map<String, String> book2SheetNamesMap = ingestedWorkbook2.getSheetFullNamesMap();
        MapDifference<String, String> difference = Maps.difference(book1SheetNamesMap, book2SheetNamesMap);
        Assert.assertThat("SheetNames - There should be no differing entries", difference.entriesDiffering().keySet(), Matchers.empty());
        Assert.assertThat("SheetNames - There should be no entries on legacy only", difference.entriesOnlyOnLeft().keySet(), Matchers.empty());
        Assert.assertThat("SheetNames - There should be no entried on streaming only", difference.entriesOnlyOnRight().keySet(), Matchers.empty());
        Assert.assertTrue("Sheet names should be equal", difference.areEqual());

        for (String sheetKey: ingestedWorkbook1.getSheets().keySet()) {
            ExcelSheet excelSheet1 = ingestedWorkbook1.getSheets().get(sheetKey);
            ExcelSheet excelSheet2 = ingestedWorkbook2.getSheets().get(sheetKey);
            checkSheetEquality(excelSheet1,excelSheet2);
        }
    }

    private void checkSheetEquality(ExcelSheet excelSheet1, ExcelSheet excelSheet2) {
        Set<String> histogramKeys = excelSheet1.getLongHistograms().getCollections().keySet();
        for (String histogramKey : histogramKeys) {
            ClaHistogramCollection<Long> histogram1 = excelSheet1.getLongHistograms().getCollection(histogramKey);
            ClaHistogramCollection<Long> histogram2 = excelSheet2.getLongHistograms().getCollection(histogramKey);
            Map<Long, Integer> histogramMap1 = histogram1.getCollection();
            Map<Long, Integer> histogramMap2 = histogram2.getCollection();
            MapDifference<Long, Integer> difference = Maps.difference(histogramMap1, histogramMap2);
            if (!difference.areEqual()) {
                Assert.assertThat("There should be no entries on legacy only "+histogramKey, difference.entriesOnlyOnLeft().keySet(), Matchers.empty());
                Assert.assertThat("There should be no entried on streaming only "+histogramKey, difference.entriesOnlyOnRight().keySet(), Matchers.empty());
                Assert.assertThat("There should be no differing entries for key "+histogramKey, difference.entriesDiffering().keySet(), Matchers.empty());
            }
        }

    }


    private void checkMetadataEquality(ExcelWorkbook legacyIngestedWorkbook, ExcelWorkbook streamIngestedWorkbook) {
        ExcelWorkbookMetaData legacyMetaData = legacyIngestedWorkbook.getMetadata();
        ExcelWorkbookMetaData streamMetaData = streamIngestedWorkbook.getMetadata();
        Assert.assertEquals(legacyMetaData.getAuthor(),streamMetaData.getAuthor());
        Assert.assertEquals(legacyMetaData.getCompany(), streamMetaData.getCompany());
        Assert.assertEquals(legacyMetaData.getOwner(),streamMetaData.getOwner());
        Assert.assertEquals("Metadata Number of sheets", legacyMetaData.getNumOfSheets(),streamMetaData.getNumOfSheets());
        Assert.assertEquals("Metadata Number of cells", legacyMetaData.getNumOfCells(),streamMetaData.getNumOfCells());
        Assert.assertEquals("Metadata Last Modified", legacyMetaData.getFsLastModified(),streamMetaData.getFsLastModified());
        Assert.assertEquals("Metadata Sheet Names",legacyMetaData.getSheetNames(),streamMetaData.getSheetNames());
        Assert.assertEquals("Metadata ACL Read", legacyMetaData.getAclReadAllowed(),streamMetaData.getAclReadAllowed());
        Assert.assertEquals("Number of formulas", legacyMetaData.getNumOfFormulas(),streamMetaData.getNumOfFormulas());
    }

}
