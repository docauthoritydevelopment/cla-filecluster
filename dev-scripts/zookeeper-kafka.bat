@echo off
echo off
echo Starting zookeeper console in the background
set DIRNAME=%~dp0
set ZOOKEEPER_HOME=%DIRNAME%\..\3rd-party\zookeeper-3.4.9
set KAFKA_HOME=%DIRNAME%\..\3rd-party\kafka

cd ~dp0

if NOT '%1' == 'do-reset' goto startServices

:doReset
echo STOP ZOOKEEPER
call %DIRNAME%\zookeeper-stop.bat
echo DELETE ZOOKEEPER FOLDER
rmdir /S /Q %DIRNAME%\zookeeper-data
echo DELETE KAFKA TMP FOLDER
rmdir /S /Q %DIRNAME%\..\3rd-party\tmp

goto startServices

:startServices
START /B "Zookeeper" "CMD /C %DIRNAME%\zookeeper-console.bat >> zookeeper.log"
REM cls
echo Zookeeper started

pushd %KAFKA_HOME%
%KAFKA_HOME%\bin\windows\kafka-server-start.bat %DIRNAME%\kafka_server.properties
popd