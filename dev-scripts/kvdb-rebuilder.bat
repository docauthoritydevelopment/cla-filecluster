@ECHO off
REM ##########################################################################
REM
REM  Run KVDB rebuilder
REM
REM usage:
REM only one parameter which is path to place the new kvdb
REM
REM ##########################################################################

set argC=0
for %%x in (%*) do Set /A argC+=1

IF %argC% == 1 goto argsok
IF %argC% == 2 goto argsok
goto usage

:argsok
REM echo Building kvdb-rebuild
pushd ..\kvdb-rebuild
REM call ..\gradlew kvdbRebuild

set VERSION=3.0.0

set LIB_ROOT=.\kvdb-rebuild\build\libs\

set "DEFAULT_JVM_OPTS=-Dfile.encoding=UTF8"

@rem set "JAVA_OPTS=-Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=y,address=8000"
set "JAVA_OPTS="

set KVDB_REBUILDER_JAR_PATH=%LIB_ROOT%.\kvdb-rebuild-%VERSION%.jar
set CONFIGPATH=.\kvdb-rebuild\config
@rem Execute cla-filecluster
echo Running kvdb-rebuild-%VERSION% on %*
java %DEFAULT_JVM_OPTS% %JAVA_OPTS% -DconfigPath="%CONFIGPATH%" -jar "%KVDB_REBUILDER_JAR_PATH%" rebuild-kvdb %*

goto finally

:usage
echo.
echo DocAuthority KVDB rebuilder tool
echo.
echo Usage:
echo     %0 [new-kvdb-path] [store-prefix]
echo.
echo     [new-kvdb-path]: The path where the new KVDB should be created
echo     [store-prefix]:  Optional store-prefix string. Default value: appserver
echo.
echo Examples:
echo     %0 ..\data\fc_data\new_kvdb
echo     %0 temp\new_mp_kvdb media-proc
goto finally


:finally
popd