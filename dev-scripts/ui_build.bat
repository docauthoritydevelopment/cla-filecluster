@ECHO OFF
set DIRNAME=%~dp0
set HOME_DIR=%DIRNAME%..\..\DocAuthority-UI
set LOCAL_ENV=server\config\local.env
IF NOT EXIST %HOME_DIR%\ GOTO bad_folder
pushd %HOME_DIR%
call grunt buildDev
call grunt packDev
ECHO UI Build done
ECHO See content of %LOCAL_ENV% :
type %LOCAL_ENV%
popd
goto finally

:bad_folder
ECHO Missing UI home dir: %HOME_DIR%

:finally
exit /b 0
