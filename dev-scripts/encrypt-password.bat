@echo off

set argC=0
for %%x in (%*) do Set /A argC+=1

IF %argC% == 0 goto usage
IF %argC% gtr 2 goto usage

set DIRNAME=%~dp0
set HOME_DIR=%DIRNAME%..\utils


set LIB_FOLDER=%HOME_DIR%\build\libs
set VERSION=3.0.0
SET CP=-cp .;%LIB_FOLDER%\utils-%VERSION%.jar;%LIB_FOLDER%\*

call java %CP% com.cla.filecluster.utils.EncMain %*

goto finally

:usage
echo.
echo DocAuthority encrypt password tool
echo.
echo Usage:
echo     %0 [password]
echo     %0 [password] [seed]
echo.
echo     [password]         - The password to be encrypted (mandatory)
echo     [seed] 	       - The seed to use for encryption
goto finally

:finally