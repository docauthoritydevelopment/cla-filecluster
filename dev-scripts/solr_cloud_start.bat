set DIRNAME=%~dp0
set CLA_HOME=%DIRNAME%\..\solr-7.7.2
set SOLR_HOME=%DIRNAME%\..\solrCloud
set LOG4J_FILE=%DIRNAME%\..\solr\conf\log4j.properties
set SOLR_JAVA_MEM=-Xms2g -Xmx2g
set ZK_CLIENT_TIMEOUT=60000

if not "%CUSTOM_SOLR_DATA_DIR%" == "" goto dataDirDef

if not exist "D:\Data\SolrData7" goto noData1
echo "CUSTOM SOLR DATA DIR FOUND at D:\Data\SolrData7"
set CUSTOM_SOLR_DATA_DIR=D:\Data\SolrData7
goto dataDirDef

:noData1
if not exist "%CLA_HOME%\data" goto noData2
echo "CUSTOM SOLR DATA DIR FOUND NEAR SOLR"
set CUSTOM_SOLR_DATA_DIR=%CLA_HOME%\data
goto dataDirDef

:noData2
if not exist "D:\Data\SolrData2" goto noData3
echo "CUSTOM SOLR DATA DIR FOUND at D:\Data\SolrData2"
set CUSTOM_SOLR_DATA_DIR=D:\Data\SolrData2
goto dataDirDef

:noData3
set CUSTOM_SOLR_DATA_DIR=%SOLR_HOME%\data
goto dataDirDef

:dataDirDef
REM ###########################
REM Section added to allow Java 11 support in parallel to other branches using Java 8.
REM Itay R. May 2019, Solr7 migration
set JAVA11_HOME=%ProgramW6432%\Java\jdk-11.0.3
IF NOT EXIST "%JAVA11_HOME%" (
   echo Could not find Java 11 home at "%JAVA11_HOME%"
   exit /b 1
)
Echo Using Java 11!
set SOLR_JAVA_HOME=%JAVA11_HOME%
REM ## End of section added for Java 11 support
REM ###########################
echo CUSTOM_SOLR_DATA_DIR=%CUSTOM_SOLR_DATA_DIR%
echo ZK_CLIENT_TIMEOUT=%ZK_CLIENT_TIMEOUT%
rem -c starts Solr in cloud mode. Since -z is not supplied, a zookeeper instance is automatically created on port 8983+1000
rem set LOG4J_CONFIG=%LOG4J_FILE%
call "%CLA_HOME%\bin\solr.cmd" start -p 8983 -c -Dsolr.data.dir="%CUSTOM_SOLR_DATA_DIR%" -a "-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=18983" %1 %2 %3
