@ECHO ON
set DIRNAME=%~dp0
REM Stop Solr7 if running
set CLA_HOME=%DIRNAME%\..\solr-7.7.2
if "%SOLR_PORT%" == "" SET SOLR_PORT=8983
IF exist "%CLA_HOME%\bin\solr.cmd" (
    @ECHO Stopping 7.7.2
    call "%CLA_HOME%\bin\solr.cmd" stop -p %SOLR_PORT%
    call "%CLA_HOME%\bin\solr.cmd" stop -p 8983
    call "%CLA_HOME%\bin\solr.cmd" stop -p 8984
)
exit /B 0
rem tasklist /FI "WINDOWTITLE eq Tomcat"
rem taskkill /FI "WINDOWTITLE eq Tomcat"