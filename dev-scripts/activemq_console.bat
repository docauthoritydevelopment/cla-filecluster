set DIRNAME=%~dp0
set ACTIVEMQ_DIR=%DIRNAME%\..\3rd-party\apache-activemq-5.14.5
set ACTIVEMQ_CONF=%DIRNAME%\..\installer\activemq\conf
rem set ACTIVEMQ_SUNJMX_START=-Dcom.sun.management.jmxremote.port=7044 -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false

pushd %ACTIVEMQ_DIR%
call bin\activemq start
popd