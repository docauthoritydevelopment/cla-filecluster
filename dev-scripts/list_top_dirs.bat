@ECHO OFF
set DIRNAME=%~dp0
if [X%1]==[X] GOTO help

FOR /F "tokens=*" %%G IN ( 'dir /b %1' ) DO ( IF exist "%1\%%G\" echo %1\%%G )
goto end

:help
echo Usage:
echo %0 [path]

:end
exit /b 0
