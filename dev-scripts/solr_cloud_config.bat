@ECHO off
IF "%OS%"=="Windows_NT" setlocal enabledelayedexpansion enableextensions

set DIRNAME=%~dp0
set CLA_HOME=%DIRNAME%\..\solr-7.7.2
set SOLR_CORES=%DIRNAME%\..\solr
set SOLR_CLOUD_DIR=%DIRNAME%\..\solrCloud
set UPGRADE_TOOL=call %DIRNAME%\..\run_upgrade_scripts.bat
set ZK_CLIENT_TIMEOUT=60000

REM if [%1]==[CreateCore] GOTO createCore
if [%1]==[CreateCollection] GOTO createCollection
if [%1]==[CreateCollectionIfNeeded] GOTO createCollectionIfNeeded
if [%1]==[ConfigCore] GOTO ConfigCore
if [%1]==[ConfigCores] GOTO ConfigCores
REM if [%1]==[CreateCores] goto createCores
if [%1]==[CreateCollections] goto createCollections
if [%1]==[RemoveClusterState] goto RemoveClusterState
if [%1]==[MakeRoot] goto MakeRoot
if [%1]==[do-reset] goto Reset
if [%1]==[do-reset-embedded-zk] goto ResetEmbeddedZookeeper
if [%1]==[DeleteCollections] goto DeleteCollections
if [%1]==[ReloadCollections] goto ReloadCollections
if [%1]==[ReloadCollection] goto reloadCollection
if [%1]==[config-reload] goto configReloadCollections
if [%1]==[createCollections2] goto CreateCollections2
if [%1]==[reload-embedded-zk] goto ReloadEmbeddedZookeeper
if [%1]==[auto-config-embedded-zk] goto AutoConfigReloadEmbeddedZookeeper
if [%1]==[auto-config-zk] goto AutoConfigReloadExternalZookeeper
if [%1]==[print-collection-status] goto printCollectionsStatus
if [%1]==[get-async-operation-status] goto getAsyncOperationStatus
if [%1]==[split-shard] goto splitShard
if [%1]==[split-and-move-shard] goto splitAndMoveShard
if [%1]==[replicate-collection] goto replicateCollection
if [%1]==[force-leader] goto forceLeader
if [%1]==[list-aliases] goto listAlias
if [%1]==[create-alias] goto createAlias
if [%1]==[delete-alias] goto deleteAlias
if [%1]==[move-replica] goto moveReplica
if [%1]==[align-shards] goto alignShards
if [%1]==[migrate-collection] goto migrateCollection
if [%1]==[backup] goto backup
if [%1]==[backup-all] goto backupAll
if [%1]==[restore] goto restore
if [%1]==[restore-all] goto restoreAll
if [%1]==[remove-task-id] goto removeTaskId
if [%1]==[remove-all-task-ids] goto removeAllTaskIds
if [%1]==[plugin-upgrade] goto pluginUpgrade
if [%1]==[plugin-upload] goto pluginUpload
if [%1]==[plugin-add-runtime-lib] goto pluginAddRuntimeLib
if [%1]==[plugin-modify-runtime-lib] goto pluginModifyRuntimeLib
if [%1]==[plugin-delete-runtime-lib] goto pluginDeleteRuntimeLib
if [%1]==[plugin-add-component] goto pluginAddComponent
if [%1]==[plugin-modify-component] goto pluginModifyComponent
if [%1]==[plugin-delete-component] goto pluginDeleteComponent
if [%1]==[delete-down-replicas] goto deleteDownReplicas

echo No Valid Parameter given
goto help

:ReloadEmbeddedZookeeper
set zkAddress=localhost:9983
echo Configure cores
CALL %0 ConfigCores %zkAddress%
echo Reload cores on zookeeper
CALL %0 ReloadCollections %zkAddress%
goto end

:configReloadCollections
echo Configure cores
CALL %0 ConfigCores %2
echo Reload cores on zookeeper
CALL %0 ReloadCollections %2
goto end

:AutoConfigReloadEmbeddedZookeeper
set zkAddress=localhost:9983
set SHARDS=%2
goto doAutoConfigReloadEmbeddedZookeeper

:AutoConfigReloadExternalZookeeper
set zkAddress=localhost:2181/solr
set SHARDS=%2
goto doAutoConfigReloadEmbeddedZookeeper

:doAutoConfigReloadEmbeddedZookeeper
echo Configure cores with %SHARDS% shards
CALL %0 ConfigCores %zkAddress%
CALL %0 CreateCollectionIfNeeded %zkAddress% AnalysisData %SHARDS%
CALL %0 CreateCollectionIfNeeded %zkAddress% CatFiles %SHARDS%
CALL %0 CreateCollectionIfNeeded %zkAddress% ContentMetadata %SHARDS%
CALL %0 CreateCollectionIfNeeded %zkAddress% GrpHelper %SHARDS%
CALL %0 CreateCollectionIfNeeded %zkAddress% CatFolders %SHARDS%
CALL %0 CreateCollectionIfNeeded %zkAddress% BizList %SHARDS%
CALL %0 CreateCollectionIfNeeded %zkAddress% FileGroups %SHARDS%
CALL %0 CreateCollectionIfNeeded %zkAddress% Extraction %SHARDS%
CALL %0 CreateCollectionIfNeeded %zkAddress% FileContent %SHARDS%
CALL %0 CreateCollectionIfNeeded %zkAddress% FileState %SHARDS%
CALL %0 CreateCollectionIfNeeded %zkAddress% PVS %SHARDS%
CALL %0 CreateCollectionIfNeeded %zkAddress% Groups 1
CALL %0 CreateCollectionIfNeeded %zkAddress% Titles 1
CALL %0 CreateCollectionIfNeeded %zkAddress% UnmatchedContent %SHARDS%
goto end

:CreateCollections2
REM pushd %DIRNAME%\..
echo Create Base Collections
%UPGRADE_TOOL% solr %SOLR_ADDRESS% create-collection Groups Groups 2
echo TODO - create other base collections
echo Create Test Collections
%UPGRADE_TOOL% solr %SOLR_ADDRESS% create-collection TestGroups Groups 2
echo TODO - create other test collections
REM popd
goto end

:createCollectionIfNeeded
REM pushd %DIRNAME%\..
echo Create %3 Collection with %4 shards if needed
echo Create Base Collections
%UPGRADE_TOOL% solr %2 create-collection-if-needed %3 %3 %4 %5
%UPGRADE_TOOL% solr %2 create-collection-if-needed Test%3 %3 %4 %5
REM popd
goto end


:DeleteCollections
set SOLR_ADDRESS=%2
REM pushd %DIRNAME%\..
echo Delete all collections
%UPGRADE_TOOL% solr %SOLR_ADDRESS% delete-collections
REM popd
goto end

:reloadCollection
set SOLR_ADDRESS=%2
REM pushd %DIRNAME%\..
echo Reload collection %3
%UPGRADE_TOOL% solr %SOLR_ADDRESS% reload-collection %3
echo cores on zookeeper reloaded
REM popd
goto end

:ReloadCollections
set SOLR_ADDRESS=%2
REM pushd %DIRNAME%\..
echo Reload all collections
%UPGRADE_TOOL% solr %SOLR_ADDRESS% reload-collections
echo cores on zookeeper reloaded
REM popd
goto end

:ResetEmbeddedZookeeper
echo STOP SOLR IF RUNNING
call %DIRNAME%\solr_stop.bat
echo Remove Solr data dir (including internal zookeeper dir)
del /F /Q /S %SOLR_CLOUD_DIR%
echo Start Solr with internal zookeeepr
call %DIRNAME%\solr_cloud_start.bat
timeout 10
echo Configure the Collections
goto createCollections

:Reset
echo STOP SOLR IF RUNNING
call %DIRNAME%\solr_stop.bat
echo Remove Solr data dir (including internal zookeeper dir)
del /F /Q /S %SOLR_CLOUD_DIR%
echo Start Solr with external zookeeepr
pushd %DIRNAME%\..
call solr_cloud_start_zk.bat
popd
timeout 10
echo Configure the Collections
goto createCollections

:MakeRoot
IF '%2'=='' GOTO help
IF '%3'=='' GOTO help
call "%CLA_HOME%\bin\solr.cmd" zk mkroot %3 -z %2
goto end

:createCollection
echo Create %2 with %3 shards
call "%CLA_HOME%\bin\solr.cmd" create_collection -c %2 -d %SOLR_CORES%\%2 -n %2 -shards %3
echo Create Test%2
call "%CLA_HOME%\bin\solr.cmd" create_collection -c Test%2 -d %SOLR_CORES%\%2 -n Test%2 -shards %3
goto end

:ConfigCore
echo Config %2
call "%CLA_HOME%\bin\solr.cmd" zk upconfig -d %SOLR_CORES%\%2 -n %2 -z %3
echo Config Test%2
call "%CLA_HOME%\bin\solr.cmd" zk upconfig -d %SOLR_CORES%\%2 -n Test%2 -z %3
goto end

:RemoveClusterState
IF '%2'=='' GOTO help
call "%CLA_HOME%\bin\solr.cmd" zk rm /clusterstate.json -z %2
goto end

:ConfigCores
echo upload configuration to zookeeper
IF '%2'=='' GOTO help
CALL %0 ConfigCore AnalysisData %2
CALL %0 ConfigCore CatFiles %2
CALL %0 ConfigCore ContentMetadata %2
CALL %0 ConfigCore GrpHelper %2
CALL %0 ConfigCore CatFolders %2
CALL %0 ConfigCore BizList %2
CALL %0 ConfigCore FileGroups %2
CALL %0 ConfigCore Extraction %2
CALL %0 ConfigCore FileContent %2
CALL %0 ConfigCore FileState %2
CALL %0 ConfigCore Groups %2
CALL %0 ConfigCore PVS %2
CALL %0 ConfigCore Titles %2
CALL %0 ConfigCore UnmatchedContent %2
goto end

:createCollections
echo Create Collections with %2 shards
set SHARDS=%2
CALL %0 CreateCollection AnalysisData %SHARDS%
CALL %0 CreateCollection CatFiles %SHARDS%
CALL %0 CreateCollection ContentMetadata %SHARDS%
CALL %0 CreateCollection GrpHelper %SHARDS%
CALL %0 CreateCollection CatFolders %SHARDS%
CALL %0 CreateCollection BizList %SHARDS%
CALL %0 CreateCollection FileGroups %SHARDS%
CALL %0 CreateCollection Extraction %SHARDS%
CALL %0 CreateCollection FileContent %SHARDS%
CALL %0 CreateCollection FileState %SHARDS%
CALL %0 CreateCollection PVS %SHARDS%
CALL %0 CreateCollection Groups 1
CALL %0 CreateCollection Titles 1
CALL %0 CreateCollection UnmatchedContent %SHARDS%
goto end

:printCollectionsStatus
set SOLR_ADDRESS=%2
REM echo Printing collections' status
%UPGRADE_TOOL% solr %SOLR_ADDRESS% dump-collection-status
goto end

:deleteDownReplicas
set SOLR_ADDRESS=%2
REM echo Deleting all down-state replicas
%UPGRADE_TOOL% solr %SOLR_ADDRESS% delete-down-replicas
goto end

:getAsyncOperationStatus
set SOLR_ADDRESS=%2
set REQUEST_ID=%3
set VERBOSITY=%4
echo Get operation status %REQUEST_ID% / %VERBOSITY%
%UPGRADE_TOOL% solr %SOLR_ADDRESS% async-operation-status %REQUEST_ID% %VERBOSITY%
IF %ERRORLEVEL% NEQ 0 ECHO Exited with code %ERRORLEVEL%
goto end

:splitShard
set SOLR_ADDRESS=%2
set COLLECTION_NAME=%3
set SHARD=%4
%UPGRADE_TOOL% solr %SOLR_ADDRESS% split-shard %COLLECTION_NAME% %SHARD%
IF %ERRORLEVEL% NEQ 0 ECHO Exited with code %ERRORLEVEL%
goto end

:splitAndMoveShard
set SOLR_ADDRESS=%2
set COLLECTION_NAME=%3
set SHARD=%4
set TARGET_NODE=%5
set VERBOSITY=%6
%UPGRADE_TOOL% solr %SOLR_ADDRESS% split-and-move-shard %COLLECTION_NAME% %SHARD% %TARGET_NODE% %VERBOSITY%
IF %ERRORLEVEL% NEQ 0 ECHO Exited with code %ERRORLEVEL%
goto end


:replicateCollection
set SOLR_ADDRESS=%2
set COLLECTION_NAME=%3
set SHARD=%4
set TARGET_NODE=%5
IF '%5'=='' (
    set TARGET_NODE_STR=''
) ELSE (
    set TARGET_NODE_STR=to node %TARGET_NODE%
)
echo Replicating collection %COLLECTION_NAME% / %SHARD% %TARGET_NODE_STR%
%UPGRADE_TOOL% solr %SOLR_ADDRESS% replicate-collection %COLLECTION_NAME% %SHARD% %TARGET_NODE%
IF %ERRORLEVEL% NEQ 0 ECHO Exited with code %ERRORLEVEL%
goto end

:forceLeader
set SOLR_ADDRESS=%2
set COLLECTION_NAME=%3
set SHARD=%4
set TARGET_NODE=%5
echo Forcing %TARGET_NODE% to take lead for %COLLECTION_NAME%/%SHARD%
%UPGRADE_TOOL% solr %SOLR_ADDRESS% force-leader %COLLECTION_NAME% %SHARD% %TARGET_NODE%
IF %ERRORLEVEL% NEQ 0 ECHO Exited with code %ERRORLEVEL%
goto end

:listAlias
set SOLR_ADDRESS=%2
echo Listing collections aliases
%UPGRADE_TOOL% solr %SOLR_ADDRESS% list-aliases
IF %ERRORLEVEL% NEQ 0 ECHO Exited with code %ERRORLEVEL%
goto end

:createAlias
set SOLR_ADDRESS=%2
set ALIAS_NAME=%3
set COLLECTIONS_TO_ALIAS=%4
echo Creating alias %ALIAS_NAME% for collections %COLLECTIONS_TO_ALIAS%
%UPGRADE_TOOL% solr %SOLR_ADDRESS% create-alias %ALIAS_NAME% %COLLECTIONS_TO_ALIAS%
IF %ERRORLEVEL% NEQ 0 ECHO Exited with code %ERRORLEVEL%
goto end

:deleteAlias
set SOLR_ADDRESS=%2
set ALIAS_NAME=%3
echo Deleting alias %ALIAS_NAME%
%UPGRADE_TOOL% solr %SOLR_ADDRESS% delete-alias %ALIAS_NAME%
IF %ERRORLEVEL% NEQ 0 ECHO Exited with code %ERRORLEVEL%
goto end

:moveReplica
set SOLR_ADDRESS=%2
set COLLECTION_NAME=%3
set SHARD=%4
set SOURCE_NODE=%5
set TARGET_NODE=%6
echo Moving %COLLECTION_NAME% %SHARD% from %SOURCE_NODE% to %TARGET_NODE% %7 %8
%UPGRADE_TOOL% solr %SOLR_ADDRESS% move-replica %COLLECTION_NAME% %SHARD% %SOURCE_NODE% %TARGET_NODE% %7 %8
IF %ERRORLEVEL% NEQ 0 ECHO Exited with code %ERRORLEVEL%
goto end

:alignShards
set SOLR_ADDRESS=%2
set OUTFILE=%3
echo Generating collection/shard alignment script.
%UPGRADE_TOOL% solr %SOLR_ADDRESS% align-shards PVS,AnalysisData,ContentMetadata,GrpHelper %OUTFILE%
%UPGRADE_TOOL% solr %SOLR_ADDRESS% align-shards Extraction,CatFiles,BizList,FileGroups %OUTFILE% append
ECHO exit /b 0 >> %OUTFILE%
goto end

:alignDaShards
set SOLR_ADDRESS=%2
set OUTFILE=%3
echo Generating collection/shard alignment script.
%UPGRADE_TOOL% solr %SOLR_ADDRESS% align-shards %OUTFILE%
goto end

:migrateCollection
set SOLR_ADDRESS=%2
set SRC_COLLECTION=%3
set DEST_COLLECTION=%4
if [%5]==[create] (
    echo Creating %DEST_COLLECTION% with configuration %6 with %7 shards ...
    call "%CLA_HOME%\bin\solr.cmd" create_collection -c %DEST_COLLECTION% -d %6 -n %DEST_COLLECTION% -shards %7
    IF %ERRORLEVEL% NEQ 0 ECHO Exited with code %ERRORLEVEL%
    SET TEMP_BAT=align_%DEST_COLLECTION%.bat
    %UPGRADE_TOOL% solr %SOLR_ADDRESS% align-shards %SRC_COLLECTION%,%DEST_COLLECTION% %TEMP_BAT%
    CALL %TEMP_BAT% do-align
    set CURSOR_MARK="*"
    set PAGE_SIZE=%8
    echo Collection %DEST_COLLECTION% created
) else (
    set CURSOR_MARK=%5
    set PAGE_SIZE=%6
)
echo Migrating collection ...
%UPGRADE_TOOL% solr %SOLR_ADDRESS% migrate-collection %SRC_COLLECTION% %DEST_COLLECTION% %CURSOR_MARK% %PAGE_SIZE%
goto end

:backup
set SOLR_ADDRESS=%2
set COLLECTION=%3
set TARGET_PATH=%4
echo Backing up collection ...
%UPGRADE_TOOL% solr %SOLR_ADDRESS% backup %COLLECTION% %TARGET_PATH%
goto end

:backupAll
set SOLR_ADDRESS=%2
set TARGET_PATH=%3
echo Backing up all collections ...
%UPGRADE_TOOL% solr %SOLR_ADDRESS% backup-all %TARGET_PATH%
goto end

:restore
set RETURN_TO=restoreGo
goto preparations

:restoreGo
set SOLR_ADDRESS=%2
set COLLECTION=%3
set TARGET_PATH=%4
set NEW_COLLECTION_NAME=%5
echo Restoring collection ...
%UPGRADE_TOOL% solr %SOLR_ADDRESS% restore %COLLECTION% %TARGET_PATH% %NEW_COLLECTION_NAME%
goto end

:restoreAll
set RETURN_TO=restoreAllGo
goto preparations

:restoreAllGo
set SOLR_ADDRESS=%2
set TARGET_PATH=%3
set COLLECTION_SUFFIX=%4
echo Restoring all collections ...
%UPGRADE_TOOL% solr %SOLR_ADDRESS% restore-all %TARGET_PATH% %COLLECTION_SUFFIX%
goto end

:removeTaskId
set SOLR_ADDRESS=%2
set TASK_ID=%3
echo Removing task id ...
%UPGRADE_TOOL% solr %SOLR_ADDRESS% remove-task-id %TASK_ID%
goto end

:removeAllTaskIds
set SOLR_ADDRESS=%2
echo Removing all task ids ...
%UPGRADE_TOOL% solr %SOLR_ADDRESS% remove-all-task-ids
goto end

:pluginUpgrade
set SOLR_ADDRESS=%2
set COLLECTION=%3
set JAR=%4
set BLOB_NAME=%5
echo Upgrading plugin...
%UPGRADE_TOOL% solr %SOLR_ADDRESS% plugin-upgrade %COLLECTION% %JAR% %BLOB_NAME%
goto end

:pluginUpload
set SOLR_ADDRESS=%2
set COLLECTION=%3
set JAR=%4
set BLOB_NAME=%5
echo Uploading plugin...
%UPGRADE_TOOL% solr %SOLR_ADDRESS% plugin-upload %COLLECTION% %JAR% %BLOB_NAME%
goto end

:pluginAddRuntimeLib
set SOLR_ADDRESS=%2
set COLLECTION=%3
set VERSION=%4
set BLOB_NAME=%5
echo Configuring plugin: Adding binaries lib to collection config...
%UPGRADE_TOOL% solr %SOLR_ADDRESS% plugin-add-runtime-lib %COLLECTION% %VERSION% %BLOB_NAME%
goto end

:pluginModifyRuntimeLib
set SOLR_ADDRESS=%2
set COLLECTION=%3
set VERSION=%4
set BLOB_NAME=%5
echo Configuring plugin: Modifying binaries lib for collection...
%UPGRADE_TOOL% solr %SOLR_ADDRESS% plugin-modify-runtime-lib %COLLECTION% %VERSION% %BLOB_NAME%
goto end

:pluginDeleteRuntimeLib
set SOLR_ADDRESS=%2
set COLLECTION=%3
set VERSION=%4
set BLOB_NAME=%5
echo Remove plugin: Removing collection's configuration binaries lib...
%UPGRADE_TOOL% solr %SOLR_ADDRESS% plugin-delete-runtime-lib %COLLECTION% %VERSION% %BLOB_NAME%
goto end

:pluginAddComponent
set SOLR_ADDRESS=%2
set COLLECTION=%3
set COMPONENT_TYPE=%4
set PLUGIN_NAME=%5
set PLUGIN_CLASS=%6
set VERSION=%7
echo Adding plugin config to collection %COLLECTION%
%UPGRADE_TOOL% solr %SOLR_ADDRESS% plugin-add-component %COLLECTION% %COMPONENT_TYPE% %PLUGIN_NAME% %PLUGIN_CLASS% %VERSION%
goto end

:pluginModifyComponent
set SOLR_ADDRESS=%2
set COLLECTION=%3
set COMPONENT_TYPE=%4
set PLUGIN_NAME=%5
set PLUGIN_CLASS=%6
set VERSION=%7
echo Modifying plugin config to collection %COLLECTION%
%UPGRADE_TOOL% solr %SOLR_ADDRESS% plugin-modify-component %COLLECTION% %COMPONENT_TYPE% %PLUGIN_NAME% %PLUGIN_CLASS% %VERSION%
goto end

:pluginDeleteComponent
set SOLR_ADDRESS=%2
set COLLECTION=%3
set COMPONENT_TYPE=%4
set PLUGIN_NAME=%5
echo Removing plugin config from collection %COLLECTION%
%UPGRADE_TOOL% solr %SOLR_ADDRESS% plugin-delete-component %COLLECTION% %COMPONENT_TYPE% %PLUGIN_NAME%
goto end


:preparations
echo Please make sure solr data-dir contains:
echo    solr.xml
echo    zoo.cfg
echo    ..\lib\jars
pause
goto %RETURN_TO%


REM --- Add more options before this line

:help
echo Usage:
echo Create a single collection: %0 CreateCollection [collection-name] [shards]
echo Create Collections: %0 CreateCollections [shards]
echo Create collections (conditional): %0 CreateCollectionIfNeeded [zookeeper-address] [collection-name] [shards]
REM echo Create Cores: %0 CreateCores
echo Config Cores: %0 ConfigCores [zookeeper-address]
echo Remove Cluster State: %0 RemoveClusterState [zookeeperaddress]
echo Make Root: %0 MakeRoot [zookeeper-address] [root]
echo Upload configuration to zookeeper and reload: %0 auto-config-embedded-zk [shards]
echo Upload configuration to zookeeper and reload: %0 auto-config-zk [shards]
echo Print collections' status: %0 print-collection-status [zookeeper-address]
echo Print request status: %0 get-async-operation-status [zookeeper-address] [request-id] [optional: true for verbosity]. If both request-id and verbosity omitted, will show last failures.
echo Split shard: %0 split-shard [zookeeper-address] [collection-name] [shard] (ie solr_cloud_config.bat split-shard zk-ip:port AnalysisData shard1)
echo Split and move shard: %0 split-and-move-shard [zookeeper-address] [collectionName] [shard] [targetNode] [optional: timeout-sec] [optional: true for verbosity] (ie solr_cloud_config.bat split-and-move-shard zk-ip:port Titles shard1_0 node-ip:port_solr)
echo Add replica: %0 replicate-collection [zookeeper-address] [collection-name] [shard] [optional: target-node] (ie solr_cloud_config.bat replicate-collection zk-ip:port AnalysisData shard1_1 node-ip:port_solr)
echo Force replica to lead: %0 force-leader [zookeeper-address] [collection-name] [shard] [target-node]
echo     Example: solr_cloud_config.bat force-leader zk-ip:port AnalysisData shard1_1 node-ip:port_solr
echo List collection alias: %0 list-aliases [zookeeper-address]
echo Create/update collection alias: %0 create-alias [zookeeper-address] [alias] [collection-names]
echo Deleted collection alias: %0 delete-alias [zookeeper-address] [alias]
echo Move replica: %0 move-replica [zookeeper-address] [collection-name] [shard-name] [source-node] [target-node] [optional: timeout-sec] [optional: 'ok-if-exists'][optional: true for verbosity] (Note: nodes end with '_solr') (ie solr_cloud_config.bat move-replica localhost:2181 Titles shard1 localhost:8983_solr localhost:8984_solr true)
echo Align shards: %0 align-shards [zookeeper-address] [dest_file]
echo Migrate collection: %0 migrate-collection [zookeeper-address] [src-collection-name] [dest-collection-name] [optional: 'create' conf-dir shards] [optional: cursor=from-cursor-mark]
echo     Example: solr_cloud_config.bat migrate-collection zk-ip:port Extraction Extraction-mod create 1        - creates the new collection with 1 shard
echo              solr_cloud_config.bat migrate-collection zk-ip:port Extraction Extraction-mod cursor=A123BCD  - continue the migration from the given cursor mark
echo Backup collection: %0 backup [zookeeper-address] [collection] [backup directory]
echo Backup all collections: %0 backup-all [zookeeper-address] [backup directory]
echo Restore backed-up collection: %0 restore [zookeeper-address] [collection] [backup directory] [optional: collection new name]
echo Restore all backed-up collections: %0 restore-all [zookeeper-address] [backup directory] [optional: suffix to add to collection names]
echo Upgrade plugin: %0 plugin-upgrade [zookeeper-address] [collection-name] [jar] [optional: blob name (Default: daplugins)]
echo Upload plugins binaries: %0 plugin-upload [zookeeper-address] [collection-name] [jar] [optional: blob name (Default: daplugins)]
echo Add new runtime library: %0 plugin-add-runtime-lib [zookeeper-address] [collection-name] [version] [optional: blob name (Default: daplugins)]
echo Modify runtime library: %0 plugin-modify-runtime-lib [zookeeper-address] [collection-name] [version] [optional: blob name (Default: daplugins)]
echo Remove runtime library: %0 plugin-delete-runtime-lib [zookeeper-address] [collection-name] [optional: blob name (Default: daplugins)]
echo Add new plugin config: %0 plugin-add-component [zookeeper-address] [collection-name] [component type: REQUEST_HANDLER/SEARCH_COMPONENT/QUERY_PARSER] [plugin name] [plugin class] [version]
echo Modify plugin config: %0 plugin-modify-component [zookeeper-address] [collection-name] [component type: REQUEST_HANDLER/SEARCH_COMPONENT/QUERY_PARSER] [plugin name] [plugin class] [version]
echo Remove plugin config: %0 plugin-delete-component [zookeeper-address] [collection-name] [component type: REQUEST_HANDLER/SEARCH_COMPONENT/QUERY_PARSER] [plugin name]



REM echo Reset solr configuration: %0 do-reset
REM echo Reset solr configuration: %0 do-reset-embedded-zk
REM echo Remove task id: %0 remove-task-id [zookeeper-address] [task-id]
REM echo Remove all task ids: %0 remove-all-task-ids [zookeeper-address]

goto end

:end
exit /b 0