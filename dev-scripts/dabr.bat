@echo off

REM Simple Backup/Restore utility to avoid restarting from scratch

echo.
set DEV_SCRIPTS_DIR=%~dp0

if  [%1]==[-b] goto backup
if  [%1]==[-r] goto restore

goto usage

:backup
if [%2]==[] goto usage
if [%3]==[] goto usage
echo ################# Preparing %3 backup #################
echo.
mkdir "%3"
pushd "%3"
for /f "delims=" %%A in ('cd') do (
     set backupname=%%~nxA
    )
mysqldump -h localhost -u root -P 3306 -B %2 -proot --routines > "%backupname%.sql"
xcopy "%DEV_SCRIPTS_DIR%\..\solr\data" "solr\data\" /s /q
xcopy "%DEV_SCRIPTS_DIR%\..\da-media-proc\data\kvdb" "media-proc\kvdb\" /s /q
xcopy "%DEV_SCRIPTS_DIR%\..\appserver\data\kvdb" "appserver\kvdb\" /s /q
cd ..
7z a "%backupname%.zip" "%backupname%"
rmdir "%backupname%" /s /q
popd

echo.
echo ################# Backup done! #################
goto finally

:restore
echo ################# Restoring %3 backup #################
echo.
if [%3]==[] goto usage

set ARCHIVE_FILE=%3
if /I "%ARCHIVE_FILE:~-4%" == ".ZIP" (
    set ARCHIVE_FILE=%ARCHIVE_FILE:~0,-4%
)

call "%DEV_SCRIPTS_DIR%\solr_stop.bat"
rmdir ..\solr\data /s /q
7z x "%ARCHIVE_FILE%.zip" -o"%ARCHIVE_FILE%\.."
pushd "%ARCHIVE_FILE%"
for /f "delims=" %%A in ('cd') do (
     set backupname=%%~nxA
    )
echo.
echo -- restore solr
rmdir "%DEV_SCRIPTS_DIR%\..\solr\data\" /s /q
mkdir "%DEV_SCRIPTS_DIR%\..\solr\data\"
xcopy "solr\data" "%DEV_SCRIPTS_DIR%\..\solr\data\"  /s /q /y
echo.
echo -- restore media-proc kvdb
rmdir "%DEV_SCRIPTS_DIR%\..\da-media-proc\data\kvdb\" /s /q
mkdir "%DEV_SCRIPTS_DIR%\..\da-media-proc\data\kvdb\"
xcopy "media-proc\kvdb" "%DEV_SCRIPTS_DIR%\..\da-media-proc\data\kvdb\"  /s /q /y
echo.

echo -- restore appserver kvdb
rmdir "%DEV_SCRIPTS_DIR%\..\appserver\data\kvdb\" /s /q
mkdir "%DEV_SCRIPTS_DIR%\..\appserver\data\kvdb\"
xcopy "appserver\kvdb" "%DEV_SCRIPTS_DIR%\..\appserver\data\kvdb\" /s /q /y
echo.

echo -- restore mysql
mysql -uroot -proot  --execute="drop database if exists %2;create database %2"
mysql -uroot -proot %2 < "%backupname%.sql"
popd
rmdir "%ARCHIVE_FILE%" /s /q
call "%DEV_SCRIPTS_DIR%\..\solr_run_start.bat"
echo.
echo ################# Restore done! #################
goto finally

:usage
echo Usage:
echo      "%0 [-b|-r] <db> <backup_name>"

:finally
