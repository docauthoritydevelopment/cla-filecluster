@ECHO off
REM ##########################################################################
REM  Activate MySql internal 'mysqlslap' benchmark tool
REM ##########################################################################
setlocal
SET ITERATIONS=5
SET /a NUM1=%1+0
IF NOT '%NUM1%' == '0' (
	SET ITERATIONS=%NUM1%
	SHIFT
)
if 'X%1' == 'X' goto usage
if 'X%2' == 'X' goto usage

set DIRNAME=%~dp0
SET EXEC=mysqlslap.exe
set MYSQL_BIN=%DIRNAME%runtime\MySql
IF NOT EXIST "%MYSQL_BIN%\%EXEC%" set MYSQL_BIN=C:\Program Files\MySQL\MySQL Server 5.7\bin
IF NOT EXIST "%MYSQL_BIN%\%EXEC%" (
  ECHO Can not find %EXEC% - abort
  goto :end
)
SET XFIELDS=30
SET YFIELDS=30
SET CONC=10
SET NUMQ=400
SET HOST=localhost
SET PORT=
if NOT 'X%3' == 'X' SET HOST=%3
if NOT 'X%4' == 'X' SET PORT=--port=%4

SET CLINE=%EXEC% -h %HOST% %PORT% -u %1 --auto-generate-sql -i %ITERATIONS% -x %XFIELDS% -y %YFIELDS% --number-of-queries=%NUMQ% --concurrency=%CONC%

ECHO %CLINE%
"%MYSQL_BIN%"\%CLINE% --password=%2 

goto end

:usage
ECHO Usage:
ECHO   %0 [num-iterations] user password [hostname] [port]

:end
endlocal
exit /b
