set DIRNAME=%~dp0
set HOME_DIR=%DIRNAME%\..\appserver
pushd %HOME_DIR%
call gradlew build -x test
call run_cla_server.bat
popd
