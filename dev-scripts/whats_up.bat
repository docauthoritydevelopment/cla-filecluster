@ECHO off
setlocal enabledelayedexpansion

FOR /F "tokens=2,4" %%F IN ('%WINDIR%\system32\netstat.exe -p tcp -a -n') DO (
	set BB=%%F
	REM echo f=%%F
	REM echo g=%%G
	REM ECHO %%G!BB:~-5!
	if "x%%G!BB:~-5!"=="xLISTENING:8080" echo AppServer
	if "x%%G!BB:~-5!"=="xLISTENING:9000" echo UI
	if "x%%G!BB:~-5!"=="xLISTENING:9001" echo UI-9001
	if "x%%G!BB:~-5!"=="xLISTENING:3306" echo MySql
	if "x%%G!BB:~-5!"=="xLISTENING:2181" echo ZooKeeper
	if "x%%G!BB:~-5!"=="xLISTENING:8983" echo Solr
	if "x%%G!BB:~-6!"=="xLISTENING:61616" echo ActiveMQ
	if "x%%G!BB:~-5!"=="xLISTENING:8089" echo Media Processor
)
endlocal
pause

