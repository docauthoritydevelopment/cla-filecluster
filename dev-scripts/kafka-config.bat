@echo off
echo off
set DIRNAME=%~dp0
set HOME_DIR=%DIRNAME%\..\3rd-party\kafka
set "TOPIC_COMMAND=%HOME_DIR%\bin\windows\kafka-topics.bat"
set "CREATE_OPTS=--create --zookeeper localhost:2181 --replication-factor 1"
set "DELETE_OPTS=--delete --if-exists --zookeeper localhost:2181 --topic "

echo CREATE SCAN_REQUESTS
call %TOPIC_COMMAND% %CREATE_OPTS% --partitions 2 retention.ms=600000 --topic scan.requests
echo CREATE SCAN_RESULTS
call %TOPIC_COMMAND% %CREATE_OPTS% --partitions 10 retention.ms=600000 --topic scan.results
echo CREATE INGEST_REQUESTS
call %TOPIC_COMMAND% %CREATE_OPTS% --partitions 10 retention.ms=600000 --topic ingest.requests
echo CREATE INGEST_RESULTS
call %TOPIC_COMMAND% %CREATE_OPTS% --partitions 10 retention.ms=600000 --topic ingest.results
echo CREATE CTRL REQUESTS
call %TOPIC_COMMAND% %CREATE_OPTS% --partitions 1 retention.ms=600000 --topic control.requests
echo CREATE CTRL RESULTS
call %TOPIC_COMMAND% %CREATE_OPTS% --partitions 1 retention.ms=600000 --topic control.results
echo CREATE FILE_CONTENT_REQUESTS
call %TOPIC_COMMAND% %CREATE_OPTS% --partitions 4 retention.ms=180000 --topic filecontent.requests
echo CREATE FILE_CONTENT_RESULTS
call %TOPIC_COMMAND% %CREATE_OPTS% --partitions 4 retention.ms=180000 --topic filecontent.results

echo DONE
