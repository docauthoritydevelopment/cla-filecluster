@ECHO OFF
set DIRNAME=%~dp0
set HOME_DIR=%DIRNAME%\..\..\DocAuthority-UI
set LOCAL_ENV=%HOME_DIR%\server\config\local.env
IF NOT EXIST %HOME_DIR%\ GOTO bad_folder
pushd %HOME_DIR%
node server\app.js
popd
goto finally

:bad_folder
ECHO Missing UI home dir: %HOME_DIR%

:finally
exit /b 0
