@echo off
if NOT '%1' == 'do-reset' goto usage
set DIRNAME=%~dp0
set UPG_DIR=%DIRNAME%\..\upgradeTools

echo Remove appserver kvdb
del /q %DIRNAME%\..\appserver\data\kvdb\*

echo Remove media-proc kvdb
del /q %DIRNAME%\..\da-media-proc\data\kvdb\*

pushd %UPG_DIR%

echo BUILD UGRADE-TOOL
call ..\gradlew build -x test

popd

echo DROP SCHEMA
pushd %DIRNAME%\..
call run_upgrade_scripts.bat drop-schema "jdbc:mysql://localhost:3306/da" root root
popd

echo BUILD APPSERVER
pushd %DIRNAME%\..\appserver
call ..\gradlew build -x test

echo RUN RESET
call .\reset-entities-schema.bat do-reset
echo RESET Exit Code is %errorlevel%

popd

echo RUN UPGRADE SCRIPTS BASELINE
pushd %DIRNAME%\..
call run_upgrade_scripts.bat baseline "jdbc:mysql://localhost:3306/da" root root
popd

goto finally

:usage
echo Usage:
echo        %0 do-reset
goto finally

:finally
