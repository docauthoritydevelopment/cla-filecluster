@echo off
echo off
set HOME_DIR=..\3rd-party\kafka

call %HOME_DIR%\bin\windows\kafka-topics.bat --zookeeper localhost:2181 --alter --topic scan.requests --config retention.ms=1000
call %HOME_DIR%\bin\windows\kafka-topics.bat --zookeeper localhost:2181 --alter --topic scan.results --config retention.ms=1000
call %HOME_DIR%\bin\windows\kafka-topics.bat --zookeeper localhost:2181 --alter --topic ingest.requests --config retention.ms=1000
call %HOME_DIR%\bin\windows\kafka-topics.bat --zookeeper localhost:2181 --alter --topic ingest.results --config retention.ms=1000
call %HOME_DIR%\bin\windows\kafka-topics.bat --zookeeper localhost:2181 --alter --topic filecontent.requests --config retention.ms=1000
call %HOME_DIR%\bin\windows\kafka-topics.bat --zookeeper localhost:2181 --alter --topic filecontent.results --config retention.ms=1000
call %HOME_DIR%\bin\windows\kafka-topics.bat --zookeeper localhost:2181 --alter --topic control.requests --config retention.ms=1000
call %HOME_DIR%\bin\windows\kafka-topics.bat --zookeeper localhost:2181 --alter --topic control.results --config retention.ms=1000

timeout 2

call %HOME_DIR%\bin\windows\kafka-topics.bat --zookeeper localhost:2181 --alter --topic scan.requests --config retention.ms=600000
call %HOME_DIR%\bin\windows\kafka-topics.bat --zookeeper localhost:2181 --alter --topic scan.results --config retention.ms=600000
call %HOME_DIR%\bin\windows\kafka-topics.bat --zookeeper localhost:2181 --alter --topic ingest.requests --config retention.ms=600000
call %HOME_DIR%\bin\windows\kafka-topics.bat --zookeeper localhost:2181 --alter --topic ingest.results --config retention.ms=600000
call %HOME_DIR%\bin\windows\kafka-topics.bat --zookeeper localhost:2181 --alter --topic filecontent.requests --config retention.ms=600000
call %HOME_DIR%\bin\windows\kafka-topics.bat --zookeeper localhost:2181 --alter --topic filecontent.results --config retention.ms=600000
call %HOME_DIR%\bin\windows\kafka-topics.bat --zookeeper localhost:2181 --alter --topic control.requests --config retention.ms=600000
call %HOME_DIR%\bin\windows\kafka-topics.bat --zookeeper localhost:2181 --alter --topic control.results --config retention.ms=600000

