@ECHO off
REM ##########################################################################
REM
REM  Run Root Folder Shifter
REM
REM ##########################################################################

set argC=0
for %%x in (%*) do Set /A argC+=1

IF NOT %argC% == 2 goto usage

echo Building rf-shifter
pushd ..\rf-shifter
call ..\gradlew rfShifter

set VERSION=3.0.0

set LIB_ROOT=.\build\libs\

set "DEFAULT_JVM_OPTS=-Dfile.encoding=UTF8"

@rem set "JAVA_OPTS=-Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=y,address=8000"
set "JAVA_OPTS="

set RF_SHIFTER_JAR_PATH=%LIB_ROOT%.\rf-shifter-tool-%VERSION%.jar
@rem Execute cla-filecluster
echo Running rf-shifter-tool-%VERSION%.jar on %*
java %DEFAULT_JVM_OPTS% %JAVA_OPTS% -jar "%RF_SHIFTER_JAR_PATH%" "..\appserver\config" %*

goto finally

:usage
echo. 
echo DocAuthority Root Folder shifting tool
echo. 
echo Usage:
echo     %0 [old-rf-path] [new-rf-path]
echo.
echo     [old-rf-path]: The old root folder we wish to remove
echo     [new-rf-path]: The new root folder we wish to move to
goto finally

:finally
popd